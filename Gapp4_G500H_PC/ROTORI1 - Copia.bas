Attribute VB_Name = "ROTORI1"
Option Explicit
'
Global MaxEr     As Single
Global MinEr     As Single
'
Global CorXX()   As Single
Global CorYY()   As Single
Global ERPU(999) As Single
Global Ri(999)   As Single
Global RS(999)   As Single
Global Rd(999)   As Single
Global PI(999)   As Single
Global PS(999)   As Single
Global PD(999)   As Single
Global errPNT()  As Single

Dim Session      As IMCDomain
'
Public Const EM_GETLINECOUNT = &HBA
Public Const EM_LINEFROMCHAR = &HC9
Public Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any) As Long

Private ParProfiloRot   As StructParProfiloROT
Public ParMolaROT       As StructParMolaROT
'
Global Const nMAXpntG4 = 400    'LIMITE MAX PUNTI PROFILO ROTORE PER GAPP4
Global Const nMaxpntROT = 401   'LIMITE MAX PUNTI PROFILO MOLA PER ROTORI
Global Const NmaxPntACV3 = 94   'LIMITE MAX PUNTI PROFILO ROTORE PER ACV3
Public MaxLcontRotori As Double
'
Public Type StructParProfiloROT
  'PARAMETRI GENERALI PROFILO ROTORE
  NDenteLOC                   As Integer  ' ------ : Numero Denti
  sNDenteLOC                  As String   ' ------ :
  DiaINT                      As Double   ' ------ : Diametro interno (Rotore)
  sDiaInt                     As String   ' ------ :
  DIAMETRO_PRIMITIVO          As Double   ' ------ : Diametro primitivo (Ingranaggio)
  sDIAMETRO_PRIMITIVO         As String   ' ------ :
  ELICA                       As Double   ' ------ : Inclinazione Mola (= angolo elica rettifica)
  sELICA                      As String   ' ------ :
  PASSO                       As Double   ' ------ : Passo Elica (Assiale)
  sPASSO                      As String   ' ------ :
  Rotazione                   As Double   ' ------ : Rotazione profilo
  sRotazione                  As String   ' ------ :
  CORREZIONE_ALTEZZA_DENTE    As Double   ' ------ :
  sCORREZIONE_ALTEZZA_DENTE   As String   ' ------ :
  CorSDENTE                   As Double   ' ------ :
  sCorSDENTE                  As String   ' ------ :
  NpTot                       As Integer  ' ------ :
  sNpTot                      As String   ' ------ :
  NPF1                        As Integer  ' ------ :
  sNpF1                       As String   ' ------ :
  EAP_F1(4)                   As Double   ' ------ :
  EAP_F2(4)                   As Double   ' ------ :
  RR_F1(4)                    As Double   ' ------ :
  RR_F2(4)                    As Double   ' ------ :
  BB_F1(4)                    As Double   ' ------ :
  BB_F2(4)                    As Double   ' ------ :
End Type
'
'PARAMETRI MOLA
Public Type StructParMolaROT
  DEDF(2)      As Double    'CORREZIONE DEDENDUM F1 E F2
  ADDF(2)      As Double    'CORREZIONE ADDENDUM F1 E F2
  CorSMOLA     As Double    'CORREZIONE DI SPESSORE MOLA
  CorX()       As Double    'CORREZIONE ASSIALE PUNTI
  corY()       As Double    'CORREZIONE RADIALE PUNTI
  XX()         As Double    ' MOLA : XX senza correzioni mola
  YY()         As Double    ' ---- : YY senza correzioni mola
  TT()         As Double    ' ---- : TT senza correzioni mola
  LMax         As Double    ' Contatto Massimo
  iLMax        As Integer   ' Punto contatto Massimo
End Type

Sub RotVec(X As Double, Y As Double, angle As Double, ByRef X1 As Double, ByRef Y1 As Double)
'
Dim RotZRad     As Double
Dim angleFormer As Double
Dim angleLater  As Double
Dim R           As Double
Dim half_PI     As Double
'
On Error GoTo errRotVec
  '
  RotZRad = angle / 180 * PG
  half_PI = PG / 2
  R = Sqr(X * X + Y * Y)
  '
  If (X = 0) Then
    If (Y > 0) Then
      angleFormer = PG / 2
    Else
      angleFormer = PG / 2
    End If
  Else
    If (X > 0) Then
      angleFormer = Atn(Y / X)
    Else
      angleFormer = Atn(Y / X) + PG
    End If
  End If
      angleLater = angleFormer + RotZRad
    If (angleLater = half_PI) Then
      X1 = 0
      Y1 = R
  ElseIf (angleLater = -half_PI) Then
      X1 = 0
      Y1 = -R
  Else
      X1 = R * Cos(angleLater)
      Y1 = R * Sin(angleLater)
  End If
  '
Exit Sub

errRotVec:
  If (FreeFile > 0) Then Close
  i = Err
  StopRegieEvents
  MsgBox Error$(i), 48, "SUB: RotVec"
  ResumeRegieEvents
  Exit Sub

End Sub

Public Function CurrentLine(ByRef txtBox As TextBox) As Long
'
On Error Resume Next
  '
  CurrentLine = SendMessage(txtBox.hwnd, EM_LINEFROMCHAR, -1&, ByVal 0&) + 1
  On Error GoTo 0
  '
End Function

Public Function NoOfLines(ByRef txtBox As TextBox) As Long
'
On Error Resume Next
  '
  NoOfLines = SendMessage(txtBox.hwnd, EM_GETLINECOUNT, 0&, ByVal 0&)
  On Error GoTo 0
  '
End Function

Sub SALVA_CASELLA_SU_FILE(ByVal CASELLA As Control, ByVal PathFILE As String)
'
Dim stmp As String
Dim ret  As Integer
Dim i    As Integer
Dim j    As Integer
Dim k    As Integer
'
On Error GoTo errSALVA_CASELLA_SU_FILE
  '
  If (Dir(Trim$(PathFILE)) <> "") Then
    ret = MsgBox(Error(58) & Chr(13) & Chr(13) & "continue" & " ?", 32 + 4 + 256, "WARNING: " & PathFILE)
    If (ret <> 6) Then Exit Sub
  End If
  'RIMUOVO FINE LINEA E NUOVA RIGA
  If (Len(CASELLA.Text) > 2) Then
    stmp = Right(CASELLA.Text, 2)
    i = InStr(stmp, Chr(13))
    j = InStr(stmp, Chr(10))
    ret = i + j
    Select Case ret
      Case 0: k = 0
      Case 1: k = 2
      Case 2: k = 1
      Case 3: k = 2
    End Select
  Else
    k = 0
  End If
  'SALVO IL CONTENUTO DELLA CASELLA
  Open Trim$(PathFILE) For Output As #1
    Print #1, Left$(CASELLA.Text, Len(CASELLA.Text) - k)
  Close #1
  '
Exit Sub

errSALVA_CASELLA_SU_FILE:
  If (FreeFile > 0) Then Close
  i = Err
  StopRegieEvents
  MsgBox Error$(i), 48, "SUB: SALVA_CASELLA_SU_FILE"
  ResumeRegieEvents
  Exit Sub

End Sub

Sub CALCOLO_CORPROP()
'
Dim PERCORSO           As String
Dim COORDINATE         As String
Dim RADICE             As String
Dim PERCORSO_CONTROLLI As String
Dim PERCORSO_PROFILI   As String
Dim sequenza           As String
Dim npt                As Integer
Dim TY_ROT             As String
Dim PASSO              As Double
Dim CONTROLLORE        As String
Dim DM_ROT             As String
Dim INVERSIONE         As Integer
'
On Error Resume Next
  '
  Call LEGGI_INFO_ROTORI(PERCORSO, COORDINATE, RADICE, PERCORSO_CONTROLLI, PERCORSO_PROFILI, sequenza, npt, TY_ROT, PASSO, CONTROLLORE, DM_ROT, INVERSIONE)
  '
  Dim NP1   As Integer
  Dim NP2   As Integer
  Dim NPM1  As Integer
  Dim NPM2  As Integer
  '
  NPM1 = val(GetInfo("CREAZIONE", "NpM_F1", RADICE & "\INFO.INI"))
  NPM2 = val(GetInfo("CREAZIONE", "NpM_F2", RADICE & "\INFO.INI"))
  NP1 = val(GetInfo("CREAZIONE", "NpC_F1", RADICE & "\INFO.INI"))
  NP2 = val(GetInfo("CREAZIONE", "NpC_F2", RADICE & "\INFO.INI"))
  '
  Dim Np    As Integer
  '
  Np = npt
  '
  'CALCOLO DELLE CORREZIONI PROPOSTE
  Dim corY() As Double
  Dim CorZ() As Double
  Dim ER()   As Double
  '
  Call CALCOLO_CORREZIONI(CorZ, corY, Np, npt, NP1, NP2, NPM1, NPM2, RADICE, TY_ROT, CONTROLLORE, ER)

End Sub

Sub LEGGI_CORREZIONI(ByVal CorrezioniMola As String)
'
Dim i     As Integer
Dim j     As Integer
Dim stmp  As String
Dim CorX  As String
Dim corY  As String
Dim Punto As String
'
On Error Resume Next
  '
  Dim COORDINATE         As String
  Dim RADICE             As String
  Dim PERCORSO           As String
  Dim PERCORSO_CONTROLLI As String
  Dim PERCORSO_PROFILI   As String
  Dim sequenza           As String
  Dim npt                As Integer
  Dim TY_ROT             As String
  Dim PASSO              As Double
  Dim CONTROLLORE        As String
  Dim DM_ROT             As String
  Dim INVERSIONE         As Integer
  '
  Call LEGGI_INFO_ROTORI(PERCORSO, COORDINATE, RADICE, PERCORSO_CONTROLLI, PERCORSO_PROFILI, sequenza, npt, TY_ROT, PASSO, CONTROLLORE, DM_ROT, INVERSIONE)
  '
  'INTESTAZIONI LISTE
  CHK0.Label7(3).Caption = "NO Check"
  CHK0.Label7(0).Caption = "nr."
  CHK0.Label7(1).Caption = "corr. X"
  CHK0.Label7(2).Caption = "corr. Y"
  CHK0.Label7(4).Caption = "ON  -> X" & Chr(13) & Chr(13) & "OFF -> -"
  'PULIZIA LISTE
  CHK0.List3(0).Clear
  CHK0.List3(1).Clear
  CHK0.List3(2).Clear
  CHK0.List3(3).Clear
  CHK0.List3(4).Clear
  'Correzioni profilo mola
  If (Len(CorrezioniMola) > 0) Then
    If (Dir$(RADICE & "\" & CorrezioniMola) <> "") Then
      Open RADICE & "\" & CorrezioniMola For Input As #1
      Open RADICE & "\ABILITA.COR" For Input As #2
        i = 1
        While (Not EOF(1)) And (i <= npt + 2)
          Line Input #1, stmp
          j = 0
          If j <= 0 Then j = InStr(2, stmp, "+")
          If j <= 0 Then j = InStr(2, stmp, "-")
          CorX = Left$(stmp, j - 1)
          corY = Mid$(stmp, j)
          'TOLGO LA VIRGOLA SE PRESENTE (CORPROP)
          j = InStr(CorX, ","): If (j > 0) Then CorX = Left$(CorX, j - 1)
          'indice del punto
          Call CHK0.List3(0).AddItem(Format$(i - 1, "#0"))
          Call CHK0.List3(1).AddItem(CorX)
          Call CHK0.List3(2).AddItem(corY)
          If (i = 1) Or (i = npt + 2) Then
              'Primo ed ultimo punto mola
              Call CHK0.List3(3).AddItem("X")
          Else
            Punto = GetInfo("PuntiControllo", "CHK[0," & CStr(i - 1) & "]", RADICE & "\CHKRAB.ROT")
            If (val(Punto) = 0) Then
              Call CHK0.List3(3).AddItem("X")
            Else
              Call CHK0.List3(3).AddItem(" ")
            End If
          End If
          ReDim Preserve CorXX(i)
          ReDim Preserve CorYY(i)
          CorXX(i) = val(CorX)
          CorYY(i) = val(corY)
          Line Input #2, stmp
          If (val(stmp) = 1) Then
            Call CHK0.List3(4).AddItem("X")
          Else
            Call CHK0.List3(4).AddItem("-")
          End If
          i = i + 1
        Wend
      Close #2
      Close #1
      CHK0.List3(0).ListIndex = 0
      CHK0.List3(1).ListIndex = 0
      CHK0.List3(2).ListIndex = 0
      CHK0.List3(3).ListIndex = 0
      CHK0.txtCORR(1).SetFocus
    Else
      StopRegieEvents
      MsgBox CorrezioniMola & ": " & Error(53), 48, "WARNING"
      ResumeRegieEvents
    End If
    '
  End If
  '
End Sub

Sub StampaChkG4_F1F2()
'
Dim i           As Integer
Dim j           As Integer
'Area di stampa
Dim MLeft       As Single
Dim MRight      As Single
Dim MTop        As Single
Dim MBottom     As Single
Dim Box0W       As Single
Dim Box0H       As Single
'Parametri per la stampa di linee
Dim X1          As Single
Dim Y1          As Single
Dim X2          As Single
Dim Y2          As Single
Dim ERX1        As Single
Dim ERY1        As Single
Dim ERX2        As Single
Dim ERY2        As Single
'Area di stampa dei grafici
Dim Box1L       As Single
Dim Box1T       As Single
Dim Box1W       As Single
Dim Box1H       As Single
Dim MerrMax     As Single

Dim PF1(nMAXpntG4)  As Single
Dim PF2(nMAXpntG4)  As Single
Dim RF1(nMAXpntG4)  As Single
Dim RF2(nMAXpntG4)  As Single

'Posizioni lista punti
Dim TopLista     As Single
Dim Left1Lista   As Single
Dim Left2Lista   As Single

Dim TabX        As Single
Dim PCYold      As Single
Dim Xerr        As Single
Dim errW        As Single
Dim iRG         As Single
Dim iCl         As Single
Dim fmt2        As String
Dim fmt3        As String
Dim DblTest     As Double
Dim stmp        As String
'
Dim nCol        As Integer
'
Dim NPF1        As Integer  ': num. punti F1 (tutti anche quelli nopn controllati)
Dim NPF2        As Integer  ': num. punti F2 (tutti anche quelli nopn controllati)
'
On Error Resume Next
  '
  Printer.ScaleMode = 7
  Call ImpostaPrinter("Arial", 8)
  MTop = 1: MBottom = 1 '2
  MLeft = 2: MRight = 1
  Box0W = Printer.ScaleWidth - (MLeft + MRight) '17
  Box0H = Printer.ScaleHeight - (MTop + MBottom) '27
  iRG = Printer.TextHeight("A")
  iCl = Printer.TextWidth("A")
  Box1L = 0: Box1T = 1
  Box1W = 17: Box1H = 16
  MerrMax = 1
  '
  iRG = Printer.TextHeight("A")
  iCl = Printer.TextWidth("A")
  '
  Dim COORDINATE         As String
  Dim RADICE             As String
  Dim PERCORSO           As String
  Dim PERCORSO_CONTROLLI As String
  Dim PERCORSO_PROFILI   As String
  Dim sequenza           As String
  Dim npt                As Integer
  Dim TY_ROT             As String
  Dim PASSO              As Double
  Dim CONTROLLORE        As String
  Dim DM_ROT             As String
  Dim INVERSIONE         As Integer
  '
  Call LEGGI_INFO_ROTORI(PERCORSO, COORDINATE, RADICE, PERCORSO_CONTROLLI, PERCORSO_PROFILI, sequenza, npt, TY_ROT, PASSO, CONTROLLORE, DM_ROT, INVERSIONE)
  '
  Dim NP1   As Integer
  Dim NP2   As Integer
  Dim NPM1  As Integer
  Dim NPM2  As Integer
  Dim ERPI  As Double
  '
  NPF1 = GetInfo("CREAZIONE", "NpM_F1", RADICE & "\INFO.INI")
  NPF2 = GetInfo("CREAZIONE", "NpM_F2", RADICE & "\INFO.INI")
  NP1 = GetInfo("CREAZIONE", "NpC_F1", RADICE & "\INFO.INI")
  NP2 = GetInfo("CREAZIONE", "NpC_F2", RADICE & "\INFO.INI")
  '
  'Call RicercaMinimoRotore(Radice & "\PRFROT", NPF1, NPF2)
  '
  Dim NEGTOL() As Single
  Dim POSTOL() As Single
  Dim MinToll  As Single
  Dim MaxToll  As Single
  '
  'Lettuta di prftol.dat
  MinToll = -999: MaxToll = -999
  Open RADICE & "\PRFTOL.DAT" For Input As #1
    For i = 1 To npt
      ReDim Preserve NEGTOL(i): ReDim Preserve POSTOL(i)
      Input #1, NEGTOL(i), POSTOL(i)
      If Abs(NEGTOL(i)) > MinToll Then MinToll = Abs(NEGTOL(i))
      If Abs(POSTOL(i)) > MaxToll Then MaxToll = Abs(POSTOL(i))
    Next i
  Close #1
  '
  If (TY_ROT = "AXIAL") And (CONTROLLORE <> "INTERNAL") Then
    For i = 1 To nMAXpntG4
      PF1(i) = PS(i): RF1(i) = RS(i)
      PF2(i) = PI(i): RF2(i) = Ri(i)
    Next i
  End If
  If (TY_ROT = "FRONT") Or (CONTROLLORE = "INTERNAL") Then
    For i = 1 To nMAXpntG4
      PF1(i) = PD(i): RF1(i) = Rd(i)
      PF2(i) = PS(i): RF2(i) = RS(i)
    Next i
  End If
  '
  fmt2 = "##0.0###"
  fmt3 = "#0.0###"
  '
  errW = 2 'LARGHEZZA LINEA ERRORI
  '
  TopLista = MTop + 1 + iRG
  Left1Lista = MLeft
  Left2Lista = Left1Lista + 8.5
  '
  'Squadratura della pagina
  X1 = MLeft: Y1 = MTop
  X2 = MLeft + Box0W: Y2 = MTop + Box0H
  Printer.Line (X1, Y1)-(X2, Y2), QBColor(0), B
  'Intestazione + linea sotto intestazione
  Call StampaChkG4_INTESTAZIONE(MLeft, MTop, X2)
  '
  'Legenda punti fuori tolleranza
  Printer.CurrentX = MLeft + iCl
  Printer.CurrentY = Box0H + 2 * iRG
  stmp = "* out of tollerance"
  Printer.Print stmp
  '
  'FIANCO 1
  nCol = 1: Y1 = TopLista
  'INTESTAZIONE
  'INDICE DEL PUNTO
  TabX = X1 + iCl
  Printer.CurrentX = TabX
  Printer.CurrentY = Y1
  Printer.Print "Nr  "
  'VALORI DEI RAGGI
  TabX = TabX + Printer.TextWidth("Nr  ") + iCl
  Printer.CurrentX = TabX
  Printer.CurrentY = Y1
  Printer.Print "R"
  'ERRORI rispetto al profilo medio
  TabX = TabX + Printer.TextWidth(fmt2) + iCl
  Printer.CurrentX = TabX
  Printer.CurrentY = Y1
  Printer.Print "Er.MF1"
  'Punti fuori tolleranza
  TabX = TabX + Printer.TextWidth(fmt3) + iCl
  Printer.CurrentX = TabX
  Printer.CurrentY = Y1
  Printer.Print ""
  'ERRORI E LINEA DEGLI ERRORI
  TabX = TabX + iCl + iCl
  Printer.CurrentX = TabX
  Printer.CurrentY = Y1
  Printer.Print "Er.TF1"
  GoSub LineaE0
  'PUNTI
  For i = 1 To NP1
    Y1 = Y1 + iRG
    'INDICE DEL PUNTO
    TabX = X1 + iCl
    Printer.CurrentX = TabX
    Printer.CurrentY = Y1
    Printer.Print Format$(PF1(i) - 1, "##0")
    'VALORE DEI RAGGI
    TabX = TabX + Printer.TextWidth("Nr  ") + iCl
    Printer.CurrentX = TabX
    Printer.CurrentY = Y1
    Printer.Print frmt(RF1(i), 4)
    'ERRORE rispetto al profilo medio
    TabX = TabX + Printer.TextWidth(fmt2) + iCl
    Printer.CurrentX = TabX
    Printer.CurrentY = Y1
    DblTest = ERPU(PF1(i)) + (POSTOL(PF1(i) - 1) - NEGTOL(PF1(i) - 1)) / 2
    Printer.Print frmt(DblTest, 4)
    'Punti fuori tolleranza
    TabX = TabX + Printer.TextWidth(fmt3) + iCl
    Printer.CurrentX = TabX
    Printer.CurrentY = Y1
    DblTest = ERPU(PF1(i))
        If (DblTest < 0) And (DblTest + POSTOL(PF1(i) - 1) < 0) Then
            Printer.Print "*"
    ElseIf (DblTest > 0) And (DblTest - NEGTOL(PF1(i) - 1) > 0) Then
            Printer.Print "*"
    Else
            Printer.Print ""
    End If
    'Posizione per Stampa errore
    Printer.CurrentY = PCYold
    Printer.CurrentX = TabX + iCl + iCl
    'Errore corrente
    DblTest = ERPU(PF1(i)): GoSub ErroreLinea
    'CONTROLLO SPAZIO OCCUPATO SU PAGINA
    If (Printer.CurrentY > (MTop + Box0H - 3 * iRG)) Then
      nCol = nCol + 1
      If (nCol / 2 <> Int(nCol / 2)) Then
        Printer.NewPage
        'Squadratura della pagina
        X1 = MLeft: Y1 = MTop
        X2 = MLeft + Box0W: Y2 = MTop + Box0H
        Printer.Line (X1, Y1)-(X2, Y2), QBColor(0), B
        'Intestazione + linea sotto intestazione
        Call StampaChkG4_INTESTAZIONE(MLeft, MTop, X2)
        'Legenda punti fuori tolleranza
        Printer.CurrentX = MLeft + iCl
        Printer.CurrentY = Box0H + 2 * iRG
        stmp = "item(8)"
        Printer.Print stmp
        X1 = Left1Lista: Y1 = TopLista
      Else
        X1 = Left2Lista: Y1 = TopLista
      End If
      'INDICE DEL PUNTO
      TabX = X1 + iCl
      Printer.CurrentX = TabX
      Printer.CurrentY = Y1
      Printer.Print "Nr"
      'VALORI DEI RAGGI
      TabX = TabX + Printer.TextWidth("Nr  ") + iCl
      Printer.CurrentX = TabX
      Printer.CurrentY = Y1
      Printer.Print "R"
      'ERRORI rispetto al profilo medio
      TabX = TabX + Printer.TextWidth(fmt2) + iCl
      Printer.CurrentX = TabX
      Printer.CurrentY = Y1
      Printer.Print "Er.MF1"
      'Punti fuori tolleranza
      TabX = TabX + Printer.TextWidth(fmt3) + iCl
      Printer.CurrentX = TabX
      Printer.CurrentY = Y1
      Printer.Print ""
      'ERRORI E LINEA DEGLI ERRORI
      TabX = TabX + iCl + iCl
      Printer.CurrentX = TabX
      Printer.CurrentY = Y1
      Printer.Print "Er.TF1"
      GoSub LineaE0
    End If
  Next i
  '
  'FIANCO 2
  Y1 = Y1 + 2 * iRG
  'INTESTAZIONE
  'INDICE DEL PUNTO
  TabX = X1 + iCl
  Printer.CurrentX = TabX
  Printer.CurrentY = Y1
  Printer.Print "Nr  "
  '** INTESTAZIONE F2 - VALORI DEI RAGGI
  TabX = TabX + Printer.TextWidth("Nr  ") + iCl
  Printer.CurrentX = TabX
  Printer.CurrentY = Y1
  Printer.Print "R"
  '** INTESTAZIONE F2 - ERRORI rispetto al profilo medio
  TabX = TabX + Printer.TextWidth(fmt2) + iCl
  Printer.CurrentX = TabX
  Printer.CurrentY = Y1
  Printer.Print "Er.MF2"
  '** INTESTAZIONE F2 - Punti fuori tolleranza
  TabX = TabX + Printer.TextWidth(fmt3) + iCl
  Printer.CurrentX = TabX
  Printer.CurrentY = Y1
  Printer.Print ""
  '** INTESTAZIONE F2 - ERRORI E LINEA DEGLI ERRORI
  TabX = TabX + iCl + iCl
  Printer.CurrentX = TabX
  Printer.CurrentY = Y1
  Printer.Print "Er.TF2"
  GoSub LineaE0
  For i = NP2 To 1 Step -1
    Y1 = Y1 + iRG
    '** F2 - INDICE DEL PUNTO
    TabX = X1 + iCl
    Printer.CurrentX = TabX
    Printer.CurrentY = Y1
    Printer.Print Format$(PF2(i) - 1, "##0")
    '** F2 - VALORE DEI RAGGI
    TabX = TabX + Printer.TextWidth("Nr  ") + iCl
    Printer.CurrentX = TabX
    Printer.CurrentY = Y1
    Printer.Print frmt(RF2(i), 4)
    '** F2 - ERRORE rispetto al profilo medio
    TabX = TabX + Printer.TextWidth(fmt2) + iCl
    Printer.CurrentX = TabX
    Printer.CurrentY = Y1
    DblTest = ERPU(PF2(i)) + (POSTOL(PF2(i) - 1) - NEGTOL(PF2(i) - 1)) / 2
    Printer.Print frmt(DblTest, 4)
    '** F2 - Punti fuori tolleranza
    TabX = TabX + Printer.TextWidth(fmt3) + iCl
    Printer.CurrentX = TabX
    Printer.CurrentY = Y1
    DblTest = ERPU(PF2(i))
    If DblTest < 0 And DblTest + POSTOL(PF2(i) - 1) < 0 Then
      Printer.Print "*"
    ElseIf DblTest > 0 And DblTest - NEGTOL(PF2(i) - 1) > 0 Then
      Printer.Print "*"
    Else
      Printer.Print ""
    End If
    '** F2 - Posizione per Stampa errore
    Printer.CurrentY = PCYold
    'Printer.CurrentX = TabX
    Printer.CurrentX = TabX + iCl + iCl
    '** F2 - Errore corrente
    DblTest = ERPU(PF2(i)): GoSub ErroreLinea
    'F2 - CONTROLLO SPAZIO OCCUPATO SU PAGINA
    If Printer.CurrentY > (MTop + Box0H - 3 * iRG) Then
      nCol = nCol + 1
      If nCol / 2 <> Int(nCol / 2) Then
        Printer.NewPage
        ' Squadratura della pagina
        X1 = MLeft: Y1 = MTop
        X2 = MLeft + Box0W: Y2 = MTop + Box0H
        Printer.Line (X1, Y1)-(X2, Y2), QBColor(0), B
        ' Intestazione + linea sotto intestazione
        Call StampaChkG4_INTESTAZIONE(MLeft, MTop, X2)
        ' Legenda punti fuori tolleranza
        Printer.CurrentX = MLeft + iCl
        Printer.CurrentY = Box0H + 2 * iRG
        stmp = "item(8)"
        Printer.Print stmp
        X1 = Left1Lista: Y1 = TopLista
      Else
        X1 = Left2Lista: Y1 = TopLista
      End If
      '** INTESTAZIONE F2 - INDICE DEL PUNTO
      TabX = X1 + iCl
      Printer.CurrentX = TabX
      Printer.CurrentY = Y1
      Printer.Print "Nr"
      '** INTESTAZIONE F2 - VALORI DEI RAGGI
      TabX = TabX + Printer.TextWidth("Nr  ") + iCl
      Printer.CurrentX = TabX
      Printer.CurrentY = Y1
      Printer.Print "R"
      '** INTESTAZIONE F2 - ERRORI rispetto al profilo medio
      TabX = TabX + Printer.TextWidth(fmt2) + iCl
      Printer.CurrentX = TabX
      Printer.CurrentY = Y1
      Printer.Print "Er.MF2"
      '** INTESTAZIONE F2 - Punti fuori tolleranza
      TabX = TabX + Printer.TextWidth(fmt3) + iCl
      Printer.CurrentX = TabX
      Printer.CurrentY = Y1
      Printer.Print ""
      '** INTESTAZIONE F2 - ERRORI E LINEA DEGLI ERRORI
      TabX = TabX + iCl + iCl
      Printer.CurrentX = TabX
      Printer.CurrentY = Y1
      Printer.Print "Er.TF2"
      GoSub LineaE0
    End If
  Next i
  '
Exit Sub

LineaE0:
  'Stampa la linea di riferimanto degli errori
  Printer.CurrentX = TabX + Printer.TextWidth(fmt3) + iCl
  PCYold = Printer.CurrentY
  'Xerr = errW
  'Printer.Line -Step(Xerr, 0), QBColor(0)
  Printer.Line (TabX + Printer.TextWidth(fmt3) + iCl, PCYold)-(TabX + Printer.TextWidth(fmt3) + iCl + errW, PCYold), QBColor(0)
  'Printer.PSet Step(0, 0), QBColor(0)
  Printer.CurrentX = TabX + Printer.TextWidth(fmt3) + iCl
  Printer.CurrentX = Printer.CurrentX - iCl / 2
  Printer.CurrentY = PCYold - iRG
  Printer.Print "-"
  Printer.CurrentX = TabX + Printer.TextWidth(fmt3) + iCl
  Printer.CurrentX = Printer.CurrentX + errW / 2 - iCl / 2
  Printer.CurrentY = PCYold - iRG
  Printer.Print "0"
  Printer.CurrentX = TabX + Printer.TextWidth(fmt3) + iCl
  Printer.CurrentX = Printer.CurrentX + errW - iCl / 2
  Printer.CurrentY = PCYold - iRG
  Printer.Print "+"
Return

ErroreLinea:
  'Stampa errore
  Printer.Print frmt(DblTest, 4)
  'Posizione per Stampa Linea errore
  Printer.CurrentX = TabX + Printer.TextWidth(fmt3) + iCl
  PCYold = Printer.CurrentY
  Printer.CurrentY = Printer.CurrentY - iCl / 2
  'Stampa Linea errore
      If (Abs(MaxEr) >= Abs(MinEr)) Then
        Xerr = (DblTest / Abs(MaxEr)) * errW / 2
  ElseIf (Abs(MaxEr) < Abs(MinEr)) Then
        Xerr = (DblTest / Abs(MinEr)) * errW / 2
  Else
        Xerr = 1 * errW / 2
  End If
  'Printer.Line -Step(Xerr, 0), vbRed
  'Printer.PSet Step(0, 0), vbRed
  Printer.Line (TabX + 2 * Printer.TextWidth(fmt3) + 3 * iCl, PCYold - iCl / 2)-(TabX + 2 * Printer.TextWidth(fmt3) + 3 * iCl + Xerr, PCYold - iCl / 2), vbRed
  'Posizione per Stampa errore
  Printer.CurrentY = PCYold
  Printer.CurrentX = TabX
Return

errStampaControllo_F1F2:
  If FreeFile > 0 Then Close
  MsgBox Error$(Err), 48, "SUB: StampaChk_F1F2"
  Exit Sub

End Sub

Sub StampaChkG4_GRAFICO(ByRef USCITA As Object)
'
Dim SiStampa As String
Dim NomeForm As Form
'
If (USCITA Is Printer) Then
  SiStampa = "Y"
Else
  SiStampa = "N"
  Set NomeForm = USCITA.Parent
End If
'
Dim i           As Integer
Dim j           As Integer
'Area di stampa
Dim MLeft       As Single
Dim MRight      As Single
Dim MTop        As Single
Dim MBottom     As Single
Dim Box0W       As Single
Dim Box0H       As Single
'Parametri per la stampa di linee
Dim X1          As Single
Dim Y1          As Single
Dim X2          As Single
Dim Y2          As Single
Dim ERX1        As Single
Dim ERY1        As Single
Dim ERX2        As Single
Dim ERY2        As Single
'Area di stampa dei grafici
Dim Box1L       As Single
Dim Box1T       As Single
Dim Box1W       As Single
Dim Box1H       As Single
Dim MerrMax     As Single
Static PF1(999)  As Single
Static PF2(999)  As Single
Static RF1(999)  As Single
Static RF2(999)  As Single
Dim TITOLO      As String
Dim TESTO       As String
Dim StrData     As String
Dim StrOra      As String
Dim TabX        As Single
Dim PCYold      As Single
Dim Xerr        As Single
Dim errW        As Single
Dim iRG         As Single
Dim iCl         As Single
Dim fmt2        As String
Dim fmt3        As String
Dim DblTest     As Double
Dim stmp        As String
Dim riga        As String
Dim jj          As Integer
Dim R           As Double
Dim A           As Double
Dim B           As Double
Dim RA(999)     As Double
Dim AA(999)     As Double
Dim TA(999)     As Double
Dim Ts(999)     As Double
Dim SEGNT(999)  As Integer
Dim ERPD
'
Dim ErrToll As Single
Dim MaxToll As Single
Dim MinToll As Single
Dim Xmax    As Single
Dim Ymax    As Single
Dim Xmin    As Single
Dim Ymin    As Single
'
Dim ERPS     As Double
Dim CORM     As Double
'
Dim NPF1      As Integer  'punto separazione F1 rotore (profilo con tutti gli Npt punti)
Dim NPF2      As Integer  'punti F2 rotore (profilo con tutti gli Npt punti)
'
On Error Resume Next
  '
  Dim COORDINATE         As String
  Dim RADICE             As String
  Dim PERCORSO           As String
  Dim PERCORSO_CONTROLLI As String
  Dim PERCORSO_PROFILI   As String
  Dim sequenza           As String
  Dim npt                As Integer
  Dim TY_ROT             As String
  Dim PASSO              As Double
  Dim CONTROLLORE        As String
  Dim DM_ROT             As String
  Dim INVERSIONE         As Integer
  '
  Call LEGGI_INFO_ROTORI(PERCORSO, COORDINATE, RADICE, PERCORSO_CONTROLLI, PERCORSO_PROFILI, sequenza, npt, TY_ROT, PASSO, CONTROLLORE, DM_ROT, INVERSIONE)
  '
  Dim NP1   As Integer
  Dim NP2   As Integer
  Dim NPM1  As Integer
  Dim NPM2  As Integer
  Dim ERPI  As Double
  '
  NPF1 = GetInfo("CREAZIONE", "NpM_F1", RADICE & "\INFO.INI")
  NPF2 = GetInfo("CREAZIONE", "NpM_F2", RADICE & "\INFO.INI")
  NP1 = GetInfo("CREAZIONE", "NpC_F1", RADICE & "\INFO.INI")
  NP2 = GetInfo("CREAZIONE", "NpC_F2", RADICE & "\INFO.INI")
  '
  'Call RicercaMinimoRotore(Radice & "\PRFROT", NPF1, NPF2)
  '
  Dim NEGTOL() As Single
  Dim POSTOL() As Single
  '
  'Lettuta di prftol.dat
  If (Dir$(RADICE & "\PRFTOL.DAT") = "") Then
    StopRegieEvents
    MsgBox Error(53) & " PRFTOL.DAT", 32, "WARNING"
    ResumeRegieEvents
    Exit Sub
  End If
  MinToll = -999: MaxToll = -999
  Open RADICE & "\PRFTOL.DAT" For Input As #1
    For i = 1 To npt
      ReDim Preserve NEGTOL(i): ReDim Preserve POSTOL(i)
      Input #1, NEGTOL(i), POSTOL(i)
      If Abs(NEGTOL(i)) > MinToll Then MinToll = Abs(NEGTOL(i))
      If Abs(POSTOL(i)) > MaxToll Then MaxToll = Abs(POSTOL(i))
    Next i
  Close #1
  '
  If (TY_ROT = "AXIAL") And (CONTROLLORE <> "INTERNAL") Then
    'Profilo controllato
    If (Dir$(RADICE & "\RVTROT.INT") = "") Then
      MsgBox Error(53) & " RVTROT.INT", 32, "WARNING"
      Exit Sub
    End If
    'Risultato controllo
    If (Dir$(RADICE & "\RISROT") = "") Then
      jj = NP1 + NP2 + 1
      If (jj <= nMAXpntG4) Then jj = nMAXpntG4
      Open RADICE & "\RISROT" For Output As #1
        For i = 1 To jj
          Print #1, "+0.0000,+0.0000"
        Next i
      Close #1
    End If
    'Lettura di RI e RS (Raggi)
    Open RADICE & "\RVTROT.INT" For Input As #1
      For i = 1 To NP1
        Input #1, riga: RS(i) = val(riga)
      Next i
      For i = 1 To NP2
        Input #1, riga: Ri(i) = val(riga)
      Next i
    Close #1
    'Ricerca degli errori Max e Min commessi sul profilo
    Open RADICE & "\PUNCHK" For Input As #1
      For i = 1 To NP1
        Input #1, PS(i)
      Next i
      For i = NP2 To 1 Step -1
        Input #1, PI(i)
      Next i
    Close #1
    For i = 1 To npt: ERPU(i) = 0: Next i
    MaxEr = -999: MinEr = 999
    Open RADICE & "\RISROT" For Input As #1
      For i = 1 To NP1
        Input #1, ERPI, ERPS
        ERPU(PS(i)) = ERPS
        If ERPS > MaxEr Then MaxEr = ERPS
        If ERPS < MinEr Then MinEr = ERPS
      Next i
    Close #1
    Open RADICE & "\RISROT" For Input As #1
      For i = 1 To NP2
        Input #1, ERPI, ERPS
        ERPU(PI(i)) = ERPI
        If ERPI > MaxEr Then MaxEr = ERPI
        If ERPI < MinEr Then MinEr = ERPI
      Next i
    Close #1
    'Passaggio da R, A, B a R, V, T
    Open RADICE & "\PRFROT" For Input As #1
      For i = 1 To npt
        Input #1, R, A, B
        RA(i) = R
        AA(i) = A * PASSO / 360
        TA(i) = Atn(Tan(B * PG / 180) * PASSO / (2 * R * PG))
        TA(i) = Abs(TA(i))
        '*****  OLD  *****
        If (RA(i) = RA(i - 1) And i > 1) Then
          SEGNT(i) = SEGNT(i - 1)
        Else
          If (RA(i) > RA(i - 1)) Then
            SEGNT(i) = -1
          Else
            SEGNT(i) = 1
          End If
        End If
        '*****  NEW  *****
        If i >= NPF1 Then
          SEGNT(i) = -1
        Else
          SEGNT(i) = 1
        End If
        '*****  ***  *****
      Next i
      '*****  OLD  *****
      'SEGNT(1) = 1
      '*****  ***  *****
    Close #1
    Dim DiaExtMis As Single
    Dim DiaIntMis As Single
    'Diametro Externo misurato
    DiaExtMis = 0
    For i = 1 To NP1
      If (RS(i) - ERPU(PS(i))) > DiaExtMis Then
        DiaExtMis = (RS(i) - ERPU(PS(i)))
      End If
    Next i
    For i = 1 To NP2
      If (Ri(i) - ERPU(PI(i))) > DiaExtMis Then
        DiaExtMis = (Ri(i) - ERPU(PI(i)))
      End If
    Next i
    DiaExtMis = DiaExtMis * 2
    'Diametro Interno misurato
    DiaIntMis = 999
    For i = 1 To NP1
      If (RS(i) - ERPU(PS(i))) < DiaIntMis Then
        DiaIntMis = (RS(i) - ERPU(PS(i)))
      End If
    Next i
    For i = 1 To NP2
      If (Ri(i) - ERPU(PI(i))) < DiaIntMis Then
        DiaIntMis = (Ri(i) - ERPU(PI(i)))
      End If
    Next i
    DiaIntMis = DiaIntMis * 2
  End If
  '
  If (TY_ROT = "FRONT") Or (CONTROLLORE = "INTERNAL") Then
    'Profilo controllato
    If (Dir$(RADICE & "\RABROT.INT") = "") Then
      MsgBox Error(53) & " RABROT.INT", 32, "WARNING"
      Exit Sub
    End If
    'Risultato controllo
    If (Dir$(RADICE & "\EPMROT") = "") Then
      jj = NP1 + NP2 + 1
      If (jj <= nMAXpntG4) Then jj = nMAXpntG4
      Open RADICE & "\EPMROT" For Output As #1
        For i = 1 To jj
          Print #1, "+0.0000,+0.0000"
        Next i
      Close #1
    End If
    'Lettura RD e RS (Raggi)
    Open RADICE & "\RABROT.INT" For Input As #1
      For i = 1 To NP1
        Input #1, riga: Rd(i) = val(riga)
      Next i
      For i = 1 To NP2
        Input #1, riga: RS(i) = val(riga)
      Next i
    Close #1
    'Ricerca degli errori Max e Min commessi sul profilo
    Open RADICE & "\PUNCHK" For Input As #1
      For i = 1 To NP1
        Input #1, PD(i)
      Next i
      For i = NP2 To 1 Step -1
        Input #1, PS(i)
      Next i
    Close #1
    For i = 1 To npt: ERPU(i) = 0: Next i
    MaxEr = -999: MinEr = 999
    Open RADICE & "\EPMROT" For Input As #1
      For i = 1 To NP1
        Input #1, ERPD, CORM
        ERPU(PD(i)) = ERPD
        If ERPD > MaxEr Then MaxEr = ERPD
        If ERPD < MinEr Then MinEr = ERPD
      Next i
      For i = 1 To NP2
        Input #1, ERPS, CORM
        ERPU(PS(i)) = ERPS
        If ERPS > MaxEr Then MaxEr = ERPS
        If ERPS < MinEr Then MinEr = ERPS
      Next i
    Close #1
    'Passaggio da R, A, T a X, Y, T
    Open RADICE & "\PRFROT" For Input As #1
      For i = 1 To npt
        Input #1, R, A, B
        RA(i) = R * Cos(A * PG / 180)
        AA(i) = R * Sin(A * PG / 180)
        Ts(i) = (A + B) * PG / 180
        TA(i) = Abs(Ts(i))
      Next i
    Close #1
    'Diametro Externo misurato
    DiaExtMis = 0
    For i = 1 To NP1
      If (Rd(i) - ERPU(PD(i))) > DiaExtMis Then
        DiaExtMis = (Rd(i) - ERPU(PD(i)))
      End If
    Next i
    For i = 1 To NP2
      If (RS(i) - ERPU(PS(i))) > DiaExtMis Then
        DiaExtMis = (RS(i) - ERPU(PS(i)))
      End If
    Next i
    DiaExtMis = DiaExtMis * 2
    'Diametro Interno misurato
    DiaIntMis = 999
    For i = 1 To NP1
      If (Rd(i) - ERPU(PD(i))) < DiaIntMis Then
        DiaIntMis = (Rd(i) - ERPU(PD(i)))
      End If
    Next i
    For i = 1 To NP2
      If (RS(i) - ERPU(PS(i))) < DiaIntMis Then
        DiaIntMis = (RS(i) - ERPU(PS(i)))
      End If
    Next i
    DiaIntMis = DiaIntMis * 2
  End If
  'INVIO DEL VALORE DEL DIAMETRO MINIMO **************************************
  Dim ITEMDDE As String
  ITEMDDE = GetInfo("PRODUZIONE", "DIAMINDDE", Path_LAVORAZIONE_INI)
  ITEMDDE = UCase$(Trim$(ITEMDDE))
  If (Len(ITEMDDE) > 0) Then Call OPC_SCRIVI_DATO(ITEMDDE, frmt(DiaIntMis, 3))
  '***************************************************************************
  If (SiStampa = "Y") Then
    MTop = 1: MBottom = 1
    MLeft = 2: MRight = 1
    Box1L = 0: Box1T = 1
    Box1W = 17: Box1H = 16
  Else
    MTop = -4.5: MBottom = 0
    MLeft = 0: MRight = 0
    Box1L = 0: Box1T = 1
    Box1W = 17: Box1H = 16
  End If
  '
  Box0W = USCITA.ScaleWidth - (MLeft + MRight) '17
  Box0H = USCITA.ScaleHeight - (MTop + MBottom) '27
  MerrMax = 1
  iRG = USCITA.TextHeight("A")
  iCl = USCITA.TextWidth("A")
  '
  ' Grafici dei profili
  X1 = Box1L + MLeft: Y1 = Box1T + MTop
  X2 = X1: Y2 = Y1 + Box1H
  USCITA.Line (X1, Y1)-(X2, Y2), QBColor(0)
  '
  X1 = Box1L + MLeft: Y1 = Box1T + MTop
  X1 = X1 + 0.5: Y1 = Y1 + Box1H - 0.5
  USCITA.CurrentX = X1
  USCITA.CurrentY = Y1
  USCITA.Print "F1"
  '
  X1 = Box1L + MLeft: Y1 = Box1T + MTop
  X1 = X1 + Box1W - 0.5: Y1 = Y1 + Box1H - 0.5
  USCITA.CurrentX = X1
  USCITA.CurrentY = Y1
  USCITA.Print "F2"
  '
  Dim NptCONTROLLATI As Long
  '
  For i = 1 To npt
    j = GetInfo("PuntiControllo", "CHK[0," & CStr(i) & "]", RADICE & "\CHKRAB.ROT")
    If (j = 1) Then NptCONTROLLATI = NptCONTROLLATI + 1
  Next i
  '
  i = 10
    USCITA.CurrentX = MLeft
    USCITA.CurrentY = MTop + i * iRG
  If (NptCONTROLLATI < npt) Then
    USCITA.ForeColor = QBColor(12)
    stmp = " CHECKED " & Format$(NptCONTROLLATI, "######0") & "/" & Format$(npt, "######0") & " "
  Else
    USCITA.ForeColor = QBColor(9)
    stmp = " All points CHECKED!! "
  End If
  USCITA.Print stmp
  USCITA.Line (MLeft, MTop + i * iRG)-(MLeft + 8, MTop + (i + 1) * iRG), QBColor(0), B
  USCITA.ForeColor = QBColor(0)
  '
  Dim NptABILITATI As Long
  '
  If (Dir$(RADICE & "\ABILITA.COR") <> "") Then
    Open RADICE & "\ABILITA.COR" For Input As #2
      i = 1: NptABILITATI = 0
      While (Not EOF(2)) And (i <= npt + 2)
        Line Input #2, stmp
        If (val(stmp) = 1) Then NptABILITATI = NptABILITATI + 1
        i = i + 1
      Wend
    Close #2
  Else
    NptABILITATI = npt + 2
    Open RADICE & "\ABILITA.COR" For Output As #1
      For i = 1 To NptABILITATI
        Print #1, "1"
      Next i
    Close #1
  End If
  '
  i = 11
    USCITA.CurrentX = MLeft
    USCITA.CurrentY = MTop + i * iRG
  If (NptABILITATI < npt + 2) Then
    USCITA.ForeColor = QBColor(12)
    stmp = " ENABLED " & Format$(NptABILITATI, "######0") & "/" & Format$(npt + 2, "######0") & " "
  Else
    USCITA.ForeColor = QBColor(9)
    stmp = " All correctors ENABLED "
  End If
  USCITA.Print stmp
  USCITA.Line (MLeft, MTop + i * iRG)-(MLeft + 8, MTop + (i + 1) * iRG), QBColor(0), B
  USCITA.ForeColor = QBColor(0)
  '
  i = 12
    USCITA.CurrentX = MLeft
    USCITA.CurrentY = MTop + i * iRG
    stmp = " Inspection: " & CONTROLLORE
  USCITA.Print stmp
  USCITA.Line (MLeft, MTop + i * iRG)-(MLeft + 8, MTop + (i + 1) * iRG), QBColor(0), B
  USCITA.ForeColor = QBColor(0)
  '
  Dim LnWidth(5)  As Single
  Dim LnColor(5)  As Variant
  Dim Rg1(5)      As Double
  Dim Rg2(5)      As Double
  Dim MsgVideo(5) As String
  '
  MsgVideo(0) = "theoretical profile"
  MsgVideo(1) = "measured profile"
  MsgVideo(2) = "major profile"
  MsgVideo(3) = "minor profile"
  MsgVideo(4) = "mean profile"
  MsgVideo(5) = "normals"
  '
  p1 = 1: p2 = npt
  '
  '0 - Profilo Teorico
  '1 - Profilo Misurato
  '2 - Profilo Maggiorato
  '3 - Profilo Minorato
  '4 - Profilo Medio
  '5 - Normali al Profilo teorico
  LnWidth(0) = 1:  LnColor(0) = vbBlue
  LnWidth(1) = 1:  LnColor(1) = vbRed
  LnWidth(2) = 1:  LnColor(2) = vbBlack
  LnWidth(3) = 1:  LnColor(3) = vbBlack
  LnWidth(4) = 1:  LnColor(4) = vbBlack
  LnWidth(5) = 1:  LnColor(5) = vbBlue
  '
  ' Legenda per i colori utilizzati per i profili
  i = 0
  For j = 0 To 4
    X1 = MLeft + Box1L + Box1W / 2 + 1: Y1 = Box1T + MTop + (i + 1) * iRG
    USCITA.CurrentX = X1
    USCITA.CurrentY = Y1
    USCITA.Print MsgVideo(j): i = i + 1
  Next j
  i = 0
  For j = 0 To 4
    USCITA.DrawWidth = LnWidth(j)
    X1 = MLeft + Box1L + Box1W / 2: Y1 = Box1T + MTop + (i + 1.5) * iRG
    X2 = X1 + 0.5: Y2 = Y1
    USCITA.Line (X1, Y1)-(X2, Y2), LnColor(j): i = i + 1
  Next j
  USCITA.DrawWidth = 1
  '
  'Sistema di Riferimento
  'Escursione massima permessa
  ErrToll = -999
  If Abs(MaxToll) > ErrToll Then ErrToll = Abs(MaxToll)
  If Abs(MinToll) > ErrToll Then ErrToll = Abs(MinToll)
  If ErrToll = 0 Then
    If Abs(MaxEr) > ErrToll Then ErrToll = Abs(MaxEr)
    If Abs(MinEr) > ErrToll Then ErrToll = Abs(MinEr)
  End If
  '
  Dim scaerr As Single
  scaerr = GetInfo("CHK0", "ScalaErroreROTORI", Path_LAVORAZIONE_INI)
  '
  'STAMPA DEL NOME ROTORE
  i = 7
  USCITA.CurrentX = MLeft + Box1L + Box1W - 6.5
  USCITA.CurrentY = Box1T + MTop + (i + 1) * iRG
  TESTO = LEGGI_NomePezzo("ROTORI_ESTERNI")
  TESTO = " " & TESTO & " "
  USCITA.Print TESTO
  USCITA.Line (MLeft + Box1L + Box1W - 6.5, Box1T + MTop + (i + 1) * iRG)-(MLeft + Box1L + Box1W - 6.5 + USCITA.TextWidth(TESTO), Box1T + MTop + (i + 2) * iRG), QBColor(0), B
  'Tolleranze
  i = 8
  USCITA.CurrentX = MLeft + Box1L + Box1W - 6.5
  USCITA.CurrentY = Box1T + MTop + (i + 1) * iRG
  USCITA.Print "toll (SUP=" & MaxToll & " INF=" & MinToll & ")"
  i = 9
  USCITA.CurrentX = MLeft + Box1L + Box1W - 6.5
  USCITA.CurrentY = Box1T + MTop + (i + 1) * iRG
  USCITA.Print "err (MAX=" & MaxEr & " MIN=" & MinEr & ")"
  i = 10
  USCITA.CurrentX = MLeft + Box1L + Box1W - 6.5
  USCITA.CurrentY = Box1T + MTop + (i + 1) * iRG
  USCITA.Print "dia (MAX=" & frmt(DiaExtMis, 3) & " MIN=" & frmt(DiaIntMis, 3) & ")"
  '
  'Escursioni massime
  Ymin = 999: Ymax = -999: Xmin = 999: Xmax = -999
  For j = p1 To p2
    If (RA(j) > Ymax) Then Ymax = RA(j)
    If (RA(j) < Ymin) Then Ymin = RA(j)
    If (AA(j) > Xmax) Then Xmax = AA(j)
    If (AA(j) < Xmin) Then Xmin = AA(j)
  Next j
  YY = (Ymax - Ymin): XX = (Xmax - Xmin)
  If (XX > YY) Then YY = XX Else XX = YY
  '
  i = 0 'Profilo Teorico
  USCITA.DrawWidth = LnWidth(i)
    For j = p1 To p2 - 1
      Rg1(i) = 0
      Rg2(i) = 0
      GoSub LineaP1P2: USCITA.Line (X1, Y1)-(X2, Y2), LnColor(i)
    Next j
  USCITA.DrawWidth = 1
  '
  i = 1 'Profilo Misurato
  USCITA.DrawWidth = LnWidth(i)
    For j = p1 To p2 - 1
      Rg1(1) = ERPU(j + 1)
      Rg2(1) = ERPU(j + 2)
      GoSub LineaP1P2: USCITA.Line (X1, Y1)-(X2, Y2), LnColor(i)
    Next j
  USCITA.DrawWidth = 1
  '
  i = 2 'Profilo Maggiorato
  USCITA.DrawWidth = LnWidth(i)
    For j = p1 To p2 - 1
      Rg1(2) = -POSTOL(j)
      Rg2(2) = -POSTOL(j + 1)
      GoSub LineaP1P2: USCITA.Line (X1, Y1)-(X2, Y2), LnColor(i)
    Next j
  USCITA.DrawWidth = 1
  '
  i = 3 'Profilo Minorato
  USCITA.DrawWidth = LnWidth(i)
    For j = p1 To p2 - 1
      Rg1(3) = NEGTOL(j)
      Rg2(3) = NEGTOL(j + 1)
      GoSub LineaP1P2: USCITA.Line (X1, Y1)-(X2, Y2), LnColor(i)
    Next j
  USCITA.DrawWidth = 1
  '
  i = 4 'Profilo Medio
  USCITA.DrawWidth = LnWidth(4)
    For j = p1 To p2 - 1
      Rg1(i) = -(POSTOL(j) - NEGTOL(j)) / 2
      Rg2(i) = -(POSTOL(j + 1) - NEGTOL(j + 1)) / 2
      GoSub LineaP1P2: USCITA.Line (X1, Y1)-(X2, Y2), LnColor(i)
    Next j
  USCITA.DrawWidth = 1
  '
  i = 5 'Normali al Profilo teorico
  USCITA.DrawWidth = LnWidth(5)
    For j = p1 To p2
      Rg1(i) = 0
      Rg2(i) = ERPU(j + 1)
      X1 = AA(j): Y1 = RA(j)
      X2 = AA(j): Y2 = RA(j)
      X1 = (X1 - Xmin) / XX: Y1 = (Y1 - Ymin) / YY
      X2 = (X2 - Xmin) / XX: Y2 = (Y2 - Ymin) / YY
      X1 = X1 * (Box1W - 2 * MerrMax) + Box1L + MLeft + MerrMax
      Y1 = (Box1T + Box1H - MerrMax) - (Y1 * (Box1H - 2 * MerrMax)) + MTop
      X2 = X2 * (Box1W - 2 * MerrMax) + Box1L + MLeft + MerrMax
      Y2 = (Box1T + Box1H - MerrMax) - (Y2 * (Box1H - 2 * MerrMax)) + MTop
      If (ErrToll <> 0) Then
        Rg1(i) = Rg1(i) * (MerrMax / ErrToll) * (scaerr / 100)
        Rg2(i) = Rg2(i) * (MerrMax / ErrToll) * (scaerr / 100)
      Else
        Rg1(i) = 0
        Rg2(i) = 0
      End If
      If (TY_ROT = "AXIAL") And (CONTROLLORE <> "INTERNAL") Then
        ERX1 = -Rg1(i) * Cos(TA(j)) * SEGNT(j)
        ERY1 = -Rg1(i) * Sin(TA(j))
        ERX2 = -Rg2(i) * Cos(TA(j)) * SEGNT(j)
        ERY2 = -Rg2(i) * Sin(TA(j))
      End If
      If (TY_ROT = "FRONT") Or (CONTROLLORE = "INTERNAL") Then
        ERX1 = -Rg1(i) * Cos(TA(j))
        ERY1 = -Rg1(i) * Sin(TA(j))
        If Ts(j) > 0 Then ERX1 = -ERX1
        ERX2 = -Rg2(i) * Cos(TA(j))
        ERY2 = -Rg2(i) * Sin(TA(j))
        If Ts(j) > 0 Then ERX2 = -ERX2
      End If
      X1 = X1 + ERX1: Y1 = Y1 - ERY1
      X2 = X2 + ERX2: Y2 = Y2 - ERY2
      USCITA.Line (X1, Y1)-(X2, Y2), LnColor(i)
    Next j
    '
Exit Sub

LineaP1P2:
  X1 = AA(j): Y1 = RA(j)
  X2 = AA(j + 1): Y2 = RA(j + 1)
  X1 = (X1 - Xmin) / XX: Y1 = (Y1 - Ymin) / YY
  X2 = (X2 - Xmin) / XX: Y2 = (Y2 - Ymin) / YY
  X1 = X1 * (Box1W - 2 * MerrMax) + Box1L + MLeft + MerrMax
  Y1 = (Box1T + Box1H - MerrMax) - (Y1 * (Box1H - 2 * MerrMax)) + MTop
  X2 = X2 * (Box1W - 2 * MerrMax) + Box1L + MLeft + MerrMax
  Y2 = (Box1T + Box1H - MerrMax) - (Y2 * (Box1H - 2 * MerrMax)) + MTop
  If ErrToll <> 0 Then
    Rg1(i) = Rg1(i) * (MerrMax / ErrToll) * (scaerr / 100)
    Rg2(i) = Rg2(i) * (MerrMax / ErrToll) * (scaerr / 100)
  Else
    Rg1(i) = 0
    Rg2(i) = 0
  End If
  If (TY_ROT = "AXIAL") And (CONTROLLORE <> "INTERNAL") Then
    ERX1 = -Rg1(i) * Cos(TA(j)) * SEGNT(j)
    ERY1 = -Rg1(i) * Sin(TA(j))
    ERX2 = -Rg2(i) * Cos(TA(j + 1)) * SEGNT(j + 1)
    ERY2 = -Rg2(i) * Sin(TA(j + 1))
  End If
  If (TY_ROT = "FRONT") Or (CONTROLLORE = "INTERNAL") Then
    ERX1 = -Rg1(i) * Cos(TA(j))
    ERY1 = -Rg1(i) * Sin(TA(j))
    If Ts(j) > 0 Then ERX1 = -ERX1
    ERX2 = -Rg2(i) * Cos(TA(j + 1))
    ERY2 = -Rg2(i) * Sin(TA(j + 1))
    If Ts(j + 1) > 0 Then ERX2 = -ERX2
  End If
  X1 = X1 + ERX1: Y1 = Y1 - ERY1
  X2 = X2 + ERX2: Y2 = Y2 - ERY2
Return
    
End Sub

Sub StampaChkG4()
'
Dim i           As Integer
Dim j           As Integer
'Area di stampa
Dim MLeft       As Single
Dim MRight      As Single
Dim MTop        As Single
Dim MBottom     As Single
Dim Box0W       As Single
Dim Box0H       As Single
'Parametri per la stampa di linee
Dim X1          As Single
Dim Y1          As Single
Dim X2          As Single
Dim Y2          As Single
Dim ERX1        As Single
Dim ERY1        As Single
Dim ERX2        As Single
Dim ERY2        As Single
'Area di stampa dei grafici
Dim Box1L       As Single
Dim Box1T       As Single
Dim Box1W       As Single
Dim Box1H       As Single
Dim MerrMax     As Single
Static PF1(nMAXpntG4)  As Single
Static PF2(nMAXpntG4)  As Single
Static RF1(nMAXpntG4)  As Single
Static RF2(nMAXpntG4)  As Single
Dim TITOLO      As String
Dim TESTO       As String
Dim StrData     As String
Dim StrOra      As String
Dim TabX        As Single
'Dim PCYold      As Single
Dim Xerr        As Single
Dim errW        As Single
Dim iRG         As Single
Dim iCl         As Single
Dim fmt2        As String
Dim fmt3        As String
Dim DblTest     As Double
Dim stmp        As String
'
'Dim MsgResults(20) As String
'Dim DatResults(20) As String
'
On Error Resume Next
  '
  Printer.ScaleMode = 7
  Call ImpostaPrinter("Arial", 8)
  MTop = 1: MBottom = 1 '2
  MLeft = 2: MRight = 1
  Box0W = Printer.ScaleWidth - (MLeft + MRight) '17
  Box0H = Printer.ScaleHeight - (MTop + MBottom) '27
  iRG = Printer.TextHeight("A")
  iCl = Printer.TextWidth("A")
  Box1L = 0: Box1T = 1
  Box1W = 17: Box1H = 16
  MerrMax = 1
  '
  ' Squadratura della pagina + Intestazione
  X1 = MLeft: Y1 = MTop
  X2 = MLeft + Box0W: Y2 = MTop + Box0H
  Printer.Line (X1, Y1)-(X2, Y2), QBColor(0), B
  Call StampaChkG4_INTESTAZIONE(X1, Y1, X2)
  '
  'MsgResults(0) = Prendi(603, "ROTORI_ESTERNI", "NOME_IT") 'LNP("PASSO")
  'MsgResults(1) = LNP("INCLINAZIONE")
  'MsgResults(2) = LNP("PRINCIPI")
  'stmp = LNP("SENSO_ELICA")
  'MsgResults(3) = Left$(stmp, InStr(stmp, "|") - 1)
  'MsgResults(4) = LNP("ROTAZIONE")
  'MsgResults(5) = LNP("R[147]")
  'MsgResults(6) = LNP("R[43]")
  'MsgResults(7) = LNP("R[45]")
  'MsgResults(8) = LNP("R[44]")
  'MsgResults(9) = LNP("R[103]")
  '
  'DatResults(0) = Lp("PASSO")
  'DatResults(1) = Lp("INCLINAZIONE")
  'DatResults(2) = Lp("PRINCIPI")
  'If (Lp("SENSO_ELICA") = 1) Then
    'DatResults(3) = "DX"
  'Else
    'DatResults(3) = "SX"
  'End If
  'DatResults(4) = Lp("ROTAZIONE")
  'DatResults(5) = Lp("R[147]")
  'DatResults(6) = Lp("R[43]")
  'DatResults(7) = Lp("R[45]")
  'DatResults(8) = Lp("R[44]")
  'DatResults(9) = Lp("R[103]")
  '
  'Dati Principali
  'X1 = MLeft: Y1 = MTop + Box1T + Box1H + iRG
  'Printer.CurrentX = X1 + iCl
  'Printer.CurrentY = Y1 + 0 * iRG
  'stmp = "Main data"
  'Printer.Print stmp
  'For j = 0 To 9
  '  Printer.CurrentX = X1 + iCl
  '  Printer.CurrentY = Y1 + (j + 1) * iRG
  '  Printer.Print MsgResults(j)
  'Next j
  'X1 = MLeft: Y1 = MTop + Box1T + Box1H + iRG
  'Printer.CurrentX = X1 + 3 + iCl
  'Printer.CurrentY = Y1 + 0 * iRG
  'Printer.Print ""
  'For j = 0 To 9
  '  Printer.CurrentX = X1 + 3 + iCl
  '  Printer.CurrentY = Y1 + (j + 1) * iRG
  '  Printer.Print "= " & DatResults(j)
  'Next j
  '
  'Dati Caratteristici
  'X1 = MLeft + Box1L: Y1 = MTop + 1
  'For j = 0 To 5
  '  Printer.CurrentX = X1 + iCl
  '  Printer.CurrentY = Y1 + (j + 1) * iRG
  '  Printer.Print "MsgDati" & Format$(j, "#0")
  'Next j
  'X1 = MLeft + Box1L: Y1 = MTop + 1
  'For j = 0 To 5
  '  Printer.CurrentX = X1 + 2.5 + iCl
  '  Printer.CurrentY = Y1 + (j + 1) * iRG
  '  Printer.Print "= " & frmt(j, 4)
  'Next j
  '
  'Stampa del numero del pezzo
  'Printer.CurrentX = X1 + iCl
  'Printer.CurrentY = Y1 + (7 + 1) * iRG
  'Testo = "MsgDati(7)" & " : " & Format$(1, "00000")
  'Printer.Print Testo
  '
  X1 = MLeft: Y1 = Box1H + Box1T + MTop
  X2 = X1 + Box0W: Y2 = Y1
  Printer.Line (X1, Y1)-(X2, Y2), QBColor(0)
  '
  Call StampaChkG4_GRAFICO(Printer)
  Printer.DrawWidth = 1
  Printer.NewPage
  Call StampaChkG4_F1F2
  Printer.EndDoc
  '
End Sub

Sub CALCOLO_PUNTO_ROTORE(ENTRX As Single, PAS As Double, INF As Double, _
                     RXF As Double, EXF As Double, APF As Double, _
                     Rayon As Double, ALFA As Double, Beta As Double)
'
Dim EXV   As Double
'
Dim SENS As Integer
'
Dim v7  As Double
Dim V12 As Double
Dim V13 As Double
Dim V14 As Double
Dim V15 As Double
Dim V16 As Double
Dim VPB As Double
Dim RXV As Double
Dim Ang As Double
Dim ang1 As Double
'
Dim YAPP As Double
Dim XAPP As Double
'
Dim Y1 As Double
Dim X1 As Double
'
On Error GoTo errCALCOLO_PUNTO_ROTORE

1920 ' ************** S/P ************ CALCUL PIECE D'APRES FRAISE **********
     ' Entrees  constantes : ENTRX, PAS,  INF
     ' Entrees  variables  : RXF,   EXF,  APF
     ' Sorties             : RAYON, ALFA, BETA
     ' **********************************************************************
     SENS = 1: If EXF < 0 Then SENS = -1
     EXF = EXF * SENS
     APF = APF * SENS
     v7 = PAS / 2 / PG
     GoSub 2050
     Rayon = RXV
     ALFA = Ang * SENS
     ang1 = Ang
     RXF = RXF + 0.01
     EXF = EXF - 0.01 * Tan(APF)
     GoSub 2050
     Y1 = YAPP: X1 = XAPP
     RXF = RXF - 0.02
     EXF = EXF + 0.02 * Tan(APF)
     GoSub 2050
     RXF = RXF + 0.01
     EXF = EXF - 0.01 * Tan(APF)
     If YAPP = Y1 Then YAPP = YAPP + 0.00001
     Beta = (Atn((X1 - XAPP) / (YAPP - Y1)) - ang1) * SENS
     If X1 = XAPP Then Beta = PG / 2
     APF = APF * SENS
     EXF = EXF * SENS
Exit Sub   '              ********* RETOUR AUX DONNEES

2050
  '
  V12 = -Tan(APF) * (ENTRX * Sin(INF) + v7 * Cos(INF))
  V13 = v7 * Sin(INF) - ENTRX * Cos(INF)
  V14 = Cos(INF) * ((-Tan(APF) * EXF) + RXF)
  V15 = (V14 ^ 2) + (V12 ^ 2) - (V13 ^ 2)
  If (V15 > 0) Then
    V15 = Sqr(V15)
    V16 = -(V15 + V12) / (V13 - V14)
    V15 = (V15 - V12) / (V13 - V14)
    VPB = V16: If Abs(V15) < Abs(V16) Then VPB = V15
    V12 = 2 * Atn(VPB)
    V13 = (RXF * Sin(INF) * Sin(V12)) + EXF * Cos(INF)
    V13 = Atn(V13 / ((RXF * Cos(V12)) - ENTRX))
    RXV = (ENTRX - RXF * Cos(V12)) / Cos(V13)
    EXV = EXF * Sin(INF) - v7 * V13 - RXF * Sin(V12) * Cos(INF)
    Ang = 2 * PG * EXV / PAS
    YAPP = RXV * Cos(Ang)
    XAPP = -RXV * Sin(Ang)
  End If
  '
Return
  
errCALCOLO_PUNTO_ROTORE:
  If FreeFile > 0 Then Close
  MsgBox Error$(Err), 48, "SUB: CALCOLO_PUNTO_ROTORE"
  Resume Next

End Sub

Sub CALCOLO_PROFILO_FRONTALE(ByVal ENTRX As Single, ByVal SpostaX As Double, ByVal DM_ROT As String)
'
Dim Dmed       As Single
'
Dim B()        As Single
Dim IdxNoChk() As Integer
Dim CountNoChk As Integer
Dim CountBeta  As Integer
Dim sgnB()
Dim i          As Integer
Dim j          As Integer
Dim k          As Integer
'
Dim R() As Double
Dim v() As Double
Dim T() As Double
'
Dim L$
Dim NoUse
Dim Scelta$
'
Dim INF   As Double
Dim pnt   As Integer
Dim EXV   As Double
Dim EXF   As Double
Dim RXF   As Double
Dim APFD  As Double
Dim APF   As Double
Dim Rayon As Double
Dim ALFA  As Double
Dim Beta  As Double
Dim BetaG As Double
'
Dim stmp As String
Dim SENS As Integer
'
Dim TG    As Double
Dim VANO  As Double
Dim N  As Integer
'
Dim PERCORSO    As String
Dim COORDINATE  As String
Dim RADICE      As String
Dim PASSO       As Double
Dim ELICA       As Double
Dim RagINT      As Double
Dim NPM1        As Integer
Dim NPM2        As Integer
Dim NP1         As Integer
Dim NP2         As Integer
Dim npt         As Integer
'
On Error GoTo errCALCOLO_PROFILO_FRONTALE
  '
  'LETTURA DEL PERCORSO DI RIFERIMENTO E COORDINATE CLIENTE
  PERCORSO = LEP_STR("PERCORSO")
  PERCORSO = g_chOemPATH & "\" & PERCORSO
  'RICAVO IL NOME DEL ROTORE DAL PERCORSO
  COORDINATE = PERCORSO
  If (Len(COORDINATE) > 0) Then
    i = InStr(COORDINATE, "\")
    While (i <> 0)
      j = i
      i = InStr(i + 1, COORDINATE, "\")
    Wend
    'RICAVO IL NOME DEL ROTORE
    COORDINATE = Right$(COORDINATE, Len(COORDINATE) - j)
  End If
  'RICAVO IL PERCORSO DI LAVORO
  RADICE = PERCORSO
  i = InStr(RADICE, COORDINATE)
  RADICE = Left$(RADICE, i - 2)
  '
  Dmed = val(DM_ROT)
  '
  Open RADICE & "\ROTOR.DAT" For Input As #1
    Line Input #1, L$: 'DireRotore = UCase$(Trim$(Left$(L$, 12)))
    Line Input #1, L$: 'NomeROTORE = UCase$(Trim$(Left$(L$, 12)))
    Line Input #1, L$: PASSO = val(L$)
    Line Input #1, L$: ELICA = val(L$)
    Line Input #1, L$: RagINT = val(L$) / 2
    Line Input #1, L$: 'Rotazione = val(L$)
    Line Input #1, L$: NPM1 = val(L$)
    Line Input #1, L$: NPM2 = val(L$)
    Line Input #1, L$: NP1 = val(L$)
    Line Input #1, L$: NP2 = val(L$)
    Line Input #1, L$: npt = val(L$)
    i = 1
    While Not EOF(1)
      Line Input #1, L$
      '
      ReDim Preserve IdxNoChk(i)
      IdxNoChk(i) = Int(val(L$))
      '
      i = i + 1
    Wend
    CountNoChk = i - 1
  Close #1
  '
  Open RADICE & "\PRFROT" For Input As #1
    i = 0
    While Not EOF(1)
      i = i + 1
      ReDim Preserve B(i)
      Input #1, NoUse, NoUse, B(i)
    Wend
    CountBeta = i  '<-------(NPT)
  Close #1
  '
  ReDim Preserve B(CountBeta + 1)
  ReDim sgnB(CountBeta + 1)
  ReDim R(CountBeta + 1)
  ReDim v(CountBeta + 1)
  ReDim T(CountBeta + 1)
  ReDim Preserve IdxNoChk(CountBeta)
  '
  'Eliminazione dei punti non controllati
  i = 1 'indice per il vettore B(*)
  j = 1 'indice per il vettore IdxNoChk(*)
  k = 1 'indice per il vettore SGNB(*)
  While (i <= CountBeta)
    If (i <> IdxNoChk(j)) Then
      sgnB(k) = Sgn(B(i))
      B(k) = B(i)
      If (sgnB(k) = 0) Then sgnB(k) = 1
      k = k + 1
    Else
      j = j + 1
    End If
    i = i + 1
  Wend
  'Sovrammetallo
  ENTRX = ENTRX + Dmed / 2 + RagINT
  INF = (90 - Abs(ELICA)) * PG / 180
  'CALCUL PIECE D'APRES FRAISE
  Open RADICE & "\XYT.ALL" For Input As #2
  Open RADICE & "\RABALL.RAB" For Output As #3
    pnt = 0
    While Not EOF(2)
      pnt = pnt + 1
      Input #2, EXF, RXF, APFD
      'SPOSTAMENTO ASSE MOLA
      EXF = EXF + SpostaX
      If APFD < 0 Then APF = (180 + APFD) * PG / 180 Else APF = APFD * PG / 180
      'GoSub 1920
      Call CALCOLO_PUNTO_ROTORE(ENTRX, PASSO, INF, RXF, EXF, APF, Rayon, ALFA, Beta)
      BetaG = Beta * 180 / PG
      If BetaG > 90 Then BetaG = BetaG - 180
      If BetaG < -90 Then BetaG = 180 + BetaG
      BetaG = B(pnt) ' ABS(BETAG) * sgnB(PNT)
      'Print #3, RAYON; ALFA * 180 / PG; BetaG
      stmp = ""
      If (Rayon > 0) Then
        stmp = stmp & "+" & Left$(frmt(Abs(Rayon), 4) & String(15, " "), 15)
      Else
        stmp = stmp & "-" & Left$(frmt(Abs(Rayon), 4) & String(15, " "), 15)
      End If
      stmp = stmp & " "
      If (ALFA * 180 / PG > 0) Then
        stmp = stmp & "+" & Left$(frmt(Abs(ALFA * 180 / PG), 4) & String(15, " "), 15)
      Else
        stmp = stmp & "-" & Left$(frmt(Abs(ALFA * 180 / PG), 4) & String(15, " "), 15)
      End If
      stmp = stmp & " "
      If (BetaG > 0) Then
        stmp = stmp & "+" & Left$(frmt(Abs(BetaG), 4) & String(15, " "), 15)
      Else
        stmp = stmp & "-" & Left$(frmt(Abs(BetaG), 4) & String(15, " "), 15)
      End If
      Print #3, stmp
    Wend
  Close #3
  Close #2
  'CRE-RAB: RIGIRA IL FIANCO 2 formattando per il controllo
  Open RADICE & "\RABALL.RAB" For Input As #1
    i = 1
    While Not EOF(1)
      Input #1, R(i), v(i), T(i)
      i = i + 1
    Wend
  Close #1
  N = i - 1
  'RIFERIMENTO PER KLINGELNBERG
  Open RADICE & "\RABROT.KLN" For Output As #2
    For i = 1 To N - 1
      stmp = ""
      If (R(i) > 0) Then
        stmp = stmp & "+" & Left$(frmt(Abs(R(i)), 4) & String(15, " "), 15)
      Else
        stmp = stmp & "-" & Left$(frmt(Abs(R(i)), 4) & String(15, " "), 15)
      End If
      stmp = stmp & " "
      If (v(i) > 0) Then
        stmp = stmp & "+" & Left$(frmt(Abs(v(i)), 4) & String(15, " "), 15)
      Else
        stmp = stmp & "-" & Left$(frmt(Abs(v(i)), 4) & String(15, " "), 15)
      End If
      stmp = stmp & " "
      If (T(i) > 0) Then
        stmp = stmp & "+" & Left$(frmt(Abs(T(i)), 4) & String(15, " "), 15)
      Else
        stmp = stmp & "-" & Left$(frmt(Abs(T(i)), 4) & String(15, " "), 15)
      End If
      Print #2, stmp
    Next i
  Close 2
  'CONTROLLO FRONTALE
  Open RADICE & "\RABROT.INT" For Output As #1
    For i = 1 To NP1
      stmp = ""
      If (R(i) > 0) Then
        stmp = stmp & "+" & frmt0(Abs(R(i)), 4)
      Else
        stmp = stmp & "-" & frmt0(Abs(R(i)), 4)
      End If
      If (v(i) > 0) Then
        stmp = stmp & "+" & frmt0(Abs(v(i)), 4)
      Else
        stmp = stmp & "-" & frmt0(Abs(v(i)), 4)
      End If
      If (T(i) > 0) Then
        stmp = stmp & "+" & frmt0(Abs(T(i)), 4)
      Else
        stmp = stmp & "-" & frmt0(Abs(T(i)), 4)
      End If
      Print #1, stmp
    Next i
    For i = NP1 + NP2 To NP1 + 1 Step -1
      stmp = ""
      If (R(i) > 0) Then
        stmp = stmp & "+" & frmt0(Abs(R(i)), 4)
      Else
        stmp = stmp & "-" & frmt0(Abs(R(i)), 4)
      End If
      If (v(i) > 0) Then
        stmp = stmp & "+" & frmt0(Abs(v(i)), 4)
      Else
        stmp = stmp & "-" & frmt0(Abs(v(i)), 4)
      End If
      If (T(i) > 0) Then
        stmp = stmp & "+" & frmt0(Abs(T(i)), 4)
      Else
        stmp = stmp & "-" & frmt0(Abs(T(i)), 4)
      End If
      Print #1, stmp
    Next i
  Close #1
  'CONTROLLO ASSIALE
  Open RADICE & "\RVTROT.INT" For Output As #1
    For i = 1 To NP1
      VANO = v(i) / 360 * PASSO
      TG = Tan(PG * T(i) / 180) * PASSO / (2 * R(i) * PG)
      stmp = ""
      If (R(i) > 0) Then
        stmp = stmp & "+" & frmt0(Abs(R(i)), 4)
      Else
        stmp = stmp & "-" & frmt0(Abs(R(i)), 4)
      End If
      If (VANO > 0) Then
        stmp = stmp & "+" & frmt0(Abs(VANO), 4)
      Else
        stmp = stmp & "-" & frmt0(Abs(VANO), 4)
      End If
      stmp = stmp & "+" & Left$(frmt(Abs(Atn(TG)) * 180 / PG, 4) & String(15, " "), 15)
      Print #1, stmp
    Next i
    For i = NP1 + NP2 To NP1 + 1 Step -1
      VANO = v(i) / 360 * PASSO
      TG = Tan(PG * T(i) / 180) * PASSO / (2 * R(i) * PG)
      stmp = ""
      If (R(i) > 0) Then
        stmp = stmp & "+" & frmt0(Abs(R(i)), 4)
      Else
        stmp = stmp & "-" & frmt0(Abs(R(i)), 4)
      End If
      If (VANO > 0) Then
        stmp = stmp & "+" & frmt0(Abs(VANO), 4)
      Else
        stmp = stmp & "-" & frmt0(Abs(VANO), 4)
      End If
      stmp = stmp & "+" & Left$(frmt(Abs(Atn(TG)) * 180 / PG, 4) & String(15, " "), 15)
      Print #1, stmp
    Next i
  Close #1
  
  'PROFILO ASSIALE
  Open RADICE & "\ASSIALE.TXT" For Output As #1
    For i = 1 To NP1 + NP2
      VANO = v(i) / 360 * PASSO
      TG = Tan(PG * T(i) / 180) * PASSO / (2 * R(i) * PG)
      stmp = ""
      If (R(i) > 0) Then
        stmp = stmp & " +" & frmt0(Abs(R(i)), 4)
      Else
        stmp = stmp & " -" & frmt0(Abs(R(i)), 4)
      End If
      If (VANO > 0) Then
        stmp = stmp & " +" & frmt0(Abs(VANO), 4)
      Else
        stmp = stmp & " -" & frmt0(Abs(VANO), 4)
      End If
      stmp = stmp & " +" & Left$(frmt(Abs(Atn(TG)) * 180 / PG, 4) & String(15, " "), 15)
      Print #1, stmp
    Next i
  Close #1

Exit Sub

errCALCOLO_PROFILO_FRONTALE:
  If FreeFile > 0 Then Close
  MsgBox Error$(Err), 48, "SUB: CALCOLO_PROFILO_FRONTALE"
  Exit Sub

End Sub

Sub PIAZZAMENTO_CONTROLLO_ZEISS_ACCURA(ByVal DIRETTORIO As String, ByVal RADICE As String, ByVal TY_ROT As String, ByVal NomeProfiloRAB As String)
'
Dim AGGIUNTA    As String
Dim ESTENSIONE  As String
'
Dim Rmax        As Double
Dim Rmin        As Double
Dim Rmed        As Double
Dim SensoEl     As Integer
'
Dim E8          As Integer
Dim E9          As Integer
Dim E14         As Integer
'
Dim stmp        As String
Dim i           As Integer
Dim j           As Integer
Dim Np          As Integer
'
Dim R()         As Double
Dim A()         As Double
Dim B()         As Double
Dim riga        As String
Dim NomeROTORE  As String
Dim MODALITA    As String
Dim NDente      As Integer
Dim PASSO       As Single
Dim NPF1        As Integer
Dim Rett_Viti   As Integer
Dim Bmin        As Single
Dim Amin        As Single
Dim iRmin       As Single
Dim iAmin       As Single
Dim ipriF1      As Integer
Dim ipriF2      As Integer
Dim NpRot       As Integer
Dim DeltaMin    As Single
Dim DeltaR      As Single
Dim iRF1        As Integer
Dim iRF2        As Integer
'
On Error GoTo errPIAZZAMENTO_CONTROLLO_ZEISS_ACCURA
  '
  'LETTURA NOME ROTORE
  MODALITA = GetInfo("Configurazione", "MODALITA", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))
  If (MODALITA <> "OFF-LINE") Then
    NomeROTORE = LEGGI_NomePezzo("ROTORI_ESTERNI")
  Else
    NomeROTORE = GetInfo("ROTORI_ESTERNI", "NomePezzoInserimento", Path_LAVORAZIONE_INI)
  End If
  'LETTURA DEL NUMERO DEI PRINCIPI
  NDente = Lp("R[5]")
  'LETTURA PASSO
  PASSO = Lp("PASSO")
  'LETTURA DEL TIPO DI RETTIFICA
  Rett_Viti = Lp("TIPO_ROTORE")
  'GESTIONE EQUIVALENTE ACV3
  'CLIENTE RIFERIMENTO TERMOMECCANICA
  'LETTURA DEI PUNTI DI CENTRATURA PER ENTRAMBI I ROTORI
  'LETTURA DELLA CORREZIONE SUI FIANCHI PER ROTORE MASCHIO
  'SensoEl = -1 'Sinistra
  'SensoEl = 1  'Destra
  SensoEl = Lp("SENSO_ELICA")
  'Leggi file profilo R, Alfa, Beta
  stmp = RADICE & "\" & NomeProfiloRAB
  If (Dir$(stmp) = "") Then
    MsgBox Error(53) & " " & stmp, 32, "WARNING"
    Exit Sub
  Else
    Open stmp For Input As #1
      i = 0: Rmin = 999: Rmax = -999
      While Not EOF(1)
        i = i + 1
        ReDim Preserve R(i): ReDim Preserve A(i): ReDim Preserve B(i)
        Input #1, R(i), A(i), B(i)
        If (R(i) < Rmin) Then Rmin = R(i): iRmin = i
        If (R(i) > Rmax) Then Rmax = R(i)
      Wend
    Close #1
    Np = i
    NpRot = Np
    E8 = LEP("TAS[0,20]")
    E9 = LEP("TAS[0,21]")
    E14 = LEP("TAS[0,22]")
    If (E8 <= 0) Or (E9 <= 0) Or (E14 <= 0) Then
      'N. punti F1 (ROTORE)
      NPF1 = GetInfo("CREAZIONE", "NpM_F1", RADICE & "\INFO.INI")
      'calcolo diametro di riferimento
      Bmin = 99999999
      For i = 1 To NPF1
        If (Abs(B(i)) < Bmin) Then
          Bmin = Abs(B(i))
          ipriF1 = i
        End If
      Next i
      Bmin = 99999999
      For i = NPF1 + 1 To NpRot
        If (Abs(B(i)) < Bmin) Then
          Bmin = Abs(B(i))
          ipriF2 = i
        End If
      Next i
      'Punto per controllo Diametro Interno  per Viti per Pompe
      Amin = 99999999
      For i = 1 To NpRot
        If (Abs(A(i)) < Amin) Then
          Amin = Abs(A(i))
          iAmin = i
        End If
      Next i
      'calcolo punti centratura Viti
      Rmed = (Rmin + Rmax) / 2
      'Fianco 1
      DeltaMin = 99999999
      For i = 1 To NPF1
        DeltaR = Abs(R(i) - Rmed)
        If (DeltaR < DeltaMin) Then
          DeltaMin = DeltaR
          iRF1 = i
        End If
      Next i
      'Fianco 2
      DeltaMin = 99999999
      For i = NPF1 + 1 To NpRot
        DeltaR = Abs(R(i) - Rmed)
        If (DeltaR < DeltaMin) Then
          DeltaMin = DeltaR
          iRF2 = i
        End If
      Next i
      If (Rett_Viti = 1) Or (Rett_Viti = 2) Then
        'Per Viti-Pompe
        E8 = iRF1:   E9 = iRF2:   E14 = iAmin
      Else
        'Per Rotori
        E8 = ipriF1: E9 = ipriF2: E14 = iRmin
      End If
    End If
    'SE NON ESISTE LO CREO
    stmp = DIRETTORIO
    If Dir$(stmp, 16) = "" Then
      i = InStr(stmp, "\")
      While (i <> 0)
        riga = Left$(stmp, i - 1)
        'SALTO LA PERIFERICA
        If Right(riga, 1) <> ":" And Dir$(riga, 16) = "" Then MkDir riga
        'MsgBox Left$(stmp, i - 1)
        i = InStr(i + 1, stmp, "\")
      Wend
      MkDir stmp
    End If
    stmp = DIRETTORIO & NomeROTORE & ".RAB"
    If Dir$(stmp) <> "" Then Kill stmp
    Open stmp For Output As #1
      For i = 1 To Np
        stmp = Left$(Format$(i, "##0") & String(3, " "), 3)
        stmp = stmp & " "
        If (R(i) > 0) Then
          stmp = stmp & "+" & Left$(frmt(Abs(R(i)), 4) & String(15, " "), 15)
        Else
          stmp = stmp & "-" & Left$(frmt(Abs(R(i)), 4) & String(15, " "), 15)
        End If
        stmp = stmp & " "
        If (A(i) > 0) Then
          stmp = stmp & "+" & Left$(frmt(Abs(A(i)), 4) & String(15, " "), 15)
        Else
          stmp = stmp & "-" & Left$(frmt(Abs(A(i)), 4) & String(15, " "), 15)
        End If
        stmp = stmp & " "
        If (B(i) > 0) Then
          stmp = stmp & "+" & Left$(frmt(Abs(B(i)), 4) & String(15, " "), 15)
        Else
          stmp = stmp & "-" & Left$(frmt(Abs(B(i)), 4) & String(15, " "), 15)
        End If
        Print #1, stmp
      Next i
      Print #1, "$IN DAT_ST"
      'NOME ROTORE
      Print #1, "NAME    : " & Right$(String(11, " ") & NomeROTORE, 11)
      'SPIRALE DEL ROTORE: +1 DESTRORSO -1 SINISTRORSO
      Print #1, "L_R     : " & Right$(String(7, " ") & frmt0(SensoEl, 1), 7)
      'ALTEZZA DEL PASSO
      Print #1, "H       : " & Right$(String(11, " ") & frmt0(PASSO, 5), 11)
      'NUMERO DI DENTI
      Print #1, "Z       : " & Right$(String(11, " ") & frmt0(NDente, 5), 11)
      'DIAMETRO ESTERNO
      Print #1, "DE      : " & Right$(String(11, " ") & frmt0(2 * Rmax, 5), 11)
      'DIAMETRO DI FONDO
      Print #1, "DF      : " & Right$(String(11, " ") & frmt0(2 * Rmin, 5), 11)
      'TIPO CONTROLLO
      If (TY_ROT = "AXIAL") Then
        'CONTROLLO ASSIALE
        Print #1, "TIPO_CHK: " & Right$(String(11, " ") & "1", 11)
      Else
        'CONTROLLO FRONTALE
        Print #1, "TIPO_CHK: " & Right$(String(11, " ") & "0", 11)
      End If
      'PUNTO CENTRATURA F1
      Print #1, "E8      : " & Right$(String(11, " ") & frmt0(E8, 5), 11)
      'PUNTO CENTRATURA F2
      Print #1, "E9      : " & Right$(String(11, " ") & frmt0(E9, 5), 11)
      'PUNTO DIAMETRO INTERNO
      Print #1, "E14     : " & Right$(String(11, " ") & frmt0(E14, 5), 11)
      Print #1, "$END"
    Close #1
  End If
  stmp = ""
  stmp = stmp & RADICE & "\" & NomeProfiloRAB
  stmp = stmp & Chr(13)
  stmp = stmp & " ---> " & Chr(13)
  stmp = stmp & DIRETTORIO & NomeROTORE & ".RAB"
  stmp = stmp & Chr(13)
  'StopRegieEvents
  'MsgBox stmp, 64, " (HMI) ---> (ACCURA)"
  'ResumeRegieEvents
  WRITE_DIALOG " ---> " & DIRETTORIO & NomeROTORE & AGGIUNTA & "." & ESTENSIONE
  '
Exit Sub

errPIAZZAMENTO_CONTROLLO_ZEISS_ACCURA:
  If (FreeFile > 0) Then Close
  If (STATO_ATTUALE = 5) Then
  MsgBox "PIAZZAMENTO_CONTROLLO_ZEISS_ACCURA: " & Error(Err), vbCritical, "WARNING"
  Else
  Call WRITE_DIALOG("PIAZZAMENTO_CONTROLLO_ZEISS_ACCURA: " & Error(Err))
  End If
  Exit Sub

End Sub

Sub PIAZZAMENTO_CONTROLLO_QUINDOS(ByVal DIRETTORIO As String, ByVal RADICE As String, ByVal TY_ROT As String, ByVal NomeProfiloRAB As String, ByVal AGGIUNTA As String)
'
Dim ESTENSIONE As String
'
Dim Rmax      As Double
Dim Rmin      As Double
Dim Rmed      As Double
Dim SensoEl   As Integer
'
Dim E8        As Integer
Dim E9        As Integer
Dim E14       As Integer
'
Dim riga      As String
Dim stmp      As String
Dim i         As Integer
Dim j         As Integer
Dim Np        As Integer
'
Dim R()       As Double
Dim A()       As Double
Dim B()       As Double
'
Dim NomeROTORE  As String
Dim MODALITA    As String
Dim NDente      As Integer
Dim PASSO       As Single
Dim NPF1        As Integer
Dim Rett_Viti   As Integer
Dim Bmin        As Single
Dim Amin        As Single
Dim iRmin       As Single
Dim iAmin       As Single
Dim ipriF1      As Integer
Dim ipriF2      As Integer
Dim NpRot       As Integer
Dim DeltaMin    As Single
Dim DeltaR      As Single
Dim iRF1        As Integer
Dim iRF2        As Integer
'
On Error GoTo errPIAZZAMENTO_CONTROLLO_QUINDOS
  '
  'LETTURA NOME ROTORE
  MODALITA = GetInfo("Configurazione", "MODALITA", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))
  If (MODALITA <> "OFF-LINE") Then
    NomeROTORE = LEGGI_NomePezzo("ROTORI_ESTERNI")
  Else
    NomeROTORE = GetInfo("ROTORI_ESTERNI", "NomePezzoInserimento", Path_LAVORAZIONE_INI)
  End If
  'LETTURA DEL NUMERO DEI PRINCIPI
  NDente = Lp("R[5]")
  'LETTURA PASSO
  PASSO = Lp("PASSO")
  'LETTURA DEL TIPO DI RETTIFICA
  Rett_Viti = Lp("TIPO_ROTORE")
  'SensoEl = -1 'Sinistra
  'SensoEl = 1  'Destra
  SensoEl = Lp("SENSO_ELICA")
  'Leggi file profilo R, Alfa, Beta
  stmp = RADICE & "\" & NomeProfiloRAB
  If (Dir$(stmp) = "") Then
    MsgBox Error(53) & " " & stmp, 32, "WARNING"
    Exit Sub
  Else
    Open stmp For Input As #1
      i = 0: Rmin = 999: Rmax = -999
      While Not EOF(1)
        i = i + 1
        ReDim Preserve R(i): ReDim Preserve A(i): ReDim Preserve B(i)
        Input #1, R(i), A(i), B(i)
        If (R(i) < Rmin) Then Rmin = R(i): iRmin = i
        If (R(i) > Rmax) Then Rmax = R(i)
      Wend
    Close #1
    Np = i
    NpRot = Np
    E8 = LEP("TAS[0,20]")
    E9 = LEP("TAS[0,21]")
    E14 = LEP("TAS[0,22]")
    If (E8 <= 0) Or (E9 <= 0) Or (E14 <= 0) Then
      'N. punti F1 (ROTORE)
      NPF1 = GetInfo("CREAZIONE", "NpM_F1", RADICE & "\INFO.INI")
      'calcolo diametro di riferimento
      Bmin = 99999999
      For i = 1 To NPF1
        If (Abs(B(i)) < Bmin) Then
          Bmin = Abs(B(i))
          ipriF1 = i
        End If
      Next i
      Bmin = 99999999
      For i = NPF1 + 1 To NpRot
        If (Abs(B(i)) < Bmin) Then
          Bmin = Abs(B(i))
          ipriF2 = i
        End If
      Next i
      'Punto per controllo Diametro Interno  per Viti per Pompe
      Amin = 99999999
      For i = 1 To NpRot
        If (Abs(A(i)) < Amin) Then
          Amin = Abs(A(i))
          iAmin = i
        End If
      Next i
      'calcolo punti centratura Viti
      Rmed = (Rmin + Rmax) / 2
      'Fianco 1
      DeltaMin = 99999999
      For i = 1 To NPF1
        DeltaR = Abs(R(i) - Rmed)
        If (DeltaR < DeltaMin) Then
          DeltaMin = DeltaR
          iRF1 = i
        End If
      Next i
      'Fianco 2
      DeltaMin = 99999999
      For i = NPF1 + 1 To NpRot
        DeltaR = Abs(R(i) - Rmed)
        If (DeltaR < DeltaMin) Then
          DeltaMin = DeltaR
          iRF2 = i
        End If
      Next i
      If (Rett_Viti = 1) Or (Rett_Viti = 2) Then
        'Per Viti-Pompe
        E8 = iRF1:   E9 = iRF2:   E14 = iAmin
      Else
        'Per Rotori
        E8 = ipriF1: E9 = ipriF2: E14 = iRmin
      End If
    End If
    'SE NON ESISTE LO CREO
    stmp = DIRETTORIO
    If Dir$(stmp, 16) = "" Then
      i = InStr(stmp, "\")
      While (i <> 0)
        riga = Left$(stmp, i - 1)
        'SALTO LA PERIFERICA
        If Right(riga, 1) <> ":" And Dir$(riga, 16) = "" Then MkDir riga
        'MsgBox Left$(stmp, i - 1)
        i = InStr(i + 1, stmp, "\")
      Wend
      MkDir stmp
    End If
  Dim XX() As Double: Dim YY() As Double: Dim GG() As Double
  Dim NX() As Double: Dim NY() As Double: Dim NZ() As Double
  ReDim XX(Np):       ReDim YY(Np)
  ReDim NX(Np):       ReDim NY(Np):       ReDim NZ(Np)
  Dim SEPARAZIONE As Integer
  'SEPARAZIONE = GetInfo("CREAZIONE", "NpC_F1", RADICE & "\INFO.INI")
  SEPARAZIONE = iRmin
  'CONVERSIONE COORDINATE
  For i = 1 To Np
    XX(i) = R(i) * Sin(A(i) * PG / 180)
    YY(i) = R(i) * Cos(A(i) * PG / 180)
    'ELICA EQUIVALENTE
    Hr = Atn(2 * PG * R(i) * Cos(B(i) / 180 * PG) / Abs(PASSO))
    ReDim Preserve GG(i)
    'Calcolo orientamento del vettore sul piano trasversale da alfa e beta
    If (A(i) < 0) Then GG(i) = -A(i) - B(i) Else GG(i) = 180 - B(i) - A(i)
    If (GG(i) < 0) Then
      If (GG(i) >= -180) Then
        GG(i) = GG(i) + 180
      Else
        If (GG(i) < -180) Then GG(i) = GG(i) + 360
      End If
    End If
    If (GG(i) > 180) Then GG(i) = GG(i) - 180
    Dim U0 As Double
    Dim V0 As Double
    Dim U1 As Double
    Dim v1 As Double
    U0 = Cos(GG(i) / 180 * PG)
    V0 = Sin(GG(i) / 180 * PG)
    Call RotVec(U0, V0, 90 - A(i), U1, v1)
    'COSENI DIRETTORI
    NX(i) = Cos(Hr) * Cos(GG(i) / 180 * PG)
    NY(i) = Abs(Cos(Hr) * Sin(GG(i) / 180 * PG))
    If (U1 >= 0) Then
      NZ(i) = Abs(Sin(Hr))
    Else
      NZ(i) = -Abs(Sin(Hr))
    End If
    If (SensoEl < 0) Then NZ(i) = -NZ(i)
  Next i
  '
  Open DIRETTORIO & NomeROTORE & AGGIUNTA & "_QDS.TXT" For Output As #2
    Print #2, "$ELE (NAM=ELE:" & NomeROTORE & AGGIUNTA & "_QDS, TYP=APT, FLD=(X,Y,Z,U,V,W))"
    For i = 1 To Np
      Print #2, Format$(XX(i), "#####0.00000") & ",";
      Print #2, Format$(YY(i), "#####0.00000") & ",";
      Print #2, Format$(0, "#####0.00000") & ",";
      Print #2, Format$(NX(i), "#####0.00000") & ",";
      Print #2, Format$(NY(i), "#####0.00000") & ",";
      Print #2, Format$(NZ(i), "#####0.00000")
    Next i
    Print #2, "$END"
  Close #2
  End If
  stmp = ""
  stmp = stmp & RADICE & "\" & NomeProfiloRAB
  stmp = stmp & Chr(13)
  stmp = stmp & " ---> " & Chr(13)
  stmp = stmp & DIRETTORIO & NomeROTORE & AGGIUNTA & "_QDS.TXT"
  stmp = stmp & Chr(13)
  'StopRegieEvents
  'MsgBox stmp, 64, " (HMI) ---> (QUINDOS)"
  'ResumeRegieEvents
  WRITE_DIALOG " ---> " & DIRETTORIO & NomeROTORE & AGGIUNTA & "." & ESTENSIONE
  '
Exit Sub

errPIAZZAMENTO_CONTROLLO_QUINDOS:
  If (FreeFile > 0) Then Close
  If (STATO_ATTUALE = 5) Then
  MsgBox "PIAZZAMENTO_CONTROLLO_QUINDOS: " & Error(Err), vbCritical, "WARNING"
  Else
  Call WRITE_DIALOG("PIAZZAMENTO_CONTROLLO_QUINDOS: " & Error(Err))
  End If
  Exit Sub

End Sub

Sub PIAZZAMENTO_CONTROLLO_DEA_IMAGE(ByVal DIRETTORIO As String, ByVal RADICE As String, ByVal TY_ROT As String, ByVal NomeProfiloRAB As String, ByVal AGGIUNTA As String)
'
Dim ESTENSIONE As String
'
Dim Rmax      As Double
Dim Rmin      As Double
Dim Rmed      As Double
Dim SensoEl   As Integer
'
Dim E8        As Integer
Dim E9        As Integer
Dim E14       As Integer
'
Dim riga      As String
Dim stmp      As String
Dim i         As Integer
Dim j         As Integer
Dim Np        As Integer
'
Dim R()       As Double
Dim A()       As Double
Dim B()       As Double
'
Dim NomeROTORE  As String
Dim MODALITA    As String
Dim NDente      As Integer
Dim PASSO       As Single
Dim NPF1        As Integer
Dim Rett_Viti   As Integer
Dim Bmin        As Single
Dim Amin        As Single
Dim iRmin       As Single
Dim iAmin       As Single
Dim ipriF1      As Integer
Dim ipriF2      As Integer
Dim NpRot       As Integer
Dim DeltaMin    As Single
Dim DeltaR      As Single
Dim iRF1        As Integer
Dim iRF2        As Integer
'
On Error GoTo errPIAZZAMENTO_CONTROLLO_DEA_IMAGE
  '
  'LETTURA NOME ROTORE
  MODALITA = GetInfo("Configurazione", "MODALITA", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))
  If (MODALITA <> "OFF-LINE") Then
    NomeROTORE = LEGGI_NomePezzo("ROTORI_ESTERNI")
  Else
    NomeROTORE = GetInfo("ROTORI_ESTERNI", "NomePezzoInserimento", Path_LAVORAZIONE_INI)
  End If
  'LETTURA DEL NUMERO DEI PRINCIPI
  NDente = Lp("R[5]")
  'LETTURA PASSO
  PASSO = Lp("PASSO")
  'LETTURA DEL TIPO DI RETTIFICA
  Rett_Viti = Lp("TIPO_ROTORE")
  'SensoEl = -1 'Sinistra
  'SensoEl = 1  'Destra
  SensoEl = Lp("SENSO_ELICA")
  'Leggi file profilo R, Alfa, Beta
  stmp = RADICE & "\" & NomeProfiloRAB
  If (Dir$(stmp) = "") Then
    MsgBox Error(53) & " " & stmp, 32, "WARNING"
    Exit Sub
  End If
  Open stmp For Input As #1
    i = 0: Rmin = 999: Rmax = -999
    While Not EOF(1)
      i = i + 1
      ReDim Preserve R(i): ReDim Preserve A(i): ReDim Preserve B(i)
      Input #1, R(i), A(i), B(i)
      If (R(i) < Rmin) Then Rmin = R(i): iRmin = i
      If (R(i) > Rmax) Then Rmax = R(i)
    Wend
  Close #1
  Np = i
  NpRot = Np
  E8 = LEP("TAS[0,20]")
  E9 = LEP("TAS[0,21]")
  E14 = LEP("TAS[0,22]")
  If (E8 <= 0) Or (E9 <= 0) Or (E14 <= 0) Then
    'N. punti F1 (ROTORE)
    NPF1 = GetInfo("CREAZIONE", "NpM_F1", RADICE & "\INFO.INI")
    'calcolo diametro di riferimento
    Bmin = 99999999
    For i = 1 To NPF1
      If (Abs(B(i)) < Bmin) Then
        Bmin = Abs(B(i))
        ipriF1 = i
      End If
    Next i
    Bmin = 99999999
    For i = NPF1 + 1 To NpRot
      If (Abs(B(i)) < Bmin) Then
        Bmin = Abs(B(i))
        ipriF2 = i
      End If
    Next i
    'Punto per controllo Diametro Interno  per Viti per Pompe
    Amin = 99999999
    For i = 1 To NpRot
      If (Abs(A(i)) < Amin) Then
        Amin = Abs(A(i))
        iAmin = i
      End If
    Next i
    'calcolo punti centratura Viti
    Rmed = (Rmin + Rmax) / 2
    'Fianco 1
    DeltaMin = 99999999
    For i = 1 To NPF1
      DeltaR = Abs(R(i) - Rmed)
      If (DeltaR < DeltaMin) Then
        DeltaMin = DeltaR
        iRF1 = i
      End If
    Next i
    'Fianco 2
    DeltaMin = 99999999
    For i = NPF1 + 1 To NpRot
      DeltaR = Abs(R(i) - Rmed)
      If (DeltaR < DeltaMin) Then
        DeltaMin = DeltaR
        iRF2 = i
      End If
    Next i
    If (Rett_Viti = 1) Or (Rett_Viti = 2) Then
      'Per Viti-Pompe
      E8 = iRF1:   E9 = iRF2:   E14 = iAmin
    Else
      'Per Rotori
      E8 = ipriF1: E9 = ipriF2: E14 = iRmin
    End If
  End If
  'SE NON ESISTE LO CREO
  stmp = DIRETTORIO
  If Dir$(stmp, 16) = "" Then
    i = InStr(stmp, "\")
    While (i <> 0)
      riga = Left$(stmp, i - 1)
      'SALTO LA PERIFERICA
      If Right(riga, 1) <> ":" And Dir$(riga, 16) = "" Then MkDir riga
      'MsgBox Left$(stmp, i - 1)
      i = InStr(i + 1, stmp, "\")
    Wend
    MkDir stmp
  End If
  '
Dim TIPO_MACCHINA As String
  '
  TIPO_MACCHINA = GetInfo("Configurazione", "TIPO_MACCHINA", Path_LAVORAZIONE_INI)
  TIPO_MACCHINA = UCase$(Trim$(TIPO_MACCHINA))
  If (TIPO_MACCHINA = "G500HL-SEIM") Then
    Dim TollA     As Single
    Dim TollB     As Single
    Dim TollC     As Single
    Dim LUNGHEZZA As Single
    'CREAZIONE NUOVA FASCIA DI TOLLERANZA
    TollA = Lp("TollA")
    TollB = Lp("TollB")
    TollC = Lp("TollC")
    LUNGHEZZA = Lp("FASCIATOTALE_G")
    Open RADICE & "\PRFTOL.DAT" For Output As #1
      For i = 1 To Np
        Print #1, "+" & Left$(frmt(TollA, 5) & String$(6, " "), 6) & " +" & Left$(frmt(TollA, 5) & String$(6, " "), 6)
      Next i
    Close #1
    Dim nTOL(999)     As Single
    Dim pTOL(999)     As Single
    'FASCIA DI TOLLERANZA
    Open RADICE & "\" & "PRFTOL.DAT" For Input As #1
      For i = 1 To Np
        Input #1, nTOL(i), pTOL(i)
      Next i
    Close #1
    'CON FASCIA DI TOLLERANZA
    Open DIRETTORIO & NomeROTORE & AGGIUNTA & ".RAB" For Output As #1
      For i = 1 To Np
        stmp = Left$(Format$(i, "##0") & String(3, " "), 3)
        stmp = stmp & " "
        If (R(i) > 0) Then
          stmp = stmp & "+" & Left$(frmt(Abs(R(i)), 4) & String(15, " "), 15)
        Else
          stmp = stmp & "-" & Left$(frmt(Abs(R(i)), 4) & String(15, " "), 15)
        End If
        stmp = stmp & " "
        If (A(i) > 0) Then
          stmp = stmp & "+" & Left$(frmt(Abs(A(i)), 4) & String(15, " "), 15)
        Else
          stmp = stmp & "-" & Left$(frmt(Abs(A(i)), 4) & String(15, " "), 15)
        End If
        stmp = stmp & " "
        If (B(i) > 0) Then
          stmp = stmp & "+" & Left$(frmt(Abs(B(i)), 4) & String(15, " "), 15)
        Else
          stmp = stmp & "-" & Left$(frmt(Abs(B(i)), 4) & String(15, " "), 15)
        End If
        stmp = stmp & " "
          stmp = stmp & "-" & Left$(frmt(nTOL(i), 4) & String(15, " "), 15)
        stmp = stmp & " "
          stmp = stmp & "+" & Left$(frmt(pTOL(i), 4) & String(15, " "), 15)
        Print #1, stmp
      Next i
      Print #1, "$IN DAT_ST"
      'NOME ROTORE
      Print #1, "NAME    : " & Right$(String(11, " ") & NomeROTORE, 11)
      'SPIRALE DEL ROTORE: +1 DESTRORSO -1 SINISTRORSO
      Print #1, "L_R     : " & Right$(String(7, " ") & frmt0(SensoEl, 1), 7)
      'ALTEZZA DEL PASSO
      Print #1, "H       : " & Right$(String(11, " ") & frmt0(PASSO, 5), 11)
      'NUMERO DI DENTI
      Print #1, "Z       : " & Right$(String(11, " ") & frmt0(NDente, 5), 11)
      'DIAMETRO ESTERNO
      Print #1, "DE      : " & Right$(String(11, " ") & frmt0(2 * Rmax, 5), 11)
      'DIAMETRO DI FONDO
      Print #1, "DF      : " & Right$(String(11, " ") & frmt0(2 * Rmin, 5), 11)
      'TIPO CONTROLLO
      If (TY_ROT = "AXIAL") Then
        'CONTROLLO ASSIALE
        Print #1, "TIPO_CHK: " & Right$(String(11, " ") & "1", 11)
      Else
        'CONTROLLO FRONTALE
        Print #1, "TIPO_CHK: " & Right$(String(11, " ") & "0", 11)
      End If
      'PUNTO CENTRATURA F1
      Print #1, "E8      : " & Right$(String(11, " ") & frmt0(E8, 5), 11)
      'PUNTO CENTRATURA F2
      Print #1, "E9      : " & Right$(String(11, " ") & frmt0(E9, 5), 11)
      'PUNTO DIAMETRO INTERNO
      Print #1, "E14     : " & Right$(String(11, " ") & frmt0(E14, 5), 11)
      'TOLLERANZE
      Print #1, "a       : " & Right$(String(11, " ") & frmt0(TollA, 5), 11)
      Print #1, "b       : " & Right$(String(11, " ") & frmt0(TollB, 5), 11)
      Print #1, "c       : " & Right$(String(11, " ") & frmt0(TollC, 5), 11)
      'LUNGHEZZA UTILE
      Print #1, "L       : " & Right$(String(11, " ") & frmt0(LUNGHEZZA, 2), 11)
      'FINE FILE
      Print #1, "$END"
    Close #1
  Else
    'SENZA FASCIA DI TOLLERANZA
    Open DIRETTORIO & NomeROTORE & AGGIUNTA & ".RAB" For Output As #1
      For i = 1 To Np
        stmp = Left$(Format$(i, "##0") & String(3, " "), 3)
        stmp = stmp & " "
        If (R(i) > 0) Then
          stmp = stmp & "+" & Left$(frmt(Abs(R(i)), 4) & String(15, " "), 15)
        Else
          stmp = stmp & "-" & Left$(frmt(Abs(R(i)), 4) & String(15, " "), 15)
        End If
        stmp = stmp & " "
        If (A(i) > 0) Then
          stmp = stmp & "+" & Left$(frmt(Abs(A(i)), 4) & String(15, " "), 15)
        Else
          stmp = stmp & "-" & Left$(frmt(Abs(A(i)), 4) & String(15, " "), 15)
        End If
        stmp = stmp & " "
        If (B(i) > 0) Then
          stmp = stmp & "+" & Left$(frmt(Abs(B(i)), 4) & String(15, " "), 15)
        Else
          stmp = stmp & "-" & Left$(frmt(Abs(B(i)), 4) & String(15, " "), 15)
        End If
        Print #1, stmp
      Next i
      Print #1, "$IN DAT_ST"
      'NOME ROTORE
      Print #1, "NAME    : " & Right$(String(11, " ") & NomeROTORE, 11)
      'SPIRALE DEL ROTORE: +1 DESTRORSO -1 SINISTRORSO
      Print #1, "L_R     : " & Right$(String(7, " ") & frmt0(SensoEl, 1), 7)
      'ALTEZZA DEL PASSO
      Print #1, "H       : " & Right$(String(11, " ") & frmt0(PASSO, 5), 11)
      'NUMERO DI DENTI
      Print #1, "Z       : " & Right$(String(11, " ") & frmt0(NDente, 5), 11)
      'DIAMETRO ESTERNO
      Print #1, "DE      : " & Right$(String(11, " ") & frmt0(2 * Rmax, 5), 11)
      'DIAMETRO DI FONDO
      Print #1, "DF      : " & Right$(String(11, " ") & frmt0(2 * Rmin, 5), 11)
      'TIPO CONTROLLO
      If (TY_ROT = "AXIAL") Then
        'CONTROLLO ASSIALE
        Print #1, "TIPO_CHK: " & Right$(String(11, " ") & "1", 11)
      Else
        'CONTROLLO FRONTALE
        Print #1, "TIPO_CHK: " & Right$(String(11, " ") & "0", 11)
      End If
      'PUNTO CENTRATURA F1
      Print #1, "E8      : " & Right$(String(11, " ") & frmt0(E8, 5), 11)
      'PUNTO CENTRATURA F2
      Print #1, "E9      : " & Right$(String(11, " ") & frmt0(E9, 5), 11)
      'PUNTO DIAMETRO INTERNO
      Print #1, "E14     : " & Right$(String(11, " ") & frmt0(E14, 5), 11)
      'FINE FILE
      Print #1, "$END"
    Close #1
  End If
  stmp = ""
  stmp = stmp & RADICE & "\" & NomeProfiloRAB
  stmp = stmp & Chr(13)
  stmp = stmp & " ---> " & Chr(13)
  stmp = stmp & DIRETTORIO & NomeROTORE & AGGIUNTA & ".RAB"
  stmp = stmp & Chr(13)
  'StopRegieEvents
  'MsgBox stmp, 64, " (HMI) ---> (DEA)"
  'ResumeRegieEvents
  WRITE_DIALOG " ---> " & DIRETTORIO & NomeROTORE & AGGIUNTA & "." & ESTENSIONE
  '
Exit Sub

errPIAZZAMENTO_CONTROLLO_DEA_IMAGE:
  If (FreeFile > 0) Then Close
  If (STATO_ATTUALE = 5) Then
  MsgBox "PIAZZAMENTO_CONTROLLO_DEA_IMAGE: " & Error(Err), vbCritical, "WARNING"
  Else
  Call WRITE_DIALOG("PIAZZAMENTO_CONTROLLO_DEA_IMAGE: " & Error(Err))
  End If
  Exit Sub

End Sub

Sub PIAZZAMENTO_CONTROLLO_KLINGELNBERG(ByVal DIRETTORIO As String, ByVal RADICE As String, ByVal TY_ROT As String, ByVal R203 As Single, ByVal NomeProfiloRAB As String, ByVal AGGIUNTA As String)
'
Dim stmp      As String
Dim Rmin      As Double
Dim i         As Integer
Dim Np        As Integer
Dim SensoEl   As Integer
Dim GAMMA     As Single
Dim INTERASSE As Single
'
Dim R() As Double
Dim A() As Double
Dim B() As Double
'
Dim X() As Double
Dim Y() As Double
Dim g() As Double
'
Dim idxmin            As Integer
Dim SEZIONE_PROFILO   As Integer
Dim NomeROTORE        As String
Dim NDente            As Integer
Dim DAng              As Double
Dim tz                As Integer
Dim ESTENSIONE        As String
Dim ELICA             As Single
Dim PASSO             As Single
Dim MODALITA          As String
Dim ret               As Integer
'
On Error GoTo errPIAZZAMENTO_CONTROLLO_KLINGELNBERG
  '
  'LETTURA NOME ROTORE
  MODALITA = GetInfo("Configurazione", "MODALITA", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))
  If (MODALITA <> "OFF-LINE") Then
    NomeROTORE = LEGGI_NomePezzo("ROTORI_ESTERNI")
  Else
    NomeROTORE = GetInfo("ROTORI_ESTERNI", "NomePezzoInserimento", Path_LAVORAZIONE_INI)
  End If
  'LETTURA DEL NUMERO DEI PRINCIPI
  NDente = Lp("R[5]")
  'LETTURA PASSO
  PASSO = Lp("PASSO")
  'LETTURA ELICA
  ELICA = Lp("R[258]")
  'LETTURA INDICE DI SEPARAZIONE F1-F2
  idxmin = Lp("R[43]")
  'RABROT.KLN: PROFILO TEORICO CONTROLLATO RUOTATO CON SOVRAMMETALLO
  stmp = RADICE & "\" & NomeProfiloRAB
  If (Dir$(stmp) = "") Then
    StopRegieEvents
    MsgBox Error(53) & " " & stmp, 32, "WARNING"
    ResumeRegieEvents
  Else
    'Leggi file profilo R, Alfa, Beta
    Open stmp For Input As #1
    i = 0: Rmin = 999
    While Not EOF(1)
      i = i + 1
      ReDim Preserve R(i): ReDim Preserve A(i): ReDim Preserve B(i)
      Input #1, R(i), A(i), B(i)
      If (R(i) < Rmin) Then Rmin = R(i) ': idxmin = i
    Wend
    Close #1
    '-- Eliminazione ultima riga con diametro interno
'***** FIL (Nel RABROT.KLN non c'� la riga con il diametro interno)
    'i = i - 1
    'ReDim Preserve R(i): ReDim Preserve A(i): ReDim Preserve B(i)
'*****
    Np = i
  End If
'***** FIL
'  DAng = Abs(A(UBound(A)) - A(1))
'  If (NDente > 0) Then
'    tz = NDente
'  Else
'    tz = Int((Abs(DAng) / 10 + 0.5) * 10)
'  End If
'  If (DAng >= (360 / tz)) Then
'    stmp = ""
'    stmp = stmp & "Profile arc (" & frmt0(DAng, 4) & "�) >= 360/Z (" & frmt0((360 / tz), 4) & "�)"
'    stmp = stmp & Chr(10) & Chr(13)
'    stmp = stmp & "Remove the Last point of profile from measuring?"
'    StopRegieEvents
'    ret = MsgBox(stmp, vbYesNo + vbQuestion)
'    ResumeRegieEvents
'    If (ret = vbYes) Then
'      i = UBound(R) - 1
'      ReDim Preserve R(i): ReDim Preserve A(i): ReDim Preserve B(i)
'      Np = i
'    End If
'  End If
'*****
  ReDim X(Np): ReDim Y(Np): ReDim g(Np)
  'CALCOLO INTERASSE MOLA PEZZO
  INTERASSE = R203 + Rmin
  'SensoEl = -1 'Sinistra
  'SensoEl = +1 'Destra
  SensoEl = Lp("SENSO_ELICA")
  GAMMA = Abs(ELICA)
  'CONVERSIONE COORDINATE
  If (TY_ROT = "AXIAL") Then
    'ASSIALE
    For i = 1 To Np
      X(i) = A(i) / 360 * PASSO
      Y(i) = R(i)
      g(i) = Atn(Tan(PG * Abs(B(i)) / 180) * PASSO / (2 * R(i) * PG)) * 180 / PG
      If (i > idxmin) Then
        'SECONDO FIANCO
        If (B(i) >= 0) Then
          If (B(i) < 90) Then g(i) = 180 - g(i) Else g(i) = 90
        Else
          g(i) = 180 + g(i)
        End If
      Else
        'PRIMO FIANCO
        If (B(i) >= 0) And (B(i) < 90) Then g(i) = 360 - g(i)
      End If
      If (g(i) < 0) Then
        If (g(i) >= -180) Then
          g(i) = g(i) + 180
        Else
          If (g(i) < -180) Then g(i) = g(i) + 360
        End If
      End If
    Next i
    ESTENSIONE = "0AX"
  Else
    'TRASVERSALE/FRONTALE
    For i = 1 To Np
      X(i) = R(i) * Sin(A(i) * PG / 180)
      Y(i) = R(i) * Cos(A(i) * PG / 180)
      If (A(i) < 0) Then g(i) = -A(i) - B(i) Else g(i) = 180 - B(i) - A(i)
      If (g(i) < 0) Then
        If (g(i) >= -180) Then
          g(i) = g(i) + 180
        Else
          If (g(i) < -180) Then g(i) = g(i) + 360
        End If
      End If
      If (g(i) > 180) Then g(i) = g(i) - 180
    Next i
    ESTENSIONE = "0ST"
  End If
  'Memorizza file in X,Y,G
  Open DIRETTORIO & NomeROTORE & AGGIUNTA & "." & ESTENSIONE For Output As #2
    For i = 1 To Np
      stmp = ""
      stmp = stmp & Right$(String(3, " ") & Format$(i, "##0"), 3)
      stmp = stmp & Right$(String(11, " ") & frmt0(X(i), 4), 11)
      stmp = stmp & Right$(String(11, " ") & frmt0(Y(i), 4), 11)
      stmp = stmp & Right$(String(11, " ") & frmt0(g(i), 4), 11)
      Print #2, stmp
    Next i
    Print #2, "$IN DAT_ST"
    'SPIRALE DEL ROTORE: +1 DESTRORSO -1 SINISTRORSO
    Print #2, "L_R     : " & Right$(String(7, " ") & frmt0(SensoEl, 1), 7)
    'ALTEZZA DEL PASSO
    Print #2, "H       : " & Right$(String(11, " ") & frmt0(PASSO, 5), 11)
    'NUMERO DI DENTI
    Print #2, "Z       : " & Right$(String(11, " ") & frmt0(NDente, 5), 11)
    'DIAMETRO DI FONDO
    Print #2, "DF      : " & Right$(String(11, " ") & frmt0(2 * Rmin, 5), 11)
    'INTERASSE TRA MOLA E PEZZO
    Print #2, "A       : " & Right$(String(11, " ") & frmt0(INTERASSE, 5), 11)
    'DIAMETRO DELLA MOLA
    Print #2, "DS      : " & Right$(String(11, " ") & frmt0(R203 * 2, 5), 11)
    'ANGOLO DI OSCILLAZIONE
    Print #2, "GAMMA_E : " & Right$(String(11, " ") & frmt0(GAMMA, 5), 11)
    'ENTITA' DELLO SPOSTAMENTO ASSIALE PER PRE-PROFONDITA'
    Print #2, "A_AX    : " & Right$(String(11, " ") & frmt0(0, 5), 11)
    Print #2, "$END"
  Close #2
  'Memorizza file in X,Y,G
  'Scrittura del file 0W con dati mola per klingelnberg
  Dim tX As Double
  Dim tY As Double
  Dim TT As Double
  Open RADICE & "\XYT.ALL" For Input As #3
  Open DIRETTORIO & NomeROTORE & AGGIUNTA & ".0W" For Output As #2
    For i = 1 To Np
      Input #3, tX, tY, TT
      stmp = ""
      stmp = stmp & Right$(String(3, " ") & Format$(i, "##0"), 3)
      stmp = stmp & Right$(String(11, " ") & frmt0(tX, 4), 11)
      stmp = stmp & Right$(String(11, " ") & frmt0(tY, 4), 11)
      stmp = stmp & Right$(String(11, " ") & frmt0(TT, 4), 11)
      Print #2, stmp
    Next i
    Print #2, "$IN DAT_WKZ"
    'SPIRALE DEL ROTORE: +1 DESTRORSO -1 SINISTRORSO
    Print #2, "L_R     : " & Right$(String(7, " ") & frmt0(SensoEl, 1), 7)
    'ALTEZZA DEL PASSO
    Print #2, "H       : " & Right$(String(11, " ") & frmt0(PASSO, 5), 11)
    'NUMERO DI DENTI
    Print #2, "Z       : " & Right$(String(11, " ") & frmt0(NDente, 5), 11)
    'DIAMETRO DI FONDO
    Print #2, "DF      : " & Right$(String(11, " ") & frmt0(2 * Rmin, 5), 11)
    'INTERASSE TRA MOLA E PEZZO
    Print #2, "A       : " & Right$(String(11, " ") & frmt0(INTERASSE, 5), 11)
    'DIAMETRO DELLA MOLA
    Print #2, "DS      : " & Right$(String(11, " ") & frmt0(R203 * 2, 5), 11)
    'ANGOLO DI OSCILLAZIONE
    Print #2, "GAMMA_E : " & Right$(String(11, " ") & frmt0(GAMMA, 5), 11)
    'ENTITA' DELLO SPOSTAMENTO ASSIALE PER PRE-PROFONDITA'
    Print #2, "A_AX    : " & Right$(String(11, " ") & frmt0(0, 5), 11)
    Print #2, "$END"
  Close #2
  Close #3
  stmp = ""
  stmp = stmp & RADICE & "\" & NomeProfiloRAB
  stmp = stmp & Chr(13)
  stmp = stmp & " ---> " & Chr(13)
  stmp = stmp & "1) " & DIRETTORIO & NomeROTORE & AGGIUNTA & "." & ESTENSIONE
  stmp = stmp & Chr(13)
  stmp = stmp & "2) " & DIRETTORIO & NomeROTORE & AGGIUNTA & ".0W"
  stmp = stmp & Chr(13)
  'StopRegieEvents
  'MsgBox stmp, 64, " (HMI) ---> (KLINGELNBERG)"
  'ResumeRegieEvents
  WRITE_DIALOG " ---> " & DIRETTORIO & NomeROTORE & AGGIUNTA & "." & ESTENSIONE
  '
Exit Sub

errPIAZZAMENTO_CONTROLLO_KLINGELNBERG:
  If (FreeFile > 0) Then Close
  If (STATO_ATTUALE = 5) Then
  MsgBox "PIAZZAMENTO_CONTROLLO_KLINGELNBERG: " & Error(Err), vbCritical, "WARNING"
  Else
  WRITE_DIALOG "PIAZZAMENTO_CONTROLLO_KLINGELNBERG: " & Error(Err)
  End If
  Exit Sub

End Sub

Public Sub LEGGI_INFO_ROTORI(PERCORSO As String, COORDINATE As String, RADICE As String, _
                     PERCORSO_CONTROLLI As String, PERCORSO_PROFILI As String, sequenza As String, npt As Integer, _
                     TY_ROT As String, PASSO As Double, CONTROLLORE As String, DM_ROT As String, INVERSIONE As Integer)
'
Dim NomeROTORE  As String
Dim DBB         As Database
Dim RSS         As Recordset
Dim DATI        As String
Dim i           As Integer
Dim j           As Integer
Dim k           As Integer
Dim stmp        As String
Dim MODALITA    As String
'
On Error Resume Next
  '
  'LETTURA NOME ROTORE
  MODALITA = GetInfo("Configurazione", "MODALITA", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))
  If (MODALITA <> "OFF-LINE") Then
    NomeROTORE = LEGGI_NomePezzo("ROTORI_ESTERNI")
  Else
    NomeROTORE = GetInfo("ROTORI_ESTERNI", "NomePezzoInserimento", Path_LAVORAZIONE_INI)
  End If
  'LETTURA DEI DATI TEORICI DELLA VITE DAL DATABASE
  Set DBB = OpenDatabase(PathDB, True, False)
  Set RSS = DBB.OpenRecordset("ARCHIVIO_ROTORI_ESTERNI")
  RSS.Index = "NOME_PEZZO"
  RSS.Seek "=", NomeROTORE
  If RSS.NoMatch Then
    'CHIUDO IL DATABASE
    RSS.Close
    DBB.Close
    'SEGNALAZIONE UTENTE
    WRITE_DIALOG "WARNING: " & NomeROTORE & " not found."
    PERCORSO = ""
    COORDINATE = ""
    RADICE = ""
    PERCORSO_CONTROLLI = ""
    PERCORSO_PROFILI = ""
    sequenza = ""
    npt = 0
    TY_ROT = ""
    PASSO = 0
    CONTROLLORE = ""
    DM_ROT = ""
    INVERSIONE = 0
  Else
    DATI = RSS.Fields("DATI").Value
    DATI = DATI & Chr(13) & Chr(10)
    'CHIUDO IL DATABASE
    RSS.Close
    DBB.Close
    '
    'LETTURA DEL PERCORSO
    i = InStr(DATI, "PERCORSO=")
    j = InStr(i, DATI, Chr(10))
    stmp = Mid(DATI, i, j - i - 1)
    k = InStr(stmp, "=")
    PERCORSO = Right$(stmp, Len(stmp) - k)
    If (Len(PERCORSO) > 0) Then
      PERCORSO = g_chOemPATH & "\" & PERCORSO
      'RICAVO IL NOME DEL ROTORE DAL PERCORSO
      COORDINATE = PERCORSO
      If (Len(COORDINATE) > 0) Then
        i = InStr(COORDINATE, "\")
        While (i <> 0)
          j = i
          i = InStr(i + 1, COORDINATE, "\")
        Wend
        'RICAVO IL NOME DEL ROTORE
        COORDINATE = Right$(COORDINATE, Len(COORDINATE) - j)
      End If
      'RICAVO IL PERCORSO DI LAVORO
      RADICE = PERCORSO
      i = InStr(RADICE, COORDINATE)
      RADICE = Left$(RADICE, i - 2)
      'LETTURA PERCORSO DEL RISULTATO DEI CONTROLLI
      i = InStr(DATI, "PERCORSO_CONTROLLI=")
      j = InStr(i, DATI, Chr(10))
      stmp = Mid(DATI, i, j - i - 1)
      k = InStr(stmp, "=")
      PERCORSO_CONTROLLI = Right$(stmp, Len(stmp) - k)
      'LETTURA PERCORSO DEI PROFILI DEI CONTROLLI
      i = InStr(DATI, "PERCORSO_PROFILI=")
      j = InStr(i, DATI, Chr(10))
      stmp = Mid(DATI, i, j - i - 1)
      k = InStr(stmp, "=")
      PERCORSO_PROFILI = Right$(stmp, Len(stmp) - k)
      'LETTURA SEQUENZA PUNTI DI CONTROLLO
      i = InStr(DATI, "SEQUENZA=")
      j = InStr(i, DATI, Chr(10))
      stmp = Mid(DATI, i, j - i - 1)
      k = InStr(stmp, "=")
      sequenza = Right$(stmp, Len(stmp) - k)
      'LETTURA NUMERO PUNTI DEL PROFILO
      i = InStr(DATI, "R[147]=")
      j = InStr(i, DATI, Chr(10))
      stmp = Mid(DATI, i, j - i - 1)
      k = InStr(stmp, "=")
      npt = CInt(Right$(stmp, Len(stmp) - k))
      'LETTURA DEL TIPO DI ROTORE
      i = InStr(DATI, "TY_ROT=")
      j = InStr(i, DATI, Chr(10))
      stmp = Mid(DATI, i, j - i - 1)
      k = InStr(stmp, "=")
      TY_ROT = Right$(stmp, Len(stmp) - k)
      'LETTURA DEL PASSO
      i = InStr(DATI, "PASSO=")
      j = InStr(i, DATI, Chr(10))
      stmp = Mid(DATI, i, j - i - 1)
      k = InStr(stmp, "=")
      PASSO = val(Right$(stmp, Len(stmp) - k))
      'LETTURA DEL TIPO DI CONTROLLO
      i = InStr(DATI, "CONTROLLORE=")
      j = InStr(i, DATI, Chr(10))
      stmp = Mid(DATI, i, j - i - 1)
      k = InStr(stmp, "=")
      CONTROLLORE = Right$(stmp, Len(stmp) - k)
      'LETTURA DEL DIAMETRO MEDIO MOLA
      i = InStr(DATI, "DM_ROT=")
      j = InStr(i, DATI, Chr(10))
      stmp = Mid(DATI, i, j - i - 1)
      k = InStr(stmp, "=")
      DM_ROT = Right$(stmp, Len(stmp) - k)
      'LETTURA DELL'INVERSIONE FIANCHI
      i = InStr(DATI, "INVERSIONE=")
      j = InStr(i, DATI, Chr(10))
      stmp = Mid(DATI, i, j - i - 1)
      k = InStr(stmp, "=")
      INVERSIONE = Right$(stmp, Len(stmp) - k)
    Else
      PERCORSO = ""
      COORDINATE = ""
      RADICE = ""
      PERCORSO_CONTROLLI = ""
      PERCORSO_PROFILI = ""
      sequenza = ""
      npt = 0
      TY_ROT = ""
      PASSO = 0
      CONTROLLORE = ""
      DM_ROT = ""
      INVERSIONE = 0
    End If
  End If
  '
End Sub

Public Sub SELEZIONA_CONTROLLORE_INTERNAL()
'
Dim NomeROTORE         As String
Dim DBB                As Database
Dim RSS                As Recordset
  Dim PRIMA_DATI As String
  Dim DOPO_DATI As String
Dim DATI               As String
Dim i                  As Integer
Dim j                  As Integer
Dim k                  As Integer
Dim stmp               As String
    Dim CONTROLLORE As String
'
On Error Resume Next
  '
  NomeROTORE = LEGGI_NomePezzo("ROTORI_ESTERNI")
  '
  'LETTURA DEI DATI TEORICI DELLA VITE DAL DATABASE
  Set DBB = OpenDatabase(PathDB, True, False)
  Set RSS = DBB.OpenRecordset("ARCHIVIO_ROTORI_ESTERNI")
  RSS.Index = "NOME_PEZZO"
  RSS.Seek "=", NomeROTORE
  If RSS.NoMatch Then
    'CHIUDO IL DATABASE
    RSS.Close
    DBB.Close
    'SEGNALAZIONE UTENTE
    WRITE_DIALOG "WARNING: " & NomeROTORE & " not found."
  Else
    'LETTURA DATI IN ARCHIVIO
    DATI = RSS.Fields("DATI").Value
    DATI = DATI & Chr(13) & Chr(10)
    '
    'LETTURA DEL TIPO DI CONTROLLO
    i = InStr(DATI, "CONTROLLORE=")
    j = InStr(i, DATI, Chr(10))
    stmp = Mid(DATI, i, j - i - 1)
    k = InStr(stmp, "=")
    CONTROLLORE = Right$(stmp, Len(stmp) - k)
    '
    PRIMA_DATI = Left$(DATI, i - 1)
    DOPO_DATI = Right$(DATI, Len(DATI) - j)
    DOPO_DATI = Left$(DOPO_DATI, Len(DOPO_DATI) - 2)
    '
    DATI = PRIMA_DATI & "CONTROLLORE=INTERNAL" & Chr(13) & Chr(10) & DOPO_DATI
    '
    RSS.Edit
    RSS.Fields("DATI").Value = DATI
    RSS.Update
    '
    'CHIUDO IL DATABASE
    RSS.Close
    DBB.Close
  End If
  '
End Sub

Sub RicercaMinimoRotore(In_FILEROTORE As String, Out_PuntiF1 As Integer, Out_PuntiF2 As Integer)
'
Dim R       As Double
Dim A       As Double
Dim B       As Double
Dim Rmin    As Double
Dim RminIDX As Integer
Dim i       As Integer
Dim sERRORE As String
'
On Error GoTo errRicercaMinimoRotore
  '
  i = 1
  If (Dir$(In_FILEROTORE) <> "") Then
    Open In_FILEROTORE For Input As #1
        Input #1, R, A, B
        Rmin = R
        RminIDX = i
    While Not EOF(1)
        i = i + 1
        Input #1, R, A, B
        'Il ricalcolo del profilo dalla mola pu� differire di qualche 0.0001mm
        'falsando la ricerca del punto di separazione
        'Problema di ACQUISIZIONE DA KLINGELNBERG di LEISTRITZ 22/10/08
        If (R < Rmin) And (Abs(R - Rmin) > 0.0005) Then
            Rmin = R
            RminIDX = i
        End If
    Wend
    Close #1
    Out_PuntiF1 = RminIDX
    Out_PuntiF2 = i - RminIDX
  Else
    StopRegieEvents
    sERRORE = "ERROR: " & Error(Err)
    MsgBox sERRORE, 48, "SUB: RicercaMinimoRotore"
    ResumeRegieEvents
  End If
  '
Exit Sub

errRicercaMinimoRotore:
  If FreeFile > 0 Then Close
  sERRORE = "ERROR: " & Error(Err)
  If Err.Number = 53 Then sERRORE = sERRORE & Chr(13) & In_FILEROTORE
  MsgBox sERRORE, 48, "SUB: RicercaMinimoRotore"
  Exit Sub

End Sub

Public Sub ACQUISIZIONE_CONTROLLO_GAPP4(ByVal RADICE As String)
'
Dim NpRint        As Integer
Dim stmp          As String
'
'Dim NomePezzoCHK  As String
'Dim DataPiazzCHK  As String
'
Dim i   As Integer
Dim j   As Integer
Dim k   As Integer
Dim kk  As Integer
Dim W   As Integer
'
Dim RTMP As Double
Dim SEP$
Dim vect$()
'
'punti ed errori (profilo completo)
Dim Idx()   As Integer
Dim RR()    As Double
Dim AA()    As Double
Dim BB()    As Double
Dim EE()    As Double
'
Dim iCHK()  As Integer
'
'punti ed errori (profilo controllato)
Dim RA(nMAXpntG4 + 1) As Double
Dim al(nMAXpntG4 + 1) As Double
Dim BE(nMAXpntG4 + 1) As Double
Dim Err(nMAXpntG4 + 1) As Double
'
Dim NPF1      As Integer  ': NUM.PNT F1 ROTORE
Dim NPF2      As Integer  ': NUM.PNT F2 ROTORE
Dim NP1_G4    As Integer  ': NUM.PNT CTRL F1 ROTORE
Dim NP2_G4    As Integer  ': NUM.PNT CTRL F2 ROTORE
'
Dim Hr()    As Double
Dim npchk   As Integer
Dim Dintmis As Double
Dim rag     As Double
Dim Rmin    As Double
Dim erint   As Double
Dim NP1     As Integer
'
On Error GoTo errACQUISIZIONE_CONTROLLO_GAPP4
  '
  ReDim iCHK(0)
  '
  'Dim COORDINATE         As String
  'Dim RADICE             As String
  'Dim PERCORSO           As String
  'Dim PERCORSO_CONTROLLI As String
  'Dim PERCORSO_PROFILI   As String
  'Dim sequenza           As String
  'Dim npt                As Integer
  'Dim TY_ROT             As String
  'Dim Passo              As Double
  'Dim CONTROLLORE        As String
  'Dim DM_ROT             As String
  'Dim INVERSIONE         As Integer
  '
  'Call LEGGI_INFO_ROTORI(PERCORSO, COORDINATE, RADICE, PERCORSO_CONTROLLI, PERCORSO_PROFILI, sequenza, npt, TY_ROT, Passo, CONTROLLORE, DM_ROT, INVERSIONE)
  '
  'IMPORTAZIONE RISULTATO DEL CONTROLLO GAPP4
  'Call FileCopy(PathMIS & "\DEFAULT", RADICE & "\GAPP4CHK.TXT")
  '
  'CONVERSIONE DA FORMATO GAPP4 in EPMROT
  '(CHK solo nel piano TR --> niente RISROT)
  Open RADICE & "\GAPP4CHK.TXT" For Input As #1
    'DATI GENERICI
    Line Input #1, stmp: j = InStr(stmp, ":")
    stmp = Trim$(Right$(stmp, Len(stmp) - j))
    'NomePezzoCHK = stmp
    Line Input #1, stmp: j = InStr(stmp, ":")
    stmp = Trim$(Right$(stmp, Len(stmp) - j))
    'DataPiazzCHK = stmp
    Line Input #1, stmp
    Line Input #1, stmp
    Line Input #1, stmp
    Line Input #1, stmp: j = InStr(stmp, ":")
    stmp = Trim$(Right$(stmp, Len(stmp) - j))
    NPF1 = val(stmp)
    Line Input #1, stmp: j = InStr(stmp, ":")
    stmp = Trim$(Right$(stmp, Len(stmp) - j))
    NPF2 = val(stmp)
    For i = 1 To 4
      Line Input #1, stmp
    Next i
    Line Input #1, stmp: j = InStr(stmp, ":")
    stmp = Trim$(Right$(stmp, Len(stmp) - j))
    NpRint = val(stmp)
    For i = 1 To 5
      Line Input #1, stmp
    Next i
    'PROFILO
    Rmin = 999: i = 0: W = 0
    While Not EOF(1)
      Line Input #1, stmp: stmp = Trim$(stmp)
      If (val(stmp) > 0) Then
        'INCREMENTO IL CONTATORE
        i = i + 1
        'ALLOCO LE VARIABILI
        ReDim Preserve Idx(i)
        ReDim Preserve RR(i)
        ReDim Preserve AA(i)
        ReDim Preserve BB(i)
        ReDim Preserve EE(i)
        j = InStr(stmp, "|")
        If (j <> 0) Then SEP$ = "|" Else SEP$ = " "
        '  vecchio formato con "|" come separatore
        '  nuovo formato con lo spazio come separatore
        vect$ = Split(stmp, SEP$)
        kk = 0
        For k = 0 To UBound(vect$)
          If (vect$(k) <> "") Then
            kk = kk + 1
            If kk = 1 Then Idx(i) = val(vect$(k)) 'INDICE
            If kk = 2 Then RR(i) = val(vect$(k))  'ASCISSA
            If kk = 3 Then AA(i) = val(vect$(k))  'ORDINATA
            If kk = 4 Then BB(i) = val(vect$(k))  'VERSORE ASCISSE
            If kk = 5 Then EE(i) = val(vect$(k))  'VERSORE ORDINATA
            'RIMOZIONE DEI PUNTI NON CONTROLLATI
            If (EE(i) = 999) Then EE(i) = 0
          End If
        Next k
      End If
      If (i = NpRint) And (i > 0) Then
         Rmin = RR(i): erint = EE(i)
      End If
    Wend
  Close #1
  'VETTORE PUNTI CONTROLLATI 15.11.07
  W = 0
  Open RADICE & "\PUNCHK" For Input As #1
      While Not EOF(1)
      Input #1, npchk
         W = W + 1
         ReDim Preserve iCHK(W)
         iCHK(W) = npchk - 1
      Wend
  Close #1
  Dintmis = (Rmin - erint) * 2
  'RICAVO "RABROT" ED "EPMROT" (con i soli punti controllati)
  NP1_G4 = 0     'Numero dei punti F1 controllati da GAPP4
  NP2_G4 = 0     'Numero dei punti F2 controllati da GAPP4
  j = 1          'Indice dei punti controllati GAPP4
  i = 1          'Indice dei punti controllati F1
  NP1 = val(GetInfo("CREAZIONE", "NpC_F1", RADICE & "\INFO.INI"))
  For j = 1 To UBound(iCHK)
    If (iCHK(j) <= iCHK(NP1)) Then
      RA(j) = RR(iCHK(j))
      al(j) = AA(iCHK(j))
      BE(j) = BB(iCHK(j))
      Err(j) = EE(iCHK(j))
      NP1_G4 = NP1_G4 + 1
    Else
      RA(j) = RR(iCHK(j))
      al(j) = AA(iCHK(j))
      BE(j) = BB(iCHK(j))
      Err(j) = EE(iCHK(j))
      NP2_G4 = NP2_G4 + 1
    End If
  Next j
  If UBound(iCHK) = 0 Then NP1_G4 = NPF1: NP2_G4 = NPF2
  'StopRegieEvents
  'stmp = ""
  'stmp = " " & stmp & " GAPP4 : " & Format$(NP1_G4, "######0") & "/" & Format$(NPF1, "######0")
  'stmp = " " & stmp & " GAPP4 : " & Format$(NP2_G4, "######0") & "/" & Format$(NPF2, "######0")
  'MsgBox stmp, vbInformation, "WARNING"
  'ResumeRegieEvents
  'COSTRUISCO IL FILE CON I PUNTI CONTROLLATI
  Open RADICE & "\RABROT.INT" For Output As #1
    'FIANCO 1: ORDINE INALTERATO
    For i = 1 To NP1_G4
      stmp = ""
      If (RA(i) >= 0) Then stmp = stmp & "+" Else stmp = stmp & "-"
      stmp = stmp & frmt0(Abs(RA(i)), 4)
      If (al(i) >= 0) Then stmp = stmp & "+" Else stmp = stmp & "-"
      stmp = stmp & frmt0(Abs(al(i)), 4)
      If (BE(i) >= 0) Then stmp = stmp & "+" Else stmp = stmp & "-"
      stmp = stmp & frmt0(Abs(BE(i)), 4)
      Print #1, stmp
    Next i
    'FIANCO 2: ORDINE INVERTITO
    For i = NP1_G4 + NP2_G4 To NP1_G4 + 1 Step -1
      stmp = ""
      If (RA(i) >= 0) Then stmp = stmp & "+" Else stmp = stmp & "-"
      stmp = stmp & frmt0(Abs(RA(i)), 4)
      If (al(i) >= 0) Then stmp = stmp & "+" Else stmp = stmp & "-"
      stmp = stmp & frmt0(Abs(al(i)), 4)
      If (BE(i) >= 0) Then stmp = stmp & "+" Else stmp = stmp & "-"
      stmp = stmp & frmt0(Abs(BE(i)), 4)
      Print #1, stmp
    Next i
  Close #1
  'COSTRUISCO IL FILE DEGLI ERRORI
  Open RADICE & "\EPMROT" For Output As #2
    'FIANCO 1: ORDINE INALTERATO
    For i = 1 To NP1_G4
      stmp = ""
      If (Err(i) >= 0) Then stmp = stmp & "+" Else stmp = stmp & "-"
      stmp = stmp & frmt0(Abs(Err(i)), 4)
      stmp = stmp & ","
      stmp = stmp & "+0.0000"
      Print #2, stmp
    Next i
    'FIANCO 2: ORDINE INVERTITO
    For i = NP1_G4 + NP2_G4 To NP1_G4 + 1 Step -1
      stmp = ""
      If (Err(i) >= 0) Then stmp = stmp & "+" Else stmp = stmp & "-"
      stmp = stmp & frmt0(Abs(Err(i)), 4)
      stmp = stmp & ","
      stmp = stmp & "+0.0000"
      Print #2, stmp
    Next i
    stmp = frmt0(Dintmis, 4) & "+0.0000"
    Print #2, stmp
    If (NP1_G4 + NP2_G4 + 1 < nMAXpntG4) Then
      For i = 1 To nMAXpntG4 - NP1_G4 - NP2_G4 - 1
        stmp = "+0.0000,+0.0000"
        Print #2, stmp
      Next i
    Else
      stmp = "+0.0000,+0.0000"
      Print #2, stmp
    End If
  Close #2
  'SEGNALAZIONE UTENTE
  WRITE_DIALOG " (HMI) <--- (CNC)"
Exit Sub

errACQUISIZIONE_CONTROLLO_GAPP4:
  If FreeFile > 0 Then Close
  MsgBox Error$(Err), 48, "SUB: ACQUISIZIONE_CONTROLLO_GAPP4"
Exit Sub

End Sub

Sub ACQUISIZIONE_CONTROLLO_ZEISS_ACCURA()
'
Dim stmp              As String
Dim riga              As String
Dim i                 As Integer
Dim j                 As Integer
Dim k                 As Integer
Dim jj()              As Integer
Dim ii()              As Integer
Dim EE()              As Double
Dim errF1(999)        As Double
Dim errF2(999)        As Double
Dim NomeFileCONTROLLO As String
Dim PathControllo     As String
Dim COMMENTO          As String
Dim RAGGIO_FONDO      As String
Dim ret               As Long
Dim LISTA_CONTROLLI() As String
Dim NOME_BASE_ROTORE  As String
Dim NOME_BASE_ROTORE1 As String
Dim Rmax              As Double
'
On Error GoTo errACQUISIZIONE_CONTROLLO_ZEISS_ACCURA
  '
  WRITE_DIALOG ""
  '
  Dim COORDINATE         As String
  Dim RADICE             As String
  Dim PERCORSO           As String
  Dim PERCORSO_CONTROLLI As String
  Dim PERCORSO_PROFILI   As String
  Dim sequenza           As String
  Dim npt                As Integer
  Dim TY_ROT             As String
  Dim PASSO              As Double
  Dim CONTROLLORE        As String
  Dim DM_ROT             As String
  Dim INVERSIONE         As Integer
  Dim Codice             As String
  Dim NP1                As Integer
  Dim NP2                As Integer
  '
  Call LEGGI_INFO_ROTORI(PERCORSO, COORDINATE, RADICE, PERCORSO_CONTROLLI, PERCORSO_PROFILI, sequenza, npt, TY_ROT, PASSO, CONTROLLORE, DM_ROT, INVERSIONE)
  '
  Codice = LEGGI_NomePezzo("ROTORI_ESTERNI")
  '
  'PERCORSO DEL RISULTATO DEL CONTROLLO
  PathControllo = PERCORSO_CONTROLLI & "\CORREZIONE.TXT"
If (Dir$(PathControllo) <> "") Then
  NP1 = GetInfo("CREAZIONE", "NpC_F1", RADICE & "\INFO.INI")
  NP2 = GetInfo("CREAZIONE", "NpC_F2", RADICE & "\INFO.INI")
  'PERCORSO DEL RISULTATO DEL CONTROLLO SUL PC
  NomeFileCONTROLLO = Codice & ".CHK"
  'IMPORTAZIONE RISULTATO DEL CONTROLLO ZEISS ACCURA
  If (Dir$(RADICE & "\" & NomeFileCONTROLLO) <> "") Then Kill RADICE & "\" & NomeFileCONTROLLO
  FileCopy PathControllo, RADICE & "\" & NomeFileCONTROLLO
  'ALLOCO LE VARIABILI IN BASE AL NUMERO MASSIMO DI PUNTI
  ReDim Preserve ii(NP1 + NP2): ReDim Preserve EE(NP1 + NP2)
  '
  'ACQUISIZIONE ERRORI NORMALI VALUTATI DAL PROGRAMMA DI CONTROLLO ZEISS_ACCURA
  Open RADICE & "\" & NomeFileCONTROLLO For Input As #1
    i = 0
    Do While Not EOF(1)
      'LEGGO LA PRIMA RIGA E LA PULISCO
      Line Input #1, stmp: stmp = UCase$(Trim$(stmp))
      i = i + 1: If i > (NP1 + NP2) Then Exit Do
      'RICAVO LA PRIMA COLONNA: INDICE
      j = InStr(stmp, Chr(9))
      If (j > 1) Then
        'LEGGO IL CAMPO
        riga = Left$(stmp, j - 1)
        'LO PRENDO E CONTINUO
        ii(i) = val(riga)
        riga = Trim$(Right$(stmp, Len(stmp) - j))
        EE(i) = val(riga)
      End If
    Loop
  Close #1
  '
  'COSTRUISCO I VETTORI DEGLI ERRORI SUI FIANCHI
  For i = 1 To NP1
    errF1(i) = -EE(i)
  Next i
  '
  For i = 1 To NP2
    errF2(i) = -EE(NP1 + NP2 + 1 - i)
  Next i
  '
  'COSTRUISCO IL FILE DEGLI ERRORI
  If (TY_ROT = "AXIAL") Then
    Open RADICE & "\RISROT" For Output As #2
      'COSTRUISCO LA RIGA RISROT CON I VALORI RICAVATI
      If (NP2 >= NP1) Then
        For i = 1 To NP1
          stmp = ""
          If (errF2(i) >= 0) Then
            stmp = stmp & "+" & frmt0(Abs(errF2(i)), 4)
          Else
            stmp = stmp & "-" & frmt0(Abs(errF2(i)), 4)
          End If
          stmp = stmp & ","
          If (errF1(i) >= 0) Then
            stmp = stmp & "+" & frmt0(Abs(errF1(i)), 4)
          Else
            stmp = stmp & "-" & frmt0(Abs(errF1(i)), 4)
          End If
          Print #2, stmp
        Next i
        For i = NP1 + 1 To NP2
          stmp = ""
          If (errF2(i) >= 0) Then
            stmp = stmp & "+" & frmt0(Abs(errF2(i)), 4)
          Else
            stmp = stmp & "-" & frmt0(Abs(errF2(i)), 4)
          End If
          stmp = stmp & ","
          stmp = stmp & "+" & frmt0(0, 4)
          Print #2, stmp
        Next i
        If (NP2 < 40) Then
          For i = 1 To 40 - NP2
            stmp = "+0.0000,+0.0000"
            Print #2, stmp
          Next i
        Else
          stmp = "+0.0000,+0.0000"
          Print #2, stmp
        End If
      Else
        For i = 1 To NP2
          stmp = ""
          If (errF2(i) >= 0) Then
            stmp = stmp & "+" & frmt0(Abs(errF2(i)), 4)
          Else
            stmp = stmp & "-" & frmt0(Abs(errF2(i)), 4)
          End If
          stmp = stmp & ","
          If (errF1(i) >= 0) Then
            stmp = stmp & "+" & frmt0(Abs(errF1(i)), 4)
          Else
            stmp = stmp & "-" & frmt0(Abs(errF1(i)), 4)
          End If
          Print #2, stmp
        Next i
        For i = NP2 + 1 To NP1
          stmp = ""
          stmp = stmp & "+" & frmt0(0, 4)
          stmp = stmp & ","
          If (errF1(i) >= 0) Then
            stmp = stmp & "+" & frmt0(Abs(errF1(i)), 4)
          Else
            stmp = stmp & "-" & frmt0(Abs(errF1(i)), 4)
          End If
          Print #2, stmp
        Next i
        If (NP1 < 40) Then
          For i = 1 To 40 - NP1
            stmp = "+0.0000,+0.0000"
            Print #2, stmp
          Next i
        Else
          stmp = "+0.0000,+0.0000"
          Print #2, stmp
        End If
      End If
    Close #2
  Else
    Open RADICE & "\EPMROT" For Output As #2
      'COSTRUISCO LA RIGA EPMROT CON I VALORI RICAVATI
      For i = 1 To NP1
        stmp = ""
        If (errF1(i) >= 0) Then
          stmp = stmp & "+" & frmt0(Abs(errF1(i)), 4)
        Else
          stmp = stmp & "-" & frmt0(Abs(errF1(i)), 4)
        End If
        stmp = stmp & ","
        stmp = stmp & "+0.0000"
        Print #2, stmp
      Next i
      For i = 1 To NP2
        stmp = ""
        If (errF2(i) >= 0) Then
          stmp = stmp & "+" & frmt0(Abs(errF2(i)), 4)
        Else
          stmp = stmp & "-" & frmt0(Abs(errF2(i)), 4)
        End If
        stmp = stmp & ","
        stmp = stmp & "+0.0000"
        Print #2, stmp
      Next i
      If (NP2 + NP1 < NmaxPntACV3) Then
        For i = 1 To NmaxPntACV3 - NP2 - NP1
          stmp = "+0.0000,+0.0000"
          Print #2, stmp
        Next i
      Else
        stmp = "+0.0000,+0.0000"
        Print #2, stmp
      End If
    Close #2
  End If
  'SEGNALAZIONE UTENTE
  WRITE_DIALOG " (HMI) <--- (ACCURA)"
Else
  StopRegieEvents
  MsgBox PathControllo & ": " & Error$(53) & Chr(13), 48, "SUB: ACQUISIZIONE_CONTROLLO_ZEISS_ACCURA"
  ResumeRegieEvents
End If
'
Exit Sub

errACQUISIZIONE_CONTROLLO_ZEISS_ACCURA:
  If FreeFile > 0 Then Close
  MsgBox Error$(Err), 48, "SUB: ACQUISIZIONE_CONTROLLO_ZEISS_ACCURA"
  'Exit Sub
  Resume Next

End Sub

Sub ACQUISIZIONE_CONTROLLO_DEA_IMAGE()
'
Dim stmp              As String
Dim riga              As String
Dim i                 As Integer
Dim j                 As Integer
Dim k                 As Integer
Dim jj()              As Integer
Dim ii()              As Integer
Dim EE()              As Double
Dim errF1(999)        As Double
Dim errF2(999)        As Double
Dim NomeFileCONTROLLO As String
Dim PathControllo     As String
Dim COMMENTO          As String
Dim RAGGIO_FONDO      As String
Dim ret               As Long
'
On Error GoTo errACQUISIZIONE_CONTROLLO_DEA_IMAGE
  '
  WRITE_DIALOG ""
  '
  Dim COORDINATE         As String
  Dim RADICE             As String
  Dim PERCORSO           As String
  Dim PERCORSO_CONTROLLI As String
  Dim PERCORSO_PROFILI   As String
  Dim sequenza           As String
  Dim npt                As Integer
  Dim TY_ROT             As String
  Dim PASSO              As Double
  Dim CONTROLLORE        As String
  Dim DM_ROT             As String
  Dim INVERSIONE         As Integer
  Dim Codice             As String
  Dim NP1                As Integer
  Dim NP2                As Integer
  '
  Call LEGGI_INFO_ROTORI(PERCORSO, COORDINATE, RADICE, PERCORSO_CONTROLLI, PERCORSO_PROFILI, sequenza, npt, TY_ROT, PASSO, CONTROLLORE, DM_ROT, INVERSIONE)
  '
  Codice = LEGGI_NomePezzo("ROTORI_ESTERNI")
  '
  'PERCORSO DEL RISULTATO DEL CONTROLLO SUL DEA
  PathControllo = PERCORSO_CONTROLLI & "\" & Codice & ".CHK"
If (Dir$(PathControllo) <> "") Then
  NP1 = GetInfo("CREAZIONE", "NpC_F1", RADICE & "\INFO.INI")
  NP2 = GetInfo("CREAZIONE", "NpC_F2", RADICE & "\INFO.INI")
  'PERCORSO DEL RISULTATO DEL CONTROLLO SUL PC
  NomeFileCONTROLLO = Codice & ".CHK"
  'IMPORTAZIONE RISULTATO DEL CONTROLLO ZEISS ACCURA
  If (Dir$(RADICE & "\" & NomeFileCONTROLLO) <> "") Then Kill RADICE & "\" & NomeFileCONTROLLO
  FileCopy PathControllo, RADICE & "\" & NomeFileCONTROLLO
  '
  'ALLOCO LE VARIABILI IN BASE AL NUMERO MASSIMO DI PUNTI
  ReDim Preserve ii(NP1 + NP2): ReDim Preserve EE(NP1 + NP2)
  '
  'ACQUISIZIONE ERRORI NORMALI VALUTATI DAL PROGRAMMA DI CONTROLLO ZEISS_MAUSER
  Open RADICE & "\" & NomeFileCONTROLLO For Input As #1
    Do
      'LEGGO LA PRIMA RIGA E LA PULISCO
      Line Input #1, stmp: stmp = UCase$(Trim$(stmp))
      'ESTRAZIONE DEL COMMENTO
      j = InStr(stmp, "COMMENT=")
      If (j > 0) Then
        j = InStr(stmp, "=")
        COMMENTO = Right(stmp, Len(stmp) - j)
        ret = WritePrivateProfileString("DEA_IMAGE", "COMMENTO", COMMENTO, RADICE & "\INFO.INI")
      End If
      'ESTRAZIONE RAGGIO INTERNO
      j = InStr(stmp, "ROOT POINT=")
      If (j > 0) Then
        j = InStr(stmp, "=")
        RAGGIO_FONDO = Right(stmp, Len(stmp) - j)
        ret = WritePrivateProfileString("DEA_IMAGE", "RAGGIO_FONDO", RAGGIO_FONDO, RADICE & "\INFO.INI")
      End If
      'CONTROLLO SE SONO ARRIVATO ALLA SEZIONE DATI
      j = InStr(stmp, "$POINTS")
    Loop While (j <= 0) Or EOF(1)
    '
    Dim COLONNA1() As Double
    Dim COLONNA2() As Double
    Dim COLONNA3() As Double
    Dim COLONNA4() As Double
    Dim COLONNA5() As Double
    Dim COLONNA6() As Double
    Dim COLONNA7() As Double
    '
    i = 0
    Do While Not EOF(1)
      'LEGGO LA PRIMA RIGA E LA PULISCO
      Line Input #1, stmp: stmp = UCase$(Trim$(stmp))
      i = i + 1
      ii(i) = i
      '
      ReDim Preserve COLONNA1(i)
      ReDim Preserve COLONNA2(i)
      ReDim Preserve COLONNA3(i)
      ReDim Preserve COLONNA4(i)
      ReDim Preserve COLONNA5(i)
      ReDim Preserve COLONNA6(i)
      ReDim Preserve COLONNA7(i)
      '
      'RICAVO LA PRIMA COLONNA: INDICE
      j = InStr(stmp, ",")
      If (j > 1) Then
        'LEGGO IL CAMPO
        riga = Left$(stmp, j - 1)
        'LO PRENDO E CONTINUO
        COLONNA1(i) = val(riga)
        stmp = Trim$(Right$(stmp, Len(stmp) - j))
        'RICAVO LA SECONDA COLONNA
        j = InStr(stmp, ",")
        If (j > 1) Then
          'LEGGO IL CAMPO
          riga = Left$(stmp, j - 1)
          'LO PRENDO E CONTINUO
          COLONNA2(i) = val(riga)
          stmp = Trim$(Right$(stmp, Len(stmp) - j))
          'RICAVO LA TERZA COLONNA
          j = InStr(stmp, ",")
          If (j > 1) Then
            'LEGGO IL CAMPO
            riga = Left$(stmp, j - 1)
            'LO PRENDO E CONTINUO
            COLONNA3(i) = val(riga)
            stmp = Trim$(Right$(stmp, Len(stmp) - j))
            'RICAVO LA QUARTA COLONNA
            j = InStr(stmp, ",")
            If (j > 1) Then
              'LEGGO IL CAMPO
              riga = Left$(stmp, j - 1)
              'LO PRENDO E CONTINUO
              COLONNA4(i) = val(riga)
              stmp = Trim$(Right$(stmp, Len(stmp) - j))
              'RICAVO LA QUINTA COLONNA
              j = InStr(stmp, ",")
              If (j > 1) Then
                'LEGGO IL CAMPO
                riga = Left$(stmp, j - 1)
                'LO PRENDO E CONTINUO
                COLONNA5(i) = val(riga)
                stmp = Trim$(Right$(stmp, Len(stmp) - j))
                'RICAVO LA SESTA COLONNA
                j = InStr(stmp, ",")
                If (j > 1) Then
                  'LEGGO IL CAMPO
                  riga = Left$(stmp, j - 1)
                  'LO PRENDO E CONTINUO
                  COLONNA6(i) = val(riga)
                  stmp = Trim$(Right$(stmp, Len(stmp) - j))
                  'RICAVO LA SETTIMA COLONNA
                  'LEGGO IL CAMPO
                  riga = stmp
                  'LO PRENDO E CONTINUO
                  COLONNA7(i) = val(riga)
                  EE(i) = val(riga)
                End If
              End If
            End If
          End If
        End If
      End If
    Loop
  Close #1
  '
  'COSTRUISCO I VETTORI DEGLI ERRORI SUI FIANCHI
  For i = 1 To NP1
    errF1(i) = -EE(i)
  Next i
  '
  For i = 1 To NP2
    errF2(i) = -EE(NP1 + NP2 + 1 - i)
  Next i
  '
  'COSTRUISCO IL FILE DEGLI ERRORI
  If (TY_ROT = "AXIAL") Then
    Open RADICE & "\RISROT" For Output As #2
      'COSTRUISCO LA RIGA RISROT CON I VALORI RICAVATI
      If (NP2 >= NP1) Then
        For i = 1 To NP1
          stmp = ""
          If (errF2(i) >= 0) Then
            stmp = stmp & "+" & frmt0(Abs(errF2(i)), 4)
          Else
            stmp = stmp & "-" & frmt0(Abs(errF2(i)), 4)
          End If
          stmp = stmp & ","
          If (errF1(i) >= 0) Then
            stmp = stmp & "+" & frmt0(Abs(errF1(i)), 4)
          Else
            stmp = stmp & "-" & frmt0(Abs(errF1(i)), 4)
          End If
          Print #2, stmp
        Next i
        For i = NP1 + 1 To NP2
          stmp = ""
          If (errF2(i) >= 0) Then
            stmp = stmp & "+" & frmt0(Abs(errF2(i)), 4)
          Else
            stmp = stmp & "-" & frmt0(Abs(errF2(i)), 4)
          End If
          stmp = stmp & ","
          stmp = stmp & "+" & frmt0(0, 4)
          Print #2, stmp
        Next i
        If (NP2 < 40) Then
          For i = 1 To 40 - NP2
            stmp = "+0.0000,+0.0000"
            Print #2, stmp
          Next i
        Else
          stmp = "+0.0000,+0.0000"
          Print #2, stmp
        End If
      Else
        For i = 1 To NP2
          stmp = ""
          If (errF2(i) >= 0) Then
            stmp = stmp & "+" & frmt0(Abs(errF2(i)), 4)
          Else
            stmp = stmp & "-" & frmt0(Abs(errF2(i)), 4)
          End If
          stmp = stmp & ","
          If (errF1(i) >= 0) Then
            stmp = stmp & "+" & frmt0(Abs(errF1(i)), 4)
          Else
            stmp = stmp & "-" & frmt0(Abs(errF1(i)), 4)
          End If
          Print #2, stmp
        Next i
        For i = NP2 + 1 To NP1
          stmp = ""
          stmp = stmp & "+" & frmt0(0, 4)
          stmp = stmp & ","
          If (errF1(i) >= 0) Then
            stmp = stmp & "+" & frmt0(Abs(errF1(i)), 4)
          Else
            stmp = stmp & "-" & frmt0(Abs(errF1(i)), 4)
          End If
          Print #2, stmp
        Next i
        If (NP1 < 40) Then
          For i = 1 To 40 - NP1
            stmp = "+0.0000,+0.0000"
            Print #2, stmp
          Next i
        Else
          stmp = "+0.0000,+0.0000"
          Print #2, stmp
        End If
      End If
    Close #2
  Else
    Open RADICE & "\EPMROT" For Output As #2
      'COSTRUISCO LA RIGA EPMROT CON I VALORI RICAVATI
      For i = 1 To NP1
        stmp = ""
        If (errF1(i) >= 0) Then
          stmp = stmp & "+" & frmt0(Abs(errF1(i)), 4)
        Else
          stmp = stmp & "-" & frmt0(Abs(errF1(i)), 4)
        End If
        stmp = stmp & ","
        stmp = stmp & "+0.0000"
        Print #2, stmp
      Next i
      For i = 1 To NP2
        stmp = ""
        If (errF2(i) >= 0) Then
          stmp = stmp & "+" & frmt0(Abs(errF2(i)), 4)
        Else
          stmp = stmp & "-" & frmt0(Abs(errF2(i)), 4)
        End If
        stmp = stmp & ","
        stmp = stmp & "+0.0000"
        Print #2, stmp
      Next i
      If (NP2 + NP1 < NmaxPntACV3) Then
        For i = 1 To NmaxPntACV3 - NP2 - NP1
          stmp = "+0.0000,+0.0000"
          Print #2, stmp
        Next i
      Else
        stmp = "+0.0000,+0.0000"
        Print #2, stmp
      End If
    Close #2
  End If
  'SEGNALAZIONE UTENTE
  WRITE_DIALOG " (HMI) <--- (DEA)"
Else
  StopRegieEvents
  MsgBox PathControllo & ": " & Error$(53) & Chr(13), 48, "SUB: ACQUISIZIONE_CONTROLLO_DEA_IMAGE"
  ResumeRegieEvents
End If
'
Exit Sub

errACQUISIZIONE_CONTROLLO_DEA_IMAGE:
  If FreeFile > 0 Then Close
  MsgBox Error$(Err), 48, "SUB: ACQUISIZIONE_CONTROLLO_DEA_IMAGE"
  Exit Sub
  'Resume Next

End Sub

Sub ACQUISIZIONE_CONTROLLO_QUINDOS()
'
Dim stmp              As String
Dim riga              As String
Dim i                 As Integer
Dim j                 As Integer
Dim k                 As Integer
Dim jj()              As Integer
Dim ii()              As Integer
Dim EE()              As Double
Dim errF1(999)        As Double
Dim errF2(999)        As Double
Dim NomeFileCONTROLLO As String
Dim PathControllo     As String
Dim COMMENTO          As String
Dim RAGGIO_FONDO      As String
Dim ret               As Long
'
On Error GoTo errACQUISIZIONE_CONTROLLO_QUINDOS
  '
  WRITE_DIALOG ""
  '
  Dim COORDINATE         As String
  Dim RADICE             As String
  Dim PERCORSO           As String
  Dim PERCORSO_CONTROLLI As String
  Dim PERCORSO_PROFILI   As String
  Dim sequenza           As String
  Dim npt                As Integer
  Dim TY_ROT             As String
  Dim PASSO              As Double
  Dim CONTROLLORE        As String
  Dim DM_ROT             As String
  Dim INVERSIONE         As Integer
  Dim Codice             As String
  Dim NP1                As Integer
  Dim NP2                As Integer
  '
  Call LEGGI_INFO_ROTORI(PERCORSO, COORDINATE, RADICE, PERCORSO_CONTROLLI, PERCORSO_PROFILI, sequenza, npt, TY_ROT, PASSO, CONTROLLORE, DM_ROT, INVERSIONE)
  '
  Codice = LEGGI_NomePezzo("ROTORI_ESTERNI")
  '
  'PERCORSO DEL RISULTATO DEL CONTROLLO SUL DEA
  PathControllo = PERCORSO_CONTROLLI & "\" & Codice & ".CHK"
If (Dir$(PathControllo) <> "") Then
  NP1 = GetInfo("CREAZIONE", "NpC_F1", RADICE & "\INFO.INI")
  NP2 = GetInfo("CREAZIONE", "NpC_F2", RADICE & "\INFO.INI")
  'PERCORSO DEL RISULTATO DEL CONTROLLO SUL PC
  NomeFileCONTROLLO = Codice & ".CHK"
  'IMPORTAZIONE RISULTATO DEL CONTROLLO ZEISS ACCURA
  If (Dir$(RADICE & "\" & NomeFileCONTROLLO) <> "") Then Kill RADICE & "\" & NomeFileCONTROLLO
  FileCopy PathControllo, RADICE & "\" & NomeFileCONTROLLO
  '
  'ALLOCO LE VARIABILI IN BASE AL NUMERO MASSIMO DI PUNTI
  ReDim Preserve ii(NP1 + NP2): ReDim Preserve EE(NP1 + NP2)
  '
  'ACQUISIZIONE ERRORI NORMALI VALUTATI DAL PROGRAMMA DI CONTROLLO ZEISS_MAUSER
  Open RADICE & "\" & NomeFileCONTROLLO For Input As #1
    'LEGGO LA PRIMA RIGA E LA PULISCO
    Line Input #1, stmp: stmp = UCase$(Trim$(stmp))
    i = 0
    Do While Not EOF(1)
      'LEGGO LA RIGA E LA PULISCO
      Line Input #1, stmp: stmp = UCase$(Trim$(stmp))
      i = i + 1
      'LO PRENDO E CONTINUO
      If (stmp <> "$END") Then
        EE(i) = val(stmp)
      Else
        Exit Do
      End If
    Loop
  Close #1
  '
  'COSTRUISCO I VETTORI DEGLI ERRORI SUI FIANCHI
  For i = 1 To NP1
    errF1(i) = -EE(i)
  Next i
  '
  For i = 1 To NP2
    errF2(i) = -EE(NP1 + NP2 + 1 - i)
  Next i
  '
  'COSTRUISCO IL FILE DEGLI ERRORI
  If (TY_ROT = "AXIAL") Then
    Open RADICE & "\RISROT" For Output As #2
      'COSTRUISCO LA RIGA RISROT CON I VALORI RICAVATI
      If (NP2 >= NP1) Then
        For i = 1 To NP1
          stmp = ""
          If (errF2(i) >= 0) Then
            stmp = stmp & "+" & frmt0(Abs(errF2(i)), 4)
          Else
            stmp = stmp & "-" & frmt0(Abs(errF2(i)), 4)
          End If
          stmp = stmp & ","
          If (errF1(i) >= 0) Then
            stmp = stmp & "+" & frmt0(Abs(errF1(i)), 4)
          Else
            stmp = stmp & "-" & frmt0(Abs(errF1(i)), 4)
          End If
          Print #2, stmp
        Next i
        For i = NP1 + 1 To NP2
          stmp = ""
          If (errF2(i) >= 0) Then
            stmp = stmp & "+" & frmt0(Abs(errF2(i)), 4)
          Else
            stmp = stmp & "-" & frmt0(Abs(errF2(i)), 4)
          End If
          stmp = stmp & ","
          stmp = stmp & "+" & frmt0(0, 4)
          Print #2, stmp
        Next i
        If (NP2 < 40) Then
          For i = 1 To 40 - NP2
            stmp = "+0.0000,+0.0000"
            Print #2, stmp
          Next i
        Else
          stmp = "+0.0000,+0.0000"
          Print #2, stmp
        End If
      Else
        For i = 1 To NP2
          stmp = ""
          If (errF2(i) >= 0) Then
            stmp = stmp & "+" & frmt0(Abs(errF2(i)), 4)
          Else
            stmp = stmp & "-" & frmt0(Abs(errF2(i)), 4)
          End If
          stmp = stmp & ","
          If (errF1(i) >= 0) Then
            stmp = stmp & "+" & frmt0(Abs(errF1(i)), 4)
          Else
            stmp = stmp & "-" & frmt0(Abs(errF1(i)), 4)
          End If
          Print #2, stmp
        Next i
        For i = NP2 + 1 To NP1
          stmp = ""
          stmp = stmp & "+" & frmt0(0, 4)
          stmp = stmp & ","
          If (errF1(i) >= 0) Then
            stmp = stmp & "+" & frmt0(Abs(errF1(i)), 4)
          Else
            stmp = stmp & "-" & frmt0(Abs(errF1(i)), 4)
          End If
          Print #2, stmp
        Next i
        If (NP1 < 40) Then
          For i = 1 To 40 - NP1
            stmp = "+0.0000,+0.0000"
            Print #2, stmp
          Next i
        Else
          stmp = "+0.0000,+0.0000"
          Print #2, stmp
        End If
      End If
    Close #2
  Else
    Open RADICE & "\EPMROT" For Output As #2
      'COSTRUISCO LA RIGA EPMROT CON I VALORI RICAVATI
      For i = 1 To NP1
        stmp = ""
        If (errF1(i) >= 0) Then
          stmp = stmp & "+" & frmt0(Abs(errF1(i)), 4)
        Else
          stmp = stmp & "-" & frmt0(Abs(errF1(i)), 4)
        End If
        stmp = stmp & ","
        stmp = stmp & "+0.0000"
        Print #2, stmp
      Next i
      For i = 1 To NP2
        stmp = ""
        If (errF2(i) >= 0) Then
          stmp = stmp & "+" & frmt0(Abs(errF2(i)), 4)
        Else
          stmp = stmp & "-" & frmt0(Abs(errF2(i)), 4)
        End If
        stmp = stmp & ","
        stmp = stmp & "+0.0000"
        Print #2, stmp
      Next i
      If (NP2 + NP1 < NmaxPntACV3) Then
        For i = 1 To NmaxPntACV3 - NP2 - NP1
          stmp = "+0.0000,+0.0000"
          Print #2, stmp
        Next i
      Else
        stmp = "+0.0000,+0.0000"
        Print #2, stmp
      End If
    Close #2
  End If
  'SEGNALAZIONE UTENTE
  WRITE_DIALOG " (HMI) <--- (QUINDOS)"
Else
  StopRegieEvents
  MsgBox PathControllo & ": " & Error$(53) & Chr(13), 48, "SUB: ACQUISIZIONE_CONTROLLO_QUINDOS"
  ResumeRegieEvents
End If
'
Exit Sub

errACQUISIZIONE_CONTROLLO_QUINDOS:
  If FreeFile > 0 Then Close
  MsgBox Error$(Err), 48, "SUB: ACQUISIZIONE_CONTROLLO_QUINDOS"
  'Exit Sub
  Resume Next

End Sub

Sub ACQUISIZIONE_CONTROLLO_KLINGELNBERG()
'
Dim stmp          As String
Dim PathControllo As String
'
Dim i   As Integer
Dim j   As Integer
Dim k   As Integer
Dim k1  As Integer
Dim k2  As Integer
Dim k3  As Integer
'
Dim ii() As Integer
Dim XX() As Double
Dim YY() As Double
Dim NX() As Double
Dim NY() As Double
Dim EE() As Double
'
Dim INTERPOLAZIONE_LINEARE As String * 1
'
Dim errF1()     As Double
Dim errF2()     As Double
Dim NP1_KLN     As Integer
Dim NP2_KLN     As Integer
'*****FIL
Dim NpCF1       As Integer
Dim NpCF2       As Integer
Dim NPF1        As Integer
Dim NPF2        As Integer
Dim INDICE      As Integer
Dim RigaSplit() As String
Dim IdxColumn   As Integer
Dim ValColumn   As Double
Dim VORTIEFE    As Single
Dim rag         As Double
Dim Rmin        As Double
Dim erint       As Double
Dim LastIndex   As Integer
'*****
'
Dim Hr()  As Double
Dim RRchk As Double
Dim AAchk As Double
Dim BBchk As Double
Dim VVchk As Double
Dim TTchk As Double
'
Dim CONTROLLO_ASSIALE As Integer
Dim DIRETTORIO        As String
Dim Codice            As String
Dim ESTENSIONE        As String
'
On Error GoTo errKLINGELNBERG_ACQUISIZIONE
  '
  WRITE_DIALOG ""
  '
  Dim COORDINATE         As String
  Dim RADICE             As String
  Dim PERCORSO           As String
  Dim PERCORSO_CONTROLLI As String
  Dim PERCORSO_PROFILI   As String
  Dim sequenza           As String
  Dim npt                As Integer
  Dim TY_ROT             As String
  Dim PASSO              As Double
  Dim CONTROLLORE        As String
  Dim DM_ROT             As String
  Dim INVERSIONE         As Integer
  '
  Call LEGGI_INFO_ROTORI(PERCORSO, COORDINATE, RADICE, PERCORSO_CONTROLLI, PERCORSO_PROFILI, sequenza, npt, TY_ROT, PASSO, CONTROLLORE, DM_ROT, INVERSIONE)
  '
  Codice = LEGGI_NomePezzo("ROTORI_ESTERNI")
  Codice = Replace(Codice, "#", "")
  ESTENSIONE = GetInfo("Configurazione", "ESTENSIONE_KLINGELBERG", Path_LAVORAZIONE_INI)
  ESTENSIONE = UCase$(Trim$(ESTENSIONE))
  '
  'PERCORSO DEL RISULTATO DEL CONTROLLO
  stmp = GetInfo("Configurazione", "RICHIESTA_MODIFICA_NOME", Path_LAVORAZIONE_INI)
  stmp = Trim(UCase(stmp))
  If (stmp = "N") Then
    If (Len(ESTENSIONE) > 0) Then
      PathControllo = Codice & "." & ESTENSIONE
    Else
      PathControllo = Codice
    End If
  Else
    If (Len(ESTENSIONE) > 0) Then
      PathControllo = InputBox("IMPORT ERROR TEXT FILE", "KLINGELNBERG CHECK", Codice & "." & ESTENSIONE)
    Else
      PathControllo = InputBox("IMPORT ERROR TEXT FILE", "KLINGELNBERG CHECK", Codice)
    End If
    PathControllo = Trim$(UCase$(PathControllo))
  End If
  If (Len(PathControllo) <= 0) Then
    WRITE_DIALOG "Operation aborted by user!!!"
    Exit Sub
  End If
  PathControllo = PERCORSO_CONTROLLI & "\" & PathControllo
If (Dir$(PathControllo) <> "") Then
  'IMPORTAZIONE RISULTATO DEL CONTROLLO KLINGELNBERG
  FileCopy PathControllo, RADICE & "\" & "KLINGCHK.TXT"
  '*****FIL
  ' IMPORTAZIONE VETTORE PUNTI DA CONTROLLARE
  'Call PuntiCHK
  'Vettore flag per eliminare i punti da non controllare del profilo.
  'Se IDX(I)=1 allora I e' un punto controllato.
  'Se IDX(I)=0 allora I e' un punto non controllato.
  'ESEMPIO SEQUENZA: 1.3.4.5. OPPURE 0
  Dim NoCHKp  As Integer
  Dim NoCHK() As Integer
  ReDim IdxCHK(npt)
  For i = 1 To npt
    IdxCHK(i) = 1
  Next i
  i = 1: j = 0: NoCHKp = 0
  ReDim NoCHK(NoCHKp)
  j = InStr(i, sequenza, ".")
  Do While (j > 0)
    NoCHKp = NoCHKp + 1
    ReDim Preserve NoCHK(NoCHKp)
    NoCHK(NoCHKp) = Abs(val(Mid(sequenza, i, j - i)))
    IdxCHK(NoCHK(NoCHKp)) = 0
    i = j + 1
    j = InStr(i, sequenza, ".")
  Loop
  'Numero totale di punti F1 e F2
  'NPF1 = NPM1 - 1
  'NPF2 = NPM2 - 1
  'Call RicercaMinimoRotore(Radice & "\RABROT.KLN", NPF1, NPF2)
  'NP1_KLN = 0:  NP2_KLN = 0
  'NpCF1 = 0:    NpCF2 = 0
  'For i = 1 To npt
  '  If (IdxCHK(i) <> 0) Then
  '      If (i <= NPF1) Then
  '          NpCF1 = NpCF1 + 1
  '      Else
  '          NpCF2 = NpCF2 + 1
  '      End If
  '  End If
  'Next i
  NPF1 = GetInfo("CREAZIONE", "NpM_F1", RADICE & "\INFO.INI")
  NPF2 = GetInfo("CREAZIONE", "NpM_F2", RADICE & "\INFO.INI")
  NpCF1 = GetInfo("CREAZIONE", "NpC_F1", RADICE & "\INFO.INI")
  NpCF2 = GetInfo("CREAZIONE", "NpC_F2", RADICE & "\INFO.INI")
  '
Dim TIPO_FORMATO As String
  '
  TIPO_FORMATO = GetInfo("Configurazione", "FORMATO_KLINGELBERG", Path_LAVORAZIONE_INI)
  '
If (TIPO_FORMATO = "TESTO") Then
  'CONVERSIONE DA FORMATO KLINGELNBERG in RISROT o EPMROT
  Open RADICE & "\KLINGCHK.TXT" For Input As #1
    INTERPOLAZIONE_LINEARE = " "
    Rmin = 999:    ReDim EE(1): ReDim NX(1): ReDim NY(1)
    LastIndex = 1: i = 0:       INDICE = 0
    Do While Not EOF(1)
      'LETTURA RIGA
      Line Input #1, stmp: stmp = Trim$(stmp)
      If InStr(1, stmp, "|") > 0 Then
        RigaSplit = Split(stmp, "|")
      Else
        RigaSplit = Split(stmp, " ")
      End If
      If UBound(RigaSplit) > -1 Then
        If IsNumeric(RigaSplit(0)) Then
          INDICE = CInt(RigaSplit(0))
          If INDICE < LastIndex Then
            Close #1
            StopRegieEvents
            MsgBox "WRONG SEQUENCE IN " & PathControllo, 48, "ERROR"
            ResumeRegieEvents
            Exit Sub
          End If
          If (INDICE > npt) Then
            Close #1
            StopRegieEvents
            MsgBox "Checked points are greater than theoretical profile!!!!", 48, "ERROR"
            ResumeRegieEvents
            Exit Sub
          End If
          IdxColumn = 0
          ValColumn = 0
          For i = 0 To UBound(RigaSplit)
            If IsNumeric(RigaSplit(i)) And (Trim(RigaSplit(i)) <> "") Then
              IdxColumn = IdxColumn + 1
              ValColumn = val(RigaSplit(i))
              Select Case IdxColumn
                Case 2
                  ReDim Preserve XX(INDICE)
                  XX(INDICE) = ValColumn
                Case 3
                  ReDim Preserve YY(INDICE)
                  YY(INDICE) = ValColumn
                Case 4
                  ReDim Preserve NX(INDICE)
                  NX(INDICE) = ValColumn
                Case 5
                  ReDim Preserve NY(INDICE)
                  NY(INDICE) = ValColumn
                Case 6
                  If (INDICE > LastIndex + 1) Then
                    If (INTERPOLAZIONE_LINEARE = " ") Then
                      'RICHIESTA DI INTERPOLAZIONE LINEARE DEGLI ERRORI SUI PUNTI NON CONTROLLATI
                      StopRegieEvents
                      k = MsgBox("Use liner interpolation between not checked points?", 32 + 4, "WARNING: KLINGELNBERG CHECK")
                      ResumeRegieEvents
                      If (k = vbYes) Then
                        INTERPOLAZIONE_LINEARE = "Y"
                      Else
                        INTERPOLAZIONE_LINEARE = "N"
                      End If
                    End If
                    For j = LastIndex + 1 To INDICE - 1
                      ReDim Preserve EE(j)
                      If INTERPOLAZIONE_LINEARE = "Y" Then
                        EE(j) = 9999
                      End If
                      If INTERPOLAZIONE_LINEARE = "N" Then
                        EE(j) = 0
                      End If
                    Next j
                  End If
                  ReDim Preserve EE(INDICE)
                  EE(INDICE) = ValColumn
              End Select
            End If
          Next i
          If (INDICE <= NPF1) Then
            NP1_KLN = NP1_KLN + 1
          Else
            NP2_KLN = NP2_KLN + 1
          End If
          rag = Sqr(XX(INDICE) ^ 2 + YY(INDICE) ^ 2)
          If (TY_ROT = "AXIAL") Then rag = XX(INDICE)
          If Rmin > rag Then Rmin = rag: erint = EE(INDICE)
          LastIndex = INDICE
        End If
      End If
    Loop
  Close #1
Else
  'CONVERSIONE DA FORMATO KLINGELNBERG in RISROT o EPMROT
  Open RADICE & "\KLINGCHK.TXT" For Input As #1
    INTERPOLAZIONE_LINEARE = " "
    Rmin = 999:    ReDim EE(1): ReDim NX(1): ReDim NY(1)
    LastIndex = 1: i = 0:       INDICE = 0
    Do While Not EOF(1)
      'LETTURA RIGA
      Line Input #1, stmp: stmp = Trim$(stmp)
      'RICAVO L'INDICE DEL PUNTO CONTROLLATO DEL PROFILO COMPLETO
      If (Len(stmp) > 0) Then
        'RICAVO IL VALORE DEL VORTIEFE
        If (InStr(stmp, "P9") > 0) Then
          k1 = InStr(stmp, ":")
          k2 = InStr(k1 + 1, stmp, ":")
          VORTIEFE = val(Trim$(Mid$(stmp, k2 + 1, Len(stmp) - k2)))
        End If
        'RICAVO IL NUMERO DEI PUNTI DA LEGGERE
        If (InStr(stmp, "PROFILEDEV") > 0) Then
          k1 = InStr(stmp, ":")
          k2 = InStr(k1 + 1, stmp, ":")
          'LETTURA PUNTI CONTROLLATI
          k1 = val(Right(stmp, Len(stmp) - k2))
          For k3 = 1 To k1
            Line Input #1, stmp: stmp = Trim$(stmp)
            If (InStr(stmp, "PROFILEDEV") > 0) Or (InStr(stmp, "LEAD") > 0) Or (InStr(stmp, "PARA") > 0) Then
                LastIndex = INDICE
                Exit For
            End If
            If InStr(1, stmp, "|") > 0 Then
              RigaSplit = Split(stmp, "|")
            Else
              RigaSplit = Split(stmp, " ")
            End If
            If UBound(RigaSplit) > -1 Then
              If IsNumeric(RigaSplit(0)) Then
                INDICE = CInt(RigaSplit(0))
                If INDICE < LastIndex Then
                  MsgBox "WRONG SEQUENCE IN " & PathControllo, 48, "ERROR"
                  Close #1
                  Exit Sub
                End If
                If (INDICE > npt) Then
                  MsgBox "Checked points are greater than theoretical profile!!!!", 48, "ERRORE"
                  Close #1
                  Exit Sub
                End If
                IdxColumn = 0: ValColumn = 0
                For i = 0 To UBound(RigaSplit)
                  If IsNumeric(RigaSplit(i)) And (Trim(RigaSplit(i)) <> "") Then
                    IdxColumn = IdxColumn + 1
                    ValColumn = val(RigaSplit(i))
                    Select Case IdxColumn
                      Case 2
                        ReDim Preserve XX(INDICE)
                        XX(INDICE) = ValColumn
                      Case 3
                        ReDim Preserve YY(INDICE)
                        YY(INDICE) = ValColumn
                      Case 4
                        ReDim Preserve NX(INDICE)
                        NX(INDICE) = ValColumn
                      Case 5
                        ReDim Preserve NY(INDICE)
                        NY(INDICE) = ValColumn
                      Case 6
                        If (INDICE > LastIndex + 1) Then
                          If (INTERPOLAZIONE_LINEARE = " ") Then
                            'RICHIESTA DI INTERPOLAZIONE LINEARE DEGLI ERRORI SUI PUNTI NON CONTROLLATI
                            StopRegieEvents
                            k = MsgBox("Use liner interpolation between not checked points?", 32 + 4, "WARNING: PROFILE CHECK ON KLINGELNBEG")
                            ResumeRegieEvents
                            If (k = vbYes) Then
                              INTERPOLAZIONE_LINEARE = "Y"
                            Else
                              INTERPOLAZIONE_LINEARE = "N"
                            End If
                          End If
                            For j = LastIndex + 1 To INDICE - 1
                              ReDim Preserve EE(j)
                              If (INTERPOLAZIONE_LINEARE = "Y") Then
                                EE(j) = 9999
                              End If
                              If (INTERPOLAZIONE_LINEARE = "N") Then
                                EE(j) = 0
                              End If
                            Next j
                          End If
                          ReDim Preserve EE(INDICE): EE(INDICE) = ValColumn
                    End Select
                  End If
                Next i
                If (INDICE <= NPF1) Then
                  NP1_KLN = NP1_KLN + 1
                Else
                  NP2_KLN = NP2_KLN + 1
                End If
                rag = Sqr(XX(INDICE) ^ 2 + YY(INDICE) ^ 2)
                If (TY_ROT = "AXIAL") Then rag = XX(INDICE)
                If (Rmin > rag) Then Rmin = rag: erint = EE(INDICE)
                LastIndex = INDICE
              End If
            End If
          Next k3
          'ESCO DAL CICLO PRINCIPALE
          Exit Do
        End If
      End If
      'IL FILE CONTIENE IL CONTROLLO DEL PASSO
      If (InStr(stmp, "LEAD") > 0) Then Exit Do
    Loop
  Close #1
End If
  '
  'registra file profilo misurato
  GoSub Wrt_xyz
  GoSub Wrt_dxf
  '
  ReDim Preserve EE(npt)
  'Gestione dell'eventuale esclusione dell'ultimo punto dal klingelnberg
  If (INDICE < npt) And (IdxCHK(npt) = 1) Then
    EE(npt) = EE(npt - 1)
  End If
  '
  'INVERSIONE RISULTATO QUANDO IL PROFILO E' INVERTITO
  'VERIFICATO SUL CF75M WOYO PER NON CAMBIARE IL CONTROLLO PIAZZATO IN PRECEDENZA
  'If INVERSIONE = 1 Then
  '  Dim EETmp() As Double
  '  ReDim EETmp(npt)
  '  For i = 1 To npt
  '    EETmp(i) = EE(i)
  '  Next i
  '  For i = 1 To npt
  '    EE(npt + 1 - i) = EETmp(i)
  '  Next i
  '  ReDim EETmp(0)
  'End If
  '
  '***** FIL
  '
  'DiaIntMis = DiaInt + erint * 2
  'DiaIntMis = (Rmin + erint) * 2
  '
  '*****
  For i = 1 To npt - 1
    If (IdxCHK(i) = 0) Then
        EE(i) = EE(i + 1)
    End If
  Next i
  '
  'CHIEDO CONFERMA SUL TIPO DI CONTROLLO ESEGUITO
  If (TY_ROT = "AXIAL") Then
    stmp = "PROFILE HAVE BEEN CHECKED ALONG AXIAL SECTION ?"
    StopRegieEvents
    CONTROLLO_ASSIALE = MsgBox(stmp, vbYesNo + vbInformation, "WARNING: KLINGELNBERG CHECK")
    ResumeRegieEvents
    If (CONTROLLO_ASSIALE <> 6) Then CONTROLLO_ASSIALE = 7
  Else
    CONTROLLO_ASSIALE = 7
  End If
    '
Dim FstIndex  As Integer: FstIndex = 0
Dim LstIndex  As Integer: LstIndex = 0
Dim tX()      As Double
Dim tY()      As Double
    '
    stmp = RADICE & "\RABROT.KLN"
    Open stmp For Input As #1
    i = 0
    While Not EOF(1)
      i = i + 1
      Input #1, RRchk, AAchk, BBchk
      ReDim Preserve tX(i): ReDim Preserve tY(i)
      If (CONTROLLO_ASSIALE = 6) Then
        tX(i) = (AAchk / 360) * PASSO
        tY(i) = RRchk
      Else
        tX(i) = RRchk * Sin(AAchk * PG / 180)
        tY(i) = RRchk * Cos(AAchk * PG / 180)
      End If
    Wend
    Close #1
    For i = 2 To npt - 1
        If (EE(i) = 9999) And (LstIndex = 0) Then
            FstIndex = i - 1
            LstIndex = 9999
        End If
        If (EE(i) <> 9999) And (FstIndex <> 0) Then
            LstIndex = i
            Dim Delta As Double
            Dim SDist As Double
            SDist = 0
            For j = FstIndex + 1 To LstIndex
                SDist = SDist + (Sqr((tX(j) - tX(j - 1)) ^ 2 + (tY(j) - tY(j - 1)) ^ 2))
            Next j
            Delta = (EE(LstIndex) - EE(FstIndex)) / SDist
            For j = FstIndex + 1 To LstIndex - 1
                EE(j) = EE(j - 1) + Delta * (Sqr((tX(j) - tX(j - 1)) ^ 2 + (tY(j) - tY(j - 1)) ^ 2))
            Next j
            FstIndex = 0
            LstIndex = 0
        End If
    Next i
    j = 0
    ReDim errF1(NpCF1)
    ReDim errF2(NpCF2)
    For i = 1 To npt
      If (IdxCHK(i) <> 0) Then
        If (i <= NpCF1) Then
            j = j + 1
            errF1(j) = EE(i)
        Else
            j = j + 1
            errF2(NpCF2 - j + 1) = EE(i)
        End If
      End If
      If (i = NpCF1) Then j = 0
    Next i
   '
  'LETTURA PROFILO RAB DI CONTROLLO
  Open RADICE & "\RABROT.KLN" For Input As #1
    If (CONTROLLO_ASSIALE = 6) Then
      '*****************************************************************************
      'TRASFORMAZIONE DA ERRORI ASSIALI SUL PROFILO ROTORE A ERRORI SUL PROFILO MOLA
      '*****************************************************************************
      ' PRG | KLN | ACV
      '  X  |  Y  |  X
      '  Y  |  Z  |  V
      '*****************************************************************************
      i = 0
      While Not EOF(1)
        i = i + 1
        ReDim Preserve Hr(i)
        Input #1, RRchk, AAchk, BBchk
        'APPARENT --> AXIAL
        VVchk = AAchk / 360 * PASSO
        TTchk = Tan(PG * BBchk / 180) * PASSO / (2 * PG * RRchk)
        TTchk = Atn(TTchk) * 180 / PG
        'COTATION DU PROFIL DANS LE PLAN AXIAL
        Hr(i) = Atn(Cos(TTchk * PG / 180) * PASSO / (2 * PG * RRchk))
        'ERRORI SUL PEZZO --> CORREZIONI NORMALI SULLA MOLA
        If (i <= NpCF1) Then
          errF1(i) = -errF1(i) * Cos(Hr(i))
        Else
          errF2(NpCF2 - (i - NpCF1) + 1) = -errF2(NpCF2 - (i - NpCF1) + 1) * Cos(Hr(i))
        End If
      Wend
    Else
      '*****************************************************************************
      'TRASFORMAZIONE DA ERRORI FRONTALI SUL PROFILO ROTORE A ERRORI SUL PROFILO MOLA
      '*****************************************************************************
      ' PRG | KLN | ACV
      '  X  |  X  |  Z
      '  Y  |  Y  |  X
      '*****************************************************************************
      i = 0
      While Not EOF(1)
        i = i + 1
        ReDim Preserve Hr(i)
        Input #1, RRchk, AAchk, BBchk
        'COTATION DU PROFIL DANS LE PLAN APPARENT
        Hr(i) = Atn(2 * PG * RRchk * Cos(BBchk * PG / 180) / PASSO)
        'ERRORI SUL PEZZO --> CORREZIONI NORMALI SULLA MOLA
        If (i <= NpCF1) Then
          errF1(i) = -errF1(i) * Cos(Hr(i))
        Else
          errF2(NpCF2 - (i - NpCF1) + 1) = -errF2(NpCF2 - (i - NpCF1) + 1) * Cos(Hr(i))
        End If
      Wend
    End If
  Close #1
  '
  'COSTRUISCO IL FILE DEGLI ERRORI
  If (TY_ROT = "AXIAL") Then
    Open RADICE & "\RISROT" For Output As #2
      'COSTRUISCO LA RIGA RISROT CON I VALORI RICAVATI
      If (NpCF2 >= NpCF1) Then
        For i = 1 To NpCF1
          stmp = ""
          If (errF2(i) >= 0) Then
            stmp = stmp & "+" & frmt0(Abs(errF2(i)), 4)
          Else
            stmp = stmp & "-" & frmt0(Abs(errF2(i)), 4)
          End If
          stmp = stmp & ","
          If (errF1(i) >= 0) Then
            stmp = stmp & "+" & frmt0(Abs(errF1(i)), 4)
          Else
            stmp = stmp & "-" & frmt0(Abs(errF1(i)), 4)
          End If
          Print #2, stmp
        Next i
        For i = NpCF1 + 1 To NpCF2
          stmp = ""
          If (errF2(i) >= 0) Then
            stmp = stmp & "+" & frmt0(Abs(errF2(i)), 4)
          Else
            stmp = stmp & "-" & frmt0(Abs(errF2(i)), 4)
          End If
          stmp = stmp & ","
          stmp = stmp & "+" & frmt0(0, 4)
          Print #2, stmp
        Next i
        If (NpCF2 < 40) Then
          For i = 1 To 40 - NpCF2
            stmp = "+0.0000,+0.0000"
            Print #2, stmp
          Next i
        Else
          stmp = "+0.0000,+0.0000"
          Print #2, stmp
        End If
      Else
        For i = 1 To NpCF2
          stmp = ""
          If (errF2(i) >= 0) Then
            stmp = stmp & "+" & frmt0(Abs(errF2(i)), 4)
          Else
            stmp = stmp & "-" & frmt0(Abs(errF2(i)), 4)
          End If
          stmp = stmp & ","
          If (errF1(i) >= 0) Then
            stmp = stmp & "+" & frmt0(Abs(errF1(i)), 4)
          Else
            stmp = stmp & "-" & frmt0(Abs(errF1(i)), 4)
          End If
          Print #2, stmp
        Next i
        For i = NpCF2 + 1 To NpCF1
          stmp = ""
          stmp = stmp & "+" & frmt0(0, 4)
          stmp = stmp & ","
          If (errF1(i) >= 0) Then
            stmp = stmp & "+" & frmt0(Abs(errF1(i)), 4)
          Else
            stmp = stmp & "-" & frmt0(Abs(errF1(i)), 4)
          End If
          Print #2, stmp
        Next i
        If (NpCF1 < 40) Then
          For i = 1 To 40 - NpCF1
            stmp = "+0.0000,+0.0000"
            Print #2, stmp
          Next i
        Else
          stmp = "+0.0000,+0.0000"
          Print #2, stmp
        End If
      End If
    Close #2
  Else
    Open RADICE & "\EPMROT" For Output As #2
      'COSTRUISCO LA RIGA EPMROT CON I VALORI RICAVATI
      For i = 1 To NpCF1
        stmp = ""
        If (errF1(i) >= 0) Then
          stmp = stmp & "+" & frmt0(Abs(errF1(i)), 4)
        Else
          stmp = stmp & "-" & frmt0(Abs(errF1(i)), 4)
        End If
        stmp = stmp & ","
        stmp = stmp & "+0.0000"
        Print #2, stmp
      Next i
      For i = 1 To NpCF2
        stmp = ""
        If (errF2(i) >= 0) Then
          stmp = stmp & "+" & frmt0(Abs(errF2(i)), 4)
        Else
          stmp = stmp & "-" & frmt0(Abs(errF2(i)), 4)
        End If
        stmp = stmp & ","
        stmp = stmp & "+0.0000"
        Print #2, stmp
      Next i
      If (NpCF2 + NpCF1 < 80) Then
        For i = 1 To 80 - NpCF2 - NpCF1
          stmp = "+0.0000,+0.0000"
          Print #2, stmp
        Next i
      Else
        stmp = "+0.0000,+0.0000"
        Print #2, stmp
      End If
    Close #2
  End If
  'SEGNALAZIONE UTENTE
  WRITE_DIALOG " (HMI) <--- (KLINGELNBERG)"
Else
  MsgBox PathControllo & ": " & Error$(53) & Chr(13), 48, "SUB: KLINGELNBERG_ACQUISIZIONE"
End If
  '
Exit Sub

errKLINGELNBERG_ACQUISIZIONE:
  If (FreeFile > 0) Then Close
  MsgBox Error$(Err), 48, "SUB: KLINGELNBERG_ACQUISIZIONE"
  Resume Next
  Exit Sub
  
Wrt_xyz:
  Open RADICE & "\ROTORE_MISURATO.XYZ" For Output As #2
   For i = 1 To INDICE
     Print #2, frmt0((XX(i) + NX(i) * EE(i)), 4); "   ";
     Print #2, frmt0((YY(i) + NY(i) * EE(i)), 4); "   ";
     Print #2, "0"
   Next i
  Close #2
Return

Wrt_dxf:
 Open RADICE & "\ROTORE_MISURATO.DXF" For Output As #3
   'INIZIO FILE
    Print #3, "0"
    Print #3, "SECTION"
    Print #3, "2"
    Print #3, "ENTITIES"
    For i = 1 To INDICE - 1
        Print #3, "0"
        Print #3, "LINE"
        Print #3, "8"
        Print #3, "1"
        Print #3, "62"
        Print #3, "1"
        'PRIMO PUNTO
        Print #3, "10"
        Print #3, frmt0((XX(i) + NX(i) * EE(i)), 4)
        Print #3, "20"
        Print #3, frmt0((YY(i) + NY(i) * EE(i)), 4)
        Print #3, "30"
        Print #3, "0.0"
        'SECONDO PUNTO
        Print #3, "11"
        Print #3, frmt0((XX(i + 1) + NX(i + 1) * EE(i + 1)), 4)
        Print #3, "21"
        Print #3, frmt0((YY(i + 1) + NY(i + 1) * EE(i + 1)), 4)
        Print #3, "31"
        Print #3, "0.0"
    Next
    'FINE FILE
    Print #3, "0"
    Print #3, "ENDSEC"
    Print #3, "0"
    Print #3, "EOF"
  Close #3
Return

End Sub

Sub SCRIVI_DATI_CONTROLLO(ByVal RADICE As String, ByVal sovrametallo As Single, PrimoPiazzamento As Boolean, Rett_Viti As Integer, NoCHK() As Integer)
'
Dim i         As Integer
Dim j         As Integer
Dim k         As Integer
Dim kRH       As Integer
Dim kLH       As Integer
Dim NpRot     As Integer
Dim NPF1      As Integer
Dim ipriF1    As Integer
Dim ipriF2    As Integer
Dim iRmin     As Integer
Dim Rmin      As Double
Dim Bmin      As Double
Dim Amin      As Double
Dim iAmin     As Integer
Dim Rmax      As Double
Dim Rmed      As Double
Dim DeltaMin  As Double
Dim DeltaR    As Double
Dim iRF1      As Integer
Dim iRF2      As Integer
'
Dim riga      As String
Dim stmp      As String
Dim sRH       As String
Dim sLH       As String
'
Dim DAT$(13)             ': DATROT (dati generali controllo)
Dim ROT$(12)             ': ROTOR.DAT (dati generali rotore)
Dim RA$(), al$(), BE$()  ': RAB rotore
Dim CHK$()               ': CONTROLLO PUNTO (1=SI, 0=NO)
Dim TAS_MACC(8) As String
Dim TAS26 As String
'
Dim vect$()
Dim kk As Integer
'
On Error GoTo errSCRIVI_DATI_CONTROLLO
  '
  Open RADICE & "\PRFROT" For Input As #2
    i = 1: Rmax = -99999: Rmin = 9999999
    While Not EOF(2)
      ReDim Preserve RA$(i): ReDim Preserve al$(i): ReDim Preserve BE$(i)
      ReDim Preserve CHK$(i)
      Line Input #2, riga
      stmp = UCase$(Trim$(riga))
      vect$ = Split(stmp, " ")
      kk = 0
      For k = 0 To UBound(vect$)
        If (vect$(k) <> "") Then
          kk = kk + 1
          If (kk = 1) Then RA$(i) = vect$(k)
          If (kk = 2) Then al$(i) = vect$(k)
          If (kk = 3) Then BE$(i) = vect$(k)
        End If
      Next k
      If (val(RA$(i)) < Rmin) Then Rmin = val(RA$(i)): iRmin = i
      If (val(RA$(i)) > Rmax) Then Rmax = val(RA$(i))
      CHK$(i) = "1"
      i = i + 1
    Wend
  Close #2
  NpRot = i - 1
  '
  'VETTORE DEI PUNTI NON CONTROLLATI
  For k = 1 To UBound(NoCHK)
    CHK$(NoCHK(k)) = "0"
  Next k
  'SCRIVI FILE CHKRAB.ROT
  Open RADICE & "\CHKRAB.ROT" For Output As #1
    Print #1, "[PuntiControllo]"
    For i = 1 To NpRot
      Print #1, "CHK[0," & CStr(i) & "]=" & CHK$(i)
    Next i
  Close #1
  '
  'N. punti F1 (ROTORE)
  NPF1 = GetInfo("CREAZIONE", "NpM_F1", RADICE & "\INFO.INI")
  'calcolo diametro di riferimento
  Bmin = 99999999
  For i = 1 To NPF1
    If (Abs(val(BE$(i))) < Bmin) Then
      Bmin = Abs(val(BE$(i)))
      ipriF1 = i
    End If
  Next i
  Bmin = 99999999
  For i = NPF1 + 1 To NpRot
    If (Abs(val(BE$(i))) < Bmin) Then
      Bmin = Abs(val(BE$(i)))
      ipriF2 = i
    End If
  Next i
  'Punto per controllo Diametro Interno  per Viti per Pompe
  Amin = 99999999
  For i = 1 To NpRot
    If (Abs(val(al$(i))) < Amin) Then
      Amin = Abs(val(al$(i)))
      iAmin = i
    End If
  Next i
  '*****  calcolo punti centratura Viti  *****
  Rmed = (Rmin + Rmax) / 2
  'Fianco 1
  DeltaMin = 99999999
  For i = 1 To NPF1
    DeltaR = Abs(val(RA$(i)) - Rmed)
    If (DeltaR < DeltaMin) Then
      DeltaMin = DeltaR
      iRF1 = i
    End If
  Next i
  'Fianco 2
  DeltaMin = 99999999
  For i = NPF1 + 1 To NpRot
    DeltaR = Abs(val(RA$(i)) - Rmed)
    If (DeltaR < DeltaMin) Then
      DeltaMin = DeltaR
      iRF2 = i
    End If
  Next i
  '***** FIL 05-03-07
  TAS26 = "+0"
  'Macchina collegata
  stmp = ""
  FileCopy RADICE & "\OEM1_PROFILO71.SPF", RADICE & "\OEM1_PROFILO71.TMP"
  Open RADICE & "\OEM1_PROFILO71.TMP" For Input As #1
  i = 20
  stmp = ""
  While Not EOF(1) And (i < 28)
    j = InStr(1, stmp, "TAS[0," & i & "]")
    If (j > 0) Then
      j = InStr(1, stmp, ";")
      If (j > 0) Then
        j = j - 1
        stmp = Left(stmp, j)
      End If
      stmp = Trim(Right(stmp, Len(stmp) - InStr(1, stmp, "=")))
      TAS_MACC(i - 19) = stmp
      i = i + 1
    End If
    Line Input #1, stmp
  Wend
  Close #1
  For i = 1 To 6
    DAT$(i + 7) = Left(DAT$(i + 7), 15) & TAS_MACC(i)
  Next i
  ipriF1 = val(TAS_MACC(1))
  ipriF2 = val(TAS_MACC(2))
  iRmin = val(TAS_MACC(3))
  TAS26 = TAS_MACC(7)
  'SCRIVI FILE CHKTAS.ROT
  Open RADICE & "\CHKTAS.ROT" For Output As #1
    j = 15
    Print #1, "[ParFasatura&Controllo]"
    If (Rett_Viti = 1) Or (Rett_Viti = 2) Then
       'Per Viti-Pompe
       i = 8:    stmp = CStr(iRF1)
       Print #1, "TAS[0,20]=" & stmp
       i = 9:    stmp = CStr(iRF2)
       Print #1, "TAS[0,21]=" & stmp
       i = 10:    stmp = CStr(iAmin)
       Print #1, "TAS[0,22]=" & stmp
    Else
       'Per Rotori
       i = 8:    stmp = CStr(ipriF1)
       Print #1, "TAS[0,20]=" & stmp
       i = 9:    stmp = CStr(ipriF2)
       Print #1, "TAS[0,21]=" & stmp
       i = 10:    stmp = CStr(iRmin)
       Print #1, "TAS[0,22]=" & stmp
    End If
    For i = 11 To 13
      stmp = DAT$(i)
      Print #1, "TAS[0," & CStr(12 + i) & "]=" & Trim$(Right$(stmp, Len(stmp) - j))
    Next i
    '***** FIL 02-03-07
    Print #1, "TAS[0,26]=" & TAS26
    stmp = frmt0(sovrametallo, 4)
    Print #1, "TAS[0,27]=" & stmp 'sovrametallo su diam. interno
  Close #1

Exit Sub

errSCRIVI_DATI_CONTROLLO:
  If FreeFile > 0 Then Close
  MsgBox "ERROR: " & Error(Err), 48, "SUB: SCRIVI_DATI_CONTROLLO"
  Exit Sub

End Sub

Sub CREAZIONE_DATI_CONTROLLO(ByVal RADICE As String, NoCHK() As Integer)
'
Dim Rett_Viti As Integer
Dim i         As Integer
Dim j         As Integer
Dim k         As Integer
Dim NpRot     As Integer
Dim NPF1      As Integer
Dim ipriF1    As Integer
Dim ipriF2    As Integer
Dim iRmin     As Integer
Dim iRmax     As Integer
Dim Rmin      As Double
Dim Bmin      As Double
Dim Amin      As Double
Dim iAmin     As Integer
Dim Rmax      As Double
Dim Rmed      As Double
Dim DeltaMin  As Double
Dim DeltaR    As Double
Dim iRF1      As Integer
Dim iRF2      As Integer
'
Dim riga      As String
Dim stmp      As String
Dim RA$()
Dim al$()
Dim BE$()
Dim CHK$()
Dim vect$()
Dim kk        As Integer
'
On Error GoTo errCREAZIONE_DATI_CONTROLLO
  '
  'LETTURA DEL TIPO DI RETTIFICA
  Rett_Viti = Lp("TIPO_ROTORE")
  'LETTURA DEL PROFILO RAB
  Open RADICE & "\PRFROT" For Input As #2
    i = 1: Rmax = -99999: Rmin = 9999999
    While Not EOF(2)
      ReDim Preserve RA$(i): ReDim Preserve al$(i): ReDim Preserve BE$(i)
      ReDim Preserve CHK$(i)
      Line Input #2, riga
      stmp = UCase$(Trim$(riga))
      vect$ = Split(stmp, " ")
      kk = 0
      For k = 0 To UBound(vect$)
        If (vect$(k) <> "") Then
          kk = kk + 1
          If (kk = 1) Then RA$(i) = vect$(k)
          If (kk = 2) Then al$(i) = vect$(k)
          If (kk = 3) Then BE$(i) = vect$(k)
        End If
      Next k
      If (val(RA$(i)) < Rmin) Then Rmin = val(RA$(i)): iRmin = i
      If (val(RA$(i)) > Rmax) Then Rmax = val(RA$(i)): iRmax = i
      CHK$(i) = "1"
      i = i + 1
    Wend
  Close #2
  NpRot = i - 1
  '
  'VETTORE DEI PUNTI NON CONTROLLATI
  For k = 1 To UBound(NoCHK)
    CHK$(NoCHK(k)) = "0"
  Next k
  'SCRIVI FILE CHKRAB.ROT
  Open RADICE & "\CHKRAB.ROT" For Output As #1
    Print #1, "[PuntiControllo]"
    For i = 1 To NpRot
      Print #1, "CHK[0," & CStr(i) & "]=" & CHK$(i)
    Next i
  Close #1
  '
  'N. punti F1 (ROTORE)
  NPF1 = GetInfo("CREAZIONE", "NpM_F1", RADICE & "\INFO.INI")
  'calcolo diametro di riferimento
  Bmin = 99999999
  For i = 1 To NPF1
    If (Abs(val(BE$(i))) < Bmin) Then
      Bmin = Abs(val(BE$(i)))
      ipriF1 = i
    End If
  Next i
  Bmin = 99999999
  For i = NPF1 + 1 To NpRot
    If (Abs(val(BE$(i))) < Bmin) Then
      Bmin = Abs(val(BE$(i)))
      ipriF2 = i
    End If
  Next i
  'Punto per controllo Diametro Interno  per Viti per Pompe
  Amin = 99999999
  For i = 1 To NpRot
    If (Abs(val(al$(i))) < Amin) Then
      Amin = Abs(val(al$(i)))
      iAmin = i
    End If
  Next i
  'calcolo punti centratura Viti
  Rmed = (Rmin + Rmax) / 2
  'Fianco 1
  DeltaMin = 99999999
  For i = 1 To NPF1
    DeltaR = Abs(val(RA$(i)) - Rmed)
    If (DeltaR < DeltaMin) Then
      DeltaMin = DeltaR
      iRF1 = i
    End If
  Next i
  'Fianco 2
  DeltaMin = 99999999
  For i = NPF1 + 1 To NpRot
    DeltaR = Abs(val(RA$(i)) - Rmed)
    If (DeltaR < DeltaMin) Then
      DeltaMin = DeltaR
      iRF2 = i
    End If
  Next i
  'SCRITTURA FILE CHKTAS.ROT
  Open RADICE & "\CHKTAS.ROT" For Output As #1
    Print #1, "[ParFasatura&Controllo]"
    If (Rett_Viti = 1) Or (Rett_Viti = 2) Then
      'Per Viti-Pompe
      Print #1, "TAS[0,20]=" & CStr(iRF1)
      Print #1, "TAS[0,21]=" & CStr(iRF2)
      Print #1, "TAS[0,22]=" & CStr(iAmin)
      Call SP("TAS[0,20]", CStr(iRF1), False)
      Call SP("TAS[0,21]", CStr(iRF2), False)
      Call SP("TAS[0,22]", CStr(iAmin), False)
    Else
      'Per Rotori
      Print #1, "TAS[0,20]=" & CStr(ipriF1)
      Print #1, "TAS[0,21]=" & CStr(ipriF2)
      Print #1, "TAS[0,22]=" & CStr(iRmin)
      Call SP("TAS[0,20]", CStr(ipriF1), False)
      Call SP("TAS[0,21]", CStr(ipriF2), False)
      Call SP("TAS[0,22]", CStr(iRmin), False)
    End If
    Print #1, "TAS[0,23]=0"
    Print #1, "TAS[0,24]=0"
    Call SP("TAS[0,25]", CStr(iRmax), False)
    Print #1, "TAS[0,25]=" & CStr(iRmax)
    Print #1, "TAS[0,26]=0"
    Print #1, "TAS[0,27]=0"
  Close #1
  '
Exit Sub

errCREAZIONE_DATI_CONTROLLO:
  If FreeFile > 0 Then Close
  MsgBox "ERROR: " & Error(Err), 48, "SUB: CREAZIONE_DATI_CONTROLLO"
  Exit Sub

End Sub

Public Sub CREAZIONE_DATI_RETTIFICA_ROTORI(RADICE As String, NomeROTORE As String, DM_ROT As String, Z As Integer, PASSO As Single, ELICA As Single, DiaINT As Single, SPALLA As String, sequenza As String, npt As Integer, TIPO_ROTORE As Integer)
'
Dim ret               As Integer
Dim CorrezioneAltezza As Double
Dim CorInterasse      As Double
Dim stmp              As String
'
Dim k     As Integer
Dim Km    As Integer
Dim Kp    As Integer
Dim Alfa1 As Double
Dim Alfa2 As Double
Dim E249  As Double
'
Dim Dm$
'
Dim NoCHKp  As Integer
Dim NoCHK() As Integer
Dim R       As Double
Dim A       As Double
Dim B       As Double
Dim Rmin    As Double
Dim Rmax    As Double
Dim Rmed    As Double
Dim j       As Integer
Dim jj      As Integer
Dim RminIDX As Integer
Dim YmaxIDX As Integer
Dim Ymax    As Double
Dim X       As Double
Dim Y       As Double
Dim T       As Double
Dim XH      As Double
Dim YH      As Double
Dim TH      As Double
Dim F1CHK   As Integer
Dim F2CHK   As Integer
Dim riga    As String
Dim i       As Integer
'
Dim A1  As Double
Dim A2  As Double
'
Dim Idx() As Integer
'
Dim BGND As Single
Dim ENDD As Single
Dim MIDD As Single
'
Dim NPMF1tmp As Integer
'
Dim kkk      As Integer
Dim DMM      As String
Dim SpallaX  As Single
Dim SpallaY  As Single
'
On Error GoTo errCREAZIONI_DATI_RETTIFICA_ROTORI
  '
  If (Dir(RADICE & "\" & "ABILITA.COR") <> "") Then Kill RADICE & "\" & "ABILITA.COR"
  '
  'Intervallo di valori per il calcolo dei diametri mola
  MIDD = val(DM_ROT)
  BGND = MIDD - 50
  ENDD = MIDD + 50
  '
  'CORREZIONE DI ALTEZZA MOLA
  ret = InStr(SPALLA, "H")
  If (ret > 0) Then
    SpallaX = -Abs(val(Left$(SPALLA, ret - 1)))
    SpallaY = Abs(val(Right$(SPALLA, Len(SPALLA) - ret)))
    CorrezioneAltezza = 0
  Else
    ret = InStr(SPALLA, "T")
    If (ret > 0) Then
      SpallaX = -Abs(val(Left$(SPALLA, ret - 1)))
      SpallaY = Abs(SpallaX) * Tan(Abs(val(Right$(SPALLA, Len(SPALLA) - ret))) * PG / 180)
      CorrezioneAltezza = 0
    Else
      SpallaX = 0
      SpallaY = 0
      CorrezioneAltezza = val(Trim$(SPALLA))
    End If
  End If
  '
  Call CALCOLO_PROFILO_MOLA_RIDOTTO(RADICE, "PRFROT", MIDD, MIDD, CorrezioneAltezza, PASSO, ELICA, DiaINT)
  Call CALCOLO_PROFILO_MOLA_RIDOTTO(RADICE, "PRFROT", ENDD, ENDD, CorrezioneAltezza, PASSO, ELICA, DiaINT)
  '
  'Vettore flag per eliminare i punti da non controllare del profilo.
  'Se IDX(I)=1 allora I e' un punto controllato.
  'Se IDX(I)=0 allora I e' un punto non controllato.
  'ESEMPIO SEQUENZA: 1.3.4.5. OPPURE 0
  ReDim Idx(npt)
  For i = 1 To npt
    Idx(i) = 1
  Next i
  i = 1: j = 0: NoCHKp = 0
  ReDim NoCHK(NoCHKp)
  j = InStr(i, sequenza, ".")
  Do While (j > 0)
    NoCHKp = NoCHKp + 1
    ReDim Preserve NoCHK(NoCHKp)
    NoCHK(NoCHKp) = Abs(val(Mid(sequenza, i, j - i)))
    Idx(NoCHK(NoCHKp)) = 0
    i = j + 1
    j = InStr(i, sequenza, ".")
  Loop
  '
  Open RADICE & "\PRFROT" For Input As #1
    Input #1, R, A, B
    Rmin = R: Rmax = 0: RminIDX = 1
    For i = 2 To npt
      Input #1, R, A, B
      If (Idx(i) = 1) And (R < Rmin) Then
        Rmin = R: RminIDX = i
      End If
      If (R > Rmax) Then Rmax = R
    Next i
  Close #1
  '
  'Creazione di TGMROT.ALL
  Open RADICE & "\XYT." & Format$(MIDD, "##0") For Input As #1
  Open RADICE & "\TGMROT.ALL" For Output As #3
    Input #1, X, Y, T
    Print #3, frmt(Abs(T), 4)
    Print #3, frmt(Abs(T), 4)
    While Not EOF(1)
      Input #1, X, Y, T
      Print #3, frmt(Abs(T), 4)
    Wend
    Print #3, frmt(Abs(T), 4)
  Close #3
  Close #1
  '
  'FISSO IL PUNTO DI SEPARAZIONE SULLA MOLA
  NPMF1tmp = val(GetInfo("CONVERSIONE", "NPMF1tmp", RADICE & "\INFO.INI"))
  '
  ' Ricerca di YMAX e YMAXIDX
  i = 1
  Ymax = -999: YmaxIDX = i
  Open RADICE & "\" & "XYT." & Format$(MIDD, "##0") For Input As #1
    While Not EOF(1)
      Input #1, X, Y, T
      If (NPMF1tmp <> 0) Then
        If (i = NPMF1tmp) Then Ymax = Y: YmaxIDX = i
      Else
        If (Y > Ymax) Then Ymax = Y: YmaxIDX = i
      End If
      If (X < 0.1) And (X > -0.1) Then CorInterasse = Y - Ymax
      i = i + 1
    Wend
  Close #1
  '
  'TIPO_ROTORE
  'Se il profilo non � simmetrico annulla CorrInterasse
  If (TIPO_ROTORE = 0) Then CorInterasse = 0
  '
  Open RADICE & "\" & "XYT." & Format$(MIDD, "##0") For Input As #1
    i = 1
    Input #1, X, Y, T
    Do
      If (i = RminIDX) Then XH = X: YH = Y: TH = T
      Input #1, X, Y, T
      i = i + 1
    Loop While Not EOF(1)
  Close #1
  '
  If (NPMF1tmp <> 0) Then
    '
  Else
    '
    YmaxIDX = YmaxIDX + 1
    If (YmaxIDX Mod 2) > 0 Then
      If (YmaxIDX > npt / 2) Then
        YmaxIDX = YmaxIDX - 1
      Else
        YmaxIDX = YmaxIDX + 1
      End If
    End If
    '
  End If
  '
  'Numero dei punti controllati su F1 e F2
  F1CHK = 0: F2CHK = 0
  For i = 1 To YmaxIDX - 1: F1CHK = F1CHK + Idx(i): Next i
  For i = YmaxIDX To npt: F2CHK = F2CHK + Idx(i): Next i
  '
  'Creazione di XYT.ALL
  Open RADICE & "\XYT." & Format$(MIDD, "##0") For Input As #1
    Open RADICE & "\XYT.ALL" For Output As #2
      For i = 1 To npt
        Input #1, X, Y, T
        If (Idx(i) = 1) Then
          riga = ""
          If (X > 0) Then riga = riga & "+" Else riga = riga & "-"
          riga = riga & frmt0(Abs(X), 4) & " "
          If (Y > 0) Then riga = riga & "+" Else riga = riga & "-"
          riga = riga & frmt0(Abs(Y), 4) & " "
          If (T > 0) Then riga = riga & "+" Else riga = riga & "-"
          riga = riga & frmt0(Abs(T), 4)
          Print #2, riga
        End If
      Next i
      riga = ""
      If (XH > 0) Then riga = riga & "+" Else riga = riga & "-"
      riga = riga & frmt0(Abs(XH), 4) & " "
      If (YH > 0) Then riga = riga & "+" Else riga = riga & "-"
      riga = riga & frmt0(Abs(YH), 4) & " "
      If (TH > 0) Then riga = riga & "+" Else riga = riga & "-"
      riga = riga & frmt0(Abs(TH), 3)
      Print #2, riga
    Close #2
  Close #1
  '
  i = WritePrivateProfileString("CREAZIONE", "PRINCIPI", Format$(Z, "#####0"), RADICE & "\INFO.INI")
  i = WritePrivateProfileString("CREAZIONE", "PASSO", frmt(PASSO, 4), RADICE & "\INFO.INI")
  i = WritePrivateProfileString("CREAZIONE", "ELICA", frmt(ELICA, 4), RADICE & "\INFO.INI")
  i = WritePrivateProfileString("CREAZIONE", "Rmax", frmt(Rmax, 4), RADICE & "\INFO.INI")
  i = WritePrivateProfileString("CREAZIONE", "Rmin", frmt(Rmin, 4), RADICE & "\INFO.INI")
  '
  i = WritePrivateProfileString("CREAZIONE", "NpM_F1", Format$(YmaxIDX, "#####0"), RADICE & "\INFO.INI")
  i = WritePrivateProfileString("CREAZIONE", "NpM_F2", Format$(npt + 2 - YmaxIDX, "#####0"), RADICE & "\INFO.INI")
  i = WritePrivateProfileString("CREAZIONE", "NpC_F1", Format$(F1CHK, "#####0"), RADICE & "\INFO.INI")
  i = WritePrivateProfileString("CREAZIONE", "NpC_F2", Format$(F2CHK, "#####0"), RADICE & "\INFO.INI")
  i = WritePrivateProfileString("CREAZIONE", "Npt", Format$(npt, "#####0"), RADICE & "\INFO.INI")
  i = WritePrivateProfileString("CREAZIONE", "CorrInterasse", frmt(CorInterasse, 3), RADICE & "\INFO.INI")
  '
  'CREAZIONE DEL FILE DI RIFERIMENTO ROTOR.DAT
  Open RADICE & "\ROTOR.DAT" For Output As #1
    Print #1, NomeROTORE
    Print #1, "PRFROT"
    Print #1, frmt(PASSO, 4)
    Print #1, frmt(ELICA, 4)
    Print #1, frmt(Rmin * 2, 4)
    Print #1, "0"
    Print #1, Format$(YmaxIDX, "#####0")
    Print #1, Format$(npt + 2 - YmaxIDX, "#####0")
    Print #1, Format$(F1CHK, "#####0")
    Print #1, Format$(F2CHK, "#####0")
    Print #1, Format$(npt, "#####0")
    If (NoCHKp > 0) Then
      For i = 1 To NoCHKp
        Print #1, Format$(NoCHK(i), "#####0")
      Next i
    End If
  Close #1
  '
  'CREAZIONE DATI DI CONTROLLO
  Call CREAZIONE_DATI_CONTROLLO(RADICE, NoCHK)
  '
  'Creazione di CORROT
  Open RADICE & "\CORROT.BAK" For Output As #1
  Open RADICE & "\CORROT" For Output As #2
      'PRIMA RIGA AGGIUNTIVA
      riga = ""
      If (SpallaX >= 0) Then riga = riga & "+" & frmt0(SpallaX, 4) Else riga = riga & frmt0(SpallaX, 4)
      If (SpallaY >= 0) Then riga = riga & "+" & frmt0(SpallaY, 4) Else riga = riga & frmt0(SpallaY, 4)
      Print #1, riga
      Print #2, riga
    For i = 1 + 1 To npt + 2 - 1
      Print #1, "+0.0000+0.0000"
      Print #2, "+0.0000+0.0000"
    Next i
      'ULTIMA RIGA AGGIUNTIVA
      riga = ""
      If (SpallaX >= 0) Then riga = riga & "+" & frmt0(SpallaX, 4) Else riga = riga & frmt0(SpallaX, 4)
      If (SpallaY >= 0) Then riga = riga & "+" & frmt0(SpallaY, 4) Else riga = riga & frmt0(SpallaY, 4)
      Print #1, riga
      Print #2, riga
  Close #1
  Close #2
  '
  'Creazione di PUNCHK
  Open RADICE & "\PUNCHK" For Output As #1
    For i = 2 To npt + 1
      If (Idx(i - 1) = 1) Then Print #1, i
    Next i
  Close #1
  '
  'Creazione di RVTROT.INT e  RABROT.INT
  DM_ROT = Format$(MIDD, "##0")
  '
  'CALCOLO DEI PROFILI DI RIFERIMENTO PER I CONTROLLI
  Call CALCOLO_PROFILO_FRONTALE(0, 0, DM_ROT)
  '
  'Creazione di PRFTOL.DAT
Dim TIPO_MACCHINA As String
  '
  TIPO_MACCHINA = GetInfo("Configurazione", "TIPO_MACCHINA", Path_LAVORAZIONE_INI)
  TIPO_MACCHINA = UCase$(Trim$(TIPO_MACCHINA))
  If (TIPO_MACCHINA = "G500HL-SEIM") Then
    '
    Dim TollA As Single
    Dim TollB As Single
    Dim TollC As Single
    '
    If (TIPO_ROTORE = 2) Then
      'VITE LATERALE
          If (2 * Rmin <= 5) Then
            TollA = 0.005:  TollB = 0.0065: TollC = 0.0075
      ElseIf (2 * Rmin <= 9) Then
            TollA = 0.0055: TollB = 0.007:  TollC = 0.0085
      ElseIf (2 * Rmin <= 16.6) Then
            TollA = 0.007:  TollB = 0.009:  TollC = 0.0105
      Else
            TollA = 0.01:   TollB = 0.02:   TollC = 0.03
      End If
    Else
      'VITE CENTRALE
          If (2 * Rmin <= 15) Then
            TollA = 0.0055: TollB = 0.007:  TollC = 0.0085
      ElseIf (2 * Rmin <= 27) Then
            TollA = 0.007:  TollB = 0.009:  TollC = 0.0105
      ElseIf (2 * Rmin <= 43.2) Then
            TollA = 0.008:  TollB = 0.0105: TollC = 0.0125
      ElseIf (2 * Rmin <= 49.8) Then
            TollA = 0.0095: TollB = 0.012:  TollC = 0.0145
      Else
            TollA = 0.01:   TollB = 0.02:   TollC = 0.03
      End If
    End If
    'SCRITTURA DELLE TOLLERANZE
    Call SP("TollA", TollA, False)
    Call SP("TollB", TollB, False)
    Call SP("TollC", TollC, False)
    '
    Open RADICE & "\PRFTOL.DAT" For Output As #1
      For i = 1 To npt
        Print #1, "+" & Left$(frmt(TollA, 5) & String$(6, " "), 6) & " +" & Left$(frmt(TollA, 5) & String$(6, " "), 6)
      Next i
    Close #1
    '
  Else
    Open RADICE & "\PRFTOL.DAT" For Output As #1
      For i = 1 To npt
        Print #1, "+0.0100 +0.0100"
        'Print #1, "+0.0090 +0.0001"   'SOLO PER LEISTRITZ
      Next i
    Close #1
  End If
  '
  'Creazione di ABILITA.COR
  Open RADICE & "\ABILITA.COR" For Output As #1
    For i = 0 To npt + 1
      Print #1, "1"
    Next i
  Close #1
  '
  'Call Kill_RISROT_EPMROT
  '
Exit Sub

errCREAZIONI_DATI_RETTIFICA_ROTORI:
  MsgBox Error(Err), 48, "SUB: CREAZIONI_DATI_RETTIFICA_ROTORI"
  'Resume Next
  Exit Sub
'Resume Next
End Sub

Sub RIDUZIONE_PUNTI_RAB(RADICE As String, COORDINATE As String, ByVal DM_ROT As String, ByVal MODO As Integer, ByRef Npunti As Integer)
'
Dim Risoluzione      As Single  'Risoluzione = 0.9 errore massimo ammesso in micron
Dim MaxDisPunti      As Single  'MaxDisPunti = 4   massima distanza dei punti
Dim PuntoSeparazione As Integer 'PRENDO SEMPRE IL PUNTO CON YMAX SULLA MOLA
Dim LISTA_ELIMINATI  As String
Dim TROVATO          As String
'
Dim A1 As Double
Dim A2 As Double
'
Dim PS   As Integer
Dim stmp As String
'
Dim XX As Double
Dim YY As Double
Dim TT As Double
Dim R0 As Double
Dim Xc As Double
Dim YC As Double
'
Dim N()  As Integer
Dim X()  As Double
Dim Y()  As Double
Dim TG() As Double
Dim XR() As Double
Dim YR() As Double
Dim RR() As Double
Dim R2() As Double
Dim A()  As Double
Dim B()  As Double
'
Dim i       As Integer
Dim j       As Integer
Dim k       As Integer
Dim LISTA() As Integer
Dim NmaxI   As Integer
Dim PFin    As Integer
Dim PDebut  As Integer
'
Dim Ntot    As Integer
Dim ErMax   As Double
Dim NRid    As Integer
'
Dim YYmax   As Double
Dim iYYmax  As Long
'
Dim lRet    As Long
'
Dim DDY     As Double
Dim DDX     As Double
'
Dim ERRORE_MASSIMO As Double
Dim DDY_MASSIMO     As Double
Dim DDX_MASSIMO     As Double
'
Dim MaxPuntiFIANCO As Long
'
On Error GoTo errRIDUZIONE_PUNTI_RAB
  '
  MaxPuntiFIANCO = 47
  '
  Risoluzione = Lp("RISOLUZIONE")
  MaxDisPunti = Lp("MASSIMA_DISTANZA")
  LISTA_ELIMINATI = Lp_Str("LISTA_ELIMINATI")
  If (LISTA_ELIMINATI = "NOTFOUND") Then LISTA_ELIMINATI = ""
  '
  'QUANDO LA RISOLUZIONE E' 0.001 NON RIDUCO IN PUNTI E LI PRENDO TUTTI
  '
  'MODO = 0  'AUTOMATICO PER CONSENTIRE A LEISTRITZ DI CANCELLARE ALTRI DUE PUNTI
  'MODO = 1  'AUTOMATICO
  'MODO = 2  'AUTOMATICO CON VISUALIZZAZIONE SCARTI
  'MODO = 3  'MANUALE CON VISUALIZZAZIONE SCARTI
  '
  'LETTURA PUNTI MOLA
  Open RADICE & "\XYT." & DM_ROT For Input As #2
    i = 0: YYmax = 0: iYYmax = 0
    While Not EOF(2)
      Input #2, XX, YY, TT
      i = i + 1
      ReDim Preserve X(i): ReDim Preserve Y(i): ReDim Preserve TG(i)
      X(i) = XX: Y(i) = YY: TG(i) = TT
      'RICERCA DEL PUNTO MASSIMO DELLA MOLA --> SEPARAZIONE TRA I FIANCHI
      If (YY > YYmax) Then YYmax = YY: iYYmax = i
    Wend
    NmaxI = i
  Close #2
  Ntot = NmaxI
  'CONTROLLO SE DISPARI
  If (iYYmax Mod 2) = 0 Then
  PuntoSeparazione = iYYmax - 1
  Else
  PuntoSeparazione = iYYmax
  End If
  '
  If (Risoluzione > 0.001) Then
    '
    'LETTURA DEL FILE RAB DI PARTENZA PER VISUALIZZARE I PUNTI
    ReDim R2(Ntot + 1): ReDim A(Ntot + 1): ReDim B(Ntot + 1)
    Open RADICE & "\PRFROT" For Input As #1
      For i = 1 To Ntot
        Input #1, R2(i), A(i), B(i)
      Next i
    Close #1
    '
    Dim NRidTot       As Integer
    Dim DDX_MASSIMO_1 As Double
    Dim DDY_MASSIMO_1 As Double
    Dim DDX_MASSIMO_2 As Double
    Dim DDY_MASSIMO_2 As Double
    Dim NRidF1        As Integer
    Dim NRidF2        As Integer
    Dim Gerr()        As Double
    '
    ReDim Preserve X(Ntot): ReDim Preserve Y(Ntot): ReDim Preserve TG(Ntot):
    ReDim N(Ntot):          ReDim XR(Ntot):         ReDim YR(Ntot):           ReDim RR(Ntot)
    '
    Call RIDUZIONE_TOT(PuntoSeparazione, 0, 0, X(), Y(), TG(), MaxDisPunti, R2(), A(), B(), MODO, Risoluzione, Gerr(), XR(), YR(), RR(), N(), ERRORE_MASSIMO, DDX_MASSIMO, DDY_MASSIMO, DDX_MASSIMO_1, DDY_MASSIMO_1, DDX_MASSIMO_2, DDY_MASSIMO_2, NRidTot, NRidF1, NRidF2)
    '
    NRid = UBound(N)
    '
    'SCRITTURA R, A, B RIDOTTI
    Open RADICE & "\PRFROT" For Output As #1
      For i = 1 To NRid
        'RICAVO IL PUNTO DI SEPARAZIONE PER IL PROFILO RIDOTTO
        'If (N(i) <= PuntoSeparazione) Then NPM1 = i ' DA VERIFICARE SVG110301
        'SCRIVO LA RIGA NEL FILE RAB
        stmp = ""
        If R2(N(i)) >= 0 Then stmp = stmp & "+" Else stmp = stmp & "-"
        stmp = stmp & Left$(frmt0(Abs(R2(N(i))), 4) & String(8, " "), 8) & " "
        If A(N(i)) >= 0 Then stmp = stmp & "+" Else stmp = stmp & "-"
        stmp = stmp & Left$(frmt0(Abs(A(N(i))), 4) & String(8, " "), 8) & " "
        If B(N(i)) >= 0 Then stmp = stmp & "+" Else stmp = stmp & "-"
        stmp = stmp & Left$(frmt0(Abs(B(N(i))), 4) & String(8, " "), 8)
        Print #1, stmp
      Next i
    Close #1
    '
    Npunti = NRid
    '
  Else
    '
    If (Len(LISTA_ELIMINATI) > 0) And (LISTA_ELIMINATI <> "0") Then
      'ESEMPIO SEQUENZA: 1.3.4.5. OPPURE 0
      i = 1: j = InStr(i, LISTA_ELIMINATI, "."): k = 0
      Do While (j > 0)
        k = k + 1
        ReDim Preserve LISTA(k)
        LISTA(k) = Abs(val(Mid(LISTA_ELIMINATI, i, j - i)))
        i = j + 1
        j = InStr(i, LISTA_ELIMINATI, ".")
      Loop
      NRid = Ntot - k
      'LETTURA DEL FILE RAB DI PARTENZA PER VISUALIZZARE I PUNTI
      ReDim R2(Ntot + 1): ReDim A(Ntot + 1): ReDim B(Ntot + 1)
      Open RADICE & "\PRFROT" For Input As #1
        For i = 1 To Ntot
          Input #1, R2(i), A(i), B(i)
        Next i
      Close #1
      'SCRITTURA R, A, B RIDOTTI
      Open RADICE & "\PRFROT" For Output As #1
        For i = 1 To Ntot
          TROVATO = "N"
          For j = 1 To UBound(LISTA)
            If LISTA(j) = i Then TROVATO = "Y"
          Next j
          If (TROVATO = "N") Then
            'RICAVO IL PUNTO DI SEPARAZIONE PER IL PROFILO RIDOTTO
            'SCRIVO LA RIGA NEL FILE RAB
            stmp = ""
            If R2(i) >= 0 Then stmp = stmp & "+" Else stmp = stmp & "-"
            stmp = stmp & Left$(frmt0(Abs(R2(i)), 4) & String(8, " "), 8) & " "
            If A(i) >= 0 Then stmp = stmp & "+" Else stmp = stmp & "-"
            stmp = stmp & Left$(frmt0(Abs(A(i)), 4) & String(8, " "), 8) & " "
            If B(i) >= 0 Then stmp = stmp & "+" Else stmp = stmp & "-"
            stmp = stmp & Left$(frmt0(Abs(B(i)), 4) & String(8, " "), 8)
            Print #1, stmp
          End If
        Next i
      Close #1
      '
      Npunti = NRid
      '
    Else
      '
      Npunti = Ntot
      '
    End If
    '
    lRet = WritePrivateProfileString("CREAZIONE", "PuntoSeparazioneRID", Npunti, RADICE & "\INFO.INI")
    '
  End If
  '
Exit Sub

errRIDUZIONE_PUNTI_RAB:
  If FreeFile > 0 Then Close
  WRITE_DIALOG "ERROR: " & Error(Err) & "SUB: RIDUZIONE_PUNTI_RAB"
  'Resume Next
  Exit Sub

End Sub

'INPUT : stmp (=stringa da splittare)
'OUTPUT: Ncln (=Numero colonne trovate, max 4),
'        cln1, cln2, cln3, cln4 (valori splittati)
Sub SPLITTA_STRINGA(stmp As String, Ncln As Integer, cln1 As Double, cln2 As Double, cln3 As Double, cln4 As Double)
'
Dim i    As Integer
Dim j    As Integer
Dim k    As Integer
Dim SEP$
Dim vect$()
'
On Error Resume Next
  '
  stmp = UCase$(Trim$(stmp))
  j = InStr(stmp, "|")
  If (j <> 0) Then SEP$ = "|" Else SEP$ = " "
  j = InStr(stmp, ";")
  If (j <> 0) Then SEP$ = ";" Else SEP$ = " "
  j = InStr(stmp, Chr(9)) 'CHR(9)=TAB
  If (j <> 0) Then SEP$ = Chr(9) Else SEP$ = " "
  j = InStr(stmp, ",") 'SVG191112 separatore aggiunto per RSP
  If (j <> 0) Then SEP$ = "," Else SEP$ = " "
  j = InStr(stmp, Chr(32)) 'SVG191112 separatore aggiunto per RSP
  If (j <> 0) Then SEP$ = Chr(32) Else SEP$ = " "
  '**
  vect$ = Split(stmp, SEP$)
  Ncln = 0
  For k = 0 To UBound(vect$)
    If vect$(k) <> "" Then
      Ncln = Ncln + 1
      If Ncln = 1 Then cln1 = val(vect$(k))
      If Ncln = 2 Then cln2 = val(vect$(k))
      If Ncln = 3 Then cln3 = val(vect$(k))
      If Ncln = 4 Then cln4 = val(vect$(k))
    End If
  Next k
  
End Sub

Sub CALCOLO_PROFILO_MOLA_RIDOTTO(RADICE As String, COORDINATE As String, ByVal Dmi As Double, ByVal Dmf As Double, ByVal CORHD As Single, PASSO As Single, ELICA As Single, DiaINT As Single)
'
Dim R()  As Double
Dim A()  As Double
Dim B()  As Double
Dim XX() As Double
Dim YY() As Double
Dim TG() As Double
Dim XD() As Double
Dim YD() As Double
Dim Xc() As Double
Dim YC() As Double
Dim R0() As Double
Dim L()  As Double
'
Dim stmp As String
Dim riga As String
Dim i    As Integer
Dim j    As Integer
'
'Linea di contatto
Dim L1 As Double
Dim L2 As Double
'
On Error GoTo errCALCOLO_PROFILO_MOLA_RIDOTTO
  '
  Dim Dmed  As Double
  Dim Dm    As Double
  Dim Np    As Integer
  Dim NPF1  As Integer
  Dim Tp    As Double
  Dim INC   As Double
  Dim Rinc  As Double
  Dim Ent   As Double
  Dim DRM   As Double
  Dim RMPD  As Double
  Dim Rayon As Double
  Dim ALFA  As Double
  Dim Beta  As Double
  Dim SuRep As Double
  'Dim Rmin  As Double
  Dim Rmax  As Double
  Dim Ymax  As Double
  Dim HDEN  As Double
  Dim Hmax1 As Double
  Dim Hmax2 As Double
  '
  Dim X As Double
  Dim Y As Double
  '
  Dim gama2 As Double
  '
  Dim SensALF As Integer
  Dim Alfa1  As Double
  Dim beta1  As Double
  Dim Rayon1 As Double
  '
  Dim Xml As Double
  Dim Yml As Double
  '
  Dim dtmp As Double
  Dim Gama As Double
  '
  Dim T0 As Double
  Dim FI0 As Double
  '
  Dim RQC As Double
  '
  Dim AA  As Double
  Dim BB  As Double
  Dim cc  As Double
  '
  Dim RAD As Double
  '
  Dim T1 As Double
  Dim T2 As Double
  Dim Ti As Double
  '
  Dim TSI   As Double
  Dim TSIa  As Double
  Dim F     As Double
  Dim FP    As Double
  '
  Dim XS As Double
  Dim Ys As Double
  Dim Zs As Double
  '
  Dim Ap  As Double
  Dim Bp  As Double
  Dim Cp  As Double
  Dim ArgSeno  As Double
  '
    'Acquisizione profilo ed eventuale rotazione
    If Dir$(RADICE & "\" & COORDINATE) <> "" Then
      Open RADICE & "\" & COORDINATE For Input As #1
      i = 1
      While Not EOF(1)
        ReDim Preserve R(i)
        ReDim Preserve A(i)
        ReDim Preserve B(i)
        Input #1, R(i), A(i), B(i)
        i = i + 1
      Wend
      Close #1
      Np = i - 1
    Else
      MsgBox RADICE & "\" & COORDINATE & Chr(13) & Chr(13) & ": " & Error(53), 48, "WARNING"
      Exit Sub
    End If
    '
    ReDim XX(Np + 1)
    ReDim YY(Np + 1)
    ReDim TG(Np + 1)
    ReDim XD(Np + 1)
    ReDim YD(Np + 1)
    ReDim Xc(Np + 1)
    ReDim YC(Np + 1)
    ReDim R0(Np + 1)
    ReDim L(Np + 1)
    '
    INC = (90 - Abs(ELICA)) * PG / 180
    Tp = PASSO / (2 * PG)
    Rinc = Tp * Tan(INC)   'Rinc
    Dmed = (Dmi + Dmf) / 2
    For Dm = Dmi To Dmf Step 2
      Ymax = 0
      NPF1 = 0
      Ent = (Dm + DiaINT) / 2
      DRM = Ent * Rinc
      RMPD = Rinc + Ent
      For i = 1 To Np
        Rayon = R(i): ALFA = A(i) * PG / 180: Beta = B(i) * PG / 180
        SuRep = 0
        GoSub 3900
        XX(i) = X: YY(i) = Y: TG(i) = gama2
        If YY(i) > Ymax Then Ymax = YY(i): NPF1 = i
      Next i
      'Calcolo altezza dente
      Rmax = -1E+308
      For i = 1 To Np
        If R(i) > Rmax Then Rmax = R(i)
      Next i
      HDEN = Rmax - DiaINT / 2 + CORHD
      'Normalizzo Tangenti nei Punti 1 e NP
      If TG(1) < 0 Then
        TG(0) = TG(1)
      Else
        TG(0) = TG(1) + 5 * PG / 180
        If TG(0) > 90 * PG / 180 Then TG(0) = PG / 2
      End If
      If TG(Np) > 0 Then
        TG(Np + 1) = TG(Np)
       Else
        TG(Np + 1) = TG(Np) - 5 * PG / 180
        If TG(Np + 1) < -90 * PG / 180 Then TG(Np + 1) = -PG / 2
      End If
      'Calcolo Altezza Fianco
      Hmax1 = 0
      Hmax2 = 0
      If (TG(0) < 0) And (TG(0) > -75 * PG / 180) Then
          Hmax1 = HDEN
      End If
      If (TG(Np + 1) > 0) And (TG(Np + 1) < 75 * PG / 180) Then
          Hmax2 = HDEN
      End If
AGGPUN:
      '**************************
      ' AGGIUNTA PUNTI 0 e NP+1
      '**************************
      If Hmax1 = 0 Then
        XX(0) = XX(1) - 1
        YY(0) = YY(1) + (XX(1) - XX(0)) / Tan(TG(0))
       Else
        YY(0) = Ymax - Hmax1
        XX(0) = XX(1) + (YY(1) - YY(0)) * Tan(TG(0))
      End If
      If Hmax2 = 0 Then
        XX(Np + 1) = XX(Np) + 1
        YY(Np + 1) = YY(Np) + (XX(Np) - XX(Np + 1)) / Tan(TG(Np + 1))
      Else
        YY(Np + 1) = Ymax - Hmax2
        XX(Np + 1) = XX(Np) + (YY(Np) - YY(Np + 1)) * Tan(TG(Np + 1))
      End If
      '** Scrive Dxxx
      Open RADICE & "\" & "D" & Format$(Dm, "##0") For Output As #3
        For j = 0 To Np + 1
          If Abs(XX(j)) < 0.0005 Then
            Print #3, "+0.000";
          Else
            If XX(j) >= 0 Then
              Print #3, "+" & frmt0(Abs(XX(j)), 3);
            Else
              Print #3, "-" & frmt0(Abs(XX(j)), 3);
            End If
          End If
          If YY(j) >= 0 Then
            Print #3, "+" & frmt0(Abs(YY(j)), 3)
          Else
            Print #3, "-" & frmt0(Abs(YY(j)), 3)
          End If
        Next j
      Close #3
      '** Scrive DMED$: PROFILO MEDIO
      If Dm = Dmed Then
        Open RADICE & "\" & "XYT." & Format$(Dm, "##0") For Output As #2
          For i = 1 To Np
            If XX(i) > 0 Then
              Print #2, "+" & frmt0(Abs(XX(i)), 4) & " ";
            Else
              Print #2, "-" & frmt0(Abs(XX(i)), 4) & " ";
            End If
            If YY(i) > 0 Then
              Print #2, "+" & frmt0(Abs(YY(i)), 4) & " ";
            Else
              Print #2, "-" & frmt0(Abs(YY(i)), 4) & " ";
            End If
            If TG(i) > 0 Then
              Print #2, "+" & frmt0(Abs(TG(i) / PG * 180), 3)
            Else
              Print #2, "-" & frmt0(Abs(TG(i) / PG * 180), 3)
            End If
          Next i
        Close #2
      End If
    Next Dm
    'Linea di contatto F1
    L1 = L(1)
    For i = 1 To NPF1
      If L(i) >= L1 Then L1 = L(i)
    Next i
    i = WritePrivateProfileString("CONTATTO", "ContattoF1", frmt(L1, 4), RADICE & "\INFO.INI")
    'Linea di contatto F2
    L2 = L(NPF1 + 1)
    For i = NPF1 + 1 To Np
      If L(i) >= L2 Then L2 = L(i)
    Next i
    i = WritePrivateProfileString("CONTATTO", "ContattoF2", frmt(L2, 4), RADICE & "\INFO.INI")
    '
Exit Sub

3900  ' ************************************  S/P  ******  CALCUL MEULE
      ' Variables  : RAYON, ALFA, BETA
      ' Constantes : ENT, INC, Pas, SUREP
      '***********************************************************************
If (PASSO < 9999) Then
      SensALF = Sgn(ALFA): If SensALF = 0 Then SensALF = 1
      ALFA = ALFA * SensALF: Beta = Beta * SensALF
      If Abs(Beta) > 0.8 Then
        Alfa1 = ALFA + 0.002
        beta1 = Beta - 0.002
        Rayon1 = Rayon * Sin(Beta) / Sin(beta1)
        GoTo 4030
      End If
      Rayon1 = Rayon - 0.1
      beta1 = FnASN(Rayon * Sin(Beta) / Rayon1)
      Alfa1 = ALFA - beta1 + Beta
4030  GoSub 4090
      Xml = X: Yml = Y
4040  Alfa1 = ALFA: beta1 = Beta: Rayon1 = Rayon
      GoSub 4090
      dtmp = Yml - Y: If dtmp = 0 Then dtmp = 1
      gama2 = Gama * Sgn((X - Xml) / dtmp) * SensALF
      X = X * SensALF
      Y = Y - SuRep * Sin(Abs(gama2))
      X = X - SuRep * Cos(gama2) * Sgn(gama2)
      Return
4090  T0 = PG / 2 - Alfa1
      FI0 = PG / 2 - Alfa1 - beta1
      If FI0 > PG Then FI0 = FI0 - 2 * PG
      If FI0 < -PG Then FI0 = FI0 + 2 * PG
      RQC = Rayon1 * Rayon1 * Cos(T0 - FI0)
      AA = RQC * Sin(T0) + (2 * Tp * Tp + DRM) * Sin(FI0)
      BB = (DRM + Tp * Tp) * Cos(FI0) + RQC * Cos(T0)
      cc = 2 * (-DRM * Sin(FI0) - RQC * Sin(T0) + Rayon1 * RMPD * Cos(T0 - FI0))
      RAD = (BB * BB / AA - cc) / AA
      If RAD < 0 Then WRITE_DIALOG "negative value for square root on R=" & Rayon1: GoTo 4290
      RAD = Sqr(RAD)
      T1 = BB / AA + RAD: T2 = BB / AA - RAD
      Ti = T2
      If Abs(T2) > Abs(T1) Then Ti = T1
      If (T0 < 0) Then Ti = T1
      If (Ti >= PG) Then Ti = PG * 0.9
      'CALCOLO ZERO CON METODO APPROSSIMATO TETA --> TS
      TSI = Ti
      j = 1
4270  F = RQC * Sin(T0 + TSI) + DRM * Sin(FI0 + TSI) + Tp * Tp * TSI * Cos(FI0 + TSI) - Rayon1 * RMPD * Cos(T0 - FI0)
      FP = RQC * Cos(T0 + TSI) + DRM * Cos(FI0 + TSI) + Tp * Tp * Cos(FI0 + TSI) - Tp * Tp * TSI * Sin(FI0 + TSI)
4290  TSIa = TSI - F / FP
      j = j + 1
      If Abs(TSIa - TSI) < 0.0002 Then GoTo 4350
      If j = 80 Then GoTo 4350
      TSI = TSIa
      GoTo 4270
4350  Ti = TSIa
      XS = Rayon1 * Cos(T0 + Ti): Ys = Rayon1 * Sin(T0 + Ti): Zs = Tp * Ti
      'Calcolo della linea di contatto in mm
      L(i) = Ti * (180 / PG) * PASSO / 360
      'CALCOLO TANGENTE AL PROFILO MOLA IN TETAS
      Ap = Tan(Ti + FI0) / XS: Bp = -1 / XS: Cp = (1 + Ys * Tan(Ti + FI0) / XS) / Tp
      ArgSeno = (Ap / Tan(INC) + Cp) / (Sqr(Ap * Ap + Bp * Bp + Cp * Cp) * (1 / Sin(INC)))
      Gama = PG / 2 - FnASN(ArgSeno): If Gama > PG / 2 Then Gama = PG - Gama
      X = Zs * Sin(INC) + XS * Cos(INC)
      Y = Sqr((XS * Sin(INC) - Zs * Cos(INC)) ^ 2 + (Ys - Ent) ^ 2)
Else
      X = Rayon * Sin(ALFA): Y = Ent - Rayon * Cos(ALFA): gama2 = Beta + ALFA
      L(i) = 0
End If
      Return
     
errCALCOLO_PROFILO_MOLA_RIDOTTO:
  If FreeFile > 0 Then Close
  MsgBox "ERROR: " & Error(Err), 48, "SUB: CALCOLO_PROFILO_MOLA_RIDOTTO"
  Exit Sub
     
End Sub

Private Sub CONVERSIONE_XYT_RAB(TIPO As String, RADICE As String, PROFILORAB As String, COORDINATE As String, PASSO As Single, ELICA As Single, Rotazione As Single)
'
Dim retTIPO As Long

Dim SETTORE_TEO As Double
Dim SETTORE_CAL As Double
Dim NumeroDenti As Integer
'
Dim PuntoSeparazioneCAD_ROTORE As Long
'
Dim CIFRE As Integer
Dim LIMITATORE As Integer
'
Dim i     As Integer
Dim j     As Integer
Dim Np    As Integer
Dim stmp  As String
Dim stmp1 As String
'
Dim cln1 As Double  'TESTO COLONNA 1 DEL FILE XYT o RAB
Dim cln2 As Double  'TESTO COLONNA 2 DEL FILE XYT o RAB
Dim cln3 As Double  'TESTO COLONNA 3 DEL FILE XYT o RAB
Dim cln4 As Double  'TESTO COLONNA 4 DEL FILE XYT o RAB
'
Dim PicW As Single
Dim PicH As Single
'
Dim pX1 As Single
Dim pX2 As Single
Dim pY1 As Single
Dim pY2 As Single
'
Dim ftmp1 As Double
'
Const NumeroRette = 30
Const ANGOLO = 30
'
Dim ii() As Long
Dim XX() As Double
Dim YY() As Double
Dim TT() As Double
'
Dim NX As Double
Dim NY As Double
'
Dim RR() As Double
Dim AA() As Double
Dim BB() As Double
'
Dim Rmax As Double
Dim Rmin As Double
Dim Ymin As Double
'
Dim iRmax As Integer
Dim iRmin As Integer
'
Dim OX As Single
Dim OY As Single
'
Dim Scala As Single
'
Dim DistanzaPunti     As Double
Dim DistanzaPuntiMIN  As Double
Dim iDistanzaPuntiMIN As Integer
'
Dim k As Integer
Dim kk As Integer
Dim vect$()
Dim SEP$
'
Dim nFIANCO1 As Long
'
Dim XX1 As Double
Dim YY1 As Double
Dim TT1 As Double
'
Dim XX2 As Double
Dim YY2 As Double
Dim TT2 As Double
'
Dim Delta  As Double
'
Dim PUNTO1          As Integer
Dim PUNTO2          As Integer
Dim NomeROTORE      As String
Dim PercorsoLavoro  As String
Dim NomeProfiloCAD  As String
'
Dim PUNTO1_F1          As Integer
Dim PUNTO2_F1          As Integer
'
Dim PUNTO1_F2          As Integer
Dim PUNTO2_F2          As Integer
'
Const PUNTI = 20
Dim A1 As Double
Dim A2 As Double
'
On Error GoTo errCONVERSIONE_XYT_RAB
  '
  'LEGGO IL VALORE DAL FILE INI PARTICOLARE
  CIFRE = 3
  LIMITATORE = 1: For i = 1 To CIFRE: LIMITATORE = LIMITATORE * 10: Next i
  PercorsoLavoro = RADICE
  NomeProfiloCAD = RADICE & "\" & COORDINATE
  'LETTURA COORDINATE DAL FILE TEORICO DEI PUNTI
  Open NomeProfiloCAD For Input As #1
    i = 1
    Rmax = -1E+308: Rmin = 1E+308: Ymin = 1E+308
    ReDim RR(i)
    '
'NAKANISHI TIPO 1 (I,X,Y,T) E TIPO 2 (X,Y,T) CON T IN GRADI
If (TIPO = "XYT01") Then
      While Not EOF(1)
        ReDim Preserve ii(i)
        ReDim Preserve XX(i): ReDim Preserve YY(i): ReDim Preserve TT(i)
        Input #1, stmp
        Call SPLITTA_STRINGA(stmp, kk, cln1, cln2, cln3, cln4)
        If kk = 4 Then
          ii(i) = cln1
          XX(i) = cln2
          YY(i) = cln3
          TT(i) = cln4
        Else 'KK=3
          ii(i) = i
          XX(i) = cln1
          YY(i) = cln2
          TT(i) = cln3
        End If
        'ROTAZIONE: CONSIDERA LO ZERO DALL'ASSE VERTICALE
        TT(i) = 90 + TT(i)
        'RICERCA DEL RAGGIO MASSIMO E MINIMO
        RR(0) = Sqr(XX(i) ^ 2 + YY(i) ^ 2)
        'FORMATTO A CIFRE DOPO LA VIRGOLA
        RR(0) = Int(RR(0) * LIMITATORE) / LIMITATORE
        If (RR(0) > Rmax) Then Rmax = RR(0): iRmax = i
        If (RR(0) < Rmin) Then Rmin = RR(0): iRmin = i
        If (YY(i) < Ymin) Then Ymin = YY(i)
        i = i + 1
      Wend
      'SEPARAZIONE
      PuntoSeparazioneCAD_ROTORE = iRmin
      GoSub CONVERSIONE_XYT_RAB
      '
'TIPO XYT, IXYT con T=dY/dX (i.e. VALORE TG)
ElseIf (TIPO = "NAKA2") Then
      While Not EOF(1)
        ReDim Preserve ii(i)
        ReDim Preserve XX(i): ReDim Preserve YY(i): ReDim Preserve TT(i)
        Input #1, stmp
        stmp = UCase$(Trim$(stmp))
        Call SPLITTA_STRINGA(stmp, kk, cln1, cln2, cln3, cln4)
        If kk = 4 Then
          ii(i) = cln1
          XX(i) = cln2
          YY(i) = cln3
          TT(i) = Atn(cln4) * 180 / PG
        Else 'KK=3
          ii(i) = i
          XX(i) = cln1
          YY(i) = cln2
          TT(i) = Atn(cln3) * 180 / PG
        End If
        'RICERCA DEL RAGGIO MASSIMO E MINIMO
        RR(0) = Sqr(XX(i) ^ 2 + YY(i) ^ 2)
        'FORMATTO A CIFRE DOPO LA VIRGOLA
        RR(0) = Int(RR(0) * LIMITATORE) / LIMITATORE
        If (RR(0) > Rmax) Then Rmax = RR(0): iRmax = i
        If (RR(0) < Rmin) Then Rmin = RR(0): iRmin = i
        If (YY(i) < Ymin) Then Ymin = YY(i)
        i = i + 1
      Wend
      'SEPARAZIONE
      PuntoSeparazioneCAD_ROTORE = iRmin
      ReDim RR(UBound(XX)): ReDim AA(UBound(XX)): ReDim BB(UBound(XX))
      stmp = ""
      stmp = stmp & "FEMALE ROTOR WITH ROTATION OF 90deg --> YES" & Chr(13) & Chr(13)
      stmp = stmp & "MALE ROTOR WITH ROTATION OF 45deg --> NO" & Chr(13)
      retTIPO = MsgBox(stmp, 36, "SELECT CONVERSION TYPE!!!!")
      If (retTIPO = 6) Then
        'ROTORE FEMMINA VANO SECONDO QUADRANTE
        'VANO XYT --> VANO RAB
        For i = UBound(XX) To 1 Step -1
          j = i
          'RAGGIO
          RR(j) = Sqr(XX(i) ^ 2 + YY(i) ^ 2)
          'FORMATTO A CIFRE DOPO LA VIRGOLA
          RR(j) = Int(RR(j) * LIMITATORE) / LIMITATORE
          'ALFA
          If (YY(i) = 0) Then
            AA(j) = -90
          Else
            AA(j) = Atn(XX(i) / YY(i)) * 180 / PG
          End If
          'BETA
          BB(j) = -AA(j) - TT(i) + 90
          If BB(j) > 90 Then BB(j) = -180 + BB(j)
          If BB(j) < -90 Then BB(j) = 180 + BB(j)
          If AA(j) > 0 Then AA(j) = -180 + AA(j)
          If (BB(j) > 89.99) And (BB(j) < 90.01) Then BB(j) = 90
          AA(j) = AA(j) + 90
          AA(j) = -AA(j): BB(j) = -BB(j)
          '07.12.07 CONTROLLO AGGIUNTO PER SISTEMARE IL BETA VICINO AL DIA ESTERNO
          If Abs(AA(j)) > 5 Then
            If (Abs(BB(j)) > 88) And (Abs(BB(j)) < 90.1) Then
              BB(j) = Sgn(AA(j)) * Abs(BB(j))
            End If
          End If
          AA(j) = AA(j) + Rotazione
        Next i
      Else
        'ROTORE MASCHIO
        'VANO XYT --> VANO RAB
        For i = 1 To UBound(XX)
          'j = i
          j = UBound(XX) - i + 1
          'RAGGIO
          RR(j) = Sqr(XX(i) ^ 2 + YY(i) ^ 2)
          'FORMATTO A CIFRE DOPO LA VIRGOLA
          RR(j) = Int(RR(j) * LIMITATORE) / LIMITATORE
          'ALFA
          If (YY(i) = 0) Then
            AA(j) = 90
          Else
            AA(j) = Atn(XX(i) / YY(i)) * 180 / PG
          End If
          'BETA
          BB(j) = 90 - AA(j) - TT(i)
          AA(j) = AA(j) - 45 'ROTAZIONE DI 45 GRADI
'          If (BB(j) > 89.99) And (BB(j) < 90.01) Then BB(j) = 90
'          If (BB(j) < 89.999) Then BB(j) = -BB(j) Else BB(j) = 180 - BB(j)
          If (BB(j) > 89.999) Then BB(j) = BB(j) - 180
          If (Abs(BB(j)) > 89.99) And (Abs(BB(j)) < 90.01) Then BB(j) = Sgn(AA(j)) * 90
          AA(j) = AA(j) + Rotazione
        Next i
      End If
      '
'KLINGELNBERG ASIMMETRICO
ElseIf (TIPO = "KLINGELNBERG") Then
      Do While Not EOF(1)
        'LEGGO LA PRIMA RIGA E LA PULISCO
        Line Input #1, stmp: stmp = UCase$(Trim$(stmp))
         'CONTROLLO SE SONO ARRIVATO ALLA SEZIONE DATI
        j = InStr(stmp, "$IN DAT_ST")
        If (Len(stmp) > 0) And (j <= 0) Then
          'ALLOCO LE VARIABILI
          ReDim Preserve ii(i)
          ReDim Preserve XX(i): ReDim Preserve YY(i): ReDim Preserve TT(i)
          Call SPLITTA_STRINGA(stmp, kk, cln1, cln2, cln3, cln4)
          If kk = 4 Then
            ii(i) = cln1  'INDICE
            XX(i) = cln2  'ASCISSA
            YY(i) = cln3  'ORDINATA
            TT(i) = cln4  'TANGENTE
          Else 'KK=3
            ii(i) = i
            XX(i) = cln1  'ASCISSA
            YY(i) = cln2  'ORDINATA
            TT(i) = cln3  'TANGENTE
          End If
          'ROTAZIONE: CONSIDERA LO ZERO DALL'ASSE VERTICALE
          TT(i) = 90 + TT(i)
          'RICERCA DEL RAGGIO MASSIMO E MINIMO
          RR(0) = Sqr(XX(i) ^ 2 + YY(i) ^ 2)
          'FORMATTO A CIFRE DOPO LA VIRGOLA
          RR(0) = Int(RR(0) * LIMITATORE) / LIMITATORE
          If RR(0) > Rmax Then Rmax = RR(0): iRmax = i
          If RR(0) < Rmin Then Rmin = RR(0): iRmin = i
          If YY(i) < Ymin Then Ymin = YY(i)
          i = i + 1
        Else
          Exit Do
        End If
      Loop
      'SEPARAZIONE
      PuntoSeparazioneCAD_ROTORE = iRmin
      GoSub CONVERSIONE_XYT_RAB
      '
'KLINGELNBERG SIMMETRICO
ElseIf (TIPO = "XYTMIRROR") Or ((TIPO = "YXTMIRROR")) Then
      Do While Not EOF(1)
        'LEGGO LA PRIMA RIGA E LA PULISCO
        Line Input #1, stmp: stmp = UCase$(Trim$(stmp))
        If (Len(stmp) > 0) Then
          'ALLOCO LE VARIABILI
          ReDim Preserve ii(i)
          ReDim Preserve XX(i): ReDim Preserve YY(i): ReDim Preserve TT(i)
          If (TIPO = "XYTMIRROR") Then
            Call SPLITTA_STRINGA(stmp, kk, cln1, cln2, cln3, cln4)
          Else
            Call SPLITTA_STRINGA(stmp, kk, cln2, cln1, cln3, cln4)
          End If
          If (kk = 4) Then
            ii(i) = cln1  'INDICE
            XX(i) = cln2  'ASCISSA
            YY(i) = cln3  'ORDINATA
            TT(i) = cln4  'TANGENTE
          Else 'KK=3
            ii(i) = i
            XX(i) = cln1  'ASCISSA
            YY(i) = cln2  'ORDINATA
            TT(i) = cln3  'TANGENTE
          End If
          'INDICE DEL PUNTO PRECEDENTE DI SEPARAZIONE
          PUNTO1 = i - 1
          'SEPARAZIONE DEL FIANCO
          If (XX(i) > 0) Then Exit Do
          'ROTAZIONE: CONSIDERA LO ZERO DALL'ASSE VERTICALE
          TT(i) = 90 + TT(i)
          'RICERCA DEL RAGGIO MASSIMO E MINIMO
          RR(0) = Sqr(XX(i) ^ 2 + YY(i) ^ 2)
          'FORMATTO A CIFRE DOPO LA VIRGOLA
          RR(0) = Int(RR(0) * LIMITATORE) / LIMITATORE
          If (RR(0) > Rmax) Then Rmax = RR(0): iRmax = i
          If (RR(0) < Rmin) Then Rmin = RR(0): iRmin = i
          If (YY(i) < Ymin) Then Ymin = YY(i)
          'INCREMENTO IL CONTATORE
          i = i + 1
        Else
          'ESCO DAL CICLO
          Exit Do
        End If
      Loop
      'SEPARAZIONE
      i = Int(iRmin / 2) * 2
      If (i = iRmin) Then
        'PARI
        PuntoSeparazioneCAD_ROTORE = iRmin - 1
      Else
        'DISPARI
        PuntoSeparazioneCAD_ROTORE = iRmin
      End If
      If (XX(PUNTO1) < 0) Then
        'INCREMENTO IL CONTATORE
        PUNTO1 = PUNTO1 + 1
        'ALLOCO LE VARIABILI
        ReDim Preserve ii(PUNTO1)
        ReDim Preserve XX(PUNTO1): ReDim Preserve YY(PUNTO1): ReDim Preserve TT(PUNTO1)
        'INIZIALIZZO XX, YY E TT
        ii(PUNTO1) = PUNTO1
        XX(PUNTO1) = 0
        YY(PUNTO1) = Sqr(XX(PUNTO1 - 2) * XX(PUNTO1 - 2) + YY(PUNTO1 - 2) * YY(PUNTO1 - 2))
        TT(PUNTO1) = 180
      End If
      'IL PROFILO DEVE CONTENERE IL PUNTO 0 DI SIMMETRIA
      Np = (PUNTO1 - 1) * 2 + 1
      'ALLOCO LE VARIABILI
      ReDim Preserve ii(Np)
      ReDim Preserve XX(Np): ReDim Preserve YY(Np): ReDim Preserve TT(Np)
      'INVERSIONE PUNTI
      For i = 1 To PUNTO1 - 1
        ii(PUNTO1 + i) = PUNTO1 + i
        XX(PUNTO1 + i) = -XX(PUNTO1 - i)
        YY(PUNTO1 + i) = YY(PUNTO1 - i)
        TT(PUNTO1 + i) = 180 - TT(PUNTO1 - i)
      Next i
      GoSub CONVERSIONE_XYT_RAB
      Delta = Lp("RISOLUZIONE")
      If (Delta <= 0) Then
        'FISSO IL PUNTO DI SEPARAZIONE MOLA
        i = WritePrivateProfileString("CONVERSIONE", "NPMF1tmp", Format$(PuntoSeparazioneCAD_ROTORE, "####0"), RADICE & "\INFO.INI")
      Else
        'FACCIO DETERMINARE IL PUNTO DI SEPARAZIONE PERCHE' RIDUCO I PUNTI
        i = WritePrivateProfileString("CONVERSIONE", "NPMF1tmp", "0", RADICE & "\INFO.INI")
      End If
      '
    'JPG
ElseIf (TIPO = "JPG") Then
      Do While Not EOF(1)
        'LEGGO LA PRIMA RIGA E LA PULISCO
        Line Input #1, stmp: stmp = UCase$(Trim$(stmp))
        If (Len(stmp) > 0) Then
          'ALLOCO LE VARIABILI
          ReDim Preserve ii(i)
          ReDim Preserve RR(i): ReDim Preserve AA(i): ReDim Preserve BB(i)
          Call SPLITTA_STRINGA(stmp, kk, cln1, cln2, cln3, cln4)
          If (kk = 4) Then
            ii(i) = cln1  'INDICE
            RR(i) = cln2  'ASCISSA
            AA(i) = cln3  'ALFA
            BB(i) = cln4  'BETA
          Else 'KK=3
            ii(i) = i
            RR(i) = cln1  'RAGGIO
            AA(i) = cln2  'ALFA
            BB(i) = cln3  'BETA
          End If
          AA(i) = AA(i) + Rotazione
          'RICERCA DEL RAGGIO MASSIMO E MINIMO
          RR(0) = RR(i)
          'FORMATTO A CIFRE DOPO LA VIRGOLA
          RR(0) = Int(RR(0) * LIMITATORE) / LIMITATORE
          If RR(0) > Rmax Then Rmax = RR(0): iRmax = i
          If RR(0) < Rmin Then Rmin = RR(0): iRmin = i
          i = i + 1
        Else
          Exit Do
        End If
      Loop
      'SEPARAZIONE
      PuntoSeparazioneCAD_ROTORE = iRmin
      'CONVERSIONE RAB --> XYT
      ReDim XX(UBound(RR)): ReDim YY(UBound(RR)): ReDim TT(UBound(RR))
      For i = 1 To UBound(RR)
        XX(i) = RR(i) * Sin(AA(i) / 180 * PG)
        YY(i) = RR(i) * Cos(AA(i) / 180 * PG)
        TT(i) = -90 - AA(i) - BB(i)
        If (YY(i) < Ymin) Then Ymin = YY(i)
      Next i
      '
    'PROFILO IN COORDINATE CARTESIANE CON TANTI PUNTI
ElseIf (TIPO = "YX") Or (TIPO = "XY") Then
      i = 0
      While Not EOF(1)
        i = i + 1
        ReDim Preserve ii(i)
        ReDim Preserve XX(i):  ReDim Preserve YY(i)
        'LEGGO LA PRIMA RIGA E LA PULISCO
        Line Input #1, stmp: stmp = UCase$(Trim$(stmp))
        'INDICE
        ii(i) = i
        If (TIPO = "YX") Then
          Call SPLITTA_STRINGA(stmp, kk, YY(i), XX(i), cln3, cln4)
        Else
          Call SPLITTA_STRINGA(stmp, kk, XX(i), YY(i), cln3, cln4)
        End If
        'RICERCA DEL RAGGIO MASSIMO E MINIMO
        RR(0) = Sqr(XX(i) ^ 2 + YY(i) ^ 2)
        'FORMATTO A CIFRE DOPO LA VIRGOLA
        RR(0) = Int(RR(0) * LIMITATORE) / LIMITATORE
        If (RR(0) > Rmax) Then Rmax = RR(0): iRmax = i
        If (RR(0) < Rmin) Then Rmin = RR(0): iRmin = i
        If (YY(i) < Ymin) Then Ymin = YY(i)
      Wend
      'RICAVO TT DALLE COORDINATE XX e YY
      ReDim TT(UBound(XX))
      'PRIMO PUNTO
      i = 1
      If (YY(i) = 0) Then YY(i) = YY(i) + 0.0000001
      If (XX(i) = XX(i + 1)) Then XX(i + 1) = XX(i) + 0.0000001
      'ULTIMO PUNTO
      i = UBound(XX)
      If (YY(i) = 0) Then YY(i) = YY(i) + 0.0000001
      If XX(i) = XX(i - 1) Then XX(i - 1) = XX(i - 1) + 0.0000001
      For i = 2 To UBound(XX) - 1
        If (YY(i) = 0) Then YY(i) = YY(i) + 0.0000001
        If (XX(i) = XX(i - 1)) Then XX(i - 1) = XX(i - 1) + 0.0000001
        If (XX(i) = XX(i + 1)) Then XX(i + 1) = XX(i) + 0.0000001
        A1 = Atn((YY(i - 1) - YY(i)) / (XX(i - 1) - XX(i)))
        A2 = Atn((YY(i) - YY(i + 1)) / (XX(i) - XX(i + 1)))
          If ((A1 > 0) And (A2 < 0)) Or ((A1 < 0) And (A2 > 0)) Then
          TT(i) = A1
        ElseIf (A1 = 0) And (A2 <> 0) Then
          TT(i) = A2
        ElseIf (A2 = 0) And (A1 <> 0) Then
          TT(i) = A1
        Else
          TT(i) = (A1 + A2) / 2
        End If
        TT(i) = TT(i) * 180 / PG
      Next i
      'PRIMO PUNTO
      i = 1
      A2 = Atn((YY(i) - YY(i + 1)) / (XX(i) - XX(i + 1)))
      TT(i) = A2
      TT(i) = TT(i) * 180 / PG
      'ULTIMO PUNTO
      i = UBound(XX)
      A1 = Atn((YY(i - 1) - YY(i)) / (XX(i - 1) - XX(i)))
      TT(i) = A1
      TT(i) = TT(i) * 180 / PG
      'CONTINUITA' DELLE TANGENTI INTERNE
      For i = 2 To UBound(XX) - 1
      A1 = TT(i) - TT(i - 1): A2 = TT(i + 1) - TT(i)
      If (Abs(A1) > 10) And (Abs(A2) > 10) Then
        TT(i) = TT(i + 1) / 2 + TT(i - 1) / 2
      End If
      Next i
      'GESTIONE DI EVENTUALI DUE SPIGOLI
      'RICERCA DEI VALORI DI PUNTO1 E PUNTO2
      PUNTO1_F1 = 0: PUNTO2_F2 = 0
      PUNTO1_F2 = 0: PUNTO2_F2 = 0
      For i = 2 To UBound(XX)
        If Abs(XX(i) - XX(i - 1)) < 0.001 And Abs(YY(i) - YY(i - 1)) < 0.001 And Abs(TT(i) - TT(i - 1)) > 10 Then
          PUNTO1_F1 = i - 1: PUNTO2_F1 = i: Exit For
        End If
      Next i
      For j = i + 1 To UBound(XX)
        If Abs(XX(j) - XX(j - 1)) < 0.001 And Abs(YY(j) - YY(j - 1)) < 0.001 And Abs(TT(j) - TT(j - 1)) > 10 Then
          PUNTO1_F2 = j - 1: PUNTO2_F2 = j: Exit For
        End If
      Next j
      If (PUNTO1_F1 > 0) Or (PUNTO1_F2 > 0) Then
        'CREAZIONE DEL PROFILO TRASVERSALE XYT
        Open RADICE & "\COORDINATE.XYT" For Output As #2
        For i = 1 To UBound(XX)
          stmp = Left$(Format$(i, "##############0") & String(15, " "), 15) & " "
          stmp1 = Left$(frmt0(Abs(XX(i)), CIFRE) & String(15, " "), 15) & " "
          If (XX(i) >= 0) Then
            If (InStr(stmp1, "+") > 0) Then
              stmp = stmp & " " & stmp1
            Else
              stmp = stmp & "+" & stmp1
            End If
          Else
            stmp = stmp & "-" & stmp1
          End If
          stmp1 = Left$(frmt0(Abs(YY(i)), CIFRE) & String(15, " "), 15) & " "
          If (YY(i) >= 0) Then
            If (InStr(stmp1, "+") > 0) Then
              stmp = stmp & " " & stmp1
            Else
              stmp = stmp & "+" & stmp1
            End If
          Else
            stmp = stmp & "-" & stmp1
          End If
          stmp1 = Left$(frmt0(Abs(TT(i)), CIFRE) & String(15, " "), 15) & " "
          If (TT(i) >= 0) Then
            If (InStr(stmp1, "+") > 0) Then
              stmp = stmp & " " & stmp1
            Else
              stmp = stmp & "+" & stmp1
            End If
          Else
            stmp = stmp & "-" & stmp1
          End If
          Print #2, stmp
        Next i
        Close #2
        'LETTURA COORDINATE DAL FILE TEORICO DEI PUNTI
        Open RADICE & "\COORDINATE.XYT" For Input As #2
        i = 1
        Rmax = -1E+308: Rmin = 1E+308: Ymin = 1E+308
        ReDim RR(i)
        While Not EOF(2)
          ReDim Preserve ii(i)
          ReDim Preserve XX(i): ReDim Preserve YY(i): ReDim Preserve TT(i)
          Input #2, stmp
          Call SPLITTA_STRINGA(stmp, kk, cln1, cln2, cln3, cln4)
          ii(i) = cln1: XX(i) = cln2: YY(i) = cln3: TT(i) = cln4
          If (ii(i) < PUNTO1_F2) Then
            Select Case ii(i)
            Case Is < PUNTO1_F1
              'RICERCA DEL RAGGIO MASSIMO E MINIMO
              RR(0) = Sqr(XX(i) ^ 2 + YY(i) ^ 2)
              'FORMATTO A CIFRE DOPO LA VIRGOLA
              RR(0) = Int(RR(0) * LIMITATORE) / LIMITATORE
              If (RR(0) > Rmax) Then Rmax = RR(0): iRmax = i
              If (RR(0) < Rmin) Then Rmin = RR(0): iRmin = i
              If (YY(i) < Ymin) Then Ymin = YY(i)
              i = i + 1
            Case Is = PUNTO1_F1
              'MEMORIZZAZIONE DEL PUNTO1
              XX1 = XX(i): YY1 = YY(i): TT1 = TT(i)
              i = i + 1
            Case Is = PUNTO2_F1
              'MEMORIZZAZIONE DEL PUNTO2
              XX2 = XX(i): YY2 = YY(i): TT2 = TT(i)
              'CALCOLO DEL DELTA
              Delta = (TT2 - TT1) / PUNTI
              'INSERIMENTO DI 20 PUNTI NELLA LISTA
              For j = 1 To PUNTI - 1
                ReDim Preserve XX(i): ReDim Preserve YY(i): ReDim Preserve TT(i)
                XX(i) = XX1
                YY(i) = YY1
                TT(i) = TT1 + Delta * j
                i = i + 1
              Next j
              'SCRITTURA DEL PUNTO2 PER CONTINUARE
              ReDim Preserve XX(i): ReDim Preserve YY(i): ReDim Preserve TT(i)
              XX(i) = XX2: YY(i) = YY2: TT(i) = TT2
              i = i + 1
            Case Is > PUNTO2_F1
              'RICERCA DEL RAGGIO MASSIMO E MINIMO
              RR(0) = Sqr(XX(i) ^ 2 + YY(i) ^ 2)
              'FORMATTO A CIFRE DOPO LA VIRGOLA
              RR(0) = Int(RR(0) * LIMITATORE) / LIMITATORE
              If (RR(0) > Rmax) Then Rmax = RR(0): iRmax = i
              If (RR(0) < Rmin) Then Rmin = RR(0): iRmin = i
              If (YY(i) < Ymin) Then Ymin = YY(i)
              i = i + 1
            End Select
          Else
            Select Case ii(i)
            Case Is < PUNTO1_F2
              'RICERCA DEL RAGGIO MASSIMO E MINIMO
              RR(0) = Sqr(XX(i) ^ 2 + YY(i) ^ 2)
              'FORMATTO A CIFRE DOPO LA VIRGOLA
              RR(0) = Int(RR(0) * LIMITATORE) / LIMITATORE
              If (RR(0) > Rmax) Then Rmax = RR(0): iRmax = i
              If (RR(0) < Rmin) Then Rmin = RR(0): iRmin = i
              If (YY(i) < Ymin) Then Ymin = YY(i)
              i = i + 1
            Case Is = PUNTO1_F2
              'MEMORIZZAZIONE DEL PUNTO1
              XX1 = XX(i): YY1 = YY(i): TT1 = TT(i)
              i = i + 1
            Case Is = PUNTO2_F2
              'MEMORIZZAZIONE DEL PUNTO2
              XX2 = XX(i): YY2 = YY(i): TT2 = TT(i)
              'CALCOLO DEL DELTA
              Delta = (TT2 - TT1) / PUNTI
              'INSERIMENTO DI 20 PUNTI NELLA LISTA
              For j = 1 To PUNTI - 1
                ReDim Preserve XX(i): ReDim Preserve YY(i): ReDim Preserve TT(i)
                XX(i) = XX1
                YY(i) = YY1
                TT(i) = TT1 + Delta * j
                i = i + 1
              Next j
              'SCRITTURA DEL PUNTO2 PER CONTINUARE
              ReDim Preserve XX(i): ReDim Preserve YY(i): ReDim Preserve TT(i)
              XX(i) = XX2: YY(i) = YY2: TT(i) = TT2
              i = i + 1
            Case Is > PUNTO2_F2
              'RICERCA DEL RAGGIO MASSIMO E MINIMO
              RR(0) = Sqr(XX(i) ^ 2 + YY(i) ^ 2)
              'FORMATTO A CIFRE DOPO LA VIRGOLA
              RR(0) = Int(RR(0) * LIMITATORE) / LIMITATORE
              If (RR(0) > Rmax) Then Rmax = RR(0): iRmax = i
              If (RR(0) < Rmin) Then Rmin = RR(0): iRmin = i
              If (YY(i) < Ymin) Then Ymin = YY(i)
              i = i + 1
            End Select
          End If
        Wend
        Close #2
      End If
      'SEPARAZIONE
      PuntoSeparazioneCAD_ROTORE = iRmin
      GoSub CONVERSIONE_XYT_RAB
      '
    'PROFILO IN COORDINATE CARTESIANE CON TANTI PUNTI CON MIRROR
ElseIf (TIPO = "YXMIRROR") Or (TIPO = "XYMIRROR") Then
      i = 0
      While Not EOF(1)
        i = i + 1
        ReDim Preserve ii(i)
        ReDim Preserve XX(i):  ReDim Preserve YY(i)
        'LEGGO LA PRIMA RIGA E LA PULISCO
        Line Input #1, stmp: stmp = UCase$(Trim$(stmp))
        'INDICE
        ii(i) = i
        If (TIPO = "YXMIRROR") Then
          Call SPLITTA_STRINGA(stmp, kk, YY(i), XX(i), cln3, cln4)
        Else
          Call SPLITTA_STRINGA(stmp, kk, XX(i), YY(i), cln3, cln4)
        End If
        'RICERCA DEL RAGGIO MASSIMO E MINIMO
        RR(0) = Sqr(XX(i) ^ 2 + YY(i) ^ 2)
        'FORMATTO A CIFRE DOPO LA VIRGOLA
        RR(0) = Int(RR(0) * LIMITATORE) / LIMITATORE
        If RR(0) > Rmax Then Rmax = RR(0): iRmax = i
        If RR(0) < Rmin Then Rmin = RR(0): iRmin = i
        If YY(i) < Ymin Then Ymin = YY(i)
      Wend
      'RICAVO TT DALLE COORDINATE XX e YY
      ReDim TT(UBound(XX))
      For i = 1 To UBound(XX)
        If (YY(i) = 0) Then YY(i) = YY(i) + 0.0000001
        If (i > 1) And (i < UBound(XX)) Then
          If XX(i) = XX(i - 1) Then XX(i - 1) = XX(i - 1) + 0.0000001
          If XX(i) = XX(i + 1) Then XX(i + 1) = XX(i) + 0.0000001
          A1 = Atn((YY(i - 1) - YY(i)) / (XX(i - 1) - XX(i)))
          A2 = Atn((YY(i) - YY(i + 1)) / (XX(i) - XX(i + 1)))
            If ((A1 > 0) And (A2 < 0)) Or ((A1 < 0) And (A2 > 0)) Then
            TT(i) = A1
          ElseIf (A1 = 0) And (A2 <> 0) Then
            TT(i) = A2
          ElseIf (A2 = 0) And (A1 <> 0) Then
            TT(i) = A1
          Else
            TT(i) = (A1 + A2) / 2
          End If
          TT(i) = TT(i) * 180 / PG
        End If
      Next i
      'PRIMO PUNTO
      i = 1
      If (YY(i) = 0) Then YY(i) = YY(i) + 0.0000001
      If XX(i) = XX(i + 1) Then XX(i + 1) = XX(i) + 0.0000001
      A2 = Atn((YY(i) - YY(i + 1)) / (XX(i) - XX(i + 1)))
      TT(i) = A2
      TT(i) = TT(i) * 180 / PG
      'ULTIMO PUNTO
      i = UBound(XX)
      If (YY(i) = 0) Then YY(i) = YY(i) + 0.0000001
      If XX(i) = XX(i - 1) Then XX(i - 1) = XX(i - 1) + 0.0000001
      A1 = Atn((YY(i - 1) - YY(i)) / (XX(i - 1) - XX(i)))
      TT(i) = A1
      TT(i) = TT(i) * 180 / PG
      'SEPARAZIONE
      i = Int(iRmin / 2) * 2
      If (i = iRmin) Then
        'PARI
        PuntoSeparazioneCAD_ROTORE = iRmin - 1
      Else
        'DISPARI
        PuntoSeparazioneCAD_ROTORE = iRmin
      End If
      PUNTO1 = UBound(XX)
      If (XX(PUNTO1) < 0) Then
        'INCREMENTO IL CONTATORE
        PUNTO1 = PUNTO1 + 1
        'ALLOCO LE VARIABILI
        ReDim Preserve ii(PUNTO1)
        ReDim Preserve XX(PUNTO1): ReDim Preserve YY(PUNTO1): ReDim Preserve TT(PUNTO1)
        'INIZIALIZZO XX, YY E TT
        ii(PUNTO1) = PUNTO1
        XX(PUNTO1) = 0
        YY(PUNTO1) = Sqr(XX(PUNTO1 - 2) * XX(PUNTO1 - 2) + YY(PUNTO1 - 2) * YY(PUNTO1 - 2))
        TT(PUNTO1) = 180
      End If
      'IL PROFILO DEVE CONTENERE IL PUNTO 0 DI SIMMETRIA
      Np = (PUNTO1 - 1) * 2 + 1
      'ALLOCO LE VARIABILI
      ReDim Preserve ii(Np)
      ReDim Preserve XX(Np): ReDim Preserve YY(Np): ReDim Preserve TT(Np)
      'INVERSIONE PUNTI
      For i = 1 To PUNTO1 - 1
        ii(PUNTO1 + i) = PUNTO1 + i
        XX(PUNTO1 + i) = -XX(PUNTO1 - i)
        YY(PUNTO1 + i) = YY(PUNTO1 - i)
        TT(PUNTO1 + i) = 180 - TT(PUNTO1 - i)
      Next i
      GoSub CONVERSIONE_XYT_RAB
      Delta = Lp("RISOLUZIONE")
      If (Delta <= 0) Then
        'FISSO IL PUNTO DI SEPARAZIONE MOLA
        i = WritePrivateProfileString("CONVERSIONE", "NPMF1tmp", Format$(PuntoSeparazioneCAD_ROTORE, "####0"), RADICE & "\INFO.INI")
      Else
        'FACCIO DETERMINARE IL PUNTO DI SEPARAZIONE PERCHE' RIDUCO I PUNTI
        i = WritePrivateProfileString("CONVERSIONE", "NPMF1tmp", "0", RADICE & "\INFO.INI")
      End If
      'SEPARAZIONE
      PuntoSeparazioneCAD_ROTORE = iRmin
      GoSub CONVERSIONE_XYT_RAB
      '
    'SIMILE A DIB (XY) MA SONO STATE APPORTATE DELLE MODIFICHE PER CONTROLLARE I PUNTI DI KOMSAN
ElseIf (TIPO = "KOMSAN") Then
      stmp = ""
      stmp = stmp & "FEMALE --> YES" & Chr(13) & Chr(13)
      stmp = stmp & "MALE   --> NO" & Chr(13)
      StopRegieEvents
      retTIPO = MsgBox(stmp, 36, "DEFINE ROTOR TYPE")
      ResumeRegieEvents
      i = 0
      While Not EOF(1)
        i = i + 1
        ReDim Preserve ii(i)
        ReDim Preserve XX(i):  ReDim Preserve YY(i)
        'LEGGO LA PRIMA RIGA E LA PULISCO
        Line Input #1, stmp: stmp = UCase$(Trim$(stmp))
        'INDICE
        ii(i) = i
        'ASCISSA
        j = InStr(stmp, " ")
        If (retTIPO = 6) Then
          YY(i) = -val(Left$(stmp, j - 1)) * 1000 'SOLO PER KOMSAN FEMMINE ESPRESSI IN METRI
        Else
          YY(i) = val(Left$(stmp, j - 1)) * 1000  'SOLO PER KOMSAN MASCHI ESPRESSI IN METRI
        End If
        stmp = Trim$(Right$(stmp, Len(stmp) - j))
        'ORDINATA
        XX(i) = val(stmp) * 1000 'SOLO PER KOMSAN ESPRESSI IN METRI
        'RICERCA DEL RAGGIO MASSIMO E MINIMO
        RR(0) = Sqr(XX(i) ^ 2 + YY(i) ^ 2)
        'FORMATTO A CIFRE DOPO LA VIRGOLA
        RR(0) = Int(RR(0) * LIMITATORE) / LIMITATORE
        If RR(0) > Rmax Then Rmax = RR(0): iRmax = i
        If RR(0) < Rmin Then Rmin = RR(0): iRmin = i
        If YY(i) < Ymin Then Ymin = YY(i)
      Wend
      'RICAVO TT DALLE COORDINATE XX e YY
      ReDim TT(UBound(XX))
      For i = 1 To UBound(XX)
        If (YY(i) = 0) Then YY(i) = YY(i) + 0.00000001
        If (i > 1) And (i < UBound(XX)) Then
          If XX(i) = XX(i - 1) Then XX(i - 1) = XX(i - 1) + 0.00000001
          If XX(i) = XX(i + 1) Then XX(i + 1) = XX(i) + 0.00000001
          A1 = Atn((YY(i - 1) - YY(i)) / (XX(i - 1) - XX(i)))
          A2 = Atn((YY(i) - YY(i + 1)) / (XX(i) - XX(i + 1)))
          TT(i) = (A1 + A2) / 2
          TT(i) = TT(i) * 180 / PG
        End If
      Next i
      'TT(1) = TT(2): TT(UBound(XX)) = TT(UBound(XX) - 1)
      'PRIMO PUNTO
      i = 1
      If (YY(i) = 0) Then YY(i) = YY(i) + 0.00000001
      If XX(i) = XX(i + 1) Then XX(i + 1) = XX(i) + 0.00000001
      A2 = Atn((YY(i) - YY(i + 1)) / (XX(i) - XX(i + 1)))
      TT(i) = A2
      TT(i) = TT(i) * 180 / PG
      'ULTIMO PUNTO
      i = UBound(XX)
      If (YY(i) = 0) Then YY(i) = YY(i) + 0.00000001
      If XX(i) = XX(i - 1) Then XX(i - 1) = XX(i - 1) + 0.00000001
      A1 = Atn((YY(i - 1) - YY(i)) / (XX(i - 1) - XX(i)))
      TT(i) = A1
      TT(i) = TT(i) * 180 / PG
      'SEPARAZIONE
      If (retTIPO = 6) Then
        PuntoSeparazioneCAD_ROTORE = iRmin
      Else
        PuntoSeparazioneCAD_ROTORE = iRmax  'SOLO PER KOMSAN MASCHI
      End If
      GoSub CONVERSIONE_XYT_RAB
      NumeroDenti = val(Lp("PRINCIPI"))
      'INVERSIONE PIENO --> VANO SOLO PER KOMSAN MASCHI
      If (retTIPO <> 6) Then
        'CONVERSIONE XYT ----> RAB
        ReDim RR(UBound(XX)): ReDim AA(UBound(XX)): ReDim BB(UBound(XX))
        For i = 1 To PuntoSeparazioneCAD_ROTORE
          j = PuntoSeparazioneCAD_ROTORE - i + 1
          'RAGGIO
          RR(j) = Sqr(XX(i) ^ 2 + YY(i) ^ 2)
          'FORMATTO A CIFRE DOPO LA VIRGOLA
          RR(j) = Int(RR(j) * LIMITATORE) / LIMITATORE
          'ALFA
          If (Abs(YY(i)) < 0.001) Then
            AA(j) = 90: If (j > 1) Then AA(j) = Sgn(AA(j - 1)) * 90
          Else
            AA(j) = Atn(XX(i) / YY(i)) * 180 / PG
          End If
          'BETA
          BB(j) = -90 - AA(j) - TT(i)
          While BB(j) >= 90: BB(j) = BB(j) - 180: Wend
          While BB(j) <= -90: BB(j) = BB(j) + 180: Wend
          If BB(j) >= 89.99 Then BB(j) = -89.99
          If BB(j) <= -89.99 Then BB(j) = -89.99
          AA(j) = AA(j) - 180 / NumeroDenti
        Next i
        For i = UBound(RR) To PuntoSeparazioneCAD_ROTORE + 1 Step -1
          j = UBound(RR) - i + PuntoSeparazioneCAD_ROTORE + 1
          'RAGGIO
          RR(j) = Sqr(XX(i) ^ 2 + YY(i) ^ 2)
          'FORMATTO A CIFRE DOPO LA VIRGOLA
          RR(j) = Int(RR(j) * LIMITATORE) / LIMITATORE
          'ALFA
          If (Abs(YY(i)) < 0.001) Then
            AA(j) = 90: If (i < UBound(RR)) Then AA(j) = Sgn(AA(j + 1)) * 90
          Else
            AA(j) = Atn(XX(i) / YY(i)) * 180 / PG
          End If
          'BETA
          BB(j) = -90 - AA(j) - TT(i)
          While BB(j) >= 90: BB(j) = BB(j) - 180: Wend
          While BB(j) <= -90: BB(j) = BB(j) + 180: Wend
          If BB(j) >= 89.99 Then BB(j) = 89.99
          If BB(j) <= -89.99 Then BB(j) = 89.99
          AA(j) = AA(j) + 180 / NumeroDenti
        Next i
      End If
      '
'JAECKLIN
ElseIf (TIPO = "JAECKLIN") Then
      'RICAVO IL NUMERO DEI PUNTI
      Do While Not EOF(1)
        'LEGGO LA PRIMA RIGA E LA PULISCO
        Line Input #1, stmp: stmp = UCase$(Trim$(stmp))
        j = InStr(stmp, "MDI/")
        If (j > 0) Then
          j = j + Len("MDI/")
          i = InStr(stmp, ",")
          stmp = Mid(stmp, j, i - j)
          stmp = Trim$(stmp)
          Np = val(stmp)
          Exit Do
        End If
      Loop
      'DIMENSIONO I VETTORI
      ReDim ii(Np): ReDim XX(Np): ReDim YY(Np): ReDim TT(Np)
      'LETTURA DELLE COORDINATE
      For i = 1 To Np
        Input #1, YY(i), XX(i), ftmp1, NY, NX, ftmp1, ftmp1
        TT(i) = Atn(NY / NX) * 180 / PG + 90
        ii(i) = i
        'RICERCA DEL RAGGIO MASSIMO E MINIMO
        RR(0) = Sqr(XX(i) ^ 2 + YY(i) ^ 2)
        'FORMATTO A CIFRE DOPO LA VIRGOLA
        RR(0) = Int(RR(0) * LIMITATORE) / LIMITATORE
        If RR(0) > Rmax Then Rmax = RR(0): iRmax = i
        If RR(0) < Rmin Then Rmin = RR(0): iRmin = i
        If YY(i) < Ymin Then Ymin = YY(i)
      Next i
      'SALVATAGGIO DEL PUNTO DI SEPARAZIONE
      PuntoSeparazioneCAD_ROTORE = iRmin
      'CONVERSIONE DA XYT A RAB
      GoSub CONVERSIONE_XYT_RAB
      '
'SCORPATH
ElseIf (TIPO = "SCORPATH") Then
      'LETTURA DEL PUNTO DI SEPARAZIONE: NECESSARIO PER ELGI
      'PuntoSeparazioneCAD_ROTORE = InputBox("punto di separazione", 0) 'da gestire
      PuntoSeparazioneCAD_ROTORE = LEP("PUNTO_SEPARAZIONE_CAD")
      i = 0
      Do While Not EOF(1)
        i = i + 1
        'LEGGO LA PRIMA RIGA E LA PULISCO
        Line Input #1, stmp: stmp = UCase$(Trim$(stmp))
        If (Len(stmp) > 0) Then
          'ALLOCO LE VARIABILI
          ReDim Preserve ii(i)
          ReDim Preserve XX(i): ReDim Preserve YY(i): ReDim Preserve TT(i)
          'INDICE
          ii(i) = i
          'COORDINATA X
          j = InStr(stmp, " ")
          XX(i) = val(Left$(stmp, j - 1))
          stmp = Trim$(Right$(stmp, Len(stmp) - j))
          'COORDINATA Y
          j = InStr(stmp, " ")
          YY(i) = val(Left$(stmp, j - 1))
          stmp = Trim$(Right$(stmp, Len(stmp) - j))
          'ANGOLO DI PRESSIONE
          TT(i) = val(Trim$(stmp))
          'RICERCA DEL RAGGIO MASSIMO E MINIMO
          RR(0) = Sqr(XX(i) ^ 2 + YY(i) ^ 2)
          'FORMATTO A CIFRE DOPO LA VIRGOLA
          RR(0) = Int(RR(0) * LIMITATORE) / LIMITATORE
          If RR(0) > Rmax Then Rmax = RR(0): iRmax = i
          If RR(0) < Rmin Then Rmin = RR(0): iRmin = i
          If YY(i) < Ymin Then Ymin = YY(i)
        Else
          Exit Do
        End If
      Loop
      ReDim RR(UBound(XX)): ReDim AA(UBound(XX)): ReDim BB(UBound(XX))
      stmp = ""
      stmp = stmp & "IF FLUTE SECTION THEN --> YES" & Chr(13) & Chr(13)
      stmp = stmp & "IF FULL TEETH SECTION THEN  --> NO" & Chr(13)
      retTIPO = MsgBox(stmp, 36, "SPECIFY ROTOR SECTION FOR COORDINATE CONVERSION")
      If (retTIPO = 6) Then
        'ROTORE FEMMINA VANO SECONDO QUADRANTE
        'VANO XYT --> VANO RAB
        For i = UBound(XX) To PuntoSeparazioneCAD_ROTORE + 1 Step -1
          j = i
          'RAGGIO
          RR(j) = Sqr(XX(i) ^ 2 + YY(i) ^ 2)
          'FORMATTO A CIFRE DOPO LA VIRGOLA
          RR(j) = Int(RR(j) * LIMITATORE) / LIMITATORE
          'ALFA
          If (YY(i) = 0) Then
            AA(j) = -90
          Else
            AA(j) = Atn(XX(i) / YY(i)) * 180 / PG
          End If
          'BETA
          BB(j) = AA(j) + TT(i)
          AA(j) = 90 + AA(j) 'ROTAZIONE DI 90 GRADI
          If (BB(j) > 89.99) And (BB(j) < 90.01) Then BB(j) = 90
          If (BB(j) < 89.999) Then BB(j) = -BB(j) Else BB(j) = 180 - BB(j)
          If AA(j) > 90 Then AA(j) = -(180 - AA(j))
          AA(j) = AA(j) + Rotazione
        Next i
        For i = 1 To PuntoSeparazioneCAD_ROTORE
          j = i
          'RAGGIO
          RR(j) = Sqr(XX(i) ^ 2 + YY(i) ^ 2)
          'FORMATTO A CIFRE DOPO LA VIRGOLA
          RR(j) = Int(RR(j) * LIMITATORE) / LIMITATORE
          'ALFA
          If (YY(i) = 0) Then
            AA(j) = 90
          Else
            AA(j) = Atn(XX(i) / YY(i)) * 180 / PG
          End If
          'BETA
          BB(j) = AA(j) + TT(i)
          AA(j) = -90 + AA(j) 'ROTAZIONE DI 90 GRADI
          If (BB(j) > 89.99) And (BB(j) < 90.01) Then BB(j) = 90
          If (BB(j) < 89.999) Then BB(j) = -BB(j) Else BB(j) = 180 - BB(j)
          AA(j) = AA(j) + Rotazione
        Next i
      Else
        'ROTORE MASCHIO PIENO PRIMO QUADRANTE
        'PIENO XYT --> VANO RAB
        For i = UBound(XX) To PuntoSeparazioneCAD_ROTORE + 1 Step -1
          j = i - PuntoSeparazioneCAD_ROTORE
          j = UBound(XX) - j + 1
          'RAGGIO
          RR(j) = Sqr(XX(i) ^ 2 + YY(i) ^ 2)
          'FORMATTO A CIFRE DOPO LA VIRGOLA
          RR(j) = Int(RR(j) * LIMITATORE) / LIMITATORE
          'ALFA
          If (YY(i) = 0) Then
            AA(j) = 90
          Else
            AA(j) = Atn(XX(i) / YY(i)) * 180 / PG
          End If
          'BETA
          BB(j) = AA(j) + TT(i)
          AA(j) = AA(j) + 45 'ROTAZIONE DI 45 GRADI
          If AA(j) > 90 Then AA(j) = AA(j) - 90
          If (BB(j) > 89.99) And (BB(j) < 90.01) Then BB(j) = 90
          If (BB(j) < 89.999) Then BB(j) = -BB(j) Else BB(j) = 180 - BB(j)
          If (AA(j) < 0) And (BB(j) > 89.999) Then BB(j) = -180 + BB(j)
          AA(j) = AA(j) + Rotazione
        Next i
        For i = 1 To PuntoSeparazioneCAD_ROTORE
          j = UBound(XX) - PuntoSeparazioneCAD_ROTORE + i
          j = UBound(XX) - j + 1
          'RAGGIO
          RR(j) = Sqr(XX(i) ^ 2 + YY(i) ^ 2)
          'FORMATTO A CIFRE DOPO LA VIRGOLA
          RR(j) = Int(RR(j) * LIMITATORE) / LIMITATORE
          'ALFA
          If (YY(i) = 0) Then
            AA(j) = -90
          Else
            AA(j) = Atn(XX(i) / YY(i)) * 180 / PG
          End If
          'BETA
          BB(j) = AA(j) + TT(i)
          AA(j) = AA(j) + 45 'ROTAZIONE DI 45 GRADI
          If (BB(j) > 89.99) And (BB(j) < 90.01) Then BB(j) = 90
          If (BB(j) < 89.999) Then BB(j) = -BB(j) Else BB(j) = 180 - BB(j)
          BB(j) = BB(j) - 180
          'If (AA(j) > 0) Then AA(j) = AA(j) - 180: If (BB(j) < -90) Then BB(j) = BB(j) + 180
          AA(j) = AA(j) + Rotazione
        Next i
      End If
      '
'I V R T DEL SOLO FIANCO 1 (LEISTRIZ 1)
ElseIf (TIPO = "L3_ASP") Then
      While Not EOF(1)
        ReDim Preserve ii(i)
        ReDim Preserve XX(i): ReDim Preserve YY(i): ReDim Preserve TT(i)
        Input #1, stmp
        Call SPLITTA_STRINGA(stmp, kk, cln1, cln2, cln3, cln4)
        ii(i) = cln1: XX(i) = cln2: YY(i) = cln3: TT(i) = cln4
        'RICERCA DEL RAGGIO MASSIMO E MINIMO
        RR(0) = YY(i)
        'FORMATTO A CIFRE DOPO LA VIRGOLA
        RR(0) = Int(RR(0) * LIMITATORE) / LIMITATORE
        If (RR(0) > Rmax) Then Rmax = RR(0): iRmax = i
        If (RR(0) < Rmin) Then Rmin = RR(0): iRmin = i
        If (YY(i) < Ymin) Then Ymin = YY(i)
        i = i + 1
      Wend
      'CONVERSIONE VRT--> RAB
      nFIANCO1 = i - 1
      ReDim RR(2 * nFIANCO1 - 1): ReDim AA(2 * nFIANCO1 - 1): ReDim BB(2 * nFIANCO1 - 1)
      'FIANCO 1
      For j = nFIANCO1 To 1 Step -1
        i = nFIANCO1 - j + 1
        RR(i) = YY(j)
        AA(i) = XX(j) * 360 / PASSO
        BB(i) = Tan(TT(j) * PG / 180)
        BB(i) = -Atn(BB(i) * 2 * YY(j) * PG / PASSO) * 180 / PG
      Next j
      'FIANCO 2
      For j = 2 To nFIANCO1
        i = nFIANCO1 + j - 1
        RR(i) = YY(j)
        AA(i) = -XX(j) * 360 / PASSO
        BB(i) = Tan(TT(j) * PG / 180)
        BB(i) = Atn(BB(i) * 2 * YY(j) * PG / PASSO) * 180 / PG
      Next j
      'SEPARAZIONE
      PuntoSeparazioneCAD_ROTORE = iRmin
      'CONVERSIONE RAB --> XYT
      ReDim XX(UBound(RR)): ReDim YY(UBound(RR)): ReDim TT(UBound(RR))
      For i = 1 To UBound(RR)
        XX(i) = RR(i) * Sin(AA(i) / 180 * PG)
        YY(i) = RR(i) * Cos(AA(i) / 180 * PG)
        TT(i) = -90 - AA(i) - BB(i)
        If (YY(i) < Ymin) Then Ymin = YY(i)
      Next i
      '
'I V R T DEL SOLO FIANCO 1 (LEISTRIZ 2)
ElseIf (TIPO = "L3_LSP") Then
      While Not EOF(1)
        ReDim Preserve ii(i)
        ReDim Preserve XX(i): ReDim Preserve YY(i): ReDim Preserve TT(i)
        Input #1, stmp
        Call SPLITTA_STRINGA(stmp, kk, cln1, cln2, cln3, cln4)
        ii(i) = cln1: XX(i) = cln2: YY(i) = cln3: TT(i) = cln4
        Select Case ii(i)
          '
          Case 24, 25
            '
          Case 26
            XX(i) = XX(i) - 0.1
            'RICERCA DEL RAGGIO MASSIMO E MINIMO
            RR(0) = YY(i)
            'FORMATTO A CIFRE DOPO LA VIRGOLA
            RR(0) = Int(RR(0) * LIMITATORE) / LIMITATORE
            If (RR(0) > Rmax) Then Rmax = RR(0): iRmax = i
            If (RR(0) < Rmin) Then Rmin = RR(0): iRmin = i
            If (YY(i) < Ymin) Then Ymin = YY(i)
            i = i + 1
            '
          Case Else
            'RICERCA DEL RAGGIO MASSIMO E MINIMO
            RR(0) = YY(i)
            'FORMATTO A CIFRE DOPO LA VIRGOLA
            RR(0) = Int(RR(0) * LIMITATORE) / LIMITATORE
            If (RR(0) > Rmax) Then Rmax = RR(0): iRmax = i
            If (RR(0) < Rmin) Then Rmin = RR(0): iRmin = i
            If (YY(i) < Ymin) Then Ymin = YY(i)
            i = i + 1
            '
        End Select
      Wend
      'CONVERSIONE VRT--> RAB
      nFIANCO1 = i - 1
      ReDim RR(2 * nFIANCO1 - 1): ReDim AA(2 * nFIANCO1 - 1): ReDim BB(2 * nFIANCO1 - 1)
      'FIANCO 1
      For j = nFIANCO1 To 1 Step -1
        i = nFIANCO1 - j + 1
        RR(i) = YY(j)
        AA(i) = XX(j) * 360 / PASSO
        BB(i) = Tan(TT(j) * PG / 180)
        BB(i) = -Atn(BB(i) * 2 * YY(j) * PG / PASSO) * 180 / PG
      Next j
      'FIANCO 2
      For j = 2 To nFIANCO1
        i = nFIANCO1 + j - 1
        RR(i) = YY(j)
        AA(i) = -XX(j) * 360 / PASSO
        BB(i) = Tan(TT(j) * PG / 180)
        BB(i) = Atn(BB(i) * 2 * YY(j) * PG / PASSO) * 180 / PG
      Next j
      'SEPARAZIONE
      PuntoSeparazioneCAD_ROTORE = iRmin
      'CONVERSIONE RAB --> XYT
      ReDim XX(UBound(RR)): ReDim YY(UBound(RR)): ReDim TT(UBound(RR))
      For i = 1 To UBound(RR)
        XX(i) = RR(i) * Sin(AA(i) / 180 * PG)
        YY(i) = RR(i) * Cos(AA(i) / 180 * PG)
        TT(i) = -90 - AA(i) - BB(i)
        If (YY(i) < Ymin) Then Ymin = YY(i)
      Next i
      '
'I V R T DEL SOLO FIANCO 1 (LEISTRIZ 5)
ElseIf (TIPO = "L4_ASP") Then
      While Not EOF(1)
        ReDim Preserve ii(i)
        ReDim Preserve XX(i): ReDim Preserve YY(i): ReDim Preserve TT(i)
        Input #1, stmp
        Call SPLITTA_STRINGA(stmp, kk, cln1, cln2, cln3, cln4)
        ii(i) = cln1: XX(i) = cln2: YY(i) = cln3: TT(i) = cln4
        Select Case ii(i)
          '
          Case 52, 53 'DA VERIFICARE SE SONO SEMPRE GLI STESSI INDICI PER TUTTI I PROFILI SVG071113
            '
          Case Else
            'RICERCA DEL RAGGIO MASSIMO E MINIMO
            RR(0) = YY(i)
            'FORMATTO A CIFRE DOPO LA VIRGOLA
            RR(0) = Int(RR(0) * LIMITATORE) / LIMITATORE
            If (RR(0) > Rmax) Then Rmax = RR(0): iRmax = i
            If (RR(0) < Rmin) Then Rmin = RR(0): iRmin = i
            If (YY(i) < Ymin) Then Ymin = YY(i)
            i = i + 1
            '
        End Select
      Wend
      'CONVERSIONE VRT--> RAB
      nFIANCO1 = i - 1
      ReDim RR(2 * nFIANCO1 - 1): ReDim AA(2 * nFIANCO1 - 1): ReDim BB(2 * nFIANCO1 - 1)
      'FIANCO 1
      For j = nFIANCO1 To 1 Step -1
        i = nFIANCO1 - j + 1
        RR(i) = YY(j)
        AA(i) = XX(j) * 360 / PASSO
        BB(i) = Tan(TT(j) * PG / 180)
        BB(i) = -Atn(BB(i) * 2 * YY(j) * PG / PASSO) * 180 / PG
      Next j
      'FIANCO 2
      For j = 2 To nFIANCO1
        i = nFIANCO1 + j - 1
        RR(i) = YY(j)
        AA(i) = -XX(j) * 360 / PASSO
        BB(i) = Tan(TT(j) * PG / 180)
        BB(i) = Atn(BB(i) * 2 * YY(j) * PG / PASSO) * 180 / PG
      Next j
      'SEPARAZIONE
      PuntoSeparazioneCAD_ROTORE = iRmin
      'CONVERSIONE RAB --> XYT
      ReDim XX(UBound(RR)): ReDim YY(UBound(RR)): ReDim TT(UBound(RR))
      For i = 1 To UBound(RR)
        XX(i) = RR(i) * Sin(AA(i) / 180 * PG)
        YY(i) = RR(i) * Cos(AA(i) / 180 * PG)
        TT(i) = -90 - AA(i) - BB(i)
        If (YY(i) < Ymin) Then Ymin = YY(i)
      Next i
      '
'I V R T DEL SOLO FIANCO 1 (LEISTRIZ 3)
'I V R T DEL SOLO FIANCO 1 (LEISTRIZ 4)
ElseIf (TIPO = "L2_LSP") Or (TIPO = "L2_ASP") Then
      'PUNTO1 = 67: PUNTO2 = 68   PER VITI LATERALI LSP
      'PUNTO1 = 104: PUNTO2 = 105 PER VITI CENTRALI ASP
      '
      'RICERCA DEI VALORI DI PUNTO1 E PUNTO2
      PUNTO1 = 0: PUNTO2 = 0: i = 1
      Do
        ReDim Preserve ii(i)
        ReDim Preserve XX(i): ReDim Preserve YY(i): ReDim Preserve TT(i)
        Input #1, stmp
        Call SPLITTA_STRINGA(stmp, kk, cln1, cln2, cln3, cln4)
        ii(i) = cln1: XX(i) = cln2: YY(i) = cln3: TT(i) = cln4
        If (XX(i) = XX(i - 1)) And (YY(i) = YY(i - 1)) Then
          PUNTO1 = i - 1: PUNTO2 = i: Exit Do
        End If
        i = i + 1
      Loop While Not EOF(1)
      'CHIUSURA DEL FILE
      Close #1
      'LETTURA COORDINATE DAL FILE TEORICO DEI PUNTI
      'NUOVA APERTURA PER AGGIUNGERE I PUNTI
      Open NomeProfiloCAD For Input As #1
      i = 1
      Rmax = -1E+308: Rmin = 1E+308: Ymin = 1E+308
      ReDim RR(i)
      While Not EOF(1)
        ReDim Preserve ii(i)
        ReDim Preserve XX(i): ReDim Preserve YY(i): ReDim Preserve TT(i)
        Input #1, stmp
        Call SPLITTA_STRINGA(stmp, kk, cln1, cln2, cln3, cln4)
        ii(i) = cln1: XX(i) = cln2: YY(i) = cln3: TT(i) = cln4
        Select Case ii(i)
          '
          Case Is < PUNTO1
            'RICERCA DEL RAGGIO MASSIMO E MINIMO
            RR(0) = YY(i)
            'FORMATTO A CIFRE DOPO LA VIRGOLA
            RR(0) = Int(RR(0) * LIMITATORE) / LIMITATORE
            If (RR(0) > Rmax) Then Rmax = RR(0): iRmax = i
            If (RR(0) < Rmin) Then Rmin = RR(0): iRmin = i
            If (YY(i) < Ymin) Then Ymin = YY(i)
            i = i + 1
            '
          Case PUNTO1
            'MEMORIZZAZIONE DEL PUNTO1
            XX1 = XX(i): YY1 = YY(i): TT1 = TT(i)
            i = i + 1
            '
          Case PUNTO2
            'MEMORIZZAZIONE DEL PUNTO2
            XX2 = XX(i): YY2 = YY(i): TT2 = TT(i)
            'CALCOLO DEL DELTA
            If (TT2 < TT1) Then
              Delta = (TT2 - TT1 + 360) / PUNTI
            Else
              Delta = (TT2 - TT1) / PUNTI
            End If
            'INSERIMENTO DI 20 PUNTI NELLA LISTA
            For j = 1 To PUNTI - 1
              ReDim Preserve XX(i): ReDim Preserve YY(i): ReDim Preserve TT(i)
              XX(i) = XX1
              YY(i) = YY1
              TT(i) = TT1 + Delta * j
              If (TT(i) >= 360) Then TT(i) = TT(i) - 360
              i = i + 1
            Next j
            'SCRITTURA DEL PUNTO2 PER CONTINUARE
            ReDim Preserve XX(i): ReDim Preserve YY(i): ReDim Preserve TT(i)
            XX(i) = XX2: YY(i) = YY2: TT(i) = TT2
            i = i + 1
            '
          Case Is > PUNTO2
            'RICERCA DEL RAGGIO MASSIMO E MINIMO
            RR(0) = YY(i)
            'FORMATTO A CIFRE DOPO LA VIRGOLA
            RR(0) = Int(RR(0) * LIMITATORE) / LIMITATORE
            If (RR(0) > Rmax) Then Rmax = RR(0): iRmax = i
            If (RR(0) < Rmin) Then Rmin = RR(0): iRmin = i
            If (YY(i) < Ymin) Then Ymin = YY(i)
            i = i + 1
            '
        End Select
      Wend
      nFIANCO1 = i - 1
      'CONVERSIONE VRT--> RAB
      ReDim RR(2 * nFIANCO1 - 1): ReDim AA(2 * nFIANCO1 - 1): ReDim BB(2 * nFIANCO1 - 1)
      'FIANCO 1
      For j = nFIANCO1 To 1 Step -1
        i = nFIANCO1 - j + 1
        RR(i) = YY(j)
        AA(i) = XX(j) * 360 / PASSO
        BB(i) = Tan(TT(j) * PG / 180)
        BB(i) = -Atn(BB(i) * 2 * YY(j) * PG / PASSO) * 180 / PG
      Next j
      'FIANCO 2
      For j = 2 To nFIANCO1
        i = nFIANCO1 + j - 1
        RR(i) = YY(j)
        AA(i) = -XX(j) * 360 / PASSO
        BB(i) = Tan(TT(j) * PG / 180)
        BB(i) = Atn(BB(i) * 2 * YY(j) * PG / PASSO) * 180 / PG
      Next j
      'SEPARAZIONE
      PuntoSeparazioneCAD_ROTORE = iRmin
      'CONVERSIONE RAB --> XYT
      ReDim XX(UBound(RR)): ReDim YY(UBound(RR)): ReDim TT(UBound(RR))
      For i = 1 To UBound(RR)
        XX(i) = RR(i) * Sin(AA(i) / 180 * PG)
        YY(i) = RR(i) * Cos(AA(i) / 180 * PG)
        TT(i) = -90 - AA(i) - BB(i)
        If (YY(i) < Ymin) Then Ymin = YY(i)
      Next i
      '
    Else
      'CHIUSURA DEL FILE
      Close #1
      'SEGNALAZIONE UTENTE
      stmp = "Please SELECT a file format" & Chr(13) & Chr(13)
      StopRegieEvents
      MsgBox stmp, vbCritical, "FILE FORMAT ERROR"
      ResumeRegieEvents
      Exit Sub
    End If
    '
  Close #1
  '
  'INFORMAZIONI UTENTE
  stmp = ""
  stmp = stmp & "Dmax = " & frmt(Rmax * 2, CIFRE) & " Rmax = " & frmt(Rmax, CIFRE) & " iRmax = " & Format$(iRmax, "####0") & Chr(13)
  stmp = stmp & "Dmin = " & frmt(Rmin * 2, CIFRE) & " Rmin = " & frmt(Rmin, CIFRE) & " iRmin = " & Format$(iRmin, "####0") & Chr(13)
  stmp = stmp & "Rmax-Rmin = " & frmt(Rmax - Rmin, CIFRE) & Chr(13)
  'MsgBox stmp
  '
  'SALVATAGGIO INFORMAZIONI
  i = WritePrivateProfileString("CONVERSIONE", "ROTAZIONE", Rotazione, RADICE & "\INFO.INI")
  i = WritePrivateProfileString("CONVERSIONE", "TIPO", TIPO, RADICE & "\INFO.INI")
  i = WritePrivateProfileString("CONVERSIONE", "Rmax", frmt(Rmax, CIFRE), RADICE & "\INFO.INI")
  i = WritePrivateProfileString("CONVERSIONE", "Rmin", frmt(Rmin, CIFRE), RADICE & "\INFO.INI")
  i = WritePrivateProfileString("CONVERSIONE", "PuntoSeparazioneCAD_ROTORE", Format$(PuntoSeparazioneCAD_ROTORE, "####0"), RADICE & "\INFO.INI")
  i = WritePrivateProfileString("CONVERSIONE", "ProfiloCAD_ROTORE", COORDINATE, RADICE & "\INFO.INI")
  '
  'AGGIORNAMENTO DEL CONTENUTO DEL FILE RAB
  Open PercorsoLavoro & "\" & PROFILORAB For Output As #1
    For i = 1 To UBound(RR)
      stmp = ""
      stmp1 = Left$(frmt0(Abs(RR(i)), CIFRE) & String(15, " "), 15) & " "
      If (RR(i) >= 0) Then
        If (InStr(stmp1, "+") > 0) Then
          stmp = stmp & " " & stmp1
        Else
          stmp = stmp & "+" & stmp1
        End If
      Else
        stmp = stmp & "-" & stmp1
      End If
      stmp1 = Left$(frmt0(Abs(AA(i)), CIFRE) & String(15, " "), 15) & " "
      If (AA(i) >= 0) Then
        If (InStr(stmp1, "+") > 0) Then
          stmp = stmp & " " & stmp1
        Else
          stmp = stmp & "+" & stmp1
        End If
      Else
        stmp = stmp & "-" & stmp1
      End If
      stmp1 = Left$(frmt0(Abs(BB(i)), CIFRE) & String(15, " "), 15) & " "
      If (BB(i) >= 0) Then
        If (InStr(stmp1, "+") > 0) Then
          stmp = stmp & " " & stmp1
        Else
          stmp = stmp & "+" & stmp1
        End If
      Else
        stmp = stmp & "-" & stmp1
      End If
      Print #1, stmp
    Next i
  Close #1
  'SALVATAGGIO DEL ROTORE COMPLETO
  If Dir$(PercorsoLavoro & "\" & "RIFERIMENTO.RAB") <> "" Then Kill PercorsoLavoro & "\" & "RIFERIMENTO.RAB"
  FileCopy PercorsoLavoro & "\" & PROFILORAB, PercorsoLavoro & "\" & "RIFERIMENTO.RAB"
  '
Exit Sub

CONVERSIONE_XYT_RAB:
  'CONVERSIONE XYT ----> RAB
  ReDim RR(UBound(XX)):  ReDim AA(UBound(XX)):  ReDim BB(UBound(XX))
  For i = 1 To PuntoSeparazioneCAD_ROTORE
    'RAGGIO
    RR(i) = Sqr(XX(i) ^ 2 + YY(i) ^ 2)
    'FORMATTO A CIFRE DOPO LA VIRGOLA
    RR(i) = Int(RR(i) * LIMITATORE) / LIMITATORE
    'ALFA
    If (YY(i) = 0) Then
      AA(i) = 90: If i > 1 Then AA(i) = Sgn(AA(i - 1)) * 90
    Else
      AA(i) = Atn(XX(i) / YY(i)) * 180 / PG
    End If
    'BETA
    BB(i) = -90 - AA(i) - TT(i)
    While BB(i) >= 90: BB(i) = BB(i) - 180: Wend
    While BB(i) <= -90: BB(i) = BB(i) + 180: Wend
    If BB(i) >= 89.99 Then BB(i) = -89.99
    If BB(i) <= -89.99 Then BB(i) = -89.99
    'DA ESEGUIRE DOPO IL CALCOLO DI BETA
    AA(i) = AA(i) + Rotazione
  Next i
  For i = UBound(RR) To PuntoSeparazioneCAD_ROTORE + 1 Step -1
    'RAGGIO
    RR(i) = Sqr(XX(i) ^ 2 + YY(i) ^ 2)
    'FORMATTO A CIFRE DOPO LA VIRGOLA
    RR(i) = Int(RR(i) * LIMITATORE) / LIMITATORE
    'ALFA
    If (YY(i) = 0) Then
      AA(i) = 90: If i < UBound(RR) Then AA(i) = Sgn(AA(i + 1)) * 90
    Else
      AA(i) = Atn(XX(i) / YY(i)) * 180 / PG
    End If
    'BETA
    BB(i) = -90 - AA(i) - TT(i)
    While BB(i) >= 90: BB(i) = BB(i) - 180: Wend
    While BB(i) <= -90: BB(i) = BB(i) + 180: Wend
    If (BB(i) >= 89.99) Then BB(i) = 89.99
    If (BB(i) <= -89.99) Then BB(i) = 89.99
    If (AA(i) = 0) And (BB(i) > 85) Then BB(i) = 89.99
    'DA ESEGUIRE DOPO IL CALCOLO DI BETA
    AA(i) = AA(i) + Rotazione
  Next i
  '
Return

errCONVERSIONE_XYT_RAB:
  If FreeFile > 0 Then Close
  MsgBox "ERROR: " & Error(Err) & "  " & i & " " & stmp, 48, "SUB: CONVERSIONE_XYT_RAB"
  Resume Next
  'Exit Sub

End Sub

Sub ImpostaPrinter(Carattere As String, Altezza As Integer)
'
Dim stmp As String
'
On Error GoTo ErrImpostaPrinter
  '
  'stmp = ""
  'stmp = stmp & "  " & Format$(Printer.FontName)
  'stmp = stmp & "  " & Format$(Printer.Font.size)
  'stmp = stmp & "  " & Format$(Printer.Font.Bold)
  'MsgBox stmp, 48, "Current settings"
  '
  Printer.Font.Size = Altezza
  Printer.FontName = Carattere
  Printer.Font.Bold = False
  Printer.Font.Size = Altezza
  Printer.FontName = Carattere
  Printer.Font.Bold = False
  Printer.Font.Size = Altezza
  Printer.FontName = Carattere
  Printer.Font.Bold = False
  Printer.Font.Size = Altezza
  '
  'stmp = ""
  'stmp = stmp & "  " & Format$(Printer.FontName)
  'stmp = stmp & "  " & Format$(Printer.Font.size)
  'stmp = stmp & "  " & Format$(Printer.Font.Bold)
  'MsgBox stmp, 48, "Actual changed settings"
  '
Exit Sub

ErrImpostaPrinter:
  If FreeFile > 0 Then Close
  MsgBox Error(Err), 48, "SUB: ImpostaPrinter"
  Resume Next

End Sub

Sub StampaChkG4_INTESTAZIONE(X1 As Single, Y1 As Single, X2 As Single)
'
Dim TITOLO      As String
Dim TESTO       As String
Dim StrData     As String
Dim StrOra      As String
Dim stmp        As String
Dim iRG         As Single
Dim iCl         As Single
Dim i           As Integer
'
On Error Resume Next
  '
  iRG = Printer.TextHeight("A")
  iCl = Printer.TextWidth("A")
  'Intestazione della pagina
  StrData = " Date: " & Format$(Now, "dd/mm/yy")
  StrOra = " Time: " & Format$(Now, "hh:mm:ss")
  TITOLO = LEGGI_NomePezzo("ROTORI_ESTERNI")
  TITOLO = CHK0.INTESTAZIONE.Caption & " [" & TITOLO & "] " & StrData & StrOra
  Printer.CurrentX = X1 + iCl
  Printer.CurrentY = Y1 + iRG
  Printer.Print TITOLO
  '
  stmp = CHK0.fraVANI.Caption
  If (Len(stmp) > 0) Then
    Printer.CurrentX = X2 - Printer.TextWidth(stmp) - iCl
    Printer.CurrentY = Y1
    Printer.Print stmp
  End If
  For i = 0 To 3
    If CHK0.optVANI(i).Value Then
      stmp = CHK0.optVANI(i).Caption
      Exit For
    End If
  Next i
  If (Len(stmp) > 0) Then
    Printer.CurrentX = X2 - Printer.TextWidth(stmp) - iCl
    Printer.CurrentY = Y1 + iRG
    Printer.Print stmp
  End If
  '
  'Contorno intestazione
  Printer.Line (X1, Y1)-(X2, Y1 + 1), QBColor(0), B
  '
End Sub

Sub StampaCOR_G4(CorZ() As Double, corY() As Double, TITOLO As String, NPM1 As Integer, NPM2 As Integer)
'
Dim i         As Integer
Dim j         As Integer
Dim k         As Integer
Dim W         As Integer
Dim rsp       As Integer
Dim NRP       As Integer
Dim stmp      As String
Dim X         As Double

Dim MLeft     As Single
Dim MRight    As Single
Dim MBottom   As Single
Dim MTop      As Single
Dim Box0W     As Single
Dim Box0H     As Single
'
'Parametri per la stampa di linee
Dim X1    As Single
Dim Y1    As Single
Dim X2    As Single
Dim Y2    As Single
'
'Area di stampa dei grafici
Dim Box1L   As Single
Dim Box1T   As Single
Dim Box1W   As Single
Dim Box1H   As Single
'
Dim iRG     As Single
Dim iCl     As Single
'
Dim riga    As String

Dim nCol    As Integer

Dim TabX0   As Double
Dim TabX1   As Double
Dim TabX2   As Double

Dim TopLista    As Double
Dim Left1Lista  As Double
Dim Left2Lista  As Double
Dim Left3Lista  As Double

On Error Resume Next
  '
  Printer.ScaleMode = 7
  Call ImpostaPrinter("Arial", 8)
  MTop = 1: MBottom = 1 '2
  MLeft = 2: MRight = 1
  Box0W = Printer.ScaleWidth - (MLeft + MRight) '17
  Box0H = Printer.ScaleHeight - (MTop + MBottom) '27
  iRG = Printer.TextHeight("A")
  iCl = Printer.TextWidth("A")
  Box1L = 0: Box1T = 1
  Box1W = 17: Box1H = 16
  '
  TopLista = MTop + 1 + 2 * iRG
  Left1Lista = MLeft
  Left2Lista = Left1Lista + Int(Box0W / 3 * 100 + 0.5) / 100  '+ 6.5
  Left3Lista = Left2Lista + Int(Box0W / 3 * 100 + 0.5) / 100  '+ 6.5
  '
  TabX0 = iCl
  TabX1 = 7 * iCl
  TabX2 = TabX1 + 10 * iCl
  '
  ' Squadratura della pagina + Intestazione
  X1 = MLeft: Y1 = MTop
  X2 = MLeft + Box0W: Y2 = MTop + Box0H
  Call StampaChkG4_INTESTAZIONE(X1, Y1, X2)
  '
  Y1 = TopLista - iRG
  Printer.CurrentX = X1 + iCl
  Printer.CurrentY = Y1
  Printer.Print TITOLO  'MESS$(4)
  '
  '** Fianco 1 ************************************************************
  nCol = 1
  Y1 = TopLista
  '** INTESTAZIONE F1
  Printer.CurrentX = X1 + TabX0
  Printer.CurrentY = Y1
  Printer.Print "F1"
  '** INTESTAZIONE F1 - INDICE DEL PUNTO
  Y1 = TopLista + iRG
  Printer.CurrentX = X1 + TabX0
  Printer.CurrentY = Y1
  riga = "nr. "
  Printer.Print riga
  '** INTESTAZIONE F1 - CORREZIONE IN X
  Printer.CurrentX = X1 + TabX1
  Printer.CurrentY = Y1
  riga = "X"
  Printer.Print riga
  '** INTESTAZIONE F1 - CORREZIONE IN Y
  Printer.CurrentX = X1 + TabX2
  Printer.CurrentY = Y1
  riga = "Y"
  Printer.Print riga
  '
  i = 1
  For k = 1 To NPM1
    Y1 = Y1 + iRG
    '** F1 - INDICE DEL PUNTO
    Printer.CurrentX = X1 + TabX0
    Printer.CurrentY = Y1
    riga = Format$(k - 1, "#0")
    Printer.Print riga
    '** F1 - CORREZIONE IN X
    Printer.CurrentX = X1 + TabX1
    Printer.CurrentY = Y1
    X = CorZ(i)
    If X >= 0 Then riga = "+" Else riga = "-"
    riga = riga & frmt0(Abs(X), 4)
    Printer.Print riga
    '** F1 - CORREZIONE IN Y
    Printer.CurrentX = X1 + TabX2
    Printer.CurrentY = Y1
    X = corY(i)
    If X >= 0 Then riga = "+" Else riga = "-"
    riga = riga & frmt0(Abs(X), 4)
    Printer.Print riga
    i = i + 1
    'j = j + 1
    'F1 - CONTROLLO SPAZIO OCCUPATO SU PAGINA
    If Printer.CurrentY > (MTop + Box0H - iRG) Then
      nCol = nCol + 1
      If nCol / 4 = Int(nCol / 4) Then
        Printer.NewPage
        ' Squadratura della pagina
        X1 = MLeft: Y1 = MTop
        X2 = MLeft + Box0W: Y2 = MTop + Box0H
        Call StampaChkG4_INTESTAZIONE(MLeft, MTop, X2)
        X1 = Left1Lista: Y1 = TopLista
      ElseIf nCol / 2 = Int(nCol / 2) Then
        X1 = Left2Lista: Y1 = TopLista
      ElseIf nCol / 3 = Int(nCol / 3) Then
        X1 = Left3Lista: Y1 = TopLista
      End If
      '** INTESTAZIONE F1
      Printer.CurrentX = X1 + TabX0
      Printer.CurrentY = Y1
      Printer.Print "F1"
      '** INTESTAZIONE F1 - INDICE DEL PUNTO
      Y1 = TopLista + iRG
      Printer.CurrentX = X1 + TabX0
      Printer.CurrentY = Y1
      riga = "nr. "
      Printer.Print riga
      '** INTESTAZIONE F1 - CORREZIONE IN X
      Printer.CurrentX = X1 + TabX1
      Printer.CurrentY = Y1
      riga = "X"
      Printer.Print riga
      '** INTESTAZIONE F1 - CORREZIONE IN Y
      Printer.CurrentX = X1 + TabX2
      Printer.CurrentY = Y1
      riga = "Y"
      Printer.Print riga
    End If
  Next k
  '
  '** Fianco 2 ************************************************************
  Y1 = Y1 + 2 * iRG
  '** INTESTAZIONE F2
  Printer.CurrentX = X1 + TabX0
  Printer.CurrentY = Y1
  Printer.Print "F2"
  '** INTESTAZIONE F2 - INDICE DEL PUNTO
  Y1 = Y1 + iRG
  Printer.CurrentX = X1 + TabX0
  Printer.CurrentY = Y1
  riga = "nr. "
  Printer.Print riga
  '** INTESTAZIONE F2 - CORREZIONE IN X
  Printer.CurrentX = X1 + TabX1
  Printer.CurrentY = Y1
  riga = "X"
  Printer.Print riga
  '** INTESTAZIONE F2 - CORREZIONE IN Y
  Printer.CurrentX = X1 + TabX2
  Printer.CurrentY = Y1
  riga = "Y"
  Printer.Print riga
  
  For W = 1 To NPM2
    Y1 = Y1 + iRG
    '** F2 - INDICE DEL PUNTO
    Printer.CurrentX = X1 + TabX0
    Printer.CurrentY = Y1
    NRP = W + NPM1
    riga = Format$(NRP - 1, "#0")
    Printer.Print riga
    '** F2 - CORREZIONE IN X
    Printer.CurrentX = X1 + TabX1
    Printer.CurrentY = Y1
    X = CorZ(i)
    If X >= 0 Then riga = "+" Else riga = "-"
    riga = riga & frmt0(Abs(X), 4)
    Printer.Print riga
    '** F2 - CORREZIONE IN Y
    Printer.CurrentX = X1 + TabX2
    Printer.CurrentY = Y1
    X = corY(i)
    If X >= 0 Then riga = "+" Else riga = "-"
    riga = riga & frmt0(Abs(X), 4)
    Printer.Print riga
    i = i + 1
    'j = j + 1
    'F2 - CONTROLLO SPAZIO OCCUPATO SU PAGINA
    If Printer.CurrentY > (MTop + Box0H - iRG) Then
      nCol = nCol + 1
      If nCol / 4 = Int(nCol / 4) Then
        Printer.NewPage
        ' Squadratura della pagina
        X1 = MLeft: Y1 = MTop
        X2 = MLeft + Box0W: Y2 = MTop + Box0H
        'Printer.Line (X1, Y1)-(X2, Y2), QBColor(0), B
        ' Intestazione + linea sotto intestazione
        Call StampaChkG4_INTESTAZIONE(MLeft, MTop, X2)
        X1 = Left1Lista: Y1 = TopLista
      ElseIf nCol / 2 = Int(nCol / 2) Then
        X1 = Left2Lista: Y1 = TopLista
      ElseIf nCol / 3 = Int(nCol / 3) Then
        X1 = Left3Lista: Y1 = TopLista
      End If
      '** INTESTAZIONE F2
      Printer.CurrentX = X1 + TabX0
      Printer.CurrentY = Y1
      Printer.Print "F2"
      '** INTESTAZIONE F2 - INDICE DEL PUNTO
      Y1 = TopLista + iRG
      Printer.CurrentX = X1 + TabX0
      Printer.CurrentY = Y1
      riga = "nr. "
      Printer.Print riga
      '** INTESTAZIONE F2 - CORREZIONE IN X
      Printer.CurrentX = X1 + TabX1
      Printer.CurrentY = Y1
      riga = "X"
      Printer.Print riga
      '** INTESTAZIONE F2 - CORREZIONE IN Y
      Printer.CurrentX = X1 + TabX2
      Printer.CurrentY = Y1
      riga = "Y"
      Printer.Print riga
    End If
  Next W
  
  Printer.EndDoc

End Sub

Sub CALCOLO_CORRETTORI_MOLA()
'
Dim i     As Integer
Dim j     As Integer
Dim rsp   As Integer
Dim MLeft As Single
Dim MTop  As Single
Dim Box0W As Single
Dim Box0H As Single
'
'Parametri per la stampa di linee
Dim X1    As Single
Dim Y1    As Single
Dim X2    As Single
Dim Y2    As Single
'
'Area di stampa dei grafici
Dim Box1L As Single
Dim Box1T As Single
Dim Box1W As Single
Dim Box1H As Single
'
Dim iRG   As Single
Dim iCl   As Single
'
Dim riga  As String
'
Dim TGMG(nMAXpntG4 + 1)    As Double
Dim TGMR(nMAXpntG4 + 1)    As Double
Dim ER(nMAXpntG4 + 1)      As Double
Dim cZ(nMAXpntG4 + 1)      As Double
Dim cy(nMAXpntG4 + 1)      As Double
Dim CorZ(nMAXpntG4 + 1)    As Double
Dim corY(nMAXpntG4 + 1)    As Double
Dim RINF(nMAXpntG4 + 1)    As Double
Dim RSUP(nMAXpntG4 + 1)    As Double
'
Dim MOLANEGTOL(nMAXpntG4 + 1) As Double
Dim MOLAPOSTOL(nMAXpntG4 + 1) As Double
'
Dim X         As Double
Dim NPun      As Integer
Dim NCOR      As Integer
Dim PFL       As Double
Dim INDPREC   As Integer
Dim INDTAB    As Integer
Dim DELTAPUN  As Integer
Dim Np        As Integer
Dim ERRR      As Double
Dim k         As Integer
Dim W         As Integer
Dim NRP       As Integer
Dim stmp      As String
Dim Abilitazioni() As Integer
'
Dim PathFileSPF         As String
Dim PathFileTMP         As String
Dim cXX                 As Double
Dim cYY                 As Double
Dim ret                 As Integer
'
On Error GoTo errCALCOLO_CORRETTORI_MOLA
  '
  Dim COORDINATE         As String
  Dim RADICE             As String
  Dim PERCORSO           As String
  Dim PERCORSO_CONTROLLI As String
  Dim PERCORSO_PROFILI   As String
  Dim sequenza           As String
  Dim npt                As Integer
  Dim TY_ROT             As String
  Dim PASSO              As Double
  Dim CONTROLLORE        As String
  Dim DM_ROT             As String
  Dim INVERSIONE         As Integer
  Dim CIFRE              As Integer
  Dim RIDUZIONE          As Double
  Dim ISTERESI           As Double
  '
  Call LEGGI_INFO_ROTORI(PERCORSO, COORDINATE, RADICE, PERCORSO_CONTROLLI, PERCORSO_PROFILI, sequenza, npt, TY_ROT, PASSO, CONTROLLORE, DM_ROT, INVERSIONE)
  '
  Dim NP1   As Integer
  Dim NP2   As Integer
  Dim NPM1  As Integer
  Dim NPM2  As Integer
  '
  NPM1 = GetInfo("CREAZIONE", "NpM_F1", RADICE & "\INFO.INI")
  NPM2 = GetInfo("CREAZIONE", "NpM_F2", RADICE & "\INFO.INI")
  NP1 = GetInfo("CREAZIONE", "NpC_F1", RADICE & "\INFO.INI")
  NP2 = GetInfo("CREAZIONE", "NpC_F2", RADICE & "\INFO.INI")
  '
  NPun = NP1 + NP2
  NCOR = NPM1 + NPM2
  '
  CIFRE = val(GetInfo("Configurazione", "DECIMALI", Path_LAVORAZIONE_INI))
  If (CIFRE <= 0) Then CIFRE = 4
  '
  RIDUZIONE = val(GetInfo("Configurazione", "RIDUZIONE", Path_LAVORAZIONE_INI))
  If (RIDUZIONE <= 0) Then RIDUZIONE = 1
  '
  ISTERESI = val(GetInfo("Configurazione", "ISTERESI", Path_LAVORAZIONE_INI))
  ISTERESI = Abs(ISTERESI)
  If (ISTERESI <= 0) Then ISTERESI = 0
  '
  PFL = 0 'CORREZIONE SPESSORE
  '
  'FASCIA DI TOLLERANZA
  Open RADICE & "\" & "PRFTOL.DAT" For Input As #1
    MOLANEGTOL(1) = 0: MOLAPOSTOL(1) = 0
    For i = 2 To npt + 1
      Input #1, MOLANEGTOL(i), MOLAPOSTOL(i)
    Next i
    MOLANEGTOL(npt + 2) = 0: MOLAPOSTOL(npt + 2) = 0
  Close #1
  '
  'CREAZIONE DELLA LISTA DEGLI ERRORI
  If (TY_ROT = "AXIAL") And (CONTROLLORE <> "INTERNAL") Then
    'CREAZIONE DI RISROT SE NON ESISTE
    If (Dir$(RADICE & "\RISROT") = "") Then
      Open RADICE & "\RISROT" For Output As #1
      If (NP2 >= NP1) Then
        For i = 1 To NP2
          Print #1, "+0.0000,+0.0000"
        Next i
      Else
        For i = 1 To NP1
          Print #1, "+0.0000,+0.0000"
        Next i
      End If
      Close #1
    End If
    'LETTURA ERRORI MASCHIO
    Open RADICE & "\RISROT" For Input As #1
      If (NP2 >= NP1) Then
        For i = 1 To NP2
          Input #1, RINF(i), RSUP(i)
        Next i
      Else
        For i = 1 To NP1
          Input #1, RINF(i), RSUP(i)
        Next i
      End If
    Close #1
    'CREAZIONE DELLA LISTA
    Open RADICE & "\ERRORI.MOD" For Output As #2
      For i = 1 To NP1
        riga = ""
        X = RSUP(i)
        If (X >= 0) Then riga = riga & "+" Else riga = riga & "-"
        riga = riga & frmt0(Abs(X), CIFRE)
        Print #2, riga
      Next i
      For i = NP2 To 1 Step -1
        riga = ""
        X = RINF(i)
        If (X >= 0) Then riga = riga & "+" Else riga = riga & "-"
        riga = riga & frmt0(Abs(X), CIFRE)
        Print #2, riga
      Next i
    Close #2
  End If
  If (TY_ROT = "FRONT") Or (CONTROLLORE = "INTERNAL") Then
    'CREAZIONE DI EPMROT
    If (Dir$(RADICE & "\EPMROT") = "") Then
      Open RADICE & "\EPMROT" For Output As #1
      For i = 1 To NPun + 1
        Print #1, "+0.0000,+0.0000"
      Next i
      Close #1
    End If
    'LETTURA ERRORI FEMMINA
    Open RADICE & "\EPMROT" For Input As #1
      For i = 1 To NPun + 1
        Input #1, RINF(i), X
      Next
    Close #1
    'CREAZIONE FILE DI APPOGGIO
    Open RADICE & "\ERRORI.MOD" For Output As #1
      For i = 1 To NP1
        riga = ""
        X = RINF(i)
        If (X >= 0) Then riga = riga & "+" Else riga = riga & "-"
        riga = riga & frmt0(Abs(X), CIFRE)
        Print #1, riga
      Next i
      For i = NPun To NP1 + 1 Step -1
        riga = ""
        X = RINF(i)
        If (X >= 0) Then riga = riga & "+" Else riga = riga & "-"
        riga = riga & frmt0(Abs(X), CIFRE)
        Print #1, riga
      Next i
      riga = ""
      X = RINF(NPun + 1)
      If (X >= 0) Then riga = riga & "+" Else riga = riga & "-"
      riga = riga & frmt0(Abs(X), CIFRE)
      Print #1, riga
    Close #1
  End If
  '
  'CREAZIONE DEL FILE ABILITAZIONI SE NON LO TROVO
  If (Dir$(RADICE & "\ABILITA.COR") = "") Then
    Open RADICE & "\ABILITA.COR" For Output As #5
      For i = 1 To NCOR
        Print #5, "1"
      Next i
    Close #5
  End If
  'LETTURA CORRETTORI ABILITATI
  Open RADICE & "\ABILITA.COR" For Input As #5
    For i = 1 To NCOR
      ReDim Preserve Abilitazioni(i)
      Input #5, Abilitazioni(i)
    Next i
  Close #5
  '
  'CALCOLO CORRETTORI
  Open RADICE & "\ERRORI.MOD" For Input As #1
    Open RADICE & "\TGMROT.ALL" For Input As #2
      Open RADICE & "\PUNCHK" For Input As #3
        '**********************************************************************
        ' Primi punti non controllati: Errore = Errore primo punto controllato
        '**********************************************************************
        i = 1: INDPREC = 0: Np = 1
        Input #1, ER(i): ER(i) = ER(i) - PFL
        Input #3, INDTAB
        DELTAPUN = INDTAB - INDPREC
        For j = 1 To DELTAPUN
          Input #2, TGMG(j): TGMG(j) = Abs(TGMG(j))
          TGMR(j) = Abs((90 - TGMG(j)) * PG / 180)
        Next j
        For j = 1 To DELTAPUN
          ERRR = ER(i)
          ERRR = ERRR + (MOLAPOSTOL(Np) - MOLANEGTOL(Np)) / 2
          If (Abs(ERRR) <= ISTERESI) Then ERRR = 0
          corY(Np) = ERRR * Cos(TGMR(j)) * Abilitazioni(Np) * RIDUZIONE
          CorZ(Np) = ERRR * Sin(TGMR(j)) * Abilitazioni(Np) * RIDUZIONE
          'CorZ(Np) = ERRR
          Np = Np + 1
        Next j
        '**********************************************************************
        ' LOOP punti controllati
        '**********************************************************************
        INDPREC = INDTAB
        For i = 2 To NPun
          Input #1, ER(i): ER(i) = ER(i) - PFL
          Input #3, INDTAB
          DELTAPUN = INDTAB - INDPREC
          For j = 1 To DELTAPUN
            Input #2, TGMG(j): TGMG(j) = Abs(TGMG(j))
            TGMR(j) = Abs((90 - TGMG(j)) * PG / 180)
          Next j
          For j = 1 To DELTAPUN
            ERRR = (ER(i) - ER(i - 1)) / (DELTAPUN / j) + ER(i - 1)
            ERRR = ERRR + (MOLAPOSTOL(Np) - MOLANEGTOL(Np)) / 2
            If (Abs(ERRR) <= ISTERESI) Then ERRR = 0
            corY(Np) = ERRR * Cos(TGMR(j)) * Abilitazioni(Np) * RIDUZIONE
            CorZ(Np) = ERRR * Sin(TGMR(j)) * Abilitazioni(Np) * RIDUZIONE
            'CorZ(Np) = ERRR
            Np = Np + 1
          Next j
          INDPREC = INDTAB
        Next i
        '**********************************************************************
        'Ultimi punti non controllati: Errore = Errore ultimo punto controllato
        '**********************************************************************
        i = NPun: DELTAPUN = NCOR - Np + 1
        For j = 1 To DELTAPUN
          Input #2, TGMG(j): TGMG(j) = Abs(TGMG(j))
          TGMR(j) = Abs((90 - TGMG(j)) * PG / 180)
        Next j
        For j = 1 To DELTAPUN
          ERRR = ER(i)
          ERRR = ERRR + (MOLAPOSTOL(Np) - MOLANEGTOL(Np)) / 2
          If (Abs(ERRR) <= ISTERESI) Then ERRR = 0
          corY(Np) = ERRR * Cos(TGMR(j)) * Abilitazioni(Np) * RIDUZIONE
          CorZ(Np) = ERRR * Sin(TGMR(j)) * Abilitazioni(Np) * RIDUZIONE
          'CorZ(Np) = ERRR
          Np = Np + 1
        Next j
      Close #3
    Close #2
  Close #1
  Np = i - 1
  'CREAZIONE DELLE CORREZIONI PROPOSTE
  Open RADICE & "\CORPROP" For Output As #1
    For i = 1 To NCOR
      riga = ""
      X = CorZ(i)
      If (X >= 0) Then riga = riga & "+" Else riga = riga & "-"
      riga = riga & frmt0(Abs(X), CIFRE)
      riga = riga & ","
      X = corY(i)
      If (X >= 0) Then riga = riga & "+" Else riga = riga & "-"
      riga = riga & frmt0(Abs(X), CIFRE)
      Print #1, riga
    Next i
  Close #1
  'RICHIESTA DI STAMPA DELLE CORREZIONI PROPOSTE
  rsp = MsgBox(Chr$(13) & Chr$(13) & "PRINT EVALUATED CORRECTORS?", 36 + 256, "POINTS CORRECTOR EVALUATING PROCEDURE")
  If (rsp = 6) Then Call StampaCOR_G4(CorZ, corY, "EVALUATED CORRECTORS: CORPROP", NPM1, NPM2)
  'RICHIESTA DI MODIFICA DEI CORRETTORI CORRENTI
  rsp = MsgBox(Chr$(13) & Chr$(13) & "ADD CORRECTORS?", vbYesNo, "POINTS CORRECTOR EVALUATING PROCEDURE")
  If (rsp = vbNo) Then
    'RICHIEDO SE VUOLE MANDARE LE ULTIME CORREZIONI SALVATE
    rsp = MsgBox(Chr$(13) & Chr$(13) & "RESTORE LAST CORRECTORS?", vbYesNo, "POINTS CORRECTOR EVALUATING PROCEDURE")
    If (rsp = vbYes) Then
      'RECUPERO CORROT.BAK
      If (Dir$(RADICE & "\CORROT.BAK") <> "") Then
        'RECUPERO DELLE CORREZIONI
        If Dir$(RADICE & "\CORROT") <> "" Then Kill RADICE & "\CORROT"
        FileCopy RADICE & "\CORROT.BAK", RADICE & "\CORROT"
        'LETTURA DELLE CORREZIONI CORRENTI
        Open RADICE & "\CORROT" For Input As #1
          i = 1
          While Not EOF(1)
            Line Input #1, riga
            k = InStr(2, riga, "+")
            If k = 0 Then k = InStr(2, riga, "-")
            cZ(i) = val(Trim$(Left$(riga, k - 1)))
            cy(i) = val(Trim$(Right$(riga, Len(riga) - k + 1)))
            i = i + 1
          Wend
        Close #1
        'RICHIESTA DI STAMPA DELLE CORREZIONI CORRENTI
        rsp = MsgBox(Chr$(13) & Chr$(13) & "PRINT RECOVERED CORRECTORS?", 36 + 256, "POINTS CORRECTOR EVALUATING PROCEDURE")
        If (rsp = 6) Then Call StampaCOR_G4(cZ, cy, "RECOVERED CORRECTORS: CORROT", NPM1, NPM2)
        'RICHIESTA DI TRASMISSINOE DELLE CORREZIONI CORRENTI
        rsp = MsgBox(Chr$(13) & Chr$(13) & "SEND RECOVERED CORRECTORS?", 36, "POINTS CORRECTOR EVALUATING PROCEDURE")
        If (rsp = 6) Then
          GoSub TRASMISSIONE
          WRITE_DIALOG "SELECT WHEEL GROUP PARAMETER TO LOAD VALUES ONTO CNC!!!"
        End If
      Else
        StopRegieEvents
        MsgBox "NO CORRECTORS TO RECOVER!!!", vbCritical, "POINTS CORRECTOR EVALUATING PROCEDURE"
        ResumeRegieEvents
      End If
    Else
      WRITE_DIALOG "ONLY CORPROP CREATED!!"
    End If
  Else
    'RICEZIONE DELLE CORREZIONI CORRENTI
    'LETTURA DELL'INDICE DI LAVORAZIONE
    INDICE_LAVORAZIONE_SINGOLA = val(GetInfo("Configurazione", "INDICE_LAVORAZIONE_SINGOLA", PathFILEINI))
    If (INDICE_LAVORAZIONE_SINGOLA <= 0) Then INDICE_LAVORAZIONE_SINGOLA = 1
    'PERCORSO DEL SOTTOPROGRAMMA SULL'HARD DISK ALLINEATO AL CN
    PathFileSPF = g_chOemPATH & "\PARAMETRI" & Format$(INDICE_LAVORAZIONE_SINGOLA, "##0") & "\OEM1_MOLA71.SPF"
    'PERCORSO DEL SOTTOPROGRAMMA INI DI RIFERIMENTO
    PathFileTMP = RADICE & "\OEM1_MOLA71.TMP"
    'CANCELLAZIONE DELL'ULTIMO CREATO
    If (Dir$(PathFileTMP) <> "") Then Kill PathFileTMP
    'CREAZIONE DEL NUOVO SOTTOPROGRAMMA INI DI RIFERIMENTO
    Call SCRIVI_SEZIONE_CORREZIONI(PathFileSPF, PathFileTMP, "CORR_MOLA")
    'CREAZIONE DELLE NUOVE CORREZIONI
    Open RADICE & "\CORROT" For Output As #1
      For i = 1 To NCOR
        cXX = val(GetInfo("CORR_MOLA", "CORX[0," & i - 1 & "]", PathFileTMP))
        cYY = val(GetInfo("CORR_MOLA", "CORY[0," & i - 1 & "]", PathFileTMP))
        riga = ""
        If (cXX >= 0) Then riga = riga & "+" Else riga = riga & "-"
        riga = riga & frmt0(Abs(cXX), CIFRE)
        If (cYY >= 0) Then riga = riga & "+" Else riga = riga & "-"
        riga = riga & frmt0(Abs(cYY), CIFRE)
        Print #1, riga
      Next i
    Close #1
    'SALVATAGGIO DEL CORROT RICEVUTO DALLA MACCHINA
    If (Dir$(RADICE & "\CORROT.BAK") <> "") Then Kill RADICE & "\CORROT.BAK"
    FileCopy RADICE & "\CORROT", RADICE & "\CORROT.BAK"
    'LETTURA DELLE CORREZIONI CORRENTI
    Open RADICE & "\CORROT" For Input As #1
      i = 1
      While Not EOF(1)
        Line Input #1, riga
        k = InStr(2, riga, "+")
        If k = 0 Then k = InStr(2, riga, "-")
        cZ(i) = val(Trim$(Left$(riga, k - 1)))
        cy(i) = val(Trim$(Right$(riga, Len(riga) - k + 1)))
        i = i + 1
      Wend
    Close #1
    'ADDIZIONE DELLE CORREZIONI PROPOSTE E CREAZIONE DEL NUOVO CORROT
    Open RADICE & "\CORROT" For Output As #1
      For i = 1 To NCOR
        cZ(i) = CorZ(i) + cZ(i): cy(i) = corY(i) + cy(i)
        riga = ""
        X = cZ(i)
        If X >= 0 Then riga = riga & "+" Else riga = riga & "-"
        riga = riga & frmt0(Abs(X), CIFRE)
        X = cy(i)
        If X >= 0 Then riga = riga & "+" Else riga = riga & "-"
        riga = riga & frmt0(Abs(X), CIFRE)
        Print #1, riga
      Next i
    Close #1
    'RICHIESTA DI STAMPA DELLE CORREZIONI CORRENTI
    rsp = MsgBox(Chr$(13) & Chr$(13) & "PRINT NEW CORRECTORS?", 36 + 256, "POINTS CORRECTOR EVALUATING PROCEDURE")
    If (rsp = 6) Then Call StampaCOR_G4(cZ, cy, "NEW CORRECTORS: CORROT", NPM1, NPM2)
    'RICHIESTA DI TRASMISSIONE DELLE CORREZIONI CORRENTI
    rsp = MsgBox(Chr$(13) & Chr$(13) & "SEND NEW CORRECTORS?", 36, "POINTS CORRECTOR EVALUATING PROCEDURE")
    If (rsp = 6) Then
      GoSub TRASMISSIONE
      WRITE_DIALOG "SELECT WHEEL GROUP PARAMETER TO LOAD VALUES ONTO CNC!!!"
    End If
  End If
  '
Exit Sub
'
TRASMISSIONE:
  'SE ESISTE UNA CORREZIONE LA CANCELLO
  If (Dir$(g_chOemPATH & "\CORMOL.ROT") <> "") Then Kill g_chOemPATH & "\CORMOL.ROT"
  'LETTURA DI CORROT E CREAZIONE DI CORMOL.ROT
  Open RADICE & "\CORROT" For Input As #1
    i = 0
    While Not EOF(1)
      Line Input #1, riga
      k = InStr(2, riga, "+")
      If k = 0 Then k = InStr(2, riga, "-")
      stmp = Trim$(Left$(riga, k - 1))
      ret = WritePrivateProfileString("CorrezioniPuntiMola", "CORX[0," & i & "]", stmp, g_chOemPATH & "\CORMOL.ROT")
      stmp = Trim$(Right$(riga, Len(riga) - k + 1))
      ret = WritePrivateProfileString("CorrezioniPuntiMola", "CORY[0," & i & "]", stmp, g_chOemPATH & "\CORMOL.ROT")
      i = i + 1
    Wend
  Close #1
  'SEGNALAZIONE PER CARICAMENTO IN MEMORIA CNC
  ret = WritePrivateProfileString("ROTORI_ESTERNI", "NuoviCorrettori", "Y", Path_LAVORAZIONE_INI)
  '
Return

errCALCOLO_CORRETTORI_MOLA:
  'If FreeFile > 0 Then Close
  MsgBox Error$(Err), 48, "SUB: CALCOLO_CORRETTORI_MOLA"
  Resume Next
  Exit Sub

End Sub

'PROGRAMMA PER LA SCRITTURA DELLA SEZIONE IN UN FILE INI
'ESEGUE LA CONVERSIONE DI UN FILE SPF IN UN FILE INI
Sub SCRIVI_SEZIONE_CORREZIONI(PathFileSPF As String, PathFileTMP As String, SEZIONE As String)
'
Dim i     As Integer
Dim k     As Integer
Dim stmp  As String
'
On Error GoTo errSCRIVI_SEZIONE_CORREZIONI
  '
  'APRE IL SOTTOPROGRAMMA LETTO DAL CNC
  If (Dir$(PathFileSPF) <> "") Then
    Open PathFileSPF For Input As #1
      'APRE IL FILE INI TEMPORANEO PER LA MODIFICA DEI DATI DEL SOTTOPROGRAMMA
      Open PathFileTMP For Append As #2
        Print #2, "[" & SEZIONE & "]"
        Do While Not EOF(1)
          'LEGGO LA RIGA DAL SOTTOPROGRAMMA LETTO DAL CNC
          Line Input #1, stmp
          'RIMUOVO GLI SPAZI
          stmp = UCase$(Trim$(stmp))
          'CONSIDERO SOLO LE RIGHE NON NULLE
          If Len(stmp) > 0 Then
            'CONTROLLO SE LA RIGA CONTIENE UN COMMENTO
            i = InStr(stmp, ";")
            If (i <= 0) Then
              'CONTROLLA SE E' ARRIVATO ALLA FINE DEL SOTTOPROGRAMMA
              i = InStr(stmp, "M17")
              If (i <= 0) Then
                Print #2, Trim$(stmp)
              Else
                'SONO ARRIVATO ALLA FINE DEL SOTTOPROGRAMMA (M17)
                Exit Do
              End If
            Else
              'LA RIGA CONTIENE UN COMMENTO
              'CONTROLLO VIENE DOPO UNA ASSEGNAZIONE
              k = InStr(stmp, "=")
              If (k > 0) And (k < i) Then
                Print #2, Trim$(Mid$(stmp, 1, i - 1))
              End If
            End If
          End If
        Loop
        'RIGA DI SEPARAZIONE
        Print #2, ""
      Close #2
    Close #1
  Else
    StopRegieEvents
    MsgBox PathFileSPF & ": " & Chr(13) & Error$(53), 48, "SUB: SCRIVI_SEZIONE_CORREZIONI"
    ResumeRegieEvents
  End If
  '
Exit Sub

errSCRIVI_SEZIONE_CORREZIONI:
  MsgBox Error$(Err), 48, "SUB: SCRIVI_SEZIONE_CORREZIONI"
  If (FreeFile > 0) Then Close
  Exit Sub

End Sub

Sub CALCOLO_MOLA_ROTORI_TEORICA(RAGGIO_ESTERNO_MOLA As Double, Np As Integer, Rmin As Single, Rmax As Single, ByRef R() As Single, ByRef A() As Single, ByRef B() As Single, ByRef XX0() As Single, ByRef YY0() As Single, ByRef TG0() As Single, ByVal PASSO As Single, ByVal ELICA As Single, ByRef DP As Single, ByRef NPF1 As Integer)
'
' input (espliciti):
' RAGGIO_ESTERNO_MOLA
' Np            (numero punti rotore)
' Rmin, Rmax    (raggi min e max rotore)
' R(), A(), B() (coordinate profilo RAB)
'
' OUTPUT (ESPLICITI):
' XX0, YY0, TG0 <-- coordinate mola senza correzioni
' DP DIAMETRO PRIMITIVO
'
Dim XX() As Double
Dim YY() As Double
Dim TG() As Double
Dim XD() As Double
Dim YD() As Double
Dim Xc() As Double
Dim YC() As Double
Dim R0() As Double
Dim L()  As Double
'
Dim stmp  As String
Dim riga  As String
Dim i     As Integer
Dim j     As Integer
'
Dim Tp              As Double
Dim INC             As Double
Dim Rpri            As Double
Dim Ent             As Double
Dim DRM             As Double
Dim RMPD            As Double
Dim Rayon           As Double
Dim ALFA            As Double
Dim Beta            As Double
Dim SuRep           As Double
Dim Ymax            As Double
Dim Ymin            As Double
Dim iYmax           As Integer
Dim iYmin           As Integer
Dim ALTEZZA_DENTE   As Double
Dim Hmax1           As Double
Dim Hmax2           As Double
'
Dim X As Double
Dim Y As Double
'
Dim gama2 As Double
'
Dim SensALF As Integer
Dim Alfa1   As Double
Dim beta1   As Double
Dim Rayon1  As Double
  
Dim Xml As Double
Dim Yml As Double
'
Dim dtmp As Double
Dim Gama As Double
'
Dim T0 As Double
Dim FI0 As Double
'
Dim RQC As Double
'
Dim AA  As Double
Dim BB  As Double
Dim cc  As Double
'
Dim RAD As Double
'
Dim T1 As Double
Dim T2 As Double
Dim Ti As Double
'
Dim TSI   As Double
Dim TSIa  As Double
Dim F     As Double
Dim FP    As Double
'
Dim XS As Double
Dim Ys As Double
Dim Zs As Double
'
Dim Ap  As Double
Dim Bp  As Double
Dim Cp  As Double
Dim ArgSeno  As Double
Const Rotazione = 0
'
On Error GoTo errCALCOLO_MOLA_ROTORI_TEORICA
  '
  ReDim ParMolaROT.XX(0)
  ReDim ParMolaROT.YY(0)
  ReDim ParMolaROT.TT(0)
  '
  ReDim XX(0):  ReDim XX0(0)
  ReDim YY(0):  ReDim YY0(0)
  ReDim TG(0):  ReDim TG0(0)
  ReDim XD(0)
  ReDim YD(0)
  ReDim Xc(0)
  ReDim YC(0)
  ReDim R0(0)
  ReDim L(0)
  
  If (PASSO = 0) Then
    WRITE_DIALOG ("WARNING: LEAD IS = " & PASSO)
    Exit Sub
  End If
  '
  ALTEZZA_DENTE = (Rmax - Rmin)
  '
  ReDim ParMolaROT.XX(Np + 1)
  ReDim ParMolaROT.YY(Np + 1)
  ReDim ParMolaROT.TT(Np + 1)
  '
  ReDim XX(Np + 1):  ReDim XX0(Np + 1)
  ReDim YY(Np + 1):  ReDim YY0(Np + 1)
  ReDim TG(Np + 1):  ReDim TG0(Np + 1)
  ReDim XD(Np + 1)
  ReDim YD(Np + 1)
  ReDim Xc(Np + 1)
  ReDim YC(Np + 1)
  ReDim R0(Np + 1)
  ReDim L(Np + 1)
  '
  INC = (90 - Abs(ELICA)) * PG / 180
  Tp = PASSO / (2 * PG)
  Rpri = Tp * Tan(INC)
  '
  Ent = RAGGIO_ESTERNO_MOLA + Rmin
  DRM = Ent * Rpri
  RMPD = Rpri + Ent
  '
  Ymax = -999999: iYmax = 0
  Ymin = 999999: iYmin = 0
  For i = 1 To Np
    Rayon = R(i): ALFA = (A(i) + Rotazione) * PG / 180: Beta = B(i) * PG / 180
    SuRep = 0
    GoSub 3900
    XX(i) = X: YY(i) = Y: TG(i) = gama2
    XX0(i) = X: YY0(i) = Y: TG0(i) = gama2
    If YY(i) > Ymax Then Ymax = YY(i): iYmax = i
    If YY(i) < Ymin Then Ymin = YY(i): iYmin = i
  Next i
  '
  'ASSEGNAZIONE DEL PUNTO DI SEPARAZIONE
  If (NPF1 <= 0) Then
  'CONTROLLO SE DISPARI
  If (iYmax Mod 2) = 0 Then
  NPF1 = iYmax - 1
  Else
  NPF1 = iYmax
  End If
  End If
  '
  'CALCOLO DEL DIAMETRO PRIMITIVO
  Dim irif    As Integer
  Dim Bmin    As Double
  Dim RrifF1  As Double
  Dim RrifF2  As Double
  '
  Bmin = 99999999
  For i = 1 To NPF1
    If Abs(B(i)) < Bmin Then
      Bmin = Abs(B(i))
      irif = i
    End If
  Next i
  RrifF1 = R(irif)
  '
  Bmin = 99999999
  For i = NPF1 + 1 To Np
    If Abs(B(i)) < Bmin Then
      Bmin = Abs(B(i))
      irif = i
    End If
  Next i
  RrifF2 = R(irif)
  '
  DP = RrifF1 + RrifF2
  '
  'PUNTI DEI TRATTI LINEARI (inizializzo)
  YY(0) = YY(1):  XX(0) = XX(1): TG(0) = TG(1)
  YY(Np + 1) = YY(Np):  XX(Np + 1) = XX(Np): TG(Np + 1) = TG(Np)
  '
  'NORMALIZZAZIONE DELLE TANGENTI SUGLI ESTREMI MOLA (Punti 0 e NP+1)
  If TG(1) < 0 Then
    TG(0) = TG(1)
  Else
    TG(0) = TG(1) + 5 * PG / 180
    If TG(0) > 90 * PG / 180 Then TG(0) = PG / 2
  End If
  If TG(Np) > 0 Then
    TG(Np + 1) = TG(Np)
   Else
    TG(Np + 1) = TG(Np) - 5 * PG / 180
    If TG(Np + 1) < -90 * PG / 180 Then TG(Np + 1) = -PG / 2
  End If
  'Calcolo Altezza Fianco
  Hmax1 = 0
  Hmax2 = 0
  If (TG(0) < 0) And (TG(0) > -75 * PG / 180) Then
      Hmax1 = ALTEZZA_DENTE
  End If
  If (TG(Np + 1) > 0) And (TG(Np + 1) < 75 * PG / 180) Then
      Hmax2 = ALTEZZA_DENTE
  End If
  '
  '**************************
  ' AGGIUNTA PUNTI 0 e NP+1
  '**************************
  If Hmax1 = 0 Then
    XX(0) = XX(1) - 1
    YY(0) = YY(1) + (XX(1) - XX(0)) / Tan(TG(0))
   Else
    YY(0) = Ymax - Hmax1
    XX(0) = XX(1) + (YY(1) - YY(0)) * Tan(TG(0))
  End If
  If Hmax2 = 0 Then
    XX(Np + 1) = XX(Np) + 1
    YY(Np + 1) = YY(Np) + (XX(Np) - XX(Np + 1)) / Tan(TG(Np + 1))
  Else
    YY(Np + 1) = Ymax - Hmax2
    XX(Np + 1) = XX(Np) + (YY(Np) - YY(Np + 1)) * Tan(TG(Np + 1))
  End If
  XX0(0) = XX(0): YY0(0) = YY(0): TG0(0) = TG(0)
  XX0(Np + 1) = XX(Np + 1): YY0(Np + 1) = YY(Np + 1): TG0(Np + 1) = TG(Np + 1)
  '
  'OUTPUT: COORDINATE MOLA (SENZA CORREZIONI MOLA)
  For i = 0 To Np + 1
    ParMolaROT.XX(i) = XX(i)
    ParMolaROT.YY(i) = YY(i)
    ParMolaROT.TT(i) = TG(i)
  Next i
  '
Exit Sub

errCALCOLO_MOLA_ROTORI_TEORICA:
  If (FreeFile > 0) Then Close
  MsgBox "ERROR: " & Error(Err), 48, "SUB: CALCOLO_MOLA_ROTORI_TEORICA"
  Exit Sub
  'Resume Next

3900  ' ************************************  S/P  ******  CALCUL MEULE
      ' Variables  : RAYON, ALFA, BETA
      ' Constantes : ENT, INC, Pas, SUREP
      '***********************************************************************
If (PASSO < 9999) Then
      SensALF = Sgn(ALFA): If SensALF = 0 Then SensALF = 1
      ALFA = ALFA * SensALF: Beta = Beta * SensALF
      If Abs(Beta) > 0.8 Then
        Alfa1 = ALFA + 0.002
        beta1 = Beta - 0.002
        Rayon1 = Rayon * Sin(Beta) / Sin(beta1)
        GoTo 4030
      End If
      Rayon1 = Rayon - 0.1
      beta1 = FnASN(Rayon * Sin(Beta) / Rayon1)
      Alfa1 = ALFA - beta1 + Beta
4030  GoSub 4090
      Xml = X: Yml = Y
4040  Alfa1 = ALFA: beta1 = Beta: Rayon1 = Rayon
      GoSub 4090
      dtmp = Yml - Y: If dtmp = 0 Then dtmp = 1
      gama2 = Gama * Sgn((X - Xml) / dtmp) * SensALF
      X = X * SensALF
      Y = Y - SuRep * Sin(Abs(gama2))
      X = X - SuRep * Cos(gama2) * Sgn(gama2)
      Return
4090  T0 = PG / 2 - Alfa1
      FI0 = PG / 2 - Alfa1 - beta1
      If FI0 > PG Then FI0 = FI0 - 2 * PG
      If FI0 < -PG Then FI0 = FI0 + 2 * PG
      RQC = Rayon1 * Rayon1 * Cos(T0 - FI0)
      AA = RQC * Sin(T0) + (2 * Tp * Tp + DRM) * Sin(FI0)
      BB = (DRM + Tp * Tp) * Cos(FI0) + RQC * Cos(T0)
      cc = 2 * (-DRM * Sin(FI0) - RQC * Sin(T0) + Rayon1 * RMPD * Cos(T0 - FI0))
      RAD = (BB * BB / AA - cc) / AA
      If (RAD < 0) Then
        WRITE_DIALOG "Radicando negativo per R=" & Rayon1
        Exit Sub
        'GoTo 4290
      End If
      RAD = Sqr(RAD)
      T1 = BB / AA + RAD: T2 = BB / AA - RAD
      Ti = T2
      If Abs(T2) > Abs(T1) Then Ti = T1
      If (T0 < 0) Then Ti = T1
      If (Ti >= PG) Then Ti = PG * 0.9
      'CALCOLO ZERO CON METODO APPROSSIMATO TETA --> TS
      TSI = Ti
      j = 1
4270  F = RQC * Sin(T0 + TSI) + DRM * Sin(FI0 + TSI) + Tp * Tp * TSI * Cos(FI0 + TSI) - Rayon1 * RMPD * Cos(T0 - FI0)
      FP = RQC * Cos(T0 + TSI) + DRM * Cos(FI0 + TSI) + Tp * Tp * Cos(FI0 + TSI) - Tp * Tp * TSI * Sin(FI0 + TSI)
4290  TSIa = TSI - F / FP
      j = j + 1
      If Abs(TSIa - TSI) < 0.0002 Then GoTo 4350
      If j = 80 Then GoTo 4350
      TSI = TSIa
      GoTo 4270
4350  Ti = TSIa
      XS = Rayon1 * Cos(T0 + Ti): Ys = Rayon1 * Sin(T0 + Ti): Zs = Tp * Ti
      'Calcolo della linea di contatto in mm
      L(i) = Ti * (180 / PG) * PASSO / 360
      'Memorizzazione Contatto Max
      'If i = 1 Then
      '  ParMolaROT.LMax = Abs(L(i))
      '  ParMolaROT.iLMax = i
      'Else
      '  If ParMolaROT.LMax < Abs(L(i)) Then
      '    ParMolaROT.LMax = Abs(L(i))
      '    ParMolaROT.iLMax = i
      '  End If
      'End If
      'MaxLcontRotori = ParMolaROT.LMax
      'CALCOLO TANGENTE AL PROFILO MOLA IN TETAS
      Ap = Tan(Ti + FI0) / XS: Bp = -1 / XS: Cp = (1 + Ys * Tan(Ti + FI0) / XS) / Tp
      ArgSeno = (Ap / Tan(INC) + Cp) / (Sqr(Ap * Ap + Bp * Bp + Cp * Cp) * (1 / Sin(INC)))
      Gama = PG / 2 - FnASN(ArgSeno): If Gama > PG / 2 Then Gama = PG - Gama
      X = Zs * Sin(INC) + XS * Cos(INC)
      Y = Sqr((XS * Sin(INC) - Zs * Cos(INC)) ^ 2 + (Ys - Ent) ^ 2)
Else
      X = Rayon * Sin(ALFA): Y = Ent - Rayon * Cos(ALFA): gama2 = Beta + ALFA
      L(i) = 0
End If
Return
      
End Sub

Sub CREAZIONE_DATI_ROTORE()
'
Dim Valore              As Double
Dim PERCORSO            As String
Dim RA()                As Single
Dim al()                As Single
Dim BE()                As Single
Dim i                   As Integer
Dim Np                  As Integer
Dim PRINCIPI            As Integer
Dim PASSO               As Single
Dim INCLINAZIONE        As Single
Dim Rotazione           As Single
Dim SPALLA              As String
Dim CORHD               As Single
Dim DIAMETRO_PRIMITIVO  As Single
Dim NPF1                As Integer
Dim RAmax               As Single
Dim RAmin               As Single
Dim XX()                As Single
Dim YY()                As Single
Dim TT()                As Single
Dim TIPO                As String
Dim RADICE              As String
Dim COORDINATE          As String
Dim sequenza            As String
Dim stmp                As String
Dim DM_ROT              As String
Dim NomeROTORE          As String
Dim Senso               As Integer
Dim TIPO_ROTORE         As Integer
Dim INVERSIONE          As Integer
Dim RAGGIO_ESTERNO_MOLA As Double
Dim ret                 As Long
'
On Error Resume Next
  '
  WRITE_DIALOG "CREATE NEW ROTOR GRINDING DATA"
  StopRegieEvents
  ret = MsgBox("CREATE NEW ROTOR GRINDING DATA???", vbQuestion + vbYesNo + vbDefaultButton2, "WARNING")
  ResumeRegieEvents
If (ret = vbYes) Then
  '
  'VERIFICA ABILITAZIONE UTENTE
  Dim ABILITAZIONE  As String
  Dim Utente        As String
  Dim UTENTI()      As String
  '
  i = 0: ReDim UTENTI(i)
  Do
    i = i + 1
    stmp = GetInfo("UTENTI", "UTENTE(" & Format$(i, "#0") & ")", Path_LAVORAZIONE_INI)
    stmp = UCase$(Trim$(stmp))
    If (stmp <> "") Then
      ReDim Preserve UTENTI(i)
      UTENTI(i) = stmp
    Else
      Exit Do
    End If
  Loop
  If (UBound(UTENTI) > 0) Then
    Utente = InputBox("INPUT USERNAME!!!!", "ROTOR DATA", "")
    Utente = UCase$(Trim$(Utente))
    For i = 1 To UBound(UTENTI)
      If (UTENTI(i) = Utente) Then
        ABILITAZIONE = "Y" 'UTENTE ABILITATO
        Exit For
      End If
    Next i
    If (ABILITAZIONE <> "Y") Then
      If (Len(Utente) <= 0) Then Utente = "?????"
      stmp = GetInfo("UTENTI", "MESSAGGIO", Path_LAVORAZIONE_INI)
      WRITE_DIALOG Utente & ": " & stmp
      Exit Sub
    End If
  End If
  '
  'SCRITTURA DEL NUMERO DEI PRINCIPI
  PRINCIPI = Lp("PRINCIPI"): Call SP("R[5]", PRINCIPI, False)
  'SCRITTURA DEL PASSO
  PASSO = Lp("PASSO"): Call SP("R[390]", PASSO, False)
  'LETTURA DELL'INCLINAZIONE
  INCLINAZIONE = Lp("INCLINAZIONE")
  'LETTURA DEL SENSO ELICA
  Senso = Lp("SENSO_ELICA")
  If (Senso = -1) Then
    'ELICA SINISTRA
    INCLINAZIONE = -INCLINAZIONE
  Else
    'ELICA DESTRA
    INCLINAZIONE = INCLINAZIONE
  End If
  Call SP("R[258]", INCLINAZIONE, False)
  'LETTURA DELLA ROTAZIONE
  Rotazione = Lp("ROTAZIONE")
  'LETTURA DELLA CORREZIONE DI ALTEZZA MOLA
  SPALLA = Lp_Str("CORHD")
  'CORREZIONE DI ALTEZZA MOLA
  i = InStr(SPALLA, ";")
  If (i > 0) Then
    CORHD = 0
  Else
    i = InStr(SPALLA, "T")
    If (i > 0) Then
      CORHD = 0
    Else
      CORHD = val(Trim$(SPALLA))
    End If
  End If
  'LETTURA DEL DIAMETRO MEDIO MOLA
  DM_ROT = Lp_Str("DM_ROT")
  'LETTURA DEL PERCORSO DI RIFERIMENTO E COORDINATE RAB CON ROTAZIONE
  PERCORSO = Lp_Str("PERCORSO")
  If (Left$(PERCORSO, 1) = "\") Then
    PERCORSO = Right$(PERCORSO, Len(PERCORSO) - 1)
    Call SP("PERCORSO", PERCORSO, False)
  End If
  PERCORSO = g_chOemPATH & "\" & PERCORSO
  'RICAVO IL NOME DEI PUNTI DEL ROTORE DAL PERCORSO
  COORDINATE = PERCORSO
  If (Len(COORDINATE) > 0) Then
    i = InStr(COORDINATE, "\")
    While (i <> 0)
      j = i
      i = InStr(i + 1, COORDINATE, "\")
    Wend
    'RICAVO IL NOME DEI PUNTI DEL ROTORE
    COORDINATE = Right$(COORDINATE, Len(COORDINATE) - j)
  End If
  'RICAVO IL PERCORSO DI LAVORO
  RADICE = PERCORSO
  i = InStr(RADICE, COORDINATE)
  RADICE = Left$(RADICE, i - 2)
  'RICAVO IL NOME DEL ROTORE
  NomeROTORE = RADICE
  If (Len(NomeROTORE) > 0) Then
    i = InStr(NomeROTORE, "\")
    While (i <> 0)
      j = i
      i = InStr(i + 1, NomeROTORE, "\")
    Wend
    'RICAVO IL NOME DEI PUNTI DEL ROTORE
    NomeROTORE = Right$(NomeROTORE, Len(NomeROTORE) - j)
  End If
  'CONTROLLO ESISTENZA DEL PERCORSO
  If (Dir$(PERCORSO) = "") Then
    If (COORDINATE <> "CREATE") Then Call WRITE_DIALOG(Error(53) & ": " & PERCORSO)
    'CREAZIONE DELLA DESTINAZIONE
    'CONTROLLO SE ESISTE LA PERIFERICA
    Dim riga As String
    stmp = RADICE
    riga = Dir(stmp)
    'SE NON ESISTE LO CREO
    If (Dir$(stmp, 16) = "") Then
      i = InStr(stmp, "\")
      While (i <> 0)
        riga = Left$(stmp, i - 1)
        'SALTO LA PERIFERICA
        If (Right(riga, 1) <> ":") And (Dir$(riga, 16) = "") Then MkDir riga
        'MsgBox Left$(stmp, i - 1)
        i = InStr(i + 1, stmp, "\")
      Wend
      MkDir stmp
    End If
    'Exit Sub
    If (COORDINATE <> "CREATE") Then
      'ACQUISIZIONE PERCORSO DI ORIGINE PROFILO
      Dim SORGENTE As String
      SORGENTE = InputBox("INPUT SOURCE PATH")
      SORGENTE = UCase$(Trim$(SORGENTE))
      If (Len(SORGENTE) <= 0) Then
        Call WRITE_DIALOG("Operation aborted by user!!!")
        Exit Sub
      End If
      If (Dir$(SORGENTE) = "") Then
        Call WRITE_DIALOG(Error(76))
        Exit Sub
      End If
      'CREAZIONE DEL PERCORSO DI DESTINAZIONE DEL PROFILO
      Call FileCopy(SORGENTE, PERCORSO)
    End If
  End If
  'LETTURA DEL TIPO DI ROTORE
  TIPO_ROTORE = Lp("TIPO_ROTORE")
  'LETTURA INVERSIONE FIANCHI
  INVERSIONE = Lp("INVERSIONE")
  'LETTURA DEL TIPO DI CREAZIONE DATI
  TIPO = Lp_Str("TIPO_CREAZIONE"): TIPO = UCase$(Trim$(TIPO))
  If (TIPO = "STANDARD") Then
      If (Dir$(PERCORSO) <> "") Then
        RAmax = 0: RAmin = 999999
        Open PERCORSO For Input As #1
        i = 1
        While Not EOF(1)
          ReDim Preserve RA(i): ReDim Preserve al(i): ReDim Preserve BE(i)
          Input #1, RA(i), al(i), BE(i)
          al(i) = al(i) + Rotazione
          If (RA(i) <= RAmin) Then RAmin = RA(i)
          If (RA(i) >= RAmax) Then RAmax = RA(i)
          i = i + 1
        Wend
        Close #1
        Np = i - 1
      Else
        Call StopRegieEvents
        Call MsgBox(PERCORSO & ": " & Error(53), 48, "WARNING")
        Call ResumeRegieEvents
        Exit Sub
      End If
      'LETTURA DELLA SEQUENZA DEI PUNTI DA NON CONTROLLARE
      sequenza = Lp_Str("SEQUENZA")
      'CREAZIONE DEL PROFILO RAB CON LA ROTAZIONE
      If (Dir$(RADICE & "\PRFROT") <> "") Then Call Kill(RADICE & "\PRFROT")
      If (INVERSIONE = 0) Then
        Open RADICE & "\PRFROT" For Output As #1
        For i = 1 To Np
          stmp = ""
          If (RA(i) > 0) Then
            stmp = stmp & "+" & Left$(frmt0(Abs(RA(i)), 4) & String(15, " "), 15)
          Else
            stmp = stmp & "-" & Left$(frmt0(Abs(RA(i)), 4) & String(15, " "), 15)
          End If
          stmp = stmp & " "
          If (al(i) > 0) Then
            stmp = stmp & "+" & Left$(frmt0(Abs(al(i)), 4) & String(15, " "), 15)
          Else
            stmp = stmp & "-" & Left$(frmt0(Abs(al(i)), 4) & String(15, " "), 15)
          End If
          stmp = stmp & " "
          If (BE(i) > 0) Then
            stmp = stmp & "+" & Left$(frmt0(Abs(BE(i)), 4) & String(15, " "), 15)
          Else
            stmp = stmp & "-" & Left$(frmt0(Abs(BE(i)), 4) & String(15, " "), 15)
          End If
          Print #1, stmp
        Next i
        Close #1
      Else
        Open RADICE & "\PRFROT" For Output As #1
        For i = Np To 1 Step -1
          al(i) = -al(i)
          BE(i) = -BE(i)
          stmp = ""
          If (RA(i) >= 0) Then
            stmp = stmp & "+" & Left$(frmt0(Abs(RA(i)), 4) & String(15, " "), 15) & " "
          Else
            stmp = stmp & "-" & Left$(frmt0(Abs(RA(i)), 4) & String(15, " "), 15) & " "
          End If
          If (al(i) >= 0) Then
            stmp = stmp & "+" & Left$(frmt0(Abs(al(i)), 4) & String(15, " "), 15) & " "
          Else
            stmp = stmp & "-" & Left$(frmt0(Abs(al(i)), 4) & String(15, " "), 15) & " "
          End If
          If (BE(i) >= 0) Then
            stmp = stmp & "+" & Left$(frmt0(Abs(BE(i)), 4) & String(15, " "), 15) & " "
          Else
            stmp = stmp & "-" & Left$(frmt0(Abs(BE(i)), 4) & String(15, " "), 15) & " "
          End If
          Print #1, stmp
        Next i
        Close #1
        Open RADICE & "\PRFROT" For Input As #1
        For i = 1 To Np
          Input #1, RA(i), al(i), BE(i)
        Next i
        Close #1
      End If
      'CREAZIONE DEI DATI COMPLETI DI RETTIFICA ROTORI
      Call CREAZIONE_DATI_RETTIFICA_ROTORI(RADICE, NomeROTORE, DM_ROT, PRINCIPI, PASSO, INCLINAZIONE, 2 * RAmin, SPALLA, sequenza, Np, TIPO_ROTORE)
  Else
      'GESTIONE DEL PARAMETRO MODO COME S1000GW-LEISTRITZ
      'TUTTO DA SVILUPPARE PER LA NUOVA MACCHINA SGV081113
      Dim MODO    As Integer
      Dim nrPUNTI As Integer
      Select Case TIPO
        Case "L3_ASP"
        Case "L3_LSP"
        Case "L2_LSP"
        Case "L2_ASP"
        Case "L4_ASP"
        Case Else
      End Select
      'CONVERSIONE DEL FILE CLIENTE A COORDINATE POLARI RAB (PRFROT CONTIENE LA ROTAZIONE)
      Call CONVERSIONE_XYT_RAB(TIPO, RADICE, "PRFROT", COORDINATE, PASSO, INCLINAZIONE, Rotazione)
      If (INVERSIONE = 1) Then
        Open RADICE & "\PRFROT" For Input As #1
        i = 1
        While Not EOF(1)
          ReDim Preserve RA(i): ReDim Preserve al(i): ReDim Preserve BE(i)
          Input #1, RA(i), al(i), BE(i)
          i = i + 1
        Wend
        Close #1
        Np = i - 1
        Open RADICE & "\PRFROT" For Output As #1
        For i = Np To 1 Step -1
          al(i) = -al(i)
          BE(i) = -BE(i)
          stmp = ""
          If (RA(i) >= 0) Then
            stmp = stmp & "+" & Left$(frmt0(Abs(RA(i)), 4) & String(15, " "), 15) & " "
          Else
            stmp = stmp & "-" & Left$(frmt0(Abs(RA(i)), 4) & String(15, " "), 15) & " "
          End If
          If (al(i) >= 0) Then
            stmp = stmp & "+" & Left$(frmt0(Abs(al(i)), 4) & String(15, " "), 15) & " "
          Else
            stmp = stmp & "-" & Left$(frmt0(Abs(al(i)), 4) & String(15, " "), 15) & " "
          End If
          If (BE(i) >= 0) Then
            stmp = stmp & "+" & Left$(frmt0(Abs(BE(i)), 4) & String(15, " "), 15) & " "
          Else
            stmp = stmp & "-" & Left$(frmt0(Abs(BE(i)), 4) & String(15, " "), 15) & " "
          End If
          Print #1, stmp
        Next i
        Close #1
      End If
      'CANCELLO IL FILE DEL PROFILO MOLA
      If (Dir$(RADICE & "\XYT." & DM_ROT) <> "") Then Kill RADICE & "\XYT." & DM_ROT
      'LETTURA DEL RAGGIO INTERNO
      RAmin = val(GetInfo("CONVERSIONE", "Rmin", RADICE & "\INFO.INI"))
      'CALCOLO E VISUALIZZAZIONE
      Call CALCOLO_PROFILO_MOLA_RIDOTTO(RADICE, "PRFROT", val(DM_ROT), val(DM_ROT), CORHD, PASSO, INCLINAZIONE, 2 * RAmin)
      'RIDUZIONE PUNTI
      Call RIDUZIONE_PUNTI_RAB(RADICE, COORDINATE, DM_ROT, MODO, Np)
      'CANCELLO IL FILE DEL PROFILO MOLA
      If (Dir$(RADICE & "\XYT." & DM_ROT) <> "") Then Kill RADICE & "\XYT." & DM_ROT
      'CALCOLO E VISUALIZZAZIONE
      Call CALCOLO_PROFILO_MOLA_RIDOTTO(RADICE, "PRFROT", val(DM_ROT), val(DM_ROT), CORHD, PASSO, INCLINAZIONE, 2 * RAmin)
      'CREAZIONE DEI DATI COMPLETI DI RETTIFICA ROTORI
      If (Np > 400) Then
        StopRegieEvents
        MsgBox Np & " too many points!!!!!", vbCritical, "WARNING"
        ResumeRegieEvents
        Exit Sub
      End If
      Call CREAZIONE_DATI_RETTIFICA_ROTORI(RADICE, NomeROTORE, DM_ROT, PRINCIPI, PASSO, INCLINAZIONE, 2 * RAmin, SPALLA, sequenza, Np, TIPO_ROTORE)
      'LETTURA DEL FILE RAB
      If (Dir$(RADICE & "\PRFROT") <> "") Then
        RAmax = 0: RAmin = 999999
        Open RADICE & "\PRFROT" For Input As #1
        i = 1
        While Not EOF(1)
          ReDim Preserve RA(i): ReDim Preserve al(i): ReDim Preserve BE(i)
          Input #1, RA(i), al(i), BE(i)
          If (RA(i) <= RAmin) Then RAmin = RA(i)
          If (RA(i) >= RAmax) Then RAmax = RA(i)
          i = i + 1
        Wend
        Close #1
        Np = i - 1
      Else
        StopRegieEvents
        MsgBox PERCORSO & "\PRFROT" & ": " & Error(53), 48, "WARNING"
        ResumeRegieEvents
        Exit Sub
      End If
  End If
  'SCRITTURA DEL NUMERO DEI PUNTI
  Call SP("R[147]", Np, False)
  'SCRITTURA DEL DIAMETRO ESTERNO
  Call SP("R[44]", 2 * RAmax, False)
  'SCRITTURA DEL DIAMETRO INTERNO
  Call SP("R[45]", 2 * RAmin, False)
  'CALCOLO DEL PROFILO MOLA
  NPF1 = GetInfo("CONVERSIONE", "NPMF1tmp", RADICE & "\INFO.INI")
  RAGGIO_ESTERNO_MOLA = val(LEP("iDiaMola")) / 2
  Call CALCOLO_MOLA_ROTORI_TEORICA(RAGGIO_ESTERNO_MOLA, Np, RAmin, RAmax, RA(), al(), BE(), XX(), YY(), TT(), PASSO, Abs(INCLINAZIONE), DIAMETRO_PRIMITIVO, NPF1)
  'SCRITTURA DEL DIAMETRO PRIMITIVO
  Call SP("R[103]", DIAMETRO_PRIMITIVO, False)
  'SCRITTURA DEL PUNTO DI SEPARAZIONE
  Call SP("R[43]", NPF1, False)
  'LETTURA DEI PUNTI CORROT
  Dim CorX    As Single
  Dim corY    As Single
  Dim CorXX() As Single
  Dim CorYY() As Single
  Open RADICE & "\CORROT" For Input As #1
    i = 1
    Do While (Not EOF(1)) And (i <= Np + 2)
      Line Input #1, stmp
      If Len(stmp) <= 0 Then Exit Do
      j = 0
      If (j <= 0) Then j = InStr(2, stmp, "+")
      If (j <= 0) Then j = InStr(2, stmp, "-")
      CorX = Left$(stmp, j - 1)
      corY = Mid$(stmp, j)
      'TOLGO LA VIRGOLA SE PRESENTE (CORPROP)
      j = InStr(CorX, ","): If (j > 0) Then CorX = Left$(CorX, j - 1)
      ReDim Preserve CorXX(i)
      ReDim Preserve CorYY(i)
      CorXX(i) = val(CorX)
      CorYY(i) = val(corY)
      i = i + 1
    Loop
  Close #1
  'SCRITTURA DELLA CORREZIONE DI INTERASSE
  stmp = GetInfo("CREAZIONE", "CorrInterasse", RADICE & "\INFO.INI")
  Call SP("R[189]", stmp, False)
  'SCRITTURA DEI CORRETTORI NEL DATABASE
  For j = 0 To Np + 1
    Call SP("CORX[0," & Format$(j, "####0") & "]", frmt(val(CorXX(j + 1)), 4), False)
    Call SP("CORY[0," & Format$(j, "####0") & "]", frmt(val(CorYY(j + 1)), 4), False)
  Next j
  'SCRITTURA DEI PUNTI RAB NEL DATABASE SOLO SE E' DEFINITA LA TABELLA OEM0_CORRETTORI71
  stmp = ""
  For j = 0 To UBound(Tabella1.Pagine)
    If (Tabella1.Pagine(j).Nomegruppo = "OEM0_CORRETTORI71") Then
      stmp = "TROVATO": Exit For
    End If
  Next j
  If (stmp = "TROVATO") Then
    For j = 1 To Np
      Call SP("RA[0," & j & "]", frmt(val(RA(j)), 4), False)
      Call SP("AL[0," & j & "]", frmt(val(al(j)), 4), False)
      Call SP("BE[0," & j & "]", frmt(val(BE(j)), 4), False)
    Next j
    For j = Np + 1 To nMAXpntG4
      Call SP("RA[0," & j & "]", Format$(0, "0"), False)
      Call SP("AL[0," & j & "]", Format$(0, "0"), False)
      Call SP("BE[0," & j & "]", Format$(0, "0"), False)
    Next j
  End If
  'CREAZIONE DEL SOTTOPROGRAMMA OEM0_CORRETTORI71.SPF NEL DIRETTORIO DI LAVORO
  Open RADICE & "\OEM0_CORRETTORI71.SPF" For Output As #1
    Print #1, ";PROGRAMMA: OEM0_CORRETTORI71.SPF"
    Print #1, ";VERSIONE : " & Date
    Print #1, ";ROTORE   : " & NomeROTORE
    For j = 1 To Np
      Print #1, "RA[0," & Format$(j, "#####0") & "]=" & frmt(RA(j), 4) & " ";
      Print #1, "AL[0," & Format$(j, "#####0") & "]=" & frmt(al(j), 4) & " ";
      Print #1, "BE[0," & Format$(j, "#####0") & "]=" & frmt(BE(j), 4)
    Next j
    For j = Np + 1 To nMAXpntG4
      Print #1, "RA[0," & Format$(j, "#####0") & "]=0" & " ";
      Print #1, "AL[0," & Format$(j, "#####0") & "]=0" & " ";
      Print #1, "BE[0," & Format$(j, "#####0") & "]=0"
    Next j
    Print #1, "M17"
  Close #1
  'SCRITTURA DELLA FREQUENZA DI PROFILATURA
  Call SP("R[6]", 1, False)
  'SCRITTURA DEL DIAMETRO MINIMO MOLA
  i = (RAGGIO_ESTERNO_MOLA * 2) * 2 / 3
  Call SP("DMINMOLA", Format$(i, "##0"), False)
  'SCRITTURA DEL MASSIMO ASSORBIMENTO
  Call SP("COR[0,58]", 80, False)
  'SCRITTURA DELLA LARGHEZZA MOLA
  Valore = (-XX(0) + XX(Np + 1))
      If (Valore <= 5) Then
        Valore = 5
  ElseIf (Valore <= 10) Then
        Valore = 10
  ElseIf (Valore <= 15) Then
        Valore = 15
  ElseIf (Valore <= 20) Then
        Valore = 20
  ElseIf (Valore <= 25) Then
        Valore = 25
  ElseIf (Valore <= 30) Then
        Valore = 30
  ElseIf (Valore <= 35) Then
        Valore = 35
  ElseIf (Valore <= 40) Then
        Valore = 40
  ElseIf (Valore <= 45) Then
        Valore = 45
  ElseIf (Valore <= 50) Then
        Valore = 50
  ElseIf (Valore <= 55) Then
        Valore = 55
  ElseIf (Valore <= 60) Then
        Valore = 60
  ElseIf (Valore <= 65) Then
        Valore = 65
  ElseIf (Valore <= 70) Then
        Valore = 70
  ElseIf (Valore <= 75) Then
        Valore = 75
  ElseIf (Valore <= 80) Then
        Valore = 80
  ElseIf (Valore <= 85) Then
        Valore = 85
  ElseIf (Valore <= 90) Then
        Valore = 90
  ElseIf (Valore <= 95) Then
        Valore = 95
  ElseIf (Valore <= 100) Then
        Valore = 100
  ElseIf (Valore <= 105) Then
        Valore = 105
  ElseIf (Valore <= 110) Then
        Valore = 110
  ElseIf (Valore <= 115) Then
        Valore = 115
  ElseIf (Valore <= 120) Then
        Valore = 120
  End If
  Call SP("R[37]", Valore, False)
  Call SP("TAS[0,96]", CORHD, False)
  'AGGIORNAMENTO DEL DATABASE
  Call AggiornaDB
  '
  WRITE_DIALOG ""
  '
Else
  '
  WRITE_DIALOG "Operation aborted by user!!!"
  '
End If
'
Exit Sub

End Sub

Public Sub LETTURA_PARPROFILO_ROT(ByRef NpRot As Integer, ByRef Rrot() As Double, ByRef Arot() As Double, ByRef Brot() As Double, ByRef Rmin As Double, ByRef Rmax As Double)
'
Dim stmp        As String
Dim RTMP        As Double
Dim RTMP1       As Double
Dim RTMP2       As Double
Dim RTMP3       As Double

Dim i           As Integer
Dim j           As Integer
Dim k           As Integer
'
'********************************************************
  NpRot = 999         ': num. punti rotore
  ReDim Rrot(NpRot)   ': Raggio - RAB rotore
  ReDim Arot(NpRot)   ': Alfa - RAB rotore
  ReDim Brot(NpRot)   ': Beta - RAB rotore
'/!\ ATTENZIONE /!\ NpRot viene riassegnato (vedi sotto)
'********************************************************
'
On Error GoTo errLETTURA_PARPROFILO_ROT
  '
  'PROFILO ROTORE: PARAMETRI
  'NUMERO DENTI
  ParProfiloRot.NDenteLOC = LEP("R[5]")
  ParProfiloRot.sNDenteLOC = LNP("R[5]")
  'DIAMETRO INTERNO
  ParProfiloRot.DiaINT = LEP("R[45]")
  ParProfiloRot.sDiaInt = LNP("R[45]")
  'DIAMETRO PRIMITIVO
  ParProfiloRot.DIAMETRO_PRIMITIVO = 0
  'NUMERO PUNTI (ROTORE)
  ParProfiloRot.NpTot = LEP("R[147]")
  ParProfiloRot.sNpTot = LNP("R[147]")
  'PUNTI DEL FIANCO 1 (ROTORE)
  ParProfiloRot.NPF1 = LEP("R[43]")
  ParProfiloRot.sNpF1 = LNP("R[43]")
  'INCLINAZIONE MOLA
  RTMP = LEP("R[258]")
  ParProfiloRot.ELICA = Sgn(RTMP) * (90 - Abs(RTMP))
  ParProfiloRot.sELICA = LNP("R[258]")
  'PASSO ELICA
  ParProfiloRot.PASSO = LEP("R[390]")
  ParProfiloRot.sPASSO = LNP("R[390]")
  'ROTAZIONE PEZZO
  ParProfiloRot.Rotazione = LEP("R[56]")
  ParProfiloRot.sRotazione = LNP("R[56]")
  'CORREZIONE DI ALTEZZA DENTE
  ParProfiloRot.CORREZIONE_ALTEZZA_DENTE = LEP("TAS[0,96]")
  ParProfiloRot.sCORREZIONE_ALTEZZA_DENTE = LNP("TAS[0,96]")
  'CORREZIONE DI SPESSORE DENTE
  ParProfiloRot.CorSDENTE = LEP("R[55]")
  ParProfiloRot.sCorSDENTE = LNP("R[55]")
  'INIZIO FIANCHI
  ParProfiloRot.EAP_F1(0) = 0
  ParProfiloRot.EAP_F2(0) = 0
  For i = 1 To 4
    'FINE SEZIONE 1
    ParProfiloRot.EAP_F1(i) = 0
    ParProfiloRot.EAP_F2(i) = 0
    'RASTREMAZIONE 1
    ParProfiloRot.RR_F1(i) = 0
    ParProfiloRot.RR_F2(i) = 0
    'BOMBATURA 1
    ParProfiloRot.BB_F1(i) = 0
    ParProfiloRot.BB_F2(i) = 0
  Next i
  '
Dim PERCORSO    As String
Dim COORDINATE  As String
Dim RADICE      As String
  '
  'LETTURA DEL PERCORSO DI RIFERIMENTO E COORDINATE CLIENTE
  PERCORSO = LEP_STR("PERCORSO")
  PERCORSO = g_chOemPATH & "\" & PERCORSO
  'RICAVO IL NOME DEL ROTORE DAL PERCORSO
  COORDINATE = PERCORSO
  If (Len(COORDINATE) > 0) Then
    i = InStr(COORDINATE, "\")
    While (i <> 0)
      j = i
      i = InStr(i + 1, COORDINATE, "\")
    Wend
    'RICAVO IL NOME DEL ROTORE
    COORDINATE = Right$(COORDINATE, Len(COORDINATE) - j)
  End If
  'RICAVO IL PERCORSO DI LAVORO
  RADICE = PERCORSO
  i = InStr(RADICE, COORDINATE)
  RADICE = Left$(RADICE, i - 2)
  '
  If (Dir$(RADICE & "\PRFROT") = "") Then
    WRITE_DIALOG RADICE & "\PRFROT" & " profile does not exist!!"
    Exit Sub
  End If
  '
  'PUNTI PROFILO ROTORE
  k = 0: i = 1
  Rmax = -999999: Rmin = 999999
  Dim iK As Integer
  Open RADICE & "\PRFROT" For Input As #1
    While (Not EOF(1)) And (i < nMAXpntG4)
    'For iK = 1 To nMAXpntG4
      Input #1, RTMP1, RTMP2, RTMP3
      'RTMP1 = LEP("RA[0," & Trim(iK) & "]")
      'RTMP2 = LEP("AL[0," & Trim(iK) & "]")
      'RTMP3 = LEP("BE[0," & Trim(iK) & "]")
      If (RTMP1 + Abs(RTMP2) + Abs(RTMP3) <> 0) Then
        k = k + 1
        Rrot(k) = RTMP1
        Arot(k) = RTMP2
        Brot(k) = RTMP3
        If (Rmax < Rrot(k)) Then Rmax = Rrot(k)
        If (Rmin > Rrot(k)) Then Rmin = Rrot(k)
      End If
      'SPOSTO IN AVANTI IL PUNTATORE AL RECORD
      i = i + 1
    'Next iK
    Wend
  Close #1
  '
  NpRot = k
  '*******************************************************
  ReDim Preserve Rrot(NpRot)   ': Raggio - RAB rotore
  ReDim Preserve Arot(NpRot)   ': Alfa   - RAB rotore
  ReDim Preserve Brot(NpRot)   ': Beta   - RAB rotore
  '*******************************************************
  '
Exit Sub

errLETTURA_PARPROFILO_ROT:
  If FreeFile > 0 Then Close
  WRITE_DIALOG "SUB LETTURA_PARPROFILO_ROT ERROR " & Err
  'Resume Next
  Exit Sub
  
End Sub

Public Sub LETTURA_PARPROFILO_RAB(ByRef NpRot As Integer, ByRef Rrot() As Double, ByRef Arot() As Double, ByRef Brot() As Double, ByRef Rmin As Double, ByRef Rmax As Double)
'
Dim stmp        As String
Dim RTMP        As Double
Dim RTMP1       As Double
Dim RTMP2       As Double
Dim RTMP3       As Double

Dim i           As Integer
Dim j           As Integer
Dim k           As Integer
'
'********************************************************
  NpRot = 999         ': num. punti rotore
  ReDim Rrot(NpRot)   ': Raggio - RAB rotore
  ReDim Arot(NpRot)   ': Alfa - RAB rotore
  ReDim Brot(NpRot)   ': Beta - RAB rotore
'/!\ ATTENZIONE /!\ NpRot viene riassegnato (vedi sotto)
'********************************************************
'
On Error GoTo errLETTURA_PARPROFILO_RAB
  '
  'PROFILO ROTORE: PARAMETRI
  'NUMERO DENTI
  ParProfiloRot.NDenteLOC = LEP("R[5]")
  ParProfiloRot.sNDenteLOC = LNP("R[5]")
  'DIAMETRO INTERNO
  ParProfiloRot.DiaINT = 0
  'DIAMETRO PRIMITIVO
  ParProfiloRot.DIAMETRO_PRIMITIVO = LEP("R[103]")
  'NUMERO PUNTI (ROTORE)
  ParProfiloRot.NpTot = LEP("R[147]")
  ParProfiloRot.sNpTot = LNP("R[147]")
  'PUNTI DEL FIANCO 1 (ROTORE)
  ParProfiloRot.NPF1 = LEP("R[43]")
  ParProfiloRot.sNpF1 = LNP("R[43]")
  'INCLINAZIONE MOLA
  RTMP = LEP("R[41]")
  ParProfiloRot.ELICA = Sgn(RTMP) * Abs(RTMP)
  ParProfiloRot.sELICA = LNP("R[41]")
  'PASSO ELICA
  ParProfiloRot.PASSO = LEP("R[390]")
  ParProfiloRot.sPASSO = LNP("R[390]")
  'ROTAZIONE PEZZO
  ParProfiloRot.Rotazione = LEP("R[56]")
  ParProfiloRot.sRotazione = LNP("R[56]")
  'CORREZIONE DI SPESSORE DENTE
  ParProfiloRot.CorSDENTE = LEP("R[55]")
  ParProfiloRot.sCorSDENTE = LNP("R[55]")
  'INIZIO FIANCHI SAP
  ParProfiloRot.EAP_F1(0) = val(Lp("R[269]")) / 2: ParProfiloRot.EAP_F2(0) = val(Lp("R[284]")) / 2
  'SEZIONE 1
  ParProfiloRot.EAP_F1(1) = val(Lp("R[270]")) / 2: ParProfiloRot.EAP_F2(1) = val(Lp("R[285]")) / 2
  ParProfiloRot.RR_F1(1) = val(Lp("R[271]")) / 2:  ParProfiloRot.RR_F2(1) = val(Lp("R[286]")) / 2
  ParProfiloRot.BB_F1(1) = val(Lp("R[272]")) / 2:  ParProfiloRot.BB_F2(1) = val(Lp("R[287]")) / 2
  'SEZIONE 2
  ParProfiloRot.EAP_F1(2) = val(Lp("R[273]")) / 2: ParProfiloRot.EAP_F2(2) = val(Lp("R[288]")) / 2
  ParProfiloRot.RR_F1(2) = val(Lp("R[274]")) / 2:  ParProfiloRot.RR_F2(2) = val(Lp("R[289]")) / 2
  ParProfiloRot.BB_F1(2) = val(Lp("R[275]")) / 2:  ParProfiloRot.BB_F2(2) = val(Lp("R[290]")) / 2
  'SEZIONE 3
  ParProfiloRot.EAP_F1(3) = val(Lp("R[276]")) / 2: ParProfiloRot.EAP_F2(3) = val(Lp("R[291]")) / 2
  ParProfiloRot.RR_F1(3) = val(Lp("R[277]")) / 2:  ParProfiloRot.RR_F2(3) = val(Lp("R[292]")) / 2
  ParProfiloRot.BB_F1(3) = val(Lp("R[278]")) / 2:  ParProfiloRot.BB_F2(3) = val(Lp("R[293]")) / 2
  'SEZIONE 4
  ParProfiloRot.EAP_F1(4) = val(Lp("R[279]")) / 2: ParProfiloRot.EAP_F2(4) = val(Lp("R[294]")) / 2
  ParProfiloRot.RR_F1(4) = val(Lp("R[280]")) / 2:  ParProfiloRot.RR_F2(4) = val(Lp("R[295]")) / 2
  ParProfiloRot.BB_F1(4) = val(Lp("R[281]")) / 2:  ParProfiloRot.BB_F2(4) = val(Lp("R[296]")) / 2
  'PUNTI PROFILO ROTORE
  k = 0: i = 1
  Rmax = -999999: Rmin = 999999
  Dim iK As Integer
  For iK = 1 To 200
      RTMP1 = LEP("RA[0," & Trim(iK) & "]")
      RTMP2 = LEP("AL[0," & Trim(iK) & "]")
      RTMP3 = LEP("BE[0," & Trim(iK) & "]")
      If (RTMP1 + Abs(RTMP2) + Abs(RTMP3) <> 0) Then
        k = k + 1
        Rrot(k) = RTMP1
        Arot(k) = RTMP2
        Brot(k) = RTMP3
        If (Rmax < Rrot(k)) Then Rmax = Rrot(k)
        If (Rmin > Rrot(k)) Then Rmin = Rrot(k)
      End If
      'SPOSTO IN AVANTI IL PUNTATORE AL RECORD
      i = i + 1
  Next iK
  '
  NpRot = k
  '*******************************************************
  ReDim Preserve Rrot(NpRot)   ': Raggio - RAB rotore
  ReDim Preserve Arot(NpRot)   ': Alfa   - RAB rotore
  ReDim Preserve Brot(NpRot)   ': Beta   - RAB rotore
  '*******************************************************
  '
Exit Sub

errLETTURA_PARPROFILO_RAB:
  If FreeFile > 0 Then Close
  WRITE_DIALOG "SUB LETTURA_PARPROFILO_RAB ERROR " & Err
  'Resume Next
  Exit Sub
  
End Sub

Public Sub LETTURA_PARMOLA_RAB(ByVal Np As Integer)
'
Dim i   As Integer
'
On Error GoTo errLETTURA_PARMOLA_RAB
  '
  'PARAMETRI MOLA
  'CORREZIONE DEDENDUM F1
  ParMolaROT.DEDF(1) = LEP("R[51]")
  'CORREZIONE ADDENDUM F1
  ParMolaROT.ADDF(1) = LEP("R[52]")
  'CORREZIONE DEDENDUM F2
  ParMolaROT.DEDF(2) = LEP("R[175]")
  'CORREZIONE ADDENDUM F2
  ParMolaROT.ADDF(2) = LEP("R[178]")
  'CORREZIONE DI SPESSORE MOLA
  ParMolaROT.CorSMOLA = LEP("CORSPED_G[0,1]")
  'CORREZIONE PER PUNTI
  ReDim ParMolaROT.CorX(Np + 1): ReDim ParMolaROT.corY(Np + 1):
  For i = 0 To Np + 1
    ParMolaROT.CorX(i) = LEP("CORX[0," & Format$(i, "##0") & "]")
    ParMolaROT.corY(i) = LEP("CORY[0," & Format$(i, "##0") & "]")
  Next i
  '
Exit Sub

errLETTURA_PARMOLA_RAB:
 If FreeFile > 0 Then Close
  WRITE_DIALOG "SUB LETTURA_PARMOLA_RAB ERROR " & Err
  Exit Sub

End Sub

Public Sub LETTURA_PARMOLA_ROT(ByVal Np As Integer)
'
Dim i   As Integer
'
On Error GoTo errLETTURA_PARMOLA_ROT
  '
  'PARAMETRI MOLA
  'CORREZIONE DEDENDUM F1
  ParMolaROT.DEDF(1) = 0 'LEP("R[51]")
  'CORREZIONE ADDENDUM F1
  ParMolaROT.ADDF(1) = 0 'LEP("R[52]")
  'CORREZIONE DEDENDUM F2
  ParMolaROT.DEDF(2) = 0 'LEP("R[175]")
  'CORREZIONE ADDENDUM F2
  ParMolaROT.ADDF(2) = 0 'LEP("R[178]")
  'CORREZIONE DI SPESSORE MOLA
  ParMolaROT.CorSMOLA = LEP("R[34]")
  'CORREZIONE PER PUNTI
  ReDim ParMolaROT.CorX(Np + 1): ReDim ParMolaROT.corY(Np + 1):
  For i = 0 To Np + 1
    ParMolaROT.CorX(i) = LEP("CORX[0," & Format$(i, "##0") & "]")
    ParMolaROT.corY(i) = LEP("CORY[0," & Format$(i, "##0") & "]")
  Next i
  '
Exit Sub

errLETTURA_PARMOLA_ROT:
 If FreeFile > 0 Then Close
  WRITE_DIALOG "SUB LETTURA_PARMOLA_ROT ERROR " & Err
  Exit Sub

End Sub

Sub CARICA_CORRETTORI_MOLA()
'
Dim MODALITA  As String
Dim sRESET    As String
Dim iRESET    As Integer
Dim i         As Integer
Dim Np        As Integer
Dim CORXmol() As String
Dim CORYmol() As String
'
On Error GoTo errCARICA_CORRETTORI_MOLA
  '
  'LEGGO LA MODALITA DI TRASFERIMENTO DATI
  MODALITA = GetInfo("Configurazione", "MODALITA", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))
  If (MODALITA = "OFF-LINE") Then
    iRESET = 1
  Else
    iRESET = CONTROLLO_STATO_RESET
  End If
  If (iRESET <> 1) Then
    'AVVERTO CHE NON FACCIO NIENTE
    WRITE_DIALOG "Data cannot be modified: CNC is not on RESET state"
    Exit Sub
  End If
  'NUMERO PUNTI (ROTORE)
  ParProfiloRot.NpTot = LEP("R[147]")
  ParProfiloRot.sNpTot = LNP("R[147]")
  Np = nMaxpntROT
  ReDim CORXmol(Np), CORYmol(Np)
  For i = 0 To Np
    CORXmol(i) = "0.0000"
    CORYmol(i) = "0.0000"
  Next i
  If (Dir(g_chOemPATH & "\CORMOL.ROT") = "") Then
    'AVVERTO CHE NON TROVO IL FILE CON LE CORREZIONI
    WRITE_DIALOG "Wheel correctors not found or already loaded"
    Exit Sub
  End If
  'LETTURA FILE INI DELLE CORREZIONI
  For i = 0 To Np
    CORXmol(i) = GetInfo("CorrezioniPuntiMola", "CORX[0," & Format$(i, "####0") & "]", g_chOemPATH & "\CORMOL.ROT")
    CORYmol(i) = GetInfo("CorrezioniPuntiMola", "CORY[0," & Format$(i, "####0") & "]", g_chOemPATH & "\CORMOL.ROT")
  Next i
  'SCRITTURA DEI CORRETTORI NEL DATABASE
  For j = 0 To Np
    Call SP("CORX[0," & Format$(j, "####0") & "]", frmt(val(CORXmol(j)), 4), False)
    Call SP("CORY[0," & Format$(j, "####0") & "]", frmt(val(CORYmol(j)), 4), False)
  Next j
  Call AggiornaDB
  'CREAZIONE DEL SOTTOPROGRAMMA DELLE CORREZIONI
  If (MODALITA = "SPF") Then
    Call OEMX.SuOEM1.CreaSPF
    Call Kill(g_chOemPATH & "\CORMOL.ROT")
  End If
  'AGGIORNAMENTO DELLA VISUALIZZAZIONE
  Call OEMX.SuOEM1.DisegnaOEM
  WRITE_DIALOG "Wheel correctors loading completed"
  '
Exit Sub

errCARICA_CORRETTORI_MOLA:
  If FreeFile > 0 Then Close
  MsgBox "ERROR: " & Error(Err), 48, "SUB: CARICA_CORRETTORI_MOLA"
  Exit Sub

End Sub

Sub CARICA_DATI_CONTROLLO()
'
Dim MODALITA      As String
Dim DB            As Database
Dim TB            As Recordset
'
Dim SovrMet       As Single
Dim FuoriCentro   As Single
'
Dim R203  As Double: Dim sR203 As String
Dim R()   As Double  ' ROTORE : Raggio
Dim A()   As Double  ' ------ : Alfa
Dim B()   As Double  ' ------ : Beta
Dim XX0() As Double  ' MOLA : XX senza correzioni mola
Dim YY0() As Double  ' ---- : YY senza correzioni mola
Dim TG0() As Double  ' ---- : TG senza correzioni mola
Dim Rmin  As Double
Dim Rmax  As Double
Dim Np    As Integer
Dim stmp  As String
Dim riga  As String
'
'GESTIONE DATI PER CONTROLLORE ESTERNO
Dim PERCORSO           As String
Dim COORDINATE         As String
Dim RADICE             As String
Dim PERCORSO_CONTROLLI As String
Dim PERCORSO_PROFILI   As String
Dim sequenza_tmp       As String
Dim npt_tmp            As Integer
Dim TY_ROT             As String
Dim PASSO_tmp          As Double
Dim CONTROLLORE        As String
Dim DM_ROT             As String
Dim INVERSIONE_tmp     As Integer
'
On Error GoTo errCARICA_DATI_CONTROLLO
  '
  'LEGGO LA MODALITA DI TRASFERIMENTO DATI
  MODALITA = GetInfo("Configurazione", "MODALITA", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))
  If (MODALITA = "SPF") Then
    'NELLA MODALITA' ONLINE LE IMPOSTAZIONI DEL CONTROLLORE SONO LETTI DALL'ARCHIVIO
    Call LEGGI_INFO_ROTORI(PERCORSO, COORDINATE, RADICE, PERCORSO_CONTROLLI, PERCORSO_PROFILI, sequenza_tmp, npt_tmp, TY_ROT, PASSO_tmp, CONTROLLORE, DM_ROT, INVERSIONE_tmp)
  Else
    'LETTURA DEL PERCORSO DI RIFERIMENTO
    PERCORSO = LEP_STR("PERCORSO")
    PERCORSO = g_chOemPATH & "\" & PERCORSO
    If (Len(PERCORSO) > 0) Then
      'RICAVO IL NOME DEL ROTORE DAL PERCORSO
      COORDINATE = PERCORSO
      If (Len(COORDINATE) > 0) Then
        i = InStr(COORDINATE, "\")
        While (i <> 0)
          j = i
          i = InStr(i + 1, COORDINATE, "\")
        Wend
        'RICAVO IL NOME DEL ROTORE
        COORDINATE = Right$(COORDINATE, Len(COORDINATE) - j)
      End If
      'RICAVO IL PERCORSO DI LAVORO
      RADICE = PERCORSO
      i = InStr(RADICE, COORDINATE)
      RADICE = Left$(RADICE, i - 2)
    End If
    DM_ROT = LEP_STR("DM_ROT")
    CONTROLLORE = LEP_STR("CONTROLLORE")
    'PERCORSO PROFILI PER CONTROLLO
    PERCORSO_PROFILI = LEP_STR("PERCORSO_PROFILI")
    TY_ROT = LEP_STR("TY_ROT")
  End If
  FuoriCentro = LEP("TAS[0,26]")
  SovrMet = LEP("TAS[0,27]")
  If (CONTROLLORE = "INTERNAL") Then
      If (MODALITA = "OFF-LINE") Then
        WRITE_DIALOG "Data cannot be loaded on " & MODALITA & " MODE"
        Exit Sub
      Else
        WRITE_DIALOG "CALCOLO PROFILO ED INVIO CHKRAB.SPF"
        sR203 = OPC_LEGGI_DATO("/Channel/Parameter/R[203]")
        R203 = val(sR203)
        If (R203 <= 0) Then R203 = val(DM_ROT) / 2
        Call LETTURA_PARPROFILO_ROT(Np, R(), A(), B(), Rmin, Rmax)
        Call LETTURA_PARMOLA_ROT(Np)
        Call CALCOLO_MOLA_ROTORI(R203, Np, Rmin, Rmax, R, A, B, XX0, YY0, TG0, "Y")
        Call CALCOLO_PROFILO_CONTROLLO(SovrMet, FuoriCentro, R203 * 2, Rmin, RADICE, B)
      End If
  Else
    '(CONTROLLORE = "EXTERNAL")
        WRITE_DIALOG "CALCOLO PROFILO ED INVIO CHKRAB.SPF"
        sR203 = OPC_LEGGI_DATO("/Channel/Parameter/R[203]")
        R203 = val(sR203)
        If (R203 <= 0) Then R203 = val(DM_ROT) / 2
        Call LETTURA_PARPROFILO_ROT(Np, R(), A(), B(), Rmin, Rmax)
        Call LETTURA_PARMOLA_ROT(Np)
      If (MODALITA <> "OFF-LINE") Then
        Call CALCOLO_MOLA_ROTORI(R203, Np, Rmin, Rmax, R, A, B, XX0, YY0, TG0, "Y")
        Call CALCOLO_PROFILO_CONTROLLO(SovrMet, FuoriCentro, R203 * 2, Rmin, RADICE, B)
      End If
        Call CALCOLO_PROFILO_FRONTALE(SovrMet, FuoriCentro, DM_ROT)
        'CONTROLLO SE ESISTE LA PERIFERICA
        'stmp = LEP_STR("PERCORSO_PROFILI")
        stmp = PERCORSO_PROFILI
        'SE NON ESISTE LO CREO
        If (Dir$(stmp, 16) = "") Then
        Call WRITE_DIALOG(stmp & " does not exist!!!")
        i = InStr(stmp, "\")
        While (i <> 0)
          riga = Left$(stmp, i - 1)
          'SALTO LA PERIFERICA
          If (Right(riga, 1) <> ":") And (Dir$(riga, 16) = "") Then MkDir riga
          i = InStr(i + 1, stmp, "\")
        Wend
        Call MkDir(stmp)
        End If
        Select Case CONTROLLORE
        Case "KLINGELNBERG"
            Call PIAZZAMENTO_CONTROLLO_KLINGELNBERG(PERCORSO_PROFILI & "\", RADICE, TY_ROT, val(DM_ROT) / 2, "RABROT.KLN", "_" & Right$(frmt0(SovrMet, 3), 3))
        Case "ACCURA"
            Call PIAZZAMENTO_CONTROLLO_ZEISS_ACCURA(PERCORSO_PROFILI & "\", RADICE, TY_ROT, "RABROT.KLN")
        Case "DEA"
            Call PIAZZAMENTO_CONTROLLO_DEA_IMAGE(PERCORSO_PROFILI & "\", RADICE, TY_ROT, "RABROT.KLN", "_" & Right$(frmt0(SovrMet, 3), 3))
        Case "QUINDOS"
            Call PIAZZAMENTO_CONTROLLO_QUINDOS(PERCORSO_PROFILI & "\", RADICE, TY_ROT, "RABROT.KLN", "_" & Right$(frmt0(SovrMet, 3), 3))
        Case Else
            StopRegieEvents
            MsgBox "TYPE OF INSPECTION MACHINE NOT ALLOWED!!!!", vbCritical, "WARNING"
            ResumeRegieEvents
        End Select
  End If
  '
Exit Sub

errCARICA_DATI_CONTROLLO:
  If FreeFile > 0 Then Close
  i = Err
  Call WRITE_DIALOG("CARICA_DATI_CONTROLLO: " & Error(i))
  Resume Next
  
End Sub

Sub CALCOLO_PROFILO_CONTROLLO(ByVal ENTRX As Single, ByVal SpostaX As Double, ByVal Dmed As Single, ByVal RagINT As Double, ByVal RADICE As String, ByRef B() As Double)
'
Dim i          As Long
Dim j          As Integer
Dim k          As Integer
Dim RR_CHK()   As Double
Dim VV_CHK()   As Double
Dim TT_CHK()   As Double
Dim AB_CHK()   As String
Dim NomePezzo  As String
Dim NomeDIR    As String
'
Dim INF   As Double
Dim PASSO As Double
Dim EXV   As Double
Dim EXF   As Double
Dim RXF   As Double
Dim APFD  As Double
Dim APF   As Double
Dim Rayon As Double
Dim ALFA  As Double
Dim Beta  As Double
Dim AlfaG As Double
Dim BetaG As Double
Dim stmp  As String
Dim SENS  As Integer
Dim v7    As Double
Dim V12   As Double
Dim V13   As Double
Dim V14   As Double
Dim V15   As Double
Dim V16   As Double
Dim VPB   As Double
Dim RXV   As Double
Dim Ang   As Double
Dim ang1  As Double
Dim YAPP  As Double
Dim XAPP  As Double
Dim Y1    As Double
Dim X1    As Double
Dim NpRot As Integer
'
On Error GoTo errCALCOLO_PROFILO_CONTROLLO
  '
  'CALCOLO PROFILO ROTORE DALLA MOLA
  ENTRX = ENTRX + Dmed / 2 + RagINT
  INF = ParProfiloRot.ELICA
  INF = (Abs(INF)) * PG / 180
  PASSO = ParProfiloRot.PASSO
  NpRot = ParProfiloRot.NpTot
  ReDim RR_CHK(NpRot): ReDim VV_CHK(NpRot): ReDim TT_CHK(NpRot): ReDim AB_CHK(NpRot)
  For i = 1 To NpRot
    EXF = ParMolaROT.XX(i)
    RXF = ParMolaROT.YY(i)
    APFD = ParMolaROT.TT(i) * 180 / PG
    'SPOSTAMENTO ASSE MOLA
    EXF = EXF + SpostaX
    If (APFD < 0) Then APF = (180 + APFD) * PG / 180 Else APF = APFD * PG / 180
    GoSub 1920
    AlfaG = ALFA * 180 / PG
    BetaG = Beta * 180 / PG
    If (BetaG > 90) Then BetaG = BetaG - 180
    If (BetaG < -90) Then BetaG = 180 + BetaG
    RR_CHK(i) = Rayon: VV_CHK(i) = AlfaG
    TT_CHK(i) = B(i) 'BetaG
  Next i
  'CREAZIONE VETTORE CHECK SI/NO
  If (Dir$(RADICE & "\CHKRAB.ROT") <> "") Then
    For i = 1 To NpRot
      AB_CHK(i) = GetInfo("PuntiControllo", "CHK[0," & CStr(i) & "]", RADICE & "\CHKRAB.ROT")
      If (Len(AB_CHK(i)) <= 0) Then AB_CHK(i) = "1"
    Next i
  Else
    For i = 1 To NpRot
      AB_CHK(i) = "1"
    Next i
  End If
  'LETTURA NOME PEZZO
  NomePezzo = LEGGI_NomePezzo("ROTORI_ESTERNI")
  NomeDIR = LEGGI_DIRETTORIO_WKS("ROTORI_ESTERNI")
  'CREAZIONE DEL FILE CHKRAB.SPF
  Open RADICE & "\CHKRAB.SPF" For Output As #1
    Print #1, ";PROGRAM: CHKRAB.SPF"
    Print #1, ";VERSION : " & Date
    Print #1, "STR[0]=" & Chr(34) & NomePezzo & Chr(34)
    Print #1, "STR[1]=" & Chr(34) & Date & Chr(34)
    Print #1, ";CHECKING POINTS"
    For i = 1 To NpRot
      Print #1, "RA[0," & Format$(i, "#####0") & "]=" & frmt(RR_CHK(i), 4) & " ";
      Print #1, "AL[0," & Format$(i, "#####0") & "]=" & frmt(VV_CHK(i), 4) & " ";
      Print #1, "BE[0," & Format$(i, "#####0") & "]=" & frmt(TT_CHK(i), 4) & " ";
      Print #1, "MISF1[0," & Format$(i, "#####0") & "]=" & AB_CHK(i)
    Next i
    Print #1, "M17 " & ";END CHKRAB.SPF"
  Close #1
  'INVIO SOTTOPROGRAMMA AL CNC
  Set Session = GetObject("@SinHMIMCDomain.MCDomain")
  Session.CopyNC RADICE & "\CHKRAB.SPF", "/NC/WKS.DIR/" & NomeDIR & ".WPD/CHKRAB.SPF", MCDOMAIN_COPY_NC
  Set Session = Nothing
  'SEGNALAZIONE UTENTE
  WRITE_DIALOG "/NC/WKS.DIR/" & NomeDIR & ".WPD/" & "CHKRAB.SPF"
  '
Exit Sub

1920 ' ************** S/P ************ CALCUL PIECE D'APRES FRAISE **********
     ' Entrees  constantes : ENTRX, PAS,  INF
     ' Entrees  variables  : RXF,   EXF,  APF
     ' Sorties             : RAYON, ALFA, BETA
     ' **********************************************************************
     SENS = 1: If EXF < 0 Then SENS = -1
     EXF = EXF * SENS
     APF = APF * SENS
     v7 = PASSO / 2 / PG
     GoSub 2050
     Rayon = RXV
     ALFA = Ang * SENS
     ang1 = Ang
     RXF = RXF + 0.01
     EXF = EXF - 0.01 * Tan(APF)
     GoSub 2050
     Y1 = YAPP: X1 = XAPP
     RXF = RXF - 0.02
     EXF = EXF + 0.02 * Tan(APF)
     GoSub 2050
     RXF = RXF + 0.01
     EXF = EXF - 0.01 * Tan(APF)
     If YAPP = Y1 Then YAPP = YAPP + 0.00001
     Beta = (Atn((X1 - XAPP) / (YAPP - Y1)) - ang1) * SENS
     If X1 = XAPP Then Beta = PG / 2
     APF = APF * SENS
     EXF = EXF * SENS
Return   '              ********* RETOUR AUX DONNEES

2050
  V12 = -Tan(APF) * (ENTRX * Sin(INF) + v7 * Cos(INF))
  V13 = v7 * Sin(INF) - ENTRX * Cos(INF)
  V14 = Cos(INF) * ((-Tan(APF) * EXF) + RXF)
  V15 = (V14 ^ 2) + (V12 ^ 2) - (V13 ^ 2)
  If (V15 > 0) Then
    V15 = Sqr(V15)
    V16 = -(V15 + V12) / (V13 - V14)
    V15 = (V15 - V12) / (V13 - V14)
    VPB = V16: If Abs(V15) < Abs(V16) Then VPB = V15
    V12 = 2 * Atn(VPB)
    V13 = (RXF * Sin(INF) * Sin(V12)) + EXF * Cos(INF)
    V13 = Atn(V13 / ((RXF * Cos(V12)) - ENTRX))
    RXV = (ENTRX - RXF * Cos(V12)) / Cos(V13)
    EXV = EXF * Sin(INF) - v7 * V13 - RXF * Sin(V12) * Cos(INF)
    Ang = 2 * PG * EXV / PASSO
    YAPP = RXV * Cos(Ang)
    XAPP = -RXV * Sin(Ang)
  End If
  '
Return
  
errCALCOLO_PROFILO_CONTROLLO:
  If (FreeFile > 0) Then Close
  i = Err
  StopRegieEvents
  MsgBox Error$(i), 48, "SUB: CALCOLO_PROFILO_CONTROLLO"
  ResumeRegieEvents
  Exit Sub
  'Resume Next

End Sub

Sub LEGGI_CNC_CONTROLLO_ROTORI(RADICE As String)
'
Dim ret   As Long
Dim i     As Integer
Dim j     As Integer
Dim k1    As Integer
Dim k2    As Integer
Dim k3    As Integer
Dim X     As Double
Dim X1    As Double
Dim X2    As Double
Dim X3    As Double
Dim Np    As Integer
Dim riga  As String
Dim stmp  As String
Dim stmp1 As String
Dim id$
Dim RR$
Dim AA$
Dim BB$
Dim EE$
Dim GEN$(30)
Dim STRG$(10)
Dim LenStr0 As Integer
Dim LenStr  As Integer
Dim NomeDIR As String
Dim nVANI   As Integer
'
On Error GoTo errLEGGI_CNC_CONTROLLO_ROTORI
  '
  'LETTURA DEL RISULTATO DEL CONTROLLO SENZA WRTMIS.SPF
  'NECESSARIO PER LA DIMENSIONE CHE POTREBBE AVERE RISROT.SPF CON 400 PUNTI
  'SEGNALAZIONE UTENTE
  WRITE_DIALOG "Reading data from CNC."
  'DIRETTORIO DEL RISULTATO
  NomeDIR = LEGGI_DIRETTORIO_WKS("ROTORI_ESTERNI")
  'LETTURA DEI PARAMETRI GENERALI
  STRG$(0) = LEGGI_NomePezzo("ROTORI_ESTERNI")
  STRG$(1) = Now
  GEN$(0) = OPC_LEGGI_DATO("/ACC/NCK/MGUD/GEN[1]")
  GEN$(14) = OPC_LEGGI_DATO("/ACC/NCK/MGUD/TAS[15]")
  GEN$(15) = OPC_LEGGI_DATO("/ACC/NCK/MGUD/TAS[16]")
  GEN$(16) = OPC_LEGGI_DATO("/ACC/NCK/MGUD/TAS[17]")
  GEN$(17) = OPC_LEGGI_DATO("/ACC/NCK/MGUD/TAS[18]")
  GEN$(18) = OPC_LEGGI_DATO("/ACC/NCK/MGUD/TAS[19]")
  GEN$(19) = OPC_LEGGI_DATO("/ACC/NCK/MGUD/TAS[20]")
  GEN$(20) = OPC_LEGGI_DATO("/ACC/NCK/MGUD/TAS[21]")
  GEN$(21) = OPC_LEGGI_DATO("/ACC/NCK/MGUD/TAS[22]")
  GEN$(22) = OPC_LEGGI_DATO("/ACC/NCK/MGUD/TAS[23]")
  GEN$(23) = OPC_LEGGI_DATO("/ACC/NCK/MGUD/TAS[24]")
  GEN$(24) = OPC_LEGGI_DATO("/ACC/NCK/MGUD/TAS[25]")
  GEN$(25) = OPC_LEGGI_DATO("/ACC/NCK/MGUD/TAS[26]")
  GEN$(27) = OPC_LEGGI_DATO("/ACC/NCK/MGUD/TAS[28]")
  'CALCOLO DEL NUMERO DEI PUNTI CONTROLLATI
  Np = val(GEN$(16)) + val(GEN$(17))
  If (Np <= 0) Then Np = 400
  'APERTURA FILE DEI PUNTI DI CONTROLLO
  If (Dir$(RADICE & "\CHKRAB.SPF") <> "") Then
    'APERTURA E LETTURA DELLE PRIME CINQUE RIGHE
    Open RADICE & "\CHKRAB.SPF" For Input As #3
    For i = 1 To 5
      Line Input #3, stmp1
    Next i
  Else
    WRITE_DIALOG "CHKRAB.SPF " & Error(53)
    Exit Sub
  End If
  'ABILITAZIONE DELLE OPZIONI DI AQUISIZIONE CONTROLLO PROFILO
  nVANI = val(GetInfo("CONTROLLO", "nVANI", Path_LAVORAZIONE_INI))
  If (nVANI > 0) Then
        If (CHK0.optVANI(0).Value = True) Then
        'PRIMO PRINCIPIO
        ret = WritePrivateProfileString("CONTROLLO", "CHK0.optVANI(0).Value", "1", Path_LAVORAZIONE_INI)
        ret = WritePrivateProfileString("CONTROLLO", "CHK0.optVANI(1).Value", "0", Path_LAVORAZIONE_INI)
        ret = WritePrivateProfileString("CONTROLLO", "CHK0.optVANI(2).Value", "0", Path_LAVORAZIONE_INI)
        ret = WritePrivateProfileString("CONTROLLO", "CHK0.optVANI(3).Value", "0", Path_LAVORAZIONE_INI)
    ElseIf (CHK0.optVANI(1).Value = True) Then
        'SECONDO PRINCIPIO
        ret = WritePrivateProfileString("CONTROLLO", "CHK0.optVANI(0).Value", "0", Path_LAVORAZIONE_INI)
        ret = WritePrivateProfileString("CONTROLLO", "CHK0.optVANI(1).Value", "1", Path_LAVORAZIONE_INI)
        ret = WritePrivateProfileString("CONTROLLO", "CHK0.optVANI(2).Value", "0", Path_LAVORAZIONE_INI)
        ret = WritePrivateProfileString("CONTROLLO", "CHK0.optVANI(3).Value", "0", Path_LAVORAZIONE_INI)
    ElseIf (CHK0.optVANI(2).Value = True) Then
        'TERZO PRINCIPIO
        ret = WritePrivateProfileString("CONTROLLO", "CHK0.optVANI(0).Value", "0", Path_LAVORAZIONE_INI)
        ret = WritePrivateProfileString("CONTROLLO", "CHK0.optVANI(1).Value", "0", Path_LAVORAZIONE_INI)
        ret = WritePrivateProfileString("CONTROLLO", "CHK0.optVANI(2).Value", "1", Path_LAVORAZIONE_INI)
        ret = WritePrivateProfileString("CONTROLLO", "CHK0.optVANI(3).Value", "0", Path_LAVORAZIONE_INI)
    ElseIf (CHK0.optVANI(3).Value = True) Then
        'MEDIA
        ret = WritePrivateProfileString("CONTROLLO", "CHK0.optVANI(0).Value", "0", Path_LAVORAZIONE_INI)
        ret = WritePrivateProfileString("CONTROLLO", "CHK0.optVANI(1).Value", "0", Path_LAVORAZIONE_INI)
        ret = WritePrivateProfileString("CONTROLLO", "CHK0.optVANI(2).Value", "0", Path_LAVORAZIONE_INI)
        ret = WritePrivateProfileString("CONTROLLO", "CHK0.optVANI(3).Value", "1", Path_LAVORAZIONE_INI)
    End If
  End If
  'APRO IL FILE DI DEFAULT PER COPIARE I VALORI LETTI
  Open RADICE & "\GAPP4CHK.TXT" For Output As #2
    'DATI GENERALI ROTORE
    LenStr0 = 25
    stmp = "Work-piece name":        Print #2, Right$(String(LenStr0, " ") & stmp & ": ", LenStr0) & STRG$(0)
    stmp = "Check setup date":       Print #2, Right$(String(LenStr0, " ") & stmp & ": ", LenStr0) & STRG$(1)
    stmp = "No. of teeth":           Print #2, Right$(String(LenStr0, " ") & stmp & ": ", LenStr0) & GEN$(0)
    stmp = "Helix (RH=-1 LH=1)":     Print #2, Right$(String(LenStr0, " ") & stmp & ": ", LenStr0) & GEN$(14)
    stmp = "Lead":                   Print #2, Right$(String(LenStr0, " ") & stmp & ": ", LenStr0) & GEN$(15)
    stmp = "No. of Pts Rh":          Print #2, Right$(String(LenStr0, " ") & stmp & ": ", LenStr0) & GEN$(16)
    stmp = "No. of Pts Lh":          Print #2, Right$(String(LenStr0, " ") & stmp & ": ", LenStr0) & GEN$(17)
    stmp = "Inside Radius":          Print #2, Right$(String(LenStr0, " ") & stmp & ": ", LenStr0) & GEN$(18)
    stmp = "Outer Radius":           Print #2, Right$(String(LenStr0, " ") & stmp & ": ", LenStr0) & GEN$(19)
    stmp = "Pt for Rh centr.":       Print #2, Right$(String(LenStr0, " ") & stmp & ": ", LenStr0) & GEN$(20)
    stmp = "Pt for Lh centr.":       Print #2, Right$(String(LenStr0, " ") & stmp & ": ", LenStr0) & GEN$(21)
    stmp = "Pt for Root Dia":        Print #2, Right$(String(LenStr0, " ") & stmp & ": ", LenStr0) & GEN$(22)
    stmp = "ZSF probe Shift":        Print #2, Right$(String(LenStr0, " ") & stmp & ": ", LenStr0) & GEN$(23)
    stmp = "PrfZ check":             Print #2, Right$(String(LenStr0, " ") & stmp & ": ", LenStr0) & GEN$(24)
    stmp = "X pos out of the way":   Print #2, Right$(String(LenStr0, " ") & stmp & ": ", LenStr0) & GEN$(25)
    stmp = "Distance from Root Dia": Print #2, Right$(String(LenStr0, " ") & stmp & ": ", LenStr0) & GEN$(27)
    Print #2, ""
    'INTESTAZIONE
    LenStr0 = 4: LenStr = 10
    riga = ""
    id$ = Left$(" ID " & String(LenStr0, " "), LenStr0)
    stmp = "  RADIUS  "
    RR$ = Left$(stmp & String(LenStr, " "), LenStr)
    stmp = "   ALFA   "
    AA$ = Left$(stmp & String(LenStr, " "), LenStr)
    stmp = "   BETA   "
    BB$ = Left$(stmp & String(LenStr, " "), LenStr)
    stmp = "   ERR    "
    EE$ = Left$(stmp & String(LenStr, " "), LenStr)
    riga = id$ & "|" & RR$ & "|" & AA$ & "|" & BB$ & "|" & EE$ & "|"
    Print #2, riga
    riga = String(5 + LenStr0 + 4 * LenStr, "-")
    Print #2, riga
    For i = 1 To Np
      'LETTURA DELLA RIGA RAGGIO, ALFA E BETA
      Line Input #3, stmp1
      riga = ""
      k3 = InStr(stmp1, "M17")
    If (k3 <= 0) Then
      'INDICE DEL PUNTO
      id$ = Right$(String(LenStr0, " ") & CStr(i), LenStr0)
      'RICAVO RAGGIO
      k1 = InStr(stmp1, "="): k2 = InStr(stmp1, " ")
      X = Mid$(stmp1, k1 + 1, k2 - k1 - 1)
      RR$ = Right$(String(LenStr, " ") & Format(X, "###0.0000"), LenStr)
      'RICAVO ALFA
      stmp1 = Right$(stmp1, Len(stmp1) - k2)
      k1 = InStr(stmp1, "="): k2 = InStr(stmp1, " ")
      X = Mid$(stmp1, k1 + 1, k2 - k1 - 1)
      AA$ = Right$(String(LenStr, " ") & Format(X, "###0.0000"), LenStr)
      'RICAVO BETA
      stmp1 = Right$(stmp1, Len(stmp1) - k2)
      k1 = InStr(stmp1, "="): k2 = InStr(stmp1, " ")
      X = Mid$(stmp1, k1 + 1, k2 - k1 - 1)
      BB$ = Right$(String(LenStr, " ") & Format(X, "###0.0000"), LenStr)
      'LETTURA DIRETTA DELL'ERRORE MISURATO
    If (nVANI > 0) Then
          If (CHK0.optVANI(0).Value = True) Then
          'PRIMO PRINCIPIO
          X = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF2[" & Format$(i + 1, "##0") & "]"))
          WRITE_DIALOG "v(" & CHK0.optVANI(0).Caption & ") pnt(" & Format$(i, "###0") & ")"
      ElseIf (CHK0.optVANI(1).Value = True) Then
          'SECONDO PRINCIPIO
          X = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF2[" & Format$(i + 1 + 500, "##0") & "]"))
          WRITE_DIALOG "v(" & CHK0.optVANI(1).Caption & ") pnt(" & Format$(i, "###0") & ")"
      ElseIf (CHK0.optVANI(2).Value = True) Then
          'TERZO PRINCIPIO
          X = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF2[" & Format$(i + 1 + 1000, "##0") & "]"))
          WRITE_DIALOG "v(" & CHK0.optVANI(2).Caption & ") pnt(" & Format$(i, "###0") & ")"
      ElseIf (CHK0.optVANI(3).Value = True) Then
        'MEDIA
        Select Case nVANI
        Case 1
          X1 = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF2[" & Format$(i + 1, "##0") & "]"))
            X = (X1) / 1
        Case 2
          X1 = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF2[" & Format$(i + 1, "##0") & "]"))
          X2 = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF2[" & Format$(i + 1 + 500, "##0") & "]"))
          If (X1 = 999) Or (X2 = 999) Then
            X = 999
          Else
            X = (X1 + X2) / 2
          End If
        Case 3
          X1 = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF2[" & Format$(i + 1, "##0") & "]"))
          X2 = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF2[" & Format$(i + 1 + 500, "##0") & "]"))
          X3 = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF2[" & Format$(i + 1 + 1000, "##0") & "]"))
          If (X1 = 999) Or (X2 = 999) Or (X3 = 999) Then
            X = 999
          Else
            X = (X1 + X2 + X3) / 3
          End If
        End Select
        WRITE_DIALOG "v(" & CHK0.optVANI(3).Caption & ") pnt(" & Format$(i, "###0") & ")"
      End If
    Else
          'PRIMO PRINCIPIO
          X = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF2[" & Format$(i + 1, "##0") & "]"))
          WRITE_DIALOG "v(" & Format$(nVANI, "0") & ") pnt(" & Format$(i, "###0") & ")"
    End If
      EE$ = Right$(String(LenStr, " ") & Format(X, "###0.0000"), LenStr)
      'COSTRUZIONE DELLA RIGA DA SCRIVERE NEL FILE
      riga = id$ & "|" & RR$ & "|" & AA$ & "|" & BB$ & "|" & EE$ & "|"
      Print #2, riga
    Else
      Exit For
    End If
    Next i
  Close #2
  Close #3
  'SEGNALAZIONE UTENTE
  WRITE_DIALOG ""
  'SALVATAGGIO NEL VECCHIO PERCORSO
  Call FileCopy(RADICE & "\GAPP4CHK.TXT", PathMIS & "\DEFAULT")
  '
Exit Sub

errLEGGI_CNC_CONTROLLO_ROTORI:
  If (FreeFile > 0) Then Close
  i = Err
  StopRegieEvents
  MsgBox "ERROR: " & Error(i), 48, "SUB: LEGGI_CNC_CONTROLLO_ROTORI"
  ResumeRegieEvents
  Exit Sub
  'Resume Next
  
End Sub

Sub LEGGI_CNC_CONTROLLO_ROTORI_ELICA(RADICE As String)
'
Dim ret     As Long
Dim i       As Integer
Dim j       As Integer
Dim k       As Integer
Dim NomeDIR As String
Dim NomeROT As String
Dim stmp    As String
'
Dim PassoElica  As Double
Dim NumeroDenti As Integer
Dim Nprincipi   As Integer
Dim PRINCIPI()  As Integer
Dim sequenza()  As Integer
Dim Npunti      As Integer
Dim FASCIA      As Double
Dim MISF0()     As Double
Dim MISF1()     As Double
Dim MISF2()     As Double
Dim X0          As Double
Dim X1          As Double
Dim X2          As Double
'
On Error GoTo errLEGGI_CNC_CONTROLLO_ROTORI_ELICA
  '
  'SEGNALAZIONE UTENTE
  WRITE_DIALOG "Reading data from CNC."
  NomeDIR = LEGGI_DIRETTORIO_WKS("ROTORI_ESTERNI")
  NomeROT = LEGGI_NomePezzo("ROTORI_ESTERNI")
  '
  PassoElica = val(OPC_LEGGI_DATO("/ACC/NCK/GUD7/PASSOELICA_G"))
  NumeroDenti = val(OPC_LEGGI_DATO("/Channel/Parameter/R[5]"))
  Npunti = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/NPunti"))
  FASCIA = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/Fascia"))
  '
  i = 1
  stmp = OPC_LEGGI_DATO("/ACC/NCK/MGUD/Principi[" & Format$(i, "##0") & "]")
  stmp = Trim$(stmp)
  Do While (val(stmp) > 0)
    ReDim Preserve PRINCIPI(i)
    PRINCIPI(i) = val(stmp)
    'CONTROLLO PROSSIMO PRINCIPIO
    i = i + 1
    stmp = OPC_LEGGI_DATO("/ACC/NCK/MGUD/Principi[" & Format$(i, "##0") & "]")
    stmp = Trim$(stmp)
  Loop
  If (PRINCIPI(1) > 0) Then
    'CALCOLO DEL NUMERO DEI PRINCIPI CONTROLLATI
    Nprincipi = UBound(PRINCIPI)
    'LETTURA ERRORI
    For j = 0 To Nprincipi - 1
      For i = 1 To Npunti
        k = 201 + i + Npunti * j
        ReDim Preserve MISF1(i + Npunti * j)
        MISF1(i + Npunti * j) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF1[" & Format$(k, "###0") & "]"))
        ReDim Preserve MISF2(i + Npunti * j)
        MISF2(i + Npunti * j) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF2[" & Format$(k, "###0") & "]"))
        ReDim Preserve MISF0(i + Npunti * j)
        MISF0(i + Npunti * j) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF0[" & Format$(k, "###0") & "]"))
        WRITE_DIALOG "p(" & Format$(j, "0") & ") pnt(" & Format$(i, "###0") & ")"
      Next i
    Next j
    'SE ESISTE CANCELLO L'ULTIMO CREATO
    If (Dir$(RADICE & "\RESULTS.SPF") <> "") Then Kill RADICE & "\RESULTS.SPF"
    'CREAZIONE FILE RESULTS
    Open RADICE & "\RESULTS.SPF" For Output As 1
      Print #1, frmt(PassoElica, 4) & " " & Format$(NumeroDenti, "##0")
      Print #1, Format$(Npunti, "##0") & " " & frmt(FASCIA, 1)
      For i = 1 To Nprincipi
        Print #1, Format$(PRINCIPI(i), "##0") & " ";
      Next i
      Print #1, ""    'FINE RIGA PRINCIPI
      Print #1, "0 0" 'PREDISPOSTA PER RAGGI SUI FIANCHI
      Print #1, ""    'RIGA VUOTA
      For i = 1 To Npunti * Nprincipi
        Print #1, frmt(MISF1(i), 4) & " " & frmt(MISF2(i), 4) & " " & frmt(MISF0(i), 4)
      Next i
    Close #1
  End If
  'SEGNALAZIONE UTENTE
  WRITE_DIALOG ""
  '
Exit Sub
'
errLEGGI_CNC_CONTROLLO_ROTORI_ELICA:
  If (FreeFile > 0) Then Close
  i = Err
  StopRegieEvents
  MsgBox "ERROR: " & Error(i), 48, "SUB: LEGGI_CNC_CONTROLLO_ROTORI_ELICA"
  ResumeRegieEvents
  Exit Sub
  'Resume Next
  
End Sub

Sub CALCOLO_MOLA_ROTORI_RAB(ByVal RAGGIO_ESTERNO_MOLA As Double, ByRef Np As Integer, Rmin As Double, Rmax As Double, ByRef R() As Double, ByRef A() As Double, ByRef B() As Double, ByRef XX0() As Double, ByRef YY0() As Double, ByRef TG0() As Double, ByVal TEORICA As String)
'
' input (espliciti):
' RAGGIO_ESTERNO_MOLA
' Np            (numero punti rotore)
' Rmin, Rmax    (raggi min e max rotore)
' R(), A(), B() (coordinate profilo RAB)
'
' input (impliciti):  CORREZIONE_ALTEZZA_DENTE
' ParProfiloRot --- DIAMETRO_PRIMITIVO , NDenteLOC,
'                   ELICA, PASSO, ROTAZIONE, CorSDENTE, NPF1
'                   EAP_F1(), EAP_F2(), RR_F1(), RR_F2(), BB_F1(), BB_F2()
' ParMolaRot --- DEDF(), ADDF(), CorSMOLA, CorX(), CorY()
'
' OUTPUT (ESPLICITI):
' XX0, YY0, TG0 <-- coordinate mola senza correzioni (CorX,CorY,CorSmola,ADD,DED)
' OUTPUT (IMPLICITI):
' ParMolaRot --- XX, YY, TT <-- coordinate mola senza correzioni (CorX,CorY,CorSmola,ADD,DED)
'
Dim CorX As Double
Dim corY As Double
'
Dim CorSDENTE As Double
Dim sCorSDENTE As String
'
Dim CorSMOLA As Double
Dim sCorSMOLA As String
'
'Dim R()  As Double
'Dim A()  As Double
'Dim B()  As Double
Dim XX() As Double
Dim YY() As Double
Dim TG() As Double
Dim XD() As Double
Dim YD() As Double
Dim Xc() As Double
Dim YC() As Double
Dim R0() As Double
Dim L()  As Double
'
Dim stmp  As String
Dim riga  As String
Dim i     As Integer
Dim j     As Integer
'
'Dim Np    As Integer
Dim Tp    As Double
Dim INC   As Double
Dim Rpri  As Double
Dim Ent   As Double
Dim DRM   As Double
Dim RMPD  As Double
Dim Rayon As Double
Dim ALFA  As Double
Dim Beta  As Double
Dim SuRep As Double
'Dim Rmax  As Double
'Dim Rmin  As Double
Dim Ymax  As Double
Dim Ymin  As Double
Dim iYmax  As Integer
Dim iYmin  As Integer
Dim ALTEZZA_DENTE  As Double
Dim Hmax1 As Double
Dim Hmax2 As Double
'
Dim X As Double
Dim Y As Double
'
Dim gama2 As Double
'
Dim SensALF As Integer
Dim Alfa1   As Double
Dim beta1   As Double
Dim Rayon1  As Double
  
Dim Xml As Double
Dim Yml As Double
'
Dim dtmp As Double
Dim Gama As Double
'
Dim T0 As Double
Dim FI0 As Double
'
Dim RQC As Double
'
Dim AA  As Double
Dim BB  As Double
Dim cc  As Double
'
Dim RAD As Double
'
Dim T1 As Double
Dim T2 As Double
Dim Ti As Double
'
Dim TSI   As Double
Dim TSIa  As Double
Dim F     As Double
Dim FP    As Double
'
Dim XS As Double
Dim Ys As Double
Dim Zs As Double
'
Dim Ap  As Double
Dim Bp  As Double
Dim Cp  As Double
Dim ArgSeno  As Double
'
Dim DBB As Database
Dim RSS As Recordset
'
Dim DIAMETRO_PRIMITIVO As Double
Dim sDIAMETRO_PRIMITIVO As String
'
Dim NDenteLOC As Integer
Dim sNDenteLOC As String
'
Dim ELICA As Double
Dim sELICA As String
'
Dim PASSO As Double
Dim sPASSO As String
'
Dim Rotazione As Double
Dim sRotazione As String
'
Dim CORREZIONE_ALTEZZA_DENTE As Double  'serve solo per il calcolo dei pnt 0 e Np+1
Dim sCORREZIONE_ALTEZZA_DENTE As String
'
Dim NPF1  As Integer
Dim sNpF1 As String
'
Dim EAP_F1(4) As Double
Dim EAP_F2(4) As Double
'
Dim RR_F1(4) As Double
Dim RR_F2(4) As Double
'
Dim BB_F1(4) As Double
Dim BB_F2(4) As Double
'
Dim DELTA_A  As Double
Dim Raggio   As Double
Dim Altezza  As Double
Dim TRATTO   As Double
Dim jj       As Integer
'
On Error GoTo errCALCOLO_MOLA_ROTORI_RAB
  '
  'CANCELLO L'ULTIMO CALCOLO
  If Dir$(g_chOemPATH & "\MOLA_F1.XYT") <> "" Then Kill g_chOemPATH & "\MOLA_F1.XYT"
  If Dir$(g_chOemPATH & "\MOLA_F2.XYT") <> "" Then Kill g_chOemPATH & "\MOLA_F2.XYT"
  '
  DIAMETRO_PRIMITIVO = ParProfiloRot.DIAMETRO_PRIMITIVO
  sDIAMETRO_PRIMITIVO = ParProfiloRot.sDIAMETRO_PRIMITIVO
  NDenteLOC = ParProfiloRot.NDenteLOC
  sNDenteLOC = ParProfiloRot.sNDenteLOC
  ELICA = ParProfiloRot.ELICA
  sELICA = ParProfiloRot.sELICA
  PASSO = ParProfiloRot.PASSO
  sPASSO = ParProfiloRot.sPASSO
  Rotazione = ParProfiloRot.Rotazione
  CorSDENTE = ParProfiloRot.CorSDENTE
  sCorSDENTE = ParProfiloRot.sCorSDENTE
  NPF1 = ParProfiloRot.NPF1
  sNpF1 = ParProfiloRot.sNpF1
  For i = 0 To 4
    EAP_F1(i) = ParProfiloRot.EAP_F1(i)
    EAP_F2(i) = ParProfiloRot.EAP_F2(i)
    RR_F1(i) = ParProfiloRot.RR_F1(i)
    RR_F2(i) = ParProfiloRot.RR_F2(i)
    BB_F1(i) = ParProfiloRot.BB_F1(i)
    BB_F2(i) = ParProfiloRot.BB_F2(i)
  Next i
  ALTEZZA_DENTE = (Rmax - Rmin) + CORREZIONE_ALTEZZA_DENTE
  '
  'CALCOLO DEL DIAMETRO PRIMITIVO SE NON E' ASSEGNATO
  If (DIAMETRO_PRIMITIVO <= 0) Then
    'DIAMETRO_PRIMITIVO = Abs(PASSO) * Tan(Abs(ELICA) * PG / 180) / PG
    '*****  calcolo diametro di riferimento  *****
    Dim irif As Integer
    Dim Bmin As Double
    Dim RrifF1 As Double, RrifF2 As Double
    Bmin = 99999999
    For i = 1 To NPF1
      If Abs(B(i)) < Bmin Then
        Bmin = Abs(B(i))
        irif = i
      End If
    Next i
    RrifF1 = R(irif)
    Bmin = 99999999
    For i = NPF1 + 1 To Np
      If Abs(B(i)) < Bmin Then
        Bmin = Abs(B(i))
        irif = i
      End If
    Next i
    RrifF2 = R(irif)
    DIAMETRO_PRIMITIVO = RrifF1 + RrifF2
    '*****  *******************************  *****
  End If
  '
  If DIAMETRO_PRIMITIVO <= 0 Then Exit Sub
  If NDenteLOC <= 0 Then Exit Sub
  '
  Dim INDF(2) As Integer
  'FIANCO 1
  'CORREZIONE DI SPESSORE DENTE
  i = NPF1 - 1
  DELTA_A = (CorSDENTE / Cos(Abs(ELICA) * PG / 180) / 2) / (DIAMETRO_PRIMITIVO / 2)
  DELTA_A = DELTA_A * 180 / PG
  While (i >= 1)
    A(i) = A(i) + DELTA_A
    If (R(i) <= DIAMETRO_PRIMITIVO / 2) Then
      INDF(1) = i
    End If
    i = i - 1
  Wend
  'CORREZIONI PROFILO
  jj = 1
  While (EAP_F1(0) >= Rmin) And (jj <= 4)
  If (EAP_F1(jj - 1) < EAP_F1(jj)) And (EAP_F1(jj) <> 0) Then
    'RASTREMAZIONE JJ
    If (RR_F1(jj) <> 0) Then
      i = NPF1
      While (i >= 1)
        If (R(i) > EAP_F1(jj - 1)) And (R(i) < EAP_F1(jj)) Then
          'RASTREMAZIONE
          DELTA_A = RR_F1(jj) * Abs((R(i) - EAP_F1(jj - 1)) / (EAP_F1(jj) - EAP_F1(jj - 1)))
          DELTA_A = DELTA_A / R(i)
          DELTA_A = DELTA_A * 180 / PG
          A(i) = A(i) + DELTA_A
        End If
        If (R(i) >= EAP_F1(jj)) Then
          'SOMMO L'ULTIMO VALORE SUGLI ALTRI PUNTI
          DELTA_A = RR_F1(jj)
          DELTA_A = DELTA_A / R(i)
          DELTA_A = DELTA_A * 180 / PG
          A(i) = A(i) + DELTA_A
        End If
        i = i - 1
      Wend
    End If
    'BOMBATURA JJ
    If (BB_F1(jj) <> 0) And (EAP_F1(jj) <> 0) Then
      Altezza = (EAP_F1(jj) - EAP_F1(jj - 1)) / 2
      Raggio = ((BB_F1(jj)) ^ 2 + (Altezza) ^ 2) / (2 * BB_F1(jj))
      i = NPF1
      While (i >= 1)
        If (R(i) > EAP_F1(jj - 1)) And (R(i) < EAP_F1(jj)) Then
          TRATTO = R(i) - EAP_F1(jj - 1)
          DELTA_A = Abs(BB_F1(jj)) - Raggio + Sqr((Raggio) ^ 2 - (TRATTO - Altezza) ^ 2)
          DELTA_A = DELTA_A / R(i)
          DELTA_A = DELTA_A * 180 / PG
          If (BB_F1(jj) > 0) Then
            A(i) = A(i) + DELTA_A
          Else
            A(i) = A(i) - DELTA_A
          End If
        End If
        i = i - 1
      Wend
    End If
  End If
  jj = jj + 1
  Wend
  '
  'FIANCO 2
  'CORREZIONE DI SPESSORE DENTE
  i = NPF1 + 1
  DELTA_A = (CorSDENTE / Cos(Abs(ELICA) * PG / 180) / 2) / (DIAMETRO_PRIMITIVO / 2)
  DELTA_A = DELTA_A * 180 / PG
  While (i <= Np)
    A(i) = A(i) - DELTA_A
    If (R(i) <= DIAMETRO_PRIMITIVO / 2) Then
      INDF(2) = i
    End If
    i = i + 1
  Wend
  'CORREZIONI PROFILO
  jj = 1
  While (EAP_F2(0) >= Rmin) And (jj <= 4)
    If (EAP_F2(jj - 1) < EAP_F2(jj)) And (EAP_F2(jj) <> 0) Then
      'RASTREMAZIONE JJ
      If (RR_F2(jj) <> 0) Then
        i = NPF1
        While (i <= Np)
          If (R(i) > EAP_F2(jj - 1)) And (R(i) < EAP_F2(jj)) Then
            DELTA_A = RR_F2(jj) * Abs((R(i) - EAP_F2(jj - 1)) / (EAP_F2(jj) - EAP_F2(jj - 1)))
            DELTA_A = DELTA_A / R(i)
            DELTA_A = DELTA_A * 180 / PG
            A(i) = A(i) - DELTA_A
          End If
          If (R(i) >= EAP_F2(jj)) Then
            DELTA_A = RR_F2(jj)
            DELTA_A = DELTA_A / R(i)
            DELTA_A = DELTA_A * 180 / PG
            A(i) = A(i) - DELTA_A
          End If
          i = i + 1
        Wend
      End If
      'BOMBATURA JJ
      If (BB_F2(jj) <> 0) And (EAP_F2(jj) <> 0) Then
        Altezza = (EAP_F2(jj) - EAP_F2(jj - 1)) / 2
        Raggio = ((BB_F2(jj)) ^ 2 + (Altezza) ^ 2) / (2 * BB_F2(jj))
        i = NPF1
        While (i <= Np)
          If (R(i) > EAP_F2(jj - 1)) And (R(i) < EAP_F2(jj)) Then
            TRATTO = R(i) - EAP_F2(jj - 1)
            DELTA_A = Abs(BB_F2(jj)) - Raggio + Sqr((Raggio) ^ 2 - (TRATTO - Altezza) ^ 2)
            DELTA_A = DELTA_A / R(i)
            DELTA_A = DELTA_A * 180 / PG
            If (BB_F2(jj) < 0) Then
              A(i) = A(i) + DELTA_A
            Else
              A(i) = A(i) - DELTA_A
            End If
          End If
          i = i + 1
        Wend
      End If
    End If
    jj = jj + 1
  Wend
  '
  ReDim ParMolaROT.XX(Np + 1)
  ReDim ParMolaROT.YY(Np + 1)
  ReDim ParMolaROT.TT(Np + 1)
  ReDim XX(Np + 1):  ReDim XX0(Np + 1)
  ReDim YY(Np + 1):  ReDim YY0(Np + 1)
  ReDim TG(Np + 1):  ReDim TG0(Np + 1)
  ReDim XD(Np + 1)
  ReDim YD(Np + 1)
  ReDim Xc(Np + 1)
  ReDim YC(Np + 1)
  ReDim R0(Np + 1)
  ReDim L(Np + 1)
  '
  Ent = RAGGIO_ESTERNO_MOLA + Rmin
  If PASSO < 999999 Then
  INC = Abs(ELICA) * PG / 180  '(90 - Abs(ELICA)) * PG / 180
  Tp = PASSO / (2 * PG)
  Rpri = Tp * Tan(INC)
  DRM = Ent * Rpri
  RMPD = Rpri + Ent
  Else
  INC = 0.0001
  End If
  Ymax = -999999: iYmax = 0
  Ymin = 999999: iYmin = 0
  For i = 1 To Np
    'Rayon = R(i): ALFA = a(i) * PG / 180: Beta = B(i) * PG / 180
    Rayon = R(i): ALFA = (A(i) + Rotazione) * PG / 180: Beta = B(i) * PG / 180
    SuRep = 0
    GoSub 3900
    XX(i) = X: YY(i) = Y: TG(i) = gama2
    XX0(i) = X: YY0(i) = Y: TG0(i) = gama2
    If YY(i) > Ymax Then Ymax = YY(i): iYmax = i
    If YY(i) < Ymin Then Ymin = YY(i): iYmin = i
  Next i
  '
  If NPF1 = 0 Then NPF1 = iYmax
  '
  'PUNTI DEI TRATTI LINEARI (inizializzo)
  YY(0) = YY(1):  XX(0) = XX(1): TG(0) = TG(1)
  YY(Np + 1) = YY(Np):  XX(Np + 1) = XX(Np): TG(Np + 1) = TG(Np)
  '
  'NORMALIZZAZIONE DELLE TANGENTI SUGLI ESTREMI MOLA (Punti 0 e NP+1)
  If TG(1) < 0 Then
    TG(0) = TG(1)
  Else
    TG(0) = TG(1) + 5 * PG / 180
    If TG(0) > 90 * PG / 180 Then TG(0) = PG / 2
  End If
  If TG(Np) > 0 Then
    TG(Np + 1) = TG(Np)
   Else
    TG(Np + 1) = TG(Np) - 5 * PG / 180
    If TG(Np + 1) < -90 * PG / 180 Then TG(Np + 1) = -PG / 2
  End If
  'Calcolo Altezza Fianco
  Hmax1 = 0
  Hmax2 = 0
  If (TG(0) < 0) And (TG(0) > -75 * PG / 180) Then
      Hmax1 = ALTEZZA_DENTE
  End If
  If (TG(Np + 1) > 0) And (TG(Np + 1) < 75 * PG / 180) Then
      Hmax2 = ALTEZZA_DENTE
  End If
  '
  '**************************
  ' AGGIUNTA PUNTI 0 e NP+1
  '**************************
  If Hmax1 = 0 Then
    XX(0) = XX(1) - 1
    YY(0) = YY(1) + (XX(1) - XX(0)) / Tan(TG(0))
   Else
    YY(0) = Ymax - Hmax1
    XX(0) = XX(1) + (YY(1) - YY(0)) * Tan(TG(0))
  End If
  If Hmax2 = 0 Then
    XX(Np + 1) = XX(Np) + 1
    YY(Np + 1) = YY(Np) + (XX(Np) - XX(Np + 1)) / Tan(TG(Np + 1))
  Else
    YY(Np + 1) = Ymax - Hmax2
    XX(Np + 1) = XX(Np) + (YY(Np) - YY(Np + 1)) * Tan(TG(Np + 1))
  End If
  '
  'OUTPUT: COORDINATE MOLA (SENZA CORREZIONI MOLA)
  For i = 0 To Np + 1
    ParMolaROT.XX(i) = XX(i)
    ParMolaROT.YY(i) = YY(i)
    ParMolaROT.TT(i) = TG(i)
  Next i
  '
  Dim DEDF(2) As Double
  Dim ADDF(2) As Double
  'LETTURA PARAMETRI MOLA
  If (TEORICA = "N") Then
    'CORREZIONE DEDENDUM F1
    DEDF(1) = ParMolaROT.DEDF(1)
    'CORREZIONE ADDENDUM F1
    ADDF(1) = ParMolaROT.ADDF(1)
    'CORREZIONE DEDENDUM F2
    DEDF(2) = ParMolaROT.DEDF(2)
    'CORREZIONE ADDENDUM F2
    ADDF(2) = ParMolaROT.ADDF(2)
    'CORREZIONE DI SPESSORE MOLA
    CorSMOLA = ParMolaROT.CorSMOLA
    'CORREZIONE PER PUNTI
    For i = 0 To Np + 1
      CorX = ParMolaROT.CorX(i)
      corY = ParMolaROT.corY(i)
      If (i <= NPF1) Then
        'Correzioni sul fianco 1
        XX(i) = XX(i) + CorX - CorSMOLA / 2
        YY(i) = YY(i) - corY
      Else
        'Correzioni sul fianco 2
        XX(i) = XX(i) - CorX + CorSMOLA / 2
        YY(i) = YY(i) - corY
      End If
    Next i
    'FIANCO 1
    'CORREZIONE DEDENDUM
    i = INDF(1)
    While (i < NPF1)
      YY(i) = YY(i) + DEDF(1) * Abs((YY(i) - YY(INDF(1))) / (YY(NPF1) - YY(INDF(1))))
      i = i + 1
    Wend
    'CORREZIONE ADDENDUM
    i = INDF(1) - 1
    While (i >= 1)
      YY(i) = YY(i) - ADDF(1) * Abs((YY(i) - YY(INDF(1) - 1)) / (YY(1) - YY(INDF(1) - 1)))
      i = i - 1
    Wend
    YY(0) = YY(0) - ADDF(1)
    'FIANCO 2
    'CORREZIONE DEDENDUM
    i = INDF(2)
    While (i > NPF1)
      YY(i) = YY(i) + DEDF(2) * Abs((YY(i) - YY(INDF(2))) / (YY(NPF1) - YY(INDF(2))))
      i = i - 1
    Wend
    'CORREZIONE ADDENDUM
    i = INDF(2) + 1
    While (i <= Np)
      YY(i) = YY(i) - ADDF(2) * Abs((YY(i) - YY(INDF(2) + 1)) / (YY(Np) - YY(INDF(2) + 1)))
      i = i + 1
    Wend
    YY(Np + 1) = YY(Np + 1) - ADDF(2)
  End If
  '
  Dim AngoloSoglia As Single
  AngoloSoglia = val(GetInfo("CALCOLO", "AngoloSoglia", Path_LAVORAZIONE_INI))
  If AngoloSoglia = 0 Then AngoloSoglia = 80
  '
  'PUNTI PER LA VERIFICA PROFILO COME INGRANAGGIO
  Open g_chOemPATH & "\MOLA_F1.TEO" For Output As #1
  For i = 1 To NPF1
    If (Abs(TG(i)) * (180 / PG)) <= AngoloSoglia Then
      Print #1, Left$(frmt(Abs(XX(i)), 6) & String(20, " "), 20);
      Print #1, Left$(frmt(YY(i), 6) & String(20, " "), 20);
      Print #1, Left$(frmt(Abs(TG(i) / PG * 180), 6) & String(20, " "), 20)
    End If
  Next i
  Close #1
  Open g_chOemPATH & "\MOLA_F2.TEO" For Output As #1
  For i = Np To NPF1 + 1 Step -1
    If (Abs(TG(i)) * (180 / PG)) <= AngoloSoglia Then
      Print #1, Left$(frmt(Abs(XX(i)), 6) & String(20, " "), 20);
      Print #1, Left$(frmt(YY(i), 6) & String(20, " "), 20);
      Print #1, Left$(frmt(Abs(TG(i) / PG * 180), 6) & String(20, " "), 20)
    End If
  Next i
  Close #1
  '
  'CALCOLO PUNTO DI SEPARAZIONE CON LA CORREZIONE DI SPESSORE DENTE DI F1
  i = NPF1
    DELTA_A = (CorSDENTE / Cos(Abs(ELICA) * PG / 180) / 2) / (DIAMETRO_PRIMITIVO / 2)
    DELTA_A = DELTA_A * 180 / PG
    Rayon = R(i): ALFA = (A(i) + DELTA_A) * PG / 180: Beta = B(i) * PG / 180
    SuRep = 0
    GoSub 3900
    XX(i) = X: YY(i) = Y: TG(i) = gama2
    'CORREZIONE DEDENDUM
    ''YY(i) = YY(i) + DEDF(1)
  '
  'PUNTI F1 PER LA VERIFICA PROFILO COME ROTORE
  Open g_chOemPATH & "\MOLA_F1.XYT" For Output As #2
    For i = 0 To NPF1
      If XX(i) > 0 Then
        Print #2, "+" & frmt0(Abs(XX(i)), 6) & " ";
      Else
        Print #2, "-" & frmt0(Abs(XX(i)), 6) & " ";
      End If
      If YY(i) > 0 Then
        Print #2, "+" & frmt0(Abs(YY(i)), 6) & " ";
      Else
        Print #2, "-" & frmt0(Abs(YY(i)), 6) & " ";
      End If
      If TG(i) > 0 Then
        Print #2, "+" & frmt0(Abs(TG(i) / PG * 180), 6)
      Else
        Print #2, "-" & frmt0(Abs(TG(i) / PG * 180), 6)
      End If
    Next i
  Close #2
  '
  'CALCOLO PUNTO DI SEPARAZIONE CON LA CORREZIONE DI SPESSORE DENTE DI F2
  i = NPF1
    DELTA_A = (CorSDENTE / Cos(Abs(ELICA) * PG / 180) / 2) / (DIAMETRO_PRIMITIVO / 2)
    DELTA_A = DELTA_A * 180 / PG
    Rayon = R(i): ALFA = (A(i) - DELTA_A) * PG / 180: Beta = B(i) * PG / 180
    SuRep = 0
    GoSub 3900
    XX(i) = X: YY(i) = Y: TG(i) = gama2
    'CORREZIONE DEDENDUM
    'YY(i) = YY(i) + DEDF(2)
  '
  'PUNTI F2 PER LA VERIFICA PROFILO COME ROTORE
  Open g_chOemPATH & "\MOLA_F2.XYT" For Output As #2
    For i = NPF1 To Np + 1
      If XX(i) > 0 Then
        Print #2, "+" & frmt0(Abs(XX(i)), 6) & " ";
      Else
        Print #2, "-" & frmt0(Abs(XX(i)), 6) & " ";
      End If
      If YY(i) > 0 Then
        Print #2, "+" & frmt0(Abs(YY(i)), 6) & " ";
      Else
        Print #2, "-" & frmt0(Abs(YY(i)), 6) & " ";
      End If
      If TG(i) > 0 Then
        Print #2, "+" & frmt0(Abs(TG(i) / PG * 180), 6)
      Else
        Print #2, "-" & frmt0(Abs(TG(i) / PG * 180), 6)
      End If
    Next i
  Close #2
  '
Exit Sub

errCALCOLO_MOLA_ROTORI_RAB:
  If (FreeFile > 0) Then Close
  StopRegieEvents
  MsgBox "ERROR: " & Error(Err), 48, "SUB: CALCOLO_MOLA_ROTORI_RAB"
  ResumeRegieEvents
  Exit Sub

3900  ' ************************************  S/P  ******  CALCUL MEULE
      ' Variables  : RAYON, ALFA, BETA
      ' Constantes : ENT, INC, Pas, SUREP
      '***********************************************************************
If (PASSO < 999999) Then
      SensALF = Sgn(ALFA): If SensALF = 0 Then SensALF = 1
      ALFA = ALFA * SensALF: Beta = Beta * SensALF
      If Abs(Beta) > 0.8 Then
        Alfa1 = ALFA + 0.002
        beta1 = Beta - 0.002
        Rayon1 = Rayon * Sin(Beta) / Sin(beta1)
        GoTo 4030
      End If
      Rayon1 = Rayon - 0.1
      beta1 = FnASN(Rayon * Sin(Beta) / Rayon1)
      Alfa1 = ALFA - beta1 + Beta
4030  GoSub 4090
      Xml = X: Yml = Y
4040  Alfa1 = ALFA: beta1 = Beta: Rayon1 = Rayon
      GoSub 4090
      dtmp = Yml - Y: If dtmp = 0 Then dtmp = 1
      gama2 = Gama * Sgn((X - Xml) / dtmp) * SensALF
      X = X * SensALF
      Y = Y - SuRep * Sin(Abs(gama2))
      X = X - SuRep * Cos(gama2) * Sgn(gama2)
      Return
4090  T0 = PG / 2 - Alfa1
      FI0 = PG / 2 - Alfa1 - beta1
      If FI0 > PG Then FI0 = FI0 - 2 * PG
      If FI0 < -PG Then FI0 = FI0 + 2 * PG
      RQC = Rayon1 * Rayon1 * Cos(T0 - FI0)
      AA = RQC * Sin(T0) + (2 * Tp * Tp + DRM) * Sin(FI0)
      BB = (DRM + Tp * Tp) * Cos(FI0) + RQC * Cos(T0)
      cc = 2 * (-DRM * Sin(FI0) - RQC * Sin(T0) + Rayon1 * RMPD * Cos(T0 - FI0))
      RAD = (BB * BB / AA - cc) / AA
      If (RAD < 0) Then
        WRITE_DIALOG "Radicando negativo per R=" & Rayon1
        Exit Sub
        'GoTo 4290
      End If
      RAD = Sqr(RAD)
      T1 = BB / AA + RAD: T2 = BB / AA - RAD
      Ti = T2
      If Abs(T2) > Abs(T1) Then Ti = T1
      If (T0 < 0) Then Ti = T1
      If (Ti >= PG) Then Ti = PG * 0.9
      'CALCOLO ZERO CON METODO APPROSSIMATO TETA --> TS
      TSI = Ti
      j = 1
      '
4270  F = RQC * Sin(T0 + TSI) + DRM * Sin(FI0 + TSI) + Tp * Tp * TSI * Cos(FI0 + TSI) - Rayon1 * RMPD * Cos(T0 - FI0)
      FP = RQC * Cos(T0 + TSI) + DRM * Cos(FI0 + TSI) + Tp * Tp * Cos(FI0 + TSI) - Tp * Tp * TSI * Sin(FI0 + TSI)
      '
4290  TSIa = TSI - F / FP
      j = j + 1
      If Abs(TSIa - TSI) < 0.0002 Then GoTo 4350
      If j = 80 Then GoTo 4350
      TSI = TSIa
      GoTo 4270
      '
4350  Ti = TSIa
      XS = Rayon1 * Cos(T0 + Ti): Ys = Rayon1 * Sin(T0 + Ti): Zs = Tp * Ti
      'Calcolo della linea di contatto in mm
      L(i) = Ti * (180 / PG) * PASSO / 360
      'Memorizzazione Contatto Max
      If i = 1 Then
        ParMolaROT.LMax = Abs(L(i))
        ParMolaROT.iLMax = i
      Else
        If ParMolaROT.LMax < Abs(L(i)) Then
          ParMolaROT.LMax = Abs(L(i))
          ParMolaROT.iLMax = i
        End If
      End If
      MaxLcontRotori = ParMolaROT.LMax
      'CALCOLO TANGENTE AL PROFILO MOLA IN TETAS
      Ap = Tan(Ti + FI0) / XS: Bp = -1 / XS: Cp = (1 + Ys * Tan(Ti + FI0) / XS) / Tp
      ArgSeno = (Ap / Tan(INC) + Cp) / (Sqr(Ap * Ap + Bp * Bp + Cp * Cp) * (1 / Sin(INC)))
      Gama = PG / 2 - FnASN(ArgSeno): If Gama > PG / 2 Then Gama = PG - Gama
      X = Zs * Sin(INC) + XS * Cos(INC)
      Y = Sqr((XS * Sin(INC) - Zs * Cos(INC)) ^ 2 + (Ys - Ent) ^ 2)
Else
      X = Rayon * Sin(ALFA): Y = Ent - Rayon * Cos(ALFA): gama2 = Beta + ALFA
      L(i) = 0
End If
      '
Return
      
End Sub

Sub CALCOLO_MOLA_ROTORI(ByVal RAGGIO_ESTERNO_MOLA As Double, ByRef Np As Integer, Rmin As Double, Rmax As Double, ByRef R() As Double, ByRef A() As Double, ByRef B() As Double, ByRef XX0() As Double, ByRef YY0() As Double, ByRef TG0() As Double, ByVal TEORICA As String)
'
' input (espliciti):
' RAGGIO_ESTERNO_MOLA
' Np            (numero punti rotore)
' Rmin, Rmax    (raggi min e max rotore)
' R(), A(), B() (coordinate profilo RAB)
'
' input (impliciti):  CORREZIONE_ALTEZZA_DENTE
' ParProfiloRot --- DIAMETRO_PRIMITIVO , NDenteLOC,
'                   ELICA, PASSO, ROTAZIONE, CorSDENTE, NPF1
'                   EAP_F1(), EAP_F2(), RR_F1(), RR_F2(), BB_F1(), BB_F2()
' ParMolaRot --- DEDF(), ADDF(), CorSMOLA, CorX(), CorY()
'
' OUTPUT (ESPLICITI):
' XX0, YY0, TG0 <-- coordinate mola senza correzioni (CorX,CorY,CorSmola,ADD,DED)
' OUTPUT (IMPLICITI):
' ParMolaRot --- XX, YY, TT <-- coordinate mola senza correzioni (CorX,CorY,CorSmola,ADD,DED)
'
Dim CorSDENTE   As Double
Dim sCorSDENTE  As String
Dim CorSMOLA    As Double
Dim sCorSMOLA   As String
'
'Dim R()  As Double
'Dim A()  As Double
'Dim B()  As Double
Dim XX() As Double
Dim YY() As Double
Dim TG() As Double
Dim XD() As Double
Dim YD() As Double
Dim Xc() As Double
Dim YC() As Double
Dim R0() As Double
Dim L()  As Double
'
Dim stmp  As String
Dim riga  As String
Dim i     As Integer
Dim j     As Integer
'
'Dim Np             As Integer
Dim Tp              As Double
Dim INC             As Double
Dim Rpri            As Double
Dim Ent             As Double
Dim DRM             As Double
Dim RMPD            As Double
Dim Rayon           As Double
Dim ALFA            As Double
Dim Beta            As Double
Dim SuRep           As Double
'Dim Rmax           As Double
'Dim Rmin           As Double
Dim Ymax            As Double
Dim Ymin            As Double
Dim iYmax           As Integer
Dim iYmin           As Integer
Dim ALTEZZA_DENTE   As Double
Dim Hmax1           As Double
Dim Hmax2           As Double
'
Dim X As Double
Dim Y As Double
'
Dim gama2 As Double
'
Dim SensALF As Integer
Dim Alfa1   As Double
Dim beta1   As Double
Dim Rayon1  As Double
  
Dim Xml As Double
Dim Yml As Double
'
Dim dtmp As Double
Dim Gama As Double
'
Dim T0 As Double
Dim FI0 As Double
'
Dim RQC As Double
'
Dim AA  As Double
Dim BB  As Double
Dim cc  As Double
'
Dim RAD As Double
'
Dim T1 As Double
Dim T2 As Double
Dim Ti As Double
'
Dim TSI   As Double
Dim TSIa  As Double
Dim F     As Double
Dim FP    As Double
'
Dim XS As Double
Dim Ys As Double
Dim Zs As Double
'
Dim Ap  As Double
Dim Bp  As Double
Dim Cp  As Double
Dim ArgSeno  As Double
'
Dim DBB As Database
Dim RSS As Recordset
'
Dim DIAMETRO_PRIMITIVO As Double
Dim sDIAMETRO_PRIMITIVO As String
'
Dim NDenteLOC As Integer
Dim sNDenteLOC As String
'
Dim ELICA As Double
Dim sELICA As String
'
Dim PASSO As Double
Dim sPASSO As String
'
Dim Rotazione As Double
Dim sRotazione As String
'
Dim CORREZIONE_ALTEZZA_DENTE As Double  'serve solo per il calcolo dei pnt 0 e Np+1
Dim sCORREZIONE_ALTEZZA_DENTE As String
'
Dim NPF1  As Integer
Dim sNpF1 As String
'
Dim EAP_F1(4) As Double
Dim EAP_F2(4) As Double
'
Dim RR_F1(4) As Double
Dim RR_F2(4) As Double
'
Dim BB_F1(4) As Double
Dim BB_F2(4) As Double
'
Dim DELTA_A  As Double
Dim Raggio   As Double
Dim Altezza  As Double
Dim TRATTO   As Double
Dim jj       As Integer
'
On Error GoTo errCALCOLO_MOLA_ROTORI
  '
  'CANCELLO L'ULTIMO CALCOLO
  If Dir$(g_chOemPATH & "\MOLA_F1.XYT") <> "" Then Kill g_chOemPATH & "\MOLA_F1.XYT"
  If Dir$(g_chOemPATH & "\MOLA_F2.XYT") <> "" Then Kill g_chOemPATH & "\MOLA_F2.XYT"
  '
  ReDim ParMolaROT.XX(0)
  ReDim ParMolaROT.YY(0)
  ReDim ParMolaROT.TT(0)
  '
  ReDim XX(0):  ReDim XX0(0)
  ReDim YY(0):  ReDim YY0(0)
  ReDim TG(0):  ReDim TG0(0)
  '
  ReDim XD(0)
  ReDim YD(0)
  ReDim Xc(0)
  ReDim YC(0)
  ReDim R0(0)
  ReDim L(0)
  '
  'LETTURA PARAMETRI PROFILO
  DIAMETRO_PRIMITIVO = ParProfiloRot.DIAMETRO_PRIMITIVO
  sDIAMETRO_PRIMITIVO = ParProfiloRot.sDIAMETRO_PRIMITIVO
  NDenteLOC = ParProfiloRot.NDenteLOC
  sNDenteLOC = ParProfiloRot.sNDenteLOC
  ELICA = ParProfiloRot.ELICA
  sELICA = ParProfiloRot.sELICA
  PASSO = ParProfiloRot.PASSO
  sPASSO = ParProfiloRot.sPASSO
  If PASSO = 0 Then
    WRITE_DIALOG ("WARNING: " & sPASSO & " = " & PASSO)
    Exit Sub
  End If
  NPF1 = ParProfiloRot.NPF1
  sNpF1 = ParProfiloRot.sNpF1
  Rotazione = 0 'ParProfiloRot.Rotazione
  CORREZIONE_ALTEZZA_DENTE = ParProfiloRot.CORREZIONE_ALTEZZA_DENTE
  sCORREZIONE_ALTEZZA_DENTE = ParProfiloRot.sCORREZIONE_ALTEZZA_DENTE
  CorSDENTE = ParProfiloRot.CorSDENTE
  sCorSDENTE = ParProfiloRot.sCorSDENTE
  For i = 0 To 4
    EAP_F1(i) = ParProfiloRot.EAP_F1(i)
    EAP_F2(i) = ParProfiloRot.EAP_F2(i)
    RR_F1(i) = ParProfiloRot.RR_F1(i)
    RR_F2(i) = ParProfiloRot.RR_F2(i)
    BB_F1(i) = ParProfiloRot.BB_F1(i)
    BB_F2(i) = ParProfiloRot.BB_F2(i)
  Next i
  ALTEZZA_DENTE = (Rmax - Rmin) + CORREZIONE_ALTEZZA_DENTE
  '
  'CALCOLO DEL DIAMETRO PRIMITIVO SE NON E' ASSEGNATO
  If (DIAMETRO_PRIMITIVO <= 0) Then
    'DIAMETRO_PRIMITIVO = Abs(PASSO) * Tan(Abs(ELICA) * PG / 180) / PG
    '*****  calcolo diametro di riferimento  *****
    Dim irif    As Integer
    Dim Bmin    As Double
    Dim RrifF1  As Double
    Dim RrifF2  As Double
    Bmin = 99999999
    For i = 1 To NPF1
      If Abs(B(i)) < Bmin Then
        Bmin = Abs(B(i))
        irif = i
      End If
    Next i
    RrifF1 = R(irif)
    Bmin = 99999999
    For i = NPF1 + 1 To Np
      If Abs(B(i)) < Bmin Then
        Bmin = Abs(B(i))
        irif = i
      End If
    Next i
    RrifF2 = R(irif)
    DIAMETRO_PRIMITIVO = RrifF1 + RrifF2
  End If
  '
  Dim INDF(2) As Integer
  'FIANCO 1
  'CORREZIONE DI SPESSORE DENTE
  i = NPF1 - 1
  DELTA_A = (CorSDENTE / Cos(Abs(ELICA) * PG / 180) / 2) / (DIAMETRO_PRIMITIVO / 2)
  DELTA_A = DELTA_A * 180 / PG
  While (i >= 1)
    A(i) = A(i) + DELTA_A
    If (R(i) <= DIAMETRO_PRIMITIVO / 2) Then
      INDF(1) = i
    End If
    i = i - 1
  Wend
  'CORREZIONI PROFILO
  jj = 1
  While (EAP_F1(0) >= Rmin) And (jj <= 4)
  If (EAP_F1(jj - 1) < EAP_F1(jj)) And (EAP_F1(jj) <> 0) Then
    'RASTREMAZIONE JJ
    If (RR_F1(jj) <> 0) Then
      i = NPF1
      While (i >= 1)
        If (R(i) > EAP_F1(jj - 1)) And (R(i) < EAP_F1(jj)) Then
          'RASTREMAZIONE
          DELTA_A = RR_F1(jj) * Abs((R(i) - EAP_F1(jj - 1)) / (EAP_F1(jj) - EAP_F1(jj - 1)))
          DELTA_A = DELTA_A / R(i)
          DELTA_A = DELTA_A * 180 / PG
          A(i) = A(i) + DELTA_A
        End If
        If (R(i) >= EAP_F1(jj)) Then
          'SOMMO L'ULTIMO VALORE SUGLI ALTRI PUNTI
          DELTA_A = RR_F1(jj)
          DELTA_A = DELTA_A / R(i)
          DELTA_A = DELTA_A * 180 / PG
          A(i) = A(i) + DELTA_A
        End If
        i = i - 1
      Wend
    End If
    'BOMBATURA JJ
    If (BB_F1(jj) <> 0) And (EAP_F1(jj) <> 0) Then
      Altezza = (EAP_F1(jj) - EAP_F1(jj - 1)) / 2
      Raggio = ((BB_F1(jj)) ^ 2 + (Altezza) ^ 2) / (2 * BB_F1(jj))
      i = NPF1
      While (i >= 1)
        If (R(i) > EAP_F1(jj - 1)) And (R(i) < EAP_F1(jj)) Then
         TRATTO = R(i) - EAP_F1(jj - 1)
          DELTA_A = Abs(BB_F1(jj)) - Raggio + Sqr((Raggio) ^ 2 - (TRATTO - Altezza) ^ 2)
          DELTA_A = DELTA_A / R(i)
          DELTA_A = DELTA_A * 180 / PG
          If (BB_F1(jj) > 0) Then
            A(i) = A(i) + DELTA_A
          Else
            A(i) = A(i) - DELTA_A
          End If
        End If
        i = i - 1
      Wend
    End If
  End If
  jj = jj + 1
  Wend
  'FIANCO 2
  'CORREZIONE DI SPESSORE DENTE
  i = NPF1 + 1
  DELTA_A = (CorSDENTE / Cos(Abs(ELICA) * PG / 180) / 2) / (DIAMETRO_PRIMITIVO / 2)
  DELTA_A = DELTA_A * 180 / PG
  While (i <= Np)
    A(i) = A(i) - DELTA_A
    If (R(i) <= DIAMETRO_PRIMITIVO / 2) Then
      INDF(2) = i
    End If
    i = i + 1
  Wend
  'CORREZIONI PROFILO
  jj = 1
  While (EAP_F2(0) >= Rmin) And (jj <= 4)
    If (EAP_F2(jj - 1) < EAP_F2(jj)) And (EAP_F2(jj) <> 0) Then
      'RASTREMAZIONE JJ
      If (RR_F2(jj) <> 0) Then
        i = NPF1
        While (i <= Np)
          If (R(i) > EAP_F2(jj - 1)) And (R(i) < EAP_F2(jj)) Then
            DELTA_A = RR_F2(jj) * Abs((R(i) - EAP_F2(jj - 1)) / (EAP_F2(jj) - EAP_F2(jj - 1)))
            DELTA_A = DELTA_A / R(i)
            DELTA_A = DELTA_A * 180 / PG
            A(i) = A(i) - DELTA_A
          End If
          If (R(i) >= EAP_F2(jj)) Then
            DELTA_A = RR_F2(jj)
            DELTA_A = DELTA_A / R(i)
            DELTA_A = DELTA_A * 180 / PG
            A(i) = A(i) - DELTA_A
          End If
          i = i + 1
        Wend
      End If
      'BOMBATURA JJ
      If (BB_F2(jj) <> 0) And (EAP_F2(jj) <> 0) Then
        Altezza = (EAP_F2(jj) - EAP_F2(jj - 1)) / 2
        Raggio = ((BB_F2(jj)) ^ 2 + (Altezza) ^ 2) / (2 * BB_F2(jj))
        i = NPF1
        While (i <= Np)
          If (R(i) > EAP_F2(jj - 1)) And (R(i) < EAP_F2(jj)) Then
            TRATTO = R(i) - EAP_F2(jj - 1)
            DELTA_A = Abs(BB_F2(jj)) - Raggio + Sqr((Raggio) ^ 2 - (TRATTO - Altezza) ^ 2)
            DELTA_A = DELTA_A / R(i)
            DELTA_A = DELTA_A * 180 / PG
            If (BB_F2(jj) < 0) Then
              A(i) = A(i) + DELTA_A
            Else
              A(i) = A(i) - DELTA_A
            End If
          End If
          i = i + 1
        Wend
      End If
    End If
    jj = jj + 1
  Wend
  '
  ReDim ParMolaROT.XX(Np + 1)
  ReDim ParMolaROT.YY(Np + 1)
  ReDim ParMolaROT.TT(Np + 1)
  '
  ReDim XX(Np + 1):  ReDim XX0(Np + 1)
  ReDim YY(Np + 1):  ReDim YY0(Np + 1)
  ReDim TG(Np + 1):  ReDim TG0(Np + 1)
  '
  ReDim XD(Np + 1)
  ReDim YD(Np + 1)
  ReDim Xc(Np + 1)
  ReDim YC(Np + 1)
  ReDim R0(Np + 1)
  ReDim L(Np + 1)
  '
  Ent = RAGGIO_ESTERNO_MOLA + Rmin
  If PASSO < 999999 Then
  INC = Abs(ELICA) * PG / 180
  Tp = PASSO / (2 * PG)
  Rpri = Tp * Tan(INC)
  DRM = Ent * Rpri
  RMPD = Rpri + Ent
  Else
  INC = 0.0001
  End If
  '
  Ymax = -999999: iYmax = 0
  Ymin = 999999: iYmin = 0
  For i = 1 To Np
    Rayon = R(i): ALFA = (A(i) + Rotazione) * PG / 180: Beta = B(i) * PG / 180
    SuRep = 0
    GoSub 3900
    XX(i) = X: YY(i) = Y: TG(i) = gama2
    XX0(i) = X: YY0(i) = Y: TG0(i) = gama2
    If YY(i) > Ymax Then Ymax = YY(i): iYmax = i
    If YY(i) < Ymin Then Ymin = YY(i): iYmin = i
  Next i
  '
  If NPF1 = 0 Then NPF1 = iYmax
  '
  'PUNTI DEI TRATTI LINEARI (inizializzo)
  YY(0) = YY(1):  XX(0) = XX(1): TG(0) = TG(1)
  YY(Np + 1) = YY(Np):  XX(Np + 1) = XX(Np): TG(Np + 1) = TG(Np)
  '
  'NORMALIZZAZIONE DELLE TANGENTI SUGLI ESTREMI MOLA (Punti 0 e NP+1)
  If TG(1) < 0 Then
    TG(0) = TG(1)
  Else
    TG(0) = TG(1) + 5 * PG / 180
    If TG(0) > 90 * PG / 180 Then TG(0) = PG / 2
  End If
  If TG(Np) > 0 Then
    TG(Np + 1) = TG(Np)
   Else
    TG(Np + 1) = TG(Np) - 5 * PG / 180
    If TG(Np + 1) < -90 * PG / 180 Then TG(Np + 1) = -PG / 2
  End If
  'Calcolo Altezza Fianco
  Hmax1 = 0
  Hmax2 = 0
  If (TG(0) < 0) And (TG(0) > -75 * PG / 180) Then
      Hmax1 = ALTEZZA_DENTE
  End If
  If (TG(Np + 1) > 0) And (TG(Np + 1) < 75 * PG / 180) Then
      Hmax2 = ALTEZZA_DENTE
  End If
  '
AGGIUNGI_PUNTO:
  '**************************
  ' AGGIUNTA PUNTI 0 e NP+1
  '**************************
  If Hmax1 = 0 Then
    XX(0) = XX(1) - 1
    YY(0) = YY(1) + (XX(1) - XX(0)) / Tan(TG(0))
   Else
    YY(0) = Ymax - Hmax1
    XX(0) = XX(1) + (YY(1) - YY(0)) * Tan(TG(0))
  End If
  If Hmax2 = 0 Then
    XX(Np + 1) = XX(Np) + 1
    YY(Np + 1) = YY(Np) + (XX(Np) - XX(Np + 1)) / Tan(TG(Np + 1))
  Else
    YY(Np + 1) = Ymax - Hmax2
    XX(Np + 1) = XX(Np) + (YY(Np) - YY(Np + 1)) * Tan(TG(Np + 1))
  End If
  '
  'OUTPUT: COORDINATE MOLA (SENZA CORREZIONI MOLA)
  For i = 0 To Np + 1
    ParMolaROT.XX(i) = XX(i)
    ParMolaROT.YY(i) = YY(i)
    ParMolaROT.TT(i) = TG(i)
  Next i
  '
  Dim DEDF(2) As Double
  Dim ADDF(2) As Double
  '
  'LETTURA PARAMETRI MOLA
  If (TEORICA = "N") Then
    'CORREZIONE DEDENDUM F1
    DEDF(1) = ParMolaROT.DEDF(1)
    'CORREZIONE ADDENDUM F1
    ADDF(1) = ParMolaROT.ADDF(1)
    'CORREZIONE DEDENDUM F2
    DEDF(2) = ParMolaROT.DEDF(2)
    'CORREZIONE ADDENDUM F2
    ADDF(2) = ParMolaROT.ADDF(2)
    'CORREZIONE DI SPESSORE MOLA
    CorSMOLA = ParMolaROT.CorSMOLA
    'CORREZIONE PER PUNTI
    For i = 0 To Np + 1
      If (i <= NPF1) Then
        'Correzioni sul fianco 1
        XX(i) = XX(i) + ParMolaROT.CorX(i) - CorSMOLA / 2
        YY(i) = YY(i) - ParMolaROT.corY(i)
      Else
        'Correzioni sul fianco 2
        XX(i) = XX(i) - ParMolaROT.CorX(i) + CorSMOLA / 2
        YY(i) = YY(i) - ParMolaROT.corY(i)
      End If
    Next i
    'FIANCO 1
    'CORREZIONE DEDENDUM
    i = INDF(1)
    While (i < NPF1)
      YY(i) = YY(i) + DEDF(1) * Abs((YY(i) - YY(INDF(1))) / (YY(NPF1) - YY(INDF(1))))
      i = i + 1
    Wend
    'CORREZIONE ADDENDUM
    i = INDF(1) - 1
    While (i >= 1)
      YY(i) = YY(i) - ADDF(1) * Abs((YY(i) - YY(INDF(1) - 1)) / (YY(1) - YY(INDF(1) - 1)))
      i = i - 1
    Wend
    YY(0) = YY(0) - ADDF(1)
    'FIANCO 2
    'CORREZIONE DEDENDUM
    i = INDF(2)
    While (i > NPF1)
      YY(i) = YY(i) + DEDF(2) * Abs((YY(i) - YY(INDF(2))) / (YY(NPF1) - YY(INDF(2))))
      i = i - 1
    Wend
    'CORREZIONE ADDENDUM
    i = INDF(2) + 1
    While (i <= Np)
      YY(i) = YY(i) - ADDF(2) * Abs((YY(i) - YY(INDF(2) + 1)) / (YY(Np) - YY(INDF(2) + 1)))
      i = i + 1
    Wend
    YY(Np + 1) = YY(Np + 1) - ADDF(2)
  End If
  '
  Dim AngoloSoglia As Single
  '
  AngoloSoglia = val(GetInfo("CALCOLO", "AngoloSoglia", Path_LAVORAZIONE_INI))
  If AngoloSoglia = 0 Then AngoloSoglia = 80

  'PUNTI PER LA VERIFICA PROFILO COME INGRANAGGIO
  Open g_chOemPATH & "\MOLA_F1.TEO" For Output As #1
  For i = 1 To NPF1
    If (Abs(TG(i)) * (180 / PG)) <= AngoloSoglia Then
      Print #1, Left$(frmt(Abs(XX(i)), 6) & String(20, " "), 20);
      Print #1, Left$(frmt(YY(i), 6) & String(20, " "), 20);
      Print #1, Left$(frmt(Abs(TG(i) / PG * 180), 6) & String(20, " "), 20)
    End If
  Next i
  Close #1
  Open g_chOemPATH & "\MOLA_F2.TEO" For Output As #1
  For i = Np To NPF1 + 1 Step -1
    If (Abs(TG(i)) * (180 / PG)) <= AngoloSoglia Then
      Print #1, Left$(frmt(Abs(XX(i)), 6) & String(20, " "), 20);
      Print #1, Left$(frmt(YY(i), 6) & String(20, " "), 20);
      Print #1, Left$(frmt(Abs(TG(i) / PG * 180), 6) & String(20, " "), 20)
    End If
  Next i
  Close #1

  'CALCOLO PUNTO DI SEPARAZIONE CON LA CORREZIONE DI SPESSORE DENTE DI F1
  i = NPF1
    DELTA_A = (CorSDENTE / Cos(Abs(ELICA) * PG / 180) / 2) / (DIAMETRO_PRIMITIVO / 2)
    DELTA_A = DELTA_A * 180 / PG
    Rayon = R(i): ALFA = (A(i) + DELTA_A) * PG / 180: Beta = B(i) * PG / 180
    SuRep = 0
    GoSub 3900
    XX(i) = X: YY(i) = Y: TG(i) = gama2
    'CORREZIONE DEDENDUM
    YY(i) = YY(i) + DEDF(1)

  'PUNTI F1 PER LA VERIFICA PROFILO COME ROTORE
  Open g_chOemPATH & "\MOLA_F1.XYT" For Output As #2
    For i = 0 To NPF1
      If XX(i) > 0 Then
        Print #2, "+" & frmt0(Abs(XX(i)), 6) & " ";
      Else
        Print #2, "-" & frmt0(Abs(XX(i)), 6) & " ";
      End If
      If YY(i) > 0 Then
        Print #2, "+" & frmt0(Abs(YY(i)), 6) & " ";
      Else
        Print #2, "-" & frmt0(Abs(YY(i)), 6) & " ";
      End If
      If TG(i) > 0 Then
        Print #2, "+" & frmt0(Abs(TG(i) / PG * 180), 6)
      Else
        Print #2, "-" & frmt0(Abs(TG(i) / PG * 180), 6)
      End If
    Next i
  Close #2

  'CALCOLO PUNTO DI SEPARAZIONE CON LA CORREZIONE DI SPESSORE DENTE DI F2
  i = NPF1
    DELTA_A = (CorSDENTE / Cos(Abs(ELICA) * PG / 180) / 2) / (DIAMETRO_PRIMITIVO / 2)
    DELTA_A = DELTA_A * 180 / PG
    Rayon = R(i): ALFA = (A(i) - DELTA_A) * PG / 180: Beta = B(i) * PG / 180
    SuRep = 0
    GoSub 3900
    XX(i) = X: YY(i) = Y: TG(i) = gama2
    'CORREZIONE DEDENDUM
    YY(i) = YY(i) + DEDF(2)
    
  'PUNTI F2 PER LA VERIFICA PROFILO COME ROTORE
  Open g_chOemPATH & "\MOLA_F2.XYT" For Output As #2
    For i = NPF1 To Np + 1
      If XX(i) > 0 Then
        Print #2, "+" & frmt0(Abs(XX(i)), 6) & " ";
      Else
        Print #2, "-" & frmt0(Abs(XX(i)), 6) & " ";
      End If
      If YY(i) > 0 Then
        Print #2, "+" & frmt0(Abs(YY(i)), 6) & " ";
      Else
        Print #2, "-" & frmt0(Abs(YY(i)), 6) & " ";
      End If
      If TG(i) > 0 Then
        Print #2, "+" & frmt0(Abs(TG(i) / PG * 180), 6)
      Else
        Print #2, "-" & frmt0(Abs(TG(i) / PG * 180), 6)
      End If
    Next i
  Close #2
  '
Exit Sub

errCALCOLO_MOLA_ROTORI:
  If (FreeFile > 0) Then Close
  MsgBox "ERROR: " & Error(Err), 48, "SUB: CALCOLO_MOLA_ROTORI"
  Exit Sub
  'Resume Next

3900  ' ************************************  S/P  ******  CALCUL MEULE
      ' Variables  : RAYON, ALFA, BETA
      ' Constantes : ENT, INC, Pas, SUREP
      '***********************************************************************
If (PASSO < 9999) Then
      SensALF = Sgn(ALFA): If SensALF = 0 Then SensALF = 1
      ALFA = ALFA * SensALF: Beta = Beta * SensALF
      If Abs(Beta) > 0.8 Then
        Alfa1 = ALFA + 0.002
        beta1 = Beta - 0.002
        Rayon1 = Rayon * Sin(Beta) / Sin(beta1)
        GoTo 4030
      End If
      Rayon1 = Rayon - 0.1
      beta1 = FnASN(Rayon * Sin(Beta) / Rayon1)
      Alfa1 = ALFA - beta1 + Beta
4030  GoSub 4090
      Xml = X: Yml = Y
4040  Alfa1 = ALFA: beta1 = Beta: Rayon1 = Rayon
      GoSub 4090
      dtmp = Yml - Y: If dtmp = 0 Then dtmp = 1
      gama2 = Gama * Sgn((X - Xml) / dtmp) * SensALF
      X = X * SensALF
      Y = Y - SuRep * Sin(Abs(gama2))
      X = X - SuRep * Cos(gama2) * Sgn(gama2)
      Return
4090  T0 = PG / 2 - Alfa1
      FI0 = PG / 2 - Alfa1 - beta1
      If FI0 > PG Then FI0 = FI0 - 2 * PG
      If FI0 < -PG Then FI0 = FI0 + 2 * PG
      RQC = Rayon1 * Rayon1 * Cos(T0 - FI0)
      AA = RQC * Sin(T0) + (2 * Tp * Tp + DRM) * Sin(FI0)
      BB = (DRM + Tp * Tp) * Cos(FI0) + RQC * Cos(T0)
      cc = 2 * (-DRM * Sin(FI0) - RQC * Sin(T0) + Rayon1 * RMPD * Cos(T0 - FI0))
      RAD = (BB * BB / AA - cc) / AA
      If (RAD < 0) Then
        WRITE_DIALOG "Radicando negativo per R=" & Rayon1
        Exit Sub
        'GoTo 4290
      End If
      RAD = Sqr(RAD)
      T1 = BB / AA + RAD: T2 = BB / AA - RAD
      Ti = T2
      If Abs(T2) > Abs(T1) Then Ti = T1
      If (T0 < 0) Then Ti = T1
      If (Ti >= PG) Then Ti = PG * 0.9
      'CALCOLO ZERO CON METODO APPROSSIMATO TETA --> TS
      TSI = Ti
      j = 1
4270  F = RQC * Sin(T0 + TSI) + DRM * Sin(FI0 + TSI) + Tp * Tp * TSI * Cos(FI0 + TSI) - Rayon1 * RMPD * Cos(T0 - FI0)
      FP = RQC * Cos(T0 + TSI) + DRM * Cos(FI0 + TSI) + Tp * Tp * Cos(FI0 + TSI) - Tp * Tp * TSI * Sin(FI0 + TSI)
4290  TSIa = TSI - F / FP
      j = j + 1
      If Abs(TSIa - TSI) < 0.0002 Then GoTo 4350
      If j = 80 Then GoTo 4350
      TSI = TSIa
      GoTo 4270
4350  Ti = TSIa
      XS = Rayon1 * Cos(T0 + Ti): Ys = Rayon1 * Sin(T0 + Ti): Zs = Tp * Ti
      'Calcolo della linea di contatto in mm
      L(i) = Ti * (180 / PG) * PASSO / 360
      'Memorizzazione Contatto Max
      If i = 1 Then
        ParMolaROT.LMax = Abs(L(i))
        ParMolaROT.iLMax = i
      Else
        If ParMolaROT.LMax < Abs(L(i)) Then
          ParMolaROT.LMax = Abs(L(i))
          ParMolaROT.iLMax = i
        End If
      End If
      MaxLcontRotori = ParMolaROT.LMax
      'CALCOLO TANGENTE AL PROFILO MOLA IN TETAS
      Ap = Tan(Ti + FI0) / XS: Bp = -1 / XS: Cp = (1 + Ys * Tan(Ti + FI0) / XS) / Tp
      ArgSeno = (Ap / Tan(INC) + Cp) / (Sqr(Ap * Ap + Bp * Bp + Cp * Cp) * (1 / Sin(INC)))
      Gama = PG / 2 - FnASN(ArgSeno): If Gama > PG / 2 Then Gama = PG - Gama
      X = Zs * Sin(INC) + XS * Cos(INC)
      Y = Sqr((XS * Sin(INC) - Zs * Cos(INC)) ^ 2 + (Ys - Ent) ^ 2)
Else
      X = Rayon * Sin(ALFA): Y = Ent - Rayon * Cos(ALFA): gama2 = Beta + ALFA
      L(i) = 0
End If
Return
      
End Sub

Sub CALCOLO_PROFILO_MOLA_ROTORI(ByVal Dm As Double, ByVal CORHD As Single)
'
Dim R()   As Double
Dim A()   As Double
Dim B()   As Double
Dim XX0() As Double
Dim YY0() As Double
Dim TG0() As Double
Dim Rmin  As Double
Dim Rmax  As Double
Dim Np    As Integer
'
On Error GoTo errCALCOLO_PROFILO_MOLA_ROTORI
  '
  Call ROTORI1.LETTURA_PARPROFILO_ROT(Np, R(), A(), B(), Rmin, Rmax)
  Call ROTORI1.LETTURA_PARMOLA_ROT(Np)
  Call CALCOLO_MOLA_ROTORI(Dm / 2, Np, Rmin, Rmax, R, A, B, XX0, YY0, TG0, "N")
  '
Exit Sub

errCALCOLO_PROFILO_MOLA_ROTORI:
  If FreeFile > 0 Then Close
  StopRegieEvents
  MsgBox "ERROR: " & Error(Err), 48, "SUB: CALCOLO_PROFILO_MOLA_ROTORI"
  ResumeRegieEvents
  Exit Sub

End Sub

Sub CALCOLO_MOLA_PROFILO_RAB_ESTERNI(ByVal Dm As Double, ByVal CORHD As Single)
'
Dim R()   As Double
Dim A()   As Double
Dim B()   As Double
Dim XX0() As Double
Dim YY0() As Double
Dim TG0() As Double
Dim Rmin  As Double
Dim Rmax  As Double
Dim i     As Integer
Dim Np    As Integer
'
On Error GoTo errCALCOLO_MOLA_PROFILO_RAB_ESTERNI
  '
  Call ROTORI1.LETTURA_PARPROFILO_RAB(Np, R(), A(), B(), Rmin, Rmax)
  Call ROTORI1.LETTURA_PARMOLA_RAB(Np)
  Call CALCOLO_MOLA_ROTORI_RAB(Dm / 2, Np, Rmin, Rmax, R, A, B, XX0, YY0, TG0, "N")
  '
Exit Sub

errCALCOLO_MOLA_PROFILO_RAB_ESTERNI:
  If FreeFile > 0 Then Close
  StopRegieEvents
  MsgBox "ERROR: " & Error(Err), 48, "SUB: CALCOLO_MOLA_PROFILO_RAB_ESTERNI"
  ResumeRegieEvents
  Exit Sub

End Sub

Sub CONTROLLO_ROTORI_VISUALIZZAZIONE()
'
Dim k                  As Integer
Dim stmp               As String
Dim stmp1              As String
Dim stmp2              As String
Dim TextFont           As String
Dim Session            As IMCDomain
Dim PERCORSO           As String
Dim COORDINATE         As String
Dim RADICE             As String
Dim PERCORSO_CONTROLLI As String
Dim PERCORSO_PROFILI   As String
Dim sequenza           As String
Dim TY_ROT             As String
Dim PASSO              As Double
Dim CONTROLLORE        As String
Dim DM_ROT             As String
Dim INVERSIONE         As Integer
Dim IdxCHKtmp()        As Integer
'
Dim tX()               As Double
Dim tY()               As Double
Dim tN()               As Integer
'
Dim tX1()              As Double
Dim tY1()              As Double
Dim tN1()              As Integer
'
Dim tXn()              As Double
Dim tYn()              As Double
Dim tNn()              As Double
Dim tXp()              As Double
Dim tYp()              As Double
Dim tNp()              As Double
'
Dim dtmp               As Double
Dim Raggio()           As Double
Dim ANGOLO()           As Double
Dim PRESSIONE()        As Double
Dim npt                As Integer
Dim VANO               As Integer
Dim sVANO              As String
Dim DDEDIAMETRO(2)     As String
Const POSX = 50
Const POSY = 150
'
Dim Np            As Integer
Dim NPF1          As Integer
Dim NP1           As Integer
Dim NP2           As Integer
Dim NPM1          As Integer
Dim NPM2          As Integer
Dim corY()        As Double
Dim CorZ()        As Double
Dim ER()          As Double
Dim SCALA_ERRORI  As Double
'
On Error GoTo ErrCONTROLLO_ROTORI_VISUALIZZAZIONE
  '
  k = CHK0.Combo1.ListIndex
  stmp = GetInfo("CONTROLLO", "itemCOD(" & Format$(k, "#0") & ")", Path_LAVORAZIONE_INI)
  stmp = UCase$(Trim$(stmp))
  If (stmp = "") Then
    Select Case k
      Case 0: stmp = "PROFILE"
      Case 1: stmp = "CORROT"
      Case 2: stmp = "CORRPROP"
      Case 3: stmp = "PROPOSED"
      Case 4: stmp = "PRFTOL"
      Case 5: stmp = "CHKRAB"
    End Select
  End If
  CHK0.lblScX.Caption = "scala prf"
  CHK0.lblScY.Caption = "scala err"
  WRITE_DIALOG ""
  Select Case stmp
  Case "PROFILE"
      CHK0.lblScX.Caption = "scala prf"
      CHK0.lblScY.Caption = "scala err"
      'GRAFICO ERRORI SU PROFILO
      CHK0.lblScX.Visible = False
      CHK0.txtScX.Visible = False
      CHK0.lblScY.Visible = True
      CHK0.txtScY.Visible = True
      CHK0.fraCORRETTORIONOFF.Visible = False
      CHK0.fraCONTROLLOONOFF.Visible = False
      CHK0.fraVISUALIZZAZIONE_CORRETTORI.Visible = False
      CHK0.fraTABELLA_CORRETTORI.Visible = False
      CHK0.fraEDITOR.Visible = False
      CHK0.txtHELP.Visible = False
      'PULISCO LO SCHERMO
      CHK0.Picture1.Cls
      CHK0.Picture1.ScaleWidth = 19.15
      CHK0.Picture1.ScaleHeight = 15.12
      'LETTURA DEI VALORI DI SCALA
      CHK0.txtScY.Text = GetInfo("CHK0", "ScalaErroreROTORI", Path_LAVORAZIONE_INI)
      CHK0.lblScY.Visible = True
      CHK0.txtScY.Visible = True
      DDEDIAMETRO(0) = GetInfo("CONTROLLO", "DDEDIAMETRO(0)", Path_LAVORAZIONE_INI): DDEDIAMETRO(0) = Trim$(DDEDIAMETRO(0))
      If (Len(DDEDIAMETRO(0)) > 0) Then
        stmp = GetInfo("CONTROLLO", "lblDIAMETRO(0)", Path_LAVORAZIONE_INI): stmp = Trim$(stmp)
        CHK0.lblDIAMETRO(0).Caption = stmp
        CHK0.lblDIAMETRO(0).Visible = True
        CHK0.txtDIAMETRO(0).Visible = True
      End If
      DDEDIAMETRO(1) = GetInfo("CONTROLLO", "DDEDIAMETRO(1)", Path_LAVORAZIONE_INI): DDEDIAMETRO(1) = Trim$(DDEDIAMETRO(1))
      If (Len(DDEDIAMETRO(1)) > 0) Then
        stmp = GetInfo("CONTROLLO", "lblDIAMETRO(1)", Path_LAVORAZIONE_INI): stmp = Trim$(stmp)
        CHK0.lblDIAMETRO(1).Caption = stmp
        CHK0.lblDIAMETRO(1).Visible = True
        CHK0.txtDIAMETRO(1).Visible = True
      End If
      DDEDIAMETRO(2) = GetInfo("CONTROLLO", "DDEDIAMETRO(2)", Path_LAVORAZIONE_INI): DDEDIAMETRO(2) = Trim$(DDEDIAMETRO(2))
      If (Len(DDEDIAMETRO(2)) > 0) Then
        stmp = GetInfo("CONTROLLO", "lblDIAMETRO(2)", Path_LAVORAZIONE_INI): stmp = Trim$(stmp)
        CHK0.lblDIAMETRO(2).Caption = stmp
        CHK0.lblDIAMETRO(2).Visible = True
        CHK0.txtDIAMETRO(2).Visible = True
      End If
      If (Len(DDEDIAMETRO(0)) > 0) Then CHK0.txtDIAMETRO(0).Text = OPC_LEGGI_DATO(DDEDIAMETRO(0))
      If (Len(DDEDIAMETRO(1)) > 0) Then CHK0.txtDIAMETRO(1).Text = OPC_LEGGI_DATO(DDEDIAMETRO(1))
      If (Len(DDEDIAMETRO(2)) > 0) Then CHK0.txtDIAMETRO(2).Text = OPC_LEGGI_DATO(DDEDIAMETRO(2))
      CHK0.lblDIAMETRO(0).Left = CHK0.lblScY.Left
      CHK0.lblDIAMETRO(0).Width = CHK0.lblScY.Width
      CHK0.lblDIAMETRO(0).Top = CHK0.lblScY.Top + CHK0.lblScY.Height
      CHK0.lblDIAMETRO(1).Left = CHK0.lblScY.Left
      CHK0.lblDIAMETRO(1).Width = CHK0.lblScY.Width
      CHK0.lblDIAMETRO(1).Top = CHK0.lblDIAMETRO(0).Top + CHK0.lblDIAMETRO(0).Height
      CHK0.lblDIAMETRO(2).Left = CHK0.lblScY.Left
      CHK0.lblDIAMETRO(2).Width = CHK0.lblScY.Width
      CHK0.lblDIAMETRO(2).Top = CHK0.lblDIAMETRO(1).Top + CHK0.lblDIAMETRO(1).Height
      CHK0.txtDIAMETRO(0).Left = CHK0.txtScY.Left
      CHK0.txtDIAMETRO(0).Width = CHK0.txtScY.Width
      CHK0.txtDIAMETRO(0).Top = CHK0.txtScY.Top + CHK0.txtScY.Height
      CHK0.txtDIAMETRO(1).Left = CHK0.txtScY.Left
      CHK0.txtDIAMETRO(1).Width = CHK0.txtScY.Width
      CHK0.txtDIAMETRO(1).Top = CHK0.txtDIAMETRO(0).Top + CHK0.txtDIAMETRO(0).Height
      CHK0.txtDIAMETRO(2).Left = CHK0.txtScY.Left
      CHK0.txtDIAMETRO(2).Width = CHK0.txtScY.Width
      CHK0.txtDIAMETRO(2).Top = CHK0.txtDIAMETRO(1).Top + CHK0.txtDIAMETRO(1).Height
      stmp = GetInfo("CONTROLLO", "SiMEDIA", Path_LAVORAZIONE_INI)
      If (Len(stmp) > 0) Then
        CHK0.fraVANI.Caption = GetInfo("CONTROLLO", "fraVANI", Path_LAVORAZIONE_INI)
        CHK0.optVANI(0).Caption = GetInfo("CONTROLLO", "optVANI(0)", Path_LAVORAZIONE_INI)
        CHK0.optVANI(1).Caption = GetInfo("CONTROLLO", "optVANI(1)", Path_LAVORAZIONE_INI)
        CHK0.optVANI(2).Caption = GetInfo("CONTROLLO", "optVANI(2)", Path_LAVORAZIONE_INI)
        CHK0.optVANI(3).Caption = GetInfo("CONTROLLO", "optVANI(3)", Path_LAVORAZIONE_INI)
        'LETTURA DEL NUMERO DEI PRINCIPI CONTROLLATI
        sVANO = OPC_LEGGI_DATO(stmp)
        VANO = val(sVANO)
            If (VANO <= 1) Then
              VANO = 1
        ElseIf (VANO >= 3) Then
              VANO = 3
        Else
              VANO = 2
        End If
        k = WritePrivateProfileString("CONTROLLO", "nVANI", VANO, Path_LAVORAZIONE_INI)
        CHK0.fraVANI.Left = CHK0.lblDIAMETRO(2).Left
        CHK0.fraVANI.Top = CHK0.lblDIAMETRO(2).Top + CHK0.lblDIAMETRO(2).Height
        CHK0.fraVANI.Width = CHK0.lblDIAMETRO(2).Width + CHK0.txtDIAMETRO(2).Width
        CHK0.fraVANI.Visible = True
        Select Case VANO
        Case 1
            CHK0.optVANI(0).Visible = True
            CHK0.optVANI(1).Visible = False
            CHK0.optVANI(2).Visible = False
            CHK0.optVANI(3).Visible = False
        Case 2
            CHK0.optVANI(0).Visible = True
            CHK0.optVANI(1).Visible = True
            CHK0.optVANI(2).Visible = False
            CHK0.optVANI(3).Visible = True
        Case 3
            CHK0.optVANI(0).Visible = True
            CHK0.optVANI(1).Visible = True
            CHK0.optVANI(2).Visible = True
            CHK0.optVANI(3).Visible = True
        Case Else
            CHK0.optVANI(0).Visible = True
            CHK0.optVANI(1).Visible = False
            CHK0.optVANI(2).Visible = False
            CHK0.optVANI(3).Visible = False
        End Select
      End If
      For i = 0 To 3
        k = val(GetInfo("CONTROLLO", "CHK0.optVANI(" & Format$(i, "0") & ").Value", Path_LAVORAZIONE_INI))
        If (k = 1) Then
          CHK0.optVANI(i).Value = True
          CHK0.optVANI(i).ForeColor = &H80000014
          CHK0.optVANI(i).BackColor = &HFF&
        Else
          CHK0.optVANI(i).Value = False
        End If
      Next i
      CHK0.Picture1.Visible = True
      CHK0.Grafico1.Visible = False
      CHK0.lstCORRETTORIONOFF2.Visible = False
      CHK0.cmdCORRETTORIONOFF2.Visible = False
      CHK0.cmdABILITA.Visible = False
      CHK0.lblCORRETTORIONOFF2(0).Visible = False
      CHK0.lblCORRETTORIONOFF2(1).Visible = False
      Call StampaChkG4_GRAFICO(CHK0.Picture1)
  Case "CORROT"
      'ABILITAZIONE/DISABILITAZIONE CORRETTORI
      CHK0.lblDIAMETRO(0).Visible = False
      CHK0.txtDIAMETRO(0).Visible = False
      CHK0.lblDIAMETRO(1).Visible = False
      CHK0.txtDIAMETRO(1).Visible = False
      CHK0.lblDIAMETRO(2).Visible = False
      CHK0.txtDIAMETRO(2).Visible = False
      CHK0.fraVANI.Visible = False
      CHK0.lblScX.Visible = False
      CHK0.txtScX.Visible = False
      CHK0.lblScY.Visible = False
      CHK0.txtScY.Visible = False
      CHK0.fraCORRETTORIONOFF.Visible = False
      CHK0.fraCONTROLLOONOFF.Visible = False
      CHK0.fraVISUALIZZAZIONE_CORRETTORI.Visible = False
      CHK0.fraTABELLA_CORRETTORI.Visible = True
      CHK0.fraEDITOR.Visible = False
      CHK0.txtHELP.Visible = False
      CHK0.Grafico1.Visible = False
      CHK0.lstCORRETTORIONOFF2.Visible = False
      CHK0.cmdCORRETTORIONOFF2.Visible = False
      CHK0.cmdABILITA.Visible = False
      CHK0.lblCORRETTORIONOFF2(0).Visible = False
      CHK0.lblCORRETTORIONOFF2(1).Visible = False
      '
      CHK0.fraTABELLA_CORRETTORI.Top = CHK0.Image1.Height
      CHK0.fraTABELLA_CORRETTORI.Left = 0
      CHK0.fraTABELLA_CORRETTORI.Width = CHK0.Image1.Width + CHK0.INTESTAZIONE.Width
      'CHK0.fraTABELLA_CORRETTORI.Height = 4800
      '
      CHK0.GraficoCORR.Visible = False
      CHK0.Label7(3).Height = CHK0.Label7(0).Height + CHK0.txtCORR(0).Height
      CHK0.Label7(4).Height = CHK0.Label7(0).Height + CHK0.txtCORR(0).Height
      CHK0.Label7(3).Top = POSY
      CHK0.Label7(3).Left = POSX
      CHK0.List3(3).Top = CHK0.Label7(3).Top + CHK0.Label7(3).Height
      CHK0.List3(3).Left = CHK0.Label7(3).Left
      CHK0.Label7(0).Top = POSY
      CHK0.Label7(0).Left = CHK0.Label7(3).Left + CHK0.Label7(3).Width
      CHK0.txtCORR(0).Top = CHK0.Label7(0).Top + CHK0.Label7(0).Height
      CHK0.txtCORR(0).Left = CHK0.Label7(0).Left
      CHK0.List3(0).Top = CHK0.txtCORR(0).Top + CHK0.txtCORR(0).Height
      CHK0.List3(0).Left = CHK0.txtCORR(0).Left
      CHK0.Label7(1).Top = POSY
      CHK0.Label7(1).Left = CHK0.Label7(0).Left + CHK0.Label7(0).Width
      CHK0.txtCORR(1).Top = CHK0.Label7(1).Top + CHK0.Label7(1).Height
      CHK0.txtCORR(1).Left = CHK0.Label7(1).Left
      CHK0.List3(1).Top = CHK0.txtCORR(1).Top + CHK0.txtCORR(1).Height
      CHK0.List3(1).Left = CHK0.txtCORR(1).Left
      CHK0.Label7(2).Top = POSY
      CHK0.Label7(2).Left = CHK0.Label7(1).Left + CHK0.Label7(1).Width
      CHK0.txtCORR(2).Top = CHK0.Label7(2).Top + CHK0.Label7(2).Height
      CHK0.txtCORR(2).Left = CHK0.Label7(2).Left
      CHK0.List3(2).Top = CHK0.txtCORR(2).Top + CHK0.txtCORR(2).Height
      CHK0.List3(2).Left = CHK0.txtCORR(2).Left
      CHK0.Label7(4).Top = POSY
      CHK0.Label7(4).Left = CHK0.Label7(2).Left + CHK0.Label7(2).Width
      CHK0.List3(4).Top = CHK0.Label7(4).Top + CHK0.Label7(4).Height
      CHK0.List3(4).Left = CHK0.Label7(4).Left
      CHK0.List3(3).Height = 4200
      CHK0.List3(0).Height = 4200
      CHK0.List3(1).Height = 4200
      CHK0.List3(2).Height = 4200
      CHK0.List3(4).Height = 4200
      CHK0.cmdCORR(0).Top = CHK0.Label7(3).Top + CHK0.Label7(3).Height
      CHK0.cmdCORR(0).Left = CHK0.Label7(4).Left + 3 * CHK0.Label7(4).Width
      CHK0.cmdCORR(1).Top = CHK0.cmdCORR(0).Top + 2 * CHK0.cmdCORR(0).Height
      CHK0.cmdCORR(1).Left = CHK0.cmdCORR(0).Left
      CHK0.cmdCORR(0).Visible = True
      CHK0.cmdCORR(2).Visible = False
      Call LEGGI_CORREZIONI("CORROT.BAK")
    
    Case "PROPOSED"
      'ABILITAZIONE/DISABILITAZIONE CORRETTORI
      CHK0.lblDIAMETRO(0).Visible = False
      CHK0.txtDIAMETRO(0).Visible = False
      CHK0.lblDIAMETRO(1).Visible = False
      CHK0.txtDIAMETRO(1).Visible = False
      CHK0.lblDIAMETRO(2).Visible = False
      CHK0.txtDIAMETRO(2).Visible = False
      CHK0.fraVANI.Visible = False
      CHK0.lblScX.Visible = False
      CHK0.txtScX.Visible = False
      CHK0.lblScY.Visible = False
      CHK0.txtScY.Visible = False
      CHK0.fraCORRETTORIONOFF.Visible = False
      CHK0.fraCONTROLLOONOFF.Visible = False
      CHK0.fraVISUALIZZAZIONE_CORRETTORI.Visible = False
      CHK0.fraTABELLA_CORRETTORI.Visible = True
      CHK0.fraEDITOR.Visible = False
      CHK0.txtHELP.Visible = False
      CHK0.Grafico1.Visible = False
      CHK0.lstCORRETTORIONOFF2.Visible = False
      CHK0.cmdCORRETTORIONOFF2.Visible = False
      CHK0.cmdABILITA.Visible = False
      CHK0.lblCORRETTORIONOFF2(0).Visible = False
      CHK0.lblCORRETTORIONOFF2(1).Visible = False
      '
      CHK0.fraTABELLA_CORRETTORI.Top = CHK0.Image1.Height
      CHK0.fraTABELLA_CORRETTORI.Left = 0
      CHK0.fraTABELLA_CORRETTORI.Width = CHK0.Image1.Width + CHK0.INTESTAZIONE.Width
      'CHK0.fraTABELLA_CORRETTORI.Height = 4800
      '
      CHK0.GraficoCORR.Visible = False
      CHK0.Label7(3).Height = CHK0.Label7(0).Height + CHK0.txtCORR(0).Height
      CHK0.Label7(4).Height = CHK0.Label7(0).Height + CHK0.txtCORR(0).Height
      CHK0.Label7(3).Top = POSY
      CHK0.Label7(3).Left = POSX
      CHK0.List3(3).Top = CHK0.Label7(3).Top + CHK0.Label7(3).Height
      CHK0.List3(3).Left = CHK0.Label7(3).Left
      CHK0.Label7(0).Top = POSY
      CHK0.Label7(0).Left = CHK0.Label7(3).Left + CHK0.Label7(3).Width
      CHK0.txtCORR(0).Top = CHK0.Label7(0).Top + CHK0.Label7(0).Height
      CHK0.txtCORR(0).Left = CHK0.Label7(0).Left
      CHK0.List3(0).Top = CHK0.txtCORR(0).Top + CHK0.txtCORR(0).Height
      CHK0.List3(0).Left = CHK0.txtCORR(0).Left
      CHK0.Label7(1).Top = POSY
      CHK0.Label7(1).Left = CHK0.Label7(0).Left + CHK0.Label7(0).Width
      CHK0.txtCORR(1).Top = CHK0.Label7(1).Top + CHK0.Label7(1).Height
      CHK0.txtCORR(1).Left = CHK0.Label7(1).Left
      CHK0.List3(1).Top = CHK0.txtCORR(1).Top + CHK0.txtCORR(1).Height
      CHK0.List3(1).Left = CHK0.txtCORR(1).Left
      CHK0.Label7(2).Top = POSY
      CHK0.Label7(2).Left = CHK0.Label7(1).Left + CHK0.Label7(1).Width
      CHK0.txtCORR(2).Top = CHK0.Label7(2).Top + CHK0.Label7(2).Height
      CHK0.txtCORR(2).Left = CHK0.Label7(2).Left
      CHK0.List3(2).Top = CHK0.txtCORR(2).Top + CHK0.txtCORR(2).Height
      CHK0.List3(2).Left = CHK0.txtCORR(2).Left
      CHK0.Label7(4).Top = POSY
      CHK0.Label7(4).Left = CHK0.Label7(2).Left + CHK0.Label7(2).Width
      CHK0.List3(4).Top = CHK0.Label7(4).Top + CHK0.Label7(4).Height
      CHK0.List3(4).Left = CHK0.Label7(4).Left
      CHK0.List3(3).Height = 4000
      CHK0.List3(0).Height = 4000
      CHK0.List3(1).Height = 4000
      CHK0.List3(2).Height = 4000
      CHK0.List3(4).Height = 4000
      CHK0.cmdCORR(2).Top = CHK0.Label7(3).Top + CHK0.Label7(3).Height
      CHK0.cmdCORR(2).Left = CHK0.Label7(4).Left + 3 * CHK0.Label7(4).Width
      CHK0.cmdCORR(1).Top = CHK0.cmdCORR(2).Top + 2 * CHK0.cmdCORR(2).Height
      CHK0.cmdCORR(1).Left = CHK0.cmdCORR(2).Left
      CHK0.cmdCORR(0).Visible = False
      CHK0.cmdCORR(2).Visible = True
      Call CALCOLO_CORPROP
      Call LEGGI_CORREZIONI("CORPROP")
    
    Case "PRFTOL"
      'PRFTOL.DAT EDITOR
      CHK0.lblDIAMETRO(0).Visible = False
      CHK0.txtDIAMETRO(0).Visible = False
      CHK0.lblDIAMETRO(1).Visible = False
      CHK0.txtDIAMETRO(1).Visible = False
      CHK0.lblDIAMETRO(2).Visible = False
      CHK0.txtDIAMETRO(2).Visible = False
      CHK0.fraVANI.Visible = False
      CHK0.lblScX.Visible = False
      CHK0.txtScX.Visible = False
      CHK0.lblScY.Visible = False
      CHK0.txtScY.Visible = False
      CHK0.fraCONTROLLOONOFF.Visible = False
      CHK0.fraCORRETTORIONOFF.Visible = False
      CHK0.fraVISUALIZZAZIONE_CORRETTORI.Visible = False
      CHK0.fraTABELLA_CORRETTORI.Visible = False
      CHK0.Grafico1.Visible = False
      CHK0.lstCORRETTORIONOFF2.Visible = False
      CHK0.cmdCORRETTORIONOFF2.Visible = False
      CHK0.cmdABILITA.Visible = False
      CHK0.lblCORRETTORIONOFF2(0).Visible = False
      CHK0.lblCORRETTORIONOFF2(1).Visible = False
      CHK0.txtHELP.Visible = False
      CHK0.cmdEDITOR_SALVA.Caption = "SAVE"
      CHK0.cmdEDITOR_STAMPA.Caption = "PRINT"
      CHK0.fraEDITOR.Visible = True
      CHK0.fraEDITOR.Height = CHK0.Picture1.Height
      CHK0.fraEDITOR.Top = CHK0.Image1.Height
      CHK0.fraEDITOR.Left = 0
      CHK0.fraEDITOR.Width = CHK0.Image1.Width + CHK0.INTESTAZIONE.Width
      '
      CHK0.txtEDITOR.Top = 250
      CHK0.txtEDITOR.Left = 50
      CHK0.txtEDITOR.Height = 4400
      CHK0.txtEDITOR.Width = 9200
      '
      CHK0.cmdEDITOR_SALVA.Top = CHK0.txtEDITOR.Top + CHK0.txtEDITOR.Height
      CHK0.cmdEDITOR_STAMPA.Top = CHK0.txtEDITOR.Top + CHK0.txtEDITOR.Height
      CHK0.lblEDITOR_INDICE.Top = CHK0.txtEDITOR.Top + CHK0.txtEDITOR.Height
      CHK0.cmdEDITOR_SALVA.Width = CHK0.txtEDITOR.Width / 3
      CHK0.cmdEDITOR_STAMPA.Width = CHK0.txtEDITOR.Width / 3
      CHK0.lblEDITOR_INDICE.Width = CHK0.txtEDITOR.Width / 3
      CHK0.cmdEDITOR_SALVA.Left = CHK0.txtEDITOR.Left
      CHK0.cmdEDITOR_STAMPA.Left = CHK0.txtEDITOR.Left + CHK0.cmdEDITOR_SALVA.Width
      CHK0.lblEDITOR_INDICE.Left = CHK0.txtEDITOR.Left + CHK0.cmdEDITOR_SALVA.Width + CHK0.cmdEDITOR_STAMPA.Width
      CHK0.cmdEDITOR_STAMPA.Height = CHK0.cmdEDITOR_SALVA.Height
      CHK0.lblEDITOR_INDICE.Height = CHK0.cmdEDITOR_SALVA.Height
      CHK0.cmdEDITOR_STAMPA.Left = CHK0.txtEDITOR.Left + CHK0.cmdEDITOR_SALVA.Width
      CHK0.lblEDITOR_INDICE.Left = CHK0.txtEDITOR.Left + CHK0.cmdEDITOR_SALVA.Width + CHK0.cmdEDITOR_STAMPA.Width
      'LETTURA DEL PERCORSO DI RIFERIMENTO
      PERCORSO = LEP_STR("PERCORSO")
      PERCORSO = g_chOemPATH & "\" & PERCORSO
      If (Len(PERCORSO) > 0) Then
        'RICAVO IL NOME DEL ROTORE DAL PERCORSO
        COORDINATE = PERCORSO
        If (Len(COORDINATE) > 0) Then
          i = InStr(COORDINATE, "\")
          While (i <> 0)
            j = i
            i = InStr(i + 1, COORDINATE, "\")
          Wend
          'RICAVO IL NOME DEL ROTORE
          COORDINATE = Right$(COORDINATE, Len(COORDINATE) - j)
        End If
        'RICAVO IL PERCORSO DI LAVORO
        RADICE = PERCORSO
        i = InStr(RADICE, COORDINATE)
        RADICE = Left$(RADICE, i - 2)
      End If
      If (Dir$(RADICE & "\PRFTOL.DAT") <> "") Then
        Open RADICE & "\PRFTOL.DAT" For Input As #1
          CHK0.txtEDITOR.Text = Input$(LOF(1), 1)
        Close #1
      End If
      CHK0.txtEDITOR.Font.Name = "Courier New"
      CHK0.fraEDITOR.Caption = RADICE & "\PRFTOL.DAT"
      CHK0.txtEDITOR.SetFocus
    
    Case "CORROTBAK"
      'CORROT.BAK EDITOR
      CHK0.lblDIAMETRO(0).Visible = False
      CHK0.txtDIAMETRO(0).Visible = False
      CHK0.lblDIAMETRO(1).Visible = False
      CHK0.txtDIAMETRO(1).Visible = False
      CHK0.lblDIAMETRO(2).Visible = False
      CHK0.txtDIAMETRO(2).Visible = False
      CHK0.fraVANI.Visible = False
      CHK0.lblScX.Visible = False
      CHK0.txtScX.Visible = False
      CHK0.lblScY.Visible = False
      CHK0.txtScY.Visible = False
      CHK0.fraCONTROLLOONOFF.Visible = False
      CHK0.fraCORRETTORIONOFF.Visible = False
      CHK0.fraVISUALIZZAZIONE_CORRETTORI.Visible = False
      CHK0.fraTABELLA_CORRETTORI.Visible = False
      CHK0.txtHELP.Visible = False
      CHK0.Grafico1.Visible = False
      CHK0.lstCORRETTORIONOFF2.Visible = False
      CHK0.cmdCORRETTORIONOFF2.Visible = False
      CHK0.cmdABILITA.Visible = False
      CHK0.lblCORRETTORIONOFF2(0).Visible = False
      CHK0.lblCORRETTORIONOFF2(1).Visible = False
      CHK0.cmdEDITOR_SALVA.Caption = "SAVE"
      CHK0.cmdEDITOR_STAMPA.Caption = "PRINT"
      CHK0.fraEDITOR.Visible = True
      CHK0.fraEDITOR.Height = CHK0.Picture1.Height
      CHK0.fraEDITOR.Top = CHK0.Image1.Height
      CHK0.fraEDITOR.Left = 0
      CHK0.fraEDITOR.Width = CHK0.Image1.Width + CHK0.INTESTAZIONE.Width
      '
      CHK0.txtEDITOR.Top = 250
      CHK0.txtEDITOR.Left = 50
      CHK0.txtEDITOR.Height = 4400
      CHK0.txtEDITOR.Width = 9200
      '
      CHK0.cmdEDITOR_SALVA.Top = CHK0.txtEDITOR.Top + CHK0.txtEDITOR.Height
      CHK0.cmdEDITOR_STAMPA.Top = CHK0.txtEDITOR.Top + CHK0.txtEDITOR.Height
      CHK0.lblEDITOR_INDICE.Top = CHK0.txtEDITOR.Top + CHK0.txtEDITOR.Height
      CHK0.cmdEDITOR_SALVA.Width = CHK0.txtEDITOR.Width / 3
      CHK0.cmdEDITOR_STAMPA.Width = CHK0.txtEDITOR.Width / 3
      CHK0.lblEDITOR_INDICE.Width = CHK0.txtEDITOR.Width / 3
      CHK0.cmdEDITOR_SALVA.Left = CHK0.txtEDITOR.Left
      CHK0.cmdEDITOR_STAMPA.Left = CHK0.txtEDITOR.Left + CHK0.cmdEDITOR_SALVA.Width
      CHK0.lblEDITOR_INDICE.Left = CHK0.txtEDITOR.Left + CHK0.cmdEDITOR_SALVA.Width + CHK0.cmdEDITOR_STAMPA.Width
      CHK0.cmdEDITOR_STAMPA.Height = CHK0.cmdEDITOR_SALVA.Height
      CHK0.lblEDITOR_INDICE.Height = CHK0.cmdEDITOR_SALVA.Height
      CHK0.cmdEDITOR_STAMPA.Left = CHK0.txtEDITOR.Left + CHK0.cmdEDITOR_SALVA.Width
      CHK0.lblEDITOR_INDICE.Left = CHK0.txtEDITOR.Left + CHK0.cmdEDITOR_SALVA.Width + CHK0.cmdEDITOR_STAMPA.Width
      'LETTURA DEL PERCORSO DI RIFERIMENTO
      PERCORSO = LEP_STR("PERCORSO")
      PERCORSO = g_chOemPATH & "\" & PERCORSO
      If (Len(PERCORSO) > 0) Then
        'RICAVO IL NOME DEL ROTORE DAL PERCORSO
        COORDINATE = PERCORSO
        If (Len(COORDINATE) > 0) Then
          i = InStr(COORDINATE, "\")
          While (i <> 0)
            j = i
            i = InStr(i + 1, COORDINATE, "\")
          Wend
          'RICAVO IL NOME DEL ROTORE
          COORDINATE = Right$(COORDINATE, Len(COORDINATE) - j)
        End If
        'RICAVO IL PERCORSO DI LAVORO
        RADICE = PERCORSO
        i = InStr(RADICE, COORDINATE)
        RADICE = Left$(RADICE, i - 2)
      End If
      If (Dir$(RADICE & "\CORROT.BAK") <> "") Then
        Open RADICE & "\CORROT.BAK" For Input As #1
          CHK0.txtEDITOR.Text = Input$(LOF(1), 1)
        Close #1
      End If
      CHK0.txtEDITOR.Font.Name = "Courier New"
      CHK0.fraEDITOR.Caption = RADICE & "\CORROT.BAK"
      CHK0.txtEDITOR.SetFocus
    
    Case "CHKRAB"
      'RICEZIONE E VISUALIZZAZIONE DI CHKRAB.SPF
      CHK0.lblDIAMETRO(0).Visible = False
      CHK0.txtDIAMETRO(0).Visible = False
      CHK0.lblDIAMETRO(1).Visible = False
      CHK0.txtDIAMETRO(1).Visible = False
      CHK0.lblDIAMETRO(2).Visible = False
      CHK0.txtDIAMETRO(2).Visible = False
      CHK0.fraVANI.Visible = False
      CHK0.lblScX.Visible = False
      CHK0.txtScX.Visible = False
      CHK0.lblScY.Visible = False
      CHK0.txtScY.Visible = False
      CHK0.fraCONTROLLOONOFF.Visible = False
      CHK0.fraCORRETTORIONOFF.Visible = False
      CHK0.fraVISUALIZZAZIONE_CORRETTORI.Visible = False
      CHK0.fraTABELLA_CORRETTORI.Visible = False
      CHK0.Grafico1.Visible = False
      CHK0.lstCORRETTORIONOFF2.Visible = False
      CHK0.cmdCORRETTORIONOFF2.Visible = False
      CHK0.cmdABILITA.Visible = False
      CHK0.lblCORRETTORIONOFF2(0).Visible = False
      CHK0.lblCORRETTORIONOFF2(1).Visible = False
      CHK0.txtHELP.BackColor = &H0&
      CHK0.txtHELP.ForeColor = &HFFFFFF
      CHK0.txtHELP.Visible = True
      CHK0.txtHELP.Text = ""
      'LETTURA DEL PERCORSO DI RIFERIMENTO
      PERCORSO = LEP_STR("PERCORSO")
      PERCORSO = g_chOemPATH & "\" & PERCORSO
      If (Len(PERCORSO) > 0) Then
        'RICAVO IL NOME DEL ROTORE DAL PERCORSO
        COORDINATE = PERCORSO
        If (Len(COORDINATE) > 0) Then
          i = InStr(COORDINATE, "\")
          While (i <> 0)
            j = i
            i = InStr(i + 1, COORDINATE, "\")
          Wend
          'RICAVO IL NOME DEL ROTORE
          COORDINATE = Right$(COORDINATE, Len(COORDINATE) - j)
        End If
        'RICAVO IL PERCORSO DI LAVORO
        RADICE = PERCORSO
        i = InStr(RADICE, COORDINATE)
        RADICE = Left$(RADICE, i - 2)
      End If
      If (Dir$(RADICE & "\CHKRAB.SPF") <> "") Then Kill RADICE & "\CHKRAB.SPF"
      Set Session = GetObject("@SinHMIMCDomain.MCDomain")
      Session.CopyNC "/NC/WKS.DIR/PARAMETRI" & Format$(INDICE_LAVORAZIONE_SINGOLA, "###0") & ".WPD/CHKRAB.SPF", RADICE & "\CHKRAB.SPF", MCDOMAIN_COPY_NC
      Set Session = Nothing
      If (Dir$(RADICE & "\CHKRAB.SPF") <> "") Then
        Open RADICE & "\CHKRAB.SPF" For Input As #1
        CHK0.txtHELP.Text = Input(LOF(1), #1)
        Close #1
      End If
    
    Case "CORRPROP"
      'VISUALIZZAZIONE MOLA CORRETTA
      CHK0.lblDIAMETRO(0).Visible = False
      CHK0.txtDIAMETRO(0).Visible = False
      CHK0.lblDIAMETRO(1).Visible = False
      CHK0.txtDIAMETRO(1).Visible = False
      CHK0.lblDIAMETRO(2).Visible = False
      CHK0.txtDIAMETRO(2).Visible = False
      CHK0.fraVANI.Visible = False
      CHK0.lblScX.Visible = False
      CHK0.txtScX.Visible = False
      CHK0.lblScY.Visible = False
      CHK0.txtScY.Visible = False
      CHK0.fraCONTROLLOONOFF.Visible = False
      CHK0.fraCORRETTORIONOFF.Visible = False
      CHK0.fraVISUALIZZAZIONE_CORRETTORI.Visible = True
      CHK0.fraTABELLA_CORRETTORI.Visible = False
      CHK0.fraEDITOR.Visible = False
      CHK0.txtHELP.Visible = False
      CHK0.Grafico1.Visible = False
      CHK0.lstCORRETTORIONOFF2.Visible = False
      CHK0.cmdCORRETTORIONOFF2.Visible = False
      CHK0.cmdABILITA.Visible = False
      CHK0.lblCORRETTORIONOFF2(0).Visible = False
      CHK0.lblCORRETTORIONOFF2(1).Visible = False
      CHK0.fraVISUALIZZAZIONE_CORRETTORI.Top = CHK0.Image1.Height
      CHK0.fraVISUALIZZAZIONE_CORRETTORI.Left = 0
      CHK0.fraVISUALIZZAZIONE_CORRETTORI.Width = CHK0.Image1.Width + CHK0.INTESTAZIONE.Width
      '
      CHK0.hsCORROT.MIN = 0
      CHK0.hsCORROT.Value = 0
      CHK0.hsCORROT.Height = CHK0.PicCORROT.Height
      '
      CHK0.fraVISUALIZZAZIONE_CORRETTORI.Caption = ""
      CHK0.lblINTESTAZIONE = ""
      CHK0.lblCORROT(0).Caption = " SCALE PROF."
      CHK0.lblCORROT(1).Caption = " SCALE CORR."
      Call VISUALIZZA_MOLA_CORRETTA(0, "N")
    
    Case "LEAD"
      CHK0.lblScX.Caption = "scala X"
      CHK0.lblScY.Caption = "scala Y"
      'RICEZIONE E VISUALIZZAZIONE DI RESULTS.SPF
      CHK0.lblDIAMETRO(0).Visible = False
      CHK0.txtDIAMETRO(0).Visible = False
      CHK0.lblDIAMETRO(1).Visible = False
      CHK0.txtDIAMETRO(1).Visible = False
      CHK0.lblDIAMETRO(2).Visible = False
      CHK0.txtDIAMETRO(2).Visible = False
      CHK0.fraVANI.Visible = False
      CHK0.lblScX.Visible = True
      CHK0.lblScY.Visible = True
      CHK0.txtScX.Visible = True
      CHK0.txtScY.Visible = True
      CHK0.fraCONTROLLOONOFF.Visible = False
      CHK0.fraCORRETTORIONOFF.Visible = False
      CHK0.fraVISUALIZZAZIONE_CORRETTORI.Visible = False
      CHK0.fraTABELLA_CORRETTORI.Visible = False
      CHK0.fraEDITOR.Visible = False
      CHK0.txtHELP.Visible = False
      CHK0.Grafico1.Visible = False
      CHK0.lstCORRETTORIONOFF2.Visible = False
      CHK0.cmdCORRETTORIONOFF2.Visible = False
      CHK0.cmdABILITA.Visible = False
      CHK0.lblCORRETTORIONOFF2(0).Visible = False
      CHK0.lblCORRETTORIONOFF2(1).Visible = False
      'PULISCO LO SCHERMO
      CHK0.Picture1.Visible = True
      CHK0.Picture1.Cls
      'LETTURA DEI VALORI DI SCALA
      CHK0.txtScX.Text = GetInfo("CHK0", "ScalaELICAROTORIX", Path_LAVORAZIONE_INI)
      CHK0.txtScY.Text = GetInfo("CHK0", "ScalaELICAROTORIY", Path_LAVORAZIONE_INI)
      Call VISUALIZZA_CONTROLLO_ELICA(val(CHK0.txtScX.Text), val(CHK0.txtScY.Text), "N")
    
    Case "RESULTS"
      'RISULTATO CONTROLLO ESTERNO
      'VERIFICA ABILITAZIONE UTENTE
      Dim ABILITAZIONE  As String
      Dim Utente        As String
      Dim UTENTI()      As String
      i = 0: ReDim UTENTI(i)
      Do
        i = i + 1
        stmp = GetInfo("UTENTI", "UTENTE(" & Format$(i, "#0") & ")", Path_LAVORAZIONE_INI)
        stmp = UCase$(Trim$(stmp))
        If (stmp <> "") Then
          ReDim Preserve UTENTI(i)
          UTENTI(i) = stmp
        Else
          Exit Do
        End If
      Loop
      If (UBound(UTENTI) > 0) Then
        Utente = InputBox("INPUT USERNAME!!!!", "ROTOR DATA", "")
        Utente = UCase$(Trim$(Utente))
        For i = 1 To UBound(UTENTI)
          If (UTENTI(i) = Utente) Then
            ABILITAZIONE = "Y" 'UTENTE ABILITATO
            Exit For
          End If
        Next i
        If (ABILITAZIONE <> "Y") Then
          If (Len(Utente) <= 0) Then Utente = "?????"
          stmp = GetInfo("UTENTI", "MESSAGGIO", Path_LAVORAZIONE_INI)
          WRITE_DIALOG Utente & ": " & stmp
          Exit Sub
        End If
      End If
      'LETTURA DEL PERCORSO DI RIFERIMENTO
      RADICE = LEP_STR("PERCORSO_CONTROLLI")
      stmp1 = LNP("PERCORSO_CONTROLLI")
      stmp = LEGGI_NomePezzo("ROTORI_ESTERNI")
      stmp = InputBox("INPUT FILE NAME!!!" & Chr(13) & Chr(13) & stmp1 & ": " & RADICE, "RESULTS", stmp & ".CHK")
      If (Len(stmp) > 0) Then
        If (Dir$(RADICE & "\" & stmp) <> "") Then
          Open RADICE & "\" & stmp For Input As #1
            CHK0.txtEDITOR.Text = Input$(LOF(1), 1)
          Close #1
          CHK0.txtEDITOR.Font.Name = "Courier New"
          CHK0.lblDIAMETRO(0).Visible = False
          CHK0.txtDIAMETRO(0).Visible = False
          CHK0.lblDIAMETRO(1).Visible = False
          CHK0.txtDIAMETRO(1).Visible = False
          CHK0.lblDIAMETRO(2).Visible = False
          CHK0.txtDIAMETRO(2).Visible = False
          CHK0.fraVANI.Visible = False
          CHK0.lblScX.Visible = False
          CHK0.txtScX.Visible = False
          CHK0.lblScY.Visible = False
          CHK0.txtScY.Visible = False
          CHK0.fraCONTROLLOONOFF.Visible = False
          CHK0.fraCORRETTORIONOFF.Visible = False
          CHK0.fraVISUALIZZAZIONE_CORRETTORI.Visible = False
          CHK0.fraTABELLA_CORRETTORI.Visible = False
          CHK0.txtHELP.Visible = False
          CHK0.Grafico1.Visible = False
          CHK0.lstCORRETTORIONOFF2.Visible = False
          CHK0.cmdCORRETTORIONOFF2.Visible = False
          CHK0.cmdABILITA.Visible = False
          CHK0.lblCORRETTORIONOFF2(0).Visible = False
          CHK0.lblCORRETTORIONOFF2(1).Visible = False
          CHK0.cmdEDITOR_SALVA.Caption = "SAVE"
          CHK0.cmdEDITOR_STAMPA.Caption = "PRINT"
          CHK0.fraEDITOR.Visible = True
          CHK0.fraEDITOR.Height = CHK0.Picture1.Height
          CHK0.fraEDITOR.Top = CHK0.Image1.Height
          CHK0.fraEDITOR.Left = 0
          CHK0.fraEDITOR.Width = CHK0.Image1.Width + CHK0.INTESTAZIONE.Width
          '
          CHK0.txtEDITOR.Top = 250
          CHK0.txtEDITOR.Left = 50
          CHK0.txtEDITOR.Height = 4400
          CHK0.txtEDITOR.Width = 9200
          '
          CHK0.cmdEDITOR_SALVA.Top = CHK0.txtEDITOR.Top + CHK0.txtEDITOR.Height
          CHK0.cmdEDITOR_STAMPA.Top = CHK0.txtEDITOR.Top + CHK0.txtEDITOR.Height
          CHK0.lblEDITOR_INDICE.Top = CHK0.txtEDITOR.Top + CHK0.txtEDITOR.Height
          CHK0.cmdEDITOR_SALVA.Width = CHK0.txtEDITOR.Width / 3
          CHK0.cmdEDITOR_STAMPA.Width = CHK0.txtEDITOR.Width / 3
          CHK0.lblEDITOR_INDICE.Width = CHK0.txtEDITOR.Width / 3
          CHK0.cmdEDITOR_SALVA.Left = CHK0.txtEDITOR.Left
          CHK0.cmdEDITOR_STAMPA.Left = CHK0.txtEDITOR.Left + CHK0.cmdEDITOR_SALVA.Width
          CHK0.lblEDITOR_INDICE.Left = CHK0.txtEDITOR.Left + CHK0.cmdEDITOR_SALVA.Width + CHK0.cmdEDITOR_STAMPA.Width
          CHK0.cmdEDITOR_STAMPA.Height = CHK0.cmdEDITOR_SALVA.Height
          CHK0.lblEDITOR_INDICE.Height = CHK0.cmdEDITOR_SALVA.Height
          CHK0.cmdEDITOR_STAMPA.Left = CHK0.txtEDITOR.Left + CHK0.cmdEDITOR_SALVA.Width
          CHK0.lblEDITOR_INDICE.Left = CHK0.txtEDITOR.Left + CHK0.cmdEDITOR_SALVA.Width + CHK0.cmdEDITOR_STAMPA.Width
          CHK0.fraEDITOR.Caption = RADICE & "\" & stmp
          CHK0.txtEDITOR.SetFocus
        Else
          WRITE_DIALOG Error(53)
        End If
      Else
        WRITE_DIALOG "Operation aborted by user!!!"
      End If
  
    Case "CORRETTORIONOFF"
      'ABILITAZIONE/DISABILITAZIONE CORRETTORI
      CHK0.lblDIAMETRO(0).Visible = False
      CHK0.txtDIAMETRO(0).Visible = False
      CHK0.lblDIAMETRO(1).Visible = False
      CHK0.txtDIAMETRO(1).Visible = False
      CHK0.lblDIAMETRO(2).Visible = False
      CHK0.txtDIAMETRO(2).Visible = False
      CHK0.fraVANI.Visible = False
      CHK0.lblScX.Visible = False
      CHK0.txtScX.Visible = False
      CHK0.lblScY.Visible = False
      CHK0.txtScY.Visible = False
      CHK0.fraCONTROLLOONOFF.Visible = False
      CHK0.fraVISUALIZZAZIONE_CORRETTORI.Visible = False
      CHK0.fraTABELLA_CORRETTORI.Visible = False
      CHK0.fraEDITOR.Visible = False
      CHK0.txtHELP.Visible = False
      '
      CHK0.fraCORRETTORIONOFF.Height = CHK0.Picture1.Height
      CHK0.fraCORRETTORIONOFF.Top = CHK0.Image1.Height
      CHK0.fraCORRETTORIONOFF.Left = 0
      CHK0.fraCORRETTORIONOFF.Width = CHK0.Image1.Width + CHK0.INTESTAZIONE.Width
      '
      CHK0.grfCORRETTORIONOFF.Visible = True
      '
      CHK0.lblCORRETTORIONOFF.Top = 0
      CHK0.lblCORRETTORIONOFF.Left = 0
      CHK0.lblCORRETTORIONOFF.Height = 500
      CHK0.lblCORRETTORIONOFF.Width = 1000
      '
      CHK0.lstCORRETTORIONOFF.Top = CHK0.lblCORRETTORIONOFF.Top + CHK0.lblCORRETTORIONOFF.Height
      CHK0.lstCORRETTORIONOFF.Left = CHK0.lblCORRETTORIONOFF.Left
      CHK0.lstCORRETTORIONOFF.Height = 4000
      CHK0.lstCORRETTORIONOFF.Width = 1000
      CHK0.lstCORRETTORIONOFF.Font.Name = "Courier New"
      '
      CHK0.grfCORRETTORIONOFF.Left = CHK0.lblCORRETTORIONOFF.Left + CHK0.lblCORRETTORIONOFF.Width
      CHK0.grfCORRETTORIONOFF.Top = 0
      CHK0.grfCORRETTORIONOFF.Width = 8000
      CHK0.grfCORRETTORIONOFF.Height = 4800
      '
      CHK0.lblCORRETTORIONOFF.Font.Name = "Courier New"
      CHK0.lblCORRETTORIONOFF.Caption = "ON  -> X" & Chr(13) & "OFF -> -"
      '
      CHK0.cmdCORRETTORIONOFF.Top = CHK0.lstCORRETTORIONOFF.Top + CHK0.lstCORRETTORIONOFF.Height
      CHK0.cmdCORRETTORIONOFF.Left = CHK0.lblCORRETTORIONOFF.Left
      CHK0.cmdCORRETTORIONOFF.Height = 500
      CHK0.cmdCORRETTORIONOFF.Width = 1000
      '
      CHK0.txtSCALA.Top = CHK0.grfCORRETTORIONOFF.Top + CHK0.grfCORRETTORIONOFF.Height - CHK0.txtSCALA.Height
      CHK0.txtSCALA.Left = CHK0.grfCORRETTORIONOFF.Left
      CHK0.txtSCALA.Width = CHK0.cmdCORRETTORIONOFF.Width / 2
      '
      Call LEGGI_INFO_ROTORI(PERCORSO, COORDINATE, RADICE, PERCORSO_CONTROLLI, PERCORSO_PROFILI, sequenza, npt, TY_ROT, PASSO, CONTROLLORE, DM_ROT, INVERSIONE)
      If (Dir$(RADICE & "\ABILITA.COR") = "") Then
        Open RADICE & "\ABILITA.COR" For Output As #1
          For i = 1 To npt + 2
            Print #1, "1"
          Next i
        Close #1
      End If
      CHK0.lstCORRETTORIONOFF.Font.Name = "Courier New"
      CHK0.lstCORRETTORIONOFF.Clear
      Open RADICE & "\ABILITA.COR" For Input As #2
        i = 0
        While (Not EOF(2))
          Line Input #2, stmp
          If (val(stmp) = 1) Then
            Call CHK0.lstCORRETTORIONOFF.AddItem(Right$(String$(3, " ") & Format$(i, "##0"), 3) & " X")
          Else
            Call CHK0.lstCORRETTORIONOFF.AddItem(Right$(String$(3, " ") & Format$(i, "##0"), 3) & " -")
          End If
          i = i + 1
        Wend
      Close #2
      CHK0.lstCORRETTORIONOFF.ListIndex = 0
      ReDim IdxCHKtmp(npt + 2)
      For i = 0 To npt + 1
        If (Right$(CHK0.lstCORRETTORIONOFF.List(i), 1) = "X") Then
          IdxCHKtmp(i + 1) = 1 'DA CORREGGERE
        Else
          IdxCHKtmp(i + 1) = 0 'DA NON CORREGGERE
        End If
      Next i
      ReDim tX1(npt + 2): ReDim tY1(npt + 2): ReDim tN1(npt + 2)
      ReDim tX(npt + 2): ReDim tY(npt + 2)
      ReDim tN(0)
      Open RADICE & "\D" & DM_ROT For Input As #1
        j = 1
        While Not EOF(1)
          Line Input #1, stmp
          'ricavo il punto
          stmp = Trim$(stmp)
          i = InStr(2, stmp, "+")
          If (i <= 0) Then i = InStr(2, stmp, "-")
          tY(j) = val(Left$(stmp, i - 1))
          tX(j) = val(Right$(stmp, Len(stmp) - i))
          If (IdxCHKtmp(j) = 0) Then
            ReDim Preserve tN(UBound(tN) + 1)
            tN(UBound(tN)) = j
          End If
          j = j + 1
        Wend
      Close #1
      NPM1 = val(GetInfo("CREAZIONE", "NpM_F1", RADICE & "\INFO.INI"))
      NPM2 = val(GetInfo("CREAZIONE", "NpM_F2", RADICE & "\INFO.INI"))
      NP1 = val(GetInfo("CREAZIONE", "NpC_F1", RADICE & "\INFO.INI"))
      NP2 = val(GetInfo("CREAZIONE", "NpC_F2", RADICE & "\INFO.INI"))
      NPF1 = NPM1
      'CALCOLO DELLE CORREZIONI PROPOSTE
      SCALA_ERRORI = val(CHK0.txtSCALA.Text)
      If (SCALA_ERRORI <= 0) Then
        SCALA_ERRORI = 50
        CHK0.txtSCALA.Text = "50"
      End If
      Call CALCOLO_CORREZIONI(CorZ, corY, Np, npt, NP1, NP2, NPM1, NPM2, RADICE, TY_ROT, CONTROLLORE, ER)
      For j = 1 To NPF1
          tY1(j) = tY(j) + SCALA_ERRORI * CorZ(j)
          tX1(j) = tX(j) - SCALA_ERRORI * corY(j)
      Next j
      For j = NPF1 + 1 To npt + 2
          tY1(j) = tY(j) - SCALA_ERRORI * CorZ(j)
          tX1(j) = tX(j) - SCALA_ERRORI * corY(j)
      Next j
      'ERRORI NORMALI AL PROFILO TEORICO
      For j = 1 To npt + 2
        Call CHK0.grfCORRETTORIONOFF.AddLinea(tY1(j), tX1(j), tY(j), tX(j), fiAquamarine, Format$(j, "######0"))
      Next j
      Call CHK0.grfCORRETTORIONOFF.SetLingua("UK")
      Call CHK0.grfCORRETTORIONOFF.AddSerieXY(tY, tX, tX, fiblack, True, True, "1")  'PROFILO TEORICO
      Call CHK0.grfCORRETTORIONOFF.AddSerieXY(tY1, tX1, tX1, fired, False, False, "2")
      Call CHK0.grfCORRETTORIONOFF.SelPunti("1", tN)
      CHK0.grfCORRETTORIONOFF.griglia = True
      CHK0.grfCORRETTORIONOFF.INIZIO_PUNTI = -1
      CHK0.grfCONTROLLOONOFF.TIPO_PROFILO = TY_ROT
      CHK0.grfCORRETTORIONOFF.Agganciare = True
      CHK0.fraCORRETTORIONOFF.Visible = True
      '
    Case "CORRETTORIONOFF2"
      'GRAFICO ERRORI SU PROFILO
      CHK0.lblScX.Visible = False
      CHK0.txtScX.Visible = False
      CHK0.lblScY.Visible = True
      CHK0.txtScY.Visible = True
      CHK0.lblScY.Caption = "scala err"
      CHK0.fraCORRETTORIONOFF.Visible = False
      CHK0.fraCONTROLLOONOFF.Visible = False
      CHK0.fraVISUALIZZAZIONE_CORRETTORI.Visible = False
      CHK0.fraTABELLA_CORRETTORI.Visible = False
      CHK0.fraEDITOR.Visible = False
      CHK0.txtHELP.Visible = False
      'PULISCO LO SCHERMO
      CHK0.Picture1.Cls
      CHK0.Picture1.ScaleWidth = 19.15
      CHK0.Picture1.ScaleHeight = 13.12
      'LETTURA DEI VALORI DI SCALA
      CHK0.txtScY.Text = GetInfo("CHK0", "ScalaErroreROTORI", Path_LAVORAZIONE_INI)
      CHK0.lblScY.Visible = True
      CHK0.txtScY.Visible = True
      DDEDIAMETRO(0) = GetInfo("CONTROLLO", "DDEDIAMETRO(0)", Path_LAVORAZIONE_INI): DDEDIAMETRO(0) = Trim$(DDEDIAMETRO(0))
      If (Len(DDEDIAMETRO(0)) > 0) Then
        stmp = GetInfo("CONTROLLO", "lblDIAMETRO(0)", Path_LAVORAZIONE_INI): stmp = Trim$(stmp)
        CHK0.lblDIAMETRO(0).Caption = stmp
        CHK0.lblDIAMETRO(0).Visible = True
        CHK0.txtDIAMETRO(0).Visible = True
      End If
      DDEDIAMETRO(1) = GetInfo("CONTROLLO", "DDEDIAMETRO(1)", Path_LAVORAZIONE_INI): DDEDIAMETRO(1) = Trim$(DDEDIAMETRO(1))
      If (Len(DDEDIAMETRO(1)) > 0) Then
        stmp = GetInfo("CONTROLLO", "lblDIAMETRO(1)", Path_LAVORAZIONE_INI): stmp = Trim$(stmp)
        CHK0.lblDIAMETRO(1).Caption = stmp
        CHK0.lblDIAMETRO(1).Visible = True
        CHK0.txtDIAMETRO(1).Visible = True
      End If
      DDEDIAMETRO(2) = GetInfo("CONTROLLO", "DDEDIAMETRO(2)", Path_LAVORAZIONE_INI): DDEDIAMETRO(2) = Trim$(DDEDIAMETRO(2))
      If (Len(DDEDIAMETRO(2)) > 0) Then
        stmp = GetInfo("CONTROLLO", "lblDIAMETRO(2)", Path_LAVORAZIONE_INI): stmp = Trim$(stmp)
        CHK0.lblDIAMETRO(2).Caption = stmp
        CHK0.lblDIAMETRO(2).Visible = True
        CHK0.txtDIAMETRO(2).Visible = True
      End If
      '
      If (Len(DDEDIAMETRO(0)) > 0) Then CHK0.txtDIAMETRO(0).Text = OPC_LEGGI_DATO(DDEDIAMETRO(0))
      If (Len(DDEDIAMETRO(1)) > 0) Then CHK0.txtDIAMETRO(1).Text = OPC_LEGGI_DATO(DDEDIAMETRO(1))
      If (Len(DDEDIAMETRO(2)) > 0) Then CHK0.txtDIAMETRO(2).Text = OPC_LEGGI_DATO(DDEDIAMETRO(2))
      '
      CHK0.lblDIAMETRO(0).Left = CHK0.lblScY.Left
      CHK0.lblDIAMETRO(0).Width = CHK0.lblScY.Width
      CHK0.lblDIAMETRO(0).Top = CHK0.lblScY.Top + CHK0.lblScY.Height
      '
      CHK0.lblDIAMETRO(1).Left = CHK0.lblScY.Left
      CHK0.lblDIAMETRO(1).Width = CHK0.lblScY.Width
      CHK0.lblDIAMETRO(1).Top = CHK0.lblDIAMETRO(0).Top + CHK0.lblDIAMETRO(0).Height
      '
      CHK0.lblDIAMETRO(2).Left = CHK0.lblScY.Left
      CHK0.lblDIAMETRO(2).Width = CHK0.lblScY.Width
      CHK0.lblDIAMETRO(2).Top = CHK0.lblDIAMETRO(1).Top + CHK0.lblDIAMETRO(1).Height
      '
      CHK0.txtDIAMETRO(0).Left = CHK0.txtScY.Left
      CHK0.txtDIAMETRO(0).Width = CHK0.txtScY.Width
      CHK0.txtDIAMETRO(0).Top = CHK0.txtScY.Top + CHK0.txtScY.Height
      '
      CHK0.txtDIAMETRO(1).Left = CHK0.txtScY.Left
      CHK0.txtDIAMETRO(1).Width = CHK0.txtScY.Width
      CHK0.txtDIAMETRO(1).Top = CHK0.txtDIAMETRO(0).Top + CHK0.txtDIAMETRO(0).Height
      '
      CHK0.txtDIAMETRO(2).Left = CHK0.txtScY.Left
      CHK0.txtDIAMETRO(2).Width = CHK0.txtScY.Width
      CHK0.txtDIAMETRO(2).Top = CHK0.txtDIAMETRO(1).Top + CHK0.txtDIAMETRO(1).Height
      '
      stmp = GetInfo("CONTROLLO", "SiMEDIA", Path_LAVORAZIONE_INI)
      If (Len(stmp) > 0) Then
        CHK0.fraVANI.Caption = GetInfo("CONTROLLO", "fraVANI", Path_LAVORAZIONE_INI)
        CHK0.optVANI(0).Caption = GetInfo("CONTROLLO", "optVANI(0)", Path_LAVORAZIONE_INI)
        CHK0.optVANI(1).Caption = GetInfo("CONTROLLO", "optVANI(1)", Path_LAVORAZIONE_INI)
        CHK0.optVANI(2).Caption = GetInfo("CONTROLLO", "optVANI(2)", Path_LAVORAZIONE_INI)
        CHK0.optVANI(3).Caption = GetInfo("CONTROLLO", "optVANI(3)", Path_LAVORAZIONE_INI)
        'LETTURA DEL NUMERO DEI PRINCIPI CONTROLLATI
        sVANO = OPC_LEGGI_DATO(stmp)
        VANO = val(sVANO)
            If (VANO <= 1) Then
              VANO = 1
        ElseIf (VANO >= 3) Then
              VANO = 3
        Else
              VANO = 2
        End If
        k = WritePrivateProfileString("CONTROLLO", "nVANI", VANO, Path_LAVORAZIONE_INI)
        CHK0.fraVANI.Left = CHK0.lblDIAMETRO(2).Left
        CHK0.fraVANI.Top = CHK0.lblDIAMETRO(2).Top + CHK0.lblDIAMETRO(2).Height
        CHK0.fraVANI.Width = CHK0.lblDIAMETRO(2).Width + CHK0.txtDIAMETRO(2).Width
        CHK0.fraVANI.Visible = True
        Select Case VANO
          Case 1
            CHK0.optVANI(0).Visible = True
            CHK0.optVANI(1).Visible = False
            CHK0.optVANI(2).Visible = False
            CHK0.optVANI(3).Visible = False
          Case 2
            CHK0.optVANI(0).Visible = True
            CHK0.optVANI(1).Visible = True
            CHK0.optVANI(2).Visible = False
            CHK0.optVANI(3).Visible = True
          Case 3
            CHK0.optVANI(0).Visible = True
            CHK0.optVANI(1).Visible = True
            CHK0.optVANI(2).Visible = True
            CHK0.optVANI(3).Visible = True
          Case Else
            CHK0.optVANI(0).Visible = True
            CHK0.optVANI(1).Visible = False
            CHK0.optVANI(2).Visible = False
            CHK0.optVANI(3).Visible = False
        End Select
      End If
      For i = 0 To 3
        k = val(GetInfo("CONTROLLO", "CHK0.optVANI(" & Format$(i, "0") & ").Value", Path_LAVORAZIONE_INI))
        If (k = 1) Then
          CHK0.optVANI(i).Value = True
          CHK0.optVANI(i).ForeColor = &H80000014
          CHK0.optVANI(i).BackColor = &HFF&
        Else
          CHK0.optVANI(i).Value = False
        End If
      Next i
      '
      CHK0.Picture1.Visible = False
      CHK0.Grafico1.Left = 0
      CHK0.Grafico1.Top = CHK0.Image1.Top + CHK0.Image1.Height
      CHK0.Grafico1.Height = CHK0.Picture1.Height
      CHK0.Grafico1.Width = CHK0.Picture1.Width
      CHK0.Grafico1.Visible = True
      Call LEGGI_INFO_ROTORI(PERCORSO, COORDINATE, RADICE, PERCORSO_CONTROLLI, PERCORSO_PROFILI, sequenza, npt, TY_ROT, PASSO, CONTROLLORE, DM_ROT, INVERSIONE)
      If (Dir$(RADICE & "\ABILITA.COR") = "") Then
        Open RADICE & "\ABILITA.COR" For Output As #1
          For i = 1 To npt + 2
            Print #1, "1"
          Next i
        Close #1
      End If
      ReDim tX(npt + 2):     ReDim tY(npt + 2):     ReDim tN(npt + 2)
      ReDim Raggio(npt + 2): ReDim ANGOLO(npt + 2): ReDim PRESSIONE(npt + 2)
      Open RADICE & "\PRFROT" For Input As #1
      Dim NptABILITATI As Integer
      NptABILITATI = 0
      '
      CHK0.lstCORRETTORIONOFF2.Clear
      Open RADICE & "\ABILITA.COR" For Input As #2
      'ABILITAZIONE DEL PRIMO PUNTO DELLA MOLA
      i = 0
      Line Input #2, stmp
      If (val(stmp) = 0) Then
        tN(1) = 1
        Call CHK0.lstCORRETTORIONOFF2.AddItem(Right$(String$(4, " ") & Format$(i, "##0"), 4) & " -")
      Else
        tN(1) = 0
        NptABILITATI = NptABILITATI + 1
        Call CHK0.lstCORRETTORIONOFF2.AddItem(Right$(String$(4, " ") & Format$(i, "##0"), 4) & " X")
      End If
      'ABILITAZIONE DEI PUNTI MOLA CORRISPONDENTI A QUELLI DEL ROTORE
      For i = 1 To npt
        Input #1, Raggio(i + 1), ANGOLO(i + 1), PRESSIONE(i + 1)
            If (TY_ROT = "FRONT") Or (CONTROLLORE = "INTERNAL") Then
          'PROFILO TRASVERSALE
          tX(i + 1) = Raggio(i + 1) * Cos(ANGOLO(i + 1) * PG / 180)
          tY(i + 1) = Raggio(i + 1) * Sin(ANGOLO(i + 1) * PG / 180)
        ElseIf (TY_ROT = "AXIAL") And (CONTROLLORE <> "INTERNAL") Then
          'PROFILO ASSIALE
          tX(i + 1) = Raggio(i + 1)
          tY(i + 1) = ANGOLO(i + 1) * PASSO / 360
        End If
        Line Input #2, stmp
        If (val(stmp) = 0) Then
          tN(i + 1) = i + 1
          Call CHK0.lstCORRETTORIONOFF2.AddItem(Right$(String$(4, " ") & Format$(i, "##0"), 4) & " -")
        Else
          NptABILITATI = NptABILITATI + 1
          Call CHK0.lstCORRETTORIONOFF2.AddItem(Right$(String$(4, " ") & Format$(i, "##0"), 4) & " X")
        End If
      Next i
      'ABILITAZIONE DELL'ULTIMO PUNTO DELLA MOLA
      i = npt + 1
      Line Input #2, stmp
      If (val(stmp) = 0) Then
        tN(npt + 2) = npt + 2
        Call CHK0.lstCORRETTORIONOFF2.AddItem(Right$(String$(4, " ") & Format$(i, "##0"), 4) & " -")
      Else
        tN(npt + 2) = 0
        NptABILITATI = NptABILITATI + 1
        Call CHK0.lstCORRETTORIONOFF2.AddItem(Right$(String$(4, " ") & Format$(i, "##0"), 4) & " X")
      End If
      If (TY_ROT = "FRONT") Or (CONTROLLORE = "INTERNAL") Then
        'PROFILO TRASVERSALE
        tX(1) = tX(2) - 1: tX(npt + 2) = tX(npt + 1) - 1
        tY(1) = tY(2) - 1: tY(npt + 2) = tY(npt + 1) + 1
      ElseIf (TY_ROT = "AXIAL") And (CONTROLLORE <> "INTERNAL") Then
        'PROFILO ASSIALE
        tX(1) = tX(2): tX(npt + 2) = tX(npt + 1)
        tY(1) = tY(2) - 1: tY(npt + 2) = tY(npt + 1) + 1
      End If
      Close #1
      Close #2
      CHK0.lstCORRETTORIONOFF.ListIndex = 0
      If (NptABILITATI < npt + 2) Then
        CHK0.lblCORRETTORIONOFF2(0).ForeColor = QBColor(15)
        CHK0.lblCORRETTORIONOFF2(0).BackColor = QBColor(12)
        CHK0.lblCORRETTORIONOFF2(0).Caption = " ENABLED " & Format$(NptABILITATI, "######0") & "/" & Format$(npt + 2, "######0") & " "
      Else
        CHK0.lblCORRETTORIONOFF2(0).ForeColor = CHK0.lblScY.ForeColor
        CHK0.lblCORRETTORIONOFF2(0).BackColor = CHK0.lblScY.BackColor
        CHK0.lblCORRETTORIONOFF2(0).Caption = "All correctors ENABLED"
      End If
      NPM1 = val(GetInfo("CREAZIONE", "NpM_F1", RADICE & "\INFO.INI"))
      NPM2 = val(GetInfo("CREAZIONE", "NpM_F2", RADICE & "\INFO.INI"))
      NP1 = val(GetInfo("CREAZIONE", "NpC_F1", RADICE & "\INFO.INI"))
      NP2 = val(GetInfo("CREAZIONE", "NpC_F2", RADICE & "\INFO.INI"))
      NPF1 = NPM1
      'CALCOLO DELLE CORREZIONI PROPOSTE
      SCALA_ERRORI = val(CHK0.txtScY.Text)
      If (SCALA_ERRORI <= 0) Then
        SCALA_ERRORI = 50
        CHK0.txtScY.Text = "50"
      End If
      Call CALCOLO_CORREZIONI(CorZ, corY, Np, npt, NP1, NP2, NPM1, NPM2, RADICE, TY_ROT, CONTROLLORE, ER)
      '
      ReDim Preserve ER(npt + 2)
      ReDim Preserve errPNT(npt + 2)
      ER(npt + 2) = ER(npt): ER(npt + 1) = ER(npt)
      errPNT(npt + 2) = ER(npt): errPNT(npt + 1) = ER(npt)
      For i = npt To 2 Step -1
        ER(i) = ER(i - 1)
        errPNT(i) = ER(i)
      Next i
      '
      Dim MinEr As Double
      Dim MaxEr As Double
      MaxEr = -999: MinEr = 999
      For i = 1 To npt + 2
        If (ER(i) > MaxEr) Then MaxEr = ER(i)
        If (ER(i) < MinEr) Then MinEr = ER(i)
      Next i
      '
      Dim TA() As Double
      ReDim TA(npt + 2)
          If (TY_ROT = "FRONT") Or (CONTROLLORE = "INTERNAL") Then
        'PROFILO TRASVERSALE
        i = 1
          TA(i) = Abs((ANGOLO(i + 1) + PRESSIONE(i + 1)) * PG / 180)
        For i = 2 To npt + 1
          TA(i) = Abs((ANGOLO(i) + PRESSIONE(i)) * PG / 180)
        Next i
        i = npt + 2
          TA(i) = Abs((ANGOLO(i - 1) + PRESSIONE(i - 1)) * PG / 180)
      ElseIf (TY_ROT = "AXIAL") And (CONTROLLORE <> "INTERNAL") Then
        'PROFILO ASSIALE
        i = 1
          TA(i) = Abs(Atn(Tan(PRESSIONE(i + 1) * PG / 180) * PASSO / (2 * Raggio(i + 1) * PG)))
        For i = 2 To npt + 1
          TA(i) = Abs(Atn(Tan(PRESSIONE(i) * PG / 180) * PASSO / (2 * Raggio(i) * PG)))
        Next i
        i = npt + 2
          TA(i) = Abs(Atn(Tan(PRESSIONE(i - 1) * PG / 180) * PASSO / (2 * Raggio(i - 1) * PG)))
      End If
      '
      ReDim tX1(npt + 2): ReDim tY1(npt + 2): ReDim tN1(npt + 2)
      'FASCIA DI TOLLERANZA
      Dim MaxToll As Double
      Dim MinToll As Double
      Dim ErrToll As Double
      ReDim tXn(npt + 2): ReDim tYn(npt + 2): ReDim tNn(npt + 2)
      ReDim tXp(npt + 2): ReDim tYp(npt + 2): ReDim tNp(npt + 2)
      MaxToll = -999: MinToll = -999
      Open RADICE & "\PRFTOL.DAT" For Input As #1
      For i = 2 To npt + 1
        Input #1, tNn(i), tNp(i)
        tNp(i) = -tNp(i)
        If Abs(tNn(i)) > MinToll Then MinToll = Abs(tNn(i))
        If Abs(tNp(i)) > MaxToll Then MaxToll = Abs(tNp(i))
      Next i
      tNn(1) = tNn(2):             tNp(1) = tNp(2)
      tNn(npt + 2) = tNn(npt + 1): tNp(npt + 2) = tNp(npt + 1)
      Close #1
      'Escursione massima permessa
      ErrToll = -999
      If (Abs(MaxToll) > ErrToll) Then ErrToll = Abs(MaxToll)
      If (Abs(MinToll) > ErrToll) Then ErrToll = Abs(MinToll)
      If (ErrToll = 0) Then
        If (Abs(MaxEr) > ErrToll) Then ErrToll = Abs(MaxEr)
        If (Abs(MinEr) > ErrToll) Then ErrToll = Abs(MinEr)
      End If
      SCALA_ERRORI = SCALA_ERRORI * (0.1 / ErrToll)
      'negativa
       j = 1
          tXn(j) = tX(j) - SCALA_ERRORI * tNn(j) * Sin(TA(j))
          tYn(j) = tY(j) - SCALA_ERRORI * tNn(j) * Cos(TA(j))
      For j = 2 To npt + 1
          tXn(j) = tX(j) - SCALA_ERRORI * tNn(j) * Sin(TA(j))
        If (ANGOLO(j) + PRESSIONE(j)) > 0 Then
          tYn(j) = tY(j) + SCALA_ERRORI * tNn(j) * Cos(TA(j))
        Else
          tYn(j) = tY(j) - SCALA_ERRORI * tNn(j) * Cos(TA(j))
        End If
      Next j
       j = npt + 2
          tXn(j) = tX(j) - SCALA_ERRORI * tNn(j) * Sin(TA(j))
          tYn(j) = tY(j) + SCALA_ERRORI * tNn(j) * Cos(TA(j))
      'positiva
       j = 1
          tXp(j) = tX(j) - SCALA_ERRORI * tNp(j) * Sin(TA(j))
          tYp(j) = tY(j) - SCALA_ERRORI * tNp(j) * Cos(TA(j))
      For j = 2 To npt + 1
          tXp(j) = tX(j) - SCALA_ERRORI * tNp(j) * Sin(TA(j))
        If (ANGOLO(j) + PRESSIONE(j)) > 0 Then
          tYp(j) = tY(j) + SCALA_ERRORI * tNp(j) * Cos(TA(j))
        Else
          tYp(j) = tY(j) - SCALA_ERRORI * tNp(j) * Cos(TA(j))
        End If
      Next j
       j = npt + 2
          tXp(j) = tX(j) - SCALA_ERRORI * tNp(j) * Sin(TA(j))
          tYp(j) = tY(j) + SCALA_ERRORI * tNp(j) * Cos(TA(j))
      'PROFILO CONTROLLATO
      Dim RaggioMinimo  As Double
      Dim RaggioMassimo As Double
      RaggioMinimo = 1E+99
      RaggioMassimo = -1E+99
       j = 1
          tX1(j) = tX(j) - SCALA_ERRORI * ER(j) * Sin(TA(j))
          tY1(j) = tY(j) - SCALA_ERRORI * ER(j) * Cos(TA(j))
        dtmp = Sqr((tX(j) - ER(j) * Sin(TA(j))) ^ 2 + (tY(j) + ER(j) * Cos(TA(j))) ^ 2)
        If (dtmp < RaggioMinimo) Then RaggioMinimo = dtmp
        If (dtmp > RaggioMassimo) Then RaggioMassimo = dtmp
      For j = 2 To npt + 1
          tX1(j) = tX(j) - SCALA_ERRORI * ER(j) * Sin(TA(j))
        If (ANGOLO(j) + PRESSIONE(j)) > 0 Then
          tY1(j) = tY(j) + SCALA_ERRORI * ER(j) * Cos(TA(j))
          dtmp = Sqr((tX(j) - ER(j) * Sin(TA(j))) ^ 2 + (tY(j) - ER(j) * Cos(TA(j))) ^ 2)
        Else
          tY1(j) = tY(j) - SCALA_ERRORI * ER(j) * Cos(TA(j))
          dtmp = Sqr((tX(j) - ER(j) * Sin(TA(j))) ^ 2 + (tY(j) + ER(j) * Cos(TA(j))) ^ 2)
        End If
        If (dtmp < RaggioMinimo) Then RaggioMinimo = dtmp
        If (dtmp > RaggioMassimo) Then RaggioMassimo = dtmp
      Next j
       j = npt + 2
          tX1(j) = tX(j) - SCALA_ERRORI * ER(j) * Sin(TA(j))
          tY1(j) = tY(j) + SCALA_ERRORI * ER(j) * Cos(TA(j))
        dtmp = Sqr((tX(j) - ER(j) * Sin(TA(j))) ^ 2 + (tY(j) - ER(j) * Cos(TA(j))) ^ 2)
        If (dtmp < RaggioMinimo) Then RaggioMinimo = dtmp
        If (dtmp > RaggioMassimo) Then RaggioMassimo = dtmp
      'INVIO DEL VALORE DEL DIAMETRO MINIMO *********************************************
      Dim ITEMDDE As String
      ITEMDDE = GetInfo("PRODUZIONE", "DIAMINDDE", Path_LAVORAZIONE_INI)
      ITEMDDE = UCase$(Trim$(ITEMDDE))
      If (Len(ITEMDDE) > 0) Then Call OPC_SCRIVI_DATO(ITEMDDE, frmt(RaggioMinimo * 2, 3))
      '**********************************************************************************
      'ERRORI NORMALI AL PROFILO TEORICO
      For j = 1 To npt + 2
        Call CHK0.Grafico1.AddLinea(tY1(j), tX1(j), tY(j), tX(j), fiAquamarine, Format$(j, "######0"))
      Next j
      Call CHK0.Grafico1.SetLingua("UK")
      Call CHK0.Grafico1.AddSerieXY(tY, tX, tX, fiblack, True, True, "1")  'PROFILO TEORICO
      Call CHK0.Grafico1.AddSerieXY(tY1, tX1, tX1, fired, False, False, "2")
      Call CHK0.Grafico1.AddSerieXY(tYn, tXn, tXn, fiblack, False, False, "3")
      Call CHK0.Grafico1.AddSerieXY(tYp, tXp, tXp, fiblack, False, False, "4")
      Call CHK0.Grafico1.SelPunti("1", tN)
      CHK0.Grafico1.INIZIO_PUNTI = -1
      CHK0.Grafico1.TIPO_PROFILO = TY_ROT
      CHK0.Grafico1.griglia = True
      CHK0.Grafico1.Agganciare = True
      If (CHK0.fraVANI.Visible = True) Then
      CHK0.cmdCORRETTORIONOFF2.Left = CHK0.fraVANI.Left
      CHK0.cmdCORRETTORIONOFF2.Top = CHK0.fraVANI.Top + CHK0.fraVANI.Height
      CHK0.cmdCORRETTORIONOFF2.Width = CHK0.fraVANI.Width
      CHK0.cmdABILITA.Left = CHK0.fraVANI.Left
      CHK0.cmdABILITA.Top = CHK0.fraVANI.Top + CHK0.fraVANI.Height + CHK0.cmdCORRETTORIONOFF2.Height
      CHK0.cmdABILITA.Width = CHK0.fraVANI.Width
      Else
      CHK0.cmdCORRETTORIONOFF2.Left = CHK0.lblScY.Left
      CHK0.cmdCORRETTORIONOFF2.Top = CHK0.lblScY.Top + CHK0.lblScY.Height
      CHK0.cmdCORRETTORIONOFF2.Width = CHK0.lblScY.Width + CHK0.txtScY.Width
      CHK0.cmdABILITA.Left = CHK0.lblScY.Left
      CHK0.cmdABILITA.Top = CHK0.lblScY.Top + CHK0.lblScY.Height + CHK0.cmdCORRETTORIONOFF2.Height
      CHK0.cmdABILITA.Width = CHK0.cmdCORRETTORIONOFF2.Width
      End If
      CHK0.cmdCORRETTORIONOFF2.Visible = True
      '
      stmp = GetInfo("Configurazione", "PERCORSO_CONTROLLI", Path_LAVORAZIONE_INI)
      stmp = UCase$(Trim$(stmp))
      If (Len(stmp) > 0) Then CHK0.cmdABILITA.Visible = True Else CHK0.cmdABILITA.Visible = False
      '
      CHK0.lblCORRETTORIONOFF2(0).Left = CHK0.fraVANI.Left
      CHK0.lblCORRETTORIONOFF2(0).Top = CHK0.Grafico1.Top
      CHK0.lblCORRETTORIONOFF2(0).Width = CHK0.lblScY.Width + CHK0.txtScY.Width
      CHK0.lblCORRETTORIONOFF2(0).Height = CHK0.txtScY.Height
      CHK0.lblCORRETTORIONOFF2(0).Visible = True
      CHK0.lblCORRETTORIONOFF2(1).Font.Name = "Arial Narrow"
      CHK0.lblCORRETTORIONOFF2(1).Font.Size = 8
      CHK0.lblCORRETTORIONOFF2(1).Font.Bold = False
      CHK0.lblCORRETTORIONOFF2(1).Left = CHK0.cmdCORRETTORIONOFF2.Left
      If (CHK0.cmdABILITA.Visible = True) Then
      CHK0.lblCORRETTORIONOFF2(1).Top = CHK0.cmdCORRETTORIONOFF2.Top + 2 * CHK0.cmdCORRETTORIONOFF2.Height
      Else
      CHK0.lblCORRETTORIONOFF2(1).Top = CHK0.cmdCORRETTORIONOFF2.Top + 1 * CHK0.cmdCORRETTORIONOFF2.Height
      End If
      CHK0.lblCORRETTORIONOFF2(1).Width = CHK0.cmdCORRETTORIONOFF2.Width
      CHK0.lblCORRETTORIONOFF2(1).Height = 4 * CHK0.cmdCORRETTORIONOFF2.Height
      stmp = ""
      stmp = stmp & Chr(13) & " Rotor: " & LEGGI_NomePezzo("ROTORI_ESTERNI")
      stmp = stmp & Chr(13) & " Inspection: " & CONTROLLORE
      stmp = stmp & Chr(13) & " Inspection type: " & TY_ROT
      stmp = stmp & Chr(13) & " tol(sup:" & frmt(MaxToll, 4) & " inf:" & frmt(MinToll, 4) & ")"
      stmp = stmp & Chr(13) & " dev(Max:" & frmt(MaxEr, 3) & " Min:" & frmt(MinEr, 3) & ")"
      stmp = stmp & Chr(13) & " dia(Max:" & frmt(RaggioMassimo * 2, 3) & " Min:" & frmt(RaggioMinimo * 2, 3) & ")"
      CHK0.lblCORRETTORIONOFF2(1).Caption = stmp
      CHK0.lblCORRETTORIONOFF2(1).Visible = True
      '
    Case "CONTROLLOONOFF"
      'ABILITAZIONE/DISABILITAZIONE PUNTI DA CONTROLLARE
      CHK0.Grafico1.Visible = False
      CHK0.lstCORRETTORIONOFF2.Visible = False
      CHK0.cmdCORRETTORIONOFF2.Visible = False
      CHK0.cmdABILITA.Visible = False
      CHK0.lblDIAMETRO(0).Visible = False
      CHK0.txtDIAMETRO(0).Visible = False
      CHK0.lblDIAMETRO(1).Visible = False
      CHK0.txtDIAMETRO(1).Visible = False
      CHK0.lblDIAMETRO(2).Visible = False
      CHK0.txtDIAMETRO(2).Visible = False
      CHK0.fraVANI.Visible = False
      CHK0.lblScX.Visible = False
      CHK0.txtScX.Visible = False
      CHK0.lblScY.Visible = False
      CHK0.txtScY.Visible = False
      CHK0.fraCORRETTORIONOFF.Visible = False
      CHK0.fraVISUALIZZAZIONE_CORRETTORI.Visible = False
      CHK0.fraTABELLA_CORRETTORI.Visible = False
      CHK0.fraEDITOR.Visible = False
      CHK0.txtHELP.Visible = False
      '
      CHK0.fraCONTROLLOONOFF.Height = CHK0.Picture1.Height
      CHK0.fraCONTROLLOONOFF.Top = CHK0.Image1.Height
      CHK0.fraCONTROLLOONOFF.Left = 0
      CHK0.fraCONTROLLOONOFF.Width = CHK0.Image1.Width + CHK0.INTESTAZIONE.Width
      '
      CHK0.grfCONTROLLOONOFF.Visible = True
      '
      CHK0.lblCONTROLLOONOFF.Top = 0
      CHK0.lblCONTROLLOONOFF.Left = 0
      CHK0.lblCONTROLLOONOFF.Height = 500
      CHK0.lblCONTROLLOONOFF.Width = 1000
      '
      CHK0.lstCONTROLLOONOFF.Top = CHK0.lblCONTROLLOONOFF.Top + CHK0.lblCONTROLLOONOFF.Height
      CHK0.lstCONTROLLOONOFF.Left = CHK0.lblCONTROLLOONOFF.Left
      CHK0.lstCONTROLLOONOFF.Height = 4000
      CHK0.lstCONTROLLOONOFF.Width = 1000
      CHK0.lstCONTROLLOONOFF.Font.Name = "Courier New"
      '
      CHK0.grfCONTROLLOONOFF.Left = CHK0.lblCONTROLLOONOFF.Left + CHK0.lblCONTROLLOONOFF.Width
      CHK0.grfCONTROLLOONOFF.Top = 0
      CHK0.grfCONTROLLOONOFF.Width = 8000
      CHK0.grfCONTROLLOONOFF.Height = 4800
      '
      CHK0.lblCONTROLLOONOFF.Font.Name = "Courier New"
      stmp1 = GetInfo("CONTROLLO", "lblSI", Path_LAVORAZIONE_INI)
      stmp2 = GetInfo("CONTROLLO", "lblNO", Path_LAVORAZIONE_INI)
      stmp = ""
      stmp = stmp & stmp1 & " -> -" & Chr(13)
      stmp = stmp & stmp2 & " -> X"
      CHK0.lblCONTROLLOONOFF.Caption = stmp
      '
      CHK0.cmdCONTROLLOONOFF.Top = CHK0.lstCONTROLLOONOFF.Top + CHK0.lstCONTROLLOONOFF.Height
      CHK0.cmdCONTROLLOONOFF.Left = CHK0.lblCONTROLLOONOFF.Left
      CHK0.cmdCONTROLLOONOFF.Height = 500
      CHK0.cmdCONTROLLOONOFF.Width = 1000
      '
      Call LEGGI_INFO_ROTORI(PERCORSO, COORDINATE, RADICE, PERCORSO_CONTROLLI, PERCORSO_PROFILI, sequenza, npt, TY_ROT, PASSO, CONTROLLORE, DM_ROT, INVERSIONE)
      '
      CHK0.lstCONTROLLOONOFF.Clear
      For i = 1 To npt
        stmp = GetInfo("PuntiControllo", "CHK[0," & CStr(i) & "]", RADICE & "\CHKRAB.ROT")
        If (val(stmp) = 0) Then
          Call CHK0.lstCONTROLLOONOFF.AddItem(Right$(String$(3, " ") & Format$(i, "##0"), 3) & " X")
        Else
          Call CHK0.lstCONTROLLOONOFF.AddItem(Right$(String$(3, " ") & Format$(i, "##0"), 3) & " -")
        End If
      Next i
      ReDim IdxCHKtmp(npt)
      For i = 1 To npt
        If (Right$(CHK0.lstCONTROLLOONOFF.List(i - 1), 1) = "X") Then
          IdxCHKtmp(i) = 0  'DA NON CONTROLLARE
        Else
          IdxCHKtmp(i) = 1  'DA CONTROLLARE
        End If
      Next i
      ReDim tX(npt): ReDim tY(npt): ReDim tN(0)
      ReDim Raggio(npt): ReDim ANGOLO(npt): ReDim PRESSIONE(npt)
      Open RADICE & "\PRFROT" For Input As #1
      For i = 1 To npt
        Input #1, Raggio(i), ANGOLO(i), PRESSIONE(i)
            If (TY_ROT = "FRONT") Or (CONTROLLORE = "INTERNAL") Then
          tX(i) = Raggio(i) * Cos(ANGOLO(i) * PG / 180)
          tY(i) = Raggio(i) * Sin(ANGOLO(i) * PG / 180)
        ElseIf (TY_ROT = "AXIAL") And (CONTROLLORE <> "INTERNAL") Then
          tX(i) = Raggio(i)
          tY(i) = ANGOLO(i) * PASSO / 360
        End If
        If (IdxCHKtmp(i) = 0) Then
          ReDim Preserve tN(UBound(tN) + 1)
          tN(UBound(tN)) = i
        End If
      Next i
      Close #1
      Call CHK0.grfCONTROLLOONOFF.SetLingua("UK")
      Call CHK0.grfCONTROLLOONOFF.AddSerieXY(tY, tX, tX, fiblack, True, True, "1")  'PROFILO TEORICO
      Call CHK0.grfCONTROLLOONOFF.SelPunti("1", tN)
      CHK0.grfCONTROLLOONOFF.griglia = True
      CHK0.grfCONTROLLOONOFF.INIZIO_PUNTI = 0
      CHK0.grfCONTROLLOONOFF.TIPO_PROFILO = TY_ROT
      CHK0.grfCONTROLLOONOFF.Agganciare = True
      CHK0.fraCONTROLLOONOFF.Visible = True
      '
      Dim NptCONTROLLATI As Long
      For i = 1 To npt
        j = GetInfo("PuntiControllo", "CHK[0," & CStr(i) & "]", RADICE & "\CHKRAB.ROT")
        If (j = 1) Then NptCONTROLLATI = NptCONTROLLATI + 1
      Next i
      If (NptCONTROLLATI < npt) Then
        stmp = " CHECKED " & Format$(NptCONTROLLATI, "######0") & "/" & Format$(npt, "######0") & " "
      Else
        stmp = " All points CHECKED!! "
      End If
      WRITE_DIALOG stmp
      '
  End Select
  '
Exit Sub

ErrCONTROLLO_ROTORI_VISUALIZZAZIONE:
  i = Err
  If FreeFile > 0 Then Close
  StopRegieEvents
  'MsgBox "ERROR: " & Error(i), 48, "SUB: CONTROLLO_ROTORI_VISUALIZZAZIONE"
  ResumeRegieEvents
  'Resume Next
  Exit Sub
     
End Sub

Sub VISUALIZZA_MOLA_RAB(ByVal NomeForm As Form, ByVal Scala As Single, ByVal SiStampa As String, ByVal SiPUNTI As Integer, ByVal SiASSI As Integer, ByVal IndiceZeroPezzoX As Integer, ByVal IndiceZeroPezzoY As Integer)
'
Dim Rmin          As Double
Dim iMIN          As Integer
Dim RaggioINTmin  As Double
Dim iINTmin       As Integer
Dim tMIN(2)       As Double
Dim jMIN(2)       As Integer
Dim DR            As Double
Dim iDR           As Integer
Dim ProfiloMola   As String
'
Dim XX(2, 999) As Double
Dim YY(2, 999) As Double
Dim TT(2, 999) As Double
'
Dim Codice As String
Dim Senso  As Integer
'
Dim stmp As String
Dim ftmp As Double
Dim i    As Integer
Dim j    As Integer
'
Dim NpF(2)    As Integer
Dim AngStart  As Double
Dim AngEnd    As Double
Dim Ang       As Double
Dim DeltaAng  As Double
Dim DeltaArco As Double
'
Dim PicW As Single
Dim PicH As Single
Dim pX1  As Double
Dim pY1  As Double
Dim pX2  As Double
Dim pY2  As Double
'
Dim ColoreCurva As Integer
Dim Raggio      As Double
'
Dim RaggioMassimo  As Double
Dim XX_Separazione As Double
'
Dim R160 As Double
Dim R161 As Double
Dim R162 As Double
Dim R163 As Double
Dim R164 As Double
Dim R165 As Double
Dim R166 As Double
Dim R167 As Double
Dim R168 As Double
Dim R169 As Double
Dim R170 As Double
Dim R171 As Double
Dim R172 As Double
Dim R173 As Double
Dim R174 As Double
'
Dim ZeroPezzoX As Double
Dim ZeroPezzoY As Double
'
Dim Xmax As Double
Dim Ymax As Double
Dim Xmin As Double
Dim Ymin As Double
'
Dim ScalaX As Double
Dim ScalaY As Double
'
Dim sL(10, 2) As String
'
Dim TabXMax As Single
Dim TabX    As Single
Dim TabY    As Single
'
Dim DBB As Database
Dim RSS As Recordset
'
Dim DF As Single
Dim AF As Single
Dim NumeroRette As Integer
Dim NumeroArchi As Integer
Dim TipoCarattere As String
'
Dim jj      As Integer
Dim sERRORE As String
'
On Error GoTo errVISUALIZZA_MOLA_RAB
  '
  DF = val(GetInfo("DIS0", "DF", Path_LAVORAZIONE_INI))
  AF = val(GetInfo("DIS0", "AF", Path_LAVORAZIONE_INI))
  NumeroRette = val(GetInfo("DIS0", "NumeroRette", Path_LAVORAZIONE_INI))
  NumeroArchi = val(GetInfo("DIS0", "NumeroArchi", Path_LAVORAZIONE_INI))
  '
  If DF = 0 Then DF = 2
  If AF = 0 Then AF = 30
  If NumeroRette = 0 Then NumeroRette = 20
  If NumeroArchi = 0 Then NumeroArchi = 50
  '
      If (LINGUA = "CH") Then
    TipoCarattere = "MS Song"
  ElseIf (LINGUA = "RU") Then
    TipoCarattere = "Arial Cyr"
  Else
    TipoCarattere = "Courier New"
  End If
  ScalaX = Scala: ScalaY = Scala
  'Acquisizione profilo mola
  'FIANCO 1
  ProfiloMola = "MOLA_F1.XYT"
  If (Dir$(g_chOemPATH & "\" & ProfiloMola) <> "") Then
    tMIN(1) = 1E+308: jMIN(1) = 0
    Open g_chOemPATH & "\" & ProfiloMola For Input As #1
      i = 1
      While Not EOF(1)
        Input #1, XX(1, i), YY(1, i), TT(1, i)
        If tMIN(1) > Abs(TT(1, i)) Then tMIN(1) = Abs(TT(1, i)): jMIN(1) = i
        i = i + 1
      Wend
    Close #1
    NpF(1) = i - 1
  Else
    WRITE_DIALOG ProfiloMola & ": " & Error(53)
    Exit Sub
  End If
  'FIANCO 2
  ProfiloMola = "MOLA_F2.XYT"
  If (Dir$(g_chOemPATH & "\" & ProfiloMola) <> "") Then
    tMIN(2) = 1E+308: jMIN(2) = 0
    Open g_chOemPATH & "\" & ProfiloMola For Input As #1
      i = 1
      While Not EOF(1)
        Input #1, XX(2, i), YY(2, i), TT(2, i)
        If tMIN(2) > Abs(TT(2, i)) Then tMIN(2) = Abs(TT(2, i)): jMIN(2) = i
        i = i + 1
      Wend
    Close #1
    NpF(2) = i - 1
  Else
    WRITE_DIALOG ProfiloMola & ": " & Error(53)
    Exit Sub
  End If
  NpF(0) = NpF(1) + NpF(2) - 1
  '
  'Coordinata XX di separazione tra il primo e secondo fianco
  XX_Separazione = XX(1, NpF(1))
  '
  'Acquisizione area grafica
  stmp = GetInfo("DIS0", "PicW", Path_LAVORAZIONE_INI)
  If Len(stmp) > 0 Then PicW = val(stmp) Else PicW = 190
  stmp = GetInfo("DIS0", "PicH", Path_LAVORAZIONE_INI)
  If Len(stmp) > 0 Then PicH = val(stmp) Else PicH = 95
  '
  NomeForm.OEM1PicCALCOLO.ScaleWidth = PicW
  NomeForm.OEM1PicCALCOLO.ScaleHeight = PicH
  '
  Dim OGGETTO As Control
  '
  'Preparazione grafico
  If (SiStampa = "Y") Then
    For i = 1 To 2
      Printer.FontName = TipoCarattere
      Printer.FontSize = 8
      Printer.FontBold = False
    Next i
    WRITE_DIALOG Printer.DeviceName & " PRINTING....."
    If PicW = 190 And PicH = 95 Then
      Printer.Orientation = 1
    Else
      Printer.Orientation = 2
    End If
    Printer.ScaleMode = 6
    Set OGGETTO = Printer
  Else
    NomeForm.OEM1PicCALCOLO.Cls
    NomeForm.OEM1PicCALCOLO.ScaleWidth = PicW
    NomeForm.OEM1PicCALCOLO.ScaleHeight = PicH
    For i = 1 To 2
      NomeForm.OEM1PicCALCOLO.FontName = TipoCarattere
      NomeForm.OEM1PicCALCOLO.FontSize = 8
      NomeForm.OEM1PicCALCOLO.FontBold = False
    Next i
    NomeForm.OEM1PicCALCOLO.Refresh
    Set OGGETTO = NomeForm.OEM1PicCALCOLO
  End If
  '
  Call CALCOLO_INGRANAGGIO_EQUIVALENTE(NomeForm, SiStampa)
  '
  If tMIN(1) > tMIN(2) Then tMIN(0) = tMIN(2): jMIN(0) = jMIN(2) Else tMIN(0) = tMIN(1): jMIN(0) = jMIN(1)
  stmp = " ANGLE MIN= " & frmt(tMIN(0), 4) & " / " & Format$(jMIN(0)) & " "
  OGGETTO.CurrentX = PicW - OGGETTO.TextWidth(stmp)
  OGGETTO.CurrentY = PicH - OGGETTO.TextHeight(stmp)
  OGGETTO.Print stmp
  '
  stmp = " WHEEL FILE= " & "MOLA_FX.XYT"
  OGGETTO.CurrentX = PicW / 2 - OGGETTO.TextWidth(stmp) / 2
  OGGETTO.CurrentY = PicH - OGGETTO.TextHeight(stmp)
  OGGETTO.Print stmp
  '
  If SiASSI = 1 Then Call SCRIVI_ASSI(NomeForm.OEM1PicCALCOLO, SiStampa)
  '
  'Ricerca degli estremi
  Xmax = -1E+308: Ymax = -1E+308: Xmin = 1E+308: Ymin = 1E+308
  For i = 1 To NpF(1)
    If XX(1, i) >= Xmax Then Xmax = XX(1, i)
    If YY(1, i) >= Ymax Then Ymax = YY(1, i)
    If XX(1, i) <= Xmin Then Xmin = XX(1, i)
    If YY(1, i) <= Ymin Then Ymin = YY(1, i)
  Next i
  For i = 1 To NpF(2)
    If XX(2, i) >= Xmax Then Xmax = XX(2, i)
    If YY(2, i) >= Ymax Then Ymax = YY(2, i)
    If XX(2, i) <= Xmin Then Xmin = XX(2, i)
    If YY(2, i) <= Ymin Then Ymin = YY(2, i)
  Next i
  '
  If (IndiceZeroPezzoX >= 1) And (IndiceZeroPezzoX <= NpF(1)) Then
    ZeroPezzoX = XX(1, IndiceZeroPezzoX)
  ElseIf (IndiceZeroPezzoX > NpF(1)) And (IndiceZeroPezzoX <= NpF(0)) Then
    ZeroPezzoX = XX(2, IndiceZeroPezzoX - NpF(1) + 1)
  Else
    ZeroPezzoX = 0
  End If
  If (IndiceZeroPezzoY >= 1) And (IndiceZeroPezzoY <= NpF(1)) Then
    ZeroPezzoY = YY(1, IndiceZeroPezzoY)
  ElseIf (IndiceZeroPezzoY > NpF(1)) And (IndiceZeroPezzoY <= NpF(0)) Then
    ZeroPezzoY = YY(2, IndiceZeroPezzoY - NpF(1) + 1)
  Else
    ZeroPezzoY = Ymin
  End If
  '
  'Calcolo scala se non � impostata
  If Scala = 0 Then
    ScalaX = PicW / (Xmax - Xmin): ScalaY = (PicH / 2) / (Ymax - Ymin)
    If ScalaY < ScalaX Then Scala = ScalaY Else Scala = ScalaX
    ScalaX = Scala: ScalaY = Scala
    NomeForm.txtINP(0).Text = frmt(Scala, 4)
  End If
  '
  'Calcolo dr
  DR = 1E+308
  iDR = 1
  For i = 1 To NpF(1) - 1
    ftmp = Sqr((XX(1, i + 1) - XX(1, i)) ^ 2 + (YY(1, i + 1) - YY(1, i)) ^ 2)
    If ftmp < DR Then DR = ftmp: iDR = i
  Next i
  For i = 1 To NpF(2) - 1
    ftmp = Sqr((XX(2, i + 1) - XX(2, i)) ^ 2 + (YY(2, i + 1) - YY(2, i)) ^ 2)
    If ftmp < DR Then DR = ftmp: iDR = i
  Next i
  If (DR = 0) Then
    stmp = "Duplicate point into WHEEL profile."
    StopRegieEvents
    MsgBox stmp, vbCritical, "WARNING: point " & Format$(iDR, "####0")
    ResumeRegieEvents
  End If
  '
  'Nomi dei fianchi
  ftmp = Xmin: stmp = " F1 ": Call VISUALIZZA_NOME(SiStampa, ftmp, stmp, Ymin, ZeroPezzoX, ZeroPezzoY, ScalaX, ScalaY, PicW, PicH, NomeForm.OEM1PicCALCOLO)
  ftmp = Xmax: stmp = " F2 ": Call VISUALIZZA_NOME(SiStampa, ftmp, stmp, Ymin, ZeroPezzoX, ZeroPezzoY, ScalaX, ScalaY, PicW, PicH, NomeForm.OEM1PicCALCOLO)
  '
  'Profilo mola
  Senso = 1                           'Indici dei punti esterni alla curva
  ColoreCurva = 0                     'Colore della curva
  sERRORE = ""
  '
  Rmin = 1E+308
  iMIN = 0
  RaggioINTmin = 1E+308
  iINTmin = 0
  'RaggioINTmax = -1E+308
  'iINTmax = 0
  'RaggioEXTmin = 1E+308
  'iEXTmin = 0
  'RaggioEXTmax = -1E+308
  'iEXTmax = 0
  '
  'FIANCO 1
    i = 1
      pX1 = (XX(1, i) - ZeroPezzoX) * ScalaX + PicW / 2
      pY1 = -(YY(1, i) - ZeroPezzoY) * ScalaY + PicH / 2
      pX2 = (XX(1, i + 1) - ZeroPezzoX) * ScalaX + PicW / 2
      pY2 = -(YY(1, i + 1) - ZeroPezzoY) * ScalaY + PicH / 2
      If (pY1 <= OGGETTO.ScaleHeight) And (pX1 <= OGGETTO.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
        OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
      End If
    jj = 0
    For i = 2 To NpF(1) - 2 Step 2
      'COORD. X        COORD. Y
      R164 = XX(1, i + 0): R165 = YY(1, i + 0)
      R166 = XX(1, i + 1): R167 = YY(1, i + 1)
      R168 = XX(1, i + 2): R169 = YY(1, i + 2)
      'CONTROLLO REGOLARITA' PROFILO
      If (R166 < R164) Then
        If (jj = 0) Then
          sERRORE = "F1: PROFILE NOT CONGRUENT!!"
          OGGETTO.CurrentX = 0
          OGGETTO.CurrentY = 0
          OGGETTO.Print sERRORE
        End If
        sERRORE = " XX(" & Format$(i + 1, "#####0") & ") < XX(" & Format$(i, "#####0") & ")"
        OGGETTO.CurrentX = 0
        OGGETTO.CurrentY = (jj + 1) * OGGETTO.TextHeight(sERRORE)
        OGGETTO.Print sERRORE
        jj = jj + 1
      End If
      If (R168 < R166) Then
        If (jj = 0) Then
          sERRORE = "F1: PROFILE NOT CONGRUENT!!"
          OGGETTO.CurrentX = 0
          OGGETTO.CurrentY = 0
          OGGETTO.Print sERRORE
        End If
        sERRORE = " XX(" & Format$(i + 2, "#####0") & ") < XX(" & Format$(i + 1, "#####0") & ")"
        OGGETTO.CurrentX = 0
        OGGETTO.CurrentY = (jj + 1) * OGGETTO.TextHeight(sERRORE)
        OGGETTO.Print sERRORE
        jj = jj + 1
      End If
      'DISTANZA PER TRE PUNTI
      R162 = Abs((R165 - R169) * (R164 - R166) + (R167 - R165) * (R164 - R168)) / Sqr((R165 - R169) * (R165 - R169) + (R164 - R168) * (R164 - R168))
      If R162 < 0.0005 Then
        'TRATTO RETTILINEO
        'G1 Y=R169 X=R168
        pX1 = (R164 - ZeroPezzoX) * ScalaX + PicW / 2
        pY1 = -(R165 - ZeroPezzoY) * ScalaY + PicH / 2
        pX2 = (R168 - ZeroPezzoX) * ScalaX + PicW / 2
        pY2 = -(R169 - ZeroPezzoY) * ScalaY + PicH / 2
        If (pY1 <= OGGETTO.ScaleHeight) And (pX1 <= OGGETTO.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
        End If
      Else
        'TRATTO CURVILINEO
        If R169 = R167 Then R169 = R167 + 0.0001
        If R165 = R167 Then R165 = R167 + 0.0001
        R160 = (R164 - R166) / (R167 - R165)
        R171 = (R166 - R168) / (R169 - R167)
        R172 = (R165 + R167) / 2 - R160 * (R164 + R166) / 2
        R173 = (R169 + R167) / 2 - R171 * (R168 + R166) / 2
        If R160 = R171 Then R160 = R171 + 0.0001
        'coordinate del centro
        R174 = (R173 - R172) / (R160 - R171)
        R170 = R160 * R174 + R172
        'raggio del cerchio
        R161 = Sqr((R174 - R166) * (R174 - R166) + (R170 - R167) * (R170 - R167))
        'AGGIORNAMENTO DEL VALORE DI RAGGIO MINIMO
        If Rmin > R161 Then Rmin = R161: iMIN = i
        'prodotto misto per senso di rotazione
        R160 = (R166 - R164) * (R169 - R165) - (R168 - R164) * (R167 - R165)
        'Calcolo angoli compresi tra 0 e 2 * Pigreco
        AngStart = CALCOLO_ANGOLO(R164 - R174, R165 - R170)
        AngEnd = CALCOLO_ANGOLO(R168 - R174, R169 - R170)
        'MsgBox "AngEnd= " & AngEnd & " AngStart= " & AngStart
        'MsgBox "R161= " & R161 & " R170= " & R170
        If R160 > 0 Then
          If RaggioINTmin > R161 Then RaggioINTmin = R161: iINTmin = i
          'If RaggioINTmax < R161 Then RaggioINTmax = R161: iINTmax = i
          'G3 Y=R169 X=R168 CR=R161
          If AngEnd >= AngStart Then
            Ang = AngEnd - AngStart
          Else
            Ang = 2 * PG - (AngStart - AngEnd)
          End If
          DeltaAng = Ang / NumeroArchi
          pX1 = R164: pY1 = R165
          For j = 1 To NumeroArchi - 1
            Ang = (AngStart + j * DeltaAng)
            pX2 = R174 + R161 * Cos(Ang): pY2 = R170 + R161 * Sin(Ang)
            'Adattamento nella finestra grafica
            pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
            pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
            pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
            pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
            'Traccio la linea
            If (pY1 <= OGGETTO.ScaleHeight) And (pX1 <= OGGETTO.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
              OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
            End If
            'Aggiorno Px1 e Py1
            pX1 = R174 + R161 * Cos(Ang): pY1 = R170 + R161 * Sin(Ang)
          Next j
          pX2 = R168: pY2 = R169
          'Adattamento nella finestra grafica
          pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
          pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
          pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
          pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
          'Traccio la linea
          If (pY1 <= OGGETTO.ScaleHeight) And (pX1 <= OGGETTO.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
            OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
          End If
        Else
          'If RaggioEXTmin > R161 Then RaggioEXTmin = R161: iEXTmin = i
          'If RaggioEXTmax < R161 Then RaggioEXTmax = R161: iEXTmax = i
          'G2 Y=R169 X=R168 CR=R161
          If AngStart >= AngEnd Then
            Ang = AngStart - AngEnd
          Else
            Ang = 2 * PG - (AngEnd - AngStart)
          End If
          DeltaAng = Ang / NumeroArchi
          pX1 = R164: pY1 = R165
          For j = 1 To NumeroArchi - 1
            Ang = (AngStart - j * DeltaAng)
            pX2 = R174 + R161 * Cos(Ang): pY2 = R170 + R161 * Sin(Ang)
            'Adattamento nella finestra grafica
            pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
            pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
            pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
            pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
            'Traccio la linea
            If (pY1 <= OGGETTO.ScaleHeight) And (pX1 <= OGGETTO.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
              OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
            End If
            'Aggiorno Px1 e Py1
            pX1 = R174 + R161 * Cos(Ang): pY1 = R170 + R161 * Sin(Ang)
          Next j
          pX2 = R168: pY2 = R169
          'Adattamento nella finestra grafica
          pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
          pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
          pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
          pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
          'Traccio la linea
          If (pY1 <= OGGETTO.ScaleHeight) And (pX1 <= OGGETTO.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
            OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
          End If
        End If
      End If
    Next
    '
    'FIANCO 2
    i = NpF(2)
      pX1 = (XX(2, i) - ZeroPezzoX) * ScalaX + PicW / 2
      pY1 = -(YY(2, i) - ZeroPezzoY) * ScalaY + PicH / 2
      pX2 = (XX(2, i - 1) - ZeroPezzoX) * ScalaX + PicW / 2
      pY2 = -(YY(2, i - 1) - ZeroPezzoY) * ScalaY + PicH / 2
      If (pY1 <= OGGETTO.ScaleHeight) And (pX1 <= OGGETTO.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
        OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
      End If
    jj = 0
    For i = NpF(2) - 1 To 3 Step -2
      'COORD. X        COORD. Y
      R164 = XX(2, i - 0): R165 = YY(2, i - 0)
      R166 = XX(2, i - 1): R167 = YY(2, i - 1)
      R168 = XX(2, i - 2): R169 = YY(2, i - 2)
      'CONTROLLO REGOLARITA PROFILO
      If (R166 > R164) Then
        If (jj = 0) Then
          sERRORE = "F2: PROFILE NOT CONGRUENT!!"
          OGGETTO.CurrentX = PicW - OGGETTO.TextWidth(sERRORE)
          OGGETTO.CurrentY = 0
          OGGETTO.Print sERRORE
        End If
        sERRORE = " XX(" & Format$(i - 1, "#####0") & ") > XX(" & Format$(i, "#####0") & ")"
        OGGETTO.CurrentX = PicW - OGGETTO.TextWidth(sERRORE)
        OGGETTO.CurrentY = (jj + 1) * OGGETTO.TextHeight(sERRORE)
        OGGETTO.Print sERRORE
        jj = jj + 1
      End If
      If (R168 > R166) Then
        If (jj = 0) Then
          sERRORE = "F2: PROFILE NOT CONGRUENT!!"
          OGGETTO.CurrentX = PicW - OGGETTO.TextWidth(sERRORE)
          OGGETTO.CurrentY = 0
          OGGETTO.Print sERRORE
        End If
        sERRORE = " XX(" & Format$(i - 2, "#####0") & ") > XX(" & Format$(i - 1, "#####0") & ")"
        OGGETTO.CurrentX = PicW - OGGETTO.TextWidth(sERRORE)
        OGGETTO.CurrentY = (jj + 1) * OGGETTO.TextHeight(sERRORE)
        OGGETTO.Print sERRORE
        jj = jj + 1
      End If
      'DISTANZA PER TRE PUNTI
      R162 = Abs((R165 - R169) * (R164 - R166) + (R167 - R165) * (R164 - R168)) / Sqr((R165 - R169) * (R165 - R169) + (R164 - R168) * (R164 - R168))
      If R162 < 0.0005 Then
        'TRATTO RETTILINEO
        'G1 Y=R169 X=R168
        pX1 = (R164 - ZeroPezzoX) * ScalaX + PicW / 2
        pY1 = -(R165 - ZeroPezzoY) * ScalaY + PicH / 2
        pX2 = (R168 - ZeroPezzoX) * ScalaX + PicW / 2
        pY2 = -(R169 - ZeroPezzoY) * ScalaY + PicH / 2
        If (pY1 <= OGGETTO.ScaleHeight) And (pX1 <= OGGETTO.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
        End If
      Else
        'TRATTO CURVILINEO
        If R169 = R167 Then R169 = R167 + 0.0001
        If R165 = R167 Then R165 = R167 + 0.0001
        R160 = (R164 - R166) / (R167 - R165)
        R171 = (R166 - R168) / (R169 - R167)
        R172 = (R165 + R167) / 2 - R160 * (R164 + R166) / 2
        R173 = (R169 + R167) / 2 - R171 * (R168 + R166) / 2
        If R160 = R171 Then R160 = R171 + 0.0001
        'coordinate del centro
        R174 = (R173 - R172) / (R160 - R171)
        R170 = R160 * R174 + R172
        'raggio del cerchio
        R161 = Sqr((R174 - R166) * (R174 - R166) + (R170 - R167) * (R170 - R167))
        'AGGIORNAMENTO DEL VALORE DI RAGGIO MINIMO
        If Rmin > R161 Then Rmin = R161: iMIN = i
        'prodotto misto per senso di rotazione
        R160 = (R166 - R164) * (R169 - R165) - (R168 - R164) * (R167 - R165)
        'Calcolo angoli compresi tra 0 e 2 * Pigreco
        AngStart = CALCOLO_ANGOLO(R164 - R174, R165 - R170)
        AngEnd = CALCOLO_ANGOLO(R168 - R174, R169 - R170)
        'MsgBox "AngEnd= " & AngEnd & " AngStart= " & AngStart
        'MsgBox "R161= " & R161 & " R170= " & R170
        'MsgBox "SUPERFICIE_PROFILO= " & SUPERFICIE_PROFILO
        If R160 > 0 Then
          'If RaggioEXTmin > R161 Then RaggioEXTmin = R161: iEXTmin = i
          'If RaggioEXTmax < R161 Then RaggioEXTmax = R161: iEXTmax = i
          'G3 Y=R169 X=R168 CR=R161
          If AngEnd >= AngStart Then
            Ang = AngEnd - AngStart
          Else
            Ang = 2 * PG - (AngStart - AngEnd)
          End If
          DeltaAng = Ang / NumeroArchi
          pX1 = R164: pY1 = R165
          For j = 1 To NumeroArchi - 1
            Ang = (AngStart + j * DeltaAng)
            pX2 = R174 + R161 * Cos(Ang): pY2 = R170 + R161 * Sin(Ang)
            'Adattamento nella finestra grafica
            pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
            pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
            pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
            pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
            'Traccio la linea
            If (pY1 <= OGGETTO.ScaleHeight) And (pX1 <= OGGETTO.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
              OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
            End If
            'Aggiorno Px1 e Py1
            pX1 = R174 + R161 * Cos(Ang): pY1 = R170 + R161 * Sin(Ang)
          Next j
          pX2 = R168: pY2 = R169
          'Adattamento nella finestra grafica
          pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
          pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
          pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
          pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
          'Traccio la linea
          If (pY1 <= OGGETTO.ScaleHeight) And (pX1 <= OGGETTO.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
            OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
          End If
        Else
          If RaggioINTmin > R161 Then RaggioINTmin = R161: iINTmin = i
          'If RaggioINTmax < R161 Then RaggioINTmax = R161: iINTmax = i
          'G2 Y=R169 X=R168 CR=R161
          If AngStart >= AngEnd Then
            Ang = AngStart - AngEnd
          Else
            Ang = 2 * PG - (AngEnd - AngStart)
          End If
          DeltaAng = Ang / NumeroArchi
          pX1 = R164: pY1 = R165
          For j = 1 To NumeroArchi - 1
            Ang = (AngStart - j * DeltaAng)
            pX2 = R174 + R161 * Cos(Ang): pY2 = R170 + R161 * Sin(Ang)
            'Adattamento nella finestra grafica
            pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
            pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
            pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
            pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
            'Traccio la linea
            If (pY1 <= OGGETTO.ScaleHeight) And (pX1 <= OGGETTO.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
              OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
            End If
            'Aggiorno Px1 e Py1
            pX1 = R174 + R161 * Cos(Ang): pY1 = R170 + R161 * Sin(Ang)
          Next j
          pX2 = R168: pY2 = R169
          'Adattamento nella finestra grafica
          pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
          pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
          pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
          pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
          'Traccio la linea
          If (pY1 <= OGGETTO.ScaleHeight) And (pX1 <= OGGETTO.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
            OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
          End If
        End If
      End If
    Next
  '
  GoSub CREAZIONE_FILE_PROFILO_MOLA_DXF
  '
  'stmp = " R min TEO= " & frmt(Rmin, 4) & " / " & Format$(iMIN)
  stmp = " Ri MIN= " & frmt(RaggioINTmin, 4) & "mm -> " & Format$(iINTmin, "##0")
  OGGETTO.CurrentX = 0
  OGGETTO.CurrentY = PicH - OGGETTO.TextHeight(stmp)
  OGGETTO.Print stmp
  '
  If SiPUNTI = 1 Then GoSub PUNTI_PROFILO
  '
  'VISUALIZZA_SEPARAZIONE TRA I FIANCHI
  ColoreCurva = 12
  stmp = " CM(" & Format$(NpF(1), "###0") & ") "
  pX1 = (XX(1, NpF(1)) - ZeroPezzoX) * ScalaX + PicW / 2: pY1 = 0
  pX2 = (XX(1, NpF(1)) - ZeroPezzoX) * ScalaX + PicW / 2: pY2 = PicH
  If (pY1 <= OGGETTO.ScaleHeight) And (pX1 <= OGGETTO.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
    OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
    OGGETTO.CurrentX = pX1
    OGGETTO.CurrentY = 0
    OGGETTO.Print stmp
  End If
  '
  Call SCRIVI_LARGHEZZA(SiStampa, Xmin, Ymin, Xmax, Ymax, ZeroPezzoX, ZeroPezzoY, ScalaX, ScalaY, PicW, PicH, 9, NomeForm.OEM1PicCALCOLO)
  Call SCRIVI_ALTEZZA(SiStampa, -Abs(Xmax), Ymin, Abs(Xmax), Ymax, ZeroPezzoX, ZeroPezzoY, ScalaX, ScalaY, PicW, PicH, 9, NomeForm.OEM1PicCALCOLO)
  '
  If Len(sERRORE) <= 0 Then
    Call SCRIVI_SCALA_ORIZZONTALE(ScalaX, NomeForm.OEM1PicCALCOLO, SiStampa)
    'Il calcolo del raggio esterno � effettuato in RICERCA_MAX_MIN
    'INFORMAZIONI DA VISUALIZZARE
    stmp = " " & Codice
    pX1 = 0: Senso = 0
    pY1 = PicH / 2
    GoSub VISUALIZZA_INFORMAZIONI_PROFILO
  End If
  '
  'Segnalazione della fine delle operazioni
  If (SiStampa = "Y") Then
    Printer.EndDoc
    WRITE_DIALOG ""
  End If
                
Exit Sub

PUNTI_PROFILO:
  'FIANCO 1
  ColoreCurva = 9
  For i = 1 To NpF(1)
    'LINEA TANGENTE
    pX1 = (XX(1, i) - DR * Sin(TT(1, i) * PG / 180) - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(YY(1, i) + DR * Cos(TT(1, i) * PG / 180) - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = (XX(1, i) + DR * Sin(TT(1, i) * PG / 180) - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(YY(1, i) - DR * Cos(TT(1, i) * PG / 180) - ZeroPezzoY) * ScalaY + PicH / 2
    OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
    'LINEA NORMALE
    pX1 = (XX(1, i) - DR * Cos(TT(1, i) * PG / 180) - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(YY(1, i) - DR * Sin(TT(1, i) * PG / 180) - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = (XX(1, i) + DR * Cos(TT(1, i) * PG / 180) - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(YY(1, i) + DR * Sin(TT(1, i) * PG / 180) - ZeroPezzoY) * ScalaY + PicH / 2
    OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
    'indice del punto
    stmp = " " & Format$(i, "##0") & " "
    Select Case Senso
      Case 1
        pX1 = pX1 - OGGETTO.TextWidth(stmp)
        pY1 = pY1 - OGGETTO.TextHeight(stmp)
        If (pY1 <= OGGETTO.ScaleHeight) And (pX1 <= OGGETTO.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          OGGETTO.CurrentX = pX1
          OGGETTO.CurrentY = pY1
          OGGETTO.Print stmp
        End If
      Case -1
        If (pY2 <= OGGETTO.ScaleHeight) And (pX2 <= OGGETTO.ScaleWidth) And (pX2 >= 0) And (pY2 >= 0) Then
          OGGETTO.CurrentX = pX2
          OGGETTO.CurrentY = pY2
          OGGETTO.Print stmp
        End If
    End Select
    'cerchio
    R174 = XX(1, i): R170 = YY(1, i): Raggio = DR: DeltaAng = 2 * PG / NumeroArchi
    pX1 = R174 + Raggio: pY1 = R170
    For j = 1 To NumeroArchi
      Ang = j * DeltaAng
      pX2 = R174 + Raggio * Cos(Ang): pY2 = R170 + Raggio * Sin(Ang)
      'Adattamento nella finestra grafica
      pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
      pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
      pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
      pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
      'Traccio la linea
      If (pY1 <= OGGETTO.ScaleHeight) And (pX1 <= OGGETTO.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
        OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
      End If
      'Aggiorno Px1 e Py1
      pX1 = R174 + Raggio * Cos(Ang): pY1 = R170 + Raggio * Sin(Ang)
    Next j
  Next i
  'FIANCO 2
  ColoreCurva = 12
  For i = 1 To NpF(2)
    'LINEA TANGENTE
    pX1 = (XX(2, i) - DR * Sin(TT(2, i) * PG / 180) - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(YY(2, i) + DR * Cos(TT(2, i) * PG / 180) - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = (XX(2, i) + DR * Sin(TT(2, i) * PG / 180) - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(YY(2, i) - DR * Cos(TT(2, i) * PG / 180) - ZeroPezzoY) * ScalaY + PicH / 2
    OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
    'LINEA NORMALE
    pX1 = (XX(2, i) - DR * Cos(TT(2, i) * PG / 180) - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(YY(2, i) - DR * Sin(TT(2, i) * PG / 180) - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = (XX(2, i) + DR * Cos(TT(2, i) * PG / 180) - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(YY(2, i) + DR * Sin(TT(2, i) * PG / 180) - ZeroPezzoY) * ScalaY + PicH / 2
    OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
    'indice del punto
    stmp = " " & Format$(NpF(1) - 1 + i, "##0") & " "
    Select Case Senso
      Case 1
        pY2 = pY2 - OGGETTO.TextHeight(stmp)
        If (pY2 <= OGGETTO.ScaleHeight) And (pX2 <= OGGETTO.ScaleWidth) And (pX2 >= 0) And (pY2 >= 0) Then
          OGGETTO.CurrentX = pX2
          OGGETTO.CurrentY = pY2
          OGGETTO.Print stmp
        End If
      Case -1
        pX1 = pX1 - OGGETTO.TextWidth(stmp)
        If (pY1 <= OGGETTO.ScaleHeight) And (pX1 <= OGGETTO.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          OGGETTO.CurrentX = pX1
          OGGETTO.CurrentY = pY1
          OGGETTO.Print stmp
        End If
    End Select
    'cerchio
    R174 = XX(2, i): R170 = YY(2, i): Raggio = DR: DeltaAng = 2 * PG / NumeroArchi
    pX1 = R174 + Raggio: pY1 = R170
    For j = 1 To NumeroArchi
      Ang = j * DeltaAng
      pX2 = R174 + Raggio * Cos(Ang): pY2 = R170 + Raggio * Sin(Ang)
      'Adattamento nella finestra grafica
      pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2: pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
      pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2: pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
      'Traccio la linea
      If (pY1 <= OGGETTO.ScaleHeight) And (pX1 <= OGGETTO.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
        OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
      End If
      'Aggiorno Px1 e Py1
      pX1 = R174 + Raggio * Cos(Ang): pY1 = R170 + Raggio * Sin(Ang)
    Next j
  Next i
Return

VISUALIZZA_INFORMAZIONI_PROFILO:
  sL(1, 1) = " DR":     sL(1, 2) = "= " & frmt(DR, 3) & "mm /" & Format$(iDR, "#####0")
  sL(2, 1) = " LF1":    sL(2, 2) = "= " & frmt(XX(1, NpF(1)) - XX(1, 1), 3) & "mm"
  sL(3, 1) = " LF2":    sL(3, 2) = "= " & frmt(XX(2, NpF(2)) - XX(2, 1), 3) & "mm"
  sL(4, 1) = " F1-0x":  sL(4, 2) = "= " & frmt(XX(1, 1), 3) & "mm"
  sL(5, 1) = " F2-0x":  sL(5, 2) = "= " & frmt(XX(2, NpF(2)), 3) & "mm"
  TabX = OGGETTO.TextWidth(sL(1, 1))
  For i = 1 To 10
    If (TabX < OGGETTO.TextWidth(sL(i, 1))) Then TabX = OGGETTO.TextWidth(sL(i, 1))
  Next i
  TabXMax = OGGETTO.TextWidth(sL(1, 2))
  For i = 1 To 10
    If (TabXMax < OGGETTO.TextWidth(sL(i, 2))) Then TabXMax = OGGETTO.TextWidth(sL(i, 2))
  Next i
  TabXMax = TabX + TabXMax
  OGGETTO.CurrentX = pX1 + Senso * TabXMax
  OGGETTO.CurrentY = pY1
  OGGETTO.Print stmp
  TabX = OGGETTO.TextWidth(sL(1, 1))
  TabY = OGGETTO.TextHeight(sL(1, 1))
  For i = 1 To 10
    If (TabX < OGGETTO.TextWidth(sL(i, 1))) Then TabX = OGGETTO.TextWidth(sL(i, 1))
    OGGETTO.CurrentX = 3 * PicW / 4 + pX1 + Senso * TabXMax
    OGGETTO.CurrentY = pY1 + i * TabY
    OGGETTO.Print sL(i, 1)
  Next i
  For i = 1 To 10
    OGGETTO.CurrentX = 3 * PicW / 4 + pX1 + Senso * TabXMax + TabX
    OGGETTO.CurrentY = pY1 + i * TabY
    OGGETTO.Print sL(i, 2)
  Next i
Return

CREAZIONE_FILE_PROFILO_MOLA_DXF:
  Const CIFRE_DXF = 20
  Open g_chOemPATH & "\MOLA.DXF" For Output As #3
    'INIZIO FILE
    Print #3, "0"
    Print #3, "SECTION"
    Print #3, "2"
    Print #3, "ENTITIES"
    i = 1
      'TRATTO INIZIALE
      Print #3, "0"
      Print #3, "LINE"
      Print #3, "8"
      Print #3, "1"
      Print #3, "62"
      Print #3, "1"
      'PRIMO PUNTO
      Print #3, "10"
      Print #3, frmt(XX(1, i), CIFRE_DXF)
      Print #3, "20"
      Print #3, frmt(YY(1, i), CIFRE_DXF)
      Print #3, "30"
      Print #3, "0.0"
      'SECONDO PUNTO
      i = i + 1
      Print #3, "11"
      Print #3, frmt(XX(1, i), CIFRE_DXF)
      Print #3, "21"
      Print #3, frmt(YY(1, i), CIFRE_DXF)
      Print #3, "31"
      Print #3, "0.0"
    'ARCHI DI CERCHIO DEL FIANCO 1
    For i = 2 To NpF(1) - 2 Step 2
      'COORD. X        COORD. Y
      R164 = XX(1, i + 0): R165 = YY(1, i + 0)
      R166 = XX(1, i + 1): R167 = YY(1, i + 1)
      R168 = XX(1, i + 2): R169 = YY(1, i + 2)
      GoSub SCRIVI_RIGA_DXF
    Next
    'ARCHI DI CERCHIO DEL FIANCO 2
    For i = 1 To NpF(2) - 2 Step 2
      'COORD. X        COORD. Y
      R164 = XX(2, i + 0): R165 = YY(2, i + 0)
      R166 = XX(2, i + 1): R167 = YY(2, i + 1)
      R168 = XX(2, i + 2): R169 = YY(2, i + 2)
      GoSub SCRIVI_RIGA_DXF
    Next
    i = NpF(2) - 1
      'TRATTO INIZIALE
      Print #3, "0"
      Print #3, "LINE"
      Print #3, "8"
      Print #3, "1"
      Print #3, "62"
      Print #3, "1"
      'PENULTIMO PUNTO
      Print #3, "10"
      Print #3, frmt(XX(2, i), CIFRE_DXF)
      Print #3, "20"
      Print #3, frmt(YY(2, i), CIFRE_DXF)
      Print #3, "30"
      Print #3, "0.0"
      'ULTIMO PUNTO
      i = i + 1
      Print #3, "11"
      Print #3, frmt(XX(2, i), CIFRE_DXF)
      Print #3, "21"
      Print #3, frmt(YY(2, i), CIFRE_DXF)
      Print #3, "31"
      Print #3, "0.0"
    'FINE FILE
    Print #3, "0"
    Print #3, "ENDSEC"
    Print #3, "0"
    Print #3, "EOF"
  Close #3
Return

SCRIVI_RIGA_DXF:
  'DISTANZA PER TRE PUNTI
  R162 = Abs((R165 - R169) * (R164 - R166) + (R167 - R165) * (R164 - R168)) / Sqr((R165 - R169) * (R165 - R169) + (R164 - R168) * (R164 - R168))
  If (R162 < 0.0005) Then
    'TRATTO RETTILINEO
    'G1 Y=R169 X=R168
    'TRATTO INTERMEDIO
    Print #3, "0"
    Print #3, "LINE"
    Print #3, "8"
    Print #3, "1"
    Print #3, "62"
    Print #3, "1"
    'PRIMO PUNTO
    Print #3, "10"
    Print #3, frmt(R164, CIFRE_DXF)
    Print #3, "20"
    Print #3, frmt(R165, CIFRE_DXF)
    Print #3, "30"
    Print #3, "0.0"
    'SECONDO PUNTO
    Print #3, "11"
    Print #3, frmt(R166, CIFRE_DXF)
    Print #3, "21"
    Print #3, frmt(R167, CIFRE_DXF)
    Print #3, "31"
    Print #3, "0.0"
    'TRATTO INTERMEDIO
    Print #3, "0"
    Print #3, "LINE"
    Print #3, "8"
    Print #3, "1"
    Print #3, "62"
    Print #3, "1"
    'SECONDO PUNTO
    Print #3, "10"
    Print #3, frmt(R166, CIFRE_DXF)
    Print #3, "20"
    Print #3, frmt(R167, CIFRE_DXF)
    Print #3, "30"
    Print #3, "0.0"
    'TERZO PUNTO
    Print #3, "11"
    Print #3, frmt(R168, CIFRE_DXF)
    Print #3, "21"
    Print #3, frmt(R169, CIFRE_DXF)
    Print #3, "31"
    Print #3, "0.0"
  Else
    'TRATTO CURVILINEO
    If R169 = R167 Then R169 = R167 + 0.0001
    If R165 = R167 Then R165 = R167 + 0.0001
    R160 = (R164 - R166) / (R167 - R165)
    R171 = (R166 - R168) / (R169 - R167)
    R172 = (R165 + R167) / 2 - R160 * (R164 + R166) / 2
    R173 = (R169 + R167) / 2 - R171 * (R168 + R166) / 2
    If R160 = R171 Then R160 = R171 + 0.0001
    'coordinate del centro
    R174 = (R173 - R172) / (R160 - R171)
    R170 = R160 * R174 + R172
    'raggio del cerchio
    R161 = Sqr((R174 - R166) * (R174 - R166) + (R170 - R167) * (R170 - R167))
    'prodotto misto per senso di rotazione
    R160 = (R166 - R164) * (R169 - R165) - (R168 - R164) * (R167 - R165)
    'Calcolo angoli compresi tra 0 e 2 * Pigreco
    AngStart = CALCOLO_ANGOLO(R164 - R174, R165 - R170)
    AngEnd = CALCOLO_ANGOLO(R168 - R174, R169 - R170)
    If (R160 > 0) Then
      'G3 Y=R169 X=R168 CR=R161
      Print #3, "0"
      Print #3, "ARC"
      Print #3, "8"
      Print #3, "1"
      Print #3, "62"
      Print #3, "1"
      Print #3, "10"
      Print #3, frmt(R174, CIFRE_DXF) & " (x centro)"
      Print #3, "20"
      Print #3, frmt(R170, CIFRE_DXF) & " (y centro)"
      Print #3, "30"
      Print #3, "0.0"
      Print #3, "40"
      Print #3, frmt(R161, CIFRE_DXF) & " (raggio)"
      Print #3, "50"
      Print #3, frmt(AngStart * 180 / PG, CIFRE_DXF) & " (angolo iniziale)"
      Print #3, "51"
      Print #3, frmt(AngEnd * 180 / PG, CIFRE_DXF) & " (angolo finale)"
    Else
      'G2 Y=R169 X=R168 CR=R161
      Print #3, "0"
      Print #3, "ARC"
      Print #3, "8"
      Print #3, "1"
      Print #3, "62"
      Print #3, "1"
      Print #3, "10"
      Print #3, frmt(R174, CIFRE_DXF) & " (x centro)"
      Print #3, "20"
      Print #3, frmt(R170, CIFRE_DXF) & " (y centro)"
      Print #3, "30"
      Print #3, "0.0"
      Print #3, "40"
      Print #3, frmt(R161, CIFRE_DXF) & " (raggio)"
      Print #3, "50"
      Print #3, frmt(AngEnd * 180 / PG, CIFRE_DXF) & " (angolo iniziale)"
      Print #3, "51"
      Print #3, frmt(AngStart * 180 / PG, CIFRE_DXF) & " (angolo finale)"
    End If
  End If
Return

errVISUALIZZA_MOLA_RAB:
  If FreeFile > 0 Then Close
  StopRegieEvents
  MsgBox "ERROR: " & Error(Err), 48, "SUB: VISUALIZZA_MOLA_RAB"
  ResumeRegieEvents
  'Exit Sub
  Resume Next
  
End Sub

Sub VISUALIZZA_MOLA_ROTORI(ByRef USCITA As Object, ByVal Scala As Single, ByVal SiPUNTI As Integer, ByVal SiASSI As Integer, ByVal IndiceZeroPezzoX As Integer, ByVal IndiceZeroPezzoY As Integer)
'
'input (impliciti):
'letti da file INI:  DF, AF, NumeroRette, NumeroArchi, TipoCarattere,
'-----------------:  PicW, PicH, MODALITA, Codice
'variabili globali:  NPF1             (ParProfiloRot)
'                    XX(), YY(), TT() (ParMolaRot) (coord.mola con correzioni)
'letti da DB:        Np, [XX(), YY(), TT()] - non pi� -, RagUtF1, RagUtF2
'letti da listbox:   CorX, CorY, CorS, R37 (larg.mola)
'
Dim SiStampa As String
Dim NomeForm As Form
'
Dim Atmp  As Double
Dim RTMP4 As Double
Dim RTMP5 As Double
Dim RTMP6 As Double
'
Dim Rmin           As Single
Dim iMIN           As Integer
Dim RaggioINTmin   As Single
Dim iINTmin        As Single
'
Dim XX() As Double
Dim YY() As Double
Dim TT() As Double
'
Dim Xt() As Single
Dim Yt() As Single
Dim Ux() As Single
Dim Uy() As Single
'
Dim TIPO_LAVORAZIONE As String
'
Dim MODALITA As String
Dim Codice   As String
Dim Senso    As Integer
'
Dim stmp As String
Dim ftmp As Single
'
Dim i    As Integer
Dim j    As Integer
Dim k    As Integer
'
Dim jj        As Integer
Dim sERRORE   As String
'
Dim Np   As Integer
Dim NPF1 As Integer
'
Dim AngStart  As Single
Dim AngEnd    As Single
Dim Ang       As Single
Dim DeltaAng  As Single
Dim DeltaArco As Single
'
Dim iDY         As Integer
Dim Dy          As Single
'
Dim Idx         As Integer
Dim dx          As Single
'
Dim iDR         As Integer
Dim DR          As Single
'
Dim DF          As Single
Dim AF          As Single
Dim NumeroRette As Integer
Dim NumeroArchi As Integer
Dim ColoreCurva As Integer
'
Dim pX1 As Single
Dim pY1 As Single
Dim pX2 As Single
Dim pY2 As Single
'
Dim Raggio As Single
'
Dim NPM            As Integer
Dim NpF1M          As Integer
Dim RaggioMassimo  As Single
Dim XX_Separazione As Single
'
Dim R37  As Double
'
Dim R160 As Single
Dim R161 As Single
Dim R162 As Single
Dim R163 As Single
Dim R164 As Single
Dim R165 As Single
Dim R166 As Single
Dim R167 As Single
Dim R168 As Single
Dim R169 As Single
Dim R170 As Single
Dim R171 As Single
Dim R172 As Single
Dim R173 As Single
Dim R174 As Single
'
Dim ZeroPezzoX As Single
Dim ZeroPezzoY As Single
'
Dim Xmax  As Single
Dim Ymax  As Single
Dim Xmin  As Single
Dim Ymin  As Single
Dim tMIN  As Single
Dim iTmin As Integer
'
Dim ScalaX As Single
Dim ScalaY As Single
'
Dim TipoCarattere As String
'
Dim TabXMax As Single
Dim TabX    As Single
Dim TabY    As Single
'
Dim sL(12, 2) As String
'
Dim CorS    As Single  '---- correzione spessore
Dim CorX    As Single  '---- correzione profilo lungo X
Dim corY    As Single  '---- correzione profilo lungo Y
Dim COXX    As Single
Dim COYY    As Single

Dim PicW    As Single
Dim PicH    As Single
'
Dim msgERRORE As String
Dim sErrF1    As String
Dim sErrF2    As String
'
Dim RagUtF1        As Double
Dim RagUtF2        As Double
Dim INTERPOLAZIONE As Integer
Dim Rett_Viti      As Integer
Dim ret            As Long
'
On Error GoTo errVISUALIZZA_MOLA_ROTORI
  '
  If (USCITA Is Printer) Then
    SiStampa = "Y"
  Else
    SiStampa = "N"
    Set NomeForm = USCITA.Parent
    NomeForm.OEM1PicCALCOLO.BackColor = &HFFFFFF
  End If
  Np = ParProfiloRot.NpTot
  NPF1 = ParProfiloRot.NPF1
  DF = val(GetInfo("DIS0", "DF", Path_LAVORAZIONE_INI))
  AF = val(GetInfo("DIS0", "AF", Path_LAVORAZIONE_INI))
  NumeroRette = val(GetInfo("DIS0", "NumeroRette", Path_LAVORAZIONE_INI))
  NumeroArchi = val(GetInfo("DIS0", "NumeroArchi", Path_LAVORAZIONE_INI))
  '
  If DF = 0 Then DF = 2
  If AF = 0 Then AF = 30
  If NumeroRette = 0 Then NumeroRette = 20
  If NumeroArchi = 0 Then NumeroArchi = 50
  '
  TipoCarattere = GetInfo("DIS0", "TextFont", Path_LAVORAZIONE_INI)
  If (LINGUA = "CH") Then TipoCarattere = "MS Song"
  If (LINGUA = "RU") Then TipoCarattere = "Arial Cyr"
  If (Len(TipoCarattere) <= 0) Then TipoCarattere = "Arial"
  '
  ScalaX = Scala: ScalaY = Scala
  'Acquisizione area grafica
  stmp = GetInfo("DIS0", "PicW", Path_LAVORAZIONE_INI):  If Len(stmp) > 0 Then PicW = val(stmp) Else PicW = 190
  stmp = GetInfo("DIS0", "PicH", Path_LAVORAZIONE_INI):  If Len(stmp) > 0 Then PicH = val(stmp) Else PicH = 95
  TIPO_LAVORAZIONE = "ROTORI_ESTERNI"
  'Lettura del codice ( GRAFICO )
  MODALITA = GetInfo("Configurazione", "MODALITA", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))
  If (MODALITA <> "OFF-LINE") Then
    stmp = LEGGI_NomePezzo(TIPO_LAVORAZIONE)
  Else
    stmp = GetInfo(TIPO_LAVORAZIONE, "NomePezzoInserimento", Path_LAVORAZIONE_INI)
  End If
  Codice = stmp
  'Preparazione grafico
  If (USCITA Is Printer) Then
    USCITA.ScaleMode = 6
    For i = 1 To 2
      USCITA.FontName = TipoCarattere
      USCITA.FontSize = 10
      USCITA.FontBold = False
    Next i
    WRITE_DIALOG OEMX.OEM1frameCalcolo.Caption & " Printing on " & USCITA.DeviceName
    If PicW = 190 And PicH = 95 Then
      USCITA.Orientation = 1
    Else
      USCITA.Orientation = 2
    End If
  Else
    USCITA.Cls
    WRITE_DIALOG ""
    USCITA.ScaleWidth = PicW
    USCITA.ScaleHeight = PicH
    For i = 1 To 2
      USCITA.FontName = TipoCarattere
      USCITA.FontSize = 8
      USCITA.FontBold = False
    Next i
    USCITA.Refresh
    NomeForm.OEM1frameCalcolo.Caption = "  " & Codice & "  (Wheel Profile)  "
  End If
  'Lettura Raggio Utensile F1 e F2 da DB
  R37 = LEP("R[37]")
  RagUtF1 = LEP("$TC_DP6[1,1]")
  RagUtF2 = LEP("$TC_DP6[1,2]")
  INTERPOLAZIONE = LEP("TIPOMOLA_G")
  Rett_Viti = LEP("TIPO_ROTORE")
  ReDim XX(Np + 1), YY(Np + 1), TT(Np + 1)
  'Acquisizione profilo mola
  tMIN = 1000000000#: iTmin = 0
  For k = 0 To Np + 1
    XX(k) = ParMolaROT.XX(k)
    YY(k) = ParMolaROT.YY(k)
    TT(k) = ParMolaROT.TT(k)
    If tMIN > Abs(TT(k)) Then tMIN = Abs(TT(k)): iTmin = k
  Next k
  If (INTERPOLAZIONE <> 2) Then
    Dim DDECX       As String
    Dim DDECY       As String
    Dim CORREZIONE  As Double
    DDECX = GetInfo("Configurazione", "DDECX", Path_LAVORAZIONE_INI)
    DDECX = UCase$(Trim$(DDECX))
    DDECY = GetInfo("Configurazione", "DDECY", Path_LAVORAZIONE_INI)
    DDECY = UCase$(Trim$(DDECY))
    If (Len(DDECX) > 0) And (Len(DDECY) > 0) And (MODALITA <> "OFF-LINE") Then
      If (Dir$(g_chOemPATH & "\SEIM.INI") <> "") Then Kill g_chOemPATH & "\SEIM.INI"
      'Correzioni profilo mola
      CORREZIONE = 0
      For i = 0 To Np + 1
        CorX = LEP("CORX[0," & Format$(i, "##0") & "]")
        corY = LEP("CORY[0," & Format$(i, "##0") & "]")
        COXX = val(OPC_LEGGI_DATO(DDECX & "[" & Format$(i + 1, "##0") & "]"))
        If (COXX <> 0) Then CORREZIONE = COXX
        ret = WritePrivateProfileString("CORREZIONI", "COXX[0," & Format$(i, "##0") & "]", frmt(COXX, 4), g_chOemPATH & "\SEIM.INI")
        COYY = val(OPC_LEGGI_DATO(DDECY & "[" & Format$(i + 1, "##0") & "]"))
        If (COYY <> 0) Then CORREZIONE = COYY
        ret = WritePrivateProfileString("CORREZIONI", "COYY[0," & Format$(i, "##0") & "]", frmt(COYY, 4), g_chOemPATH & "\SEIM.INI")
        If (i <= NPF1) Then
          'Correzioni sul fianco 1
          XX(i) = XX(i) + CorX + COXX
          YY(i) = YY(i) - corY - COYY
        Else
          'Correzioni sul fianco 2
          XX(i) = XX(i) - CorX - COXX
          YY(i) = YY(i) - corY - COYY
        End If
      Next i
      'SE TUTTE LE CORREZIONI SONO NULLE CANCELLO IL FILE
      'PER EVITARE DI CARICARE CORREZIONI NULLE E PERDERE TEMPO
      If (CORREZIONE = 0) Then Kill g_chOemPATH & "\SEIM.INI"
    Else
      For i = 0 To Np + 1
        CorX = LEP("CORX[0," & Format$(i, "##0") & "]")
        corY = LEP("CORY[0," & Format$(i, "##0") & "]")
        If (i <= NPF1) Then
          'Correzioni sul fianco 1
          XX(i) = XX(i) + CorX
          YY(i) = YY(i) - corY
        Else
          'Correzioni sul fianco 2
          XX(i) = XX(i) - CorX
          YY(i) = YY(i) - corY
        End If
      Next i
    End If
  End If
  'CorrezioneSpessore
  CorS = LEP("R[34]")
  'Coordinata XX di separazione tra il primo e secondo fianco
  XX_Separazione = XX(NPF1)
  stmp = " ANGLE MIN= " & frmt(FnRG(tMIN), 4) & " / " & Format$(iTmin) & " "
  USCITA.CurrentX = PicW - USCITA.TextWidth(stmp)
  USCITA.CurrentY = PicH / 2 - USCITA.TextHeight(stmp)
  USCITA.Print stmp
  stmp = Codice & " "
  USCITA.CurrentX = PicW - USCITA.TextWidth(stmp)
  USCITA.CurrentY = PicH / 2
  USCITA.Print stmp
  If (SiASSI = 1) Then Call SCRIVI_ASSI(USCITA, SiStampa)
  GoSub RICERCA_MAX_MIN
  If (IndiceZeroPezzoX >= 1) And (IndiceZeroPezzoX <= Np) And (IndiceZeroPezzoY >= 1) And (IndiceZeroPezzoY <= Np) Then
    'Profilo AZZERATO SU DI UN PUNTO DEL PROFILO STESSO
    ZeroPezzoX = XX(IndiceZeroPezzoX): ZeroPezzoY = YY(IndiceZeroPezzoY)
  Else
    'Profilo centrato
    ZeroPezzoX = (Xmax + Xmin) / 2: ZeroPezzoY = (Ymax + Ymin) / 2
  End If
  'Calcolo scala se non � impostata
  If Scala = 0 Then
    ScalaX = PicW / (Xmax - Xmin): ScalaY = PicH / (Ymax - Ymin)
    If ScalaY < ScalaX Then Scala = ScalaY Else Scala = ScalaX
    i = WritePrivateProfileString("DIS0", "ScalaMOLA", frmt(Scala, 4), Path_LAVORAZIONE_INI)
    ScalaX = Scala: ScalaY = Scala
  End If
  'Calcolo DR, DY e DX
  DR = 999999:  iDR = 1
  Dy = 0:  iDY = 1
  dx = 0:  Idx = 1
  For i = 1 To Np - 1
    ftmp = Sqr((XX(i + 1) - XX(i)) ^ 2 + (YY(i + 1) - YY(i)) ^ 2)
    If (ftmp < DR) Then DR = ftmp: iDR = i
    ftmp = Abs(YY(i + 1) - YY(i))
    If (ftmp > Dy) Then Dy = ftmp: iDY = i
    ftmp = Abs(XX(i + 1) - XX(i))
    If (ftmp > dx) Then dx = ftmp: Idx = i
  Next i
  If (DR = 0) Then
    stmp = "Duplicate point into WHEEL profile."
    StopRegieEvents
    MsgBox stmp, vbCritical, "WARNING: point " & Format$(iDR, "####0")
    ResumeRegieEvents
  End If
  If (SiPUNTI = 1) Then GoSub CONTROLLO_SEQUENZA_PUNTI
  'Nomi dei fianchi
  ftmp = Xmin: stmp = " F1 ": Call VISUALIZZA_NOME(SiStampa, ftmp, stmp, Ymin, ZeroPezzoX, ZeroPezzoY, ScalaX, ScalaY, PicW, PicH, USCITA)
  ftmp = Xmax: stmp = " F2 ": Call VISUALIZZA_NOME(SiStampa, ftmp, stmp, Ymin, ZeroPezzoX, ZeroPezzoY, ScalaX, ScalaY, PicW, PicH, USCITA)
  'Scale
  'Call SCRIVI_SCALA_VERTICALE(ScalaY, USCITA, SiStampa)
  Call SCRIVI_SCALA_ORIZZONTALE(ScalaX, USCITA, SiStampa)
  'Correzione di spessore x TEORICO
  GoSub CORREZIONE_SPESSORE
  ColoreCurva = 9
  GoSub RICERCA_MAX_MIN
  GoSub SCARICO_FIANCHI
  Senso = 1
  GoSub VISUALIZZA_XX_YY
  'VISUALIZZAZIONE DELLA CORREZIONE D'INTERASSE
  If (Rett_Viti = 1) Or (Rett_Viti = 2) Then
    'ricerca del secondo massimo da sinistra
    Dim iMax2 As Integer
    Dim YMax2 As Single
    Dim YMin2 As Single
    YMax2 = Ymax
    For i = Np To NPF1 - 1 Step -1
      If Abs(YY(i) - YMax2) <= 0.001 Then iMax2 = i: Exit For
    Next i
    'RICERCA DEL MINIMO TRA I MASSIMI
    YMin2 = 1E+38
    For i = NPF1 To iMax2
      If (YY(i) <= YMin2) Then YMin2 = YY(i)
    Next i
    stmp = LNP("R[189]")
    stmp = " " & stmp & "= "
    stmp = stmp & frmt(-(Ymax - YMin2), 3) & "mm"
    USCITA.CurrentX = 0
    USCITA.CurrentY = PicH - USCITA.TextHeight(stmp)
    USCITA.Print stmp
  End If
  Call VISUALIZZA_SEPARAZIONE(USCITA, NPF1, XX(NPF1), ZeroPezzoX, ZeroPezzoY, ScalaX, ScalaY, PicW, PicH, 0)
  If (CorS <> 0) Then
    pX1 = (XX(NPF1) - CorS / 2 - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(Ymax - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = (XX(NPF1) + CorS / 2 - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(Ymax - DF / 2 - ZeroPezzoY) * ScalaY + PicH / 2
    USCITA.Line (pX1, pY1)-(pX1, pY2), QBColor(4)
    USCITA.Line (pX2, pY1)-(pX2, pY2), QBColor(4)
    Call SCRIVI_LARGHEZZA(SiStampa, XX(NPF1) - CorS / 2, Ymax - DF / 2, XX(NPF1) + CorS / 2, Ymax - DF / 2, ZeroPezzoX, ZeroPezzoY, ScalaX, ScalaY, PicW, PicH, 4, USCITA)
  End If
  Call SCRIVI_LARGHEZZA(SiStampa, Xmin, Ymin, Xmax, Ymax, ZeroPezzoX, ZeroPezzoY, ScalaX, ScalaY, PicW, PicH, 0, USCITA)
  Call SCRIVI_ALTEZZA(SiStampa, -Abs(Xmax), Ymin, Abs(Xmax), Ymax, ZeroPezzoX, ZeroPezzoY, ScalaX, ScalaY, PicW, PicH, 0, USCITA)
  'Controllo se devo visualizzare i punti
  If (SiPUNTI = 1) Then
    GoSub PUNTI_PROFILO
    Call CREAZIONE_FILE_PROFILO_MOLA_DXF(Np, XX(), YY(), TT(), NPF1)
  End If
  'Il calcolo del raggio esterno � effettuato in RICERCA_MAX_MIN
  'INFORMAZIONI DA VISUALIZZARE
  pX1 = 0: pY1 = 0: Senso = 0
  GoSub VISUALIZZA_INFORMAZIONI_PROFILO
  'Segnalazione della fine delle operazioni
  If (USCITA Is Printer) Then
    USCITA.EndDoc
    WRITE_DIALOG "printed on " & USCITA.DeviceName
    'CREAZIONE DI PROFILO.TXT PER RETTIFICA PER PUNTI
    If (SiPUNTI = 1) Then
      Open g_chOemPATH & "\PROFILO.TXT" For Output As #1
        For i = UBound(XX) To 0 Step -1
        Print #1, Format$(UBound(XX) - i + 1, "###0") & ";" & frmt(XX(i), 4) & ";" & frmt(YY(i) - YY(0), 4) & ";"
        Next i
      Close #1
    End If
  Else
    'If Len(msgERRORE) > 0 Then
    '  Write_Dialog msgERRORE
    '  stmp = msgERRORE & Chr(13)
    '  If Len(sErrF1) > 0 Then stmp = stmp & Chr(13) & "ERROR ON F1" & sErrF1
    '  If Len(sErrF2) > 0 Then stmp = stmp & Chr(13) & "ERROR ON F2" & sErrF2
    '  StopRegieEvents
    '  MsgBox stmp, vbExclamation, "WARNING"
    '  ResumeRegieEvents
    'Else
    '  Write_Dialog ""
    'End If
  End If
  '
Exit Sub

CONTROLLO_SEQUENZA_PUNTI:
Dim I_MAX_F1  As Integer
Dim I_MAX_F2  As Integer
Dim Y_MAX_F1  As Double
Dim Y_MAX_F2  As Double
Dim MESSAGGIO(2) As String
Dim II_TEMP   As Integer
  '
  MESSAGGIO(0) = "": MESSAGGIO(1) = "": MESSAGGIO(2) = ""
  'ANALISI SEQUENZA FIANCO 1
  I_MAX_F1 = 1: Y_MAX_F1 = YY(1)
  For i = 1 To NPF1
    If (YY(i) > Y_MAX_F1) Then Y_MAX_F1 = YY(i): I_MAX_F1 = i
  Next i
  For i = 1 To I_MAX_F1 - 1
    If (YY(i) > YY(i + 1)) Then
    MESSAGGIO(1) = MESSAGGIO(1) & "YY(" & Format$(i, "##0") & ") > YY(" & Format$(i + 1, "##0") & ") -> " & frmt(YY(i) - YY(i + 1), 4) & Chr(13)
    End If
  Next i
  'ANALISI SEQUENZA FIANCO 2
  I_MAX_F2 = Np: Y_MAX_F2 = YY(Np)
  For i = Np To I_MAX_F1 + 1 Step -1
    If (YY(i) > Y_MAX_F2) Then Y_MAX_F2 = YY(i): I_MAX_F2 = i
  Next i
  II_TEMP = Int(I_MAX_F2 / 2) * 2
  If (II_TEMP = I_MAX_F2) Then Y_MAX_F2 = YY(II_TEMP + 1): I_MAX_F2 = II_TEMP + 1
  If (I_MAX_F2 <= NPF1) Then Y_MAX_F2 = YY(NPF1): I_MAX_F2 = NPF1
  For i = Np To I_MAX_F2 + 1 Step -1
    If (YY(i) > YY(i - 1)) Then
    MESSAGGIO(2) = MESSAGGIO(2) & "YY(" & Format$(i, "##0") & ") > YY(" & Format$(i - 1, "##0") & ") -> " & frmt(YY(i) - YY(i - 1), 4) & Chr(13)
    End If
  Next i
  'COMPOSIZIONE MESSAGGIO
  If (Len(MESSAGGIO(1)) > 0) Then MESSAGGIO(0) = MESSAGGIO(0) & "--- F1 ---" & Chr(13) & MESSAGGIO(1) & Chr(13)
  If (Len(MESSAGGIO(2)) > 0) Then MESSAGGIO(0) = MESSAGGIO(0) & "--- F2 ---" & Chr(13) & MESSAGGIO(2) & Chr(13)
  'VISUALIZZAZIONE MESSAGGIO
  If (Len(MESSAGGIO(0)) > 0) Then MsgBox MESSAGGIO(0), vbCritical, "WARNING: " & I_MAX_F1 & " " & I_MAX_F2 & " " & Np
  '
Return

SCARICO_FIANCHI:
Dim XX_F1(99) As Double
Dim YY_F1(99) As Double
Dim XX_F2(99) As Double
Dim YY_F2(99) As Double
Dim LL_F1     As Double
Dim LL_F2     As Double
Dim RR_F1     As Double
Dim RR_F2     As Double
Dim IUPP_F1   As Integer
Dim IFIN_F1   As Integer
Dim IUPP_F2   As Integer
Dim IFIN_F2   As Integer
  '
  'FIANCO 1
  IUPP_F1 = LEP("COR[0,410]")
    RR_F1 = LEP("COR[0,411]")
    LL_F1 = LEP("COR[0,412]")
  If (IUPP_F1 > 0) And (RR_F1 > 0) And (LL_F1 > 0) Then
    IFIN_F1 = LEP("COR[0,413]")
    If (IFIN_F1 <= IUPP_F1) Then IFIN_F1 = IUPP_F1 + 1
    'FINE PROLUNGAMENTO
    XX_F1(1) = XX(IUPP_F1) + LL_F1 * Sin(Abs(TT(IUPP_F1)))
    YY_F1(1) = YY(IUPP_F1) + LL_F1 * Cos(Abs(TT(IUPP_F1)))
    'TRATTO RETTILINEO
    pX1 = (XX(IUPP_F1) - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(YY(IUPP_F1) - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = (XX_F1(1) - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(YY_F1(1) - ZeroPezzoY) * ScalaY + PicH / 2
    USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
    'PUNTO INTERMEDIO DEL PROLUNGAMENTO
    XX_F1(11) = (XX(IUPP_F1) + XX_F1(1)) / 2
    YY_F1(11) = (YY(IUPP_F1) + YY_F1(1)) / 2
    'CENTRO DEL CERCHIO DI RACCORDO
    XX_F1(0) = XX_F1(1) - RR_F1 * Sin(PG / 2 - Abs(TT(IUPP_F1)))
    YY_F1(0) = YY_F1(1) + RR_F1 * Cos(PG / 2 - Abs(TT(IUPP_F1)))
    If (SiPUNTI = 1) Then
      'TRATTO RETTILINEO
      pX1 = (XX_F1(0) - ZeroPezzoX) * ScalaX + PicW / 2
      pY1 = -(YY_F1(0) - ZeroPezzoY) * ScalaY + PicH / 2
      pX2 = (XX_F1(1) - ZeroPezzoX) * ScalaX + PicW / 2
      pY2 = -(YY_F1(1) - ZeroPezzoY) * ScalaY + PicH / 2
      USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
      'CERCHIO DEL RACCORDO
      R174 = XX_F1(0): R170 = YY_F1(0): Raggio = RR_F1: DeltaAng = 2 * PG / NumeroArchi
      pX1 = R174 + Raggio: pY1 = R170
      For j = 1 To NumeroArchi
        Ang = j * DeltaAng
        pX2 = R174 + Raggio * Cos(Ang): pY2 = R170 + Raggio * Sin(Ang)
        'Adattamento nella finestra grafica
        pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
        pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
        pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
        pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
        'Traccio la linea
        USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
        'Aggiorno Px1 e Py1
        pX1 = R174 + Raggio * Cos(Ang): pY1 = R170 + Raggio * Sin(Ang)
      Next j
    End If
    'PUNTO DI TANGENZA
    XX_F1(2) = XX_F1(0) + RR_F1 * Cos(Abs(TT(IFIN_F1)))
    YY_F1(2) = YY_F1(0) - RR_F1 * Sin(Abs(TT(IFIN_F1)))
    'TRATTO RETTILINEO
    pX1 = (XX(IFIN_F1) - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(YY(IFIN_F1) - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = (XX_F1(2) - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(YY_F1(2) - ZeroPezzoY) * ScalaY + PicH / 2
    USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
    If (SiPUNTI = 1) Then
      'TRATTO RETTILINEO
      pX1 = (XX_F1(0) - ZeroPezzoX) * ScalaX + PicW / 2
      pY1 = -(YY_F1(0) - ZeroPezzoY) * ScalaY + PicH / 2
      pX2 = (XX_F1(2) - ZeroPezzoX) * ScalaX + PicW / 2
      pY2 = -(YY_F1(2) - ZeroPezzoY) * ScalaY + PicH / 2
      USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
    End If
    'PUNTO INTERMEDIO DEL SECONDO PROLUNGAMENTO
    XX_F1(22) = (XX(IFIN_F1) + XX_F1(2)) / 2
    YY_F1(22) = (YY(IFIN_F1) + YY_F1(2)) / 2
    'PUNTO INTERMEDIO SULL'ARCO DI RACCORDO
    XX_F1(33) = XX_F1(0) + RR_F1 * Cos(Abs(TT(IUPP_F1)) / 2 + Abs(TT(IFIN_F1)) / 2)
    YY_F1(33) = YY_F1(0) - RR_F1 * Sin(Abs(TT(IUPP_F1)) / 2 + Abs(TT(IFIN_F1)) / 2)
    If (SiPUNTI = 1) Then
      'TRATTO RETTILINEO
      pX1 = (XX_F1(0) - ZeroPezzoX) * ScalaX + PicW / 2
      pY1 = -(YY_F1(0) - ZeroPezzoY) * ScalaY + PicH / 2
      pX2 = (XX_F1(33) - ZeroPezzoX) * ScalaX + PicW / 2
      pY2 = -(YY_F1(33) - ZeroPezzoY) * ScalaY + PicH / 2
      USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
    End If
    'TRATTO RETTILINEO
    If (IFIN_F1 = IUPP_F1 + 1) Then
      pX1 = (XX(IFIN_F1) - ZeroPezzoX) * ScalaX + PicW / 2
      pY1 = -(YY(IFIN_F1) - ZeroPezzoY) * ScalaY + PicH / 2
      pX2 = (XX(IFIN_F1 + 1) - ZeroPezzoX) * ScalaX + PicW / 2
      pY2 = -(YY(IFIN_F1 + 1) - ZeroPezzoY) * ScalaY + PicH / 2
      USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
    End If
    'ARCO DI RACCORDO
    'COORD. X        COORD. Y
    R164 = XX_F1(1):  R165 = YY_F1(1)
    R166 = XX_F1(33): R167 = YY_F1(33)
    R168 = XX_F1(2):  R169 = YY_F1(2)
    GoSub DISEGNA_RACCORDO
  Else
    IUPP_F1 = 0
  End If
  '
  'FIANCO 2
  IUPP_F2 = LEP("COR[0,420]")
    RR_F2 = LEP("COR[0,421]")
    LL_F2 = LEP("COR[0,422]")
  If (IUPP_F2 > 0) And (RR_F2 > 0) And (LL_F2 > 0) Then
    IFIN_F2 = LEP("COR[0,423]")
    If (IFIN_F2 >= IUPP_F2) Or ((IFIN_F2 <= 0)) Then IFIN_F2 = IUPP_F2 - 1
    'FINE PROLUNGAMENTO
    XX_F2(1) = XX(IUPP_F2) - LL_F2 * Sin(Abs(TT(IUPP_F2)))
    YY_F2(1) = YY(IUPP_F2) + LL_F2 * Cos(Abs(TT(IUPP_F2)))
    'TRATTO RETTILINEO
    pX1 = (XX(IUPP_F2) - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(YY(IUPP_F2) - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = (XX_F2(1) - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(YY_F2(1) - ZeroPezzoY) * ScalaY + PicH / 2
    USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
    'PUNTO INTERMEDIO DEL PROLUNGAMENTO
    XX_F2(11) = (XX(IUPP_F2) + XX_F2(1)) / 2
    YY_F2(11) = (YY(IUPP_F2) + YY_F2(1)) / 2
    'CENTRO DEL CERCHIO DI RACCORDO
    XX_F2(0) = XX_F2(1) + RR_F2 * Sin(PG / 2 - Abs(TT(IUPP_F2)))
    YY_F2(0) = YY_F2(1) + RR_F2 * Cos(PG / 2 - Abs(TT(IUPP_F2)))
    If (SiPUNTI = 1) Then
      'TRATTO RETTILINEO
      pX1 = (XX_F2(0) - ZeroPezzoX) * ScalaX + PicW / 2
      pY1 = -(YY_F2(0) - ZeroPezzoY) * ScalaY + PicH / 2
      pX2 = (XX_F2(1) - ZeroPezzoX) * ScalaX + PicW / 2
      pY2 = -(YY_F2(1) - ZeroPezzoY) * ScalaY + PicH / 2
      USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
      'CERCHIO DEL RACCORDO
      R174 = XX_F2(0): R170 = YY_F2(0): Raggio = RR_F2: DeltaAng = 2 * PG / NumeroArchi
      pX1 = R174 + Raggio: pY1 = R170
      For j = 1 To NumeroArchi
        Ang = j * DeltaAng
        pX2 = R174 + Raggio * Cos(Ang): pY2 = R170 + Raggio * Sin(Ang)
        'Adattamento nella finestra grafica
        pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
        pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
        pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
        pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
        'Traccio la linea
        USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
        'Aggiorno Px1 e Py1
        pX1 = R174 + Raggio * Cos(Ang): pY1 = R170 + Raggio * Sin(Ang)
      Next j
    End If
    'PUNTO DI TANGENZA
    XX_F2(2) = XX_F2(0) - RR_F2 * Cos(Abs(TT(IFIN_F2)))
    YY_F2(2) = YY_F2(0) - RR_F2 * Sin(Abs(TT(IFIN_F2)))
    'TRATTO RETTILINEO
    pX1 = (XX(IFIN_F2) - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(YY(IFIN_F2) - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = (XX_F2(2) - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(YY_F2(2) - ZeroPezzoY) * ScalaY + PicH / 2
    USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
    If (SiPUNTI = 1) Then
      'TRATTO RETTILINEO
      pX1 = (XX_F2(0) - ZeroPezzoX) * ScalaX + PicW / 2
      pY1 = -(YY_F2(0) - ZeroPezzoY) * ScalaY + PicH / 2
      pX2 = (XX_F2(2) - ZeroPezzoX) * ScalaX + PicW / 2
      pY2 = -(YY_F2(2) - ZeroPezzoY) * ScalaY + PicH / 2
      USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
    End If
    'PUNTO INTERMEDIO DEL SECONDO PROLUNGAMENTO
    XX_F2(22) = (XX(IFIN_F2) + XX_F2(2)) / 2
    YY_F2(22) = (YY(IFIN_F2) + YY_F2(2)) / 2
    'PUNTO INTERMEDIO SULL'ARCO DI RACCORDO
    XX_F2(33) = XX_F2(0) - RR_F2 * Cos(Abs(TT(IUPP_F2)) / 2 + Abs(TT(IFIN_F2)) / 2)
    YY_F2(33) = YY_F2(0) - RR_F2 * Sin(Abs(TT(IUPP_F2)) / 2 + Abs(TT(IFIN_F2)) / 2)
    If (SiPUNTI = 1) Then
      'TRATTO RETTILINEO
      pX1 = (XX_F2(0) - ZeroPezzoX) * ScalaX + PicW / 2
      pY1 = -(YY_F2(0) - ZeroPezzoY) * ScalaY + PicH / 2
      pX2 = (XX_F2(33) - ZeroPezzoX) * ScalaX + PicW / 2
      pY2 = -(YY_F2(33) - ZeroPezzoY) * ScalaY + PicH / 2
      USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
    End If
    'TRATTO RETTILINEO
    If (IFIN_F2 = IUPP_F2 - 1) Then
      pX1 = (XX(IFIN_F2) - ZeroPezzoX) * ScalaX + PicW / 2
      pY1 = -(YY(IFIN_F2) - ZeroPezzoY) * ScalaY + PicH / 2
      pX2 = (XX(IFIN_F2 - 1) - ZeroPezzoX) * ScalaX + PicW / 2
      pY2 = -(YY(IFIN_F2 - 1) - ZeroPezzoY) * ScalaY + PicH / 2
      USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
    End If
    'ARCO DI RACCORDO
    'COORD. X        COORD. Y
    R164 = XX_F2(1):  R165 = YY_F2(1)
    R166 = XX_F2(33): R167 = YY_F2(33)
    R168 = XX_F2(2):  R169 = YY_F2(2)
    GoSub DISEGNA_RACCORDO
  Else
    IUPP_F2 = 0
  End If
Return

DISEGNA_RACCORDO:
'DISTANZA PER TRE PUNTI
R162 = Abs((R165 - R169) * (R164 - R166) + (R167 - R165) * (R164 - R168)) / Sqr((R165 - R169) * (R165 - R169) + (R164 - R168) * (R164 - R168))
If (R162 < 0.0005) Then
  'TRATTO RETTILINEO
  'G1 Y=R169 X=R168
  pX1 = (R164 - ZeroPezzoX) * ScalaX + PicW / 2
  pY1 = -(R165 - ZeroPezzoY) * ScalaY + PicH / 2
  pX2 = (R168 - ZeroPezzoX) * ScalaX + PicW / 2
  pY2 = -(R169 - ZeroPezzoY) * ScalaY + PicH / 2
  If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
  USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
  End If
Else
  'TRATTO CURVILINEO
  If R169 = R167 Then R169 = R167 + 0.0001
  If R165 = R167 Then R165 = R167 + 0.0001
  R160 = (R164 - R166) / (R167 - R165)
  R171 = (R166 - R168) / (R169 - R167)
  R172 = (R165 + R167) / 2 - R160 * (R164 + R166) / 2
  R173 = (R169 + R167) / 2 - R171 * (R168 + R166) / 2
  If R160 = R171 Then R160 = R171 + 0.0001
  'coordinate del centro
  R174 = (R173 - R172) / (R160 - R171)
  R170 = R160 * R174 + R172
  'raggio del cerchio
  R161 = Sqr((R174 - R166) * (R174 - R166) + (R170 - R167) * (R170 - R167))
  'AGGIORNAMENTO DEL VALORE DI RAGGIO MINIMO
  If Rmin > R161 Then Rmin = R161: iMIN = i
  'prodotto misto per senso di rotazione
  R160 = (R166 - R164) * (R169 - R165) - (R168 - R164) * (R167 - R165)
  'Calcolo angoli compresi tra 0 e 2 * Pigreco
  AngStart = CALCOLO_ANGOLO(R164 - R174, R165 - R170)
  AngEnd = CALCOLO_ANGOLO(R168 - R174, R169 - R170)
  If (R160 > 0) Then
    'G3 Y=R169 X=R168 CR=R161
    If AngEnd >= AngStart Then
      Ang = AngEnd - AngStart
    Else
      Ang = 2 * PG - (AngStart - AngEnd)
    End If
    DeltaAng = Ang / NumeroArchi
    pX1 = R164: pY1 = R165
    For j = 1 To NumeroArchi - 1
      Ang = (AngStart + j * DeltaAng)
      pX2 = R174 + R161 * Cos(Ang): pY2 = R170 + R161 * Sin(Ang)
      'Adattamento nella finestra grafica
      pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
      pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
      pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
      pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
      'Traccio la linea
      If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
      USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
      End If
      'Aggiorno Px1 e Py1
      pX1 = R174 + R161 * Cos(Ang): pY1 = R170 + R161 * Sin(Ang)
    Next j
    pX2 = R168: pY2 = R169
    'Adattamento nella finestra grafica
    pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
    'Traccio la linea
    If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
    USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
    End If
  Else
    'G2 Y=R169 X=R168 CR=R161
    If AngStart >= AngEnd Then
      Ang = AngStart - AngEnd
    Else
      Ang = 2 * PG - (AngEnd - AngStart)
    End If
    DeltaAng = Ang / NumeroArchi
    pX1 = R164: pY1 = R165
    For j = 1 To NumeroArchi - 1
      Ang = (AngStart - j * DeltaAng)
      pX2 = R174 + R161 * Cos(Ang): pY2 = R170 + R161 * Sin(Ang)
      'Adattamento nella finestra grafica
      pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
      pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
      pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
      pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
      'Traccio la linea
      If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
      USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
      End If
      'Aggiorno Px1 e Py1
      pX1 = R174 + R161 * Cos(Ang): pY1 = R170 + R161 * Sin(Ang)
    Next j
    pX2 = R168: pY2 = R169
    'Adattamento nella finestra grafica
    pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
    'Traccio la linea
    If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
    USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
    End If
  End If
End If
Return

VISUALIZZA_XX_YY:
Dim XCR(3) As Double  'XCR = X Centro Raggio-utensile
Dim YCR(3) As Double  'YCR = Y Centro Raggio-utensile
Dim TCR(3) As Double  'TCR = T Centro Raggio-utensile
Dim RagUt  As Double
'
Dim DIS_p0p1  As Double
Dim DIS_p1p2  As Double
Dim BOM_p0p1  As Double
Dim BOM_p1p2  As Double
Dim BOM_MIN   As Double
Dim BOM_MAX   As Double
Dim SINGOLARI As String
  '
  RagUt = 0.25
  Rmin = 3E+38
  iMIN = 0
  RaggioINTmin = 3E+38
  iINTmin = 0
  SINGOLARI = UCase$(Trim$(GetInfo("Configurazione", "GESTIONE_PUNTI_SINGOLARI", Path_LAVORAZIONE_INI)))
  '-- ****** ******  FIANCO 1  ****** ******
  'SPALLA MOLA F1
  Dim ZINF1 As Double
  'ZINF1 = XX(0) + (Abs(R37) - (XX(0) - XX(Np + 1))) / 2
  ZINF1 = Abs(R37) / 2
  pX1 = (-ZINF1 - ZeroPezzoX) * ScalaX + PicW / 2
  pY1 = PicH
  pX2 = pX1
  pY2 = -(YY(0) - ZeroPezzoY) * ScalaY + PicH / 2
  USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
  pX1 = (-ZINF1 - ZeroPezzoX) * ScalaX + PicW / 2
  pY1 = -(YY(0) - ZeroPezzoY) * ScalaY + PicH / 2
  pX2 = (XX(0) - ZeroPezzoX) * ScalaX + PicW / 2
  pY2 = pY1
  USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
  'TRATTO RETTILINEO DI ENTRATA F1
  'modificato per visualizzare primo e ultimo punto  'i = 1
  i = 0
  pX1 = (XX(i) - ZeroPezzoX) * ScalaX + PicW / 2
  pY1 = -(YY(i) - ZeroPezzoY) * ScalaY + PicH / 2
  pX2 = (XX(i + 1) - ZeroPezzoX) * ScalaX + PicW / 2
  pY2 = -(YY(i + 1) - ZeroPezzoY) * ScalaY + PicH / 2
  If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
  USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
  End If
  'CURVA DI F1 CON ARCHI DI CERCHIO O RETTE
  sErrF1 = ""
  XCR(0) = -9999: YCR(0) = 9999
  If (RagUtF1 > 0) Then RagUt = RagUtF1
  jj = 0
  'modificato per visualizzare primo e ultimo punto   'For i = 2 To NPF1 - 2 Step 2
  For i = 1 To NPF1 - 2 Step 2
    If (i <> IUPP_F1) Or (SiPUNTI = 1) Then
      'COORD. X        COORD. Y
      R164 = XX(i + 0): R165 = YY(i + 0)
      R166 = XX(i + 1): R167 = YY(i + 1)
      R168 = XX(i + 2): R169 = YY(i + 2)
      'DISTANZA PER TRE PUNTI
      R162 = Abs((R165 - R169) * (R164 - R166) + (R167 - R165) * (R164 - R168)) / Sqr((R165 - R169) * (R165 - R169) + (R164 - R168) * (R164 - R168))
      If (INTERPOLAZIONE = 2) Then
        'TRATTO RETTILINEO
        'G1 Y=R167 X=R166
        pX1 = (R164 - ZeroPezzoX) * ScalaX + PicW / 2
        pY1 = -(R165 - ZeroPezzoY) * ScalaY + PicH / 2
        pX2 = (R166 - ZeroPezzoX) * ScalaX + PicW / 2
        pY2 = -(R167 - ZeroPezzoY) * ScalaY + PicH / 2
        If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
        USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
        End If
        'TRATTO RETTILINEO
        'G1 Y=R169 X=R168
        pX1 = (R166 - ZeroPezzoX) * ScalaX + PicW / 2
        pY1 = -(R167 - ZeroPezzoY) * ScalaY + PicH / 2
        pX2 = (R168 - ZeroPezzoX) * ScalaX + PicW / 2
        pY2 = -(R169 - ZeroPezzoY) * ScalaY + PicH / 2
        If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
        USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
        End If
        'TRATTO CURVILINEO
        If R169 = R167 Then R169 = R167 + 0.0001
        If R165 = R167 Then R165 = R167 + 0.0001
        R160 = (R164 - R166) / (R167 - R165)
        R171 = (R166 - R168) / (R169 - R167)
        R172 = (R165 + R167) / 2 - R160 * (R164 + R166) / 2
        R173 = (R169 + R167) / 2 - R171 * (R168 + R166) / 2
        If R160 = R171 Then R160 = R171 + 0.0001
        'coordinate del centro
        R174 = (R173 - R172) / (R160 - R171)
        R170 = R160 * R174 + R172
        'raggio del cerchio
        R161 = Sqr((R174 - R166) * (R174 - R166) + (R170 - R167) * (R170 - R167))
        'AGGIORNAMENTO DEL VALORE DI RAGGIO MINIMO
        If Rmin > R161 Then Rmin = R161: iMIN = i
        'prodotto misto per senso di rotazione
        R160 = (R166 - R164) * (R169 - R165) - (R168 - R164) * (R167 - R165)
        '***  ********* *********** ******* ********  ***
        'Calcolo angoli compresi tra 0 e 2 * Pigreco
        AngStart = CALCOLO_ANGOLO(R164 - R174, R165 - R170)
        AngEnd = CALCOLO_ANGOLO(R168 - R174, R169 - R170)
        If (R162 >= 0.0005) And (R160 > 0) And (RaggioINTmin > R161) Then RaggioINTmin = R161: iINTmin = i
      Else
        If (R162 < 0.0005) Then
          'TRATTO RETTILINEO
          'G1 Y=R169 X=R168
          pX1 = (R164 - ZeroPezzoX) * ScalaX + PicW / 2
          pY1 = -(R165 - ZeroPezzoY) * ScalaY + PicH / 2
          pX2 = (R168 - ZeroPezzoX) * ScalaX + PicW / 2
          pY2 = -(R169 - ZeroPezzoY) * ScalaY + PicH / 2
          If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
          End If
        Else
          'TRATTO CURVILINEO
          If R169 = R167 Then R169 = R167 + 0.0001
          If R165 = R167 Then R165 = R167 + 0.0001
          R160 = (R164 - R166) / (R167 - R165)
          R171 = (R166 - R168) / (R169 - R167)
          R172 = (R165 + R167) / 2 - R160 * (R164 + R166) / 2
          R173 = (R169 + R167) / 2 - R171 * (R168 + R166) / 2
          If R160 = R171 Then R160 = R171 + 0.0001
          'coordinate del centro
          R174 = (R173 - R172) / (R160 - R171)
          R170 = R160 * R174 + R172
          'raggio del cerchio
          R161 = Sqr((R174 - R166) * (R174 - R166) + (R170 - R167) * (R170 - R167))
          'bombatura p1-p2
          DIS_p0p1 = Sqr((R166 - R164) ^ 2 + (R167 - R165) ^ 2)
          BOM_p0p1 = R161 - Sqr(R161 ^ 2 - DIS_p0p1 ^ 2 / 4)
          DIS_p1p2 = Sqr((R168 - R166) ^ 2 + (R169 - R167) ^ 2)
          BOM_p1p2 = R161 - Sqr(R161 ^ 2 - DIS_p1p2 ^ 2 / 4)
          If (BOM_p0p1 > BOM_p1p2) Then
            BOM_MIN = BOM_p1p2
            BOM_MAX = BOM_p0p1
          Else
            BOM_MIN = BOM_p0p1
            BOM_MAX = BOM_p1p2
          End If
          If (BOM_MAX > R162) And (SINGOLARI = "Y") Then
          'MsgBox BOM_p0p1 & " " & BOM_p1p2, vbCritical, i
          'TRATTO RETTILINEO
          pX1 = (R164 - ZeroPezzoX) * ScalaX + PicW / 2
          pY1 = -(R165 - ZeroPezzoY) * ScalaY + PicH / 2
          pX2 = (R166 - ZeroPezzoX) * ScalaX + PicW / 2
          pY2 = -(R167 - ZeroPezzoY) * ScalaY + PicH / 2
          If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
          End If
          'TRATTO RETTILINEO
          pX1 = (R166 - ZeroPezzoX) * ScalaX + PicW / 2
          pY1 = -(R167 - ZeroPezzoY) * ScalaY + PicH / 2
          pX2 = (R168 - ZeroPezzoX) * ScalaX + PicW / 2
          pY2 = -(R169 - ZeroPezzoY) * ScalaY + PicH / 2
          If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
          End If
          Else
          'AGGIORNAMENTO DEL VALORE DI RAGGIO MINIMO
          If Rmin > R161 Then Rmin = R161: iMIN = i
          'prodotto misto per senso di rotazione
          R160 = (R166 - R164) * (R169 - R165) - (R168 - R164) * (R167 - R165)
          '***  CONTROLLO REGOLARITA' PROFILO FIANCO 1  ***
          'F1: Calcolo posizioni Centro Raggio Utensile (CRU)
          XCR(1) = R164 + (Sgn(R160)) * RagUt / R161 * (R174 - R164)
          YCR(1) = R165 + (Sgn(R160)) * RagUt / R161 * (R170 - R165)
          XCR(2) = R166 + (Sgn(R160)) * RagUt / R161 * (R174 - R166)
          YCR(2) = R167 + (Sgn(R160)) * RagUt / R161 * (R170 - R167)
          XCR(3) = R168 + (Sgn(R160)) * RagUt / R161 * (R174 - R168)
          YCR(3) = R169 + (Sgn(R160)) * RagUt / R161 * (R170 - R169)
          'F1: Controllo che il 1� punto del cerchio corrente
          '    sia a DX del 2� punto del cerchio precedente
          stmp = ""
          If XCR(1) < XCR(0) Then
            stmp = "peack on point " & Format$(i, "#####0")
            If Len(stmp) > 0 Then sErrF1 = sErrF1 & Chr(13) & stmp
          End If
          'Memorizzo l'ascissa del CRU sull'ultimo punto dell'arco
          XCR(0) = XCR(2): YCR(0) = YCR(2)
          'Verifica congruenza posizioni CRU sui punti dell'arco
          If (XCR(2) < XCR(1)) Or (XCR(3) < XCR(2)) Then
            'SEGNALAZIONE DI PROFILO NON CONGRUENTE
            msgERRORE = "WARNING: profile is not congruent!!!!"
          End If
          'SCRIVO SU USCITA ELENCO INCONGRUENZE FIANCO 1
          If (XCR(2) < XCR(1)) Then
            If (jj = 0) Then
              sERRORE = "F1: PROFILE NOT CONGRUENT!!"
              USCITA.CurrentX = 0
              USCITA.CurrentY = PicH / 2
              USCITA.Print sERRORE
            End If
            sERRORE = " XX(" & Format$(i + 1, "#####0") & ") < XX(" & Format$(i, "#####0") & ")"
            USCITA.CurrentX = 0
            USCITA.CurrentY = PicH / 2 + (jj + 1) * USCITA.TextHeight(sERRORE)
            USCITA.Print sERRORE
            jj = jj + 1
          End If
          If (XCR(3) < XCR(2)) Then
            If (jj = 0) Then
              sERRORE = "F1: PROFILE NOT CONGRUENT!!"
              USCITA.CurrentX = 0
              USCITA.CurrentY = PicH / 2
              USCITA.Print sERRORE
            End If
            sERRORE = " XX(" & Format$(i + 2, "#####0") & ") < XX(" & Format$(i + 1, "#####0") & ")"
            USCITA.CurrentX = 0
            USCITA.CurrentY = PicH / 2 + (jj + 1) * USCITA.TextHeight(sERRORE)
            USCITA.Print sERRORE
            jj = jj + 1
          End If
          'Calcolo angoli compresi tra 0 e 2 * Pigreco
          AngStart = CALCOLO_ANGOLO(R164 - R174, R165 - R170)
          AngEnd = CALCOLO_ANGOLO(R168 - R174, R169 - R170)
          'MsgBox "AngEnd= " & AngEnd & " AngStart= " & AngStart
          'MsgBox "R161= " & R161 & " R170= " & R170
          If (R160 > 0) Then
            If RaggioINTmin > R161 Then RaggioINTmin = R161: iINTmin = i
            'If RaggioINTmax < R161 Then RaggioINTmax = R161: iINTmax = i
            'G3 Y=R169 X=R168 CR=R161
            If AngEnd >= AngStart Then
              Ang = AngEnd - AngStart
            Else
              Ang = 2 * PG - (AngStart - AngEnd)
            End If
            DeltaAng = Ang / NumeroArchi
            pX1 = R164: pY1 = R165
            For j = 1 To NumeroArchi - 1
              Ang = (AngStart + j * DeltaAng)
              pX2 = R174 + R161 * Cos(Ang): pY2 = R170 + R161 * Sin(Ang)
              'Adattamento nella finestra grafica
              pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
              pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
              pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
              pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
              'Traccio la linea
              If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
              USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
              End If
              'Aggiorno Px1 e Py1
              pX1 = R174 + R161 * Cos(Ang): pY1 = R170 + R161 * Sin(Ang)
            Next j
            pX2 = R168: pY2 = R169
            'Adattamento nella finestra grafica
            pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
            pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
            pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
            pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
            'Traccio la linea
            If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
            USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
            End If
          Else
            'G2 Y=R169 X=R168 CR=R161
            If AngStart >= AngEnd Then
              Ang = AngStart - AngEnd
            Else
              Ang = 2 * PG - (AngEnd - AngStart)
            End If
            DeltaAng = Ang / NumeroArchi
            pX1 = R164: pY1 = R165
            For j = 1 To NumeroArchi - 1
              Ang = (AngStart - j * DeltaAng)
              pX2 = R174 + R161 * Cos(Ang): pY2 = R170 + R161 * Sin(Ang)
              'Adattamento nella finestra grafica
              pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
              pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
              pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
              pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
              'Traccio la linea
              If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
              USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
              End If
              'Aggiorno Px1 e Py1
              pX1 = R174 + R161 * Cos(Ang): pY1 = R170 + R161 * Sin(Ang)
            Next j
            pX2 = R168: pY2 = R169
            'Adattamento nella finestra grafica
            pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
            pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
            pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
            pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
            'Traccio la linea
            If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
            USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
            End If
          End If
          End If
        End If
      End If
    End If
  Next
  '
  '-- ****** ******  FIANCO 2  ****** ******
  'SPALLA MOLA F2
  Dim ZINF2 As Double
  'ZINF2 = XX(Np + 1) - (Abs(R37) - (XX(0) - XX(Np + 1))) / 2
  ZINF2 = -Abs(R37) / 2
  pX1 = (-ZINF2 - ZeroPezzoX) * ScalaX + PicW / 2
  pY1 = PicH
  pX2 = pX1
  pY2 = -(YY(Np + 1) - ZeroPezzoY) * ScalaY + PicH / 2
  USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
  pX1 = (-ZINF2 - ZeroPezzoX) * ScalaX + PicW / 2
  pY1 = -(YY(Np + 1) - ZeroPezzoY) * ScalaY + PicH / 2
  pX2 = (XX(Np + 1) - ZeroPezzoX) * ScalaX + PicW / 2
  pY2 = pY1
  USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
  'TRATTO RETTILINEO DI ENTRATA F2
  'modificato per visualizzare primo e ultimo punto  'i = Np
  i = Np + 1
  pX1 = (XX(i) - ZeroPezzoX) * ScalaX + PicW / 2
  pY1 = -(YY(i) - ZeroPezzoY) * ScalaY + PicH / 2
  pX2 = (XX(i - 1) - ZeroPezzoX) * ScalaX + PicW / 2
  pY2 = -(YY(i - 1) - ZeroPezzoY) * ScalaY + PicH / 2
  If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
  USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
  End If
  'CURVA DI F2 CON ARCHI DI CERCHIO O RETTE
  sErrF2 = ""
  XCR(0) = 9999
  If (RagUtF2 > 0) Then RagUt = RagUtF2
  jj = 0
  'modificato per visualizzare primo e ultimo punto  'For i = Np - 1 To NPF1 Step -2
  For i = Np To NPF1 + 2 Step -2
    If (i <> IUPP_F2) Or (SiPUNTI = 1) Then
      'COORD. X        COORD. Y
      R164 = XX(i - 0): R165 = YY(i - 0)
      R166 = XX(i - 1): R167 = YY(i - 1)
      R168 = XX(i - 2): R169 = YY(i - 2)
      'DISTANZA PER TRE PUNTI
      R162 = Abs((R165 - R169) * (R164 - R166) + (R167 - R165) * (R164 - R168)) / Sqr((R165 - R169) * (R165 - R169) + (R164 - R168) * (R164 - R168))
      If (INTERPOLAZIONE = 2) Then
        'TRATTO RETTILINEO
        'G1 Y=R169 X=R168
        pX1 = (R164 - ZeroPezzoX) * ScalaX + PicW / 2
        pY1 = -(R165 - ZeroPezzoY) * ScalaY + PicH / 2
        pX2 = (R166 - ZeroPezzoX) * ScalaX + PicW / 2
        pY2 = -(R167 - ZeroPezzoY) * ScalaY + PicH / 2
        If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
        USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
        End If
        'TRATTO RETTILINEO
        'G1 Y=R169 X=R168
        pX1 = (R166 - ZeroPezzoX) * ScalaX + PicW / 2
        pY1 = -(R167 - ZeroPezzoY) * ScalaY + PicH / 2
        pX2 = (R168 - ZeroPezzoX) * ScalaX + PicW / 2
        pY2 = -(R169 - ZeroPezzoY) * ScalaY + PicH / 2
        If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
        USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
        End If
        'TRATTO CURVILINEO
        If R169 = R167 Then R169 = R167 + 0.0001
        If R165 = R167 Then R165 = R167 + 0.0001
        R160 = (R164 - R166) / (R167 - R165)
        R171 = (R166 - R168) / (R169 - R167)
        R172 = (R165 + R167) / 2 - R160 * (R164 + R166) / 2
        R173 = (R169 + R167) / 2 - R171 * (R168 + R166) / 2
        If R160 = R171 Then R160 = R171 + 0.0001
        'coordinate del centro
        R174 = (R173 - R172) / (R160 - R171)
        R170 = R160 * R174 + R172
        'raggio del cerchio
        R161 = Sqr((R174 - R166) * (R174 - R166) + (R170 - R167) * (R170 - R167))
        'AGGIORNAMENTO DEL VALORE DI RAGGIO MINIMO
        If Rmin > R161 Then Rmin = R161: iMIN = i
        'prodotto misto per senso di rotazione
        R160 = (R166 - R164) * (R169 - R165) - (R168 - R164) * (R167 - R165)
        'Calcolo angoli compresi tra 0 e 2 * Pigreco
        AngStart = CALCOLO_ANGOLO(R164 - R174, R165 - R170)
        AngEnd = CALCOLO_ANGOLO(R168 - R174, R169 - R170)
        If (R162 >= 0.0005) And (R160 < 0) And (RaggioINTmin > R161) Then RaggioINTmin = R161: iINTmin = i
      Else
        If (R162 < 0.0005) Then
          'TRATTO RETTILINEO
          'G1 Y=R169 X=R168
          pX1 = (R164 - ZeroPezzoX) * ScalaX + PicW / 2
          pY1 = -(R165 - ZeroPezzoY) * ScalaY + PicH / 2
          pX2 = (R168 - ZeroPezzoX) * ScalaX + PicW / 2
          pY2 = -(R169 - ZeroPezzoY) * ScalaY + PicH / 2
          If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
          End If
        Else
          'TRATTO CURVILINEO
          If R169 = R167 Then R169 = R167 + 0.0001
          If R165 = R167 Then R165 = R167 + 0.0001
          R160 = (R164 - R166) / (R167 - R165)
          R171 = (R166 - R168) / (R169 - R167)
          R172 = (R165 + R167) / 2 - R160 * (R164 + R166) / 2
          R173 = (R169 + R167) / 2 - R171 * (R168 + R166) / 2
          If R160 = R171 Then R160 = R171 + 0.0001
          'coordinate del centro
          R174 = (R173 - R172) / (R160 - R171)
          R170 = R160 * R174 + R172
          'raggio del cerchio
          R161 = Sqr((R174 - R166) * (R174 - R166) + (R170 - R167) * (R170 - R167))
          'bombatura p1-p2
          DIS_p0p1 = Sqr((R166 - R164) ^ 2 + (R167 - R165) ^ 2)
          BOM_p0p1 = R161 - Sqr(R161 ^ 2 - DIS_p0p1 ^ 2 / 4)
          DIS_p1p2 = Sqr((R168 - R166) ^ 2 + (R169 - R167) ^ 2)
          BOM_p1p2 = R161 - Sqr(R161 ^ 2 - DIS_p1p2 ^ 2 / 4)
          If (BOM_p0p1 > BOM_p1p2) Then
            BOM_MIN = BOM_p1p2
            BOM_MAX = BOM_p0p1
          Else
            BOM_MIN = BOM_p0p1
            BOM_MAX = BOM_p1p2
          End If
          If (BOM_MAX > R162) And (SINGOLARI = "Y") Then
          'MsgBox BOM_p0p1 & " " & BOM_p1p2, vbCritical, i & " " & R162
          'TRATTO RETTILINEO
          pX1 = (R164 - ZeroPezzoX) * ScalaX + PicW / 2
          pY1 = -(R165 - ZeroPezzoY) * ScalaY + PicH / 2
          pX2 = (R166 - ZeroPezzoX) * ScalaX + PicW / 2
          pY2 = -(R167 - ZeroPezzoY) * ScalaY + PicH / 2
          If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
          End If
          'TRATTO RETTILINEO
          pX1 = (R166 - ZeroPezzoX) * ScalaX + PicW / 2
          pY1 = -(R167 - ZeroPezzoY) * ScalaY + PicH / 2
          pX2 = (R168 - ZeroPezzoX) * ScalaX + PicW / 2
          pY2 = -(R169 - ZeroPezzoY) * ScalaY + PicH / 2
          If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
          End If
          Else
          'AGGIORNAMENTO DEL VALORE DI RAGGIO MINIMO
          If Rmin > R161 Then Rmin = R161: iMIN = i
          'prodotto misto per senso di rotazione
          R160 = (R166 - R164) * (R169 - R165) - (R168 - R164) * (R167 - R165)
          '***  CONTROLLO REGOLARITA' PROFILO FIANCO 2  ***
          'F2: Calcolo posizioni Centro Raggio Utensile (CRU)
          XCR(1) = R164 + (-Sgn(R160)) * RagUt / R161 * (R174 - R164)
          YCR(1) = R165 + (-Sgn(R160)) * RagUt / R161 * (R170 - R165)
          XCR(2) = R166 + (-Sgn(R160)) * RagUt / R161 * (R174 - R166)
          YCR(2) = R167 + (-Sgn(R160)) * RagUt / R161 * (R170 - R167)
          XCR(3) = R168 + (-Sgn(R160)) * RagUt / R161 * (R174 - R168)
          YCR(3) = R169 + (-Sgn(R160)) * RagUt / R161 * (R170 - R169)
          'F2: Controllo che il 1� punto dell'arco di cerchio corrente
          '    sia a SX del 2� punto dell'arco di cerchio precedente
          If XCR(1) > XCR(0) Then
            stmp = "peack on point " & Format$(i, "#####0")
            If Len(stmp) > 0 Then sErrF2 = sErrF2 & Chr(13) & stmp
          End If
          'Memorizzo l'ascissa del CRU sull'ultimo punto dell'arco
          XCR(0) = XCR(2)
          'Verifica congruenza posizioni CRU sui punti dell'arco
          If (XCR(2) > XCR(1)) Or (XCR(3) > XCR(2)) Then
            'SEGNALAZIONE DI PROFILO NON CONGRUENTE
            msgERRORE = "WARNING: profile is not congruent!!!!"
          End If
          'SCRIVO SU USCITA ELENCO INCONGRUENZE FIANCO 2
          If (XCR(2) > XCR(1)) Then
            If (jj = 0) Then
              sERRORE = "F2: PROFILE NOT CONGRUENT!!"
              USCITA.CurrentX = PicW - USCITA.TextWidth(sERRORE)
              USCITA.CurrentY = PicH / 2
              USCITA.Print sERRORE
            End If
            sERRORE = " XX(" & Format$(i - 1, "#####0") & ") > XX(" & Format$(i, "#####0") & ")"
            USCITA.CurrentX = PicW - USCITA.TextWidth(sERRORE)
            USCITA.CurrentY = PicH / 2 + (jj + 1) * USCITA.TextHeight(sERRORE)
            USCITA.Print sERRORE
            jj = jj + 1
          End If
          If (XCR(3) > XCR(2)) Then
            If (jj = 0) Then
              sERRORE = "F2: PROFILE NOT CONGRUENT!!"
              USCITA.CurrentX = PicW - USCITA.TextWidth(sERRORE)
              USCITA.CurrentY = PicH / 2
              USCITA.Print sERRORE
            End If
            sERRORE = " XX(" & Format$(i - 2, "#####0") & ") > XX(" & Format$(i - 1, "#####0") & ")"
            USCITA.CurrentX = PicW - USCITA.TextWidth(sERRORE)
            USCITA.CurrentY = PicH / 2 + (jj + 1) * USCITA.TextHeight(sERRORE)
            USCITA.Print sERRORE
            jj = jj + 1
          End If
          '***  ********* *********** ******* ********  ***
          'Calcolo angoli compresi tra 0 e 2 * Pigreco
          AngStart = CALCOLO_ANGOLO(R164 - R174, R165 - R170)
          AngEnd = CALCOLO_ANGOLO(R168 - R174, R169 - R170)
          If R160 > 0 Then
            'G3 Y=R169 X=R168 CR=R161
            If AngEnd >= AngStart Then
              Ang = AngEnd - AngStart
            Else
              Ang = 2 * PG - (AngStart - AngEnd)
            End If
            DeltaAng = Ang / NumeroArchi
            pX1 = R164: pY1 = R165
            For j = 1 To NumeroArchi - 1
              Ang = (AngStart + j * DeltaAng)
              pX2 = R174 + R161 * Cos(Ang): pY2 = R170 + R161 * Sin(Ang)
              'Adattamento nella finestra grafica
              pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
              pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
              pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
              pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
              'Traccio la linea
              If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
              USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
              End If
              'Aggiorno Px1 e Py1
              pX1 = R174 + R161 * Cos(Ang): pY1 = R170 + R161 * Sin(Ang)
            Next j
            pX2 = R168: pY2 = R169
            'Adattamento nella finestra grafica
            pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
            pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
            pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
            pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
            'Traccio la linea
            If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
            USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
            End If
          Else
            If RaggioINTmin > R161 Then RaggioINTmin = R161: iINTmin = i
            'G2 Y=R169 X=R168 CR=R161
            If AngStart >= AngEnd Then
              Ang = AngStart - AngEnd
            Else
              Ang = 2 * PG - (AngEnd - AngStart)
            End If
            DeltaAng = Ang / NumeroArchi
            pX1 = R164: pY1 = R165
            For j = 1 To NumeroArchi - 1
              Ang = (AngStart - j * DeltaAng)
              pX2 = R174 + R161 * Cos(Ang): pY2 = R170 + R161 * Sin(Ang)
              'Adattamento nella finestra grafica
              pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
              pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
              pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
              pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
              'Traccio la linea
              If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
              USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
              End If
              'Aggiorno Px1 e Py1
              pX1 = R174 + R161 * Cos(Ang): pY1 = R170 + R161 * Sin(Ang)
            Next j
            pX2 = R168: pY2 = R169
            'Adattamento nella finestra grafica
            pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
            pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
            pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
            pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
            'Traccio la linea
            If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
            USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
            End If
          End If
          End If
        End If
      End If
    End If
  Next
Return

RICERCA_MAX_MIN:
  'Ricerca degli estremi
  Xmax = -1E+38: Ymax = -1E+38: Xmin = 1E+38: Ymin = 1E+38
  For i = 0 To Np + 1
    If XX(i) >= Xmax Then Xmax = XX(i)
    If YY(i) >= Ymax Then Ymax = YY(i)
    If XX(i) <= Xmin Then Xmin = XX(i)
    If YY(i) <= Ymin Then Ymin = YY(i)
  Next i
Return

PUNTI_PROFILO:
  For i = 1 To NPF1
    'LINEA TANGENTE
    pX1 = (XX(i) - 1.2 * DR * Sin(TT(i)) - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(YY(i) + 1.2 * DR * Cos(TT(i)) - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = (XX(i) + 1.2 * DR * Sin(TT(i)) - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(YY(i) - 1.2 * DR * Cos(TT(i)) - ZeroPezzoY) * ScalaY + PicH / 2
    USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
    'LINEA NORMALE
    pX1 = (XX(i) - DR * Cos(TT(i)) - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(YY(i) - DR * Sin(TT(i)) - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = (XX(i) + DR * Cos(TT(i)) - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(YY(i) + DR * Sin(TT(i)) - ZeroPezzoY) * ScalaY + PicH / 2
    USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
    'INDICE DEL PUNTO
    stmp = " " & Format$(i, "##0") & " "
    Select Case Senso
      Case 1
        pX1 = pX1 - USCITA.TextWidth(stmp)
        pY1 = pY1 - USCITA.TextHeight(stmp)
        If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          USCITA.CurrentX = pX1
          USCITA.CurrentY = pY1
          USCITA.Print stmp
        End If
      Case -1
        If (pY2 <= USCITA.ScaleHeight) And (pX2 <= USCITA.ScaleWidth) And (pX2 >= 0) And (pY2 >= 0) Then
          USCITA.CurrentX = pX2
          USCITA.CurrentY = pY2
          USCITA.Print stmp
        End If
    End Select
    'cerchio
    R174 = XX(i): R170 = YY(i): Raggio = DR: DeltaAng = 2 * PG / NumeroArchi
    pX1 = R174 + Raggio: pY1 = R170
    For j = 1 To NumeroArchi
      Ang = j * DeltaAng
      pX2 = R174 + Raggio * Cos(Ang): pY2 = R170 + Raggio * Sin(Ang)
      'Adattamento nella finestra grafica
      pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
      pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
      pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
      pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
      'Traccio la linea
      If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
        USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
      End If
      'Aggiorno Px1 e Py1
      pX1 = R174 + Raggio * Cos(Ang): pY1 = R170 + Raggio * Sin(Ang)
    Next j
  Next i
  For i = NPF1 + 1 To Np
    'LINEA TANGENTE
    pX1 = (XX(i) - 1.2 * DR * Sin(TT(i)) - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(YY(i) + 1.2 * DR * Cos(TT(i)) - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = (XX(i) + 1.2 * DR * Sin(TT(i)) - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(YY(i) - 1.2 * DR * Cos(TT(i)) - ZeroPezzoY) * ScalaY + PicH / 2
    USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
    'LINEA NORMALE
    pX1 = (XX(i) - DR * Cos(TT(i)) - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(YY(i) - DR * Sin(TT(i)) - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = (XX(i) + DR * Cos(TT(i)) - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(YY(i) + DR * Sin(TT(i)) - ZeroPezzoY) * ScalaY + PicH / 2
    USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
    'indice del punto
    stmp = " " & Format$(i, "##0") & " "
    Select Case Senso
      Case 1
        pY2 = pY2 - USCITA.TextHeight(stmp)
        If (pY2 <= USCITA.ScaleHeight) And (pX2 <= USCITA.ScaleWidth) And (pX2 >= 0) And (pY2 >= 0) Then
          USCITA.CurrentX = pX2
          USCITA.CurrentY = pY2
          USCITA.Print stmp
        End If
      Case -1
        pX1 = pX1 - USCITA.TextWidth(stmp)
        If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          USCITA.CurrentX = pX1
          USCITA.CurrentY = pY1
          USCITA.Print stmp
        End If
    End Select
    'cerchio
    R174 = XX(i): R170 = YY(i): Raggio = DR: DeltaAng = 2 * PG / NumeroArchi
    pX1 = R174 + Raggio: pY1 = R170
    For j = 1 To NumeroArchi
      Ang = j * DeltaAng
      pX2 = R174 + Raggio * Cos(Ang): pY2 = R170 + Raggio * Sin(Ang)
      'Adattamento nella finestra grafica
      pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2: pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
      pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2: pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
      'Traccio la linea
      If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
        USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
      End If
      'Aggiorno Px1 e Py1
      pX1 = R174 + Raggio * Cos(Ang): pY1 = R170 + Raggio * Sin(Ang)
    Next j
  Next i
Return

VISUALIZZA_INFORMAZIONI_PROFILO:
  'LE VARIABILI stmp E pY1 SONO IMPOSTATE
  sL(1, 1) = " CM-" & Format$(NPF1, "#0"): sL(1, 2) = "= " & frmt(Abs(XX(NPF1)), 4)
  sL(2, 1) = " CM-F1":                     sL(2, 2) = "= " & frmt(Abs(XX(1)), 4)
  sL(3, 1) = " CM-F2":                     sL(3, 2) = "= " & frmt(Abs(XX(Np)), 4)
  sL(4, 1) = " Ri min":                    sL(4, 2) = "= " & frmt(Abs(RaggioINTmin), 4) & " --> (" & Format$(iINTmin, "##0") & ")"
  sL(5, 1) = " H":                         sL(5, 2) = "= " & frmt(Ymax - Ymin, 4)
  sL(6, 1) = " DExt":                      sL(6, 2) = "= " & frmt(2 * Ymax, 4)
  sL(7, 1) = " DEnd":                      sL(7, 2) = "= " & frmt(2 * YY(1), 4)
  sL(8, 1) = " SCALE":                     sL(8, 2) = "= " & frmt(Scala, 4) & ":1"
  sL(9, 1) = " LA":                        sL(9, 2) = "= " & frmt(Abs(XX(1) - XX(Np)), 4)
  sL(10, 1) = " Lmax":                     sL(10, 2) = "= " & frmt(ParMolaROT.LMax, 4)
  sL(11, 1) = " DY":                       sL(11, 2) = "= " & frmt(Dy, 4) & " --> (" & Format$(iDY, "##0") & ")"
  sL(12, 1) = " DX":                       sL(12, 2) = "= " & frmt(dx, 4) & " --> (" & Format$(Idx, "##0") & ")"
  '
  TabX = USCITA.TextWidth(sL(1, 1))
  For i = 1 To 12
    If TabX < USCITA.TextWidth(sL(i, 1)) Then TabX = USCITA.TextWidth(sL(i, 1))
  Next i
  TabXMax = USCITA.TextWidth(sL(1, 2))
  For i = 1 To 12
    If TabXMax < USCITA.TextWidth(sL(i, 2)) Then TabXMax = USCITA.TextWidth(sL(i, 2))
  Next i
  TabXMax = TabX + TabXMax
  '
  TabX = USCITA.TextWidth(sL(1, 1))
  TabY = USCITA.TextHeight(sL(1, 1))
  For i = 1 To 12
    If TabX < USCITA.TextWidth(sL(i, 1)) Then TabX = USCITA.TextWidth(sL(i, 1))
    USCITA.CurrentX = pX1 + Senso * TabXMax
    USCITA.CurrentY = pY1 + (i - 1) * TabY
    USCITA.Print sL(i, 1)
  Next i
  For i = 1 To 12
    USCITA.CurrentX = pX1 + Senso * TabXMax + TabX
    USCITA.CurrentY = pY1 + (i - 1) * TabY
    USCITA.Print sL(i, 2)
  Next i
  '
Return

CORREZIONE_SPESSORE:
  'Fianco 1
  For i = 1 To NPF1 - 1
    XX(i) = XX(i) - CorS / 2
  Next i
  'non correggo lo spessore sul punto di separazione
  'Fianco 2
  For i = NPF1 + 1 To Np
    XX(i) = XX(i) + CorS / 2
  Next i
Return

errVISUALIZZA_MOLA_ROTORI:
  If FreeFile > 0 Then Close
  WRITE_DIALOG "SUB VISUALIZZA_MOLA_ROTORI ERROR: " & str(Err)
  Resume Next

End Sub

Sub VISUALIZZA_SEPARAZIONE(ByVal USCITA As Object, ByVal NPF1 As Integer, ByVal CoordX As Single, ByVal ZeroPezzoX As Single, ByVal ZeroPezzoY As Single, ByVal ScalaX As Single, ByVal ScalaY As Single, ByVal PicW As Single, ByVal PicH As Single, ByVal OldColoreFont As Integer)
'
Dim stmp        As String
Dim pX1         As Single
Dim pX2         As Single
Dim pY1         As Single
Dim pY2         As Single
Dim ColoreCurva As Integer
'
On Error GoTo errVISUALIZZA_SEPARAZIONE
  '
  ColoreCurva = 12
  'Testo da stampare
  stmp = " CM(" & Format$(NPF1, "###0") & ") "
  pX1 = (CoordX - ZeroPezzoX) * ScalaX + PicW / 2: pY1 = 0
  pX2 = (CoordX - ZeroPezzoX) * ScalaX + PicW / 2: pY2 = PicH
  '
  If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
    ''linea continua
    'USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
    '--- linea tratteggiata ---
    USCITA.DrawStyle = vbDashDot 'vbDot
    USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
    USCITA.DrawStyle = 0
    '--------------------------
    USCITA.CurrentX = pX1
    USCITA.CurrentY = 0 'pY2 / 2
    USCITA.ForeColor = QBColor(12)
    USCITA.Print stmp
  End If
  USCITA.ForeColor = OldColoreFont
  '
Exit Sub

errVISUALIZZA_SEPARAZIONE:
  WRITE_DIALOG "SUB VISUALIZZA_SEPARAZIONE ERROR: " & str(Err)
  Resume Next

End Sub

Sub VISUALIZZA_PROFILO_RAB_ROTORI(ByVal NomeForm As Form, ByVal SiStampa As String, ByVal Scala As Single, ByVal IndiceZeroPezzoX As Integer, ByVal IndiceZeroPezzoY As Integer, ByVal SiPUNTI As Integer, ByVal SiASSI As Integer, ByVal SiASSIALE As Integer)
'
Dim USCITA           As Object
Dim MODALITA         As String
Dim TIPO_LAVORAZIONE As String
Dim Codice           As String
Dim DBB              As Database
Dim RSS              As Recordset
'
Dim DENTRO As String
'
Dim R160 As Double
Dim R161 As Double
Dim R162 As Double
Dim R163 As Double
Dim R164 As Double
Dim R165 As Double
Dim R166 As Double
Dim R167 As Double
Dim R168 As Double
Dim R169 As Double
Dim R170 As Double
Dim R171 As Double
Dim R172 As Double
Dim R173 As Double
Dim R174 As Double
'
Dim vRaggioMinimo As Double
Dim iRaggioMinimo As Single
'
Dim Rmax As Double
Dim Rmin As Double
'
Dim DR  As Double
Dim iDR As Integer
'
Dim AngStart  As Double
Dim AngEnd    As Double
Dim Ang       As Double
Dim DeltaAng  As Double
Dim DeltaArco As Double
'
Dim ftmp   As Double
Dim ScalaX As Double
Dim ScalaY As Double
'
Dim ZeroPezzoX As Double
Dim ZeroPezzoY As Double
'
Dim R()  As Double
Dim A()  As Double
Dim B()  As Double
'
Dim stmp  As String
Dim riga  As String
Dim i     As Integer
Dim j     As Integer
Dim k     As Integer
'
Dim NDenteLOC  As Integer
Dim sNDenteLOC As String
'
Dim ELICA  As Double
Dim sELICA As String
Dim PASSO  As Double
Dim sPASSO As String
Dim Np     As Integer
Dim NPF1   As Integer
Dim sNpF1  As String
Dim dtmp   As Integer
'
Dim ColoreCurva  As Integer
Dim OldForeColor As Integer
'
Dim PicW As Single
Dim PicH As Single
'
Dim pX1 As Double
Dim pY1 As Double
Dim pX2 As Double
Dim pY2 As Double
'
Dim da            As Single
Dim Raggio        As Double
Dim ARCO_ESTERNO  As Double
Dim TY_ROT        As String
Dim DF            As Single
Dim AF            As Single
Dim NumeroRette   As Integer
Dim NumeroArchi   As Integer
Dim TipoCarattere As String
Dim Atmp          As String * 255
Dim iERR          As Long
'
On Error GoTo errVISUALIZZA_PROFILO_RAB_ROTORI
  '
  If (UCase$(SiStampa) = "Y") Then
    Set USCITA = Printer
  Else
    Set USCITA = NomeForm.OEM1PicCALCOLO
    USCITA.BackColor = &HFFFFFF
  End If
  NDenteLOC = LEP("R[5]"):  sNDenteLOC = LNP("R[5]") 'NUMERO DENTI
  ELICA = LEP("R[258]"):    sELICA = LNP("R[258]")   'INCLINAZIONE MOLA
  PASSO = LEP("R[390]"):    sPASSO = LNP("R[390]")   'PASSO ASSIALE
  NPF1 = LEP("R[43]"):      sNpF1 = LNP("R[43]")     'PUNTI DEL FIANCO 1
  'LETTURA DEL TIPO DI LAVORAZIONE
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  'NOME ROTORE
  MODALITA = GetInfo("Configurazione", "MODALITA", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))
  If (MODALITA <> "OFF-LINE") Then
    stmp = LEGGI_NomePezzo(TIPO_LAVORAZIONE)
  Else
    stmp = GetInfo(TIPO_LAVORAZIONE, "NomePezzoInserimento", Path_LAVORAZIONE_INI)
  End If
  Codice = stmp
  If Not (USCITA Is Printer) Then
    NomeForm.OEM1frameCalcolo.Caption = "  " & Codice & "  (Rotor Profile)"
    NomeForm.OEM1chkINPUT(0).Visible = True
    NomeForm.OEM1chkINPUT(1).Visible = True
    NomeForm.OEM1chkINPUT(2).Visible = True
    NomeForm.OEM1lblOX.Visible = True
    NomeForm.OEM1lblOY.Visible = True
    NomeForm.OEM1txtOX.Visible = True
    NomeForm.OEM1txtOY.Visible = True
    NomeForm.lblMANDRINI.Visible = False
    NomeForm.McCombo1.Visible = False
    i = LoadString(g_hLanguageLibHandle, 1000, Atmp, 255)
    If i > 0 Then NomeForm.lblINPUT.Caption = Left$(Atmp, i)
    NomeForm.txtINPUT.Text = frmt(Scala, 4)
    If i > 0 Then NomeForm.lblINP(0).Caption = Left$(Atmp, i)
    NomeForm.txtINP(0).Text = frmt(Scala, 4)
  End If
  'AREA GRAFICA
  stmp = GetInfo("DIS0", "PicW", Path_LAVORAZIONE_INI)
  If (Len(stmp) > 0) Then PicW = val(stmp) Else PicW = 190
  stmp = GetInfo("DIS0", "PicH", Path_LAVORAZIONE_INI)
  If (Len(stmp) > 0) Then PicH = val(stmp) Else PicH = 95
  'TIPOLOGIA DI RAPPRESENTAZIONE
  If SiASSIALE Then TY_ROT = "AXIAL" Else TY_ROT = "FRONT"
  '
  DF = val(GetInfo("DIS0", "DF", Path_LAVORAZIONE_INI))
  AF = val(GetInfo("DIS0", "AF", Path_LAVORAZIONE_INI))
  NumeroRette = val(GetInfo("DIS0", "NumeroRette", Path_LAVORAZIONE_INI))
  NumeroArchi = val(GetInfo("DIS0", "NumeroArchi", Path_LAVORAZIONE_INI))
  '
  If DF = 0 Then DF = 2
  If AF = 0 Then AF = 30
  If NumeroRette = 0 Then NumeroRette = 20
  If NumeroArchi = 0 Then NumeroArchi = 50
  '
  TipoCarattere = GetInfo("DIS0", "TextFont", Path_LAVORAZIONE_INI)
  If (LINGUA = "CH") Then TipoCarattere = "MS Song"
  If (LINGUA = "RU") Then TipoCarattere = "Arial Cyr"
  If (Len(TipoCarattere) <= 0) Then TipoCarattere = "Arial"
  '
  If (SiStampa = "Y") Then
    For i = 1 To 2
      USCITA.FontName = TipoCarattere
      USCITA.FontBold = False
      USCITA.FontSize = 10
    Next i
    WRITE_DIALOG USCITA.DeviceName & " PRINTING....."
    If PicW = 190 And PicH = 95 Then
      USCITA.Orientation = 1
    Else
      USCITA.Orientation = 2
    End If
    USCITA.ScaleMode = 6
  Else
    USCITA.Cls
    USCITA.ScaleWidth = PicW
    USCITA.ScaleHeight = PicH
    USCITA.FontName = TipoCarattere
    USCITA.FontBold = False
    USCITA.FontSize = 8
    USCITA.Refresh
  End If
  Rmax = -999999: Rmin = 999999
  Select Case NomeForm.Name
    Case "OEM0"
      For i = 1 To NomeForm.List1(0).ListCount
        ReDim Preserve R(i): ReDim Preserve A(i): ReDim Preserve B(i)
        R(i) = val(NomeForm.List1(1).List(i - 1))
        A(i) = val(NomeForm.List1(2).List(i - 1))
        B(i) = val(NomeForm.List1(3).List(i - 1))
        If R(i) = 0 And A(i) = 0 And B(i) = 0 Then Exit For
        If Rmax < R(i) Then Rmax = R(i)
        If Rmin > R(i) Then Rmin = R(i)
      Next i
      Np = i - 1
    Case "OEM1", "OEMX": Call ROTORI1.LETTURA_PARPROFILO_ROT(Np, R(), A(), B(), Rmin, Rmax)
    Case Else:           WRITE_DIALOG "ERROR: Form type not defined": Exit Sub
  End Select
  '
  If (Scala = 0) Then
    Scala = PicH / (2 * Rmax * 1.15)
    If (NomeForm.Name = "OEM0") Then NomeForm.txtINPUT.Text = frmt(Scala, 4)
    If (NomeForm.Name = "OEM1") Or (NomeForm.Name = "OEMX") Then NomeForm.txtINP(0).Text = frmt(Scala, 4)
  End If
  '
  ScalaX = Scala: ScalaY = Scala
  '
  If (TY_ROT = "AXIAL") Then
    If (IndiceZeroPezzoX >= 1) And (IndiceZeroPezzoX <= Np) Then
      'Profilo AZZERATO SU UN PUNTO DEL PROFILO STESSO
      ZeroPezzoX = A(IndiceZeroPezzoX) / 360 * PASSO
      NDenteLOC = 1
    Else
      'Profilo centrato
      ZeroPezzoX = (NDenteLOC - 1) * (A(Np) / 360 * PASSO)
    End If
    If (IndiceZeroPezzoY >= 1) And (IndiceZeroPezzoY <= Np) Then
      'Profilo AZZERATO SU UN PUNTO DEL PROFILO STESSO
      ZeroPezzoY = R(IndiceZeroPezzoY)
      NDenteLOC = 1
    Else
      'Profilo centrato
      ZeroPezzoY = Rmin
    End If
  Else
    If (IndiceZeroPezzoX >= 1) And (IndiceZeroPezzoX <= Np) Then
      'Profilo AZZERATO SU UN PUNTO DEL PROFILO STESSO
      ZeroPezzoX = R(IndiceZeroPezzoX) * Sin((A(IndiceZeroPezzoX)) * PG / 180)
      NDenteLOC = 1
    Else
      ZeroPezzoX = 0
    End If
    If (IndiceZeroPezzoY >= 1) And (IndiceZeroPezzoY <= Np) Then
      'Profilo AZZERATO SU DI UN PUNTO DEL PROFILO STESSO
      ZeroPezzoY = R(IndiceZeroPezzoY) * Cos((A(IndiceZeroPezzoY)) * PG / 180)
      NDenteLOC = 1
    Else
      'Profilo centrato
      If NDenteLOC = 1 Then ZeroPezzoY = Rmin Else ZeroPezzoY = 0
    End If
  End If
  '
  stmp = "Rotor Name:  " & Codice
  USCITA.CurrentX = PicW / 2 - USCITA.TextWidth(stmp) / 2
  USCITA.CurrentY = PicH - USCITA.TextHeight(stmp)
  USCITA.Print stmp
  '
  If (SiASSI = 1) Then Call SCRIVI_ASSI(USCITA, SiStampa)
  '
  'Calcolo dr
  DR = 999999: iDR = 1
  For i = 1 To Np - 1
    pX1 = (R(i) * Sin((A(i) + da) * PG / 180))
    pY1 = -(R(i) * Cos((A(i) + da) * PG / 180))
    pX2 = (R(i + 1) * Sin((A(i + 1) + da) * PG / 180))
    pY2 = -(R(i + 1) * Cos((A(i + 1) + da) * PG / 180))
    ftmp = Sqr((pX2 - pX1) ^ 2 + (pY2 - pY1) ^ 2)
    If ftmp < DR Then DR = ftmp: iDR = i
  Next i
  If (DR = 0) And Abs(B(iDR) - B(iDR + 1)) < 0.01 Then
    stmp = "Duplicate point into ROTOR profile."
    StopRegieEvents
    MsgBox stmp, vbCritical, "WARNING: point " & Format$(iDR, "####0")
    ResumeRegieEvents
  End If
  '
  Call SCRIVI_SCALA_VERTICALE(ScalaY, USCITA, SiStampa)
  Call SCRIVI_SCALA_ORIZZONTALE(ScalaX, USCITA, SiStampa)
  '
  'CURVA CON ARCHI DI CERCHIO
  ColoreCurva = 9: GoSub VISUALIZZA_RAB_CERCHI
  '
  If (TY_ROT <> "AXIAL") Then
    ColoreCurva = 12
    DENTRO = "N": Raggio = Rmax: GoSub SCRIVI_DIAMETRO: GoSub DISEGNA_DIAMETRO
    ColoreCurva = 9  '0
    DENTRO = "Y": Raggio = Rmin: GoSub SCRIVI_DIAMETRO: GoSub DISEGNA_DIAMETRO
    If (SiPUNTI = 1) Then Call CREAZIONE_FILE_PROFILO_ROTORE_DXF(Np, R, A, PASSO, "FRONT", Codice, NDenteLOC)
  Else
    ColoreCurva = 12
    DENTRO = "N": Raggio = Rmax: GoSub SCRIVI_DIAMETRO
    ColoreCurva = 9  '0
    DENTRO = "Y": Raggio = Rmin: GoSub SCRIVI_DIAMETRO
    If (SiPUNTI = 1) Then Call CREAZIONE_FILE_PROFILO_ROTORE_DXF(Np, R, A, PASSO, "AXIAL", Codice, NDenteLOC)
  End If
  '
  'NOME DEL FILE DEL PROFILO E NUMERO DEI PUNTI
  stmp = "" '" " & NomeRotore
  stmp = stmp & " (Np=F1+F2="
  stmp = stmp & Format$(NPF1, "#######0") & "+" & Format$(Np - NPF1, "#######0")
  stmp = stmp & "=" & Format$(Np, "#######0") & ")"
  USCITA.CurrentX = 0
  USCITA.CurrentY = USCITA.TextHeight(stmp) * 0
  USCITA.Print stmp
  'DISTANZA MININA TRA I PUNTI DEL PROFILO
  stmp = " DR=" & frmt(DR, 8) & "mm /" & Format$(iDR, "########0")
  USCITA.CurrentX = 0
  USCITA.CurrentY = USCITA.TextHeight(stmp) * 1
  USCITA.Print stmp
  'ALTEZZA DENTE
  stmp = " HD=" & frmt(Rmax - Rmin, 8) & "mm"
  USCITA.CurrentX = 0
  USCITA.CurrentY = USCITA.TextHeight(stmp) * 2
  USCITA.Print stmp
  'RAGGIO DI CURVATURA MINIMO
  stmp = " RPmin= " & frmt(vRaggioMinimo, 4) & "mm "
  stmp = stmp & "/" & frmt(iRaggioMinimo, 4)
  USCITA.CurrentX = 0
  USCITA.CurrentY = PicH / 2
  USCITA.Print stmp
  'ARCO SUL DIAMETRO ESTERNO DEL ROTORE TEORICO
  If (NDenteLOC >= 1) Then
    If (TY_ROT = "AXIAL") Then
      ARCO_ESTERNO = PASSO / NDenteLOC - (A(Np) - A(1)) / 360 * PASSO
      stmp = " Seg= " & frmt(ARCO_ESTERNO, 4) & "mm "
    Else
      ARCO_ESTERNO = (2 * PG / NDenteLOC - (Abs(A(1)) + Abs(A(Np))) * PG / 180) * Rmax
      stmp = " Arc= " & frmt(ARCO_ESTERNO, 4) & "mm "
    End If
    USCITA.CurrentX = PicW - USCITA.TextWidth(stmp)
    USCITA.CurrentY = PicH / 2
    USCITA.Print stmp
  End If
  'PUNTI DEL PROFILO ROTORE
  If (SiPUNTI = 1) Then GoSub VISUALIZZA_PUNTI
  'Segnalazione della fine delle operazioni
  If (SiStampa = "Y") Then USCITA.EndDoc
  WRITE_DIALOG ""
  '
Exit Sub

VISUALIZZA_RAB_CERCHI:
'
Dim DIS_p0p1  As Double
Dim DIS_p1p2  As Double
Dim BOM_p0p1  As Double
Dim BOM_p1p2  As Double
Dim BOM_MIN   As Double
Dim BOM_MAX   As Double
'
  For k = 0 To NDenteLOC - 1
    vRaggioMinimo = 1E+38
    iRaggioMinimo = 1
    da = (360 / NDenteLOC) * k
    For i = 1 To Np - 2 Step 2
      If (TY_ROT = "AXIAL") Then
        'COORD. X        COORD. Y
        R164 = (A(i + 0) + da) / 360 * PASSO: R165 = R(i + 0)
        R166 = (A(i + 1) + da) / 360 * PASSO: R167 = R(i + 1)
        R168 = (A(i + 2) + da) / 360 * PASSO: R169 = R(i + 2)
      Else
        'COORD. X        COORD. Y
        R164 = R(i + 0) * Sin((A(i + 0) + da) * PG / 180): R165 = R(i + 0) * Cos((A(i + 0) + da) * PG / 180)
        R166 = R(i + 1) * Sin((A(i + 1) + da) * PG / 180): R167 = R(i + 1) * Cos((A(i + 1) + da) * PG / 180)
        R168 = R(i + 2) * Sin((A(i + 2) + da) * PG / 180): R169 = R(i + 2) * Cos((A(i + 2) + da) * PG / 180)
      End If
      'DISTANZA PER TRE PUNTI
      ftmp = Sqr((R165 - R169) * (R165 - R169) + (R164 - R168) * (R164 - R168))
      If (ftmp < 0.0001) Then ftmp = 0.0001
      R162 = Abs((R165 - R169) * (R164 - R166) + (R167 - R165) * (R164 - R168))
      R162 = R162 * (1 / ftmp)
      If (R162 < 0.0005) Then
        'TRATTO RETTILINEO
        'G1 Y=R169 X=R168
        pX1 = (R164 - ZeroPezzoX) * ScalaX + PicW / 2
        pY1 = -(R165 - ZeroPezzoY) * ScalaY + PicH / 2
        pX2 = (R166 - ZeroPezzoX) * ScalaX + PicW / 2
        pY2 = -(R167 - ZeroPezzoY) * ScalaY + PicH / 2
        If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
        End If
        'TRATTO RETTILINEO
        'G1 Y=R169 X=R168
        pX1 = (R166 - ZeroPezzoX) * ScalaX + PicW / 2
        pY1 = -(R167 - ZeroPezzoY) * ScalaY + PicH / 2
        pX2 = (R168 - ZeroPezzoX) * ScalaX + PicW / 2
        pY2 = -(R169 - ZeroPezzoY) * ScalaY + PicH / 2
        If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
        End If
      Else
        'TRATTO CURVILINEO
        If (R169 = R167) Then R169 = R167 + 0.0001
        If (R165 = R167) Then R165 = R167 + 0.0001
        R160 = (R164 - R166) / (R167 - R165)
        R171 = (R166 - R168) / (R169 - R167)
        R172 = (R165 + R167) / 2 - R160 * (R164 + R166) / 2
        R173 = (R169 + R167) / 2 - R171 * (R168 + R166) / 2
        If (R160 = R171) Then R160 = R171 + 0.0001
        'Coordinate del centro
        R174 = (R173 - R172) / (R160 - R171)
        R170 = R160 * R174 + R172
        'Raggio del cerchio
        R161 = Sqr((R174 - R166) * (R174 - R166) + (R170 - R167) * (R170 - R167))
        'bombatura p1-p2
        DIS_p0p1 = Sqr((R166 - R164) ^ 2 + (R167 - R165) ^ 2)
        BOM_p0p1 = R161 - Sqr(R161 ^ 2 - DIS_p0p1 ^ 2 / 4)
        DIS_p1p2 = Sqr((R168 - R166) ^ 2 + (R169 - R167) ^ 2)
        BOM_p1p2 = R161 - Sqr(R161 ^ 2 - DIS_p1p2 ^ 2 / 4)
        If (BOM_p0p1 > BOM_p1p2) Then
          BOM_MIN = BOM_p1p2
          BOM_MAX = BOM_p0p1
        Else
          BOM_MIN = BOM_p0p1
          BOM_MAX = BOM_p1p2
        End If
        If (BOM_MAX > R162) Then
        'MsgBox BOM_p0p1 & " " & BOM_p1p2, vbCritical, i
        'TRATTO RETTILINEO
        pX1 = (R164 - ZeroPezzoX) * ScalaX + PicW / 2
        pY1 = -(R165 - ZeroPezzoY) * ScalaY + PicH / 2
        pX2 = (R166 - ZeroPezzoX) * ScalaX + PicW / 2
        pY2 = -(R167 - ZeroPezzoY) * ScalaY + PicH / 2
        If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
        USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
        End If
        'TRATTO RETTILINEO
        pX1 = (R166 - ZeroPezzoX) * ScalaX + PicW / 2
        pY1 = -(R167 - ZeroPezzoY) * ScalaY + PicH / 2
        pX2 = (R168 - ZeroPezzoX) * ScalaX + PicW / 2
        pY2 = -(R169 - ZeroPezzoY) * ScalaY + PicH / 2
        If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
        USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
        End If
        Else
        If (R161 <= vRaggioMinimo) Then vRaggioMinimo = R161: iRaggioMinimo = i
        If NDenteLOC > 1 Then
          'TRATTO RETTILINEO
          'G1 Y=R166 X=R167
          pX1 = (R164 - ZeroPezzoX) * ScalaX + PicW / 2
          pY1 = -(R165 - ZeroPezzoY) * ScalaY + PicH / 2
          pX2 = (R166 - ZeroPezzoX) * ScalaX + PicW / 2
          pY2 = -(R167 - ZeroPezzoY) * ScalaY + PicH / 2
          If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
            USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
          End If
          'TRATTO RETTILINEO
          'G1 Y=R168 X=R169
          pX1 = (R166 - ZeroPezzoX) * ScalaX + PicW / 2
          pY1 = -(R167 - ZeroPezzoY) * ScalaY + PicH / 2
          pX2 = (R168 - ZeroPezzoX) * ScalaX + PicW / 2
          pY2 = -(R169 - ZeroPezzoY) * ScalaY + PicH / 2
          If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
            USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
          End If
        Else
          'Prodotto misto per senso di rotazione
          R160 = (R166 - R164) * (R169 - R165) - (R168 - R164) * (R167 - R165)
          'Calcolo angoli compresi tra 0 e 2 * Pigreco. --------------------------
          AngStart = CALCOLO_ANGOLO(R164 - R174, R165 - R170)
          AngEnd = CALCOLO_ANGOLO(R168 - R174, R169 - R170)
          '-----------------------------------------------------------------------
          If (R160 > 0) Then
            'G3 Y=R169 X=R168 CR=R161
            If AngEnd >= AngStart Then
              Ang = AngEnd - AngStart
            Else
              Ang = 2 * PG - (AngStart - AngEnd)
            End If
            DeltaAng = Ang / NumeroArchi
            pX1 = R164: pY1 = R165
            For j = 1 To NumeroArchi - 1
              Ang = (AngStart + j * DeltaAng)
              pX2 = R174 + R161 * Cos(Ang): pY2 = R170 + R161 * Sin(Ang)
              'Adattamento nella finestra grafica
              pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
              pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
              pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
              pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
              'Traccio la linea
              If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
                USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
              End If
              'Aggiorno Px1 e Py1
              pX1 = R174 + R161 * Cos(Ang): pY1 = R170 + R161 * Sin(Ang)
            Next j
            pX2 = R168: pY2 = R169
            'Adattamento nella finestra grafica
            pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
            pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
            pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
            pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
            'Traccio la linea
            If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
              USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
            End If
          Else
            'G2 Y=R169 X=R168 CR=R161
            If AngStart >= AngEnd Then
              Ang = AngStart - AngEnd
            Else
              Ang = 2 * PG - (AngEnd - AngStart)
            End If
            DeltaAng = Ang / NumeroArchi
            pX1 = R164: pY1 = R165
            For j = 1 To NumeroArchi - 1
              Ang = (AngStart - j * DeltaAng)
              pX2 = R174 + R161 * Cos(Ang): pY2 = R170 + R161 * Sin(Ang)
              'Adattamento nella finestra grafica
              pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
              pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
              pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
              pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
              'Traccio la linea
              If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
                USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
              End If
              'Aggiorno Px1 e Py1
              pX1 = R174 + R161 * Cos(Ang): pY1 = R170 + R161 * Sin(Ang)
            Next j
            pX2 = R168: pY2 = R169
            'Adattamento nella finestra grafica
            pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
            pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
            pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
            pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
            'Traccio la linea
            If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
              USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
            End If
          End If
        End If
        End If
      End If
    Next i
  Next k
Return

VISUALIZZA_PUNTI:
Dim SiINDICI As Integer
  '
  If (SiStampa = "Y") Then
    StopRegieEvents
    j = MsgBox("Indeces ? ", 32 + 4, "WARNING")
    ResumeRegieEvents
    If j = 6 Then SiINDICI = 1 Else SiINDICI = 0
  End If
  'For j = 0 To NDenteloc - 1
  j = 0
  da = (360 / NDenteLOC) * j
    For i = 1 To NPF1
      'LINEA TANGENTE
      If (TY_ROT = "AXIAL") Then
        Ang = Atn(Tan(PG * B(i) / 180) * PASSO / (2 * R(i) * PG))
        pX1 = A(i) / 360 * PASSO - ZeroPezzoX
        pY1 = R(i) - ZeroPezzoY
      Else
        Ang = (A(i) + B(i)) * PG / 180
        pX1 = R(i) * Sin((A(i) + da) * PG / 180) - ZeroPezzoX
        pY1 = R(i) * Cos((A(i) + da) * PG / 180) - ZeroPezzoY
      End If
      pX2 = pX1
      pY2 = pY1
      pX1 = (pX1 + DR * Sin(Ang)) * ScalaX + PicW / 2
      pY1 = -(pY1 + DR * Cos(Ang)) * ScalaY + PicH / 2
      pX2 = (pX2 + DR * Sin(Ang - PG)) * ScalaX + PicW / 2
      pY2 = -(pY2 + DR * Cos(Ang - PG)) * ScalaY + PicH / 2
      If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
        USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
      End If
      'LINEA NORMALE
      If (TY_ROT = "AXIAL") Then
        Ang = Atn(Tan(PG * B(i) / 180) * PASSO / (2 * R(i) * PG))
        pX1 = A(i) / 360 * PASSO - ZeroPezzoX
        pY1 = R(i) - ZeroPezzoY
      Else
        Ang = (A(i) + B(i)) * PG / 180
        pX1 = R(i) * Sin((A(i) + da) * PG / 180) - ZeroPezzoX
        pY1 = R(i) * Cos((A(i) + da) * PG / 180) - ZeroPezzoY
      End If
      pX2 = pX1
      pY2 = pY1
      pX1 = (pX1 + DR * Sin(Ang - PG / 2)) * ScalaX + PicW / 2
      pY1 = -(pY1 + DR * Cos(Ang - PG / 2)) * ScalaY + PicH / 2
      pX2 = (pX2 + DR * Sin(Ang - PG - PG / 2)) * ScalaX + PicW / 2
      pY2 = -(pY2 + DR * Cos(Ang - PG - PG / 2)) * ScalaY + PicH / 2
      'pX2 = pX2 * ScalaX + PicW / 2
      'pY2 = -pY2 * ScalaY + PicH / 2
      If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
        USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
      End If
      'indice del punto
      stmp = " " & Format$(i, "##0") & " "
      If SiINDICI = 1 Then
        pX1 = pX1 - USCITA.TextWidth(stmp)
        pY1 = pY1 - USCITA.TextHeight(stmp)
        If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          USCITA.CurrentX = pX1
          USCITA.CurrentY = pY1
          USCITA.Print stmp
        End If
      Else
        pX1 = pX1 - USCITA.TextWidth(stmp)
        pY1 = pY1 - USCITA.TextHeight(stmp)
        If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          USCITA.CurrentX = pX1
          USCITA.CurrentY = pY1
          USCITA.Print stmp
        End If
      End If
      'CERCHIO
      If (TY_ROT = "AXIAL") Then
        R174 = A(i) / 360 * PASSO - ZeroPezzoX
        R170 = R(i) - ZeroPezzoY
      Else
        R174 = R(i) * Sin((A(i) + da) * PG / 180) - ZeroPezzoX
        R170 = R(i) * Cos((A(i) + da) * PG / 180) - ZeroPezzoY
      End If
      Raggio = DR
      DeltaAng = 2 * PG / NumeroArchi
      pX1 = R174 + Raggio: pY1 = R170
      For k = 1 To NumeroArchi
        Ang = k * DeltaAng
        pX2 = R174 + Raggio * Cos(Ang): pY2 = R170 + Raggio * Sin(Ang)
        'Adattamento nella finestra grafica
        pX1 = pX1 * ScalaX + PicW / 2
        pY1 = -pY1 * ScalaY + PicH / 2
        pX2 = pX2 * ScalaX + PicW / 2
        pY2 = -pY2 * ScalaY + PicH / 2
        'Traccio la linea
        If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
        End If
        'Aggiorno Px1 e Py1
        pX1 = R174 + Raggio * Cos(Ang): pY1 = R170 + Raggio * Sin(Ang)
      Next k
    Next i
    For i = NPF1 + 1 To Np
      'LINEA TANGENTE
      If (TY_ROT = "AXIAL") Then
        Ang = Atn(Tan(PG * B(i) / 180) * PASSO / (2 * R(i) * PG))
        pX1 = A(i) / 360 * PASSO - ZeroPezzoX
        pY1 = R(i) - ZeroPezzoY
      Else
        Ang = (A(i) + B(i)) * PG / 180
        pX1 = R(i) * Sin((A(i) + da) * PG / 180) - ZeroPezzoX
        pY1 = R(i) * Cos((A(i) + da) * PG / 180) - ZeroPezzoY
      End If
      pX2 = pX1
      pY2 = pY1
      pX1 = (pX1 + DR * Sin(Ang)) * ScalaX + PicW / 2
      pY1 = -(pY1 + DR * Cos(Ang)) * ScalaY + PicH / 2
      pX2 = (pX2 + DR * Sin(Ang - PG)) * ScalaX + PicW / 2
      pY2 = -(pY2 + DR * Cos(Ang - PG)) * ScalaY + PicH / 2
      If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
        USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
      End If
      'LINEA NORMALE
      If (TY_ROT = "AXIAL") Then
        Ang = Atn(Tan(PG * B(i) / 180) * PASSO / (2 * R(i) * PG))
        pX1 = A(i) / 360 * PASSO - ZeroPezzoX
        pY1 = R(i) - ZeroPezzoY
      Else
        Ang = (A(i) + B(i)) * PG / 180
        pX1 = R(i) * Sin((A(i) + da) * PG / 180) - ZeroPezzoX
        pY1 = R(i) * Cos((A(i) + da) * PG / 180) - ZeroPezzoY
      End If
      pX2 = pX1
      pY2 = pY1
      pX1 = (pX1 + DR * Sin(Ang - PG / 2)) * ScalaX + PicW / 2
      pY1 = -(pY1 + DR * Cos(Ang - PG / 2)) * ScalaY + PicH / 2
      pX2 = (pX2 + DR * Sin(Ang - PG - PG / 2)) * ScalaX + PicW / 2
      pY2 = -(pY2 + DR * Cos(Ang - PG - PG / 2)) * ScalaY + PicH / 2
      'pX2 = pX2 * ScalaX + PicW / 2
      'pY2 = -pY2 * ScalaY + PicH / 2
      If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
        USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
      End If
      'indice del punto
      stmp = " " & Format$(i, "##0") & " "
      If SiINDICI = 1 Then
        If (pY2 <= USCITA.ScaleHeight) And (pX2 <= USCITA.ScaleWidth) And (pX2 >= 0) And (pY2 >= 0) Then
          USCITA.CurrentX = pX2
          USCITA.CurrentY = pY2
          USCITA.Print stmp
        End If
      Else
        If (pY2 <= USCITA.ScaleHeight) And (pX2 <= USCITA.ScaleWidth) And (pX2 >= 0) And (pY2 >= 0) Then
          USCITA.CurrentX = pX2
          USCITA.CurrentY = pY2
          USCITA.Print stmp
        End If
      End If
      'CERCHIO
      If (TY_ROT = "AXIAL") Then
        R174 = A(i) / 360 * PASSO - ZeroPezzoX
        R170 = R(i) - ZeroPezzoY
      Else
        R174 = R(i) * Sin((A(i) + da) * PG / 180) - ZeroPezzoX
        R170 = R(i) * Cos((A(i) + da) * PG / 180) - ZeroPezzoY
      End If
      Raggio = DR
      DeltaAng = 2 * PG / NumeroArchi
      pX1 = R174 + Raggio: pY1 = R170
      For k = 1 To NumeroArchi
        Ang = k * DeltaAng
        pX2 = R174 + Raggio * Cos(Ang): pY2 = R170 + Raggio * Sin(Ang)
        'Adattamento nella finestra grafica
        pX1 = pX1 * ScalaX + PicW / 2
        pY1 = -pY1 * ScalaY + PicH / 2
        pX2 = pX2 * ScalaX + PicW / 2
        pY2 = -pY2 * ScalaY + PicH / 2
        'Traccio la linea
        If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
        End If
        'Aggiorno Px1 e Py1
        pX1 = R174 + Raggio * Cos(Ang): pY1 = R170 + Raggio * Sin(Ang)
      Next k
    Next i
  'Next j
Return
     
SCRIVI_DIAMETRO:
  'Testo da stampare
  stmp = " " & frmt(2 * Raggio, 4) & "mm "
  'Linea sopra il del testo
  pX1 = (0 - ZeroPezzoX) * ScalaX + PicW / 2
  pY1 = -(Raggio - ZeroPezzoY) * ScalaY + PicH / 2
  pX2 = (0 - ZeroPezzoX) * ScalaX + PicW / 2
  pY2 = -(0 - ZeroPezzoY) * ScalaY + PicH / 2 - USCITA.TextHeight(stmp) / 2
  If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
    USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
  End If
  'Freccia sopra il testo
  Ang = AF / 180 * PG
  DeltaAng = Ang / NumeroRette
  For i = -NumeroRette To NumeroRette
    pX2 = (0 - ZeroPezzoX) * ScalaX + DF * Sin(i * DeltaAng) + PicW / 2
    pY2 = -(Raggio - ZeroPezzoY) * ScalaY + DF * Cos(Ang) + PicH / 2
    If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
      USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
    End If
  Next i
  'Linea di riferimento per la freccia sopra al testo
  pX1 = (0 - ZeroPezzoX) * ScalaX - DF + PicW / 2
  pY1 = -(Raggio - ZeroPezzoY) * ScalaY + PicH / 2
  pX2 = (0 - ZeroPezzoX) * ScalaX + DF + PicW / 2
  pY2 = -(Raggio - ZeroPezzoY) * ScalaY + PicH / 2
  If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
    USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
  End If
  'Scrittura del testo
  Select Case DENTRO
    Case "N"
      If (SiStampa = "Y") Then
        pX1 = (0 - ZeroPezzoX) * ScalaX + PicW / 2 - USCITA.TextWidth(stmp) / 2
        pY1 = -(Raggio - ZeroPezzoY) * ScalaY + PicH / 2 - USCITA.TextHeight(stmp)
        If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          USCITA.CurrentX = pX1
          USCITA.CurrentY = pY1
          USCITA.Print stmp
        End If
      Else
        pX1 = (0 - ZeroPezzoX) * ScalaX + PicW / 2 - USCITA.TextWidth(stmp) / 2
        pY1 = -(Raggio - ZeroPezzoY) * ScalaY + PicH / 2 - USCITA.TextHeight(stmp)
        If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          USCITA.CurrentX = pX1
          USCITA.CurrentY = pY1
          USCITA.Print stmp
        End If
      End If
  
    Case "Y"
      If (SiStampa = "Y") Then
        pX1 = (0 - ZeroPezzoX) * ScalaX + PicW / 2 - USCITA.TextWidth(stmp) / 2
        pY1 = -(0 - ZeroPezzoY) * ScalaY + PicH / 2 - USCITA.TextHeight(stmp) / 2
        If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          USCITA.CurrentX = pX1
          USCITA.CurrentY = pY1
          USCITA.Print stmp
        End If
      Else
        pX1 = (0 - ZeroPezzoX) * ScalaX + PicW / 2 - USCITA.TextWidth(stmp) / 2
        pY1 = -(0 - ZeroPezzoY) * ScalaY + PicH / 2 - USCITA.TextHeight(stmp) / 2
        If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          USCITA.CurrentX = pX1
          USCITA.CurrentY = pY1
          USCITA.Print stmp
        End If
      End If
  End Select
  'Linea sotto il  testo
  pX1 = (0 - ZeroPezzoX) * ScalaX + PicW / 2
  pY1 = -(0 - ZeroPezzoY) * ScalaY + PicH / 2 + USCITA.TextHeight(stmp) / 2
  pX2 = (0 - ZeroPezzoX) * ScalaX + PicW / 2
  pY2 = -(-Raggio - ZeroPezzoY) * ScalaY + PicH / 2
  If (pY2 <= USCITA.ScaleHeight) And (pX2 <= USCITA.ScaleWidth) And (pX2 >= 0) And (pY2 >= 0) Then
    USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
  End If
  'Freccia sotto il testo
  Ang = AF / 180 * PG
  DeltaAng = Ang / NumeroRette
  For i = -NumeroRette To NumeroRette
    pX1 = (0 - ZeroPezzoX) * ScalaX + DF * Sin(i * DeltaAng) + PicW / 2
    pY1 = -(-Raggio - ZeroPezzoY) * ScalaY - DF * Cos(Ang) + PicH / 2
    If (pY2 <= USCITA.ScaleHeight) And (pX2 <= USCITA.ScaleWidth) And (pX2 >= 0) And (pY2 >= 0) Then
      USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
    End If
  Next i
  'Linea di riferimento per la freccia sotto il testo
  pX1 = (0 - ZeroPezzoX) * ScalaX - DF + PicW / 2
  pY1 = -(-Raggio - ZeroPezzoY) * ScalaY + PicH / 2
  pX2 = (0 - ZeroPezzoX) * ScalaX + DF + PicW / 2
  pY2 = -(-Raggio - ZeroPezzoY) * ScalaY + PicH / 2
  If (pY2 <= USCITA.ScaleHeight) And (pX2 <= USCITA.ScaleWidth) And (pX2 >= 0) And (pY2 >= 0) Then
    USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
  End If
Return
          
DISEGNA_DIAMETRO:
    da = 1
    For i = 1 To 360
      pX1 = (Raggio * Sin(i * da * PG / 180) - ZeroPezzoX) * ScalaX + PicW / 2
      pY1 = -(Raggio * Cos(i * da * PG / 180) - ZeroPezzoY) * ScalaY + PicH / 2
      pX2 = (Raggio * Sin((i + 1) * da * PG / 180) - ZeroPezzoX) * ScalaX + PicW / 2
      pY2 = -(Raggio * Cos((i + 1) * da * PG / 180) - ZeroPezzoY) * ScalaY + PicH / 2
      If (pY2 <= USCITA.ScaleHeight) And (pX2 <= USCITA.ScaleWidth) And (pX2 >= 0) And (pY2 >= 0) Then
        USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
      End If
    Next i
Return

errVISUALIZZA_PROFILO_RAB_ROTORI:
  If FreeFile > 0 Then Close
  iERR = Err
  StopRegieEvents
  MsgBox "ERROR: " & Error(iERR), 48, "SUB: VISUALIZZA_PROFILO_RAB_ROTORI "
  ResumeRegieEvents
  Exit Sub
  'Resume Next
  
End Sub

Sub VISUALIZZA_PROFILO_RAB_ESTERNI(ByVal NomeForm As Form, ByVal SiStampa As String, ByVal Scala As Single, ByVal IndiceZeroPezzoX As Integer, ByVal IndiceZeroPezzoY As Integer, ByVal SiPUNTI As Integer, ByVal SiASSI As Integer, ByVal SiASSIALE As Integer)
'
Dim USCITA            As Object
Dim MODALITA          As String
Dim TIPO_LAVORAZIONE  As String
Dim Codice            As String
Dim DBB               As Database
Dim RSS               As Recordset

Dim DENTRO As String
Dim R160 As Double
Dim R161 As Double
Dim R162 As Double
Dim R163 As Double
Dim R164 As Double
Dim R165 As Double
Dim R166 As Double
Dim R167 As Double
Dim R168 As Double
Dim R169 As Double
Dim R170 As Double
Dim R171 As Double
Dim R172 As Double
Dim R173 As Double
Dim R174 As Double

Dim vRaggioMinimo As Double
Dim iRaggioMinimo As Single

Dim Rmax As Double
Dim Rmin As Double

Dim DR  As Double
Dim iDR  As Integer

Dim AngStart  As Double
Dim AngEnd    As Double
Dim Ang       As Double
Dim DeltaAng  As Double
Dim DeltaArco As Double

Dim ftmp     As Double

Dim ScalaX As Double
Dim ScalaY As Double

Dim ZeroPezzoX As Double
Dim ZeroPezzoY As Double

Dim R()  As Double
Dim A()  As Double
Dim B()  As Double

Dim stmp  As String
Dim riga  As String
Dim i     As Integer
Dim j     As Integer
Dim k     As Integer

Dim NDenteLOC As Integer
Dim sNDenteLOC As String

Dim ELICA As Double
Dim sELICA As String

Dim PASSO As Double
Dim sPASSO As String

Dim Np As Integer

Dim NPF1 As Integer
Dim sNpF1 As String

Dim dtmp As Integer

Dim ColoreCurva  As Integer
Dim OldForeColor As Integer

Dim PicW As Single
Dim PicH As Single
            
Dim pX1 As Double
Dim pY1 As Double
Dim pX2 As Double
Dim pY2 As Double
  
Dim da As Single

Dim Raggio As Double

Dim ARCO_ESTERNO As Double

Dim TY_ROT As String
Dim DF As Single
Dim AF As Single
Dim NumeroRette As Integer
Dim NumeroArchi As Integer
Dim TipoCarattere As String

Dim Atmp As String * 255
Dim iERR As Long

On Error GoTo errVISUALIZZA_PROFILO_RAB_ESTERNI
  '
  If UCase$(SiStampa) = "Y" Then
    Set USCITA = Printer
  Else
    Set USCITA = NomeForm.OEM1PicCALCOLO
    USCITA.BackColor = &HFFFFFF
  End If
  NDenteLOC = LEP("R[5]"):  sNDenteLOC = LNP("R[5]") 'NUMERO DENTI
  ELICA = LEP("R[41]"):     sELICA = LNP("R[41]")   'INCLINAZIONE MOLA
  PASSO = LEP("R[390]"):    sPASSO = LNP("R[390]")   'PASSO ASSIALE
  NPF1 = LEP("R[43]"):      sNpF1 = LNP("R[43]")     'PUNTI DEL FIANCO 1
  'LETTURA DEL TIPO DI LAVORAZIONE
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  'NOME ROTORE
  MODALITA = GetInfo("Configurazione", "MODALITA", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))
  If (MODALITA <> "OFF-LINE") Then
    stmp = LEGGI_NomePezzo(TIPO_LAVORAZIONE)
  Else
    stmp = GetInfo(TIPO_LAVORAZIONE, "NomePezzoInserimento", Path_LAVORAZIONE_INI)
  End If
  Codice = stmp
  If Not (USCITA Is Printer) Then
    NomeForm.OEM1frameCalcolo.Caption = "  " & Codice & "  (RAB Profile)"
    NomeForm.OEM1chkINPUT(0).Visible = True
    NomeForm.OEM1chkINPUT(1).Visible = True
    NomeForm.OEM1chkINPUT(2).Visible = True
    NomeForm.OEM1lblOX.Visible = True
    NomeForm.OEM1lblOY.Visible = True
    NomeForm.OEM1txtOX.Visible = True
    NomeForm.OEM1txtOY.Visible = True
    NomeForm.lblMANDRINI.Visible = False
    NomeForm.McCombo1.Visible = False
    i = LoadString(g_hLanguageLibHandle, 1000, Atmp, 255)
    If i > 0 Then NomeForm.lblINPUT.Caption = Left$(Atmp, i)
    NomeForm.txtINPUT.Text = frmt(Scala, 4)
    If i > 0 Then NomeForm.lblINP(0).Caption = Left$(Atmp, i)
    NomeForm.txtINP(0).Text = frmt(Scala, 4)
  End If
  'AREA GRAFICA
  stmp = GetInfo("DIS0", "PicW", Path_LAVORAZIONE_INI)
  If (Len(stmp) > 0) Then PicW = val(stmp) Else PicW = 190
  stmp = GetInfo("DIS0", "PicH", Path_LAVORAZIONE_INI)
  If (Len(stmp) > 0) Then PicH = val(stmp) Else PicH = 95
  'TIPOLOGIA DI RAPPRESENTAZIONE
  If SiASSIALE Then TY_ROT = "AXIAL" Else TY_ROT = "FRONT"
  '
  DF = val(GetInfo("DIS0", "DF", Path_LAVORAZIONE_INI))
  AF = val(GetInfo("DIS0", "AF", Path_LAVORAZIONE_INI))
  NumeroRette = val(GetInfo("DIS0", "NumeroRette", Path_LAVORAZIONE_INI))
  NumeroArchi = val(GetInfo("DIS0", "NumeroArchi", Path_LAVORAZIONE_INI))
  '
  If DF = 0 Then DF = 2
  If AF = 0 Then AF = 30
  If NumeroRette = 0 Then NumeroRette = 20
  If NumeroArchi = 0 Then NumeroArchi = 50
  '
  TipoCarattere = GetInfo("DIS0", "TextFont", Path_LAVORAZIONE_INI)
  If (LINGUA = "CH") Then TipoCarattere = "MS Song"
  If (LINGUA = "RU") Then TipoCarattere = "Arial Cyr"
  If (Len(TipoCarattere) <= 0) Then TipoCarattere = "Arial"
  '
  If (SiStampa = "Y") Then
    For i = 1 To 2
      USCITA.FontName = TipoCarattere
      USCITA.FontBold = False
      USCITA.FontSize = 10
    Next i
    WRITE_DIALOG USCITA.DeviceName & " PRINTING....."
    If PicW = 190 And PicH = 95 Then
      USCITA.Orientation = 1
    Else
      USCITA.Orientation = 2
    End If
    USCITA.ScaleMode = 6
  Else
    USCITA.Cls
    USCITA.ScaleWidth = PicW
    USCITA.ScaleHeight = PicH
    USCITA.FontName = TipoCarattere
    USCITA.FontBold = False
    USCITA.FontSize = 8
    USCITA.Refresh
  End If
  Rmax = -999999: Rmin = 999999
  Call ROTORI1.LETTURA_PARPROFILO_RAB(Np, R(), A(), B(), Rmin, Rmax)
  '
  If (Scala = 0) Then
    Scala = PicH / (2 * Rmax * 1.15)
    If (NomeForm.Name = "OEM0") Then NomeForm.txtINPUT.Text = frmt(Scala, 4)
    If (NomeForm.Name = "OEM1") Or (NomeForm.Name = "OEMX") Then NomeForm.txtINP(0).Text = frmt(Scala, 4)
  End If
  '
  ScalaX = Scala: ScalaY = Scala
  '
  If (TY_ROT = "AXIAL") Then
    If (IndiceZeroPezzoX >= 1) And (IndiceZeroPezzoX <= Np) Then
      'Profilo AZZERATO SU UN PUNTO DEL PROFILO STESSO
      ZeroPezzoX = A(IndiceZeroPezzoX) / 360 * PASSO
      NDenteLOC = 1
    Else
      'Profilo centrato
      ZeroPezzoX = (NDenteLOC - 1) * (A(Np) / 360 * PASSO)
    End If
    If (IndiceZeroPezzoY >= 1) And (IndiceZeroPezzoY <= Np) Then
      'Profilo AZZERATO SU UN PUNTO DEL PROFILO STESSO
      ZeroPezzoY = R(IndiceZeroPezzoY)
      NDenteLOC = 1
    Else
      'Profilo centrato
      ZeroPezzoY = Rmin
    End If
  Else
    If (IndiceZeroPezzoX >= 1) And (IndiceZeroPezzoX <= Np) Then
      'Profilo AZZERATO SU UN PUNTO DEL PROFILO STESSO
      ZeroPezzoX = R(IndiceZeroPezzoX) * Sin((A(IndiceZeroPezzoX)) * PG / 180)
      NDenteLOC = 1
    Else
      ZeroPezzoX = 0
    End If
    If (IndiceZeroPezzoY >= 1) And (IndiceZeroPezzoY <= Np) Then
      'Profilo AZZERATO SU DI UN PUNTO DEL PROFILO STESSO
      ZeroPezzoY = R(IndiceZeroPezzoY) * Cos((A(IndiceZeroPezzoY)) * PG / 180)
      NDenteLOC = 1
    Else
      'Profilo centrato
      If NDenteLOC = 1 Then ZeroPezzoY = Rmin Else ZeroPezzoY = 0
    End If
  End If
  '
  stmp = "RAB code:  " & Codice
  USCITA.CurrentX = PicW / 2 - USCITA.TextWidth(stmp) / 2
  USCITA.CurrentY = PicH - USCITA.TextHeight(stmp)
  USCITA.Print stmp
  '
  If (SiASSI = 1) Then Call SCRIVI_ASSI(USCITA, SiStampa)
  '
  'Calcolo dr
  DR = 999999: iDR = 1
  For i = 1 To Np - 1
    pX1 = (R(i) * Sin((A(i) + da) * PG / 180))
    pY1 = -(R(i) * Cos((A(i) + da) * PG / 180))
    pX2 = (R(i + 1) * Sin((A(i + 1) + da) * PG / 180))
    pY2 = -(R(i + 1) * Cos((A(i + 1) + da) * PG / 180))
    ftmp = Sqr((pX2 - pX1) ^ 2 + (pY2 - pY1) ^ 2)
    If ftmp < DR Then DR = ftmp: iDR = i
  Next i
  If (DR = 0) And Abs(B(iDR) - B(iDR + 1)) < 0.01 Then
    stmp = "Duplicate point into RAB profile."
    StopRegieEvents
    MsgBox stmp, vbCritical, "WARNING: point " & Format$(iDR, "####0")
    ResumeRegieEvents
  End If
  '
  Call SCRIVI_SCALA_VERTICALE(ScalaY, USCITA, SiStampa)
  Call SCRIVI_SCALA_ORIZZONTALE(ScalaX, USCITA, SiStampa)
  '
  'CURVA CON ARCHI DI CERCHIO
  ColoreCurva = 9: GoSub VISUALIZZA_RAB_CERCHI
  '
  'Call CREAZIONE_FILE_PROFILO_ROTORE_DXF(Np, R, a, PASSO, "AXIAL")
  'Call CREAZIONE_FILE_PROFILO_ROTORE_DXF(Np, R, a, PASSO, "FRONT")
  '
  If (TY_ROT <> "AXIAL") Then
    ColoreCurva = 12
    DENTRO = "N": Raggio = Rmax: GoSub SCRIVI_DIAMETRO: GoSub DISEGNA_DIAMETRO
    ColoreCurva = 9  '0
    DENTRO = "Y": Raggio = Rmin: GoSub SCRIVI_DIAMETRO: GoSub DISEGNA_DIAMETRO
  Else
    ColoreCurva = 12
    DENTRO = "N": Raggio = Rmax: GoSub SCRIVI_DIAMETRO
    ColoreCurva = 9  '0
    DENTRO = "Y": Raggio = Rmin: GoSub SCRIVI_DIAMETRO
  End If
  '
  'NOME DEL FILE DEL PROFILO E NUMERO DEI PUNTI
  stmp = "" '" " & NomeRotore
  stmp = stmp & " (Np=F1+F2="
  stmp = stmp & Format$(NPF1, "#######0") & "+" & Format$(Np - NPF1, "#######0")
  stmp = stmp & "=" & Format$(Np, "#######0") & ")"
  USCITA.CurrentX = 0
  USCITA.CurrentY = USCITA.TextHeight(stmp) * 0
  USCITA.Print stmp
  'DISTANZA MININA TRA I PUNTI DEL PROFILO
  stmp = " DR=" & frmt(DR, 8) & "mm /" & Format$(iDR, "########0")
  USCITA.CurrentX = 0
  USCITA.CurrentY = USCITA.TextHeight(stmp) * 1
  USCITA.Print stmp
  'ALTEZZA DENTE
  stmp = " HD=" & frmt(Rmax - Rmin, 8) & "mm"
  USCITA.CurrentX = 0
  USCITA.CurrentY = USCITA.TextHeight(stmp) * 2
  USCITA.Print stmp
  'RAGGIO DI CURVATURA MINIMO
  stmp = " RPmin= " & frmt(vRaggioMinimo, 4) & "mm "
  stmp = stmp & "/" & frmt(iRaggioMinimo, 4)
  USCITA.CurrentX = 0
  USCITA.CurrentY = PicH / 2
  USCITA.Print stmp
  'ARCO SUL DIAMETRO ESTERNO DEL ROTORE TEORICO
  If (NDenteLOC >= 1) Then
    If (TY_ROT = "AXIAL") Then
      ARCO_ESTERNO = PASSO / NDenteLOC - (A(Np) - A(1)) / 360 * PASSO
      stmp = " Seg= " & frmt(ARCO_ESTERNO, 4) & "mm "
    Else
      ARCO_ESTERNO = (2 * PG / NDenteLOC - (Abs(A(1)) + Abs(A(Np))) * PG / 180) * Rmax
      stmp = " Arc= " & frmt(ARCO_ESTERNO, 4) & "mm "
    End If
    USCITA.CurrentX = PicW - USCITA.TextWidth(stmp)
    USCITA.CurrentY = PicH / 2
    USCITA.Print stmp
  End If
  'PUNTI DEL PROFILO ROTORE
  If SiPUNTI = 1 Then GoSub VISUALIZZA_PUNTI
  'Segnalazione della fine delle operazioni
  If (SiStampa = "Y") Then USCITA.EndDoc
  WRITE_DIALOG ""
  '
Exit Sub

VISUALIZZA_RAB_CERCHI:
  For k = 0 To NDenteLOC - 1
    vRaggioMinimo = 1E+38
    iRaggioMinimo = 1
    da = (360 / NDenteLOC) * k
    For i = 1 To Np - 2 Step 2
      If (TY_ROT = "AXIAL") Then
        'COORD. X        COORD. Y
        R164 = (A(i + 0) + da) / 360 * PASSO: R165 = R(i + 0)
        R166 = (A(i + 1) + da) / 360 * PASSO: R167 = R(i + 1)
        R168 = (A(i + 2) + da) / 360 * PASSO: R169 = R(i + 2)
      Else
        'COORD. X        COORD. Y
        R164 = R(i + 0) * Sin((A(i + 0) + da) * PG / 180): R165 = R(i + 0) * Cos((A(i + 0) + da) * PG / 180)
        R166 = R(i + 1) * Sin((A(i + 1) + da) * PG / 180): R167 = R(i + 1) * Cos((A(i + 1) + da) * PG / 180)
        R168 = R(i + 2) * Sin((A(i + 2) + da) * PG / 180): R169 = R(i + 2) * Cos((A(i + 2) + da) * PG / 180)
      End If
      'DISTANZA PER TRE PUNTI
      ftmp = Sqr((R165 - R169) * (R165 - R169) + (R164 - R168) * (R164 - R168))
      If (ftmp < 0.0001) Then ftmp = 0.0001
      R162 = Abs((R165 - R169) * (R164 - R166) + (R167 - R165) * (R164 - R168))
      R162 = R162 * (1 / ftmp)
      If (R162 < 0.0005) Then
        'TRATTO RETTILINEO
        'G1 Y=R169 X=R168
        pX1 = (R164 - ZeroPezzoX) * ScalaX + PicW / 2
        pY1 = -(R165 - ZeroPezzoY) * ScalaY + PicH / 2
        pX2 = (R168 - ZeroPezzoX) * ScalaX + PicW / 2
        pY2 = -(R169 - ZeroPezzoY) * ScalaY + PicH / 2
        If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
        End If
      Else
        'TRATTO CURVILINEO
        If (R169 = R167) Then R169 = R167 + 0.0001
        If (R165 = R167) Then R165 = R167 + 0.0001
        R160 = (R164 - R166) / (R167 - R165)
        R171 = (R166 - R168) / (R169 - R167)
        R172 = (R165 + R167) / 2 - R160 * (R164 + R166) / 2
        R173 = (R169 + R167) / 2 - R171 * (R168 + R166) / 2
        If (R160 = R171) Then R160 = R171 + 0.0001
        'Coordinate del centro
        R174 = (R173 - R172) / (R160 - R171)
        R170 = R160 * R174 + R172
        'Raggio del cerchio
        R161 = Sqr((R174 - R166) * (R174 - R166) + (R170 - R167) * (R170 - R167))
        If (R161 <= vRaggioMinimo) Then vRaggioMinimo = R161: iRaggioMinimo = i
        If NDenteLOC > 1 Then
          'TRATTO RETTILINEO
          'G1 Y=R166 X=R167
          pX1 = (R164 - ZeroPezzoX) * ScalaX + PicW / 2
          pY1 = -(R165 - ZeroPezzoY) * ScalaY + PicH / 2
          pX2 = (R166 - ZeroPezzoX) * ScalaX + PicW / 2
          pY2 = -(R167 - ZeroPezzoY) * ScalaY + PicH / 2
          If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
            USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
          End If
          'TRATTO RETTILINEO
          'G1 Y=R168 X=R169
          pX1 = (R166 - ZeroPezzoX) * ScalaX + PicW / 2
          pY1 = -(R167 - ZeroPezzoY) * ScalaY + PicH / 2
          pX2 = (R168 - ZeroPezzoX) * ScalaX + PicW / 2
          pY2 = -(R169 - ZeroPezzoY) * ScalaY + PicH / 2
          If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
            USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
          End If
        Else
          'Prodotto misto per senso di rotazione
          R160 = (R166 - R164) * (R169 - R165) - (R168 - R164) * (R167 - R165)
          'Calcolo angoli compresi tra 0 e 2 * Pigreco. --------------------------
          AngStart = CALCOLO_ANGOLO(R164 - R174, R165 - R170)
          AngEnd = CALCOLO_ANGOLO(R168 - R174, R169 - R170)
          '-----------------------------------------------------------------------
          If (R160 > 0) Then
            'G3 Y=R169 X=R168 CR=R161
            If AngEnd >= AngStart Then
              Ang = AngEnd - AngStart
            Else
              Ang = 2 * PG - (AngStart - AngEnd)
            End If
            DeltaAng = Ang / NumeroArchi
            pX1 = R164: pY1 = R165
            For j = 1 To NumeroArchi - 1
              Ang = (AngStart + j * DeltaAng)
              pX2 = R174 + R161 * Cos(Ang): pY2 = R170 + R161 * Sin(Ang)
              'Adattamento nella finestra grafica
              pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
              pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
              pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
              pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
              'Traccio la linea
              If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
                USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
              End If
              'Aggiorno Px1 e Py1
              pX1 = R174 + R161 * Cos(Ang): pY1 = R170 + R161 * Sin(Ang)
            Next j
            pX2 = R168: pY2 = R169
            'Adattamento nella finestra grafica
            pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
            pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
            pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
            pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
            'Traccio la linea
            If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
              USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
            End If
          Else
            'G2 Y=R169 X=R168 CR=R161
            If AngStart >= AngEnd Then
              Ang = AngStart - AngEnd
            Else
              Ang = 2 * PG - (AngEnd - AngStart)
            End If
            DeltaAng = Ang / NumeroArchi
            pX1 = R164: pY1 = R165
            For j = 1 To NumeroArchi - 1
              Ang = (AngStart - j * DeltaAng)
              pX2 = R174 + R161 * Cos(Ang): pY2 = R170 + R161 * Sin(Ang)
              'Adattamento nella finestra grafica
              pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
              pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
              pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
              pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
              'Traccio la linea
              If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
                USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
              End If
              'Aggiorno Px1 e Py1
              pX1 = R174 + R161 * Cos(Ang): pY1 = R170 + R161 * Sin(Ang)
            Next j
            pX2 = R168: pY2 = R169
            'Adattamento nella finestra grafica
            pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
            pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
            pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
            pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
            'Traccio la linea
            If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
              USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
            End If
          End If
        End If
      End If
    Next i
  Next k
Return

VISUALIZZA_PUNTI:
Dim SiINDICI As Integer
  '
  If (SiStampa = "Y") Then
    StopRegieEvents
    j = MsgBox("Indeces ? ", 32 + 4, "WARNING")
    ResumeRegieEvents
    If j = 6 Then SiINDICI = 1 Else SiINDICI = 0
  End If
  'For j = 0 To NDenteloc - 1
  j = 0
  da = (360 / NDenteLOC) * j
    For i = 1 To NPF1
      'LINEA TANGENTE
      If (TY_ROT = "AXIAL") Then
        Ang = Atn(Tan(PG * B(i) / 180) * PASSO / (2 * R(i) * PG))
        pX1 = A(i) / 360 * PASSO - ZeroPezzoX
        pY1 = R(i) - ZeroPezzoY
      Else
        Ang = (A(i) + B(i)) * PG / 180
        pX1 = R(i) * Sin((A(i) + da) * PG / 180) - ZeroPezzoX
        pY1 = R(i) * Cos((A(i) + da) * PG / 180) - ZeroPezzoY
      End If
      pX2 = pX1
      pY2 = pY1
      pX1 = (pX1 + DR * Sin(Ang)) * ScalaX + PicW / 2
      pY1 = -(pY1 + DR * Cos(Ang)) * ScalaY + PicH / 2
      pX2 = (pX2 + DR * Sin(Ang - PG)) * ScalaX + PicW / 2
      pY2 = -(pY2 + DR * Cos(Ang - PG)) * ScalaY + PicH / 2
      If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
        USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
      End If
      'LINEA NORMALE
      If (TY_ROT = "AXIAL") Then
        Ang = Atn(Tan(PG * B(i) / 180) * PASSO / (2 * R(i) * PG))
        pX1 = A(i) / 360 * PASSO - ZeroPezzoX
        pY1 = R(i) - ZeroPezzoY
      Else
        Ang = (A(i) + B(i)) * PG / 180
        pX1 = R(i) * Sin((A(i) + da) * PG / 180) - ZeroPezzoX
        pY1 = R(i) * Cos((A(i) + da) * PG / 180) - ZeroPezzoY
      End If
      pX2 = pX1
      pY2 = pY1
      pX1 = (pX1 + DR * Sin(Ang - PG / 2)) * ScalaX + PicW / 2
      pY1 = -(pY1 + DR * Cos(Ang - PG / 2)) * ScalaY + PicH / 2
      pX2 = (pX2 + DR * Sin(Ang - PG - PG / 2)) * ScalaX + PicW / 2
      pY2 = -(pY2 + DR * Cos(Ang - PG - PG / 2)) * ScalaY + PicH / 2
      'pX2 = pX2 * ScalaX + PicW / 2
      'pY2 = -pY2 * ScalaY + PicH / 2
      If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
        USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
      End If
      'indice del punto
      stmp = " " & Format$(i, "##0") & " "
      If SiINDICI = 1 Then
        pX1 = pX1 - USCITA.TextWidth(stmp)
        pY1 = pY1 - USCITA.TextHeight(stmp)
        If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          USCITA.CurrentX = pX1
          USCITA.CurrentY = pY1
          USCITA.Print stmp
        End If
      Else
        pX1 = pX1 - USCITA.TextWidth(stmp)
        pY1 = pY1 - USCITA.TextHeight(stmp)
        If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          USCITA.CurrentX = pX1
          USCITA.CurrentY = pY1
          USCITA.Print stmp
        End If
      End If
      'CERCHIO
      If (TY_ROT = "AXIAL") Then
        R174 = A(i) / 360 * PASSO - ZeroPezzoX
        R170 = R(i) - ZeroPezzoY
      Else
        R174 = R(i) * Sin((A(i) + da) * PG / 180) - ZeroPezzoX
        R170 = R(i) * Cos((A(i) + da) * PG / 180) - ZeroPezzoY
      End If
      Raggio = DR
      DeltaAng = 2 * PG / NumeroArchi
      pX1 = R174 + Raggio: pY1 = R170
      For k = 1 To NumeroArchi
        Ang = k * DeltaAng
        pX2 = R174 + Raggio * Cos(Ang): pY2 = R170 + Raggio * Sin(Ang)
        'Adattamento nella finestra grafica
        pX1 = pX1 * ScalaX + PicW / 2
        pY1 = -pY1 * ScalaY + PicH / 2
        pX2 = pX2 * ScalaX + PicW / 2
        pY2 = -pY2 * ScalaY + PicH / 2
        'Traccio la linea
        If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
        End If
        'Aggiorno Px1 e Py1
        pX1 = R174 + Raggio * Cos(Ang): pY1 = R170 + Raggio * Sin(Ang)
      Next k
    Next i
    For i = NPF1 + 1 To Np
      'LINEA TANGENTE
      If (TY_ROT = "AXIAL") Then
        Ang = Atn(Tan(PG * B(i) / 180) * PASSO / (2 * R(i) * PG))
        pX1 = A(i) / 360 * PASSO - ZeroPezzoX
        pY1 = R(i) - ZeroPezzoY
      Else
        Ang = (A(i) + B(i)) * PG / 180
        pX1 = R(i) * Sin((A(i) + da) * PG / 180) - ZeroPezzoX
        pY1 = R(i) * Cos((A(i) + da) * PG / 180) - ZeroPezzoY
      End If
      pX2 = pX1
      pY2 = pY1
      pX1 = (pX1 + DR * Sin(Ang)) * ScalaX + PicW / 2
      pY1 = -(pY1 + DR * Cos(Ang)) * ScalaY + PicH / 2
      pX2 = (pX2 + DR * Sin(Ang - PG)) * ScalaX + PicW / 2
      pY2 = -(pY2 + DR * Cos(Ang - PG)) * ScalaY + PicH / 2
      If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
        USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
      End If
      'LINEA NORMALE
      If (TY_ROT = "AXIAL") Then
        Ang = Atn(Tan(PG * B(i) / 180) * PASSO / (2 * R(i) * PG))
        pX1 = A(i) / 360 * PASSO - ZeroPezzoX
        pY1 = R(i) - ZeroPezzoY
      Else
        Ang = (A(i) + B(i)) * PG / 180
        pX1 = R(i) * Sin((A(i) + da) * PG / 180) - ZeroPezzoX
        pY1 = R(i) * Cos((A(i) + da) * PG / 180) - ZeroPezzoY
      End If
      pX2 = pX1
      pY2 = pY1
      pX1 = (pX1 + DR * Sin(Ang - PG / 2)) * ScalaX + PicW / 2
      pY1 = -(pY1 + DR * Cos(Ang - PG / 2)) * ScalaY + PicH / 2
      pX2 = (pX2 + DR * Sin(Ang - PG - PG / 2)) * ScalaX + PicW / 2
      pY2 = -(pY2 + DR * Cos(Ang - PG - PG / 2)) * ScalaY + PicH / 2
      'pX2 = pX2 * ScalaX + PicW / 2
      'pY2 = -pY2 * ScalaY + PicH / 2
      If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
        USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
      End If
      'indice del punto
      stmp = " " & Format$(i, "##0") & " "
      If SiINDICI = 1 Then
        If (pY2 <= USCITA.ScaleHeight) And (pX2 <= USCITA.ScaleWidth) And (pX2 >= 0) And (pY2 >= 0) Then
          USCITA.CurrentX = pX2
          USCITA.CurrentY = pY2
          USCITA.Print stmp
        End If
      Else
        If (pY2 <= USCITA.ScaleHeight) And (pX2 <= USCITA.ScaleWidth) And (pX2 >= 0) And (pY2 >= 0) Then
          USCITA.CurrentX = pX2
          USCITA.CurrentY = pY2
          USCITA.Print stmp
        End If
      End If
      'CERCHIO
      If (TY_ROT = "AXIAL") Then
        R174 = A(i) / 360 * PASSO - ZeroPezzoX
        R170 = R(i) - ZeroPezzoY
      Else
        R174 = R(i) * Sin((A(i) + da) * PG / 180) - ZeroPezzoX
        R170 = R(i) * Cos((A(i) + da) * PG / 180) - ZeroPezzoY
      End If
      Raggio = DR
      DeltaAng = 2 * PG / NumeroArchi
      pX1 = R174 + Raggio: pY1 = R170
      For k = 1 To NumeroArchi
        Ang = k * DeltaAng
        pX2 = R174 + Raggio * Cos(Ang): pY2 = R170 + Raggio * Sin(Ang)
        'Adattamento nella finestra grafica
        pX1 = pX1 * ScalaX + PicW / 2
        pY1 = -pY1 * ScalaY + PicH / 2
        pX2 = pX2 * ScalaX + PicW / 2
        pY2 = -pY2 * ScalaY + PicH / 2
        'Traccio la linea
        If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
        End If
        'Aggiorno Px1 e Py1
        pX1 = R174 + Raggio * Cos(Ang): pY1 = R170 + Raggio * Sin(Ang)
      Next k
    Next i
  'Next j
Return
     
SCRIVI_DIAMETRO:
  'Testo da stampare
  stmp = " " & frmt(2 * Raggio, 4) & "mm "
  'Linea sopra il del testo
  pX1 = (0 - ZeroPezzoX) * ScalaX + PicW / 2
  pY1 = -(Raggio - ZeroPezzoY) * ScalaY + PicH / 2
  pX2 = (0 - ZeroPezzoX) * ScalaX + PicW / 2
  pY2 = -(0 - ZeroPezzoY) * ScalaY + PicH / 2 - USCITA.TextHeight(stmp) / 2
  If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
    USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
  End If
  'Freccia sopra il testo
  Ang = AF / 180 * PG
  DeltaAng = Ang / NumeroRette
  For i = -NumeroRette To NumeroRette
    pX2 = (0 - ZeroPezzoX) * ScalaX + DF * Sin(i * DeltaAng) + PicW / 2
    pY2 = -(Raggio - ZeroPezzoY) * ScalaY + DF * Cos(Ang) + PicH / 2
    If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
      USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
    End If
  Next i
  'Linea di riferimento per la freccia sopra al testo
  pX1 = (0 - ZeroPezzoX) * ScalaX - DF + PicW / 2
  pY1 = -(Raggio - ZeroPezzoY) * ScalaY + PicH / 2
  pX2 = (0 - ZeroPezzoX) * ScalaX + DF + PicW / 2
  pY2 = -(Raggio - ZeroPezzoY) * ScalaY + PicH / 2
  If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
    USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
  End If
  'Scrittura del testo
  Select Case DENTRO
    Case "N"
      If (SiStampa = "Y") Then
        pX1 = (0 - ZeroPezzoX) * ScalaX + PicW / 2 - USCITA.TextWidth(stmp) / 2
        pY1 = -(Raggio - ZeroPezzoY) * ScalaY + PicH / 2 - USCITA.TextHeight(stmp)
        If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          USCITA.CurrentX = pX1
          USCITA.CurrentY = pY1
          USCITA.Print stmp
        End If
      Else
        pX1 = (0 - ZeroPezzoX) * ScalaX + PicW / 2 - USCITA.TextWidth(stmp) / 2
        pY1 = -(Raggio - ZeroPezzoY) * ScalaY + PicH / 2 - USCITA.TextHeight(stmp)
        If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          USCITA.CurrentX = pX1
          USCITA.CurrentY = pY1
          USCITA.Print stmp
        End If
      End If
  
    Case "Y"
      If (SiStampa = "Y") Then
        pX1 = (0 - ZeroPezzoX) * ScalaX + PicW / 2 - USCITA.TextWidth(stmp) / 2
        pY1 = -(0 - ZeroPezzoY) * ScalaY + PicH / 2 - USCITA.TextHeight(stmp) / 2
        If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          USCITA.CurrentX = pX1
          USCITA.CurrentY = pY1
          USCITA.Print stmp
        End If
      Else
        pX1 = (0 - ZeroPezzoX) * ScalaX + PicW / 2 - USCITA.TextWidth(stmp) / 2
        pY1 = -(0 - ZeroPezzoY) * ScalaY + PicH / 2 - USCITA.TextHeight(stmp) / 2
        If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          USCITA.CurrentX = pX1
          USCITA.CurrentY = pY1
          USCITA.Print stmp
        End If
      End If
  End Select
  'Linea sotto il  testo
  pX1 = (0 - ZeroPezzoX) * ScalaX + PicW / 2
  pY1 = -(0 - ZeroPezzoY) * ScalaY + PicH / 2 + USCITA.TextHeight(stmp) / 2
  pX2 = (0 - ZeroPezzoX) * ScalaX + PicW / 2
  pY2 = -(-Raggio - ZeroPezzoY) * ScalaY + PicH / 2
  If (pY2 <= USCITA.ScaleHeight) And (pX2 <= USCITA.ScaleWidth) And (pX2 >= 0) And (pY2 >= 0) Then
    USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
  End If
  'Freccia sotto il testo
  Ang = AF / 180 * PG
  DeltaAng = Ang / NumeroRette
  For i = -NumeroRette To NumeroRette
    pX1 = (0 - ZeroPezzoX) * ScalaX + DF * Sin(i * DeltaAng) + PicW / 2
    pY1 = -(-Raggio - ZeroPezzoY) * ScalaY - DF * Cos(Ang) + PicH / 2
    If (pY2 <= USCITA.ScaleHeight) And (pX2 <= USCITA.ScaleWidth) And (pX2 >= 0) And (pY2 >= 0) Then
      USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
    End If
  Next i
  'Linea di riferimento per la freccia sotto il testo
  pX1 = (0 - ZeroPezzoX) * ScalaX - DF + PicW / 2
  pY1 = -(-Raggio - ZeroPezzoY) * ScalaY + PicH / 2
  pX2 = (0 - ZeroPezzoX) * ScalaX + DF + PicW / 2
  pY2 = -(-Raggio - ZeroPezzoY) * ScalaY + PicH / 2
  If (pY2 <= USCITA.ScaleHeight) And (pX2 <= USCITA.ScaleWidth) And (pX2 >= 0) And (pY2 >= 0) Then
    USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
  End If
Return
          
DISEGNA_DIAMETRO:
    da = 1
    For i = 1 To 360
      pX1 = (Raggio * Sin(i * da * PG / 180) - ZeroPezzoX) * ScalaX + PicW / 2
      pY1 = -(Raggio * Cos(i * da * PG / 180) - ZeroPezzoY) * ScalaY + PicH / 2
      pX2 = (Raggio * Sin((i + 1) * da * PG / 180) - ZeroPezzoX) * ScalaX + PicW / 2
      pY2 = -(Raggio * Cos((i + 1) * da * PG / 180) - ZeroPezzoY) * ScalaY + PicH / 2
      If (pY2 <= USCITA.ScaleHeight) And (pX2 <= USCITA.ScaleWidth) And (pX2 >= 0) And (pY2 >= 0) Then
        USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
      End If
    Next i
Return

errVISUALIZZA_PROFILO_RAB_ESTERNI:
  If FreeFile > 0 Then Close
  iERR = Err
  StopRegieEvents
  MsgBox "ERROR: " & Error(iERR), 48, "SUB: VISUALIZZA_PROFILO_RAB_ESTERNI "
  ResumeRegieEvents
  Exit Sub
  'Resume Next
  
End Sub

Sub CREAZIONE_FILE_PROFILO_MOLA_DXF(ByVal Np As Integer, ByRef XX() As Double, ByRef YY() As Double, ByRef TT() As Double, ByVal NPF1 As Integer)
'
Dim R160 As Double, R161 As Double, R162 As Double
Dim R163 As Double, R164 As Double, R165 As Double
Dim R166 As Double, R167 As Double, R168 As Double
Dim R169 As Double
Dim R170 As Double, R171 As Double, R172 As Double
Dim R173 As Double, R174 As Double
Dim R180 As Double, R184 As Double
'
Dim AngStart As Double
Dim AngEnd   As Double
'
Dim Altezza(2)    As Double
Dim TIPO_MACCHINA As String
'
Dim XX_F1(99) As Double
Dim YY_F1(99) As Double
Dim XX_F2(99) As Double
Dim YY_F2(99) As Double
Dim LL_F1     As Double
Dim LL_F2     As Double
Dim RR_F1     As Double
Dim RR_F2     As Double
Dim IUPP_F1   As Integer
Dim IFIN_F1   As Integer
Dim IUPP_F2   As Integer
Dim IFIN_F2   As Integer
Dim MODALITA   As String
Dim NomeROTORE As String
'
Const CIFRE_DXF = 20
'
On Error GoTo errCREAZIONE_FILE_PROFILO_MOLA_DXF
  '
  Altezza(1) = YY(NPF1) - YY(0)
  Altezza(2) = YY(NPF1) - YY(Np + 1)
  If (Altezza(1) > Altezza(2)) Then
    Altezza(0) = Altezza(1)
  Else
    Altezza(0) = Altezza(2)
  End If
  TIPO_MACCHINA = GetInfo("Configurazione", "TIPO_MACCHINA", Path_LAVORAZIONE_INI)
  TIPO_MACCHINA = UCase$(Trim$(TIPO_MACCHINA))
  MODALITA = GetInfo("Configurazione", "MODALITA", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))
  If (MODALITA <> "OFF-LINE") Then
    NomeROTORE = LEGGI_NomePezzo("ROTORI_ESTERNI")
  Else
    NomeROTORE = GetInfo("ROTORI_ESTERNI", "NomePezzoInserimento", Path_LAVORAZIONE_INI)
  End If
  Open g_chOemPATH & "\WHEEL_" & NomeROTORE & ".DXF" For Output As #3
    'INIZIO FILE
    Print #3, "0"
    Print #3, "SECTION"
    Print #3, "2"
    Print #3, "ENTITIES"
    i = 0
      'TRATTO INIZIALE
      Print #3, "0"
      Print #3, "LINE"
      Print #3, "8"
      Print #3, "1"
      Print #3, "62"
      Print #3, "1"
      'PRIMO PUNTO
      Print #3, "10"
      Print #3, frmt(XX(i), CIFRE_DXF)
      Print #3, "20"
      Print #3, frmt(YY(i), CIFRE_DXF)
      Print #3, "30"
      Print #3, "0.0"
      'SECONDO PUNTO
      i = i + 1
      Print #3, "11"
      Print #3, frmt(XX(i), CIFRE_DXF)
      Print #3, "21"
      Print #3, frmt(YY(i), CIFRE_DXF)
      Print #3, "31"
      Print #3, "0.0"
    'ARCHI DI CERCHIO
    For i = 1 To NPF1 - 2 Step 2
      'COORD. X        COORD. Y
      R164 = XX(i + 0): R165 = YY(i + 0)
      R166 = XX(i + 1): R167 = YY(i + 1)
      R168 = XX(i + 2): R169 = YY(i + 2)
      GoSub SCRIVI_RIGA_DXF
    Next
  If (TIPO_MACCHINA = "G500HL-SEIM") Then
    'FIANCO 1
    IUPP_F1 = LEP("COR[0,410]")
      RR_F1 = LEP("COR[0,411]")
      LL_F1 = LEP("COR[0,412]")
    If (IUPP_F1 > 0) And (RR_F1 > 0) And (LL_F1 > 0) Then
      IFIN_F1 = LEP("COR[0,413]")
      If (IFIN_F1 <= IUPP_F1) Then IFIN_F1 = IUPP_F1 + 1
      'FINE PROLUNGAMENTO
      XX_F1(1) = XX(IUPP_F1) + LL_F1 * Sin(Abs(TT(IUPP_F1)))
      YY_F1(1) = YY(IUPP_F1) + LL_F1 * Cos(Abs(TT(IUPP_F1)))
      'TRATTO RETTILINEO
      Print #3, "0"
      Print #3, "LINE"
      Print #3, "8"
      Print #3, "1"
      Print #3, "62"
      Print #3, "1"
      'PRIMO PUNTO
      Print #3, "10"
      Print #3, frmt(XX(IUPP_F1), CIFRE_DXF)
      Print #3, "20"
      Print #3, frmt(YY(IUPP_F1), CIFRE_DXF)
      Print #3, "30"
      Print #3, "0.0"
      'SECONDO PUNTO
      Print #3, "11"
      Print #3, frmt(XX_F1(1), CIFRE_DXF)
      Print #3, "21"
      Print #3, frmt(YY_F1(1), CIFRE_DXF)
      Print #3, "31"
      Print #3, "0.0"
      'PUNTO INTERMEDIO DEL PROLUNGAMENTO
      XX_F1(11) = (XX(IUPP_F1) + XX_F1(1)) / 2
      YY_F1(11) = (YY(IUPP_F1) + YY_F1(1)) / 2
      'CENTRO DEL CERCHIO DI RACCORDO
      XX_F1(0) = XX_F1(1) - RR_F1 * Sin(PG / 2 - Abs(TT(IUPP_F1)))
      YY_F1(0) = YY_F1(1) + RR_F1 * Cos(PG / 2 - Abs(TT(IUPP_F1)))
      'PUNTO DI TANGENZA
      XX_F1(2) = XX_F1(0) + RR_F1 * Cos(Abs(TT(IFIN_F1)))
      YY_F1(2) = YY_F1(0) - RR_F1 * Sin(Abs(TT(IFIN_F1)))
      'TRATTO RETTILINEO
      Print #3, "0"
      Print #3, "LINE"
      Print #3, "8"
      Print #3, "1"
      Print #3, "62"
      Print #3, "1"
      'PRIMO PUNTO
      Print #3, "10"
      Print #3, frmt(XX(IFIN_F1), CIFRE_DXF)
      Print #3, "20"
      Print #3, frmt(YY(IFIN_F1), CIFRE_DXF)
      Print #3, "30"
      Print #3, "0.0"
      'SECONDO PUNTO
      Print #3, "11"
      Print #3, frmt(XX_F1(2), CIFRE_DXF)
      Print #3, "21"
      Print #3, frmt(YY_F1(2), CIFRE_DXF)
      Print #3, "31"
      Print #3, "0.0"
      'PUNTO INTERMEDIO DEL SECONDO PROLUNGAMENTO
      XX_F1(22) = (XX(IFIN_F1) + XX_F1(2)) / 2
      YY_F1(22) = (YY(IFIN_F1) + YY_F1(2)) / 2
      'PUNTO INTERMEDIO SULL'ARCO DI RACCORDO
      XX_F1(33) = XX_F1(0) + RR_F1 * Cos(Abs(TT(IUPP_F1)) / 2 + Abs(TT(IFIN_F1)) / 2)
      YY_F1(33) = YY_F1(0) - RR_F1 * Sin(Abs(TT(IUPP_F1)) / 2 + Abs(TT(IFIN_F1)) / 2)
      'ARCO DI RACCORDO
      'COORD. X        COORD. Y
      R164 = XX_F1(1):  R165 = YY_F1(1)
      R166 = XX_F1(33): R167 = YY_F1(33)
      R168 = XX_F1(2):  R169 = YY_F1(2)
      GoSub SCRIVI_RIGA_DXF
    End If
    '
    'FIANCO 2
    IUPP_F2 = LEP("COR[0,420]")
      RR_F2 = LEP("COR[0,421]")
      LL_F2 = LEP("COR[0,422]")
    If (IUPP_F2 > 0) And (RR_F2 > 0) And (LL_F2 > 0) Then
      IFIN_F2 = LEP("COR[0,423]")
      If (IFIN_F2 >= IUPP_F2) Or ((IFIN_F2 <= 0)) Then IFIN_F2 = IUPP_F2 - 1
      'FINE PROLUNGAMENTO
      XX_F2(1) = XX(IUPP_F2) - LL_F2 * Sin(Abs(TT(IUPP_F2)))
      YY_F2(1) = YY(IUPP_F2) + LL_F2 * Cos(Abs(TT(IUPP_F2)))
      'TRATTO RETTILINEO
      Print #3, "0"
      Print #3, "LINE"
      Print #3, "8"
      Print #3, "1"
      Print #3, "62"
      Print #3, "1"
      'PRIMO PUNTO
      Print #3, "10"
      Print #3, frmt(XX(IUPP_F2), CIFRE_DXF)
      Print #3, "20"
      Print #3, frmt(YY(IUPP_F2), CIFRE_DXF)
      Print #3, "30"
      Print #3, "0.0"
      'SECONDO PUNTO
      Print #3, "11"
      Print #3, frmt(XX_F2(1), CIFRE_DXF)
      Print #3, "21"
      Print #3, frmt(YY_F2(1), CIFRE_DXF)
      Print #3, "31"
      Print #3, "0.0"
      'PUNTO INTERMEDIO DEL PROLUNGAMENTO
      XX_F2(11) = (XX(IUPP_F2) + XX_F2(1)) / 2
      YY_F2(11) = (YY(IUPP_F2) + YY_F2(1)) / 2
      'CENTRO DEL CERCHIO DI RACCORDO
      XX_F2(0) = XX_F2(1) + RR_F2 * Sin(PG / 2 - Abs(TT(IUPP_F2)))
      YY_F2(0) = YY_F2(1) + RR_F2 * Cos(PG / 2 - Abs(TT(IUPP_F2)))
      'PUNTO DI TANGENZA
      XX_F2(2) = XX_F2(0) - RR_F2 * Cos(Abs(TT(IFIN_F2)))
      YY_F2(2) = YY_F2(0) - RR_F2 * Sin(Abs(TT(IFIN_F2)))
      'TRATTO RETTILINEO
      Print #3, "0"
      Print #3, "LINE"
      Print #3, "8"
      Print #3, "1"
      Print #3, "62"
      Print #3, "1"
      'PRIMO PUNTO
      Print #3, "10"
      Print #3, frmt(XX(IFIN_F2), CIFRE_DXF)
      Print #3, "20"
      Print #3, frmt(YY(IFIN_F2), CIFRE_DXF)
      Print #3, "30"
      Print #3, "0.0"
      'SECONDO PUNTO
      Print #3, "11"
      Print #3, frmt(XX_F2(2), CIFRE_DXF)
      Print #3, "21"
      Print #3, frmt(YY_F2(2), CIFRE_DXF)
      Print #3, "31"
      Print #3, "0.0"
      'PUNTO INTERMEDIO DEL SECONDO PROLUNGAMENTO
      XX_F2(22) = (XX(IFIN_F2) + XX_F2(2)) / 2
      YY_F2(22) = (YY(IFIN_F2) + YY_F2(2)) / 2
      'PUNTO INTERMEDIO SULL'ARCO DI RACCORDO
      XX_F2(33) = XX_F2(0) - RR_F2 * Cos(Abs(TT(IUPP_F2)) / 2 + Abs(TT(IFIN_F2)) / 2)
      YY_F2(33) = YY_F2(0) - RR_F2 * Sin(Abs(TT(IUPP_F2)) / 2 + Abs(TT(IFIN_F2)) / 2)
      'ARCO DI RACCORDO
      'COORD. X        COORD. Y
      R164 = XX_F2(1):  R165 = YY_F2(1)
      R166 = XX_F2(33): R167 = YY_F2(33)
      R168 = XX_F2(2):  R169 = YY_F2(2)
      GoSub SCRIVI_RIGA_DXF
    End If
End If
    '
    i = NPF1
      'SEPARAZIONE
      Print #3, "0"
      Print #3, "LINE"
      Print #3, "8"
      Print #3, "1"
      Print #3, "62"
      Print #3, "1"
      'PRIMO PUNTO
      Print #3, "10"
      Print #3, frmt(XX(i), CIFRE_DXF)
      Print #3, "20"
      Print #3, frmt(YY(i) + 0.1 * Altezza(0), CIFRE_DXF)
      Print #3, "30"
      Print #3, "0.0"
      'SECONDO PUNTO
      Print #3, "11"
      Print #3, frmt(XX(i), CIFRE_DXF)
      Print #3, "21"
      Print #3, frmt(YY(i) - 1.1 * Altezza(0), CIFRE_DXF)
      Print #3, "31"
      Print #3, "0.0"
    For i = Np To NPF1 + 2 Step -2
      'COORD. X        COORD. Y
      R164 = XX(i - 0): R165 = YY(i - 0)
      R166 = XX(i - 1): R167 = YY(i - 1)
      R168 = XX(i - 2): R169 = YY(i - 2)
      GoSub SCRIVI_RIGA_DXF
    Next
    i = Np
      'TRATTO INIZIALE
      Print #3, "0"
      Print #3, "LINE"
      Print #3, "8"
      Print #3, "1"
      Print #3, "62"
      Print #3, "1"
      'PENULTIMO PUNTO
      Print #3, "10"
      Print #3, frmt(XX(i), CIFRE_DXF)
      Print #3, "20"
      Print #3, frmt(YY(i), CIFRE_DXF)
      Print #3, "30"
      Print #3, "0.0"
      'ULTIMO PUNTO
      i = i + 1
      Print #3, "11"
      Print #3, frmt(XX(i), CIFRE_DXF)
      Print #3, "21"
      Print #3, frmt(YY(i), CIFRE_DXF)
      Print #3, "31"
      Print #3, "0.0"
    'FINE FILE
    Print #3, "0"
    Print #3, "ENDSEC"
    Print #3, "0"
    Print #3, "EOF"
  Close #3
  '
Exit Sub

SCRIVI_RIGA_DXF:
  'DISTANZA PER TRE PUNTI
  R162 = Abs((R165 - R169) * (R164 - R166) + (R167 - R165) * (R164 - R168)) / Sqr((R165 - R169) * (R165 - R169) + (R164 - R168) * (R164 - R168))
  If (R162 < 0.0005) Then
    'TRATTO RETTILINEO
    'G1 Y=R169 X=R168
    'TRATTO INTERMEDIO
    Print #3, "0"
    Print #3, "LINE"
    Print #3, "8"
    Print #3, "1"
    Print #3, "62"
    Print #3, "1"
    'PRIMO PUNTO
    Print #3, "10"
    Print #3, frmt(R164, CIFRE_DXF)
    Print #3, "20"
    Print #3, frmt(R165, CIFRE_DXF)
    Print #3, "30"
    Print #3, "0.0"
    'SECONDO PUNTO
    Print #3, "11"
    Print #3, frmt(R166, CIFRE_DXF)
    Print #3, "21"
    Print #3, frmt(R167, CIFRE_DXF)
    Print #3, "31"
    Print #3, "0.0"
    'TRATTO INTERMEDIO
    Print #3, "0"
    Print #3, "LINE"
    Print #3, "8"
    Print #3, "1"
    Print #3, "62"
    Print #3, "1"
    'SECONDO PUNTO
    Print #3, "10"
    Print #3, frmt(R166, CIFRE_DXF)
    Print #3, "20"
    Print #3, frmt(R167, CIFRE_DXF)
    Print #3, "30"
    Print #3, "0.0"
    'TERZO PUNTO
    Print #3, "11"
    Print #3, frmt(R168, CIFRE_DXF)
    Print #3, "21"
    Print #3, frmt(R169, CIFRE_DXF)
    Print #3, "31"
    Print #3, "0.0"
  Else
    'TRATTO CURVILINEO
    If R169 = R167 Then R169 = R167 + 0.0001
    If R165 = R167 Then R165 = R167 + 0.0001
    R160 = (R164 - R166) / (R167 - R165)
    R171 = (R166 - R168) / (R169 - R167)
    R172 = (R165 + R167) / 2 - R160 * (R164 + R166) / 2
    R173 = (R169 + R167) / 2 - R171 * (R168 + R166) / 2
    If R160 = R171 Then R160 = R171 + 0.0001
    'coordinate del centro
    R174 = (R173 - R172) / (R160 - R171)
    R170 = R160 * R174 + R172
    'raggio del cerchio
    R161 = Sqr((R174 - R166) * (R174 - R166) + (R170 - R167) * (R170 - R167))
    'prodotto misto per senso di rotazione
    R160 = (R166 - R164) * (R169 - R165) - (R168 - R164) * (R167 - R165)
    'Calcolo angoli compresi tra 0 e 2 * Pigreco
    AngStart = CALCOLO_ANGOLO(R164 - R174, R165 - R170)
    AngEnd = CALCOLO_ANGOLO(R168 - R174, R169 - R170)
    If (R160 > 0) Then
      'G3 Y=R169 X=R168 CR=R161
      Print #3, "0"
      Print #3, "ARC"
      Print #3, "8"
      Print #3, "1"
      Print #3, "62"
      Print #3, "1"
      Print #3, "10"
      Print #3, frmt(R174, CIFRE_DXF) & " (x centro)"
      Print #3, "20"
      Print #3, frmt(R170, CIFRE_DXF) & " (y centro)"
      Print #3, "30"
      Print #3, "0.0"
      Print #3, "40"
      Print #3, frmt(R161, CIFRE_DXF) & " (raggio)"
      Print #3, "50"
      Print #3, frmt(AngStart * 180 / PG, CIFRE_DXF) & " (angolo iniziale)"
      Print #3, "51"
      Print #3, frmt(AngEnd * 180 / PG, CIFRE_DXF) & " (angolo finale)"
    Else
      'G2 Y=R169 X=R168 CR=R161
      Print #3, "0"
      Print #3, "ARC"
      Print #3, "8"
      Print #3, "1"
      Print #3, "62"
      Print #3, "1"
      Print #3, "10"
      Print #3, frmt(R174, CIFRE_DXF) & " (x centro)"
      Print #3, "20"
      Print #3, frmt(R170, CIFRE_DXF) & " (y centro)"
      Print #3, "30"
      Print #3, "0.0"
      Print #3, "40"
      Print #3, frmt(R161, CIFRE_DXF) & " (raggio)"
      Print #3, "50"
      Print #3, frmt(AngEnd * 180 / PG, CIFRE_DXF) & " (angolo iniziale)"
      Print #3, "51"
      Print #3, frmt(AngStart * 180 / PG, CIFRE_DXF) & " (angolo finale)"
    End If
  End If
Return

errCREAZIONE_FILE_PROFILO_MOLA_DXF:
  If FreeFile <> 0 Then Close
  WRITE_DIALOG "SUB CREAZIONE_FILE_PROFILO_MOLA_DXF: " & str(Err)
  Exit Sub

End Sub
'
Sub CREAZIONE_FILE_PROFILO_ROTORE_DXF(ByVal Np As Integer, ByRef R() As Double, ByRef A() As Double, ByVal PASSO As Single, ByVal TY_ROT As String, ByVal NomeROTORE As String, ByVal NDenteLOC As Integer)
'
Dim i    As Integer
Dim k    As Integer
Dim ftmp As Double
'
Dim R160 As Double, R161 As Double, R162 As Double
Dim R163 As Double, R164 As Double, R165 As Double
Dim R166 As Double, R167 As Double, R168 As Double
Dim R169 As Double
Dim R170 As Double, R171 As Double, R172 As Double
Dim R173 As Double, R174 As Double
Dim R180 As Double, R184 As Double
'
Dim da       As Double
Dim AngStart As Double
Dim AngEnd   As Double
'
Const CIFRE_DXF = 20
'
On Error GoTo errCREAZIONE_FILE_PROFILO_ROTORE_DXF
  '
  If (NDenteLOC <= 0) Then NDenteLOC = 1
  '
  Open g_chOemPATH & "\ROTORE_" & NomeROTORE & "_" & TY_ROT & ".DXF" For Output As #3
  'INIZIO FILE
  Print #3, "0"
  Print #3, "SECTION"
  Print #3, "2"
  Print #3, "ENTITIES"
  For k = 0 To NDenteLOC - 1
    da = (360 / NDenteLOC) * k
    For i = 1 To Np - 2 Step 2
      If (TY_ROT = "AXIAL") Then
        'COORD. X        COORD. Y
        R164 = (A(i + 0) + da) / 360 * PASSO: R165 = R(i + 0)
        R166 = (A(i + 1) + da) / 360 * PASSO: R167 = R(i + 1)
        R168 = (A(i + 2) + da) / 360 * PASSO: R169 = R(i + 2)
      Else
        'COORD. X        COORD. Y
        R164 = R(i + 0) * Sin((A(i + 0) + da) * PG / 180): R165 = R(i + 0) * Cos((A(i + 0) + da) * PG / 180)
        R166 = R(i + 1) * Sin((A(i + 1) + da) * PG / 180): R167 = R(i + 1) * Cos((A(i + 1) + da) * PG / 180)
        R168 = R(i + 2) * Sin((A(i + 2) + da) * PG / 180): R169 = R(i + 2) * Cos((A(i + 2) + da) * PG / 180)
      End If
      'DISTANZA PER TRE PUNTI
      ftmp = Sqr((R165 - R169) * (R165 - R169) + (R164 - R168) * (R164 - R168))
      If (ftmp < 0.0001) Then ftmp = 0.0001
      R162 = Abs((R165 - R169) * (R164 - R166) + (R167 - R165) * (R164 - R168))
      R162 = R162 * (1 / ftmp)
      If R162 < 0.0005 Then
        'TRATTO RETTILINEO
        'G1 Y=R169 X=R168
        'TRATTO INTERMEDIO
        Print #3, "0"
        Print #3, "LINE"
        Print #3, "8"
        Print #3, "1"
        Print #3, "62"
        Print #3, "1"
        'PRIMO PUNTO
        Print #3, "10"
        Print #3, frmt(R164, CIFRE_DXF)
        Print #3, "20"
        Print #3, frmt(R165, CIFRE_DXF)
        Print #3, "30"
        Print #3, "0.0"
        'SECONDO PUNTO
        Print #3, "11"
        Print #3, frmt(R166, CIFRE_DXF)
        Print #3, "21"
        Print #3, frmt(R167, CIFRE_DXF)
        Print #3, "31"
        Print #3, "0.0"
        'TRATTO INTERMEDIO
        Print #3, "0"
        Print #3, "LINE"
        Print #3, "8"
        Print #3, "1"
        Print #3, "62"
        Print #3, "1"
        'SECONDO PUNTO
        Print #3, "10"
        Print #3, frmt(R166, CIFRE_DXF)
        Print #3, "20"
        Print #3, frmt(R167, CIFRE_DXF)
        Print #3, "30"
        Print #3, "0.0"
        'TERZO PUNTO
        Print #3, "11"
        Print #3, frmt(R168, CIFRE_DXF)
        Print #3, "21"
        Print #3, frmt(R169, CIFRE_DXF)
        Print #3, "31"
        Print #3, "0.0"
      Else
        'TRATTO CURVILINEO
        If R169 = R167 Then R169 = R167 + 0.0001
        If R165 = R167 Then R165 = R167 + 0.0001
        R160 = (R164 - R166) / (R167 - R165)
        R171 = (R166 - R168) / (R169 - R167)
        R172 = (R165 + R167) / 2 - R160 * (R164 + R166) / 2
        R173 = (R169 + R167) / 2 - R171 * (R168 + R166) / 2
        If R160 = R171 Then R160 = R171 + 0.0001
        'coordinate del centro
        R174 = (R173 - R172) / (R160 - R171)
        R170 = R160 * R174 + R172
        'raggio del cerchio
        R161 = Sqr((R174 - R166) * (R174 - R166) + (R170 - R167) * (R170 - R167))
        'prodotto misto per senso di rotazione
        R160 = (R166 - R164) * (R169 - R165) - (R168 - R164) * (R167 - R165)
        'Calcolo angoli compresi tra 0 e 2 * Pigreco
        AngStart = CALCOLO_ANGOLO(R164 - R174, R165 - R170)
        AngEnd = CALCOLO_ANGOLO(R168 - R174, R169 - R170)
        If R160 > 0 Then
          'G3 Y=R169 X=R168 CR=R161
          Print #3, "0"
          Print #3, "ARC"
          Print #3, "8"
          Print #3, "1"
          Print #3, "62"
          Print #3, "1"
          Print #3, "10"
          Print #3, frmt(R174, CIFRE_DXF) & " (x centro)"
          Print #3, "20"
          Print #3, frmt(R170, CIFRE_DXF) & " (y centro)"
          Print #3, "30"
          Print #3, "0.0"
          Print #3, "40"
          Print #3, frmt(R161, CIFRE_DXF) & " (raggio)"
          Print #3, "50"
          Print #3, frmt(AngStart * 180 / PG, CIFRE_DXF) & " (angolo iniziale)"
          Print #3, "51"
          Print #3, frmt(AngEnd * 180 / PG, CIFRE_DXF) & " (angolo finale)"
        Else
          'G2 Y=R169 X=R168 CR=R161
          Print #3, "0"
          Print #3, "ARC"
          Print #3, "8"
          Print #3, "1"
          Print #3, "62"
          Print #3, "1"
          Print #3, "10"
          Print #3, frmt(R174, CIFRE_DXF) & " (x centro)"
          Print #3, "20"
          Print #3, frmt(R170, CIFRE_DXF) & " (y centro)"
          Print #3, "30"
          Print #3, "0.0"
          Print #3, "40"
          Print #3, frmt(R161, CIFRE_DXF) & " (raggio)"
          Print #3, "50"
          Print #3, frmt(AngEnd * 180 / PG, CIFRE_DXF) & " (angolo iniziale)"
          Print #3, "51"
          Print #3, frmt(AngStart * 180 / PG, CIFRE_DXF) & " (angolo finale)"
        End If
      End If
    Next i
  Next k
  'FINE FILE
  Print #3, "0"
  Print #3, "ENDSEC"
  Print #3, "0"
  Print #3, "EOF"
  Close #3
  '
Exit Sub

errCREAZIONE_FILE_PROFILO_ROTORE_DXF:
  If FreeFile <> 0 Then Close
  WRITE_DIALOG "SUB CREAZIONE_FILE_PROFILO_ROTORE_DXF: " & str(Err)
  Exit Sub

End Sub

Sub CALCOLO_CORREZIONI(CorZ() As Double, corY() As Double, Np As Integer, npt As Integer, NP1 As Integer, NP2 As Integer, NPM1 As Integer, NPM2 As Integer, _
                       RADICE As String, TY_ROT As String, CONTROLLORE As String, ER() As Double)
'
Dim NPun         As Integer
Dim NCOR         As Integer
Dim MOLANEGTOL() As Double
Dim MOLAPOSTOL() As Double
Dim X            As Double
Dim RINF()       As Double
Dim RSUP()       As Double
Dim riga         As String
Dim INDPREC      As Integer
Dim INDTAB       As Integer
Dim DELTAPUN     As Integer
'Dim ER()         As Double
Dim Err()        As Double
Dim ERRR         As Double
Dim TGMG()       As Double
Dim TGMR()       As Double
'
On Error Resume Next
  '
  NPun = NP1 + NP2
  NCOR = NPM1 + NPM2
  '
  'FASCIA DI TOLLERANZA
  Open RADICE & "\PRFTOL.DAT" For Input As #1
    i = 1
      ReDim Preserve MOLANEGTOL(i): ReDim Preserve MOLAPOSTOL(i)
      MOLANEGTOL(i) = 0: MOLAPOSTOL(i) = 0
    For i = 2 To npt + 1
      ReDim Preserve MOLANEGTOL(i): ReDim Preserve MOLAPOSTOL(i)
      Input #1, MOLANEGTOL(i), MOLAPOSTOL(i)
    Next i
    i = npt + 2
      ReDim Preserve MOLANEGTOL(i): ReDim Preserve MOLAPOSTOL(i)
      MOLANEGTOL(i) = 0: MOLAPOSTOL(i) = 0
  Close #1
  '
  'Creazione della lista errori FILERR$.
  If (TY_ROT = "AXIAL") And (CONTROLLORE <> "INTERNAL") Then
      If (Dir$(RADICE & "\RISROT") = "") Then
        Open RADICE & "\RISROT" For Output As #1
          If (NP2 >= NP1) Then
            For i = 1 To NP2   '39
              Print #1, "+0.000,+0.000"
            Next i
          Else
            For i = 1 To NP1   '39
              Print #1, "+0.000,+0.000"
            Next i
          End If
        Close #1
      End If
      Open RADICE & "\RISROT" For Input As #1
          If (NP2 >= NP1) Then
            For i = 1 To NP2   '39
              ReDim Preserve RINF(i)
              ReDim Preserve RSUP(i)
              Input #1, RINF(i), RSUP(i)
            Next i
          Else
            For i = 1 To NP1   '39
              ReDim Preserve RINF(i)
              ReDim Preserve RSUP(i)
              Input #1, RINF(i), RSUP(i)
            Next i
          End If
      Close #1
      Open RADICE & "\ERRORI.MOD" For Output As #2
        For i = 1 To NP1
          riga = ""
          X = RSUP(i)
          If X >= 0 Then riga = riga & "+" Else riga = riga & "-"
          riga = riga & frmt0(Abs(X), 4)
          Print #2, riga
        Next i
        For i = NP2 To 1 Step -1
          riga = ""
          X = RINF(i)
          If X >= 0 Then riga = riga & "+" Else riga = riga & "-"
          riga = riga & frmt0(Abs(X), 4)
          Print #2, riga
        Next i
      Close #2
  End If
  '
  If (TY_ROT = "FRONT") Or (CONTROLLORE = "INTERNAL") Then
      If (Dir$(RADICE & "\EPMROT") = "") Then
        Open RADICE & "\EPMROT" For Output As #1
          For i = 1 To NPun + 1
            Print #1, "+0.000,+0.000"
          Next
        Close #1
      End If
      Open RADICE & "\EPMROT" For Input As #1
        For i = 1 To NPun + 1
          ReDim Preserve RINF(i)
          Input #1, RINF(i), X
        Next
      Close #1
      Open RADICE & "\ERRORI.MOD" For Output As #1
        For i = 1 To NP1
          riga = ""
          X = RINF(i)
          If X >= 0 Then riga = riga & "+" Else riga = riga & "-"
          riga = riga & frmt0(Abs(X), 4)
          Print #1, riga
        Next i
        For i = NPun To NP1 - 1 Step -1
          riga = ""
          X = RINF(i)
          If X >= 0 Then riga = riga & "+" Else riga = riga & "-"
          riga = riga & frmt0(Abs(X), 4)
          Print #1, riga
        Next i
        riga = ""
        X = RINF(NPun + 1)
        If X >= 0 Then riga = riga & "+" Else riga = riga & "-"
        riga = riga & frmt0(Abs(X), 4)
        Print #1, riga
      Close #1
  End If
  '
  'NUMERO PUNTI DI CORREZIONE
  Np = 1
  Open RADICE & "\ERRORI.MOD" For Input As #1
  Open RADICE & "\TGMROT.ALL" For Input As #2
  Open RADICE & "\PUNCHK" For Input As #3
    ' Primi punti non controllati: Errore = Errore primo punto controllato
    i = 1
    INDPREC = 0
      'LETTURA ERRORE
      ReDim Preserve ER(i)
      Input #1, ER(i)
      'LETTURA INDICE PUNTO CONTROLLATO
      Input #3, INDTAB
      'CALCOLO DEL NUMERO DEI PUNTI NON CONTROLLATI
      DELTAPUN = INDTAB - INDPREC
      'LETTURA DELLE TANGENTI
      For j = 1 To DELTAPUN
        ReDim Preserve TGMG(j): ReDim Preserve TGMR(j)
        Input #2, TGMG(j): TGMG(j) = Abs(TGMG(j))
        TGMR(j) = Abs((90 - TGMG(j)) * PG / 180)
      Next j
      'CALCOLO DELLE CORREZIONI
      For j = 1 To DELTAPUN
        ERRR = ER(i)
        ERRR = ERRR + (MOLAPOSTOL(Np) - MOLANEGTOL(Np)) / 2
        ReDim Preserve corY(Np): ReDim Preserve CorZ(Np)
        corY(Np) = ERRR * Cos(TGMR(j))
        CorZ(Np) = ERRR * Sin(TGMR(j))
        'INCREMENTO DEL NUMERO DEI PUNTI
        Np = Np + 1
      Next j
      'RIDIMENSIONO I VETTORI DELLE TANGENTI
      ReDim TGMG(0): ReDim TGMR(0)
    '
    ' LOOP punti controllati
    INDPREC = INDTAB
    For i = 2 To NPun
      'LETTURA ERRORE
      ReDim Preserve ER(i)
      Input #1, ER(i)
      'LETTURA INDICE PUNTO CONTROLLATO
      Input #3, INDTAB
      'CALCOLO DEL NUMERO DEI PUNTI NON CONTROLLATI
      DELTAPUN = INDTAB - INDPREC
      'LETTURA DELLE TANGENTI
      For j = 1 To DELTAPUN
        ReDim Preserve TGMG(j): ReDim Preserve TGMR(j)
        Input #2, TGMG(j): TGMG(j) = Abs(TGMG(j))
        TGMR(j) = Abs((90 - TGMG(j)) * PG / 180)
      Next j
      'CALCOLO DELLE CORREZIONI
      For j = 1 To DELTAPUN
        ERRR = (ER(i) - ER(i - 1)) / (DELTAPUN / j) + ER(i - 1)
        ERRR = ERRR + (MOLAPOSTOL(Np) - MOLANEGTOL(Np)) / 2
        ReDim Preserve corY(Np): ReDim Preserve CorZ(Np)
        corY(Np) = ERRR * Cos(TGMR(j))
        CorZ(Np) = ERRR * Sin(TGMR(j))
        'INCREMENTO DEL NUMERO DEI PUNTI
        Np = Np + 1
      Next j
      'RIDIMENSIONO I VETTORI DELLE TANGENTI
      ReDim TGMG(0): ReDim TGMR(0)
      'AGGIORNAMENTO DELL'INDICE DEL PUNTO PRECEDENTE CONTROLLATO
      INDPREC = INDTAB
    Next i
    '
    'Ultimi punti non controllati: Errore = Errore ultimo punto controllato
    i = NPun
    DELTAPUN = NCOR - Np + 1
      'LETTURA DELLE TANGENTI
      For j = 1 To DELTAPUN
        ReDim Preserve TGMG(j): ReDim Preserve TGMR(j)
        Input #2, TGMG(j): TGMG(j) = Abs(TGMG(j))
        TGMR(j) = Abs((90 - TGMG(j)) * PG / 180)
      Next j
      'CALCOLO DELLE CORREZIONI
      For j = 1 To DELTAPUN
        ERRR = ER(i)
        ERRR = ERRR + (MOLAPOSTOL(Np) - MOLANEGTOL(Np)) / 2
        ReDim Preserve corY(Np): ReDim Preserve CorZ(Np)
        corY(Np) = ERRR * Cos(TGMR(j))
        CorZ(Np) = ERRR * Sin(TGMR(j))
        'INCREMENTO DEL NUMERO DEI PUNTI
        Np = Np + 1
      Next j
  '
  Close #3
  Close #2
  Close #1
  '
  'CREAZIONE FILE CORREZIONI PROPOSTE
  Open RADICE & "\CORPROP" For Output As #1
    For i = 1 To NCOR
      riga = ""
      X = CorZ(i)
      If X >= 0 Then riga = riga & "+" Else riga = riga & "-"
      riga = riga & frmt0(Abs(X), 4)
      riga = riga & ","
      X = corY(i)
      If X >= 0 Then riga = riga & "+" Else riga = riga & "-"
      riga = riga & frmt0(Abs(X), 4)
      Print #1, riga
    Next i
  Close #1
  '
End Sub

Sub VISUALIZZA_MOLA_CORRETTA(ByVal INDICE As Integer, ByVal SiStampa As String)
'
Dim Rmin          As Double
Dim iMIN          As Integer
Dim RaggioINTmin  As Double
Dim iINTmin       As Integer
Dim tMIN          As Double
Dim jMIN          As Integer
'
Dim DR    As Double
Dim iDR   As Integer
'
Dim ProfiloMola    As String
Dim CorrezioniMola As String
'
Dim XX()   As Double
Dim YY()   As Double
Dim TT()   As Double
'
Dim Codice As String
Dim Senso  As Integer
'
Dim stmp   As String
Dim ftmp   As Double
'
Dim i      As Integer
Dim j      As Integer
'
Dim Np     As Integer
Dim NPF1   As Integer
'
Dim AngStart  As Double
Dim AngEnd    As Double
Dim Ang       As Double
Dim DeltaAng  As Double
Dim DeltaArco As Double
'
Dim PicW As Single
Dim PicH As Single
'
Dim pX1 As Double
Dim pY1 As Double
Dim pX2 As Double
Dim pY2 As Double
'
Dim ColoreCurva As Integer
Dim Raggio      As Double
'
Dim NpF1M          As Integer
Dim RaggioMassimo  As Double
Dim XX_Separazione As Double
'
Dim R160 As Double
Dim R161 As Double
Dim R162 As Double
Dim R163 As Double
Dim R164 As Double
Dim R165 As Double
Dim R166 As Double
Dim R167 As Double
Dim R168 As Double
Dim R169 As Double
Dim R170 As Double
Dim R171 As Double
Dim R172 As Double
Dim R173 As Double
Dim R174 As Double
'
Dim ZeroPezzoX As Double
Dim ZeroPezzoY As Double
'
Dim Xmax    As Double
Dim Ymax    As Double
Dim iYmax   As Long
Dim Xmin    As Double
Dim Ymin    As Double
'
Dim ScalaX  As Double
Dim ScalaY  As Double
'
Dim IndiceZeroPezzoX As Integer
Dim IndiceZeroPezzoY As Integer
'
Dim sL(10, 2)   As String
'
Dim TabXMax     As Single
Dim TabX        As Single
Dim TabY        As Single
'
Dim dYmax       As Double
Dim IdYmax      As Integer
'
Dim Scala       As Double
'
Dim ScalaERRORE As Double
Const NumeroArchi = 50
Dim INTERPOLAZIONE As Integer
'
On Error GoTo errVISUALIZZA_MOLA_CORRETTA
  '
  Dim COORDINATE         As String
  Dim RADICE             As String
  Dim PERCORSO           As String
  Dim PERCORSO_CONTROLLI As String
  Dim PERCORSO_PROFILI   As String
  Dim sequenza           As String
  Dim npt                As Integer
  Dim TY_ROT             As String
  Dim PASSO              As Double
  Dim CONTROLLORE        As String
  Dim DM_ROT             As String
  Dim INVERSIONE         As Integer
  '
  Call LEGGI_INFO_ROTORI(PERCORSO, COORDINATE, RADICE, PERCORSO_CONTROLLI, PERCORSO_PROFILI, sequenza, npt, TY_ROT, PASSO, CONTROLLORE, DM_ROT, INVERSIONE)
  '
  Dim NP1   As Integer
  Dim NP2   As Integer
  Dim NPM1  As Integer
  Dim NPM2  As Integer
  '
  NPM1 = val(GetInfo("CREAZIONE", "NpM_F1", RADICE & "\INFO.INI"))
  NPM2 = val(GetInfo("CREAZIONE", "NpM_F2", RADICE & "\INFO.INI"))
  NP1 = val(GetInfo("CREAZIONE", "NpC_F1", RADICE & "\INFO.INI"))
  NP2 = val(GetInfo("CREAZIONE", "NpC_F2", RADICE & "\INFO.INI"))
  '
  NPF1 = NPM1
  Np = npt
  INTERPOLAZIONE = LEP("TIPOMOLA_G")
  '
  CHK0.hsCORROT.Max = npt + 2
  '
  'CALCOLO DELLE CORREZIONI PROPOSTE
  Dim corY() As Double
  Dim CorZ() As Double
  Dim ER()   As Double
  '
  Call CALCOLO_CORREZIONI(CorZ, corY, Np, npt, NP1, NP2, NPM1, NPM2, RADICE, TY_ROT, CONTROLLORE, ER)
  '
  'SCALA DELLA VISUALIZZAZIONE
  Scala = val(CHK0.txtCORROT(0).Text)
  If (Scala <= 0) Then Scala = 1
  CHK0.txtCORROT(0).Text = frmt(Scala, 4)
  '
  ScalaX = Scala: ScalaY = Scala
  '
  'SCALA DELLA VISUALIZZAZIONE ERRORE
  ScalaERRORE = val(CHK0.txtCORROT(1).Text)
  If (ScalaERRORE <= 0) Then ScalaERRORE = 1
  CHK0.txtCORROT(1).Text = frmt(ScalaERRORE, 4)
  '
  IndiceZeroPezzoX = INDICE
  IndiceZeroPezzoY = INDICE
  '
  ProfiloMola = "XYT." & Format$(val(DM_ROT), "##0")
  '
  'Acquisizione profilo mola
  If (Dir$(RADICE & "\" & ProfiloMola) <> "") Then
    tMIN = 1E+308: jMIN = 0
    Open RADICE & "\" & ProfiloMola For Input As #1
      i = 2
      While Not EOF(1)
        ReDim Preserve XX(i): ReDim Preserve YY(i): ReDim Preserve TT(i)
        Input #1, XX(i), YY(i), TT(i)
        If tMIN > Abs(TT(i)) Then tMIN = Abs(TT(i)): jMIN = i
        i = i + 1
      Wend
    Close #1
    'PRIMO PUNTO
    XX(1) = XX(2): YY(1) = YY(2): TT(1) = TT(2)
    Np = i - 1
    Np = Np + 1
    ReDim Preserve XX(Np): ReDim Preserve YY(Np): ReDim Preserve TT(Np)
    'ULTIMO PUNTO
    XX(Np) = XX(Np - 1): YY(Np) = YY(Np - 1): TT(Np) = TT(Np - 1)
  Else
    MsgBox ProfiloMola & ": " & Error(53), 48, "WARNING"
    Exit Sub
  End If
  '
  Open RADICE & "\ABILITA.COR" For Input As #2
    i = 1
    While (Not EOF(2)) And (i <= npt + 2)
      Line Input #2, stmp
      If (val(stmp) = 1) Then
        Call CHK0.List3(4).AddItem("X")
      Else
        Call CHK0.List3(4).AddItem("-")
      End If
      i = i + 1
    Wend
  Close #2
  '
  'DISTANZA RADIALE MASSIMA
  dYmax = 0: IdYmax = 1
  For i = 1 To Np - 1
    DR = Abs(YY(i + 1) - YY(i))
    If (DR > dYmax) Then dYmax = DR: IdYmax = i
  Next i
  '
  'Leggo il primo ed ultimo punto della mola
  If (Dir$(RADICE & "\D" & Format$(val(DM_ROT), "##0")) <> "") Then
    Open RADICE & "\D" & Format$(val(DM_ROT), "##0") For Input As #1
      Line Input #1, stmp
      stmp = Trim$(stmp)
      i = InStr(2, stmp, "+")
      If i <= 0 Then i = InStr(2, stmp, "-")
      XX(1) = val(Left$(stmp, i - 1))
      YY(1) = val(Right$(stmp, Len(stmp) - i))
      'ricavo il primo punto
      While Not EOF(1)
        Line Input #1, stmp
      Wend
      'ricavo il l'ultimo punto
      stmp = Trim$(stmp)
      i = InStr(2, stmp, "+")
      If i <= 0 Then i = InStr(2, stmp, "-")
      XX(Np) = val(Left$(stmp, i - 1))
      YY(Np) = val(Right$(stmp, Len(stmp) - i))
    Close #1
  Else
    MsgBox "D" & Format$(val(DM_ROT), "##0") & ": " & Error(53), 48, "WARNING"
  End If
  '
  'Coordinata XX di separazione tra il primo e secondo fianco
  XX_Separazione = XX(NPF1)
  '
  'Preparazione grafico
  If (SiStampa = "Y") Then
    Call ImpostaPrinter("Arial", 8)
    WRITE_DIALOG Printer.DeviceName & " PRINTING....."
    'Printer.Orientation = i
    Printer.ScaleMode = 6
    PicW = Printer.ScaleWidth
    PicH = Printer.ScaleHeight
  Else
    WRITE_DIALOG "DIPLAYING....."
    CHK0.PicCORROT.Cls
    CHK0.PicCORROT.FontName = "Arial"
    CHK0.PicCORROT.Font.Bold = False
    CHK0.PicCORROT.Font.Size = 8
    CHK0.PicCORROT.ScaleMode = 6
    PicW = CHK0.PicCORROT.ScaleWidth
    PicH = CHK0.PicCORROT.ScaleHeight
  End If
  '---------- -------------------- ----------
  '
  stmp = " F1 "
  If (SiStampa = "Y") Then
    Printer.CurrentX = 0
    Printer.CurrentY = 0
    Printer.Print stmp
  Else
    CHK0.PicCORROT.CurrentX = 0
    CHK0.PicCORROT.CurrentY = 0
    CHK0.PicCORROT.Print stmp
  End If
  '
  stmp = " F2 "
  If (SiStampa = "Y") Then
    Printer.CurrentX = PicW - Printer.TextWidth(stmp)
    Printer.CurrentY = 0
    Printer.Print stmp
  Else
    CHK0.PicCORROT.CurrentX = PicW - CHK0.PicCORROT.TextWidth(stmp)
    CHK0.PicCORROT.CurrentY = 0
    CHK0.PicCORROT.Print stmp
  End If
  '
  stmp = " ANGLE MIN= " & frmt(tMIN, 4) & " / " & Format$(jMIN) & " "
  If (SiStampa = "Y") Then
    Printer.CurrentX = PicW - Printer.TextWidth(stmp)
    Printer.CurrentY = PicH - Printer.TextHeight(stmp)
    Printer.Print stmp
  Else
    CHK0.PicCORROT.CurrentX = PicW - CHK0.PicCORROT.TextWidth(stmp)
    CHK0.PicCORROT.CurrentY = PicH - CHK0.PicCORROT.TextHeight(stmp)
    CHK0.PicCORROT.Print stmp
  End If
  '
  stmp = " WHEEL FILE= " & ProfiloMola
  If (SiStampa = "Y") Then
    Printer.CurrentX = PicW / 2 - Printer.TextWidth(stmp) / 2
    Printer.CurrentY = PicH - Printer.TextHeight(stmp)
    Printer.Print stmp
  Else
    CHK0.PicCORROT.CurrentX = PicW / 2 - CHK0.PicCORROT.TextWidth(stmp) / 2
    CHK0.PicCORROT.CurrentY = PicH - CHK0.PicCORROT.TextHeight(stmp)
    CHK0.PicCORROT.Print stmp
  End If
  '
  GoSub RICERCA_MAX_MIN
  '
  If (IndiceZeroPezzoX >= 1) And (IndiceZeroPezzoX <= Np) Then
    'Profilo AZZERATO SU DI UN PUNTO DEL PROFILO STESSO
    ZeroPezzoX = XX(IndiceZeroPezzoX)
  Else
    ZeroPezzoX = 0
  End If
  If (IndiceZeroPezzoY >= 1) And (IndiceZeroPezzoY <= Np) Then
    'Profilo AZZERATO SU DI UN PUNTO DEL PROFILO STESSO
    ZeroPezzoY = YY(IndiceZeroPezzoY)
  Else
    'Profilo centrato
     ZeroPezzoY = Ymin
  End If
  '
  'Calcolo scala se non � impostata
  If (Scala = 0) Then
    ScalaX = PicW / (Xmax - Xmin): ScalaY = PicH / (Ymax - Ymin)
    If ScalaY < ScalaX Then Scala = ScalaY Else Scala = ScalaX
    ScalaX = Scala: ScalaY = Scala
  End If
  '
  'Calcolo dr
  DR = 1E+308
  iDR = 1
  For i = 1 To Np - 1
      ftmp = Sqr((XX(i + 1) - XX(i)) ^ 2 + (YY(i + 1) - YY(i)) ^ 2)
      If ftmp < DR Then DR = ftmp: iDR = i
  Next i
  If (DR = 0) Then
    stmp = "Duplicate point into WHEEL profile."
    MsgBox stmp, vbCritical, "WARNING: point " & Format$(iDR, "####0")
  End If
  '
  'Profilo mola
  Senso = 1                           'Indici dei punti esterni alla curva
  ColoreCurva = 0                     'Colore della curva
  GoSub RICERCA_MAX_MIN
  GoSub VISUALIZZA_XX_YY
  '
  'stmp = " R min TEO= " & frmt(Rmin, 4) & " / " & Format$(iMIN)
  stmp = " Ri MIN= " & frmt(RaggioINTmin, 4) & "mm -> " & Format$(iINTmin, "##0")
  If (SiStampa = "Y") Then
    Printer.CurrentX = 0
    Printer.CurrentY = PicH - Printer.TextHeight(stmp)
    Printer.Print stmp
  Else
    CHK0.PicCORROT.CurrentX = 0
    CHK0.PicCORROT.CurrentY = PicH - CHK0.PicCORROT.TextHeight(stmp)
    CHK0.PicCORROT.Print stmp
  End If
  '
  'VISUALIZZA_SEPARAZIONE
  stmp = " " & Format$(NPF1, "###0") & " "
  pX1 = (XX(NPF1) - ZeroPezzoX) * ScalaX + PicW / 2: pY1 = 0
  pX2 = (XX(NPF1) - ZeroPezzoX) * ScalaX + PicW / 2: pY2 = PicH
  If (SiStampa = "Y") Then
    Printer.Line (pX1, pY1 + Printer.TextHeight("X"))-(pX2, pY2 - 2 * Printer.TextHeight("X")), QBColor(ColoreCurva)
    Printer.CurrentX = pX1 - Printer.TextWidth(stmp) / 2
    Printer.CurrentY = pY1
    Printer.Print stmp
  Else
    CHK0.PicCORROT.Line (pX1, pY1 + CHK0.PicCORROT.TextHeight("X"))-(pX2, pY2 - 2 * CHK0.PicCORROT.TextHeight("X")), QBColor(ColoreCurva)
    CHK0.PicCORROT.CurrentX = pX1 - CHK0.PicCORROT.TextWidth(stmp) / 2
    CHK0.PicCORROT.CurrentY = pY1
    CHK0.PicCORROT.Print stmp
  End If
  '
  'VISUALIZZAZIONE DEL CENTRO MOLA
  stmp = "CM"
  If (SiStampa = "Y") Then
    Printer.DrawWidth = 2
    pX1 = (0 - ZeroPezzoX) * ScalaX + PicW / 2: pY1 = 0 + Printer.TextHeight("X")
    pX2 = (0 - ZeroPezzoX) * ScalaX + PicW / 2: pY2 = PicH - 2 * Printer.TextHeight("X")
    Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
    Printer.DrawWidth = 1
    Printer.CurrentX = pX1 - Printer.TextWidth(stmp) / 2
    Printer.CurrentY = pY2
    Printer.Print stmp
  Else
    CHK0.PicCORROT.DrawWidth = 2
    pX1 = (0 - ZeroPezzoX) * ScalaX + PicW / 2: pY1 = 0 + CHK0.PicCORROT.TextHeight("X")
    pX2 = (0 - ZeroPezzoX) * ScalaX + PicW / 2: pY2 = PicH - 2 * CHK0.PicCORROT.TextHeight("X")
    CHK0.PicCORROT.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
    CHK0.PicCORROT.DrawWidth = 1
    CHK0.PicCORROT.CurrentX = pX1 - CHK0.PicCORROT.TextWidth(stmp) / 2
    CHK0.PicCORROT.CurrentY = pY2
    CHK0.PicCORROT.Print stmp
  End If
  '
  GoSub CORREZIONI_PROFILO
  GoSub INVILUPPO_CORREZIONI_PROFILO
  '
  If (INDICE >= 1) And (INDICE <= Np) Then
    stmp = Chr(13)
    stmp = stmp & " "
    stmp = stmp & "CorX(" & Format$(INDICE - 1, "######0") & ")=" & frmt(CorZ(INDICE), 4)
    stmp = stmp & " "
    stmp = stmp & "CorY(" & Format$(INDICE - 1, "######0") & ")=" & frmt(corY(INDICE), 4)
    CHK0.lblINTESTAZIONE.Caption = stmp
    CHK0.PicCORROT.FillStyle = 0
    CHK0.PicCORROT.FillColor = QBColor(12)
    CHK0.PicCORROT.Circle (PicW / 2, PicH / 2), 2
    CHK0.PicCORROT.FillStyle = 1
  End If
  '
  'Il calcolo del raggio esterno � effettuato in RICERCA_MAX_MIN
  'INFORMAZIONI DA VISUALIZZARE
  stmp = " " & Codice & " (" & Format$(Np, "####0") & ")"
  pX1 = 0: Senso = 0
  pY1 = 0
  'GoSub VISUALIZZA_INFORMAZIONI_PROFILO
  '
  'Segnalazione della fine delle operazioni
  If (SiStampa = "Y") Then
    Printer.EndDoc
  Else
    WRITE_DIALOG ""
  End If
  '
Exit Sub

VISUALIZZA_XX_YY:
  Rmin = 3E+38
  iMIN = 0
  RaggioINTmin = 3E+38
  iINTmin = 0
    i = 1
      pX1 = (XX(i) - ZeroPezzoX) * ScalaX + PicW / 2
      pY1 = -(YY(i) - ZeroPezzoY) * ScalaY + PicH / 2
      pX2 = (XX(i + 1) - ZeroPezzoX) * ScalaX + PicW / 2
      pY2 = -(YY(i + 1) - ZeroPezzoY) * ScalaY + PicH / 2
      If SiStampa = "Y" Then
        If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
        End If
      Else
        If (pY1 <= CHK0.PicCORROT.ScaleHeight) And (pX1 <= CHK0.PicCORROT.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          CHK0.PicCORROT.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
        End If
      End If
    For i = 2 To NPF1 - 2 Step 2
      'COORD. X        COORD. Y
      R164 = XX(i + 0): R165 = YY(i + 0)
      R166 = XX(i + 1): R167 = YY(i + 1)
      R168 = XX(i + 2): R169 = YY(i + 2)
      'CONTROLLO REGOLARITA' PROFILO
      stmp = ""
      If (R166 < R164) Then stmp = stmp & " PNT : " & Format$(i + 1, "#####0")
      If (R168 < R166) Then stmp = stmp & " PNT : " & Format$(i + 2, "#####0")
      If Len(stmp) > 0 Then
        stmp = "PROFILE NOT CONGRUENT: " & Chr(13) & stmp
        StopRegieEvents
        MsgBox stmp, 48, "ERROR ON F1: START (" & Format$(i, "#####0") & ")"
        ResumeRegieEvents
      End If
      'DISTANZA PER TRE PUNTI
      R162 = Abs((R165 - R169) * (R164 - R166) + (R167 - R165) * (R164 - R168)) / Sqr((R165 - R169) * (R165 - R169) + (R164 - R168) * (R164 - R168))
      If R162 < 0.0005 Then
        'TRATTO RETTILINEO
        'G1 Y=R169 X=R168
        pX1 = (R164 - ZeroPezzoX) * ScalaX + PicW / 2
        pY1 = -(R165 - ZeroPezzoY) * ScalaY + PicH / 2
        pX2 = (R168 - ZeroPezzoX) * ScalaX + PicW / 2
        pY2 = -(R169 - ZeroPezzoY) * ScalaY + PicH / 2
        If SiStampa = "Y" Then
          If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
            Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
          End If
        Else
          If (pY1 <= CHK0.PicCORROT.ScaleHeight) And (pX1 <= CHK0.PicCORROT.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
            CHK0.PicCORROT.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
          End If
        End If
      Else
        'TRATTO CURVILINEO
        If R169 = R167 Then R169 = R167 + 0.0001
        If R165 = R167 Then R165 = R167 + 0.0001
        R160 = (R164 - R166) / (R167 - R165)
        R171 = (R166 - R168) / (R169 - R167)
        R172 = (R165 + R167) / 2 - R160 * (R164 + R166) / 2
        R173 = (R169 + R167) / 2 - R171 * (R168 + R166) / 2
        If R160 = R171 Then R160 = R171 + 0.0001
        'coordinate del centro
        R174 = (R173 - R172) / (R160 - R171)
        R170 = R160 * R174 + R172
        'raggio del cerchio
        R161 = Sqr((R174 - R166) * (R174 - R166) + (R170 - R167) * (R170 - R167))
        'AGGIORNAMENTO DEL VALORE DI RAGGIO MINIMO
        If Rmin > R161 Then Rmin = R161: iMIN = i
        'prodotto misto per senso di rotazione
        R160 = (R166 - R164) * (R169 - R165) - (R168 - R164) * (R167 - R165)
        'Calcolo angoli compresi tra 0 e 2 * Pigreco
        AngStart = CALCOLO_ANGOLO(R164 - R174, R165 - R170)
        AngEnd = CALCOLO_ANGOLO(R168 - R174, R169 - R170)
        If R160 > 0 Then
          If RaggioINTmin > R161 Then RaggioINTmin = R161: iINTmin = i
          'G3 Y=R169 X=R168 CR=R161
          If AngEnd >= AngStart Then
            Ang = AngEnd - AngStart
          Else
            Ang = 2 * PG - (AngStart - AngEnd)
          End If
          DeltaAng = Ang / NumeroArchi
          pX1 = R164: pY1 = R165
          For j = 1 To NumeroArchi - 1
            Ang = (AngStart + j * DeltaAng)
            pX2 = R174 + R161 * Cos(Ang): pY2 = R170 + R161 * Sin(Ang)
            'Adattamento nella finestra grafica
            pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
            pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
            pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
            pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
            'Traccio la linea
            If SiStampa = "Y" Then
              If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
                Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
              End If
            Else
              If (pY1 <= CHK0.PicCORROT.ScaleHeight) And (pX1 <= CHK0.PicCORROT.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
                CHK0.PicCORROT.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
              End If
            End If
            'Aggiorno Px1 e Py1
            pX1 = R174 + R161 * Cos(Ang): pY1 = R170 + R161 * Sin(Ang)
          Next j
          pX2 = R168: pY2 = R169
          'Adattamento nella finestra grafica
          pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
          pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
          pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
          pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
          'Traccio la linea
          If SiStampa = "Y" Then
            If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
              Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
            End If
          Else
            If (pY1 <= CHK0.PicCORROT.ScaleHeight) And (pX1 <= CHK0.PicCORROT.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
              CHK0.PicCORROT.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
            End If
          End If
        Else
          'G2 Y=R169 X=R168 CR=R161
          If AngStart >= AngEnd Then
            Ang = AngStart - AngEnd
          Else
            Ang = 2 * PG - (AngEnd - AngStart)
          End If
          DeltaAng = Ang / NumeroArchi
          pX1 = R164: pY1 = R165
          For j = 1 To NumeroArchi - 1
            Ang = (AngStart - j * DeltaAng)
            pX2 = R174 + R161 * Cos(Ang): pY2 = R170 + R161 * Sin(Ang)
            'Adattamento nella finestra grafica
            pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
            pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
            pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
            pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
            'Traccio la linea
            If SiStampa = "Y" Then
              If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
                Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
              End If
            Else
              If (pY1 <= CHK0.PicCORROT.ScaleHeight) And (pX1 <= CHK0.PicCORROT.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
                CHK0.PicCORROT.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
              End If
            End If
            'Aggiorno Px1 e Py1
            pX1 = R174 + R161 * Cos(Ang): pY1 = R170 + R161 * Sin(Ang)
          Next j
          pX2 = R168: pY2 = R169
          'Adattamento nella finestra grafica
          pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
          pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
          pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
          pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
          'Traccio la linea
          If SiStampa = "Y" Then
            If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
              Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
            End If
          Else
            If (pY1 <= CHK0.PicCORROT.ScaleHeight) And (pX1 <= CHK0.PicCORROT.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
              CHK0.PicCORROT.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
            End If
          End If
        End If
      End If
    Next
    'Fianco 2
    i = Np
      pX1 = (XX(i) - ZeroPezzoX) * ScalaX + PicW / 2
      pY1 = -(YY(i) - ZeroPezzoY) * ScalaY + PicH / 2
      pX2 = (XX(i - 1) - ZeroPezzoX) * ScalaX + PicW / 2
      pY2 = -(YY(i - 1) - ZeroPezzoY) * ScalaY + PicH / 2
      If SiStampa = "Y" Then
        If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
        End If
      Else
        If (pY1 <= CHK0.PicCORROT.ScaleHeight) And (pX1 <= CHK0.PicCORROT.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          CHK0.PicCORROT.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
        End If
      End If
    For i = Np - 1 To NPF1 Step -2
      'COORD. X        COORD. Y
      R164 = XX(i - 0): R165 = YY(i - 0)
      R166 = XX(i - 1): R167 = YY(i - 1)
      R168 = XX(i - 2): R169 = YY(i - 2)
      'CONTROLLO REGOLARITA PROFILO
      stmp = ""
      If (R166 > R164) Then stmp = stmp & " PNT : " & Format$(i - 1, "#####0")
      If (R168 > R166) Then stmp = stmp & " PNT : " & Format$(i - 2, "#####0")
      If Len(stmp) > 0 Then
        stmp = "PROFILE NOT CONGRUENT: " & Chr(13) & stmp
        StopRegieEvents
        MsgBox stmp, 48, "ERROR ON F2: START (" & Format$(i, "#####0") & ")"
        ResumeRegieEvents
      End If
      'DISTANZA PER TRE PUNTI
      R162 = Abs((R165 - R169) * (R164 - R166) + (R167 - R165) * (R164 - R168)) / Sqr((R165 - R169) * (R165 - R169) + (R164 - R168) * (R164 - R168))
      If R162 < 0.0005 Then
        'TRATTO RETTILINEO
        'G1 Y=R169 X=R168
        pX1 = (R164 - ZeroPezzoX) * ScalaX + PicW / 2
        pY1 = -(R165 - ZeroPezzoY) * ScalaY + PicH / 2
        pX2 = (R168 - ZeroPezzoX) * ScalaX + PicW / 2
        pY2 = -(R169 - ZeroPezzoY) * ScalaY + PicH / 2
        If SiStampa = "Y" Then
          If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
            Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
          End If
        Else
          If (pY1 <= CHK0.PicCORROT.ScaleHeight) And (pX1 <= CHK0.PicCORROT.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
            CHK0.PicCORROT.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
          End If
        End If
      Else
        'TRATTO CURVILINEO
        If R169 = R167 Then R169 = R167 + 0.0001
        If R165 = R167 Then R165 = R167 + 0.0001
        R160 = (R164 - R166) / (R167 - R165)
        R171 = (R166 - R168) / (R169 - R167)
        R172 = (R165 + R167) / 2 - R160 * (R164 + R166) / 2
        R173 = (R169 + R167) / 2 - R171 * (R168 + R166) / 2
        If R160 = R171 Then R160 = R171 + 0.0001
        'coordinate del centro
        R174 = (R173 - R172) / (R160 - R171)
        R170 = R160 * R174 + R172
        'raggio del cerchio
        R161 = Sqr((R174 - R166) * (R174 - R166) + (R170 - R167) * (R170 - R167))
        'AGGIORNAMENTO DEL VALORE DI RAGGIO MINIMO
        If Rmin > R161 Then Rmin = R161: iMIN = i
        'prodotto misto per senso di rotazione
        R160 = (R166 - R164) * (R169 - R165) - (R168 - R164) * (R167 - R165)
        'Calcolo angoli compresi tra 0 e 2 * Pigreco
        AngStart = CALCOLO_ANGOLO(R164 - R174, R165 - R170)
        AngEnd = CALCOLO_ANGOLO(R168 - R174, R169 - R170)
        If R160 > 0 Then
          'G3 Y=R169 X=R168 CR=R161
          If AngEnd >= AngStart Then
            Ang = AngEnd - AngStart
          Else
            Ang = 2 * PG - (AngStart - AngEnd)
          End If
          DeltaAng = Ang / NumeroArchi
          pX1 = R164: pY1 = R165
          For j = 1 To NumeroArchi - 1
            Ang = (AngStart + j * DeltaAng)
            pX2 = R174 + R161 * Cos(Ang): pY2 = R170 + R161 * Sin(Ang)
            'Adattamento nella finestra grafica
            pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
            pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
            pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
            pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
            'Traccio la linea
            If SiStampa = "Y" Then
              If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
                Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
              End If
            Else
              If (pY1 <= CHK0.PicCORROT.ScaleHeight) And (pX1 <= CHK0.PicCORROT.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
                CHK0.PicCORROT.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
              End If
            End If
            'Aggiorno Px1 e Py1
            pX1 = R174 + R161 * Cos(Ang): pY1 = R170 + R161 * Sin(Ang)
          Next j
          pX2 = R168: pY2 = R169
          'Adattamento nella finestra grafica
          pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
          pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
          pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
          pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
          'Traccio la linea
          If SiStampa = "Y" Then
            If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
              Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
            End If
          Else
            If (pY1 <= CHK0.PicCORROT.ScaleHeight) And (pX1 <= CHK0.PicCORROT.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
              CHK0.PicCORROT.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
            End If
          End If
        Else
          If RaggioINTmin > R161 Then RaggioINTmin = R161: iINTmin = i
          'If RaggioINTmax < R161 Then RaggioINTmax = R161: iINTmax = i
          'G2 Y=R169 X=R168 CR=R161
          If AngStart >= AngEnd Then
            Ang = AngStart - AngEnd
          Else
            Ang = 2 * PG - (AngEnd - AngStart)
          End If
          DeltaAng = Ang / NumeroArchi
          pX1 = R164: pY1 = R165
          For j = 1 To NumeroArchi - 1
            Ang = (AngStart - j * DeltaAng)
            pX2 = R174 + R161 * Cos(Ang): pY2 = R170 + R161 * Sin(Ang)
            'Adattamento nella finestra grafica
            pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
            pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
            pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
            pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
            'Traccio la linea
            If SiStampa = "Y" Then
              If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
                Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
              End If
            Else
              If (pY1 <= CHK0.PicCORROT.ScaleHeight) And (pX1 <= CHK0.PicCORROT.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
                CHK0.PicCORROT.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
              End If
            End If
            'Aggiorno Px1 e Py1
            pX1 = R174 + R161 * Cos(Ang): pY1 = R170 + R161 * Sin(Ang)
          Next j
          pX2 = R168: pY2 = R169
          'Adattamento nella finestra grafica
          pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
          pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
          pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
          pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
          'Traccio la linea
          If SiStampa = "Y" Then
            If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
              Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
            End If
          Else
            If (pY1 <= CHK0.PicCORROT.ScaleHeight) And (pX1 <= CHK0.PicCORROT.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
              CHK0.PicCORROT.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
            End If
          End If
        End If
      End If
    Next
Return

RICERCA_MAX_MIN:
  'Ricerca degli estremi
  iYmax = 1
  Xmax = -1E+308: Ymax = -1E+308: Xmin = 1E+308: Ymin = 1E+308
  For i = 1 To Np
    If XX(i) >= Xmax Then Xmax = XX(i)
    If YY(i) >= Ymax Then Ymax = YY(i): iYmax = i
    If XX(i) <= Xmin Then Xmin = XX(i)
    If YY(i) <= Ymin Then Ymin = YY(i)
  Next i
Return

PUNTI_PROFILO:
  For i = 1 To NPF1
    'LINEA TANGENTE
    pX1 = (XX(i) - DR * Sin(TT(i) * PG / 180) - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(YY(i) + DR * Cos(TT(i) * PG / 180) - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = (XX(i) + DR * Sin(TT(i) * PG / 180) - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(YY(i) - DR * Cos(TT(i) * PG / 180) - ZeroPezzoY) * ScalaY + PicH / 2
    If SiStampa = "Y" Then
      Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
    Else
      CHK0.PicCORROT.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
    End If
    'LINEA NORMALE
    pX1 = (XX(i) - DR * Cos(TT(i) * PG / 180) - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(YY(i) - DR * Sin(TT(i) * PG / 180) - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = (XX(i) + DR * Cos(TT(i) * PG / 180) - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(YY(i) + DR * Sin(TT(i) * PG / 180) - ZeroPezzoY) * ScalaY + PicH / 2
    If SiStampa = "Y" Then
      Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
    Else
      CHK0.PicCORROT.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
    End If
    'INDICE DEL PUNTO
    stmp = " " & Format$(i, "##0") & " "
    Select Case Senso
      Case 1
        If SiStampa = "Y" Then
          pX1 = pX1 - Printer.TextWidth(stmp)
          pY1 = pY1 - Printer.TextHeight(stmp)
          If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
            Printer.CurrentX = pX1
            Printer.CurrentY = pY1
            Printer.Print stmp
          End If
        Else
          pX1 = pX1 - CHK0.PicCORROT.TextWidth(stmp)
          pY1 = pY1 - CHK0.PicCORROT.TextHeight(stmp)
          If (pY1 <= CHK0.PicCORROT.ScaleHeight) And (pX1 <= CHK0.PicCORROT.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
            CHK0.PicCORROT.CurrentX = pX1
            CHK0.PicCORROT.CurrentY = pY1
            CHK0.PicCORROT.Print stmp
          End If
        End If
      Case -1
        If SiStampa = "Y" Then
          If (pY2 <= Printer.ScaleHeight) And (pX2 <= Printer.ScaleWidth) And (pX2 >= 0) And (pY2 >= 0) Then
            Printer.CurrentX = pX2
            Printer.CurrentY = pY2
            Printer.Print stmp
          End If
        Else
          If (pY2 <= CHK0.PicCORROT.ScaleHeight) And (pX2 <= CHK0.PicCORROT.ScaleWidth) And (pX2 >= 0) And (pY2 >= 0) Then
            CHK0.PicCORROT.CurrentX = pX2
            CHK0.PicCORROT.CurrentY = pY2
            CHK0.PicCORROT.Print stmp
          End If
        End If
    End Select
    'CERCHIO
    R174 = XX(i): R170 = YY(i): Raggio = DR: DeltaAng = 2 * PG / NumeroArchi
    pX1 = R174 + Raggio: pY1 = R170
    For j = 1 To NumeroArchi
      Ang = j * DeltaAng
      pX2 = R174 + Raggio * Cos(Ang): pY2 = R170 + Raggio * Sin(Ang)
      'Adattamento nella finestra grafica
      pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
      pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
      pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
      pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
      'Traccio la linea
      If SiStampa = "Y" Then
        If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
        End If
      Else
        If (pY1 <= CHK0.PicCORROT.ScaleHeight) And (pX1 <= CHK0.PicCORROT.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          CHK0.PicCORROT.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
        End If
      End If
      'Aggiorno Px1 e Py1
      pX1 = R174 + Raggio * Cos(Ang): pY1 = R170 + Raggio * Sin(Ang)
    Next j
  Next i
  For i = NPF1 + 1 To Np
    'LINEA TANGENTE
    pX1 = (XX(i) - DR * Sin(TT(i) * PG / 180) - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(YY(i) + DR * Cos(TT(i) * PG / 180) - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = (XX(i) + DR * Sin(TT(i) * PG / 180) - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(YY(i) - DR * Cos(TT(i) * PG / 180) - ZeroPezzoY) * ScalaY + PicH / 2
    If SiStampa = "Y" Then
      Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
    Else
      CHK0.PicCORROT.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
    End If
    'LINEA NORMALE
    pX1 = (XX(i) - DR * Cos(TT(i) * PG / 180) - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(YY(i) - DR * Sin(TT(i) * PG / 180) - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = (XX(i) + DR * Cos(TT(i) * PG / 180) - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(YY(i) + DR * Sin(TT(i) * PG / 180) - ZeroPezzoY) * ScalaY + PicH / 2
    If SiStampa = "Y" Then
      Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
    Else
      CHK0.PicCORROT.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
    End If
    'INDICE
    stmp = " " & Format$(i, "##0") & " "
    Select Case Senso
      Case 1
        If SiStampa = "Y" Then
          pY2 = pY2 - Printer.TextHeight(stmp)
          If (pY2 <= Printer.ScaleHeight) And (pX2 <= Printer.ScaleWidth) And (pX2 >= 0) And (pY2 >= 0) Then
            Printer.CurrentX = pX2
            Printer.CurrentY = pY2
            Printer.Print stmp
          End If
        Else
          pY2 = pY2 - CHK0.PicCORROT.TextHeight(stmp)
          If (pY2 <= CHK0.PicCORROT.ScaleHeight) And (pX2 <= CHK0.PicCORROT.ScaleWidth) And (pX2 >= 0) And (pY2 >= 0) Then
            CHK0.PicCORROT.CurrentX = pX2
            CHK0.PicCORROT.CurrentY = pY2
            CHK0.PicCORROT.Print stmp
          End If
        End If
      Case -1
        If SiStampa = "Y" Then
          pX1 = pX1 - Printer.TextWidth(stmp)
          If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
            Printer.CurrentX = pX1
            Printer.CurrentY = pY1
            Printer.Print stmp
          End If
        Else
          pX1 = pX1 - CHK0.PicCORROT.TextWidth(stmp)
          If (pY1 <= CHK0.PicCORROT.ScaleHeight) And (pX1 <= CHK0.PicCORROT.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
            CHK0.PicCORROT.CurrentX = pX1
            CHK0.PicCORROT.CurrentY = pY1
            CHK0.PicCORROT.Print stmp
          End If
        End If
    End Select
    'CERCHIO
    R174 = XX(i): R170 = YY(i): Raggio = DR: DeltaAng = 2 * PG / NumeroArchi
    pX1 = R174 + Raggio: pY1 = R170
    For j = 1 To NumeroArchi
      Ang = j * DeltaAng
      pX2 = R174 + Raggio * Cos(Ang): pY2 = R170 + Raggio * Sin(Ang)
      'Adattamento nella finestra grafica
      pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2: pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
      pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2: pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
      'Traccio la linea
      If SiStampa = "Y" Then
        If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
        End If
      Else
        If (pY1 <= CHK0.PicCORROT.ScaleHeight) And (pX1 <= CHK0.PicCORROT.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          CHK0.PicCORROT.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
        End If
      End If
      'Aggiorno Px1 e Py1
      pX1 = R174 + Raggio * Cos(Ang): pY1 = R170 + Raggio * Sin(Ang)
    Next j
  Next i
Return

NORMALI_PROFILO:
  For i = 1 To NPF1
    'LINEA NORMALE
    pX1 = (XX(i) - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(YY(i) - ZeroPezzoY) * ScalaY + PicH / 2
    If (TT(i) > 0) Then
      pX2 = (XX(i) + ScalaERRORE * DR * Cos(TT(i) * PG / 180) - ZeroPezzoX) * ScalaX + PicW / 2
      pY2 = -(YY(i) + ScalaERRORE * DR * Sin(TT(i) * PG / 180) - ZeroPezzoY) * ScalaY + PicH / 2
    Else
      pX2 = (XX(i) - ScalaERRORE * DR * Cos(TT(i) * PG / 180) - ZeroPezzoX) * ScalaX + PicW / 2
      pY2 = -(YY(i) - ScalaERRORE * DR * Sin(TT(i) * PG / 180) - ZeroPezzoY) * ScalaY + PicH / 2
    End If
    If (SiStampa = "Y") Then
      Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
    Else
      CHK0.PicCORROT.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
    End If
  Next i
  For i = NPF1 + 1 To Np
    'LINEA NORMALE
    pX1 = (XX(i) - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(YY(i) - ZeroPezzoY) * ScalaY + PicH / 2
    If (TT(i) < 0) Then
      pX2 = (XX(i) - ScalaERRORE * DR * Cos(TT(i) * PG / 180) - ZeroPezzoX) * ScalaX + PicW / 2
      pY2 = -(YY(i) - ScalaERRORE * DR * Sin(TT(i) * PG / 180) - ZeroPezzoY) * ScalaY + PicH / 2
    Else
      pX2 = (XX(i) + ScalaERRORE * DR * Cos(TT(i) * PG / 180) - ZeroPezzoX) * ScalaX + PicW / 2
      pY2 = -(YY(i) + ScalaERRORE * DR * Sin(TT(i) * PG / 180) - ZeroPezzoY) * ScalaY + PicH / 2
    End If
    If (SiStampa = "Y") Then
      Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
    Else
      CHK0.PicCORROT.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
    End If
  Next i
Return

CORREZIONI_PROFILO:
  For i = 1 To NPF1
    'LINEA NORMALE
    pX1 = (XX(i) - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(YY(i) - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = (XX(i) + ScalaERRORE * CorZ(i) - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(YY(i) - ScalaERRORE * corY(i) - ZeroPezzoY) * ScalaY + PicH / 2
    If (SiStampa = "Y") Then
      If (CHK0.List3(4).List(i - 1) = "X") Then
        'PUNTO CORRETTO
        Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(0)
      Else
        'PUNTO NON CORRETTO
        Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
        Printer.CurrentX = pX2
        Printer.CurrentY = pY2
        Printer.Print Format$(i - 1, "####0")
      End If
    Else
      If (CHK0.List3(4).List(i - 1) = "X") Then
        'PUNTO CORRETTO
        CHK0.PicCORROT.Line (pX1, pY1)-(pX2, pY2), QBColor(0)
      Else
        'PUNTO NON CORRETTO
        CHK0.PicCORROT.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
        CHK0.PicCORROT.CurrentX = pX2
        CHK0.PicCORROT.CurrentY = pY2
        CHK0.PicCORROT.Print Format$(i - 1, "####0")
      End If
    End If
  Next i
  For i = NPF1 + 1 To Np
    'LINEA NORMALE
    pX1 = (XX(i) - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(YY(i) - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = (XX(i) - ScalaERRORE * CorZ(i) - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(YY(i) - ScalaERRORE * corY(i) - ZeroPezzoY) * ScalaY + PicH / 2
    If (SiStampa = "Y") Then
      If (CHK0.List3(4).List(i - 1) = "X") Then
        'PUNTO CORRETTO
        Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(0)
      Else
        'PUNTO NON CORRETTO
        Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
        Printer.CurrentX = pX2
        Printer.CurrentY = pY2
        Printer.Print Format$(i - 1, "####0")
      End If
    Else
      If (CHK0.List3(4).List(i - 1) = "X") Then
        'PUNTO CORRETTO
        CHK0.PicCORROT.Line (pX1, pY1)-(pX2, pY2), QBColor(0)
      Else
        'PUNTO NON CORRETTO
        CHK0.PicCORROT.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
        CHK0.PicCORROT.CurrentX = pX2
        CHK0.PicCORROT.CurrentY = pY2
        CHK0.PicCORROT.Print Format$(i - 1, "####0")
      End If
    End If
  Next i
Return

INVILUPPO_CORREZIONI_PROFILO:
  For i = 1 To NPF1 - 1
    If (CHK0.List3(4).List(i - 1) = "X") Then
      pX1 = (XX(i) + ScalaERRORE * CorZ(i) - ZeroPezzoX) * ScalaX + PicW / 2
      pY1 = -(YY(i) - ScalaERRORE * corY(i) - ZeroPezzoY) * ScalaY + PicH / 2
    Else
      pX1 = (XX(i) - ZeroPezzoX) * ScalaX + PicW / 2
      pY1 = -(YY(i) - ZeroPezzoY) * ScalaY + PicH / 2
    End If
    If (CHK0.List3(4).List(i) = "X") Then
      pX2 = (XX(i + 1) + ScalaERRORE * CorZ(i + 1) - ZeroPezzoX) * ScalaX + PicW / 2
      pY2 = -(YY(i + 1) - ScalaERRORE * corY(i + 1) - ZeroPezzoY) * ScalaY + PicH / 2
    Else
      pX2 = (XX(i + 1) - ZeroPezzoX) * ScalaX + PicW / 2
      pY2 = -(YY(i + 1) - ZeroPezzoY) * ScalaY + PicH / 2
    End If
    If (SiStampa = "Y") Then
      Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(0)
    Else
      CHK0.PicCORROT.Line (pX1, pY1)-(pX2, pY2), QBColor(0)
    End If
  Next i
  For i = NPF1 To Np - 1
    If (CHK0.List3(4).List(i - 1) = "X") Then
      pX1 = (XX(i) - ScalaERRORE * CorZ(i) - ZeroPezzoX) * ScalaX + PicW / 2
      pY1 = -(YY(i) - ScalaERRORE * corY(i) - ZeroPezzoY) * ScalaY + PicH / 2
    Else
      pX1 = (XX(i) - ZeroPezzoX) * ScalaX + PicW / 2
      pY1 = -(YY(i) - ZeroPezzoY) * ScalaY + PicH / 2
    End If
    If (CHK0.List3(4).List(i) = "X") Then
      pX2 = (XX(i + 1) - ScalaERRORE * CorZ(i + 1) - ZeroPezzoX) * ScalaX + PicW / 2
      pY2 = -(YY(i + 1) - ScalaERRORE * corY(i + 1) - ZeroPezzoY) * ScalaY + PicH / 2
    Else
      pX2 = (XX(i + 1) - ZeroPezzoX) * ScalaX + PicW / 2
      pY2 = -(YY(i + 1) - ZeroPezzoY) * ScalaY + PicH / 2
    End If
    If (SiStampa = "Y") Then
      Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(0)
    Else
      CHK0.PicCORROT.Line (pX1, pY1)-(pX2, pY2), QBColor(0)
    End If
  Next i
Return

VISUALIZZA_INFORMAZIONI_PROFILO:
  'LE VARIABILI stmp E pY1 SONO IMPOSTATE
  sL(1, 1) = " D EXT":   sL(1, 2) = "= " & frmt(2 * Ymax, 4) & "mm /" & Format$(iYmax, "########0")
  sL(2, 1) = " D END":   sL(2, 2) = "= " & frmt(2 * YY(1), 4) & "mm"
  sL(3, 1) = " DR":      sL(3, 2) = "= " & frmt(DR, 8) & "mm /" & Format$(iDR, "########0")
  sL(4, 1) = " LF1":     sL(4, 2) = "= " & frmt(XX(NPF1) - XX(1), 8) & "mm"
  sL(5, 1) = " LF2":     sL(5, 2) = "= " & frmt(XX(Np) - XX(NPF1), 8) & "mm"
  sL(6, 1) = " LF1_CM":  sL(6, 2) = "= " & frmt(-XX(1), 8) & "mm"
  sL(7, 1) = " LF2_CM":  sL(7, 2) = "= " & frmt(XX(Np), 8) & "mm"
  sL(8, 1) = " LA":      sL(8, 2) = "= " & frmt(XX(Np - 1) - XX(2), 8) & "mm"
  sL(9, 1) = " dYmax":   sL(9, 2) = "= " & frmt(dYmax, 3) & "mm /" & Format$(IdYmax, "########0")
  If Len(CorrezioniMola) > 0 Then
    sL(10, 1) = CorrezioniMola:     sL(10, 2) = "= YES"
  End If
  '
  If SiStampa = "Y" Then
    TabX = Printer.TextWidth(sL(1, 1))
    For i = 1 To 10
      If TabX < Printer.TextWidth(sL(i, 1)) Then TabX = Printer.TextWidth(sL(i, 1))
    Next i
    TabXMax = Printer.TextWidth(sL(1, 2))
    For i = 1 To 10
      If TabXMax < Printer.TextWidth(sL(i, 2)) Then TabXMax = Printer.TextWidth(sL(i, 2))
    Next i
    TabXMax = TabX + TabXMax
  Else
    TabX = CHK0.PicCORROT.TextWidth(sL(1, 1))
    For i = 1 To 10
      If TabX < CHK0.PicCORROT.TextWidth(sL(i, 1)) Then TabX = CHK0.PicCORROT.TextWidth(sL(i, 1))
    Next i
    TabXMax = CHK0.PicCORROT.TextWidth(sL(1, 2))
    For i = 1 To 10
      If TabXMax < CHK0.PicCORROT.TextWidth(sL(i, 2)) Then TabXMax = CHK0.PicCORROT.TextWidth(sL(i, 2))
    Next i
    TabXMax = TabX + TabXMax
  End If
  If SiStampa = "Y" Then
    Printer.CurrentX = pX1 + Senso * TabXMax
    Printer.CurrentY = pY1
    Printer.Print stmp
    TabX = Printer.TextWidth(sL(1, 1))
    TabY = Printer.TextHeight(sL(1, 1))
    For i = 1 To 10
      If TabX < Printer.TextWidth(sL(i, 1)) Then TabX = Printer.TextWidth(sL(i, 1))
      Printer.CurrentX = pX1 + Senso * TabXMax
      Printer.CurrentY = pY1 + i * TabY
      Printer.Print sL(i, 1)
    Next i
    For i = 1 To 10
      Printer.CurrentX = pX1 + Senso * TabXMax + TabX
      Printer.CurrentY = pY1 + i * TabY
      Printer.Print sL(i, 2)
    Next i
  Else
    CHK0.PicCORROT.CurrentX = pX1 + Senso * TabXMax
    CHK0.PicCORROT.CurrentY = pY1
    CHK0.PicCORROT.Print stmp
    TabX = CHK0.PicCORROT.TextWidth(sL(1, 1))
    TabY = CHK0.PicCORROT.TextHeight(sL(1, 1))
    For i = 1 To 10
      If TabX < CHK0.PicCORROT.TextWidth(sL(i, 1)) Then TabX = CHK0.PicCORROT.TextWidth(sL(i, 1))
      CHK0.PicCORROT.CurrentX = pX1 + Senso * TabXMax
      CHK0.PicCORROT.CurrentY = pY1 + i * TabY
      CHK0.PicCORROT.Print sL(i, 1)
    Next i
    For i = 1 To 10
      CHK0.PicCORROT.CurrentX = pX1 + Senso * TabXMax + TabX
      CHK0.PicCORROT.CurrentY = pY1 + i * TabY
      CHK0.PicCORROT.Print sL(i, 2)
    Next i
  End If
Return

errVISUALIZZA_MOLA_CORRETTA:
  '
  If FreeFile > 0 Then Close
  MsgBox "ERROR: " & Error(Err), 48, "SUB: VISUALIZZA_MOLA_CORRETTA"
  'Exit Sub
  Resume Next

End Sub

Sub VISUALIZZA_CONTROLLO_ELICA(ByVal ScalaX As Double, ByVal ScalaY As Double, ByVal SiStampa As String)
'
Dim stmp          As String
Dim PicW          As Double
Dim PicH          As Double
Dim i             As Integer
Dim j             As Integer
Dim k             As Integer
Dim TipoCarattere As String
'
Dim pX1          As Double
Dim pY1          As Double
Dim pX2          As Double
Dim pY2          As Double
'
Dim PERCORSO    As String
Dim COORDINATE  As String
Dim RADICE      As String
Dim ELICA       As Double
'
'Nota : Errore fhb positivo indica passo lungo sia per eliche Dx che Sx
'       valori misf1 e misf2  positivi indicano materiale su fianco
'       Es. Misf1<0 e Misf2>0 il passo � lungo (grafici paralleli)
'       Es. Misf1>0 e Misf2>0 il passo F1 corto e passo F2 lungo(grafici convergenti)
'       cambiato segno di fhbf1 per rispettare convenzione
'       cambiato segno di misf2 solo per grafico
'
On Error GoTo errVISUALIZZA_CONTROLLO_ELICA
  '
  'LETTURA DEL PERCORSO DI RIFERIMENTO
  PERCORSO = LEP_STR("PERCORSO")
  PERCORSO = g_chOemPATH & "\" & PERCORSO
  If (Len(PERCORSO) > 0) Then
    'RICAVO IL NOME DEL ROTORE DAL PERCORSO
    COORDINATE = PERCORSO
    If (Len(COORDINATE) > 0) Then
      i = InStr(COORDINATE, "\")
      While (i <> 0)
        j = i
        i = InStr(i + 1, COORDINATE, "\")
      Wend
      'RICAVO IL NOME DEL ROTORE
      COORDINATE = Right$(COORDINATE, Len(COORDINATE) - j)
    End If
    'RICAVO IL PERCORSO DI LAVORO
    RADICE = PERCORSO
    i = InStr(RADICE, COORDINATE)
    RADICE = Left$(RADICE, i - 2)
  End If
  '
  If (Dir$(RADICE & "\RESULTS.SPF") = "") Then
    WRITE_DIALOG "Download lead inspection results!!!"
    Exit Sub
  End If
  '
  'AREA GRAFICA
  PicW = 280
  PicH = 140
  'TIPO DI CARATTERI
    If (LINGUA = "CH") Then
    TipoCarattere = "MS Song"
  ElseIf (LINGUA = "RU") Then
    TipoCarattere = "Arial Cyr"
  Else
    TipoCarattere = "Courier New"
  End If
  If (ScalaX <= 0) Then ScalaX = 1
  If (ScalaY <= 0) Then ScalaY = 1
  '
  'INIZIALIZZAZIONE
  If (SiStampa = "Y") Then
    Call ImpostaPrinter(TipoCarattere, 10)
    WRITE_DIALOG Printer.DeviceName & " PRINTING....."
    Printer.Orientation = 2
    Printer.ScaleMode = 6
  Else
    WRITE_DIALOG "DIPLAYING....."
    CHK0.Picture1.Cls
    CHK0.Picture1.ScaleWidth = PicW
    CHK0.Picture1.ScaleHeight = PicH
    CHK0.Picture1.FontName = TipoCarattere
    CHK0.Picture1.FontSize = 7
    CHK0.Picture1.Refresh
  End If
  '
  'LETTURA RISULTATI DEL CONTROLLO
  Dim Npunti      As Integer
  Dim FASCIA      As Double
  Dim PassoElica  As Double
  Dim NumeroDenti As Integer
  Dim Raggio(2)   As Double
  Dim MISF1()     As Double
  Dim MISF2()     As Double
  Dim MISF0()     As Double
  Dim PRINCIPI()  As Integer
  Dim Nprincipi   As Integer
  Dim kk          As Integer
  Dim sPRINCIPI   As String
  '
  Dim cln1 As Double
  Dim cln2 As Double
  Dim cln3 As Double
  Dim cln4 As Double
  '
  Open RADICE & "\RESULTS.SPF" For Input As 1
    'LETTURA PARAMETRI PRINCIPALI
    Input #1, PassoElica, NumeroDenti
    Input #1, Npunti, FASCIA
    Input #1, sPRINCIPI: sPRINCIPI = Trim$(sPRINCIPI)
    Input #1, Raggio(1), Raggio(2)
    'ESTRAZIONE PRINCIPI DALLA LISTA
    i = 0
    Do
      If (Len(sPRINCIPI) > 0) Then
        kk = InStr(sPRINCIPI, " ")
        If (kk > 0) Then
          'RICAVO IL PRINCIPO E VADO AVANTI
          ReDim Preserve PRINCIPI(i)
          PRINCIPI(i) = val(Left(sPRINCIPI, kk - 1))
          sPRINCIPI = Trim$(Right$(sPRINCIPI, Len(sPRINCIPI) - kk))
          i = i + 1
        Else
          'PRENDO L'ULTIMO VALORE
          ReDim Preserve PRINCIPI(i)
          PRINCIPI(i) = val(sPRINCIPI)
          'ESCO DAL CICLO
          Exit Do
        End If
      Else
        'ESCO DAL CICLO
        Exit Do
      End If
    Loop
    'INCLINAZIONE MOLA
    stmp = GetInfo("ELICA", "ERRORI_ASSIALI", Path_LAVORAZIONE_INI): stmp = UCase$(Trim$(stmp))
    If (stmp = "Y") Then
      ELICA = LEP("R[258]")
    Else
      ELICA = 90
    End If
    ELICA = ELICA / 180 * PG
    'NUMERO DEI PRINCIPI CONTROLLATI
    Nprincipi = (UBound(PRINCIPI) + 1)
    'RICAVO GLI INDICI DEI PRINCIPI CONTROLLATI
    If (Npunti <> 0) Then
      'RIGA VUOTA
      Input #1, stmp
      'DIMENSIONO I VETTORI DEGLI ERRORI
      Dim MISF1_MAX() As Double
      Dim MISF1_MED() As Double
      Dim MISF2_MAX() As Double
      Dim MISF2_MED() As Double
      Dim MISF0_MAX() As Double
      Dim MISF0_MED() As Double
      ReDim MISF1_MAX(Nprincipi): ReDim MISF1_MED(Nprincipi)
      ReDim MISF2_MAX(Nprincipi): ReDim MISF2_MED(Nprincipi)
      ReDim MISF0_MAX(Nprincipi): ReDim MISF0_MED(Nprincipi)
      ReDim MISF1(Npunti * Nprincipi): ReDim MISF2(Npunti * Nprincipi): ReDim MISF0(Npunti * Nprincipi)
      'LETTURA ERRORI
      j = 1: i = 1
      While (j <= Nprincipi)
        MISF1_MAX(j) = -1E+99: MISF2_MAX(j) = -1E+99: MISF0_MAX(j) = -1E+99
        MISF1_MED(j) = 0:      MISF2_MED(j) = 0:      MISF0_MED(j) = 0
        For k = 1 To Npunti
          Input #1, stmp
          Call SPLITTA_STRINGA(stmp, kk, cln1, cln2, cln3, cln4)
          If (kk = 3) Then
            MISF1(i) = cln1 * Sin(ELICA)
            If (MISF1(i) >= MISF1_MAX(j)) Then MISF1_MAX(j) = MISF1(i)
            If (Npunti > 0) Then MISF1_MED(j) = MISF1_MED(j) + MISF1(i) / Npunti
            MISF2(i) = cln2 * Sin(ELICA)
            If (MISF2(i) >= MISF2_MAX(j)) Then MISF2_MAX(j) = MISF2(i)
            If (Npunti > 0) Then MISF2_MED(j) = MISF2_MED(j) + MISF2(i) / Npunti
            MISF0(i) = cln3
            If (MISF0(i) >= MISF0_MAX(j)) Then MISF0_MAX(j) = MISF0(i)
            If (Npunti > 0) Then MISF0_MED(j) = MISF0_MED(j) + MISF0(i) / Npunti
          Else 'KK=2
            MISF1(i) = cln1 * Sin(ELICA)
            If (MISF1(i) >= MISF1_MAX(j)) Then MISF1_MAX(j) = MISF1(i)
            If (Npunti > 0) Then MISF1_MED(j) = MISF1_MED(j) + MISF1(i) / Npunti
            MISF2(i) = cln2 * Sin(ELICA)
            If (MISF2(i) >= MISF2_MAX(j)) Then MISF2_MAX(j) = MISF2(i)
            If (Npunti > 0) Then MISF2_MED(j) = MISF2_MED(j) + MISF2(i) / Npunti
          End If
          i = i + 1
        Next k
        j = j + 1
      Wend
    End If
  Close #1
  '
  'SPOSTAMENTO SUL VALORE MASSIMO DI OGNI PRINCIPIO
  stmp = GetInfo("ELICA", "OFFSET_MAX", Path_LAVORAZIONE_INI): stmp = UCase$(Trim$(stmp))
  If (stmp = "Y") Then
    i = 1
    For j = 1 To Nprincipi
      For k = 1 To Npunti
        MISF1(i) = MISF1(i) - MISF1_MAX(j)
        MISF2(i) = MISF2(i) - MISF2_MAX(j)
        MISF0(i) = MISF0(i) - MISF0_MAX(j)
        i = i + 1
      Next k
    Next j
  End If
  '
  'SPOSTAMENTO SUL VALORE MEDIO DI OGNI PRINCIPIO
  stmp = GetInfo("ELICA", "OFFSET_MED", Path_LAVORAZIONE_INI): stmp = UCase$(Trim$(stmp))
  If (stmp = "Y") Then
    i = 1
    For j = 1 To Nprincipi
      For k = 1 To Npunti
        MISF1(i) = MISF1(i) - MISF1_MED(j)
        MISF2(i) = MISF2(i) - MISF2_MED(j)
        MISF0(i) = MISF0(i) - MISF0_MED(j)
        i = i + 1
      Next k
    Next j
  End If
  '
  Dim sXh     As Double
  Dim sYh     As Double
  Dim sXhXh   As Double
  Dim sXhYh   As Double
  Dim B       As Double
  Dim A       As Double
  Dim qMAX    As Double
  Dim qMIN    As Double
  Dim FbF0()  As Double
  Dim FbF1()  As Double
  Dim FbF2()  As Double
  Dim fhbF0() As Double
  Dim fhbF1() As Double
  Dim fhbF2() As Double
  Dim ffbF0() As Double
  Dim ffbF1() As Double
  Dim ffbF2() As Double
  '
  ReDim FbF0(Nprincipi)
  ReDim FbF1(Nprincipi)
  ReDim FbF2(Nprincipi)
  '
  ReDim fhbF0(Nprincipi)
  ReDim fhbF1(Nprincipi)
  ReDim fhbF2(Nprincipi)
  '
  ReDim ffbF0(Nprincipi)
  ReDim ffbF1(Nprincipi)
  ReDim ffbF2(Nprincipi)
  '
  For j = 1 To Nprincipi
    '
    'INDICE DI RIFERIMENTO
    kk = Npunti * (j - 1)
    '
    '1) CONICITA' DI FONDO VANO
    sXh = MISF0(kk + 1): sYh = MISF0(kk + 1)
    For i = 1 To Npunti
      If MISF0(kk + i) <= sXh Then sXh = MISF0(kk + i)
      If MISF0(kk + i) >= sYh Then sYh = MISF0(kk + i)
    Next i
    FbF0(j) = sYh - sXh
    sXh = 0
    For i = 1 To Npunti
      sXh = sXh + (FASCIA / (Npunti - 1) * i)
    Next i
    sYh = 0
    For i = 1 To Npunti
      sYh = sYh + MISF0(kk + i)
    Next i
    sXhXh = 0
    For i = 1 To Npunti
      sXhXh = sXhXh + (FASCIA / (Npunti - 1) * i) ^ 2
    Next i
    sXhYh = 0
    For i = 1 To Npunti
      sXhYh = sXhYh + (FASCIA / (Npunti - 1) * i) * MISF0(kk + i)
    Next i
    'Coefficiente angolare della retta di regressione
    B = (Npunti * sXhYh - sXh * sYh) / (Npunti * sXhXh - sXh * sXh)
    fhbF0(j) = B * FASCIA
    'Costante minima delle rette parallele a quella di regressione
    qMIN = B * (FASCIA / (Npunti - 1) * 2) - MISF0(kk + 1)
    qMAX = B * (FASCIA / (Npunti - 1) * 2) - MISF0(kk + 1)
    For i = 1 To Npunti
      sXh = B * (FASCIA / (Npunti - 1) * i) - MISF0(kk + i)
      If sXh <= qMIN Then qMIN = sXh
      If sXh >= qMAX Then qMAX = sXh
    Next i
    'Calcolo della distanza massima dalla retta
    ffbF0(j) = Abs(qMIN - qMAX) / Sqr(1 + B * B)
    '
    '2) FIANCO 1
    sXh = MISF1(kk + 1): sYh = MISF1(kk + 1)
    For i = 1 To Npunti
      If MISF1(kk + i) <= sXh Then sXh = MISF1(kk + i)
      If MISF1(kk + i) >= sYh Then sYh = MISF1(kk + i)
    Next i
    FbF1(j) = sYh - sXh
    sXh = 0
    For i = 1 To Npunti
      sXh = sXh + (FASCIA / (Npunti - 1) * i)
    Next i
    sYh = 0
    For i = 1 To Npunti
      sYh = sYh + MISF1(kk + i)
    Next i
    sXhXh = 0
    For i = 1 To Npunti
      sXhXh = sXhXh + (FASCIA / (Npunti - 1) * i) ^ 2
    Next i
    sXhYh = 0
    For i = 1 To Npunti
      sXhYh = sXhYh + (FASCIA / (Npunti - 1) * i) * MISF1(kk + i)
    Next i
    'Coefficiente angolare della retta di regressione
    B = (Npunti * sXhYh - sXh * sYh) / (Npunti * sXhXh - sXh * sXh)
    fhbF1(j) = B * FASCIA
    'Costante minima delle rette parallele a quella di regressione
    qMIN = B * (FASCIA / (Npunti - 1) * 2) - MISF1(kk + 1)
    qMAX = B * (FASCIA / (Npunti - 1) * 2) - MISF1(kk + 1)
    For i = 1 To Npunti
      sXh = B * (FASCIA / (Npunti - 1) * i) - MISF1(kk + i)
      If sXh <= qMIN Then qMIN = sXh
      If sXh >= qMAX Then qMAX = sXh
    Next i
    'Calcolo della distanza massima dalla retta
    ffbF1(j) = Abs(qMIN - qMAX) / Sqr(1 + B * B)
    '
    '3) FIANCO 2
    sXh = MISF2(kk + 1): sYh = MISF2(kk + 1)
    For i = 1 To Npunti
      If MISF2(kk + i) <= sXh Then sXh = MISF2(kk + i)
      If MISF2(kk + i) >= sYh Then sYh = MISF2(kk + i)
    Next i
    FbF2(j) = sYh - sXh
    sXh = 0
    For i = 1 To Npunti
      sXh = sXh + (FASCIA / (Npunti - 1) * i)
    Next i
    sYh = 0
    For i = 1 To Npunti
      sYh = sYh + MISF2(kk + i)
    Next i
    sXhXh = 0
    For i = 1 To Npunti
      sXhXh = sXhXh + (FASCIA / (Npunti - 1) * i) ^ 2
    Next i
    sXhYh = 0
    For i = 1 To Npunti
      sXhYh = sXhYh + (FASCIA / (Npunti - 1) * i) * MISF2(kk + i)
    Next i
    'Coefficiente angolare della retta di regressione
    B = (Npunti * sXhYh - sXh * sYh) / (Npunti * sXhXh - sXh * sXh)
    fhbF2(j) = B * FASCIA
    'Costante minima delle rette parallele a quella di regressione
    qMIN = B * (FASCIA / (Npunti - 1) * 2) - MISF2(kk + 1)
    qMAX = B * (FASCIA / (Npunti - 1) * 2) - MISF2(kk + 1)
    For i = 1 To Npunti
      sXh = B * (FASCIA / (Npunti - 1) * i) - MISF2(kk + i)
      If sXh <= qMIN Then qMIN = sXh
      If sXh >= qMAX Then qMAX = sXh
    Next i
    'Calcolo della distanza massima dalla retta
    ffbF2(j) = Abs(qMIN - qMAX) / Sqr(1 + B * B)
  Next j
  '
  Dim OX          As Double
  Dim OY          As Double
  Dim jj          As Integer
  Dim Larghezza   As Double
  Dim Altezza     As Double
  Dim ZeroPezzoX  As Double
  Dim ZeroPezzoY  As Double
  Dim ColoreCurva As Integer
  Dim OFF_Y       As Double
  '
  Larghezza = (PicW / Nprincipi): Altezza = PicH
  '
  'VISUALIZZAZIONE DEL CONTROLLO ELICA PER PRINCIPIO CONTROLLATO
  For i = 1 To Nprincipi
    If (PRINCIPI(i - 1) <> 0) Then
      OY = Altezza
      OX = Larghezza / 2 + Larghezza * (i - 1)
      'FIANCO 1
      stmp = "F1 (" & Format$(PRINCIPI(i - 1), "##0") & ")"
      pX1 = (0 - ZeroPezzoX) * ScalaX + OX - Larghezza / 2
      pY1 = 0
      If (SiStampa = "Y") Then
        Printer.CurrentX = pX1
        Printer.CurrentY = pY1 + Printer.TextHeight("X") * 0
        Printer.Print stmp
      Else
        CHK0.Picture1.CurrentX = pX1
        CHK0.Picture1.CurrentY = pY1 + CHK0.Picture1.TextHeight("X") * 0
        CHK0.Picture1.Print stmp
      End If
      stmp = "Fb=" & Format$(FbF1(i) * 1000, "#######0")
      pX1 = (0 - ZeroPezzoX) * ScalaX + OX - Larghezza / 2
      pY1 = 0
      If (SiStampa = "Y") Then
        Printer.CurrentX = pX1
        Printer.CurrentY = pY1 + Printer.TextHeight("X") * 1
        Printer.Print stmp
      Else
        CHK0.Picture1.CurrentX = pX1
        CHK0.Picture1.CurrentY = pY1 + CHK0.Picture1.TextHeight("X") * 1
        CHK0.Picture1.Print stmp
      End If
      stmp = "fhb=" & Format$(-fhbF1(i) * 1000, "#######0")  'cambio segno 6.12.07 (+:passo lungo)
      pX1 = (0 - ZeroPezzoX) * ScalaX + OX - Larghezza / 2
      pY1 = 0
      If (SiStampa = "Y") Then
        Printer.CurrentX = pX1
        Printer.CurrentY = pY1 + Printer.TextHeight("X") * 2
        Printer.Print stmp
      Else
        CHK0.Picture1.CurrentX = pX1
        CHK0.Picture1.CurrentY = pY1 + CHK0.Picture1.TextHeight("X") * 2
        CHK0.Picture1.Print stmp
      End If
      stmp = "ffb=" & Format$(ffbF1(i) * 1000, "#######0")
      pX1 = (0 - ZeroPezzoX) * ScalaX + OX - Larghezza / 2
      pY1 = 0
      If (SiStampa = "Y") Then
        Printer.CurrentX = pX1
        Printer.CurrentY = pY1 + Printer.TextHeight("X") * 3
        Printer.Print stmp
      Else
        CHK0.Picture1.CurrentX = pX1
        CHK0.Picture1.CurrentY = pY1 + CHK0.Picture1.TextHeight("X") * 3
        CHK0.Picture1.Print stmp
      End If
      stmp = "fpz=" & Format$(-fhbF1(i) * 1000 * PassoElica / FASCIA, "#######0") 'cambio segno 6.12.07 (+:passo lungo)
      pX1 = (0 - ZeroPezzoX) * ScalaX + OX - Larghezza / 2
      pY1 = 0
      If (SiStampa = "Y") Then
        Printer.CurrentX = pX1
        Printer.CurrentY = pY1 + Printer.TextHeight("X") * 4
        Printer.Print stmp
      Else
        CHK0.Picture1.CurrentX = pX1
        CHK0.Picture1.CurrentY = pY1 + CHK0.Picture1.TextHeight("X") * 4
        CHK0.Picture1.Print stmp
      End If
      '
      'RIFERIMENTO VERTICALE x CONICITA' DI FONDO
      stmp = "-r+"
      pX1 = (0 - ZeroPezzoX) * ScalaX + OX
      pY1 = -(FASCIA - ZeroPezzoY) * ScalaY + OY
      If (SiStampa = "Y") Then
        Printer.CurrentX = pX1 - Printer.TextWidth(stmp) / 2
        Printer.CurrentY = pY1 - Printer.TextHeight(stmp)
        Printer.Print stmp
      Else
        CHK0.Picture1.CurrentX = pX1 - CHK0.Picture1.TextWidth(stmp) / 2
        CHK0.Picture1.CurrentY = pY1 - CHK0.Picture1.TextHeight(stmp)
        CHK0.Picture1.Print stmp
      End If
      stmp = "Fb=" & Format$(FbF0(i) * 1000, "#######0")
      pX1 = (0 - ZeroPezzoX) * ScalaX + OX
      pY1 = -(FASCIA - ZeroPezzoY) * ScalaY + OY
      If (SiStampa = "Y") Then
        Printer.CurrentX = pX1 - Printer.TextWidth(stmp) / 2
        Printer.CurrentY = pY1 - Printer.TextHeight(stmp) * 4
        Printer.Print stmp
      Else
        CHK0.Picture1.CurrentX = pX1 - CHK0.Picture1.TextWidth(stmp) / 2
        CHK0.Picture1.CurrentY = pY1 - CHK0.Picture1.TextHeight(stmp) * 4
        CHK0.Picture1.Print stmp
      End If
      stmp = "fhb=" & Format$(-fhbF0(i) * 1000, "#######0")
      pX1 = (0 - ZeroPezzoX) * ScalaX + OX
      pY1 = -(FASCIA - ZeroPezzoY) * ScalaY + OY
      If (SiStampa = "Y") Then
        Printer.CurrentX = pX1 - Printer.TextWidth(stmp) / 2
        Printer.CurrentY = pY1 - Printer.TextHeight(stmp) * 3
        Printer.Print stmp
      Else
        CHK0.Picture1.CurrentX = pX1 - CHK0.Picture1.TextWidth(stmp) / 2
        CHK0.Picture1.CurrentY = pY1 - CHK0.Picture1.TextHeight(stmp) * 3
        CHK0.Picture1.Print stmp
      End If
      stmp = "ffb=" & Format$(ffbF0(i) * 1000, "#######0")
      pX1 = (0 - ZeroPezzoX) * ScalaX + OX
      pY1 = -(FASCIA - ZeroPezzoY) * ScalaY + OY
      If (SiStampa = "Y") Then
        Printer.CurrentX = pX1 - Printer.TextWidth(stmp) / 2
        Printer.CurrentY = pY1 - Printer.TextHeight(stmp) * 2
        Printer.Print stmp
      Else
        CHK0.Picture1.CurrentX = pX1 - CHK0.Picture1.TextWidth(stmp) / 2
        CHK0.Picture1.CurrentY = pY1 - CHK0.Picture1.TextHeight(stmp) * 2
        CHK0.Picture1.Print stmp
      End If
      ColoreCurva = 0
        pX1 = (0 - ZeroPezzoX) * ScalaX + OX
        pY1 = -(0 - ZeroPezzoY) * ScalaY + OY
        pX2 = (0 - ZeroPezzoX) * ScalaX + OX
        pY2 = -(FASCIA - ZeroPezzoY) * ScalaY + OY
        If (SiStampa = "Y") Then
          If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
            Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
          End If
        Else
          If (pY1 <= CHK0.Picture1.ScaleHeight) And (pX1 <= CHK0.Picture1.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
            CHK0.Picture1.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
          End If
        End If
      '
      'ERRORE DI CONICITA' SUL FONDO
      ColoreCurva = 9
      For jj = 1 To Npunti - 1
        pX1 = (MISF0(jj + (i - 1) * Npunti) - ZeroPezzoX) * ScalaX + OX
        pY1 = -(FASCIA / (Npunti - 1) * (jj - 1) - ZeroPezzoY) * ScalaY + OY
        pX2 = (MISF0(jj + 1 + (i - 1) * Npunti) - ZeroPezzoX) * ScalaX + OX
        pY2 = -(FASCIA / (Npunti - 1) * (jj + 0) - ZeroPezzoY) * ScalaY + OY
        If (SiStampa = "Y") Then
          If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
            Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
          End If
        Else
          If (pY1 <= CHK0.Picture1.ScaleHeight) And (pX1 <= CHK0.Picture1.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
            CHK0.Picture1.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
          End If
        End If
      Next jj
      '
      'RIFERIMENTO VERTICALE F1
      stmp = "-0+"
      pX1 = (0 - ZeroPezzoX) * ScalaX + OX - Larghezza / 4
      pY1 = -(FASCIA - ZeroPezzoY) * ScalaY + OY
      If (SiStampa = "Y") Then
        Printer.CurrentX = pX1 - Printer.TextWidth(stmp) / 2
        Printer.CurrentY = pY1 - Printer.TextHeight(stmp)
        Printer.Print stmp
      Else
        CHK0.Picture1.CurrentX = pX1 - CHK0.Picture1.TextWidth(stmp) / 2
        CHK0.Picture1.CurrentY = pY1 - CHK0.Picture1.TextHeight(stmp)
        CHK0.Picture1.Print stmp
      End If
      ColoreCurva = 0
        pX1 = (0 - ZeroPezzoX) * ScalaX + OX - Larghezza / 4
        pY1 = -(0 - ZeroPezzoY) * ScalaY + OY
        pX2 = (0 - ZeroPezzoX) * ScalaX + OX - Larghezza / 4
        pY2 = -(FASCIA - ZeroPezzoY) * ScalaY + OY
        If (SiStampa = "Y") Then
          If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
            Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
          End If
        Else
          If (pY1 <= CHK0.Picture1.ScaleHeight) And (pX1 <= CHK0.Picture1.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
            CHK0.Picture1.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
          End If
        End If
      '
      'ERRORE DI PASSO DI F1
      ColoreCurva = 9
      For jj = 1 To Npunti - 1
        pX1 = (MISF1(jj + (i - 1) * Npunti) - ZeroPezzoX) * ScalaX + OX - Larghezza / 4
        pY1 = -(FASCIA / (Npunti - 1) * (jj - 1) - ZeroPezzoY) * ScalaY + OY
        pX2 = (MISF1(jj + 1 + (i - 1) * Npunti) - ZeroPezzoX) * ScalaX + OX - Larghezza / 4
        pY2 = -(FASCIA / (Npunti - 1) * (jj + 0) - ZeroPezzoY) * ScalaY + OY
        If (SiStampa = "Y") Then
          If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
            Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
          End If
        Else
          If (pY1 <= CHK0.Picture1.ScaleHeight) And (pX1 <= CHK0.Picture1.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
            CHK0.Picture1.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
          End If
        End If
      Next jj
      '
      'FIANCO 2
      stmp = "F2 (" & Format$(PRINCIPI(i - 1), "##0") & ")"
      pX1 = (0 - ZeroPezzoX) * ScalaX + OX
      pY1 = 0
      If (SiStampa = "Y") Then
        Printer.CurrentX = pX1
        Printer.CurrentY = pY1 + Printer.TextHeight("X") * 0
        Printer.Print stmp
      Else
        CHK0.Picture1.CurrentX = pX1
        CHK0.Picture1.CurrentY = pY1 + CHK0.Picture1.TextHeight("X") * 0
        CHK0.Picture1.Print stmp
      End If
      stmp = "Fb=" & Format$(FbF2(i) * 1000, "#######0")
      pX1 = (0 - ZeroPezzoX) * ScalaX + OX
      pY1 = 0
      If (SiStampa = "Y") Then
        Printer.CurrentX = pX1
        Printer.CurrentY = pY1 + Printer.TextHeight("X") * 1
        Printer.Print stmp
      Else
        CHK0.Picture1.CurrentX = pX1
        CHK0.Picture1.CurrentY = pY1 + CHK0.Picture1.TextHeight("X") * 1
        CHK0.Picture1.Print stmp
      End If
      stmp = "fhb=" & Format$(fhbF2(i) * 1000, "#######0")
      pX1 = (0 - ZeroPezzoX) * ScalaX + OX
      pY1 = 0
      If (SiStampa = "Y") Then
        Printer.CurrentX = pX1
        Printer.CurrentY = pY1 + Printer.TextHeight("X") * 2
        Printer.Print stmp
      Else
        CHK0.Picture1.CurrentX = pX1
        CHK0.Picture1.CurrentY = pY1 + CHK0.Picture1.TextHeight("X") * 2
        CHK0.Picture1.Print stmp
      End If
      stmp = "ffb=" & Format$(ffbF2(i) * 1000, "#######0")
      pX1 = (0 - ZeroPezzoX) * ScalaX + OX
      pY1 = 0
      If (SiStampa = "Y") Then
        Printer.CurrentX = pX1
        Printer.CurrentY = pY1 + Printer.TextHeight("X") * 3
        Printer.Print stmp
      Else
        CHK0.Picture1.CurrentX = pX1
        CHK0.Picture1.CurrentY = pY1 + CHK0.Picture1.TextHeight("X") * 3
        CHK0.Picture1.Print stmp
      End If
      stmp = "fpz=" & Format$(fhbF2(i) * 1000 * PassoElica / FASCIA, "#######0")
      pX1 = (0 - ZeroPezzoX) * ScalaX + OX
      pY1 = 0
      If (SiStampa = "Y") Then
        Printer.CurrentX = pX1
        Printer.CurrentY = pY1 + Printer.TextHeight("X") * 4
        Printer.Print stmp
      Else
        CHK0.Picture1.CurrentX = pX1
        CHK0.Picture1.CurrentY = pY1 + CHK0.Picture1.TextHeight("X") * 4
        CHK0.Picture1.Print stmp
      End If
      '
      'RIFERIMENTO VERTICALE F2
      stmp = "+0-"
      pX1 = (0 - ZeroPezzoX) * ScalaX + OX + Larghezza / 4
      pY1 = -(FASCIA - ZeroPezzoY) * ScalaY + OY
      If (SiStampa = "Y") Then
        Printer.CurrentX = pX1 - Printer.TextWidth(stmp) / 2
        Printer.CurrentY = pY1 - Printer.TextHeight(stmp)
        Printer.Print stmp
      Else
        CHK0.Picture1.CurrentX = pX1 - CHK0.Picture1.TextWidth(stmp) / 2
        CHK0.Picture1.CurrentY = pY1 - CHK0.Picture1.TextHeight(stmp)
        CHK0.Picture1.Print stmp
      End If
      ColoreCurva = 12
        pX1 = (0 - ZeroPezzoX) * ScalaX + OX + Larghezza / 4
        pY1 = -(0 - ZeroPezzoY) * ScalaY + OY
        pX2 = (0 - ZeroPezzoX) * ScalaX + OX + Larghezza / 4
        pY2 = -(FASCIA - ZeroPezzoY) * ScalaY + OY
        If (SiStampa = "Y") Then
          If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
            Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
          End If
        Else
          If (pY1 <= CHK0.Picture1.ScaleHeight) And (pX1 <= CHK0.Picture1.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
            CHK0.Picture1.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
          End If
        End If
      '
      'ERRORE DI PASSO DI F2
      ColoreCurva = 9
      For jj = 1 To Npunti - 1 'cambio segno di MisF2 solo per grafico 6.12.07
        pX1 = (-MISF2(jj + (i - 1) * Npunti) - ZeroPezzoX) * ScalaX + OX + Larghezza / 4
        pY1 = -(FASCIA / (Npunti - 1) * (jj - 1) - ZeroPezzoY) * ScalaY + OY
        pX2 = (-MISF2(jj + 1 + (i - 1) * Npunti) - ZeroPezzoX) * ScalaX + OX + Larghezza / 4
        pY2 = -(FASCIA / (Npunti - 1) * (jj + 0) - ZeroPezzoY) * ScalaY + OY
        If (SiStampa = "Y") Then
          If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
            Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
          End If
        Else
          If (pY1 <= CHK0.Picture1.ScaleHeight) And (pX1 <= CHK0.Picture1.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
            CHK0.Picture1.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
          End If
        End If
      Next jj
      '
    End If
    '
  Next i
  '
  'VISUALIZZAZIONE DEL CONTROLLO DIVISIONE: SOLO SE HA CONTROLLATO TUTTI I PRINCIPI
  If (NumeroDenti = Nprincipi) Then
    '
    ReDim Divf1(NumeroDenti + 1): ReDim Divf2(NumeroDenti + 1)
    '
    For i = 1 To NumeroDenti
      Divf1(i) = MISF1(1 + (i - 1) * Npunti)
      Divf2(i) = -MISF2(1 + (i - 1) * Npunti) '21.11.07 Cambiato il segno
    Next i
    Divf1(NumeroDenti + 1) = Divf1(1)
    Divf2(NumeroDenti + 1) = Divf2(1)
    '
    For i = 1 To NumeroDenti + 1
      Divf1(i) = Divf1(i) - Divf1(1)
      Divf2(i) = Divf2(i) - Divf2(1)
    Next i
    '
    Dim BASE As Double
    '
    OY = PicH / 4
    BASE = PicW / 4 / (NumeroDenti + 2)
    '
    'DIVISIONE FIANCO 1
    ZeroPezzoX = -BASE
    ZeroPezzoY = 0
    '
    OX = (PicW / 4) * 0
    '
    'ASSOLUTA
    stmp = " Fp1"
    pX1 = OX
    pY1 = -(0 - ZeroPezzoY) * ScalaX + OY
    If (SiStampa = "Y") Then
      Printer.CurrentX = pX1
      Printer.CurrentY = pY1 - Printer.TextHeight(stmp) / 2
      Printer.Print stmp
    Else
      CHK0.Picture1.CurrentX = pX1
      CHK0.Picture1.CurrentY = pY1 - CHK0.Picture1.TextHeight(stmp) / 2
      CHK0.Picture1.Print stmp
    End If
    '
    ColoreCurva = 0
    For i = 1 To NumeroDenti
      pX1 = (BASE * (i - 1) + BASE / 2 - ZeroPezzoX) + OX
      pY1 = -(Divf1(i) - ZeroPezzoY) * ScalaX + OY
      If (SiStampa = "Y") Then
        If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          Printer.Line (pX1 - BASE / 2, pY1)-(pX1 - BASE / 2, -(0 - ZeroPezzoY) * ScalaX + OY), QBColor(ColoreCurva)
          Printer.Line (pX1 - BASE / 2, pY1)-(pX1 + BASE / 2, pY1), QBColor(ColoreCurva)
          Printer.Line (pX1 + BASE / 2, pY1)-(pX1 + BASE / 2, -(0 - ZeroPezzoY) * ScalaX + OY), QBColor(ColoreCurva)
        End If
      Else
        If (pY1 <= CHK0.Picture1.ScaleHeight) And (pX1 <= CHK0.Picture1.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          CHK0.Picture1.Line (pX1 - BASE / 2, pY1)-(pX1 - BASE / 2, -(0 - ZeroPezzoY) * ScalaX + OY), QBColor(ColoreCurva)
          CHK0.Picture1.Line (pX1 - BASE / 2, pY1)-(pX1 + BASE / 2, pY1), QBColor(ColoreCurva)
          CHK0.Picture1.Line (pX1 + BASE / 2, pY1)-(pX1 + BASE / 2, -(0 - ZeroPezzoY) * ScalaX + OY), QBColor(ColoreCurva)
        End If
      End If
    Next i
    'RIFERIMENTO
    ColoreCurva = 12
    pX1 = (0 - ZeroPezzoX) + OX
    pY1 = -(0 - ZeroPezzoY) * ScalaX + OY
    pX2 = (BASE * NumeroDenti - ZeroPezzoX) + OX
    pY2 = -(0 - ZeroPezzoY) * ScalaX + OY
    If (SiStampa = "Y") Then
      If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
        Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
      End If
    Else
      If (pY1 <= CHK0.Picture1.ScaleHeight) And (pX1 <= CHK0.Picture1.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
        CHK0.Picture1.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
      End If
    End If
    '
    OX = (PicW / 4) * 1
    '
    'DIFFERENZA
    stmp = " fp1"
    pX1 = OX
    pY1 = -(0 - ZeroPezzoY) * ScalaX + OY
    If (SiStampa = "Y") Then
      Printer.CurrentX = pX1
      Printer.CurrentY = pY1 - Printer.TextHeight(stmp) / 2
      Printer.Print stmp
    Else
      CHK0.Picture1.CurrentX = pX1
      CHK0.Picture1.CurrentY = pY1 - CHK0.Picture1.TextHeight(stmp) / 2
      CHK0.Picture1.Print stmp
    End If
    '
    ColoreCurva = 9
    For i = 1 To NumeroDenti
      pX1 = (BASE * (i - 1) + BASE / 2 - ZeroPezzoX) + OX
      pY1 = -(Divf1(i + 1) - Divf1(i) - ZeroPezzoY) * ScalaX + OY
      If (SiStampa = "Y") Then
        If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          Printer.Line (pX1 - BASE / 2, pY1)-(pX1 - BASE / 2, -(0 - ZeroPezzoY) * ScalaX + OY), QBColor(ColoreCurva)
          Printer.Line (pX1 - BASE / 2, pY1)-(pX1 + BASE / 2, pY1), QBColor(ColoreCurva)
          Printer.Line (pX1 + BASE / 2, pY1)-(pX1 + BASE / 2, -(0 - ZeroPezzoY) * ScalaX + OY), QBColor(ColoreCurva)
        End If
      Else
        If (pY1 <= CHK0.Picture1.ScaleHeight) And (pX1 <= CHK0.Picture1.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          CHK0.Picture1.Line (pX1 - BASE / 2, pY1)-(pX1 - BASE / 2, -(0 - ZeroPezzoY) * ScalaX + OY), QBColor(ColoreCurva)
          CHK0.Picture1.Line (pX1 - BASE / 2, pY1)-(pX1 + BASE / 2, pY1), QBColor(ColoreCurva)
          CHK0.Picture1.Line (pX1 + BASE / 2, pY1)-(pX1 + BASE / 2, -(0 - ZeroPezzoY) * ScalaX + OY), QBColor(ColoreCurva)
        End If
      End If
    Next i
    'RIFERIMENTO
    ColoreCurva = 12
    pX1 = (0 - ZeroPezzoX) + OX
    pY1 = -(0 - ZeroPezzoY) * ScalaX + OY
    pX2 = (BASE * NumeroDenti - ZeroPezzoX) + OX
    pY2 = -(0 - ZeroPezzoY) * ScalaX + OY
    If (SiStampa = "Y") Then
      If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
        Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
      End If
    Else
      If (pY1 <= CHK0.Picture1.ScaleHeight) And (pX1 <= CHK0.Picture1.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
        CHK0.Picture1.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
      End If
    End If
    '
    'DIVISIONE FIANCO 2
    OX = (PicW / 4) * 2
    '
    'ASSOLUTA
    stmp = " Fp2"
    pX1 = OX
    pY1 = -(0 - ZeroPezzoY) * ScalaX + OY
    If (SiStampa = "Y") Then
      Printer.CurrentX = pX1
      Printer.CurrentY = pY1 - Printer.TextHeight(stmp) / 2
      Printer.Print stmp
    Else
      CHK0.Picture1.CurrentX = pX1
      CHK0.Picture1.CurrentY = pY1 - CHK0.Picture1.TextHeight(stmp) / 2
      CHK0.Picture1.Print stmp
    End If
    '
    ColoreCurva = 0
    For i = 1 To NumeroDenti
      pX1 = (BASE * (i - 1) + BASE / 2 - ZeroPezzoX) + OX
      pY1 = -(Divf2(i) - ZeroPezzoY) * ScalaX + OY
      If (SiStampa = "Y") Then
        If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          Printer.Line (pX1 - BASE / 2, pY1)-(pX1 - BASE / 2, -(0 - ZeroPezzoY) * ScalaX + OY), QBColor(ColoreCurva)
          Printer.Line (pX1 - BASE / 2, pY1)-(pX1 + BASE / 2, pY1), QBColor(ColoreCurva)
          Printer.Line (pX1 + BASE / 2, pY1)-(pX1 + BASE / 2, -(0 - ZeroPezzoY) * ScalaX + OY), QBColor(ColoreCurva)
        End If
      Else
        If (pY1 <= CHK0.Picture1.ScaleHeight) And (pX1 <= CHK0.Picture1.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          CHK0.Picture1.Line (pX1 - BASE / 2, pY1)-(pX1 - BASE / 2, -(0 - ZeroPezzoY) * ScalaX + OY), QBColor(ColoreCurva)
          CHK0.Picture1.Line (pX1 - BASE / 2, pY1)-(pX1 + BASE / 2, pY1), QBColor(ColoreCurva)
          CHK0.Picture1.Line (pX1 + BASE / 2, pY1)-(pX1 + BASE / 2, -(0 - ZeroPezzoY) * ScalaX + OY), QBColor(ColoreCurva)
        End If
      End If
    Next i
    'RIFERIMENTO
    ColoreCurva = 12
    pX1 = (0 - ZeroPezzoX) + OX
    pY1 = -(0 - ZeroPezzoY) * ScalaX + OY
    pX2 = (BASE * NumeroDenti - ZeroPezzoX) + OX
    pY2 = -(0 - ZeroPezzoY) * ScalaX + OY
    If (SiStampa = "Y") Then
      If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
        Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
      End If
    Else
      If (pY1 <= CHK0.Picture1.ScaleHeight) And (pX1 <= CHK0.Picture1.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
        CHK0.Picture1.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
      End If
    End If
    '
    OX = (PicW / 4) * 3
    '
    'DIFFERENZA
    stmp = " fp2"
    pX1 = OX
    pY1 = -(0 - ZeroPezzoY) * ScalaX + OY
    If (SiStampa = "Y") Then
      Printer.CurrentX = pX1
      Printer.CurrentY = pY1 - Printer.TextHeight(stmp) / 2
      Printer.Print stmp
    Else
      CHK0.Picture1.CurrentX = pX1
      CHK0.Picture1.CurrentY = pY1 - CHK0.Picture1.TextHeight(stmp) / 2
      CHK0.Picture1.Print stmp
    End If
    '
    ColoreCurva = 9
    For i = 1 To NumeroDenti
      pX1 = (BASE * (i - 1) + BASE / 2 - ZeroPezzoX) + OX
      pY1 = -(Divf2(i + 1) - Divf2(i) - ZeroPezzoY) * ScalaX + OY
      If (SiStampa = "Y") Then
        If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          Printer.Line (pX1 - BASE / 2, pY1)-(pX1 - BASE / 2, -(0 - ZeroPezzoY) * ScalaX + OY), QBColor(ColoreCurva)
          Printer.Line (pX1 - BASE / 2, pY1)-(pX1 + BASE / 2, pY1), QBColor(ColoreCurva)
          Printer.Line (pX1 + BASE / 2, pY1)-(pX1 + BASE / 2, -(0 - ZeroPezzoY) * ScalaX + OY), QBColor(ColoreCurva)
        End If
      Else
        If (pY1 <= CHK0.Picture1.ScaleHeight) And (pX1 <= CHK0.Picture1.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          CHK0.Picture1.Line (pX1 - BASE / 2, pY1)-(pX1 - BASE / 2, -(0 - ZeroPezzoY) * ScalaX + OY), QBColor(ColoreCurva)
          CHK0.Picture1.Line (pX1 - BASE / 2, pY1)-(pX1 + BASE / 2, pY1), QBColor(ColoreCurva)
          CHK0.Picture1.Line (pX1 + BASE / 2, pY1)-(pX1 + BASE / 2, -(0 - ZeroPezzoY) * ScalaX + OY), QBColor(ColoreCurva)
        End If
      End If
    Next i
    'RIFERIMENTO
    ColoreCurva = 12
    pX1 = (0 - ZeroPezzoX) + OX
    pY1 = -(0 - ZeroPezzoY) * ScalaX + OY
    pX2 = (BASE * NumeroDenti - ZeroPezzoX) + OX
    pY2 = -(0 - ZeroPezzoY) * ScalaX + OY
    If (SiStampa = "Y") Then
      If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
        Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
      End If
    Else
      If (pY1 <= CHK0.Picture1.ScaleHeight) And (pX1 <= CHK0.Picture1.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
        CHK0.Picture1.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
      End If
    End If
    '
  End If
  '
  If (SiStampa = "Y") Then Printer.EndDoc
  WRITE_DIALOG ""
  '
Exit Sub

errVISUALIZZA_CONTROLLO_ELICA:
  If FreeFile > 0 Then Close
  MsgBox "ERROR: " & Error(Err), 48, "SUB: VISUALIZZA_CONTROLLO_ELICA " & Format$(i)
  'Resume Next
  Exit Sub

End Sub

Sub VISUALIZZA_CORREZIONI_PROFILO_RAB(ByVal SiStampa As String)

Const Risoluzione = 0.000001
Dim TextFont As String

Dim XX() As Double
Dim YY() As Double
Dim TT() As Double

Dim Atmp As String * 255
Dim stmp As String
Dim ftmp As Single

Dim i    As Integer
Dim j    As Integer

Dim Fianco As Integer

Dim AngStart   As Double
Dim AngEnd     As Double
Dim QUADRANTE  As Integer
Dim Ang        As Double
Dim DeltaAng   As Double
Dim DeltaArco  As Double

Dim dx         As Double
Dim Dy         As Double
Dim DT         As Double

Dim NumeroArchi As Integer

Dim PicW As Single
Dim PicH As Single

Dim ColoreCurva As Integer

Dim pX1 As Single
Dim pY1 As Single
Dim pX2 As Single
Dim pY2 As Single

Dim Raggio As Single

Dim ZeroPezzoX As Single
Dim ZeroPezzoY As Single

Dim R160 As Double
Dim R161 As Double
Dim R162 As Double
Dim R163 As Double
Dim R164 As Double
Dim R165 As Double
Dim R166 As Double
Dim R167 As Double
Dim R168 As Double
Dim R169 As Double
Dim R170 As Double
Dim R171 As Double
Dim R172 As Double
Dim R173 As Double
Dim R174 As Double

Dim ScalaX As Single
Dim ScalaY As Single

Dim oldtext  As String

Dim Np As Integer

Dim SiSpezzata As Integer

On Error GoTo errVISUALIZZA_CORREZIONI_PROFILO_RAB
  '
  Np = 9
  '
  ReDim XX(2, Np)
  ReDim YY(2, Np)
  ReDim TT(2, Np)
  '
  Dim FF(2, 4)   As Single
  Dim RR(2, 4)   As Single
  Dim BB(2, 4)   As Single
  Dim XBB(2, 4)  As Double
  Dim YBB(2, 4)  As Double
  Dim XBB1(2, 4) As Double
  Dim YBB1(2, 4) As Double
  Dim XBB2(2, 4) As Double
  Dim YBB2(2, 4) As Double
  Dim Xm(2, 4)   As Double
  Dim Ym(2, 4)   As Double
  Dim mp(2, 4)   As Double
  Dim M1         As Double
  Dim M2         As Double
  '
  Dim RaggioBombatura As String
  '
  OEMX.OEM1chkINPUT(0).Visible = False
  OEMX.OEM1chkINPUT(1).Visible = False
  '
  OEMX.OEM1lblOX.Visible = False
  OEMX.OEM1lblOY.Visible = False
  OEMX.OEM1txtOX.Visible = False
  OEMX.OEM1txtOY.Visible = False
  '
  SiSpezzata = 1
  'Lettura del valore di scala
  stmp = GetInfo("DIS0", "ScalaCORREZIONIX", Path_LAVORAZIONE_INI)
  ScalaX = val(stmp)
  If ScalaX = 0 Then ScalaX = 500
  OEMX.txtINP(0) = frmt(ScalaX, 4)
  stmp = GetInfo("DIS0", "ScalaCORREZIONIY", Path_LAVORAZIONE_INI)
  ScalaY = val(stmp)
  If ScalaY = 0 Then ScalaY = 2
  OEMX.txtINP(1) = frmt(ScalaY, 4)
  'Lettura del numero di ARCHI per i cerchi
  stmp = GetInfo("DIS0", "NumeroArchi", Path_LAVORAZIONE_INI)
  NumeroArchi = val(stmp)
  If NumeroArchi = 0 Then NumeroArchi = 50
  'Lettura
  stmp = GetInfo("DIS0", "DT", Path_LAVORAZIONE_INI)
  DT = val(stmp)
  If DT = 0 Then DT = 5
  'Acquisizione area grafica
  stmp = GetInfo("DIS0", "PicW", Path_LAVORAZIONE_INI)
  If Len(stmp) > 0 Then PicW = val(stmp) Else PicW = 190
  stmp = GetInfo("DIS0", "PicH", Path_LAVORAZIONE_INI)
  If Len(stmp) > 0 Then PicH = val(stmp) Else PicH = 95
      If (LINGUA = "CH") Then
    TextFont = "MS Song"
  ElseIf (LINGUA = "RU") Then
    TextFont = "Arial Cyr"
  Else
    TextFont = "Courier New"
  End If
  If (SiStampa = "Y") Then
    For i = 1 To 2
      Printer.FontName = TextFont
      Printer.FontSize = 10
      Printer.FontBold = True
    Next i
  Else
    For i = 1 To 2
      OEMX.OEM1PicCALCOLO.FontName = TextFont
      OEMX.OEM1PicCALCOLO.FontSize = 8
      OEMX.OEM1PicCALCOLO.FontBold = True
    Next i
  End If
  '
  i = LoadString(g_hLanguageLibHandle, 1000, Atmp, 255)
  If i > 0 Then OEMX.lblINP(0).Caption = Left$(Atmp, i)
  i = LoadString(g_hLanguageLibHandle, 1001, Atmp, 255)
  If i > 0 Then OEMX.lblINP(1).Caption = Left$(Atmp, i)
  OEMX.lblINP(0).Visible = True
  OEMX.txtINP(0).Visible = True
  OEMX.lblINP(1).Visible = True
  OEMX.txtINP(1).Visible = True
  OEMX.OEM1txtHELP.Visible = False
  OEMX.OEM1Command2.Visible = False
  OEMX.OEM1fraOpzioni.Visible = False
  OEMX.txtINP(0).SetFocus
  '
  'Preparazione grafico
  If (SiStampa = "Y") Then
    WRITE_DIALOG OEMX.OEM1frameCalcolo.Caption & " --> " & Printer.DeviceName
    Printer.ScaleMode = 6
  Else
    OEMX.OEM1PicCALCOLO.Cls
    OEMX.OEM1PicCALCOLO.ScaleWidth = PicW
    OEMX.OEM1PicCALCOLO.ScaleHeight = PicH
    OEMX.OEM1PicCALCOLO.Refresh
  End If
  '
    
  'INIZIO FIANCHI SAP
  ParProfiloRot.EAP_F1(0) = val(Lp("R[269]")) / 2
  ParProfiloRot.EAP_F2(0) = val(Lp("R[284]")) / 2
  'SEZIONE 1
  ParProfiloRot.EAP_F1(1) = val(Lp("R[270]")) / 2
  ParProfiloRot.EAP_F2(1) = val(Lp("R[285]")) / 2
  ParProfiloRot.RR_F1(1) = val(Lp("R[271]")) / 2
  ParProfiloRot.RR_F2(1) = val(Lp("R[286]")) / 2
  ParProfiloRot.BB_F1(1) = val(Lp("R[272]")) / 2
  ParProfiloRot.BB_F2(1) = val(Lp("R[287]")) / 2
  'SEZIONE 2
  ParProfiloRot.EAP_F1(2) = val(Lp("R[273]")) / 2
  ParProfiloRot.EAP_F2(2) = val(Lp("R[288]")) / 2
  ParProfiloRot.RR_F1(2) = val(Lp("R[274]")) / 2
  ParProfiloRot.RR_F2(2) = val(Lp("R[289]")) / 2
  ParProfiloRot.BB_F1(2) = val(Lp("R[275]")) / 2
  ParProfiloRot.BB_F2(2) = val(Lp("R[290]")) / 2
  'SEZIONE 3
  ParProfiloRot.EAP_F1(3) = val(Lp("R[276]")) / 2
  ParProfiloRot.EAP_F2(3) = val(Lp("R[291]")) / 2
  ParProfiloRot.RR_F1(3) = val(Lp("R[277]")) / 2
  ParProfiloRot.RR_F2(3) = val(Lp("R[292]")) / 2
  ParProfiloRot.BB_F1(3) = val(Lp("R[278]")) / 2
  ParProfiloRot.BB_F2(3) = val(Lp("R[293]")) / 2
  'SEZIONE 4
  ParProfiloRot.EAP_F1(4) = val(Lp("R[279]")) / 2
  ParProfiloRot.EAP_F2(4) = val(Lp("R[294]")) / 2
  ParProfiloRot.RR_F1(4) = val(Lp("R[280]")) / 2
  ParProfiloRot.RR_F2(4) = val(Lp("R[295]")) / 2
  ParProfiloRot.BB_F1(4) = val(Lp("R[281]")) / 2
  ParProfiloRot.BB_F2(4) = val(Lp("R[296]")) / 2
  
  'Acquisizione valori dalla finestra OEM
  Fianco = 1
    FF(Fianco, 1) = (val(Lp("R[270]")) - val(Lp("R[269]"))) / 2
    If FF(Fianco, 1) > 0 Then
      RR(Fianco, 1) = val(Lp("R[271]"))
      BB(Fianco, 1) = val(Lp("R[272]"))
    Else
      FF(Fianco, 1) = 0: RR(Fianco, 1) = 0: BB(Fianco, 1) = 0
    End If
    FF(Fianco, 2) = (val(Lp("R[273]")) - val(Lp("R[270]"))) / 2
    If FF(Fianco, 2) > 0 Then
      RR(Fianco, 2) = val(Lp("R[274]"))
      BB(Fianco, 2) = val(Lp("R[275]"))
    Else
      FF(Fianco, 2) = 0: RR(Fianco, 2) = 0: BB(Fianco, 2) = 0
    End If
    FF(Fianco, 3) = (val(Lp("R[276]")) - val(Lp("R[273]"))) / 2
    If FF(Fianco, 3) > 0 Then
      RR(Fianco, 3) = val(Lp("R[277]"))
      BB(Fianco, 3) = val(Lp("R[278]"))
    Else
      FF(Fianco, 3) = 0: RR(Fianco, 3) = 0: BB(Fianco, 3) = 0
    End If
    FF(Fianco, 4) = (val(Lp("R[279]")) - val(Lp("R[276]"))) / 2
    If FF(Fianco, 4) > 0 Then
      RR(Fianco, 4) = val(Lp("R[280]"))
      BB(Fianco, 4) = val(Lp("R[281]"))
    Else
      FF(Fianco, 4) = 0: RR(Fianco, 4) = 0: BB(Fianco, 4) = 0
    End If
  Fianco = 2
    FF(Fianco, 1) = (val(Lp("R[285]")) - val(Lp("R[284]"))) / 2
    If FF(Fianco, 1) > 0 Then
      RR(Fianco, 1) = val(Lp("R[286]"))
      BB(Fianco, 1) = val(Lp("R[287]"))
    Else
      FF(Fianco, 1) = 0: RR(Fianco, 1) = 0: BB(Fianco, 1) = 0
    End If
    FF(Fianco, 2) = (val(Lp("R[288]")) - val(Lp("R[285]"))) / 2
    If FF(Fianco, 2) > 0 Then
      RR(Fianco, 2) = val(Lp("R[289]"))
      BB(Fianco, 2) = val(Lp("R[290]"))
    Else
      FF(Fianco, 2) = 0: RR(Fianco, 2) = 0: BB(Fianco, 2) = 0
    End If
    FF(Fianco, 3) = (val(Lp("R[291]")) - val(Lp("R[285]"))) / 2
    If FF(Fianco, 3) > 0 Then
      RR(Fianco, 3) = val(Lp("R[292]"))
      BB(Fianco, 3) = val(Lp("R[293]"))
    Else
      FF(Fianco, 3) = 0: RR(Fianco, 3) = 0: BB(Fianco, 3) = 0
    End If
    FF(Fianco, 4) = (val(Lp("R[294]")) - val(Lp("R[291]"))) / 2
    If FF(Fianco, 4) > 0 Then
      RR(Fianco, 4) = val(Lp("R[295]"))
      BB(Fianco, 4) = val(Lp("R[296]"))
    Else
      FF(Fianco, 4) = 0: RR(Fianco, 4) = 0: BB(Fianco, 4) = 0
    End If
  '
  Dim POSIZIONE_INIZIALE_Y(2) As Double
  If val(Lp("R[269]")) > val(Lp("R[284]")) Then
    POSIZIONE_INIZIALE_Y(2) = 0
    POSIZIONE_INIZIALE_Y(1) = (val(Lp("R[269]")) - val(Lp("R[284]"))) / 2
  End If
  If val(Lp("R[284]")) > val(Lp("R[269]")) Then
    POSIZIONE_INIZIALE_Y(1) = 0
    POSIZIONE_INIZIALE_Y(2) = (val(Lp("R[284]")) - val(Lp("R[269]"))) / 2
  End If
  '
  'Costruzione del profilo per punti
  For Fianco = 1 To 2
    'Coordinate dei punti principali della spezzata
    XX(Fianco, 1) = 0
    YY(Fianco, 1) = 0 + POSIZIONE_INIZIALE_Y(Fianco)
    XX(Fianco, 3) = RR(Fianco, 1)
    YY(Fianco, 3) = FF(Fianco, 1) + POSIZIONE_INIZIALE_Y(Fianco)
    XX(Fianco, 5) = RR(Fianco, 1) + RR(Fianco, 2)
    YY(Fianco, 5) = FF(Fianco, 1) + FF(Fianco, 2) + POSIZIONE_INIZIALE_Y(Fianco)
    XX(Fianco, 7) = RR(Fianco, 1) + RR(Fianco, 2) + RR(Fianco, 3)
    YY(Fianco, 7) = FF(Fianco, 1) + FF(Fianco, 2) + FF(Fianco, 3) + POSIZIONE_INIZIALE_Y(Fianco)
    XX(Fianco, 9) = RR(Fianco, 1) + RR(Fianco, 2) + RR(Fianco, 3) + RR(Fianco, 4)
    YY(Fianco, 9) = FF(Fianco, 1) + FF(Fianco, 2) + FF(Fianco, 3) + FF(Fianco, 4) + POSIZIONE_INIZIALE_Y(Fianco)
    'Coordinate dei punti medi
    Xm(Fianco, 1) = RR(Fianco, 1) / 2
    Ym(Fianco, 1) = FF(Fianco, 1) / 2 + POSIZIONE_INIZIALE_Y(Fianco)
    Xm(Fianco, 2) = RR(Fianco, 1) + RR(Fianco, 2) / 2
    Ym(Fianco, 2) = FF(Fianco, 1) + FF(Fianco, 2) / 2 + POSIZIONE_INIZIALE_Y(Fianco)
    Xm(Fianco, 3) = RR(Fianco, 1) + RR(Fianco, 2) + RR(Fianco, 3) / 2
    Ym(Fianco, 3) = FF(Fianco, 1) + FF(Fianco, 2) + FF(Fianco, 3) / 2 + POSIZIONE_INIZIALE_Y(Fianco)
    Xm(Fianco, 4) = RR(Fianco, 1) + RR(Fianco, 2) + RR(Fianco, 3) + RR(Fianco, 4) / 2
    Ym(Fianco, 4) = FF(Fianco, 1) + FF(Fianco, 2) + FF(Fianco, 3) + FF(Fianco, 4) / 2 + POSIZIONE_INIZIALE_Y(Fianco)
    'Calcolo dei coefficienti angolari perpendicolari
    For i = 1 To 4
      If FF(Fianco, i) <> 0 Then mp(Fianco, i) = -RR(Fianco, i) / FF(Fianco, i)
    Next i
    For i = 1 To 4
      'PRIMA SOLUZIONE
      XBB1(Fianco, i) = Xm(Fianco, i) + BB(Fianco, i) * Sqr(1 / (1 + mp(Fianco, i) ^ 2))
      YBB1(Fianco, i) = Ym(Fianco, i) + BB(Fianco, i) * Sqr(1 / (1 + mp(Fianco, i) ^ 2)) * mp(Fianco, i)
      'SECONDA SOLUZIONE
      XBB2(Fianco, i) = Xm(Fianco, i) - BB(Fianco, i) * Sqr(1 / (1 + mp(Fianco, i) ^ 2))
      YBB2(Fianco, i) = Ym(Fianco, i) - BB(Fianco, i) * Sqr(1 / (1 + mp(Fianco, i) ^ 2)) * mp(Fianco, i)
    Next i
    'SCELTA DELLA SOLUZIONE ------------------------------------------------------------------------------------------------------------------------
    'PRIMO PUNTO DI BOMBATURA
    'PRODOTTI MISTI
    M1 = (XBB1(Fianco, 1) - XX(Fianco, 1)) * (YY(Fianco, 3) - YY(Fianco, 1)) - (YBB1(Fianco, 1) - YY(Fianco, 1)) * (XX(Fianco, 3) - XX(Fianco, 1))
    M2 = (XBB2(Fianco, 1) - XX(Fianco, 1)) * (YY(Fianco, 3) - YY(Fianco, 1)) - (YBB2(Fianco, 1) - YY(Fianco, 1)) * (XX(Fianco, 3) - XX(Fianco, 1))
    'PRENDO COME SOLUZIONE QUELLA CON LO STESSO SEGNO DI BB
    If BB(Fianco, 1) * M1 >= 0 Then
      XBB(Fianco, 1) = XBB1(Fianco, 1): YBB(Fianco, 1) = YBB1(Fianco, 1)
      XX(Fianco, 2) = XBB1(Fianco, 1): YY(Fianco, 2) = YBB1(Fianco, 1)
    ElseIf BB(Fianco, 1) * M2 >= 0 Then
      XBB(Fianco, 1) = XBB2(Fianco, 1): YBB(Fianco, 1) = YBB2(Fianco, 1)
      XX(Fianco, 2) = XBB2(Fianco, 1):  YY(Fianco, 2) = YBB2(Fianco, 1)
    Else
      WRITE_DIALOG "ERROR ON CROWNING 1 EVALUATION"
    End If
    'SECONDO PUNTO DI BOMBATURA
    'PRODOTTI MISTI
    M1 = (XBB1(Fianco, 2) - XX(Fianco, 3)) * (YY(Fianco, 5) - YY(Fianco, 3)) - (YBB1(Fianco, 2) - YY(Fianco, 3)) * (XX(Fianco, 5) - XX(Fianco, 3))
    M2 = (XBB2(Fianco, 2) - XX(Fianco, 3)) * (YY(Fianco, 5) - YY(Fianco, 3)) - (YBB2(Fianco, 2) - YY(Fianco, 3)) * (XX(Fianco, 5) - XX(Fianco, 3))
    'PRENDO COME SOLUZIONE QUELLA CON LO STESSO SEGNO DI BB
    If BB(Fianco, 2) * M1 >= 0 Then
      XBB(Fianco, 2) = XBB1(Fianco, 2): YBB(Fianco, 2) = YBB1(Fianco, 2)
      XX(Fianco, 4) = XBB1(Fianco, 2): YY(Fianco, 4) = YBB1(Fianco, 2)
    ElseIf BB(Fianco, 2) * M2 >= 0 Then
      XBB(Fianco, 2) = XBB2(Fianco, 2): YBB(Fianco, 2) = YBB2(Fianco, 2)
      XX(Fianco, 4) = XBB2(Fianco, 2): YY(Fianco, 4) = YBB2(Fianco, 2)
    Else
      WRITE_DIALOG "ERROR ON CROWNING 2 EVALUATION"
    End If
    'TERZO PUNTO DI BOMBATURA
    'PRODOTTI MISTI
    M1 = (XBB1(Fianco, 3) - XX(Fianco, 5)) * (YY(Fianco, 7) - YY(Fianco, 5)) - (YBB1(Fianco, 3) - YY(Fianco, 5)) * (XX(Fianco, 7) - XX(Fianco, 5))
    M2 = (XBB2(Fianco, 3) - XX(Fianco, 5)) * (YY(Fianco, 7) - YY(Fianco, 5)) - (YBB2(Fianco, 3) - YY(Fianco, 5)) * (XX(Fianco, 7) - XX(Fianco, 5))
    'PRENDO COME SOLUZIONE QUELLA CON LO STESSO SEGNO DI BB
    If BB(Fianco, 3) * M1 >= 0 Then
      XBB(Fianco, 3) = XBB1(Fianco, 3): YBB(Fianco, 3) = YBB1(Fianco, 3)
      XX(Fianco, 6) = XBB1(Fianco, 3): YY(Fianco, 6) = YBB1(Fianco, 3)
    ElseIf BB(Fianco, 3) * M2 >= 0 Then
      XBB(Fianco, 3) = XBB2(Fianco, 3): YBB(Fianco, 3) = YBB2(Fianco, 3)
      XX(Fianco, 6) = XBB2(Fianco, 3): YY(Fianco, 6) = YBB2(Fianco, 3)
    Else
      WRITE_DIALOG "ERROR ON CROWNING 3 EVALUATION"
    End If
    'QUARTO PUNTO DI BOMBATURA
    'PRODOTTI MISTI
    M1 = (XBB1(Fianco, 4) - XX(Fianco, 7)) * (YY(Fianco, 9) - YY(Fianco, 7)) - (YBB1(Fianco, 4) - YY(Fianco, 7)) * (XX(Fianco, 9) - XX(Fianco, 7))
    M2 = (XBB2(Fianco, 4) - XX(Fianco, 7)) * (YY(Fianco, 9) - YY(Fianco, 7)) - (YBB2(Fianco, 4) - YY(Fianco, 7)) * (XX(Fianco, 9) - XX(Fianco, 7))
    'PRENDO COME SOLUZIONE QUELLA CON LO STESSO SEGNO DI BB
    If BB(Fianco, 4) * M1 >= 0 Then
      XBB(Fianco, 4) = XBB1(Fianco, 4): YBB(Fianco, 4) = YBB1(Fianco, 4)
      XX(Fianco, 8) = XBB1(Fianco, 4): YY(Fianco, 8) = YBB1(Fianco, 4)
    ElseIf BB(Fianco, 4) * M2 >= 0 Then
      XBB(Fianco, 4) = XBB2(Fianco, 4): YBB(Fianco, 4) = YBB2(Fianco, 4)
      XX(Fianco, 8) = XBB2(Fianco, 4): YY(Fianco, 8) = YBB2(Fianco, 4)
    Else
      WRITE_DIALOG "ERROR ON CROWNING 4 EVALUATION"
    End If
  Next Fianco
  '
  'VISUALIZZAZIONE CORREZIONI PROFILO RAB
  If (SiStampa = "Y") Then
    stmp = "+"
    pX1 = 0
    pY1 = 0
    Printer.CurrentX = pX1
    Printer.CurrentY = pY1
    Printer.Print stmp
    '
    stmp = "-"
    pX1 = PicW / 2 - Printer.TextWidth(stmp) / 2
    pY1 = 0
    Printer.CurrentX = pX1
    Printer.CurrentY = pY1
    Printer.Print stmp
    '
    stmp = " OUT DIAMETER "
    pX1 = PicW / 2 - Printer.TextWidth(stmp) / 2
    pY1 = 0 + Printer.TextHeight(stmp) * 2
    Printer.CurrentX = pX1
    Printer.CurrentY = pY1
    Printer.Print stmp
    '
    pX1 = PicW / 2 - Printer.TextWidth(stmp) / 2
    pY1 = 0 + Printer.TextHeight(stmp) * 2
    pX2 = PicW / 2 + Printer.TextWidth(stmp) / 2
    pY2 = 0 + Printer.TextHeight(stmp) * 3
    Printer.Line (pX1, pY1)-(pX2, pY2), , B
    '
    stmp = "+"
    pX1 = PicW - Printer.TextWidth(stmp)
    pY1 = 0
    Printer.CurrentX = pX1
    Printer.CurrentY = pY1
    Printer.Print stmp
  Else
    stmp = "+"
    pX1 = 0
    pY1 = 0
    OEMX.OEM1PicCALCOLO.CurrentX = pX1
    OEMX.OEM1PicCALCOLO.CurrentY = pY1
    OEMX.OEM1PicCALCOLO.Print stmp
    '
    stmp = "-"
    pX1 = PicW / 2 - OEMX.OEM1PicCALCOLO.TextWidth(stmp) / 2
    pY1 = 0
    OEMX.OEM1PicCALCOLO.CurrentX = pX1
    OEMX.OEM1PicCALCOLO.CurrentY = pY1
    OEMX.OEM1PicCALCOLO.Print stmp
    '
    stmp = " OUT DIAMETER "
    pX1 = PicW / 2 - OEMX.OEM1PicCALCOLO.TextWidth(stmp) / 2
    pY1 = 0 + OEMX.OEM1PicCALCOLO.TextHeight(stmp) * 2
    OEMX.OEM1PicCALCOLO.CurrentX = pX1
    OEMX.OEM1PicCALCOLO.CurrentY = pY1
    OEMX.OEM1PicCALCOLO.Print stmp
    '
    pX1 = PicW / 2 - OEMX.OEM1PicCALCOLO.TextWidth(stmp) / 2
    pY1 = 0 + OEMX.OEM1PicCALCOLO.TextHeight(stmp) * 2
    pX2 = PicW / 2 + OEMX.OEM1PicCALCOLO.TextWidth(stmp) / 2
    pY2 = 0 + OEMX.OEM1PicCALCOLO.TextHeight(stmp) * 3
    OEMX.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2), , B
    '
    stmp = "+"
    pX1 = PicW - OEMX.OEM1PicCALCOLO.TextWidth(stmp)
    pY1 = 0
    OEMX.OEM1PicCALCOLO.CurrentX = pX1
    OEMX.OEM1PicCALCOLO.CurrentY = pY1
    OEMX.OEM1PicCALCOLO.Print stmp
  End If
  For Fianco = 1 To 2
    'ORIGINE DELLE CURVE
    ZeroPezzoX = 0
    ZeroPezzoY = 0
    stmp = "F" & Format$(Fianco, "0") & " = " & frmt(FF(Fianco, 1) + FF(Fianco, 2) + FF(Fianco, 3) + FF(Fianco, 4), 4) & " "
    If (SiStampa = "Y") Then
      pX1 = PicW / 2 + (2 * Fianco - 3) * PicW / 4 - Printer.TextWidth(stmp) / 2
      pY1 = 0 'PicH - Printer.TextHeight(stmp)
      Printer.CurrentX = pX1
      Printer.CurrentY = pY1
      Printer.Print stmp
    Else
      pX1 = PicW / 2 + (2 * Fianco - 3) * PicW / 4 - OEMX.OEM1PicCALCOLO.TextWidth(stmp) / 2
      pY1 = 0 'PicH - OEMX.OEM1PICCALCOLO.TextHeight(stmp)
      OEMX.OEM1PicCALCOLO.CurrentX = pX1
      OEMX.OEM1PicCALCOLO.CurrentY = pY1
      OEMX.OEM1PicCALCOLO.Print stmp
    End If
    'PRIMA ZONA
    If FF(Fianco, 1) <> 0 Then
      ColoreCurva = 0:  i = 1: GoSub VISUALIZZA_CERCHIO
      ColoreCurva = 12: j = 1: If SiSpezzata = 1 Then GoSub VISUALIZZA_SPEZZATA
    End If
    'SECONDA ZONA
    If FF(Fianco, 2) <> 0 Then
      ColoreCurva = 0:  i = 3: GoSub VISUALIZZA_CERCHIO
      ColoreCurva = 12: j = 2: If SiSpezzata = 1 Then GoSub VISUALIZZA_SPEZZATA
    End If
    'TERZA ZONA
    If FF(Fianco, 3) <> 0 Then
      ColoreCurva = 0:  i = 5: GoSub VISUALIZZA_CERCHIO
      ColoreCurva = 12: j = 3: If SiSpezzata = 1 Then GoSub VISUALIZZA_SPEZZATA
    End If
    'QUARTA ZONA
    If FF(Fianco, 4) <> 0 Then
      ColoreCurva = 0:  i = 7: GoSub VISUALIZZA_CERCHIO
      ColoreCurva = 12: j = 4: If SiSpezzata = 1 Then GoSub VISUALIZZA_SPEZZATA
    End If
    If SiSpezzata Then
      ColoreCurva = 0
      Const AF = 15
      Const NumeroRette = 30
      Const DP = 5
      Const DF = 5
      'LINEE DI RIFERIMENTO VERTICALI
      If (SiStampa = "Y") Then
        pX1 = PicW / 2 + (2 * Fianco - 3) * PicW / 4
        pY1 = Printer.TextHeight(stmp)
        If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          Printer.Line (pX1, PicH)-(pX1, pY1)
        End If
        'Freccia IN ALTO
        Ang = AF / 180 * PG
        DeltaAng = Ang / NumeroRette
        For i = -NumeroRette To NumeroRette
          pX2 = DF * Sin(i * DeltaAng) + PicW / 2 + (2 * Fianco - 3) * PicW / 4
          pY2 = (-DP / 2 + DF * Cos(Ang)) + pY1
          Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
        Next i
      Else
        pX1 = PicW / 2 + (2 * Fianco - 3) * PicW / 4
        pY1 = OEMX.OEM1PicCALCOLO.TextHeight(stmp)
        If (pY1 <= OEMX.OEM1PicCALCOLO.ScaleHeight) And (pX1 <= OEMX.OEM1PicCALCOLO.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          OEMX.OEM1PicCALCOLO.Line (pX1, PicH)-(pX1, pY1)
        End If
        'Freccia IN ALTO
        Ang = AF / 180 * PG
        DeltaAng = Ang / NumeroRette
        For i = -NumeroRette To NumeroRette
          pX2 = DF * Sin(i * DeltaAng) + PicW / 2 + (2 * Fianco - 3) * PicW / 4
          pY2 = (-DP / 2 + DF * Cos(Ang)) + pY1
          OEMX.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
        Next i
      End If
    End If
  Next Fianco
  'Segnalazione della fine delle operazioni
  GoSub FineDocumento
                
Exit Sub

VISUALIZZA_CERCHIO:
  'COORD. X        COORD. Y
  R164 = 2 * (Fianco - 1.5) * XX(Fianco, i + 0): R165 = YY(Fianco, i + 0)
  R166 = 2 * (Fianco - 1.5) * XX(Fianco, i + 1): R167 = YY(Fianco, i + 1)
  R168 = 2 * (Fianco - 1.5) * XX(Fianco, i + 2): R169 = YY(Fianco, i + 2)
  'DISTANZA PER TRE PUNTI
  R162 = Abs((R165 - R169) * (R164 - R166) + (R167 - R165) * (R164 - R168)) / Sqr((R165 - R169) * (R165 - R169) + (R164 - R168) * (R164 - R168))
  If R162 < Risoluzione Then
    'TRATTO RETTILINEO
    'G1 Y=R169 X=R168
    pX1 = (R164 - ZeroPezzoX) * ScalaX + PicW / 2 + (2 * Fianco - 3) * PicW / 4
    pY1 = -(R165 - ZeroPezzoY) * ScalaY + PicH
    pX2 = (R168 - ZeroPezzoX) * ScalaX + PicW / 2 + (2 * Fianco - 3) * PicW / 4
    pY2 = -(R169 - ZeroPezzoY) * ScalaY + PicH
    If (SiStampa = "Y") Then
      If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
        Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
      End If
    Else
      If (pY1 <= OEMX.OEM1PicCALCOLO.ScaleHeight) And (pX1 <= OEMX.OEM1PicCALCOLO.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
        OEMX.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
      End If
    End If
    RaggioBombatura = ""
  Else
    'TRATTO CURVILINEO
    If R169 = R167 Then
      R169 = R167 + Risoluzione
    End If
    If R165 = R167 Then
      R165 = R167 + Risoluzione
    End If
    R160 = (R164 - R166) / (R167 - R165)
    R171 = (R166 - R168) / (R169 - R167)
    R172 = (R165 + R167) / 2 - R160 * (R164 + R166) / 2
    R173 = (R169 + R167) / 2 - R171 * (R168 + R166) / 2
    If R160 = R171 Then
      R160 = R171 + Risoluzione
    End If
    'coordinate del centro
    R174 = (R173 - R172) / (R160 - R171)
    R170 = R160 * R174 + R172
    'raggio del cerchio
    R161 = Sqr((R174 - R166) * (R174 - R166) + (R170 - R167) * (R170 - R167))
    'prodotto misto per senso di rotazione
    R160 = (R166 - R164) * (R169 - R165) - (R168 - R164) * (R167 - R165)
    'Calcolo angoli compresi tra 0 e 2 * Pigreco. --------------------------
    dx = R164 - R174: Dy = R165 - R170
    GoSub CalcoloAng
    AngStart = Ang
    dx = R168 - R174: Dy = R169 - R170
    GoSub CalcoloAng
    AngEnd = Ang
    '-----------------------------------------------------------------------
    RaggioBombatura = frmt(R161, 4)
    '-----------------------------------------------------------------------
    If R160 > 0 Then
      'G3 Y=R169 X=R168 CR=R161
      If AngEnd >= AngStart Then
        Ang = AngEnd - AngStart
      Else
        Ang = 2 * PG - (AngStart - AngEnd)
      End If
      DeltaAng = Ang / NumeroArchi
      pX1 = R164: pY1 = R165
      For j = 1 To NumeroArchi - 1
        Ang = (AngStart + j * DeltaAng)
        pX2 = R174 + R161 * Cos(Ang): pY2 = R170 + R161 * Sin(Ang)
        'Adattamento nella finestra grafica
        pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2 + (2 * Fianco - 3) * PicW / 4
        pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH
        pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2 + (2 * Fianco - 3) * PicW / 4
        pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH
        'Traccio la linea
        If (SiStampa = "Y") Then
          If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
            Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
          End If
        Else
          If (pY1 <= OEMX.OEM1PicCALCOLO.ScaleHeight) And (pX1 <= OEMX.OEM1PicCALCOLO.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
            OEMX.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
          End If
        End If
        'Aggiorno Px1 e Py1
        pX1 = R174 + R161 * Cos(Ang): pY1 = R170 + R161 * Sin(Ang)
      Next j
      pX2 = R168: pY2 = R169
      'Adattamento nella finestra grafica
      pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2 + (2 * Fianco - 3) * PicW / 4
      pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH
      pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2 + (2 * Fianco - 3) * PicW / 4
      pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH
      'Traccio la linea
      If (SiStampa = "Y") Then
        If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
        End If
      Else
        If (pY1 <= OEMX.OEM1PicCALCOLO.ScaleHeight) And (pX1 <= OEMX.OEM1PicCALCOLO.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          OEMX.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
        End If
      End If
    Else
      'G2 Y=R169 X=R168 CR=R161
      If AngStart >= AngEnd Then
        Ang = AngStart - AngEnd
      Else
        Ang = 2 * PG - (AngEnd - AngStart)
      End If
      DeltaAng = Ang / NumeroArchi
      pX1 = R164: pY1 = R165
      For j = 1 To NumeroArchi - 1
        Ang = (AngStart - j * DeltaAng)
        pX2 = R174 + R161 * Cos(Ang): pY2 = R170 + R161 * Sin(Ang)
        'Adattamento nella finestra grafica
        pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2 + (2 * Fianco - 3) * PicW / 4
        pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH
        pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2 + (2 * Fianco - 3) * PicW / 4
        pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH
        'Traccio la linea
        If (SiStampa = "Y") Then
          If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
            Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
          End If
        Else
          If (pY1 <= OEMX.OEM1PicCALCOLO.ScaleHeight) And (pX1 <= OEMX.OEM1PicCALCOLO.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
            OEMX.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
          End If
        End If
        'Aggiorno Px1 e Py1
        pX1 = R174 + R161 * Cos(Ang): pY1 = R170 + R161 * Sin(Ang)
      Next j
      pX2 = R168: pY2 = R169
      'Adattamento nella finestra grafica
      pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2 + (2 * Fianco - 3) * PicW / 4
      pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH
      pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2 + (2 * Fianco - 3) * PicW / 4
      pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH
      'Traccio la linea
      If (SiStampa = "Y") Then
        If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
        End If
      Else
        If (pY1 <= OEMX.OEM1PicCALCOLO.ScaleHeight) And (pX1 <= OEMX.OEM1PicCALCOLO.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          OEMX.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
        End If
      End If
    End If
  End If
Return

CalcoloAng: 'Calcolo dell'angolo in radianti tra 0 e 2 * PG.
  If dx = 0 Then dx = Risoluzione
  If dx >= 0 And Dy >= 0 Then QUADRANTE = 1
  If dx < 0 And Dy >= 0 Then QUADRANTE = 2
  If dx < 0 And Dy < 0 Then QUADRANTE = 3
  If dx >= 0 And Dy < 0 Then QUADRANTE = 4
  Select Case QUADRANTE
    Case 1
      Ang = Atn(Dy / dx) + 0 * PG
    Case 2
      Ang = Atn(Dy / dx) + 1 * PG
    Case 3
      Ang = Atn(Dy / dx) + 1 * PG
    Case 4
      Ang = Atn(Dy / dx) + 2 * PG
  End Select
Return

VISUALIZZA_SPEZZATA:
  'COORD. X        COORD. Y
  R164 = 2 * (Fianco - 1.5) * XX(Fianco, i + 0): R165 = YY(Fianco, i + 0)
  R166 = 2 * (Fianco - 1.5) * XX(Fianco, i + 1): R167 = YY(Fianco, i + 1)
  R168 = 2 * (Fianco - 1.5) * XX(Fianco, i + 2): R169 = YY(Fianco, i + 2)
  'TRATTO RETTILINEO PER LA RASTREMAZIONE E FASCIA
  pX1 = (R164 - ZeroPezzoX) * ScalaX + PicW / 2 + (2 * Fianco - 3) * PicW / 4
  pY1 = -(R165 - ZeroPezzoY) * ScalaY + PicH
  pX2 = (R168 - ZeroPezzoX) * ScalaX + PicW / 2 + (2 * Fianco - 3) * PicW / 4
  pY2 = -(R169 - ZeroPezzoY) * ScalaY + PicH
  If (SiStampa = "Y") Then
    If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
      Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
    End If
  Else
    If (pY1 <= OEMX.OEM1PicCALCOLO.ScaleHeight) And (pX1 <= OEMX.OEM1PicCALCOLO.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
      OEMX.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
    End If
  End If
  'TRATTO RETTILINEO PER LA BOMBATURA
  pX1 = (2 * (Fianco - 1.5) * Xm(Fianco, j) - ZeroPezzoX) * ScalaX + PicW / 2 + (2 * Fianco - 3) * PicW / 4
  pY1 = (-Ym(Fianco, j) - ZeroPezzoY) * ScalaY + PicH
  pX2 = (2 * (Fianco - 1.5) * XBB(Fianco, j) - ZeroPezzoX) * ScalaX + PicW / 2 + (2 * Fianco - 3) * PicW / 4
  pY2 = (-YBB(Fianco, j) - ZeroPezzoY) * ScalaY + PicH
  If (SiStampa = "Y") Then
    If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
      Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
    End If
  Else
    If (pY1 <= OEMX.OEM1PicCALCOLO.ScaleHeight) And (pX1 <= OEMX.OEM1PicCALCOLO.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
      OEMX.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
    End If
  End If
  'INDICE TRATTO
  pX1 = (2 * (Fianco - 1.5) * XBB(Fianco, j) - ZeroPezzoX) * ScalaX + PicW / 2 + (2 * Fianco - 3) * PicW / 4
  pY1 = (-Ym(Fianco, j) - ZeroPezzoY) * ScalaY + PicH
  If Len(RaggioBombatura) > 0 Then
    stmp = " r" & Format$(j, "0") & "=" & RaggioBombatura & " "
  Else
    stmp = " " & Format$(j, "0") & " "
  End If
  If (SiStampa = "Y") Then
    If BB(Fianco, j) > 0 Then
      pX1 = pX1 + (Fianco - 2) * Printer.TextWidth(stmp)
    Else
      pX1 = pX1 - (Fianco - 1) * Printer.TextWidth(stmp)
    End If
    pY1 = pY1 - Printer.TextHeight(stmp) / 2
    If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
      Printer.CurrentX = pX1
      Printer.CurrentY = pY1
      Printer.Print stmp
    End If
  Else
    If BB(Fianco, j) > 0 Then
      pX1 = pX1 + (Fianco - 2) * OEMX.OEM1PicCALCOLO.TextWidth(stmp)
    Else
      pX1 = pX1 - (Fianco - 1) * OEMX.OEM1PicCALCOLO.TextWidth(stmp)
    End If
    pY1 = pY1 - OEMX.OEM1PicCALCOLO.TextHeight(stmp) / 2
    If (pY1 <= OEMX.OEM1PicCALCOLO.ScaleHeight) And (pX1 <= OEMX.OEM1PicCALCOLO.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
      OEMX.OEM1PicCALCOLO.CurrentX = pX1
      OEMX.OEM1PicCALCOLO.CurrentY = pY1
      OEMX.OEM1PicCALCOLO.Print stmp
    End If
  End If
  'ESTREMO INFERIORE
  pX1 = (R164 - ZeroPezzoX) * ScalaX + PicW / 2 + (2 * Fianco - 3) * PicW / 4
  pY1 = -(R165 - ZeroPezzoY) * ScalaY + PicH
  If (SiStampa = "Y") Then
    If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
      Printer.Line (pX1 - DT / 2, pY1)-(pX1 + DT / 2, pY1), QBColor(ColoreCurva)
    End If
  Else
    If (pY1 <= OEMX.OEM1PicCALCOLO.ScaleHeight) And (pX1 <= OEMX.OEM1PicCALCOLO.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
      OEMX.OEM1PicCALCOLO.Line (pX1 - DT / 2, pY1)-(pX1 + DT / 2, pY1), QBColor(ColoreCurva)
    End If
  End If
  'ESTREMO SUPERIORE
  pX1 = (R168 - ZeroPezzoX) * ScalaX + PicW / 2 + (2 * Fianco - 3) * PicW / 4
  pY1 = -(R169 - ZeroPezzoY) * ScalaY + PicH
  If (SiStampa = "Y") Then
    If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
      Printer.Line (pX1 - DT / 2, pY1)-(pX1 + DT / 2, pY1), QBColor(ColoreCurva)
    End If
  Else
    If (pY1 <= OEMX.OEM1PicCALCOLO.ScaleHeight) And (pX1 <= OEMX.OEM1PicCALCOLO.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
      OEMX.OEM1PicCALCOLO.Line (pX1 - DT / 2, pY1)-(pX1 + DT / 2, pY1), QBColor(ColoreCurva)
    End If
  End If
Return

errVISUALIZZA_CORREZIONI_PROFILO_RAB:
  If FreeFile > 0 Then Close
  WRITE_DIALOG "SUB: VISUALIZZA_CORREZIONI_PROFILO_RAB " & Error(Err)
  'Exit Sub
  Resume Next
  
FineDocumento:
  'Dimensione dei caratteri
  If (SiStampa = "Y") Then
    For i = 1 To 2
      Printer.FontName = TextFont
      Printer.FontSize = 14
      Printer.FontBold = True
    Next i
    'Logo
    'Printer.PaintPicture OEMX.PicLogo, 0, PicH, 20, 20
    'DATA E ORA DI STAMPA
    Printer.CurrentX = 0 + 25
    Printer.CurrentY = PicH
    Printer.Print Date & " " & Time
    'Nome del documento
    Printer.CurrentX = 0 + PicW / 2
    Printer.CurrentY = PicH
    Printer.Print OEMX.frameCalcolo.Caption
    'Scala X
    If OEMX.lblINP(0).Visible = True Then
      stmp = OEMX.lblINP(0).Caption & ": x" & OEMX.txtINP(0).Text
      Printer.CurrentX = 0 + 25
      Printer.CurrentY = PicH + 10
      Printer.Print stmp
    End If
    'Scala Y
    If OEMX.lblINP(1).Visible = True Then
      stmp = OEMX.lblINP(1).Caption & ": x" & OEMX.txtINP(1).Text
      Printer.CurrentX = 0 + PicW / 2
      Printer.CurrentY = PicH + 10
      Printer.Print stmp
    End If
    Printer.EndDoc
    WRITE_DIALOG OEMX.frameCalcolo.Caption & " --> " & Printer.DeviceName & " OK!!"
  End If
Return

End Sub

Sub STAMPA_CORREZIONI_DI_PROCESSO()
'
Dim i     As Integer
Dim k1    As Integer
Dim k2    As Integer
Dim k3    As Integer
Dim k4    As Integer
Dim j     As Integer
Dim rsp   As Integer
Dim MLeft As Single
Dim MTop  As Single
Dim Box0W As Single
Dim Box0H As Single
'Parametri per la stampa di linee
Dim X1    As Single
Dim Y1    As Single
Dim X2    As Single
Dim Y2    As Single
'Area di stampa dei grafici
Dim Box1L As Single
Dim Box1T As Single
Dim Box1W As Single
Dim Box1H As Single
Dim iRG   As Single
Dim iCl   As Single
Dim riga  As String
Dim stmp  As String
'
On Error Resume Next
  '
  Printer.ScaleMode = 7
  Call ImpostaPrinter("Courier New", 10)
  MLeft = 2: MTop = 1
  Box0W = 17: Box0H = 27
  iRG = Printer.TextHeight("A")
  iCl = Printer.TextWidth("A")
  Box1L = 5: Box1T = 1
  Box1W = 12: Box1H = 12
  GoSub STAMPA_INTESTAZIONE
  '
  Dim NrigheMax As Integer
  Dim iiRG      As Integer
  Dim iiPG      As Integer
  '
  NrigheMax = (Box0H - 3) / iRG
  iiPG = 1: riga = "": i = 1: k1 = 1: k4 = 1
  Do
    'terminatore di riga
    k2 = InStr(k1, OEMX.OEM1txtHELP.Text, Chr(13))
    k3 = InStr(k1, OEMX.OEM1txtHELP.Text, Chr(10))
    'prendo il minimo
    If k3 >= k2 Then k4 = k2 Else k4 = k3
    If k4 > k1 Then
      riga = Mid$(OEMX.OEM1txtHELP.Text, k1, k4 - k1)
      If k3 >= k2 Then k1 = k3 + 1 Else k1 = k2 + 1
      i = i + 1
      iiRG = (i - 1) Mod NrigheMax
      Printer.CurrentX = X1 + iCl
      Printer.CurrentY = Y1 + (iiRG + 3) * iRG
      'AGGIUNGO I NUMERI DI RIGA
      'riga = Right(String(6, " ") & Format$(i - 1, "#####0"), 6) & "] " & riga
      Printer.Print riga
      If i = iiPG * NrigheMax Then
        Printer.NewPage
        GoSub STAMPA_INTESTAZIONE
        iiPG = iiPG + 1
      End If
    Else
      Exit Do
    End If
  Loop
  Printer.EndDoc
  '
Exit Sub

STAMPA_INTESTAZIONE:
  'Squadratura della pagina
  X1 = MLeft: Y1 = MTop
  X2 = MLeft + Box0W: Y2 = MTop + Box0H
  'Printer.Line (X1, Y1)-(X2, Y2), QBColor(0), B
  'Intestazione della pagina
  Printer.CurrentX = X1 + iCl
  Printer.CurrentY = Y1 + iRG
  stmp = ""
  stmp = stmp & " Date: " & Format$(Now, "dd/mm/yy")
  stmp = stmp & " Time: " & Format$(Now, "hh:mm:ss")
  Printer.Print OEMX.OEM1Combo1.Caption & stmp
  'Linea di separazione
  X1 = MLeft: Y1 = MTop + 1
  X2 = MLeft + Box0W: Y2 = MTop + 1
  Printer.Line (X1, Y1)-(X2, Y2), QBColor(0)
  Printer.CurrentX = X1 + iCl
  Printer.CurrentY = Y1 + 1 * iRG
  Printer.Print OEMX.OEM1frameCalcolo.Caption
Return

End Sub
