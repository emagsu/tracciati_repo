Attribute VB_Name = "CRFORMA"

Option Explicit

Dim Session As IMCDomain
'
Dim PI As Double
Global Et$                      ' Codice progetto
'
Dim Mn       As Double          ' Cremagliera : Modulo normale
Dim AlfaDeg  As Double          ' ----------- : Angolo di pressione (in gradi)
Dim Sw       As Double          ' ----------- : Spessore su riferimento
Dim Hkw      As Double          ' ----------- : Addendum su riferimento
'*** PUNTI CREMAGLIERA PER CONTROLLO ***
Global NPctrl As Integer        ' Cremagliera : numero punti per controllo
Global PCRE() As String         ' ----------- : punti controllo
Dim SWcre() As Double           ' ----------- : spessore punto crema
Dim ADDcre() As Double          ' ----------- : addendum punto crema
Global APcre() As Double        ' ----------- : Ang.Press. punto crema
Global Xcre() As Double         ' ----------- : Ascissa punto crema
Global Rcre() As Double         ' ----------- : Raggio punto crema
'***************************************
Dim Hr       As Double          ' -----------: Alt.totale fianco ( Dato )
'
Dim NFil         As Long        ' CREATORE : Numero Filetti
Dim InFilD       As Double      ' -------- : Inclinazione Filetti in gradi
Dim InFil        As Double      ' -------- : Inclinazione Filetti in radianti
Dim AngProjD     As Double      ' -------- : Inclinazione sur projecteur in gradi
Dim AngProj      As Double      ' -------- : Inclinazione sur projecteur in radianti
Dim PasHel       As Double      ' -------- : Passo Spirale
Dim DestCr       As Double      ' -------- : Diametro esterno
Dim PasAxial     As Double      ' -------- : Passo Assiale
Dim DprimCr      As Double      ' -------- : Diametro primitivo
Dim RprimCr      As Double
Dim SottIntaglio As Double      ' -------- : Valore del sottintaglio in mm
Dim Nscan        As Long        ' -------- :
Dim Camma        As Double      ' --------
Dim Senso        As Double      ' senso elica filetti ( 1=dx ; -1=sx)
'*** PUNTI SEZ.ASSIALE CREATORE PER CONTROLLO ***
Global Xcrfa()   As Double      ' CREATORE : Ascissa punto sez.assiale CR
Global Rcrfa()   As Double      ' -------- : Raggio punto sez.assiale CR
Global APcrfa()  As Double      ' -------- : Ang.Press. punto sez.assiale CR
'************************************************
'
'������� CONTROLLO ��������
Dim idCtrlSW     As Integer     '--- INDICE punto per controllo SW
Dim CtrlDiam     As Integer     '--- tipo controllo diam. (1=Est,-1=Int,0=No)
Dim GEN(20)      As Double
Dim XX_F1()      As Double
Dim YY_F1()      As Double
Dim TT_F1()      As Double
Dim MISF1()      As Double
Dim XX_F2()      As Double
Dim YY_F2()      As Double
Dim TT_F2()      As Double
Dim MISF2()      As Double
Dim Np           As Integer
'��������������������������
'
'Global Dsf          As Double      ' diametro sonda
'Global CamCor       As Double
'Global YSCP         As Double      ' quota Y per portare il centro sonda sull'asse pezzo
Dim DisTagliente As Double      ' Distanza dal tagliente della sezione di controllo
'
Dim InMola       As Double       ' MOLA : Inclinazione Mola (= angolo elica rettifica)
'
Dim FlagFia   As Long      ' 1 = fianco dx ; -1 = fianco sx
Dim Fianco    As Long      ' 1 = fianco dx ;  2 = fianco sx

Sub VISUALIZZAZIONE_PROFILO_MOLA_CRFORMA(ByVal Scala As Single, ByVal SiStampa As String)

StopRegieEvents
MsgBox ("Sub: Not S380 ready")
ResumeRegieEvents
''
'PI = PG
'Dim DELTA As Double: DELTA = 0.0001
''
'Dim DBB As Database
'Dim RSS As Recordset
''
'Dim stmp As String
'Dim Atmp As String * 255
'Dim i As Integer
'Dim j As Integer
''
'Dim X1 As Single
'Dim X2 As Single
'Dim Y1 As Single
'Dim Y2 As Single
''
'Dim PicH As Single
'Dim PicW As Single
''
'Dim dx As Single, dy As Single
'Dim a$, msg$
'Dim L As Double
'Dim SensoRot As String  '"SX"=orario; "DX"=antiorario
'Dim FunzG As String     'G2, G3
''
'Dim xx(2, 120) As Double
'Dim YY(2, 120) As Double
'Dim RR(2, 120) As Double
'Dim TT(2, 120) As Double
''
'Dim R235 As Double
'Dim R238 As Double
''
'Dim R242 As Double
'Dim R228 As Double
''
''LOCALI
''
'Dim A1     As Double
'Dim A2     As Double
'Dim ANGOLO As Double
'Dim PMC    As Double
'Dim Xc     As Double
'Dim YC     As Double
''
'Dim R20 As Double
''
'Dim R198 As Double
'Dim R37 As Double
''
'Dim R40 As Double
'Dim R50 As Double
'Dim R60 As Double
'Dim R125 As Double
''
'Dim R61 As Double
'Dim R62 As Double
'Dim R63 As Double
'Dim R64 As Double
'Dim R65 As Double
'Dim R66 As Double
'Dim R67 As Double
''
'Dim R91 As Double
'Dim R92 As Double
'Dim R93 As Double
'Dim R94 As Double
'Dim R95 As Double
'Dim R96 As Double
'Dim R97 As Double
''
'Dim R41 As Double
'Dim R42 As Double
'Dim R43 As Double
'Dim R44 As Double
'Dim R45 As Double
'Dim R46 As Double
'Dim R47 As Double
''
'Dim R51 As Double
'Dim R52 As Double
'Dim R53 As Double
'Dim R54 As Double
'Dim R55 As Double
'Dim R56 As Double
'Dim R57 As Double
''
'Dim R71 As Double
'Dim R72 As Double
'Dim R73 As Double
'Dim R74 As Double
'Dim R75 As Double
'Dim R76 As Double
'Dim R77 As Double
''
'Dim R81 As Double
'Dim R82 As Double
'Dim R83 As Double
'Dim R84 As Double
'Dim R85 As Double
'Dim R86 As Double
'Dim R87 As Double
''
'Dim R121 As Double
'Dim R122 As Double
'Dim R123 As Double
'Dim R124 As Double
''
'Dim R89 As Double
'Dim R68 As Double
'Dim R69 As Double
'Dim R78 As Double
'Dim R79 As Double
'Dim R88 As Double
'Dim R99 As Double
''
'Dim FattoreH1 As Double
'Dim FattoreH2 As Double
''
'Dim APL8 As Double
'Dim APL18 As Double
''
'Dim R110 As Double
'Dim R111 As Double
'Dim R155 As Double
'Dim R98 As Double
''
'Dim TipoCarattere As String
'Dim ZeroPezzoX As Single
'Dim ZeroPezzoY As Single
''
''--- DICHIARAZIONI CREA_DXF
'Dim R164, R165 As Double
'Dim R168, R169 As Double
'Dim R174, R170 As Double
'Dim R161 As Double
'Dim Pin As Integer, Pfin As Integer, Numcer As Integer
'Dim Orario As Variant
''
'  '
'  OEM1.OEM1chkINPUT(0).Visible = False
'  OEM1.OEM1chkINPUT(1).Visible = False
'  '
'  OEM1.OEM1lblOX.Visible = False
'  OEM1.OEM1lblOY.Visible = False
'  OEM1.OEM1txtOX.Visible = False
'  OEM1.OEM1txtOY.Visible = False
'  '-------------------------------------------------------------
'  stmp = GetInfo("DIS0", "PicW", Path_LAVORAZIONE_INI)
'  If val(stmp) > 0 Then
'    OEM1.OEM1PicCALCOLO.ScaleWidth = val(stmp)
'  Else
'    OEM1.OEM1PicCALCOLO.ScaleWidth = 190
'  End If
'  stmp = GetInfo("DIS0", "PicH", Path_LAVORAZIONE_INI)
'  If val(stmp) > 0 Then
'    OEM1.OEM1PicCALCOLO.ScaleHeight = val(stmp)
'  Else
'    OEM1.OEM1PicCALCOLO.ScaleHeight = 95
'  End If
'  '-------------------------------------------------------------
'  PicH = OEM1.OEM1PicCALCOLO.ScaleHeight
'  PicW = OEM1.OEM1PicCALCOLO.ScaleWidth
'  '
'  If OEM1.lblINP(0).Visible = False Then OEM1.lblINP(0).Visible = True
'  If OEM1.txtINP(0).Visible = False Then OEM1.txtINP(0).Visible = True
'  '
'  'VISUALIZZO LA SCALA
'  i = LoadString(g_hLanguageLibHandle, 1000, Atmp, 255)
'  If i > 0 Then OEM1.lblINP(0).Caption = Left$(Atmp, i)
'  i = LoadString(g_hLanguageLibHandle, 1100, Atmp, 255)
'  If i > 0 Then OEM1.OEM1frameCalcolo.Caption = Left$(Atmp, i)
'  '
'  stmp = GetInfo("DIS0", "ZeroPezzoX", Path_LAVORAZIONE_INI)
'  ZeroPezzoX = val(stmp)
'  stmp = GetInfo("DIS0", "ZeroPezzoY", Path_LAVORAZIONE_INI)
'  ZeroPezzoY = val(stmp) - 4 / 10 * PicH / Scala
'  'ZeroPezzoY = Val(stmp)
'  '
'  If (SiStampa = "Y") Then
'    '
'    OEM1.PicLogo.Picture = LoadPicture(g_chOemPATH & "\" & "Logo2.bmp")
'    Printer.ScaleMode = 6
'    For i = 1 To 2
'      Printer.FontName = TipoCarattere
'      Printer.FontSize = 10
'    Next i
'    Write_Dialog OEM1.OEM1frameCalcolo.Caption & " Printing on " & Printer.DeviceName
'    '
'  Else
'    '
'    OEM1.OEM1PicCALCOLO.Cls
'    Write_Dialog ""
'    '
'  End If
'  '
'  If (OEM1.OEM1Check2.Value = 1) Then
'    Call SCRIVI_ASSI(OEM1.OEM1PicCALCOLO, SiStampa)
'    Call SCRIVI_SCALA_ORIZZONTALE(Scala, OEM1.OEM1PicCALCOLO, SiStampa)
'  End If
'  '
'  'Lettura parametri
'  GoSub LETTURA_PARAMETRI
'  '
'  'controllo parametri
'  GoSub CONTROLLO_PARAMETRI
'  '
'  'Convertire gli angoli in radianti (solo PC)
'  GoSub CONVERSIONE_RADIANTI
'  '
'  'Calcolo profilo
'  GoSub CALCOLO_PROFILO
'  '
'  'Registra profilo per visualizzarlo
'  GoSub VISUALIZZA_PROFILO_MOLA
'  '
'  'Registra coordinate punti su file PUNTI.TXT
'  GoSub WRTPUNTI
'  '
'  'Profilo mola: registra su file MOLA.DXF
'  GoSub SCRIVI_FILE_DXF_MOLA
'  '
''  'Calcolo valore R98
''  GoSub CALCOLOALTEZZE
'  '
'  If (SiStampa = "Y") Then
'    '
'    'Carattere per titolo
'    For i = 1 To 2
'      Printer.FontName = TipoCarattere
'      Printer.FontSize = 12
'      Printer.FontBold = True
'    Next i
'    'Logo
'    Printer.PaintPicture OEM1.PicLogo, 0, PicH, 20, 20
'    'Nome del documento
'    stmp = OEM1.OEM1frameCalcolo.Caption & "  " & Date & " " & Time
'    Printer.CurrentX = 0 + 25
'    Printer.CurrentY = PicH
'    Printer.Print stmp
'    'Scala
'    stmp = OEM1.lblINP(0).Caption & ": x" & OEM1.txtINP(0).Text
'    Printer.CurrentX = 0 + 25
'    Printer.CurrentY = PicH + 10
'    Printer.Print stmp
'    'Carattere per parametri
'    For i = 1 To 2
'      Printer.FontName = TipoCarattere
'      Printer.FontSize = 10
'      Printer.FontBold = True
'    Next i
'    Printer.EndDoc
'    '
'    Write_Dialog OEM1.OEM1frameCalcolo.Caption & " Printed on " & Printer.DeviceName
'    '
'  End If
'  '
'Exit Sub
'
'CALCOLO_PROFILO:
'  '
'  If R40 = 0 Then R40 = 0.001
'  If R50 = 0 Then R50 = 0.001
'
'  'Correttore altezza e spostamento
'  FattoreH1 = 1 - R68 / R41
'  FattoreH2 = 1 - R69 / R71
'
'  R41 = R41 * FattoreH1 + R88
'  R71 = R71 * FattoreH2 + R99
'  R42 = R42 * FattoreH1 + R88
'  R72 = R72 * FattoreH2 + R99
'  R43 = R43 * FattoreH1 + R88
'  R73 = R73 * FattoreH2 + R99
'  R44 = R44 * FattoreH1 + R88
'  R74 = R74 * FattoreH2 + R99
'  R45 = R45 * FattoreH1 + R88
'  R75 = R75 * FattoreH2 + R99
'  R46 = R46 * FattoreH1 + R88
'  R76 = R76 * FattoreH2 + R99
'  R47 = R47 * FattoreH1 + R88
'  R77 = R77 * FattoreH2 + R99
'  R122 = R122 * FattoreH1 + R88
'  R124 = R124 * FattoreH2 + R99
'  'Correttore angolo
'  R51 = R51 + R78 * R41 / R41 - R78 / 2
'  R52 = R52 + R78 * R42 / R41 - R78 / 2
'  R53 = R53 + R78 * R43 / R41 - R78 / 2
'  R54 = R54 + R78 * R44 / R41 - R78 / 2
'  R55 = R55 + R78 * R45 / R41 - R78 / 2
'  R56 = R56 + R78 * R46 / R41 - R78 / 2
'  R57 = R57 + R78 * R47 / R41 - R78 / 2
'  R121 = R121 + R78 * R122 / R41 - R78 / 2
'
'  R81 = R81 - R79 * R71 / R71 + R79 / 2
'  R82 = R82 - R79 * R72 / R71 + R79 / 2
'  R83 = R83 - R79 * R73 / R71 + R79 / 2
'  R84 = R84 - R79 * R74 / R71 + R79 / 2
'  R85 = R85 - R79 * R75 / R71 + R79 / 2
'  R86 = R86 - R79 * R76 / R71 + R79 / 2
'  R87 = R87 - R79 * R77 / R71 + R79 / 2
'  R123 = R123 - R79 * R124 / R71 + R79 / 2
'
'
'  ' Modifico fondo rettilineo in un raggio grande (+ = piccola cavit�)
'  If Abs(R125) < (R121 - R123) / 2 Then R125 = 9999
'
'  ';***************************************************************
'  ';* Calcolo fianco 1
'  ';***************************************************************
'  ';Punto 6
'  xx(0, 6) = R55: YY(0, 6) = R45        'p6=YE45 ZE55
'  xx(0, 7) = R56: YY(0, 7) = R46        'p7=YE46 ZE56
'  Call Cerchio2PuntiRaggio(xx(0, 6), YY(0, 6), xx(0, 7), YY(0, 7), -R65, xx(0, 105), YY(0, 105)) 'c5=p6,p7,rE65
'  Call Cerchio2PuntiRaggio(xx(0, 6), YY(0, 6), xx(0, 7), YY(0, 7), -R65, xx(0, 106), YY(0, 106)) 'c6=p6,p7,rE65
'  RR(0, 105) = R65
'  RR(0, 106) = R65
'  xx(0, 10) = 0: YY(0, 10) = R88      'p10=YE88 ZE110
'  TT(0, 10) = R60                     'l10=p10,aE111
'  R111 = R125
'  xx(0, 108) = xx(0, 10) + R111 * Sin(R60): YY(0, 108) = YY(0, 10) + R111 * Cos(R60) 'c8=p10,l10,rE111
'  RR(0, 108) = R111
'  xx(0, 8) = R57: YY(0, 8) = R47        'p8=YE47 ZE57
'  R110 = -R66
'  R111 = -R67
'  If R66 = 0 Then
'      xx(0, 8) = R56: YY(0, 8) = R46    'p8=YE46 ZE56
'    Else
'      Call Cerchio2PuntiRaggio(xx(0, 7), YY(0, 7), xx(0, 8), YY(0, 8), R66, xx(0, 106), YY(0, 106)) 'c6=p7,p8,rE66 invece di  c6=c5,p8,rR111
'      RR(0, 106) = R66
'  End If
'  xx(0, 31) = R121: YY(0, 31) = R122    'p31=YE122 ZE121
'  'SEMITOPPING ?
'  R155 = Sqr((xx(0, 8) - R121) * (xx(0, 8) - R121) + (YY(0, 8) - R122) * (YY(0, 8) - R122))
'  If R155 < 0.01 Then 'semitopping assente
'    'c7=p8,c8,rE111
'    Call Cerchio_PuntoCerchioRaggio(xx(0, 108), YY(0, 108), xx(0, 8), YY(0, 8), R125, R67, xx(0, 107), YY(0, 107), "F1")
'  Else
'    'c7=l8,c8,rE111
'    APL8 = Atn((xx(0, 31) - xx(0, 8)) / (YY(0, 31) - YY(0, 8)))
'    Call Cerchio_RettaCerchio(xx(0, 108), YY(0, 108), R125, xx(0, 8), YY(0, 8), APL8, R67, xx(0, 107), YY(0, 107), xx(0, 9), YY(0, 9), xx(0, 31), YY(0, 31))
'    'p31=l8,c7 :  viene ricalcolato
'  End If
'  RR(0, 107) = R67
'
'  'p9=c7,c8
'  If R125 > 0 Then
'      xx(0, 9) = xx(0, 108) + (xx(0, 107) - xx(0, 108)) * Abs(RR(0, 108)) / (Abs(RR(0, 108)) + RR(0, 107))
'      YY(0, 9) = YY(0, 108) + (YY(0, 107) - YY(0, 108)) * Abs(RR(0, 108)) / (Abs(RR(0, 108)) + RR(0, 107))  ''p9=c8,c7
'  Else
'      xx(0, 9) = xx(0, 108) + (xx(0, 107) - xx(0, 108)) * Abs(RR(0, 108)) / (Abs(RR(0, 108)) - RR(0, 107))
'      YY(0, 9) = YY(0, 108) + (YY(0, 107) - YY(0, 108)) * Abs(RR(0, 108)) / (Abs(RR(0, 108)) - RR(0, 107))  ''p9=c8,c7
'  End If
'  xx(0, 5) = R54: YY(0, 5) = R44   ''p5=YE44 ZE54
'  xx(0, 4) = R53: YY(0, 4) = R43   ''p4=YE43 ZE53
'
'  If R63 <> 0 Then
'      Call Cerchio2PuntiRaggio(xx(0, 5), YY(0, 5), xx(0, 6), YY(0, 6), -R64, xx(0, 104), YY(0, 104))  ''c4=p5,p6,rE64
'      Call Cerchio2PuntiRaggio(xx(0, 4), YY(0, 4), xx(0, 5), YY(0, 5), -R63, xx(0, 103), YY(0, 103))  ''c3=p4,p5,rE63
'      RR(0, 103) = R63
'      RR(0, 104) = R64
'  Else
'      Call Cerchio2PuntiRaggio(xx(0, 4), YY(0, 4), xx(0, 6), YY(0, 6), -R64, xx(0, 104), YY(0, 104))  ''c3=p4,p6,rE64
'      RR(0, 103) = R64
'      RR(0, 104) = R64
'      Call Cerchio2PuntiRaggio(xx(0, 4), YY(0, 4), xx(0, 6), YY(0, 6), -R64, xx(0, 103), YY(0, 103))  ''c4=p4,p6,rE64
'  End If
'  ''"P1"
'   xx(0, 1) = R51: YY(0, 1) = R41   ''p1=YE41 ZE51
'   TT(0, 2) = R40     ''l2=p1,aE110
'   xx(0, 3) = R52: YY(0, 3) = R42   ''p3=YE42 ZE52
'   RR(0, 101) = R61
'   If R62 <> 0 Then
'      Call Cerchio2PuntiRaggio(xx(0, 3), YY(0, 3), xx(0, 4), YY(0, 4), -R62, xx(0, 102), YY(0, 102))   ''c2=p3,p4,rE62
'      RR(0, 102) = R62
'      ''c1=l2,c2,rE61
'      ''p2=l2,c1
'      Call Cerchio_LineaPuntoRaggio(xx(0, 3), YY(0, 3), xx(0, 102), YY(0, 102), R61, TT(0, 2), xx(0, 101), YY(0, 101), xx(0, 2), YY(0, 2))
'      ''p4=c2,c3 :  non modifico le coordinate di P4
'  Else
'      ''c1=l2,c3,rE61
'      ''p2=l2,c1
'      Call Cerchio_LineaPuntoRaggio(xx(0, 3), YY(0, 3), xx(0, 103), YY(0, 103), R61, TT(0, 2), xx(0, 101), YY(0, 101), xx(0, 2), YY(0, 2))
'      ''p3=c1,c3 : non ricalcolo le coordinate di P3
'  End If
'  xx(0, 21) = R37 / 2
'  YY(0, 21) = YY(0, 2) - (xx(0, 21) - xx(0, 2)) * Tan(TT(0, 2))   ''p21=l1,l2
'
'
'  ';***************************************************************
'  ';* Calcolo fianco 2
'  ';***************************************************************
'  'punto 16
'  xx(0, 16) = R85: YY(0, 16) = R75
'  xx(0, 17) = R86: YY(0, 17) = R76
'  Call Cerchio2PuntiRaggio(xx(0, 16), YY(0, 16), xx(0, 17), YY(0, 17), R95, xx(0, 115), YY(0, 115)) 'c15=p16,p17,rE95
'  Call Cerchio2PuntiRaggio(xx(0, 16), YY(0, 16), xx(0, 17), YY(0, 17), R95, xx(0, 116), YY(0, 116)) 'c16=p16,p17,rE95
'  RR(0, 115) = R95
'  RR(0, 116) = R95
'  xx(0, 20) = 0: YY(0, 20) = R99        'p20=YE99 ZE110
'  TT(0, 20) = R60                   'l20=p20,aE111
'  If R125 = 0 Then
'      R111 = 9999
'    Else
'      R111 = R125
'  End If
'  xx(0, 118) = xx(0, 20) + R111 * Sin(R60): YY(0, 118) = YY(0, 20) + R111 * Cos(R60)      'c18=p20,l20,rE111
'  RR(0, 118) = R111
'
'  xx(0, 18) = R87: YY(0, 18) = R77      'p18=YE77 ZE87
'  If R96 = 0 Then
'      xx(0, 18) = R86: YY(0, 18) = R76    'p18=YE76 ZE86
'    Else
'      Call Cerchio2PuntiRaggio(xx(0, 17), YY(0, 17), xx(0, 18), YY(0, 18), -R96, xx(0, 116), YY(0, 116)) 'c16=p17,p18,rE96 invece di  c16=c15,p18,rR96
'      RR(0, 116) = R96
'  End If
'  ''"P32"
'  xx(0, 32) = R123: YY(0, 32) = R124    'p32=YE124 ZE123
'  'SEMITOPPING ?
'  R155 = Sqr((xx(0, 18) - R123) * (xx(0, 18) - R123) + (YY(0, 18) - R124) * (YY(0, 18) - R124))
'  If R155 < 0.01 Then 'semitopping assente
'    'c17=p18,c18,rE97
'    Call Cerchio_PuntoCerchioRaggio(xx(0, 118), YY(0, 118), xx(0, 18), YY(0, 18), R125, R97, xx(0, 117), YY(0, 117), "F2")
'  Else
'    'c17=l18,c18,rE111
'    APL18 = Atn((xx(0, 32) - xx(0, 18)) / (YY(0, 32) - YY(0, 18)))
'    Call Cerchio_RettaCerchio(xx(0, 118), YY(0, 118), R125, xx(0, 18), YY(0, 18), APL18, R97, xx(0, 117), YY(0, 117), xx(0, 19), YY(0, 19), xx(0, 32), YY(0, 32))
'    'p32=l18,c17 :  viene ricalcolato
'  End If
'  RR(0, 117) = R97
'  ''"P19"
'  If R125 > 0 Then
'      xx(0, 19) = xx(0, 118) + (xx(0, 117) - xx(0, 118)) * Abs(RR(0, 118)) / (Abs(RR(0, 118)) + RR(0, 117))
'      YY(0, 19) = YY(0, 118) + (YY(0, 117) - YY(0, 118)) * Abs(RR(0, 118)) / (Abs(RR(0, 118)) + RR(0, 117)) ''p19=c18,c17
'  Else
'      xx(0, 19) = xx(0, 118) + (xx(0, 117) - xx(0, 118)) * Abs(RR(0, 118)) / (Abs(RR(0, 118)) - RR(0, 117))
'      YY(0, 19) = YY(0, 118) + (YY(0, 117) - YY(0, 118)) * Abs(RR(0, 118)) / (Abs(RR(0, 118)) - RR(0, 117)) ''p19=c18,c17
'  End If
'  xx(0, 15) = R84: YY(0, 15) = R74   ''p15=YE74 ZE84
'  xx(0, 14) = R83: YY(0, 14) = R73   ''p14=YE73 ZE83
'
'  If R93 <> 0 Then   'BEQ,E93,0,NO93)
'      Call Cerchio2PuntiRaggio(xx(0, 15), YY(0, 15), xx(0, 16), YY(0, 16), R94, xx(0, 114), YY(0, 114))  ''c14=p15,p16,rE110 (-E94)
'      Call Cerchio2PuntiRaggio(xx(0, 14), YY(0, 14), xx(0, 15), YY(0, 15), R93, xx(0, 113), YY(0, 113))  ''c13=p14,p15,rE111 (-E93)
'      RR(0, 113) = R93
'      RR(0, 114) = R94
'  Else
'      Call Cerchio2PuntiRaggio(xx(0, 14), YY(0, 14), xx(0, 16), YY(0, 16), R94, xx(0, 114), YY(0, 114)) ''c13=p14,p16,rE110 (-E94)
'      RR(0, 113) = R94
'      Call Cerchio2PuntiRaggio(xx(0, 14), YY(0, 14), xx(0, 16), YY(0, 16), R94, xx(0, 113), YY(0, 113)) ''c14=p14,p16,rE110 (-E94)
'      RR(0, 114) = R94
'  End If
'  ''"P11"
'   xx(0, 11) = R81: YY(0, 11) = R71 ''p11=YE71 ZE81
'   TT(0, 12) = R50     ''l12=p11,aE110
'   xx(0, 13) = R82: YY(0, 13) = R72   ''p13=YE72 ZE82
'   RR(0, 111) = R91
'   If R92 <> 0 Then
'      Call Cerchio2PuntiRaggio(xx(0, 13), YY(0, 13), xx(0, 14), YY(0, 14), R92, xx(0, 112), YY(0, 112))   ''c12=p13,p14,rE110 (-E92)
'      RR(0, 112) = R92
'      ''c11=l12,c12,rE111 (-E91)
'      ''p12=c11,c12
'      Call Cerchio_LineaPuntoRaggio(xx(0, 13), YY(0, 13), xx(0, 112), YY(0, 112), R91, TT(0, 12), xx(0, 111), YY(0, 111), xx(0, 12), YY(0, 12))
'      ''p14=c12,c13 :  non modifico le coordinate di P14
'  Else
'      ''c11=-l12,c13,rE111 (-E91)
'      ''p12=l12,c11
'      Call Cerchio_LineaPuntoRaggio(xx(0, 13), YY(0, 13), xx(0, 113), YY(0, 113), R91, TT(0, 12), xx(0, 111), YY(0, 111), xx(0, 12), YY(0, 12))
'      ''p13=c11,c13 : non ricalcolo le coordinate di P13
'  End If
'  ''l11=Y0 ZE110,a0
'  ''p23=l11,l12
'  xx(0, 23) = -R37 / 2
'  YY(0, 23) = YY(0, 12) - (xx(0, 23) - xx(0, 12)) * Tan(TT(0, 12))
'
'  '*******  FINE FIANCHI  *******
'  '- ULTIMO PUNTO FIANCO 1: P22 -
'  xx(0, 22) = xx(0, 19) - 0.5
'  YY(0, 22) = YY(0, 108) - Sqr(RR(0, 108) ^ 2 - (xx(0, 22) - xx(0, 108)) ^ 2)
'  If R125 < 0 Then
'      YY(0, 22) = YY(0, 108) + Sqr(RR(0, 108) ^ 2 - (-xx(0, 22) + xx(0, 108)) ^ 2)
'  End If
'  '- ULTIMO PUNTO FIANCO 2: P24 -
'  xx(0, 24) = xx(0, 9) + 0.5
'  YY(0, 24) = YY(0, 118) - Sqr(RR(0, 118) ^ 2 - (xx(0, 24) - xx(0, 118)) ^ 2)
'  If R125 < 0 Then
'      YY(0, 24) = YY(0, 118) + Sqr(RR(0, 118) ^ 2 - (-xx(0, 24) + xx(0, 118)) ^ 2)
'  End If
'  '******************************
'  ''; E98= Y minima
'  If (YY(0, 21) < YY(0, 23)) Then
'      R98 = YY(0, 21)
'    Else
'      R98 = YY(0, 23)
'  End If
'  ''; fine CALPFF
'  '
'  For i = 1 To 10
'      xx(0, i) = xx(0, i) - R89 / 2
'  Next i
'  For i = 11 To 20
'      xx(0, i) = xx(0, i) + R89 / 2
'  Next i
'  i = 22
'  xx(0, i) = xx(0, i) - R89 / 2
'  i = 31
'  xx(0, i) = xx(0, i) - R89 / 2
'  i = 24
'  xx(0, i) = xx(0, i) + R89 / 2
'  i = 32
'  xx(0, i) = xx(0, i) + R89 / 2
'  'centri
'  For i = 101 To 108
'      xx(0, i) = xx(0, i) - R89 / 2
'  Next i
'  For i = 111 To 118
'      xx(0, i) = xx(0, i) + R89 / 2
'  Next i
'  '
'  If (xx(0, 9) < xx(0, 19)) Then Write_Dialog "ATTENZIONE!   PIATTINO < 0"
'Return
'
'
'CALCOLOALTEZZE:
''  '; Calcolo altezza profilo
'Return
'
'
'LETTURA_PARAMETRI:
'  '
'  R37 = val(OEM1.List1(1).List(0))
'  R60 = val(OEM1.List1(1).List(1))
'  R40 = val(OEM1.List1(1).List(2))
'  R50 = val(OEM1.List1(1).List(3))
'  R125 = val(OEM1.List1(1).List(4))
'  '
'  R61 = val(OEM1.List1(1).List(5))
'  R62 = val(OEM1.List1(1).List(6))
'  R63 = val(OEM1.List1(1).List(7))
'  R64 = val(OEM1.List1(1).List(8))
'  R65 = val(OEM1.List1(1).List(9))
'  R66 = val(OEM1.List1(1).List(10))
'  R67 = val(OEM1.List1(1).List(11))
'  R91 = val(OEM1.List1(1).List(12))
'  R92 = val(OEM1.List1(1).List(13))
'  R93 = val(OEM1.List1(1).List(14))
'  R94 = val(OEM1.List1(1).List(15))
'  R95 = val(OEM1.List1(1).List(16))
'  R96 = val(OEM1.List1(1).List(17))
'  R97 = val(OEM1.List1(1).List(18))
'  '
'  R51 = val(OEM1.List3(1).List(0))
'  R52 = val(OEM1.List3(1).List(1))
'  R53 = val(OEM1.List3(1).List(2))
'  R54 = val(OEM1.List3(1).List(3))
'  R55 = val(OEM1.List3(1).List(4))
'  R56 = val(OEM1.List3(1).List(5))
'  R57 = val(OEM1.List3(1).List(6))
'  R121 = val(OEM1.List3(1).List(7))
'  R81 = val(OEM1.List3(1).List(8))
'  R82 = val(OEM1.List3(1).List(9))
'  R83 = val(OEM1.List3(1).List(10))
'  R84 = val(OEM1.List3(1).List(11))
'  R85 = val(OEM1.List3(1).List(12))
'  R86 = val(OEM1.List3(1).List(13))
'  R87 = val(OEM1.List3(1).List(14))
'  R123 = val(OEM1.List3(1).List(15))
'  '
'  R41 = val(OEM1.List3(2).List(0))
'  R42 = val(OEM1.List3(2).List(1))
'  R43 = val(OEM1.List3(2).List(2))
'  R44 = val(OEM1.List3(2).List(3))
'  R45 = val(OEM1.List3(2).List(4))
'  R46 = val(OEM1.List3(2).List(5))
'  R47 = val(OEM1.List3(2).List(6))
'  R122 = val(OEM1.List3(2).List(7))
'  R71 = val(OEM1.List3(2).List(8))
'  R72 = val(OEM1.List3(2).List(9))
'  R73 = val(OEM1.List3(2).List(10))
'  R74 = val(OEM1.List3(2).List(11))
'  R75 = val(OEM1.List3(2).List(12))
'  R76 = val(OEM1.List3(2).List(13))
'  R77 = val(OEM1.List3(2).List(14))
'  R124 = val(OEM1.List3(2).List(15))
'  '
'  'CORRETTORI:
'  '
'  Set DBB = OpenDatabase(PathDB, True, False)
'  '
'  Set RSS = DBB.OpenRecordset("OEM0_CORRETTORI15")
'  RSS.Index = "INDICE"
'  RSS.Seek "=", 1
'  R89 = val(RSS.Fields("ACTUAL_1").Value)
'  RSS.Seek "=", 2
'  R68 = val(RSS.Fields("ACTUAL_1").Value)
'  RSS.Seek "=", 3
'  R69 = val(RSS.Fields("ACTUAL_1").Value)
'  RSS.Seek "=", 4
'  R78 = val(RSS.Fields("ACTUAL_1").Value)
'  RSS.Seek "=", 5
'  R79 = val(RSS.Fields("ACTUAL_1").Value)
'  RSS.Seek "=", 6
'  R88 = val(RSS.Fields("ACTUAL_1").Value)
'  RSS.Seek "=", 7
'  R99 = val(RSS.Fields("ACTUAL_1").Value)
'  RSS.Close
'  '
'  DBB.Close
'  '
'Return
'
'CONVERSIONE_RADIANTI: 'Convertire gli angoli in radianti (solo PC)
'  R40 = FnGR(R40)
'  R50 = FnGR(R50)
'  R60 = FnGR(R60)
'Return
'
'CONTROLLO_PARAMETRI:
'    If R62 = 0 Then
'        R42 = R43
'        R52 = R53
'    End If
'    If R63 = 0 Then
'        R44 = R43
'        R54 = R53
'    End If
'    If R92 = 0 Then
'        R72 = R73
'        R82 = R83
'    End If
'    If R93 = 0 Then
'        R74 = R73
'        R84 = R83
'    End If
'    'il controllo sui cerchi c6 e c16 viene effettuato anche nel programma di profilatura
'    If R66 = 0 Then
'        R46 = R47
'        R56 = R57
'    End If
'    If R96 = 0 Then
'        R76 = R77
'        R86 = R87
'    End If
'
'    a$ = vbCrLf & "larghezza mola (R37) insufficiente!"
'    If (R37 < (R51 - R81)) Then GoTo ERRORE
'
'    a$ = "FIANCO 1"
'    'a$ = "- 1st FLANK"
'    'controllo raggi obbligatori
'    If Abs(R61) < 0.01 Then GoTo ERRORE
'    If Abs(R64) < 0.01 Then GoTo ERRORE
'    If Abs(R65) < 0.01 Then GoTo ERRORE
'    If Abs(R67) < 0.01 Then GoTo ERRORE
'    'controllo raggi > 0
'    If R61 < 0 Then GoTo ERRORE
'    If R62 < 0 Then GoTo ERRORE
'    If R66 < 0 Then GoTo ERRORE
'    'verifica punti crescenti
'    If R41 > R42 Then GoTo ERRORE
'    If R42 > R43 Then GoTo ERRORE
'    If R43 > R44 Then GoTo ERRORE
'    If R44 > R45 Then GoTo ERRORE
'    If R45 > R46 Then GoTo ERRORE
'    If R46 > R47 Then GoTo ERRORE
'    If R47 > R122 Then GoTo ERRORE
'    'controllo congruenza punti con raggi di raccordo
'    If R62 <> 0 And DIST(R52, R42, R53, R43) > 2 * Abs(R62) Then GoTo ERRORE   'c2
'    If R63 <> 0 And DIST(R53, R43, R54, R44) > 2 * Abs(R63) Then GoTo ERRORE 'c3
'    If DIST(R54, R44, R55, R45) > 2 * Abs(R64) Then GoTo ERRORE   'c4
'    If DIST(R55, R45, R56, R46) > 2 * Abs(R65) Then GoTo ERRORE   'c5
'    '...
'
'    a$ = "FIANCO 2"
'    'a$ = "- 2nd FLANK"
'    'controllo raggi obbligatori
'    If Abs(R91) < 0.01 Then GoTo ERRORE
'    If Abs(R94) < 0.01 Then GoTo ERRORE
'    If Abs(R95) < 0.01 Then GoTo ERRORE
'    If Abs(R97) < 0.01 Then GoTo ERRORE
'    'controllo raggi > 0
'    If R91 < 0 Then GoTo ERRORE
'    If R92 < 0 Then GoTo ERRORE
'    If R96 < 0 Then GoTo ERRORE
'    'verifica punti crescenti
'    If R71 > R72 Then GoTo ERRORE
'    If R72 > R73 Then GoTo ERRORE
'    If R73 > R74 Then GoTo ERRORE
'    If R74 > R75 Then GoTo ERRORE
'    If R75 > R76 Then GoTo ERRORE
'    If R76 > R77 Then GoTo ERRORE
'    If R77 > R124 Then GoTo ERRORE
'    'controllo congruenza punti con raggi di raccordo
'    If R92 <> 0 And DIST(R82, R72, R83, R73) > 2 * Abs(R92) Then GoTo ERRORE  'c12
'    If R93 <> 0 And DIST(R83, R73, R84, R74) > 2 * Abs(R93) Then GoTo ERRORE  'c13
'    If DIST(R84, R74, R85, R75) > 2 * Abs(R94) Then GoTo ERRORE  'c14
'    If DIST(R85, R75, R86, R76) > 2 * Abs(R95) Then GoTo ERRORE  'c15
'    '...
'
'    msg$ = ""
'    If R63 <> 0 Then
'        L = Sqr((R53 - R54) ^ 2 + (R43 - R44) ^ 2)
'        If L < 0.01 Then msg$ = " - " & "Distanza P4-P5 minore di " & Format(L + 0.001, "0.000") & vbCrLf
'        L = Sqr((R83 - R84) ^ 2 + (R73 - R74) ^ 2)
'        If L < 0.01 Then msg$ = msg$ & " - " & "Distanza P14-P15 minore di " & Format(L + 0.001, "0.000") & vbCrLf
'    End If
'    L = Sqr((R54 - R55) ^ 2 + (R44 - R45) ^ 2)
'    If L < 0.01 Then msg$ = msg$ & " - " & "Distanza P5-P6 minore di " & Format(L + 0.001, "0.000") & vbCrLf
'    L = Sqr((R84 - R85) ^ 2 + (R74 - R75) ^ 2)
'    If L < 0.01 Then msg$ = msg$ & " - " & "Distanza P15-P16 minore di " & Format(L + 0.001, "0.000") & vbCrLf
'    L = Sqr((R55 - R56) ^ 2 + (R45 - R46) ^ 2)
'    If L < 0.01 Then msg$ = msg$ & " - " & "Distanza P6-P7 minore di " & Format(L + 0.001, "0.000") & vbCrLf
'    L = Sqr((R85 - R86) ^ 2 + (R75 - R76) ^ 2)
'    If L < 0.01 Then msg$ = msg$ & " - " & "Distanza P16-P17 minore di " & Format(L + 0.001, "0.000") & vbCrLf
'    If msg$ <> "" Then
'      msg$ = msg$ & vbCrLf & " ( CNC ERROR !!!! )"
'      StopRegieEvents
'      MsgBox msg$, vbCritical, "WARNING!"
'      ResumeRegieEvents
'    End If
'
'  GoTo PAROK
'  '
'ERRORE:
'  '
'  'MsgBox "ERROR : wrong parameters " & a$, vbCritical, "WARNING"
'  StopRegieEvents
'  MsgBox "Parameters errors: " & a$, vbCritical, "ERROR"
'  ResumeRegieEvents
'  Exit Sub
'  '
'PAROK:
'
'Return
'
'VISUALIZZA_PROFILO_MOLA:
'  '
'  Open g_chOemPATH & "\CNC.TXT" For Output As #1
'    '****************************
'    '*        FIANCO 1          *
'    '****************************
'    stmp = "F1"
'    If (SiStampa = "Y") Then
'      X1 = 0.1 * PicW
'      Y1 = 0.9 * PicH - Printer.TextHeight(stmp)
'      If (Y1 <= Printer.ScaleHeight) And (X1 <= Printer.ScaleWidth) And (X1 >= 0) And (Y1 >= 0) Then
'        Printer.CurrentX = X1
'        Printer.CurrentY = Y1
'        Printer.Print stmp
'      End If
'    Else
'      X1 = 0.1 * PicW '- OEM1.OEM1PicCALCOLO.TextWidth(stmp)
'      Y1 = 0.9 * PicH - OEM1.OEM1PicCALCOLO.TextHeight(stmp)
'      'X1 = (-xx(0, 2) - ZeroPezzoX) * Scala + PicW / 2 - OEM1.OEM1PicCALCOLO.TextWidth(stmp) / 2
'      'Y1 = -(YY(0, 2)) * Scala + PicH / 10 + OEM1.OEM1PicCALCOLO.TextHeight(stmp)
'      If (Y1 <= OEM1.OEM1PicCALCOLO.ScaleHeight) And (X1 <= OEM1.OEM1PicCALCOLO.ScaleWidth) And (X1 >= 0) And (Y1 >= 0) Then
'        OEM1.OEM1PicCALCOLO.CurrentX = X1
'        OEM1.OEM1PicCALCOLO.CurrentY = Y1
'        OEM1.OEM1PicCALCOLO.Print stmp
'      End If
'    End If
'    Print #1, stmp
'    '
'    j = 21
'    Print #1, "G1 X=" & frmt(xx(0, j), 4)
'    Print #1, "G1 Y=" & frmt(YY(0, j), 4)
'    Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -xx(0, j), -0.9 * PicH / Scala, -xx(0, j), YY(0, j), 0, "", SiStampa, ZeroPezzoX, ZeroPezzoY)
'    X1 = xx(0, j): Y1 = YY(0, j)
'    '*** linea 2 ***
'    j = 2
'    Print #1, "G1 X=" & frmt(xx(0, j), 4) & " Y=" & frmt(YY(0, j), 4)
'    j = 1
'    Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -X1, Y1, -xx(0, j), YY(0, j), 0, "", SiStampa, ZeroPezzoX, ZeroPezzoY)
'    X1 = xx(0, j): Y1 = YY(0, j)
'    j = 2
'    Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -X1, Y1, -xx(0, j), YY(0, j), 0, "", SiStampa, ZeroPezzoX, ZeroPezzoY)
'    X1 = xx(0, j): Y1 = YY(0, j)
'    '*** cerchio 1 ***
'    j = 3
'    If Sgn(R61) > 0 Then SensoRot = "DX": FunzG = "G2" Else SensoRot = "SX": FunzG = "G3"
'    Print #1, FunzG & " X=" & frmt(xx(0, j), 4) & " Y=" & frmt(YY(0, j), 4) & " R=" & frmt(Abs(R61), 4)
'    Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -X1, Y1, -xx(0, j), YY(0, j), Abs(R61), SensoRot, SiStampa, ZeroPezzoX, ZeroPezzoY)
'    X1 = xx(0, j): Y1 = YY(0, j)
'    '*** cerchio 2 ***
'    If (R62 <> 0) Then
'      j = 4
'      If Sgn(R62) > 0 Then SensoRot = "DX": FunzG = "G2" Else SensoRot = "SX": FunzG = "G3"
'      Print #1, FunzG & " X=" & frmt(xx(0, j), 4) & " Y=" & frmt(YY(0, j), 4) & " R=" & frmt(Abs(R62), 4)
'      Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -X1, Y1, -xx(0, j), YY(0, j), Abs(R62), SensoRot, SiStampa, ZeroPezzoX, ZeroPezzoY)
'      X1 = xx(0, j): Y1 = YY(0, j)
'    End If
'    '*** cerchio 3 ***
'    If (R63 <> 0) Then
'      j = 5
'      If Sgn(R63) > 0 Then SensoRot = "DX": FunzG = "G2" Else SensoRot = "SX": FunzG = "G3"
'      Print #1, FunzG & " X=" & frmt(xx(0, j), 4) & " Y=" & frmt(YY(0, j), 4) & " R=" & frmt(Abs(R63), 4)
'      Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -X1, Y1, -xx(0, j), YY(0, j), Abs(R63), SensoRot, SiStampa, ZeroPezzoX, ZeroPezzoY)
'      X1 = xx(0, j): Y1 = YY(0, j)
'    End If
'    '*** cerchio 4 ***
'    j = 6
'    If Sgn(R64) > 0 Then SensoRot = "DX": FunzG = "G2" Else SensoRot = "SX": FunzG = "G3"
'    Print #1, FunzG & " X=" & frmt(xx(0, j), 4) & " Y=" & frmt(YY(0, j), 4) & " R=" & frmt(Abs(R64), 4)
'    Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -X1, Y1, -xx(0, j), YY(0, j), Abs(R64), SensoRot, SiStampa, ZeroPezzoX, ZeroPezzoY)
'    X1 = xx(0, j): Y1 = YY(0, j)
'    '*** cerchio 5 ***
'    j = 7
'    If Sgn(R65) > 0 Then SensoRot = "DX": FunzG = "G2" Else SensoRot = "SX": FunzG = "G3"
'    Print #1, FunzG & " X=" & frmt(xx(0, j), 4) & " Y=" & frmt(YY(0, j), 4) & " R=" & frmt(Abs(R65), 4)
'    Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -X1, Y1, -xx(0, j), YY(0, j), Abs(R65), SensoRot, SiStampa, ZeroPezzoX, ZeroPezzoY)
'    X1 = xx(0, j): Y1 = YY(0, j)
'    '*** cerchio 6 ***
'    If (R66 <> 0) Then
'      j = 8
'      If Sgn(R66) > 0 Then SensoRot = "SX": FunzG = "G3" Else SensoRot = "DX": FunzG = "G2"
'      Print #1, FunzG & " X=" & frmt(xx(0, j), 4) & " Y=" & frmt(YY(0, j), 4) & " R=" & frmt(Abs(R66), 4)
'      Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -X1, Y1, -xx(0, j), YY(0, j), Abs(R66), SensoRot, SiStampa, ZeroPezzoX, ZeroPezzoY)
'      X1 = xx(0, j): Y1 = YY(0, j)
'    End If
'    '*** linea 8 ***
'    j = 31
'    If DIST(X1, Y1, xx(0, j), YY(0, j)) > DELTA Then
'      Print #1, "G1 X=" & frmt(xx(0, j), 4) & " Y=" & frmt(YY(0, j), 4)
'      Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -X1, Y1, -xx(0, j), YY(0, j), 0, "", SiStampa, ZeroPezzoX, ZeroPezzoY)
'      X1 = xx(0, j): Y1 = YY(0, j)
'    End If
'    '*** cerchio 7 ***
'    j = 9
'    If Sgn(R67) > 0 Then SensoRot = "SX": FunzG = "G3" Else SensoRot = "DX": FunzG = "G2"
'    Print #1, FunzG & " X=" & frmt(xx(0, j), 4) & " Y=" & frmt(YY(0, j), 4) & " R=" & frmt(Abs(R67), 4)
'    Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -X1, Y1, -xx(0, j), YY(0, j), Abs(R67), SensoRot, SiStampa, ZeroPezzoX, ZeroPezzoY)
'    X1 = xx(0, j): Y1 = YY(0, j)
'    '*** cerchio 8 ***
'    j = 22
'    If Sgn(R125) > 0 Then SensoRot = "DX": FunzG = "G2" Else SensoRot = "SX": FunzG = "G3"
'    Print #1, FunzG & " X=" & frmt(xx(0, j), 4) & " Y=" & frmt(YY(0, j), 4) & " R=" & frmt(Abs(R125), 4)
'    Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -X1, Y1, -xx(0, j), YY(0, j), Abs(R125), SensoRot, SiStampa, ZeroPezzoX, ZeroPezzoY)
'  '
'    '****************************
'    '*        FIANCO 2          *
'    '****************************
'    stmp = "F2"
'    If (SiStampa = "Y") Then
'      X1 = 0.9 * PicW - Printer.TextWidth(stmp)
'      Y1 = 0.9 * PicH - Printer.TextHeight(stmp)
'      If (Y1 <= Printer.ScaleHeight) And (X1 <= Printer.ScaleWidth) And (X1 >= 0) And (Y1 >= 0) Then
'        Printer.CurrentX = X1
'        Printer.CurrentY = Y1
'        Printer.Print stmp
'      End If
'    Else
'      X1 = 0.9 * PicW - OEM1.OEM1PicCALCOLO.TextWidth(stmp)
'      Y1 = 0.9 * PicH - OEM1.OEM1PicCALCOLO.TextHeight(stmp)
'      'X1 = (-xx(0, 12) - ZeroPezzoX) * Scala + PicW / 2 - OEM1.OEM1PicCALCOLO.TextWidth(stmp) / 2
'      'Y1 = -(YY(0, 12)) * Scala + PicH / 10 + OEM1.OEM1PicCALCOLO.TextHeight(stmp)
'      If (Y1 <= OEM1.OEM1PicCALCOLO.ScaleHeight) And (X1 <= OEM1.OEM1PicCALCOLO.ScaleWidth) And (X1 >= 0) And (Y1 >= 0) Then
'        OEM1.OEM1PicCALCOLO.CurrentX = X1
'        OEM1.OEM1PicCALCOLO.CurrentY = Y1
'        OEM1.OEM1PicCALCOLO.Print stmp
'      End If
'    End If
'    Print #1, stmp
'    '
'    j = 23
'    Print #1, "G1 X=" & frmt(xx(0, j), 4)
'    Print #1, "G1 Y=" & frmt(YY(0, j), 4)
'    Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -xx(0, j), -0.9 * PicH / Scala, -xx(0, j), YY(0, j), 0, "", SiStampa, ZeroPezzoX, ZeroPezzoY)
'    X1 = xx(0, j): Y1 = YY(0, j)
'    '*** linea 12 ***
'    j = 12
'    Print #1, "G1 X=" & frmt(xx(0, j), 4) & " Y=" & frmt(YY(0, j), 4)
'    j = 11
'    Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -X1, Y1, -xx(0, j), YY(0, j), 0, "", SiStampa, ZeroPezzoX, ZeroPezzoY)
'    X1 = xx(0, j): Y1 = YY(0, j)
'    j = 12
'    Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -X1, Y1, -xx(0, j), YY(0, j), 0, "", SiStampa, ZeroPezzoX, ZeroPezzoY)
'    X1 = xx(0, j): Y1 = YY(0, j)
'    '*** cerchio 11 ***
'    j = 13
'    If Sgn(R91) > 0 Then SensoRot = "SX": FunzG = "G3" Else SensoRot = "DX": FunzG = "G2"
'    Print #1, FunzG & " X=" & frmt(xx(0, j), 4) & " Y=" & frmt(YY(0, j), 4) & " R=" & frmt(Abs(R91), 4)
'    Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -X1, Y1, -xx(0, j), YY(0, j), Abs(R91), SensoRot, SiStampa, ZeroPezzoX, ZeroPezzoY)
'    X1 = xx(0, j): Y1 = YY(0, j)
'    '*** cerchio 12 ***
'    If (R92 <> 0) Then
'      j = 14
'      If Sgn(R92) > 0 Then SensoRot = "SX": FunzG = "G3" Else SensoRot = "DX": FunzG = "G2"
'      Print #1, FunzG & " X=" & frmt(xx(0, j), 4) & " Y=" & frmt(YY(0, j), 4) & " R=" & frmt(Abs(R92), 4)
'      Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -X1, Y1, -xx(0, j), YY(0, j), Abs(R92), SensoRot, SiStampa, ZeroPezzoX, ZeroPezzoY)
'      X1 = xx(0, j): Y1 = YY(0, j)
'    End If
'    '*** cerchio 13 ***
'    If (R93 <> 0) Then
'      j = 15
'      If Sgn(R93) > 0 Then SensoRot = "SX": FunzG = "G3" Else SensoRot = "DX": FunzG = "G2"
'      Print #1, FunzG & " X=" & frmt(xx(0, j), 4) & " Y=" & frmt(YY(0, j), 4) & " R=" & frmt(Abs(R93), 4)
'      Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -X1, Y1, -xx(0, j), YY(0, j), Abs(R93), SensoRot, SiStampa, ZeroPezzoX, ZeroPezzoY)
'      X1 = xx(0, j): Y1 = YY(0, j)
'    End If
'    '*** cerchio 14 ***
'    j = 16
'    If Sgn(R94) > 0 Then SensoRot = "SX": FunzG = "G3" Else SensoRot = "DX": FunzG = "G2"
'    Print #1, FunzG & " X=" & frmt(xx(0, j), 4) & " Y=" & frmt(YY(0, j), 4) & " R=" & frmt(Abs(R94), 4)
'    Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -X1, Y1, -xx(0, j), YY(0, j), Abs(R94), SensoRot, SiStampa, ZeroPezzoX, ZeroPezzoY)
'    X1 = xx(0, j): Y1 = YY(0, j)
'    '*** cerchio 15 ***
'    j = 17
'    If Sgn(R95) > 0 Then SensoRot = "SX": FunzG = "G3" Else SensoRot = "DX": FunzG = "G2"
'    Print #1, FunzG & " X=" & frmt(xx(0, j), 4) & " Y=" & frmt(YY(0, j), 4) & " R=" & frmt(Abs(R95), 4)
'    Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -X1, Y1, -xx(0, j), YY(0, j), Abs(R95), SensoRot, SiStampa, ZeroPezzoX, ZeroPezzoY)
'    X1 = xx(0, j): Y1 = YY(0, j)
'    '*** cerchio 16 ***
'    If (R96 <> 0) Then
'      j = 18
'      If Sgn(R96) > 0 Then SensoRot = "DX": FunzG = "G2" Else SensoRot = "SX": FunzG = "G3"
'      Print #1, FunzG & " X=" & frmt(xx(0, j), 4) & " Y=" & frmt(YY(0, j), 4) & " R=" & frmt(Abs(R96), 4)
'      Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -X1, Y1, -xx(0, j), YY(0, j), Abs(R96), SensoRot, SiStampa, ZeroPezzoX, ZeroPezzoY)
'      X1 = xx(0, j): Y1 = YY(0, j)
'    End If
'    '*** linea 18 ***
'    j = 32
'    If DIST(X1, Y1, xx(0, j), YY(0, j)) > DELTA Then
'      Print #1, "G1 X=" & frmt(xx(0, j), 4) & " Y=" & frmt(YY(0, j), 4)
'      Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -X1, Y1, -xx(0, j), YY(0, j), 0, "", SiStampa, ZeroPezzoX, ZeroPezzoY)
'      X1 = xx(0, j): Y1 = YY(0, j)
'    End If
'    '*** cerchio 17 ***
'    j = 19
'    If Sgn(R97) > 0 Then SensoRot = "DX": FunzG = "G2" Else SensoRot = "SX": FunzG = "G3"
'    Print #1, FunzG & " X=" & frmt(xx(0, j), 4) & " Y=" & frmt(YY(0, j), 4) & " R=" & frmt(Abs(R97), 4)
'    Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -X1, Y1, -xx(0, j), YY(0, j), Abs(R97), SensoRot, SiStampa, ZeroPezzoX, ZeroPezzoY)
'    X1 = xx(0, j): Y1 = YY(0, j)
'    '*** cerchio 18 ***
'    j = 24
'    If Sgn(R125) > 0 Then SensoRot = "SX": FunzG = "G3" Else SensoRot = "DX": FunzG = "G2"
'    Print #1, FunzG & " X=" & frmt(xx(0, j), 4) & " Y=" & frmt(YY(0, j), 4) & " R=" & frmt(Abs(R125), 4)
'    Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -X1, Y1, -xx(0, j), YY(0, j), Abs(R125), SensoRot, SiStampa, ZeroPezzoX, ZeroPezzoY)
'    'X1 = XX(0, j): Y1 = YY(0, j)
'  Close #1
'  '
'Return
'
'
'SCRIVI_FILE_DXF_MOLA:
'
'    Open g_chOemPATH & "\MOLA.DXF" For Output As #1
'    'INIZIO FILE
'    Print #1, "0"
'    Print #1, "SECTION"
'    Print #1, "2"
'    Print #1, "ENTITIES"
'''ascissa
''    Print #1, "0"
''    Print #1, "LINE"
''    Print #1, "8"
''    Print #1, "1"
''    Print #1, "62"
''    Print #1, "1"
''    'PRIMO PUNTO
''    Print #1, "10"
''    Print #1, XX(0, 23)
''    Print #1, "20"
''    Print #1, "0"
''    Print #1, "30"
''    Print #1, "0.0"
''    'SECONDO PUNTO
''    Print #1, "11"
''    Print #1, XX(0, 21)
''    Print #1, "21"
''    Print #1, "0"
''    Print #1, "31"
''    Print #1, "0.0"
''ordinata
'    Print #1, "0"
'    Print #1, "LINE"
'    Print #1, "8"
'    Print #1, "1"
'    Print #1, "62"
'    Print #1, "1"
'    'PRIMO PUNTO
'    Print #1, "10"
'    Print #1, "0"
'    Print #1, "20"
'    Print #1, "1"
'    Print #1, "30"
'    Print #1, "0.0"
'    'SECONDO PUNTO
'    Print #1, "11"
'    Print #1, "0"
'    Print #1, "21"
'    Print #1, YY(0, 21)
'    Print #1, "31"
'    Print #1, "0.0"
'
''linea p21-p2
'     Pin = 21: Pfin = 2: GoSub DxfLine
''cerchio 1 - p2,p3
'     Numcer = 1: Pin = 2: Pfin = 3: Orario = 1: GoSub DxfCerchio
''cerchio 2 - p3,p4
'    If R62 <> 0 Then
'        Numcer = 2: Pin = 3: Pfin = 4: Orario = Sgn(R62): GoSub DxfCerchio
'    End If
''cerchio 3 - p4,p5
'    If R63 <> 0 Then
'        Numcer = 3: Pin = 4: Pfin = 5: Orario = Sgn(R63): GoSub DxfCerchio
'    End If
''cerchio 4 - p5,p6
'    Numcer = 4: Pin = 5: Pfin = 6: Orario = Sgn(R64): GoSub DxfCerchio
''cerchio 5 - p6,p7
'    Numcer = 5: Pin = 6: Pfin = 7: Orario = Sgn(R65): GoSub DxfCerchio
''cerchio 6 - p7,p8
'    If R66 <> 0 Then
'        Numcer = 6: Pin = 7: Pfin = 8: Orario = -1: GoSub DxfCerchio
'    End If
''linea p8-p31
'     Pin = 8: Pfin = 31: GoSub DxfLine
''cerchio 7 - p31,p9
'    Numcer = 7: Pin = 31: Pfin = 9: Orario = -1: GoSub DxfCerchio
''cerchio 8 - p9,p22
'    Numcer = 8: Pin = 9: Pfin = 22: Orario = Sgn(R125): GoSub DxfCerchio
''FIANCO 2
''linea p23-p12
'     Pin = 23: Pfin = 12: GoSub DxfLine
''cerchio 11 - p12,p13
'     Numcer = 11: Pin = 12: Pfin = 13: Orario = -1: GoSub DxfCerchio
''cerchio 12 - p13,p14
'    If R92 <> 0 Then
'        Numcer = 12: Pin = 13: Pfin = 14: Orario = -1: GoSub DxfCerchio
'    End If
''cerchio 13 - p14,p15
'    If R93 <> 0 Then
'        Numcer = 13: Pin = 14: Pfin = 15: Orario = -Sgn(R93): GoSub DxfCerchio
'    End If
''cerchio 14 - p15,p16
'    Numcer = 14: Pin = 15: Pfin = 16: Orario = -Sgn(R94): GoSub DxfCerchio
''cerchio 15 - p16,p17
'    Numcer = 15: Pin = 16: Pfin = 17: Orario = -Sgn(R95): GoSub DxfCerchio
''cerchio 16 - p17,p18
'    If R96 <> 0 Then
'        Numcer = 16: Pin = 17: Pfin = 18: Orario = 1: GoSub DxfCerchio
'    End If
''linea p18-p32
'     Pin = 18: Pfin = 32: GoSub DxfLine
''cerchio 17 - p32,p19
'    Numcer = 17: Pin = 32: Pfin = 19: Orario = 1: GoSub DxfCerchio
''cerchio 18 - p19,p24
'    Numcer = 18: Pin = 19: Pfin = 24: Orario = -Sgn(R125): GoSub DxfCerchio
''FINE FILE
'    Print #1, "0"
'    Print #1, "ENDSEC"
'    Print #1, "0"
'    Print #1, "EOF"
'  Close #1
'Return
'
'DxfLine:
'    Print #1, "0"
'    Print #1, "LINE"
'    Print #1, "8"
'    Print #1, "1"
'    Print #1, "62"
'    Print #1, "1"
'    'PRIMO PUNTO
'    Print #1, "10"
'    Print #1, xx(0, Pin)
'    Print #1, "20"
'    Print #1, YY(0, Pin)
'    Print #1, "30"
'    Print #1, "0.0"
'    'SECONDO PUNTO
'    Print #1, "11"
'    Print #1, xx(0, Pfin)
'    Print #1, "21"
'    Print #1, YY(0, Pfin)
'    Print #1, "31"
'    Print #1, "0.0"
'Return
'DxfCerchio:
'    Dim AngStart As Double
'    Dim AngEnd As Double
'    'COORD. X                             COORD. Y
'    R164 = xx(0, Pin): R165 = YY(0, Pin)
'    R168 = xx(0, Pfin): R169 = YY(0, Pfin)
'    'coordinate del centro
'    R174 = xx(0, Numcer + 100): R170 = YY(0, Numcer + 100)
'    'raggio del cerchio
'    R161 = Abs(RR(0, Numcer + 100))
'    'Calcolo angoli compresi tra 0 e 2 * Pigreco
'    AngStart = CALCOLO_ANGOLO(R164 - R174, R165 - R170)
'    AngEnd = CALCOLO_ANGOLO(R168 - R174, R169 - R170)
'    If Orario = -1 Then 'AntiOrario
'      Print #1, "0"
'      Print #1, "ARC"
'      Print #1, "8"
'      Print #1, "1"
'      Print #1, "62"
'      Print #1, "1"
'      Print #1, "10"
'      Print #1, R174 & " (x centro)"
'      Print #1, "20"
'      Print #1, R170 & " (y centro)"
'      Print #1, "30"
'      Print #1, "0.0"
'      Print #1, "40"
'      Print #1, R161 & " (raggio)"
'      Print #1, "50"
'      Print #1, AngStart * 180 / PI & " (angolo iniziale)"
'      Print #1, "51"
'      Print #1, AngEnd * 180 / PI & " (angolo finale)"
'    Else
'      'G2 Y=R169 X=R168 CR=R161
'      Print #1, "0"
'      Print #1, "ARC"
'      Print #1, "8"
'      Print #1, "1"
'      Print #1, "62"
'      Print #1, "1"
'      Print #1, "10"
'      Print #1, R174 & " (x centro)"
'      Print #1, "20"
'      Print #1, R170 & " (y centro)"
'      Print #1, "30"
'      Print #1, "0.0"
'      Print #1, "40"
'      Print #1, R161 & " (raggio)"
'      Print #1, "50"
'      Print #1, AngEnd * 180 / PI & " (angolo iniziale)"
'      Print #1, "51"
'      Print #1, AngStart * 180 / PI & " (angolo finale)"
'    End If
'Return
'
'WRTPUNTI:
''Open g_chOemPATH & "\PUNTI.TXT" For Output As #1
''Print #1, " P R F R C R F: " & Format$(Date)
''Print #1, "       R39 =" & frmt(R39, 4)
''Print #1, "       R37 =" & frmt(R37, 4)
''Print #1, "       R38 =" & frmt(R38, 4)
''Print #1, " R106=" & frmt(R106, 4) & " R107=" & frmt(R107, 4)
''Print #1, " R41 =" & frmt(FnRG(R41), 4) & "  R51=" & frmt(FnRG(R51), 4)
''Print #1, " R42 =" & frmt(FnRG(R42), 4) & "  R52=" & frmt(FnRG(R52), 4)
''...
''Close #1
'Return
'
'Exit Sub
'
End Sub

Sub CONTROLLO_CRFORMA_CALCOLO_DATI(ByVal FINESTRA As Form, ByVal TIPO As String)
'
Dim DBB  As Database
Dim RSS  As Recordset
'
Dim i    As Integer
Dim j    As Integer
Dim Atmp As String * 255
Dim stmp As String
Dim riga As String
Dim ret  As Integer
Dim rsp  As Integer
Dim scmd As String
'
Dim DIRETTORIO_WKStmp  As String
Dim MODALITA           As String
'
On Error GoTo errCONTROLLO_CRFORMA_CALCOLO_DATI
  '
  'ABILITAZIONE DELLA SELEZIONE DELLA PAGINA DEL CONTROLLO IN MACCHINA
  '
  'LEGGO IL PERCORSO PER IL DIRETTORIO DEI CONTROLLI
  riga = GetInfo("CREATORI_FORMA", "PathMIS", Path_LAVORAZIONE_INI): riga = UCase$(Trim$(riga))
  If (Len(riga) <= 0) Then Exit Sub
  '
  flagErrore = False
  '
  If (flagErrore = False) Then Call LETTURA_PROFILO_CRFORMA(FINESTRA, TIPO)
                               'fuori dal For perch� i due fianchi sono identici
  '
  ReDim Xcrfa(2, NPctrl)   ': Ascissa sez.Assiale CR di forma
  ReDim Rcrfa(2, NPctrl)   ': Ordinata sez.Assiale CR di forma
  ReDim APcrfa(2, NPctrl)  ': AP sez.Assiale CR di forma
  '
  For Fianco = 1 To 2
    FlagFia = 3 - Fianco * 2 ' 1=F1 , -1=F2
    If (flagErrore = False) Then
      For i = 1 To NPctrl
        Call CalcUnPuntoAxialCRF(Xcre(i), Rcre(i), APcre(i), _
              Xcrfa(Fianco, i), Rcrfa(Fianco, i), APcrfa(Fianco, i), _
              InFilD, InFil, DprimCr, PasAxial)
      Next i
    End If
    'elimino punti non controllabili
    Call CtrlSCREMAPUNTI_CRF(Fianco, NPctrl)
  Next Fianco
  '
  If (flagErrore = False) Then Call CONTROLLO_CRFORMA_CREAZIONE_SOTTOPROGRAMMI
  '
  If (flagErrore = False) Then
    '
    'VISUALIZZAZIONE DEL PROFILO SOLO SULLA FINESTRA DEI PARAMETRI
    If (UCase$(FINESTRA.Name) = "OEM1") Then Call VISUALIZZAZIONE_PROFILO_CRFORMA(0, "N")
    '
    MODALITA = GetInfo("Configurazione", "MODALITA", PathFILEINI)
    MODALITA = UCase$(Trim$(MODALITA))
    '
    If (MODALITA <> "OFF-LINE") Then
      '
      DIRETTORIO_WKStmp = LEGGI_DIRETTORIO_WKS("CREATORI_FORMA")
      '
      ReDim DIRETTORIO_WKS(0)
      ReDim SOTTOPROGRAMMI(0)
      '
      ReDim Preserve DIRETTORIO_WKS(1): DIRETTORIO_WKS(1) = DIRETTORIO_WKStmp
      ReDim Preserve SOTTOPROGRAMMI(1): SOTTOPROGRAMMI(1) = GetInfo("CREATORI_FORMA", "SPF(1)", Path_LAVORAZIONE_INI)
      ReDim Preserve DIRETTORIO_WKS(2): DIRETTORIO_WKS(2) = DIRETTORIO_WKStmp
      ReDim Preserve SOTTOPROGRAMMI(2): SOTTOPROGRAMMI(2) = GetInfo("CREATORI_FORMA", "SPF(2)", Path_LAVORAZIONE_INI)
      ReDim Preserve DIRETTORIO_WKS(3): DIRETTORIO_WKS(3) = DIRETTORIO_WKStmp
      ReDim Preserve SOTTOPROGRAMMI(3): SOTTOPROGRAMMI(3) = GetInfo("CREATORI_FORMA", "SPF(3)", Path_LAVORAZIONE_INI)
      '
      Set Session = GetObject("@SinHMIMCDomain.MCDomain")
      For i = 1 To 3
        WRITE_DIALOG SOTTOPROGRAMMI(i) & ".SPF --> CNC"
        Session.CopyNC g_chOemPATH & "\" & SOTTOPROGRAMMI(i) & ".SPF", "/NC/WKS.DIR/" & DIRETTORIO_WKS(i) & ".WPD/" & SOTTOPROGRAMMI(i) & ".SPF", MCDOMAIN_COPY_NC
      Next i
      Set Session = Nothing
      '
    End If
    '
  End If
  '
Exit Sub

errCONTROLLO_CRFORMA_CALCOLO_DATI:
  WRITE_DIALOG "SUB CONTROLLO_CRFOMA_CALCOLO_DATI ERROR " & Err
  Resume Next
  
End Sub

Sub CtrlSCREMAPUNTI_CRF(ByVal Fianco As Integer, ByVal NPctrl As Integer)
  '--- non la definirei neanche "impostata"
  '--- definire il criterio con cui scremare i punti ---
  '
  Dim i As Integer
  '
  Dim Xc As Double, YC As Double, Rc As Double
  Dim RSonda As Double: RSonda = 0.6
  '
  Dim Xctrl() As Double
  Dim Rctrl() As Double
  Dim APctrl() As Double
  '
  ReDim Xctrl(2, NPctrl)
  ReDim Rctrl(2, NPctrl)
  ReDim APctrl(2, NPctrl)
  '
  Dim Np As Integer: Np = 0
  'elimino punti non controllabili
  For i = 3 To NPctrl
    RAG3P Xcrfa(Fianco, i - 2), Rcrfa(Fianco, i - 2), _
          Xcrfa(Fianco, i - 1), Rcrfa(Fianco, i - 1), _
          Xcrfa(Fianco, i), Rcrfa(Fianco, i), Xc, YC, Rc
    If Rc > RSonda Then
      Np = Np + 1
      Xctrl(Fianco, Np) = Xcrfa(Fianco, i - 2)
      Rctrl(Fianco, Np) = Rcrfa(Fianco, i - 2)
      APctrl(Fianco, Np) = APcrfa(Fianco, i - 2)
      If i = NPctrl Then
        Np = Np + 2
        Xctrl(Fianco, Np + 1) = Xcrfa(Fianco, i - 1)
        Rctrl(Fianco, Np + 1) = Rcrfa(Fianco, i - 1)
        APctrl(Fianco, Np + 1) = APcrfa(Fianco, i - 1)
        Xctrl(Fianco, Np + 2) = Xcrfa(Fianco, i)
        Rctrl(Fianco, Np + 2) = Rcrfa(Fianco, i)
        APctrl(Fianco, Np + 2) = APcrfa(Fianco, i)
      End If
    Else
      If YC > Rcrfa(Fianco, i - 2) And _
         YC > Rcrfa(Fianco, i - 1) And _
         YC > Rcrfa(Fianco, i) Then
          'SALTA PUNTO
        
          
      End If
    End If
  Next i
  
End Sub

Public Sub LETTURA_PROFILO_CRFORMA(ByVal FINESTRA As Form, ByVal TIPO_LETTURA As String)
'
Dim stmp        As String
Dim vect$()
Dim k           As Integer
Dim kk          As Integer
Dim PercorsoINI As String
Dim MODALITA    As String
'
'********************************************************
  NPctrl = 99        ': num. punti per controllo
  ReDim PCRE(NPctrl) ': stringa listbox punti crema
'/!\ ATTENZIONE /!\ Npctrl viene riassegnato (vedi sotto)
'********************************************************
'
Const SEZIONE = "OEM1_PROFILO"
Const TIPO_LAVORAZIONE = "CREATORI_FORMA"
'
On Error GoTo errLETTURA_PROFILO_CRFORMA
  '
  MODALITA = GetInfo("Configurazione", "MODALITA", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))
  '
  'GRAFICO
  If (MODALITA <> "OFF-LINE") Then
    Et$ = LEGGI_NomePezzo(TIPO_LAVORAZIONE)
  Else
    Et$ = GetInfo(TIPO_LAVORAZIONE, "NomePezzoInserimento", Path_LAVORAZIONE_INI)
  End If
  '
  Select Case TIPO_LETTURA
    '
    Case "FILE_INI"
      '
      PercorsoINI = g_chOemPATH & "\" & TIPO_LAVORAZIONE & ".INI"
      '
      NFil = val(GetInfo(SEZIONE, "Nfil", PercorsoINI))
      PasAxial = Abs(val(GetInfo(SEZIONE, "PasAxial", PercorsoINI)))
      PasHel = val(GetInfo(SEZIONE, "PasHel", PercorsoINI))
      DestCr = val(GetInfo(SEZIONE, "DestCr", PercorsoINI))
      Nscan = val(GetInfo(SEZIONE, "Nscan", PercorsoINI))
      Camma = val(GetInfo(SEZIONE, "Camma", PercorsoINI))
      SottIntaglio = val(GetInfo(SEZIONE, "SottIntaglio", PercorsoINI))
      Senso = val(GetInfo(SEZIONE, "Senso", PercorsoINI))
      DisTagliente = val(GetInfo(SEZIONE, "DisTagliente", PercorsoINI))
      InMola = val(GetInfo(SEZIONE, "InMola", PercorsoINI))
      Mn = val(GetInfo(SEZIONE, "Mn", PercorsoINI))
      idCtrlSW = val(GetInfo(SEZIONE, "idCtrlSW", PercorsoINI))
      CtrlDiam = val(GetInfo(SEZIONE, "CtrlDiam", PercorsoINI))
      '
      'Hr = val(GetInfo(SEZIONE, "HR", PercorsoINI))
      '
      InFil = Acs(Mn * PG * NFil / PasAxial)
      InFilD = InFil * 180 / PG
      AngProjD = InFilD
      DprimCr = PasAxial / PG / Tan(InFil)
      AngProj = AngProjD * PG / 180
      '
      '*** PUNTI CREMAGLIERA PER CONTROLLO ***
      For i = 1 To NPctrl
        PCRE(i) = GetInfo(SEZIONE, "PCRE(" & CStr(i) & ")", PercorsoINI)
      Next i
      '
    Case "LISTE"
      '
      NFil = val(FINESTRA.List1(1).List(0))
      PasAxial = Abs(val(FINESTRA.List1(1).List(1)))
      PasHel = val(FINESTRA.List1(1).List(2))
      DestCr = val(FINESTRA.List1(1).List(3))
      Nscan = val(FINESTRA.List1(1).List(4))
      Camma = val(FINESTRA.List1(1).List(5))
      SottIntaglio = val(FINESTRA.List1(1).List(6))
      Senso = val(FINESTRA.List1(1).List(7))
      DisTagliente = val(FINESTRA.List1(1).List(8))
      InMola = val(FINESTRA.List1(1).List(10))
      Mn = val(FINESTRA.List1(1).List(11))
      idCtrlSW = val(FINESTRA.List1(1).List(12))
      CtrlDiam = val(FINESTRA.List1(1).List(13))
      '
      InFil = Acs(Mn * PG * NFil / PasAxial)
      InFilD = InFil * 180 / PG
      AngProjD = InFilD
      DprimCr = PasAxial / PG / Tan(InFil)
      AngProj = AngProjD + PG / 180
      '
      '*** PUNTI CREMAGLIERA PER CONTROLLO ***
      For i = 1 To NPctrl
        PCRE(i) = FINESTRA.List2(2).List(i - 1)
      Next i
      '
  End Select
  '
  k = 0
  For i = 1 To NPctrl
    If PCRE(i) <> "" And PCRE(i) <> "0" Then
      k = k + 1
      PCRE(k) = PCRE(i)
    End If
    'If val(PCRE(i)) <> 0 Then k = i
  Next i
  NPctrl = k
  '*******************************************************
  ReDim Xcre(NPctrl)   ': Ascissa sez.tr. punto crema
  ReDim Rcre(NPctrl)   ': Raggio sez.tr. punto crema
  ReDim APcre(NPctrl)  ': Ang.Press. sez.tr. punto crema
  '*******************************************************
  For i = 1 To NPctrl
    stmp = UCase$(Trim$(PCRE(i)))
    vect$ = Split(stmp, " ")
    kk = 0
    For k = 0 To UBound(vect$)
      If vect$(k) <> "" Then
        kk = kk + 1
        If kk = 1 Then Xcre(i) = val(vect$(k))
        If kk = 2 Then Rcre(i) = DestCr / 2 - val(vect$(k))
        If kk = 3 Then APcre(i) = val(vect$(k)) * PG / 180
      End If
    Next k
  Next i
  '
  'spessore e addendum (sez.tr.crema) su punto di rif.
  Sw = 2 * Xcre(idCtrlSW)
  Hkw = DestCr / 2 - Rcre(idCtrlSW)
  '
Exit Sub

errLETTURA_PROFILO_CRFORMA:
  WRITE_DIALOG "SUB LETTURA_PROFILO_CRFORMA ERROR " & Err
  Resume Next
  
End Sub


Sub VISUALIZZAZIONE_PROFILO_CRFORMA(Scala As Double, ByVal SiStampa As String)
'
Dim ZeroPezzoX    As Single
Dim ZeroPezzoY    As Single
Dim pXX()         As Single
Dim pYY()         As Single
Dim i             As Integer
Dim j             As Integer
Dim pX1           As Single
Dim pX2           As Single
Dim pY1           As Single
Dim pY2           As Single
Dim PicW          As Single
Dim PicH          As Single
Dim ScalaY        As Single
Dim ScalaX        As Single
Dim stmp          As String
Dim Atmp          As String * 255
Dim TipoCarattere As String
Dim iFianco       As Integer
Dim Np            As Integer
'
Dim USCITA As Control
'
On Error GoTo errVISUALIZZAZIONE_PROFILO_CRFORMA
  '
  OEMX.OEM1frameCalcolo.Caption = ""
  '
      If (LINGUA = "CH") Then
    TipoCarattere = "MS Song"
  ElseIf (LINGUA = "RU") Then
    TipoCarattere = "Arial Cyr"
  Else
    TipoCarattere = "Courier New"
  End If
  '
  OEMX.lblINP(0).Visible = True
  OEMX.txtINP(0).Visible = True
  '
  OEMX.OEM1lblOX.Visible = True
  OEMX.OEM1txtOX.Visible = True
  '
  OEMX.OEM1lblOY.Visible = True
  OEMX.OEM1txtOY.Visible = True
  '
  stmp = GetInfo("DIS0", "PicW", Path_LAVORAZIONE_INI)
  If val(stmp) > 0 Then
    OEMX.OEM1PicCALCOLO.ScaleWidth = val(stmp)
  Else
    OEMX.OEM1PicCALCOLO.ScaleWidth = 190
  End If
  stmp = GetInfo("DIS0", "PicH", Path_LAVORAZIONE_INI)
  If val(stmp) > 0 Then
    OEMX.OEM1PicCALCOLO.ScaleHeight = val(stmp)
  Else
    OEMX.OEM1PicCALCOLO.ScaleHeight = 95
  End If
  '
  PicH = OEMX.OEM1PicCALCOLO.ScaleHeight
  PicW = OEMX.OEM1PicCALCOLO.ScaleWidth
  '
  ZeroPezzoX = val(OEMX.OEM1txtOX.Text)
  ZeroPezzoY = val(OEMX.OEM1txtOY.Text)
  '
  'VISUALIZZO LA SCALA
  i = LoadString(g_hLanguageLibHandle, 1000, Atmp, 255)
  If (i > 0) Then OEMX.lblINP(0).Caption = Left$(Atmp, i)
  i = LoadString(g_hLanguageLibHandle, 1099, Atmp, 255)
  If (i > 0) Then OEMX.OEM1frameCalcolo.Caption = Left$(Atmp, i)
  '
  If (SiStampa = "Y") Then
    Set USCITA = Printer
    'OEMX.PicLogo.Picture = LoadPicture(g_chOemPATH & "\" & "Logo2.bmp")
    Printer.ScaleMode = 6
    For i = 1 To 2
      Printer.FontName = TipoCarattere
      Printer.FontSize = 10
    Next i
    WRITE_DIALOG OEMX.OEM1frameCalcolo.Caption & " Printing on " & Printer.DeviceName
  Else
    Set USCITA = OEMX.OEM1PicCALCOLO
    OEMX.OEM1PicCALCOLO.Cls
    WRITE_DIALOG ""
  End If
  '
  'CALCOLO SCALA
  Dim Xmin As Double, Xmax As Double, Ymin As Double, Ymax As Double
  Xmin = 99999: Xmax = -99999: Ymin = 99999: Ymax = -99999
  For iFianco = 1 To 2
    For i = 1 To NPctrl
      If Xcrfa(iFianco, i) < Xmin Then Xmin = Xcrfa(iFianco, i)
      If Xcrfa(iFianco, i) > Xmax Then Xmax = Xcrfa(iFianco, i)
      If Rcrfa(iFianco, i) < Ymin Then Ymin = Rcrfa(iFianco, i)
      If Rcrfa(iFianco, i) > Ymax Then Ymax = Rcrfa(iFianco, i)
    Next i
  Next iFianco
  If (Scala <= 0) Then
    ScalaY = PicH / (Ymax - Ymin + 0.2)
    ScalaX = PicW / (2 * Xmax + 0.2)
    'ScalaY = Abs(PicH / (Rcrfa(1, 1) - Rcrfa(1, NPctrl) + 0.2))
    'ScalaX = Abs(PicW / (2 * Xcrfa(1, NPctrl) + 0.2))
    Scala = ScalaX
    If (ScalaX > ScalaY) Then Scala = ScalaY
  End If
  '
  If (Scala >= 1) Then Scala = Int(Scala)
  '
  OEMX.txtINP(0).Text = frmt(Scala, 4)
  '
  ScalaY = Scala
  ScalaX = Scala
  '
  'ASSE ORIZZONTALE
  pX1 = 0
  pY1 = PicH
  pX2 = PicW
  pY2 = PicH
  'Traccio la linea
  If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
    USCITA.Line (pX1, pY1)-(pX2, pY2)
  End If
  '
  'ASSE VERTICALE
  pX1 = PicW / 2
  pY1 = 0
  pX2 = PicW / 2
  pY2 = PicH
  'Traccio la linea
  If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
    USCITA.Line (pX1, pY1)-(pX2, pY2)
  End If
  '
  stmp = " " & OEMX.lblINP(0).Caption & " --> " & frmt$(Scala, 4) & " : 1"
  USCITA.CurrentX = 0
  USCITA.CurrentY = 0
  USCITA.Print stmp
  '
  stmp = " " & Et$ & " "
  USCITA.CurrentX = PicW - USCITA.TextWidth(stmp)
  USCITA.CurrentY = 0
  USCITA.Print stmp
  '
  'CAMBIO NOMI
  Np = NPctrl
  ReDim pXX(Np): ReDim pYY(Np)
  For iFianco = 1 To 2
    For i = 1 To Np
      pXX(i) = -(iFianco - 1.5) / 0.5 * Xcrfa(iFianco, i): pYY(i) = Rcrfa(iFianco, i) - Rcrfa(iFianco, Np)
    Next i
    For i = 1 To Np - 1
      pX1 = pXX(i)
      pY1 = pYY(i) + 0.1
      pX2 = pXX(i + 1)
      pY2 = pYY(i + 1) + 0.1
      pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
      pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH
      pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
      pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH
      'Traccio la linea
      If (pY1 <= USCITA.ScaleHeight) And (pX1 <= USCITA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
        USCITA.Line (pX1, pY1)-(pX2, pY2)
      End If
    Next i
    stmp = " F" & Format$(iFianco, "0") & " "
    If (pY2 <= USCITA.ScaleHeight) And (pX2 <= USCITA.ScaleWidth) And (pX2 >= 0) And (pY2 >= 0) Then
      USCITA.CurrentX = pX2 - USCITA.TextWidth(stmp) / 2
      USCITA.CurrentY = pY2 - USCITA.TextHeight(stmp)
      USCITA.Print stmp
    End If
    '
  Next iFianco
  '
  If (SiStampa = "Y") Then USCITA.EndDoc
  '
Exit Sub

errVISUALIZZAZIONE_PROFILO_CRFORMA:
  WRITE_DIALOG "SUB VISUALIZZAZIONE_PROFILO_CRFORMA ERROR " & Err
  Resume Next

End Sub

Public Sub AGGIORNA_LAVORO_CRFORMA()
'
Dim MODALITA As String
'
On Error GoTo errAGGIORNA_LAVORO_CRFORMA
  '
  'Call AZZERA_LAVORO_CRFORMA
  '
  MsgBox "DA RIFARE PER IL NUOVO DATABASE SVG 12/01/09"
  'LETTURA DELLA MODALITA DI MODIFICA DATI
  MODALITA = GetInfo("Configurazione", "Modalita", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))
  '
  Select Case MODALITA
    '
    Case "OFF-LINE"
      'LETTURA PAGINA PROFILO
      'Call LETTURA_PROFILO_CRFORMA(OEM1, "LISTE")
      'AGGIORNA SEZIONE LAVORO CRFORMA
      'Call AGGIORNA_SEZIONE_LAVORO
    Case "SPF"
      'LETTURA PAGINA PROFILO
      'Call LETTURA_PROFILO_CRFORMA(OEM1, "LISTE")
      'CREAZIONE ED INVIO SOTTOPROGRAMMA OEM1_LAVORO15
      'Call CREAZIONE_OEM1_LAVORO15
  End Select
  '
Exit Sub

errAGGIORNA_LAVORO_CRFORMA:
  WRITE_DIALOG "SUB AGGIORNA_LAVORO_CRFORMA ERROR " & Err
  Resume Next

End Sub

Public Sub AGGIORNA_SEZIONE_LAVORO()
'
Const TIPO_LAVORAZIONE = "CREATORI_FORMA"
'
Dim SEZIONE     As String
Dim ret         As Long
Dim PercorsoINI As String
Dim Ndec        As Integer
'
On Error GoTo errAGGIORNA_SEZIONE_LAVORO
  '
  PercorsoINI = g_chOemPATH & "\" & TIPO_LAVORAZIONE & ".INI"
  '
  Ndec = 4
  SEZIONE = "OEM1_LAVORO"
  'MOFIDICO IL FILE INI CON I VALORI DEI PARAMETRI IMPOSTATI DALLA PAGINA OEM1_PROFILO15
  ret = WritePrivateProfileString(SEZIONE, "R5", frmt(NFil, Ndec), PercorsoINI)
  If (Senso >= 0) Then
    ret = WritePrivateProfileString(SEZIONE, "R206", frmt(Abs(PasAxial), Ndec), PercorsoINI)
  Else
    ret = WritePrivateProfileString(SEZIONE, "R206", frmt(-Abs(PasAxial), Ndec), PercorsoINI)
  End If
  If (PasHel <> 0) Then
    ret = WritePrivateProfileString(SEZIONE, "R207", "1/" & frmt(PasHel, Ndec), PercorsoINI)
  Else
    ret = WritePrivateProfileString(SEZIONE, "R207", "0", PercorsoINI)
  End If
  ret = WritePrivateProfileString(SEZIONE, "R191", frmt(Nscan, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R192", frmt(DestCr, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R193", frmt(Camma, Ndec), PercorsoINI)
  If (Senso >= 0) Then
    ret = WritePrivateProfileString(SEZIONE, "R126", frmt(Abs(InMola), Ndec), PercorsoINI)
  Else
    ret = WritePrivateProfileString(SEZIONE, "R126", frmt(-Abs(InMola), Ndec), PercorsoINI)
  End If
  '
Exit Sub

errAGGIORNA_SEZIONE_LAVORO:
  WRITE_DIALOG "SUB AGGIORNA_SEZIONE_LAVORO ERROR " & Err
  Resume Next

End Sub

Public Sub CREAZIONE_OEM1_LAVORO15()
'
'INPUT:
'NomeTbCorrente = pagina video da cui richiamo la creazione di OEM1_LAVORO15
'Serve per evitare di perdere i dati della pagina corrente salvati su S375G.INI
'
Dim Session        As IMCDomain
Dim DIRETTORIO_WKS As String
Dim SOTTOPROGRAMMA As String
'
Dim rsp     As Long
'
Const Ndec = 4
'
On Error GoTo errCREAZIONE_OEM1_LAVORO15
  '
  DIRETTORIO_WKS = LEGGI_DIRETTORIO_WKS("CREATORI_FORMA")
  '
  Set Session = GetObject("@SinHMIMCDomain.MCDomain")
  '
  '**** CREAZIONE ED INVIO SOTTOPROGRAMMA OEM1_LAVORO15
  SOTTOPROGRAMMA = "OEM1_LAVORO15"
  '***
    'CREAZIONE DEL FILE INI PER SALVARE I DATI ATTUALI
    Call CREAZIONE_OEM1_FILESPF(SOTTOPROGRAMMA)
    '
    'MOFIDICO IL FILE INI CON I VALORI DEI PARAMETRI IMPOSTATI DALLA PAGINA OEM1_PROFILO15
    rsp = WritePrivateProfileString(SOTTOPROGRAMMA, "R5", frmt(NFil, Ndec), PathFileSPF)
    If (Senso >= 0) Then
      rsp = WritePrivateProfileString(SOTTOPROGRAMMA, "R206", frmt(Abs(PasAxial), Ndec), PathFileSPF)
    Else
      rsp = WritePrivateProfileString(SOTTOPROGRAMMA, "R206", frmt(-Abs(PasAxial), Ndec), PathFileSPF)
    End If
    If (PasHel <> 0) Then
      rsp = WritePrivateProfileString(SOTTOPROGRAMMA, "R207", "1/" & frmt(PasHel, Ndec), PathFileSPF)
    Else
      rsp = WritePrivateProfileString(SOTTOPROGRAMMA, "R207", "0", PathFileSPF)
    End If
    rsp = WritePrivateProfileString(SOTTOPROGRAMMA, "R191", frmt(Nscan, Ndec), PathFileSPF)
    rsp = WritePrivateProfileString(SOTTOPROGRAMMA, "R192", frmt(DestCr, Ndec), PathFileSPF)
    rsp = WritePrivateProfileString(SOTTOPROGRAMMA, "R193", frmt(Camma, Ndec), PathFileSPF)
    If (Senso >= 0) Then
      rsp = WritePrivateProfileString(SOTTOPROGRAMMA, "R126", frmt(Abs(InMola), Ndec), PathFileSPF)
    Else
      rsp = WritePrivateProfileString(SOTTOPROGRAMMA, "R126", frmt(-Abs(InMola), Ndec), PathFileSPF)
    End If
    '
    'CREAZIONE DEL SOTTOPROGRAMMA OEM1_LAVORO15 DAL FILE INI
    Call CREA_SOTTOPROGRAMMA(SOTTOPROGRAMMA)
    '
  '***
  Session.CopyNC g_chOemPATH & "\" & SOTTOPROGRAMMA & ".SPF", "/NC/WKS.DIR/" & DIRETTORIO_WKS & ".WPD/" & SOTTOPROGRAMMA & ".SPF", MCDOMAIN_COPY_NC
  '
  Set Session = Nothing
  '
  '*** RIPRISTINO DEI DATI DELLA PAGINA CORRENTE SU FILE DI APPOGGIO (S375G.INI)
  Call CREAZIONE_OEM1_FILESPF(ACTUAL_PARAMETER_GROUP)
  
Exit Sub

errCREAZIONE_OEM1_LAVORO15:
  WRITE_DIALOG "SUB CREAZIONE_OEM1_LAVORO15 ERROR " & Err
  Resume Next

End Sub

Sub CANCELLAMI()
'
'crea finto controllo
Dim i             As Integer
Dim NomeFile      As String
Dim PathMIS       As String: PathMIS = g_chOemPATH & "\MEASURE\CREATORI_FORMA"
'
On Error GoTo errCANCELLAMI:
'
Dim Err           As Double: Err = 0.1
Dim errSpost      As Double  'ERRORE SPOSTAMENTO
Dim errHr         As Double  'ERRORE ALTEZZA
Dim Aux           As Double
Dim MIN           As Double
Dim Max           As Double
'
Dim TIPO_LAVORAZIONE As String
'
  MIN = 99999: Max = -99999
  '
  'LETTURA DEL TIPO DI LAVORAZIONE
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  '
  NomeFile = LEGGI_NomePezzo(TIPO_LAVORAZIONE)
  NomeFile = "ctrl_" & NomeFile
  Open PathMIS & "\" & NomeFile For Output As #1
    Print #1, "0 0 0 0 0 0 0 0 0 0 "; CStr(NPctrl); " 0 0 0 ";
    Print #1, frmt(DestCr + 0.05 * (Rnd() - 0.5), 4); " ";
    Print #1, frmt(0.01 * (Rnd() - 0.5), 4); " ";
    Print #1, frmt(Sw + 0.02 * (Rnd() - 0.5), 4)
    
    For i = 1 To NPctrl
      Print #1, frmt(Xcrfa(1, i), 4); " ";
      Print #1, frmt(Rcrfa(1, i), 4); " ";
      Print #1, frmt(APcrfa(1, i) * 180 / PG, 4); " ";
      'Print #1, frmt(0.01 * (Rnd() - 0.5), 4)
      errSpost = 0.02:  errHr = 0.01
      Aux = ((Rcrfa(1, i) - Rcrfa(1, NPctrl)) / (Rcrfa(1, 1) - Rcrfa(1, NPctrl)))
      Aux = errSpost + errHr * Aux
      Print #1, frmt(Aux / Sin(APcrfa(1, i)), 4)
    Next i
    For i = 1 To NPctrl
      Print #1, frmt(-Xcrfa(2, i), 4); " ";
      Print #1, frmt(Rcrfa(2, i), 4); " ";
      Print #1, frmt(APcrfa(2, i) * 180 / PG, 4); " ";
      'Print #1, frmt(0.01 * (Rnd() - 0.5), 4)
      'Print #1, frmt(0.05, 4)
      If i <= 5 Then Print #1, frmt(0, 4)
      If i > 5 And i <= 16 Then
        Aux = (Rcrfa(2, i) - Rcrfa(2, Int(NPctrl / 2))) / (Rcrfa(2, 1) - Rcrfa(2, NPctrl))
        Print #1, frmt(Err * Aux / Cos(APcrfa(2, i)), 4)
      End If
      If i > 16 Then Print #1, frmt(0, 4)
'      Print #1, frmt(0.01 * (0.5 - Rcrfa(2, i) / Rcrfa(2, 1)), 4)
      If Err * Aux < MIN Then MIN = Err * Aux
      If Err * Aux > Max Then Max = Err * Aux
    Next i
  Close #1
Exit Sub

errCANCELLAMI:
  'CHIUDO I FILE EVENTUALMENTE APERTI
  If (FreeFile > 1) Then Close

End Sub

Public Sub CONTROLLO_CRFORMA_CREAZIONE_SOTTOPROGRAMMI()
'
Dim i         As Integer
Dim FeedCtr   As Double
Dim ErMax     As Double
Dim InerziaY  As Double
Dim InerziaAX As Double
Dim DSM       As Double
Dim Xsr       As Double
Dim NomeFile  As String
'
On Error Resume Next
  '
  Call CANCELLAMI
  '
  NomeFile = GetInfo("CREATORI_FORMA", "SPF(1)", PathFILEINI)
  NomeFile = NomeFile & ".SPF"
  Open g_chOemPATH & "\" & NomeFile For Output As #1
    Print #1, ";PROGRAMMA: " & NomeFile
    Print #1, ";DISEGNO  : " & Et$
    Print #1, ";DATA     : " & Date
    Print #1, "R300 ="; Mn; Tab(20); ";Mn       = Modulo normale"
    Print #1, "R301 ="; NFil; Tab(20); ";Nfil     = numero principi CR"
    Print #1, "R302 ="; AlfaDeg; Tab(20); ";Alfa     = Angolo di pressione"
    Print #1, "R303 ="; PasAxial; Tab(20); ";PasAxial = Passo assiale"
    Print #1, "R304 ="; PasHel; Tab(20); ";PasHel   = Passo spirale"
    Print #1, "R305 ="; DestCr; Tab(20); ";DestCr   = Diametro esterno teorico"
    Print #1, "R306 ="; Nscan; Tab(20); ";Nscan    = numero taglienti"
    Print #1, "R307 ="; Camma; Tab(20); ";Camma    = Camma"
    Print #1, "R308 ="; SottIntaglio; Tab(20); ";Sottintaglio = Sottintaglio in mm"
    Print #1, "R309 ="; Senso; Tab(20); ";Senso    = senso elica (1=DX, -1=SX)"
    Print #1, "R310 ="; DisTagliente; Tab(20); ";DisTagliente = Distanza da tagliente x controllo"
    Print #1, "R311 ="; Hkw; Tab(20); ";HKW      = Addendum"
    Print #1, "R312 = "; frmt(Sw, 4); Tab(20); ";SW       = Spessore"
    Print #1, "R313 = "; CStr(idCtrlSW); Tab(20); ";idCtrlSW = indice punto rif. x controllo SW"
    Print #1, "R314 = "; CStr(CtrlDiam); Tab(20); ";CtrlDiam = tipo controllo diametro"
    Print #1, "M17 ;Fine"
  Close #1
  '
  NomeFile = GetInfo("CREATORI_FORMA", "SPF(2)", PathFILEINI)
  NomeFile = NomeFile & ".SPF"
  Open g_chOemPATH & "\" & NomeFile For Output As #1
    Print #1, ";PROGRAMMA: " & NomeFile
    Print #1, ";DISEGNO  : " & Et$
    Print #1, ";DATA     : " & Date
    Print #1, "GEN[0,11] ="; NPctrl; Tab(20); ";Ntot="; Tab(28); "ultimo punto"
    Print #1, ";Profilo assiale fianco 1"
    For i = 1 To NPctrl  'NbpTotal
      Print #1, "XX[0,"; Format(i, "##0"); "]="; frmt(Xcrfa(1, i), 4); Tab(20);
      Print #1, "YY[0,"; Format(i, "##0"); "]="; frmt(Rcrfa(1, i), 4); Tab(40);
      Print #1, "TT[0,"; Format(i, "##0"); "]="; frmt(APcrfa(1, i) * 180 / PG, 4)
    Next i
    Print #1, ";Profilo assiale fianco 2"
    For i = 1 To NPctrl  'NbpTotal
      Print #1, "XX[0,"; Format(i + 100, "##0"); "]="; frmt(Xcrfa(2, i), 4); Tab(20);
      Print #1, "YY[0,"; Format(i + 100, "##0"); "]="; frmt(Rcrfa(2, i), 4); Tab(40);
      Print #1, "TT[0,"; Format(i + 100, "##0"); "]="; frmt(APcrfa(2, i) * 180 / PG, 4)
    Next i
    Print #1, "M17 ;Fine"
  Close #1
  '
  'Profilo cremagliera
  NomeFile = GetInfo("CREATORI_FORMA", "SPF(3)", PathFILEINI)
  NomeFile = NomeFile & ".SPF"
  Open g_chOemPATH & "\" & NomeFile For Output As #1
    Print #1, ";PROGRAMMA: " & NomeFile
    Print #1, ";DISEGNO  : " & Et$
    Print #1, ";DATA     : " & Date
    Print #1, ";Profilo fianco 1"
    For i = 1 To NPctrl  'NbpTotal
      Print #1, "XX[0,"; Format(i, "##0"); "]="; frmt(Xcre(i), 4); Tab(20);
      Print #1, "YY[0,"; Format(i, "##0"); "]="; frmt(Rcre(i), 4); Tab(40);
      Print #1, "TT[0,"; Format(i, "##0"); "]="; frmt(APcre(i) * 180 / PG, 4)
    Next i
    Print #1, ";Profilo fianco 2"
    For i = 1 To NPctrl  'NbpTotal
      Print #1, "XX[0,"; Format(i + 100, "##0"); "]="; frmt(Xcre(i), 4); Tab(20);
      Print #1, "YY[0,"; Format(i + 100, "##0"); "]="; frmt(Rcre(i), 4); Tab(40);
      Print #1, "TT[0,"; Format(i + 100, "##0"); "]="; frmt(APcre(i) * 180 / PG, 4)
    Next i
    Print #1, "M17 ;Fine"
  Close #1
  '
End Sub

Sub LEGGI_CNC_CONTROLLO_CRFORMA(ByVal NomeFile As String)
'
Dim i    As Integer
Dim j    As Integer
Dim X    As Double
Dim Np   As Integer
Dim riga As String
'
Dim GEN(20) As Double
'
On Error GoTo errLEGGI_CNC_CONTROLLO_CRFORMA
  '
  WRITE_DIALOG "Reading data from CNC."
  '
  Open NomeFile For Output As #1
  '
  'APRO IL FILE DI DEFAULT PER COPIARE I VALORI LETTI
  riga = ""
  For i = 1 To 17
    GEN(i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/GEN[" & Format$(i + 1, "#0") & "]"))
    riga = riga & frmt(GEN(i), 4) & " "
  Next i
  Print #1, riga
  '
  Np = GEN(11)
  '
  'FIANCO 1
  WRITE_DIALOG "Reading F1 data from CNC."
  For i = 1 To Np
    riga = ""
    X = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/XX[" & Format$(i + 1, "#0") & "]"))
    riga = riga & frmt(X, 4) & " "
    X = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/YY[" & Format$(i + 1, "#0") & "]"))
    riga = riga & frmt(X, 4) & " "
    X = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/TT[" & Format$(i + 1, "#0") & "]"))
    riga = riga & frmt(X, 4) & " "
    X = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF1[" & Format$(i + 1, "#0") & "]"))
    riga = riga & frmt(X, 4)
    'MsgBox "[" & riga & "]"
    Print #1, riga
  Next i
  '
  'FIANCO 2
  WRITE_DIALOG "Reading F2 data from CNC."
  For i = 1 To Np
    riga = ""
    X = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/XX[" & Format$(i + 1 + 100, "#0") & "]"))
    riga = riga & frmt(-X, 4) & " "
    X = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/YY[" & Format$(i + 1 + 100, "#0") & "]"))
    riga = riga & frmt(X, 4) & " "
    X = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/TT[" & Format$(i + 1 + 100, "#0") & "]"))
    riga = riga & frmt(X, 4) & " "
    X = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF2[" & Format$(i + 1, "#0") & "]"))
    riga = riga & frmt(X, 4)
    'MsgBox "[" & riga & "]"
    Print #1, riga
  Next i
  '
  Close #1
  '
  i = InStr(NomeFile, "DEFAULT")
  If (i <= 0) Then
    If (Dir$(PathMIS & "\DEFAULT") <> "") Then Kill PathMIS & "\DEFAULT"
    FileCopy NomeFile, PathMIS & "\DEFAULT"
  End If
  '
  WRITE_DIALOG ""
  '
Exit Sub

errLEGGI_CNC_CONTROLLO_CRFORMA:
  WRITE_DIALOG "SUB: LEGGI_CNC_CONTROLLO_CRFORMA " & str(Err)
  Exit Sub
  
End Sub

Sub CONTROLLO_CRFORMA_STAMPA(ByVal iXX As Integer, ByVal iYY As Integer)
'
Dim TextFont      As String
Dim i             As Integer
'
On Error Resume Next
  '
  'SEGNALAZIONE UTENTE
  WRITE_DIALOG CHK0.Text2.Text & " --> " & Printer.DeviceName
  '
  Select Case CHK0.Combo1.ListIndex
    '
    Case 0 'STAMPA DEL CERTIFICATO DI CONTROLLO
      'CARATTERI DI STAMPA
          If (LINGUA = "CH") Then
        TextFont = "MS Song"
      ElseIf (LINGUA = "RU") Then
        TextFont = "Arial Cyr"
      Else
        TextFont = "Courier New"
      End If
      'IMPOSTAZIONE STAMPANTE
      Printer.ScaleMode = 7
      Printer.DrawWidth = 1
      Printer.Orientation = 2
      'IMPOSTAZIONE DEI CARATTERI
      Printer.FontName = TextFont
      Printer.FontBold = False
      Printer.FontSize = 12
      '
      Call CtrlGRAFICO_CRF(Printer, iXX, iYY)
      '
    Case 1  'ELENCO ERRORI SUI PUNTI
      'Call CtrlPUNTI_CRF
      '
    Case 2  'RIASSUNTO ERRORI
      'Call CtrlRESULT_CRF
      '
  End Select
  '
End Sub

Sub CONTROLLO_CRFORMA_VISUALIZZAZIONE(ByVal iXX As Integer, ByVal iYY As Integer)
'
Dim TextFont      As String
'
On Error Resume Next
  '
  'NASCONDO LA CASELLA DI TESTO SE NON SERVE
  If (CHK0.Combo1.ListIndex > 0) Then
    CHK0.txtHELP.BackColor = &H0&
    CHK0.txtHELP.ForeColor = &HFFFFFF
    CHK0.txtHELP.Visible = True
  Else
    CHK0.txtHELP.Visible = False
  End If
    '
  Select Case CHK0.Combo1.ListIndex
    '
    Case 0  'GRAFICO ERRORI SU PROFILO
      'PULISCO LO SCHERMO
      CHK0.Picture1.Cls
      'CARATTERI DI STAMPA
          If (LINGUA = "CH") Then
        TextFont = "MS Song"
      ElseIf (LINGUA = "RU") Then
        TextFont = "Arial Cyr"
      Else
        TextFont = "Courier New"
      End If
      CHK0.Picture1.FontName = TextFont
      CHK0.Picture1.FontBold = False
      CHK0.Picture1.FontSize = 8
      Call CtrlGRAFICO_CRF(CHK0.Picture1, iXX, iYY)
    '
    Case 1  'ELENCO ERRORI SUI PUNTI
      Call CtrlPUNTI_CRF
    '
    Case 2  'RIASSUNTO ERRORI
      Call CtrlRESULT_CRF
    '
  End Select
  '
End Sub

Sub LETTURA_FILECONTROLLO(ByVal NomeControllo As String)

'LETTURA DATI DEL CREATORE DAL FILE DI CONTROLLO
Dim riga          As String
Dim stmp          As String
Dim i             As Integer
Dim j             As Integer
'
On Error GoTo ErrLETTURA_FILECONTROLLO
  '
  Open PathMIS & "\" & NomeControllo For Input As #1
    '
    'LETTURA DELLA PRIMA RIGA: INDICI DEI PUNTI
    Line Input #1, riga
    riga = Trim$(riga)
    i = 1
    j = InStr(riga, " ")
    While (j > 0)
      GEN(i) = val(Left$(riga, j - 1))
      riga = Right(riga, Len(riga) - j)
      i = i + 1
      j = InStr(riga, " ")
    Wend
    GEN(i) = val(riga)
    'INIZIALIZZO IL NUMERO DEI PUNTI DEL PROFILO
    Np = GEN(11)
    '
    'DIMENSIONO I VETTORI IN BASE AL NUMERO DEI PUNTI
    ReDim XX_F1(Np): ReDim YY_F1(Np): ReDim TT_F1(Np): ReDim MISF1(Np)
    ReDim XX_F2(Np): ReDim YY_F2(Np): ReDim TT_F2(Np): ReDim MISF2(Np)
    '
    'FIANCO 1
    For i = 1 To Np
      '
      Line Input #1, riga
      '
      'XX
      j = InStr(riga, " ")
      stmp = Left$(riga, j - 1)
      XX_F1(i) = val(stmp)
      'If (SCRIVI_CNC = "Y") Then Call OPC_SCRIVI_DATO("/ACC/NCK/MGUD/XX[" & Format$(i + 1, "##0") & "]", stmp)
      riga = Right(riga, Len(riga) - j)
      riga = Trim$(riga)
      'YY
      j = InStr(riga, " ")
      stmp = Left$(riga, j - 1)
      YY_F1(i) = val(stmp)
      'If (SCRIVI_CNC = "Y") Then Call OPC_SCRIVI_DATO("/ACC/NCK/MGUD/YY[" & Format$(i + 1, "##0") & "]", stmp)
      riga = Right(riga, Len(riga) - j)
      riga = Trim$(riga)
      'TT
      j = InStr(riga, " ")
      stmp = Left$(riga, j - 1)
      TT_F1(i) = val(stmp)
      'If (SCRIVI_CNC = "Y") Then Call OPC_SCRIVI_DATO("/ACC/NCK/MGUD/TT[" & Format$(i + 1, "##0") & "]", stmp)
      riga = Right(riga, Len(riga) - j)
      riga = Trim$(riga)
      'MIS
      stmp = riga
      MISF1(i) = val(stmp)
      'If (SCRIVI_CNC = "Y") Then Call OPC_SCRIVI_DATO("/ACC/NCK/MGUD/MISF1[" & Format$(i + 1, "##0") & "]", stmp)
      '
    Next i
    '
    'FIANCO 2
    For i = 1 To Np
      '
      Line Input #1, riga
      '
      'XX
      j = InStr(riga, " ")
      stmp = Left$(riga, j - 1)
      XX_F2(i) = val(stmp)
      'If (SCRIVI_CNC = "Y") Then Call OPC_SCRIVI_DATO("/ACC/NCK/MGUD/XX[" & Format$(i + 1 + 100, "##0") & "]", Mid$(stmp, 2))
      riga = Right(riga, Len(riga) - j)
      riga = Trim$(riga)
      'YY
      j = InStr(riga, " ")
      stmp = Left$(riga, j - 1)
      YY_F2(i) = val(stmp)
      'If (SCRIVI_CNC = "Y") Then Call OPC_SCRIVI_DATO("/ACC/NCK/MGUD/YY[" & Format$(i + 1 + 100, "##0") & "]", stmp)
      riga = Right(riga, Len(riga) - j)
      riga = Trim$(riga)
      'TT
      j = InStr(riga, " ")
      stmp = Left$(riga, j - 1)
      TT_F2(i) = val(stmp)
      'If (SCRIVI_CNC = "Y") Then Call OPC_SCRIVI_DATO("/ACC/NCK/MGUD/TT[" & Format$(i + 1 + 100, "##0") & "]", stmp)
      riga = Right(riga, Len(riga) - j)
      riga = Trim$(riga)
      'MIS
      stmp = riga
      MISF2(i) = val(stmp)
      'If (SCRIVI_CNC = "Y") Then Call OPC_SCRIVI_DATO("/ACC/NCK/MGUD/MISF2[" & Format$(i + 1, "##0") & "]", stmp)
      '
      'VERIFICA DEI VALORI LETTI DALLA PRIMA RIGA DEL FILE
      'stmp = ""
      'stmp = stmp & "XX_F2[0," & Format$(i, "##0") & "]=" & frmt(XX_F2(i), 4) & Chr(13)
      'stmp = stmp & "YY_F2[0," & Format$(i, "##0") & "]=" & frmt(YY_F2(i), 4) & Chr(13)
      'stmp = stmp & "TT_F2[0," & Format$(i, "##0") & "]=" & frmt(TT_F2(i), 4) & Chr(13)
      'stmp = stmp & "MISF2[0," & Format$(i, "##0") & "]=" & frmt(MISF2(i), 4) & Chr(13)
      'MsgBox stmp
      '
    Next i
    '
  Close #1

Exit Sub
  
ErrLETTURA_FILECONTROLLO:
  WRITE_DIALOG "SUB: LETTURA_FILECONTROLLO " & str(Err)
  'Resume Next
  Exit Sub

End Sub

Sub CtrlGRAFICO_CRF(ByRef USCITA As Object, ByVal iXX As Integer, ByVal iYY As Integer)
'
Dim stmp          As String
Dim i             As Integer
Dim j             As Integer
'Dim GEN(20)       As Double
Dim NomeControllo As String
'Dim XX_F1()       As Double
'Dim YY_F1()       As Double
'Dim TT_F1()       As Double
'Dim MISF1()       As Double
'Dim XX_F2()       As Double
'Dim YY_F2()       As Double
'Dim TT_F2()       As Double
'Dim MISF2()       As Double
'Dim Np            As Integer
Dim PicW          As Single
Dim PicH1         As Single
Dim PicH2         As Single
Dim MLeft         As Single
Dim MTop          As Single
Dim pX1           As Single
Dim pX2           As Single
Dim pY1           As Single
Dim pY2           As Single
Dim ScalaPROFILO  As Single
Dim ScalaERRORI   As Single
Dim OX            As Single
Dim OY            As Single
Dim DL            As Single
Dim k1            As Integer
Dim k2            As Integer
'Dim TextFont      As String
Dim MISF1max      As Double
Dim MISF1min      As Double
Dim MISF2max      As Double
Dim MISF2min      As Double
'Dim SCRIVI_CNC    As String
Dim COLORE        As Integer
'
On Error GoTo errCtrlGRAFICO_CRF
  '
  'LETTURA DEL NOME DEL FILE DI MISURA
  NomeControllo = GetInfo("CHK0", "FileMisura", Path_LAVORAZIONE_INI)
  'LETTURA DATI DEL CREATORE DAL FILE DI CONTROLLO
  Call LETTURA_FILECONTROLLO(NomeControllo)
  '
  'VISUALIZZAZIONE DEL RISULTATO OTTENUTO
  'LETTURA DEI VALORI DI SCALA
  CHK0.txtScX.Text = GetInfo("CHK0", "ScalaProfiloCreatori", Path_LAVORAZIONE_INI)
  CHK0.txtScY.Text = GetInfo("CHK0", "ScalaErroreCreatori", Path_LAVORAZIONE_INI)
  CHK0.lblScX.Visible = True
  CHK0.lblScY.Visible = True
  CHK0.txtScX.Visible = True
  CHK0.txtScY.Visible = True
  '
  'RIPOSIZIONO LA FINESTRA
  USCITA.ScaleTop = 0
  USCITA.ScaleLeft = 0
  '
  'MARGINI
  Dim MRight As Double
  If USCITA Is Printer Then
    MLeft = val(GetInfo("CHK0", "MLeft", Path_LAVORAZIONE_INI))
    MTop = val(GetInfo("CHK0", "MTop", Path_LAVORAZIONE_INI))
    MRight = MLeft
  Else
    MLeft = 0.4
    MTop = 0.4
    MRight = 0.4
  End If
  'DEFINIZIONE AREA DI STAMPA
  PicW = USCITA.ScaleWidth - (MLeft + MRight)
  PicH1 = PicW / 2
  PicH2 = 3.5
  '
  'RIQUADRI PER VISUALIZZAZIONI E STAMPE
  'Uscita.Line (MLeft, MTop)-(PicW + MLeft, PicH1 + MTop), , B
  'Uscita.Line (MLeft, MTop)-(PicW / 2 + MLeft, PicH1 + MTop), , B
  'Uscita.Line (MLeft, MTop + PicH1)-(PicW + MLeft, PicH1 + MTop + PicH2), , B
  '
  Dim ANGOLO As Single
  Dim NumeroRette As Integer
  Dim ftmp1 As Double
  '
  ANGOLO = 30
  NumeroRette = 30
  
  'LETTURA VALORI DELLE SCALE DALLE CASELLE DI TESTO
  ScalaPROFILO = val(CHK0.txtScX.Text)
  ScalaERRORI = val(CHK0.txtScY.Text)
  'SCALA PROFILO
  pX1 = MLeft + 1
  pY1 = MTop + 2
  stmp = frmt(ScalaPROFILO, 4) & ":1"
  USCITA.CurrentX = pX1 - USCITA.TextWidth(stmp) / 2
  USCITA.CurrentY = pY1 - 0.5 - 2 * USCITA.TextHeight(stmp)
  USCITA.Print stmp
  USCITA.Line (pX1 - 0.5, pY1 - 0.5)-(pX1 + 0.5, pY1 - 0.5), QBColor(9)
  'FRECCIA SUPERIORE
  ftmp1 = (ANGOLO / 180 * PG) / NumeroRette
  For i = -NumeroRette To NumeroRette
    pX2 = pX1 + 0.3 * Sin(i * ftmp1)
    pY2 = pY1 - 0.5 - 0.3 * Cos((ANGOLO / 180 * PG))
    USCITA.Line (pX1, pY1 - 0.5)-(pX2, pY2), QBColor(9)
  Next i
  'FRECCIA INFERIORE
  ftmp1 = (ANGOLO / 180 * PG) / NumeroRette
  For i = -NumeroRette To NumeroRette
    pX2 = pX1 + 0.3 * Sin(i * ftmp1)
    pY2 = pY1 + 0.5 + 0.3 * Cos((ANGOLO / 180 * PG))
    USCITA.Line (pX1, pY1 + 0.5)-(pX2, pY2), QBColor(9)
  Next i
  USCITA.Line (pX1 - 0.5, pY1 + 0.5)-(pX1 + 0.5, pY1 + 0.5), QBColor(9)
  stmp = frmt(10 / ScalaPROFILO, 4) & "mm"
  USCITA.CurrentX = pX1 - USCITA.TextWidth(stmp) / 2
  If USCITA.CurrentX < MLeft Then USCITA.CurrentX = MLeft
  USCITA.CurrentY = pY1 - USCITA.TextHeight(stmp) / 2
  USCITA.Print stmp
  'ORIGINE DEL RIFERIMENTO
  Dim xFIANCO As Integer
  Dim yFIANCO As Integer
  Dim iXXtmp  As Integer
  Dim iYYtmp  As Integer
  'INDICE XX PER IL RIFERIMENTO OX
    If (iXX >= Np) Then
      iXXtmp = Np: iXX = Np: xFIANCO = 1
  ElseIf (iXX >= 1) And (iXX < Np) Then
      iXXtmp = iXX: xFIANCO = 1
  ElseIf (iXX = 0) Then
      iXXtmp = 1: iXX = 1: xFIANCO = 1
  ElseIf (iXX > -Np) And (iXX <= -1) Then
      iXXtmp = Abs(iXX): xFIANCO = 2
  Else
      iXXtmp = Np: iXX = -Np: xFIANCO = 2
  End If
  'INDICE YY PER IL RIFERIMENTO OY
    If (iYY >= Np) Then
      iYYtmp = Np: iYY = Np: yFIANCO = 1
  ElseIf (iYY >= 1) And (iYY < Np) Then
      iYYtmp = iYY: yFIANCO = 1
  ElseIf (iYY = 0) Then
      iYYtmp = Np: iYY = Np: yFIANCO = 1
  ElseIf (iYY > -Np) And (iYY <= -1) Then
      iYYtmp = Abs(iYY): yFIANCO = 2
  Else
      iYYtmp = Np: iYY = -Np: yFIANCO = 2
  End If
  If xFIANCO = 1 Then
    OX = XX_F1(iXXtmp)
  Else
    OX = XX_F2(iXXtmp)
  End If
  If yFIANCO = 1 Then
    OY = YY_F1(iYYtmp)
  Else
    OY = YY_F2(iYYtmp)
  End If
  'VISUALIZZO OX E OY
  stmp = "(" & frmt(OX, 4) & ", " & frmt(OY, 4) & ")"
  USCITA.CurrentX = MLeft + PicW / 2 - USCITA.TextWidth(stmp) / 2
  USCITA.CurrentY = MTop + PicH1
  USCITA.Print stmp
  'AGGIORNO LE CASELLE
  CHK0.Text1(0).Text = Format$(iXX, "##0")
  CHK0.Text1(1).Text = Format$(iYY, "##0")
  'ASSE XX
  pX1 = MLeft + PicW
  pY1 = MTop + PicH1
  USCITA.Line (pX1 - PicW, pY1)-(pX1, pY1)
  'FRECCIA XX
  ftmp1 = (ANGOLO / 180 * PG) / NumeroRette
  For i = -NumeroRette To NumeroRette
    pX2 = pX1 - 0.3 * Cos((ANGOLO / 180 * PG))
    pY2 = pY1 + 0.3 * Sin(i * ftmp1)
    USCITA.Line (pX1, pY1)-(pX2, pY2)
  Next i
  'NOME DELL'ASSE XX
  stmp = "XX"
  USCITA.CurrentX = pX1 - USCITA.TextWidth(stmp)
  USCITA.CurrentY = pY1 - 0.3 - USCITA.TextHeight(stmp)
  USCITA.Print stmp
  'ASSE YY
  pX1 = MLeft + PicW / 2  'MLeft + PicW / 2 + ScalaPROFILO * (0 - OX) / 10
  pY1 = MTop
  USCITA.Line (pX1, pY1)-(pX1, pY1 + PicH1)
  'FRECCIA YY
  ftmp1 = (ANGOLO / 180 * PG) / NumeroRette
  For i = -NumeroRette To NumeroRette
    pX2 = pX1 + 0.3 * Sin(i * ftmp1)
    pY2 = pY1 + 0.3 * Cos((ANGOLO / 180 * PG))
    USCITA.Line (pX1, pY1)-(pX2, pY2)
  Next i
  'NOME DELL'ASSE YY
  stmp = "YY"
  USCITA.CurrentX = pX1 + 0.3
  USCITA.CurrentY = pY1
  USCITA.Print stmp
  
  'RISOLUZIONE CROCI
  DL = 0.1
  'PROFILO DEL FIANCO 1
  For i = 1 To Np - 1
    pX1 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F1(i) - OX) / 10
    pY1 = MTop + PicH1 - ScalaPROFILO * (YY_F1(i) - OY) / 10
    pX2 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F1(i + 1) - OX) / 10
    pY2 = MTop + PicH1 - ScalaPROFILO * (YY_F1(i + 1) - OY) / 10
    USCITA.Line (pX1, pY1)-(pX2, pY2)
  Next i
  'PUNTI DEL FIANCO 1
  For i = 1 To Np
    pX1 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F1(i) - DL / 2 * Sin(TT_F1(i) * PG / 180) - OX) / 10
    pY1 = MTop + PicH1 - ScalaPROFILO * (YY_F1(i) + DL / 2 * Cos(TT_F1(i) * PG / 180) - OY) / 10
    pX2 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F1(i) + DL / 2 * Sin(TT_F1(i) * PG / 180) - OX) / 10
    pY2 = MTop + PicH1 - ScalaPROFILO * (YY_F1(i) - DL / 2 * Cos(TT_F1(i) * PG / 180) - OY) / 10
    USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
    pX1 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F1(i) - DL / 2 * Cos(TT_F1(i) * PG / 180) - OX) / 10
    pY1 = MTop + PicH1 - ScalaPROFILO * (YY_F1(i) - DL / 2 * Sin(TT_F1(i) * PG / 180) - OY) / 10
    pX2 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F1(i) + DL / 2 * Cos(TT_F1(i) * PG / 180) - OX) / 10
    pY2 = MTop + PicH1 - ScalaPROFILO * (YY_F1(i) + DL / 2 * Sin(TT_F1(i) * PG / 180) - OY) / 10
    USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(9)
  Next i
  'ERRORI SUL FIANCO 1
  MISF1max = 0
  MISF1min = 0
  For i = 1 To Np
    If (MISF1(i) < 1000) Then
      pX1 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F1(i) + ScalaERRORI * MISF1(i) * Cos(TT_F1(i) * PG / 180) / ScalaPROFILO - OX) / 10
      pY1 = MTop + PicH1 - ScalaPROFILO * (YY_F1(i) + ScalaERRORI * MISF1(i) * Sin(TT_F1(i) * PG / 180) / ScalaPROFILO - OY) / 10
      pX2 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F1(i) - OX) / 10
      pY2 = MTop + PicH1 - ScalaPROFILO * (YY_F1(i) - OY) / 10
      USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
      If MISF1(i) >= MISF1max Then MISF1max = MISF1(i)
      If MISF1(i) <= MISF1min Then MISF1min = MISF1(i)
    End If
  Next i
  'CURVA DEGLI ERRORI SUL FIANCO 1
  If Np >= 2 Then
    k1 = 1
    Do
      'INDICE INIZIALE TRATTO
      While (MISF1(k1) = 1000) And (k1 < Np)
        k1 = k1 + 1
      Wend
      'INDICE FINALE TRATTO
      k2 = k1 + 1
      While (MISF1(k2) = 1000) And (k2 < Np)
        k2 = k2 + 1
      Wend
      'DISEGNO IL TRATTO
      If (k2 > k1) And (MISF1(k1) <> 1000) And (MISF1(k2) <> 1000) Then
        pX1 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F1(k1) + ScalaERRORI * MISF1(k1) * Cos(TT_F1(k1) * PG / 180) / ScalaPROFILO - OX) / 10
        pY1 = MTop + PicH1 - ScalaPROFILO * (YY_F1(k1) + ScalaERRORI * MISF1(k1) * Sin(TT_F1(k1) * PG / 180) / ScalaPROFILO - OY) / 10
        pX2 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F1(k2) + ScalaERRORI * MISF1(k2) * Cos(TT_F1(k2) * PG / 180) / ScalaPROFILO - OX) / 10
        pY2 = MTop + PicH1 - ScalaPROFILO * (YY_F1(k2) + ScalaERRORI * MISF1(k2) * Sin(TT_F1(k2) * PG / 180) / ScalaPROFILO - OY) / 10
        USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
      End If
      'AGGIORNO PER IL PROSSIMO TRATTO
      k1 = k2
    Loop While (k2 < Np)
  End If
  '
  'NOME DEL FIANCO 1
  i = Np
  stmp = " F1 "
  USCITA.CurrentX = MLeft + PicW / 2 + ScalaPROFILO * (XX_F1(i) - OX) / 10
  USCITA.CurrentY = MTop + PicH1 - ScalaPROFILO * (YY_F1(i) - OY) / 10
  USCITA.Print stmp
  '
  'PROFILO DEL FIANCO 2
  For i = 1 To Np - 1
    pX1 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F2(i) - OX) / 10
    pY1 = MTop + PicH1 - ScalaPROFILO * (YY_F2(i) - OY) / 10
    pX2 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F2(i + 1) - OX) / 10
    pY2 = MTop + PicH1 - ScalaPROFILO * (YY_F2(i + 1) - OY) / 10
    USCITA.Line (pX1, pY1)-(pX2, pY2)
  Next i
  '
  'PUNTI DEL FIANCO 2
  For i = 1 To Np
    pX1 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F2(i) + DL / 2 * Sin(TT_F2(i) * PG / 180) - OX) / 10
    pY1 = MTop + PicH1 - ScalaPROFILO * (YY_F2(i) + DL / 2 * Cos(TT_F2(i) * PG / 180) - OY) / 10
    pX2 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F2(i) - DL / 2 * Sin(TT_F2(i) * PG / 180) - OX) / 10
    pY2 = MTop + PicH1 - ScalaPROFILO * (YY_F2(i) - DL / 2 * Cos(TT_F2(i) * PG / 180) - OY) / 10
    USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
    pX1 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F2(i) - DL / 2 * Cos(TT_F2(i) * PG / 180) - OX) / 10
    pY1 = MTop + PicH1 - ScalaPROFILO * (YY_F2(i) + DL / 2 * Sin(TT_F2(i) * PG / 180) - OY) / 10
    pX2 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F2(i) + DL / 2 * Cos(TT_F2(i) * PG / 180) - OX) / 10
    pY2 = MTop + PicH1 - ScalaPROFILO * (YY_F2(i) - DL / 2 * Sin(TT_F2(i) * PG / 180) - OY) / 10
    USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(9)
  Next i
  '
  'ERRORI SUL FIANCO 2
  MISF2max = 0
  MISF2min = 0
  For i = 1 To Np
    If (MISF2(i) < 1000) Then
      pX1 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F2(i) - ScalaERRORI * MISF2(i) * Cos(TT_F2(i) * PG / 180) / ScalaPROFILO - OX) / 10
      pY1 = MTop + PicH1 - ScalaPROFILO * (YY_F2(i) + ScalaERRORI * MISF2(i) * Sin(TT_F2(i) * PG / 180) / ScalaPROFILO - OY) / 10
      pX2 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F2(i) - OX) / 10
      pY2 = MTop + PicH1 - ScalaPROFILO * (YY_F2(i) - OY) / 10
      USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
      If MISF2(i) >= MISF2max Then MISF2max = MISF2(i)
      If MISF2(i) <= MISF2min Then MISF2min = MISF2(i)
    End If
  Next i
  '
  'CURVA DEGLI ERRORI SUL FIANCO 2
  If Np >= 2 Then
    k1 = 1
    Do
      'INDICE INIZIALE TRATTO
      While (MISF2(k1) = 1000) And (k1 < Np)
        k1 = k1 + 1
      Wend
      'INDICE FINALE TRATTO
      k2 = k1 + 1
      While (MISF2(k2) = 1000) And (k2 < Np)
        k2 = k2 + 1
      Wend
      'DISEGNO IL TRATTO
      If (k2 > k1) And (MISF2(k1) <> 1000) And (MISF2(k2) <> 1000) Then
        pX1 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F2(k1) - ScalaERRORI * MISF2(k1) * Cos(TT_F2(k1) * PG / 180) / ScalaPROFILO - OX) / 10
        pY1 = MTop + PicH1 - ScalaPROFILO * (YY_F2(k1) + ScalaERRORI * MISF2(k1) * Sin(TT_F2(k1) * PG / 180) / ScalaPROFILO - OY) / 10
        pX2 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F2(k2) - ScalaERRORI * MISF2(k2) * Cos(TT_F2(k2) * PG / 180) / ScalaPROFILO - OX) / 10
        pY2 = MTop + PicH1 - ScalaPROFILO * (YY_F2(k2) + ScalaERRORI * MISF2(k2) * Sin(TT_F2(k2) * PG / 180) / ScalaPROFILO - OY) / 10
        USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
      End If
      'AGGIORNO PER IL PROSSIMO TRATTO
      k1 = k2
    Loop While (k2 < Np)
  End If
  '
  'NOME DEL FIANCO 2
  i = Np
  stmp = " F2 "
  USCITA.CurrentX = MLeft + PicW / 2 + ScalaPROFILO * (XX_F2(i) - OX) / 10 - USCITA.TextWidth(stmp)
  USCITA.CurrentY = MTop + PicH1 - ScalaPROFILO * (YY_F2(i) - OY) / 10
  USCITA.Print stmp
  

'  Dim Areg As Double, Xreg As Double, Yreg As Double
'  Dim minYreg, maxYreg
'  Dim bgnFIANCO As Integer: bgnFIANCO = 1
'  Dim endFIANCO As Integer: endFIANCO = 21
'  'Xreg = XX_F2(9): Yreg = YY_F2(9)
  Call CtrlCALCOLOCORREZIONI_CRF  '(Areg, Xreg, Yreg, minYreg, maxYreg)
  '
'  pX1 = MLeft + PicW / 2 + ScalaPROFILO * (Xreg - (Yreg - minYreg) * Tan(Areg) - OX) / 10
'  pY1 = MTop + PicH1 - ScalaPROFILO * (minYreg - OY) / 10
'  pX2 = MLeft + PicW / 2 + ScalaPROFILO * (Xreg - (Yreg - maxYreg) * Tan(Areg) - OX) / 10
'  pY2 = MTop + PicH1 - ScalaPROFILO * (maxYreg - OY) / 10
'  Uscita.Line (pX1, pY1)-(pX2, pY2), vbBlue
  
  
  'SCRITTURA MESSAGGI IN ALTO A DX
  Dim Stringa$()
  Dim Nrighe As Integer: Nrighe = 3
  ReDim Stringa$(Nrighe)
  Stringa$(1) = "SPE meas:  " & frmt(GEN(17), 3) & " mm"
  Stringa$(2) = "SPE err:  " & frmt(GEN(16), 3) & " mm"
'  If CtrlDiam = 1 Then  'controllo diametro su Desterno
    Stringa$(3) = "Dest meas:  " & frmt(GEN(15), 3) & " mm"
'  ElseIf CtrlDiam = -1 Then 'controllo diametro su Dinterno
'    stringa$(3) = "Dint meas:  " & frmt(GEN(15), 3) & " mm"
'  Else 'CtrlDiam = 0  'nessun controllo
'    Nrighe = Nrighe - 1
'  End If
  '
  'VISUALIZZAZIONE CALCOLI
  For i = 1 To Nrighe
    USCITA.CurrentX = MLeft + PicW - USCITA.TextWidth(Stringa$(i))
    USCITA.CurrentY = MTop + USCITA.TextHeight(Stringa$(i)) * (i - 1)
    USCITA.Print Stringa$(i)
  Next i
  
  If USCITA Is Printer Then
    'IMMAGINE BITMAP
    Dim LogoL As Double, LogoH As Double
    LogoL = 2: LogoH = LogoL
    Printer.PaintPicture CHK0.PicLogo.Picture, MLeft, MTop + PicH1 + PicH2 - LogoH / 2, LogoL, LogoH
    'INTESTAZIONE
    stmp = CHK0.INTESTAZIONE.Caption & ":  " & NomeControllo
    Printer.CurrentX = MLeft + (PicW - Printer.TextWidth(stmp)) / 2
    Printer.CurrentY = MTop + PicH1 + PicH2 - Printer.TextHeight(stmp) / 2
    Printer.Print stmp
    'DATA
    stmp = "DATE:  " & FileDateTime(PathMIS & "\" & NomeControllo)
    Printer.CurrentX = MLeft + PicW - Printer.TextWidth(stmp)
    Printer.CurrentY = MTop + PicH1 + PicH2 - Printer.TextHeight(stmp) / 2
    Printer.Print stmp
    '
    Printer.EndDoc
  End If
Exit Sub

errCtrlGRAFICO_CRF:
  WRITE_DIALOG "SUB: CtrlGRAFICO_CRF " & str(Err)
  Resume Next
  Exit Sub

End Sub

Sub CtrlPUNTI_CRF()
'
On Error GoTo errCtrlPUNTI_CRF
'
If CHK0.txtHELP.Visible = True Then
  '
  Dim Tab1 As Integer: Tab1 = 4
  '
  Dim NomeFileTXT As String
  NomeFileTXT = g_chOemPATH & "\PUNTI.TXT"
  Open NomeFileTXT For Output As #1
    Print #1, String(20, "-") & " F1 " & String(20, "-")
    'FIANCO 1
    Print #1, Left$("nr." & String(Tab1, " "), Tab1);
    Print #1, Left$("XX " & String(8, " "), 8);
    Print #1, Left$("YY" & String(8, " "), 8);
    Print #1, Left$("TT" & String(8, " "), 8);
    Print #1, Left$("MISF1" & String(8, " "), 8)
    For i = 1 To Np
      If MISF1(i) <> 1000 Then
        Print #1, Left$(Format$(i, "###0") & String(Tab1, " "), Tab1);
        Print #1, Left$(frmt(XX_F1(i), 4) & String(8, " "), 8);
        Print #1, Left$(frmt(YY_F1(i), 4) & String(8, " "), 8);
        Print #1, Left$(frmt(TT_F1(i), 4) & String(8, " "), 8);
        Print #1, Left$(frmt(MISF1(i), 3) & String(8, " "), 8)
      End If
    Next i
    Print #1, String(20, "-") & " F2 " & String(20, "-")
    'FIANCO 2
    Print #1, Left$("nr." & String(Tab1, " "), Tab1);
    Print #1, Left$("XX " & String(8, " "), 8);
    Print #1, Left$("YY" & String(8, " "), 8);
    Print #1, Left$("TT" & String(8, " "), 8);
    Print #1, Left$("MISF2" & String(8, " "), 8)
    For i = 1 To Np
      If MISF2(i) <> 1000 Then
        Print #1, Left$(Format$(i, "###0") & String(Tab1, " "), Tab1);
        Print #1, Left$(frmt(XX_F2(i), 4) & String(8, " "), 8);
        Print #1, Left$(frmt(YY_F2(i), 4) & String(8, " "), 8);
        Print #1, Left$(frmt(TT_F2(i), 4) & String(8, " "), 8);
        Print #1, Left$(frmt(MISF2(i), 3) & String(8, " "), 8)
      End If
    Next i
  Close #1
  Open NomeFileTXT For Input As #1
    CHK0.txtHELP.Text = Input$(LOF(1), 1)
  Close #1
  '
  'SEGNALAZIONE UTENTE
  WRITE_DIALOG CHK0.Combo1.List(CHK0.Combo1.ListIndex)
  '
End If
Exit Sub

errCtrlPUNTI_CRF:
  WRITE_DIALOG "SUB: CtrlPUNTI_CRF " & str(Err)
  Exit Sub

End Sub

Sub CtrlRESULT_CRF()

If CHK0.txtHELP.Visible = True Then
  '
  Dim Tab1 As Integer: Tab1 = 6
  '
  Dim NomeFileTXT As String
  NomeFileTXT = g_chOemPATH & "\ERRORI.TXT"
  Open NomeFileTXT For Output As #1
    'INTESTAZIONE
    Print #1, Left$("***  " & String(Tab1, " "), Tab1);
    Print #1, Left$("theo " & String(8, " "), 8);
    Print #1, Left$("meas " & String(8, " "), 8);
    Print #1, Left$("err " & String(8, " "), 8)
    'Print #1, String(56, "-")
    'SPESSORE
    Print #1, Left$("SPE" & String(Tab1, " "), Tab1);
    Print #1, Left$(frmt(GEN(17) - GEN(16), 3) & String(8, " "), 8);
    Print #1, Left$(frmt(GEN(17), 3) & String(8, " "), 8);
    Print #1, Left$(frmt(GEN(16), 3) & String(8, " "), 8)
    'DIAMETRO ESTERNO
    Print #1, Left$("DEX" & String(Tab1, " "), Tab1);
    'Print #1, Left$(String(8, " "), 8);
    Print #1, Left$(frmt(2 * YY_F1(1), 3) & String(8, " "), 8);
    Print #1, Left$(frmt(GEN(15), 3) & String(8, " "), 8);
     Print #1, Left$(frmt(GEN(15) - 2 * YY_F1(1), 3) & String(8, " "), 8)
     Print #1, String(56, "-")
  Close #1
  Open NomeFileTXT For Input As #1
    CHK0.txtHELP.Text = Input$(LOF(1), 1)
  Close #1
  'MsgBox "DA COMPLETARE"
  '
  'SEGNALAZIONE UTENTE
  WRITE_DIALOG CHK0.Combo1.List(CHK0.Combo1.ListIndex)
  '
End If
End Sub


Sub CONTROLLO_CRFORMA_MODIFICA_CORRETTORI_MOLA()
'
StopRegieEvents
MsgBox ("Sub: Not S380 ready")
ResumeRegieEvents

'Dim MODALITA            As String
'Dim TIPO_LAVORAZIONE    As String
'Dim sRESET              As String
'Dim iRESET              As Integer
'Dim CorrezioniCalcolate As String
'Dim rsp                 As Integer
'Dim DB                  As Database
'Dim TB                  As Recordset
'Dim Correttore          As String
'Dim stmp                As String
'Dim stmp1               As String
'Dim stmp2               As String
'Dim Atmp                As String * 255
'Dim sDIAMETRO_RULLO     As String
'Dim DIAMETRO_RULLO      As Double
''
'On Error GoTo errCONTROLLO_CRFORMA_MODIFICA_CORRETTORI_MOLA
'  '
'  'LEGGO LA MODALITA DI TRASFERIMENTO DATI
'  MODALITA = GetInfo("Configurazione", "MODALITA", PathFILEINI)
'  MODALITA = UCase$(Trim$(MODALITA))
'  If (MODALITA = "OFF-LINE") Then
'    iRESET = 1
'  Else
'    iRESET = CONTROLLO_STATO_RESET
'  End If
'  '
'  If (iRESET <> 1) Then
'    'AVVERTO CHE NON FACCIO NIENTE
'    Write_Dialog "Data cannot be modified: CNC is not on RESET state"
'    Exit Sub
'  End If
'  '
'  'LEGGO IL TIPO DI LAVORAZIONE
'  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
'  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
'  '
'  Select Case TIPO_LAVORAZIONE
'
'    Case "CREATORI_FORMA"   'FlagFORMA 016
'      '
'      'SEGNALAZIONE DI CORRETTORI CALCOLATI DAL PROGRAMMA DI CONTROLLO
'      CorrezioniCalcolate = GetInfo(TIPO_LAVORAZIONE, "CorrezioniCalcolate", Path_LAVORAZIONE_INI)
'      '
'      If (CorrezioniCalcolate = "Y") Then
'        'LE CORREZIONI SONO GIA' CALCOLATE E DISPONIBILI IN GAPP4.INI
'        '
'        'RICHIESTA CONFERMA ALL'UTENTE
'        StopRegieEvents
'        'rsp = 0
'        'rsp = LoadString(g_hLanguageLibHandle, rsp, Atmp, 255)
'        'If (rsp > 0) Then stmp = Left$(Atmp, rsp)
'        stmp = "Do you want to add corrections evaluated during the last check?"
'        rsp = MsgBox(stmp, 32 + 4 + 256, "WARNING")
'        ResumeRegieEvents
'        '
'        If (rsp = 6) Then
'
'        'PULISCO LA LISTA DEI CORRETTORI
'        OEM0.List1(1).Clear
'        '
'        'APRO IL DATABASE
'        Set DB = OpenDatabase(PathDB, True, False)
'        Set TB = DB.OpenRecordset("OEM0_CORRETTORI15")
'        '
'        'AUTOCORREZIONE
'        '
'        i = 1
'        'RICERCO NELLA TABELLA
'        TB.Index = "INDICE": TB.Seek "=", i
'        While TB.Fields("ABILITATO").Value = "Y"
'          '
'          'LEGGO LA VARIABILE
'          stmp1 = TB.Fields("VARIABILE_1").Value
'          '
'          'LEGGO LA CORREZIONE ATTUALE
'          stmp = TB.Fields("ACTUAL_1").Value
'          stmp = Trim$(stmp)
'          '
'          'LEGGO LA MODIFICA
'          Correttore = GetInfo(TIPO_LAVORAZIONE, "Correttore(" & Format$(i, "#0") & ")", Path_LAVORAZIONE_INI)
'          '
'          'AGGIORNO IL VALORE DELLA CORREZIONE
'          stmp = frmt(val(stmp) + val(Correttore), 6)
'          '
'          'AGGIORNO LA TABELLA SOMMANDO IL VALORE DELLE CORREZIONI
'          TB.Edit
'          TB.Fields("ACTUAL_1").Value = stmp
'          TB.Update
'          '
'          'AGGIORNO LA LISTA DEI PARAMETRI
'          CALL OEM0.List1(1).AddItem (stmp)
'          '
'          'SCRIVO NEI CORRETTORI
'          rsp = WritePrivateProfileString("OEM0_CORRETTORI15", stmp1, stmp, PathFileSPF)
'          '
'          'SCRIVO NELLA SEZIONE PARAMETRI
'          rsp = WritePrivateProfileString("OEM0_CORRETTORI", stmp1, stmp, g_chOemPATH & "\" & TIPO_LAVORAZIONE & ".INI")
'          '
'          i = i + 1
'          TB.Seek "=", i
'          '
'        Wend
'        '
'        'CHIUDO IL DATABASE
'        TB.Close
'        DB.Close
'        '
'        OEM0.List1(1).Refresh
'        OEM0.List1(1).ListIndex = 0
'        '
'        If (MODALITA = "SPF") Then Call INVIO_SOTTOPROGRAMMA("OEM0_CORRETTORI15")
'        '
'        rsp = WritePrivateProfileString(TIPO_LAVORAZIONE, "CorrezioniCalcolate", "N", Path_LAVORAZIONE_INI)
'        '
'        End If
'        '
'      Else
'        '
'        'COMPENSAZIONE DEL DIAMETRO RULLO CON L'ERRORE DI SPESSORE
'        stmp = GetInfo("CREATORI_FORMA", "COMPENSAZIONE_DIAMETRO_RULLO", PathFILEINI)
'        stmp = UCase$(Trim$(stmp))
'        '
'        If (stmp = "Y") Then
'          '
'          If (val(OEM0.List1(1).List(0)) <> 0) Then
'            '
'            'RICHIESTA CONFERMA ALL'UTENTE
'            StopRegieEvents
'            rsp = val(GetInfo("SRV0_CREATORI_FORMA", "AzioneSTR(0)", PathFILEINI))
'            rsp = LoadString(g_hLanguageLibHandle, rsp, Atmp, 255)
'            If (rsp > 0) Then sDIAMETRO_RULLO = Left$(Atmp, rsp)
'            stmp = OEM0.List1(0).List(0) & " --> " & sDIAMETRO_RULLO
'            rsp = MsgBox(stmp, 32 + 4 + 256, "WARNING")
'            ResumeRegieEvents
'            '
'            If (rsp = 6) Then
'              '
'              'AGGIORNAMENTO DEL DIAMETRO RULLO
'              stmp2 = GetInfo("SRV0_CREATORI_FORMA", "AzioneDDE(0)", PathFILEINI)
'              DIAMETRO_RULLO = val(OPC_LEGGI_DATO(stmp2))
'              DIAMETRO_RULLO = -val(OEM0.List1(1).List(0)) + DIAMETRO_RULLO
'              stmp = frmt(DIAMETRO_RULLO, 8)
'              Call OPC_SCRIVI_DATO(stmp2, stmp)
'              '
'              'AZZERAMENTO DEL VALORE DI CORREZIONE DI SPESSORE
'              OEM0.List1(1).Clear
'              '
'              'APRO IL DATABASE
'              Set DB = OpenDatabase(PathDB, True, False)
'              Set TB = DB.OpenRecordset("OEM0_CORRETTORI15")
'              '
'              'AUTOCORREZIONE
'              '
'              i = 1
'              'RICERCO NELLA TABELLA
'              TB.Index = "INDICE": TB.Seek "=", i
'              While TB.Fields("ABILITATO").Value = "Y"
'                '
'                'LEGGO LA VARIABILE
'                stmp1 = TB.Fields("VARIABILE_1").Value
'                '
'                'LEGGO LA CORREZIONE ATTUALE
'                stmp = TB.Fields("ACTUAL_1").Value
'                stmp = Trim$(stmp)
'                '
'                'AZZERO LA CORREZIONE DI SPESSORE
'                If (i = 1) Then stmp = "0"
'                '
'                'AGGIORNO LA TABELLA
'                TB.Edit
'                TB.Fields("ACTUAL_1").Value = stmp
'                TB.Update
'                '
'                'AGGIORNO LA LISTA DEI PARAMETRI
'                CALL OEM0.List1(1).AddItem (stmp)
'                '
'                'SCRIVO NEI CORRETTORI
'                rsp = WritePrivateProfileString("OEM0_CORRETTORI15", stmp1, stmp, PathFileSPF)
'                '
'                'SCRIVO NELLA SEZIONE PARAMETRI
'                rsp = WritePrivateProfileString("OEM0_CORRETTORI", stmp1, stmp, g_chOemPATH & "\" & TIPO_LAVORAZIONE & ".INI")
'                '
'                i = i + 1
'                TB.Seek "=", i
'                '
'              Wend
'              '
'              'CHIUDO IL DATABASE
'              TB.Close
'              DB.Close
'              '
'              OEM0.List1(1).Refresh
'              OEM0.List1(1).ListIndex = 0
'              '
'              If (MODALITA = "SPF") Then Call INVIO_SOTTOPROGRAMMA("OEM0_CORRETTORI15")
'              '
'              'SEGNALAZIONE UTENTE
'              Write_Dialog sDIAMETRO_RULLO & ": " & frmt(DIAMETRO_RULLO, 8) & " (Updated)"
'              '
'            Else
'              '
'              'SEGNALAZIONE UTENTE
'              Write_Dialog "Operation aborted by user."
'              '
'            End If
'            '
'          Else
'            '
'            'AVVERTO CHE NON FACCIO NIENTE
'            Write_Dialog "SOFTKEY DISENABLED!!"
'            '
'          End If
'          '
'        End If
'        '
'      End If
'
'  End Select
'  '
'  'CHIUDO I FILE EVENTUALMENTE APERTI
'  If (FreeFile > 1) Then Close
'  '
'Exit Sub
'
'errCONTROLLO_CRFORMA_MODIFICA_CORRETTORI_MOLA:
'  Write_Dialog "SUB: CONTROLLO_CRFORMA_MODIFICA_CORRETTORI_MOLA " & Str(Err)
'  Resume Next

End Sub

Sub CtrlCALCOLOCORREZIONI_CRF() '(ANGOLO, Xreg, Yreg, minYreg, maxYreg)
'**********************************************
'APPROSSIMAZIONE:
'calcolo gli errori sul profilo assiale del CR
'e li applico cos� come sono alla mola
'**********************************************
Dim NpFIANCO  As Integer
Dim NpSEZIONE As Integer
Dim B         As Double
Dim teoX()    As Double 'X punto teorico
Dim teoY()    As Double 'Y punto teorico
Dim misX()    As Double 'X punto misurato
Dim misY()    As Double 'Y punto misurato
'
Dim i       As Integer
Dim j       As Integer
Dim Fianco  As Integer
'
'FIANCO
Dim MAXerrMISF(2) As Double
Dim MINerrMISF(2) As Double
Dim HrTeoF(2)     As Double '--- ALTEZZA FIANCO teorica
Dim HrMisF(2)     As Double '--- ALTEZZA FIANCO misurata
Dim errHrF(2)     As Double '--- ERRORE ALTEZZA FIANCO
Dim erroreF(2)    As Double '--- ERRORE ANGOLO FIANCO (gradi)
Dim errAngoloF(2) As Double '--- ERRORE ANGOLO FIANCO (mm)
Dim errSpostF(2)  As Double
Dim errFormaF(2)  As Double
Dim AngoloTeoF(2) As Double
Dim AngoloMisF(2) As Double
Dim pntListaF(2)  As String

Dim XXcrf() As Double, YYcrf() As Double
Dim TTcrf() As Double, MIScrf() As Double

On Error GoTo errCtrlCALCOLOCORREZIONI_CRF

'VALUTAZIONI SUI FIANCHI
Dim bgnFIANCO As Integer: bgnFIANCO = 1
Dim endFIANCO As Integer: endFIANCO = GEN(11)  'Np
  
ReDim XXcrf(endFIANCO), YYcrf(endFIANCO)
ReDim TTcrf(endFIANCO), MIScrf(endFIANCO)
  
For Fianco = 1 To 2

  'CALCOLO DEI PUNTI DEL FIANCO 1
  NpFIANCO = 0
  'CONSIDERO TUTTI I PUNTI DEL FIANCO 1
  For i = bgnFIANCO To endFIANCO
    If Fianco = 1 Then
      XXcrf(i) = XX_F1(i): YYcrf(i) = YY_F1(i)
      TTcrf(i) = TT_F1(i): MIScrf(i) = MISF1(i)
    End If
    If Fianco = 2 Then
      XXcrf(i) = XX_F2(i): YYcrf(i) = YY_F2(i)
      TTcrf(i) = TT_F2(i): MIScrf(i) = MISF2(i)
    End If
    If MIScrf(i) <> 1000 Then
      'SALVO GLI INDICI DEI PUNTI
      pntListaF(Fianco) = pntListaF(Fianco) & Format$(i, "##0") & ";"
      'INCREMENTO SOLO PER I PUNTI CONTROLLATI
      NpFIANCO = NpFIANCO + 1
      'ALLOCO LO SPAZIO
      ReDim Preserve teoX(NpFIANCO): ReDim Preserve teoY(NpFIANCO)
      ReDim Preserve misX(NpFIANCO): ReDim Preserve misY(NpFIANCO)
      'CALCOLO LE COORDINATE DEL PUNTO TEORICO
      teoX(NpFIANCO) = XXcrf(i)
      teoY(NpFIANCO) = YYcrf(i)
      'CALCOLO LE COORDINATE DEL PUNTO MISURATO
      misX(NpFIANCO) = XXcrf(i) + (3 - 2 * Fianco) * MIScrf(i) * Cos(TTcrf(i) * PG / 180)
      misY(NpFIANCO) = YYcrf(i) + MIScrf(i) * Sin(TTcrf(i) * PG / 180)
      'RICERCA DEL MASSIMO E MINIMO
      If MIScrf(i) >= MAXerrMISF(Fianco) Then MAXerrMISF(Fianco) = MIScrf(i)
      If MIScrf(i) <= MINerrMISF(Fianco) Then MINerrMISF(Fianco) = MIScrf(i)
    End If
  Next i
  pntListaF(Fianco) = Left$(pntListaF(Fianco), Len(pntListaF(Fianco)) - 1)
  'CALCOLO ALTEZZA FIANCO teorica
  HrTeoF(Fianco) = teoY(bgnFIANCO) - teoY(NpFIANCO)
  'CALCOLO ALTEZZA FIANCO misurata
  HrMisF(Fianco) = misY(bgnFIANCO) - misY(NpFIANCO)
  'ERRORE ALTEZZA
  errHrF(Fianco) = HrMisF(Fianco) - HrTeoF(Fianco)
  'ERRORE SPOSTAMENTO
  errSpostF(Fianco) = misY(bgnFIANCO) - errHrF(Fianco) - teoY(bgnFIANCO)
  '
  If NpFIANCO >= 2 Then
    NpSEZIONE = NpFIANCO
    'CALCOLO ANGOLO TEORICO IN RADIANTI
    Call CALCOLO_COEFFICIENTE_ANGOLARE_b(NpSEZIONE, teoX(), teoY(), B)
    AngoloTeoF(Fianco) = (2 * Fianco - 3) * Atn(B)
    'ADEGUO MISURA CON CORREZIONE ALTEZZA
    For i = 1 To NpSEZIONE
        misY(i) = misY(i) * (1 + errHrF(Fianco) / teoY(1)) - errHrF(Fianco) * misY(1) / teoY(1)
    Next i
    'CALCOLO ANGOLO MISURATO IN RADIANTI
    Call CALCOLO_COEFFICIENTE_ANGOLARE_b(NpSEZIONE, misX, misY, B)
    AngoloMisF(Fianco) = (2 * Fianco - 3) * Atn(B)
  Else
    'CALCOLO ANGOLO TEORICO IN RADIANTI
    AngoloTeoF(Fianco) = TTcrf(bgnFIANCO) / 180 * PG   'TT_F1(bgnFIANCO + 1) / 180 * PG
    'CALCOLO ANGOLO MISURATO IN RADIANTI
    AngoloMisF(Fianco) = TTcrf(bgnFIANCO) / 180 * PG
  End If
  erroreF(Fianco) = (AngoloMisF(Fianco) - AngoloTeoF(Fianco))
  errAngoloF(Fianco) = HrTeoF(Fianco) * Tan(erroreF(Fianco))  '(2 * Fianco - 3) *
  'CALCOLO ERRORE DI FORMA DEL FIANCO
  errFormaF(Fianco) = 0
  For i = 1 To NpFIANCO
    For j = 1 To NpFIANCO
      errFormaF(0) = (3 - 2 * Fianco) * (misX(j) - misX(i)) * Cos(AngoloTeoF(Fianco)) _
                    + (misY(j) - misY(i)) * Sin(AngoloTeoF(Fianco))
      errFormaF(0) = Abs(errFormaF(0))
      If errFormaF(0) > errFormaF(Fianco) Then errFormaF(Fianco) = errFormaF(0)
    Next j
  Next i
  
Next Fianco

'R68=errHrF(1) :R69=errHrF(2)
'


'ANGOLO = AngoloMisF(2)
'Xreg = 0: Yreg = 0
'minYreg = 999999: maxYreg = -999999
'For i = 1 To NpSEZIONE
'  Xreg = Xreg + misX(i) / NpSEZIONE
'  Yreg = Yreg + misY(i) / NpSEZIONE
'  If misY(i) < minYreg Then minYreg = misY(i)
'  If misY(i) > maxYreg Then maxYreg = misY(i)
'Next i


  'SEGNALAZIONE UTENTE
  WRITE_DIALOG CHK0.Combo1.List(CHK0.Combo1.ListIndex)
  '
  'SALVATAGGIO CORREZIONI
  '
  Dim TIPOLAV As String: TIPOLAV = "CREATORI_FORMA"
  'CORR. DI SPESSORE DENTE
  i = WritePrivateProfileString(TIPOLAV, "Correttore(1)", frmt(-GEN(16), 3), PathFILEINI)
  'CORR. ALTEZZA F1  (R68)
  i = WritePrivateProfileString(TIPOLAV, "Correttore(2)", frmt(-errHrF(1), 3), PathFILEINI)
  'CORR. ALTEZZA F2  (R69)
  i = WritePrivateProfileString(TIPOLAV, "Correttore(3)", frmt(-errHrF(2), 3), PathFILEINI)
  'CORR. ANGOLO F1  (R78)
  i = WritePrivateProfileString(TIPOLAV, "Correttore(4)", frmt(-errAngoloF(1), 3), PathFILEINI)
  'CORR. ANGOLO F2  (R79)
  i = WritePrivateProfileString(TIPOLAV, "Correttore(5)", frmt(-errAngoloF(2), 3), PathFILEINI)
  'CORR. SPOSTAMENTO F1  (R88)
  i = WritePrivateProfileString(TIPOLAV, "Correttore(6)", frmt(errSpostF(1), 3), PathFILEINI)
  'CORR. SPOSTAMENTO F2  (R99)
  i = WritePrivateProfileString(TIPOLAV, "Correttore(7)", frmt(errSpostF(2), 3), PathFILEINI)
  '
  i = WritePrivateProfileString(TIPOLAV, "Correttore(8)", "0", PathFILEINI)
  i = WritePrivateProfileString(TIPOLAV, "Correttore(9)", "0", PathFILEINI)
  i = WritePrivateProfileString(TIPOLAV, "Correttore(10)", "0", PathFILEINI)
  i = WritePrivateProfileString(TIPOLAV, "Correttore(11)", "0", PathFILEINI)
  i = WritePrivateProfileString(TIPOLAV, "Correttore(12)", "0", PathFILEINI)
  i = WritePrivateProfileString(TIPOLAV, "Correttore(13)", "0", PathFILEINI)
  i = WritePrivateProfileString(TIPOLAV, "Correttore(14)", "0", PathFILEINI)
  i = WritePrivateProfileString(TIPOLAV, "Correttore(15)", "0", PathFILEINI)
  '
  i = WritePrivateProfileString(TIPOLAV, "CorrezioniCalcolate", "Y", PathFILEINI)
  '
'  'SCRITTURA MESSAGGI PER LA STAMPA
'  i = WritePrivateProfileString("CHK0", "stmp(1)", "SPE meas: " & frmt(GEN(17), 3) & "mm", Path_LAVORAZIONE_INI)
'  i = WritePrivateProfileString("CHK0", "stmp(2)", "SPE  err: " & frmt(GEN(16), 3) & "mm", Path_LAVORAZIONE_INI)
'  i = WritePrivateProfileString("CHK0", "stmp(3)", "DEX meas: " & frmt(GEN(15), 3) & "mm", Path_LAVORAZIONE_INI)
'  i = WritePrivateProfileString("CHK0", "stmp(4)", "fha F1: " & frmt(erroreF(1) * AltezzaF(1), 3) & "mm", Path_LAVORAZIONE_INI)
'  i = WritePrivateProfileString("CHK0", "stmp(5)", "fha F2: " & frmt(erroreF(2) * AltezzaF(2), 3) & "mm", Path_LAVORAZIONE_INI)
Exit Sub

errCtrlCALCOLOCORREZIONI_CRF:
  WRITE_DIALOG "SUB: CtrlCALCOLOCORREZIONI_CRF " & str(Err)
  Resume Next

End Sub

Sub CALCOLO_COEFFICIENTE_ANGOLARE_b(NpSEZIONE As Integer, _
        pntX() As Double, pntY() As Double, B As Double)
'
ReDim Preserve pntX(NpSEZIONE), pntY(NpSEZIONE)
'
Dim sXh   As Double
Dim sYh   As Double
Dim sXhXh As Double
Dim sXhYh As Double
  '
  Select Case NpSEZIONE
    '
    Case 0
      StopRegieEvents
      MsgBox "ERROR: ANGLE CANNOT BE EVALUATED WITHOUT A POINT.", 16, "SUB: CONTROLLO_CREATORI_VISUALIZZAZIONE"
      ResumeRegieEvents
      B = 0
      '
    Case 1
      B = 0
      '
    Case 2
      B = (pntX(2) - pntX(1)) / (pntY(2) - pntY(1))
      '
    Case Else
      '--------------------------------------------------------------------------
      'INPUT : NpFIANCO, pntX(.), pntY(.)
      'OUTPUT: b
      '--------------------------------------------------------------------------
      sXh = 0:   For i = 1 To NpSEZIONE: sXh = sXh + pntY(i):               Next i
      sYh = 0:   For i = 1 To NpSEZIONE: sYh = sYh + pntX(i):               Next i
      sXhXh = 0: For i = 1 To NpSEZIONE: sXhXh = sXhXh + pntY(i) * pntY(i): Next i
      sXhYh = 0: For i = 1 To NpSEZIONE: sXhYh = sXhYh + pntY(i) * pntX(i): Next i
      'Coefficiente angolare della retta di regressione
      B = (NpSEZIONE * sXhYh - sXh * sYh) / (NpSEZIONE * sXhXh - sXh * sXh)
      '--------------------------------------------------------------------------
      '
  End Select
  
End Sub

Sub RAG3P(ByVal X1 As Double, ByVal Y1 As Double, _
          ByVal X2 As Double, ByVal Y2 As Double, _
          ByVal X3 As Double, ByVal Y3 As Double, _
          ByRef Xc As Double, ByRef YC As Double, ByRef R0 As Double)
'  Calcolo raggio per tre punti
Dim R160 As Double, R171 As Double, R172 As Double, R173 As Double
If Y3 = Y2 Then Y3 = Y2 + 0.0001
If Y1 = Y2 Then Y1 = Y2 + 0.0001
R160 = (X1 - X2) / (Y2 - Y1)
R171 = (X2 - X3) / (Y3 - Y2)
R172 = (Y1 + Y2) / 2 - R160 * (X1 + X2) / 2
R173 = (Y3 + Y2) / 2 - R171 * (X3 + X2) / 2
If R160 = R171 Then R160 = R171 + 0.0001
'coordinate del centro
Xc = (R173 - R172) / (R160 - R171)
YC = R160 * Xc + R172
'raggio del cerchio
R0 = Sqr((Xc - X2) * (Xc - X2) + (YC - Y2) * (YC - Y2))
End Sub


Public Sub Cerchio2PuntiRaggio(X1, Y1, X2, Y2, R0, Xc, YC)
'segno di R0:
'   - per orario
'   + per antiorario
   Dim L1, L2, A1, X0, Y0 As Double
   Dim L2X, L2Y As Double
   
   PI = PG
   
   X0 = (X1 + X2) / 2
   Y0 = (Y1 + Y2) / 2
   L1 = Sqr((X1 - X0) ^ 2 + (Y1 - Y0) ^ 2)
   If X2 = X1 Then
        A1 = PI / 2
    Else
        A1 = Abs(Atn((Y2 - Y1) / (X2 - X1)))
   End If
   L2 = Sqr(R0 ^ 2 - L1 ^ 2)
'A1=0
   If A1 = 0 Then
    If X2 > X1 Then
        If R0 < 0 Then L2 = -L2
      Else
        If R0 > 0 Then L2 = -L2
    End If
    Xc = X0
    YC = Y0 + L2 * Cos(A1)
    End If
'A1=90�
   If A1 = PI / 2 Then
     If Y2 > Y1 Then
        If R0 > 0 Then L2 = -L2
      Else
        If R0 < 0 Then L2 = -L2
     End If
     Xc = X0 + L2 * Sin(A1)
     YC = Y0
   End If

If ((A1 > 0) And (A1 < PI / 2)) Then
   
   L2X = L2: L2Y = L2
   
   If (X1 < X2) And (Y1 > Y2) Then
    If R0 < 0 Then L2 = -L2
   End If
   
'   If ((X1 < X2) And (Y1 < Y2)) Then
'    If R0 > 0 Then L2X = -L2X ': L2Y = -L2Y
'   End If
   
   If (X1 > X2) And (Y1 < Y2) Then
    If R0 > 0 Then L2X = -L2X: L2Y = -L2Y
   End If
   
   If ((X1 < X2) And (Y1 < Y2)) Then
     If R0 > 0 Then L2X = -L2X Else L2Y = -L2Y
   End If
   
   If ((X1 > X2) And (Y1 > Y2)) Then
     If R0 < 0 Then L2X = -L2X Else L2Y = -L2Y
   End If

   Xc = X0 + L2X * Sin(A1)
   YC = Y0 + L2Y * Cos(A1)
   
   'verifica
   L1 = Sqr((Xc - X1) ^ 2 + (YC - Y1) ^ 2)
   L2 = Sqr((Xc - X2) ^ 2 + (YC - Y2) ^ 2)
   
   If (L1 - Abs(R0)) > 0.001 Then
          StopRegieEvents
          MsgBox "ERROR in verifica raggio: L1<>R0"
          ResumeRegieEvents
   End If
   If (L2 - Abs(R0)) > 0.001 Then
          StopRegieEvents
          MsgBox "ERROR in verifica raggio: L2<>R0"
          ResumeRegieEvents
   End If
End If

End Sub

Private Sub Cerchio_LineaPuntoRaggio(XT1, YT1, Xc1, Yc1, R1, T1, Xc, YC, Xt, Yt)
    'Noti: punto su cerchio (Xt1, Yt1)
    '       centro cerchio (Xc1, Yc1)
    '       raggio cerchio da calcolare: R1
    '       angolo retta tangente: T1
    'calcola: centro cerchio (Xc, Yc)
    '         punto tangente retta e cerchio (Xt, Yt)
    Dim ALFA
    ALFA = Atn((Xc1 - XT1) / (Yc1 - YT1))
    Xc = XT1 + R1 * Sin(ALFA)
    YC = YT1 + R1 * Cos(ALFA)
    Xt = Xc - R1 * Sin(T1)
    Yt = YC - R1 * Cos(T1)
    
End Sub
Private Sub Cerchio_PuntoCerchioRaggio(Xc1, Yc1, XP2, YP2, R1, R2, Xc2, Yc2, AA$)
' calcola XC2, YC2
' A$= "F1" o "F2"
   Dim ALFA, Beta, GAMMA
   Dim A, B, C
   
    A = R2
    B = Abs(R1 + R2)
    C = Sqr((XP2 - Xc1) ^ 2 + (YP2 - Yc1) ^ 2)
    If AA$ = "F1" Then
        Beta = Atn((XP2 - Xc1) / (YP2 - Yc1))
        If Beta > 0 Then
          ALFA = -Acs((B ^ 2 + C ^ 2 - A ^ 2) / 2 / B / C) 'Carnot
        Else
          ALFA = Acs((B ^ 2 + C ^ 2 - A ^ 2) / 2 / B / C) 'Carnot
        End If
     Else
        Beta = Atn((XP2 - Xc1) / (YP2 - Yc1))
        If Beta > 0 Then
          ALFA = Acs((B ^ 2 + C ^ 2 - A ^ 2) / 2 / B / C) 'Carnot
        Else
          ALFA = -Acs((B ^ 2 + C ^ 2 - A ^ 2) / 2 / B / C) 'Carnot
        End If
    End If
    'gamma = -Sgn(R1) * Sgn(beta) * (Abs(beta) - ALFA)
    
    If R1 > 0 Then
        GAMMA = -Sgn(Beta) * (Abs(Beta) - ALFA)
        Xc2 = Xc1 + (R1 + R2) * Sin(GAMMA)
        Yc2 = Yc1 - (R1 + R2) * Cos(GAMMA)
      Else
        GAMMA = -Sgn(Beta) * (Abs(Beta) + ALFA)
        Xc2 = Xc1 - Abs(R1 + R2) * Sin(GAMMA)
        Yc2 = Yc1 + Abs(R1 + R2) * Cos(GAMMA)
    End If

End Sub

Sub Cerchio_RettaCerchio(Xc1, Yc1, RC1, XL2, YL2, ApL2, RC0, XC0, YC0, XT1, YT1, XT2, YT2)
'INPUT: cerchio C1: centro = XC1, YC1
'       cerchio C1: raggio = RC1
'       linea L2: punto noto = XL2, YL2
'       linea L2: Ang.Press. = ApL2
'       raggio cerchio tangente = RC0
'OUTPUT: centro cerchio tangente = XC0, YC0
'        punto di tangenza su cerchio C1 = XT1, YT1
'        punto di tangenza su linea L2 = XT2, YT2
    
Dim AA As Double
Dim Aux As Double
Dim ApC1 As Double

If RC1 < 0 Then
  RC1 = Abs(RC1)
  If ApL2 < 0 Then
    '--- FIANCO 1 ---
    ApL2 = Abs(ApL2)
    AA = XL2 + (YL2 - Yc1) * Tan(ApL2) - Xc1 - RC0 / Cos(ApL2)
    Aux = Asn(AA / (RC1 - RC0) * Sin(PI / 2 - ApL2))
    ApC1 = PI / 2 + ApL2 - Aux
    
    XC0 = Xc1 + (RC1 - RC0) * Cos(ApC1)
    YC0 = Yc1 + (RC1 - RC0) * Sin(ApC1)
        
    XT1 = XC0 + RC0 * Cos(ApC1)
    YT1 = YC0 + RC0 * Sin(ApC1)
    
    XT2 = XC0 + RC0 * Cos(ApL2)
    YT2 = YC0 + RC0 * Sin(ApL2)
  Else
    '--- FIANCO 2 ---
    'APL2 = Abs(APL2)
    AA = Xc1 - (XL2 - (YL2 - Yc1) * Tan(ApL2)) - RC0 / Cos(ApL2)
    Aux = Asn(AA / (RC1 - RC0) * Sin(PI / 2 - ApL2))
    ApC1 = PI / 2 + ApL2 - Aux
    
    XC0 = Xc1 - (RC1 - RC0) * Cos(ApC1)
    YC0 = Yc1 + (RC1 - RC0) * Sin(ApC1)
        
    XT1 = XC0 - RC0 * Cos(ApC1)
    YT1 = YC0 + RC0 * Sin(ApC1)
    
    XT2 = XC0 - RC0 * Cos(ApL2)
    YT2 = YC0 + RC0 * Sin(ApL2)
  End If
  RC1 = -RC1
Else
  If ApL2 < 0 Then
    '--- FIANCO 1 ---
    ApL2 = Abs(ApL2)
    AA = Yc1 - (YL2 + (XL2 - Xc1) / Tan(ApL2)) + RC0 / Sin(ApL2)
    Aux = Asn(AA / (RC1 + RC0) * Sin(ApL2))
    ApC1 = ApL2 - Aux
    
    XC0 = Xc1 + (RC1 + RC0) * Sin(ApC1)
    YC0 = Yc1 - (RC1 + RC0) * Cos(ApC1)
        
    XT1 = Xc1 + RC1 * Sin(ApC1)
    YT1 = Yc1 - RC1 * Cos(ApC1)
    
    XT2 = XC0 + RC0 * Cos(ApL2)
    YT2 = YC0 + RC0 * Sin(ApL2)
  Else
    '--- FIANCO 2 ---
    AA = Yc1 - (YL2 + (Xc1 - XL2) / Tan(ApL2)) + RC0 / Sin(ApL2)
    Aux = Asn(AA / (RC1 + RC0) * Sin(ApL2))
    ApC1 = ApL2 - Aux
    
    XC0 = Xc1 - (RC1 + RC0) * Sin(ApC1)
    YC0 = Yc1 - (RC1 + RC0) * Cos(ApC1)
        
    XT1 = Xc1 - RC1 * Sin(ApC1)
    YT1 = Yc1 - RC1 * Cos(ApC1)
    
    XT2 = XC0 - RC0 * Cos(ApL2)
    YT2 = YC0 + RC0 * Sin(ApL2)
  End If
End If
End Sub

Private Function CALCOLO_ANGOLO(ByVal dx As Single, ByVal Dy As Single) As Single
Dim QUADRANTE As Integer
  
  PI = PG
  
  'Calcolo dell'angolo in radianti tra 0 e 2 * PG.
  If dx = 0 Then dx = 0.0001
  If dx >= 0 And Dy >= 0 Then QUADRANTE = 1
  If dx < 0 And Dy >= 0 Then QUADRANTE = 2
  If dx < 0 And Dy < 0 Then QUADRANTE = 3
  If dx >= 0 And Dy < 0 Then QUADRANTE = 4
  Select Case QUADRANTE
    Case 1
      CALCOLO_ANGOLO = Atn(Dy / dx) + 0 * PI
    Case 2
      CALCOLO_ANGOLO = Atn(Dy / dx) + 1 * PI
    Case 3
      CALCOLO_ANGOLO = Atn(Dy / dx) + 1 * PI
    Case 4
      CALCOLO_ANGOLO = Atn(Dy / dx) + 2 * PI
  End Select

End Function

Private Sub Rayon5pts(L1, L2, L3, L4, L5, H1, H2, H3, H4, H5, R0, Xr0, Yr0, Ecart)
   Dim L6, H6, L7, H7, L8, L9
   Dim A1, A2, A3, A4, A5
   Dim EC2, EC4, Aux, Aux1
   
   If H1 > H5 Then
      L6 = L3 - L1: H6 = H1 - H3: L7 = L5 - L3: H7 = H3 - H5
   Else
      L6 = L3 - L5: H6 = H5 - H3: L7 = L1 - L3: H7 = H3 - H1
   End If
   A1 = Atn(L6 / H6)
   A2 = Atn(L7 / H7)
   A3 = A1 - A2
   L8 = (H6 / 2) / Cos(A1)
   L9 = (H7 / 2) / Cos(A2)
   Aux = ((L8 * Cos(A3)) + L9) / Sin(A3)
   R0 = Sqr((Aux * Aux) + (L8 * L8))
   A4 = Asn(L9 / R0)
   A5 = A2 + (A4 * Sgn(A3))
   Xr0 = L3 - (R0 * Cos(A5) * Sgn(A3))
   Yr0 = H3 - (R0 * Sin(A5) * Sgn(A3))
   Aux = L2 - Xr0
   Aux1 = H2 - Yr0
   Ecart = Abs(Sqr((Aux * Aux) + (Aux1 * Aux1)) - R0)
   Aux = L4 - Xr0: Aux1 = H4 - Yr0
   Aux = Abs(Sqr((Aux * Aux) + (Aux1 * Aux1)) - R0)
   If Aux > Ecart Then Ecart = Aux
End Sub

Function Acs(ByVal Aux As Double) As Double
'
Dim Aux1 As Double
On Error Resume Next
  '
  Aux1 = (1 - Aux * Aux)
  If (Aux1 < 0) Then
    flagErrore = True
  Else
    If (Aux = 0) Then
      Acs = PG / 2
    Else
      If (Aux > 0) Then
        Acs = Atn(Sqr(1 - Aux * Aux) / Aux)
      Else
        Aux = Abs(Aux)
        Acs = Atn(Aux / Sqr(1 - Aux * Aux)) + PG / 2
      End If
    End If
  End If
  '
End Function

Function Asn(ByVal Aux As Double) As Double
'
Dim Aux1 As Double
On Error Resume Next
  '
  Aux1 = (1 - Aux * Aux)
  If (Aux1 < 0) Then
    flagErrore = True
  Else
    If (Aux = 1) Then
      Asn = PG / 2
    Else
      Asn = Atn(Aux / Sqr(Aux1))
    End If
  End If
  '
End Function

Private Function DIST(X1, Y1, X2, Y2) As Double
  DIST = Sqr((X1 - X2) ^ 2 + (Y1 - Y2) ^ 2)
End Function

Private Sub CalcUnPuntoAxialCRF(Xcrem As Double, Rref As Double, ApCrem As Double, _
                                Xax As Double, Rax As Double, ApAx As Double, _
                                InFilD As Double, InFil As Double, _
                                DprimCr As Double, PasAxial As Double)
'
Dim InEngr  As Double
Dim Ycrem   As Double
Dim Apa     As Double
Dim Rb      As Double
Dim A       As Double
Dim Intf$
Dim ApApp   As Double
Dim Helx    As Double
'
On Error Resume Next
   '
   ' Calcul de 1 point dans le plan axial
   ' obtenu en considerant le profil du plan axial de la fraise-mere
   ' comme etant le profil conjugue de la cremaillere de reference
   ' roulant sur le diametre primitif correspondant a l'inclinaison
   ' Ceci pour une inclinaison < 1 degre
   '
   ' Donnees :  PG
   ' + pour chaque point du profil de la cremaillere de reference :
   '           Xcrem  : Distance point considere a l'axe du plein
   '           Rref   : Distance point considere a l'axe de la fraise-mere
   '           Apcrem : Angle de pression sur le point considere en radians
   ' + CR : InFild , InFil , DprimCr , PasAxial
  '
  'Stop
   If InFilD < 1 Then
     Xax = Xcrem / Cos(InFil)
     Rax = Rref
     ApAx = Atn(Tan(ApCrem) / Cos(InFil))
   Else
     InEngr = PG / 2 - InFil
     RprimCr = DprimCr / 2
     Ycrem = RprimCr - Rref
     If Abs(ApCrem) < (89 * PG / 180) Then
        If Abs(ApCrem) = 0 Then
          ApCrem = 0.00001
        End If
        Apa = Atn(Tan(ApCrem) / Cos(InEngr))
        Rb = RprimCr * Cos(Apa)
        A = Rb * Tan(Apa) - Ycrem / Sin(Apa)
        If A < 0 Then Intf$ = "OUI" Else Intf$ = "NON"
        Rax = Sqr(Rb ^ 2 + A ^ 2)
        Aux = (Ycrem / Tan(Apa) + Xcrem / Cos(InEngr)) / RprimCr
        Aux = Aux - Apa + Atn(A / Rb)
        ApApp = Acs(Rb / Rax)
        Helx = Atn(PasAxial / PG / (Rax * 2))
        ApAx = Atn(Tan(ApApp) * Tan(Helx))
     Else
        Intf$ = "NON"
        Rax = Rref
        Aux = Xcrem / Cos(InEngr) / RprimCr
        ApApp = PG / 2
        Helx = Atn(PasAxial / PG / (Rax * 2))
        ApAx = PG / 2
     End If
     Xax = Aux * PasAxial / PG / 2
   End If
  '
End Sub


