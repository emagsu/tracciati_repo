Attribute VB_Name = "ROTORI2"
Option Explicit

Public picLARGHEZZA As Single
Public picALTEZZA   As Single
Public picSCALA     As Single

Public NomeMAS   As String
Public ZMAS      As Integer
Public NomeFEM   As String
Public ZFEM      As Integer
Public INTERASSE As Double
Public PassoFEM  As String
Public PassoMAS  As String
Public ImFEM     As String
Public ImMAS     As String

Sub ROTOLA(ByVal SiStampa As String)
'
Dim SpostamentoX    As Single
Dim SpostamentoY    As Single
Dim X(999)          As Double
Dim Y(999)          As Double
Dim R(999)          As Double
Dim A(999)          As Double
Dim i               As Integer
Dim j               As Integer
Dim stmp            As String
Dim PicW            As Single
Dim PicH            As Single
Dim SCALA           As Single
Dim PercorsoRotore  As String
Dim Z               As Integer
Dim dx              As Double
Static St           As Double
Dim SFASAGRAD       As Double
Dim DIAMPRIM        As Double
Dim COLORE          As Integer
Dim Np              As Integer
Dim Rmin            As Double
Dim Rmax            As Double
Dim RR              As Double
Dim AA              As Double
Dim BB              As Double
Dim X1              As Double
Dim Y1              As Double
Dim X2              As Double
Dim Y2                As Double
Dim Dente             As Integer
Dim deltALFA          As Double
Dim NumeroPassi       As Integer
Dim INTERASSE_LOCALE  As Single
Static iNumeroPassi   As Integer
Dim bb1               As Double
Dim bb2               As Double
Dim sp1               As Double
Dim sp2               As Double
'
Dim Pic As Object
Set Pic = OEMX.OEM1PicCALCOLO
'
On Error GoTo errROTOLA
  '
  OEMX.OEM1txtHELP.Visible = False
  OEMX.txtINP(1).Text = frmt(val(OEMX.txtINP(1).Text), 4)
  OEMX.txtINP(2).Text = frmt(val(OEMX.txtINP(2).Text), 4)
  SpostamentoX = val(OEMX.txtINP(3).Text): OEMX.txtINP(3).Text = frmt(SpostamentoX, 4)
  SpostamentoY = val(OEMX.txtINP(4).Text): OEMX.txtINP(4).Text = frmt(SpostamentoY, 4)
  'CONSIDERA LA CORREZIONE D'INTERASSE
  INTERASSE_LOCALE = INTERASSE + val(OEMX.txtINP(2).Text)
  If (SiStampa = "Y") Then
    'INIZIALIZZAZIONE STAMPANTE
    WRITE_DIALOG Printer.DeviceName & " PRINTING....."
    Printer.Orientation = 2
    Printer.ScaleMode = 6
  End If
  NumeroPassi = 360
  'LETTURA DEL VALORE DELLA SCALA
  SCALA = val(OEMX.txtINP(0).Text)
  If (SCALA <= 0) Then SCALA = 1
  OEMX.txtINP(0).Text = frmt(SCALA, 4)
  'Acquisizione area grafica
  'stmp = GetInfo("Configurazione", "PicW", PathCONFIGURAZIONE)
  'If Len(stmp) > 0 Then PicW = val(stmp) Else PicW = 190
  PicW = 190
  'stmp = GetInfo("Configurazione", "PicH", PathCONFIGURAZIONE)
  'If Len(stmp) > 0 Then PicH = val(stmp) Else PicH = 95
  PicH = 95
  Pic.ScaleWidth = PicW
  Pic.ScaleHeight = PicH
  'CALCOLO L'INCREMENTO
  If (OEMX.OEM1chkINPUT(2).Value = 1) Then
    If (iNumeroPassi < NumeroPassi) Then
      St = 90 - (360 / NumeroPassi) * iNumeroPassi
    Else
      iNumeroPassi = 1
      St = 90
    End If
  Else
    If (iNumeroPassi < NumeroPassi) Then
      St = -90 + (360 / NumeroPassi) * iNumeroPassi
    Else
      iNumeroPassi = 1
      St = -90
    End If
  End If
  iNumeroPassi = iNumeroPassi + 1
  'ASSI DI RIFERIMENTO
  If (SiStampa = "Y") Then
    Printer.Line (0, PicH / 2)-(PicW, PicH / 2)
    Printer.Line (PicW / 2, 0)-(PicW / 2, PicH)
  Else
    'PULISCO LO SCHERMO
    Pic.Cls
    Pic.Line (0, PicH / 2)-(PicW, PicH / 2)
    Pic.Line (PicW / 2, 0)-(PicW / 2, PicH)
  End If
  'INTERASSE DI LAVORO
  stmp = " Distance between centers" & "= " & frmt(INTERASSE_LOCALE, 4)
  If (SiStampa = "Y") Then
    Printer.CurrentX = 0
    Printer.CurrentY = PicH / 2
    Printer.Print stmp
  Else
    Pic.CurrentX = 0
    Pic.CurrentY = PicH / 2
    Pic.Print stmp
  End If
  'Primo rotore MASCHIO
  'If (SiSTAMPA = "Y") Then
  '  Printer.CurrentX = 0
  '  Printer.CurrentY = 0
  '  Printer.Print NomeMAS
  'Else
  '  Pic.CurrentX = 0
  '  Pic.CurrentY = 0
  '  Pic.Print NomeMAS
  'End If
  If (Dir$(NomeMAS) = "") Then
    WRITE_DIALOG Error(53) & " " & NomeMAS
    Exit Sub
  End If
  If (ZMAS <= 0) Then
    WRITE_DIALOG "ERROR: " & ZMAS & " not allowed!!"
    Exit Sub
  End If
  If (ZFEM <= 0) Then
    WRITE_DIALOG "ERROR: " & ZFEM & " not allowed!!"
    Exit Sub
  End If
  PercorsoRotore = NomeMAS
  Z = ZMAS
  dx = -INTERASSE_LOCALE / 2
  SFASAGRAD = St + val(OEMX.txtINP(1).Text)
  DIAMPRIM = INTERASSE_LOCALE * 2 * ZMAS / (ZMAS + ZFEM)
  'DpMAS
  stmp = " Dp1=" & frmt(DIAMPRIM, 4)
  If (SiStampa = "Y") Then
    Printer.CurrentX = 0
    Printer.CurrentY = Printer.TextHeight(stmp)
    Printer.Print stmp
  Else
    Pic.CurrentX = 0
    Pic.CurrentY = Pic.TextHeight(stmp)
    Pic.Print stmp
  End If
  'PassoMAS
  stmp = " P1=" & PassoMAS
  If (SiStampa = "Y") Then
    Printer.CurrentX = 0
    Printer.CurrentY = Printer.TextHeight(stmp) * 2
    Printer.Print stmp
  Else
    Pic.CurrentX = 0
    Pic.CurrentY = Pic.TextHeight(stmp) * 2
    Pic.Print stmp
  End If
  'Im
  stmp = " I1=" & ImMAS
  If (SiStampa = "Y") Then
    Printer.CurrentX = 0
    Printer.CurrentY = Printer.TextHeight(stmp) * 3
    Printer.Print stmp
  Else
    Pic.CurrentX = 0
    Pic.CurrentY = Pic.TextHeight(stmp) * 3
    Pic.Print stmp
  End If
  'epMAS
  bb1 = 90 - Atn(Abs(val(PassoMAS)) / PG / DIAMPRIM) * 180 / PG
  stmp = " ep1=" & frmt(bb1, 3)
  If (SiStampa = "Y") Then
    Printer.CurrentX = 0
    Printer.CurrentY = Printer.TextHeight(stmp) * 4
    Printer.Print stmp
  Else
    Pic.CurrentX = 0
    Pic.CurrentY = Pic.TextHeight(stmp) * 4
    Pic.Print stmp
  End If
  'MnMAS
  stmp = " Mn1=" & frmt(val(PassoMAS) * Sin(bb1 * PG / 180) / (Z * PG), 3)
  If (SiStampa = "Y") Then
    Printer.CurrentX = 0
    Printer.CurrentY = Printer.TextHeight(stmp) * 5
    Printer.Print stmp
  Else
    Pic.CurrentX = 0
    Pic.CurrentY = Pic.TextHeight(stmp) * 5
    Pic.Print stmp
  End If
  GoSub LEGGIROT
  COLORE = 9
  GoSub VISUALIZZA_PROFILO
  'Secondo rotore FEMMINA
  'If (SiSTAMPA = "Y") Then
  '  Printer.CurrentX = PicW - Printer.TextWidth(NomeFEM)
  '  Printer.CurrentY = 0
  '  Printer.Print NomeFEM
  'Else
  '  Pic.CurrentX = PicW - Pic.TextWidth(NomeFEM)
  '  Pic.CurrentY = 0
  '  Pic.Print NomeFEM
  'End If
  If (Dir$(NomeFEM) = "") Then
    WRITE_DIALOG Error(53) & " " & NomeFEM
    Exit Sub
  End If
  PercorsoRotore = NomeFEM
  Z = ZFEM
  dx = INTERASSE_LOCALE / 2
  SFASAGRAD = 90 - St * ZMAS / ZFEM
  COLORE = 12
  DIAMPRIM = INTERASSE_LOCALE * 2 * ZFEM / (ZMAS + ZFEM)
  'DpFEM
  stmp = "Dp2=" & frmt(DIAMPRIM, 4) & " "
  If (SiStampa = "Y") Then
    Printer.CurrentX = PicW - Printer.TextWidth(stmp)
    Printer.CurrentY = Printer.TextHeight(stmp)
    Printer.Print stmp
  Else
    Pic.CurrentX = PicW - Pic.TextWidth(stmp)
    Pic.CurrentY = Pic.TextHeight(stmp)
    Pic.Print stmp
  End If
  'PassoFEM
  stmp = "P2=" & PassoFEM & " "
  If (SiStampa = "Y") Then
    Printer.CurrentX = PicW - Printer.TextWidth(stmp)
    Printer.CurrentY = Printer.TextHeight(stmp) * 2
    Printer.Print stmp
  Else
    Pic.CurrentX = PicW - Pic.TextWidth(stmp)
    Pic.CurrentY = Pic.TextHeight(stmp) * 2
    Pic.Print stmp
  End If
  'Im
  stmp = "I2=" & ImFEM & " "
  If (SiStampa = "Y") Then
    Printer.CurrentX = PicW - Printer.TextWidth(stmp)
    Printer.CurrentY = Printer.TextHeight(stmp) * 3
    Printer.Print stmp
  Else
    Pic.CurrentX = PicW - Pic.TextWidth(stmp)
    Pic.CurrentY = Pic.TextHeight(stmp) * 3
    Pic.Print stmp
  End If
  'epFEM
  bb2 = 90 - Atn(Abs(val(PassoFEM)) / PG / DIAMPRIM) * 180 / PG
  stmp = "ep2=" & frmt(bb2, 3) & " "
  If (SiStampa = "Y") Then
    Printer.CurrentX = PicW - Printer.TextWidth(stmp)
    Printer.CurrentY = Printer.TextHeight(stmp) * 4
    Printer.Print stmp
  Else
    Pic.CurrentX = PicW - Pic.TextWidth(stmp)
    Pic.CurrentY = Pic.TextHeight(stmp) * 4
    Pic.Print stmp
  End If
  'MnFEM
  stmp = "Mn2=" & frmt(val(PassoFEM) * Sin(bb2 * PG / 180) / (Z * PG), 3)
  If (SiStampa = "Y") Then
    Printer.CurrentX = PicW - Printer.TextWidth(stmp)
    Printer.CurrentY = Printer.TextHeight(stmp) * 5
    Printer.Print stmp
  Else
    Pic.CurrentX = PicW - Pic.TextWidth(stmp)
    Pic.CurrentY = Pic.TextHeight(stmp) * 5
    Pic.Print stmp
  End If
  GoSub LEGGIROT
  'INVERSIONE FEMMINA
  If (OEMX.OEM1chkINPUT(0).Value = 1) Then
    SFASAGRAD = 90 + St * ZMAS / ZFEM
    Open PercorsoRotore For Input As #2
      i = Np
      Rmin = 999
      While Not EOF(2)
        Input #2, RR, AA, BB
        AA = -(SFASAGRAD + AA) * PG / 180
        R(i) = RR
        A(i) = AA
        X(i) = RR * Sin(AA)
        Y(i) = RR * Cos(AA)
        i = i - 1
        If RR < Rmin Then Rmin = RR
        If RR > Rmax Then Rmax = RR
      Wend
    Close #2
  End If
  GoSub VISUALIZZA_PROFILO
  If (SiStampa = "Y") Then
    'CHIUSURA PROCEDURA DI STAMPA
    WRITE_DIALOG ""
    Printer.EndDoc
  End If
  '
Exit Sub

errROTOLA:
  If (FreeFile > 0) Then Close
  OEMX.Timer1.Interval = 0
  MsgBox Error$(Err), 48, "SUB: ROTOLA"
  Exit Sub

LEGGIROT:
  Open PercorsoRotore For Input As #2
    i = 1: Rmin = 999: Rmax = -999
    While Not EOF(2)
      Input #2, RR, AA, BB
      AA = (SFASAGRAD + AA) * PG / 180
      R(i) = RR
      A(i) = AA
      X(i) = RR * Sin(AA)
      Y(i) = RR * Cos(AA)
      i = i + 1
      If RR < Rmin Then Rmin = RR
      If RR > Rmax Then Rmax = RR
    Wend
  Close #2
  Np = i - 1
  Return

VISUALIZZA_PROFILO:
  If (SiStampa = "Y") Then
    'ASSE SUL CENTRO PEZZO
    Printer.Line (dx * SCALA + PicW / 2 + SpostamentoX * SCALA, 0)-(dx * SCALA + PicW / 2 + SpostamentoX * SCALA, PicH)
    'DIAMETRO ESTERNO
    stmp = frmt(Rmax * 2, 4)
    Printer.CurrentX = dx * SCALA + PicW / 2 + SpostamentoX * SCALA - Printer.TextWidth(stmp) / 2
    Printer.CurrentY = -Rmax * SCALA + PicH / 2 - Printer.TextHeight(stmp)
    Printer.Print stmp
    deltALFA = 1
    For i = 1 To 359
      X1 = (Rmax * Sin(i * deltALFA * PG / 180) + dx) * SCALA + SpostamentoX * SCALA + PicW / 2
      Y1 = -(Rmax * Cos(i * deltALFA * PG / 180)) * SCALA + PicH / 2
      X2 = (Rmax * Sin((i + 1) * deltALFA * PG / 180) + dx) * SCALA + SpostamentoX * SCALA + PicW / 2
      Y2 = -(Rmax * Cos((i + 1) * deltALFA * PG / 180)) * SCALA + PicH / 2
      Printer.Line (X1, Y1)-(X2, Y2), QBColor(COLORE)
    Next i
    'DIAMETRO PRIMITIVO
    If (OEMX.OEM1chkINPUT(1).Value = 1) Then
      Printer.DrawWidth = 2
      deltALFA = 1
      For i = 1 To 359
        X1 = (DIAMPRIM / 2 * Sin(i * deltALFA * PG / 180) + dx) * SCALA + SpostamentoX * SCALA + PicW / 2
        Y1 = -(DIAMPRIM / 2 * Cos(i * deltALFA * PG / 180)) * SCALA + PicH / 2
        X2 = (DIAMPRIM / 2 * Sin((i + 1) * deltALFA * PG / 180) + dx) * SCALA + SpostamentoX * SCALA + PicW / 2
        Y2 = -(DIAMPRIM / 2 * Cos((i + 1) * deltALFA * PG / 180)) * SCALA + PicH / 2
        Printer.Line (X1, Y1)-(X2, Y2), QBColor(9)
      Next i
      Printer.DrawWidth = 1
    End If
    'DIAMETRO INTERNO
    stmp = frmt(Rmin * 2, 4)
    Printer.CurrentX = dx * SCALA + PicW / 2 + SpostamentoX * SCALA - Printer.TextWidth(stmp) / 2
    Printer.CurrentY = -Rmin * SCALA + PicH / 2 + SpostamentoX * SCALA - Printer.TextHeight(stmp)
    Printer.Print stmp
    deltALFA = 1
    For i = 1 To 359
      X1 = (Rmin * Sin(i * deltALFA * PG / 180) + dx) * SCALA + SpostamentoX * SCALA + PicW / 2
      Y1 = -(Rmin * Cos(i * deltALFA * PG / 180)) * SCALA + PicH / 2
      X2 = (Rmin * Sin((i + 1) * deltALFA * PG / 180) + dx) * SCALA + SpostamentoX * SCALA + PicW / 2
      Y2 = -(Rmin * Cos((i + 1) * deltALFA * PG / 180)) * SCALA + PicH / 2
      Printer.Line (X1, Y1)-(X2, Y2), QBColor(COLORE)
    Next i
    'Primo dente
    X1 = X(1) + dx: Y1 = Y(1)
    X1 = X1 * SCALA + SpostamentoX * SCALA + PicW / 2: Y1 = Y1 * SCALA + PicH / 2
    Printer.PSet (X1, Y1)
    GoSub VISDENTE
    'ALTRI DENTI
    For Dente = 2 To Z
      deltALFA = 2 * PG / Z * (Dente - 1)
      For j = 1 To Np
        X(j) = R(j) * Sin(A(j) + deltALFA)
        Y(j) = R(j) * Cos(A(j) + deltALFA)
      Next j
      X1 = X(1) + dx: Y1 = Y(1)
      X1 = X1 * SCALA + SpostamentoX * SCALA + PicW / 2: Y1 = Y1 * SCALA + PicH / 2
      Printer.PSet (X1, Y1)
      GoSub VISDENTE
    Next Dente
  Else
    'ASSE SUL CENTRO PEZZO
    Pic.Line (dx * SCALA + SpostamentoX * SCALA + PicW / 2, 0)-(dx * SCALA + SpostamentoX * SCALA + PicW / 2, PicH)
    'DIAMETRO ESTERNO
    stmp = frmt(Rmax * 2, 4)
    Pic.CurrentX = dx * SCALA + SpostamentoX * SCALA + PicW / 2 - Pic.TextWidth(stmp) / 2
    Pic.CurrentY = -Rmax * SCALA + PicH / 2 - Pic.TextHeight(stmp) - SpostamentoY * SCALA
    Pic.Print stmp
    deltALFA = 1
    For i = 1 To 359
      X1 = (Rmax * Sin(i * deltALFA * PG / 180) + dx) * SCALA + SpostamentoX * SCALA + PicW / 2
      Y1 = -(Rmax * Cos(i * deltALFA * PG / 180)) * SCALA + PicH / 2 - SpostamentoY * SCALA
      X2 = (Rmax * Sin((i + 1) * deltALFA * PG / 180) + dx) * SCALA + SpostamentoX * SCALA + PicW / 2
      Y2 = -(Rmax * Cos((i + 1) * deltALFA * PG / 180)) * SCALA + PicH / 2 - SpostamentoY * SCALA
      Pic.Line (X1, Y1)-(X2, Y2), QBColor(COLORE)
    Next i
    'DIAMETRO PRIMITIVO
    If (OEMX.OEM1chkINPUT(1).Value = 1) Then
      Pic.DrawWidth = 2
      deltALFA = 1
      For i = 1 To 359
        X1 = (DIAMPRIM / 2 * Sin(i * deltALFA * PG / 180) + dx) * SCALA + SpostamentoX * SCALA + PicW / 2
        Y1 = -(DIAMPRIM / 2 * Cos(i * deltALFA * PG / 180)) * SCALA + PicH / 2 - SpostamentoY * SCALA
        X2 = (DIAMPRIM / 2 * Sin((i + 1) * deltALFA * PG / 180) + dx) * SCALA + SpostamentoX * SCALA + PicW / 2
        Y2 = -(DIAMPRIM / 2 * Cos((i + 1) * deltALFA * PG / 180)) * SCALA + PicH / 2 - SpostamentoY * SCALA
        Pic.Line (X1, Y1)-(X2, Y2), QBColor(9)
      Next i
      Pic.DrawWidth = 1
    End If
    'DIAMETRO INTERNO
    stmp = frmt(Rmin * 2, 4)
    Pic.CurrentX = dx * SCALA + SpostamentoX * SCALA + PicW / 2 - Pic.TextWidth(stmp) / 2
    Pic.CurrentY = -Rmin * SCALA + PicH / 2 - Pic.TextHeight(stmp) - SpostamentoY * SCALA
    Pic.Print stmp
    deltALFA = 1
    For i = 1 To 359
      X1 = (Rmin * Sin(i * deltALFA * PG / 180) + dx) * SCALA + SpostamentoX * SCALA + PicW / 2
      Y1 = -(Rmin * Cos(i * deltALFA * PG / 180)) * SCALA + PicH / 2 - SpostamentoY * SCALA
      X2 = (Rmin * Sin((i + 1) * deltALFA * PG / 180) + dx) * SCALA + SpostamentoX * SCALA + PicW / 2
      Y2 = -(Rmin * Cos((i + 1) * deltALFA * PG / 180)) * SCALA + PicH / 2 - SpostamentoY * SCALA
      Pic.Line (X1, Y1)-(X2, Y2), QBColor(COLORE)
    Next i
    'Primo dente
    X1 = X(1) + dx: Y1 = Y(1)
    X1 = X1 * SCALA + SpostamentoX * SCALA + PicW / 2: Y1 = Y1 * SCALA + PicH / 2 - SpostamentoY * SCALA
    Pic.PSet (X1, Y1)
    GoSub VISDENTE
    'ALTRI DENTI
    For Dente = 2 To Z
      deltALFA = 2 * PG / Z * (Dente - 1)
      For j = 1 To Np
        X(j) = R(j) * Sin(A(j) + deltALFA)
        Y(j) = R(j) * Cos(A(j) + deltALFA)
      Next j
      X1 = X(1) + dx: Y1 = Y(1)
      X1 = X1 * SCALA + SpostamentoX * SCALA + PicW / 2: Y1 = Y1 * SCALA + PicH / 2 - SpostamentoY * SCALA
      Pic.PSet (X1, Y1)
      GoSub VISDENTE
    Next Dente
  End If
  Return

VISDENTE:
  If (SiStampa = "Y") Then
    For i = 2 To Np
      X2 = X(i) + dx: Y2 = Y(i)
      X2 = X2 * SCALA + SpostamentoX * SCALA + PicW / 2: Y2 = Y2 * SCALA + PicH / 2 - SpostamentoY * SCALA
      Printer.Line (X1, Y1)-(X2, Y2), QBColor(COLORE)
      X1 = X2: Y1 = Y2
    Next i
  Else
    For i = 2 To Np
      X2 = X(i) + dx: Y2 = Y(i)
      X2 = X2 * SCALA + SpostamentoX * SCALA + PicW / 2: Y2 = Y2 * SCALA + PicH / 2 - SpostamentoY * SCALA
      Pic.Line (X1, Y1)-(X2, Y2), QBColor(COLORE)
      X1 = X2: Y1 = Y2
    Next i
  End If
Return

End Sub

Sub VISUALIZZA_SOVRAMETALLO_ROTORI(ByVal SiStampa As String)

Dim ScalaPROFILO  As Single
Dim ScalaERRORE   As Single
Dim SiNORMALI     As Integer
Dim SiINDICI      As Integer
Dim SiASSI        As Integer
Dim PicW          As Single
Dim PicH          As Single
'
Dim stmp          As String
Dim PERCORSO      As String
Dim COORDINATE    As String
Dim RADICE        As String
Dim NomeROTORE    As String
Dim Rmax          As Double
Dim Rmin          As Double
Dim TY_ROT        As String
Dim DM_ROT        As String
Dim PASSO         As Single
Dim ELICA         As Single
Dim NpM_F1        As Long
Dim NpM_F2        As Long
'
Dim FilePROFILO_RIFERIMENTO As String
Dim FilePROFILO_TEORICO     As String
Dim FilePROFILO_CALCOLATO   As String
Dim FilePROFILO_MOLA        As String
Dim IndiceZeroPezzoX        As Integer
Dim IndiceZeroPezzoY        As Integer
Dim ZeroPezzoX              As Double
Dim ZeroPezzoY              As Double
Dim R()                     As Single
Dim A()                     As Single
Dim B()                     As Single
Dim Np                      As Integer
Dim DeltaX                  As Single
Dim DeltaY                  As Single
Dim ColoreCurva             As Long
'
On Error Resume Next
  '
  OEMX.OEM1hsPROFILO.Left = OEMX.OEM1PicCALCOLO.Left + OEMX.OEM1PicCALCOLO.Width - OEMX.OEM1hsPROFILO.Width
  OEMX.OEM1hsPROFILO.Top = OEMX.txtINP(4).Top
  OEMX.OEM1hsPROFILO.Visible = True
  OEMX.Refresh
  '
  PERCORSO = Lp_Str("PERCORSO")
  PERCORSO = g_chOemPATH & "\" & PERCORSO
  'RICAVO IL NOME DEI PUNTI DEL ROTORE DAL PERCORSO
  COORDINATE = PERCORSO
  If (Len(COORDINATE) > 0) Then
    i = InStr(COORDINATE, "\")
    While (i <> 0)
      j = i
      i = InStr(i + 1, COORDINATE, "\")
    Wend
    'RICAVO IL NOME DEI PUNTI DEL ROTORE
    COORDINATE = Right$(COORDINATE, Len(COORDINATE) - j)
  End If
  'RICAVO IL PERCORSO DI LAVORO
  RADICE = PERCORSO
  i = InStr(RADICE, COORDINATE)
  RADICE = Left$(RADICE, i - 2)
  'RICAVO IL NOME DEL ROTORE
  NomeROTORE = RADICE
  If (Len(NomeROTORE) > 0) Then
    i = InStr(NomeROTORE, "\")
    While (i <> 0)
      j = i
      i = InStr(i + 1, NomeROTORE, "\")
    Wend
    'RICAVO IL NOME DEI PUNTI DEL ROTORE
    NomeROTORE = Right$(NomeROTORE, Len(NomeROTORE) - j)
  End If
  DM_ROT = Lp_Str("DM_ROT")
  TY_ROT = Lp_Str("TY_ROT")
  PASSO = Lp("PASSO")
  ELICA = Lp("INCLINAZIONE")
  NpM_F1 = GetInfo("CREAZIONE", "NpM_F1", RADICE & "\INFO.INI")
  NpM_F2 = GetInfo("CREAZIONE", "NpM_F2", RADICE & "\INFO.INI")
  '
  ScalaPROFILO = val(OEMX.txtINP(0).Text)
  ScalaERRORE = 1
  SiNORMALI = 1
  SiINDICI = 0
  SiASSI = 0
  '
  'Preparazione grafico
  PicW = 190
  PicH = 95
  If (SiStampa = "Y") Then
    Call ImpostaPrinter("Arial", 10)
    WRITE_DIALOG Printer.DeviceName & " PRINTING....."
    Printer.ScaleWidth = PicW
    Printer.ScaleHeight = PicH
    Printer.Orientation = 2
    Printer.ScaleMode = 6
  Else
    OEMX.OEM1PicCALCOLO.Cls
    OEMX.OEM1PicCALCOLO.FontName = "Arial"
    OEMX.OEM1PicCALCOLO.Refresh
    OEMX.OEM1PicCALCOLO.ScaleWidth = PicW
    OEMX.OEM1PicCALCOLO.ScaleHeight = PicH
  End If
  '
  FilePROFILO_RIFERIMENTO = RADICE & "\PRFROT"
  FilePROFILO_TEORICO = RADICE & "\" & NomeROTORE & ".RIF"
  FilePROFILO_CALCOLATO = RADICE & "\" & NomeROTORE & ".CAL"
  FilePROFILO_MOLA = RADICE & "\XYT." & Format(val(DM_ROT), "##0")
  '
  picSCALA = ScalaPROFILO
  picLARGHEZZA = PicW
  picALTEZZA = PicH
  '
  'DETERMINAZIONE DEI PUNTI DI RIFERIMENTO SUL PROFILO TEORICO
  If (Dir$(FilePROFILO_RIFERIMENTO) <> "") Then
    Open FilePROFILO_RIFERIMENTO For Input As #1
      i = 1: Rmax = -1E+308: Rmin = 1E+308
      While Not EOF(1)
        ReDim Preserve R(i): ReDim Preserve A(i): ReDim Preserve B(i)
        Input #1, R(i), A(i), B(i)
        A(i) = A(i) '+  val(txtDELTA(3).Text)
        If (Rmax < R(i)) Then Rmax = R(i)
        If (Rmin > R(i)) Then Rmin = R(i)
        i = i + 1
      Wend
    Close #1
    Np = i - 1
  Else
    MsgBox FilePROFILO_RIFERIMENTO & ": " & Error(53), 48, "WARNING"
    Exit Sub
  End If
  '
  OEMX.OEM1hsPROFILO.Max = Np
  '
  'LETTURA DEGLI INDICI DEI PUNTI DI RIFERIMENTO PER LO ZERO PEZZO
  IndiceZeroPezzoX = OEMX.OEM1hsPROFILO.Value
  IndiceZeroPezzoY = OEMX.OEM1hsPROFILO.Value
  '
  'DETERMINAZIONE DELLE ORIGINI PER LA VISUALIZZAZIONE DEI PROFILI
  If (TY_ROT = "AXIAL") Then
    If (IndiceZeroPezzoX >= 1) And (IndiceZeroPezzoX <= Np) Then
      'Profilo AZZERATO SU DI UN PUNTO DEL PROFILO STESSO
      ZeroPezzoX = A(IndiceZeroPezzoX) / 360 * PASSO
    Else
      'Profilo centrato
      ZeroPezzoX = 0
    End If
    If (IndiceZeroPezzoY >= 1) And (IndiceZeroPezzoY <= Np) Then
      'Profilo AZZERATO SU DI UN PUNTO DEL PROFILO STESSO
      ZeroPezzoY = R(IndiceZeroPezzoY)
    Else
      'Profilo centrato
      ZeroPezzoY = (Rmin + Rmax) / 2 'Rmin
    End If
  Else
    If (IndiceZeroPezzoX >= 1) And (IndiceZeroPezzoX <= Np) Then
      'Profilo AZZERATO SU DI UN PUNTO DEL PROFILO STESSO
      ZeroPezzoX = R(IndiceZeroPezzoX) * Sin((A(IndiceZeroPezzoX)) * PG / 180)
    Else
      ZeroPezzoX = 0
    End If
    If (IndiceZeroPezzoY >= 1) And (IndiceZeroPezzoY <= Np) Then
      'Profilo AZZERATO SU DI UN PUNTO DEL PROFILO STESSO
      ZeroPezzoY = R(IndiceZeroPezzoY) * Cos((A(IndiceZeroPezzoY)) * PG / 180)
    Else
      'Profilo centrato
      ZeroPezzoY = (Rmin + Rmax) / 2 'Rmin
    End If
  End If
  If (val(OEMX.txtINP(1).Text) <> 0) Then
    stmp = " IM=" & frmt(Abs(ELICA) + val(OEMX.txtINP(1).Text), 4)
    'stmp = " IM=" & frmt(Abs(ELICA), 4)
    OEMX.OEM1PicCALCOLO.CurrentX = PicW / 2
    OEMX.OEM1PicCALCOLO.CurrentY = 0
    OEMX.OEM1PicCALCOLO.Print stmp
  End If
  '
  'VISUALIZZAZIONE DEL PROFILO TEORICO DI RIFERIMENTO: CON IL MATERIALE NORMALE
  ColoreCurva = 0
  Call VERIFICA_PROFILO(OEMX.OEM1PicCALCOLO, ScalaPROFILO, ScalaERRORE, SiStampa, SiNORMALI, SiINDICI, SiASSI, FilePROFILO_RIFERIMENTO, ColoreCurva, 2, ZeroPezzoX, ZeroPezzoY, val(OEMX.txtINP(1).Text), val(OEMX.txtINP(2).Text), 0, NpM_F1, TY_ROT, PASSO)
  '
  'CALCOLO DEL PROFILO DI RIFERIMENTO GENERATO DALLA MOLA CON EQUIDISTANZA SUL CENTRO MOLA
  Call CALCOLO_ROTORE_DA_MOLA(FilePROFILO_MOLA, "", FilePROFILO_TEORICO, 0, 0, 0, -val(OEMX.txtINP(2).Text), 0, 0, RADICE, NpM_F1, DM_ROT, PASSO, ELICA)
  'VISUALIZZAZIONE DEL CALCOLO DEL PROFILO DI RIFERIMENTO GENERATO DALLA MOLA CON EQUIDISTANZA SUL CENTRO MOLA
  ColoreCurva = 9
  Call VERIFICA_PROFILO(OEMX.OEM1PicCALCOLO, ScalaPROFILO, ScalaERRORE, SiStampa, SiNORMALI, SiINDICI, SiASSI, FilePROFILO_TEORICO, ColoreCurva, 3, ZeroPezzoX, ZeroPezzoY, val(OEMX.txtINP(1).Text), 0, 0, NpM_F1, TY_ROT, PASSO)
  If (SiStampa = "Y") Then
    Printer.Line (PicW / 2 + DeltaX * ScalaPROFILO, 0)-(PicW / 2 + DeltaX * ScalaPROFILO, PicH), QBColor(ColoreCurva)
    Printer.Line (0 + DeltaX * ScalaPROFILO, PicH / 2)-(PicW + DeltaX * ScalaPROFILO, PicH / 2), QBColor(ColoreCurva)
  Else
    OEMX.OEM1PicCALCOLO.Line (PicW / 2 + DeltaX * ScalaPROFILO, 0)-(PicW / 2 + DeltaX * ScalaPROFILO, PicH), QBColor(ColoreCurva)
    OEMX.OEM1PicCALCOLO.Line (0 + DeltaX * ScalaPROFILO, PicH / 2)-(PicW + DeltaX * ScalaPROFILO, PicH / 2), QBColor(ColoreCurva)
  End If
  '
  'CALCOLO DEL PROFILO ROTORE DALLA MOLA CAMBIANDO INTERASSE, CENTRO MOLA ED INCLINAZIONE SENZA EQUIDISTANZA
  Call CALCOLO_ROTORE_DA_MOLA(FilePROFILO_MOLA, "", FilePROFILO_CALCOLATO, val(OEMX.txtINP(4).Text), val(OEMX.txtINP(3).Text), 0, 0, val(OEMX.txtINP(1).Text), 0, RADICE, NpM_F1, DM_ROT, PASSO, ELICA)
  'VISUALIZZAZIONE DEL CALCOLO DEL PROFILO ROTORE DALLA MOLA CAMBIANDO INTERASSE, CENTRO MOLA ED INCLINAZIONE SENZA EQUIDISTANZA
  ColoreCurva = 12
  Call VERIFICA_PROFILO(OEMX.OEM1PicCALCOLO, ScalaPROFILO, ScalaERRORE, SiStampa, SiNORMALI, SiINDICI, SiASSI, FilePROFILO_CALCOLATO, ColoreCurva, 4, ZeroPezzoX, ZeroPezzoY, 0, 0, 1, NpM_F1, TY_ROT, PASSO)
  If (SiStampa = "Y") Then
    Printer.Line (PicW / 2 + DeltaX * ScalaPROFILO, 0)-(PicW / 2 + DeltaX * ScalaPROFILO, PicH), QBColor(ColoreCurva)
  Else
    OEMX.OEM1PicCALCOLO.Line (PicW / 2 + DeltaX * ScalaPROFILO, 0)-(PicW / 2 + DeltaX * ScalaPROFILO, PicH), QBColor(ColoreCurva)
  End If
  '
  Call SCRIVI_SCALA_ORIZZONTALE(ScalaPROFILO, OEMX.OEM1PicCALCOLO, SiStampa)
  '
  Open RADICE & "\VERIFICA.ERR" For Output As #1
    Print #1, "--------"
    Print #1, "ROTORE: " & NomeROTORE
    For i = 0 To 5
      Print #1, i 'lblDELTA(i).Caption & ":" & txtDELTA(i).Text
    Next i
  Close #1
  '
  'PROVE PER LA VERIFICA PROFILO MOLA
  Call CALCOLO_ERRORI_COME_RISULTATO_CONTROLLO(FilePROFILO_TEORICO, FilePROFILO_CALCOLATO, 0, RADICE, NpM_F1 - 1, NpM_F2 - 1, PASSO)
  '
  'Preparazione grafico
  If (SiStampa = "Y") Then
    Printer.EndDoc
    WRITE_DIALOG ""
    Printer.Orientation = 1
  End If
  '
End Sub
'
Sub CALCOLO_ERRORI_COME_RISULTATO_CONTROLLO(ByVal FilePROFILO_TEORICO As String, ByVal FilePROFILO_CALCOLATO As String, ByVal CORREZIONE_ROTAZIONE As Double, ByVal RADICE As String, ByVal NUM_F1 As Long, ByVal NUM_F2 As Long, ByVal PASSO As Single)
'
Dim i    As Long
'
Dim R1() As Double
Dim A1() As Double
Dim B1() As Double
Dim X1() As Double
Dim Y1() As Double
Dim NpT1 As Long
'
Dim R2() As Double
Dim A2() As Double
Dim B2() As Double
Dim X2() As Double
Dim Y2() As Double
Dim NpT2 As Long
'
Dim EE() As Double
'
On Error GoTo errCALCOLO_ERRORI_COME_RISULTATO_CONTROLLO
  '
  'Acquisizione FilePROFILO_TEORICO (BLU)
  If (Dir$(FilePROFILO_TEORICO) <> "") Then
    Open FilePROFILO_TEORICO For Input As #1
      i = 1
      While Not EOF(1)
        ReDim Preserve R1(i): ReDim Preserve A1(i): ReDim Preserve B1(i)
        ReDim Preserve X1(i): ReDim Preserve Y1(i)
        Input #1, R1(i), A1(i), B1(i)
        A1(i) = A1(i) + CORREZIONE_ROTAZIONE
        X1(i) = R1(i) * Sin(A1(i) * PG / 180)
        Y1(i) = R1(i) * Cos(A1(i) * PG / 180)
        i = i + 1
      Wend
    Close #1
    NpT1 = i - 1
  Else
    MsgBox FilePROFILO_TEORICO & ": " & Error(53), 48, "WARNING"
    Exit Sub
  End If
  '
  'Acquisizione FilePROFILO_CALCOLATO (ROSSO)
  If (Dir$(FilePROFILO_CALCOLATO) <> "") Then
    Open FilePROFILO_CALCOLATO For Input As #1
      i = 1
      While Not EOF(1)
        ReDim Preserve R2(i): ReDim Preserve A2(i): ReDim Preserve B2(i)
        ReDim Preserve X2(i): ReDim Preserve Y2(i)
        Input #1, R2(i), A2(i), B2(i)
        X2(i) = R2(i) * Sin(A2(i) * PG / 180)
        Y2(i) = R2(i) * Cos(A2(i) * PG / 180)
        i = i + 1
      Wend
    Close #1
    NpT2 = i - 1
  Else
    MsgBox FilePROFILO_CALCOLATO & ": " & Error(53), 48, "WARNING"
    Exit Sub
  End If
  '
  'CASO IMPOSSIBILE
  If (NpT1 <> NpT2) Then
    StopRegieEvents
    MsgBox "NUMERO DI PUNTI DIVERSI", vbCritical, "WARNING"
    ResumeRegieEvents
    Exit Sub
  End If
  '
  ReDim EE(NpT2)
  '
  'CALCOLO DEGLI ERRORI ASSOLUTI
  For i = 1 To NpT2
    EE(i) = Sqr((X2(i) - X1(i)) ^ 2 + (Y2(i) - Y1(i)) ^ 2)
  Next i
  '
  'ASSEGNAZIONE DEI SEGNI
  '
  'DEVO MODIFICARE: RIPORTARE GLI ERRORI DAL PIANO FRONTALE SULLE NORMALI AL PROFILO.
  '
  Dim Ang   As Double
  Dim NX    As Double
  Dim NY    As Double
  Dim dx    As Double
  Dim Dy    As Double
  Dim SEGNO As Double
  '
  Dim stmp  As String
  '
  Open RADICE & "\VERIFICA.ERR" For Append As #1
  'FIANCO 1
  Print #1, "--------"
  Print #1, "FIANCO 1"
  Print #1, "--------"
  For i = 1 To NUM_F1
    'NORMALE
    Ang = (A1(i) + B1(i)) * PG / 180
    NX = Cos(Ang): NY = -Sin(Ang)
    dx = X2(i) - X1(i)
    Dy = Y2(i) - Y1(i)
    'SEGNO = dx * NX + Dy * NY
    If (Dy > 0) Then
      SEGNO = 1
    Else
      SEGNO = -1
    End If
    If (SEGNO < 0) Then EE(i) = -EE(i)
    stmp = ""
    stmp = Format$(i, "00000") & " "
    If (dx >= 0) Then stmp = stmp & "+" Else stmp = stmp & "-"
    stmp = stmp & frmt0(Abs(dx), 4) & " "
    If (Dy >= 0) Then stmp = stmp & "+" Else stmp = stmp & "-"
    stmp = stmp & frmt0(Abs(Dy), 4) & " "
    If (EE(i) >= 0) Then stmp = stmp & "+" Else stmp = stmp & "-"
    stmp = stmp & frmt0(Abs(EE(i)), 4) & " "
    If (NX >= 0) Then stmp = stmp & "+" Else stmp = stmp & "-"
    stmp = stmp & frmt0(Abs(NX), 4) & " "
    If (NY >= 0) Then stmp = stmp & "+" Else stmp = stmp & "-"
    stmp = stmp & frmt0(Abs(NY), 4) & " "
    Print #1, stmp
  Next i
  'FIANCO 2
  Print #1, "--------"
  Print #1, "FIANCO 2"
  Print #1, "--------"
  For i = 1 To NUM_F2
    'NORMALE
    Ang = (A1(NUM_F1 + i) + B1(NUM_F1 + i)) * PG / 180
    NX = -Cos(Ang): NY = Sin(Ang)
    dx = X2(NUM_F1 + i) - X1(NUM_F1 + i)
    Dy = Y2(NUM_F1 + i) - Y1(NUM_F1 + i)
    'SEGNO = dx * NX + Dy * NY
    If (Dy > 0) Then
      SEGNO = 1
    Else
      SEGNO = -1
    End If
    If (SEGNO < 0) Then EE(NUM_F1 + i) = -EE(NUM_F1 + i)
    stmp = ""
    stmp = Format$(i + NUM_F1, "00000") & " "
    If (dx >= 0) Then stmp = stmp & "+" Else stmp = stmp & "-"
    stmp = stmp & frmt0(Abs(dx), 4) & " "
    If (Dy >= 0) Then stmp = stmp & "+" Else stmp = stmp & "-"
    stmp = stmp & frmt0(Abs(Dy), 4) & " "
    If (EE(i) >= 0) Then stmp = stmp & "+" Else stmp = stmp & "-"
    stmp = stmp & frmt0(Abs(EE(i)), 4) & " "
    If (NX >= 0) Then stmp = stmp & "+" Else stmp = stmp & "-"
    stmp = stmp & frmt0(Abs(NX), 4) & " "
    If (NY >= 0) Then stmp = stmp & "+" Else stmp = stmp & "-"
    stmp = stmp & frmt0(Abs(NY), 4) & " "
    Print #1, stmp
  Next i
  Close #1
  '
  Dim j As Integer
  Dim k As Integer
  '
  Dim errF1(999) As Double
  Dim errF2(999) As Double
  '
  Dim Hr()  As Double
  Dim RRchk As Double
  Dim AAchk As Double
  Dim BBchk As Double
  '
  'SEPARAZIONE ERRORI SUI PUNTI CONTROLLATI
  'FIANCO 1: ORDINE INALTERATO
  For i = 1 To NUM_F1
    errF1(i) = EE(i)
  Next i
  'FIANCO 2: ORDINE INVERTITO
  For i = 1 To NUM_F2
    errF2(i) = EE(NUM_F1 + 1 + NUM_F2 - i)
  Next i
  '
  'ERRORI --> CORREZIONI SULLA MOLA
  'LETTURA PROFILO RAB TEORICO
  Open FilePROFILO_TEORICO For Input As #1
    'TRASFORMAZIONE DA ERRORI FRONTALI SUL PROFILO ROTORE A ERRORI SUL PROFILO MOLA
    '*****************************************************************************
    ' PRG | KLN | ACV
    '  X  |  X  |  Z
    '  Y  |  Y  |  X
    '*****************************************************************************
    i = 0
    While Not EOF(1)
      i = i + 1
      ReDim Preserve Hr(i)
      Input #1, RRchk, AAchk, BBchk
      'COTATION DU PROFIL DANS LE PLAN APPARENT
      Hr(i) = Atn(2 * PG * RRchk * Cos(BBchk * PG / 180) / PASSO)
      'ERRORI SUL PEZZO --> CORREZIONI NORMALI SULLA MOLA
      If (i <= NUM_F1) Then
        errF1(i) = -errF1(i) * Cos(Hr(i))
      Else
        errF2(NUM_F2 - (i - NUM_F1 - 1)) = -errF2(NUM_F2 - (i - NUM_F1 - 1)) * Cos(Hr(i))
      End If
    Wend
  Close #1
  '
  'COSTRUISCO I FILE DEGLI ERRORI
  Open RADICE & "\RISROT" For Output As #2
    'COSTRUISCO LA RIGA RISROT CON I VALORI RICAVATI
    If (NUM_F2 >= NUM_F1) Then
      For i = 1 To NUM_F1
        stmp = ""
        If errF2(i) >= 0 Then
          stmp = stmp & "+" & frmt0(Abs(errF2(i)), 4)
        Else
          stmp = stmp & "-" & frmt0(Abs(errF2(i)), 4)
        End If
        stmp = stmp & ","
        If errF1(i) >= 0 Then
          stmp = stmp & "+" & frmt0(Abs(errF1(i)), 4)
        Else
          stmp = stmp & "-" & frmt0(Abs(errF1(i)), 4)
        End If
        Print #2, stmp
      Next i
      For i = NUM_F1 + 1 To NUM_F2
        stmp = ""
        If errF2(i) >= 0 Then
          stmp = stmp & "+" & frmt0(Abs(errF2(i)), 4)
        Else
          stmp = stmp & "-" & frmt0(Abs(errF2(i)), 4)
        End If
        stmp = stmp & ","
        stmp = stmp & "+" & frmt0(0, 4)
        Print #2, stmp
      Next i
      If NUM_F2 < 40 Then
        For i = 1 To 40 - NUM_F2
          stmp = "+0.0000,+0.0000"
          Print #2, stmp
        Next i
      Else
        stmp = "+0.0000,+0.0000"
        Print #2, stmp
      End If
    Else
      For i = 1 To NUM_F2
        stmp = ""
        If errF2(i) >= 0 Then
          stmp = stmp & "+" & frmt0(Abs(errF2(i)), 4)
        Else
          stmp = stmp & "-" & frmt0(Abs(errF2(i)), 4)
        End If
        stmp = stmp & ","
        If errF1(i) >= 0 Then
          stmp = stmp & "+" & frmt0(Abs(errF1(i)), 4)
        Else
          stmp = stmp & "-" & frmt0(Abs(errF1(i)), 4)
        End If
        Print #2, stmp
      Next i
      For i = NUM_F2 + 1 To NUM_F1
        stmp = ""
        stmp = stmp & "+" & frmt0(0, 4)
        stmp = stmp & ","
        If errF1(i) >= 0 Then
          stmp = stmp & "+" & frmt0(Abs(errF1(i)), 4)
        Else
          stmp = stmp & "-" & frmt0(Abs(errF1(i)), 4)
        End If
        Print #2, stmp
      Next i
      If NUM_F1 < 40 Then
        For i = 1 To 40 - NUM_F1
          stmp = "+0.0000,+0.0000"
          Print #2, stmp
        Next i
      Else
        stmp = "+0.0000,+0.0000"
        Print #2, stmp
      End If
    End If
  Close #2
  '
  Open RADICE & "\EPMROT" For Output As #2
    'COSTRUISCO LA RIGA EPMROT CON I VALORI RICAVATI
    For i = 1 To NUM_F1
      stmp = ""
      If errF1(i) >= 0 Then
        stmp = stmp & "+" & frmt0(Abs(errF1(i)), 4)
      Else
        stmp = stmp & "-" & frmt0(Abs(errF1(i)), 4)
      End If
      stmp = stmp & ","
      stmp = stmp & "+0.0000"
      Print #2, stmp
    Next i
    For i = 1 To NUM_F2
      stmp = ""
      If errF2(i) >= 0 Then
        stmp = stmp & "+" & frmt0(Abs(errF2(i)), 4)
      Else
        stmp = stmp & "-" & frmt0(Abs(errF2(i)), 4)
      End If
      stmp = stmp & ","
      stmp = stmp & "+0.0000"
      Print #2, stmp
    Next i
    If NUM_F2 + NUM_F1 < nMAXpntG4 Then
      For i = 1 To nMAXpntG4 - NUM_F2 - NUM_F1
        stmp = "+0.0000,+0.0000"
        Print #2, stmp
      Next i
    Else
      stmp = "+0.0000,+0.0000"
      Print #2, stmp
    End If
  Close #2
  '
Exit Sub

errCALCOLO_ERRORI_COME_RISULTATO_CONTROLLO:
  If (FreeFile > 0) Then Close
  MsgBox Error$(Err), 48, "SUB: CALCOLO_ERRORI_COME_RISULTATO_CONTROLLO"
  'Resume Next
  Exit Sub
  '
End Sub

Sub VERIFICA_PROFILO(ByVal OGGETTO As Object, _
              ByVal ScalaPROFILO As Single, ByVal ScalaERRORE As Single, _
              ByVal SiStampa As String, ByVal SiNORMALI As Integer, _
              ByVal SiINDICI As Integer, ByVal SiASSI As Integer, _
              ByVal FilePROFILO As String, ByVal ColoreCurva As Integer, _
              ByVal QUADRANTE As Integer, _
              ZeroPezzoX As Double, ZeroPezzoY As Double, _
              ByVal ROTAZIONE_AGGIUNTIVA As Double, _
              ByVal EQUIDISTANZA As Double, ByVal SiSTIMA_CERCHIO As Integer, NpM_F1 As Long, TY_ROT As String, PASSO As Single)
'
Dim R160 As Double
Dim R161 As Double
Dim R162 As Double
Dim R163 As Double
Dim R164 As Double
Dim R165 As Double
Dim R166 As Double
Dim R167 As Double
Dim R168 As Double
Dim R169 As Double
Dim R170 As Double
Dim R171 As Double
Dim R172 As Double
Dim R173 As Double
Dim R174 As Double
'
Dim DR   As Double
Dim iDR  As Integer
'
Dim vRaggioMinimo As Double
Dim iRaggioMinimo As Integer
'
Dim Rmax As Double
Dim Rmin As Double
'
Dim AngStart  As Double
Dim AngEnd    As Double
Dim Ang       As Double
Dim DeltaAng  As Double
Dim DeltaArco As Double
'
Dim oldtext  As String
Dim ftmp     As Double
'
Dim ScalaX As Double
Dim ScalaY As Double
'
Dim R()  As Double
Dim A()  As Double
Dim B()  As Double
'
Dim stmp As String
Dim riga As String
Dim i    As Integer
Dim j    As Integer
Dim k    As Integer
'
Dim Np   As Integer
Dim NPF1 As Integer
Dim dtmp As Integer
'
Dim PicW As Single
Dim PicH As Single
'
Dim pX1 As Double
Dim pY1 As Double
Dim pX2 As Double
Dim pY2 As Double
'
Dim da  As Single
'
Dim Raggio As Double
Dim ARCO_ESTERNO As Double
'
Const AF = 30
Const DF = 2
Const NumeroRette = 50
Const NumeroArchi = 50
'
Dim X1        As Double
Dim Y1        As Double
Dim X2        As Double
Dim Y2        As Double
Dim deltALFA  As Double
Dim iERR      As Long
'
On Error GoTo errVERIFICA_PROFILO
  '
  If (SiStampa = "Y") Then Set OGGETTO = Printer
  '
  'Acquisizione area grafica  '--- FLAGMOD 006 04.07.2006
  PicW = OGGETTO.ScaleWidth
  PicH = OGGETTO.ScaleHeight
  '
  ScalaX = ScalaPROFILO: ScalaY = ScalaPROFILO
  '
  If (SiASSI = 1) Then
    'ASSI COORDINATI
    OGGETTO.Line (0, PicH / 2)-(PicW, PicH / 2)
    OGGETTO.Line (PicW / 2, 0)-(PicW / 2, PicH)
    'FRECCIA SUPERIORE
    X1 = PicW / 2: Y1 = 0
    deltALFA = (30 / 180 * PG) / NumeroRette
    For i = -NumeroRette To NumeroRette
      X2 = X1 + 2 * Sin(i * deltALFA)
      Y2 = Y1 + 2 * Cos((30 / 180 * PG))
      OGGETTO.Line (X1, Y1)-(X2, Y2)
    Next i
    'NOME ASSE Y
    stmp = "   Y"
    OGGETTO.CurrentX = PicW / 2
    OGGETTO.CurrentY = 0
    OGGETTO.Print stmp
    'FRECCIA LATERALE
    X1 = PicW: Y1 = PicH / 2
    deltALFA = (30 / 180 * PG) / NumeroRette
    For i = -NumeroRette To NumeroRette
      Y2 = Y1 + 2 * Sin(i * deltALFA)
      X2 = X1 - 2 * Cos((30 / 180 * PG))
      OGGETTO.Line (X1, Y1)-(X2, Y2)
    Next i
    'NOME ASSE X
    stmp = " X "
    OGGETTO.CurrentX = PicW - OGGETTO.TextWidth(stmp)
    OGGETTO.CurrentY = PicH / 2 + OGGETTO.TextHeight(stmp)
    OGGETTO.Print stmp
  End If
  '
  'Indice del punto di separazione
  NPF1 = NpM_F1 - 1
  '
  'Acquisizione profilo ed eventuale rotazione
  If (Dir$(FilePROFILO) <> "") Then
    Open FilePROFILO For Input As #1
      i = 1: Rmax = -1E+38: Rmin = 1E+38
      While Not EOF(1)
        ReDim Preserve R(i):  ReDim Preserve A(i): ReDim Preserve B(i)
        Input #1, R(i), A(i), B(i)
        A(i) = A(i) + ROTAZIONE_AGGIUNTIVA
        If (Rmax < R(i)) Then Rmax = R(i)
        If (Rmin > R(i)) Then Rmin = R(i)
        i = i + 1
      Wend
    Close #1
    Np = i - 1
  Else
    MsgBox FilePROFILO & ": " & Error(53), 48, "WARNING"
    Exit Sub
  End If
  '
  'Calcolo dr
  DR = 1E+308: DR = 1
  For i = 1 To Np - 1
    pX1 = (R(i) * Sin((A(i) + da) * PG / 180))
    pY1 = -(R(i) * Cos((A(i) + da) * PG / 180))
    pX2 = (R(i + 1) * Sin((A(i + 1) + da) * PG / 180))
    pY2 = -(R(i + 1) * Cos((A(i + 1) + da) * PG / 180))
    ftmp = Sqr((pX2 - pX1) ^ 2 + (pY2 - pY1) ^ 2)
    If ftmp < DR Then DR = ftmp: iDR = i
  Next i
  If (DR = 0) And Abs(B(iDR) - B(iDR + 1)) < 0.01 Then
    stmp = "Duplicate point into ROTOR profile."
    MsgBox stmp, vbCritical, "WARNING: point " & Format$(iDR, "####0")
  End If
  '
  'CURVA CON ARCHI DI CERCHIO
  GoSub VISUALIZZA_RAB_CERCHI
  '
  OGGETTO.ForeColor = QBColor(ColoreCurva)
  '
  Select Case QUADRANTE
    Case 1
      'NOME DEL FILE DEL PROFILO E NUMERO DEI PUNTI
      stmp = FilePROFILO
      stmp = stmp & " (Np=F1+F2="
      stmp = stmp & Format$(NPF1, "#######0") & "+" & Format$(Np - NPF1, "#######0")
      stmp = stmp & "=" & Format$(Np, "#######0") & ") "
      OGGETTO.CurrentX = PicW - OGGETTO.TextWidth(stmp)
      OGGETTO.CurrentY = OGGETTO.TextHeight(stmp) * 0
      'OGGETTO.Print stmp
      'DISTANZA MININA TRA I PUNTI DEL PROFILO
      stmp = " DR=" & frmt(DR, 8) & "/" & Format$(iDR, "#######0") & " "
      OGGETTO.CurrentX = PicW - OGGETTO.TextWidth(stmp)
      OGGETTO.CurrentY = OGGETTO.TextHeight(stmp) * 1
      OGGETTO.Print stmp
      'RAGGIO DI CURVATURA MINIMO
      stmp = " RPmin= " & frmt(vRaggioMinimo, 8) & "mm "
      stmp = stmp & "/" & Format(iRaggioMinimo, "#######0") & " "
      OGGETTO.CurrentX = PicW - OGGETTO.TextWidth(stmp)
      OGGETTO.CurrentY = OGGETTO.TextHeight(stmp) * 2
      OGGETTO.Print stmp
    Case 2
      'NOME DEL FILE DEL PROFILO E NUMERO DEI PUNTI
      stmp = " " & FilePROFILO
      stmp = stmp & " (Np=F1+F2="
      stmp = stmp & Format$(NPF1, "#######0") & "+" & Format$(Np - NPF1, "#######0")
      stmp = stmp & "=" & Format$(Np, "#######0") & ")"
      OGGETTO.CurrentX = 0
      OGGETTO.CurrentY = OGGETTO.TextHeight(stmp) * 0
      'OGGETTO.Print stmp
      'DISTANZA MININA TRA I PUNTI DEL PROFILO
      stmp = " DR=" & frmt(DR, 8) & "/" & Format$(iDR, "#######0")
      OGGETTO.CurrentX = 0
      OGGETTO.CurrentY = OGGETTO.TextHeight(stmp) * 1
      OGGETTO.Print stmp
      'RAGGIO DI CURVATURA MINIMO
      stmp = " RPmin= " & frmt(vRaggioMinimo, 8) & "mm "
      stmp = stmp & "/" & Format(iRaggioMinimo, "#######0")
      OGGETTO.CurrentX = 0
      OGGETTO.CurrentY = OGGETTO.TextHeight(stmp) * 2
      OGGETTO.Print stmp
    Case 3
      'NOME DEL FILE DEL PROFILO E NUMERO DEI PUNTI
      stmp = " " & FilePROFILO
      stmp = stmp & " (Np=F1+F2="
      stmp = stmp & Format$(NPF1, "#######0") & "+" & Format$(Np - NPF1, "#######0")
      stmp = stmp & "=" & Format$(Np, "#######0") & ")"
      OGGETTO.CurrentX = 0
      OGGETTO.CurrentY = PicH - OGGETTO.TextHeight(stmp) * 3
      'OGGETTO.Print stmp
      'DISTANZA MININA TRA I PUNTI DEL PROFILO
      stmp = " DR=" & frmt(DR, 8) & "/" & Format$(iDR, "#######0")
      OGGETTO.CurrentX = 0
      OGGETTO.CurrentY = PicH - OGGETTO.TextHeight(stmp) * 2
      OGGETTO.Print stmp
      'RAGGIO DI CURVATURA MINIMO
      stmp = " RPmin= " & frmt(vRaggioMinimo, 8) & "mm "
      stmp = stmp & "/" & Format(iRaggioMinimo, "#######0")
      OGGETTO.CurrentX = 0
      OGGETTO.CurrentY = PicH - OGGETTO.TextHeight(stmp) * 1
      OGGETTO.Print stmp
    Case 4
      'NOME DEL FILE DEL PROFILO E NUMERO DEI PUNTI
      stmp = FilePROFILO
      stmp = stmp & " (Np=F1+F2="
      stmp = stmp & Format$(NPF1, "#######0") & "+" & Format$(Np - NPF1, "#######0")
      stmp = stmp & "=" & Format$(Np, "#######0") & ") "
      OGGETTO.CurrentX = PicW - OGGETTO.TextWidth(stmp)
      OGGETTO.CurrentY = PicH - OGGETTO.TextHeight(stmp) * 3
      'OGGETTO.Print stmp
      'DISTANZA MININA TRA I PUNTI DEL PROFILO
      stmp = " DR=" & frmt(DR, 8) & "/" & Format$(iDR, "#######0") & " "
      OGGETTO.CurrentX = PicW - OGGETTO.TextWidth(stmp)
      OGGETTO.CurrentY = PicH - OGGETTO.TextHeight(stmp) * 2
      OGGETTO.Print stmp
      'RAGGIO DI CURVATURA MINIMO
      stmp = " RPmin= " & frmt(vRaggioMinimo, 8) & "mm "
      stmp = stmp & "/" & Format(iRaggioMinimo, "#######0") & " "
      OGGETTO.CurrentX = PicW - OGGETTO.TextWidth(stmp)
      OGGETTO.CurrentY = PicH - OGGETTO.TextHeight(stmp) * 1
      OGGETTO.Print stmp
  End Select
  OGGETTO.ForeColor = QBColor(0)
  '
  If (SiSTIMA_CERCHIO = 1) Then GoSub STIMA_CERCHIO
  '
  'PUNTI DEL PROFILO ROTORE
  GoSub VISUALIZZA_PUNTI
  '
Exit Sub

VISUALIZZA_RAB_CERCHI:
  '
  'For k = 0 To NDenteLOC - 1
    vRaggioMinimo = 1E+308
    iRaggioMinimo = 1
    da = 0 '(360 / NDenteLOC) * k
    For i = 1 To Np - 2 Step 2
      If (TY_ROT = "AXIAL") Then
        'COORD. X        COORD. Y
        R164 = (A(i + 0) + da) / 360 * PASSO: R165 = R(i + 0)
        R166 = (A(i + 1) + da) / 360 * PASSO: R167 = R(i + 1)
        R168 = (A(i + 2) + da) / 360 * PASSO: R169 = R(i + 2)
      Else
        'COORD. X        COORD. Y
        R164 = R(i + 0) * Sin((A(i + 0) + da) * PG / 180): R165 = R(i + 0) * Cos((A(i + 0) + da) * PG / 180)
        R166 = R(i + 1) * Sin((A(i + 1) + da) * PG / 180): R167 = R(i + 1) * Cos((A(i + 1) + da) * PG / 180)
        R168 = R(i + 2) * Sin((A(i + 2) + da) * PG / 180): R169 = R(i + 2) * Cos((A(i + 2) + da) * PG / 180)
      End If
      '
      'DISTANZA PER TRE PUNTI
      ftmp = Sqr((R165 - R169) * (R165 - R169) + (R164 - R168) * (R164 - R168))
      If ftmp < 0.0001 Then ftmp = 0.0001
      R162 = Abs((R165 - R169) * (R164 - R166) + (R167 - R165) * (R164 - R168))
      R162 = R162 * (1 / ftmp)
      If (R162 < 0.0005) Then
        '
        'TRATTO RETTILINEO
        'G1 Y=R169 X=R168
        pX1 = (R164 - ZeroPezzoX) * ScalaX + PicW / 2
        pY1 = -(R165 - ZeroPezzoY) * ScalaY + PicH / 2
        pX2 = (R168 - ZeroPezzoX) * ScalaX + PicW / 2
        pY2 = -(R169 - ZeroPezzoY) * ScalaY + PicH / 2
        If (pY1 <= OGGETTO.ScaleHeight) And _
            (pX1 <= OGGETTO.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
        End If
        '
      Else
        '
        'TRATTO CURVILINEO
        If R169 = R167 Then R169 = R167 + 0.0001
        If R165 = R167 Then R165 = R167 + 0.0001
        R160 = (R164 - R166) / (R167 - R165)
        R171 = (R166 - R168) / (R169 - R167)
        R172 = (R165 + R167) / 2 - R160 * (R164 + R166) / 2
        R173 = (R169 + R167) / 2 - R171 * (R168 + R166) / 2
        If R160 = R171 Then R160 = R171 + 0.0001
        'coordinate del centro
        R174 = (R173 - R172) / (R160 - R171)
        R170 = R160 * R174 + R172
        'raggio del cerchio
        R161 = Sqr((R174 - R166) * (R174 - R166) + (R170 - R167) * (R170 - R167))
        If R161 <= vRaggioMinimo Then
          vRaggioMinimo = R161
          iRaggioMinimo = i
        End If
        'prodotto misto per senso di rotazione
        R160 = (R166 - R164) * (R169 - R165) - (R168 - R164) * (R167 - R165)
        'Calcolo angoli compresi tra 0 e 2 * Pigreco. --------------------------
        AngStart = CALCOLO_ANGOLO(R164 - R174, R165 - R170)
        AngEnd = CALCOLO_ANGOLO(R168 - R174, R169 - R170)
        '-----------------------------------------------------------------------
        If (R160 > 0) Then
          '
          'G3 Y=R169 X=R168 CR=R161
          If AngEnd >= AngStart Then
            Ang = AngEnd - AngStart
          Else
            Ang = 2 * PG - (AngStart - AngEnd)
          End If
          DeltaAng = Ang / NumeroArchi
          pX1 = R164: pY1 = R165
          For j = 1 To NumeroArchi - 1
            Ang = (AngStart + j * DeltaAng)
            pX2 = R174 + R161 * Cos(Ang): pY2 = R170 + R161 * Sin(Ang)
            'Adattamento nella finestra grafica
            pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
            pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
            pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
            pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
            'Traccio la linea
            If SiStampa = "Y" Then
              If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
                Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
              End If
            Else
              If (pY1 <= OGGETTO.ScaleHeight) And (pX1 <= OGGETTO.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
                OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
              End If
            End If
            'Aggiorno Px1 e Py1
            pX1 = R174 + R161 * Cos(Ang): pY1 = R170 + R161 * Sin(Ang)
          Next j
          pX2 = R168: pY2 = R169
          'Adattamento nella finestra grafica
          pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
          pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
          pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
          pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
          'Traccio la linea
          If SiStampa = "Y" Then
            If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
              Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
            End If
          Else
            If (pY1 <= OGGETTO.ScaleHeight) And (pX1 <= OGGETTO.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
              OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
            End If
          End If
          '
        Else
          '
          'G2 Y=R169 X=R168 CR=R161
          If AngStart >= AngEnd Then
            Ang = AngStart - AngEnd
          Else
            Ang = 2 * PG - (AngEnd - AngStart)
          End If
          DeltaAng = Ang / NumeroArchi
          pX1 = R164: pY1 = R165
          For j = 1 To NumeroArchi - 1
            Ang = (AngStart - j * DeltaAng)
            pX2 = R174 + R161 * Cos(Ang): pY2 = R170 + R161 * Sin(Ang)
            'Adattamento nella finestra grafica
            pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
            pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
            pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
            pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
            'Traccio la linea
            If SiStampa = "Y" Then
              If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
                Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
              End If
            Else
              If (pY1 <= OGGETTO.ScaleHeight) And (pX1 <= OGGETTO.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
                OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
              End If
            End If
            'Aggiorno Px1 e Py1
            pX1 = R174 + R161 * Cos(Ang): pY1 = R170 + R161 * Sin(Ang)
          Next j
          pX2 = R168: pY2 = R169
          'Adattamento nella finestra grafica
          pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
          pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
          pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
          pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
          'Traccio la linea
          If SiStampa = "Y" Then
            If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
              Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
            End If
          Else
            If (pY1 <= OGGETTO.ScaleHeight) And (pX1 <= OGGETTO.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
              OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
            End If
          End If
          '
        End If
        '
      End If
      '
    Next i
  'Next k
Return
     
VISUALIZZA_PUNTI:
    DR = EQUIDISTANZA * ScalaERRORE
    j = 0
      da = 0
        For i = 1 To NPF1
          'LINEA NORMALE
          If (TY_ROT = "AXIAL") Then
            Ang = Atn(Tan(PG * B(i) / 180) * PASSO / (2 * R(i) * PG))
            pX1 = A(i) / 360 * PASSO - ZeroPezzoX
            pY1 = R(i) - ZeroPezzoY
          Else
            Ang = (A(i) + B(i)) * PG / 180
            pX1 = R(i) * Sin((A(i) + da) * PG / 180) - ZeroPezzoX
            pY1 = R(i) * Cos((A(i) + da) * PG / 180) - ZeroPezzoY
          End If
          pX2 = pX1
          pY2 = pY1
          pX1 = pX1 * ScalaX + PicW / 2
          pY1 = -pY1 * ScalaY + PicH / 2
          pX2 = (pX2 + DR * Sin(Ang - PG - PG / 2)) * ScalaX + PicW / 2
          pY2 = -(pY2 + DR * Cos(Ang - PG - PG / 2)) * ScalaY + PicH / 2
          If SiNORMALI = 1 Then
            If SiStampa = "Y" Then
              If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
                Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
              End If
            Else
              If (pY1 <= OGGETTO.ScaleHeight) And (pX1 <= OGGETTO.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
                OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
              End If
            End If
          End If
          'indice del punto
          stmp = " " & Format$(i, "##0") & " "
          If SiINDICI = 1 Then
            If SiStampa = "Y" Then
              pX2 = pX2 - Printer.TextWidth(stmp)
              pY2 = pY2 - Printer.TextHeight(stmp)
              If (pY2 <= Printer.ScaleHeight) And (pX2 <= Printer.ScaleWidth) And (pX2 >= 0) And (pY2 >= 0) Then
                Printer.CurrentX = pX2
                Printer.CurrentY = pY2
                Printer.Print stmp
              End If
            Else
              pX2 = pX2 - OGGETTO.TextWidth(stmp)
              pY2 = pY2 - OGGETTO.TextHeight(stmp)
              If (pY2 <= OGGETTO.ScaleHeight) And (pX2 <= OGGETTO.ScaleWidth) And (pX2 >= 0) And (pY2 >= 0) Then
                OGGETTO.CurrentX = pX2
                OGGETTO.CurrentY = pY2
                OGGETTO.Print stmp
              End If
            End If
          End If
        Next i
        For i = NPF1 + 1 To Np
          'LINEA NORMALE
          If (TY_ROT = "AXIAL") Then
            Ang = Atn(Tan(PG * B(i) / 180) * PASSO / (2 * R(i) * PG))
            pX1 = A(i) / 360 * PASSO - ZeroPezzoX
            pY1 = R(i) - ZeroPezzoY
          Else
            Ang = (A(i) + B(i)) * PG / 180
            pX1 = R(i) * Sin((A(i) + da) * PG / 180) - ZeroPezzoX
            pY1 = R(i) * Cos((A(i) + da) * PG / 180) - ZeroPezzoY
          End If
          pX2 = pX1
          pY2 = pY1
          pX1 = pX1 * ScalaX + PicW / 2
          pY1 = -pY1 * ScalaY + PicH / 2
          pX2 = (pX2 - DR * Sin(Ang - PG - PG / 2)) * ScalaX + PicW / 2
          pY2 = -(pY2 - DR * Cos(Ang - PG - PG / 2)) * ScalaY + PicH / 2
          If SiNORMALI = 1 Then
            If SiStampa = "Y" Then
              If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
                Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
              End If
            Else
              If (pY1 <= OGGETTO.ScaleHeight) And (pX1 <= OGGETTO.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
                OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
              End If
            End If
          End If
          'indice del punto
          stmp = " " & Format$(i, "##0") & " "
          If SiINDICI = 1 Then
            If SiStampa = "Y" Then
              If (pY2 <= Printer.ScaleHeight) And (pX2 <= Printer.ScaleWidth) And (pX2 >= 0) And (pY2 >= 0) Then
                Printer.CurrentX = pX2
                Printer.CurrentY = pY2
                Printer.Print stmp
              End If
            Else
              If (pY2 <= OGGETTO.ScaleHeight) And (pX2 <= OGGETTO.ScaleWidth) And (pX2 >= 0) And (pY2 >= 0) Then
                OGGETTO.CurrentX = pX2
                OGGETTO.CurrentY = pY2
                OGGETTO.Print stmp
              End If
            End If
          End If
        Next i
Return
     
SCRIVI_DIAMETRO:
  'Testo da stampare
  stmp = " " & frmt(2 * Raggio, 4) & "mm "
  'Linea sopra il del testo
  If SiStampa = "Y" Then
    pX1 = (0 - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(Raggio - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = (0 - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(0 - ZeroPezzoY) * ScalaY + PicH / 2 - Printer.TextHeight(stmp) / 2
    If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
      Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
    End If
  Else
    pX1 = (0 - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(Raggio - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = (0 - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(0 - ZeroPezzoY) * ScalaY + PicH / 2 - OGGETTO.TextHeight(stmp) / 2
    If (pY1 <= OGGETTO.ScaleHeight) And (pX1 <= OGGETTO.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
      OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
    End If
  End If
  'Freccia sopra il testo
  Ang = AF / 180 * PG
  DeltaAng = Ang / NumeroRette
  For i = -NumeroRette To NumeroRette
    pX2 = (0 - ZeroPezzoX) * ScalaX + DF * Sin(i * DeltaAng) + PicW / 2
    pY2 = -(Raggio - ZeroPezzoY) * ScalaY + DF * Cos(Ang) + PicH / 2
    If SiStampa = "Y" Then
      If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
        Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
      End If
    Else
      If (pY1 <= OGGETTO.ScaleHeight) And (pX1 <= OGGETTO.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
        OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
      End If
    End If
  Next i
  'Linea di riferimento per la freccia sopra al testo
  pX1 = (0 - ZeroPezzoX) * ScalaX - DF + PicW / 2
  pY1 = -(Raggio - ZeroPezzoY) * ScalaY + PicH / 2
  pX2 = (0 - ZeroPezzoX) * ScalaX + DF + PicW / 2
  pY2 = -(Raggio - ZeroPezzoY) * ScalaY + PicH / 2
  If SiStampa = "Y" Then
    If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
      Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
    End If
  Else
    If (pY1 <= OGGETTO.ScaleHeight) And (pX1 <= OGGETTO.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
      OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
    End If
  End If
  'Scrittura del testo
  If SiStampa = "Y" Then
    pX1 = (0 - ZeroPezzoX) * ScalaX + PicW / 2 - Printer.TextWidth(stmp) / 2
    pY1 = -(0 - ZeroPezzoY) * ScalaY + PicH / 2 - Printer.TextHeight(stmp) / 2
    If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
      Printer.CurrentX = pX1
      Printer.CurrentY = pY1
      Printer.Print stmp
    End If
  Else
    pX1 = (0 - ZeroPezzoX) * ScalaX + PicW / 2 - OGGETTO.TextWidth(stmp) / 2
    pY1 = -(0 - ZeroPezzoY) * ScalaY + PicH / 2 - OGGETTO.TextHeight(stmp) / 2
    If (pY1 <= OGGETTO.ScaleHeight) And (pX1 <= OGGETTO.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
      OGGETTO.CurrentX = pX1
      OGGETTO.CurrentY = pY1
      OGGETTO.Print stmp
    End If
  End If
  'Linea sotto il  testo
  If SiStampa = "Y" Then
    pX1 = (0 - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(0 - ZeroPezzoY) * ScalaY + PicH / 2 + Printer.TextHeight(stmp) / 2
    pX2 = (0 - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(-Raggio - ZeroPezzoY) * ScalaY + PicH / 2
    If (pY2 <= Printer.ScaleHeight) And (pX2 <= Printer.ScaleWidth) And (pX2 >= 0) And (pY2 >= 0) Then
      Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
    End If
  Else
    pX1 = (0 - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(0 - ZeroPezzoY) * ScalaY + PicH / 2 + OGGETTO.TextHeight(stmp) / 2
    pX2 = (0 - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(-Raggio - ZeroPezzoY) * ScalaY + PicH / 2
    If (pY2 <= OGGETTO.ScaleHeight) And (pX2 <= OGGETTO.ScaleWidth) And (pX2 >= 0) And (pY2 >= 0) Then
      OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
    End If
  End If
  'Freccia sotto il testo
  Ang = AF / 180 * PG
  DeltaAng = Ang / NumeroRette
  For i = -NumeroRette To NumeroRette
    pX1 = (0 - ZeroPezzoX) * ScalaX + DF * Sin(i * DeltaAng) + PicW / 2
    pY1 = -(-Raggio - ZeroPezzoY) * ScalaY - DF * Cos(Ang) + PicH / 2
    If SiStampa = "Y" Then
      If (pY2 <= Printer.ScaleHeight) And (pX2 <= Printer.ScaleWidth) And (pX2 >= 0) And (pY2 >= 0) Then
        Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
      End If
    Else
      If (pY2 <= OGGETTO.ScaleHeight) And (pX2 <= OGGETTO.ScaleWidth) And (pX2 >= 0) And (pY2 >= 0) Then
        OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
      End If
    End If
  Next i
  'Linea di riferimento per la freccia sotto il testo
  pX1 = (0 - ZeroPezzoX) * ScalaX - DF + PicW / 2
  pY1 = -(-Raggio - ZeroPezzoY) * ScalaY + PicH / 2
  pX2 = (0 - ZeroPezzoX) * ScalaX + DF + PicW / 2
  pY2 = -(-Raggio - ZeroPezzoY) * ScalaY + PicH / 2
  If SiStampa = "Y" Then
    If (pY2 <= Printer.ScaleHeight) And (pX2 <= Printer.ScaleWidth) And (pX2 >= 0) And (pY2 >= 0) Then
      Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
    End If
  Else
    If (pY2 <= OGGETTO.ScaleHeight) And (pX2 <= OGGETTO.ScaleWidth) And (pX2 >= 0) And (pY2 >= 0) Then
      OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
    End If
  End If
Return
     
DISEGNA_DIAMETRO:
    da = 1
    For i = 1 To 360
      pX1 = (Raggio * Sin(i * da * PG / 180) - ZeroPezzoX) * ScalaX + PicW / 2
      pY1 = -(Raggio * Cos(i * da * PG / 180) - ZeroPezzoY) * ScalaY + PicH / 2
      pX2 = (Raggio * Sin((i + 1) * da * PG / 180) - ZeroPezzoX) * ScalaX + PicW / 2
      pY2 = -(Raggio * Cos((i + 1) * da * PG / 180) - ZeroPezzoY) * ScalaY + PicH / 2
      If SiStampa = "Y" Then
        If (pY2 <= Printer.ScaleHeight) And (pX2 <= Printer.ScaleWidth) And (pX2 >= 0) And (pY2 >= 0) Then
          Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
        End If
      Else
        If (pY2 <= OGGETTO.ScaleHeight) And (pX2 <= OGGETTO.ScaleWidth) And (pX2 >= 0) And (pY2 >= 0) Then
          OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
        End If
      End If
    Next i
Return

STIMA_CERCHIO:
Dim Npunti As Long
'
Dim AA As Double
Dim BB As Double
Dim cc As Double
Dim DD As Double
Dim EE As Double
'
Dim Sx   As Double
Dim Sy   As Double
Dim Sxy  As Double
Dim Sxy2 As Double
Dim Sx2y As Double
Dim Sx2  As Double
Dim Sy2  As Double
Dim Sx3  As Double
Dim Sy3  As Double
'
Dim AM As Double
Dim BM As Double
Dim RM As Double
'
Dim XXX() As Double
Dim YYY() As Double
  '
  'NomeForm.pntSCROLL.Visible = True
  'NomeForm.txtINIZIO.Visible = True
  'NomeForm.txtFINE.Visible = True
  '
  Dim INIZIO As Long
  Dim FINE   As Long
  '
  INIZIO = 0 'val(NomeForm.txtINIZIO.Text)
  FINE = 0 'val(NomeForm.txtFINE.Text)
  '
  If ((INIZIO <> 0) Or (FINE <> 0)) And (SiINDICI = 1) Then
  '
  If (INIZIO < 0) Then INIZIO = 1
  If (INIZIO >= Np) Then INIZIO = Np - 2
  If (FINE - INIZIO < 2) Then FINE = INIZIO + 2
  If (FINE > Np) Then FINE = Np: INIZIO = Np - 2
  '
  'NomeForm.txtINIZIO.Text = INIZIO
  'NomeForm.txtFINE.Text = FINE
  '
  Npunti = 1
  For i = INIZIO To FINE
    ReDim Preserve XXX(Npunti)
    ReDim Preserve YYY(Npunti)
    If (TY_ROT = "AXIAL") Then
      'COORD. X
      XXX(Npunti) = A(i) / 360 * PASSO
      'COORD. Y
      YYY(Npunti) = R(i)
    Else
      'COORD. X
      XXX(Npunti) = R(i) * Sin(A(i) * PG / 180)
      'COORD. Y
      YYY(Npunti) = R(i) * Cos(A(i) * PG / 180)
    End If
    Npunti = Npunti + 1
  Next i
  Npunti = Npunti - 1
  '
  'SOMMA X
  Sx = 0:   For i = 1 To Npunti: Sx = Sx + XXX(i):                       Next i
  'SOMMA Y
  Sy = 0:   For i = 1 To Npunti: Sy = Sy + YYY(i):                       Next i
  'SOMMA X Y
  Sxy = 0:  For i = 1 To Npunti: Sxy = Sxy + XXX(i) * YYY(i):            Next i
  'SOMMA X X
  Sx2 = 0:  For i = 1 To Npunti: Sx2 = Sx2 + XXX(i) * XXX(i):            Next i
  'SOMMA X X X
  Sx3 = 0:  For i = 1 To Npunti: Sx3 = Sx3 + XXX(i) * XXX(i) * XXX(i):   Next i
  'SOMMA X X Y
  Sx2y = 0: For i = 1 To Npunti: Sx2y = Sx2y + XXX(i) * XXX(i) * YYY(i): Next i
  'SOMMA X Y Y
  Sxy2 = 0: For i = 1 To Npunti: Sxy2 = Sxy2 + XXX(i) * YYY(i) * YYY(i): Next i
  'SOMMA Y Y
  Sy2 = 0:  For i = 1 To Npunti: Sy2 = Sy2 + YYY(i) * YYY(i):            Next i
  'SOMMA Y Y Y
  Sy3 = 0:  For i = 1 To Npunti: Sy3 = Sy3 + YYY(i) * YYY(i) * YYY(i):   Next i
  'CALCOLO A
  AA = Npunti * Sx2 - Sx ^ 2
  'CALCOLO B
  BB = Npunti * Sxy - Sx * Sy
  'CALCOLO C
  cc = Npunti * Sy2 - Sy ^ 2
  'CALCOLO D
  DD = 0.5 * (Npunti * Sxy2 - Sx * Sy2 + Npunti * Sx3 - Sx * Sx2)
  'CALCOLO E
  EE = 0.5 * (Npunti * Sx2y - Sy * Sx2 + Npunti * Sy3 - Sy * Sy2)
  'CALCOLO aM
  AM = (DD * cc - BB * EE) / (AA * cc - BB * BB)
  'CALCOLO bM
  BM = (AA * EE - BB * DD) / (AA * cc - BB * BB)
  'CALCOLO rM
  RM = 0
  For i = 1 To Npunti
    RM = RM + Sqr((XXX(i) - AM) ^ 2 + (YYY(i) - BM) ^ 2)
  Next i
  RM = RM / Npunti
  ColoreCurva = 12
  GoSub DISEGNA_DIAMETRO2
  '
  End If

Return

DISEGNA_DIAMETRO2:
  da = 1
  For i = 1 To 360
    pX1 = (RM * Sin(i * da * PG / 180) + AM - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(RM * Cos(i * da * PG / 180) + BM - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = (RM * Sin((i + 1) * da * PG / 180) + AM - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(RM * Cos((i + 1) * da * PG / 180) + BM - ZeroPezzoY) * ScalaY + PicH / 2
    If (SiStampa = "Y") Then
      If (pY2 <= Printer.ScaleHeight) And (pX2 <= Printer.ScaleWidth) And (pX2 >= 0) And (pY2 >= 0) Then
        Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
      End If
    Else
      If (pY2 <= OGGETTO.ScaleHeight) And (pX2 <= OGGETTO.ScaleWidth) And (pX2 >= 0) And (pY2 >= 0) Then
        OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
      End If
    End If
  Next i
  'Testo da stampare
  stmp = " " & frmt(RM, 4) & "mm "
  If (SiStampa = "Y") Then
    pX1 = (AM - ZeroPezzoX) * ScalaX + PicW / 2 - Printer.TextWidth(stmp) / 2
    pY1 = -(BM - ZeroPezzoY) * ScalaY + PicH / 2 - Printer.TextHeight(stmp) / 2
    If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
      Printer.CurrentX = pX1
      Printer.CurrentY = pY1
      Printer.Print stmp
    End If
  Else
    pX1 = (AM - ZeroPezzoX) * ScalaX + PicW / 2 - OGGETTO.TextWidth(stmp) / 2
    pY1 = -(BM - ZeroPezzoY) * ScalaY + PicH / 2 - OGGETTO.TextHeight(stmp) / 2
    If (pY1 <= OGGETTO.ScaleHeight) And (pX1 <= OGGETTO.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
      OGGETTO.CurrentX = pX1
      OGGETTO.CurrentY = pY1
      OGGETTO.Print stmp
    End If
  End If

Return

errVERIFICA_PROFILO:
  If FreeFile > 0 Then Close
  iERR = Err
  StopRegieEvents
  MsgBox "ERROR: " & Error(iERR), 48, "SUB: VERIFICA_PROFILO"
  ResumeRegieEvents
  'Resume Next
  Exit Sub
     
End Sub

Sub CALCOLO_ROTORE_DA_MOLA(ByVal ProfiloMola As String, ByVal CorrezioniMola As String, ByVal ProfiloRotore As String, ByVal SpostaY As Double, ByVal SpostaX As Double, ByVal CorS As Double, ByVal EquidistanzaLOC As Double, ByVal CorElica As Double, ByVal CorANGOLO As Double, ByVal RADICE As String, ByVal NPM1 As Long, ByVal DM_ROT As String, ByVal PASSO As Single, ELICA As Single)
'
Dim Raggio As Double
Dim ANGOLO As Double
Dim B()   As Single
Dim NoUse
'
Dim i     As Integer
Dim j     As Integer
Dim k     As Integer
'
Dim ENTRX As Double
Dim INF   As Double
Dim pnt   As Integer
Dim EXV   As Double
Dim EXF   As Double
Dim RXF   As Double
Dim APFD  As Double
Dim APF   As Double
Dim Rayon As Double
Dim ALFA  As Double
Dim Beta  As Double
Dim BetaG As Double
'
Dim stmp As String
'
Dim SENS As Integer
'
Dim v7  As Double
Dim V12 As Double
Dim V13 As Double
Dim V14 As Double
Dim V15 As Double
Dim V16 As Double
Dim VPB As Double
Dim RXV As Double
Dim Ang As Double
Dim ang1 As Double
'
Dim YAPP As Double
Dim XAPP As Double
'
Dim Y1 As Double
Dim X1 As Double
'
Dim XX() As Double
Dim YY() As Double
Dim TT() As Double
'
Dim tMIN  As Double
Dim jMIN  As Integer
Dim Np    As Integer
'
Dim CorX As Double
Dim corY As Double
'
Dim NPF1 As Integer
Dim RagINT As Single
'
On Error GoTo errCALCOLO_ROTORE_DA_MOLA
  '
  NPF1 = NPM1
  '
  'LETTURA PROFILO MOLA
  If (Dir$(ProfiloMola) <> "") Then
    tMIN = 1E+308: jMIN = 0
    Open ProfiloMola For Input As #1
      i = 2
      While Not EOF(1)
        ReDim Preserve XX(i): ReDim Preserve YY(i): ReDim Preserve TT(i)
        Input #1, XX(i), YY(i), TT(i)
        If tMIN > Abs(TT(i)) Then tMIN = Abs(TT(i)): jMIN = i
        i = i + 1
      Wend
    Close #1
    'PRIMO PUNTO
    XX(1) = XX(2): YY(1) = YY(2): TT(1) = TT(2)
    Np = i - 1
    Np = Np + 1
    ReDim Preserve XX(Np): ReDim Preserve YY(Np): ReDim Preserve TT(Np)
    'ULTIMO PUNTO
    XX(Np) = XX(Np - 1): YY(Np) = YY(Np - 1): TT(Np) = TT(Np - 1)
  Else
    MsgBox ProfiloMola & ": " & Error(53), 48, "WARNING"
    Exit Sub
  End If
  '
  'CALCOLO PROFILO EQUIDISTANTE
  'Fianco 1
  For i = 1 To NPF1
    Ang = Abs(TT(i)) * PG / 180
    XX(i) = XX(i) - EquidistanzaLOC * Cos(Ang)
    YY(i) = YY(i) + EquidistanzaLOC * Sin(Ang)
  Next i
  'Fianco 2
  For i = NPF1 + 1 To Np
    Ang = Abs(TT(i)) * PG / 180
    XX(i) = XX(i) + EquidistanzaLOC * Cos(Ang)
    YY(i) = YY(i) + EquidistanzaLOC * Sin(Ang)
  Next i
  '
  'CORREZIONE DI SPESSORE
  'Fianco 1
  For i = 1 To NPF1
    XX(i) = XX(i) - CorS / 2
  Next i
  'Fianco 2
  For i = NPF1 + 1 To Np
    XX(i) = XX(i) + CorS / 2
  Next i
  '
  'Leggo il primo ed ultimo punto della mola
  If Dir$(RADICE & "\D" & Format$(val(DM_ROT), "##0")) <> "" Then
    Open RADICE & "\D" & Format$(val(DM_ROT), "##0") For Input As #1
      Line Input #1, stmp
      'ricavo il primo punto
      stmp = Trim$(stmp)
      i = InStr(2, stmp, "+")
      If (i <= 0) Then i = InStr(2, stmp, "-")
      XX(1) = val(Left$(stmp, i - 1))
      YY(1) = val(Right$(stmp, Len(stmp) - i))
      While Not EOF(1)
        Line Input #1, stmp
      Wend
      'ricavo il l'ultimo punto
      stmp = Trim$(stmp)
      i = InStr(2, stmp, "+")
      If (i <= 0) Then i = InStr(2, stmp, "-")
      XX(Np) = val(Left$(stmp, i - 1))
      YY(Np) = val(Right$(stmp, Len(stmp) - i))
    Close #1
  Else
    MsgBox RADICE & "\D" & Format$(val(DM_ROT), "##0") & ": " & Error(53), 48, "WARNING"
  End If
  '
  'Correzioni profilo mola
  If (Len(CorrezioniMola) > 0) Then
    If (Dir$(RADICE & "\" & CorrezioniMola) <> "") Then
      Open RADICE & "\" & CorrezioniMola For Input As #1
        i = 1
        While Not EOF(1) And (i < (Np + 1))
          Line Input #1, stmp
          j = 0
          If (j <= 0) Then j = InStr(2, stmp, "+")
          If (j <= 0) Then j = InStr(2, stmp, "-")
          CorX = val(Left$(stmp, j - 1))
          corY = val(Mid$(stmp, j))
          If (i <= NPF1) Then
            'Correzioni sul fianco 1
            XX(i) = XX(i) + CorX
            YY(i) = YY(i) - corY
          Else
            'Correzioni sul fianco 2
            XX(i) = XX(i) - CorX
            YY(i) = YY(i) - corY
          End If
          i = i + 1
        Wend
      Close #1
    Else
      MsgBox RADICE & "\" & CorrezioniMola & ": " & Error(53), 48, "WARNING"
    End If
  End If
  '
  ReDim B(Np + 1)
  RagINT = 999999999
  Open RADICE & "\PRFROT" For Input As #1
    i = 1
    While Not EOF(1)
      i = i + 1
      Input #1, Raggio, NoUse, B(i)
      If (Raggio <= RagINT) Then RagINT = Raggio
    Wend
  Close #1
  '
  'CORREZIONE DI INCLINAZIONE RADIALE: ERRATO CAMBIANO ANCHE XX E TT
  For pnt = 2 To Np - 1
    Raggio = Sqr(XX(pnt) ^ 2 + YY(pnt) ^ 2)
    ANGOLO = Atn(XX(pnt) / YY(pnt))
    ANGOLO = ANGOLO + CorANGOLO * PG / 180
    YY(pnt) = Raggio * Cos(ANGOLO)
    XX(pnt) = Raggio * Sin(ANGOLO)
    TT(pnt) = TT(pnt) + CorANGOLO
  Next pnt
  '
  'INTERASSE MOLA PEZZO: CONSIDERO LO SPOSTAMENTO RADIALE
  ENTRX = SpostaY + val(DM_ROT) / 2 + RagINT
  'ANGOLO ELICA
  INF = (90 - (Abs(ELICA) + CorElica)) * PG / 180
  '
  Open ProfiloRotore For Output As #1
    For pnt = 2 To Np - 1
      'COORDINATE MOLA X Y T
      EXF = XX(pnt): RXF = YY(pnt): APFD = TT(pnt)
      'SPOSTAMENTO ASSE MOLA
      EXF = EXF + SpostaX
      If (APFD < 0) Then APF = (180 + APFD) * PG / 180 Else APF = APFD * PG / 180
      GoSub 1920
      BetaG = Beta * 180 / PG
      If (BetaG >= 90) Then BetaG = BetaG - 180
      If (BetaG < -90) Then BetaG = 180 + BetaG
      BetaG = B(pnt)
      stmp = ""
      stmp = stmp & " " & Left$(frmt(Rayon, 4) & String(15, " "), 15)
      stmp = stmp & " " & Left$(frmt(ALFA * 180 / PG, 4) & String(15, " "), 15)
      stmp = stmp & " " & Left$(frmt(BetaG, 4) & String(15, " "), 15)
      Print #1, stmp
    Next pnt
  Close #1
     
Exit Sub

1920 ' ************** S/P ************ CALCUL PIECE D'APRES FRAISE **********
     ' Entrees  constantes : ENTRX, PAS,  INF
     ' Entrees  variables  : RXF,   EXF,  APF
     ' Sorties             : RAYON, ALFA, BETA
     ' **********************************************************************
     SENS = 1: If EXF < 0 Then SENS = -1
     EXF = EXF * SENS
     APF = APF * SENS
     v7 = PASSO / 2 / PG
     GoSub 2050
     Rayon = RXV
     ALFA = Ang * SENS
     ang1 = Ang
     RXF = RXF + 0.01
     EXF = EXF - 0.01 * Tan(APF)
     GoSub 2050
     Y1 = YAPP: X1 = XAPP
     RXF = RXF - 0.02
     EXF = EXF + 0.02 * Tan(APF)
     GoSub 2050
     RXF = RXF + 0.01
     EXF = EXF - 0.01 * Tan(APF)
     If YAPP = Y1 Then YAPP = YAPP + 0.00001
     Beta = (Atn((X1 - XAPP) / (YAPP - Y1)) - ang1) * SENS
     If X1 = XAPP Then Beta = PG / 2
     APF = APF * SENS
     EXF = EXF * SENS
Return   '              ********* RETOUR AUX DONNEES

2050 V12 = -Tan(APF) * (ENTRX * Sin(INF) + v7 * Cos(INF))
     V13 = v7 * Sin(INF) - ENTRX * Cos(INF)
     V14 = Cos(INF) * ((-Tan(APF) * EXF) + RXF)
     V15 = (V14 ^ 2) + (V12 ^ 2) - (V13 ^ 2)
     If V15 > 0 Then
        V15 = Sqr(V15)
        V16 = -(V15 + V12) / (V13 - V14)
        V15 = (V15 - V12) / (V13 - V14)
        VPB = V16: If Abs(V15) < Abs(V16) Then VPB = V15
        V12 = 2 * Atn(VPB)
        V13 = (RXF * Sin(INF) * Sin(V12)) + EXF * Cos(INF)
        V13 = Atn(V13 / ((RXF * Cos(V12)) - ENTRX))
        RXV = (ENTRX - RXF * Cos(V12)) / Cos(V13)
        EXV = EXF * Sin(INF) - v7 * V13 - RXF * Sin(V12) * Cos(INF)
        Ang = 2 * PG * EXV / PASSO
        YAPP = RXV * Cos(Ang)
        XAPP = -RXV * Sin(Ang)
     End If
Return
  
errCALCOLO_ROTORE_DA_MOLA:
  If FreeFile > 0 Then Close
  MsgBox Error$(Err), 48, "SUB: CALCOLO_ROTORE_DA_MOLA"
Exit Sub

End Sub

Sub MODIFICA_FASCIA_DI_TOLLERANZA_ROTORI()
'
Dim stmp        As String
Dim i           As Integer
Dim FASCIA      As Single
Dim LIMITE_SUP  As Single
Dim LIMITE_INF  As Single
'
On Error GoTo errMODIFICA_FASCIA_DI_TOLLERANZA_ROTORI
  '
  Dim COORDINATE         As String
  Dim RADICE             As String
  Dim PERCORSO           As String
  Dim PERCORSO_CONTROLLI As String
  Dim PERCORSO_PROFILI   As String
  Dim sequenza           As String
  Dim npt                As Integer
  Dim TY_ROT             As String
  Dim PASSO              As Double
  Dim CONTROLLORE        As String
  Dim DM_ROT             As String
  Dim INVERSIONE         As Integer
  '
  Call LEGGI_INFO_ROTORI(PERCORSO, COORDINATE, RADICE, PERCORSO_CONTROLLI, PERCORSO_PROFILI, sequenza, npt, TY_ROT, PASSO, CONTROLLORE, DM_ROT, INVERSIONE)
  '
  LIMITE_SUP = 0.01
  FASCIA = 0.02
  StopRegieEvents
  stmp = InputBox("TOLLERANCE EDITING", "INPUT: PRFTOL.DAT", frmt(FASCIA, 4))
  ResumeRegieEvents
  FASCIA = val(stmp)
  If (FASCIA <= 0) Then
    StopRegieEvents
    stmp = InputBox("SUP", "INPUT: PRFTOL.DAT", frmt(LIMITE_SUP, 4))
    ResumeRegieEvents
    LIMITE_SUP = val(stmp)
    If (LIMITE_SUP <= 0) Then
      WRITE_DIALOG "Operation aborted by user!!"
    Else
      LIMITE_INF = LIMITE_SUP
      StopRegieEvents
      stmp = InputBox("INF", "INPUT: PRFTOL.DAT", frmt(LIMITE_INF, 4))
      ResumeRegieEvents
      LIMITE_INF = val(stmp)
      If (LIMITE_INF <= 0) Then
        WRITE_DIALOG "Operation aborted by user!!"
      Else
        'CREAZIONE DEL FILE PRFTOL.DAT: ASIMMETRICO
        Open RADICE & "\PRFTOL.DAT" For Output As #1
        For i = 1 To npt
          Print #1, "+" & frmt0(LIMITE_INF, 4) & " +" & frmt0(LIMITE_SUP, 4)
        Next i
        Close #1
        Call VISUALIZZA_CONTROLLO
        WRITE_DIALOG "PRFTOL.DAT UPDATED!!"
      End If
    End If
  Else
    'CREAZIONE DEL FILE PRFTOL.DAT: SIMMETRICO
    Open RADICE & "\PRFTOL.DAT" For Output As #1
    For i = 1 To npt
      Print #1, "+" & frmt0(FASCIA / 2, 4) & " +" & frmt0(FASCIA / 2, 4)
    Next i
    Close #1
    Call VISUALIZZA_CONTROLLO
    WRITE_DIALOG "PRFTOL.DAT UPDATED!!"
  End If
  Exit Sub
  '
errMODIFICA_FASCIA_DI_TOLLERANZA_ROTORI:
  If (FreeFile > 0) Then Close
  MsgBox "ERROR: " & Error(Err), 48, "SUB: MODIFICA_FASCIA_DI_TOLLERANZA_ROTORI"
  Exit Sub
  '
End Sub

Sub FASCIA_DI_TOLLERANZA_VITI()
'
Dim stmp        As String
Dim i           As Integer
Dim FASCIA      As Single
Dim LIMITE_SUP  As Single
Dim LIMITE_INF  As Single
Dim NpunF1      As Integer
Dim riga        As String
Dim LIMINT_SUP  As Single
Dim LIMINT_INF  As Single
Dim InizF2      As Integer
Dim FineF2      As Integer
'
On Error GoTo errFASCIA_DI_TOLLERANZA_VITI
  '
  Dim COORDINATE         As String
  Dim RADICE             As String
  Dim PERCORSO           As String
  Dim PERCORSO_CONTROLLI As String
  Dim PERCORSO_PROFILI   As String
  Dim sequenza           As String
  Dim npt                As Integer
  Dim TY_ROT             As String
  Dim PASSO              As Double
  Dim CONTROLLORE        As String
  Dim DM_ROT             As String
  Dim INVERSIONE         As Integer
  '
  Call LEGGI_INFO_ROTORI(PERCORSO, COORDINATE, RADICE, PERCORSO_CONTROLLI, PERCORSO_PROFILI, sequenza, npt, TY_ROT, PASSO, CONTROLLORE, DM_ROT, INVERSIONE)
  '
  LIMITE_SUP = 0.01
  StopRegieEvents
  stmp = InputBox("MAX", "Flanc Tollerance", frmt(LIMITE_SUP, 4))
  ResumeRegieEvents
  LIMITE_SUP = val(stmp)
  If (LIMITE_SUP = 0) Then LIMITE_SUP = 0.0001
  If (LIMITE_SUP < 0) Then
    WRITE_DIALOG "Operation aborted by user!!"
  Else
    LIMITE_INF = LIMITE_SUP
    StopRegieEvents
    stmp = InputBox("MIN", "Flanc Tollerance", frmt(LIMITE_INF, 4))
    ResumeRegieEvents
    LIMITE_INF = val(stmp)
    If (LIMITE_INF = 0) Then LIMITE_INF = 0.0001
    If (LIMITE_INF < 0) Then
      WRITE_DIALOG "Operation aborted by user!!"
    Else
      'Tolleranze Raggio interno
      LIMINT_SUP = 0.01
      StopRegieEvents
      stmp = InputBox("MAX", "Root Tollerance", frmt(LIMINT_SUP, 4))
      ResumeRegieEvents
      LIMINT_SUP = val(stmp)
      If (LIMINT_SUP = 0) Then LIMINT_SUP = 0.0001
      If (LIMINT_SUP < 0) Then
         WRITE_DIALOG "Operation aborted by user!!"
         Exit Sub
      End If
      LIMINT_INF = 0.01
      stmp = InputBox("MIN", "Root Tollerance", frmt(LIMINT_INF, 4))
      LIMINT_INF = val(stmp)
      If (LIMINT_INF = 0) Then LIMINT_INF = 0.0001
      If (LIMINT_INF < 0) Then
         WRITE_DIALOG "Operation aborted by user!!"
         Exit Sub
      End If
      Open RADICE & "\ROTOR.DAT" For Input As #1
           Line Input #1, riga
           Line Input #1, riga
           Line Input #1, riga
           Line Input #1, riga
           Line Input #1, riga
           Line Input #1, riga
           Line Input #1, riga: NpunF1 = val(riga) - 1
      Close #1
      InizF2 = npt - NpunF1 + 2
      FineF2 = npt
      'CREAZIONE DEL FILE PRFTOL.DAT: ASIMMETRICO PER FIANCHI e Raggio Interno
      Open RADICE & "\PRFTOL.DAT" For Output As #1
      'Fianco 1
      For i = 1 To NpunF1 - 1
        Print #1, "+" & frmt0(LIMITE_INF, 4) & " +" & frmt0(LIMITE_SUP, 4)
      Next i
      'Fondo
      For i = NpunF1 To InizF2 - 1
        Print #1, "+" & frmt0(LIMINT_INF, 4) & " +" & frmt0(LIMINT_SUP, 4)
      Next i
      'Fianco 2
      For i = InizF2 To FineF2
        Print #1, "+" & frmt0(LIMITE_INF, 4) & " +" & frmt0(LIMITE_SUP, 4)
      Next i
      Close #1
      Call VISUALIZZA_CONTROLLO
      WRITE_DIALOG "PRFTOL.DAT UPDATED!!"
    End If
  End If
  Exit Sub
  '
errFASCIA_DI_TOLLERANZA_VITI:
  If FreeFile > 0 Then Close
  MsgBox "ERROR: " & Error(Err), 48, "SUB: FASCIA_DI_TOLLERANZA_VITI"
  Exit Sub
  '
End Sub

Sub INVERSIONE_PROFILO_ROTORE(ByVal NomeFileRidotto As String, ByVal NpFianco1 As Integer)
'
Dim R2() As Double
Dim A()  As Double
Dim B()  As Double
'
Dim i    As Integer
Dim Np   As Integer
Dim stmp As String
'
On Error Resume Next
  '
  Open NomeFileRidotto For Input As #1
    Np = 0
    While Not EOF(1)
      Np = Np + 1
      '
      ReDim Preserve R2(Np): ReDim Preserve A(Np): ReDim Preserve B(Np)
      '
      Input #1, R2(Np), A(Np), B(Np)
      '
    Wend
  Close #1
  '
  If (Dir$(NomeFileRidotto) <> "") Then Kill NomeFileRidotto
  '
  Open NomeFileRidotto For Output As #1
    For i = Np To 1 Step -1
      A(i) = -A(i)
      If (i <> NpFianco1) Then
        B(i) = -B(i)
      Else
        If (B(i) > 0) Then B(i) = -B(i)
      End If
        stmp = ""
        If (R2(i) >= 0) Then
          stmp = stmp & "+" & Left$(frmt0(Abs(R2(i)), 4) & String(8, " "), 8) & " "
        Else
          stmp = stmp & "-" & Left$(frmt0(Abs(R2(i)), 4) & String(8, " "), 8) & " "
        End If
        If (A(i) >= 0) Then
          stmp = stmp & "+" & Left$(frmt0(Abs(A(i)), 4) & String(8, " "), 8) & " "
        Else
          stmp = stmp & "-" & Left$(frmt0(Abs(A(i)), 4) & String(8, " "), 8) & " "
        End If
        If (B(i) >= 0) Then
          stmp = stmp & "+" & Left$(frmt0(Abs(B(i)), 4) & String(8, " "), 8) & " "
        Else
          stmp = stmp & "-" & Left$(frmt0(Abs(B(i)), 4) & String(8, " "), 8) & " "
        End If
        Print #1, stmp
    Next i
  Close #1
  '
End Sub

Sub COPIA_FILE_ROTORI(ByVal DATI As String, ByVal SORGENTE As String, ByVal DESTINAZIONE As String)
'
Dim i           As Integer
Dim j           As Integer
Dim k           As Integer
Dim stmp        As String
Dim PERCORSO    As String
Dim BASE        As String
Dim NomeROTORE  As String
Dim COORDINATE  As String
Dim riga        As String
Dim LISTA_FILE() As String
'
On Error Resume Next
  '
  'LETTURA DEL PERCORSO
  i = InStr(DATI, "PERCORSO=")
  j = InStr(i, DATI, Chr(10))
  stmp = Mid(DATI, i, j - i - 1)
  k = InStr(stmp, "=")
  PERCORSO = Right$(stmp, Len(stmp) - k)
  'RICAVO IL NOME DEI PUNTI DEL ROTORE DAL PERCORSO
  COORDINATE = PERCORSO
  If (Len(COORDINATE) > 0) Then
    i = InStr(COORDINATE, "\")
    While (i <> 0)
      j = i
      i = InStr(i + 1, COORDINATE, "\")
    Wend
    'RICAVO IL NOME DEI PUNTI DEL ROTORE
    COORDINATE = Right$(COORDINATE, Len(COORDINATE) - j)
  End If
  'RICAVO IL NOME DEL ROTORE
  NomeROTORE = PERCORSO
  i = InStr(NomeROTORE, COORDINATE)
  NomeROTORE = Left$(NomeROTORE, i - 2)
  If (Len(NomeROTORE) > 0) Then
    i = InStr(NomeROTORE, "\")
    BASE = Left$(NomeROTORE, i - 1)
    NomeROTORE = Right$(NomeROTORE, Len(NomeROTORE) - i)
  End If
  'SE NON ESISTE LO CREO
  stmp = DESTINAZIONE & "\" & BASE & "\" & NomeROTORE
  If (Dir$(stmp, 16) = "") Then
    i = InStr(stmp, "\")
    While (i <> 0)
      riga = Left$(stmp, i - 1)
      'SALTO LA PERIFERICA
      If Right(riga, 1) <> ":" And Dir$(riga, 16) = "" Then MkDir riga
      'MsgBox Left$(stmp, i - 1)
      i = InStr(i + 1, stmp, "\")
    Wend
    MkDir stmp
  End If
  'COSTRUISCO LA LISTA DEI FILE PRESENTI
  k = 0
  riga = Dir(SORGENTE & "\" & BASE & "\" & NomeROTORE & "\", vbNormal)
  riga = UCase$(Trim$(riga))
  Do While (riga <> "")
    If (riga <> ".") And (riga <> "..") Then
      If (GetAttr(SORGENTE & "\" & BASE & "\" & NomeROTORE & "\" & riga) And vbNormal) = vbNormal Then
        k = k + 1
        ReDim Preserve LISTA_FILE(k)
        LISTA_FILE(k) = riga
      End If
    End If
    riga = Dir
    riga = UCase$(Trim$(riga))
  Loop
  'COPIA DI TUTTI I FILE PRESENTI NEL DIRETTORIO
  For i = 1 To UBound(LISTA_FILE)
    If Dir$(DESTINAZIONE & "\" & BASE & "\" & NomeROTORE & "\" & LISTA_FILE(i)) <> "" Then
      Kill DESTINAZIONE & "\" & BASE & "\" & NomeROTORE & "\" & LISTA_FILE(i)
    End If
    FileCopy SORGENTE & "\" & BASE & "\" & NomeROTORE & "\" & LISTA_FILE(i), DESTINAZIONE & "\" & BASE & "\" & NomeROTORE & "\" & LISTA_FILE(i)
    WRITE_DIALOG NomeROTORE & ": copying file " & LISTA_FILE(i)
  Next i

End Sub
