Attribute VB_Name = "MOLAVITE01"
'*********************************************************************
'
' 2014.03.04
'
' Moduilo per il calcolo dell'altezza minima di dente della mola a vite
'
' comprende anche il gioco di testa ruota mola
'
' Routines estratte da MV_Ver
'
' Call GetInput to populate internal paramenters
'
' Calcolo Ciclo di Rettifica
'*********************************************************************



Option Explicit

' VARI
Private PI As Double
Private Phi$, PAR$, Par1$, VPB

Private Rast$ 'DisegnoRul$,
Private RtRulIni
Dim Codice As Integer
Private Const Nsample = 50

' MOLA VITE
Private DrMola As Double             ' OTTENUTO: Ingranaggio : Diametro di rotolamento
Private DedMola As Double            ' OTTENUTO: Cre.Rif.Mola : Dedendum rispetto a SpMola
Private GioccoFondoMola As Double    ' INPUT/OTTENUTO:
Private DGioccoFondoMola As Double   ' INPUT
Private Apmola As Double             ' OTTENUTO: Cre.Rif.Mola : Ang.Di Press.Normale in radianti
Private ApMolaD As Double                  ' Cre.Rif.Mola : Ang.Di Press.Normale in gradi
Private SpMola As Double             ' OTTENUTO: Cre.Rif.Mola : Spessore
Private MrMola As Double                    ' Cre.Rif.Mola : Modulo Normale
Private GioccoFondoMolaMax As Double
Private HscGioccoFondoMola_Max As Double
Private HscGioccoFondoMola_Min As Double
Private HscGioccoFondoMola_Value As Double


' RULLO
Private YdapRul As Double
Private ApMedRul As Double       ' in radianti

Private ApMedRulD As Double      ' INPUT: EW
'Public SpRul As Double          ' INPUT:
Private SpRul_A As Double        ' OTTENUTO: semispessore sinistro zp_A; si lavora solo con rulli simmetrici => calcolato da input
Private SpRul_B As Double        ' OTTENUTO: semispessore destro zp_B
Private SpRul_IN As Double      ' INPUT:
Private RfiancoRul As Double     ' INPUT: raggio di bombatura del rullo HBR
Private Drul As Double           ' INPUT: duiametro esterno rullo da
Private RmedRul As Double        ' INPUT: xp ( raggio punto medio fianco profilante rullo)
Private ArastRulD As Double       ' INPUT: ras
Private YrastRul As Double       ' INPUT: Ascissa rastremazione rullo

Private RtRul As Double          ' INPUT: raggio di testa del rullo  r
Private ApRast As Double         ' INPUT/OTTENUTO: angolo di rastremazione
Private XrfRul As Double         ' OTTENUTO: ascissa riferiemnto rullo
Private YrfRul As Double         ' OTTENUTO: ordinata riferimento rullo
Private SpMedRul As Double       ' OTTENUTO: ( zp_A + zp_B ) / 2
Private XrastRul As Double       ' OTTENUTO:
Private XrtRul As Double
Private SpostRul As Double       ' OTTENUTO: spostamento rullo lungo ascissa
Private YdatRul As Double
Private XdatRul As Double
Private ApRastApp As Double
Private DbRast As Double
Private XdapRul As Double
Private CorrAp As Double
Private DrastOtt As Double
Private DmaxEvolv As Double
Private AminEvolv As Double
Private AmaxEvolv As Double
Private EbRastOtt As Double
Private ErastOtt As Double

' INGRANAGGIO
Private Dp1 As Double        ' Ingranaggio : Diametro Primitivo
Private Mapp As Double       ' Ingranaggio : Modulo Apparente
Private DB1 As Double        ' OTTENUTO: Ingranaggio : Diametro di Base
Private DatPiede As Double   ' INPUT: Ingranaggio : Diametro Attivo Richisto SAP
Private DatTesta As Double   ' INPUT: DEAP
Private ApApp As Double      ' Ingranaggio : Ang.Press.Apparente In radianti
Private HELB As Double       ' OTTENUTO: Ingranaggio : Elica di base in radianti
Private HelpD As Double       ' INPUT: Ingranaggio : Elica Primitiva in Radianti
Private Help As Double       ' OTTENUTO: Ingranaggio : Elica Primitiva in Radianti
Private Apr As Double        ' OTTENUTO: Ingranaggio : ANG.DI PRESS.NORMALE in Radianti
Private Aprd As Double       ' INPUT: Ingranaggio : ANG.DI PRESS.NORMALE in Gradi
Private Helr As Double       ' OTTENUTO: Ingranaggio : Angolo elica su diametro di rotolamento (in radianti)
Private Mr As Double         ' INPUT: Ingranaggio : MODULO NORMALE
Private Z1 As Double         ' INPUT: Ingranaggio : Numero Denti
'Mr,HelpD,Aprd,Z1
Private Eb1 As Double        ' OTTENUTO: Ingranaggio : spessore Di Base Apparente
Private Wdt As Double        ' INPUT: Ingranaggio wildhaber
Private Kdt As Double        ' INPUT: Ingranaggio : Numero Denti Per Controllo quota W
Private Epr1 As Double
Private DE1 As Double        ' INPUT : Diametro esterno
Private BombOtt As Double    ' OTTENUTO
Private Di1 As Double        ' INPUT : Ingranaggio : Diametro Interno
Private NbpBomb As Integer
Private NbpTot As Integer
Private Revolv(30) As Single
Private Levolv(30) As Single
Private Eevolv(30) As Single
Private Aevolv(30) As Single

Private Wpg, Dpige As Double
' LA RASTREMAZIONE
Private DIAMETRO_RASTREMAZIONE_OTTENUTO, ENTITA_RASTREMAZIONE_OTTENUTA As Double  ' OUTPUT
Private X_TIP_MOLA, Y_TIP_MOLA As Double  'Ascissa e Ordinata spessore TIP mola
Public MinimizzaDeltaTip As Boolean
Private IsGearTipPresent As Boolean
Private IsToolTipPresent As Boolean
Private ShowOptimizeButton As Boolean
Private TipRelifDiamRef As Double
Private TipRelifAmplRef As Double
Private HRULLOREF As Double
Private HDENTEMOLA As Double

Private DeltaG As Double
' GRAFICO

Private XmaxOLD(2) As Double
Private YmaxOLD(2) As Double
Private XminOLD(2) As Double
Private YminOLD(2) As Double
'--------------------------CONTEX
'Private Xmax(1 To 2) As Double
'Private Ymax(1 To 2) As Double
'Private Xmin(1 To 2) As Double
'Private Ymin(1 To 2) As Double
'--------------------------
Private ScalaFha As Double
Private Const Sampling = 12
Private Const SubSampling = 5
Dim FhaToll, FhaDToll As Double
'---------Opty check box
Private OptiDtip As Boolean
Private OptiEtip As Boolean
'Private OptiDmi As Boolean
Private Const OptiDmi = False

Dim Vin(1 To 17) As Double

Private ErroMV As Boolean
'---------------------------- Inponi il diametro di rotolamento mola
Dim ImponiDrMola As Boolean
'Dim MMin As Double ' Modulo mola
'---------------------------- AUTODENTATURA
Dim SOVMETRAD_REF As Double
Private apDen_Ref As Double
Private ProDen_Ref As Double
Private Deltaden_Ref As Double
Private Rtden_Ref As Double
'-------------------------------------------
Dim KeyCode
Public Function SpMola_Get()
 SpMola_Get = SpMola
End Function
Public Function Hob_ap_Get()
 Hob_ap_Get = apDen_Ref
End Function
Public Function Hob_Pro_Get()
 Hob_Pro_Get = ProDen_Ref
End Function
Public Function Hob_Rt_Get()
 Hob_Rt_Get = Rtden_Ref
End Function

' arcocoseno
Private Function Acs(Aux)
   Dim Aux1
   Aux1 = (1 - Aux * Aux)
   If Aux1 < 0 Then
      MsgBox "Coseno > 1 : Il programma si ferma : Controllare i dati", 16
      WRITE_DIALOG "ERROR in MOLAVITE01 : Cos > 1 (ERR01)" '& Err
      Acs = 0.2
      'Exit Function
      'End
   Else
      If Aux = 0 Then
         Acs = PI / 2
      Else
         If Aux > 0 Then
            Acs = Atn(Sqr(1 - Aux * Aux) / Aux)
         Else
            Aux = Abs(Aux)
            Acs = Atn(Aux / Sqr(1 - Aux * Aux)) + 1.570796
         End If
      End If
   End If
End Function
Private Function DAcsD(Aux)
   Dim Aux1
   Aux1 = (1 - Aux * Aux)
   If Aux1 < 0 Then
      MsgBox "Coseno > 1 : Il programma si ferma : Controllare i dati", 16
      WRITE_DIALOG "ERROR in MOLAVITE01 : Cos > 1 (ERR02)" '& Err
      'End
      'Exit Function
      DAcsD = 0.2
   Else
      If Aux = 0 Then
         DAcsD = PI / 2
      Else
         If Aux > 0 Then
            DAcsD = -1 / Sqr(1 - Aux ^ 2) 'Atn(Sqr(1 - Aux * Aux) / Aux)
         Else
            Aux = Abs(Aux)
            DAcsD = 1 / Sqr(1 - Aux ^ 2)
         End If
      End If
   End If
End Function
Private Function Asn(Aux)
Dim Aux1
  Aux1 = (1 - Aux * Aux)
  If Aux1 < 0 Then
  '   MsgBox "Seno > 1 : Check data", 16
     WRITE_DIALOG "ERROR in MOLAVITE01 : Sen > 1 (ERR03)" '& Err
     Asn = 0.5
    ' End
     Exit Function
    
  Else
     If Aux = 1 Then
        Asn = PI / 2
     Else
        Asn = Atn(Aux / Sqr(Aux1))
     End If
  End If
End Function
Private Function Fndr(A)
   Fndr = A * PI / 180
End Function
Private Function Fnrd(A)
   Fnrd = A * 180 / PI
End Function
Private Function inv(Aux)
  inv = Tan(Aux) - Aux
End Function
Private Function DinvD(Aux)
  DinvD = 1 + Tan(Aux) ^ 1 - 1
End Function
Private Function Ainv(Aux)
   Dim i, Ymini, Ymaxi, Ymoyen, X1
    Ymini = 0: Ymaxi = 1.570796
    For i = 1 To 25
       Ymoyen = (Ymini + Ymaxi) / 2: X1 = inv(Ymoyen)
       If X1 < Aux Then Ymini = Ymoyen Else: Ymaxi = Ymoyen
    Next i
    Ainv = Ymoyen
End Function
Private Sub CotePiges(Dpige, Z, Dbx, Helbx, Eb, Kpig, Code$)
  ' Code$ = "E": Calcul de Eb
  ' Code$ = "K": Calcul de Wpg
  Dim Aux, Pba, W, We, X
  Pba = PI * Dbx / Z
  If Int(Z / 2) = Z / 2 Then W = Dbx Else W = Cos(PI / (2 * Z)) * Dbx
  If Dpige = 0 Then Kpig = 0: Exit Sub
  If Kpig = 0 And Code$ = "E" Then Exit Sub
  If Code$ = "K" Then
     We = Eb - Pba
     X = (We + Dpige / Cos(Helbx)) / Dbx
     Kpig = W / Cos(Ainv(X)) + Dpige
  Else
     Aux = W / (Kpig - Dpige)
     If Abs(Aux) > 1 Then Eb = 0: Wpg = 0: Exit Sub
     Aux = Acs(W / (Kpig - Dpige))
     Eb = Dbx * inv(Aux) - Dpige / Cos(Helbx) + Pba
  End If
End Sub
Private Sub LdevAdev(DB, dx, Ldev, Adev, Entita)
   Dim Aux
   If dx > DB Then
      ' Connu : Dx
      Ldev = Sqr((dx / 2) ^ 2 - (DB / 2) ^ 2)
      Adev = Ldev / (DB / 2)
   Else
      If Ldev > 0 Then
         ' Connu : Ldev
         dx = 2 * Sqr((DB / 2) ^ 2 + Ldev ^ 2)
         Adev = Ldev / (DB / 2)
      Else
         ' Connu : Adev
         Ldev = Adev * (DB / 2)
         dx = 2 * Sqr((DB / 2) ^ 2 + Ldev ^ 2)
      End If
   End If
   Par1$ = " " & Phi$ & Format(dx, "0.00")
   If Entita <> 0 Then Par1$ = Par1$ & " E=" & Format(Entita, "0.000")
   PAR$ = " L=" & Format(Ldev, "0.00") & "  A=" & Format(Fnrd(Adev), "0.00") & "�"
End Sub
Private Sub CalcoloIngranaggio(Code$) 'Code$
    Dim Aux, MappFm, DemiPas
    Dim DR, Ap
    Help = Fndr(HelpD): Apr = Fndr(Aprd)
    Mapp = Mr / Cos(Help): Dp1 = Mapp * Z1
    ApApp = Atn(Tan(Apr) / Cos(Help))
    DB1 = Dp1 * Cos(ApApp)
    If Code$ = "DB" Then Exit Sub
    HELB = Atn(Tan(Help) * Cos(ApApp))
    If Z1 < 2 Or Aprd < 0 Or Mr < 0.1 Then DB1 = 0: Exit Sub
        
    '                           *** calcul ep. base  (ep= spessore)
    Eb1 = 0
    If Wdt <> 0 Then
       Aux = PI * DB1 / Z1
       Eb1 = (Wdt / Cos(HELB)) - (Kdt - 1) * Aux
    End If
'    If Wpg <> 0 And Eb1 = 0 Then
'       Call CotePiges(DPG, Z1, Db1, Helb, Eb1, Wpg, "E")
'    End If
'    If Epr1 <> 0 Then
'       Aux = Epr1 / Cos(Help)
'       Eb1 = (Aux / Dp1 + inv(ApApp)) * Db1
'    End If
       
    '                 *** calcul cotes de controle
   
'    If QuotaControllo$ <> "E" Then Epr1 = ((Eb1 / Db1) - inv(ApApp)) * Dp1 * Cos(Help)
    Epr1 = ((Eb1 / DB1) - inv(ApApp)) * Dp1 * Cos(Help)
'    If Apr > 0 Then
'        Aux = (Epr1 - (PI * Mr / 2)) / (2 * Tan(Apr))
'    End If
'    Kdep = Aux / Mr
'    If Abs(Kdep) < 0.0001 Then
'        Kdep = 0
'    End If
'    If Kdt > 0 Then
'        Aux = PI * Db1 / Z1
'        If QuotaControllo$ <> "W" Then Wdt = ((Kdt - 1) * Aux + Eb1) * Cos(Helb)
'    End If
'    If DPG <> 0 And QuotaControllo$ <> "Q" Then
'        Call CotePiges(DPG, Z1, Db1, Helb, Eb1, Wpg, "K")
'    End If
End Sub

Public Sub CalcoloCicloRettificaSAMP(ByRef CODICE_PEZZO As String, ByRef Modulo As Single, ByRef Zing As Single, ByRef ALFAn As Single, ByRef Betan As Single, ByRef SAP As Single, ByRef EAP As Single, ByRef FASCIA As Single, ByRef SovraMetalloRAD As Single, ByRef Nprincipi As Single, ByVal Gruppo As String)
'Routine per la definizione del ciclo di rettifica
'a partire dai dati del pezzo e della mola.
'Routine per rettifica con mola a vite su G250, G450, G160


Dim FileNum As Integer
'Input
'Dim Modulo As Single
'Dim Zing   As Single
'Dim ALFAn  As Single     'Probabilmente non necessario (serve) cambiare filosofia di input
'Dim Betan  As Single     'Non utilizzato. Serve per calcolo Q'??
'Dim SAP    As Single     'Non utilizzato. Serve per calcolo Q'??
'Dim EAP    As Single
'Dim FASCIA As Single
'Dim SovraMetalloRAD As Single
'Dim Nprincipi    As Single


Dim FasciaLavoro As Single
Dim sovrametallo As Single
'Coefficienti numerici per calcolo incrementi.
'Derivano dalla definizione di numeratore e denominatore per la somma di frazioni
Dim PI As Single
Dim IncSgr As Single
Dim IncFin As Single
Dim k(4, 7) As Single

Dim MaxGiriTav As Single
Dim DiametroTaglio As Single
Dim DiametroMola As Single
Dim VelTaglio1 As Single
Dim VelTaglio2 As Single
Dim Girimola1 As Single
Dim Girimola2 As Single


Dim CoefficienteAvanzamento As Single
Dim FattoreAvanzamento As Single
Dim DiametroAvanzamento As Single

Dim FattoreShifting1 As Single
Dim FattoreShifting(4, 8) As Single

Dim SaltoMola(4) As Single
Dim Ritorno(4) As Single
Dim UltimoCiclo As Integer
Dim RitTemp As Single


'Output

Dim Cicli     As Integer
Dim TotRip    As Integer
Dim NumRip(4) As Integer
Dim TipoCiclo(4) As Integer
Dim IncRip(4, 8) As Single
Dim GiriMola(4) As Single
Dim Veltaglio(4) As Single
Dim Avanzamento(4) As Single
Dim Shifting(4, 8) As Single
Dim SaltoShifting(4) As Single

Dim OutPutCicloSAMP(4, 10) As Single


On Error Resume Next
'Dati di input - Acquisizione.
PI = 3.14159265358979

'Modulo = InputCiclo(1) 'Modulo = LEP("MN_G")
'Zing = InputCiclo(2) 'Zing = LEP("NUMDENTI_G")
'ALFAn = InputCiclo(3) 'ALFAn = LEP("ALFA_G")
'Betan = InputCiclo(4) 'Betan = LEP("BETA_G")
'SAP = InputCiclo(5) 'SAP = LEP("DSAP_G")
'EAP = InputCiclo(6) 'EAP = LEP("DEAP_G")
'FASCIA = InputCiclo(7) 'FASCIA = LEP("iFas1F1") + LEP("iFas2F1") + LEP("iFas3F1")
'SovraMetalloRAD = InputCiclo(8) 'SovraMetallo = LEP("SOVRMETRAD_G")
'Nprincipi = InputCiclo(9) 'Nprincipi = LEP("NUMPRINC_G")

sovrametallo = SovraMetalloRAD * Sin(ALFAn * PI / 180)

'1. Calcolo ripetizioni totali da sovrametallo radiale.
'   Calcolo numero cicli da utilizzare : Suddivisione ripetizioni totali sui 4 cicli a disposizione.
'   Massime ripetizioni totali = 8
'   Massime ripetizioni per cicli di sgrossatura  = 3
'   Massime ripetizioni per cicli di finitura     = 1
'   Gestione a riempimento progressivo. Garantisce che l'ultima ripetizione realizza l'incremento minimo.
TotRip = 0

If sovrametallo < 0.121 Then
     TotRip = 2
     NumRip(1) = 1: TipoCiclo(1) = 2
     NumRip(2) = 1: TipoCiclo(2) = 1
     NumRip(3) = 0: TipoCiclo(3) = 0
     NumRip(4) = 0: TipoCiclo(4) = 0
     Else
        If sovrametallo < 0.181 Then
             TotRip = 3
             NumRip(1) = 1: TipoCiclo(1) = 2
             NumRip(2) = 1: TipoCiclo(2) = 2
             NumRip(3) = 1: TipoCiclo(3) = 1
             NumRip(4) = 0: TipoCiclo(4) = 0
             Else
                If sovrametallo < 0.241 Then
                     TotRip = 4
                     NumRip(1) = 1: TipoCiclo(1) = 2
                     NumRip(2) = 1: TipoCiclo(2) = 2
                     NumRip(3) = 1: TipoCiclo(3) = 1
                     NumRip(4) = 1: TipoCiclo(4) = 1
                     Else
                        If sovrametallo < 0.301 Then
                             TotRip = 5
                             NumRip(1) = 2: TipoCiclo(1) = 2
                             NumRip(2) = 1: TipoCiclo(2) = 2
                             NumRip(3) = 1: TipoCiclo(3) = 1
                             NumRip(4) = 1: TipoCiclo(4) = 1
                             Else
                                If sovrametallo < 0.361 Then
                                     TotRip = 6
                                     NumRip(1) = 2: TipoCiclo(1) = 2
                                     NumRip(2) = 2: TipoCiclo(2) = 2
                                     NumRip(3) = 1: TipoCiclo(3) = 1
                                     NumRip(4) = 1: TipoCiclo(4) = 1
                                     Else
                                        If sovrametallo < 0.421 Then
                                             TotRip = 7
                                             NumRip(1) = 3: TipoCiclo(1) = 2
                                             NumRip(2) = 2: TipoCiclo(2) = 2
                                             NumRip(3) = 1: TipoCiclo(3) = 1
                                             NumRip(4) = 1: TipoCiclo(4) = 1
                                             Else
                                                If sovrametallo < 0.481 Then
                                                     TotRip = 8
                                                     NumRip(1) = 3: TipoCiclo(1) = 2
                                                     NumRip(2) = 3: TipoCiclo(2) = 2
                                                     NumRip(3) = 1: TipoCiclo(3) = 1
                                                     NumRip(4) = 1: TipoCiclo(4) = 1
                                                End If
                                        End If
                                End If
                        End If
               End If
        End If
End If
'2.  Calcolo incremento radiale per ripetizione.
'    Incremento radiale minimo 0.06 mm
'    (Sovrametallototale-incremento minimo)/K(n,m)
'2.1 Assegnazione valori a coefficienti
'K(n,m) n=ciclo m=ripetizioni totali (m=1 non si verifica mai)
k(1, 1) = 1: k(1, 2) = 1: k(1, 3) = 1.8:  k(1, 4) = 2.4: k(1, 5) = 3.6925: k(1, 6) = 4.4837: k(1, 7) = 5.67:   k(1, 8) = 6.5202
k(2, 1) = 0: k(2, 2) = 1: k(2, 3) = 2.25: k(2, 4) = 3:   k(2, 5) = 3.8182: k(2, 6) = 5.0997: k(2, 7) = 6:      k(2, 8) = 7.1373
k(3, 1) = 0: k(3, 2) = 0: k(3, 3) = 1:    k(3, 4) = 4:   k(3, 5) = 5.0909: k(3, 6) = 6.1818: k(3, 7) = 7.2727: k(3, 8) = 8.3636
k(4, 1) = 0: k(4, 2) = 0: k(4, 3) = 0:    k(4, 4) = 1:   k(4, 5) = 1:      k(4, 6) = 1:      k(4, 7) = 1:      k(4, 8) = 1
               
IncFin = 0.02 / Sin(ALFAn * PI / 180)
IncSgr = (sovrametallo - 0.02) / Sin(ALFAn * PI / 180)
If TotRip = 2 Then
         IncRip(1, 2) = IncSgr
         IncRip(2, 2) = IncFin
         IncRip(3, 2) = 0
         IncRip(4, 2) = 0
End If
If TotRip = 3 Then
         IncRip(1, 3) = IncSgr / k(1, 3)
         IncRip(2, 3) = IncSgr / k(2, 3)
         IncRip(3, 3) = IncFin
         IncRip(4, 3) = 0
End If
For j = 4 To 8
         IncRip(1, j) = IncSgr / k(1, j)
         IncRip(2, j) = IncSgr / k(2, j)
         IncRip(3, j) = IncSgr / k(3, j)
         IncRip(4, j) = IncFin
Next j

'3. Calcolo giri al minuto mandrino portamola
DiametroMola = 250     'Diametro mola di riferimento
DiametroTaglio = 400   'Diametro pezzo di separazione per scelta giri mola

'MaxGiriTav = 400      'Giri massimi tavola portapezzo
MaxGiriTav = 1000      'Giri massimi tavola portapezzo G250

'Aggiungerei un ulteriore parametro per moduli pi� piccoli es. <2
'VelTaglio0 = 80        'Velocit� di taglio per moduli inferiori di 2 (m/sec.)

VelTaglio1 = 63        'Velocit� di taglio per moduli inferiori di 4 (m/sec.)
VelTaglio2 = 53        'Velocit� di taglio per moduli superiori di 7 (m/sec.)

Girimola1 = 1000 * VelTaglio1 / PI / DiametroMola * 60
Girimola2 = 1000 * VelTaglio2 / PI / DiametroMola * 60

If (EAP < DiametroTaglio) Then
              If (Modulo <= 3) Then
                  For i = 1 To 4
                    GiriMola(i) = Girimola1
                  Next i
              Else
                  If (Modulo < 7) Then
                      For i = 1 To 4
                        GiriMola(i) = (2200 / (7 - 3)) * (7 - Modulo) + 2000
                      Next i
                  Else
                      For i = 1 To 4
                        GiriMola(i) = 2000
                      Next i
                  End If
              End If
Else
              If (Modulo <= 3) Then
                  For i = 1 To 4
                    GiriMola(i) = Girimola1 - (((EAP - 400) / 600) * 2700)
                  Next i
              Else
                  If (Modulo < 7) Then
                      For i = 1 To 4
                        GiriMola(i) = ((((2200 / (7 - 3)) * (7 - Modulo)) + 2000) - (((EAP - 400) / 600) * (((2200 / (7 - 3)) * (7 - Modulo)) + 500)))
                      Next i
                  Else
                      For i = 1 To 4
                        GiriMola(i) = (2000 - (((EAP - 400) / 600) * 500))
                      Next i
                  End If
              End If
End If
' Verifica compatibilit� con giri massimi tavola portapezzo e conversione in velocit� di taglio
For i = 1 To 4
    If (GiriMola(i) > (MaxGiriTav * Zing / Nprincipi)) Then
           GiriMola(i) = (MaxGiriTav * Zing / Nprincipi)
    End If
    Veltaglio(i) = Round(GiriMola(i) * PI * DiametroMola / 60 / 1000, 0)
    
Next i
              
                                   
'4. Calcolo avanzamento assiale.
DiametroAvanzamento = 500
If EAP < DiametroAvanzamento Then
           CoefficienteAvanzamento = 1
   Else
           CoefficienteAvanzamento = 0.833
End If
              
If Modulo < 1.51 Then
         If Nprincipi = 1 Then
               FattoreAvanzamento = CoefficienteAvanzamento * 1.8
         End If
         If Nprincipi = 2 Then
               FattoreAvanzamento = CoefficienteAvanzamento * 1.6
         End If
         If Nprincipi = 3 Then
               FattoreAvanzamento = CoefficienteAvanzamento * 1.4
         End If
         If Nprincipi > 3 Then
              FattoreAvanzamento = CoefficienteAvanzamento * 4.8 / Nprincipi
         End If
Else
    If Modulo < 2.251 Then
             If Nprincipi = 1 Then
                   FattoreAvanzamento = CoefficienteAvanzamento * 1.7
             End If
             If Nprincipi = 2 Then
                   FattoreAvanzamento = CoefficienteAvanzamento * 1.5
             End If
             If Nprincipi = 3 Then
                   FattoreAvanzamento = CoefficienteAvanzamento * 1.3
             End If
             If Nprincipi > 3 Then
                   FattoreAvanzamento = CoefficienteAvanzamento * 4.4 / Nprincipi
             End If
Else
    If Modulo < 3.01 Then
             If Nprincipi = 1 Then
                   FattoreAvanzamento = CoefficienteAvanzamento * 1.6
             End If
             If Nprincipi = 2 Then
                   FattoreAvanzamento = CoefficienteAvanzamento * 1.4
             End If
             If Nprincipi = 3 Then
                   FattoreAvanzamento = CoefficienteAvanzamento * 1.2
             End If
             If Nprincipi > 3 Then
                   FattoreAvanzamento = CoefficienteAvanzamento * 4 / Nprincipi
             End If
Else
    If Modulo < 4.01 Then
             If Nprincipi = 1 Then
                   FattoreAvanzamento = CoefficienteAvanzamento * 1.4
             End If
             If Nprincipi = 2 Then
                   FattoreAvanzamento = CoefficienteAvanzamento * 1.2
             End If
             If Nprincipi = 3 Then
                   FattoreAvanzamento = CoefficienteAvanzamento * 1
             End If
             If Nprincipi > 3 Then
                   FattoreAvanzamento = CoefficienteAvanzamento * 3 / Nprincipi
             End If
Else
    If Modulo < 6.01 Then
             If Nprincipi = 1 Then
                   FattoreAvanzamento = CoefficienteAvanzamento * 1.2
             End If
             If Nprincipi = 2 Then
                   FattoreAvanzamento = CoefficienteAvanzamento * 1
             End If
             If Nprincipi = 3 Then
                   FattoreAvanzamento = CoefficienteAvanzamento * 0.85
             End If
             If Nprincipi > 3 Then
                   FattoreAvanzamento = CoefficienteAvanzamento * 2.6 / Nprincipi
             End If
Else
    If Modulo < 10.01 Then
             If Nprincipi = 1 Then
                   FattoreAvanzamento = CoefficienteAvanzamento * 1
             End If
             If Nprincipi = 2 Then
                   FattoreAvanzamento = CoefficienteAvanzamento * 0.8
             End If
             If Nprincipi = 3 Then
                   FattoreAvanzamento = CoefficienteAvanzamento * 0.65
             End If
             If Nprincipi > 3 Then
                   FattoreAvanzamento = CoefficienteAvanzamento * 2.2 / Nprincipi
             End If
End If
End If
End If
End If
End If
End If


If TotRip = 2 Then
          Avanzamento(1) = FattoreAvanzamento
          Avanzamento(2) = FattoreAvanzamento * 0.42
          Avanzamento(3) = 0
          Avanzamento(4) = 0
     Else
          If TotRip = 3 Then
                    Avanzamento(1) = FattoreAvanzamento
                    Avanzamento(2) = FattoreAvanzamento * 0.85
                    Avanzamento(3) = FattoreAvanzamento * 0.42
                    Avanzamento(4) = 0
               Else
                    Avanzamento(1) = FattoreAvanzamento
                    Avanzamento(2) = FattoreAvanzamento * 0.85
                    Avanzamento(3) = FattoreAvanzamento * 0.85
                    Avanzamento(4) = FattoreAvanzamento * 0.42
          End If
End If

'5. Calcolo shifting tangenziale durante la passata (mm/mm di corsa assiale)
'Calcolo shifting per prima passata e salto mola per ripetizione di finitura

If TotRip < 5 Then
      UltimoCiclo = TotRip
   Else
      UltimoCiclo = 4
End If

For i = 1 To 4
If i = UltimoCiclo Then
SaltoMola(i) = Modulo * Nprincipi
Ritorno(i) = SaltoMola(i)
Else
SaltoMola(i) = 0
Ritorno(i) = 0
End If
Next i


If Modulo < 2.01 Then
    FattoreShifting1 = 0.025
    If TotRip = 2 Then
                      FattoreShifting(2, 2) = 0.015
                      FattoreShifting(3, 2) = 0
                      FattoreShifting(4, 2) = 0
                  Else
                      If TotRip = 3 Then
                                        FattoreShifting(2, 3) = 0.02
                                        FattoreShifting(3, 3) = 0.01
                                        FattoreShifting(4, 3) = 0
                                    Else
                                        For j = 4 To 8
                                        FattoreShifting(2, j) = 0.02
                                        FattoreShifting(3, j) = 0.02
                                        FattoreShifting(4, j) = 0.01
                                        Next j
                                    
                      End If
    End If
      Else
         If Modulo < 3.01 Then
             FattoreShifting1 = 0.03
                 If TotRip = 2 Then
                      FattoreShifting(2, 2) = 0.02
                      FattoreShifting(3, 2) = 0
                      FattoreShifting(4, 2) = 0
                  Else
                      If TotRip = 3 Then
                                        FattoreShifting(2, 3) = 0.02
                                        FattoreShifting(3, 3) = 0.015
                                        FattoreShifting(4, 3) = 0
                                    Else
                                        For j = 4 To 8
                                        FattoreShifting(2, j) = 0.025
                                        FattoreShifting(3, j) = 0.02
                                        FattoreShifting(4, j) = 0.015
                                        Next j
                                    
                      End If
    End If
               Else
                  If Modulo < 4.01 Then
                      FattoreShifting1 = 0.035
                          If TotRip = 2 Then
                      FattoreShifting(2, 2) = 0.025
                      FattoreShifting(3, 2) = 0
                      FattoreShifting(4, 2) = 0
                  Else
                      If TotRip = 3 Then
                                        FattoreShifting(2, 3) = 0.03
                                        FattoreShifting(3, 3) = 0.02
                                        FattoreShifting(4, 3) = 0
                                    Else
                                        For j = 4 To 8
                                        FattoreShifting(2, j) = 0.03
                                        FattoreShifting(3, j) = 0.025
                                        FattoreShifting(4, j) = 0.02
                                        Next j
                                    
                      End If
    End If
                        Else
                           If Modulo < 6.01 Then
                               FattoreShifting1 = 0.045
                                   If TotRip = 2 Then
                      FattoreShifting(2, 2) = 0.03
                      FattoreShifting(3, 2) = 0
                      FattoreShifting(4, 2) = 0
                  Else
                      If TotRip = 3 Then
                                        FattoreShifting(2, 3) = 0.04
                                        FattoreShifting(3, 3) = 0.025
                                        FattoreShifting(4, 3) = 0
                                    Else
                                        For j = 4 To 8
                                        FattoreShifting(2, j) = 0.04
                                        FattoreShifting(3, j) = 0.03
                                        FattoreShifting(4, j) = 0.025
                                        Next j
                                    
                      End If
    End If
                                 Else
                                    If Modulo < 10.51 Then
                                        FattoreShifting1 = 0.055
                                        If TotRip = 2 Then
                      FattoreShifting(2, 2) = 0.04
                      FattoreShifting(3, 2) = 0
                      FattoreShifting(4, 2) = 0
                  Else
                      If TotRip = 3 Then
                                        FattoreShifting(2, 3) = 0.05
                                        FattoreShifting(3, 3) = 0.035
                                        FattoreShifting(4, 3) = 0
                                    Else
                                        For j = 4 To 8
                                        FattoreShifting(2, j) = 0.05
                                        FattoreShifting(3, j) = 0.045
                                        FattoreShifting(4, j) = 0.035
                                        Next j
                                    
                      End If
    End If
                                    End If
                           End If
                  End If
        End If
End If
        
For j = 1 To 8
     Shifting(1, j) = FattoreShifting1
     FattoreShifting(1, j) = FattoreShifting1
     For i = 2 To 4
          Shifting(i, j) = FattoreShifting(i, j)
     Next i
Next j


'RitTemp = 0
'For i = 1 To 4
' If i < UltimoCiclo Then
'    RitTemp = FASCIA * FattoreShifting(i, TotRip) * NumRip(i) + RitTemp
'    Ritorno(i) = 0
' Else
'   If i = UltimoCiclo Then
'    Ritorno(i) = FASCIA * FattoreShifting(i, TotRip) * NumRip(i) + RitTemp
'   Else
'    Ritorno(i) = 0
'   End If
' End If
'Next i

              
For i = 1 To 4
OutPutCicloSAMP(i, 1) = Format(TipoCiclo(i), "0")            'Tipo ciclo
OutPutCicloSAMP(i, 2) = Format(NumRip(i), "0")               'Numero ripetizioni
OutPutCicloSAMP(i, 3) = Format(IncRip(i, TotRip), "0.00")    'Incremento ciclo
OutPutCicloSAMP(i, 4) = Format(Avanzamento(i), "0.000")      'Avanzamento
OutPutCicloSAMP(i, 5) = Format(Shifting(i, TotRip), "0.000") 'Fattore Shift diagonale
OutPutCicloSAMP(i, 6) = Format(Veltaglio(i), "0")            'Velocit� di taglio mola
OutPutCicloSAMP(i, 7) = 0
OutPutCicloSAMP(i, 8) = Format(SaltoMola(i), "0.0")          'Shift mola
OutPutCicloSAMP(i, 9) = Format(Ritorno(i), "0.00")           'Ritorno mola
Next i
              

FileNum = FreeFile

Open g_chOemPATH & "\CICLO_MV.INI" For Output As #FileNum
    'Print #FileNum, "[OEM0_CICLI85]"
    Print #FileNum, "[" & Gruppo & "]"
    Print #FileNum, "DATE =" & Date & " " & Time
    Print #FileNum, "NOME =" & CODICE_PEZZO
    For j = 1 To 9
      If (j <> 7) Then
       For i = 1 To 4
          If (j = 1) Or (j = 2) Or (j = 6) Then
           Print #FileNum, "CYC[0," & Format(j + 20 * (i - 1)) & "]=" & Format(OutPutCicloSAMP(i, j), "0")
          Else
           Print #FileNum, "CYC[0," & Format(j + 20 * (i - 1)) & "]=" & Format(OutPutCicloSAMP(i, j), "0.00")
          End If
       Next i
      End If
    Next j
    
Close FileNum




  
End Sub

Private Sub CalcoloRullo(Code$)
    Dim Aux As Double, ApApp As Double, X As Double, Y As Double
    Dim Ear1 As Double, SpRul As Double
    Dim i As Integer, DB As Double, Apa As Double

    'SpRul_A = Epr1 / 2: SpRul_B = Epr1 / 2
    
    On Error GoTo errCalcoloRullo
    
    SpMedRul = (SpRul_A + SpRul_B) / 2
    If (ImponiDrMola) Then
    ApMedRul = Fndr(ApMedRulD)
    Else
    ApMedRul = Fndr(ApMedRulD)
    End If
    XrfRul = RfiancoRul * Cos(ApMedRul) - SpMedRul
    YrfRul = RfiancoRul * Sin(ApMedRul) + (Drul / 2 - RmedRul)
    If Rast$ = "O" Then
        Y = YrfRul - YrastRul
        XrastRul = Sqr(RfiancoRul ^ 2 - Y ^ 2) - XrfRul
        ApRast = Asn((YrfRul - YrastRul) / RfiancoRul) + Fndr(ArastRulD)
        XrtRul = XrastRul - (YrastRul - RtRul) * Tan(ApRast) - RtRul / Cos(ApRast)
    Else
        Aux = RfiancoRul - RtRul
        Y = YrfRul - RtRul
        XrtRul = Sqr(Aux ^ 2 - Y ^ 2) - XrfRul
    End If
    Apmola = ApMedRul
    
    For i = 1 To 5
    
    ' Imponi DrMola
    '    DrMola
    'If (ImponiDrMola) Then
    '     DrMola = MMin * Z1 / Cos(Help)
    '    DrMola = 46.801
    '    Apmola = 15 * PG / 180
    '    MrMola = 2.4807
    'Else
    '    DrMola = (DB1 * Cos(HELB)) / Sqr(Cos(Apmola + HELB) * Cos(Apmola - HELB))
    'End If
        
        DrMola = (DB1 * Cos(HELB)) / Sqr(Cos(Apmola + HELB) * Cos(Apmola - HELB))
        Helr = Atn(Tan(HELB) * DrMola / DB1)
        MrMola = DrMola / Z1 * Cos(Helr)
        ApApp = Acs(DB1 / DrMola)
        Ear1 = (Eb1 / DB1 - inv(ApApp)) * DrMola
        SpMola = PI * MrMola - Ear1 * Cos(Helr)
        If Code$ = "SENSA_SPOST" Then
            SpRul = Ear1 * Cos(Helr)
            Aux = XrfRul + SpRul / 2
            DedMola = YrfRul - Sqr(RfiancoRul ^ 2 - Aux ^ 2)  ' FIN QUA
'********************************************************************************************
            GioccoFondoMolaMax = DrMola / 2 + DedMola - DE1 / 2 'DatTesta/ 2 calcolo il gioco quando no applico spostamento di ruollo in profilatura
            If (MinimizzaDeltaTip) Then
            Else
            GioccoFondoMola = (DrMola / 2 + DedMola - DatTesta / 2) - DGioccoFondoMola 'GioccoFondoMolaMax - DGioccoFondoMola ' impongo il gioco pari gioco quando non applico spostamento di ruollo in profilatura
            End If
  '          GioccoFondoMola = DrMola / 2 + DedMola - DE1 / 2
        Else
            DedMola = -DrMola / 2 + GioccoFondoMola + DatTesta / 2   'DE1/ 2
            Aux = YrfRul - DedMola
            SpRul = (Sqr(RfiancoRul ^ 2 - Aux ^ 2) - XrfRul) * 2
            SpostRul = PI * MrMola - SpMola - SpRul
        End If
                
        ' Definizione YdatRul , XdatRul
        
        If YdatRul = 0 Then
            DB = DB1: Apa = ApApp
        Else
            Aux = Asn((YrfRul - YdatRul) / RfiancoRul)
            Apa = Atn(Tan(Aux) / Cos(Helr))
            DB = DrMola * Cos(Apa)
        End If
        Aux = Sqr((DatTesta / 2) ^ 2 - (DB / 2) ^ 2)
        YdatRul = DedMola - (Aux - DB / 2 * Tan(Apa)) * Sin(Apa)
        XdatRul = Sqr(RfiancoRul ^ 2 - (YrfRul - YdatRul) ^ 2) - XrfRul
        If Rast$ = "O" Then
            ApRastApp = Atn(Tan(ApRast) / Cos(Helr))
            DbRast = DrMola * Cos(ApRastApp)
            If YrastRul > YdatRul Then
                Aux = Sqr((DatTesta / 2) ^ 2 - (DbRast / 2) ^ 2)
                YdatRul = DedMola - (Aux - DbRast / 2 * Tan(ApRastApp)) * Sin(ApRastApp)
                XdatRul = XrastRul - (YrastRul - YdatRul) * Tan(ApRast)
            End If
        End If
        
        ' Definizione YdapRul , XdapRul
        
        If YdapRul = 0 Then
            DB = DB1: Apa = ApApp
        Else
            Aux = Asn((YrfRul - YdapRul) / RfiancoRul)
            Apa = Atn(Tan(Aux) / Cos(Helr))
            DB = DrMola * Cos(Apa)
        End If
        If DatPiede < DB Then Exit Sub
        Aux = Sqr((DatPiede / 2) ^ 2 - (DB / 2) ^ 2)
        YdapRul = DedMola - (Aux - DB / 2 * Tan(Apa)) * Sin(Apa)
        Aux = YrfRul - YdapRul
        XdapRul = Sqr(RfiancoRul ^ 2 - Aux ^ 2) - XrfRul
    
        ' A.P. effettivo + GioccoFondo
        
        If Rast$ = "O" And YrastRul > YdatRul Then
         If (ImponiDrMola) Then
          Apmola = 15 * PG / 180
         Else
          Apmola = Atn((XdapRul - XrastRul) / (YdapRul - YrastRul))
         End If
         Aux = (YdapRul - YrastRul) / Cos(Apmola) / 2
        Else
         If (ImponiDrMola) Then
          Apmola = 15 * PG / 180
         Else
          Apmola = Atn((XdapRul - XdatRul) / (YdapRul - YdatRul))
         End If
         Aux = (YdapRul - YdatRul) / Cos(Apmola) / 2
        End If
        BombOtt = (RfiancoRul - Sqr(RfiancoRul ^ 2 - Aux ^ 2)) / Cos(HELB)
         If (ImponiDrMola) Then
         Else
        Apmola = Apmola + CorrAp
        End If
        ApMolaD = Fnrd(Apmola)
    Next i
  
  Exit Sub

errCalcoloRullo:
    MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Sub: MOLAVITE01.CalcoloRullo"
    'Resume Next
  Exit Sub

End Sub
Private Sub PntTrocFM(Hel, DR, Z, R0, Yr0, Xr0, ALFA, X, Y, dis$, RX, ApAppx)
   Dim Ypx, Xpx, AlfaPx, B, C, D
   Dim XCT, YCT, Xpt, Ypt
   Ypx = Yr0 + R0 * Cos(ALFA)
   Xpx = (Xr0 + R0 * Sin(ALFA)) / Cos(Hel)
   AlfaPx = Atn(Tan(ALFA) * Cos(Hel))
   C = Atn((Ypx * Tan(AlfaPx)) / (DR / 2 - Ypx))
   B = (Ypx * Tan(AlfaPx) - Xpx) / (DR / 2)
   D = C - B
   RX = (DR / 2 - Ypx) / Cos(C)
   XCT = RX * Sin(D): Xpt = RX * Sin(PI / Z - D)
   YCT = RX * Cos(D): Ypt = RX * Cos(PI / Z - D)
   If dis$ = "P" Then
     X = Xpt: Y = Ypt
   Else
     X = XCT: Y = YCT
   End If
   ApAppx = PI / 2 - AlfaPx - C
End Sub
Private Sub PntTroc(Hel, DR, Z, R0, Yr0, Xr0, ALFA, X, Y, dis$, RX, ApAppx)
   Dim Ypx, Xpx, AlfaPx, B, C, D
   Dim XCT, YCT, Xpt, Ypt
   Ypx = Yr0 + R0 * Cos(ALFA)
   Xpx = (Xr0 + R0 * Sin(ALFA)) / Cos(Hel)
   AlfaPx = Atn(Tan(ALFA) * Cos(Hel))
   C = Atn((Ypx * Tan(AlfaPx)) / (DR / 2 - Ypx))
   B = (Ypx * Tan(AlfaPx) - Xpx) / (DR / 2)
   D = C - B
   RX = (DR / 2 - Ypx) / Cos(C)
   XCT = RX * Sin(D): Xpt = RX * Sin(PI / Z - D)
   YCT = RX * Cos(D): Ypt = RX * Cos(PI / Z - D)
'   Angp = PG / Z - D
   If dis$ = "P" Then
     X = Xpt: Y = Ypt
   Else
     X = XCT: Y = YCT
   End If
   ApAppx = PI / 2 - AlfaPx - C
End Sub
Private Sub TgTroc(ByRef Hel, ByRef DR, ByRef Z, ByRef R0, ByRef Yr0, ByRef Xr0, ByRef ALFA, ByRef X, ByRef Y, dis$)
   Dim Xa, Ya, Xb, Yb
   Dim RX, ApAppx
   Dim E As Double: E = 0.00000001
   Call PntTroc(Hel, DR, Z, R0, Yr0, Xr0, ALFA, Xa, Ya, dis$, RX, ApAppx)
   Call PntTroc(Hel, DR, Z, R0, Yr0, Xr0, ALFA + E, Xb, Yb, dis$, RX, ApAppx)
   X = (Xb - Xa) / E
   Y = (Yb - Ya) / E
End Sub
Private Sub PntEvolv(Eb, DB, dx, X, Y, dis$)
   '  CALCUL UN POINT DE LA DEVELOPPANTE
   '  Donnees : Eb , Db , Dx
   '  Dis$ : "P" pour Pieno
   '          "V" pour Vano
   Dim Aux
   
   Aux = Eb / DB - inv(Acs(DB / dx))
   If dis$ = "P" Then
        X = dx * Sin(Aux) / 2
        Y = dx * Cos(Aux) / 2
   Else
        X = dx * Sin(PI / Z1 - Aux) / 2
        Y = dx * Cos(PI / Z1 - Aux) / 2
   End If
End Sub
Private Sub Get_Eb_Db_from_Evolv(EbI, DbI, DrI, DI, DF, Etip, EbO, DbO)
'   Dim AuxH(1 To 2) As Double
'   Dim AuxK(1 To 2) As Double
'   Dim EbI, DbI As Double
'   Dim Eb0, Db0 As Double
'   EbI = Eb: DbI = DB
   
'   AuxH(1) = EbI / DbI - inv(Acs(DbI / D1))
'   AuxK(1) = EbO / Db0 - inv(Acs(Db0 / D1))
'   AuxH(2) = EbI / DbI - inv(Acs(DbI / D2))
'   AuxK(2) = EbO / Db0 - inv(Acs(Db0 / D2))
'   Ldev = Sqr((Dx / 2) ^ 2 - (DB / 2) ^ 2)
'        F(1) = AuxK(1) - AuxH(1)
'        F(2) = Sqr((DF / 2) ^ 2 - (DbI / 2) ^ 2) - Sqr((DF / 2) ^ 2 - (Db0 / 2) ^ 2) - Etip
 '  Sqr((DF / 2) ^ 2 - (DbI / 2) ^ 2) =  Sqr((DF / 2) ^ 2 - (Db0 / 2) ^ 2) + Etip
 ' Sqr((DF / 2) ^ 2 - (DbI / 2) ^ 2) -Etip =  Sqr((DF / 2) ^ 2 - (Db0 / 2) ^ 2)
 ' (Sqr((DF / 2) ^ 2 - (DbI / 2) ^ 2) -Etip)^2 =  (DF / 2) ^ 2 - (Db0 / 2) ^ 2
'(Db0 / 2) ^ 2= (DF / 2) ^ 2-(Sqr((DF / 2) ^ 2 - (DbI / 2) ^ 2) -Etip)^2
' Db0 = 2*Sqr((DF / 2) ^ 2-(Sqr((DF / 2) ^ 2 - (DbI / 2) ^ 2) -Etip)^2)
'    DbO = 2 * Sqr(-((Sqr((DF / 2) ^ 2 - (DbI / 2) ^ 2) - Etip) ^ 2 - (DF / 2) ^ 2))
'            ApRastApp = Atn(Tan(ApRast) / Cos(Helr))
'            DbRast = DrMola * Cos(ApRastApp)
 '                   Aux = EbRastOtt / DbRast - inv(Acs(DbRast / DatTesta))
 '                   ErastOtt = ((Atn(x / Y) + inv(Acs(DB1 / DrastOtt)) - inv(Acs(DB1 / DatTesta))) - Aux) * DB1 / 2
                    
Dim DL, DApO As Double
    DL = Sqr((DF / 2) ^ 2 - (DbI / 2) ^ 2) - Sqr((DI / 2) ^ 2 - (DbI / 2) ^ 2)
    DApO = Acs(DbI / DI) + inv(Acs(DbI / DI) + Atn(Etip / DL))
'    DbO = 2 * Sqr((DF / 2) ^ 2 - (Sqr((DF / 2) ^ 2 - (DbI / 2) ^ 2) - Etip) ^ 2) * 0.9

'    DbO = DbI * 0.9 ' - Etip
'    DbO = DF * Cos(ApO)
'    DbO = 2 * Sqr((DF / 2) ^ 2 - (Sqr((DF / 2) ^ 2 - (DbI / 2) ^ 2) - Etip) ^ 2) * Sin(DApO)
    ' DrI
    
'    DbO = DrMola * Cos(DApO) DrI
    DbO = DI * Cos(DApO)
    EbO = (EbI / DbI - inv(Acs(DbI / DI)) + inv(Acs(DbO / DI))) * DbO
        
        
        DApO = Etip / (DbI / 2) - (2 * inv(Acs(DbI / DI)) - inv(Acs(DbI / DF)) + EbI / DbI)
        
        
        
    DbO = DI * Cos(DApO + Acs(DbI / DI))
    EbO = (EbI / DbI - inv(Acs(DbI / DI)) + inv(Acs(DbO / DI))) * DbO
    
    
    'Etip / (DbI / 2) = Acs(DbO / DI) + 2 * inv(Acs(DbI / DI)) - inv(Acs(DbI / DF)) + EbI / DbI
    'Etip / (DbI / 2) -(2 * inv(Acs(DbI / DI)) - inv(Acs(DbI / DF)) + EbI / DbI)= Acs(DbO / DI)
    'Cos(Etip / (DbI / 2) -(2 * inv(Acs(DbI / DI)) - inv(Acs(DbI / DF)) + EbI / DbI)= DbO / DI
    'Cos(Etip / (DbI / 2) -(2 * inv(Acs(DbI / DI)) - inv(Acs(DbI / DF)) + EbI / DbI)*DI= DbO
'    DbO = Cos(Etip / (DbI / 2) - (2 * inv(Acs(DbI / DI)) - inv(Acs(DbI / DF)) + EbI / DbI)) * DI
'    EbO = (EbI / DbI - inv(Acs(DbI / DI)) + inv(Acs(DbO / DI))) * DbO
' 3
    Dim L1, A1, B1, L2 As Double
    
    L2 = Sqr((DF / 2) ^ 2 - (DbI / 2) ^ 2)
    L1 = L2 - Etip
    
    A1 = Asn(L1 / (DF / 2))
    B1 = PI / 2 - A1 ' Roll Angle
 '   B1 = A1 + inv(Acos(DbO / DF))
    
    DbO = DF / Sqr(1 + B1 ^ 2)
    
 '   DbO -DF / Sqr(1 + (A1 + inv(Acos(DbO / DF))) ^ 2) = 0
    
    'DbO = 2 * Sqr((DF / 2) ^ 2 - (L1) ^ 2)
    Dim i_max, j, i As Integer
    Dim DbT(1 To 3), F(1 To 3) As Double
    i_max = 20
    i = 0
    DbT(1) = DbI / 2
    DbT(2) = (DbT(1) + DbT(3)) / 2
    DbT(3) = DI
    
    Do While (i < i_max)
    
    For j = 1 To 3
    Call fDbO(DF, DI, EbI, DbI, Etip, DbT(j), F(j))
    Next j
    If (F(1) * F(2) < 0) Then
    DbT(1) = DbT(1): DbT(3) = DbT(2): DbT(2) = (DbT(1) + DbT(3)) / 2
    
    ElseIf (F(2) * F(3) < 0) Then
    DbT(1) = DbT(2): DbT(3) = DbT(3): DbT(2) = (DbT(1) + DbT(3)) / 2
    Else
    ' warnint: solution not bracket
    End If
    
    
    
    
    i = i + 1
    Loop
    
    DbO = DbT(3)
    EbO = (EbI / DbI - inv(Acs(DbI / DI)) + inv(Acs(DbO / DI))) * DbO

End Sub
Private Sub fDbO(ByRef DF, ByRef DI, ByRef EbI, ByRef DbI, ByRef Etip, ByRef DbT, ByRef F)
    Dim L1, A1, B1, L2, EbO As Double
Dim Test, Adev, Ldev, DB As Double

'-----------------
'   Aux = Eb / db - inv(Acs(db / dx))
'   If dis$ = "P" Then
'        x = dx * Sin(Aux) / 2
'        Y = dx * Cos(Aux) / 2
Dim Aux(1 To 2) As Double

'-----------------
Test = DbT / DI
Test = Acs(DbT / DI)
EbO = (EbI / DbI - inv(Acs(DbI / DI)) + inv(Acs(DbT / DI))) * DbT
Aux(1) = EbI / DbI - inv(Acs(DbI / DF))
Aux(2) = EbO / DbT - inv(Acs(DbT / DF))
'-----------------
    L2 = Sqr((DF / 2) ^ 2 - (DbI / 2) ^ 2)
    L1 = L2 - Etip
    A1 = Asn(L1 / (DF / 2))
 '    A1 = L1 / (DF / 2)
    Ldev = L1: DB = DbT
 
    Adev = Ldev / (DB / 2)
  '  B1 = 0 * A1 + 0 * Ainv(Acos(DbT / DF)) + Ainv(A1)
 '    B1 = 0 * EbO / DbT - inv(A1) 'L1 / (DbT / 2)
'    B1 = Tan(A1)
'     B1 = EbO / DbT + inv(A1)
    B1 = L2 / (DbI / 2) - Etip / (DbT / 2)
    
'    F = DbT - DF / Sqr(1 + B1 ^ 2)
    F = Aux(1) - Aux(2) - 2 * Etip / DbT
'    F = EbO / DbT - (EbI / DbI - inv(Acs(DbI / DF)) + inv(Acs(DbT / DF) - Etip / DF))
End Sub
Private Sub TgEvolv(Eb, DB, dx, X, Y, dis$)
   '  CALCUL UN POINT DE LA DEVELOPPANTE
   '  Donnees : Eb , Db , Dx
   '  Dis$ : "P" pour Pieno
   '          "V" pour Vano
   Dim Aux, DAuxDdx As Double
   
   Aux = Eb / DB - inv(Acs(DB / dx))
   DAuxDdx = -DinvD(Acs(DB / dx)) * DAcsD(DB / dx) * (-1 / dx ^ 2)
   
   If dis$ = "P" Then
        X = (1 / 2) * (Sin(Aux) + dx * (Cos(Aux)) * DAuxDdx)
        Y = (1 / 2) * (Cos(Aux) + dx * (-Sin(Aux)) * DAuxDdx)
   Else
        X = (1 / 2) * (Sin(PI / Z1 - Aux) + dx * (Cos(PI / Z1 - Aux)) * (-DAuxDdx))
        Y = (1 / 2) * (Cos(PI / Z1 - Aux) + dx * (-Sin(PI / Z1 - Aux)) * (-DAuxDdx))
   End If
End Sub
Private Sub NgEvolv(Eb, DB, dx, X, Y, dis$)
   '  CALCUL UN POINT DE LA DEVELOPPANTE
   '  Donnees : Eb , Db , Dx
   '  Dis$ : "P" pour Pieno
   '          "V" pour Vano
   Dim Aux, DAuxDdx, DDAuxDdx As Double

   Aux = Eb / DB - inv(Acs(DB / dx))
   DAuxDdx = -DinvD(Acs(DB / dx)) * DAcsD(DB / dx) * (-1 / dx ^ 2)
   
   If dis$ = "P" Then
        X = (1 / 2) * (Cos(Aux) * DAuxDdx + (Cos(Aux)) * DAuxDdx + dx * (-Sin(Aux)) * DAuxDdx ^ 2 + dx * (Cos(Aux)) * DDAuxDdx)
        Y = (1 / 2) * (-Sin(Aux) * DAuxDdx + (-Sin(Aux)) * DAuxDdx + dx * (-Cos(Aux)) * DAuxDdx ^ 2 + dx * (-Sin(Aux)) * DDAuxDdx)
   Else
        X = (1 / 2) * (Cos(PI / Z1 - Aux) * (-DAuxDdx) + (Cos(PI / Z1 - Aux)) * (-DAuxDdx) + dx * (-Sin(PI / Z1 - Aux)) * (DAuxDdx ^ 2) + dx * (Cos(PI / Z1 - Aux)) * (-DDAuxDdx))
        Y = (1 / 2) * (-Sin(PI / Z1 - Aux) * (-DAuxDdx) + (-Sin(PI / Z1 - Aux)) * (-DAuxDdx) + dx * (-Cos(PI / Z1 - Aux)) * (DAuxDdx ^ 2) + dx * (-Sin(PI / Z1 - Aux)) * (-DDAuxDdx))
   End If
End Sub
Public Sub Get_GearProfile_cp(ByRef cpX(), ByRef cpY(), ByRef cpZ())
Dim D, Eb, DB
Dim i As Integer
Dim dx As Double
Dim cpX0, cpY0 As Double

'Dim cpX(1 To 4) As Double
'Dim cpY(1 To 4) As Double
'Dim cpZ(1 To 4) As Double

D = 0.5
Eb = Eb1
DB = DB1

' punti raggio piede
' punti trocoide
' punti evolvente

    Call PntEvolv(Eb, DB, DatPiede, cpX0, cpY0, "P")
    
    For i = 1 To 4
    dx = (DatPiede + (i - 1) * (DE1 - DatPiede) / 3)
    Call PntEvolv(Eb, DB, dx, cpX(i), cpY(i), "P")
 '   cpY(i) = cpY(i) - cpY0
    cpZ(i) = -0.5 + ((i - 1) / 3)
    Next i
' punti raggio testa
' FINE TESTED

   Dim Aux, Aux1, A$, Xm, Ym, Pb9, Eb9, Aini, Atot, X, Y, X1, Y1, Arad ' i,
   Dim nVANI, ExNvani, PAS, YmMola, RX, dMin
   Dim XR, YR, Amax, Nbp As Integer
    
    Nbp = 4
'    Call CadreVide(-SpostX, -SpostY, LStamp, Hstamp, Uscita)
    'Nvani = 2
    Xm = 0
    Ym = DrMola / 2 + DedMola
'    SpostY = 0 '10
'    SpostX = 0 'LStamp / 2


    ' trocoide generata dal finaco bombato rullo ( LA RASTREMAZIONE cambia il range angolare)
    YR = YrfRul - DedMola
    XR = Sqr(RfiancoRul ^ 2 - YR ^ 2) + SpMola / 2
    ' estensione angolare raggio testa rullo
    If Rast$ = "O" Then
        Amax = PI / 2 - Asn((YrfRul - YrastRul) / RfiancoRul)
    Else
        Amax = PI / 2 - Asn((YrfRul - RtRul) / (RfiancoRul - RtRul))
    End If

    For i = 1 To Nbp
        Aux = AminEvolv + (Amax - AminEvolv) / (Nbp) * (i - 1)
        Call PntTrocFM(Helr, DrMola, Z1, -RfiancoRul, YR, XR, Aux, X, Y, "P", RX, VPB)
        cpX(i * 2 - 1) = (X - Xm)
        cpY(i * 2 - 1) = (Y - Ym)
        cpX(i * 2) = (X - Xm)
        cpY(i * 2) = (Y - Ym)
        
    '    cpZ(i + Nbp) = -0.5 + ((j - 1) / (2 * Nbp))
    Next i
    dMin = RX * 2
    
    ' trocoide genrata dal raggio testa rullo
    'Yr = RtRul - DedMola
    'Xr = MrMola * Pi / 2 - XrtRul - SpostRul / 2
    'For i = 1 To Nbp
    '    Aux = Amax / (Nbp) * (i - 1)
    '    Call PntTrocFm(Helr, DrMola, Z1, -RtRul, Yr, Xr, Aux, X, Y, "P", Rx, Vpb)
    '    cpX(i + Nbp) = (X - Xm)
    '    cpY(i + Nbp) = (Y - Ym)
    'Next i
    

    ' raggio esterno + evolvente (CASO RASTREMAZIONE)
    ' raggio esterno (CASO SENZA RASTREMAZIONE)
    
    If Rast$ = "O" Then
' Evolvente della rastremazione
    
    Dim D9, dMax  'Pas,
'    Nbp = 20
    dMin = RX * 2
    PAS = (dMax - dMin) / (Nbp - 1)
    D9 = dMin - PAS

    For i = 1 To Nbp
        D9 = D9 + PAS
        Call PntEvolv(EbRastOtt, DbRast, D9, X, Y, "P")
        cpX(i + Nbp) = (X - Xm)
        cpY(i + Nbp) = (Y - Ym)
    Next i
    
' Curconferenza testa
    Aux = EbRastOtt / DbRast - inv(Acs(DbRast / DE1))
'   Call DisegnoRayon(De1 / 2, 0, 0, -Pi / 2 + Aux, -Pi / 2 - Aux, Xm, Ym, 0, Uscita)
    Dim TH, Afin
        
    Aini = -PI / 2 + Aux
    Afin = -PI / 2 - Aux
    PAS = (Afin - Aini) / (Nbp - 1)
    For i = 1 To Nbp
        TH = Aini + PAS * (i - 1)
        Call PntRayon(DE1 / 2, 0, 0, 0, TH, X, Y)
        cpX(i + Nbp * 2) = (X - Xm)
        cpY(i + Nbp * 2) = (Y - Ym)
    Next i
    
    Else
' Curconferenza testa
        Aux = Eb1 / DB1 - inv(Acs(DB1 / DE1))
        'Call DisegnoRayon(De1 / 2, 0, 0, -Pi / 2 + Aux, -Pi / 2 - Aux, Xm, Ym, 0, Uscita)
        Aini = (-PI / 2 + Aux) * 1
        Afin = (-PI / 2 - Aux) * 1
        PAS = (Afin - Aini) / (Nbp - 1)
        PAS = (-Aux) / (Nbp - 1)
        For i = 1 To Nbp
            TH = Aini + PAS * (i - 1)
            Call PntRayon(DE1 / 2, 0, 0, 0, TH, X, Y)
            cpX(i * 2 - 1 + Nbp * 2) = (X - Xm)
            cpY(i * 2 - 1 + Nbp * 2) = (Y - Ym)
            cpX(i * 2 + Nbp * 2) = (X - Xm)
            cpY(i * 2 + Nbp * 2) = (Y - Ym)
            
        Next i

    End If
    
    Dim j As Integer
    
    For j = 1 To Nbp * 2
        cpZ(j * 2 - 1) = -0.5 + ((j - 1) / (2 * Nbp - 1))
        cpZ(j * 2) = -0.5 + ((j - 1) / (2 * Nbp - 1))
    Next j
    
GoTo 10
' Circonferenza piede
        'Call DisegnoRayon(Di1 / 2, 0, 0, -Pi / 2 + Pi / Z1, -Pi / 2 - Pi / Z1, Xm, Ym, 0, Uscita)
        Aini = -PI / 2 + PI / Z1
        Afin = -PI / 2 - PI / Z1
        PAS = (Afin - Aini) / (Nbp - 1)
        For i = 1 To Nbp
            TH = Aini + PAS * (i - 1)
            Call PntRayon(DE1 / 2, 0, 0, 0, TH, X, Y)
            cpX(i) = (X - Xm)
            cpY(i) = (Y - Ym)
        Next i
        
10:
End Sub
Private Sub Get_Rc_Fha(ByRef XYer, ByRef R, ByRef fHa)
    Dim Eb, DB As Double
    Dim XYet(1 To 2) As Double
    Dim nXYer(1 To 2) As Double
'    Dim XYer(1 To 2) As Double
    Dim Delta(1 To 2) As Double
    Dim F(1 To 3) As Double
    Dim FP As Double
    Dim Rc(1 To 3) As Double
    Dim eps As Double
    Dim icount, icountMax As Integer
    Dim Ri As Double
    
    icountMax = 35
    Eb = Eb1: DB = DB1
    
    eps = 0.0000001
    
    Ri = Sqr(XYer(1) ^ 2 + XYer(2) ^ 2)
    Rc(1) = Sqr(XYer(1) ^ 2 + XYer(2) ^ 2)
    Call PntEvolv(Eb, DB, 2 * Rc(1), XYet(1), XYet(2), "P")
    Delta(1) = XYer(1) - XYet(1)
    Delta(2) = XYer(2) - XYet(2)
    Call NgEvolv(Eb, DB, 2 * Rc(1), nXYer(1), nXYer(2), "P")
    F(1) = Delta(1) * nXYer(2) - Delta(2) * nXYer(1)
    
    Rc(2) = Rc(1) + eps
    Call PntEvolv(Eb, DB, 2 * Rc(2), XYet(1), XYet(2), "P")
    Delta(1) = XYer(1) - XYet(1)
    Delta(2) = XYer(2) - XYet(2)
    Call NgEvolv(Eb, DB, 2 * Rc(2), nXYer(1), nXYer(2), "P")
    F(2) = Delta(1) * nXYer(2) - Delta(2) * nXYer(1)
    FP = (F(2) - F(1)) / eps
    
   For icount = 0 To icountMax
    Rc(3) = Rc(1) - F(2) / FP
    Rc(1) = Rc(3)
   
    Call PntEvolv(Eb, DB, 2 * Rc(1), XYet(1), XYet(2), "P")
    Delta(1) = XYer(1) - XYet(1): Delta(2) = XYer(2) - XYet(2)
    Call NgEvolv(Eb, DB, 2 * Rc(1), nXYer(1), nXYer(2), "P")
    F(1) = Delta(1) * nXYer(2) - Delta(2) * nXYer(1)
    
    Rc(2) = Rc(1) + eps

    Call PntEvolv(Eb, DB, 2 * Rc(2), XYet(1), XYet(2), "P")
    Delta(1) = XYer(1) - XYet(1): Delta(2) = XYer(2) - XYet(2)
    
    Call NgEvolv(Eb, DB, 2 * Rc(2), nXYer(1), nXYer(2), "P")
    F(2) = Delta(1) * nXYer(2) - Delta(2) * nXYer(1)
    FP = (F(2) - F(1)) / eps
    
   Next icount
   
   
   R = Rc(3)
   Dim Sign As Double
   
   If (Delta(1) > 0) Then  'And Delta(2) > 0
   Sign = -1
   Else
   Sign = 1
   End If
   
   fHa = Sign * Sqr(Delta(1) ^ 2 + Delta(2) ^ 2)
   
End Sub

Public Sub Get_GearRollingProfile_cp(Np, ByRef cpXr(), ByRef cpYr(), ByRef cpZr())

Dim cpX(1 To Nsample)
Dim cpY(1 To Nsample)
Dim cpZ(1 To Nsample)
Dim XYer(1 To 2) As Double
Dim XYm(1 To 2) As Double
Dim fHa As Double
Dim RR As Double


Call Get_ProfiloControllato(Np, cpX, cpY, cpZ)
    XYm(1) = 0
    XYm(2) = DrMola / 2 + DedMola
  
    For i = 1 To Np
    XYer(1) = cpX(i) + XYm(1): XYer(2) = cpY(i) + XYm(2)
        Call Get_Rc_Fha(XYer, RR, fHa)
      '  Lr = Sqr(RR ^ 2 - db ^ 2)
        cpXr(i) = fHa * ScalaFha - SpMola / 2
        cpYr(i) = RR - XYm(2)
    Next i
    
End Sub
Private Sub Get_Nominal_Gear_Specs(ByRef API, ByRef DrI, ByRef DbI, ByRef HelbI, ByRef EbI, ByRef EarI, ByRef SpMolaI, ByRef DedGear)
'    Dim apI, DbI, DrI, HelbI, AuxI, EbI, EarI, SpMolaI, DedGear
    Dim AuxI As Double
    API = Atn(Tan(Fndr(Aprd)) / Cos(Fndr(HelpD)))
    DrI = Mr * Z1 / Cos(Fndr(HelpD))
    DbI = DrI * Cos(API)
    
    HelbI = Atn(Tan(Fndr(HelpD)) * Cos(API)) 'Fndr(Aprd)
    AuxI = PI * DbI / Z1
    EbI = (Wdt / Cos(HelbI)) - (Kdt - 1) * AuxI
    EarI = (EbI / DbI - inv(API)) * DrI
    
    SpMolaI = PI * Mr - EarI * Cos(Fndr(HelpD))
    DedGear = DrI / 2 - DatPiede / 2  'DedGear = DE1 / 2 - DrI / 2 '+ GioccoFondoMola
    
End Sub
Private Sub Get_Nominal_Gear_Specs_Dentato(ByRef API, ByRef DrI, ByRef DbI, ByRef HelbI, ByRef EbI, ByRef EarI, ByRef SpMolaI, ByRef DedGear, Optional apDen As Double = 0)
'    Dim apI, DbI, DrI, HelbI, AuxI, EbI, EarI, SpMolaI, DedGear
    Dim AuxI As Double
    Dim Allowance As Double
    Dim DrDen As Double
    
    ' INVARIANTI
    API = Atn(Tan(Fndr(Aprd)) / Cos(Fndr(HelpD)))
    Allowance = Lp("SOVRMETRAD_G") * Tan(API) * 2
    DrI = Mr * Z1 / Cos(Fndr(HelpD))
    DbI = DrI * Cos(API)
    HelbI = Atn(Tan(Fndr(HelpD)) * Cos(API)) 'Fndr(Aprd)
    AuxI = PI * DbI / Z1
    EbI = ((Wdt + Allowance) / Cos(HelbI)) - (Kdt - 1) * AuxI
    '
    If (apDen = 0) Then
    EarI = (EbI / DbI - inv(API)) * DrI
    Else
    DrDen = DbI / Cos(apDen)
    EarI = (EbI / DbI - inv(apDen)) * DrDen
    End If
    SpMolaI = PI * Mr - EarI * Cos(Fndr(HelpD))
    DedGear = DrI / 2 - DatPiede / 2  'DedGear = DE1 / 2 - DrI / 2 '+ GioccoFondoMola
    
End Sub
Public Sub Get_GearRollingProfile_cp_toll(NbpIN, ByRef cpXtol1(), ByRef cpYtol1(), ByRef cpZtol1(), ByRef cpXtol2(), ByRef cpYtol2(), ByRef cpZtol2())

Dim cpX1(1 To Nsample)
Dim cpY1(1 To Nsample)
Dim cpZ1(1 To Nsample)
Dim cpX2(1 To Nsample)
Dim cpY2(1 To Nsample)
Dim cpZ2(1 To Nsample)
Dim XYer(1 To 2) As Double
Dim XYm(1 To 2) As Double
Dim fHa As Double
Dim RR As Double
Dim Nbp As Integer
Dim IsTipPresent As Boolean

Call Get_GearProfile_cp_Ref(NbpIN, cpX1, cpY1, cpZ1)


    Dim API, DbI, DrI, HelbI, AuxI, EbI, EarI, SpMolaI, DedGear As Double
    Call Get_Nominal_Gear_Specs(API, DrI, DbI, HelbI, EbI, EarI, SpMolaI, DedGear)
    
    XYm(1) = 0
   ' XYm(2) = DrI / 2 + DedGear + GioccoFondoMola
  XYm(2) = DrMola / 2 + DedMola
    For i = 1 To NbpIN
    XYer(1) = cpX1(i) + XYm(1): XYer(2) = cpY1(i) + XYm(2)
        Call Get_Rc_Fha(XYer, RR, fHa)
      '  Lr = Sqr(RR ^ 2 - db ^ 2)
        cpXtol1(i) = fHa * ScalaFha - SpMolaI / 2 - FhaToll * ScalaFha / 2
        cpYtol1(i) = RR - XYm(2)
        cpXtol2(i) = fHa * ScalaFha - SpMolaI / 2 + FhaToll * ScalaFha / 2
        cpYtol2(i) = cpYtol1(i)
    Next i
    
End Sub
Function Get_Amax(Optional dx As Double = 0)
    
    Dim X(0 To 2), Y(0 To 2), TH(0 To 2) As Double
    Dim Xm, Ym As Double
    Dim XR, YR As Double
    Dim Amax  As Double
    Dim i, j As Integer
    Dim Xt, Yt As Double
    Dim Invert As Boolean
    Dim Dfin As Double
    
    If (dx = 0) Then
    Dfin = DE1
    Else
    Dfin = dx
    End If
    
    Invert = True
    
    
    Xm = 0
    Ym = DrMola / 2 + DedMola

    YR = YrfRul - DedMola
    XR = Sqr(RfiancoRul ^ 2 - YR ^ 2) + SpMola / 2

    Call PntRayon(Dfin / 2, 0, 0, 0, (-PI / 2 + Eb1 / DB1 - inv(Acs(DB1 / Dfin))), Xt, Yt)
    Xt = (Xt - Xm)
    Yt = (Yt - Ym)

    ' estensione angolare raggio testa rullo
    If Rast$ = "O" Then
        Amax = PI / 2 - Asn((YrfRul - YrastRul) / RfiancoRul)
    Else
        Amax = PI / 2 - Asn((YrfRul - RtRul) / (RfiancoRul + RtRul))
    End If
    
    TH(0) = AminEvolv
    TH(2) = Amax
    TH(1) = (TH(0) + TH(2)) / 2
    
    Dim IsDetect As Boolean
    IsDetect = False
'    While (Not IsDetect)
        For i = 0 To 2
            Call PntTrocFM(Helr, DrMola, Z1, -RfiancoRul, YR, XR, TH(i), X(i), Y(i), "P", RX, VPB)
            X(i) = X(i) - Xm
            Y(i) = Y(i) - Ym
        Next i
    If (((X(0) - Xt) * (X(1) - Xt)) < 0) Then
        TH(2) = TH(1)
        IsDetect = True
    ElseIf (((X(2) - Xt) * (X(1) - Xt)) < 0) Then
        TH(0) = TH(1)
        IsDetect = True
    Else
     TH(0) = TH(2)
         IsDetect = True
    End If
'    Wend
    
    
    TH(1) = (TH(0) + TH(2)) / 2
    
 '   Get_Amax = TH(1)
 '   Return
   '         Aux = AminEvolv + ((Amax - AminEvolv) / (Nbp - 1)) * (i - 1)
   '          Call PntTrocFM(Helr, DrMola, Z1, -RfiancoRul, YR, XR, TH(i), x(i), Y(i), "P", RX, VPB)
   '          x(i) = x(i) - Xm
    '         Y(i) = Y(i) - Ym
   ' Circonferenza testa
   '     Aux = Eb1 / DB1 - inv(Acs(DB1 / DE1))
        'Call DisegnoRayon(De1 / 2, 0, 0, -Pi / 2 + Aux, -Pi / 2 - Aux, Xm, Ym, 0, Uscita)
   '     Aini = (-PI / 2 + Aux) * 1
   '     Afin = (-PI / 2 - Aux) * 1
      '  PAS = (Afin - Aini) / (Nbp - 1)
    '    PAS = (-Aux) / (Nbp - 1)

   '     For i = 1 To Nbp
   '         TH = Aini + PAS * (i - 1)
   '         Call PntRayon(DE1 / 2, 0, 0, 0, TH, x, Y)
   '         cpX(i + Nbp) = (x - Xm)
   '         cpY(i + Nbp) = (Y - Ym)
   '     Next i
   
   
'   Dim TH(1 To 2) As Double ' Raggio testa, raggio trocoide
'   Dim FUNC(1 To 2) As Double
'   TH(1) = 0
'   TH(2) = 0
    Dim THh As Double: Dim AuxH As Double: Dim Ainih As Double
    Dim XYh(1 To 2) As Double
    Dim THt(1 To 3) As Double: Dim Auxt As Double: Dim Ainit As Double
    
    If (Rast$ = "O" And (DrastOtt < DatTesta)) Then
    AuxH = EbRastOtt / DbRast - inv(Acs(DbRast / Dfin))
    Ainih = -PI / 2 + AuxH
    THh = Ainih
    Call PntRayon(Dfin / 2, 0, 0, 0, THh, XYh(1), XYh(2))
    THt(1) = TH(1)
    
'    Dim D9, dMax  'Pas,
'    dMax = DatTesta 'DmaxEvolv
'    Nbp = 20
'    dMin = RX * 2
'    PAS = (dMax - dMin) / (Nbp - 1)
'    D9 = dMin - PAS

'    For i = 1 To Nbp
'        D9 = D9 + PAS
'        Call PntEvolv(EbRastOtt, DbRast, D9, x, Y, "P")
'        cpX(i + Nbp) = (x - Xm)
'        cpY(i + Nbp) = (Y - Ym)
'    Next i







    Get_Amax = TH(1)
 '    X_TIP_MOLA = Y_TIP_MOLA
    Else
   Dim F(1 To 3) As Double: Dim FP As Double

   
   Dim XYT(1 To 2) As Double
   
   Dim eps As Double
   
   eps = 0.000001
   AuxH = Eb1 / DB1 - inv(Acs(DB1 / Dfin))
   Ainih = (-PI / 2 + AuxH) * 1
   THh = Ainih
   Call PntRayon(Dfin / 2, 0, 0, 0, THh, XYh(1), XYh(2))
   

   THt(1) = TH(1)
   Call PntTrocFM(Helr, DrMola, Z1, -RfiancoRul, YR, XR, THt(1), XYT(1), XYT(2), "P", RX, VPB)

   F(1) = Sqr((XYh(1) - XYT(1)) ^ 2 + (XYh(2) - XYT(2)) ^ 2)
   THt(2) = THt(1) + eps
   
   Call PntTrocFM(Helr, DrMola, Z1, -RfiancoRul, YR, XR, THt(2), XYT(1), XYT(2), "P", RX, VPB)
   F(2) = Sqr((XYh(1) - XYT(1)) ^ 2 + (XYh(2) - XYT(2)) ^ 2)
   FP = (F(2) - F(1)) / eps
   
   Dim icount As Integer
   Dim icountMax As Integer
   icountMax = 40
   
   For icount = 0 To icountMax
   THt(3) = THt(1) - F(2) / FP
   THt(1) = THt(3)
   Call PntTrocFM(Helr, DrMola, Z1, -RfiancoRul, YR, XR, THt(1), XYT(1), XYT(2), "P", RX, VPB)
   F(1) = Sqr((XYh(1) - XYT(1)) ^ 2 + (XYh(2) - XYT(2)) ^ 2)
   THt(2) = THt(1) + eps
   Call PntTrocFM(Helr, DrMola, Z1, -RfiancoRul, YR, XR, THt(2), XYT(1), XYT(2), "P", RX, VPB)
   F(2) = Sqr((XYh(1) - XYT(1)) ^ 2 + (XYh(2) - XYT(2)) ^ 2)
   FP = (F(2) - F(1)) / eps
 '  icount = icount + 1
   Next icount
   Get_Amax = THt(3)
   End If
End Function
Public Sub Get_ProfiloControllato(ByVal NbpIN As Integer, ByRef cpX(), ByRef cpY(), ByRef cpZ())
Dim D, Eb, DB
Dim i As Integer
Dim dx As Double
Dim cpX0, cpY0 As Double
Dim Nbp As Integer
If ((Rast$ = "O") And (DrastOtt < DatTesta)) Then
Nbp = Int(NbpIN / 3)
Else
Nbp = Int(NbpIN / 2)
End If
'Dim cpX(1 To 4) As Double
'Dim cpY(1 To 4) As Double
'Dim cpZ(1 To 4) As Double

D = 0.5
Eb = Eb1
DB = DB1

' punti raggio piede
' punti trocoide
' punti evolvente
    
' punti raggio testa
' FINE TESTED

   Dim Aux, Aux1, A$, Xm, Ym, Pb9, Eb9, Aini, Atot, X, Y, X1, Y1, Arad  As Double
   Dim nVANI, ExNvani
   Dim PAS, YmMola, RX, dMin As Double
  ' Dim XR, YR As Integer
   Dim YR, XR, Amax As Double
    
'    Nbp = 4
'    Call CadreVide(-SpostX, -SpostY, LStamp, Hstamp, Uscita)
    ' Nvani = 2
    Xm = 0
    Ym = DrMola / 2 + DedMola
'    SpostY = 0 '10
'    SpostX = 0 'LStamp / 2

    ' trocoide generata dal finaco bombato rullo ( LA RASTREMAZIONE cambia il range angolare)
    YR = YrfRul - DedMola
    XR = Sqr(RfiancoRul ^ 2 - YR ^ 2) + SpMola / 2
    ' estensione angolare raggio testa rullo
    If ((Rast$ = "O") And (DrastOtt < DatTesta)) Then
        Amax = PI / 2 - Asn((YrfRul - YrastRul) / RfiancoRul)
       Amax = Get_Amax(DatTesta)
    Else
        Amax = PI / 2 - Asn((YrfRul - RtRul) / (RfiancoRul + RtRul))
      '  Amax = PI / 2 - Atn((YrfRul - RtRul) / (RfiancoRul - RtRul))
      Amax = Get_Amax(DatTesta)
    End If

    For i = 1 To Nbp
        Aux = AminEvolv + ((Amax - AminEvolv) / (Nbp - 1)) * (i - 1)
        Call PntTrocFM(Helr, DrMola, Z1, -RfiancoRul, YR, XR, Aux, X, Y, "P", RX, VPB)
        cpX(i) = (X - Xm)
        cpY(i) = (Y - Ym)
    Next i
    
    dMin = RX * 2
    dMin = DrastOtt
    ' trocoide genrata dal raggio testa rullo
    'Yr = RtRul - DedMola
    'Xr = MrMola * Pi / 2 - XrtRul - SpostRul / 2
    'For i = 1 To Nbp
    '    Aux = Amax / (Nbp) * (i - 1)
    '    Call PntTrocFm(Helr, DrMola, Z1, -RtRul, Yr, Xr, Aux, X, Y, "P", Rx, Vpb)
    '    cpX(i + Nbp) = (X - Xm)
    '    cpY(i + Nbp) = (Y - Ym)
    'Next i
    

    ' raggio esterno + evolvente (CASO RASTREMAZIONE)
    ' raggio esterno (CASO SENZA RASTREMAZIONE)
    
    If ((Rast$ = "O") And (DrastOtt < DatTesta)) Then
' Evolvente della rastremazione
    
    Dim D9, dMax  'Pas,
    dMax = DatTesta 'Deap interfaccia
'    Nbp = 20
'    dMin = RX * 2
    dMin = DrastOtt
    PAS = (dMax - dMin) / (Nbp - 1)
    D9 = dMin - PAS

    For i = 1 To Nbp
        D9 = dMin + PAS * (i - 1) 'D9 + PAS
        Call PntEvolv(EbRastOtt, DbRast, D9, X, Y, "P")
        cpX(i + Nbp) = (X - Xm)
        cpY(i + Nbp) = (Y - Ym)
    Next i
'
' Circonferenza EAP 'testa
    Aux = EbRastOtt / DbRast - inv(Acs(DbRast / DatTesta)) 'DE1
'   Call DisegnoRayon(De1 / 2, 0, 0, -Pi / 2 + Aux, -Pi / 2 - Aux, Xm, Ym, 0, Uscita)
    Dim TH, Afin

    Aini = -PI / 2 + Aux
    Afin = (-PI / 2 - Aux * 0)
    PAS = (Afin - Aini) / (Nbp - 1)
    For i = 1 To Nbp
        TH = Aini + PAS * (i - 1)
        Call PntRayon(DatTesta / 2, 0, 0, 0, TH, X, Y) 'DE1
        cpX(i + Nbp * 2) = (X - Xm)
        cpY(i + Nbp * 2) = (Y - Ym)
    Next i

    Else
' Circonferenza testa EAP
        Aux = Eb1 / DB1 - inv(Acs(DB1 / DatTesta)) 'DE1
        'Call DisegnoRayon(De1 / 2, 0, 0, -Pi / 2 + Aux, -Pi / 2 - Aux, Xm, Ym, 0, Uscita)
        Aini = (-PI / 2 + Aux) * 1
        Afin = (-PI / 2 - Aux * 0)
      '  PAS = (Afin - Aini) / (Nbp - 1)
        PAS = (-Aux) / (Nbp - 1)

        For i = 1 To Nbp
            TH = Aini + PAS * (i - 1)
            Call PntRayon(DatTesta / 2, 0, 0, 0, TH, X, Y) 'DE1
            cpX(i + Nbp) = (X - Xm)
            cpY(i + Nbp) = (Y - Ym)
        Next i

    End If
    
    Dim j As Integer
    
 '   For j = 1 To Nbp * 2
 '       cpZ(j * 2 - 1) = -0.5 + ((j - 1) / (2 * Nbp - 1))
 '       cpZ(j * 2) = -0.5 + ((j - 1) / (2 * Nbp - 1))
 '   Next j
    
GoTo 10
' Circonferenza piede
        'Call DisegnoRayon(Di1 / 2, 0, 0, -Pi / 2 + Pi / Z1, -Pi / 2 - Pi / Z1, Xm, Ym, 0, Uscita)
        Aini = -PI / 2 + PI / Z1
        Afin = -PI / 2 - PI / Z1
        PAS = (Afin - Aini) / (Nbp - 1)
        For i = 1 To Nbp
            TH = Aini + PAS * (i - 1)
            Call PntRayon(Di1 / 2, 0, 0, 0, TH, X, Y)
            cpX(i) = (X - Xm)
            cpY(i) = (Y - Ym)
        Next i
        
10:
End Sub
Public Sub Get_GearProfile_cp_Ref(ByVal NbpIN As Integer, ByRef cpX(), ByRef cpY(), ByRef cpZ())

Dim D, Eb, DB, dx, PAS, cpX0, cpY0, dMax, dMin As Double
Dim TH, Aini, Afin, Aux As Double
Dim XYm(1 To 2), XY(1 To 2) As Double

Dim i, Nbp As Integer
Dim IsTipPresent As Boolean

Dim EbO, DbO As Double

Dim j As Double
    Dim API, DbI, DrI, HelbI, AuxI, EbI, EarI, SpMolaI, DedGear As Double
    Call Get_Nominal_Gear_Specs(API, DrI, DbI, HelbI, EbI, EarI, SpMolaI, DedGear)
'-------
    XYm(1) = 0
    DedGear = DrI / 2 - DatPiede / 2  'DE1 / 2 - DrI / 2 '+ GioccoFondoMola
    
    XYm(2) = DrMola / 2 + DedMola 'DrI / 2 + DedMola + GioccoFondoMola
'-----
If (TipRelifDiamRef < DatTesta And TipRelifAmplRef > 0) Then
IsTipPresent = True
Else
IsTipPresent = False
End If
'---
If (IsTipPresent) Then
    Nbp = Int(NbpIN / 3)
    Call Get_Eb_Db_from_Evolv(EbI, DbI, DrI, TipRelifDiamRef, DatTesta, TipRelifAmplRef, EbO, DbO)
    j = 0: Eb = EbI: DB = DbI: dMin = DatPiede: dMax = TipRelifDiamRef
Else
    Nbp = Int(NbpIN / 2)
    j = 0: Eb = EbI: DB = DbI: dMin = DatPiede: dMax = DatTesta 'DE1
End If
'    D = 0.5: Eb = Eb1: Db = DB1

    GoSub EVOLVENTE ' evolvente principale


If (IsTipPresent) Then
    j = 1: Eb = EbO: DB = DbO: dMax = DatTesta: dMin = TipRelifDiamRef
    GoSub EVOLVENTE
End If

' Circonferenza testa

If (IsTipPresent) Then
    Aux = EbO / DbO - inv(Acs(DbO / dMax)): j = 2
Else
    Aux = EbI / DbI - inv(Acs(DbI / dMax)): j = 1
End If
    Aini = -PI / 2 + Aux
    Afin = (-PI / 2 - Aux * 0)
    PAS = (Afin - Aini) / (Nbp - 1)
    For i = 1 To Nbp
        TH = Aini + PAS * (i - 1)
        Call PntRayon(dMax / 2, 0, 0, 0, TH, XY(1), XY(2))
        cpX(i + Nbp * j) = (XY(1) - XYm(1))
        cpY(i + Nbp * j) = (XY(2) - XYm(2))
    Next i
    
Exit Sub
    
EVOLVENTE:
    PAS = (dMax - dMin) / (Nbp - 1)
    dx = dMin - PAS
    For i = 1 To Nbp
        dx = dMin + PAS * (i - 1) 'D9 + PAS
        Call PntEvolv(Eb, DB, dx, XY(1), XY(2), "P")
        cpX(i + Nbp * j) = (XY(1) - XYm(1))
        cpY(i + Nbp * j) = (XY(2) - XYm(2))
    Next i
Return

End Sub
Public Sub Get_GearProfile_cp_Dentato(ByVal NbpIN As Integer, ByRef cpX(), ByRef cpY(), ByRef cpZ())

 Dim D, Eb, DB, dx, PAS, cpX0, cpY0, dMax, dMin As Double
 Dim TH, Aini, Afin, Aux As Double
 Dim XYm(1 To 2), XY(1 To 2) As Double
 Dim i, Nbp As Integer
 Dim IsTipPresent As Boolean
 Dim EbO, DbO As Double
 Dim j As Double ' SPANS
 Dim API, DbI, DrI, HelbI, AuxI, EbI, EarI, SpMolaI, DedGear As Double
 Dim Rtd, XYc(1 To 2), XYtr(1 To 2), RX, Apx, ApMn, ApMx, ApTr, ApMi As Double
 ' TEST
 Dim DrDen, EarD, HelpDen, ApnD, MnD, XcDen, YcDen As Double
 Dim apDen As Double: Dim ProDen As Double: Dim Rtden  As Double: Dim Deltaden As Double
 Dim SOVMETRAD As Double
 
 
 Call Get_Nominal_Gear_Specs(API, DrI, DbI, HelbI, EbI, EarI, SpMolaI, DedGear)
'-------
 XYm(1) = 0
 DedGear = DrI / 2 - DatPiede / 2  'DE1 / 2 - DrI / 2 '+ GioccoFondoMola
 XYm(2) = DrMola / 2 + DedMola 'DrI / 2 + DedMola + GioccoFondoMola
'-----
' If (TipRelifDiamRef < DatTesta And TipRelifAmplRef > 0) Then
'  IsTipPresent = True
' Else
  IsTipPresent = False
' End If
 
'---TROCOIDE DENTATURA Deltaden_Ref Deltaden
SOVMETRAD = Lp("SOVRMETRAD_G")


 Call N3x3(apDen, ProDen, Rtden, Deltaden)
 If (Deltaden < Deltaden_Ref Or SOVMETRAD_REF <> SOVMETRAD) Then
 apDen_Ref = apDen: ProDen_Ref = ProDen: Rtden_Ref = Rtden: Deltaden_Ref = Deltaden
 Else
 apDen = apDen_Ref: ProDen = ProDen_Ref: Rtden = Rtden_Ref: Deltaden = Deltaden_Ref
 End If
 If (SOVMETRAD_REF <> SOVMETRAD) Then SOVMETRAD_REF = SOVMETRAD
 
 Call Get_Nominal_Gear_Specs_Dentato2(API, DrI, DbI, HelbI, EbI, EarI, SpMolaI, DedGear, DrDen, EarD, HelpDen, ApnD, MnD, XcDen, YcDen, apDen, ProDen, Rtden, 2)
 'Call Get_Nominal_Gear_Specs_Dentato(apI, DrI, DbI, HelbI, EbI, EarI, SpMolaI, DedGear)
'---------------------------------------------------
 If (IsTipPresent) Then
  Nbp = Int(NbpIN / 4)
 Else
  Nbp = Int(NbpIN / 3)
 End If
 j = 0 ': Eb = EbI: db = DbI: dMin = DatPiede: dMax = DatTesta 'DE1
 GoSub TROCOIDE

'---EVOLVENTE
' Call Get_Nominal_Gear_Specs(apI, DrI, DbI, HelbI, EbI, EarI, SpMolaI, DedGear)
 If (IsTipPresent) Then
  Nbp = Int(NbpIN / 4)
  Call Get_Eb_Db_from_Evolv(EbI, DbI, DrI, TipRelifDiamRef, DatTesta, TipRelifAmplRef, EbO, DbO)
  j = 1: Eb = EbI: DB = DbI: dMin = DatPiede: dMax = TipRelifDiamRef
 Else
  Nbp = Int(NbpIN / 3)
  j = 1: Eb = EbI: DB = DbI: dMin = DatPiede: dMax = DE1 'DatTesta
 End If
'    D = 0.5: Eb = Eb1: Db = DB1

 GoSub EVOLVENTE ' evolvente principale


If (IsTipPresent) Then
    j = 2: Eb = EbO: DB = DbO: dMax = DatTesta: dMin = TipRelifDiamRef
    GoSub EVOLVENTE
End If

' Circonferenza testa

If (IsTipPresent) Then
    j = 3: Aux = EbO / DbO - inv(Acs(DbO / dMax))
Else
    j = 2: Aux = EbI / DbI - inv(Acs(DbI / dMax))
End If
    Aini = -PI / 2 + Aux
    Afin = (-PI / 2 - Aux * 0)
    PAS = (Afin - Aini) / (Nbp - 1)
    For i = 1 To Nbp
        TH = Aini + PAS * (i - 1)
        Call PntRayon(dMax / 2, 0, 0, 0, TH, XY(1), XY(2))
        cpX(i + Nbp * j) = (XY(1) - XYm(1))
        cpY(i + Nbp * j) = (XY(2) - XYm(2))
    Next i
    
Exit Sub
    
EVOLVENTE:
    PAS = (dMax - dMin) / (Nbp - 1)
    dx = dMin - PAS
    For i = 1 To Nbp
        dx = dMin + PAS * (i - 1) 'D9 + PAS
        Call PntEvolv(Eb, DB, dx, XY(1), XY(2), "P")
        cpX(i + Nbp * j) = (XY(1) - XYm(1))
        cpY(i + Nbp * j) = (XY(2) - XYm(2))
    Next i
Return
'--------------------------------------------------------------------------------------------
TROCOIDE:
'DrDen, EarD, HelpDen, ApnD, MnD, XcDen, YcDen, apDen, ProDen, RtDen
 ApMi = -(PG / 2 - apDen) * 0: ApMx = PG / 2 - API + 0 * API: PAS = (ApMx - ApMi) / (Nbp - 1) ': Rtd = 0.01
 'PG * Mr / (4 * Cos(HelpD * PG / 180)) -
 'XYc(1) = -(Mr * PG / 4 - (EarI / 2 + (((DrI - Di1) / 2 - Rtd * (1 - Sin(apI))) * Tan(apI) + Rtd * Cos(apI))) / Cos(Fndr(HelpD)))
 'XYc(2) = (DrI - Di1) / 2 - Rtd + 0 * Di1 / 2
 XYc(1) = XcDen: XYc(2) = YcDen
 ' Trova Rtd ottimale e m rotolamento affinche PntTrc(-ApMx) sia vicino pnto sap dentatura
 'if(XcDen)
 For i = 1 To Nbp
  ApTr = ApMi + PAS * (i - 1)
  Call PntTroc(HelpDen, DrDen, Z1, Rtden, YcDen, Abs(XcDen), ApTr, XYtr(1), XYtr(2), "P", RX, Apx)
  cpX(i + Nbp * j) = (XYtr(1) - XYm(1))
  cpY(i + Nbp * j) = (XYtr(2) - XYm(2))
 Next i
Return

End Sub
Private Sub Get_Nominal_Gear_Specs_Dentato2(ByRef API, ByRef DrI, ByRef DbI, ByRef HelbI, ByRef EbI, ByRef EarI, ByRef SpMolaI, ByRef DedGear, ByRef DrDen, ByRef EarDen, ByRef HelpDed, ByRef ApnDen, ByRef MnDen, ByRef XcDen, ByRef YcDen, ByRef apDen, ByRef ProDen, ByRef Rtden, vid)
'    Dim apI, DbI, DrI, HelbI, AuxI, EbI, EarI, SpMolaI, DedGear
    Dim AuxI As Double
    Dim Allowance As Double
    Dim Shift As Double
 '   Dim DrDen As Double
    
    ' INVARIANTI DI RUOTA
    API = Atn(Tan(Fndr(Aprd)) / Cos(Fndr(HelpD)))
    Allowance = Lp("SOVRMETRAD_G") * Tan(API) '* 0 ' * 2
    DrI = Mr * Z1 / Cos(Fndr(HelpD))
    DbI = DrI * Cos(API)
    HelbI = Atn(Tan(Fndr(HelpD)) * Cos(API)) 'Fndr(Aprd)
    AuxI = PI * DbI / Z1
    EbI = ((Wdt + Allowance * 2) / Cos(HelbI)) - (Kdt - 1) * AuxI
    EarI = (EbI / DbI - inv(API)) * DrI
    SpMolaI = PI * Mr - EarI * Cos(Fndr(HelpD))
    DedGear = DrI / 2 - DatPiede / 2  'DedGear = DE1 / 2 - DrI / 2 '+ GioccoFondoMola
    '
 '   Dim DrD, EarD, HelpDed, ApnD, MnD, XcD, YcD As Double
    Dim rDx, rDn, ProDx, ProDn, Apdx, ApDn As Double
    Dim Ricalcola As Double
    Dim i, ix As Integer
   
   Dim XYtr(1 To 2) As Double
   Dim XYev(1 To 2) As Double
   Dim RX, Apx As Double
   
    i = 0: ix = 5
    Shift = 0#
Calcola:
    Ricalcola = False
    ' VARIANTI DI DENTATURA
    DrDen = DbI / Cos(apDen) ' Diametro rotolamento di dentatura.
    EarDen = (EbI / DbI - inv(apDen)) * DrDen ' Spessore di dentatura
    HelpDed = Tan(HelbI) * Cos(apDen) ' Helica dentatura
    ApnDen = Atn(Tan(apDen) * Cos(HelpDed)) ' angolo pressione nomral edi dentatura
    MnDen = DrDen / Z1 ' Modulo rotolamento ruota
    ' Approssimazione: dovrei mettere angolo pressione protuberanza
' ORI    XcD = -(MnD * PG / 4 - (EarD / 2 + (-ProD * Cos(apDen) + ((DrDen - Di1) / 2 - Rtd * (1 - Sin(ApnD))) * Tan(ApnD) + Rtd * Cos(ApnD))) / Cos(HelpDed)) ' Ascissa centro tip dentatura
'Rtden = (Cos(HelpDed) * EarDen / 2 + ((-ProDen * Cos(ApnDen) + ((DrI - Di1) / 2) * Tan(ApnDen))) / Cos(0 * HelpDed)) / ((Cos(ApnDen) - (1 - Sin(ApnDen)) * Tan(ApnDen)) / Cos(HelpDed * 0))
    XcDen = -(PG * Mr / vid - (Cos(HelpDed) * EarDen / 2 + (-ProDen * Cos(ApnDen) + ((DrDen - Di1) / 2 - Rtden * (1 - Sin(ApnDen))) * Tan(ApnDen) + Rtden * Cos(ApnDen))) / Cos(HelpDed * 0)) ' Ascissa centro tip dentatura
    YcDen = (DrDen - Di1) / 2 - Rtden ' Ordinata centro tip dentatura
    
    ' CONSTRAINS (- Rtd * (1 - Sin(ApnD)) * Tan(ApnD)+ Rtd * Cos(ApnD))/ Cos(Fndr(HelpD))' -(Rtd * (Cos(ApnD)-  (1 - Sin(ApnD)) * Tan(ApnD))/ Cos(Fndr(HelpD))) =' EarD / 2 + ((-pro * Cos(Apden) + ((DrD - Di1) / 2 ) * Tan(ApnD) )) / Cos(Fndr(HelpD))
    rDn = 0.02: rDx = (Cos(HelpDed) * EarDen / vid + ((-ProDen * Cos(ApnDen) + ((DrDen - Di1) / 2) * Tan(ApnDen))) / Cos(0 * HelpDed)) / ((Cos(ApnDen) - (1 - Sin(ApnDen)) * Tan(ApnDen)) / Cos(HelpDed * 0))
    ProDx = Allowance: ProDn = 0
    Apdx = API: ApDn = 5 * PG / 180
    Call PntTroc(HelpDed, DrDen, Z1, Rtden, YcDen, XcDen, PG / 2 - apDen, XYtr(1), XYtr(2), "P", RX, Apx)
    Call PntEvolv(EbI, DbI, DatPiede, XYev(1), XYev(2), "P")
    
    Ricalcola = Rtden > rDx Or Rtden < rDn Or ProDen > ProDx Or ProDen < ProDn Or apDen > API Or apDen < ApDn 'Or XYtr(2) - XYev(2) > 0
   Dim Num As Double
   Num = ((10) * Rnd + 1) / 10
   If (Rtden > rDx Or Rtden < rDn) Then Rtden = (rDx - rDn) * Num + rDn ' If (Rtden > rDx) Then Rtden = rDx / 2: If (Rtden < rDn) Then Rtden = rDn
   If (ProDen > ProDx Or ProDen < ProDn) Then ProDen = (ProDx - ProDn) * Num + ProDn ': If (ProDen > ProDx) Then ProDen = ProDx: If (ProDen < ProDn) Then ProDen = ProDn
   If (apDen > API Or apDen < ApDn) Then apDen = (Apdx - ApDn) * Num + ApDn 'If (apDen > apI) Then apDen = ApDx: If (apDen < ApDn) Then apDen = ApDn * ((10) * Rnd + 1) / 10 + ApDn

   

   If (XYtr(2) - XYev(2) > 0) Then
'   Rtden = (RX - DatPiede / 2)
'   apDen = ApDx
'   shift = -PG * Mr / 4
'    apDen = ApDx: Rtden = rDn: shift = 0 * PG * Mr / 4
'   XcDen = -(PG * Mr / 4 - (Cos(HelpDed) * EarDen / 2 + (-ProDen * Cos(ApnDen) + ((DrDen - Di1) / 2 - Rtden * (1 - Sin(ApnDen))) * Tan(ApnDen) + Rtden * Cos(ApnDen))) / Cos(HelpDed * 0)) ' Ascissa centro tip dentatura
'   GoTo Calcola
   Else
   ' shift = 0#
   End If
   
   If (Ricalcola And i < ix) Then
    i = i + 1: GoTo Calcola
   End If
   
   If (i = ix And Ricalcola) Then GoTo IterationError
   '
   Exit Sub
   '
IterationError:
   WRITE_DIALOG "Get_Nominal_Gear_Specs_Dentato2 -> Iteration ERROR(01)"
   Rtden = (rDn + rDx) / 2: ProDen = (ProDn + ProDx) / 2: apDen = (Apdx + ApDn) / 2
   Exit Sub
   
End Sub
'---------------------------------------------------------------------------------Distanza_Dentatura
Private Function DD(ByRef apDen, ByRef ProDen, ByRef Rtden) As Double
 Dim API, DrI, DbI, HelbI, EbI, EarI, SpMolaI, DedGear As Double
 Dim DrDen, EarD, HelpDen, ApnD, MnD, XcDen, YcDen As Double 'apDen,, ProD, RtDen
 Dim RX, Apx As Double
 
 Dim XYtr(1 To 2) As Double
 Dim XYev(1 To 2) As Double
 ' C.I.
 'apDen = 30 * PG / 180: ProD = 1: RtDen = 1
 Call Get_Nominal_Gear_Specs_Dentato2(API, DrI, DbI, HelbI, EbI, EarI, SpMolaI, DedGear, DrDen, EarD, HelpDen, ApnD, MnD, XcDen, YcDen, apDen, ProDen, Rtden, 2)
 'MsgBox "ApDen = " & Apden & "; ProD = " & ProD & "; Rtd = " & Rtd, vbCritical
' Call pntTR(RtDen, apDen, HelpDen, DrDen, XcDen, YcDen, XYtr)
 Call PntTroc(HelpDen, DrDen, Z1, Rtden, YcDen, XcDen, PG / 2 - apDen, XYtr(1), XYtr(2), "V", RX, Apx)
 Call PntEvolv(EbI, DbI, DatPiede, XYev(1), XYev(2), "P")
 
 DD = Sqr((XYtr(1) - XYev(1)) ^ 2 + (XYtr(2) - XYev(2)) ^ 2)
 
End Function
'---------------------------------------------------------------------------------F_Dentatura
Private Function Fd(ByRef apDen, ByRef ProDen, ByRef Rtden) As Double()
 Dim API, DrI, DbI, HelbI, EbI, EarI, SpMolaI, DedGear As Double
 Dim DrDen, EarD, HelpDen, ApnD, MnD, XcDen, YcDen As Double 'apDen,, ProD, RtDen
 Dim RX, Apx As Double
 Dim tmp(0 To 2) As Double
 Dim Aw As Double
 
 Dim XYtr(1 To 2) As Double: Dim XYtrA(1 To 2) As Double
 Dim XYev(1 To 2) As Double: Dim XYevA(1 To 2) As Double
' C.I.
' apDen = 30 * PG / 180: ProD = 1: RtDen = 1
 Call Get_Nominal_Gear_Specs_Dentato2(API, DrI, DbI, HelbI, EbI, EarI, SpMolaI, DedGear, DrDen, EarD, HelpDen, ApnD, MnD, XcDen, YcDen, apDen, ProDen, Rtden, 2)
' MsgBox "ApDen = " & Apden & "; ProD = " & ProD & "; Rtd = " & Rtd, vbCritical
' Call pntTR(RtDen, apDen, HelpDen, DrDen, XcDen, YcDen, XYtr)

 Call PntTroc(HelpDen, DrDen, Z1, Rtden, YcDen, XcDen, PG / 2 - apDen, XYtr(1), XYtr(2), "P", RX, Apx)
' Call PntTroc(HelpDen, DrDen, Z1, Rtden, YcDen, XcDen, (PG / 2 - apDen), XYtrA(1), XYtrA(2), "P", RX, Apx)
 Call PntEvolv(EbI, DbI, DatPiede, XYev(1), XYev(2), "P")
 Call TgEvolv(EbI, DbI, DatPiede, XYevA(1), XYevA(2), "P")
 Call TgTroc(HelpDen, DrDen, Z1, Rtden, YcDen, XcDen, PG / 4, XYtrA(1), XYtrA(2), "P")
' DD = Sqr((XYtr(1) - XYev(1)) ^ 2 + (XYtr(2) - XYev(2)) ^ 2)
 Aw = Lp("SOVRMETRAD_G") * Tan(API)  ' * 2

' tg 30 � massima
 tmp(0) = XYtr(1) - XYev(1) '+ Aw * 0 ' Tmp(0) = Abs(XYtr(1)) - Abs(XYev(1)) + Aw * 0
 tmp(1) = (PG / 4) * Mr - Abs(XcDen) ' raggio full rouded
' Tmp(2) = XYtrA(1) * XYevA(2) - XYtrA(2) * XYevA(1) ' Tmp(2) = Abs(XYtr(2)) - Abs(XYev(2))'+ Aw  'Tmp(0) 'Abs(XYtrA(1)) - Abs(XYev(1)) + Aw 'XYtr(1) * XYev(2) + XYev(1) * XYtr(2)
 tmp(2) = XYtr(2) - XYev(2)
 
 Fd = tmp

End Function
'---------------------------------------------------------------------------------F_Dentatura
Private Function fDa(ByRef apDen, ByRef ProDen, ByRef Rtden) As Double()
 Dim API, DrI, DbI, HelbI, EbI, EarI, SpMolaI, DedGear As Double
 Dim DrDen, EarD, HelpDen, ApnD, MnD, XcDen, YcDen As Double 'apDen,, ProD, RtDen
 Dim RX, Apx As Double
 Dim tmp(0 To 1) As Double
 Dim Aw As Double
 
 Dim XYtr(1 To 2) As Double: Dim XYtrA(1 To 2) As Double
 Dim XYev(1 To 2) As Double: Dim XYevA(1 To 2) As Double
 Dim Shift As Double
 Shift = 0
' C.I.
' apDen = 30 * PG / 180: ProD = 1: RtDen = 1
RIPETI:
 Call Get_Nominal_Gear_Specs_Dentato2(API, DrI, DbI, HelbI, EbI, EarI, SpMolaI, DedGear, DrDen, EarD, HelpDen, ApnD, MnD, XcDen, YcDen, apDen, ProDen, Rtden, 2)
' MsgBox "ApDen = " & Apden & "; ProD = " & ProD & "; Rtd = " & Rtd, vbCritical
' Call pntTR(RtDen, apDen, HelpDen, DrDen, XcDen, YcDen, XYtr)

 Call PntTroc(HelpDen, DrDen, Z1, Rtden, YcDen, XcDen, PG / 2 - API, XYtr(1), XYtr(2), "P", RX, Apx) ' apI
' Call PntTroc(HelpDen, DrDen, Z1, Rtden, YcDen, XcDen, (PG / 2 - apDen), XYtrA(1), XYtrA(2), "P", RX, Apx)
 Call PntEvolv(EbI, DbI, DatPiede, XYev(1), XYev(2), "P")
' Call TgEvolv(EbI, DbI, DatPiede, XYevA(1), XYevA(2), "P")
' Call TgTroc(HelpDen, DrDen, Z1, Rtden, YcDen, XcDen, PG / 4, XYtrA(1), XYtrA(2), "P")
' DD = Sqr((XYtr(1) - XYev(1)) ^ 2 + (XYtr(2) - XYev(2)) ^ 2)
 Aw = Lp("SOVRMETRAD_G") * Tan(API)  ' * 2

' tg 30 � massima
 tmp(0) = XYtr(1) - XYev(1) '+ Aw * 0 ' Tmp(0) = Abs(XYtr(1)) - Abs(XYev(1)) + Aw * 0
' Tmp(1) = 0 * (PG / 2) * Mr / Cos(Fndr(HelpD)) - Abs(XcDen) 'Tmp(0) 'Abs(XYtr(2)) - Abs(XYev(2)) ' (PG / 4) * Mr - Abs(XcDen) ' raggio full rouded DrDen / Z1
' Tmp(2) = XYtrA(1) * XYevA(2) - XYtrA(2) * XYevA(1) ' Tmp(2) = Abs(XYtr(2)) - Abs(XYev(2))'+ Aw  'Tmp(0) 'Abs(XYtrA(1)) - Abs(XYev(1)) + Aw 'XYtr(1) * XYev(2) + XYev(1) * XYtr(2)
' Tmp(2) = XYtr(2) - XYev(2)
' Tmp(0) = Tmp(1)
 '- (PG / 16) * Mr / Cos(Fndr(HelpD))
 tmp(1) = Abs(XcDen) ' - 0 * PG * Mr / 4
If (XYtr(2) - XYev(2) > 0) Then
'Tmp(1) = Rtden - (RX - Di1 / 2)  'Tmp(1) + (XYtr(2) - XYev(2))

'Rtden = 0.02
'apDen = apI 'Fndr(20)
'shift = -(PG / 4) * Mr
'GoTo ripeti
Else
' Tmp(1) = Abs(XcDen)
'shift = 0
End If
' Tmp(1) = shift + Abs(XcDen)   '/ Cos(Fndr(HelpD))
 fDa = tmp

End Function
'---------------------------------------------------------------------------------fp_Dentatura
'Function fDp(ByRef A, ByRef P, ByRef R) As Double()

' Dim D0 As Double
' Dim E As Double: E = 0.00000001
' Dim Tmp(0 To 2) As Double
' D0 = fD(A, P, R)
' Tmp(0) = (fD(A + E, P, R) - D0) / E
' Tmp(1) = (fD(A, P + E, R) - D0) / E
' Tmp(2) = (fD(A, P, R + E) - D0) / E
''Jd(0) = tmp(0): Jd(1) = tmp(1): Jd(2) = tmp(2) 'Jd = tmp '
'fDp = Tmp
'End Function
'---------------------------------------------------------------------------------fpp_Dentatura
Function fDp(ByRef A, ByRef P, ByRef R) As Double()
 Dim fDp0() As Double
 Dim D0() As Double
 Dim E As Double: E = 0.00000001
 Dim tmp(0 To 8) As Double
 
  D0 = Fd(A, P, R)
'fDp0 = fDp(A, P, R)
 
 tmp(0) = (Fd(A + E, P, R)(0) - D0(0)) / E
 tmp(1) = (Fd(A, P + E, R)(0) - D0(0)) / E
 tmp(2) = (Fd(A, P, R + E)(0) - D0(0)) / E
 tmp(3) = (Fd(A + E, P, R)(1) - D0(1)) / E
 tmp(4) = (Fd(A, P + E, R)(1) - D0(1)) / E
 tmp(5) = (Fd(A, P, R + E)(1) - D0(1)) / E
 tmp(6) = (Fd(A + E, P, R)(2) - D0(2)) / E
 tmp(7) = (Fd(A, P + E, R)(2) - D0(2)) / E
 tmp(8) = (Fd(A, P, R + E)(2) - D0(2)) / E
 
 fDp = tmp
End Function
Function fDap(ByRef A, ByRef P, ByRef R) As Double()
 Dim fDap0() As Double
 Dim Da0() As Double
 Dim E As Double: E = 0.00000001
 Dim tmp(0 To 5) As Double
 
  Da0 = fDa(A, P, R)
'fDp0 = fDp(A, P, R)
 
 tmp(0) = (fDa(A + E, P, R)(0) - Da0(0)) / E
 tmp(2) = (fDa(A, P + E, R)(0) - Da0(0)) / E
 tmp(4) = (fDa(A, P, R + E)(0) - Da0(0)) / E
 tmp(1) = (fDa(A + E, P, R)(1) - Da0(1)) / E
 tmp(3) = (fDa(A, P + E, R)(1) - Da0(1)) / E
 tmp(5) = (fDa(A, P, R + E)(1) - Da0(1)) / E
 
 fDap = tmp
End Function
'---------------------------------------------------------------------------------Jacobiano_Dentatura
Function Jd(ByRef A, ByRef P, ByRef R) As Double()

 Dim D0 As Double
 Dim E As Double: E = 0.00000001
 Dim tmp(0 To 2) As Double
 D0 = DD(A, P, R)
 tmp(0) = (DD(A + E, P, R) - D0) / E
 tmp(1) = (DD(A, P + E, R) - D0) / E
 tmp(2) = (DD(A, P, R + E) - D0) / E
'Jd(0) = tmp(0): Jd(1) = tmp(1): Jd(2) = tmp(2) 'Jd = tmp '
Jd = tmp
End Function
'---------------------------------------------------------------------------------Hessiana_Dentatura
Function Hd(ByRef A, ByRef P, ByRef R) As Double()
 Dim Jd0() As Double
 Dim D0 As Double
 Dim E As Double: E = 0.00000001
 Dim tmp(0 To 8) As Double
 
 D0 = DD(A, P, R)
 Jd0 = Jd(A, P, R)
 
 tmp(0) = (DD(A + E, P, R) - 2 * D0 + DD(A - E, P, R)) / (E) ^ 2
 tmp(1) = (Jd(A, P + E, R)(0) - Jd0(0)) / E
 tmp(2) = (Jd(A, P, R + E)(0) - Jd0(0)) / E
 tmp(3) = (Jd(A + E, P, R)(1) - Jd0(1)) / E
 tmp(4) = (DD(A, P + E, R) - 2 * D0 + DD(A, P - E, R)) / (E) ^ 2
 tmp(5) = (Jd(A, P, R + E)(1) - Jd0(1)) / E
 tmp(6) = (Jd(A + E, P, R)(2) - Jd0(2)) / E
 tmp(7) = (Jd(A, P + E, R)(2) - Jd0(2)) / E
 tmp(8) = (DD(A, P, R + E) - 2 * D0 + DD(A, P, R - E)) / (E) ^ 2
 
 Hd = tmp
End Function
'---------------------------------------------------------------------------------Inverse 3x3
Function I3x3(ByRef M) As Double()
 Dim det As Double
 Dim i As Integer
 Dim tE(0 To 8) As Double
 '  0  1  2  3    0 1 2
 '  4  5  6  7 => 3 4 5
 '  8  9 10 11    6 7 8
 ' 12 13 14 15
 
 tE(0) = M(8) * M(4) - M(5) * M(7)
 tE(1) = -M(8) * M(1) + M(2) * M(7)
 tE(2) = M(5) * M(1) - M(2) * M(4)
 tE(3) = -M(8) * M(3) + M(5) * M(6)
 tE(4) = M(8) * M(0) - M(2) * M(6)
 tE(5) = -M(5) * M(0) + M(2) * M(3)
 tE(6) = M(7) * M(3) - M(4) * M(6)
 tE(7) = -M(7) * M(0) + M(1) * M(6)
 tE(8) = M(4) * M(0) - M(1) * M(3)

 det = M(0) * tE(0) + M(1) * tE(3) + M(2) * tE(6)
 If (det = 0) Then
  WRITE_DIALOG "inverse3x3: can't invert matrix, determinant is 0"
  Exit Function
 End If
 
 For i = 0 To 8
  tE(i) = tE(i) / det
 Next i
 I3x3 = tE
 
End Function
Function I3x3a(ByRef M, ByRef Sing) As Double()
 Dim det As Double
 Dim i As Integer
 Dim tE(0 To 8) As Double
 '  0  1  2  3    0 1 2
 '  4  5  6  7 => 3 4 5
 '  8  9 10 11    6 7 8
 ' 12 13 14 15
 Sing = False
 tE(0) = M(8) * M(4) - M(5) * M(7)
 tE(1) = -M(8) * M(1) + M(2) * M(7)
 tE(2) = M(5) * M(1) - M(2) * M(4)
 tE(3) = -M(8) * M(3) + M(5) * M(6)
 tE(4) = M(8) * M(0) - M(2) * M(6)
 tE(5) = -M(5) * M(0) + M(2) * M(3)
 tE(6) = M(7) * M(3) - M(4) * M(6)
 tE(7) = -M(7) * M(0) + M(1) * M(6)
 tE(8) = M(4) * M(0) - M(1) * M(3)

 det = M(0) * tE(0) + M(1) * tE(3) + M(2) * tE(6)
 If (det = 0) Then
  Sing = True
  WRITE_DIALOG "inverse3x3: can't invert matrix, determinant is 0"
  Exit Function
 End If
 
 For i = 0 To 8
  tE(i) = tE(i) / det
 Next i
 I3x3a = tE
 
End Function
Function I3x3b(ByRef M, ByRef Sing) As Double()
 Dim det As Double
 Dim i As Integer
 Dim tE(0 To 8) As Double
 '  0  1  2  3    0 1 2
 '  4  5  6  7 => 3 4 5
 '  8  9 10 11    6 7 8
 ' 12 13 14 15
 Sing = False
 tE(0) = M(8) * M(4) - M(5) * M(7)
 tE(3) = -M(8) * M(1) + M(2) * M(7)
 tE(6) = M(5) * M(1) - M(2) * M(4)
 tE(1) = -M(8) * M(3) + M(5) * M(6)
 tE(4) = M(8) * M(0) - M(2) * M(6)
 tE(7) = -M(5) * M(0) + M(2) * M(3)
 tE(2) = M(7) * M(3) - M(4) * M(6)
 tE(5) = -M(7) * M(0) + M(1) * M(6)
 tE(8) = M(4) * M(0) - M(1) * M(3)

 det = M(0) * tE(0) + M(1) * tE(3) + M(2) * tE(6)
 If (det = 0) Then
  Sing = True
  WRITE_DIALOG "inverse3x3: can't invert matrix, determinant is 0"
  Exit Function
 End If
 
 For i = 0 To 8
  tE(i) = tE(i) / det
 Next i
 I3x3b = tE
 
End Function
'---------------------------------------------------------M3xeT
Private Function Mt(ByRef M) As Double()
 Dim det As Double
 Dim i As Integer
 Dim tE(0 To 5) As Double
 
' tE(0) = M(0)
' tE(1) = M(3)
' tE(2) = M(1)
' tE(3) = M(4)
' tE(4) = M(2)
' tE(5) = M(5)
 
 tE(0) = M(0)
 tE(1) = M(2)
 tE(2) = M(4)
 tE(3) = M(1)
 tE(4) = M(3)
 tE(5) = M(5)
 
 Mt = tE
End Function
'---------------------------------------------------------M3x2x2x3
Private Function Mq(M1, M2) As Double()
Dim i, j, k As Integer
Dim tE(0 To 8) As Double
'For k = 0 To 2
' For i = 0 To 2
'  tE(k + i * 3) = 0
'  For j = 0 To 1
'   tE(k + i * 3) = tE(k + i * 3) + M2(j + 2 * i) * M2(i + 2 * j) 'j + 2 * i   i + 2 * j
'  Next j
' Next i
'Next k

tE(0) = M1(0) * M2(0) + M1(3) * M2(1)
tE(1) = M1(1) * M2(0) + M1(4) * M2(1)
tE(2) = M1(2) * M2(0) + M1(5) * M2(1)
'
tE(3) = M1(0) * M2(2) + M1(3) * M2(3)
tE(4) = M1(1) * M2(2) + M1(4) * M2(3)
tE(5) = M1(2) * M2(2) + M1(5) * M2(3)
'
tE(6) = M1(0) * M2(4) + M1(3) * M2(5)
tE(7) = M1(1) * M2(4) + M1(4) * M2(5)
tE(8) = M1(1) * M2(4) + M1(5) * M2(5)
Mq = tE
End Function
'-----------------------------------------------------------------------------Pseudo inversa
Private Function I3x2a(ByRef M, ByRef Sing) As Double()
 Dim det As Double
 Dim i As Integer
 Dim tE() As Double
 Dim Mq0() As Double
 Dim Mt0() As Double
 Dim iMq0() As Double
 
 Mt0 = Mt(M)
 Mq0 = Mq(Mt0, M)
 iMq0 = I3x3a(Mq0, Sing) 'I3x3b  Mq0 simmetrica
 '( At*A )-1 * At
 If (Not Sing) Then
    tE = Mul2(iMq0, Mt0)
    I3x2a = tE
 End If
 
End Function
'---------------------------------------------------------------------------------Norm2_Jacobiano
Function n2Jd(ByRef A, ByRef P, ByRef R) As Double
 Dim Jd0() As Double '1 To 3As Double
 Jd0 = Jd(A, P, R)
 n2Jd = Jd0(0) ^ 2 + Jd0(1) ^ 2 + Jd0(1) ^ 2
End Function
'---------------------------------------------------------------------------------Divergenza_Jacobiano_Distanza
Function Jn2Jd(ByRef A, ByRef P, ByRef R) As Double()
 Dim n2Jd0 As Double
 Dim E As Double: E = 0.00000001
 Dim tmp(0 To 2) As Double
 n2Jd0 = n2Jd(A, P, R)
 tmp(0) = (n2Jd(A + E, P, R) - n2Jd0) / E
 tmp(1) = (n2Jd(A, P + E, R) - n2Jd0) / E
 tmp(2) = (n2Jd(A, P, R + E) - n2Jd0) / E
 
 Jn2Jd = tmp
End Function
'---------------------------------------------------------------------------------Hessiana_Jacobiano_Distanza
Function Hn2Jd(ByRef A, ByRef P, ByRef R) As Double()
 Dim Jn2Jd0() As Double
 Dim E As Double: E = 0.00000001
 Dim tmp(0 To 8) As Double
 Jn2Jd0 = Jn2Jd(A, P, R)
 
 tmp(0) = (Jn2Jd(A + E, P, R)(0) - Jn2Jd0(0)) / E
 tmp(1) = (Jn2Jd(A, P + E, R)(0) - Jn2Jd0(0)) / E
 tmp(2) = (Jn2Jd(A, P, R + E)(0) - Jn2Jd0(0)) / E
 tmp(3) = (Jn2Jd(A + E, P, R)(1) - Jn2Jd0(1)) / E
 tmp(4) = (Jn2Jd(A, P + E, R)(1) - Jn2Jd0(1)) / E
 tmp(5) = (Jn2Jd(A, P, R + E)(1) - Jn2Jd0(1)) / E
 tmp(6) = (Jn2Jd(A + E, P, R)(2) - Jn2Jd0(2)) / E
 tmp(7) = (Jn2Jd(A, P + E, R)(2) - Jn2Jd0(2)) / E
 tmp(8) = (Jn2Jd(A, P, R + E)(2) - Jn2Jd0(2)) / E
Hn2Jd = tmp
End Function
'---------------------------------------------------------------------------------Inversa Hessiana_Jacobiano_Distanza
Function iHn2Jd(ByRef A, ByRef P, ByRef R) As Double()
 Dim Hn2Jd0() As Double
 Dim tmp() As Double
 Hn2Jd0 = Hn2Jd(A, P, R)
 tmp = I3x3(Hn2Jd0)
 iHn2Jd = tmp
End Function
Function iHd(ByRef A, ByRef P, ByRef R) As Double()
 Dim Hd0() As Double
 Dim tmp() As Double
 Hd0 = Hd(A, P, R)
 tmp = I3x3(Hd0)
 iHd = tmp
End Function
Function ifDp(ByRef A, ByRef P, ByRef R) As Double()
 Dim fDp0() As Double
 Dim tmp() As Double
 fDp0 = fDp(A, P, R)
 tmp = I3x3(fDp0)
 ifDp = tmp
End Function
'---------------------------------------------------------------------------------
Function Mul(A, B) As Double()
Dim i, j, iA, jA, iB, jB As Integer
Dim tmp() As Double
'iA = UBound(A, 0): jA = UBound(A, 1)
'iB = UBound(B, 0) ': jB = UBound(B, 1)
iA = 2: jA = 2: iB = 2
ReDim tmp(jA)

' Check for inconsinstence input matrixs
For j = 0 To jA
 For i = 0 To iB
  tmp(j) = tmp(j) + A(i + j * 3) * B(i) '+ A(1 + i * 3) * B(1) + A(2 + i * 3) * B(2)
 Next i
Next j

Mul = tmp
End Function
Function Mul1(A, B) As Double()
Dim i, j, iA, jA, iB, jB As Integer
Dim tmp() As Double
'iA = UBound(A, 0): jA = UBound(A, 1)
'iB = UBound(B, 0) ': jB = UBound(B, 1)
iA = 2: jA = 2: iB = 2
ReDim tmp(jA)

' Check for inconsinstence input matrixs
'For j = 0 To jA
 For i = 0 To iB
  tmp(i) = A(i) * B(i)   '+ A(1 + i * 3) * B(1) + A(2 + i * 3) * B(2)
 Next i
'Next j

Mul1 = tmp
End Function
'---------------------------------------------------------------------------------(3x3)x(3x2)
Function Mul2(A, B) As Double()
Dim i, j, iA, jA, iB, jB As Integer
Dim tmp(0 To 5) As Double
'iA = UBound(A, 0): jA = UBound(A, 1)
'iB = UBound(B, 0) ': jB = UBound(B, 1)
iA = 2: jA = 2: iB = 2
'ReDim Tmp(jA)

' Check for inconsinstence input matrixs
'For j = 0 To jA
' For i = 0 To iB
'  Tmp(j) = Tmp(j) + A(i + j * 3) * B(j + i * 3)
' Next i
'Next j
tmp(0) = A(0) * B(0) + A(3) * B(1) + A(6) * B(2)
tmp(1) = A(0) * B(3) + A(3) * B(4) + A(6) * B(5)
tmp(2) = A(1) * B(0) + A(4) * B(1) + A(7) * B(2)
tmp(3) = A(1) * B(3) + A(4) * B(4) + A(7) * B(5)
tmp(4) = A(2) * B(0) + A(5) * B(1) + A(8) * B(2)
tmp(5) = A(2) * B(3) + A(5) * B(4) + A(8) * B(5)
Mul2 = tmp
End Function
'-----------------------------------------------(3x2)(2x1)
Function Mul3(A, B) As Double()
Dim i, j, iA, jA, iB, jB As Integer
Dim tmp(0 To 2) As Double
'iA = UBound(A, 0): jA = UBound(A, 1)
'iB = UBound(B, 0) ': jB = UBound(B, 1)
'iA = 2: jA = 2: iB = 2
'ReDim Tmp(jA)

' Check for inconsinstence input matrixs
'For j = 0 To jA
' For i = 0 To iB
'  Tmp(j) = Tmp(j) + A(i + j * 3) * B(i) '+ A(1 + i * 3) * B(1) + A(2 + i * 3) * B(2)
' Next i
'Next j

tmp(0) = A(0) * B(0) + A(3) * B(1)
tmp(1) = A(1) * B(0) + A(4) * B(1)
tmp(2) = A(2) * B(0) + A(5) * B(1)

Mul3 = tmp

End Function
'---------------------------------------------------------------------------------
Function sum(A, B) As Double()
 Dim i As Integer
 Dim tmp(0 To 2) As Double
 For i = 0 To 2
  tmp(i) = A(i) + B(i)
 Next i
 sum = tmp
End Function
'---------------------------------------------------------------------------------
Function Sut(A, B) As Double()
 Dim i As Integer
 Dim tmp(0 To 2) As Double
 For i = 0 To 2
  tmp(i) = A(i) - B(i)
 Next i
 Sut = tmp
End Function
'---------------------------------------------------------------------------------
Sub N3x3(ByRef apDen As Double, ByRef ProDen As Double, ByRef Rtden As Double, ByRef Deltaden As Double)
' Dim apDen: Dim ProDen: Dim RtDen
 Dim i, ix, j, Jx As Integer
' Dim func, func1, func2 As Double
' Dim dfunc(1 To 3), dfunc1(1 To 3), dfunc2(1 To 3) As Double
 Dim apr0(0 To 2)
 Dim apr1(0 To 2)
 Dim aprB(0 To 2)
 Dim Dapr(0 To 2)
 Dim fDp0() As Double
 Dim iFDp0() As Double
 
 Dim F0() As Double:  Dim F1() As Double
 
 Dim tmp() As Double
 Dim E As Double: E = 0.00000001
 Dim ha(0 To 2) As Double: Dim hb() As Double
 Dim eps, epsB As Double
 Dim singular As Boolean
 
 i = 0: j = 0
 ix = 50: Jx = 25
 Dapr(0) = 0: Dapr(1) = 0: Dapr(2) = 0
 
 epsB = 10
guess:
 apr0(0) = Apr / 2 + Dapr(0): apr0(1) = 0.0001 + Dapr(1): apr0(2) = 0.02 + Dapr(2)
 apr1(0) = Apr / 2 + Dapr(0): apr1(1) = 0.02 + Dapr(1): apr1(2) = 0.05 + Dapr(2)
 j = j + 1
 ha(0) = E: ha(1) = E: ha(2) = E
 singular = False
 F0 = fDa(apr0(0), apr0(1), apr0(2)): eps = Sqr((F0(0)) ^ 2 + (F0(1)) ^ 2)
 If (eps < epsB) Then
  epsB = eps: aprB(0) = apr0(0): aprB(1) = apr0(1): aprB(2) = apr0(2)
 End If
For i = 1 To ix
' GoTo 10
 F0 = fDa(apr0(0), apr0(1), apr0(2))
 fDp0 = fDap(apr0(0), apr0(1), apr0(2))
 iFDp0 = I3x2a(fDp0, singular) 'I3x2a
'  GoTo 10
 If (singular) Then
  Dapr(0) = Dapr(0) + 0.002: Dapr(2) = Dapr(2) + 0.1
  If (j < Jx) Then
   GoTo guess
  Else
   GoTo Error
  End If
 End If
 
 hb = Mul3(iFDp0, F0)

 apr1(0) = Abs(apr0(0) - hb(0)): apr1(1) = Abs(apr0(1) - hb(1)): apr1(2) = Abs(apr0(2) - hb(2)) 'apr1 = Sut(apr0, hb)
 
 F1 = fDa(apr1(0), apr1(1), apr1(2))
 eps = Sqr((F1(0) - F1(0) * 0) ^ 2 + (F1(1) - F1(1) * 0) ^ 2)  'eps = Sqr((F0(0) - F1(0)) ^ 2 + (F0(1) - F1(1)) ^ 2 + (F0(2) - F1(2)) ^ 2)
 
 If (eps < epsB) Then
  epsB = eps: aprB(0) = apr1(0): aprB(1) = apr1(1): aprB(2) = apr1(2)
 End If
 If (eps < 0.000001) Then GoTo Convergence
 
 apr0(0) = apr1(0): apr0(1) = apr1(1): apr0(2) = apr1(2)
' apr1(0) = apr0(0) + ha(0): apr1(1) = apr0(1) + ha(1): apr1(2) = apr0(2) + ha(2)
 
Next i
Convergence:
' apDen = Abs(apr1(0)): ProDen = Abs(apr1(1)): Rtden = Abs(apr1(2))
 Dim API, DrI, DbI, HelbI, EbI, EarI, SpMolaI, DedGear As Double
 Dim DrDen, EarD, HelpDen, ApnD, MnD, XcDen, YcDen As Double 'apDen,, ProD, RtDen
 Dim RX, Apx As Double
 
 Dim XYtr(1 To 2) As Double
 Dim XYev(1 To 2) As Double
 ' C.I.
 'apDen = 30 * PG / 180: ProD = 1: RtDen = 1
 Call Get_Nominal_Gear_Specs_Dentato2(API, DrI, DbI, HelbI, EbI, EarI, SpMolaI, DedGear, DrDen, EarD, HelpDen, ApnD, MnD, XcDen, YcDen, apDen, ProDen, Rtden, 2)
 Call PntTroc(HelpDen, DrDen, Z1, Rtden, YcDen, XcDen, PG / 2 - API, XYtr(1), XYtr(2), "P", RX, Apx) ' apI
 Call PntEvolv(EbI, DbI, DatPiede, XYev(1), XYev(2), "P")
If (XYtr(2) - XYev(2) > 0.5) Then
'apr0(0) = Apr / 2 + Dapr(0): apr0(1) = 0.0001 + Dapr(1): apr0(2) = 0.02 + Dapr(2)
apDen = API: ProDen = 0#: Rtden = 0.03: Deltaden = epsB
F1 = fDa(apDen, ProDen, Rtden)
Else

F1 = fDa(aprB(0), aprB(1), aprB(2))
 apDen = aprB(0): ProDen = aprB(1): Rtden = aprB(2): Deltaden = epsB
 End If
Exit Sub

10:
 apDen = Abs(apr0(0)): ProDen = Abs(apr0(1)): Rtden = Abs(apr0(2)): Deltaden = epsB
Exit Sub

Error:

WRITE_DIALOG "ERROR: Auto Dentatura -> no convergence" '& Err
apDen = aprB(0): ProDen = aprB(1): Rtden = aprB(2): Deltaden = epsB
Exit Sub

End Sub
'---------------------------------------------------------------------------------
Sub N3x3b(ByRef apDen As Double, ByRef ProDen As Double, ByRef Rtden As Double)
' Dim apDen: Dim ProDen: Dim RtDen
 Dim i, ix, j, Jx As Integer
' Dim func, func1, func2 As Double
' Dim dfunc(1 To 3), dfunc1(1 To 3), dfunc2(1 To 3) As Double
 Dim apr0(0 To 2)
 Dim apr1(0 To 2)
 Dim aprB(0 To 2)
 Dim Dapr(0 To 2)
 Dim fDp0() As Double
 Dim iFDp0() As Double
 
 Dim F0() As Double:  Dim F1() As Double
 
 Dim tmp() As Double
 Dim E As Double: E = 0.00000001
 Dim ha(0 To 2) As Double: Dim hb() As Double
 Dim eps, epsB As Double
 Dim singular As Boolean
 
 i = 0: j = 0
 ix = 50: Jx = 30
 Dapr(0) = 0: Dapr(1) = 0: Dapr(2) = 0
 
guess:
 apr0(0) = Apr / 2 + Dapr(0): apr0(1) = 0.01 + Dapr(1): apr0(2) = 0.04 + Dapr(2)
 apr1(0) = Apr + Dapr(0): apr1(1) = 0.02 + Dapr(1): apr1(2) = 0.05 + Dapr(2)
 j = j + 1
 ha(0) = E: ha(1) = E: ha(2) = E
 epsB = 10
 singular = False
For i = 1 To ix
 
 F0 = Fd(apr0(0), apr0(1), apr0(2))
 fDp0 = fDp(apr0(0), apr0(1), apr0(2))
 iFDp0 = I3x3a(fDp0, singular)
 
 If (singular) Then
  Dapr(0) = Dapr(0) + 0.001: Dapr(2) = Dapr(2) + 0.01
  If (j < Jx) Then
   GoTo guess
  Else
   GoTo Error
  End If
 End If
 
 hb = Mul(iFDp0, F0)

 apr1(0) = apr0(0) - hb(0): apr1(1) = apr0(1) - hb(1): apr1(2) = apr0(2) - hb(2) 'apr1 = Sut(apr0, hb)
 
 F1 = Fd(apr1(0), apr1(1), apr1(2))
 eps = Sqr((F0(0) - F1(0) * 0) ^ 2 + (F0(1) - F1(1) * 0) ^ 2 + (F0(2) - F1(2) * 0) ^ 2) 'eps = Sqr((F0(0) - F1(0)) ^ 2 + (F0(1) - F1(1)) ^ 2 + (F0(2) - F1(2)) ^ 2)
 
 If (eps < epsB) Then
  epsB = eps: aprB(0) = apr1(0): aprB(1) = apr1(1): aprB(2) = apr1(2)
 End If
 If (eps < 0.000001) Then GoTo Convergence
 
 apr0(0) = apr1(0): apr0(1) = apr1(1): apr0(2) = apr1(2)
 apr1(0) = apr0(0) + ha(0): apr1(1) = apr0(1) + ha(1): apr1(2) = apr0(2) + ha(2)
 
Next i
Convergence:
 apDen = Abs(apr1(0)): ProDen = Abs(apr1(1)): Rtden = Abs(apr1(2))
 apDen = aprB(0): ProDen = aprB(1): Rtden = aprB(2)
Exit Sub

Error:

WRITE_DIALOG "ERROR: Auto Dentatura -> no convergence" '& Err
Exit Sub

End Sub
Sub N3x3a(ByRef apDen As Double, ByRef ProDen As Double, ByRef Rtden As Double)
' Dim apDen: Dim ProDen: Dim RtDen
 Dim i, ix, j, Jx As Integer
' Dim func, func1, func2 As Double
' Dim dfunc(1 To 3), dfunc1(1 To 3), dfunc2(1 To 3) As Double
 Dim apr0(0 To 2)
 Dim apr1(0 To 2)
 Dim J0() As Double:  Dim J1() As Double
 Dim iH0() As Double
 Dim tmp() As Double
 Dim E As Double: E = 0.00000001
 Dim ha(0 To 2) As Double: Dim hb() As Double
 Dim eps As Double
 
 i = 0: j = 0
 ix = 100: Jx = 200
'guess
 apr0(0) = Apr / 2: apr0(1) = 0.001: apr0(2) = 0.03
 apr1(0) = 2 * Apr / 3: apr1(1) = 0.02: apr1(2) = 0.05
 
'GoTo 10
'F1 = n2Jd(apr2(0), apr2(1), apr2(2)) Hd
 'J0 = Jd(apr0(0), apr0(1), apr0(2)) 'J0 = Jn2Jd(apr0(0), apr0(1), apr0(2))
 'J1 = Jd(apr1(0), apr1(1), apr1(2))
 'iH0 = iHd(apr0(0), apr0(1), apr0(2)) 'H0 = iHn2Jd(apr0(0), apr0(1), apr0(2))
 'h = Sut(apr1, apr0)
 ha(0) = E: ha(1) = E: ha(2) = E
For i = 1 To ix
' f(a0+h) = f(a0) + Df0(a0)*(h)
' Df0(a0+h) = Df0(a0) + H(a0)*(h)
' a1 = a0 + h -> h = a1-a0
' a1 = a0 + iH(a0) * Df0(a1) * h
 
 apr0(0) = apr1(0): apr0(1) = apr1(1): apr0(2) = apr1(2)
 apr1(0) = apr0(0) + ha(0): apr1(1) = apr0(1) + ha(1): apr1(2) = apr0(2) + ha(2)
 
 
 J0 = Jd(apr0(0), apr0(1), apr0(2))
 
 iH0 = iHd(apr0(0), apr0(1), apr0(2))
 
 tmp = Mul(iH0, J0)
 hb = Mul1(tmp, ha):
 apr1(0) = apr0(0) - hb(0): apr1(1) = apr0(1) - hb(1): apr1(2) = apr0(2) - hb(2) 'apr1 = Sut(apr0, hb)
 
 J1 = Jd(apr1(0), apr1(1), apr1(2))
 eps = Sqr((J0(0) - J1(0)) ^ 2 + (J0(1) - J1(1)) ^ 2 + (J0(2) - J1(2)) ^ 2)
 If (eps < 0.000001) Then GoTo 10
 
 'apr1(0) = apr0(0) + hb(0): apr1(1) = apr0(1) + hb(1): apr1(2) = apr0(2) + hb(2)
 ' Check
 
 'apr1 = Sum(apr0, hb)
 
 'h = Sut(apr1, apr0)
 'apr0(0) = apr1(0): apr0(1) = apr1(1): apr0(2) = apr1(2)
  
 'J1 = Jd(apr1(0), apr1(1), apr1(2))
 'iH0 = iHd(apr0(0), apr0(1), apr0(2)) 'H0 = iHn2Jd(apr0(0), apr0(1), apr0(2))
 'h = Sut(apr1, apr0)
  
 'J0 = Jd(apr1(0), apr1(1), apr1(2)) 'J0 = Jn2Jd(apr1(0), apr1(1), apr1(2)) 'F1 = n2Jd(apr0(1), apr0(2), apr0(3))
 'J1 = Jd(apr1(0), apr1(1), apr1(2))
 'iH0 = iHd(apr1(0), apr1(1), apr1(2)) 'H0 = iHn2Jd(apr1(0), apr1(1), apr1(2))
 'apr0(0) = Abs(apr1(0)): apr0(1) = Abs(apr1(1)): apr0(2) = Abs(apr1(2))
 
 
Next i
10:
apDen = Abs(apr0(0)): ProDen = Abs(apr0(1)): Rtden = Abs(apr0(2))
End Sub
'---------------------------------------------------------------------------------

Public Sub Get_GearProfile_cp_Tol(ByVal NbpIN As Integer, ByRef cpXtol1(), ByRef cpYtol1(), ByRef cpZtol1(), ByRef cpXtol2(), ByRef cpYtol2(), ByRef cpZtol2())

Dim cpX(1 To Nsample)
Dim cpY(1 To Nsample)
Dim cpZ(1 To Nsample)
'Dim apI, DrI, DbI, DedGear As Double Get_GearProfile_cp_Dentato
Dim XYm(1 To 2) As Double

'apI = Atn(Tan(Fndr(Aprd)) / Cos(Fndr(HelpD)))
'DrI = Mr * Z1 / Cos(Fndr(HelpD))
'DbI = DrI * Cos(apI)
    Dim API, DbI, DrI, HelbI, AuxI, EbI, EarI, SpMolaI, DedGear
    Call Get_Nominal_Gear_Specs(API, DrI, DbI, HelbI, EbI, EarI, SpMolaI, DedGear)
'    HelbI = Atn(Tan(Fndr(HelpD)) * Cos(Fndr(Aprd)))
'    AuxI = PI * DbI / Z1
'    EbI = (Wdt / Cos(HelbI)) - (Kdt - 1) * AuxI
Call Get_GearProfile_cp_Ref(NbpIN, cpX, cpY, cpZ)
    XYm(1) = 0
'   XYm(2) = DrMola / 2 + DedMola
'    DedGear = DE1 / 2 - DrI / 2 '+ GioccoFondoMola
    XYm(2) = DrI / 2 + DedGear + GioccoFondoMola
'-----
    Dim Raggio As Double
    For i = 1 To NbpIN
    Raggio = (Sqr((cpX(i) + XYm(1)) ^ 2 + (cpY(i) + XYm(2)) ^ 2))
    If (Raggio < DbI / 2) Then Raggio = DbI / 2
    
    API = Acs(DbI / (2 * Raggio))
    cpXtol1(i) = cpX(i) - FhaToll * Cos(API): cpYtol1(i) = cpY(i) - FhaToll * Sin(API): cpZtol1(i) = cpZ(i)
    cpXtol2(i) = cpX(i) + FhaToll * Cos(API): cpYtol2(i) = cpY(i) + FhaToll * Sin(API): cpZtol2(i) = cpZ(i)
    Next i

End Sub
'------------------
' Export control points of worm axial section
' 2 kind of section:
' KIND A)component curves:
' 1) linear segment external diameter
' 2) cowned profile
' 3) tip circumference
' 5) linear internal worm diameter
' KIND B)component curves:
' 4) tip relief
'--------------------

Public Sub Get_WormProfile_cp(ByVal NbpIN As Integer, ByRef cpX(), ByRef cpY(), ByRef cpZ())
Dim D, Eb, DB
Dim i As Integer
Dim dx As Double
Dim cpX0, cpY0 As Double
Dim Nbp As Integer

'If Rast$ = "O" Then
'Nbp = Int(NbpIN / 3)
'Else
Nbp = Int(NbpIN / 2)
'End If
'Dim cpX(1 To 4) As Double
'Dim cpY(1 To 4) As Double
'Dim cpZ(1 To 4) As Double

D = 0.5
Eb = Eb1
DB = DB1

' punti raggio piede
' punti trocoide
' punti evolvente



   Dim Aux, Aux1, A$, Xm, Ym, Pb9, Eb9, Atot, X, Y, X1, Y1, Arad  As Double
   Dim nVANI, ExNvani
   Dim PAS, YmMola, RX, dMin  As Double
   Dim XR, YR As Double
   Dim Amax  As Double
   Dim Aini, Afin, TH  As Double

    Xm = 0
    Ym = DrMola / 2 + DedMola
    
    Dim j As Integer
    
    
' MOLA
    
' Uscita.ForeColor = vbRed
' raggio testa mola
    
    
    'Aux = DrMola / 2 - (YdapRul - DedMola)
    
    ' Diametro mola inviluppato: IMPORTANTE
    'Call DisegnoRayon(Aux, 0, 0, 4.71 - 2 * PI / Z1, 4.71 + 2 * PI / Z1, Xm, Ym, 0, Uscita) ' 4.71= 1.5 * pi
    
    YmMola = 0
    Aux = Asn((YrfRul - YdapRul) / RfiancoRul)
    If Rast$ = "O" Then
        Aux1 = Asn((YrfRul - YrastRul) / RfiancoRul)
    Else
        Aux1 = Asn((YrfRul - RtRul) / (RfiancoRul - RtRul))
    End If
    
    Dim PASin
    'For j = -1 To 1 ' 3 vani
    j = 0
    ' traccia diametro piede mola
   '     Call Riga( (XdapRul + Pas + SpostRul / 2) / Cos(Helr), YdapRul,  Pi * Mapp / 2 + Pas / Cos(Helr), YdapRul, Xm, YmMola, Uscita)

'Sub Riga(X1, Y1, X2, Y2, Xm, Ym, Uscita)
   ' X1 , Y1 : Inizio riga
   ' X2 , Y2 : Fine riga
   ' X2 > 999 : Andare al punto X1,Y1 senza tracciare
   ' X1 > 999 : Andare al punto X2,Y2 tracciando
   ' Riga disegnata in scala rispetto a un punto ( Xm , Ym ) e
   ' spostata di un valore fisso in mm ( SpostX,SpostY)

'   Dim Aux
'   If Scala = 0 Then Scala = 1
'   Aux = Uscita.DrawWidth
'   If X1 <= 999 Then
'      Uscita.DrawWidth = 1
'      Uscita.PSet ((X1 - Xm) * Scala + SpostX, (Y1 - Ym) * Scala + SpostY)
'   Uscita.Line -((X2 - Xm) * Scala + SpostX, (Y2 - Ym) * Scala + SpostY)
   
        cpX(1) = -(((XdapRul + PAS + SpostRul / 2) / Cos(Helr)) - Xm)
        cpY(1) = ((-YdapRul) - YmMola)
        cpX(2) = -((PI * Mapp / 2 + PAS / Cos(Helr)) - Xm)
        cpY(2) = ((-YdapRul) - YmMola)

   '     Call Riga(-(XdapRul + Pas + SpostRul / 2) / Cos(Helr), YdapRul, -Pi * Mapp / 2 - Pas / Cos(Helr), YdapRul, Xm, YmMola, Uscita)
    ' raggio fianco
        PAS = MrMola * PI * j
        ' Call DisegnoRayon(RfiancoRul, XrfRul + PAS - SpostRul / 2, -YrfRul, PI + Aux, PI + Aux1, Xm, YmMola, Helr, Uscita)
        Aini = PI + Aux
        Afin = PI + Aux1
        PASin = (Afin - Aini) / (Nbp - 3)
        For i = 1 To Nbp - 2
            TH = Aini + PASin * (i - 1)
            Call PntRayon(RfiancoRul, XrfRul + PAS - SpostRul / 2, -YrfRul, Helr, TH, X, Y)
            cpX(i + 2) = (X - Xm)
            cpY(i + 2) = (Y - YmMola)
        Next i
        Call PntRayon(RfiancoRul, XrfRul - SpostRul / 2, -YrfRul, Helr, Afin, X, Y)
            X_TIP_MOLA = (X - Xm)
            Y_TIP_MOLA = (Y - YmMola)
    ' Call DisegnoRayon(RfiancoRul, -XrfRul + PAS + SpostRul / 2, -YrfRul, 2 * PI - Aux, 2 * PI - Aux1, Xm, YmMola, Helr, Uscita)
    ' Profilo rullo
        If Rast$ = "O" Then
          Aini = PI + ApRast
          Afin = 3 * PI / 2
            'Call DisegnoRayon(RtRul, -XrtRul + PAS - SpostRul / 2, -RtRul, 3 * PI / 2, PI + ApRast, Xm, YmMola, Helr, Uscita)
            'Call DisegnoRayon(RtRul, XrtRul + PAS + SpostRul / 2, -RtRul, 3 * PI / 2, 2 * PI - ApRast, Xm, YmMola, Helr, Uscita)
            'X = (XrastRul + PAS + SpostRul / 2) / Cos(Helr)
            
            'X1 = (XrtRul + RtRul * Cos(ApRast) + PAS + SpostRul / 2) / Cos(Helr)
            'Y1 = RtRul - RtRul * Sin(ApRast)
            
    '        Call Riga(X, YrastRul, X1, Y1, Xm, YmMola, Uscita)
    '        Call Riga(-X, YrastRul, -X1, Y1, Xm, YmMola, Uscita)
    
        Else
          Aini = PI + Aux1
          Afin = 3 * PI / 2
        End If
      ' raggio di tip
      ' Call DisegnoRayon(RtRul, -XrtRul + PAS - SpostRul / 2, -RtRul, 3 * PI / 2, PI + Aux1, Xm, YmMola, Helr, Uscita)
          

          PASin = (Afin - Aini) / (Nbp - 2)
          For i = 1 To Nbp - 1
              TH = Aini + PASin * (i - 1)
              Call PntRayon(RtRul, -XrtRul + PAS - SpostRul / 2, -RtRul, Helr, TH, X, Y)
              cpX(i + Nbp) = (X - Xm)
              cpY(i + Nbp) = (Y - YmMola)
          Next i
       ' segmento piede mola
       
        cpX(2 * Nbp) = 0
        cpY(2 * Nbp) = cpY(2 * Nbp - 1)
            
End Sub

' RULLO
Public Sub Get_GeneratingProfile_cp(ByVal NbpIN As Integer, ByVal Altezza As Double, ByRef cpX(), ByRef cpY(), ByRef cpZ())

Dim D, Eb, DB
Dim i As Integer
Dim dx As Double
Dim cpX0, cpY0 As Double
Dim Nbp As Integer

Nbp = Int(NbpIN / 2)

D = 0.5
Eb = Eb1
DB = DB1

' punti raggio piede
' punti trocoide
' punti evolvente HRULLOREF



   Dim Aux, Aux1, A$, Xm, Ym, Pb9, Eb9, Atot, X, Y, X1, Y1, Arad  As Double
   Dim nVANI, ExNvani
   Dim PAS, YmMola, RX, dMin  As Double
   Dim XR, YR As Double
   Dim Amax  As Double
   Dim Aini, Afin, TH  As Double

    Xm = 0
    Ym = DrMola / 2 + DedMola
    
    Dim j As Integer
    
    
' MOLA
    
' Uscita.ForeColor = vbRed
' raggio testa mola Altezza
    
    
    'Aux = DrMola / 2 - (YdapRul - DedMola)
    
    ' Diametro mola inviluppato: IMPORTANTE
    'Call DisegnoRayon(Aux, 0, 0, 4.71 - 2 * PI / Z1, 4.71 + 2 * PI / Z1, Xm, Ym, 0, Uscita) ' 4.71= 1.5 * pi
    
    YmMola = 0
    Aux = Asn((YrfRul - YdapRul) / RfiancoRul)
    
    Aux = Asn((YrfRul - Altezza) / RfiancoRul)
    If Rast$ = "O" Then
        Aux1 = Asn((YrfRul - YrastRul) / RfiancoRul)
    Else
        Aux1 = Asn((YrfRul - RtRul) / (RfiancoRul - RtRul))
    End If
    
    Dim PASin
    'For j = -1 To 1 ' 3 vani
    j = 0
    ' traccia diametro piede mola
   '     Call Riga( (XdapRul + Pas + SpostRul / 2) / Cos(Helr), YdapRul,  Pi * Mapp / 2 + Pas / Cos(Helr), YdapRul, Xm, YmMola, Uscita)

'Sub Riga(X1, Y1, X2, Y2, Xm, Ym, Uscita)
   ' X1 , Y1 : Inizio riga
   ' X2 , Y2 : Fine riga
   ' X2 > 999 : Andare al punto X1,Y1 senza tracciare
   ' X1 > 999 : Andare al punto X2,Y2 tracciando
   ' Riga disegnata in scala rispetto a un punto ( Xm , Ym ) e
   ' spostata di un valore fisso in mm ( SpostX,SpostY)

'   Dim Aux
'   If Scala = 0 Then Scala = 1
'   Aux = Uscita.DrawWidth
'   If X1 <= 999 Then
'      Uscita.DrawWidth = 1
'      Uscita.PSet ((X1 - Xm) * Scala + SpostX, (Y1 - Ym) * Scala + SpostY)
'   Uscita.Line -((X2 - Xm) * Scala + SpostX, (Y2 - Ym) * Scala + SpostY)
   PAS = MrMola * PI * j
   '     cpX(1) = -(((XdapRul + PAS + SpostRul / 2) / Cos(Helr)) - Xm)
        cpY(1) = ((-Altezza) - YmMola)
        cpX(2) = -((PI * Mapp / 2 + PAS / Cos(Helr)) - Xm)
        cpY(2) = ((-Altezza) - YmMola)
cpX(1) = cpX(2)
   '     Call Riga(-(XdapRul + Pas + SpostRul / 2) / Cos(Helr), YdapRul, -Pi * Mapp / 2 - Pas / Cos(Helr), YdapRul, Xm, YmMola, Uscita)
    ' raggio fianco
        
        ' Call DisegnoRayon(RfiancoRul, XrfRul + PAS - SpostRul / 2, -YrfRul, PI + Aux, PI + Aux1, Xm, YmMola, Helr, Uscita)
        Aini = PI + Aux
        Afin = PI + Aux1
        PASin = (Afin - Aini) / (Nbp - 3)
        For i = 1 To Nbp - 2
            TH = Aini + PASin * (i - 1)
            Call PntRayon(RfiancoRul, XrfRul + PAS - SpostRul / 2, -YrfRul, Helr, TH, X, Y)
            cpX(i + 2) = (X - Xm)
            cpY(i + 2) = (Y - YmMola)
        Next i
        Call PntRayon(RfiancoRul, XrfRul - SpostRul / 2, -YrfRul, Helr, Afin, X, Y)
            X_TIP_MOLA = (X - Xm)
            Y_TIP_MOLA = (Y - YmMola)
    ' Call DisegnoRayon(RfiancoRul, -XrfRul + PAS + SpostRul / 2, -YrfRul, 2 * PI - Aux, 2 * PI - Aux1, Xm, YmMola, Helr, Uscita)
    ' Profilo rullo
        If Rast$ = "O" Then
          Aini = PI + ApRast
          Afin = 3 * PI / 2
            'Call DisegnoRayon(RtRul, -XrtRul + PAS - SpostRul / 2, -RtRul, 3 * PI / 2, PI + ApRast, Xm, YmMola, Helr, Uscita)
            'Call DisegnoRayon(RtRul, XrtRul + PAS + SpostRul / 2, -RtRul, 3 * PI / 2, 2 * PI - ApRast, Xm, YmMola, Helr, Uscita)
            'X = (XrastRul + PAS + SpostRul / 2) / Cos(Helr)
            
            'X1 = (XrtRul + RtRul * Cos(ApRast) + PAS + SpostRul / 2) / Cos(Helr)
            'Y1 = RtRul - RtRul * Sin(ApRast)
            
    '        Call Riga(X, YrastRul, X1, Y1, Xm, YmMola, Uscita)
    '        Call Riga(-X, YrastRul, -X1, Y1, Xm, YmMola, Uscita)
    
        Else
          Aini = PI + Aux1
          Afin = 3 * PI / 2
        End If
      ' raggio di tip
      ' Call DisegnoRayon(RtRul, -XrtRul + PAS - SpostRul / 2, -RtRul, 3 * PI / 2, PI + Aux1, Xm, YmMola, Helr, Uscita)
          

          PASin = (Afin - Aini) / (Nbp - 2)
          For i = 1 To Nbp - 1
              TH = Aini + PASin * (i - 1)
              Call PntRayon(RtRul, -XrtRul + PAS - SpostRul / 2, -RtRul, Helr, TH, X, Y)
              cpX(i + Nbp) = (X - Xm)
              cpY(i + Nbp) = (Y - YmMola)
          Next i
       ' segmento piede mola
       
        cpX(2 * Nbp) = 0
        cpY(2 * Nbp) = cpY(2 * Nbp - 1)
    
End Sub

Private Sub Get_WormThickness_Cartesian(Xout, Yout)
    
    Dim Aini, Afin
    Dim TH(1 To 3)
    Dim X(1 To 3)
    Dim Y(1 To 3)
    Dim i As Integer
    Dim Test(1 To 3) As Double
    
    Aux = Asn((YrfRul - YdapRul) / RfiancoRul)
    If Rast$ = "O" Then
        Aux1 = Asn((YrfRul - YrastRul) / RfiancoRul)
    Else
        Aux1 = Asn((YrfRul - RtRul) / (RfiancoRul - RtRul))
    End If
    
    Aini = PI + Aux
    Afin = PI + Aux1

    TH(1) = Aini
    TH(2) = (Aini + Afin) / 2
    TH(3) = Afin
    
    For i = 1 To 20
    Call PntRayon(RfiancoRul, XrfRul - SpostRul / 2, -YrfRul, Helr, TH(1), X(1), Y(1)) ' coordinate reali
    Call PntRayon(RfiancoRul, XrfRul - SpostRul / 2, -YrfRul, Helr, TH(2), X(2), Y(2)) ' coordinate reali
    Call PntRayon(RfiancoRul, XrfRul - SpostRul / 2, -YrfRul, Helr, TH(3), X(3), Y(3)) ' coordinate reali
    'MrMola,Mapp,YdapRul,AddMolAtt=YdapRul - DedMola
    Test(1) = (X(1) + (MrMola * PI * Cos(Helr) - SpMola) / 2) * (X(2) + (MrMola * PI * Cos(Helr) - SpMola) / 2)
    Test(2) = (X(2) + (MrMola * PI * Cos(Helr) - SpMola) / 2) * (X(3) + (MrMola * PI * Cos(Helr) - SpMola) / 2)

    Test(1) = (Y(1) + DedMola) * (Y(2) + DedMola)
    Test(2) = (Y(2) + DedMola) * (Y(3) + DedMola)

''    If ((x(1) + SpMola / 2) * (x(2) + SpMola / 2) < 0) Then
''        TH(3) = TH(2)
''    ElseIf (x(2) + SpMola / 2) * (x(3) + SpMola / 2) < 0 Then
''        TH(1) = TH(2)
''    End If
    If Test(1) < 0 Then
        TH(3) = TH(2)
    ElseIf Test(2) < 0 Then
        TH(1) = TH(2)
    Else
        If (Test(1) < Test(2)) Then
        TH(1) = TH(1) - (TH(3) - TH(1)) / 2
        TH(3) = TH(2)
        Else
        TH(3) = TH(3) + (TH(3) - TH(1)) / 2
        TH(1) = TH(2)
        End If
    End If
    
    
    
    TH(2) = (TH(1) + TH(3)) / 2
    
    Next i
    
    Call PntRayon(RfiancoRul, XrfRul - SpostRul / 2, -YrfRul, Helr, TH(2), Xout, Yout) ' coordinate reali
    
End Sub
'--------------------------------------------------
' ESPORTA DATI SENSIBILI
' OUT(01) gioco fondo mola testa ingranaggio
' OUT(02) spostamento rullo
' OUT(03) bombatura ottenuta
' OUT(04) mola_modulo normale
' OUT(05) mola angolo pressione normale
' OUT(06) mola_spessore
' OUT(07) mola dedendum
' OUT(08) mola addendum per rispettare sap
' OUT(09) mola altezza dente minima
' OUT(10) Raggio Minimo Inviluppato da mola
' OUT(11) Ascissa spessore mola
' OUT(12) Ordinata spessore mola
' OUT(13) Diametro Rotolamento Mola
' OUT(14) Diametro Di Rastremazione Inviluppato
' OUT(15) Entit� Della Rastremazione Inviluppata
' OUT(16) Ascissa spessore TIP mola
' OUT(17) Ordinata spessore TIP mola
' OUT(18) Spostamento interasse
'--------------------------------------------------

Public Sub Get_WormWheel_Specs(ByRef OUT())
  
  OUT(1) = GioccoFondoMola 'Agg. 5.12.13 Visualizzava Gioco Max
  OUT(2) = SpostRul
  OUT(3) = BombOtt
  OUT(4) = MrMola
  OUT(5) = ApMolaD
  OUT(6) = SpMola
  OUT(7) = DedMola
  OUT(8) = YdapRul - DedMola
  OUT(9) = YdapRul
  OUT(10) = DrMola / 2 - (YdapRul - DedMola)
  Call Get_WormThickness_Cartesian(OUT(11), OUT(12))
  OUT(13) = DrMola
  OUT(14) = DIAMETRO_RASTREMAZIONE_OTTENUTO
  OUT(15) = ENTITA_RASTREMAZIONE_OTTENUTA
  OUT(16) = X_TIP_MOLA
  OUT(17) = Y_TIP_MOLA
  OUT(18) = GioccoFondoMola - GioccoFondoMolaMax - (DE1 - DatTesta) / 2 'Max

End Sub

Private Sub Devel_2diametri(E1, E2, D1, D2, DB, Eb)
   
   Dim Emn, Emx, Dmn, Dmx
   Dim Amn, Amx
   Dim PAS, X, A
   
   ' E1 , E2 : cordale
   If D1 < 0.1 Then DB = 0: Exit Sub
   If D2 < 0.1 Then DB = 0: Exit Sub
   If D1 > D2 Then
      Emn = E2: Dmn = D2: Emx = E1: Dmx = D1
   Else
      Emn = E1: Dmn = D1: Emx = E2: Dmx = D2
   End If
   Amn = Asn(Emn / Dmn): Amx = Asn(Emx / Dmx)
   'Amn = Emn / Dmn: Amx = Emx / Dmx
   PAS = 1: DB = Dmn - PAS
   X = -1
   Do
      DB = DB + PAS: PAS = PAS / 10
      Do
        DB = DB - PAS
        A = Amx + inv(Acs(DB / Dmx))
        Eb = A * DB
        X = (A - inv(Acs(DB / Dmn))) - Amn
        
      Loop While X < 0
      If Abs(DB - Dmn) < 0.001 Then
         ' non esiste un'evolvente passando da questi punti
         DB = 0: Eb = 0
         Exit Do
      End If
   Loop While Abs(X) > 0.000001
   
End Sub

Private Sub CalcoloEvolvente(DbOtt, Code$) 'Uscita,
    
    Dim Aux
    Dim Nbp As Integer, i As Integer
    Dim RX, ApAppx, X, Y, EbX
    Dim XR, YR
    Dim EbRif, NbpIni As Integer, i_1 As Integer
    Dim Xmax, Rmax, Xmin, Rmin
    
    YR = YrfRul - DedMola
    XR = Sqr(RfiancoRul ^ 2 - YR ^ 2) + SpMola / 2
    
    If Rast$ = "O" Then
        If DrastOtt < DatTesta Then DmaxEvolv = DrastOtt Else DmaxEvolv = DatTesta
    Else
        DmaxEvolv = DatTesta
    End If
    
    ' 1 ) Calcolo diametro di base ottenuto
    
    Call PntTrocFM(Helr, DrMola, Z1, -RfiancoRul, YR, XR, AminEvolv, Xmin, Y, "P", Rmin, ApAppx)
    Call PntTrocFM(Helr, DrMola, Z1, -RfiancoRul, YR, XR, AmaxEvolv, Xmax, Y, "P", Rmax, ApAppx)
    Call Devel_2diametri(Xmax * 2, Xmin * 2, Rmax * 2, Rmin * 2, DbOtt, EbRif)
    If Code$ = "CalcDbOtt" Then Exit Sub
    
    ' 2 ) Calcolo bombatura tra DatPiede e DmaxEvolv
        
    NbpBomb = 20
    For i = 1 To NbpBomb
        Aux = AminEvolv + (AmaxEvolv - AminEvolv) / (NbpBomb - 1) * (i - 1)
        Call PntTrocFM(Helr, DrMola, Z1, -RfiancoRul, YR, XR, Aux, X, Y, "P", Revolv(i), ApAppx)
        EbX = (Asn(X / Revolv(i)) + inv(Acs(DB1 / 2 / Revolv(i)))) * DB1
        Eevolv(i) = (EbX - EbRif) / 2
    Next i
    
    ' 3 ) Calcolo rastremazione tra DmaxEvolv e DatTesta
    
    If Rast$ = "O" And DrastOtt < DatTesta Then
        NbpIni = NbpBomb - 1
        Nbp = 10
        NbpTot = NbpIni + Nbp
        For i = 1 To Nbp
            i_1 = i + NbpIni
            Revolv(i_1) = (DmaxEvolv + (DatTesta - DmaxEvolv) / (Nbp - 1) * (i - 1)) / 2
            Aux = EbRastOtt / DbRast - inv(Acs(DbRast / 2 / Revolv(i_1)))
            EbX = (Aux + inv(Acs(DB1 / 2 / Revolv(i_1)))) * DB1
            Eevolv(i_1) = (EbX - EbRif) / 2
        Next i
    Else
        NbpTot = NbpBomb
    End If
    
    For i = 1 To NbpTot
        ' NbpTot = 1 a 20 : NbpBomb ; 20 a 29 Rastremazione
        Call LdevAdev(DB1, Revolv(i) * 2, Levolv(i), Aux, 0)
        Aevolv(i) = Fnrd(Aux)
    Next i

End Sub

Private Sub Definizione(badcondition As Boolean) 'Code$
    
    Dim Aux, Aux1, AddMolAtt, AddMolMax, Xm, Ym, ht, X1, DemiPas
    Dim Lmin, LMax, A$, Ap, YR, XR, X, Y
    Dim ALFA As Double, RX As Double, ApAppx As Double, PAS As Double
    Dim dx, DxPrecedent, Apa
    Dim Calc As Integer, DbOtt As Double
    
On Error GoTo errDefinizione

    badcondition = False
 
    Call CalcoloIngranaggio("DB")
    If DB1 > 0 Then
'        PicInfoIng.Visible = True
'        PicInfoIng.Print "Diametro di base : "; Format(Db1, "0.###")
    End If
    SpRul_A = SpRul_IN / 2: SpRul_B = SpRul_IN / 2
    If RtRul > 0 Then RtRulIni = RtRul
    
    Call CalcoloIngranaggio("")
    
    If DatPiede > DB1 Then
        Call LdevAdev(DB1, DatPiede, VPB, Aux, 0)
 '       LblDevDatPiede.Caption = PAR$
    End If
    If DatTesta > DB1 Then
        Call LdevAdev(DB1, DatTesta, VPB, Aux, 0)
  '      LblDevDatTesta.Caption = PAR$
    End If
    
    '  RULLO
    
    If RtRul > 0 Then
        If RfiancoRul = 0 Or RfiancoRul > Mr * 100000 Then
            RfiancoRul = (Int(Mr - 0.0001) + 1) * 100000
        End If
        If YrastRul > 0 And ArastRulD > 0 Then Rast$ = "O" Else Rast$ = "N"
        Call CalcoloRullo("SENSA_SPOST") '"SENSA_SPOST" Code$
        Aux = RtRul * (1 - Sin(ApRast))
        If Rast$ = "O" And (YrastRul - Aux) < Mr * 0.05 Then
            MsgBox "Rullo : Raggio testa e Altezza rastremazione incompatibili", vbCritical
            Exit Sub
        End If
        Aux = Round(GioccoFondoMolaMax * 100, 0)
        If HscGioccoFondoMola_Max <> Aux Then
            Codice = 0
            If Aux < 0 Then
                HscGioccoFondoMola_Max = Aux
                HscGioccoFondoMola_Min = Aux
            Else
                HscGioccoFondoMola_Max = Aux
                HscGioccoFondoMola_Value = Aux
            End If
            Codice = 1
        End If
        If HscGioccoFondoMola_Max > 0 And HscGioccoFondoMola_Min < 0 Then
            Codice = 0
            HscGioccoFondoMola_Value = 0
            HscGioccoFondoMola_Min = 0
            Codice = 1
        End If
  '      GioccoFondoMola = HscGioccoFondoMola_Value / 100
        For Calc = 1 To 2
            If Calc = 1 Then
                CorrAp = 0
            Else
                CorrAp = Atn(Tan(Acs(DbOtt / DrMola)) * Cos(Helr)) - Apmola
            End If
            Call CalcoloRullo("CON_SPOST")
            
            ' OTTENUTO
            
            If Rast$ = "O" Then
                ' 1 ) DrastOtt secondo ApRastApp
                Aux = (DedMola - YrastRul) / Sin(ApRastApp) + DbRast / 2 * Tan(ApRastApp)
                DrastOtt = Sqr(Aux ^ 2 + (DbRast / 2) ^ 2) * 2
                'Debug.Print DrastOtt
                ' 2 ) DrastOtt secondo Ap fianco
                Ap = Asn((YrfRul - YrastRul) / RfiancoRul)
                Ap = Atn(Tan(Ap) / Cos(Helr))
                Aux = (DedMola - YrastRul) / Sin(Ap) + DrMola / 2 * Sin(Ap)
                Aux = Sqr(Aux ^ 2 + (DrMola * Cos(Ap) / 2) ^ 2)
                ' 3 ) DrastOtt finale
                DrastOtt = DrastOtt / 2 + Aux
            End If
                
            ' Zona di RfiancoRul lavorando la zona ingranaggio attiva
            
            If Rast$ = "O" Then DmaxEvolv = DrastOtt Else DmaxEvolv = DatTesta
            If DmaxEvolv > DatTesta Then DmaxEvolv = DatTesta
            YR = YrfRul - DedMola
            XR = Sqr(RfiancoRul ^ 2 - YR ^ 2) + SpMola / 2
                   
            ' Saillie per rispettare DmaxEvolv
            
            PAS = 0.1
            Y = -PAS
            Do
                Do
                    Y = Y + PAS
                    If (Abs((YrfRul - Y) / RfiancoRul) > 1) Then GoTo Error
                    Ap = Asn((YrfRul - Y) / RfiancoRul)
                    Apa = Atn(Tan(Ap) / Cos(Helr))
                    Aux = (Y - DedMola) / Sin(Apa)
                    Aux1 = DrMola * Cos(Apa) / 2 * Tan(Apa)
                    RX = Sqr((DrMola * Cos(Apa) / 2) ^ 2 + (Aux1 - Aux) ^ 2)
                Loop While RX * 2 > DmaxEvolv
                Y = Y - PAS * 2: PAS = PAS / 2
            Loop While PAS > 0.00001
            AmaxEvolv = PI / 2 - Ap
            Call PntTrocFM(Helr, DrMola, Z1, -RfiancoRul, YR, XR, AmaxEvolv, X, Y, "P", RX, ApAppx)
            If Abs(RX * 2 - DmaxEvolv) > 0.01 Then
    '            MsgBox "Error in MOLAVITE01_Definizione ", vbCritical
                'Errore 1: Possibili errori in input dati Geometrici,Rullo,Mola
                WRITE_DIALOG "MOLAVITE01 -> Definizione: ERROR -> 01" '& Err
                Exit Sub
            End If
                                            
            If Rast$ = "O" Then
                ' Saillie per rispettare DrastOtt
                
                PAS = 0.1
                Y = -PAS
                Do
                    Do
                        Y = Y + PAS
                        If (Abs((YrfRul - Y) / RfiancoRul) > 1) Then GoTo Error
                        Ap = Asn((YrfRul - Y) / RfiancoRul)
                        Apa = Atn(Tan(Ap) / Cos(Helr))
                        Aux = (Y - DedMola) / Sin(Apa)
                        Aux1 = DrMola * Cos(Apa) / 2 * Tan(Apa)
                        RX = Sqr((DrMola * Cos(Apa) / 2) ^ 2 + (Aux1 - Aux) ^ 2)
                    Loop While RX * 2 > DrastOtt
                    Y = Y - PAS * 2: PAS = PAS / 2
                Loop While PAS > 0.00001
                Call PntTrocFM(Helr, DrMola, Z1, -RfiancoRul, YR, XR, PI / 2 - Ap, X, Y, "P", RX, ApAppx)
                EbRastOtt = (Atn(X / Y) + inv(Acs(DbRast / DrastOtt))) * DbRast
                If DrastOtt < DatTesta Then
                    Aux = EbRastOtt / DbRast - inv(Acs(DbRast / DatTesta))
                    ErastOtt = ((Atn(X / Y) + inv(Acs(DB1 / DrastOtt)) - inv(Acs(DB1 / DatTesta))) - Aux) * DB1 / 2
                Else
                    ErastOtt = 0
                End If
            Else
                ErastOtt = 0
            End If
                    
            ' Saillie maxi interferenza
            
            PAS = 0.1
            Y = PAS
            Do
                Do
                    Y = Y + PAS
                    If (Abs((YrfRul - Y) / RfiancoRul) > 1) Then GoTo Error
                    Ap = Asn((YrfRul - Y) / RfiancoRul)
                    Apa = Atn(Tan(Ap) / Cos(Helr))
                    Aux = (Y - DedMola) / Sin(Apa)
                    Aux1 = DrMola * Cos(Apa) / 2 * Tan(Apa)
                Loop While Aux < Aux1
                Y = Y - PAS * 2: PAS = PAS / 2
            Loop While PAS > 0.00001
            dx = DrMola * Cos(Apa)
            If dx > DatPiede Then
                MsgBox "Gear SAP not reachable", vbCritical
                WRITE_DIALOG "Gear SAP not reachable" '& Err MOLAVITE01 -> Definizione: ERROR -> 03
                badcondition = True
                Exit Sub
            End If
                    
            ' Saillie per rispettare DatPiede
            
            PAS = 0.1
            Y = -PAS
            Do
                Do
                    Y = Y + PAS
                    If (Abs((YrfRul - Y) / RfiancoRul) > 1) Then GoTo Error
                    Ap = Asn((YrfRul - Y) / RfiancoRul)
                    Apa = Atn(Tan(Ap) / Cos(Helr))
                    Aux = (Y - DedMola) / Sin(Apa)
                    Aux1 = DrMola * Cos(Apa) / 2 * Tan(Apa)
                    RX = Sqr((DrMola * Cos(Apa) / 2) ^ 2 + (Aux1 - Aux) ^ 2)
                Loop While RX * 2 > DatPiede
                Y = Y - PAS * 2: PAS = PAS / 2
            Loop While PAS > 0.00001
            AminEvolv = PI / 2 - Ap
            Call PntTrocFM(Helr, DrMola, Z1, -RfiancoRul, YR, XR, AminEvolv, X, Aux, "P", RX, ApAppx)
            If Abs(RX * 2 - DatPiede) > 0.01 Then
           '     MsgBox "Errore in AltezzaMinimaMolaV02", vbCritical
                WRITE_DIALOG "MOLAVITE01 -> Definizione: ERROR -> 02" '& Err
                Exit Sub
            Else
                YdapRul = Y ' piu precise rispetto a CalcoloRullo
            End If
            Call CalcoloEvolvente(DbOtt, "CalcDbOtt") 'PicDis,
        Next Calc
        Call CalcoloEvolvente(DbOtt, "Complet") 'PicDis,

    Dim GIOCCO_FONDO_MOLA_TESTA_ING, SPOSTAMENTO_RULLO, BOMBATURA_OTTENUTA, MOLA_MODULO_NORMALE, A_P_NORMALE, MOLA_SPESSORE, DEDENDUM, MOLA_ADDENDUM_PER_RISPETTARE_SAP, MOLA_ALTEZZA_DENTE_MINIMA
    
    GIOCCO_FONDO_MOLA_TESTA_ING = GioccoFondoMola
        
    Aux = SpostRul
    If Aux < 0 And Aux > -0.005 Then Aux = 0
        SPOSTAMENTO_RULLO = Aux
        BOMBATURA_OTTENUTA = BombOtt
        MOLA_MODULO_NORMALE = MrMola
        A_P_NORMALE = ApMolaD
        MOLA_SPESSORE = SpMola
        DEDENDUM = DedMola
        AddMolAtt = YdapRul - DedMola
        AddMolMax = (DrMola - Di1) / 2
        MOLA_ADDENDUM_PER_RISPETTARE_SAP = AddMolAtt
        MOLA_ALTEZZA_DENTE_MINIMA = AddMolAtt + DedMola

        If Rast$ = "O" Then
          If DrastOtt > DatTesta Then
                'PicGioccoFondoMola.ForeColor = vbRed
          End If
          DIAMETRO_RASTREMAZIONE_OTTENUTO = DrastOtt
          ENTITA_RASTREMAZIONE_OTTENUTA = ErastOtt
        Else
          DIAMETRO_RASTREMAZIONE_OTTENUTO = DrMola + 2 * DedMola
          DrastOtt = DrMola + 2 * DedMola
          ENTITA_RASTREMAZIONE_OTTENUTA = 0#
          ErastOtt = 0#
        End If
    Else
        Exit Sub
    End If
    
    Exit Sub

Error:
    WRITE_DIALOG "Dresser Not Suitable -> Check data ERR01"
    badcondition = True
    Exit Sub

errDefinizione:
    MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Sub: MOLAVITE01.Definizione"
    badcondition = True
    'Resume Next
    Exit Sub

End Sub

Sub FillParam()
    
    GioccoFondoMola = 0.13 * 0
    ApMedRulD = 20
    SpRul_IN = 2.16
    RfiancoRul = 1000
    Drul = 120
    RmedRul = 58
    YrastRul = 0
    ArastRulD = 0
    RtRul = 0.35
    ApRast = 0
    DatPiede = 21.2
    HelpD = 18.31
    Aprd = 20
    Mr = 1
    Z1 = 21
    Wdt = 10.8804
    Kdt = 4
    DE1 = 24.85
    Di1 = 20.375
    DatTesta = 24.85
    SpRul_A = SpRul_IN / 2: SpRul_B = SpRul_IN / 2
    PI = 4 * Atn(1)
    Call CalcoloIngranaggio("")
    Call Definizione(ErroMV)
    
End Sub

Function FillParamMV(ApMedRulD_IN, SpRul_IN_IN, RfiancoRul_IN, Drul_IN, RmedRul_IN, YrastRul_IN, ArastRulD_IN, RtRul_IN, ApRast_IN, DatPiede_IN, HelpD_IN, Aprd_IN, Mr_IN, Z1_IN, Wdt_IN, Kdt_IN, DE1_IN, Di1_IN, DatTesta_IN, DGioccoFondoMola_IN) As Boolean
    
On Error GoTo errFillParamMV

    FillParamMV = True
 '  DGioccoFondoMola = DGioccoFondoMola_IN '0.2
    ApMedRulD = ApMedRulD_IN
    SpRul_IN = SpRul_IN_IN
    RfiancoRul = RfiancoRul_IN ' INPUT: raggio di bombatura del rullo HBR
    Drul = Drul_IN
    RmedRul = RmedRul_IN
    YrastRul = YrastRul_IN
    ArastRulD = ArastRulD_IN
    RtRul = RtRul_IN
    ApRast = ApRast_IN ' angolo pressione evolvente del tratto di rastremazione
    DatPiede = DatPiede_IN
    HelpD = HelpD_IN
    Aprd = Aprd_IN
    Mr = Mr_IN
    Z1 = Z1_IN
    Wdt = Wdt_IN
    Kdt = Kdt_IN ' INPUT: Ingranaggio : Numero Denti Per Controllo quota W
    DE1 = DE1_IN
    Di1 = Di1_IN
    DatTesta = DatTesta_IN
    'MMin = ModuloMolaIN
    ImponiDrMola = False
    
    SpRul_A = SpRul_IN / 2: SpRul_B = SpRul_IN / 2
    ShowOptimizeButton = True
    PI = 4 * Atn(1)
 '   MinimizzaDeltaTip = False
    Y_TIP_MOLA = -YrastRul
    IsToolTipPresent = ArastRulD > 0 And YrastRul > 0
    Call CalcoloIngranaggio("")
    Call Definizione(ErroMV)
    
    TipRelifDiamRef = LEP("TIPRELIEF_DIAMETER")
    TipRelifAmplRef = LEP("TIPRELIEF_AMPLITUDE")
    'HRULLOREF = LEP("H_RIF_RULLO")
    HDENTEMOLA = LEP("HDENTEMOLA_G")
    If (ErroMV) Then GoTo Error
'    X_TIP_MOLA = Y_TIP_MOLA
    FillParamMV = True
    Exit Function
Error:
  WRITE_DIALOG "Dresser Not Suitable -> Check data ERR02"
 ' Err = False
  'Resume Next
  FillParamMV = False
  Exit Function

errFillParamMV:
  FillParamMV = False
  MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Function: FillParamMV"
Exit Function

End Function

Function AltezzaDenteMinima()
    
Dim DB
Dim Apa
Dim Aux
Dim AddMolAtt
Dim SpRul
    
    PI = 4 * Atn(1)
'GioccoFondoMola,
'ApMedRulD,SpRul_A,SpRul_B,RfiancoRul,Drul,RmedRul,YrastRul,RtRul,ApRast,
'DatPiede,Help,Apr,Mr,Z1,Wdt,Kdt

    GioccoFondoMola = 0.13 * 0
    ApMedRulD = 20
    SpRul = 2.16
    SpRul_IN = 2.16
    RfiancoRul = 1000
    Drul = 120
    RmedRul = 58
    YrastRul = 0
    ArastRulD = 0
    RtRul = 0.35
    ApRast = 0
    DatPiede = 21.2
    HelpD = 18.31
    Aprd = 20
    Mr = 1
    Z1 = 21
    Wdt = 10.8804
    Kdt = 4
    DE1 = 24.85
    Di1 = 20.375
    DatTesta = 24.85
    
    SpRul_A = SpRul / 2: SpRul_B = SpRul / 2
    
Call CalcoloIngranaggio("")   'Code$

Call Definizione(ErroMV)
If (ErroMV) Then GoTo Error
        
        Dim GIOCCO_FONDO_MOLA_TESTA_ING, SPOSTAMENTO_RULLO, BOMBATURA_OTTENUTA, MOLA_MODULO_NORMALE, A_P_NORMALE, MOLA_SPESSORE, DEDENDUM, AddMolMax, MOLA_ADDENDUM_PER_RISPETTARE_SAP, MOLA_ALTEZZA_DENTE_MINIMA, DIAMETRO_RASTREMAZIONE_OTTENUTO, ENTITA_RASTREMAZIONE_OTTENUTA
    
        GIOCCO_FONDO_MOLA_TESTA_ING = GioccoFondoMola
        
        
        Aux = SpostRul
        If Aux < 0 And Aux > -0.005 Then Aux = 0
        SPOSTAMENTO_RULLO = Aux
        BOMBATURA_OTTENUTA = BombOtt
        MOLA_MODULO_NORMALE = MrMola
        A_P_NORMALE = ApMolaD
        MOLA_SPESSORE = SpMola
        DEDENDUM = DedMola
        AddMolAtt = YdapRul - DedMola
        AddMolMax = (DrMola - Di1) / 2
        MOLA_ADDENDUM_PER_RISPETTARE_SAP = AddMolAtt
        MOLA_ALTEZZA_DENTE_MINIMA = AddMolAtt + DedMola
    
    
    AltezzaDenteMinima = AddMolAtt + DedMola

Error:
  WRITE_DIALOG "Set up MOLAVITE01 paramenters: ERROR -> Check data"
  Exit Function
    
End Function

Function func(ByVal X As Double) As Double

Dim F(3) As Double
Dim DmiRef, Dmi As Double

  DmiRef = 43.5
  GioccoFondoMola = X
    
  Call CalcoloIngranaggio("")
  Call Definizione(ErroMV)
  Dmi = (DrMola / 2 - (YdapRul - DedMola)) * 2
     
  If (OptiDtip) Then
    F(0) = (DIAMETRO_RASTREMAZIONE_OTTENUTO - TipRelifDiamRef) ^ 2
  Else
    F(0) = 0
  End If
 
  If (OptiEtip) Then
    F(1) = (ENTITA_RASTREMAZIONE_OTTENUTA - TipRelifAmplRef) ^ 2
  Else
    F(1) = 0
  End If
 
  If (OptiDmi) Then
    F(3) = (Dmi - DmiRef) ^ 2
  Else
    F(3) = 0
  End If
 
  func = Sqr(F(0) + F(1) + F(3))
  
End Function

Function pFunc(ByVal X As Double) As Double
 
 Dim F As Double
 Dim Fa As Double
 Dim Fb As Double
 Fa = func(X)
 Fb = func(X + DeltaG)

 F = (Fb - Fa) / DeltaG
 pFunc = F
 
End Function

Sub GetParamCondition(ByVal Operate As Boolean)

    TipRelifDiamRef = LEP("TIPRELIEF_DIAMETER")
    TipRelifAmplRef = LEP("TIPRELIEF_AMPLITUDE")
    
IsGearTipPresent = TipRelifAmplRef > 0 And TipRelifDiamRef > 0
    Call CalcoloIngranaggio("")
    Call Definizione(ErroMV)

ShowOptimizeButton = True
If (Not IsToolTipPresent) Then
If (Operate) Then
    MinimizzaDeltaTip = False
End If
ShowOptimizeButton = False
Else
    If (Not IsGearTipPresent) Then
        If (DrastOtt > DE1) Then
            If (Operate) Then
                MinimizzaDeltaTip = False
            End If
            ShowOptimizeButton = False
        Else
            TipRelifDiamRef = DE1 + 0.001
            TipRelifAmplRef = 0#
            If (Operate) Then
                MinimizzaDeltaTip = Not MinimizzaDeltaTip
            End If
            ShowOptimizeButton = True
        End If
    Else
            If (Operate) Then
                MinimizzaDeltaTip = Not MinimizzaDeltaTip
            End If
            ShowOptimizeButton = True
    End If
End If

End Sub

Sub calcolaspostamentorullo()

Dim oper As Boolean
oper = True
Call GetParamCondition(oper)
'
OEMX.OEM1hsMOLA1.Value = OEMX.OEM1hsMOLA1.MIN
    
 Dim fo As Double
 Dim FP As Double
 Dim fn As Double
 Dim Fb As Double
 
 Dim Gm, GX, Gd, Gdn, GB As Double
 Dim iMax As Integer
 Dim i As Integer
 Dim stmp As String
 

 iMax = 40 '25
 i = 0
 
 DeltaG = 0.00001
 GB = -10
 Gm = 0
 GX = GioccoFondoMolaMax
 Gd = (Gm + GX) / 4#
 Fb = 10
 fo = Fb
 Gd = 0
       OptiDtip = OEMX.OEM1chkINPUT(0).Value
      OptiEtip = OEMX.OEM1chkINPUT(1).Value
   If (Not (OptiDtip Or OptiEtip)) Then
    stmp = "WARNING: no objective value is selected"
    MinimizzaDeltaTip = False
    Else
    stmp = ""
   End If
   ' Dim OptiDmi As Logical

   OEMX.OEM1hsMOLA1.Enabled = Not ((MinimizzaDeltaTip And (OptiDtip Or OptiEtip)) Or OptiDmi)
 
 If ((MinimizzaDeltaTip And (OptiDtip Or OptiEtip)) Or OptiDmi) Then
MinimizzaDeltaTip = True
'OEMX.OEM1hsMOLA1.Enabled = False
   While (i < iMax And fo > DeltaG)
        fo = func(Gd)
        FP = pFunc(Gd)
        Gdn = Gd - fo / FP
        Gd = Gdn
        If (fo < Fb And (Gd > Gm And Gd < GX)) Then '
        Fb = fo
        GB = Gd
        End If
        
        If (Gd < Gm Or Gd > GX) Then
         Gd = (iMax - i) * (GX + Gm) / iMax  'If (Not OptiDmi) Then
        End If
        
        i = i + 1
    Wend
    If (GB > 0) Then
    fn = func(GB)
    Else
    If (Not OptiDmi) Then MinimizzaDeltaTip = False
    Call CalcoloIngranaggio("")
    Call Definizione(ErroMV)
 '   OEMX.OEM1hsMOLA1.Value = OEMX.OEM1hsMOLA1.MIN
'    fn = Func(GX)
    End If
    
        If (fo > DeltaG) Then
    '    ret = LoadString(g_hLanguageLibHandle, 1208, Atmp, 255)
    '    If ret > 0 Then stmp = Left$(Atmp, ret) Else stmp = "Root Diameter"
        stmp = "WARNING:   bad convergence ( = " & frmt(fo, 8) & " )" & "  >  " & frmt(DeltaG, 8)
'        Write_Dialog "WARNING:  " & stmp
    '    Warn(1) = True
    Else
            stmp = "INFO:  good convergence ( = " & frmt(fo, 8) & " )" & "  <  " & frmt(DeltaG, 8)
            
        'Write_Dialog "INFO:  " & stmp
    '    Warn(1) = False
    End If
     OEMX.OEM1hsMOLA1.Value = (GioccoFondoMolaMax - GioccoFondoMola) * 1000
     
    Else
    OEMX.OEM1hsMOLA1.Enabled = True
  '  SpRul_A = SpRul_IN / 2: SpRul_B = SpRul_IN / 2
    Call CalcoloIngranaggio("")
    Call Definizione(ErroMV)
    
   ' fn = Func(GX)
    End If

    Call CALCOLO_INGRANAGGIO_ESTERNO_380_MV(3)
WRITE_DIALOG stmp
End Sub

Sub DisDeveloppante(Z, DB, Eb, dMax, dMin, Xm, Ym, Prof$, USCITA)
   ' Prof$ = "P"  ou  "V"
   Dim Aux, D9, PAS, i, X, Y, Nbp
   Nbp = 20
   PAS = (dMax - dMin) / (Nbp - 1)
   D9 = dMin - PAS
   For i = 1 To Nbp
      D9 = D9 + PAS
      Call PntEvolv(Eb, DB, D9, X, Y, Prof$)
      If i = 1 Then
   '      Call Riga(X, -Y, 1000, 1000, Xm, -Ym, Uscita)
      Else
   '      Call Riga(1000, 1000, X, -Y, Xm, -Ym, Uscita)
      End If
   Next i
   D9 = dMin - PAS
   For i = 1 To Nbp
      D9 = D9 + PAS
      Call PntEvolv(Eb, DB, D9, X, Y, Prof$)
      If i = 1 Then
   '      Call Riga(-X, -Y, 1000, 1000, Xm, -Ym, Uscita)
      Else
   '      Call Riga(1000, 1000, -X, -Y, Xm, -Ym, Uscita)
      End If
   Next i
End Sub
Public Sub PntRayon(R9, Xr9, Yr9, Hel, TH, X1, Y1)
   'Dim i, X1, Y1, Nbp, Erreur
   'Dim Nvani
   ' Aini et Afin : trigo
   'Nvani = 2
   'If R9 * Scala > 0.1 Then
      'Erreur = 0.01
      'Nbp = Int(Abs(Aini - Afin) / Acs((R9 * Scala - Erreur) / (R9 * Scala)))
   'Else
      'Nbp = 5
   'End If
   'If Nbp < 5 Then Nbp = 5
   'If Nbp > 72 Then Nbp = 72
   'For i = Aini To Afin Step ((Afin - Aini) / Nbp) * 0.99999
      X1 = (Xr9 + R9 * Cos(TH)) / Cos(Hel)
      Y1 = Yr9 - R9 * Sin(TH)
      'If Abs(i - Aini) < 0.00001 Then
 '        Call Riga(X1, -Y1, 1000, 0, Xm, -Ym, Uscita)
      'Else
'         Call Riga(1000, 0, X1, -Y1, Xm, -Ym, Uscita)
      'End If
   'Next i
End Sub

'Sub DisegnoRayon(R9, Xr9, Yr9, Aini, Afin, Xm, Ym, Hel, Uscita)
'   Dim i, X1, Y1, Nbp, Erreur
'   Dim Nvani
   ' Aini et Afin : trigo
'   Nvani = 2
'   If R9 * Scala > 0.1 Then
'      Erreur = 0.01
'      Nbp = Int(Abs(Aini - Afin) / Acs((R9 * Scala - Erreur) / (R9 * Scala)))
'   Else
'      Nbp = 5
'   End If
'   If Nbp < 5 Then Nbp = 5
'   If Nbp > 72 Then Nbp = 72
'   For i = Aini To Afin Step ((Afin - Aini) / Nbp) * 0.99999
'      X1 = (Xr9 + R9 * Cos(i)) / Cos(Hel)
'      Y1 = Yr9 - R9 * Sin(i)
'      If Abs(i - Aini) < 0.00001 Then
      '((X1 - Xm) * Scala + SpostX, (Y1 - Ym) * Scala + SpostY)
 '        Call Riga(X1, -Y1, 1000, 0, Xm, -Ym, Uscita)
'      Else
'         Call Riga(1000, 0, X1, -Y1, Xm, -Ym, Uscita)
'      End If
'   Next i
'End Sub
'------------------------------------
Sub DisegnoEvolvente(USCITA)
    Dim Aux, i As Integer, ExSpostX, ExSpostY, ExScala
    Dim X1, Y1, X2, Y2 ', A$(30)
    
 '   For i = 1 To NbpTot
        ' NbpTot = 1 a 20 : NbpBomb ; 20 a 29 Rastremazione
'        Revolv(i) = 0#
'        Levolv(i) = 0#
''        Aevolv(i) = 0#
'        Eevolv(i) = 0#
'        Debug.Print i; "  "; A$(i)
 '   Next i
'    ExSpostX = SpostX
'    ExSpostY = SpostY
'    ExScala = Scala
'    Scala = 1
'    SpostY = Hstamp - 50
'    SpostX = 10
'    Call CadreVide(-SpostX, -10, LStamp, 60, Uscita)
'    X2 = (Levolv(NbpTot) - Levolv(1)) * ScalaL + 5
    X2 = Levolv(NbpTot) - Levolv(1)
'    Call Riga(-5, BombOtt * ScalaE, X2, BombOtt * ScalaE, 0, 0, Uscita)
    For i = 1 To NbpTot - 1
        X1 = (Levolv(i) - Levolv(1)) '* ScalaL
        X2 = (Levolv(i + 1) - Levolv(1)) '* ScalaL
        Y1 = (BombOtt - Eevolv(i)) '* ScalaE
        Y2 = (BombOtt - Eevolv(i + 1)) '* ScalaE
        'Call Riga(X1, Y1, X2, Y2, 0, 0, Uscita)
        If i = 1 Then
            'Call Riga(X1 - 2, Y1 - 6, 10000, 0, 0, 0, Uscita): Uscita.Print "P1"
            'Call Riga(120, -5, 10000, 0, 0, 0, Uscita): Uscita.Print "P1 : "; A$(i)
        End If
        If i = Int(NbpBomb / 2) And BombOtt > 0.001 Then
            'Call Riga(X2 - 2, -5, 10000, 0, 0, 0, Uscita): Uscita.Print "P2"
            'Call Riga(120, 0, 10000, 0, 0, 0, Uscita): Uscita.Print "P2 : "; A$(i)
        End If
        If i = NbpBomb - 1 Then
            'Call Riga(X2 - 2, Y2 - 6, 10000, 0, 0, 0, Uscita): Uscita.Print "P3"
            'Call Riga(120, 5, 10000, 0, 0, 0, Uscita): Uscita.Print "P3 : "; A$(i + 1)
        End If
        If Rast$ = "O" And i = NbpTot - 1 And NbpTot > NbpBomb Then
            'Call Riga(X2 - 2, Y2, 10000, 0, 0, 0, Uscita): Uscita.Print "P4"
            'Call Riga(120, 10, 10000, 0, 0, 0, Uscita): Uscita.Print "P4 : "; A$(NbpTot)
        End If
    Next i
 '   A$(0) = "Scala :" & Str$(ScalaE) & " *" & Str$(ScalaL)
 '   Call Riga(0, 30, 10000, 0, 0, 0, Uscita): Uscita.Print A$(0)

 '   SpostX = ExSpostX
 '   SpostY = ExSpostY
 '   Scala = ExScala
End Sub
Sub Riduci_Gioco_Testa_Rullo_MOLAVITE()
DGioccoFondoMola = val(OEMX.OEM1hsMOLA1.Value) / 1000
Call CALCOLO_INGRANAGGIO_ESTERNO_380_MV(0)
End Sub
'----------------------------------------------------
' 2013 07 09
'-----------------------------------------------------
Public Sub CALCOLO_INGRANAGGIO_ESTERNO_380_MV(ByVal SiStampa As Integer, Optional SoloCalcolo As Boolean = False)
'
Dim stmpCal As String
Dim TIPO_LAVORAZIONE As String
Dim fscala

  fscala = 0.9
  'LETTURA DEL TIPO DI LAVORAZIONE
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  '
  If (stmpCal <> "") Then
    Dim Calcolo_De As Boolean
    stmpCal = Trim(stmpCal)
    Calcolo_De = IIf(stmpCal = "1", True, False)
    If Calcolo_De Then
      Call CALCOLO_INGRANAGGIO_ESTERNO_DE_380(SiStampa)
      Exit Sub
    End If
  End If
  '
  OEMX.OEM1hsROTORE.Visible = False
  OEMX.OEM1hsMOLA.Visible = False
  OEMX.OEM1hsPROFILO.Visible = False
  '
Dim DBB         As Database
Dim RSS         As Recordset
Dim Np          As Integer
Dim NpC         As Integer
Dim ALFAn       As Double
Dim Beta        As Double
Dim AM          As Double
Dim Z           As Double
Dim DEm         As Double
Dim D1          As Double
Dim D2          As Double
Dim S0Nn        As Double
Dim W           As Double
Dim N           As Double
Dim Q           As Double
Dim DR          As Double
Dim RF          As Double
Dim DIint       As Double
Dim DIPU        As Double
Dim Valutazione As Integer
Dim BETAmG      As Double
'
Dim AngR         As Double
Dim CorrSpessore As Double
Dim FHaF(2)      As Double
'
Dim Tif(2, 3)           As Double
Dim Rastremazione(2, 3) As Double
Dim Bombatura(2, 3)     As Double
'
Dim stmp  As String
Dim stmp1 As String
Dim stmp2 As String
Dim i     As Integer
Dim j     As Integer
Dim jj    As Integer
Dim Val1  As Single
Dim Val2  As Single
'
Dim SapMin  As Double
Dim EapMin  As Double
Dim DIPUmin As Double
'
Dim NPR1  As Integer
'
Dim BetaMr As Double
Dim ALFAt  As Double
Dim F3     As Double
Dim Rp     As Double
Dim Rb     As Double
Dim PB     As Double
Dim DB     As Double
Dim DP     As Double
Dim Betab  As Double
Dim MM0    As Double
Dim MM1    As Double
Dim MM2    As Double
'
Dim IL    As Double
Dim S0Nt  As Double
Dim Sb    As Double
Dim iB    As Double
'
Dim CORREZIONE As Double
'
Dim E1   As Double
Dim H2   As Double
Dim H3, F2   As Double
'
Dim MM(), CT(), CE(), CB(), CK() As Double
'
Dim InvAx  As Double
'
Dim Ax, Sx, VX, RX, X0, Y0, T0, FI0, R0 As Double
'
Dim L(), B(), A(), D(), AlfaM(), API(), X(), Y(), XD(), YD()   As Double
'
Dim PAS, Xi, Xtr, Yi, Ytr, Ri, vi As Double
'
Dim hu1, HU2, AngMed, Sh, PM, B1 As Double
'
Dim dALFAm, MMEst, MMp As Double
'
Dim E17  As Integer
Dim E260, E262, E264, E266, E268, E19, E261, E263, E265, E267, E269 As Double
'
Dim TanA As Double
'
Dim SIGMA, Rmin As Double
'
Dim Tp, DRM, RMPD, RQC As Double
'
Dim AA, BB, cc, RAD, T1, T2, Ti, TSI As Double
'
Dim fp1, fp2 As Double
'
Dim XS, Ys, Zs As Double
'
Dim Ap#, Bp#, Cp#, Lq#
'
Dim ArgSeno, SINa2  As Double
'
Dim Uprof, Wprof, Vprof, Rprof  As Double
'
Dim ENTRX, INF, EXF, RXF, APFD, APF, Rayon, ALFAs, AngS, BETAi, SENS   As Double
'
Dim v7, V12, V13, V17, V14, V15, VPB, V16 As Double
'
Dim RXV, EXV, Ang As Double
'
Dim X1, Y1, XAPP, YAPP As Double
'
Dim Atmp  As String * 255
'
Dim dtmp1, dtmp2 As Double
'
Dim BETAseg, Fianco  As Integer
Dim TextFont As String
Dim ret      As Integer
Dim RulloMax(2) As Double
'
Dim MOLA_CBN        As String

On Error GoTo errCALCOLO_INGRANAGGIO_ESTERNO_380_MV
  '
  WRITE_DIALOG ""
  OEMX.OEM1chkINPUT(0).Visible = True
  If Not SoloCalcolo Then
    OEMX.OEM1PicCALCOLO.BackColor = &HFFFFFF
    OEMX.OEM1PicCALCOLO.Cls
    For i = 0 To 4
      OEMX.txtINP(i).Visible = False
      OEMX.lblINP(i).Visible = False
    Next i
    OEMX.OEM1chkINPUT(0).Visible = False
    OEMX.OEM1chkINPUT(1).Visible = False
    OEMX.OEM1chkINPUT(2).Visible = False
    OEMX.OEM1lblOX.Visible = False
    OEMX.OEM1lblOY.Visible = False
    OEMX.OEM1txtOX.Visible = False
    OEMX.OEM1txtOY.Visible = False
    OEMX.OEM1Command2.Visible = False
    OEMX.OEM1Label2.Visible = False
    OEMX.OEM1fraOpzioni.Visible = False
    OEMX.OEM1frameCalcolo.Caption = ""
    DEm = val(GetInfo("CALCOLO", "DiametroEsternoMola", Path_LAVORAZIONE_INI))
  Else
    DEm = LEP("iDiamola")
  End If
  '
  MOLA_CBN = "N"
  If (LEP("TMOLA_G[0,1]") = 2) Then MOLA_CBN = "Y"
  '
  'Numero dei punti del raggio di testa della mola
  NPR1 = val(GetInfo("CALCOLO", "NumeroPuntiRaggio", Path_LAVORAZIONE_INI))
  If (NPR1 = 0) Then NPR1 = 5
  '
  For Fianco = 1 To 2
    'PROFILO INGRANAGGIO MV cache
    Z = Lp("NUMDENTI_G")
    AM = Lp("MN_G")
    ALFAn = Lp("ALFA_G")
    Beta = Lp("BETA_G")
    D1 = Lp("DSAP_G")
    D2 = Lp("DEXT_G")
    DIint = Lp("DINT_G")
    S0Nn = Lp("R[938]")
    W = Lp("QW")
    N = Lp("ZW")
 ' Caso in cui QW sia nullo
      If (W = 0 And S0Nn > 0) Then
        'CALCOLO QUOTA WILDHABER
        If (N = 0) Then N = 4
        Call SP("ZW", N)
        Call CalcoloParametri_MOLAVITE
        W = Lp("QW")
      End If
 
 
    Q = Lp("QR")
    DR = Lp("DR")
	
    RF = LEP("DIAMMOLA_G") ' R[52]raggio esterno mola
    DIPU = 0.5 '(D2 - D2) / 10 'LEP("R[194]") distanza punti
    Valutazione = LEP("R[901]")
    BETAmG = IIf(SoloCalcolo, LEP("R[258]"), val(GetInfo("CALCOLO", "InclinazioneMola", Path_LAVORAZIONE_INI)))
    BETAmG = Beta
    'TRASFORMAZIONE IN RADIANTI
    ALFAn = ALFAn * (PG / 180)
    'CONSIDERO UN INGRANAGGIO DRITTO CON UN ELICA PICCOLISSIMA
    If Beta = 0 Then Beta = (0.00001) * (PG / 180) Else Beta = Beta * (PG / 180)
    If BETAmG = 0 Then BetaMr = (0.00001) * (PG / 180) Else BetaMr = BETAmG * (PG / 180)
    ALFAt = Atn(Tan(ALFAn) / Cos(Beta))
    T1 = Tan(ALFAt)
    F3 = T1 - Atn(T1)                   'INVOLUTA(ALFAON)
    Rp = AM * Z / 2 / Cos(Beta)         'Raggio primitivo
    Betab = Atn(Tan(Beta) * Cos(ALFAt)) 'Elica di base
    Rb = Rp * Cos(ALFAt)                'Raggio di base
    PB = (Rb * 2 * PG) / Z              'Passo di base
    DB = 2 * Rb                         'Diametro di base
    DP = 2 * Rp                         'Diametro primitivo
    'SALVO IL SEGNO DI BETA
    If Beta < 0 Then BETAseg = -1 Else BETAseg = 1
    'CONSIDERO SOLO IL VALORE ASSOLUTO
    Beta = Abs(Beta)
    'CALCOLO DEL SAP MINIMO
    SapMin = 2 * Sqr(Rb ^ 2 + DIPU ^ 2)
    If (D1 < SapMin) Then WRITE_DIALOG "D1 < SAP min " & frmt(SapMin, 4): Exit Sub
    MM1 = Sqr(D1 ^ 2 - DB ^ 2) / 2
    'CALCOLO DEL EAP MINIMO
    EapMin = 2 * Sqr(Rb ^ 2 + (DIPU * 3 + MM1) ^ 2)
    If D2 < EapMin Then WRITE_DIALOG "D2 < EAPmin " & frmt(EapMin, 4): Exit Sub
    MM2 = Sqr(D2 ^ 2 - DB ^ 2) / 2
    'CONTROLLO DISTANZA PUNTI MININA: max 50 per CNC
    DIPUmin = (MM2 - MM1) / 50
    DIPU = DIPUmin * 1.1
    If DIPU < DIPUmin Then WRITE_DIALOG "DIPU < DIPUmin: max 50 X CNC " & frmt(DIPUmin, 4)
    'CALCOLO DEL S0Nn, W e Q
    If (S0Nn <= 0) Then
      If W > 0 Then
        'TRASFORMAZIONE_W_SON
        iB = N * PB - W / Cos(Betab)      'Vano Base
        Sb = PB - iB                      'Spessore base
        S0Nt = Rp * 2 * (Sb / Rb / 2 - F3)
        S0Nn = S0Nt * Cos(Beta)
        S0Nn = Int(S0Nn * 10000 + 0.5) / 10000
      Else
        If Q > 0 Then
        'TRASFORMAZIONE_Q_SON
        If Int(Z / 2) = Z / 2 Then E1 = 0 Else E1 = PG / 2 / Z
        H2 = AM * Z * Cos(ALFAt) * Cos(E1) / ((Q - DR) * Cos(Beta))
        H3 = Tan(FnARC(H2))
        F2 = (F3 + DR / (AM * Z * Cos(ALFAn)) - H3 + Atn(H3)) * AM * Z
        S0Nn = (PG * AM - F2)
        End If
      End If
    End If
    'UTILIZZO LA CORREZIONE DI SPESSORE
    S0Nn = S0Nn + CorrSpessore
    IL = (DEm + DIint) / 2
    S0Nt = S0Nn / Cos(Beta)
    Sb = Rb * (S0Nt / Rp + 2 * F3)
    iB = PB - Sb
    'CALCOLO IL PUNTO PRIMA DELL'INIZIO DEL PROFILO ATTIVO
    MM0 = MM1 - DIPU
    'CALCOLO DEL NUMERO DEI PUNTI
    Np = Int((MM2 - MM0) / DIPU) + 1 + 1
    'RICALCOLO DELLA FINE DEL PROFILO ATTIVO
    MM2 = MM0 + (Np - 1) * DIPU
    '
  Next Fianco
  '
  If (DIPU < DIPUmin) Then WRITE_DIALOG "DIPU < DIPUmin: max 50 X CNC " & frmt(DIPUmin, 4)
  '
  'SALVATAGGIO DEL CALCOLO SU FILE ASCII
  Dim XX1           As Single
  Dim YY1           As Single
  Dim XX2           As Single
  Dim YY2           As Single
  Dim DISTANZA      As Single
  Dim ANGOLO        As Single
  Dim AngoloSoglia  As Single
'
If Not SoloCalcolo Then
  'Visualizzazione e/o stampa dei risultati --------------------------------------------------
  Dim pX1       As Single
  Dim pY1       As Single
  Dim pX2       As Single
  Dim pY2       As Single
  Dim Altezza   As Single
  Dim Larghezza As Single
  Dim ScalaX    As Single
  Dim ScalaY    As Single
  Dim PicH      As Single
  Dim PicW      As Single
  Dim da        As Double
  '
  'LARGHEZZA
  stmp = GetInfo("CALCOLO", "Larghezza", Path_LAVORAZIONE_INI):  If val(stmp) > 0 Then PicW = val(stmp) Else PicW = 190
  OEMX.OEM1PicCALCOLO.ScaleWidth = PicW
  'ALTEZZA
  stmp = GetInfo("CALCOLO", "Altezza", Path_LAVORAZIONE_INI):    If val(stmp) > 0 Then PicH = val(stmp) Else PicH = 95
  OEMX.OEM1PicCALCOLO.ScaleHeight = PicH
  'CARATTERE DI STAMPA TESTO
  TextFont = GetInfo("CALCOLO", "TextFont", Path_LAVORAZIONE_INI)
  If (LINGUA = "CH") Then TextFont = "MS Song"
  If (SiStampa = 1) Then
    For i = 1 To 2
      Printer.FontName = TextFont
      Printer.FontSize = 12
      Printer.FontBold = False
    Next i
  Else
    For i = 1 To 2
      OEMX.OEM1PicCALCOLO.FontName = TextFont
      OEMX.OEM1PicCALCOLO.FontSize = 8
      OEMX.OEM1PicCALCOLO.FontBold = False
    Next i
  End If
'
Dim cpX(1 To Nsample)
Dim cpY(1 To Nsample)
Dim cpZ(1 To Nsample)
'------------------------
' GEAR RETTIFICATA
Dim cpXg(1 To Nsample)
Dim cpYg(1 To Nsample)
Dim cpZg(1 To Nsample)
'------------------------
' HMI
Dim cpXref(1 To Nsample)
Dim cpYref(1 To Nsample)
Dim cpZref(1 To Nsample)
'------------------------
' RUOTA DENTATA
Dim cpXden(1 To Nsample)
Dim cpYden(1 To Nsample)
Dim cpZden(1 To Nsample)
'------------------------
Dim cpXtol1(1 To Nsample)
Dim cpYtol1(1 To Nsample)
Dim cpZtol1(1 To Nsample)
Dim cpXtol2(1 To Nsample)
Dim cpYtol2(1 To Nsample)
Dim cpZtol2(1 To Nsample)
'--------------------------CONTEX
Dim Xmax(0 To 2) As Single
Dim Ymax(0 To 2) As Single
Dim Xmin(0 To 2) As Single
Dim Ymin(0 To 2) As Single
'--------------------------
Dim cpXd(1 To Nsample)
Dim cpYd(1 To Nsample)
Dim cpZd(1 To Nsample)
'--------------------------
Dim cpXw(1 To Nsample)
Dim cpYw(1 To Nsample)
Dim cpZw(1 To Nsample)
'--------------------------

 Dim ApMedRulD_IN As Double
 Dim SpRul_IN_IN As Double
 Dim RfiancoRul_IN As Double ' R bombatura rullo
 Dim Drul_IN As Double
 Drul_IN = LEP("DRULLOPROF_G")
 Dim RmedRul_IN As Double
 Dim YrastRul_IN As Double
 Dim ArastRulD_IN As Double
 Dim ApRast_IN As Double
 Dim RtRul_IN As Double ' Raggio testa rullo
 Dim DatTesta_IN As Double
 Dim TipoRullo_IN As Integer
 
 TipoRullo_IN = LEP("TIPORULLO_G")
 
 If (TipoRullo_IN <> 3) And (TipoRullo_IN <> 4) Then
    'Rullo Standard
    ApMedRulD_IN = LEP("AP_RULLO")
    SpRul_IN_IN = LEP("SP_RIF_RULLO")
    RfiancoRul_IN = LEP("BOMBATURA_RULLO")
    RmedRul_IN = LEP("RG_RIF_RULLO")
    YrastRul_IN = LEP("TIPRELIEF_DIS_RULLO")
    ArastRulD_IN = LEP("TIPRELIEF_ANG_RULLO")
    ApRast_IN = 0
    RtRul_IN = LEP("RG_RAC_TESTA_RULLO")
    DatTesta_IN = LEP("DEAP_G")
    HRULLOREF = LEP("H_RIF_RULLO")
    
Else
    'Rullo SAMP per Contour Dressing -- Dati Zona Standard
    ApMedRulD_IN = LEP("ANGOLOPRESSIONEZ1_CD")
    'Ruotando il rullo di 180� lo spessore di riferimento coincide con il doppio
    'dello spessore di riferimento della zona standard del rullo per Contour Dressing
    SpRul_IN_IN = LEP("SPESRIFZ1_G") * 2
    RfiancoRul_IN = LEP("RAGGIOBOMBZ1_CD")
    RmedRul_IN = LEP("RAGGRIFZ1_G")
    YrastRul_IN = 0
    ArastRulD_IN = 0
    ApRast_IN = 0
    RtRul_IN = LEP("RAGGIOTESTAZ1_CD")
    DatTesta_IN = LEP("DEAP_G")
    HRULLOREF = LEP("ALTEZZARULLOZ1_CD")
    
    '
 End If
 
Dim Checkinp As Double

Dim PlotTollerance As Boolean ' Activate Tollerance curve

Checkinp = HRULLOREF > 0 And ApMedRulD_IN > 0 And SpRul_IN_IN > 0 And RfiancoRul_IN > 0 And Drul_IN > 0 And RmedRul_IN > 0 And YrastRul_IN >= 0 And ArastRulD_IN >= 0 And RtRul_IN > 0 And DatTesta_IN > 0
If (Checkinp) Then

 If (FillParamMV(ApMedRulD_IN, SpRul_IN_IN, RfiancoRul_IN, Drul_IN, RmedRul_IN, YrastRul_IN, ArastRulD_IN, RtRul_IN, ApRast_IN, D1, Beta * 180 / PG, ALFAn * 180 / PG, AM, Z, W, N, D2, DIint, DatTesta_IN, 0)) Then
'--------------------------------------------------
' DATI SENSIBILI INGRANAMENTO MOLA RUOTA
' Specs(01) gioco fondo mola testa ingranaggio
' Specs(02) spostamento rullo
' Specs(03) bombatura ottenuta
' Specs(04) mola_modulo normale
' Specs(05) mola angolo pressione normale
' Specs(06) mola_spessore
' Specs(07) mola dedendum
' Specs(08) mola addendum per rispettare sap
' Specs(09) mola altezza dente minima
' Specs(10) Raggio Minimo Inviluppato da mola
' Specs(11) Ascissa spessore mola
' Specs(12) Ordinata spessore mola
' Specs(13) Diametro Rotolamento Mola
' Specs(14) Diametro Di Rastremazione Inviluppato
' Specs(15) Entit� Della Rastremazione Inviluppata
' Specs(16) Ascissa spessore tip mola
' Specs(17) Ordinata spessore tip mola
'--------------------------------------------------

    Dim Specs(1 To 18)
    Dim Warn(1 To 2) As Boolean
    
    Call Get_WormWheel_Specs(Specs)
    
    Dim RminEnvelopped As Double
    RminEnvelopped = Specs(10)
    Else
    WRITE_DIALOG "Input parameter not suitable (ERR03)" '& Err
    Exit Sub
    End If
 '   Exit Sub
    End If
    'Specs(13) / 2 - Specs(8)
  
  'CONTROLLO RISULTATI IMPORTANTI
    stmp = "WARNING: "
  'DIAMETRO INTERNO MINIMO
    If (2 * Specs(10) - DIint < -0.0001) Then
        ret = LoadString(g_hLanguageLibHandle, 1208, Atmp, 255)
        If ret > 0 Then stmp = Left$(Atmp, ret) Else stmp = "Root Diameter"
        stmp = stmp & " ( = " & frmt(Specs(10) * 2, 4) & " )"
        stmp = stmp & "  <  " & frmt(DIint, 4)
        WRITE_DIALOG "WARNING:  " & stmp
        Warn(1) = True
    Else
        Warn(1) = False
    End If

      Dim PX3, pY3, pX4, pY4, pX5, pY5, pX6, pY6
      Dim PXY(1 To 10, 1 To 2) As Double
  'SELEZIONE DEL TIPO DI GRAFICO
   ' Set up edit box per scala
      OEMX.lblINP(0).Visible = False 'True
      OEMX.txtINP(0).Visible = False 'True
      OEMX.lblINP(1).Visible = False
      OEMX.txtINP(1).Visible = False
   '   GoSub ETICHETTE_GRAFICI
   
   Dim tTX, tTY As Double
   Dim Inspection As Boolean
   Dim oper As Boolean
   stmp = GetInfo("CALCOLO", "fHaToll", Path_LAVORAZIONE_INI)
   FhaToll = val(stmp)
   FhaDToll = 0.001
   Dim Rappresentazione As String
' Close HGKinetic
   If (hndHGkinetic <> 0) Then HGkinetic_cmdCloseWindow
   If (OEMX.OEM1ListaCombo1.ListIndex <> 4) Then OEMX.OEM1hsMOLA1.Visible = True
'--------------------------------------
  Select Case OEMX.OEM1ListaCombo1.ListIndex
    '
    Case 0    '"MOLA" HRULLOREF cpX
'Giocofondomola----------SLIDER
OEMX.OEM1hsMOLA1.Visible = True: OEMX.OEM1hsMOLA1.MIN = 0: OEMX.OEM1hsMOLA1.Max = GioccoFondoMolaMax * 1000: DGioccoFondoMola = val(OEMX.OEM1hsMOLA1.Value) / 1000 'OEMX.OEM1hsMOLA1.ToolTipText = "Reduce Gt"
'------------------------------
    PlotTollerance = False
      Call Get_GeneratingProfile_cp(Np, YdapRul, cpX, cpY, cpZ) 'Get_WormProfile_cp
      Call Get_ProfiloControllato(Np, cpXg, cpYg, cpZg) 'cpXden
      ' SOSTRITUZIONE TEMPORANEA (Np, cpXden, cpYden, cpZden)
    '  Call Get_GearProfile_cp_Dentato(Np, cpXg, cpYg, cpZg)
      
      Call Get_GeneratingProfile_cp(Np, HRULLOREF, cpXd, cpYd, cpZd) 'Get_WormProfile_cp
      Call Get_GeneratingProfile_cp(Np, HDENTEMOLA, cpXw, cpYw, cpZw) 'Get_WormProfile_cp
      Rappresentazione = "RULLO-MOLA"
      GoSub RAPPRESENTAPAGINA
      GoSub FineDocumento
      '
    Case 1    '"INGRANAGGIO"
    Rappresentazione = "INGRANAGGIO"
    PlotTollerance = False
' Input fHatoll------------------
      OEMX.lblINP(1).Visible = True: OEMX.txtINP(1).Visible = True: OEMX.lblINP(1).Left = 100 + OEMX.lblINP(1).Width: OEMX.txtINP(1).Left = OEMX.lblINP(1).Left: OEMX.txtINP(1).Width = OEMX.lblINP(1).Width: OEMX.lblINP(1).Height = OEMX.txtINP(1).Height: OEMX.lblINP(1).Top = 100: OEMX.txtINP(1).Top = OEMX.lblINP(1).Top + OEMX.lblINP(1).Height: OEMX.lblINP(1).Caption = "fHatoll": FhaToll = val(OEMX.txtINP(1).Text): OEMX.lblINP(1).ToolTipText = "Set admissible stripe width": OEMX.txtINP(1).ToolTipText = "[mm]"
      If (FhaToll <= 0 Or FhaToll > 1) Then
      FhaToll = 0.02
      End If
      OEMX.txtINP(1).Text = frmt(FhaToll, 4)
'Obiettivo----------Check box
      OEMX.OEM1chkINPUT(0).Visible = True: OEMX.OEM1chkINPUT(0).Caption = "Dtip" ': OEMX.OEM1chkINPUT(0).Caption = "": OEMX.OEM1chkINPUT(0).Width = 200: OEMX.OEM1chkINPUT(0).Top = 3170: OEMX.OEM1chkINPUT(0).Left = 0 '6120
      OEMX.OEM1chkINPUT(1).Visible = True: OEMX.OEM1chkINPUT(1).Caption = "Etip": OEMX.OEM1chkINPUT(1).Top = OEMX.OEM1chkINPUT(0).Top ': OEMX.OEM1chkINPUT(1).Caption = "": OEMX.OEM1chkINPUT(1).Width = 200: OEMX.OEM1chkINPUT(1).Top = 3385: OEMX.OEM1chkINPUT(1).Left = 0 '6120
'      OEMX.OEM1chkINPUT(2).Visible = True: OEMX.OEM1chkINPUT(2).Caption = "": OEMX.OEM1chkINPUT(2).Width = 200: OEMX.OEM1chkINPUT(2).Top = 3385: OEMX.OEM1chkINPUT(2).Left = 0 '6120
      OptiDtip = OEMX.OEM1chkINPUT(0).Value: OEMX.OEM1chkINPUT(0).ToolTipText = "Fix tip Diameter"
      OptiEtip = OEMX.OEM1chkINPUT(1).Value: OEMX.OEM1chkINPUT(1).ToolTipText = "Fix tip Entity"
'Optimize--------------Button
      oper = False
      OEMX.lblINP(0).Top = 100: OEMX.lblINP(0).Left = 100: OEMX.lblINP(0).Width = OEMX.lblINP(1).Width: OEMX.lblINP(0).Height = OEMX.lblINP(1).Height: OEMX.lblINP(0).ToolTipText = "Look for optimal dressing displacement and center distance correction"
      Call GetParamCondition(oper)
      If (ShowOptimizeButton) Then 'IsToolTipPresent
       OEMX.lblINP(0).Visible = True
      End If
      If (MinimizzaDeltaTip) Then '
      OEMX.lblINP(0).Caption = "Optimized"
      Else
      OEMX.lblINP(0).Caption = "Optimize"
      End If
'------------------------GET
OEMX.lblINP(2).Visible = True: OEMX.txtINP(2).Visible = False: OEMX.lblINP(2).Caption = "Get Cor": OEMX.lblINP(2).Left = OEMX.lblINP(0).Left: OEMX.lblINP(2).Width = OEMX.lblINP(0).Width: OEMX.lblINP(2).Top = OEMX.lblINP(0).Top + OEMX.lblINP(0).Height: OEMX.lblINP(2).Height = OEMX.lblINP(0).Height
'Giocofondomola----------SLIDER
OEMX.OEM1hsMOLA1.Visible = True: OEMX.OEM1hsMOLA1.MIN = 0: OEMX.OEM1hsMOLA1.Max = GioccoFondoMolaMax * 1000: DGioccoFondoMola = val(OEMX.OEM1hsMOLA1.Value) / 1000 'OEMX.OEM1hsMOLA1.ToolTipText = "Reduce Gt"
'------------------------------
        Inspection = False
        Call Get_WormProfile_cp(Np, cpX, cpY, cpZ)
        Call Get_Context(Np, cpX, cpY, Xmax, Ymax, Xmin, Ymin)
        
        Call Get_ProfiloControllato(Np, cpX, cpY, cpZ)
        Call Get_GearProfile_cp_Ref(Np, cpXref, cpYref, cpZref)
        
        apDen_Ref = 0.1
        ProDen_Ref = 0.01
        Rtden_Ref = 0.2
        Call Get_GearProfile_cp_Dentato(Np, cpXden, cpYden, cpZden) 'cpX, cpY, cpZ
        Call Get_GearProfile_cp_Tol(Np, cpXtol1, cpYtol1, cpZtol1, cpXtol2, cpYtol2, cpZtol2)
'        Call Get_GearRollingProfile_cp(Np, cpX, cpY, cpZ) Get_GearProfile_cp_Dentato
     '   GoSub ETICHETTE_GRAFICI
      'Estremi dei fianchi
      
      
      Call PntRayon(DIint / 2, 0, -(Specs(13) / 2 + Specs(7)), 0, -PG / 2 + PG / Z, tTX, tTY)
      For Fianco = 1 To 2
          If tTX * (-1) ^ Fianco >= Xmax(Fianco) Then Xmax(Fianco) = tTX * (-1) ^ Fianco
          If tTY >= Ymax(Fianco) Then Ymax(Fianco) = tTY
          If tTX * (-1) ^ Fianco <= Xmin(Fianco) Then Xmin(Fianco) = tTX * (-1) ^ Fianco
          If tTY <= Ymin(Fianco) Then Ymin(Fianco) = tTY
      Next Fianco
      
      GoSub Estremi_Massimi
      GoSub RAPPRESENTAPAGINA2
      GoSub FineDocumento
      '
    Case 2    '"GRINDING CONFIGURATION"
    Rappresentazione = "WHEEL-GEAR"
    PlotTollerance = False
'Obiettivo----------Check box
      OEMX.OEM1chkINPUT(0).Visible = True: OEMX.OEM1chkINPUT(0).Caption = "Dtip" ': OEMX.OEM1chkINPUT(0).Caption = "": OEMX.OEM1chkINPUT(0).Width = 200: OEMX.OEM1chkINPUT(0).Top = 3170: OEMX.OEM1chkINPUT(0).Left = 0 '6120
      OEMX.OEM1chkINPUT(1).Visible = True: OEMX.OEM1chkINPUT(1).Caption = "Etip": OEMX.OEM1chkINPUT(1).Top = OEMX.OEM1chkINPUT(0).Top ': OEMX.OEM1chkINPUT(1).Caption = "": OEMX.OEM1chkINPUT(1).Width = 200: OEMX.OEM1chkINPUT(1).Top = 3385: OEMX.OEM1chkINPUT(1).Left = 0 '6120
'      OEMX.OEM1chkINPUT(2).Visible = True: OEMX.OEM1chkINPUT(2).Caption = "": OEMX.OEM1chkINPUT(2).Width = 200: OEMX.OEM1chkINPUT(2).Top = 3385: OEMX.OEM1chkINPUT(2).Left = 0 '6120
      OptiDtip = OEMX.OEM1chkINPUT(0).Value: OEMX.OEM1chkINPUT(0).ToolTipText = "Fix tip Diameter"
      OptiEtip = OEMX.OEM1chkINPUT(1).Value: OEMX.OEM1chkINPUT(1).ToolTipText = "Fix tip Entity"
'Optimize--------------Button
      oper = False
      OEMX.lblINP(0).Top = 100: OEMX.lblINP(0).Left = 100: OEMX.lblINP(0).Width = OEMX.lblINP(1).Width: OEMX.lblINP(0).Height = OEMX.lblINP(1).Height: OEMX.lblINP(0).ToolTipText = "Look for optimal dressing displacement and center distance correction"
      Call GetParamCondition(oper)
      If (ShowOptimizeButton) Then 'IsToolTipPresent
       OEMX.lblINP(0).Visible = True
      End If
      If (MinimizzaDeltaTip) Then '
      OEMX.lblINP(0).Caption = "Optimized"
      Else
      OEMX.lblINP(0).Caption = "Optimize"
      End If
'Acquisizione----------Button
OEMX.lblINP(2).Visible = True: OEMX.txtINP(2).Visible = False: OEMX.lblINP(2).Caption = "Get Cor": OEMX.lblINP(2).Left = OEMX.lblINP(0).Left: OEMX.lblINP(2).Width = OEMX.lblINP(0).Width: OEMX.lblINP(2).Top = OEMX.lblINP(0).Top + OEMX.lblINP(0).Height: OEMX.lblINP(2).Height = OEMX.lblINP(0).Height: OEMX.lblINP(2).ToolTipText = "Pick up current corrections"
'Giocofondomola----------SLIDER
OEMX.OEM1hsMOLA1.Visible = True: OEMX.OEM1hsMOLA1.MIN = 0: OEMX.OEM1hsMOLA1.Max = GioccoFondoMolaMax * 1000: DGioccoFondoMola = val(OEMX.OEM1hsMOLA1.Value) / 1000 'OEMX.OEM1hsMOLA1.ToolTipText = "Reduce Gt"
'------------------------------
      Inspection = False
      Call Get_ProfiloControllato(Np, cpXg, cpYg, cpZg)
      Call Get_GeneratingProfile_cp(Np, YdapRul, cpX, cpY, cpZ) 'Get_WormProfile_cp
      Call Get_GeneratingProfile_cp(Np, HRULLOREF, cpXd, cpYd, cpZd) 'Get_WormProfile_cp
      Call Get_GeneratingProfile_cp(Np, HDENTEMOLA, cpXw, cpYw, cpZw) 'Get_WormProfile_cp
      
  '    Call Get_WormProfile_cp(Np, cpX, cpY, cpZ)
  '    Call Get_Context(np, cpX, cpY, Xmax, Ymax, Xmin, Ymin)
      Call Get_GearProfile_cp_Dentato(Np, cpXden, cpYden, cpZden)
      GoSub RAPPRESENTAPAGINA
      Call Get_ProfiloControllato(Np, cpX, cpY, cpZ)
      
      Call Get_GearProfile_cp_Ref(Np, cpXref, cpYref, cpZref)
      Call Get_GearProfile_cp_Tol(Np, cpXtol1, cpYtol1, cpZtol1, cpXtol2, cpYtol2, cpZtol2)
        
      GoSub RAPPRESENTAPAGINA2
      GoSub RAPPRESENTAPAGINA3
      GoSub FineDocumento
      '
    Case 3    '"INSPECTION ELICA"
      Rappresentazione = "CHECK-GEAR"
      PlotTollerance = True
' 0) Riempi Routine
' 0) Effettua il calcolo dello svergolamento
' 1) scivi svergolamento
' 2) grafica elica
      Inspection = True
      Dim IntX, IntY As Double
        
      Call Get_WormProfile_cp(Np, cpX, cpY, cpZ)
      Call Get_Context(Np, cpX, cpY, Xmax, Ymax, Xmin, Ymin)
      IntX = (Xmax(0) - Xmin(0)) / (Sampling - 1)
      IntY = (Ymax(0) - Ymin(0)) / (Sampling - 1)
        
      ScalaX = (PicH / (Ymax(0) - Ymin(0))) * fscala
      ScalaY = ScalaX
' Input fHatoll------------------
      OEMX.lblINP(1).Visible = True: OEMX.txtINP(1).Visible = True: OEMX.lblINP(1).Left = 100 + OEMX.lblINP(1).Width: OEMX.txtINP(1).Left = OEMX.lblINP(1).Left: OEMX.txtINP(1).Width = OEMX.lblINP(1).Width: OEMX.lblINP(1).Height = OEMX.txtINP(1).Height: OEMX.lblINP(1).Top = 100: OEMX.txtINP(1).Top = OEMX.lblINP(1).Top + OEMX.lblINP(1).Height: OEMX.lblINP(1).Caption = "fHatoll": FhaToll = val(OEMX.txtINP(1).Text): OEMX.lblINP(1).ToolTipText = "Set admissible stripe width": OEMX.txtINP(1).ToolTipText = "[mm]"
      If (FhaToll <= 0 Or FhaToll > 1) Then
      FhaToll = 0.02
      End If
      OEMX.txtINP(1).Text = frmt(FhaToll, 4)
' Input micro---------------------
      Dim tmps As Double
      stmp = GetInfo("CALCOLO", "ScalafHA", Path_LAVORAZIONE_INI)
      tmps = val(stmp)
      OEMX.lblINP(4).Visible = True: OEMX.txtINP(4).Visible = True: OEMX.lblINP(4).Caption = ">SCALE<": tmps = val(OEMX.txtINP(4).Text): OEMX.lblINP(4).Top = 100: OEMX.lblINP(4).Left = OEMX.lblINP(1).Left + OEMX.lblINP(1).Width: OEMX.lblINP(4).Width = OEMX.lblINP(1).Width: OEMX.txtINP(4).Width = OEMX.txtINP(1).Width: OEMX.lblINP(4).Height = OEMX.lblINP(1).Height: OEMX.txtINP(4).Height = OEMX.txtINP(1).Height: OEMX.txtINP(4).Top = OEMX.txtINP(1).Top: OEMX.txtINP(4).Left = OEMX.lblINP(4).Left: OEMX.lblINP(4).ToolTipText = "Set orizzontal scale in micrometer": OEMX.txtINP(4).ToolTipText = "[mn]"
      If (tmps <= 0) Then
      tmps = 20 ' (IntX * 1000)
      End If
      ScalaFha = (IntX * 1000) / tmps
      OEMX.txtINP(4).Text = frmt(tmps, 4)

'Obiettivo----------Check box
      OEMX.OEM1chkINPUT(0).Visible = True: OEMX.OEM1chkINPUT(0).Caption = "Dtip" ': OEMX.OEM1chkINPUT(0).Caption = "": OEMX.OEM1chkINPUT(0).Width = 200: OEMX.OEM1chkINPUT(0).Top = 3170: OEMX.OEM1chkINPUT(0).Left = 0 '6120
      OEMX.OEM1chkINPUT(1).Visible = True: OEMX.OEM1chkINPUT(1).Caption = "Etip": OEMX.OEM1chkINPUT(1).Top = OEMX.OEM1chkINPUT(0).Top ': OEMX.OEM1chkINPUT(1).Caption = "": OEMX.OEM1chkINPUT(1).Width = 200: OEMX.OEM1chkINPUT(1).Top = 3385: OEMX.OEM1chkINPUT(1).Left = 0 '6120
'      OEMX.OEM1chkINPUT(2).Visible = True: OEMX.OEM1chkINPUT(2).Caption = "": OEMX.OEM1chkINPUT(2).Width = 200: OEMX.OEM1chkINPUT(2).Top = 3385: OEMX.OEM1chkINPUT(2).Left = 0 '6120
      OptiDtip = OEMX.OEM1chkINPUT(0).Value: OEMX.OEM1chkINPUT(0).ToolTipText = "Fix tip Diameter"
      OptiEtip = OEMX.OEM1chkINPUT(1).Value: OEMX.OEM1chkINPUT(1).ToolTipText = "Fix tip Entity"
'Optimize--------------Button
      oper = False
      OEMX.lblINP(0).Top = 100: OEMX.lblINP(0).Left = 100: OEMX.lblINP(0).Width = OEMX.lblINP(1).Width: OEMX.lblINP(0).Height = OEMX.lblINP(1).Height: OEMX.lblINP(0).ToolTipText = "Look for optimal dressing displacement and center distance correction"
      Call GetParamCondition(oper)
      If (ShowOptimizeButton) Then 'IsToolTipPresent
       OEMX.lblINP(0).Visible = True
      End If
'       OEMX.txtINP(0).Visible = True
      If (MinimizzaDeltaTip) Then '
      OEMX.lblINP(0).Caption = "Optimized"
      Else
      OEMX.lblINP(0).Caption = "Optimize"
      End If
'------------------------GET
OEMX.lblINP(2).Visible = True: OEMX.txtINP(2).Visible = False: OEMX.lblINP(2).Caption = "Get Cor": OEMX.lblINP(2).Left = OEMX.lblINP(0).Left: OEMX.lblINP(2).Width = OEMX.lblINP(0).Width: OEMX.lblINP(2).Top = OEMX.lblINP(0).Top + OEMX.lblINP(0).Height: OEMX.lblINP(2).Height = OEMX.lblINP(0).Height: OEMX.lblINP(2).ToolTipText = "Pick up current corrections"
'Giocofondomola----------SLIDER
OEMX.OEM1hsMOLA1.Visible = True: OEMX.OEM1hsMOLA1.MIN = 0: OEMX.OEM1hsMOLA1.Max = GioccoFondoMolaMax * 1000: DGioccoFondoMola = val(OEMX.OEM1hsMOLA1.Value) / 1000 'OEMX.OEM1hsMOLA1.ToolTipText = "Reduce Gt"
'------------------------------
      Call Get_GearRollingProfile_cp(Np, cpX, cpY, cpZ)
      Call Get_GearRollingProfile_cp_toll(Np, cpXtol1, cpYtol1, cpZtol1, cpXtol2, cpYtol2, cpZtol2)
      Call PntRayon(DIint / 2, 0, -(Specs(13) / 2 + Specs(7)), 0, -PG / 2 + PG / Z, tTX, tTY)
      For Fianco = 1 To 2

          If tTX * (-1) ^ Fianco >= Xmax(Fianco) Then Xmax(Fianco) = tTX * (-1) ^ Fianco
          If tTY >= Ymax(Fianco) Then Ymax(Fianco) = tTY
          If tTX * (-1) ^ Fianco <= Xmin(Fianco) Then Xmin(Fianco) = tTX * (-1) ^ Fianco
          If tTY <= Ymin(Fianco) Then Ymin(Fianco) = tTY
      Next Fianco
      
      GoSub Estremi_Massimi
      GoSub RAPPRESENTAPAGINA2
      GoSub FineDocumento

'-------------------------------------
'-> Dim INCOMINGS(1 To 20)
'-------------------------------------
' RULLO
'-> INCOMINGS(1) = LEP("DRULLOPROF_G") 'FIN.Drullo     '= 120                     ' Diametro rullo
'-> INCOMINGS(2) = Get_Worm_Tilt("GRADI") 'LEP("INCLMOLA_G") 'FIN.IncRulloD  '= 1.8356               ' Inclinazione rullo
' Mola VITE
'-> INCOMINGS(3) = 2 * val(OPC_LEGGI_DATO("/ACC/NCK/GUD7/REXTMOLA_G"))   ' 176 'Lp("REXTMOLA_G")
'-> INCOMINGS(4) = Get_Worm_Tilt("GRADI") 'LEP("INCLMOLA_G")    '= 1.836 '
'-> INCOMINGS(5) = LEP("NUMPRINC_G")  'FIN.Nfil       '= 2                    ' Numero filetti
'-> INCOMINGS(6) = LEP("PassoMola_G")   '= 17.1705 '
'-> INCOMINGS(7) = LEP("ALFAUT_G") 'FIN.ApMolaD    '= 21
'-> INCOMINGS(8) = LEP("MNUT_G")    'FIN.MrMola     '= 2.73137                 ' Cre.Rif.Mola : Modulo Normale MODULO_MOLA
' DISEGNO
'-> INCOMINGS(9) = LEP("ALFA_G") ' Aprd           '= 21
'-> INCOMINGS(10) = LEP("BETA_G") 'HelpD(NE)     '= 33.5
'-> INCOMINGS(11) = LEP("MN_G")    'LEP("MN_G") 'Mr            '= 2.73137 MODULO NORMALE
'-> INCOMINGS(12) = LEP("MN_G") 'LEP("ALFA_G") 'Mapp(NE)      '= 2.73137 MODULO APPARENTE
'-> INCOMINGS(13) = LEP("NumDenti_G")  'Z1(NE)        '= 67
'-> INCOMINGS(14) = LEP("QW") 'Wdt(NE)       '= 31.7252
'-> INCOMINGS(15) = LEP("ZW") 'Kdt(NE)       '= 4
'-> INCOMINGS(16) = LEP("DEXT_G")       '= 224.52
'-> INCOMINGS(17) = LEP("DINT_G")       '= 208.55
'-> INCOMINGS(18) = LEP("DSAP_G") 'SAP(NE)       '= 211
' Dim Fascia1  As Double: Dim Fascia2 As Double: Dim Bomb1 As Double: Dim Bomb2 As Double
' Call CALCOLA_BOMBATURA_EQUIVALENTE(Fascia1, Fascia2, Bomb1, Bomb2)
' INCOMINGS(19) = (Fascia1 + Fascia2) / 2
' INCOMINGS(20) = (Bomb1 + Bomb2) / 2
 
'->INCOMINGS(19) = (LEP("iFas1Fsx") + LEP("iFas1Fdx")) / 2
'->INCOMINGS(20) = (LEP("iBom1Fsx") + LEP("iBom1Fdx")) / 2
'-------------------------------------
'->Call Fill_AntiTwist_MV_paramenter(INCOMINGS)
Call Init_AntiTwist_MV
'-------------------------------------
Dim CorrEvolv(1 To 3) As Double: Call CalcoloSvergolamento(LEP("iBom1Fsx"), LEP("iBom1Fdx"), CorrEvolv)

GoSub RAPPRESENTAPAGINA4

    Case 4    ' HGkinetic
        ' Dati worm
 OEMX.OEM1hsMOLA1.Visible = False
 If (hndHGkinetic = 0) Then
'-------------------------------------
' read HQ flags
 Dim sFileText As String ': sFileText = "0"
 Dim Path As String: Path = App.Path  '.Directory.GetCurrentDirectory()
 Dim idFileNo As Integer: idFileNo = FreeFile
 Dim Filepath(1) As String: Filepath(0) = Path & "\HGkinetic\COR.txt": Filepath(1) = Path & "\HGkinetic\HQ.txt"
 For i = 0 To 1
  Open Filepath(i) For Input As #idFileNo
   Do While Not EOF(idFileNo)
    Input #idFileNo, sFileText
   Loop
  OEMX.HGkineticF(i).Value = sFileText
 Close #idFileNo
 Next i
 '-------------------------------------
  OEMX.HGkineticF(0).Visible = True
  OEMX.HGkineticF(1).Visible = True
 '-------------------------------------
 If (OEMX.HGkineticF(1).Value = 1) Then
  OEMX.HGkineticF(1).Caption = "HQS": OEMX.HGkineticF(1).ToolTipText = "Hight Quality Simulation" ': OEMX.HGkineticF(0).Left = 7400 'OEMX.HGkineticF(1).Top = 240
 Else
  OEMX.HGkineticF(1).Caption = "LQS": OEMX.HGkineticF(1).ToolTipText = "Low Quality Simulation" ': OEMX.HGkineticF(0).Left = 7400  'OEMX.HGkineticF(1).Top = 120
 End If
 If (OEMX.HGkineticF(0).Value = 1) Then
  OEMX.HGkineticF(0).Caption = "WC": OEMX.HGkineticF(0).ToolTipText = "With Correction" ':  OEMX.HGkineticF(1).Left = 7400 'OEMX.HGkineticF(1).Top = 360
 Else
  OEMX.HGkineticF(0).Caption = "WOC": OEMX.HGkineticF(0).ToolTipText = "WithOut Correction" ': OEMX.HGkineticF(1).Left = 7400  'OEMX.HGkineticF(1).Top = 360
 End If
'-------------------------------------
      HGkinetic_XMLpopulate
      HGkinetic_cmdOpenWindow
'-------------------------------------
 End If
'-------------------------------------
GoTo salta:
      'CARATTERE DI STAMPA TESTO
      If (LINGUA = "CH") Then TextFont = "MS Song"
      If (SiStampa = 1) Then
        For i = 1 To 2
          Printer.FontName = "Courier New"
          Printer.FontSize = 8
          Printer.FontBold = False
        Next i
      Else
        For i = 1 To 2
          OEMX.OEM1txtHELP.FontName = TextFont
          OEMX.OEM1txtHELP.FontSize = 10
          OEMX.OEM1txtHELP.FontBold = False
          OEMX.OEM1txtHELP.BackColor = &H0&
          OEMX.OEM1txtHELP.ForeColor = &HFFFFFF
        Next i
      End If
      OEMX.lblINP(0).Visible = False
      OEMX.txtINP(0).Visible = False
      OEMX.lblINP(1).Visible = False
      OEMX.txtINP(1).Visible = False
      GoSub ETICHETTE_GRAFICI
      OEMX.OEM1txtHELP.Visible = True
      OEMX.OEM1txtHELP.Move OEMX.OEM1PicCALCOLO.Left, OEMX.OEM1PicCALCOLO.Top, OEMX.OEM1PicCALCOLO.Width, OEMX.OEM1PicCALCOLO.Height + OEMX.OEM1Combo1.Height
      Open g_chOemPATH & "\DATI.TEO" For Input As 1
        OEMX.OEM1txtHELP.Text = ""
        OEMX.OEM1txtHELP.Text = Input$(LOF(1), 1)
      Close #1
      If (SiStampa = 1) Then
        Open g_chOemPATH & "\DATI.TEO" For Input As 1
          While Not EOF(1)
            Line Input #1, stmp
            Printer.Print stmp
            WRITE_DIALOG stmp
          Wend
        Close #1
        Printer.EndDoc
        WRITE_DIALOG OEMX.OEM1Combo1.Caption & " --> " & Printer.DeviceName & " OK!!"
      End If
      '
salta:
     ' Return
  End Select
  '
End If
  '
  Dim SensoElica As Integer
  
    SensoElica = LEP("iSensoElica")
    For i = 1 To UBound(CalcoloEsterni.LCont, 2)
      CalcoloEsterni.LCont(1, i) = -SensoElica * CalcoloEsterni.LCont(1, i)
      CalcoloEsterni.LCont(2, i) = -SensoElica * CalcoloEsterni.LCont(2, i)
    Next i
    CalcoloEsterni.DB = DB
    CalcoloEsterni.DP = DP
    CalcoloEsterni.DE = D2
    CalcoloEsterni.DI = DIint
    CalcoloEsterni.DIOtt = Ri * 2
    CalcoloEsterni.Np = Np
  
Exit Sub

errCALCOLO_INGRANAGGIO_ESTERNO_380_MV:
  If FreeFile > 0 Then Close
  If (Err = 6) Or (Err = 11) Then
    WRITE_DIALOG "Sub CALCOLO_INGRANAGGIO_ESTERNO_380_MV: ERROR -> " & Err
    If (LINGUA = "IT") Then
        MsgBox ("Allarme: Probabile tratto per correzione troppo corto")
    Else
        MsgBox ("Warning: Profile Correction Zone too short")
    End If
    Err = 0
    Exit Sub
  End If
  WRITE_DIALOG "Sub CALCOLO_INGRANAGGIO_ESTERNO_380_MV: ERROR -> " & Err
  Err = 0
  'Resume Next
  Exit Sub
  
CALCOLO_DELLE_CORREZIONI_PROFILO_K:
  ' 13.04.98
  ' Fianco 1
  E265 = 0: E19 = 1
  For i = 0 To Np
    CB(Fianco, i) = 0
  Next i
  ' Controllo TIF1
  If Tif(Fianco, 1) = 0 Then GoTo CALCK2
  If Tif(Fianco, 1) < MM1 Then GoTo CALCK2
  If Tif(Fianco, 1) > MM2 Then Tif(Fianco, 1) = MM2
  E260 = Int((Tif(Fianco, 1) - MM1 + DIPU / 2) / DIPU) * DIPU
  E261 = E260 / DIPU + 1
  If Bombatura(Fianco, 1) <> 0 Then
    E262 = (E260 * E260 / 4 + Bombatura(Fianco, 1) * Bombatura(Fianco, 1)) / 2 / Bombatura(Fianco, 1) ' RB ( - , + )
  End If
  E263 = 0: E17 = 0
ALCK1:
  E263 = E263 + 1: E17 = E17 + 1
  CB(Fianco, E17) = Rastremazione(Fianco, 1) * (E263 - 1) / (E261 - 1)
  If Bombatura(Fianco, 1) <> 0 Then
    E264 = Abs(E260 / 2 - (E263 - 1) * DIPU)
    CB(Fianco, E17) = CB(Fianco, E17) + Bombatura(Fianco, 1) + Sqr(E262 * E262 - E264 * E264) * E262 / Abs(E262) - E262
  End If
  If E263 < E261 Then GoTo ALCK1
  E265 = CB(Fianco, E17)
  ' Tratto 2
  E19 = E261
  ' Tratto 2:TIF2, RASTR2, BOMB2
  If Tif(Fianco, 2) = 0 Then GoTo AZCK
  If Tif(Fianco, 2) <= Tif(Fianco, 1) Then
    WRITE_DIALOG "ERROR: TIF2 <= TIF1"
    Exit Sub     ' ERRORE se TIF2 <= TIF1
  End If
  If Tif(Fianco, 2) > MM2 Then Tif(Fianco, 2) = MM2
  E266 = Int((Tif(Fianco, 2) - MM1 - E260 + DIPU / 2) / DIPU) * DIPU
  E267 = E266 / DIPU + 1
  If Bombatura(Fianco, 2) <> 0 Then E262 = (E266 * E266 / 4 + Bombatura(Fianco, 2) * Bombatura(Fianco, 2)) / 2 / Bombatura(Fianco, 2)
  E263 = 1
ALCK2:
  E263 = E263 + 1
  E17 = E17 + 1
  CB(Fianco, E17) = Rastremazione(Fianco, 2) * (E263 - 1) / (E267 - 1) + E265
  If Bombatura(Fianco, 2) <> 0 Then
    E264 = Abs(E266 / 2 - (E263 - 1) * DIPU)
    CB(Fianco, E17) = CB(Fianco, E17) + Bombatura(Fianco, 2) + Sqr(E262 * E262 - E264 * E264) * E262 / Abs(E262) - E262
  End If
  If E263 < E267 Then GoTo ALCK2
  E265 = CB(Fianco, E17): E19 = E17 - 1
  If Tif(Fianco, 3) = 0 Then GoTo AZCK
  If Tif(Fianco, 3) <= Tif(Fianco, 2) Then
    WRITE_DIALOG "ERROR: TIF3 <= TIF2"
    Exit Sub
  End If
  If Tif(Fianco, 3) > MM2 Then Tif(Fianco, 3) = MM2
  E268 = Int((Tif(Fianco, 3) - MM1 - E260 - E266 + DIPU / 2) / DIPU) * DIPU
  E269 = E268 / DIPU + 1
  If Bombatura(Fianco, 3) <> 0 Then E262 = (E268 * E268 / 4 + Bombatura(Fianco, 3) * Bombatura(Fianco, 3)) / 2 / Bombatura(Fianco, 3)
  E263 = 1
ALCK3:
  E263 = E263 + 1: E17 = E17 + 1
  CB(Fianco, E17) = Rastremazione(Fianco, 3) * (E263 - 1) / (E269 - 1) + E265
  If Bombatura(Fianco, 3) <> 0 Then
    E264 = Abs(E268 / 2 - (E263 - 1) * DIPU)
    CB(Fianco, E17) = CB(Fianco, E17) + Bombatura(Fianco, 3) + Sqr(E262 * E262 - E264 * E264) * E262 / Abs(E262) - E262
  End If
  If E263 < E269 Then GoTo ALCK3
  E19 = E17 - 1:  E265 = CB(Fianco, E17)
AZCK:
  E17 = 1 + E19: E19 = Np - E19
  If E19 < 1 Then GoTo CALCK2
  For i = 1 To E19
    CB(Fianco, E17) = E265
    E17 = E17 + 1
  Next i
CALCK2:
  MMp = Sqr(Rp * Rp - Rb * Rb)
  E261 = Int((MMp - MM0 + 0.5) / DIPU)
  If E261 < 0 Then E261 = 0
  If E261 >= Np Then E261 = Np - 1
  E262 = CB(Fianco, E261): E17 = 1
  For i = 1 To Np
    CB(Fianco, E17) = CB(Fianco, E17) - E262
    E17 = E17 + 1
  Next i
  CB(Fianco, 0) = CB(Fianco, 1) + (CB(Fianco, 1) - CB(Fianco, 2))
Return

CALCOLO_T0_FI0:
  RX = Sqr(MM(i) ^ 2 + Rb ^ 2)
  TanA = Sqr((RX / Rb) ^ 2 - 1)
  Ax = Atn(TanA)
  InvAx = TanA - Ax
  Sx = (S0Nt / 2 / Rp + F3 - InvAx) * 2 * RX
  VX = 2 * PG * RX / Z - Sx
  If Abs(Beta) < PG / 4 Then
    T0 = PG / 2 - VX / (2 * RX) + Atn(CORREZIONE / Rb)
  Else
    'Calcolo del passo assiale
    PAS = Tan(PG / 2 - Beta) * 2 * PG * Rp
    T0 = PG / 2 - VX / (2 * RX) + (2 * PG * CORREZIONE) / (Sin(Beta) * PAS * Cos(ALFAn))
  End If
  FI0 = T0 - Ax
  If T0 < 0 Then FI0 = FI0 - 2 * PG
7092:
  If FI0 > PG Then FI0 = FI0 - PG * 2: GoTo 7092
  If FI0 < -PG Then FI0 = FI0 + PG * 2: GoTo 7092
  A(Fianco, i) = PG / 2 - T0
Return

Purea:
  PAS = Tan(PG / 2 - Beta) * 2 * PG * Rp
  SIGMA = (90 - BetaMr * 180 / PG) * PG / 180
  Rmin = PAS / ((PG * 2) * Tan(SIGMA))
  Tp = PAS / (PG * 2)
  DRM = IL * Rmin
  RMPD = Rmin + IL
  YD(Fianco, i) = RX * Sin(T0)
  XD(Fianco, i) = RX * Cos(T0)
  RQC = RX * RX * Cos(T0 - FI0)
  AA = RQC * Sin(T0) + (2 * Tp * Tp + DRM) * Sin(FI0)
  BB = (DRM + Tp * Tp) * Cos(FI0) + RQC * Cos(T0)
  cc = 2 * (-DRM * Sin(FI0) - RQC * Sin(T0) + RX * RMPD * Cos(T0 - FI0))
  Dim tmp
  tmp = (BB / AA) * (BB / AA) - (cc / AA)
  If (tmp > 0) Then
  RAD = Sqr(tmp)
  Else
  RAD = 0
  ' warning error
  End If
  T1 = BB / AA + RAD
  T2 = BB / AA - RAD
  Ti = T2
  If Abs(T2) > Abs(T1) Then Ti = T1
  If T0 < 0 Then Ti = T1
  While Ti >= PG: Ti = PG * 0.9: Wend
  TSI = Ti
  For j = 1 To 10
    fp1 = RQC * Sin(T0 + TSI) + DRM * Sin(FI0 + TSI) + Tp * Tp * TSI * Cos(FI0 + TSI) - RX * RMPD * Cos(T0 - FI0)
    fp2 = RQC * Cos(T0 + TSI) + DRM * Cos(FI0 + TSI) - Tp * Tp * TSI * Sin(FI0 + TSI) + Tp * Tp * Cos(FI0 + TSI)
    TSI = TSI - fp1 / fp2
  Next j
  Ti = TSI
  XS = RX * Cos(T0 + Ti)
  Ys = RX * Sin(T0 + Ti)
  Zs = Tp * Ti
  'CALCOLO TANGENTE AL PROFILO MOLA IN TETAS
  Ap = Tan(Ti + FI0) / XS
  Bp = -1 / XS
  Cp = (1 + Ys * Tan(Ti + FI0) / XS) / Tp
  Lq = Tan(SIGMA)
  ArgSeno = (Ap * Lq + Cp) / (Sqr(Ap * Ap + Bp * Bp + Cp * Cp) * Sqr(Lq * Lq + 1))
  SINa2 = ArgSeno * ArgSeno
  TanA = Sqr(SINa2 / (1 - SINa2))
  B(i) = 90 - (Atn(TanA) * (180 / PG))
  Uprof = XS * Cos(SIGMA) - Zs * Sin(SIGMA)
  Vprof = Ys - IL
  Wprof = Zs * Cos(SIGMA) + XS * Sin(SIGMA)
  Rprof = Sqr(Uprof ^ 2 + Vprof ^ 2)
  X(Fianco, i) = Wprof
  Y(Fianco, i) = Rprof
Return

19000:
  'MOLA --> INGRANAGGIO (sez.trasversale)
  'INPUT : X(JJ), Y(JJ), ALFAM(JJ)
  'OUTPUT: XD(JJ), YD(JJ), R(JJ), A(JJ), API(JJ)
  ENTRX = IL:  INF = BetaMr:  EXF = X(Fianco, jj): RXF = Y(Fianco, jj): APFD = AlfaM(Fianco, jj) * 180 / PG
  If Abs(APFD) > 87 Then APFD = Sgn(APFD) * 87
  If EXF < 0 Then APFD = -APFD
  If APFD < 0 Then APF = ((180 + APFD) * (PG / 180)) Else APF = ((APFD) * (PG / 180))
  GoSub 19200
  L(Fianco, jj) = V17: D(jj) = Rayon * 2: A(Fianco, jj) = ALFAs: API(jj) = BETAi * 180 / PG
  XD(Fianco, jj) = Rayon * Sin(ALFAs): YD(Fianco, jj) = Rayon * Cos(ALFAs)
Return

19200: '************** S/PAS ************ CALCUL PIECE D'APRES FRAISE
  SENS = 1: If EXF < 0 Then SENS = -1
  EXF = EXF * SENS: APF = APF * SENS: v7 = PAS / 2 / PG
  GoSub 20500: Rayon = RXV: ALFAs = Ang * SENS: AngS = Ang
  RXF = RXF + 0.01: EXF = EXF - 0.01 * Tan(APF): GoSub 20500: Y1 = YAPP: X1 = XAPP
  RXF = RXF - 0.02: EXF = EXF + 0.02 * Tan(APF): GoSub 20500
  RXF = RXF + 0.01: EXF = EXF - 0.01 * Tan(APF)
  BETAi = (Atn((X1 - XAPP) / (YAPP - Y1)) - AngS) * SENS
  APF = APF * SENS: EXF = EXF * SENS
Return

20500:
  V12 = -Tan(APF) * (ENTRX * Sin(INF) + v7 * Cos(INF))
  V13 = v7 * Sin(INF) - ENTRX * Cos(INF)
  V14 = Cos(INF) * ((-Tan(APF) * EXF) + RXF)
  V15 = (V14 ^ 2) + (V12 ^ 2) - (V13 ^ 2)
  If V15 > 0 Then
    V15 = Sqr(V15)
    V16 = -(V15 + V12) / (V13 - V14)
    V15 = (V15 - V12) / (V13 - V14)
    VPB = V16: If Abs(V15) < Abs(V16) Then VPB = V15
    V12 = 2 * Atn(VPB)
    V13 = (RXF * Sin(INF) * Sin(V12)) + EXF * Cos(INF)
    V13 = Atn(V13 / ((RXF * Cos(V12)) - ENTRX))
    'DISTANZA ASSE MOLA PIANO TANGENTE --------------
    V17 = EXF * Sin(INF) - RXF * Sin(V12) * Cos(INF)
    '------------------------------------------------
    RXV = (ENTRX - RXF * Cos(V12)) / Cos(V13)
    EXV = EXF * Sin(INF) - v7 * V13 - RXF * Sin(V12) * Cos(INF)
    Ang = 2 * PG * EXV / PAS
    YAPP = RXV * Cos(Ang): XAPP = -RXV * Sin(Ang)
  Else
    WRITE_DIALOG "EVALUATION NOT POSSIBLE!!!"
    Exit Sub
  End If
Return

Estremi_Massimi:
  Xmax(0) = Xmax(1): Ymax(0) = Ymax(1): Xmin(0) = Xmin(1): Ymin(0) = Ymin(1)
  For Fianco = 1 To 2
    If Xmax(Fianco) >= Xmax(0) Then Xmax(0) = Xmax(Fianco)
    If Ymax(Fianco) >= Ymax(0) Then Ymax(0) = Ymax(Fianco)
    If Xmin(Fianco) <= Xmin(0) Then Xmin(0) = Xmin(Fianco)
    If Ymin(Fianco) <= Ymin(0) Then Ymin(0) = Ymin(Fianco)
  Next Fianco
Return

LineaF1:
  pX1 = -(D(i) - Xmin(0)) * ScalaX + 3 * PicW / 5
  pY1 = -L(1, i) * ScalaY * BETAseg + PicH / 2
  pX2 = -(D(i) - Xmin(0)) * ScalaX + 3 * PicW / 5
  pY2 = 0 + PicH / 2
  Call LINEA_FIANCO(OEMX, 1, SiStampa, stmp, pX1, pY1, pX2, pY2)
Return

LineaF2:
  pX1 = (D(i) - Xmin(0)) * ScalaX + 3 * PicW / 5
  pY1 = L(2, i) * ScalaY * BETAseg + PicH / 2
  pX2 = (D(i) - Xmin(0)) * ScalaX + 3 * PicW / 5
  pY2 = 0 + PicH / 2
  Call LINEA_FIANCO(OEMX, 2, SiStampa, stmp, pX1, pY1, pX2, pY2)
Return

FineDocumento:
  'Dimensione dei caratteri
  If (SiStampa = 1) Then
    For i = 1 To 2
      Printer.FontName = TextFont
      Printer.FontSize = 14
      Printer.FontBold = True
    Next i
    'Logo
    'Printer.PaintPicture OEMX.PicLogo, 0, PicH, 20, 20
    'Nome del documento
    stmp = Date & " " & Time & "                  " & OEMX.OEM1Combo1.Caption
    Printer.CurrentX = 0 + 25
    Printer.CurrentY = PicH
    Printer.Print stmp
    'Scala X
    If OEMX.lblINP(0).Visible = True Then
      stmp = OEMX.lblINP(0).Caption & ": x" & OEMX.txtINP(0).Text
      Printer.CurrentX = 0 + 25
      Printer.CurrentY = PicH + 10
      Printer.Print stmp
    End If
    'Scala Y
    If OEMX.lblINP(1).Visible = True Then
      stmp = OEMX.lblINP(1).Caption & ": x" & OEMX.txtINP(1).Text
      Printer.CurrentX = 0 + 3 * PicW / 5
      Printer.CurrentY = PicH + 10
      Printer.Print stmp
    End If
    'Parametri
    Call STAMPA_INGRANAGGIO_ESTERNO_380(TextFont, PicW, PicH)
    '
    OEMX.SuOEM1.Visible = True
    Call OEMX.SuOEM1.STAMPA(False, , , , 150)
    OEMX.SuOEM1.Visible = False
    '
    Printer.EndDoc
    WRITE_DIALOG OEMX.OEM1Combo1.Caption & " --> " & Printer.DeviceName & " OK!!"
  End If
Return

Scrivi:
  'Visualizza nella finestra di calcolo
  OEMX.OEM1PicCALCOLO.CurrentX = pX1
  OEMX.OEM1PicCALCOLO.CurrentY = pY1 + i * pY2
  OEMX.OEM1PicCALCOLO.Print stmp
  If (SiStampa = 1) Then
    'Invia alla stampante
    Printer.CurrentX = pX1
    Printer.CurrentY = pY1 + i * pY2
    Printer.Print stmp
  End If
  i = i + 1
Return

ETICHETTE_GRAFICI:
  'NASCONDO LA CASELLA DI TESTO
  OEMX.OEM1txtHELP.Visible = False
  'VISUALIZZO LE SCALE
  i = LoadString(g_hLanguageLibHandle, 1000, Atmp, 255)
  If i > 0 Then OEMX.lblINP(0).Caption = Left$(Atmp, i)
  i = LoadString(g_hLanguageLibHandle, 1001, Atmp, 255)
  If i > 0 Then OEMX.lblINP(1).Caption = Left$(Atmp, i)
  If (SiStampa = 1) Then
    Printer.ScaleMode = 6
    WRITE_DIALOG OEMX.OEM1Combo1.Caption & " --> " & Printer.DeviceName
  Else
    OEMX.OEM1PicCALCOLO.BackColor = &HFFFFFF
    OEMX.OEM1PicCALCOLO.Cls
  End If
Return

GRAFICO_MOLA_o_INGRANAGGIO_FIANCO:
  If (SiStampa = 1) Then
    Printer.Line (pX1, pY1)-(pX2, pY2)
  Else
    OEMX.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2)
  End If
Return
  
ScriviCONTATTO:
  If (SiStampa = 1) Then
    If BETAseg > 0 Then
      Printer.CurrentX = PicW - Printer.TextWidth(stmp)
    Else
      Printer.CurrentX = 0
    End If
    Printer.CurrentY = Printer.TextHeight(stmp) * i
    Printer.Print stmp
  Else
    If BETAseg > 0 Then
      OEMX.OEM1PicCALCOLO.CurrentX = PicW - OEMX.OEM1PicCALCOLO.TextWidth(stmp)
    Else
      OEMX.OEM1PicCALCOLO.CurrentX = 0
    End If
    OEMX.OEM1PicCALCOLO.CurrentY = OEMX.OEM1PicCALCOLO.TextHeight(stmp) * i
    OEMX.OEM1PicCALCOLO.Print stmp
  End If
  i = i + 1
Return

RAPPRESENTAPAGINA:
 
      'Estremi dei fianchi
      Dim tX, tY As Double
      Call PntRayon(DIint / 2, 0, -(Specs(13) / 2 + Specs(7)), 0, -PG / 2 + PG / Z, tX, tY)
   '   ty = (ty + (Specs(13) / 2 + Specs(7)))
   If (Not MinimizzaDeltaTip) Then
      For Fianco = 1 To 2
        Xmax(Fianco) = cpX(1)
        Ymax(Fianco) = cpY(1)
        Xmin(Fianco) = cpX(1)
        Ymin(Fianco) = cpY(1)
        For i = 1 To (Np) '  + NPR1 + 1
          If cpX(i) * (-1) ^ Fianco >= Xmax(Fianco) Then Xmax(Fianco) = cpX(i) * (-1) ^ Fianco
          If cpY(i) >= Ymax(Fianco) Then Ymax(Fianco) = cpY(i)
          If cpX(i) * (-1) ^ Fianco <= Xmin(Fianco) Then Xmin(Fianco) = cpX(i) * (-1) ^ Fianco
          If cpY(i) <= Ymin(Fianco) Then Ymin(Fianco) = cpY(i)
        Next i

        
          If tX * (-1) ^ Fianco >= Xmax(Fianco) Then Xmax(Fianco) = tX * (-1) ^ Fianco
          If tY >= Ymax(Fianco) Then Ymax(Fianco) = tY
          If tX * (-1) ^ Fianco <= Xmin(Fianco) Then Xmin(Fianco) = tX * (-1) ^ Fianco
          If tY <= Ymin(Fianco) Then Ymin(Fianco) = tY
        
        XmaxOLD(Fianco) = Xmax(Fianco)
        YmaxOLD(Fianco) = Ymax(Fianco)
        XminOLD(Fianco) = Xmin(Fianco)
        YminOLD(Fianco) = Ymin(Fianco)
      Next Fianco
      Else
      For Fianco = 1 To 2
              Xmax(Fianco) = XmaxOLD(Fianco)
        Ymax(Fianco) = YmaxOLD(Fianco)
        Xmin(Fianco) = XminOLD(Fianco)
        Ymin(Fianco) = YminOLD(Fianco)
      Next Fianco
      
      End If
      GoSub Estremi_Massimi
      
      'ScalaX = Val(GetInfo("CALCOLO", "ScalaMOLA", Path_LAVORAZIONE_INI))
      ScalaX = (PicH / (Ymax(0) - Ymin(0))) * fscala
      ScalaY = ScalaX
      OEMX.txtINP(0) = ScalaX
'      OEMX.txtINP(0).Text = frmt(ScalaX, 4)
'      OEMX.txtINP(1).Text = frmt(ScalaY, 4)
'-------------------------------------
If (Rappresentazione = "RULLO-MOLA") Then Call Plot_Rullo(cpXd, ScalaX, PicW, cpYd, Ymax(0), Ymin(0), ScalaY, PicH, Np, "Surface")  'Call FillRullo(cpXd, ScalaX, PicW, cpYd, Ymax(0), Ymin(0), ScalaY, PicH, Np)
If (Rappresentazione = "RULLO-MOLA") Then Call FillWheel(cpXw, ScalaX, PicW, cpYw, Ymax(0), Ymin(0), ScalaY, PicH, Np)
If (Rappresentazione = "MOLA-INGRANAGGIO") Then Call FillGear(cpXden, ScalaX, PicW, cpYden, Ymax(0), Ymin(0), ScalaY, PicH, Np - 1, DIint, (Specs(13) / 2 + Specs(7)), PG / Z)
If (Rappresentazione = "MOLA-INGRANAGGIO") Then Call FillWheel(cpXw, ScalaX, PicW, cpYw, Ymax(0), Ymin(0), ScalaY, PicH, Np)
'-------------------------------------
      'Fianco 1: profilo teorico
      stmp = "F1"
      OEMX.OEM1PicCALCOLO.ForeColor = RGB(134, 243, 174): OEMX.OEM1PicCALCOLO.DrawStyle = 0: OEMX.OEM1PicCALCOLO.DrawWidth = 2
      For i = 1 To (Int(Np) - 1)
        pX1 = -cpX(i) * ScalaX + 3 * PicW / 5: pY1 = -(cpY(i) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
        pX2 = -cpX(i + 1) * ScalaX + 3 * PicW / 5: pY2 = -(cpY(i + 1) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
        If (i = 2) Then
        OEMX.OEM1PicCALCOLO.ForeColor = vbBlack
        stmp = "Required": OEMX.OEM1PicCALCOLO.CurrentX = pX2 - 0 * OEMX.OEM1PicCALCOLO.TextWidth(stmp): OEMX.OEM1PicCALCOLO.CurrentY = pY2 - OEMX.OEM1PicCALCOLO.TextHeight(stmp): OEMX.OEM1PicCALCOLO.Print stmp: OEMX.OEM1PicCALCOLO.ForeColor = RGB(134, 243, 174)
        End If
        GoSub GRAFICO_MOLA_o_INGRANAGGIO_FIANCO
      Next i
      'Fianco 2: profilo teorico
      stmp = "F2"
      For i = (Int(Np) - 1) To 1 Step -1
        pX1 = cpX(i) * ScalaX + 3 * PicW / 5: pY1 = -(cpY(i) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
        pX2 = cpX(i + 1) * ScalaX + 3 * PicW / 5: pY2 = -(cpY(i + 1) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
        GoSub GRAFICO_MOLA_o_INGRANAGGIO_FIANCO
      Next i
      OEMX.OEM1PicCALCOLO.ForeColor = vbBlack: OEMX.OEM1PicCALCOLO.DrawWidth = 1
'-------------------------------------
'Fianco 1: profilo rullo
'-------------------------------------

 Dim stmWarn As String: stmWarn = "WARNING: "
 If (YdapRul > HRULLOREF) Then
  OEMX.OEM1PicCALCOLO.ForeColor = vbRed: OEMX.OEM1PicCALCOLO.DrawWidth = 2
  stmWarn = "-DRESSING CORRECTION REQUIRED-": WRITE_DIALOG stmWarn
 Else
  OEMX.OEM1PicCALCOLO.ForeColor = RGB(154, 205, 50): OEMX.OEM1PicCALCOLO.DrawWidth = 1
 End If
'-------------------------------------
 If (Rappresentazione = "RULLO-MOLA") Then
  Call Plot_Rullo(cpXd, ScalaX, PicW, cpYd, Ymax(0), Ymin(0), ScalaY, PicH, Np, "Boundary")
  i = 2: stmp = "Dresser": OEMX.OEM1PicCALCOLO.CurrentX = 3 * PicW / 5 - OEMX.OEM1PicCALCOLO.TextWidth(stmp) / 2: OEMX.OEM1PicCALCOLO.CurrentY = pY2 - OEMX.OEM1PicCALCOLO.TextHeight(stmp): OEMX.OEM1PicCALCOLO.Print stmp
 End If
'----------------------------------
' mola reale
'----------------------------------
 If (HDENTEMOLA > HRULLOREF Or HDENTEMOLA < YdapRul) Then
  OEMX.OEM1PicCALCOLO.ForeColor = vbRed: OEMX.OEM1PicCALCOLO.DrawWidth = 2
  stmWarn = stmWarn & "-CHANGE WHEEL HEIGHT-"
  WRITE_DIALOG stmWarn
 Else
  OEMX.OEM1PicCALCOLO.ForeColor = RGB(71, 60, 139): OEMX.OEM1PicCALCOLO.DrawWidth = 1
 End If
      'Fianco 1: profilo mola reale
      For i = 1 To (Int(Np) - 1)
  '    If (i = 1) Then cpXw(i) = -10000
        pX1 = -cpXw(i) * ScalaX + 3 * PicW / 5: pY1 = -(cpYw(i) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
        pX2 = -cpXw(i + 1) * ScalaX + 3 * PicW / 5: pY2 = -(cpYw(i + 1) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2

        GoSub GRAFICO_MOLA_o_INGRANAGGIO_FIANCO
      Next i
      'Fianco 2: profilo mola reale
      For i = (Int(Np) - 1) To 1 Step -1
  '    If (i = 1) Then cpXw(i) = -10000
        pX1 = cpXw(i) * ScalaX + 3 * PicW / 5: pY1 = -(cpYw(i) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
        pX2 = cpXw(i + 1) * ScalaX + 3 * PicW / 5: pY2 = -(cpYw(i + 1) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
        GoSub GRAFICO_MOLA_o_INGRANAGGIO_FIANCO
        If (i = 1) Then
        stmp = "Wheel": OEMX.OEM1PicCALCOLO.CurrentX = pX2 - OEMX.OEM1PicCALCOLO.TextWidth(stmp) * 0: OEMX.OEM1PicCALCOLO.CurrentY = pY2 - OEMX.OEM1PicCALCOLO.TextHeight(stmp): OEMX.OEM1PicCALCOLO.Print stmp
        End If
      Next i
'----------------------------------
OEMX.OEM1PicCALCOLO.ForeColor = vbBlack: OEMX.OEM1PicCALCOLO.DrawWidth = 1
'linee riferimento
    PXY(3, 1) = -((Specs(11) - SpMola * 2) / 2) * ScalaX + 3 * PicW / 5
    PXY(4, 1) = ((Specs(11) - SpMola * 2) / 2) * ScalaX + 3 * PicW / 5
      PX3 = -(Specs(11) - SpMola / 2) * ScalaX + 3 * PicW / 5
      pY3 = -(Specs(12) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2   '- (Ymax(0) + Ymin(0)/ 2
      pX4 = (Specs(11) - SpMola / 2) * ScalaX + 3 * PicW / 5
      pY4 = -(Specs(12) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2  '- (Ymax(0) + Ymin(0)) / 2
'linee TIP
      pX5 = -Specs(16) * ScalaX + 3 * PicW / 5: pY5 = -(Specs(17) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
      pX6 = Specs(16) * ScalaX + 3 * PicW / 5: pY6 = -(Specs(17) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
    pX5 = PX3: pX6 = pX4
    
    
        OEMX.OEM1PicCALCOLO.ForeColor = vbBlack
        OEMX.OEM1PicCALCOLO.DrawWidth = 1
        
      pX1 = -Xmax(1) * ScalaX + 3 * PicW / 5
      pY1 = -(cpY(Np) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
      pX2 = Xmax(1) * ScalaX + 3 * PicW / 5
      pY2 = -(cpY(1) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
      
      If (SiStampa = 1) Then
       'Nome
        stmp = "F1"
        Printer.CurrentX = pX4 - Printer.TextWidth(stmp) 'pX1
        Printer.CurrentY = pY4 - Printer.TextHeight(stmp)
        Printer.Print stmp
        stmp = "F2"
        Printer.CurrentX = PX3 + 0 * Printer.TextWidth(stmp) 'pX2
        Printer.CurrentY = pY3 - Printer.TextHeight(stmp)
        Printer.Print stmp
        'Spessore mola: valore
        stmp = "W" '& frmt(Specs(6), 3) & "mm"
        Printer.CurrentX = 3 * PicW / 5 + Printer.TextWidth(stmp)
        Printer.CurrentY = pY3 - Printer.TextHeight(stmp)
        Printer.Print stmp
        'MOLAV: Altezza dente minima: valore

        stmp = "Hmn" '& frmt(Specs(9), 3) & "mm" 'Specs(7) + Specs(8)
        Printer.CurrentX = 3 * PicW / 5 - Printer.TextWidth(stmp)
        Printer.CurrentY = pY3 + Printer.TextHeight(stmp)
        Printer.Print stmp
        
        'Larghezza mola: linea
        Printer.DrawStyle = 2
        Printer.Line (PX3, pY3)-(pX4, pY4)
        'Altezza mola: linea
        Printer.DrawStyle = 4
        Printer.Line (3 * PicW / 5, pY1)-(3 * PicW / 5, pY2)
        Printer.DrawStyle = 0
        
        ' Dati worm
        stmp = "Wheel meshing param: "
        Printer.CurrentX = 1 + 0 * 3 * PicW / 5 - 0 * Printer.TextWidth(stmp)
        Printer.CurrentY = 0 * pY3 + Printer.TextHeight(stmp)
        Printer.Print stmp
        ' Modulo normale di rotolamento: valore
        stmp = "Mn  [mm] = " & frmt(Specs(4), 3)
        Printer.CurrentX = 1
        Printer.CurrentY = 0 * pY3 + 2 * Printer.TextHeight(stmp)
        Printer.Print stmp
        
        
        ' Angolo di pressione normale
        stmp = "02) apn [deg]= " & frmt(Specs(5), 3)
        Printer.CurrentX = 1 + 0 * 3 * PicW / 5 - 0 * Printer.TextWidth(stmp)
        Printer.CurrentY = 0 * pY3 + 3 * Printer.TextHeight(stmp)
        Printer.Print stmp
        
        ' Spessore
        stmp = "W   [mm] = " & frmt(Specs(6), 3)
        Printer.CurrentX = 1 + 0 * 3 * PicW / 5 - 0 * Printer.TextWidth(stmp)
        Printer.CurrentY = 0 * pY3 + 4 * Printer.TextHeight(stmp)
        Printer.Print stmp
        ' Dedendum
        stmp = "DE  [mm] = " & frmt(Specs(7), 3)
        Printer.CurrentX = 1 + 0 * 3 * PicW / 5 - 0 * Printer.TextWidth(stmp)
        Printer.CurrentY = 0 * pY3 + 5 * Printer.TextHeight(stmp)
        Printer.Print stmp
        ' addendum
        stmp = "AD  [mm] = " & frmt(Specs(8), 3)
        Printer.CurrentX = 1 + 0 * 3 * PicW / 5 - 0 * Printer.TextWidth(stmp)
        Printer.CurrentY = 0 * pY3 + 6 * Printer.TextHeight(stmp)
        Printer.Print stmp
        ' altezza dente minima
        If (HRULLOREF < Specs(9)) Then
        OEMX.OEM1PicCALCOLO.ForeColor = vbRed
        Else
        OEMX.OEM1PicCALCOLO.ForeColor = vbBlack
        End If
        stmp = "Hmn [mm] = " & frmt(Specs(9), 3)
        Printer.CurrentX = 1 + 0 * 3 * PicW / 5 - 0 * Printer.TextWidth(stmp): Printer.CurrentY = 0 * pY3 + 7 * Printer.TextHeight(stmp): Printer.Print stmp
      Else
        'Nome
        stmp = "F1"
        OEMX.OEM1PicCALCOLO.CurrentX = pX4 - OEMX.OEM1PicCALCOLO.TextWidth(stmp) 'pX1
        OEMX.OEM1PicCALCOLO.CurrentY = pY4 - OEMX.OEM1PicCALCOLO.TextHeight(stmp)
        OEMX.OEM1PicCALCOLO.Print stmp
        stmp = "F2"
        OEMX.OEM1PicCALCOLO.CurrentX = PX3 + 0 * OEMX.OEM1PicCALCOLO.TextWidth(stmp) 'pX2
        OEMX.OEM1PicCALCOLO.CurrentY = pY3 - OEMX.OEM1PicCALCOLO.TextHeight(stmp)
        OEMX.OEM1PicCALCOLO.Print stmp
        'Spessore mola: valore
        stmp = "W" '& frmt(Specs(6), 3) & "mm"
        OEMX.OEM1PicCALCOLO.CurrentX = PXY(3, 1) + OEMX.OEM1PicCALCOLO.TextWidth(stmp) + 0 * 3 * PicW / 5
        OEMX.OEM1PicCALCOLO.CurrentY = pY3 - OEMX.OEM1PicCALCOLO.TextHeight(stmp)
    ''    OEMX.OEM1PicCALCOLO.Print stmp
        OEMX.OEM1PicCALCOLO.CurrentX = PXY(4, 1) - OEMX.OEM1PicCALCOLO.TextWidth(stmp) + 0 * 3 * PicW / 5
        OEMX.OEM1PicCALCOLO.CurrentY = pY3 - OEMX.OEM1PicCALCOLO.TextHeight(stmp)
    ''    OEMX.OEM1PicCALCOLO.Print stmp
        'MOLAV: Altezza dente minima: valore
        stmp = "Hmn" '& frmt(Specs(9), 3) & "mm" 'Specs(7) + Specs(8)
        OEMX.OEM1PicCALCOLO.CurrentX = 3 * PicW / 5 - OEMX.OEM1PicCALCOLO.TextWidth(stmp)
        OEMX.OEM1PicCALCOLO.CurrentY = pY3 + OEMX.OEM1PicCALCOLO.TextHeight(stmp)
        OEMX.OEM1PicCALCOLO.Print stmp
        
        'Larghezza mola: linea
        OEMX.OEM1PicCALCOLO.DrawStyle = 2
        OEMX.OEM1PicCALCOLO.Line (PX3, pY3)-(pX4, pY4)
        'Altezza mola: linea
        OEMX.OEM1PicCALCOLO.DrawStyle = 4
        OEMX.OEM1PicCALCOLO.Line (3 * PicW / 5, pY1)-(3 * PicW / 5, pY2)
        OEMX.OEM1PicCALCOLO.DrawStyle = 0
        'Larghezza TIP: linea
        OEMX.OEM1PicCALCOLO.DrawStyle = 4
        OEMX.OEM1PicCALCOLO.Line (pX5, pY5)-(pX6, pY6)
        OEMX.OEM1PicCALCOLO.DrawStyle = 0
        ' Dati worm
        stmp = "Wheel meshing param: "
        OEMX.OEM1PicCALCOLO.CurrentX = 1 + 0 * 3 * PicW / 5 - 0 * OEMX.OEM1PicCALCOLO.TextWidth(stmp)
        OEMX.OEM1PicCALCOLO.CurrentY = 0 * pY3 + OEMX.OEM1PicCALCOLO.TextHeight(stmp)
        OEMX.OEM1PicCALCOLO.Print stmp
        ' Modulo normale di rotolamento: valore
        stmp = "Mn  [mm] = " & frmt(Specs(4), 4)
        OEMX.OEM1PicCALCOLO.CurrentX = 1
        OEMX.OEM1PicCALCOLO.CurrentY = 0 * pY3 + 2 * OEMX.OEM1PicCALCOLO.TextHeight(stmp)
        OEMX.OEM1PicCALCOLO.Print stmp
        
        
        ' Angolo di pressione normale
        stmp = "apn [deg]= " & frmt(Specs(5), 4)
        OEMX.OEM1PicCALCOLO.CurrentX = 1 + 0 * 3 * PicW / 5 - 0 * OEMX.OEM1PicCALCOLO.TextWidth(stmp)
        OEMX.OEM1PicCALCOLO.CurrentY = 0 * pY3 + 3 * OEMX.OEM1PicCALCOLO.TextHeight(stmp)
        OEMX.OEM1PicCALCOLO.Print stmp
        
        ' Spessore
        stmp = "W   [mm] = " & frmt(Specs(6), 3)
        OEMX.OEM1PicCALCOLO.CurrentX = 1 + 0 * 3 * PicW / 5 - 0 * OEMX.OEM1PicCALCOLO.TextWidth(stmp)
        OEMX.OEM1PicCALCOLO.CurrentY = 0 * pY3 + 4 * OEMX.OEM1PicCALCOLO.TextHeight(stmp)
        OEMX.OEM1PicCALCOLO.Print stmp
        ' Dedendum
        stmp = "DE  [mm] = " & frmt(Specs(7), 3)
        OEMX.OEM1PicCALCOLO.CurrentX = 1 + 0 * 3 * PicW / 5 - 0 * OEMX.OEM1PicCALCOLO.TextWidth(stmp)
        OEMX.OEM1PicCALCOLO.CurrentY = 0 * pY3 + 5 * OEMX.OEM1PicCALCOLO.TextHeight(stmp)
        OEMX.OEM1PicCALCOLO.Print stmp
        ' addendum
        stmp = "AD  [mm] = " & frmt(Specs(8), 3)
        OEMX.OEM1PicCALCOLO.CurrentX = 1 + 0 * 3 * PicW / 5 - 0 * OEMX.OEM1PicCALCOLO.TextWidth(stmp)
        OEMX.OEM1PicCALCOLO.CurrentY = 0 * pY3 + 6 * OEMX.OEM1PicCALCOLO.TextHeight(stmp)
        OEMX.OEM1PicCALCOLO.Print stmp
        ' altezza dente minima
        If (HRULLOREF < Specs(9)) Then
        OEMX.OEM1PicCALCOLO.ForeColor = vbRed
        Else
        OEMX.OEM1PicCALCOLO.ForeColor = vbBlack
        End If
        stmp = "Hmn [mm] = " & frmt(Specs(9), 3)
        OEMX.OEM1PicCALCOLO.CurrentX = 1 + 0 * 3 * PicW / 5 - 0 * OEMX.OEM1PicCALCOLO.TextWidth(stmp): OEMX.OEM1PicCALCOLO.CurrentY = 0 * pY3 + 7 * OEMX.OEM1PicCALCOLO.TextHeight(stmp): OEMX.OEM1PicCALCOLO.Print stmp
        
      End If
      
      Return
      
' Gear plot
RAPPRESENTAPAGINA2:

    TipRelifDiamRef = LEP("TIPRELIEF_DIAMETER") 'LEP("DEAP_G") 'TipRelifDiamRef = LEP("TIPRELIEF_DIAMETER")
    TipRelifAmplRef = LEP("TIPRELIEF_AMPLITUDE")
        Dim Dvect(1 To 6) As Double
        ' Dint, Dsap, Drol, Dtip, Deap
        Dvect(1) = DIint: Dvect(2) = D1: Dvect(3) = Specs(13): Dvect(4) = Specs(14): Dvect(5) = D2: Dvect(6) = TipRelifDiamRef
        
        Dim DXX, DYX As Double
              'ScalaX = Val(GetInfo("CALCOLO", "ScalaMOLA", Path_LAVORAZIONE_INI))
      ScalaX = (PicH / (Ymax(0) - Ymin(0))) * fscala
      ScalaY = ScalaX
      OEMX.txtINP(0) = ScalaX
'ScalaFha Xmax
 If (Inspection) Then
  ' RETINATURA
  DXX = (Xmax(0) - Xmin(0)) / (Sampling - 1)
  DYX = (Ymax(0) - Ymin(0)) / (Sampling - 1)
  For i = 1 To Sampling - 1
   OEMX.OEM1PicCALCOLO.ForeColor = RGB(142, 142, 142): OEMX.OEM1PicCALCOLO.DrawStyle = 4: OEMX.OEM1PicCALCOLO.DrawWidth = 1
   pX1 = Xmin(0) * ScalaX + 3 * PicW / 5: pY1 = (Ymin(0) + DYX * (i - 1) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
   pX2 = Xmax(0) * ScalaX + 3 * PicW / 5: pY2 = (Ymin(0) + DYX * (i - 1) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
   GoSub GRAFICO_MOLA_o_INGRANAGGIO_FIANCO
   For j = 1 To SubSampling - 1
    OEMX.OEM1PicCALCOLO.ForeColor = RGB(228, 228, 228): OEMX.OEM1PicCALCOLO.DrawStyle = 2: OEMX.OEM1PicCALCOLO.DrawWidth = 1
    pX1 = Xmin(0) * ScalaX + 3 * PicW / 5
    pY1 = (Ymin(0) + DYX * (i - 1 + j / SubSampling) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
    pX2 = Xmax(0) * ScalaX + 3 * PicW / 5
    pY2 = (Ymin(0) + DYX * (i - 1 + j / SubSampling) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
    GoSub GRAFICO_MOLA_o_INGRANAGGIO_FIANCO
   Next j
  Next i
  For i = 1 To Sampling - 1
   OEMX.OEM1PicCALCOLO.ForeColor = RGB(142, 142, 142): OEMX.OEM1PicCALCOLO.DrawStyle = 4: OEMX.OEM1PicCALCOLO.DrawWidth = 1
   pX1 = (Xmin(0) + DXX * (i - 1)) * ScalaX + 3 * PicW / 5: pY1 = (Ymin(0) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
   pX2 = (Xmin(0) + DXX * (i - 1)) * ScalaX + 3 * PicW / 5: pY2 = (Ymax(0) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
   GoSub GRAFICO_MOLA_o_INGRANAGGIO_FIANCO
   For j = 1 To SubSampling - 1
    OEMX.OEM1PicCALCOLO.ForeColor = RGB(228, 228, 228): OEMX.OEM1PicCALCOLO.DrawStyle = 2: OEMX.OEM1PicCALCOLO.DrawWidth = 1
    pX1 = (Xmin(0) + DXX * (i - 1 + j / SubSampling)) * ScalaX + 3 * PicW / 5: pY1 = (Ymin(0) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
    pX2 = (Xmin(0) + DXX * (i - 1 + j / SubSampling)) * ScalaX + 3 * PicW / 5: pY2 = (Ymax(0) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
    GoSub GRAFICO_MOLA_o_INGRANAGGIO_FIANCO
   Next j
  Next i
' tacche collaudo misure verticali
  OEMX.OEM1PicCALCOLO.ForeColor = RGB(0, 0, 0): OEMX.OEM1PicCALCOLO.DrawStyle = 1: OEMX.OEM1PicCALCOLO.DrawWidth = 2
  pX1 = (0) * ScalaX + 3 * PicW / 5: pY1 = (Ymin(0) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
  pX2 = (0) * ScalaX + 3 * PicW / 5: pY2 = (Ymax(0) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
  GoSub GRAFICO_MOLA_o_INGRANAGGIO_FIANCO
  For i = 1 To 5
   OEMX.OEM1PicCALCOLO.ForeColor = RGB(15, 17, 31): OEMX.OEM1PicCALCOLO.DrawStyle = 1: OEMX.OEM1PicCALCOLO.DrawWidth = 2
   pX1 = (-0.3): pY1 = Dvect(i) / 2 - (Specs(13) / 2 + Specs(7))
   pX2 = -pX1: pY2 = pY1
    
        pX1 = pX1 * ScalaX + 3 * PicW / 5
        pY1 = -(pY1 - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2 '- (Ymax(0) + Ymin(0)) / 2
        pX2 = pX2 * ScalaX + 3 * PicW / 5
        pY2 = -(pY2 - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2 '- (Ymax(0) + Ymin(0)) / 2
        
        GoSub GRAFICO_MOLA_o_INGRANAGGIO_FIANCO
        OEMX.OEM1PicCALCOLO.ForeColor = vbBlack
        stmp1 = frmt(Dvect(i), 2)
        OEMX.OEM1PicCALCOLO.CurrentX = pX2 + 0 * OEMX.OEM1PicCALCOLO.TextWidth(stmp1)
        OEMX.OEM1PicCALCOLO.CurrentY = pY2 - 0.5 * OEMX.OEM1PicCALCOLO.TextHeight(stmp1)
        OEMX.OEM1PicCALCOLO.Print stmp1
        
        Next i
        ' FhaToll
        
        End If ' END INSPECTION SECTION
        
        Dim Samp As Integer
        
        If (Inspection) Then
            If ((Rast$ = "O") And (DrastOtt < DatTesta)) Then
                Samp = 2 * Int(Np / 3)
            Else
                Samp = Int(Np / 2) - 1
            End If
        Else
         Samp = Np - 1
        End If ' INSPECTION

If (Rappresentazione = "INGRANAGGIO") Then Call FillGear(cpXden, ScalaX, PicW, cpYden, Ymax(0), Ymin(0), ScalaY, PicH, Samp, DIint, (Specs(13) / 2 + Specs(7)), PG / Z)
Call Disegna_Profilo_Vettoriale(cpXden, ScalaX, PicW, cpYden, Ymax(0), Ymin(0), ScalaY, PicH, Samp, vbBlack, 1)
Call Disegna_Profilo_Vettoriale(cpX, ScalaX, PicW, cpY, Ymax(0), Ymin(0), ScalaY, PicH, Samp, vbBlue, 2)
'' plot FhaToll region -------------------------------
 If ((TipRelifDiamRef < DatTesta And TipRelifAmplRef > 0)) Then
  Samp = 2 * Int(Np / 3) '+ 1
 Else
  Samp = Int(Np / 2) '- 1
 End If
     '       Samp = Np - 1
     'FhaToll region: Fianco 1: Rigth Flank : TOLL DX
  OEMX.OEM1PicCALCOLO.ForeColor = RGB(4, 4, 4): OEMX.OEM1PicCALCOLO.DrawStyle = 0: OEMX.OEM1PicCALCOLO.DrawWidth = 1
  If (PlotTollerance) Then
      
      Dim XYStarDX(1 To 2)
      Dim XYStarSX(1 To 2)
      Call Get_interceptionindex(cpXtol1, cpYtol1, cpX, cpY, XYStarDX, Np, 1)
      For i = 1 To Samp
'      Call set_colour(cpXtol1(i + 1), cpX(i), Inspection, 1)
      If (Inspection) Then
      If (cpYtol1(i) > XYStarDX(2)) Then
      OEMX.OEM1PicCALCOLO.ForeColor = RGB(255, 0, 0)
      Else
      OEMX.OEM1PicCALCOLO.ForeColor = RGB(60, 255, 0)
      End If
      Else
      OEMX.OEM1PicCALCOLO.ForeColor = RGB(4, 4, 4)
      End If

        pX1 = -cpXtol1(i) * ScalaX + 3 * PicW / 5: pY1 = -(cpYtol1(i) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
        pX2 = -cpXtol1(i + 1) * ScalaX + 3 * PicW / 5: pY2 = -(cpYtol1(i + 1) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
        GoSub GRAFICO_MOLA_o_INGRANAGGIO_FIANCO
      Next i
      'FhaToll region: F2: Rigth Flank : TOLL SX
      Call Get_interceptionindex(cpXtol2, cpYtol1, cpX, cpY, XYStarSX, Np, 2)
      For i = 1 To Samp
      If (Inspection) Then
     If (cpYtol2(i) > XYStarSX(2)) Then
      OEMX.OEM1PicCALCOLO.ForeColor = RGB(255, 0, 0)
      Else
      OEMX.OEM1PicCALCOLO.ForeColor = RGB(60, 255, 0)
      End If
            Else
      OEMX.OEM1PicCALCOLO.ForeColor = RGB(4, 4, 4)
      End If

        pX1 = -cpXtol2(i) * ScalaX + 3 * PicW / 5: pY1 = -(cpYtol2(i) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
        pX2 = -cpXtol2(i + 1) * ScalaX + 3 * PicW / 5: pY2 = -(cpYtol2(i + 1) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
        GoSub GRAFICO_MOLA_o_INGRANAGGIO_FIANCO
      Next i
      'FhaToll region: F1
      For i = 1 To Samp
'      Call set_colour(cpXtol1(i + 1), cpX(i), Inspection, 1)
      If (Inspection) Then
      If (cpYtol1(i) > XYStarDX(2)) Then
      OEMX.OEM1PicCALCOLO.ForeColor = RGB(255, 0, 0)
      Else
      OEMX.OEM1PicCALCOLO.ForeColor = RGB(60, 255, 0)
      End If
            Else
      OEMX.OEM1PicCALCOLO.ForeColor = RGB(4, 4, 4)
      End If
        pX1 = cpXtol1(i) * ScalaX + 3 * PicW / 5: pY1 = -(cpYtol1(i) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
        pX2 = cpXtol1(i + 1) * ScalaX + 3 * PicW / 5: pY2 = -(cpYtol1(i + 1) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
        GoSub GRAFICO_MOLA_o_INGRANAGGIO_FIANCO
      Next i
      For i = 1 To Samp
      If (Inspection) Then
      If (cpYtol2(i) > XYStarSX(2)) Then
      OEMX.OEM1PicCALCOLO.ForeColor = RGB(255, 0, 0)
      Else
      OEMX.OEM1PicCALCOLO.ForeColor = RGB(60, 255, 0)
      End If
            Else
      OEMX.OEM1PicCALCOLO.ForeColor = RGB(4, 4, 4)
      End If
        pX1 = cpXtol2(i) * ScalaX + 3 * PicW / 5: pY1 = -(cpYtol2(i) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
        pX2 = cpXtol2(i + 1) * ScalaX + 3 * PicW / 5: pY2 = -(cpYtol2(i + 1) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
        GoSub GRAFICO_MOLA_o_INGRANAGGIO_FIANCO
      Next i
      End If
      ' Call FillParamMV(ApMedRulD_IN, SpRul_IN_IN, RfiancoRul_IN, Drul_IN, RmedRul_IN, YrastRul_IN, ArastRulD_IN, RtRul_IN, ApRast_IN, D1, Beta * 180 / PG, ALFAn * 180 / PG, AM, Z, W, N, D2, DIint, DatTesta_IN)
        If (Inspection) Then
        Samp = 1
        Else
        Samp = Np - 1
        End If ' INSPECTION
        
        ' Dint, Dsap, Drol, Dtip, Deap
        ' Dvect(1) = DIint: Dvect(2) = D1: Dvect(3) = Specs(14): Dvect(4) = Specs(13): Dvect(5) = D2
        
        ' Circonference
        Dim TH2(1 To 2) As Double
        
        
    '    Dim Tollerance As Double

        ' Dint, Dsap, Drol, Dtip, Deap
        Dim plotc As Boolean
        plotc = True
        
        For i = 1 To 6
        Select Case (i)
        Case (1)
      ' D1 Circonferenza di piede
        OEMX.OEM1PicCALCOLO.ForeColor = vbBlue: OEMX.OEM1PicCALCOLO.DrawStyle = 0: OEMX.OEM1PicCALCOLO.DrawWidth = 2: plotc = True
        Case (2)
        OEMX.OEM1PicCALCOLO.ForeColor = vbGreen: OEMX.OEM1PicCALCOLO.DrawStyle = 2: OEMX.OEM1PicCALCOLO.DrawWidth = 1: plotc = True
        Case (4)
         If (Specs(14) < TipRelifDiamRef + FhaDToll And Specs(14) > TipRelifDiamRef - FhaDToll) Then
          OEMX.OEM1PicCALCOLO.ForeColor = vbGreen
         Else
          OEMX.OEM1PicCALCOLO.ForeColor = vbRed
         End If
         If (Specs(14) >= D2) Then
            plotc = False
         End If
        Case (6) ' D tip required
        OEMX.OEM1PicCALCOLO.ForeColor = vbBlack: OEMX.OEM1PicCALCOLO.DrawStyle = 2: OEMX.OEM1PicCALCOLO.DrawWidth = 1: plotc = True
        Case Else
        OEMX.OEM1PicCALCOLO.ForeColor = vbBlack: OEMX.OEM1PicCALCOLO.DrawStyle = 0: OEMX.OEM1PicCALCOLO.DrawWidth = 1: plotc = True
        End Select
        If (plotc) Then
        For j = 1 To Samp
            TH2(1) = (-PG / 2 + PG / Z) + (-2 * PG / Z) * (j - 1) / Samp
            TH2(2) = (-PG / 2 + PG / Z) + (-2 * PG / Z) * (j) / Samp
        If (Inspection) Then
            pX1 = (PG / Z) * (Dvect(i) / 2): pY1 = Dvect(i) / 2 - (Specs(13) / 2 + Specs(7))
            pX2 = -pX1: pY2 = pY1
        Else
            Call PntRayon(Dvect(i) / 2, 0, -(Specs(13) / 2 + Specs(7)), 0, TH2(1), pX1, pY1)
            Call PntRayon(Dvect(i) / 2, 0, -(Specs(13) / 2 + Specs(7)), 0, TH2(2), pX2, pY2)
        End If ' INSPECTION
    
        pX1 = pX1 * ScalaX + 3 * PicW / 5
        pY1 = -(pY1 - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2 '- (Ymax(0) + Ymin(0)) / 2
        pX2 = pX2 * ScalaX + 3 * PicW / 5
        pY2 = -(pY2 - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2 '- (Ymax(0) + Ymin(0)) / 2

            GoSub GRAFICO_MOLA_o_INGRANAGGIO_FIANCO
        Next j
        End If
        plotc = True
        Next i
    
       OEMX.OEM1PicCALCOLO.ForeColor = vbBlack: OEMX.OEM1PicCALCOLO.DrawWidth = 1: OEMX.OEM1PicCALCOLO.DrawStyle = 1

      PX3 = -Specs(11) * ScalaX + 3 * PicW / 5
      pY3 = -(Specs(12) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2   '- (Ymax(0) + Ymin(0)/ 2
      pX4 = Specs(11) * ScalaX + 3 * PicW / 5
      pY4 = -(Specs(12) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2  '- (Ymax(0) + Ymin(0)) / 2

'linee TIP
'      pX5 = -Specs(16) * ScalaX + 3 * PicW / 5: pY5 = -(Specs(17) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
'      pX6 = Specs(16) * ScalaX + 3 * PicW / 5: pY6 = -(Specs(17) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
'      pX5 = PX3: pX6 = pX4

' Posizione Scritte
      pX1 = -(Xmax(0) / 2) * ScalaX + 3 * PicW / 5
      pY1 = -((Ymax(0) - Ymin(0)) / 2) * ScalaY + PicH / 2
      pX2 = (Xmax(0) / 2) * ScalaX + 3 * PicW / 5
'      pY2 = ((Ymax(0) - Ymin(0)) / 2) * ScalaY + PicH / 2
      pY2 = pY1

      
      If (SiStampa = 1) Then
        'Nome
        stmp = "F1"
        Printer.CurrentX = pX1 - Printer.TextWidth(stmp)
        Printer.CurrentY = pY2 - Printer.TextHeight(stmp)
        Printer.Print stmp
        stmp = "F2"
        Printer.CurrentX = pX2
        Printer.CurrentY = pY2 - Printer.TextHeight(stmp)
        Printer.Print stmp
        
        ' Dati Ruota
        stmp = "Gear meshing param: "
        Printer.CurrentX = 1 + 0 * 3 * PicW / 5 - 0 * Printer.TextWidth(stmp)
        Printer.CurrentY = 0 * pY3 + 8 * Printer.TextHeight(stmp)
        Printer.Print stmp
        ' Bombatura ottenuta: valore
        stmp = "07) Ca  [mm] = " & frmt(Specs(3), 5)
        Printer.CurrentX = 1
        Printer.CurrentY = 0 * pY3 + 9 * Printer.TextHeight(stmp)
        Printer.Print stmp
      Else
        'Nome
        If (Inspection) Then
        stmp = "F2 left flank": stmp1 = "F1 right flank"
        Else
        stmp = "F1": stmp1 = "F2"
        End If
        OEMX.OEM1PicCALCOLO.CurrentX = pX1 - OEMX.OEM1PicCALCOLO.TextWidth(stmp) / 2
        OEMX.OEM1PicCALCOLO.CurrentY = pY2 - OEMX.OEM1PicCALCOLO.TextHeight(stmp)
        OEMX.OEM1PicCALCOLO.Print stmp
        ' stmp1 = "right flank" '"F2"
        OEMX.OEM1PicCALCOLO.CurrentX = pX2 - OEMX.OEM1PicCALCOLO.TextWidth(stmp1) / 2
        OEMX.OEM1PicCALCOLO.CurrentY = pY2 - OEMX.OEM1PicCALCOLO.TextHeight(stmp1)
        OEMX.OEM1PicCALCOLO.Print stmp1
        
        ' Dati Ruota
        stmp = "Gear meshing param: "
        OEMX.OEM1PicCALCOLO.CurrentX = 1 + 0 * 3 * PicW / 5 - 0 * OEMX.OEM1PicCALCOLO.TextWidth(stmp)
        OEMX.OEM1PicCALCOLO.CurrentY = 0 * pY3 + 8 * OEMX.OEM1PicCALCOLO.TextHeight(stmp)
        OEMX.OEM1PicCALCOLO.Print stmp
        ' Bombatura ottenuta: valore
        stmp = "Ca  [mm] = " & frmt(Specs(3), 5)
        OEMX.OEM1PicCALCOLO.CurrentX = 1
        OEMX.OEM1PicCALCOLO.CurrentY = 0 * pY3 + 9 * OEMX.OEM1PicCALCOLO.TextHeight(stmp)
        OEMX.OEM1PicCALCOLO.Print stmp
        
        ' Diametro SAP
        OEMX.OEM1PicCALCOLO.ForeColor = vbGreen
        OEMX.OEM1PicCALCOLO.DrawStyle = 1
        OEMX.OEM1PicCALCOLO.DrawWidth = 1
        
        stmp = "Dsap[mm] = " '& frmt(D1, 5)
        OEMX.OEM1PicCALCOLO.CurrentX = 1
        OEMX.OEM1PicCALCOLO.CurrentY = 0 * pY3 + 10 * OEMX.OEM1PicCALCOLO.TextHeight(stmp)
        OEMX.OEM1PicCALCOLO.Print stmp
        
        
        OEMX.OEM1PicCALCOLO.ForeColor = vbBlack
        stmp1 = frmt(D1, 5)
        OEMX.OEM1PicCALCOLO.CurrentX = 1 + OEMX.OEM1PicCALCOLO.TextWidth(stmp)
        OEMX.OEM1PicCALCOLO.CurrentY = 0 * pY3 + 10 * OEMX.OEM1PicCALCOLO.TextHeight(stmp1)
        OEMX.OEM1PicCALCOLO.Print stmp1
        
        OEMX.OEM1PicCALCOLO.ForeColor = vbBlue
        ' Diametro interno
        stmp = "Dint[mm] = " ' & frmt(DIint, 5)
        OEMX.OEM1PicCALCOLO.CurrentX = 1
        OEMX.OEM1PicCALCOLO.CurrentY = 0 * pY3 + 11 * OEMX.OEM1PicCALCOLO.TextHeight(stmp)
        OEMX.OEM1PicCALCOLO.Print stmp
        
        OEMX.OEM1PicCALCOLO.ForeColor = vbBlack
        stmp1 = frmt(DIint, 5)
        OEMX.OEM1PicCALCOLO.CurrentX = 1 + OEMX.OEM1PicCALCOLO.TextWidth(stmp)
        OEMX.OEM1PicCALCOLO.CurrentY = 0 * pY3 + 11 * OEMX.OEM1PicCALCOLO.TextHeight(stmp1)
        OEMX.OEM1PicCALCOLO.Print stmp1
        
        OEMX.OEM1PicCALCOLO.ForeColor = vbBlack
        ' Diametro della rastremazione di testa ottenuto
        If ((Specs(14) < TipRelifDiamRef + FhaDToll And Specs(14) > TipRelifDiamRef - FhaDToll) Or (Specs(14) >= D2)) Then
         OEMX.OEM1PicCALCOLO.ForeColor = vbGreen
        Else
         OEMX.OEM1PicCALCOLO.ForeColor = vbRed
        End If
        stmp = "Dtip[mm] = " & frmt(Specs(14), 4)
        OEMX.OEM1PicCALCOLO.CurrentX = 1 '+ OEMX.OEM1PicCALCOLO.TextWidth(stmp)
        OEMX.OEM1PicCALCOLO.CurrentY = 0 * pY3 + 12 * OEMX.OEM1PicCALCOLO.TextHeight(stmp)
        OEMX.OEM1PicCALCOLO.Print stmp
        OEMX.OEM1PicCALCOLO.ForeColor = vbBlack
        ' Entit� della rastremazione di testa ottenuta
        stmp = "Ltip[mm] = " & frmt(Specs(15), 4)
        OEMX.OEM1PicCALCOLO.CurrentX = 1 '+ OEMX.OEM1PicCALCOLO.TextWidth(stmp)
        OEMX.OEM1PicCALCOLO.CurrentY = 0 * pY3 + 13 * OEMX.OEM1PicCALCOLO.TextHeight(stmp)
        OEMX.OEM1PicCALCOLO.Print stmp
      End If
      
      Return
      
RAPPRESENTAPAGINA3:
    ' Circonferenza inviluppata da mola
        ' minimo raggio inviluppato
    If (Warn(1)) Then
        OEMX.OEM1PicCALCOLO.ForeColor = vbRed: OEMX.OEM1PicCALCOLO.DrawWidth = 2
    Else
        OEMX.OEM1PicCALCOLO.ForeColor = vbMagenta: OEMX.OEM1PicCALCOLO.DrawWidth = 1
    End If
                
        Dim TH(1 To 2) As Double
        For i = 1 To Np - 1
            TH(1) = (-PG / 2 + PG / Z) + (-2 * PG / Z) * (i - 1) / (Np - 1)
            TH(2) = (-PG / 2 + PG / Z) + (-2 * PG / Z) * (i) / (Np - 1)
            Call PntRayon(RminEnvelopped, 0, -(Specs(13) / 2 + Specs(7)), 0, TH(1), pX1, pY1)
            Call PntRayon(RminEnvelopped, 0, -(Specs(13) / 2 + Specs(7)), 0, TH(2), pX2, pY2)
        
        pX1 = pX1 * ScalaX + 3 * PicW / 5
        pY1 = -(pY1 - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2 '- (Ymax(0) + Ymin(0)) / 2
        pX2 = pX2 * ScalaX + 3 * PicW / 5
        pY2 = -(pY2 - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2 '- (Ymax(0) + Ymin(0)) / 2
            
            GoSub GRAFICO_MOLA_o_INGRANAGGIO_FIANCO
        Next i

      OEMX.OEM1PicCALCOLO.ForeColor = vbBlack
      OEMX.OEM1PicCALCOLO.DrawWidth = 1


      If (SiStampa = 1) Then
        'Nome
        stmp = "F1"
        Printer.CurrentX = pX1 - Printer.TextWidth(stmp)
        Printer.CurrentY = pY2 - Printer.TextHeight(stmp)
        Printer.Print stmp
        stmp = "F2"
        Printer.CurrentX = pX2
        Printer.CurrentY = pY2 - Printer.TextHeight(stmp)
        Printer.Print stmp
        
        ' Dati Ruota
        stmp = "Grinding param: "
        Printer.CurrentX = 1 + 0 * 3 * PicW / 5 - 0 * Printer.TextWidth(stmp)
        Printer.CurrentY = 0 * pY3 + 10 * Printer.TextHeight(stmp)
        Printer.Print stmp
        ' Gioco di testa: valore
        stmp = "Gt  [mm] = " & frmt(Specs(1), 4)
        Printer.CurrentX = 1
        Printer.CurrentY = 0 * pY3 + 11 * Printer.TextHeight(stmp)
        Printer.Print stmp
        ' Spostamento rullo: valore
        stmp = "Spr [mm] = " & frmt(Specs(2), 4)
        Printer.CurrentX = 1
        Printer.CurrentY = 0 * pY3 + 12 * Printer.TextHeight(stmp)
        Printer.Print stmp
        ' Raggio Minimo inviluppato da mola: valore
        stmp = "Rmi [mm] = " & frmt(Specs(10), 4)
        Printer.CurrentX = 1
        Printer.CurrentY = 0 * pY3 + 13 * Printer.TextHeight(stmp)
        Printer.Print stmp
        
        ' Diametro di rotolamento mola: valore
        stmp = "Drm [mm] = " & frmt(Specs(13), 4)
        Printer.CurrentX = 1
        Printer.CurrentY = 0 * pY3 + 14 * Printer.TextHeight(stmp)
        Printer.Print stmp
      Else
        'Nome
        'stmp = "F1"
        'OEMX.OEM1PicCALCOLO.CurrentX = pX1 - OEMX.OEM1PicCALCOLO.TextWidth(stmp)
        'OEMX.OEM1PicCALCOLO.CurrentY = pY2 - OEMX.OEM1PicCALCOLO.TextHeight(stmp)
        'OEMX.OEM1PicCALCOLO.Print stmp
        'stmp = "F2"
        'OEMX.OEM1PicCALCOLO.CurrentX = pX2
        'OEMX.OEM1PicCALCOLO.CurrentY = pY2 - OEMX.OEM1PicCALCOLO.TextHeight(stmp)
        'OEMX.OEM1PicCALCOLO.Print stmp
        
        ' Dati Ruota
        stmp = "Grinding param: "
        OEMX.OEM1PicCALCOLO.CurrentX = 1 + 0 * 3 * PicW / 5 - 0 * OEMX.OEM1PicCALCOLO.TextWidth(stmp)
        OEMX.OEM1PicCALCOLO.CurrentY = 0 * pY3 + 14 * OEMX.OEM1PicCALCOLO.TextHeight(stmp)
        OEMX.OEM1PicCALCOLO.Print stmp
        ' Gioco di testa: valore
        stmp = "Gt  [mm] = " & frmt(Specs(1), 4)
        OEMX.OEM1PicCALCOLO.CurrentX = 1
        OEMX.OEM1PicCALCOLO.CurrentY = 0 * pY3 + 15 * OEMX.OEM1PicCALCOLO.TextHeight(stmp)
        OEMX.OEM1PicCALCOLO.Print stmp
        ' Spostamento rullo: valore
        stmp = "Spr [mm] = " & frmt(Specs(2), 4)
        OEMX.OEM1PicCALCOLO.CurrentX = 1
        OEMX.OEM1PicCALCOLO.CurrentY = 0 * pY3 + 16 * OEMX.OEM1PicCALCOLO.TextHeight(stmp)
        OEMX.OEM1PicCALCOLO.Print stmp
        ' Raggio Minimo inviluppato da mola: valore
    If (Warn(1)) Then
        OEMX.OEM1PicCALCOLO.ForeColor = vbRed
        OEMX.OEM1PicCALCOLO.DrawWidth = 2
    Else
        OEMX.OEM1PicCALCOLO.ForeColor = vbMagenta
        OEMX.OEM1PicCALCOLO.DrawWidth = 1
    End If
        stmp = "Dmi [mm] = " '& frmt(2 * Specs(10), 4)
        OEMX.OEM1PicCALCOLO.CurrentX = 1
        OEMX.OEM1PicCALCOLO.CurrentY = 0 * pY3 + 17 * OEMX.OEM1PicCALCOLO.TextHeight(stmp)
        OEMX.OEM1PicCALCOLO.Print stmp
        
        OEMX.OEM1PicCALCOLO.ForeColor = vbBlack
        stmp1 = frmt(2 * Specs(10), 4)
        OEMX.OEM1PicCALCOLO.CurrentX = 1 + OEMX.OEM1PicCALCOLO.TextWidth(stmp)
        OEMX.OEM1PicCALCOLO.CurrentY = 0 * pY3 + 17 * OEMX.OEM1PicCALCOLO.TextHeight(stmp1)
        OEMX.OEM1PicCALCOLO.Print stmp1

        ' Diametro di rotolamento mola: valore
        stmp = "Drm [mm] = " & frmt(Specs(13), 4)
        OEMX.OEM1PicCALCOLO.CurrentX = 1
        OEMX.OEM1PicCALCOLO.CurrentY = 0 * pY3 + 18 * OEMX.OEM1PicCALCOLO.TextHeight(stmp)
        OEMX.OEM1PicCALCOLO.Print stmp
        
        ' Riduzione Interasse
        stmp = "Ccd [mm] = " & frmt(Specs(18), 4)
        OEMX.OEM1PicCALCOLO.CurrentX = 1
        OEMX.OEM1PicCALCOLO.CurrentY = 0 * pY3 + 19 * OEMX.OEM1PicCALCOLO.TextHeight(stmp)
        OEMX.OEM1PicCALCOLO.Print stmp
      End If
      
      Return
      
RAPPRESENTAPAGINA4:
        
      
      If (SiStampa = 1) Then
         ' Dati worm
        stmp = "Inspection param: "
        Printer.CurrentX = 1 + 0 * 3 * PicW / 5 - 0 * OEMX.OEM1PicCALCOLO.TextWidth(stmp)
        Printer.CurrentY = OEMX.OEM1PicCALCOLO.TextHeight(stmp)
        Printer.Print stmp
        ' Modulo normale di rotolamento: valore
        stmp = "fHa_max  [mn] = " & frmt(Specs(4), 1)
        Printer.CurrentX = 1
        Printer.CurrentY = 2 * OEMX.OEM1PicCALCOLO.TextHeight(stmp)
        Printer.Print stmp
    
      Else
        
        ' Dati worm
        stmp = "Inspection param: "
        OEMX.OEM1PicCALCOLO.CurrentX = 1 + 0 * 3 * PicW / 5 - 0 * OEMX.OEM1PicCALCOLO.TextWidth(stmp)
        OEMX.OEM1PicCALCOLO.CurrentY = OEMX.OEM1PicCALCOLO.TextHeight(stmp)
        OEMX.OEM1PicCALCOLO.Print stmp
        ' Modulo normale di rotolamento: valore
        'stmp = "fHa_twst [mn] = ( " & TRUNC(CorrEvolv(1)) & " , " & TRUNC(CorrEvolv(2)) & " )"
        stmp = "Bias[mm] =(" & TRUNC(CorrEvolv(1)) & " , " & TRUNC(CorrEvolv(2)) & ")"
        OEMX.OEM1PicCALCOLO.CurrentX = 1
        OEMX.OEM1PicCALCOLO.CurrentY = 2 * OEMX.OEM1PicCALCOLO.TextHeight(stmp)
        OEMX.OEM1PicCALCOLO.Print stmp
' Grinding parameters
        stmp = "Grinding param: "
        OEMX.OEM1PicCALCOLO.CurrentX = 1 + 0 * 3 * PicW / 5 - 0 * OEMX.OEM1PicCALCOLO.TextWidth(stmp)
        OEMX.OEM1PicCALCOLO.CurrentY = 0 * pY3 + 14 * OEMX.OEM1PicCALCOLO.TextHeight(stmp)
        OEMX.OEM1PicCALCOLO.Print stmp
        ' Gioco di testa: valore
        stmp = "Gt  [mm] = " & frmt(Specs(1), 4)
        OEMX.OEM1PicCALCOLO.CurrentX = 1
        OEMX.OEM1PicCALCOLO.CurrentY = 0 * pY3 + 15 * OEMX.OEM1PicCALCOLO.TextHeight(stmp)
        OEMX.OEM1PicCALCOLO.Print stmp
        ' Spostamento rullo: valore
        stmp = "Spr [mm] = " & frmt(Specs(2), 4)
        OEMX.OEM1PicCALCOLO.CurrentX = 1
        OEMX.OEM1PicCALCOLO.CurrentY = 0 * pY3 + 16 * OEMX.OEM1PicCALCOLO.TextHeight(stmp)
        OEMX.OEM1PicCALCOLO.Print stmp
        ' Raggio Minimo inviluppato da mola: valore
    If (Warn(1)) Then
        OEMX.OEM1PicCALCOLO.ForeColor = vbRed
        OEMX.OEM1PicCALCOLO.DrawWidth = 2
    Else
        OEMX.OEM1PicCALCOLO.ForeColor = vbMagenta
        OEMX.OEM1PicCALCOLO.DrawWidth = 1
    End If
        stmp = "Dmi [mm] = " '& frmt(2 * Specs(10), 4)
        OEMX.OEM1PicCALCOLO.CurrentX = 1
        OEMX.OEM1PicCALCOLO.CurrentY = 0 * pY3 + 17 * OEMX.OEM1PicCALCOLO.TextHeight(stmp)
        OEMX.OEM1PicCALCOLO.Print stmp
        
        OEMX.OEM1PicCALCOLO.ForeColor = vbBlack
        stmp1 = frmt(2 * Specs(10), 4)
        OEMX.OEM1PicCALCOLO.CurrentX = 1 + OEMX.OEM1PicCALCOLO.TextWidth(stmp)
        OEMX.OEM1PicCALCOLO.CurrentY = 0 * pY3 + 17 * OEMX.OEM1PicCALCOLO.TextHeight(stmp1)
        OEMX.OEM1PicCALCOLO.Print stmp1

        ' Diametro di rotolamento mola: valore
        stmp = "Drm [mm] = " & frmt(Specs(13), 4)
        OEMX.OEM1PicCALCOLO.CurrentX = 1
        OEMX.OEM1PicCALCOLO.CurrentY = 0 * pY3 + 18 * OEMX.OEM1PicCALCOLO.TextHeight(stmp)
        OEMX.OEM1PicCALCOLO.Print stmp
        
        ' Riduzione Interasse
        stmp = "Ccd [mm] = " & frmt(Specs(18), 4)
        OEMX.OEM1PicCALCOLO.CurrentX = 1
        OEMX.OEM1PicCALCOLO.CurrentY = 0 * pY3 + 19 * OEMX.OEM1PicCALCOLO.TextHeight(stmp)
        OEMX.OEM1PicCALCOLO.Print stmp
        
      End If
      
      Return
      
End Sub
Private Sub Get_Context(ByRef Np, ByRef cpX(), ByRef cpY(), ByRef Xmax, ByRef Ymax, ByRef Xmin, ByRef Ymin)
Dim Fianco, i As Integer

      For Fianco = 1 To 2
        Xmax(Fianco) = cpX(1)
        Ymax(Fianco) = cpY(1)
        Xmin(Fianco) = cpX(1)
        Ymin(Fianco) = cpY(1)
        For i = 1 To Np - 1 '  + NPR1 + 1
          If cpX(i) * (-1) ^ Fianco >= Xmax(Fianco) Then Xmax(Fianco) = cpX(i) * (-1) ^ Fianco
          If cpY(i) >= Ymax(Fianco) Then Ymax(Fianco) = cpY(i)
          If cpX(i) * (-1) ^ Fianco <= Xmin(Fianco) Then Xmin(Fianco) = cpX(i) * (-1) ^ Fianco
          If cpY(i) <= Ymin(Fianco) Then Ymin(Fianco) = cpY(i)
        Next i
    Next Fianco
    Xmax(0) = (Xmax(1) + Xmax(2)) / 2
    Xmin(0) = (Xmin(1) + Xmin(2)) / 2
    Ymax(0) = (Ymax(1) + Ymax(2)) / 2
    Ymin(0) = (Ymin(1) + Ymin(2)) / 2
End Sub
Private Sub set_colour(cpXtol, cpX, Inspection, lato)
If (lato = 1) Then
If (Inspection) Then
      If (cpXtol > cpX) Then
      OEMX.OEM1PicCALCOLO.ForeColor = RGB(255, 0, 0)
      Else
      OEMX.OEM1PicCALCOLO.ForeColor = RGB(60, 255, 0)
      End If
Else
      If (cpXtol < cpX) Then
      OEMX.OEM1PicCALCOLO.ForeColor = RGB(4, 4, 4)
      Else
      OEMX.OEM1PicCALCOLO.ForeColor = RGB(4, 4, 4)
      End If
End If

Else
If (Inspection) Then
      If (cpXtol < cpX) Then
      OEMX.OEM1PicCALCOLO.ForeColor = RGB(255, 0, 0)
      Else
      OEMX.OEM1PicCALCOLO.ForeColor = RGB(60, 255, 0)
      End If
Else
      If (cpXtol > cpX) Then
      OEMX.OEM1PicCALCOLO.ForeColor = RGB(4, 4, 4)
      
      Else
      OEMX.OEM1PicCALCOLO.ForeColor = RGB(4, 4, 4)
      End If
End If
End If

End Sub
Private Sub Get_interceptionindex(ByRef cpXtol(), ByRef cpYtol(), ByRef cpX(), ByRef cpY(), ByRef XYStar(), Np, side)

Dim F(1 To 3) As Double
Dim Index(1 To 3) As Double
Dim i As Integer
Dim ni As Integer
Dim X(1 To 4) As Double
Dim Y(1 To 4) As Double
Dim M(1 To 2) As Double
'Dim XYStar(1 To 2) As Double

If (IsGearTipPresent Or IsToolTipPresent) Then
    X(1) = cpXtol(Np / 3): X(2) = cpXtol(2 * Np / 3)
    Y(1) = cpYtol(Np / 3): Y(2) = cpYtol(2 * Np / 3)
    X(3) = cpX(Np / 3): X(4) = cpX(2 * Np / 3)
    Y(3) = cpY(Np / 3): Y(4) = cpY(2 * Np / 3)
    M(1) = (Y(2) - Y(1)) / (X(2) - X(1))
    M(2) = (Y(4) - Y(3)) / (X(4) - X(3))
    
    XYStar(1) = (Y(3) - Y(1) - M(2) * X(3) + M(1) * X(1)) / (M(1) - M(2))
    XYStar(2) = Y(3) + M(2) * (XYStar(1) - X(3))
    If ((side = 1 And XYStar(2) < Y(3)) Or (side = 2 And XYStar(1) < X(1))) Then
    XYStar(1) = X(2)
    XYStar(2) = Y(2)
    End If
    
End If

'ni = np

'For index = 1 To 3
'    i = Round(index / 3 * ni, 0)
'    F(i) = cpXtol(i) - cpX(i)
'Next i

'If (F(1) * F(2) < 0) Then
'index(3) = index(2)
'index(2) = (index(1) + index(3)) / 2
'ElseIf (F(2) * F(3) < 0) Then
'index(1) = index(2)
'index(2) = (index(1) + index(3)) / 2
'Else
' no interception
'End If

'For i = 1 To np
'    F(i) = cpXtol(i) - cpX(i)

'Next i

End Sub
Function Get_Worm_Tilt(str As String)
Dim SENSO_FILETTO, MODULO_MOLA, PRINCIPI_MOLA, DIAMETRO_MOLA_EXT_EFF, DIAMETRO_MOLA_MED_EFF, ARGOMENTO, INCLINAZIONE_MOLA_RAD, INCLINAZIONE_MOLA_GRD As Double
 SENSO_FILETTO = Lp("SENSOFIL_G")          '
 MODULO_MOLA = Lp("MNUT_G")       'MnUtensileMV
 PRINCIPI_MOLA = Lp("NUMPRINC_G") 'NPrincipiMV
 DIAMETRO_MOLA_EXT_EFF = 2 * val(OPC_LEGGI_DATO("/ACC/NCK/GUD7/REXTMOLA_G"))
 DIAMETRO_MOLA_MED_EFF = DIAMETRO_MOLA_EXT_EFF - 2 * MODULO_MOLA '(ALTEZZA_DENTEMOLA_EFF + DIAMETRO_MOLA_INT_EFF)
'CALCOLO INCLINAZIONE FILETTO
 ARGOMENTO = MODULO_MOLA * PRINCIPI_MOLA / DIAMETRO_MOLA_MED_EFF   'OLD:  DIAMETRO_MOLA
  INCLINAZIONE_MOLA_RAD = Atn(ARGOMENTO / Sqr(1 - ARGOMENTO * ARGOMENTO))  'RADIANTI
  INCLINAZIONE_MOLA_GRD = INCLINAZIONE_MOLA_RAD / PG * 180                 'GRADI
 If (str = "RAD") Then
Get_Worm_Tilt = INCLINAZIONE_MOLA_RAD
 Else
Get_Worm_Tilt = INCLINAZIONE_MOLA_GRD
 End If
 
End Function
Private Sub Plot_Rullo(ByRef cpXd(), ScalaX, PicW, ByRef cpYd(), Ymax0, Ymin0, ScalaY, PicH, Np, str)
Dim i, j, ii, jj As Integer
Dim pX1, pX2, pY1, pY2, YR As Double
Dim MinX, MaxX, MinY, MaxY
Dim stmp As String
If (str = "Surface") Then
 OEMX.OEM1PicCALCOLO.ForeColor = RGB(240, 235, 172): OEMX.OEM1PicCALCOLO.DrawStyle = 0: OEMX.OEM1PicCALCOLO.DrawWidth = 1
ElseIf (str = "Boundary") Then
 If (YdapRul > HRULLOREF) Then
   OEMX.OEM1PicCALCOLO.ForeColor = vbRed: OEMX.OEM1PicCALCOLO.DrawWidth = 2
 Else
   OEMX.OEM1PicCALCOLO.ForeColor = RGB(154, 205, 50): OEMX.OEM1PicCALCOLO.DrawWidth = 1
 End If
End If
'-------------------------------------
  For i = 1 To (Int(Np) - 1)
   If (i = 1) Then cpYd(i) = -10000: cpYd(i + 1) = -10000
   If (i = 2) Then cpYd(i) = -10000
   If (cpXd(i) > -SpostRul / 2) Then cpXd(i) = -SpostRul / 2
   If (cpXd(i + 1) > -SpostRul / 2) Then cpXd(i + 1) = -SpostRul / 2
   pX1 = -cpXd(i) * ScalaX + 3 * PicW / 5: pY1 = -(cpYd(i) - (Ymax0 + Ymin0) / 2) * ScalaY + PicH / 2
   pX2 = -cpXd(i + 1) * ScalaX + 3 * PicW / 5: pY2 = -(cpYd(i + 1) - (Ymax0 + Ymin0) / 2) * ScalaY + PicH / 2
   
   If (str = "Surface") Then
    GoSub Reimann
   ElseIf (str = "Boundary") Then
    GoSub Euclide
   End If
  Next i

 'Fianco 2: profilo rullo
  For i = (Int(Np) - 1) To 1 Step -1
   If (i = 1 Or i = 2) Then cpYd(i) = -10000
   pX1 = cpXd(i) * ScalaX + 3 * PicW / 5: pY1 = -(cpYd(i) - (Ymax0 + Ymin0) / 2) * ScalaY + PicH / 2
   pX2 = cpXd(i + 1) * ScalaX + 3 * PicW / 5: pY2 = -(cpYd(i + 1) - (Ymax0 + Ymin0) / 2) * ScalaY + PicH / 2
   If (str = "Surface") Then
    GoSub Reimann
   ElseIf (str = "Boundary") Then
    GoSub Euclide
   End If
  Next i
  
' asse simmetria
  If (str = "Boundary" And SpostRul <> 0) Then
  OEMX.OEM1PicCALCOLO.DrawStyle = 4
   
   pX1 = -cpXd(Int(Np)) * ScalaX + 3 * PicW / 5: pY1 = -(cpYd(Int(Np)) - (Ymax0 + Ymin0) / 2) * ScalaY + PicH / 2
   pX2 = cpXd(Int(Np)) * ScalaX + 3 * PicW / 5: pY2 = -(cpYd(Int(Np)) - (Ymax0 + Ymin0) / 2) * ScalaY + PicH / 2
        
        OEMX.OEM1PicCALCOLO.Line (pX1, pY1)-(pX1, 1000)
        OEMX.OEM1PicCALCOLO.Line (pX2, pY2)-(pX2, 1000)
  End If
' If (str = "Boundary") Then
' OEMX.OEM1PicCALCOLO.ForeColor = vbBlack 'RGB(154, 205, 50)
'   pX1 = -cpXd(2) * ScalaX + 3 * PicW / 5: pY1 = -(cpYd(2) - (Ymax0 + Ymin0) / 2) * ScalaY + PicH / 2
'   pX2 = -cpXd(2 + 1) * ScalaX + 3 * PicW / 5: pY2 = -(cpYd(2 + 1) - (Ymax0 + Ymin0) / 2) * ScalaY + PicH / 2
'  stmp = "Dresser": OEMX.OEM1PicCALCOLO.CurrentX = 3 * PicW / 5 - OEMX.OEM1PicCALCOLO.TextWidth(stmp) / 2: OEMX.OEM1PicCALCOLO.CurrentY = pY2 - OEMX.OEM1PicCALCOLO.TextHeight(stmp): OEMX.OEM1PicCALCOLO.Print stmp
' End If
 
 Exit Sub
       
Reimann:
 If (pX1 < pX2) Then
  MinX = pX1: MaxX = pX2
  MinY = pY1: MaxY = pY2
 Else
  MaxX = pX1: MinX = pX2
  MaxY = pY1: MinY = pY2
 End If
'YR = pY1 + ii * (pY2 - pY1) / (Int(Max * 10) - Int(Min * 10))
 For ii = Int(MinX * 10) To Int(MaxX * 10) - 1
 YR = MinY + (ii / 10 - Int(MinX * 10) / 10) * ((pY2 - pY1) / (pX2 - pX1))
        OEMX.OEM1PicCALCOLO.Line (ii / 10, YR)-((ii + 1) / 10, 10000)
'    Next ii
 Next ii
 Return
Euclide:
 OEMX.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2)
Return
End Sub
Private Sub FillWheel(ByRef cpXd(), ScalaX, PicW, ByRef cpYd(), Ymax0, Ymin0, ScalaY, PicH, Np)
Dim i, j, ii, jj As Integer
Dim pX1, pX2, pY1, pY2, YR As Double
Dim MinX, MaxX, MinY, MaxY
OEMX.OEM1PicCALCOLO.ForeColor = RGB(216, 218, 229): OEMX.OEM1PicCALCOLO.DrawStyle = 0: OEMX.OEM1PicCALCOLO.DrawWidth = 1
'-------------------------------------
      For i = 1 To (Int(Np) - 1)
'            If (i = 1) Then cpYd(i) = -10000: cpYd(i + 1) = -10000
 '     If (i = 2) Then cpYd(i) = -10000
        pX1 = -cpXd(i) * ScalaX + 3 * PicW / 5: pY1 = -(cpYd(i) - (Ymax0 + Ymin0) / 2) * ScalaY + PicH / 2
        pX2 = -cpXd(i + 1) * ScalaX + 3 * PicW / 5: pY2 = -(cpYd(i + 1) - (Ymax0 + Ymin0) / 2) * ScalaY + PicH / 2
        GoSub Reimann
      Next i
      'Fianco 2: profilo rullo
      For i = (Int(Np) - 1) To 1 Step -1
 '     If (i = 1 Or i = 2) Then cpYd(i) = -10000
        pX1 = cpXd(i) * ScalaX + 3 * PicW / 5: pY1 = -(cpYd(i) - (Ymax0 + Ymin0) / 2) * ScalaY + PicH / 2
        pX2 = cpXd(i + 1) * ScalaX + 3 * PicW / 5: pY2 = -(cpYd(i + 1) - (Ymax0 + Ymin0) / 2) * ScalaY + PicH / 2
        GoSub Reimann
      Next i
      
      Exit Sub
       
Reimann:
If (pX1 < pX2) Then
MinX = pX1: MaxX = pX2
MinY = pY1: MaxY = pY2
Else
MaxX = pX1: MinX = pX2
MaxY = pY1: MinY = pY2
End If
'YR = pY1 + ii * (pY2 - pY1) / (Int(Max * 10) - Int(Min * 10))
For ii = Int(MinX * 10) To Int(MaxX * 10) - 1
YR = MinY + (ii / 10 - Int(MinX * 10) / 10) * ((pY2 - pY1) / (pX2 - pX1))
        OEMX.OEM1PicCALCOLO.Line (ii / 10, YR)-((ii + 1) / 10, -10000)
'    Next ii
Next ii
Return
Euclide:
 OEMX.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2)
Return
End Sub
Private Sub FillGear(ByRef cpX(), ScalaX, PicW, ByRef cpY(), Ymax0, Ymin0, ScalaY, PicH, Samp, DI, DeltaI, DeltaTh)
Dim i, j, ii, jj As Integer
Dim pX1, pX2, pY1, pY2, XR1, XR2 As Double
Dim PX3, pY3, pX4, pY4 As Double
Dim MinX, MaxX, MinY, MaxY
OEMX.OEM1PicCALCOLO.ForeColor = RGB(198, 221, 246): OEMX.OEM1PicCALCOLO.DrawStyle = 0: OEMX.OEM1PicCALCOLO.DrawWidth = 2
'-------------------------------------
 For i = 1 To Samp

        pX1 = -cpX(i) * ScalaX + 3 * PicW / 5: pY1 = -(cpY(i) - (Ymax0 + Ymin0) / 2) * ScalaY + PicH / 2
        pX2 = -cpX(i + 1) * ScalaX + 3 * PicW / 5: pY2 = -(cpY(i + 1) - (Ymax0 + Ymin0) / 2) * ScalaY + PicH / 2
        
        PX3 = cpX(i) * ScalaX + 3 * PicW / 5: pY3 = -(cpY(i) - (Ymax0 + Ymin0) / 2) * ScalaY + PicH / 2
        pX4 = cpX(i + 1) * ScalaX + 3 * PicW / 5: pY4 = -(cpY(i + 1) - (Ymax0 + Ymin0) / 2) * ScalaY + PicH / 2
        
   '      Call PntRayon(Di / 2, 0, -DeltaI, 0, -(DeltaTh * (Samp + 1 - i) / (Samp + 1)) + (-PG / 2 + 0 * DeltaTh), pX3, pY3)
   '      Call PntRayon(Di / 2, 0, -DeltaI, 0, -(DeltaTh * (Samp - i) / (Samp + 1)) + (-PG / 2 + 0 * DeltaTh), pX4, pY4)
   '     pX3 = pX3 * ScalaX + 3 * PicW / 5: pY3 = -(pY3 - (Ymax0 + Ymin0) / 2) * ScalaY + PicH / 2 '- (Ymax0 + Ymin0) / 2
   '     pX4 = pX4 * ScalaX + 3 * PicW / 5: pY4 = -(pY4 - (Ymax0 + Ymin0) / 2) * ScalaY + PicH / 2 '- (Ymax0 + Ymin0) / 2'
        GoSub Reimann
 Next i
      'Fianco 2: profilo gear
 'For i = Samp To 1 Step -1
 '       pX1 = cpX(i) * ScalaX + 3 * PicW / 5: pY1 = -(cpY(i) - (Ymax0 + Ymin0) / 2) * ScalaY + PicH / 2
 '       pX2 = cpX(i + 1) * ScalaX + 3 * PicW / 5: pY2 = -(cpY(i + 1) - (Ymax0 + Ymin0) / 2) * ScalaY + PicH / 2
 '        Call PntRayon(Di / 2, 0, -DeltaI, 0, DeltaTh * (Samp + 1 - i) / (Samp + 1) + (-PG / 2 + 0 * DeltaTh), pX3, pY3)
 '        Call PntRayon(Di / 2, 0, -DeltaI, 0, DeltaTh * (Samp - i) / (Samp + 1) + (-PG / 2 + 0 * DeltaTh), pX4, pY4)
 '       pX3 = pX3 * ScalaX + 3 * PicW / 5: pY3 = -(pY3 - (Ymax0 + Ymin0) / 2) * ScalaY + PicH / 2 '- (Ymax0 + Ymin0) / 2
 '       pX4 = pX4 * ScalaX + 3 * PicW / 5: pY4 = -(pY4 - (Ymax0 + Ymin0) / 2) * ScalaY + PicH / 2 '- (Ymax0 + Ymin0) / 2'
 ' GoSub Reimann
 'Next i
Exit Sub

Reimann:
If (pY1 < pY2) Then
 MinX = pX1: MaxX = pX2
 MinY = pY1: MaxY = pY2
Else
 MaxX = pX1: MinX = pX2
 MaxY = pY1: MinY = pY2
End If

For ii = Int(MinY * 5) To Int(MaxY * 5) - 1
 XR1 = pX1 + (ii / 5 - Int(MaxY * 5) / 5) * ((pX2 - pX1) / (pY2 - pY1))
 XR2 = PX3 + (ii / 5 - Int(MaxY * 5) / 5) * ((pX4 - PX3) / (pY4 - pY3))
 OEMX.OEM1PicCALCOLO.Line (XR1, ii / 5)-(XR2, ii / 5)
Next ii
Return

Euclide:
 OEMX.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2)
Return
End Sub
Private Sub FillGear1(ByRef cpX(), ScalaX, PicW, ByRef cpY(), Ymax0, Ymin0, ScalaY, PicH, Samp, DI, DeltaI, DeltaTh)
Dim i, j, ii, jj As Integer
Dim pX1, pX2, pY1, pY2, YR As Double
Dim PX3, pY3, pX4, pY4 As Double
Dim MinX, MaxX, MinY, MaxY
OEMX.OEM1PicCALCOLO.ForeColor = RGB(198, 221, 246): OEMX.OEM1PicCALCOLO.DrawStyle = 0: OEMX.OEM1PicCALCOLO.DrawWidth = 2
'-------------------------------------
 For i = 1 To Samp

        pX1 = -cpX(i) * ScalaX + 3 * PicW / 5
        pY1 = -(cpY(i) - (Ymax0 + Ymin0) / 2) * ScalaY + PicH / 2
        pX2 = -cpX(i + 1) * ScalaX + 3 * PicW / 5
        pY2 = -(cpY(i + 1) - (Ymax0 + Ymin0) / 2) * ScalaY + PicH / 2
         Call PntRayon(DI / 2, 0, -DeltaI, 0, -(DeltaTh * (Samp + 1 - i) / (Samp + 1)) + (-PG / 2 + 0 * DeltaTh), PX3, pY3)
         Call PntRayon(DI / 2, 0, -DeltaI, 0, -(DeltaTh * (Samp - i) / (Samp + 1)) + (-PG / 2 + 0 * DeltaTh), pX4, pY4)
        PX3 = PX3 * ScalaX + 3 * PicW / 5: pY3 = -(pY3 - (Ymax0 + Ymin0) / 2) * ScalaY + PicH / 2 '- (Ymax0 + Ymin0) / 2
        pX4 = pX4 * ScalaX + 3 * PicW / 5: pY4 = -(pY4 - (Ymax0 + Ymin0) / 2) * ScalaY + PicH / 2 '- (Ymax0 + Ymin0) / 2'
        GoSub Reimann
 Next i
      'Fianco 2: profilo gear
 For i = Samp To 1 Step -1
        pX1 = cpX(i) * ScalaX + 3 * PicW / 5: pY1 = -(cpY(i) - (Ymax0 + Ymin0) / 2) * ScalaY + PicH / 2
        pX2 = cpX(i + 1) * ScalaX + 3 * PicW / 5: pY2 = -(cpY(i + 1) - (Ymax0 + Ymin0) / 2) * ScalaY + PicH / 2
         Call PntRayon(DI / 2, 0, -DeltaI, 0, DeltaTh * (Samp + 1 - i) / (Samp + 1) + (-PG / 2 + 0 * DeltaTh), PX3, pY3)
         Call PntRayon(DI / 2, 0, -DeltaI, 0, DeltaTh * (Samp - i) / (Samp + 1) + (-PG / 2 + 0 * DeltaTh), pX4, pY4)
        PX3 = PX3 * ScalaX + 3 * PicW / 5: pY3 = -(pY3 - (Ymax0 + Ymin0) / 2) * ScalaY + PicH / 2 '- (Ymax0 + Ymin0) / 2
        pX4 = pX4 * ScalaX + 3 * PicW / 5: pY4 = -(pY4 - (Ymax0 + Ymin0) / 2) * ScalaY + PicH / 2 '- (Ymax0 + Ymin0) / 2'
  GoSub Reimann
 Next i
Exit Sub
       
Reimann:
If (pX1 < pX2) Then
 MinX = pX1: MaxX = pX2
 MinY = pY1: MaxY = pY2
Else
 MaxX = pX1: MinX = pX2
 MaxY = pY1: MinY = pY2
End If

For ii = Int(MinX * 10) To Int(MaxX * 10) - 1
YR = MinY + (ii / 10 - Int(MinX * 10) / 10) * ((pY2 - pY1) / (pX2 - pX1))
        OEMX.OEM1PicCALCOLO.Line (ii / 10, YR)-(PX3, pY4)
'    Next ii
Next ii
Return
Euclide:
 OEMX.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2)
Return
End Sub
Private Sub Disegna_Profilo_Vettoriale(ByRef cpX, ScalaX, PicW, ByRef cpY, Ymax0, Ymin0, ScalaY, PicH, Samp, COLORE, Spessore)
'Dim i, j, ii, jj As Integer
Dim pX1, pX2, pY1, pY2 As Double
'Dim pX3, pY3, pX4, pY4, YR As Double

      'Ingranaggio: Fianco 1 stmp = "F1":
      OEMX.OEM1PicCALCOLO.ForeColor = COLORE: OEMX.OEM1PicCALCOLO.DrawStyle = 0: OEMX.OEM1PicCALCOLO.DrawWidth = Spessore
      For i = 1 To Samp
        pX1 = -cpX(i) * ScalaX + 3 * PicW / 5: pY1 = -(cpY(i) - (Ymax0 + Ymin0) / 2) * ScalaY + PicH / 2
        pX2 = -cpX(i + 1) * ScalaX + 3 * PicW / 5: pY2 = -(cpY(i + 1) - (Ymax0 + Ymin0) / 2) * ScalaY + PicH / 2
        GoSub Disegna
      Next i
      'Ingranaggio: Fianco 2 stmp = "F2"
      For i = Samp To 1 Step -1
        pX1 = cpX(i) * ScalaX + 3 * PicW / 5: pY1 = -(cpY(i) - (Ymax0 + Ymin0) / 2) * ScalaY + PicH / 2
        pX2 = cpX(i + 1) * ScalaX + 3 * PicW / 5: pY2 = -(cpY(i + 1) - (Ymax0 + Ymin0) / 2) * ScalaY + PicH / 2
        GoSub Disegna
      Next i
      Exit Sub
Disegna:
OEMX.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2)
Return
End Sub
'------------------------------------------
' Caloclo parametri ragionevoli di dentatura per rappresentazione profilo
' INCOGNITE: mnd, pro, rp  ( modulo rotolamento dentatura, protuberanza di dentatura, raggio tip dentatura)
' CONDIZIONI:
' Protuberanza massima pari al sovrametallo
' La tangente al sap comune alla trocoide ( introduco incognita punto tangenza)
' Cerco modulo dentatura tale da minimizzare distanza inizio trocoide e punto sap di progetto.
'------------------------------------------



