Attribute VB_Name = "CONTROLO"
Option Explicit

Dim Session As IMCDomain

Global sQUOTA_RULLI     As String
Global sQUOTA_WILDHABER As String

'Variabili dal file di controllo
Global SAP          As Double
Global EAP          As Double
Global AngPreNor    As Double
Global AngEli       As Double
Global NZ           As Integer
Global ModNor       As Double
Global SpeCirNorMis As Double
Global MoreUse      As Double
Global Np           As Integer
Global LunCon       As Double
Global iPd(4)       As Integer
Global Pd0f1()      As Double
Global Pd0f2()      As Double
Global HyP()        As Double
Global Pd1f1()      As Double
Global Pd1f2()      As Double
Global Pd2f1()      As Double
Global Pd2f2()      As Double
Global Pd3f1()      As Double
Global Pd3f2()      As Double
Global Pd4f1()      As Double
Global Pd4f2()      As Double
Global FaF1d(4)     As Double
Global faF2d(4)     As Double
Global ffaF1d(4)    As Double
Global ffaF2d(4)    As Double
Global fhaF1d(4)    As Double
Global fhaF2d(4)    As Double
Global NE           As Integer
Global FasCon       As Double
Global iEd(4)       As Integer
Global HyE()        As Double
Global Ed0f1()      As Double
Global Ed0f2()      As Double
Global Ed1f1()      As Double
Global Ed1f2()      As Double
Global Ed2f1()      As Double
Global Ed2f2()      As Double
Global Ed3f1()      As Double
Global Ed3f2()      As Double
Global Ed4f1()      As Double
Global Ed4f2()      As Double
Global fbF1d(4)     As Double
Global fbF2d(4)     As Double
Global ffbF1d(4)    As Double
Global ffbF2d(4)    As Double
Global fhbF1d(4)    As Double
Global fhbF2d(4)    As Double

Global DivCon       As Integer
Global Divf1()      As Double
Global Divf2()      As Double
Global HxD()        As Double
Global NZMinF1      As Double
Global ErrMinF1     As Double
Global NZMaxF1      As Double
Global ErrMaxF1     As Double
Global NZMinF2      As Double
Global ErrMinF2     As Double
Global NZMaxF2      As Double
Global ErrMaxF2     As Double

Global Const Box0W = 18.5
Global Const Box0H = 27.5

Global Const Box1W = 14
Global Const Box1H = 9

'** Parametri per la stampa di linee
Global X1 As Single
Global Y1 As Single
Global R1 As Single
Global X2 As Single
Global Y2 As Single
Global R2 As Single

'** Incrementi per stampa testi
Global iRG As Single
Global iCl As Single

Global i        As Integer
Global j        As Integer
Global MoreUse1 As Single
Global MoreUse2 As Single
Global TESTO    As String

'** Scale grafiche per la stampa su carta A4
Global Testa         As String
Global Piede         As String

'** Opzioni
Global SiOpt(4, 1)  As Integer

'** Cambio lingua
Global INTESTAZIONE   As String

Global TitoloPROELI    As String
Global TitoloDIV       As String

Global ComPROELI1(4)   As String
Global ComPROELI2(4)   As String

Global comVisPROELI(7, 2) As String

Global Logo1 As String
Global Logo2 As String
Global logo3 As String

Global SINISTRA As String
Global DESTRA   As String

Global PathMIS   As String

Global FPmaxF(2) As Double
Global FPminF(2) As Double

'Variabili controllo Viti
Public DiamRulli  As Double
Public QRviteMis  As Double

'Punti inizio e fine controllo
Public CHK_Ini_PROFILO  As Integer
Public CHK_Fin_PROFILO  As Integer
Public CHK_Ini_ELICA    As Integer
Public CHK_Fin_ELICA    As Integer
 
Public CorPrF1T As Double
Public CorPrF2T As Double
Public CorElF1T As Double
Public CorElF2T As Double
'

Sub STAMPA_DIAMETRI()
'
Dim X    As Single
Dim i    As Integer
Dim k1   As Integer
Dim k2   As Integer
Dim k3   As Integer
Dim k4   As Integer
Dim j    As Integer
Dim rsp  As Integer
'
Dim MLeft As Single
Dim MTop  As Single
Dim Box0W As Single
Dim Box0H As Single
'** Parametri per la stampa di linee
Dim X1 As Single
Dim Y1 As Single
Dim X2 As Single
Dim Y2 As Single
'** Area di stampa dei grafici
Dim Box1L As Single
Dim Box1T As Single
Dim Box1W As Single
Dim Box1H As Single
'
Dim iRG As Single
Dim iCl As Single
'
Dim riga As String
Dim stmp As String
'
On Error Resume Next
  '
  Printer.ScaleMode = 7
  For i = 1 To 2
    Printer.FontName = "Courier New"
    Printer.FontSize = 10
  Next i
  '
  MLeft = 2: MTop = 1
  Box0W = 17: Box0H = 27
  '
  iRG = Printer.TextHeight("A")
  iCl = Printer.TextWidth("A")
  '
  Box1L = 5: Box1T = 1
  Box1W = 12: Box1H = 12
  '
  GoSub STAMPA_INTESTAZIONE
  '
  Dim NrigheMax As Integer
  Dim iiRG As Integer
  Dim iiPG As Integer
  '
  NrigheMax = (Box0H - 3) / iRG
  iiPG = 1
  riga = ""
  i = 1
  k1 = 1
  k4 = 1
  Do
    'terminatore di riga
    k2 = InStr(k1, CHK0.txtHELP.Text, Chr(13))
    k3 = InStr(k1, CHK0.txtHELP.Text, Chr(10))
    'prendo il minimo
    If k3 >= k2 Then k4 = k2 Else k4 = k3
    If k4 > k1 Then
      riga = Mid$(CHK0.txtHELP.Text, k1, k4 - k1)
      riga = Trim$(riga)
      If k3 >= k2 Then k1 = k3 + 1 Else k1 = k2 + 1
      i = i + 1
      iiRG = (i - 1) Mod NrigheMax
      Printer.CurrentX = X1 + iCl
      Printer.CurrentY = Y1 + (iiRG + 3) * iRG
      'riga = Right(String(6, " ") & Format$(i - 1, "#####0"), 6) & "] " & riga
      Printer.Print riga
      If i = iiPG * NrigheMax Then
        Printer.NewPage
        GoSub STAMPA_INTESTAZIONE
        iiPG = iiPG + 1
      End If
    Else
      Exit Do
    End If
  Loop
  Printer.EndDoc

Exit Sub

STAMPA_INTESTAZIONE:
  'Squadratura della pagina
  X1 = MLeft: Y1 = MTop
  X2 = MLeft + Box0W: Y2 = MTop + Box0H
  'Printer.Line (X1, Y1)-(X2, Y2), QBColor(0), B
  'Intestazione della pagina
  Printer.CurrentX = X1 + iCl
  Printer.CurrentY = Y1 + iRG
  stmp = CHK0.INTESTAZIONE.Caption
  stmp = stmp & " Date: " & Format$(Now, "dd/mm/yy")
  stmp = stmp & " Time: " & Format$(Now, "hh:mm:ss")
  Printer.Print stmp
  'Linea di separazione
  X1 = MLeft: Y1 = MTop + 1
  X2 = MLeft + Box0W: Y2 = MTop + 1
  Printer.Line (X1, Y1)-(X2, Y2), QBColor(0)
  '
  Printer.CurrentX = X1 + iCl
  Printer.CurrentY = Y1 + 1 * iRG
  Printer.Print PathMIS & "\DIAMETRI"
  '
Return

End Sub

Sub STAMPA_CONTENUTO_txtHELP()
'
Dim X    As Single
Dim i    As Integer
Dim k1   As Integer
Dim k2   As Integer
Dim k3   As Integer
Dim k4   As Integer
Dim j    As Integer
Dim rsp  As Integer
'
Dim MLeft As Single
Dim MTop  As Single
Dim Box0W As Single
Dim Box0H As Single
'
'Parametri per la stampa di linee
Dim X1 As Single
Dim Y1 As Single
Dim X2 As Single
Dim Y2 As Single
'
'Area di stampa dei grafici
Dim Box1L As Single
Dim Box1T As Single
Dim Box1W As Single
Dim Box1H As Single
'
Dim iRG   As Single
Dim iCl   As Single
'
Dim riga  As String
Dim stmp  As String
'
On Error Resume Next
  '
  StopRegieEvents
  rsp = MsgBox(CHK0.Text2.Text & " (Text FILE) " & " --> " & Printer.DeviceName & " ? ", 4 + 32, "WARNING: " & CHK0.INTESTAZIONE.Caption)
  ResumeRegieEvents
  '
  If (rsp = 6) Then
    stmp = "Courier New"
    stmp = CHK0.txtHELP.FontName
    '
    Printer.ScaleMode = 7
    For i = 1 To 2
      Printer.FontName = stmp
      Printer.FontSize = 10
    Next i
    '
    MLeft = 2: MTop = 1
    Box0W = 17: Box0H = 27
    '
    iRG = Printer.TextHeight("A")
    iCl = Printer.TextWidth("A")
    '
    Box1L = 5: Box1T = 1
    Box1W = 12: Box1H = 12
    '
    Dim NrigheMax As Integer
    Dim iiRG      As Integer
    Dim iiPG      As Integer
    '
    NrigheMax = (Box0H - 3) / iRG
    iiPG = 1
    riga = ""
    i = 1
    k1 = 1
    k4 = 1
    '
    GoSub STAMPA_INTESTAZIONE
    '
    Do
      '
      'terminatore di riga
      k2 = InStr(k1, CHK0.txtHELP.Text, Chr(13))
      k3 = InStr(k1, CHK0.txtHELP.Text, Chr(10))
      'prendo il minimo
      If k3 >= k2 Then k4 = k2 Else k4 = k3
      If k4 > k1 Then
        '
        riga = Mid$(CHK0.txtHELP.Text, k1, k4 - k1)
        riga = Trim$(riga)
        If k3 >= k2 Then k1 = k3 + 1 Else k1 = k2 + 1
        i = i + 1
        iiRG = (i - 1) Mod NrigheMax
        Printer.CurrentX = X1 + iCl
        Printer.CurrentY = Y1 + (iiRG + 3) * iRG
        'riga = Right(String(6, " ") & Format$(i - 1, "#####0"), 6) & "] " & riga
        Printer.Print riga
        '
        If (i = iiPG * NrigheMax) Then
          '
          Printer.NewPage
          iiPG = iiPG + 1
          GoSub STAMPA_INTESTAZIONE
          '
        End If
        '
      Else
        '
        Exit Do
        '
      End If
      '
    Loop
    '
    Printer.EndDoc
    '
  Else
    '
    WRITE_DIALOG "Opeation aborted by user!"
    '
  End If
  '
Exit Sub

STAMPA_INTESTAZIONE:
  'Squadratura della pagina
  X1 = MLeft: Y1 = MTop
  X2 = MLeft + Box0W: Y2 = MTop + Box0H
  'Printer.Line (X1, Y1)-(X2, Y2), QBColor(0), B
  'Intestazione della pagina
  Printer.CurrentX = X1 + iCl
  Printer.CurrentY = Y1 + iRG
  stmp = CHK0.INTESTAZIONE.Caption
  stmp = stmp & " Date: " & Format$(Now, "dd/mm/yy")
  stmp = stmp & " Time: " & Format$(Now, "hh:mm:ss")
  Printer.Print stmp
  'NUMERO DELLA PAGINA
  stmp = Format$(iiPG, "######0")
  Printer.CurrentX = MLeft + Box0W - Printer.TextWidth(stmp)
  Printer.CurrentY = Y1 + iRG
  Printer.Print stmp
  'Linea di separazione
  X1 = MLeft: Y1 = MTop + 1
  X2 = MLeft + Box0W: Y2 = MTop + 1
  Printer.Line (X1, Y1)-(X2, Y2), QBColor(0)
  '
  Printer.CurrentX = X1 + iCl
  Printer.CurrentY = Y1 + 1 * iRG
  Printer.Print PathMIS & "\" & CHK0.Text2.Text
  '
Return

End Sub

Sub StampaMisuraPeE_VITI()
'
Dim TabX             As Single
Dim TabY             As Single
Dim stmp             As String
Dim TextFont         As String
Dim MLeft            As Single
Dim MTop             As Single
Dim NomeControllo    As String
Dim DataControllo    As String
Dim NomeLavorazione  As String
Dim NomePezzo        As String
Dim NomePezzoDEFAULT As String
Dim TIPO_LAVORAZIONE As String
Dim rsp              As Long
Dim k1               As Long
Dim ftmp             As Double
Static Com1(4)       As String
Static Com2(4)       As String
'
On Error Resume Next
  '
  'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  'RICAVO IL NOME DEL TIPO DI LAVORAZIONE
  NomeLavorazione = RICERCA_NOME_TIPO(TIPO_LAVORAZIONE)
  'LEGGO IL NOME DEL PEZZO
  NomePezzo = LEGGI_NomePezzo(TIPO_LAVORAZIONE)
  'LEGGO IL NOME DEL CONTROLLO
  NomeControllo = GetInfo("CHK0", "FileMisura", Path_LAVORAZIONE_INI)
  'RICAVO LA DATA DAL FILE
  If Len(NomeControllo) > 0 And Dir(CHK0.File1.Path & "\" & NomeControllo) <> "" Then
    DataControllo = FileDateTime(CHK0.File1.Path & "\" & NomeControllo)
  Else
    DataControllo = FileDateTime(CHK0.File1.Path & "\DEFAULT")
  End If
  Open PathMIS & "\" & "DEFAULT" For Input As 1
    For i = 1 To 6
      Line Input #1, NomePezzoDEFAULT
    Next i
  Close #1
  'SEGNALAZIONE UTENTE
  WRITE_DIALOG TitoloPROELI & " --> " & Printer.DeviceName
  'CARATTERI DI STAMPA
      If (LINGUA = "CH") Then
    TextFont = "MS Song"
  ElseIf (LINGUA = "RU") Then
    TextFont = "Arial Cyr"
  Else
    TextFont = "Arial"
  End If
  'IMPOSTAZIONE STAMPANTE
  Printer.ScaleMode = 7
  Printer.DrawWidth = 1
  'MARGINI
  MLeft = val(GetInfo("CHK0", "MLeft", Path_LAVORAZIONE_INI))
  MTop = val(GetInfo("CHK0", "MTop", Path_LAVORAZIONE_INI))
  'Squadratura della pagina
  X1 = MLeft: Y1 = MTop
  X2 = MLeft + Box0W: Y2 = MTop + Box0H
  'Printer.Line (X1, Y1)-(X2, Y2), QBColor(0), B
  'Linea di separazione per titolo
  X1 = MLeft: Y1 = (MTop + 1)
  X2 = MLeft + Box0W: Y2 = Y1
  Printer.Line (X1, Y1)-(X2, Y2), QBColor(0)
  'Linea di separazione: primo gruppo
  X1 = MLeft: Y1 = (MTop + 1) + 2.5
  X2 = MLeft + Box0W: Y2 = Y1
  Printer.Line (X1, Y1)-(X2, Y2), QBColor(0)
  'Linea di separazione: secondo gruppo
  X1 = MLeft: Y1 = (MTop + 1) + 2.5 + 10
  X2 = MLeft + Box0W: Y2 = Y1
  Printer.Line (X1, Y1)-(X2, Y2), QBColor(0)
  X1 = MLeft: Y1 = (MTop + 1) + 2.5 + 10 + 2
  X2 = MLeft + Box0W: Y2 = Y1
  'Printer.Line (X1, Y1)-(X2, Y2), QBColor(0)
  'Linea di separazione: terzo gruppo
  X1 = MLeft: Y1 = (MTop + 1) + 2.5 + 10 + 2 + 10
  X2 = MLeft + Box0W: Y2 = Y1
  'Printer.Line (X1, Y1)-(X2, Y2), QBColor(0)
  'Linea di separazione verticale
  X1 = MLeft + 1.5: Y1 = (MTop + 1) + 2.5
  X2 = X1: Y2 = Y1 + Box0H - 1 - 2.5
  'Printer.Line (X1, Y1)-(X2, Y2), QBColor(0)
  'Intestazione della pagina
  X1 = MLeft: Y1 = MTop
  'IMMAGINE BITMAP
  Printer.PaintPicture CHK0.PicLogo.Picture, X1, Y1, 2, 2
  Printer.PaintPicture CHK0.PicLogo2.Picture, X1, Y1 + 2, 2, 2
  For i = 1 To 2
    Printer.FontName = TextFont
    Printer.FontSize = 14
  Next i
  'SCRIVO IL TITOLO DELLA PAGINA
  TESTO = logo3 & " " & TitoloPROELI
  Printer.CurrentX = X1 + Box0W - 0.25 - Printer.TextWidth(TESTO)
  Printer.CurrentY = Y1 + (1 - Printer.TextHeight(TESTO)) / 2
  Printer.Print TESTO
  'Leggi VBhelp di FontSize (1440/567) [cm] = 1 [inch]
  For i = 1 To 2
    Printer.FontName = TextFont
    Printer.FontSize = 10
  Next i
  iRG = Printer.TextHeight("A")
  iCl = Printer.TextWidth("A")
  '*********
  Com1(1) = ""
  Com1(2) = ""
  Com1(3) = ""
  Com1(4) = Prendi(211 + 3, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
  '*********
  If Len(NomePezzoDEFAULT) > 0 Then
    Com2(1) = NomeLavorazione & ": " & NomePezzoDEFAULT
  Else
    If NomeControllo = "DEFAULT" Then
      Com2(1) = NomeLavorazione & ": " & NomePezzo
    Else
      Com2(1) = NomeLavorazione
    End If
  End If
  Com2(2) = " : "
  Com2(3) = " : "
  Com2(4) = " : " & frmt(SpeCirNorMis, 3)
  '*********
  X1 = MLeft + 2 + iCl: Y1 = MTop + 1
  TabX = 0
  For i = 1 To 4
    Printer.CurrentX = X1
    Printer.CurrentY = Y1 + i * iRG
    Printer.Print Com1(i)
    If TabX < Printer.TextWidth(Com1(i)) Then
      TabX = Printer.TextWidth(Com1(i))
    End If
  Next i
  i = 1
    Printer.CurrentX = X1
    Printer.CurrentY = Y1 + i * iRG
    Printer.Print Com2(i)
  For i = 2 To 4
    Printer.CurrentX = X1 + TabX
    Printer.CurrentY = Y1 + i * iRG
    Printer.Print Com2(i)
  Next i
  '*********
  Com1(1) = Prendi(215 + 1, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
  Com1(2) = Prendi(215 + 2, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
  Com1(3) = Prendi(203, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
  Com1(4) = "Date"
  '*********
  Com2(1) = " : " & Format$(NZ, "##0")
  Com2(2) = " : " & frmt(90 - Abs(AngEli), 3)
  If (AngEli < 0) Then
    Com2(2) = Com2(2) & "    (" & SINISTRA & ")"
  Else
    Com2(2) = Com2(2) & "    (" & DESTRA & ")"
  End If
  Com2(3) = " : " & NomeControllo
  Com2(4) = " : " & DataControllo
  '*********
  X1 = MLeft + 2 + 3 + (Box0W - 2) / 2 + iCl: Y1 = MTop + 1
  TabX = 0
  i = 1
    Printer.CurrentX = X1
    Printer.CurrentY = Y1 + (i + 1) * iRG
    Printer.Print Com1(i)
    If TabX < Printer.TextWidth(Com1(i)) Then
      TabX = Printer.TextWidth(Com1(i))
    End If
  For i = 2 To 4
    Printer.CurrentX = X1 - 6
    Printer.CurrentY = Y1 + i * iRG
    Printer.Print Com1(i)
    If TabX < Printer.TextWidth(Com1(i)) Then
      TabX = Printer.TextWidth(Com1(i))
    End If
  Next i
  i = 1
    Printer.CurrentX = X1 + TabX
    Printer.CurrentY = Y1 + (i + 1) * iRG
    Printer.Print Com2(i)
  For i = 2 To 4
    Printer.CurrentX = X1 + TabX - 6
    Printer.CurrentY = Y1 + i * iRG
    Printer.Print Com2(i)
  Next i
  'STAMPA DEI GRAFICI
  For i = 1 To 2
    Printer.FontName = TextFont
    Printer.FontSize = 8
  Next i
  iRG = Printer.TextHeight("A")
  iCl = Printer.TextWidth("A")
  '*************************************************************************
  Select Case TIPO_LAVORAZIONE
    Case "VITI_ESTERNE", "VITIV_ESTERNE", "VITI_MULTIFILETTO": Call VisPROFILO_VITI(Printer, 1)
    Case Else:                                WRITE_DIALOG "SOFTKEY DISENABLED"
  End Select
  Call VisELICA_VITI(Printer)
  Printer.EndDoc
  'SEGNALAZIONE UTENTE
  WRITE_DIALOG TitoloPROELI & " --> " & Printer.DeviceName & " OK!!"
  '
Exit Sub

StampaHeaderPeE_VITI:
  TabX = 0
  For i = 1 To 4
    Printer.CurrentX = X1
    Printer.CurrentY = Y1 + i * iRG
    Printer.Print Com1(i)
    If TabX < Printer.TextWidth(Com1(i)) Then
      TabX = Printer.TextWidth(Com1(i))
    End If
  Next i
  i = 1
    Printer.CurrentX = X1
    Printer.CurrentY = Y1 + i * iRG
    Printer.Print Com2(i)
  For i = 2 To 4
    Printer.CurrentX = X1 + TabX
    Printer.CurrentY = Y1 + i * iRG
    Printer.Print Com2(i)
  Next i
Return

End Sub

Sub VisScXY(X As Control, SCx As Single, SCy As Single, OkX As Integer, OkY As Integer)
'
Dim Fattore As Integer
Dim j       As Integer
'
On Error Resume Next
'
  If OkY = 1 Then
    '----------------
    TESTO = Testa
    X.CurrentX = X1 + (1.5 - X.TextWidth(TESTO)) / 2
    X.CurrentY = Y1
    X.Print TESTO
    '----------------
    TESTO = Format$(SCy, "###") & " : 1"
    X.CurrentX = X1 + (1.5 - X.TextWidth(TESTO)) / 2
    X.CurrentY = Y1 + 0.5
    X.Print TESTO
    If Int(10 / SCy) > 0 Then
      Fattore = 1: j = 109
    ElseIf Int((10 / SCy) * 1000) > 0 Then
      Fattore = 1000: j = 181
    End If
    X.Line (X1 + 0.25, Y1 + 1.5)-(X1 + 1.25, Y1 + 1.5), QBColor(0)
    TESTO = frmt(Fattore * 10 / SCy, 3)
    X.CurrentX = X1 + (1.5 - X.TextWidth(TESTO)) / 2
    X.CurrentY = Y1 + 2 - iRG / 2
    X.Print TESTO
    TESTO = Chr$(j) & "m"
    X.CurrentX = X1 + (1.5 - X.TextWidth(TESTO)) / 2
    X.CurrentY = Y1 + 2 + iRG / 2
    X.Print TESTO
    X.Line (X1 + 0.25, Y1 + 2.5)-(X1 + 1.25, Y1 + 2.5), QBColor(0)
  End If
  '
  If OkX = 1 Then
    TESTO = Format$(SCx, "###") & " : 1"
    X.CurrentX = X2 + (1.5 - X.TextWidth(TESTO)) / 2
    X.CurrentY = Y2 - 2.5
    X.Print TESTO
    If Int(10 / SCx) > 0 Then
      Fattore = 1: j = 109
    ElseIf Int((10 / SCx) * 1000) > 0 Then
      Fattore = 1000: j = 181
    End If
    X.Line (X2 + 0.25, Y2 - 1)-(X2 + 0.25, Y2 - 2), QBColor(0)
    TESTO = frmt(Fattore * 10 / SCx, 3)
    X.CurrentX = X2 + (1.5 - X.TextWidth(TESTO)) / 2
    X.CurrentY = Y2 - 1.5 - iRG / 2
    X.Print TESTO
    TESTO = Chr$(j) & "m"
    X.CurrentX = X2 + (1.5 - X.TextWidth(TESTO)) / 2
    X.CurrentY = Y2 - 1.5 + iRG / 2
    X.Print TESTO
    X.Line (X2 + 1.25, Y2 - 1)-(X2 + 1.25, Y2 - 2), QBColor(0)
    '----------------
    TESTO = Piede
    X.CurrentX = X2 + (1.5 - X.TextWidth(TESTO)) / 2
    X.CurrentY = Y2 - 0.5
    X.Print TESTO
    '----------------
  End If

End Sub

Sub Disegna_Griglia(X As Control, Optional Xi As Double = -1, Optional Yi As Double = -1)
'
Dim MargX   As Double
Dim MargY   As Double
Dim i       As Integer
Dim j       As Integer
Dim k       As Integer
Dim tCol    As Long
Dim Grigio  As Long
'
On Error Resume Next
  '
  MargX = 0.1
  MargY = 0.1
  MargX = X.ScaleLeft + (X.ScaleWidth - X.ScaleLeft) * MargX
  MargY = X.ScaleTop + (X.ScaleHeight - X.ScaleTop) * MargY
  '
  If Xi <> -1 Then MargX = Xi
  If Yi <> -1 Then MargY = Yi
  '
  Grigio = 180
  tCol = X.ForeColor
  X.ForeColor = RGB(Grigio, Grigio, Grigio)
  For i = 0 To 140
    For j = 0 To 90
      If ((i Mod 10) <> 0) And ((j Mod 10) <> 0) Then
        X.PSet (MargX + i * 0.1, MargY + j * 0.1)
      End If
    Next j
  Next i
  For i = 0 To 14
    X.Line (MargX + i, MargY)-(MargX + i, MargY + 9), RGB(Grigio, Grigio, Grigio)
  Next i
  For i = 0 To 9
    X.Line (MargX, MargY + i)-(MargX + 14, MargY + i), RGB(Grigio, Grigio, Grigio)
  Next i
  X.ForeColor = tCol

End Sub

Sub Disegna_Griglia1(X As Control, Optional Xi As Double = -1, Optional Yi As Double = -1)
'
Dim MargX   As Double
Dim MargY   As Double
Dim i       As Integer
Dim j       As Integer
Dim k       As Integer
Dim tCol    As Long
Dim Grigio  As Long
'
On Error Resume Next
  '
  MargX = 0.1
  MargY = 0.1
  MargX = X.ScaleLeft + (X.ScaleWidth - X.ScaleLeft) * MargX
  MargY = X.ScaleTop + (X.ScaleHeight - X.ScaleTop) * MargY
  '
  If Xi <> -1 Then MargX = Xi
  If Yi <> -1 Then MargY = Yi
  '
  Grigio = 50
  tCol = X.ForeColor
  X.ForeColor = RGB(Grigio, Grigio, Grigio)
  For i = 0 To 140
    For j = 0 To 90
      If ((i Mod 10) <> 0) And ((j Mod 10) <> 0) Then
        X.PSet (MargX + i * 0.1, MargY + j * 0.1)
      End If
    Next j
  Next i
  For i = 0 To 14
    X.Line (MargX + i, MargY)-(MargX + i, MargY + 9), RGB(Grigio, Grigio, Grigio)
  Next i
  For i = 0 To 9
    X.Line (MargX, MargY + i)-(MargX + 14, MargY + i), RGB(Grigio, Grigio, Grigio)
  Next i
  X.ForeColor = tCol

End Sub

Sub Disegna_Griglia2(X As Control, Optional Xi As Double = -1, Optional Yi As Double = -1)
'
Dim MargX   As Double
Dim MargY   As Double
Dim i       As Integer
Dim j       As Integer
Dim k       As Integer
Dim tCol    As Long
Dim Grigio  As Long
'
On Error Resume Next
  '
  MargX = 0.1
  MargY = 0.1
  MargX = X.ScaleLeft + (X.ScaleWidth - X.ScaleLeft) * MargX
  MargY = X.ScaleTop + (X.ScaleHeight - X.ScaleTop) * MargY
  '
  If Xi <> -1 Then MargX = Xi
  If Yi <> -1 Then MargY = Yi
  '
  Grigio = 50
  tCol = X.ForeColor
  X.ForeColor = RGB(Grigio, Grigio, Grigio)
  For i = 0 To 14
    X.Line (MargX + i, MargY)-(MargX + i, MargY + 9), RGB(Grigio, Grigio, Grigio)
  Next i
  For i = 0 To 9
    X.Line (MargX, MargY + i)-(MargX + 14, MargY + i), RGB(Grigio, Grigio, Grigio)
  Next i
  X.ForeColor = tCol

End Sub


Function LeggiFile_VITI(ByVal PathFILE As String) As Integer
'
Dim i             As Integer
Dim j             As Integer
Dim k             As Integer
Dim riga          As String
Dim stmp          As String
Dim stmp1         As String
Dim stmp2         As String
Dim PASSO         As Double
Dim NIENTE        As Double
Dim XXf1()        As Double
Dim XXf2()        As Double
Dim sDATI         As String
Dim DiamP         As Double
Dim QRviteTeo     As Double
Dim CODICE_PEZZO  As String
Dim DATI          As String
Dim DBB           As Database
Dim RSS           As Recordset
Dim TIPO_MACCHINA As String
'
On Error GoTo errLeggiFile_VITI
  '
  TIPO_MACCHINA = GetInfo("Configurazione", "TIPO_MACCHINA", Path_LAVORAZIONE_INI)
  TIPO_MACCHINA = UCase$(Trim$(TIPO_MACCHINA))
  '
  DiamRulli = 0: QRviteMis = 0
  LeggiFile_VITI = False
  Open PathFILE For Input As 1
    Line Input #1, riga  'COMMENTO INZIALE
    Input #1, PASSO, NZ, AngEli, Np, NE, FasCon, DivCon
    '
    ReDim XXf1(Np):      ReDim XXf2(Np)
    '
    ReDim HyP(Np)
    ReDim Pd0f1(Np):     ReDim Pd0f2(Np)
    ReDim Pd1f1(Np):     ReDim Pd1f2(Np)
    ReDim Pd2f1(Np):     ReDim Pd2f2(Np)
    ReDim Pd3f1(Np):     ReDim Pd3f2(Np)
    ReDim Pd4f1(Np):     ReDim Pd4f2(Np)
    '
    ReDim HyE(NE)
    ReDim Ed0f1(NE):     ReDim Ed0f2(NE)
    ReDim Ed1f1(NE):     ReDim Ed1f2(NE)
    ReDim Ed2f1(NE):     ReDim Ed2f2(NE)
    ReDim Ed3f1(NE):     ReDim Ed3f2(NE)
    ReDim Ed4f1(NE):     ReDim Ed4f2(NE)
    '
    ReDim HxD(DivCon)
    ReDim Divf1(DivCon): ReDim Divf2(DivCon)
    '
    Line Input #1, riga  'RITEO RIMIS
    Line Input #1, riga  'RETEO REMIS
    '
    Input #1, iPd(1), iPd(2), iPd(3), iPd(4)
    iEd(1) = iPd(1): iEd(2) = iPd(2): iEd(3) = iPd(3): iEd(4) = iPd(4)
    '
    Line Input #1, CODICE_PEZZO 'NOME PEZZO AGGIUNTO DOPO L'ACQUISIZIONE
    '
    Line Input #1, riga  'COMMENTO
    For i = 0 To Np
      Input #1, HyP(i), XXf1(i), NIENTE, NIENTE, XXf2(i), NIENTE
    Next i
    '
    'INIZIO E FINE PROFILO
    SAP = HyP(1): EAP = HyP(Np)
    '
    NIENTE = HyP(1)
    For i = 1 To Np
      HyP(i) = HyP(i) - NIENTE
    Next i
    '
    If (NE > 1) Then
      For i = 1 To NE
        HyE(i) = (i - 1) * FasCon / (NE - 1)
      Next i
    End If
    '
    'LETTURA DEI DATI TEORICI DELLA VITE DAL DATABASE
    Set DBB = OpenDatabase(PathDB, True, False)
    If (TIPO_MACCHINA = "GR250") Then
       Set RSS = DBB.OpenRecordset("ARCHIVIO_VITIV_ESTERNE")
    Else
       Set RSS = DBB.OpenRecordset("ARCHIVIO_VITI_ESTERNE")
    End If
    RSS.Index = "NOME_PEZZO"
    RSS.Seek "=", CODICE_PEZZO
    If RSS.NoMatch Then
      'SEGNALAZIONE UTENTE
      WRITE_DIALOG "WARNING: " & CODICE_PEZZO & " not found."
      'NON ESEGUO IL CALCOLO DELLA QUOTA RULLI
      'CHIUDO IL DATABASE
      RSS.Close
      DBB.Close
    Else
      DATI = RSS.Fields("DATI").Value
      'CHIUDO IL DATABASE
      RSS.Close
      DBB.Close
      i = InStr(DATI, "TIPO=")
      j = InStr(i, DATI, Chr(10))
      stmp = Mid(DATI, i, j - i - 1)
      k = InStr(stmp, "=")
      TipoVite = Right$(stmp, Len(stmp) - k)
      'If (TipoVite = "ZA") Or (TipoVite = "BALL") Then
      '  StopRegieEvents
      '  MsgBox "Invalid procedure call!!", vbCritical, "WARNING"
      '  ResumeRegieEvents
      'Else
        i = InStr(DATI, "NPRI=")
        j = InStr(i, DATI, Chr(10))
        stmp = Mid(DATI, i, j - i - 1)
        k = InStr(stmp, "=")
        'NFil = val(Right$(stmp, Len(stmp) - k))
        i = InStr(DATI, "MN=")
        j = InStr(i, DATI, Chr(10))
        stmp = Mid(DATI, i, j - i - 1)
        k = InStr(stmp, "=")
        ModNor = val(Right$(stmp, Len(stmp) - k))
        i = InStr(DATI, "ALFA=")
        j = InStr(i, DATI, Chr(10))
        stmp = Mid(DATI, i, j - i - 1)
        k = InStr(stmp, "=")
        AngPreNor = val(Right$(stmp, Len(stmp) - k))
        i = InStr(DATI, "BETA=")
        j = InStr(i, DATI, Chr(10))
        stmp = Mid(DATI, i, j - i - 1)
        k = InStr(stmp, "=")
        'BetaV = val(Right$(stmp, Len(stmp) - k))
        i = InStr(DATI, "DINT=")
        j = InStr(i, DATI, Chr(10))
        stmp = Mid(DATI, i, j - i - 1)
        k = InStr(stmp, "=")
        'DIntV = val(Right$(stmp, Len(stmp) - k))
        i = InStr(DATI, "DEXT=")
        j = InStr(i, DATI, Chr(10))
        stmp = Mid(DATI, i, j - i - 1)
        k = InStr(stmp, "=")
        'DExtV = val(Right$(stmp, Len(stmp) - k))
        i = InStr(DATI, "S0N=")
        j = InStr(i, DATI, Chr(10))
        stmp = Mid(DATI, i, j - i - 1)
        k = InStr(stmp, "=")
        'SpNormV = val(Right$(stmp, Len(stmp) - k))
        i = InStr(DATI, "QR=")
        j = InStr(i, DATI, Chr(10))
        stmp = Mid(DATI, i, j - i - 1)
        k = InStr(stmp, "=")
        QRviteTeo = val(Right$(stmp, Len(stmp) - k))
        i = InStr(DATI, "DR=")
        j = InStr(i, DATI, Chr(10))
        stmp = Mid(DATI, i, j - i - 1)
        k = InStr(stmp, "=")
        DiamRulli = val(Right$(stmp, Len(stmp) - k))
        i = InStr(DATI, "XC=")
        j = InStr(i, DATI, Chr(10))
        stmp = Mid(DATI, i, j - i - 1)
        k = InStr(stmp, "=")
        'CoordinataXV = val(Right$(stmp, Len(stmp) - k))
        i = InStr(DATI, "YC=")
        j = InStr(i, DATI, Chr(10))
        stmp = Mid(DATI, i, j - i - 1)
        k = InStr(stmp, "=")
        'CoordinataYV = val(Right$(stmp, Len(stmp) - k))
        i = InStr(DATI, "RC=")
        j = InStr(i, DATI, Chr(10))
        stmp = Mid(DATI, i, j - i - 1)
        k = InStr(stmp, "=")
        'RaggioFiancoV = val(Right$(stmp, Len(stmp) - k))
        i = InStr(DATI, "PassoElica=")
        j = InStr(i, DATI, Chr(10))
        stmp = Mid(DATI, i, j - i - 1)
        k = InStr(stmp, "=")
        'PassoElica = val(Right$(stmp, Len(stmp) - k))
        'CALCOLO DEL MODULO NORMALE DAL PASSO ELICA
        'Modulo = (PASSO * Sin(abs(AngEli) * PG / 180)) / (PG * NZ)
        i = InStr(DATI, "DprimV=")
        j = InStr(i, DATI, Chr(10))
        stmp = Mid(DATI, i, j - i - 1)
        k = InStr(stmp, "=")
        'DprimV = val(Right$(stmp, Len(stmp) - k))
        'CALCOLO DEL DIAMETRO PRIMITIVO
        DiamP = (PASSO / PG) * Tan(Abs(AngEli) * PG / 180)
        i = InStr(DATI, "DTEO=")
        j = InStr(i, DATI, Chr(10))
        stmp = Mid(DATI, i, j - i - 1)
        k = InStr(stmp, "=")
        'DiaMolTeo = val(Right$(stmp, Len(stmp) - k))
        i = InStr(DATI, "DEFF=")
        j = InStr(i, DATI, Chr(10))
        stmp = Mid(DATI, i, j - i - 1)
        k = InStr(stmp, "=")
        'DiaMolEff = val(Right$(stmp, Len(stmp) - k))
        i = InStr(DATI, "ANGR=")
        j = InStr(i, DATI, Chr(10))
        stmp = Mid(DATI, i, j - i - 1)
        k = InStr(stmp, "=")
        'AngRaccordoTesta = val(Right$(stmp, Len(stmp) - k))
        i = InStr(DATI, "RT=")
        j = InStr(i, DATI, Chr(10))
        stmp = Mid(DATI, i, j - i - 1)
        k = InStr(stmp, "=")
        'RT = val(Right$(stmp, Len(stmp) - k))
        i = InStr(DATI, "RF=")
        j = InStr(i, DATI, Chr(10))
        stmp = Mid(DATI, i, j - i - 1)
        k = InStr(stmp, "=")
        'RF = val(Right$(stmp, Len(stmp) - k))
      'End If
      '
    End If
    ' Modulo, AngP, Diamp, DiamRulli
    If (iPd(1) <> 0) Then
      'PROFILO
      Line Input #1, riga  'COMMENTO
      'TASTATURA SUL DIAMETRO PRIMITIVO PER CALCOLO SPESSORE MISURATO
      i = 0
        Input #1, Pd1f1(i), Pd1f2(i)
        'Calcolo Quota rulli da Spessore normale misurato
        SpeCirNorMis = (PASSO / NZ + Pd1f1(i) - Pd1f2(i)) * Cos((90 - Abs(AngEli)) * PG / 180)
        If (DiamRulli > 0) Then
          If (TipoVite <> "ZA") Then
            ApNormV = AngPreNor
            ApNormVrad = AngPreNor * (PG / 180)
            '
            ApViteRad = AngPreNor * (PG / 180)
          Else
            ApAssV = AngPreNor
            ApAssVRad = ApAssV * (PG / 180)
            '
            ApViteRad = AngPreNor * (PG / 180)
          End If
          Call QR_Vite("W", SpeCirNorMis, QRviteMis, ModNor, AngPreNor * PG / 180, DiamP, NZ, DiamRulli)
        Else
          QRviteMis = 0
        End If
        Pd1f1(i) = Pd1f1(i) - XXf1(i)
        Pd1f2(i) = -Pd1f2(i) + XXf2(i)
      'TASTATURE SUCCESSIVE SUI FIANCHI
      For i = 1 To Np
        Input #1, Pd1f1(i), Pd1f2(i)
        Pd1f1(i) = Pd1f1(i) - XXf1(i)
        Pd1f2(i) = -Pd1f2(i) + XXf2(i)
      Next i
      'AZZERAMENTO ERRORE SUL DIAMETRO PRIMITIVO
      For i = 1 To Np
        Pd1f1(i) = Pd1f1(i) - Pd1f1(0)
        Pd1f2(i) = Pd1f2(i) - Pd1f2(0)
      Next i
      For i = 1 To Np
        Pd0f1(i) = Pd1f1(i)
        Pd0f2(i) = Pd1f2(i)
      Next i
      Call CalcoloFaFHaFFa(1)
      'ELICA
      If (NE > 1) Then
        Line Input #1, riga  'COMMENTO
        'ERRORI
        For i = 1 To NE
          Input #1, Ed1f1(i), Ed1f2(i)
          Ed1f1(i) = Ed1f1(i) - XXf1(0)
          Ed1f2(i) = -Ed1f2(i) + XXf2(0)
        Next i
        'AZZERAMENTO RISPETTO AL PRIMO PUNTO
        For i = 2 To NE
          Ed1f1(i) = Ed1f1(i) - Ed1f1(1)
          Ed1f2(i) = Ed1f2(i) - Ed1f2(1)
        Next i
        Ed1f1(1) = 0
        Ed1f2(1) = 0
        'ASSEGNAZIONE PER CALCOLO
        For i = 1 To NE
          Ed0f1(i) = Ed1f1(i)
          Ed0f2(i) = Ed1f2(i)
        Next i
        Call CalcoloFbFHbFFb(1)
      End If
    End If
    If (iPd(2) <> 0) Then
      'PROFILO
      Line Input #1, riga  'COMMENTO
      For i = 0 To Np
        Input #1, Pd2f1(i), Pd2f2(i)
        Pd2f1(i) = Pd2f1(i) - XXf1(i)
        Pd2f2(i) = -Pd2f2(i) + XXf2(i)
      Next i
      For i = 1 To Np
        Pd2f1(i) = Pd2f1(i) - Pd2f1(0)
        Pd2f2(i) = Pd2f2(i) - Pd2f2(0)
      Next i
      For i = 1 To Np
        Pd0f1(i) = Pd2f1(i)
        Pd0f2(i) = Pd2f2(i)
      Next i
      Call CalcoloFaFHaFFa(2)
      'ELICA
      If (NE > 1) Then
        Line Input #1, riga  'COMMENTO
        For i = 1 To NE
          Input #1, Ed2f1(i), Ed2f2(i)
          Ed2f1(i) = Ed2f1(i) - XXf1(0)
          Ed2f2(i) = -Ed2f2(i) + XXf2(0)
        Next i
        'AZZERAMENTO RISPETTO AL PRIMO PUNTO
        For i = 2 To NE
          Ed2f1(i) = Ed2f1(i) - Ed2f1(1)
          Ed2f2(i) = Ed2f2(i) - Ed2f2(1)
        Next i
        Ed2f1(1) = 0
        Ed2f2(1) = 0
        'ASSEGNAZIONE PER CALCOLO
        For i = 1 To NE
          Ed0f1(i) = Ed2f1(i)
          Ed0f2(i) = Ed2f2(i)
        Next i
        Call CalcoloFbFHbFFb(2)
      End If
    End If
    If (iPd(3) <> 0) Then
      'PROFILO
      Line Input #1, riga  'COMMENTO
      For i = 0 To Np
        Input #1, Pd3f1(i), Pd3f2(i)
        Pd3f1(i) = Pd3f1(i) - XXf1(i)
        Pd3f2(i) = -Pd3f2(i) + XXf2(i)
      Next i
      For i = 1 To Np
        Pd3f1(i) = Pd3f1(i) - Pd3f1(0)
        Pd3f2(i) = Pd3f2(i) - Pd3f2(0)
      Next i
      For i = 1 To Np
        Pd0f1(i) = Pd3f1(i)
        Pd0f2(i) = Pd3f2(i)
      Next i
      Call CalcoloFaFHaFFa(3)
      'ELICA
      If (NE > 1) Then
        Line Input #1, riga  'COMMENTO
        'ERRORI
        For i = 1 To NE
          Input #1, Ed3f1(i), Ed3f2(i)
          Ed3f1(i) = Ed3f1(i) - XXf1(0)
          Ed3f2(i) = -Ed3f2(i) + XXf2(0)
        Next i
        'AZZERAMENTO RISPETTO AL PRIMO PUNTO
        For i = 2 To NE
          Ed3f1(i) = Ed3f1(i) - Ed3f1(1)
          Ed3f2(i) = Ed3f2(i) - Ed3f2(1)
        Next i
        Ed3f1(1) = 0
        Ed3f2(1) = 0
        'ASSEGNAZIONE PER CALCOLO
        For i = 1 To NE
          Ed0f1(i) = Ed3f1(i)
          Ed0f2(i) = Ed3f2(i)
        Next i
        Call CalcoloFbFHbFFb(3)
      End If
    End If
    If (iPd(4) <> 0) Then
      'PROFILO
      Line Input #1, riga  'COMMENTO
      For i = 0 To Np
        Input #1, Pd4f1(i), Pd4f2(i)
        Pd4f1(i) = Pd4f1(i) - XXf1(i)
        Pd4f2(i) = -Pd4f2(i) + XXf2(i)
      Next i
      'AZZERAMENTO ERRORE SUL DIAMETRO PRIMITIVO
      For i = 1 To Np
        Pd4f1(i) = Pd4f1(i) - Pd4f1(0)
        Pd4f2(i) = Pd4f2(i) - Pd4f2(0)
      Next i
      For i = 1 To Np
        Pd0f1(i) = Pd4f1(i)
        Pd0f2(i) = Pd4f2(i)
      Next i
      Call CalcoloFaFHaFFa(4)
      'ELICA
      If (NE > 1) Then
        Line Input #1, riga  'COMMENTO
        For i = 1 To NE
          Input #1, Ed4f1(i), Ed4f2(i)
          Ed4f1(i) = Ed4f1(i) - XXf1(0)
          Ed4f2(i) = -Ed4f2(i) + XXf2(0)
        Next i
        'AZZERAMENTO RISPETTO AL PRIMO PUNTO
        For i = 2 To NE
          Ed4f1(i) = Ed4f1(i) - Ed4f1(1)
          Ed4f2(i) = Ed4f2(i) - Ed4f2(1)
        Next i
        Ed4f1(1) = 0
        Ed4f2(1) = 0
        'ASSEGNAZIONE PER CALCOLO
        For i = 1 To NE
          Ed0f1(i) = Ed4f1(i)
          Ed0f2(i) = Ed4f2(i)
        Next i
        Call CalcoloFbFHbFFb(4)
      End If
    End If
    'PASSO ASSIALE
    If (DivCon > 0) Then
      Line Input #1, riga  'COMMENTO
      i = 1
        Input #1, Divf1(i), Divf2(i)
        HxD(i) = (i - 0.5) * Box1W / DivCon
        'ERRORE RELATIVO AL PRIMITIVO:     SIGNIFICATO DEI SEGNI SUL PEZZO
        Divf1(i) = (Divf1(i) - XXf1(0))   'Divf1 > 0 --> SPESSORE GRANDE
        Divf2(i) = -(Divf2(i) - XXf2(0))  'Divf2 > 0 --> SPESSORE GRANDE
        'CAMBIO SEGNO
        Divf1(i) = -Divf1(i)
        Divf2(i) = -Divf2(i)
      For i = 2 To DivCon
        Input #1, Divf1(i), Divf2(i)
        HxD(i) = (i - 0.5) * Box1W / DivCon
        'ERRORE RELATIVO AL PRIMITIVO:     SIGNIFICATO DEI SEGNI SUL PEZZO
        Divf1(i) = (Divf1(i) - XXf1(0))   'Divf1 > 0 --> SPESSORE GRANDE
        Divf2(i) = -(Divf2(i) - XXf2(0))  'Divf2 > 0 --> SPESSORE GRANDE
        'TOLGO L'ERRORE DI SPESSORE
        Divf1(i) = Divf1(i) + Divf1(1)
        Divf2(i) = Divf2(i) + Divf2(1)
        'CAMBIO SEGNO
        Divf1(i) = -Divf1(i)
        Divf2(i) = -Divf2(i)
      Next i
      Divf1(1) = 0
      Divf2(1) = 0
    End If
  Close #1
  LeggiFile_VITI = True
  '
Exit Function

errLeggiFile_VITI:
  Close
  LeggiFile_VITI = False
  WRITE_DIALOG "SUB LeggiFile_VITI ERROR " & str(Err)
  Exit Function
    
End Function

Sub MODIFICA_TOLLERANZA_CONTROLLO(ByVal TIPO_LAVORAZIONE As String)
'
Dim DBB                 As Database
Dim RSS                 As Recordset
Dim pXX(2, 9)           As Double
Dim pYY(2, 9)           As Double
Dim pBB(2, 9)           As Double
Dim k1                  As Long
Dim k2                  As Long
Dim k3                  As Long
Dim k4                  As Long
Dim rsp                 As Long
Dim CODICE_PEZZO        As String
Dim lstTOLLERANZA       As String
Dim sTIPO_LAVORAZIONE   As String
Dim TABELLA_TOLLERANZA  As String
Dim stmp                As String
Dim ftmp                As Double
'
On Error Resume Next
  '
  'TASTO FUNZIONE SEMPRE ABILITATO PER LA RETTIFICA DEI ROTORI
  If (TIPO_LAVORAZIONE = "ROTORI_ESTERNI") Then
    StopRegieEvents
    rsp = MsgBox(" Standard Tollerance ?", vbYesNo, "TOLLERANCE EDITING")
    ResumeRegieEvents
    If (rsp = vbYes) Then
      Call MODIFICA_FASCIA_DI_TOLLERANZA_ROTORI
    Else
      Call FASCIA_DI_TOLLERANZA_VITI
    End If
    Exit Sub
  End If
  '
  'LETTURA DELL'ABILITAZIONE DELLA TOLLERANZA PROFILO
  stmp = GetInfo("CHK0", "TOLLERANZA_PROFILO", Path_LAVORAZIONE_INI): stmp = UCase$(Trim$(stmp))
  If (stmp <> "Y") Then WRITE_DIALOG "SOFTKEY DISENABLED!!": Exit Sub
  'LETTURA DELL'ABILITAZIONE DELLA TOLLERANZA ELICA
  stmp = GetInfo("CHK0", "TOLLERANZA_ELICA", Path_LAVORAZIONE_INI): stmp = UCase$(Trim$(stmp))
  If (stmp <> "Y") Then WRITE_DIALOG "SOFTKEY DISENABLED!!": Exit Sub
  '
  Select Case CHK0.Combo1.ListIndex
    '
    Case 0    'PROFILO
      TABELLA_TOLLERANZA = "TOLLERANZA_PROFILO"
      CHK0.ChkBOMBATURA.Value = val(GetInfo("CHK0", "BOMBATURA_PROFILO", Path_LAVORAZIONE_INI))
      '
    Case 1    'ELICA
      TABELLA_TOLLERANZA = "TOLLERANZA_ELICA"
      CHK0.ChkBOMBATURA.Value = val(GetInfo("CHK0", "BOMBATURA_ELICA", Path_LAVORAZIONE_INI))
      '
    Case 2, 3 'fp e Fp
      stmp = GetInfo("CHK0", "Tolleranza_DIV(1)", Path_LAVORAZIONE_INI): stmp = Trim$(stmp)
      StopRegieEvents
      stmp = InputBox(CHK0.Combo1.Text & " TOLL. ", "INPUT", stmp)
      stmp = Trim$(stmp)
      ResumeRegieEvents
      If (Len(stmp) > 0) Then
        rsp = WritePrivateProfileString("CHK0", "Tolleranza_DIV(1)", stmp, Path_LAVORAZIONE_INI)
        rsp = WritePrivateProfileString("CHK0", "Tolleranza_DIV(2)", stmp, Path_LAVORAZIONE_INI)
        Call VISUALIZZA_CONTROLLO
      Else
        WRITE_DIALOG "Operation aborted by user!!"
      End If
      Exit Sub
      '
    Case 4    'fr
      stmp = GetInfo("CHK0", "Tolleranza_CON", Path_LAVORAZIONE_INI): stmp = Trim$(stmp)
      StopRegieEvents
      stmp = InputBox(CHK0.Combo1.Text & " TOLL. ", "INPUT", stmp)
      stmp = Trim$(stmp)
      ResumeRegieEvents
      If (Len(stmp) > 0) Then
        rsp = WritePrivateProfileString("CHK0", "Tolleranza_CON", stmp, Path_LAVORAZIONE_INI)
        Call VISUALIZZA_CONTROLLO
      Else
        WRITE_DIALOG "Operation aborted by user!!"
      End If
      Exit Sub
      '
  End Select
  '
  Select Case TIPO_LAVORAZIONE
    '
    Case "VITI_ESTERNE", "VITIV_ESTERNE"
      Open PathMIS & "\" & "DEFAULT" For Input As 1
        For i = 1 To 6
          Line Input #1, CODICE_PEZZO
        Next i
      Close #1
      CODICE_PEZZO = UCase$(Trim$(CODICE_PEZZO))
      '
    Case Else
      'STRUTTURA DEL FILE DEI RISULTATI DEL CONTROLLO INGRANAGGI
      Open PathMIS & "\" & "DEFAULT" For Input As 1
        Line Input #1, stmp
        Line Input #1, stmp
        Line Input #1, stmp
        Input #1, ftmp, rsp  '0=SERIE10, 1=840D 2=840D CON TOLLERANZE
        If (rsp = 2) Then
          'CODICE DEL PEZZO SALVATO NEL FILE DI CONTROLLO
          Input #1, k1, ftmp, k1, k1, k1, k1, CODICE_PEZZO
        Else
          'CODICE DEL PEZZO COME IL NOME DEL FILE DI CONTROLLO
          CODICE_PEZZO = GetInfo("CHK0", "FileMisura", Path_LAVORAZIONE_INI): CODICE_PEZZO = UCase$(Trim$(CODICE_PEZZO))
          'LEGGO COMUNQUE FINO AL PRIMO SPAZIO
          rsp = InStr(CODICE_PEZZO, " "): If rsp > 0 Then CODICE_PEZZO = Left$(CODICE_PEZZO, rsp - 1)
          'LEGGO COMUNQUE FINO AL PRIMO PUNTO
          rsp = InStr(CODICE_PEZZO, "."): If rsp > 0 Then CODICE_PEZZO = Left$(CODICE_PEZZO, rsp - 1)
        End If
      Close #1
      CODICE_PEZZO = UCase$(Trim$(CODICE_PEZZO))
      '
  End Select
  '
  sTIPO_LAVORAZIONE = RICERCA_NOME_TIPO(TIPO_LAVORAZIONE)
  Set DBB = OpenDatabase(PathDB, True, False)
  Set RSS = DBB.OpenRecordset("ARCHIVIO_" & TIPO_LAVORAZIONE)
  RSS.Index = "NOME_PEZZO"
  RSS.Seek "=", CODICE_PEZZO
  If Not RSS.NoMatch Then
    If (CHK0.fraTOLLERANZA.Visible = True) And (CHK0.lblARCHIVIO.Visible = False) Then
      CHK0.fraTOLLERANZA.Visible = False
      CHK0.txtPBBF1(7).Text = "0"
      CHK0.txtPBBF2(7).Text = "0"
      CHK0.Refresh
      'CREAZIONE DELLA STRINGA PER IL CAMPO
      lstTOLLERANZA = ""
      For j = 1 To 8
        lstTOLLERANZA = lstTOLLERANZA & frmt(val(CHK0.txtPXXF1(j - 1).Text), 8) & ";"
        lstTOLLERANZA = lstTOLLERANZA & frmt(val(CHK0.txtPYYF1(j - 1).Text), 8) & ";"
        lstTOLLERANZA = lstTOLLERANZA & frmt(val(CHK0.txtPBBF1(j - 1).Text), 8) & ";"
      Next j
      For j = 1 To 8
        lstTOLLERANZA = lstTOLLERANZA & frmt(val(CHK0.txtPXXF2(j - 1).Text), 8) & ";"
        lstTOLLERANZA = lstTOLLERANZA & frmt(val(CHK0.txtPYYF2(j - 1).Text), 8) & ";"
        lstTOLLERANZA = lstTOLLERANZA & frmt(val(CHK0.txtPBBF2(j - 1).Text), 8) & ";"
      Next j
      'SALVATAGGIO DEI VALORI NEL CAMPO
      RSS.Edit
      RSS.Fields(TABELLA_TOLLERANZA).Value = lstTOLLERANZA
      RSS.Update
      'CHIUSURA TABELLA E DATABASE
      RSS.Close
      DBB.Close
    Else
      'LETTURA PER MODIFICA
      If Len(RSS.Fields(TABELLA_TOLLERANZA).Value) > 0 Then
        lstTOLLERANZA = UCase$(Trim$(RSS.Fields(TABELLA_TOLLERANZA).Value))
      Else
        lstTOLLERANZA = ""
      End If
      'CHIUSURA TABELLA E DATABASE
      RSS.Close
      DBB.Close
      'VISUALIZZAZIONE CAMPI DI MODIFICA
      CHK0.fraTOLLERANZA.Move CHK0.Picture1.Left, CHK0.Picture1.Top, CHK0.Picture1.Width, CHK0.Picture1.Height
      '
      CHK0.lblTOLLERANZA.Left = 50
      CHK0.lblTOLLERANZA.Top = 0
      CHK0.lblTOLLERANZA.Width = 7400
      '
      For i = 0 To 7
        CHK0.IF1(i).Caption = " p" & Format$(i + 1, "0")
        CHK0.IF2(i).Caption = " p" & Format$(i + 1, "0")
      Next i
      CHK0.fraTOLLERANZA.Caption = " " & CHK0.Combo1.Text & " TOLL. --> " & CODICE_PEZZO
      CHK0.lblTOLLERANZA.Caption = " " & CHK0.Combo1.Text & " TOLL. --> " & CODICE_PEZZO
      CHK0.Image3.Visible = False
      CHK0.lstARCHIVIO.Visible = False
      CHK0.lblARCHIVIO.Visible = False
      CHK0.fraFIANCO1.Visible = True
      CHK0.fraFIANCO2.Visible = True
      CHK0.ChkBOMBATURA.Visible = True
      CHK0.fraTOLLERANZA.Visible = True
      '
      'RICAVO I SINGOLI VALORI DALLA LISTA
      If (Len(lstTOLLERANZA) > 0) Then
        k1 = InStr(lstTOLLERANZA, ";;")
        If (k1 <= 0) Then
          k1 = InStr(lstTOLLERANZA, ";")
          k2 = InStr(k1 + 1, lstTOLLERANZA, ";")
          k3 = InStr(k2 + 1, lstTOLLERANZA, ";")
          k4 = 1
          For j = 1 To 8
            CHK0.txtPXXF1(j - 1).Text = Mid$(lstTOLLERANZA, k4, k1 - k4)
            CHK0.txtPYYF1(j - 1).Text = Mid$(lstTOLLERANZA, k1 + 1, k2 - k1 - 1)
            CHK0.txtPBBF1(j - 1).Text = Mid$(lstTOLLERANZA, k2 + 1, k3 - k2 - 1)
            k4 = k3 + 1
            k1 = InStr(k4, lstTOLLERANZA, ";")
            k2 = InStr(k1 + 1, lstTOLLERANZA, ";")
            k3 = InStr(k2 + 1, lstTOLLERANZA, ";")
          Next j
          For j = 1 To 8
            CHK0.txtPXXF2(j - 1).Text = Mid$(lstTOLLERANZA, k4, k1 - k4)
            CHK0.txtPYYF2(j - 1).Text = Mid$(lstTOLLERANZA, k1 + 1, k2 - k1 - 1)
            CHK0.txtPBBF2(j - 1).Text = Mid$(lstTOLLERANZA, k2 + 1, k3 - k2 - 1)
            k4 = k3 + 1
            k1 = InStr(k4, lstTOLLERANZA, ";")
            k2 = InStr(k1 + 1, lstTOLLERANZA, ";")
            k3 = InStr(k2 + 1, lstTOLLERANZA, ";")
          Next j
        End If
      Else
        For j = 1 To 8
          CHK0.txtPXXF1(j - 1).Text = "0"
          CHK0.txtPYYF1(j - 1).Text = "0"
          CHK0.txtPBBF1(j - 1).Text = "0"
        Next j
        For j = 1 To 8
          CHK0.txtPXXF2(j - 1).Text = "0"
          CHK0.txtPYYF2(j - 1).Text = "0"
          CHK0.txtPBBF2(j - 1).Text = "0"
        Next j
      End If
    End If
    'VISUALIZZAZIONE
    Call VISUALIZZA_CONTROLLO
  Else
    'CHIUSURA TABELLA E DATABASE
    RSS.Close
    DBB.Close
    If CHK0.fraTOLLERANZA.Visible = True Then
      CHK0.fraTOLLERANZA.Visible = False
    Else
      'SEGNALAZIONE ERRORE
      StopRegieEvents
      stmp = "HD: " & sTIPO_LAVORAZIONE & Chr(13) & Chr(13)
      stmp = stmp & " TOLL. " & CHK0.Combo1.Text & ": " & CODICE_PEZZO & " NOT FOUND!!!" & Chr(13) & Chr(13)
      stmp = stmp & ">>> SELECT ASSIGNMENT!! <<<"
      If (rsp = 2) Then
        MsgBox stmp, 64, "WARNING: key FROM FILE"
      Else
        MsgBox stmp, 64, "WARNING: key FROM SECTION FileMisura"
      End If
      ResumeRegieEvents
      'ASSOCIAZIONE DISEGNO A CONTROLLO VECCHIO PER SALVTAGGIO TOLLERANZE
      'VISUALIZZAZIONE CAMPI DI MODIFICA
      CHK0.fraTOLLERANZA.Move CHK0.Picture1.Left, CHK0.Picture1.Top, CHK0.Picture1.Width + CHK0.File1.Width, CHK0.Picture1.Height
      '
      CHK0.lblTOLLERANZA.Left = 50
      CHK0.lblTOLLERANZA.Top = 0
      CHK0.lblTOLLERANZA.Width = 7400
      '
      CHK0.Image3.Move CHK0.fraFIANCO2.Left, CHK0.fraFIANCO2.Top
      '
      CHK0.lblARCHIVIO.Left = CHK0.fraFIANCO2.Left + CHK0.Image3.Width
      CHK0.lblARCHIVIO.Top = CHK0.fraFIANCO2.Top
      CHK0.lblARCHIVIO.Width = 7800
      CHK0.lblARCHIVIO.Height = CHK0.Image3.Height
      '
      CHK0.lblARCHIVIO.BackColor = QBColor(12)
      CHK0.lblARCHIVIO.ForeColor = QBColor(15)
      '
      CHK0.lstARCHIVIO.Left = CHK0.fraFIANCO2.Left
      CHK0.lstARCHIVIO.Top = CHK0.fraFIANCO2.Top + CHK0.Image3.Height
      CHK0.lstARCHIVIO.Width = CHK0.lblARCHIVIO.Width + CHK0.Image3.Width
      '
      For i = 0 To 7
        CHK0.IF1(i).Caption = " p" & Format$(i + 1, "0")
        CHK0.IF2(i).Caption = " p" & Format$(i + 1, "0")
      Next i
      '
      CHK0.fraTOLLERANZA.Caption = " " & CHK0.Combo1.Text & " TOLL. --> " & CODICE_PEZZO
      CHK0.lblTOLLERANZA.Caption = " " & CHK0.Combo1.Text & " TOLL. --> " & CODICE_PEZZO
      '
      CHK0.Image3.Visible = True
      CHK0.lstARCHIVIO.Visible = True
      CHK0.lblARCHIVIO.Visible = True
      CHK0.fraFIANCO1.Visible = False
      CHK0.fraFIANCO2.Visible = False
      CHK0.ChkBOMBATURA.Visible = False
      CHK0.fraTOLLERANZA.Visible = True
      For j = 1 To 8
        CHK0.txtPXXF1(j - 1).Text = "0"
        CHK0.txtPYYF1(j - 1).Text = "0"
        CHK0.txtPBBF1(j - 1).Text = "0"
      Next j
      For j = 1 To 8
        CHK0.txtPXXF2(j - 1).Text = "0"
        CHK0.txtPYYF2(j - 1).Text = "0"
        CHK0.txtPBBF2(j - 1).Text = "0"
      Next j
      'LEGGO I NOMI DEI PEZZI SALVATI NELL'ARCHIVIO
      Set DBB = OpenDatabase(PathDB, True, False)
      Set RSS = DBB.OpenRecordset("ARCHIVIO_" & TIPO_LAVORAZIONE)
      'PULISCO LA LISTA
      CHK0.lstARCHIVIO.Clear
      Do Until RSS.EOF
        'AGGIUNGO IL NOME
        Call CHK0.lstARCHIVIO.AddItem(RSS.Fields("NOME_PEZZO").Value)
        'SPOSTO IL PUNTATORE AL RECORD SUCCESSIVO
        RSS.MoveNext
      Loop
      RSS.Close
      DBB.Close
    End If
    CHK0.lstARCHIVIO.ListIndex = 0
    CHK0.lstARCHIVIO.SetFocus
  End If

End Sub

Sub VisELICA(X As Control)
'
Dim SiSTIMA_BOMBATURA As Integer
Dim SiGriglia1        As Integer
Dim SiGriglia2        As Integer
Dim Sc1X              As Single
Dim Sc1Y              As Single
Dim k                 As Integer
Dim stmp              As String
Dim SiDisegna         As Integer
Dim MLeft             As Single
Dim MTop              As Single
Dim DBB               As Database
Dim RSS               As Recordset
Dim pXX(2, 9)         As Double
Dim pYY(2, 9)         As Double
Dim pBB(2, 9)         As Double
Dim k1                As Long
Dim k2                As Long
Dim k3                As Long
Dim k4                As Long
Dim rsp               As Long
Dim CODICE_PEZZO      As String
Dim lstTOLLERANZA     As String
Dim TIPO_LAVORAZIONE  As String
Dim ftmp              As Double
Dim AM                As Double
Dim BM                As Double
Dim RM                As Double
Dim ANGOLO            As Double
Dim Bombatura(5)      As Double
Dim Abgn              As Double
Dim Aend              As Double
Dim Hinf              As Single
Dim FASCIA_RIDOTTA    As Single
Dim RRB_cur           As Double
Dim RRB_max           As Double
Dim RRB_min           As Double
Dim fbMAX(2)          As Double
Dim ffbMAX(2)         As Double
Dim fhbMAX(2)         As Double
Dim BombaturaMAX      As Double
Dim M                 As Double
'
On Error GoTo errVisELICA
  '
  SiGriglia1 = val(GetInfo("CHK0", "Griglia1", Path_LAVORAZIONE_INI))
  SiGriglia2 = val(GetInfo("CHK0", "Griglia2", Path_LAVORAZIONE_INI))
  '
  SiSTIMA_BOMBATURA = val(GetInfo("CHK0", "BOMBATURA_ELICA", Path_LAVORAZIONE_INI))
  stmp = GetInfo("CHK0", "TOLLERANZA_ELICA", Path_LAVORAZIONE_INI): stmp = UCase$(Trim$(stmp))
  If (stmp <> "Y") Then SiSTIMA_BOMBATURA = 0
  '
  MLeft = val(GetInfo("CHK0", "MLeft", Path_LAVORAZIONE_INI))
  MTop = val(GetInfo("CHK0", "MTop", Path_LAVORAZIONE_INI))
  stmp = GetInfo("CHK0", "ScalaELICAx", Path_LAVORAZIONE_INI): Sc1X = val(stmp)
  stmp = GetInfo("CHK0", "ScalaELICAy", Path_LAVORAZIONE_INI): Sc1Y = val(stmp)
  '
  X1 = MLeft + 2: Y1 = (MTop - 0.5 + 1) + 15
  X.DrawWidth = 2
  If (AngEli >= 0) Then
    'SEGNO -
    X.Circle (X1, Y1), 0.25, QBColor(0)
    X.Line (X1 - 0.15, Y1)-(X1 + 0.15, Y1)
  Else
    'SEGNO +
    X.Circle (X1, Y1), 0.25, QBColor(0)
    X.Line (X1, Y1 + 0.15)-(X1, Y1 - 0.15)
    X.Line (X1 - 0.15, Y1)-(X1 + 0.15, Y1)
  End If
  X1 = MLeft + 18: Y1 = (MTop - 0.5 + 1) + 15
  If (AngEli >= 0) Then
    'SEGNO +
    X.Circle (X1, Y1), 0.25, QBColor(0)
    X.Line (X1, Y1 + 0.15)-(X1, Y1 - 0.15)
    X.Line (X1 - 0.15, Y1)-(X1 + 0.15, Y1)
  Else
    'SEGNO -
    X.Circle (X1, Y1), 0.25, QBColor(0)
    X.Line (X1 - 0.15, Y1)-(X1 + 0.15, Y1)
  End If
  X.DrawWidth = 1
  'Sinistra e Destra per zona grafica ELICA
  X1 = MLeft + 2: Y1 = (MTop - 0.5 + 1) + 15 + 5 - 3 - 1
  X.CurrentX = X1 - iCl / 2: X.CurrentY = Y1 - iRG / 2
  X.Print DESTRA
  X1 = MLeft + 2: Y1 = (MTop - 0.5 + 1) + 15 + 5 + 3
  X.CurrentX = X1 - iCl / 2: X.CurrentY = Y1 - iRG / 2
  X.Print SINISTRA
  If (SiGriglia1 = 1) Then
    X1 = MLeft + 3: Y1 = MTop - 0.5 + 16
    Call Disegna_Griglia1(X, CDbl(X1), CDbl(Y1))
  End If
  If (SiGriglia2 = 1) Then
    X1 = MLeft + 3: Y1 = MTop - 0.5 + 16
    Call Disegna_Griglia2(X, CDbl(X1), CDbl(Y1))
  End If
  X1 = MLeft + 3: Y1 = MTop - 0.5 + 16
  X1 = X1 + Box1W / 2: Y1 = Y1
  TESTO = comVisPROELI(1, 2)
  X.CurrentX = X1 - X.TextWidth(TESTO) / 2
  X.CurrentY = Y1 - iRG
  X.FontBold = True
  X.Print TESTO
  X.FontBold = False
  '
  If (SiOpt(4, 1) = 1) Then
    X.DrawStyle = 1
    X1 = MLeft + 3: Y1 = MTop - 0.5 + 16
    X1 = X1 + Box1W / 2: Y1 = Y1
    X2 = X1: Y2 = Y1 + Box1H
    X.Line (X1, Y1)-(X2, Y2), QBColor(0)
    X1 = MLeft + 3: Y1 = MTop - 0.5 + 16
    X1 = X1: Y1 = Y1 + Box1H / 2
    X2 = X1 + Box1W: Y2 = Y1
    X.Line (X1, Y1)-(X2, Y2), QBColor(0)
    X.DrawStyle = 0
  End If
  '
  If (NE <> 0) Then
    FASCIA_RIDOTTA = (HyE(CHK_Fin_ELICA) - HyE(CHK_Ini_ELICA))
    Hinf = FASCIA_RIDOTTA * Sc1Y / 10 / 2
    X1 = MLeft + 3 + Box1W / 4: Y1 = MTop - 0.5 + 16
    'VISUALIZZAZIONE QUOTA RULLI
    If (Len(sQUOTA_RULLI) > 0) And (Np <= 0) Then
      X.ForeColor = QBColor(12)
      TESTO = TESTO & " " & sQUOTA_RULLI
      X.CurrentX = X1 - X.TextWidth(TESTO) / 2 + X.TextWidth("F2 ")
      X.CurrentY = Y1 - iRG
      X.Print sQUOTA_RULLI
    End If
    X.ForeColor = QBColor(0)
    X.CurrentX = X1 - X.TextWidth(TESTO) / 2
    X.CurrentY = Y1 - iRG
    X.FontBold = True
    X.Print "F2"
    X.FontBold = False
    If (SiOpt(4, 1) = 1) Then
        k = 1
          X1 = (MLeft + 3 + k * (Box1W / 10))
          Y1 = (MTop - 0.5 + 16 + Box1H / 2)
          Y1 = Y1 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
          Y1 = Y1 - (HyE(1) - HyE(1)) * Sc1Y / 10
          X2 = (MLeft + 3 + k * (Box1W / 10))
          Y2 = (MTop - 0.5 + 16 + Box1H / 2)
          Y2 = Y2 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
          Y2 = Y2 - (HyE(NE) - HyE(1)) * Sc1Y / 10
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
          X1 = (MLeft + 3 + Box1W / 10) - 0.5
          X2 = X1 + 1
          Y1 = (MTop - 0.5 + 16 + Box1H / 2) + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
          Y2 = Y1
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
          X1 = (MLeft + 3 + Box1W / 10) - 0.5
          X2 = X1 + 1
          Y1 = (MTop - 0.5 + 16 + Box1H / 2) - (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
          Y2 = Y1
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
      For k = 2 To 4
        If (iEd(k) <> 0) Then
          X1 = (MLeft + 3 + k * (Box1W / 10))
          Y1 = (MTop - 0.5 + 16 + Box1H / 2)
          Y1 = Y1 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
          Y1 = Y1 - (HyE(1) - HyE(1)) * Sc1Y / 10
          X2 = (MLeft + 3 + k * (Box1W / 10))
          Y2 = (MTop - 0.5 + 16 + Box1H / 2)
          Y2 = Y2 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
          Y2 = Y2 - (HyE(NE) - HyE(1)) * Sc1Y / 10
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
          X1 = (MLeft + 3 + k * (Box1W / 10)) - 0.5
          X2 = X1 + 1
          Y1 = (MTop - 0.5 + 16 + Box1H / 2) + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
          Y2 = Y1
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
          X1 = (MLeft + 3 + k * (Box1W / 10)) - 0.5
          X2 = X1 + 1
          Y1 = (MTop - 0.5 + 16 + Box1H / 2) - (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
          Y2 = Y1
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
        End If
      Next
    End If
    'RICERCA MASSIMI
    For k = 1 To 4
      If (fbMAX(1) <= Abs(fbF1d(k))) Then fbMAX(1) = Abs(fbF1d(k))
      If (fbMAX(2) <= Abs(fbF2d(k))) Then fbMAX(2) = Abs(fbF2d(k))
      If (ffbMAX(1) <= Abs(ffbF1d(k))) Then ffbMAX(1) = Abs(ffbF1d(k))
      If (ffbMAX(2) <= Abs(ffbF2d(k))) Then ffbMAX(2) = Abs(ffbF2d(k))
      If (fhbMAX(1) <= Abs(fhbF1d(k))) Then fhbMAX(1) = Abs(fhbF1d(k))
      If (fhbMAX(2) <= Abs(fhbF2d(k))) Then fhbMAX(2) = Abs(fhbF2d(k))
    Next k
    For k = 1 To 4
      SiDisegna = False
      Select Case k
        Case 1
          For i = 1 To NE: Ed0f2(i) = Ed1f2(i): Next i
            GoSub STIMA_BOMBATURA_ELICA_FIANCO2
            SiDisegna = True
        Case 2
          For i = 1 To NE: Ed0f2(i) = Ed2f2(i): Next i
          If (iEd(2) <> 0) Then
            GoSub STIMA_BOMBATURA_ELICA_FIANCO2
            SiDisegna = True
          End If
        Case 3
          For i = 1 To NE: Ed0f2(i) = Ed3f2(i): Next i
          If (iEd(3) <> 0) Then
            GoSub STIMA_BOMBATURA_ELICA_FIANCO2
            SiDisegna = True
          End If
        Case 4
          For i = 1 To NE: Ed0f2(i) = Ed4f2(i): Next i
          If (iEd(4) <> 0) Then
            GoSub STIMA_BOMBATURA_ELICA_FIANCO2
            SiDisegna = True
          End If
      End Select
      If SiDisegna Then
        For i = 1 To NE
          X1 = (MLeft + 3 + k * Box1W / 10)
          X1 = X1 - (Ed0f2(i) - MV(Ed0f2, CHK_Ini_ELICA, CHK_Fin_ELICA)) * Sc1X / 10
          Y1 = (MTop - 0.5 + 16 + Box1H / 2)
          Y1 = Y1 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
          Y1 = Y1 - (HyE(i) - HyE(1)) * Sc1Y / 10
          'If (SiOpt(0, 1) = 1) Then
          '  R1 = 0.025
          '  X.Circle (X1, Y1), R1, QBColor(0)
          'End If
          'If (SiOpt(1, 1) = 1) Then
          '  X.Line ((MLeft + 3 + k * Box1W / 10), Y1)-(X1, Y1), QBColor(0)
          'End If
          If (i < NE) And (SiOpt(2, 1) = 1) Then
            X2 = (MLeft + 3 + k * Box1W / 10)
            X2 = X2 - (Ed0f2(i + 1) - MV(Ed0f2, CHK_Ini_ELICA, CHK_Fin_ELICA)) * Sc1X / 10
            Y2 = (MTop - 0.5 + 16 + Box1H / 2)
            Y2 = Y2 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
            Y2 = Y2 - (HyE(i + 1) - HyE(1)) * Sc1Y / 10
            X.Line (X1, Y1)-(X2, Y2), QBColor(0)
          End If
        Next i
        X1 = (MLeft + 3 + k * (Box1W / 10))
        Y1 = (MTop - 0.5 + 16 + Box1H)
        TESTO = Format$(iEd(k), "#")
        X.CurrentX = X1 - X.TextWidth(TESTO) / 2
        X.CurrentY = Y1
        X.Print TESTO
        j = 0
        X1 = (MLeft + 3 + k * (Box1W / 10))
        Y1 = MTop - 0.5 + 25.5 + j * iRG
        TESTO = frmt(fbF2d(k) * 1000, 1)
        X.CurrentX = X1 - X.TextWidth(TESTO) / 2 '+ ICl
        X.CurrentY = Y1
        X.FontBold = (Abs(fbF2d(k)) = fbMAX(2))
        X.Print TESTO
        X.FontBold = False
        j = 1
        X1 = (MLeft + 3 + k * (Box1W / 10))
        Y1 = MTop - 0.5 + 25.5 + j * iRG
        TESTO = frmt(ffbF2d(k) * 1000, 1)
        X.CurrentX = X1 - X.TextWidth(TESTO) / 2 '+ ICl
        X.CurrentY = Y1
        X.FontBold = (Abs(ffbF2d(k)) = ffbMAX(2))
        X.Print TESTO
        X.FontBold = False
        j = 2
        X1 = (MLeft + 3 + k * (Box1W / 10))
        Y1 = MTop - 0.5 + 25.5 + j * iRG
        TESTO = frmt(fhbF2d(k) * 1000, 1)
        X.CurrentX = X1 - X.TextWidth(TESTO) / 2 '+ ICl
        X.CurrentY = Y1
        X.FontBold = (Abs(fhbF2d(k)) = fhbMAX(2))
        X.Print TESTO
        X.FontBold = False
      End If
    Next k
    '
    If (SiSTIMA_BOMBATURA = 1) Then
      BombaturaMAX = 0
      For k = 1 To 4
        If (iEd(k) <> 0) Then
          If (BombaturaMAX <= Abs(Bombatura(k))) Then BombaturaMAX = Abs(Bombatura(k))
        End If
      Next k
      For k = 1 To 4
        If (iEd(k) <> 0) Then
          j = 3
          X1 = (MLeft + 3 + k * (Box1W / 10))
          Y1 = MTop - 0.5 + 25.5 + j * iRG
          TESTO = frmt(Abs(Bombatura(k)) * 1000, 1)
          X.CurrentX = X1 - X.TextWidth(TESTO) / 2
          X.CurrentY = Y1
          X.FontBold = (Abs(Bombatura(k)) = BombaturaMAX)
          X.Print TESTO
          X.FontBold = False
        End If
      Next k
    End If
    '
    For j = 0 To 2
      X1 = MLeft
      Y1 = MTop - 0.5 + 25.5 + j * iRG
      TESTO = comVisPROELI(4 + j, 2)
      X.CurrentX = X1 + (1.5 - X.TextWidth(TESTO)) / 2
      X.CurrentY = Y1
      X.Print TESTO
      X.Line (X1, Y1 + iRG)-(X1 + Box0W, Y1 + iRG), QBColor(0)
    Next j
    '
    'VISUALIZZAZIONE DELLE TOLLERANZE DIN
    j = -1
    X1 = MLeft + 3
    Y1 = MTop - 0.5 + 25.5 + j * iRG
    X.Line (X1 + Box1W / 2, Y1 + iRG)-(X1 + Box1W / 2, Y1 + 4 * iRG)
    If (FASCIA_RIDOTTA < FasCon) Then
      TESTO = "Act.(" & Format(FASCIA_RIDOTTA / FasCon * 100, "##0") & "%)"
    Else
      TESTO = "Act."
    End If
    X.CurrentX = X1 + (Box1W - X.TextWidth(TESTO)) / 2
    X.CurrentY = (MTop - 0.5 + 16 + Box1H)
    X.Font.Bold = True
    X.Print TESTO
    X.Font.Bold = False
    For j = 0 To 2
      X.Font.Bold = True
      X1 = MLeft + 3
      Y1 = MTop - 0.5 + 25.5 + j * iRG
      'DIN F2
      Select Case j
        Case 0:    TESTO = DIN_Fb(FASCIA_RIDOTTA, MaxVett(fbF2d, iEd))
        Case 1:    TESTO = DIN_ffb(FASCIA_RIDOTTA, MaxVett(ffbF2d, iEd))
        Case 2:    TESTO = DIN_fhb(FASCIA_RIDOTTA, MaxVett(fhbF2d, iEd))
        Case Else: TESTO = "-"
      End Select
      X.CurrentX = X1 - X.TextWidth(TESTO & " ") + (Box1W) / 2
      X.CurrentY = Y1
      X.Print TESTO
      X1 = MLeft + 3
      Y1 = MTop - 0.5 + 25.5 + j * iRG
      'DIN F1
      Select Case j
        Case 0:    TESTO = DIN_Fb(FASCIA_RIDOTTA, MaxVett(fbF1d, iEd))
        Case 1:    TESTO = DIN_ffb(FASCIA_RIDOTTA, MaxVett(ffbF1d, iEd))
        Case 2:    TESTO = DIN_fhb(FASCIA_RIDOTTA, MaxVett(fhbF1d, iEd))
        Case Else: TESTO = "-"
      End Select
      X.CurrentX = X1 + X.TextWidth(" ") + (Box1W) / 2
      X.CurrentY = Y1
      X.Print TESTO
      X.Font.Bold = False
    Next j
    '
    If (SiSTIMA_BOMBATURA = 1) Then
      j = 3
        X1 = MLeft
        Y1 = MTop - 0.5 + 25.5 + j * iRG
        TESTO = "Cb" 'comVisPROELI(7, 2)
        X.CurrentX = X1 + (1.5 - X.TextWidth(TESTO)) / 2
        X.CurrentY = Y1
        X.Print TESTO
        X.Line (X1, Y1 + iRG)-(X1 + Box0W, Y1 + iRG), QBColor(0)
    End If
    '
    X1 = (MLeft + 3 + Box1W / 10) - 1
    X2 = X1 + 2
    Y1 = (MTop - 0.5 + 16 + Box1H / 2) + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
    Y2 = Y1
    'x.Line (X1, Y1)-(X2, Y2), QBColor(0)
    '
    TESTO = frmt(-FasCon / 2, 3)
    X.CurrentX = X1 - X.TextWidth(TESTO) - iCl
    X.CurrentY = Y1 - iRG / 2
    X.Print TESTO
    '
    X1 = (MLeft + 3 + Box1W / 10) - 1
    X2 = X1 + 2
    Y1 = (MTop - 0.5 + 16 + Box1H / 2) - (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
    Y2 = Y1
    'x.Line (X1, Y1)-(X2, Y2), QBColor(0)
    '
    TESTO = frmt(FasCon / 2, 3)
    X.CurrentX = X1 - X.TextWidth(TESTO) - iCl
    X.CurrentY = Y1 - iRG / 2
    X.Print TESTO
    '
    If FASCIA_RIDOTTA < FasCon Then
      X1 = (MLeft + 3 + Box1W / 10) - 1
      X2 = X1 + 2
      Y1 = (MTop - 0.5 + 16 + Box1H / 2) + Hinf
      Y2 = Y1
      X.Line (X1, Y1)-(X2, Y2), QBColor(12)
      '
      TESTO = frmt(-FASCIA_RIDOTTA / 2, 3)
      X.CurrentX = X1 - X.TextWidth(TESTO) - iCl
      X.CurrentY = Y1 - iRG / 2
      X.Print TESTO
      '
      X1 = (MLeft + 3 + Box1W / 10) - 1
      X2 = X1 + 2
      Y1 = (MTop - 0.5 + 16 + Box1H / 2) - Hinf
      Y2 = Y1
      X.Line (X1, Y1)-(X2, Y2), QBColor(12)
      '
      TESTO = frmt(FASCIA_RIDOTTA / 2, 3)
      X.CurrentX = X1 - X.TextWidth(TESTO) - iCl
      X.CurrentY = Y1 - iRG / 2
      X.Print TESTO
    End If
    '
    X1 = MLeft + 3 + 3 * Box1W / 4: Y1 = MTop - 0.5 + 16
    TESTO = "F1"
    'VISUALIZZAZIONE QUOTA WILDHABER
    If (Len(sQUOTA_WILDHABER) > 0) And (Np <= 0) Then
      X.ForeColor = QBColor(12)
      TESTO = sQUOTA_WILDHABER & " " & TESTO
      X.CurrentX = X1 - X.TextWidth(TESTO) / 2
      X.CurrentY = Y1 - iRG
      X.Print sQUOTA_WILDHABER
    End If
    X.ForeColor = QBColor(0)
    X.CurrentX = X1 + X.TextWidth(TESTO) / 2 - X.TextWidth("F1")
    X.CurrentY = Y1 - iRG
    X.FontBold = True
    X.Print "F1"
    X.FontBold = False
    '
    If (SiOpt(4, 1) = 1) Then
        k = 1
          X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
          Y1 = (MTop - 0.5 + 16 + Box1H / 2)
          Y1 = Y1 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
          Y1 = Y1 - (HyE(1) - HyE(1)) * Sc1Y / 10
          X2 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
          Y2 = (MTop - 0.5 + 16 + Box1H / 2)
          Y2 = Y2 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
          Y2 = Y2 - (HyE(NE) - HyE(1)) * Sc1Y / 10
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
          X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10))) - 0.5
          X2 = X1 + 1
          Y1 = (MTop - 0.5 + 16 + Box1H / 2) + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
          Y2 = Y1
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
          X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10))) - 0.5
          X2 = X1 + 1
          Y1 = (MTop - 0.5 + 16 + Box1H / 2) - (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
          Y2 = Y1
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
      For k = 2 To 4
        If (iEd(k) <> 0) Then
          X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
          Y1 = (MTop - 0.5 + 16 + Box1H / 2)
          Y1 = Y1 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
          Y1 = Y1 - (HyE(1) - HyE(1)) * Sc1Y / 10
          X2 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
          Y2 = (MTop - 0.5 + 16 + Box1H / 2)
          Y2 = Y2 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
          Y2 = Y2 - (HyE(NE) - HyE(1)) * Sc1Y / 10
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
          X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10))) - 0.5
          X2 = X1 + 1
          Y1 = (MTop - 0.5 + 16 + Box1H / 2) + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
          Y2 = Y1
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
          X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10))) - 0.5
          X2 = X1 + 1
          Y1 = (MTop - 0.5 + 16 + Box1H / 2) - (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
          Y2 = Y1
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
        End If
      Next
    End If
    For k = 1 To 4
      SiDisegna = False
      Select Case k
        Case 1
          For i = 1 To NE: Ed0f1(i) = Ed1f1(i): Next i
            GoSub STIMA_BOMBATURA_ELICA_FIANCO1
            SiDisegna = True
        Case 2
          For i = 1 To NE: Ed0f1(i) = Ed2f1(i): Next i
          If (iEd(2) <> 0) Then
            GoSub STIMA_BOMBATURA_ELICA_FIANCO1
            SiDisegna = True
          End If
        Case 3
          For i = 1 To NE: Ed0f1(i) = Ed3f1(i): Next i
          If (iEd(3) <> 0) Then
            GoSub STIMA_BOMBATURA_ELICA_FIANCO1
            SiDisegna = True
          End If
        Case 4
          For i = 1 To NE: Ed0f1(i) = Ed4f1(i): Next i
          If (iEd(4) <> 0) Then
            GoSub STIMA_BOMBATURA_ELICA_FIANCO1
            SiDisegna = True
          End If
      End Select
      If SiDisegna Then
        For i = 1 To NE
          X1 = (MLeft + 3 + (Box1W - k * Box1W / 10))
          X1 = X1 + (Ed0f1(i) - MV(Ed0f1, CHK_Ini_ELICA, CHK_Fin_ELICA)) * Sc1X / 10
          Y1 = (MTop - 0.5 + 16 + Box1H / 2)
          Y1 = Y1 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
          Y1 = Y1 - (HyE(i) - HyE(1)) * Sc1Y / 10
          'If (SiOpt(0, 1) = 1) Then
          '  R1 = 0.025
          '  X.Circle (X1, Y1), R1, QBColor(0)
          'End If
          'If (SiOpt(1, 1) = 1) Then
          '  X.Line ((MLeft + 3 + (Box1W - k * Box1W / 10)), Y1)-(X1, Y1), QBColor(0)
          'End If
          If (i < NE) And (SiOpt(2, 1) = 1) Then
            X2 = (MLeft + 3 + (Box1W - k * Box1W / 10))
            X2 = X2 + (Ed0f1(i + 1) - MV(Ed0f1, CHK_Ini_ELICA, CHK_Fin_ELICA)) * Sc1X / 10
            Y2 = (MTop - 0.5 + 16 + Box1H / 2)
            Y2 = Y2 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
            Y2 = Y2 - (HyE(i + 1) - HyE(1)) * Sc1Y / 10
            X.Line (X1, Y1)-(X2, Y2), QBColor(0)
          End If
        Next i
        X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
        Y1 = (MTop - 0.5 + 16 + Box1H)
        TESTO = Format$(iEd(k), "#")
        X.CurrentX = X1 - X.TextWidth(TESTO) / 2
        X.CurrentY = Y1
        X.Print TESTO
        j = 0
        X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
        Y1 = MTop - 0.5 + 25.5 + j * iRG
        TESTO = frmt(fbF1d(k) * 1000, 1)
        X.CurrentX = X1 - X.TextWidth(TESTO) / 2 '+ ICl
        X.CurrentY = Y1
        X.FontBold = (Abs(fbF1d(k)) = fbMAX(1))
        X.Print TESTO
        X.FontBold = False
        j = 1
        X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
        Y1 = MTop - 0.5 + 25.5 + j * iRG
        TESTO = frmt(ffbF1d(k) * 1000, 1)
        X.CurrentX = X1 - X.TextWidth(TESTO) / 2 '+ ICl
        X.CurrentY = Y1
        X.FontBold = (Abs(ffbF1d(k)) = ffbMAX(1))
        X.Print TESTO
        X.FontBold = False
        j = 2
        X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
        Y1 = MTop - 0.5 + 25.5 + j * iRG
        TESTO = frmt(fhbF1d(k) * 1000, 1)
        X.CurrentX = X1 - X.TextWidth(TESTO) / 2 '+ ICl
        X.CurrentY = Y1
        X.FontBold = (Abs(fhbF1d(k)) = fhbMAX(1))
        X.Print TESTO
        X.FontBold = False
      End If
    Next k
    '
    If (SiSTIMA_BOMBATURA = 1) Then
      BombaturaMAX = 0
      For k = 1 To 4
        If (iEd(k) <> 0) Then
          If (BombaturaMAX <= Abs(Bombatura(k))) Then BombaturaMAX = Abs(Bombatura(k))
        End If
      Next k
      For k = 1 To 4
        If (iEd(k) <> 0) Then
          j = 3
          X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
          Y1 = MTop - 0.5 + 25.5 + j * iRG
          TESTO = frmt(Abs(Bombatura(k)) * 1000, 1)
          X.CurrentX = X1 - X.TextWidth(TESTO) / 2
          X.CurrentY = Y1
          X.FontBold = (Abs(Bombatura(k)) = BombaturaMAX)
          X.Print TESTO
          X.FontBold = False
        End If
      Next k
    End If
    X1 = (MLeft + 3 + (Box1W - Box1W / 10)) - 1
    X2 = X1 + 2
    Y1 = (MTop - 0.5 + 16 + Box1H / 2) + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
    Y2 = Y1
    'x.Line (X1, Y1)-(X2, Y2), QBColor(0)
    TESTO = frmt(-FasCon / 2, 3)
    X.CurrentX = X2 + iCl
    X.CurrentY = Y1 - iRG / 2
    X.Print TESTO
    X1 = (MLeft + 3 + (Box1W - Box1W / 10)) - 1
    X2 = X1 + 2
    Y1 = (MTop - 0.5 + 16 + Box1H / 2) - (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
    Y2 = Y1
    'x.Line (X1, Y1)-(X2, Y2), QBColor(0)
    TESTO = frmt(FasCon / 2, 3)
    X.CurrentX = X2 + iCl
    X.CurrentY = Y1 - iRG / 2
    X.Print TESTO
    If FASCIA_RIDOTTA < FasCon Then
      X1 = (MLeft + 3 + (Box1W - Box1W / 10)) - 1
      X2 = X1 + 2
      Y1 = (MTop - 0.5 + 16 + Box1H / 2) + Hinf
      Y2 = Y1
      X.Line (X1, Y1)-(X2, Y2), QBColor(12)
      TESTO = frmt(-FASCIA_RIDOTTA / 2, 3)
      X.CurrentX = X2 + iCl
      X.CurrentY = Y1 - iRG / 2
      X.Print TESTO
      X1 = (MLeft + 3 + (Box1W - Box1W / 10)) - 1
      X2 = X1 + 2
      Y1 = (MTop - 0.5 + 16 + Box1H / 2) - Hinf
      Y2 = Y1
      X.Line (X1, Y1)-(X2, Y2), QBColor(12)
      TESTO = frmt(FASCIA_RIDOTTA / 2, 3)
      X.CurrentX = X2 + iCl
      X.CurrentY = Y1 - iRG / 2
      X.Print TESTO
    End If
  End If
  '
  'Scale X e Y del controllo ELICA
  X.DrawStyle = 0
  X1 = MLeft: Y1 = MTop - 0.5 + 16
  X2 = MLeft: Y2 = MTop - 0.5 + 25.5
  Testa = "HEAD"
  Piede = "TAIL"
  Call VisScXY(X, Sc1X, Sc1Y, 1, 1)
  X.DrawStyle = 0
  '
  stmp = GetInfo("CHK0", "TOLLERANZA_ELICA", Path_LAVORAZIONE_INI): stmp = UCase$(Trim$(stmp))
  If (stmp <> "Y") Then Exit Sub
  '
  'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  Open PathMIS & "\" & "DEFAULT" For Input As 1
    Line Input #1, stmp
    Line Input #1, stmp
    Line Input #1, stmp
    Input #1, ftmp, rsp
    If (rsp = 2) Then
      Input #1, k1, ftmp, k1, k1, k1, k1, CODICE_PEZZO
    Else
      CODICE_PEZZO = GetInfo("CHK0", "FileMisura", Path_LAVORAZIONE_INI): CODICE_PEZZO = UCase$(Trim$(CODICE_PEZZO))
      'LEGGO COMUNQUE FINO AL PRIMO SPAZIO
      rsp = InStr(CODICE_PEZZO, " "): If rsp > 0 Then CODICE_PEZZO = Left$(CODICE_PEZZO, rsp - 1)
      'LEGGO COMUNQUE FINO AL PRIMO PUNTO
      rsp = InStr(CODICE_PEZZO, "."): If rsp > 0 Then CODICE_PEZZO = Left$(CODICE_PEZZO, rsp - 1)
    End If
  Close #1
  '
  'LETTURA DELLA TOLLERANZA DAL DATABASE
  Set DBB = OpenDatabase(PathDB, True, False)
  Set RSS = DBB.OpenRecordset("ARCHIVIO_" & TIPO_LAVORAZIONE)
    RSS.Index = "NOME_PEZZO"
    RSS.Seek "=", CODICE_PEZZO
    If Not RSS.NoMatch Then
      If (Len(RSS.Fields("TOLLERANZA_ELICA").Value) > 0) Then
        lstTOLLERANZA = UCase$(Trim$(RSS.Fields("TOLLERANZA_ELICA").Value))
      Else
        lstTOLLERANZA = ""
      End If
    End If
  RSS.Close
  DBB.Close
  '
  'RICAVO I SINGOLI VALORI DALLA LISTA
  If (Len(lstTOLLERANZA) > 0) Then
    Dim NumeroPuntiUsatiF(2) As Integer
      k1 = InStr(lstTOLLERANZA, ";;")
      If (k1 <= 0) Then
        k1 = InStr(lstTOLLERANZA, ";")
        k2 = InStr(k1 + 1, lstTOLLERANZA, ";")
        k3 = InStr(k2 + 1, lstTOLLERANZA, ";")
        k4 = 1
        For j = 1 To 8
          pXX(1, j) = val(Mid$(lstTOLLERANZA, k4, k1 - k4))
          pYY(1, j) = val(Mid$(lstTOLLERANZA, k1 + 1, k2 - k1 - 1))
          pBB(1, j) = val(Mid$(lstTOLLERANZA, k2 + 1, k3 - k2 - 1))
          k4 = k3 + 1
          k1 = InStr(k4, lstTOLLERANZA, ";")
          k2 = InStr(k1 + 1, lstTOLLERANZA, ";")
          k3 = InStr(k2 + 1, lstTOLLERANZA, ";")
        Next j
        For j = 1 To 8
          pXX(2, j) = val(Mid$(lstTOLLERANZA, k4, k1 - k4))
          pYY(2, j) = val(Mid$(lstTOLLERANZA, k1 + 1, k2 - k1 - 1))
          pBB(2, j) = val(Mid$(lstTOLLERANZA, k2 + 1, k3 - k2 - 1))
          k4 = k3 + 1
          k1 = InStr(k4, lstTOLLERANZA, ";")
          k2 = InStr(k1 + 1, lstTOLLERANZA, ";")
          k3 = InStr(k2 + 1, lstTOLLERANZA, ";")
        Next j
      End If
      i = 1
      NumeroPuntiUsatiF(1) = 8
        For j = 1 To 8
          If (pYY(i, j) = 0) And (pXX(i, j) = 0) Then
            For k1 = j To 8
              pYY(i, k1) = pYY(i, 1):  pXX(i, k1) = pXX(i, 1)
              NumeroPuntiUsatiF(1) = NumeroPuntiUsatiF(1) - 1
            Next k1
            Exit For
          End If
        Next j
      i = 2
      NumeroPuntiUsatiF(2) = 8
        For j = 1 To 8
          If (pYY(i, j) = 0) And (pXX(i, j) = 0) Then
            For k1 = j To 8
              pYY(i, k1) = pYY(i, 1):  pXX(i, k1) = pXX(i, 1)
              NumeroPuntiUsatiF(2) = NumeroPuntiUsatiF(2) - 1
            Next k1
            Exit For
          End If
        Next j
      For j = 1 To 2
        pXX(j, 9) = pXX(j, 1)
        pYY(j, 9) = pYY(j, 1)
      Next j
      'FIANCO 2 TOLLERANZA ELICA --> FIANCO 1 PEZZO
      If NumeroPuntiUsatiF(2) > 1 Then
        For k = 1 To 4
          SiDisegna = False
          Select Case k
            Case 1:                     SiDisegna = True
            Case 2: If iEd(2) <> 0 Then SiDisegna = True
            Case 3: If iEd(3) <> 0 Then SiDisegna = True
            Case 4: If iEd(4) <> 0 Then SiDisegna = True
          End Select
          If SiDisegna Then
            For i = 1 To 8
            If (i <> 8) And (pBB(2, i) = 0) Then
              X1 = (MLeft + 3 + k * Box1W / 10)
              X1 = X1 + pXX(2, i) * Sc1X / 10
              Y1 = (MTop - 0.5 + 16 + Box1H / 2)
              Y1 = Y1 - (pYY(2, i) - HyE(1)) * Sc1Y / 10
              X2 = (MLeft + 3 + k * Box1W / 10)
              X2 = X2 + pXX(2, i + 1) * Sc1X / 10
              Y2 = (MTop - 0.5 + 16 + Box1H / 2)
              Y2 = Y2 - (pYY(2, i + 1) - HyE(1)) * Sc1Y / 10
              X.Line (X1, Y1)-(X2, Y2), QBColor(9)
            Else
              Call visRACCORDA_DUE_PUNTI(X, pXX(2, i), (pYY(2, i) - HyE(1)), pXX(2, i + 1), (pYY(2, i + 1) - HyE(1)), pBB(2, i), "SX", (MLeft + 3 + k * Box1W / 10), (MTop - 0.5 + 16 + Box1H / 2), Sc1X / 10, Sc1Y / 10)
            End If
            Next i
          End If
        Next k
        i = 1
        X.FillStyle = 0
        X.FillColor = QBColor(12)
        X1 = (MLeft + 3 + Box1W / 10)
        X1 = X1 + pXX(2, i) * Sc1X / 10
        Y1 = (MTop - 0.5 + 16 + Box1H / 2)
        Y1 = Y1 - (pYY(2, i) - HyE(1)) * Sc1Y / 10
        X.Circle (X1, Y1), 0.05
        X.FillStyle = 1
        X.FillColor = QBColor(0)
        X.Line (X1 - 0.05, Y1)-(X1 + 0.05, Y1), QBColor(0)
        X.Line (X1, Y1 - 0.05)-(X1, Y1 + 0.05), QBColor(0)
      End If
      'FIANCO 1 TOLLERANZA ELICA --> FIANCO 2 PEZZO
      If NumeroPuntiUsatiF(1) > 1 Then
        For k = 1 To 4
          SiDisegna = False
          Select Case k
            Case 1:                     SiDisegna = True
            Case 2: If iEd(2) <> 0 Then SiDisegna = True
            Case 3: If iEd(3) <> 0 Then SiDisegna = True
            Case 4: If iEd(4) <> 0 Then SiDisegna = True
          End Select
          If SiDisegna Then
            For i = 1 To 8
            If (i <> 8) And (pBB(1, i) = 0) Then
              X1 = (MLeft + 3 + (Box1W - k * Box1W / 10))
              X1 = X1 + pXX(1, i) * Sc1X / 10
              Y1 = (MTop - 0.5 + 16 + Box1H / 2)
              Y1 = Y1 - (pYY(1, i) - HyE(1)) * Sc1Y / 10
              X2 = (MLeft + 3 + (Box1W - k * Box1W / 10))
              X2 = X2 + pXX(1, i + 1) * Sc1X / 10
              Y2 = (MTop - 0.5 + 16 + Box1H / 2)
              Y2 = Y2 - (pYY(1, i + 1) - HyE(1)) * Sc1Y / 10
              X.Line (X1, Y1)-(X2, Y2), QBColor(9)
            Else
              Call visRACCORDA_DUE_PUNTI(X, pXX(1, i), (pYY(1, i) - HyE(1)), pXX(1, i + 1), (pYY(1, i + 1) - HyE(1)), pBB(1, i), "SX", (MLeft + 3 + (Box1W - k * Box1W / 10)), (MTop - 0.5 + 16 + Box1H / 2), Sc1X / 10, Sc1Y / 10)
            End If
            Next i
          End If
        Next k
        i = 1
        X.FillStyle = 0
        X.FillColor = QBColor(12)
        X1 = (MLeft + 3 + (Box1W - Box1W / 10))
        X1 = X1 + pXX(1, i) * Sc1X / 10
        Y1 = (MTop - 0.5 + 16 + Box1H / 2)
        Y1 = Y1 - (pYY(1, i) - HyE(1)) * Sc1Y / 10
        X.Circle (X1, Y1), 0.05
        X.FillStyle = 1
        X.FillColor = QBColor(0)
        X.Line (X1 - 0.05, Y1)-(X1 + 0.05, Y1), QBColor(0)
        X.Line (X1, Y1 - 0.05)-(X1, Y1 + 0.05), QBColor(0)
      End If
    End If
    '
Exit Sub

STIMA_BOMBATURA_ELICA_FIANCO1:
  'VISUALIZZAZIONE RETTA DI REGRESSIONE
  If (SiOpt(4, 1) = 1) Then
    M = fhbF1d(k) / (HyE(CHK_Fin_ELICA) - HyE(CHK_Ini_ELICA))
    'PER ELICA SINISTRA CAMBIO IL SEGNO DI fhb DI F1
    If (AngEli < 0) Then M = -M
    X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
    X1 = X1 - M * (HyE(CHK_Fin_ELICA) - HyE(CHK_Ini_ELICA)) * Sc1X / 10 / 2
    Y1 = (MTop - 0.5 + 16 + Box1H / 2)
    Y1 = Y1 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
    Y1 = Y1 - (HyE(CHK_Ini_ELICA) - HyE(1)) * Sc1Y / 10
    X2 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
    X2 = X2 + M * (HyE(CHK_Fin_ELICA) - HyE(CHK_Ini_ELICA)) * Sc1X / 10 / 2
    Y2 = (MTop - 0.5 + 16 + Box1H / 2)
    Y2 = Y2 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
    Y2 = Y2 - (HyE(CHK_Fin_ELICA) - HyE(1)) * Sc1Y / 10
    X.DrawWidth = 2
    X.Line (X1, Y1)-(X2, Y2), QBColor(12)
    X.DrawWidth = 1
  End If
  '
  'STIMA BOMBATURA CON VISUALIZZAZIONE DELL' ARCO DI CERCHIO SVG 07/11/09
  If (SiSTIMA_BOMBATURA = 1) Then
    Call CalcoloCerchio(HyE, Ed0f1, AM, BM, RM)
    '
    X1 = AM + Sqr(RM ^ 2 - (Ed0f1(1) - BM) ^ 2):  Y1 = HyE(1)
    X2 = AM + Sqr(RM ^ 2 - (Ed0f1(NE) - BM) ^ 2): Y2 = HyE(NE)
    ANGOLO = Sqr((X2 - X1) ^ 2 + (Y2 - Y1) ^ 2)
    ANGOLO = FnASN(ANGOLO / 2 / RM)
    '
    Bombatura(k) = RM * (1 - Cos(ANGOLO))
    '
    ffbF1d(k) = 0: RRB_max = -1E+99: RRB_min = 1E+99
    Select Case k
    Case 1
      For i = 1 To NE
        RRB_cur = Sqr((Ed1f1(i) - AM) ^ 2 + (HyE(i) - BM) ^ 2)
        If (RRB_cur > RRB_max) Then RRB_max = RRB_cur
        If (RRB_cur < RRB_min) Then RRB_min = RRB_cur
      Next i
    Case 2
      For i = 1 To NE
        RRB_cur = Sqr((Ed2f1(i) - AM) ^ 2 + (HyE(i) - BM) ^ 2)
        If (RRB_cur > RRB_max) Then RRB_max = RRB_cur
        If (RRB_cur < RRB_min) Then RRB_min = RRB_cur
      Next i
    Case 3
      For i = 1 To NE
        RRB_cur = Sqr((Ed3f1(i) - AM) ^ 2 + (HyE(i) - BM) ^ 2)
        If (RRB_cur > RRB_max) Then RRB_max = RRB_cur
        If (RRB_cur < RRB_min) Then RRB_min = RRB_cur
      Next i
    Case 4
      For i = 1 To NE
        RRB_cur = Sqr((Ed4f1(i) - AM) ^ 2 + (HyE(i) - BM) ^ 2)
        If (RRB_cur > RRB_max) Then RRB_max = RRB_cur
        If (RRB_cur < RRB_min) Then RRB_min = RRB_cur
      Next i
    End Select
    ffbF1d(k) = RRB_max - RRB_min
    '
    X.DrawWidth = 2
    If (AM > 0) Then
      For i = 1 To NE - 1
        X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
        X1 = X1 + (AM - Sqr(RM ^ 2 - (HyE(i) - BM) ^ 2) - MV(Ed0f1, CHK_Ini_ELICA, CHK_Fin_ELICA)) * Sc1X / 10
        Y1 = (MTop - 0.5 + 16 + Box1H / 2)
        Y1 = Y1 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
        Y1 = Y1 - HyE(i) * Sc1Y / 10
        '
        X2 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
        X2 = X2 + (AM - Sqr(RM ^ 2 - (HyE(i + 1) - BM) ^ 2) - MV(Ed0f1, CHK_Ini_ELICA, CHK_Fin_ELICA)) * Sc1X / 10
        Y2 = (MTop - 0.5 + 16 + Box1H / 2)
        Y2 = Y2 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
        Y2 = Y2 - HyE(i + 1) * Sc1Y / 10
        '
        X.Line (X1, Y1)-(X2, Y2), QBColor(12)
      Next i
    Else
      For i = 1 To NE - 1
        X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
        X1 = X1 + (AM + Sqr(RM ^ 2 - (HyE(i) - BM) ^ 2) - MV(Ed0f1, CHK_Ini_ELICA, CHK_Fin_ELICA)) * Sc1X / 10
        Y1 = (MTop - 0.5 + 16 + Box1H / 2)
        Y1 = Y1 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
        Y1 = Y1 - HyE(i) * Sc1Y / 10
        '
        X2 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
        X2 = X2 + (AM + Sqr(RM ^ 2 - (HyE(i + 1) - BM) ^ 2) - MV(Ed0f1, CHK_Ini_ELICA, CHK_Fin_ELICA)) * Sc1X / 10
        Y2 = (MTop - 0.5 + 16 + Box1H / 2)
        Y2 = Y2 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
        Y2 = Y2 - HyE(i + 1) * Sc1Y / 10
        '
        X.Line (X1, Y1)-(X2, Y2), QBColor(12)
      Next i
    End If
    X.DrawWidth = 1
    '
  End If
  '
Return

STIMA_BOMBATURA_ELICA_FIANCO2:
  'VISUALIZZAZIONE RETTA DI REGRESSIONE
  If (SiOpt(4, 1) = 1) Then
    M = fhbF2d(k) / (HyE(CHK_Fin_ELICA) - HyE(CHK_Ini_ELICA))
    'PER ELICA DESTRA CAMBIO IL SEGNO DI fhb DI F2
    If (AngEli > 0) Then M = -M
    X1 = (MLeft + 3 + k * (Box1W / 10))
    X1 = X1 + M * (HyE(CHK_Fin_ELICA) - HyE(CHK_Ini_ELICA)) * Sc1X / 10 / 2
    Y1 = (MTop - 0.5 + 16 + Box1H / 2)
    Y1 = Y1 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
    Y1 = Y1 - (HyE(CHK_Ini_ELICA) - HyE(1)) * Sc1Y / 10
    X2 = (MLeft + 3 + k * (Box1W / 10))
    X2 = X2 - M * (HyE(CHK_Fin_ELICA) - HyE(CHK_Ini_ELICA)) * Sc1X / 10 / 2
    Y2 = (MTop - 0.5 + 16 + Box1H / 2)
    Y2 = Y2 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
    Y2 = Y2 - (HyE(CHK_Fin_ELICA) - HyE(1)) * Sc1Y / 10
    X.DrawWidth = 2
    X.Line (X1, Y1)-(X2, Y2), QBColor(12)
    X.DrawWidth = 1
  End If
  '
  'STIMA BOMBATURA CON VISUALIZZAZIONE DELL' ARCO DI CERCHIO SVG 07/11/09
  If (SiSTIMA_BOMBATURA = 1) Then
    Call CalcoloCerchio(HyE, Ed0f2, AM, BM, RM)
    '
    X1 = AM + Sqr(RM ^ 2 - (Ed0f2(1) - BM) ^ 2):  Y1 = HyE(1)
    X2 = AM + Sqr(RM ^ 2 - (Ed0f2(NE) - BM) ^ 2): Y2 = HyE(NE)
    ANGOLO = Sqr((X2 - X1) ^ 2 + (Y2 - Y1) ^ 2)
    ANGOLO = FnASN(ANGOLO / 2 / RM)
    '
    Bombatura(k) = RM * (1 - Cos(ANGOLO))
    '
    ffbF2d(k) = 0: RRB_max = -1E+99: RRB_min = 1E+99
    Select Case k
    Case 1
      For i = 1 To NE
        RRB_cur = Sqr((Ed1f2(i) - AM) ^ 2 + (HyE(i) - BM) ^ 2)
        If (RRB_cur > RRB_max) Then RRB_max = RRB_cur
        If (RRB_cur < RRB_min) Then RRB_min = RRB_cur
      Next i
    Case 2
      For i = 1 To NE
        RRB_cur = Sqr((Ed2f2(i) - AM) ^ 2 + (HyE(i) - BM) ^ 2)
        If (RRB_cur > RRB_max) Then RRB_max = RRB_cur
        If (RRB_cur < RRB_min) Then RRB_min = RRB_cur
      Next i
    Case 3
      For i = 1 To NE
        RRB_cur = Sqr((Ed3f2(i) - AM) ^ 2 + (HyE(i) - BM) ^ 2)
        If (RRB_cur > RRB_max) Then RRB_max = RRB_cur
        If (RRB_cur < RRB_min) Then RRB_min = RRB_cur
      Next i
    Case 4
      For i = 1 To NE
        RRB_cur = Sqr((Ed4f2(i) - AM) ^ 2 + (HyE(i) - BM) ^ 2)
        If (RRB_cur > RRB_max) Then RRB_max = RRB_cur
        If (RRB_cur < RRB_min) Then RRB_min = RRB_cur
      Next i
    End Select
    ffbF2d(k) = RRB_max - RRB_min
    '
    X.DrawWidth = 2
    If (AM < 0) Then
      For i = 1 To NE - 1
        X1 = (MLeft + 3 + k * (Box1W / 10))
        X1 = X1 - (AM + Sqr(RM ^ 2 - (HyE(i) - BM) ^ 2) - MV(Ed0f2, CHK_Ini_ELICA, CHK_Fin_ELICA)) * Sc1X / 10
        Y1 = (MTop - 0.5 + 16 + Box1H / 2)
        Y1 = Y1 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
        Y1 = Y1 - HyE(i) * Sc1Y / 10
        '
        X2 = (MLeft + 3 + k * (Box1W / 10))
        X2 = X2 - (AM + Sqr(RM ^ 2 - (HyE(i + 1) - BM) ^ 2) - MV(Ed0f2, CHK_Ini_ELICA, CHK_Fin_ELICA)) * Sc1X / 10
        Y2 = (MTop - 0.5 + 16 + Box1H / 2)
        Y2 = Y2 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
        Y2 = Y2 - HyE(i + 1) * Sc1Y / 10
        '
        X.Line (X1, Y1)-(X2, Y2), QBColor(12)
      Next i
    Else
      For i = 1 To NE - 1
        X1 = (MLeft + 3 + k * (Box1W / 10))
        X1 = X1 - (AM - Sqr(RM ^ 2 - (HyE(i) - BM) ^ 2) - MV(Ed0f2, CHK_Ini_ELICA, CHK_Fin_ELICA)) * Sc1X / 10
        Y1 = (MTop - 0.5 + 16 + Box1H / 2)
        Y1 = Y1 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
        Y1 = Y1 - HyE(i) * Sc1Y / 10
        '
        X2 = (MLeft + 3 + k * (Box1W / 10))
        X2 = X2 - (AM - Sqr(RM ^ 2 - (HyE(i + 1) - BM) ^ 2) - MV(Ed0f2, CHK_Ini_ELICA, CHK_Fin_ELICA)) * Sc1X / 10
        Y2 = (MTop - 0.5 + 16 + Box1H / 2)
        Y2 = Y2 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
        Y2 = Y2 - HyE(i + 1) * Sc1Y / 10
        '
        X.Line (X1, Y1)-(X2, Y2), QBColor(12)
      Next i
    End If
    X.DrawWidth = 1
    '
  End If
  '
Return

errVisELICA:
  WRITE_DIALOG "SUB VisELICA ERROR " & str(Err)
  Resume Next

End Sub

Private Function TIF2DIA(Tif As Double, Z As Integer, AM As Double, ByVal Beta As Double, ByVal ALFAn As Double) As Double

Dim Rp As Double
Dim ALFAt As Double
Dim Rb As Double
Dim DB As Double

On Error Resume Next

    ALFAn = ALFAn * (PG / 180)
    'CONSIDERO UN INGRANAGGIO DRITTO CON UN ELICA PICCOLISSIMA
    If Beta = 0 Then Beta = (0.00001) * (PG / 180) Else Beta = Beta * (PG / 180)
    ALFAt = Atn(Tan(ALFAn) / Cos(Beta))
    Rp = AM * Z / 2 / Cos(Beta)         'Raggio primitivo
    Rb = Rp * Cos(ALFAt)                'Raggio di base
    'DB = 2 * RB                         'Diametro di base
    TIF2DIA = 2 * Sqr(Tif ^ 2 + Rb ^ 2)

End Function

Private Function DIA2TIF(Dia As Double, Z As Integer, AM As Double, ByVal Beta As Double, ByVal ALFAn As Double) As Double
'
Dim Rp      As Double
Dim ALFAt   As Double
Dim Rb      As Double
Dim DB      As Double
'
On Error Resume Next
  '
  ALFAn = ALFAn * (PG / 180)
  'CONSIDERO UN INGRANAGGIO DRITTO CON UN ELICA PICCOLISSIMA
  If Beta = 0 Then Beta = (0.00001) * (PG / 180) Else Beta = Beta * (PG / 180)
  ALFAt = Atn(Tan(ALFAn) / Cos(Beta))
  Rp = AM * Z / 2 / Cos(Beta)         'Raggio primitivo
  Rb = Rp * Cos(ALFAt)                'Raggio di base
  'DB = 2 * RB                         'Diametro di base
  DIA2TIF = Sqr((Dia / 2) ^ 2 - Rb ^ 2)
  '
End Function

Function Idx2Dia(Idx As Integer) As Double
'
On Error Resume Next
  '
  Idx2Dia = 0
  '
  If Idx > Np Then Idx = Np
  If Idx < 1 Then Idx = 1
  Idx2Dia = TIF2DIA(DIA2TIF(SAP, NZ, ModNor, AngEli, AngPreNor) + HyP(Idx), NZ, ModNor, AngEli, AngPreNor)
  If Idx2Dia > EAP Then Idx2Dia = EAP
  If Idx2Dia < SAP Then Idx2Dia = SAP
  '
End Function

Function Dia2Idx(Dia As Double) As Integer
'
Dim i     As Integer
Dim Diff  As Double
Dim iDiff As Integer
'
On Error Resume Next
  '
  Dia2Idx = 0
  '
  Diff = Abs(Dia - TIF2DIA(DIA2TIF(SAP, NZ, ModNor, AngEli, AngPreNor) + HyP(1), NZ, ModNor, AngEli, AngPreNor))
  iDiff = 1
  For i = 2 To UBound(HyP)
    If Abs(Dia - TIF2DIA(DIA2TIF(SAP, NZ, ModNor, AngEli, AngPreNor) + HyP(i), NZ, ModNor, AngEli, AngPreNor)) < Diff Then
      Diff = Abs(Dia - TIF2DIA(DIA2TIF(SAP, NZ, ModNor, AngEli, AngPreNor) + HyP(i), NZ, ModNor, AngEli, AngPreNor))
      iDiff = i
    End If
  Next i
  '
  Dia2Idx = iDiff
  '
End Function

' Calcolo del valore massimo del vettore errori fa, ffa , fha , il vettore dei punti
' serve per sapere quanti denti sono stati controllati
Function MaxVett(VettoreValori() As Double, VettorePunti() As Integer) As Double
'
Dim i As Integer
'
On Error Resume Next
  '
  MaxVett = Abs(VettoreValori(1))
  For i = 2 To UBound(VettoreValori)
    If VettorePunti(i) <> 0 Then
      If Abs(VettoreValori(i)) > MaxVett Then
        MaxVett = Abs(VettoreValori(i))
      End If
    End If
  Next i
  MaxVett = CInt(MaxVett * 1000)
  
End Function

' Calcolo del valore medio dei valori in un vettore
Function MediaVett(VettoreValori() As Double) As Double
'
Dim i As Integer
'
On Error Resume Next
  '
  MediaVett = 0
  If UCase(Trim(GetInfo("CHK0", "CentraControlli", Path_LAVORAZIONE_INI))) = "N" Then Exit Function
  If UBound(VettoreValori) <= 0 Then Exit Function
  For i = 1 To UBound(VettoreValori)
    MediaVett = MediaVett + VettoreValori(i)
  Next i
  MediaVett = MediaVett / UBound(VettoreValori)
  
End Function

' Calcolo del valore medio dei valori in un vettore
Function MV(VV() As Double, INI As Integer, FIN As Integer) As Double
'
Dim i As Integer
'
On Error Resume Next
  '
  MV = 0
  If UBound(VV) <= 0 Then Exit Function
  If (INI <= 1) Then INI = 1
  If (FIN <= INI) Then FIN = UBound(VV)
  For i = INI To FIN
    MV = MV + VV(i)
  Next i
  MV = MV / (FIN - INI + 1)
  
End Function

Sub VisPROFILO(X As Control, ByVal SEGNO_fha As Integer)
'
Dim Sc1X              As Single
Dim Sc1Y              As Single
Dim k                 As Integer
Dim stmp              As String
Dim SiDisegna         As Integer
Dim MLeft             As Single
Dim MTop              As Single
Dim DBB               As Database
Dim RSS               As Recordset
Dim pXX(2, 9)         As Double
Dim pYY(2, 9)         As Double
Dim pBB(2, 9)         As Double
Dim k1                As Long
Dim k2                As Long
Dim k3                As Long
Dim k4                As Long
Dim rsp               As Long
Dim CODICE_PEZZO      As String
Dim lstTOLLERANZA     As String
Dim TIPO_LAVORAZIONE  As String
Dim ftmp              As Double
Dim SiSTIMA_BOMBATURA As Integer
Dim SiGriglia1        As Integer
Dim SiGriglia2        As Integer
Dim AM                As Double
Dim BM                As Double
Dim RM                As Double
Dim ANGOLO            As Double
Dim Bombatura(5)      As Double
Dim Abgn              As Double
Dim Aend              As Double
Dim tDia              As Double
Dim RRB_cur           As Double
Dim RRB_max           As Double
Dim RRB_min           As Double
Dim faMAX(2)          As Double
Dim ffaMAX(2)         As Double
Dim fhaMAX(2)         As Double
Dim BombaturaMAX      As Double
Dim M                 As Double
Dim Q                 As Double
'
On Error GoTo errVisPROFILO
  '
  SiGriglia1 = val(GetInfo("CHK0", "Griglia1", Path_LAVORAZIONE_INI))
  SiGriglia2 = val(GetInfo("CHK0", "Griglia2", Path_LAVORAZIONE_INI))
  '
  SiSTIMA_BOMBATURA = val(GetInfo("CHK0", "BOMBATURA_PROFILO", Path_LAVORAZIONE_INI))
  stmp = GetInfo("CHK0", "TOLLERANZA_PROFILO", Path_LAVORAZIONE_INI): stmp = UCase$(Trim$(stmp))
  If (stmp <> "Y") Then SiSTIMA_BOMBATURA = 0
  '
  MLeft = val(GetInfo("CHK0", "MLeft", Path_LAVORAZIONE_INI))
  MTop = val(GetInfo("CHK0", "MTop", Path_LAVORAZIONE_INI))
  stmp = GetInfo("CHK0", "ScalaPROFILOx", Path_LAVORAZIONE_INI): Sc1X = val(stmp)
  stmp = GetInfo("CHK0", "ScalaPROFILOy", Path_LAVORAZIONE_INI): Sc1Y = val(stmp)
  '
  X.DrawWidth = 2
  'SEGNO +
  X1 = MLeft + 2: Y1 = (MTop + 1) + 3
  X.Circle (X1, Y1), 0.25, QBColor(0)
  X.Line (X1, Y1 + 0.15)-(X1, Y1 - 0.15)
  X.Line (X1 - 0.15, Y1)-(X1 + 0.15, Y1)
  'SEGNO -
  X1 = MLeft + 10: Y1 = (MTop + 1) + 7.5
  X.Circle (X1, Y1), 0.25, QBColor(0)
  X.Line (X1 - 0.15, Y1)-(X1 + 0.15, Y1)
  'SEGNO +
  X1 = MLeft + 18: Y1 = (MTop + 1) + 3
  X.Circle (X1, Y1), 0.25, QBColor(0)
  X.Line (X1, Y1 + 0.15)-(X1, Y1 - 0.15)
  X.Line (X1 - 0.15, Y1)-(X1 + 0.15, Y1)
  X.DrawWidth = 1
  If (SiGriglia1 = 1) Then
    X1 = MLeft + 3: Y1 = MTop + 4
    Call Disegna_Griglia1(X, CDbl(X1), CDbl(Y1))
  End If
  If (SiGriglia2 = 1) Then
    X1 = MLeft + 3: Y1 = MTop + 4
    Call Disegna_Griglia2(X, CDbl(X1), CDbl(Y1))
  End If
  TESTO = comVisPROELI(1, 1)
  X1 = MLeft + 3: Y1 = MTop + 4
  X1 = X1 + Box1W / 2: Y1 = Y1
  X.CurrentX = X1 - X.TextWidth(TESTO) / 2
  X.CurrentY = Y1 - iRG
  X.FontBold = True
  X.Print TESTO
  X.FontBold = False
  '
  If (SiOpt(4, 0) = 1) Then
    X.DrawStyle = 1
    R1 = 1.2 * Sqr(iRG * iRG + iCl * iCl)
    X1 = MLeft + 3: Y1 = MTop + 4
    X1 = X1 + Box1W / 2: Y1 = Y1
    X2 = X1: Y2 = Y1 + Box1H / 2
    X.Line (X1, Y1)-(X2, Y2 - R1), QBColor(0)
    X1 = MLeft + 3: Y1 = MTop + 4
    X1 = X1 + Box1W / 2: Y1 = Y1 + Box1H
    X2 = X1: Y2 = Y1 - Box1H / 2
    X.Line (X1, Y1)-(X2, Y2 + R1), QBColor(0)
    X1 = MLeft + 3: Y1 = MTop + 4
    X1 = X1: Y1 = Y1 + Box1H / 2
    X2 = X1 + Box1W / 2: Y2 = Y1
    X.Line (X1, Y1)-(X2 - R1, Y2), QBColor(0)
    X1 = MLeft + 3: Y1 = MTop + 4
    X1 = X1 + Box1W: Y1 = Y1 + Box1H / 2
    X2 = X1 - Box1W / 2: Y2 = Y1
    X.Line (X1, Y1)-(X2 + R1, Y2), QBColor(0)
    X.DrawStyle = 0
  End If
  '
  'Grafico del controllo PROFILO
  If (Np <> 0) Then
    X1 = MLeft + 3 + Box1W / 4: Y1 = MTop + 4
    TESTO = "F2"
    'VISUALIZZAZIONE QUOTA RULLI
    If (Len(sQUOTA_RULLI) > 0) Then
      X.ForeColor = QBColor(12)
      TESTO = TESTO & " " & sQUOTA_RULLI
      X.CurrentX = X1 - X.TextWidth(TESTO) / 2 + X.TextWidth("F2 ")
      X.CurrentY = Y1 - iRG
      X.Print sQUOTA_RULLI
    End If
    X.ForeColor = QBColor(0)
    X.CurrentX = X1 - X.TextWidth(TESTO) / 2
    X.CurrentY = Y1 - iRG
    X.FontBold = True
    X.Print "F2"
    X.FontBold = False
    If (SiOpt(4, 0) = 1) Then
        k = 1
          X1 = (MLeft + 3 + k * (Box1W / 10))
          Y1 = (MTop + 4 + Box1H / 2)
          Y1 = Y1 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
          Y1 = Y1 - (HyP(1) - HyP(1)) * Sc1Y / 10
          X2 = (MLeft + 3 + k * (Box1W / 10))
          Y2 = (MTop + 4 + Box1H / 2)
          Y2 = Y2 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
          Y2 = Y2 - (HyP(Np) - HyP(1)) * Sc1Y / 10
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
          X1 = (MLeft + 3 + Box1W / 10) - 0.5
          X2 = X1 + 1
          Y1 = (MTop + 4 + Box1H / 2) + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
          Y2 = Y1
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
          X1 = (MLeft + 3 + Box1W / 10) - 0.5
          X2 = X1 + 1
          Y1 = (MTop + 4 + Box1H / 2) - (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
          Y2 = Y1
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
      For k = 2 To 4
        If (iPd(k) <> 0) Then
          X1 = (MLeft + 3 + k * (Box1W / 10))
          Y1 = (MTop + 4 + Box1H / 2)
          Y1 = Y1 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
          Y1 = Y1 - (HyP(1) - HyP(1)) * Sc1Y / 10
          X2 = (MLeft + 3 + k * (Box1W / 10))
          Y2 = (MTop + 4 + Box1H / 2)
          Y2 = Y2 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
          Y2 = Y2 - (HyP(Np) - HyP(1)) * Sc1Y / 10
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
          X1 = (MLeft + 3 + k * (Box1W / 10)) - 0.5
          X2 = X1 + 1
          Y1 = (MTop + 4 + Box1H / 2) + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
          Y2 = Y1
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
          X1 = (MLeft + 3 + k * (Box1W / 10)) - 0.5
          X2 = X1 + 1
          Y1 = (MTop + 4 + Box1H / 2) - (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
          Y2 = Y1
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
        End If
      Next
    End If
    'RICERCA MASSIMI
    For k = 1 To 4
      If (faMAX(1) <= Abs(FaF1d(k))) Then faMAX(1) = Abs(FaF1d(k))
      If (faMAX(2) <= Abs(faF2d(k))) Then faMAX(2) = Abs(faF2d(k))
      If (ffaMAX(1) <= Abs(ffaF1d(k))) Then ffaMAX(1) = Abs(ffaF1d(k))
      If (ffaMAX(2) <= Abs(ffaF2d(k))) Then ffaMAX(2) = Abs(ffaF2d(k))
      If (fhaMAX(1) <= Abs(fhaF1d(k))) Then fhaMAX(1) = Abs(fhaF1d(k))
      If (fhaMAX(2) <= Abs(fhaF2d(k))) Then fhaMAX(2) = Abs(fhaF2d(k))
    Next k
    For k = 1 To 4
      SiDisegna = False
      Select Case k
        Case 1
          For i = 1 To Np: Pd0f2(i) = Pd1f2(i): Next i
            GoSub STIMA_BOMBATURA_PROFILO_FIANCO2
            SiDisegna = True
        Case 2
          For i = 1 To Np: Pd0f2(i) = Pd2f2(i): Next i
          If (iPd(2) <> 0) Then
            GoSub STIMA_BOMBATURA_PROFILO_FIANCO2
            SiDisegna = True
          End If
        Case 3
          For i = 1 To Np: Pd0f2(i) = Pd3f2(i): Next i
          If (iPd(3) <> 0) Then
            GoSub STIMA_BOMBATURA_PROFILO_FIANCO2
            SiDisegna = True
          End If
        Case 4
          For i = 1 To Np: Pd0f2(i) = Pd4f2(i): Next i
          If (iPd(4) <> 0) Then
            GoSub STIMA_BOMBATURA_PROFILO_FIANCO2
            SiDisegna = True
          End If
      End Select
      '
      If SiDisegna Then
        For i = 1 To Np
          X1 = (MLeft + 3 + k * (Box1W / 10))
          X1 = X1 - (Pd0f2(i) - MV(Pd0f2, CHK_Ini_PROFILO, CHK_Fin_PROFILO)) * Sc1X / 10
          Y1 = (MTop + 4 + Box1H / 2)
          Y1 = Y1 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
          Y1 = Y1 - (HyP(i) - HyP(1)) * Sc1Y / 10
          'If SiOpt(0, 0) = 1 Then
          '  R1 = 0.025
          '  X.Circle (X1, Y1), R1, QBColor(0)
          'End If
          'If SiOpt(1, 0) = 1 Then
          '  X.Line ((MLeft + 3 + k * (Box1W / 10)), Y1)-(X1, Y1), QBColor(0)
          'End If
          If (i < Np) And (SiOpt(2, 0) = 1) Then
            X2 = (MLeft + 3 + k * (Box1W / 10))
            X2 = X2 - (Pd0f2(i + 1) - MV(Pd0f2, CHK_Ini_PROFILO, CHK_Fin_PROFILO)) * Sc1X / 10
            Y2 = (MTop + 4 + Box1H / 2)
            Y2 = Y2 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
            Y2 = Y2 - (HyP(i + 1) - HyP(1)) * Sc1Y / 10
            X.Line (X1, Y1)-(X2, Y2), QBColor(0)
          End If
        Next i
        X1 = (MLeft + 3 + k * (Box1W / 10))
        Y1 = (MTop + 4 + Box1H)
        TESTO = Format$(iPd(k), "#")
        X.CurrentX = X1 - X.TextWidth(TESTO) / 2 '+ ICl
        X.CurrentY = Y1
        X.Print TESTO
        j = 0
        X1 = (MLeft + 3 + k * (Box1W / 10))
        Y1 = MTop + 13.5 + j * iRG
        TESTO = frmt(faF2d(k) * 1000, 1)
        X.CurrentX = X1 - X.TextWidth(TESTO) / 2 '+ ICl
        X.CurrentY = Y1
        X.FontBold = (Abs(faF2d(k)) = faMAX(2))
        X.Print TESTO
        X.FontBold = False
        j = 1
        X1 = (MLeft + 3 + k * (Box1W / 10))
        Y1 = MTop + 13.5 + j * iRG
        TESTO = frmt(ffaF2d(k) * 1000, 1)
        X.CurrentX = X1 - X.TextWidth(TESTO) / 2 '+ ICl
        X.CurrentY = Y1
        X.FontBold = (Abs(ffaF2d(k)) = ffaMAX(2))
        X.Print TESTO
        X.FontBold = False
        j = 2
        X1 = (MLeft + 3 + k * (Box1W / 10))
        Y1 = MTop + 13.5 + j * iRG
        TESTO = frmt(SEGNO_fha * fhaF2d(k) * 1000, 1)
        X.CurrentX = X1 - X.TextWidth(TESTO) / 2 '+ ICl
        X.CurrentY = Y1
        X.FontBold = (Abs(fhaF2d(k)) = fhaMAX(2))
        X.Print TESTO
        X.FontBold = False
      End If
    Next k
    '
    If (SiSTIMA_BOMBATURA = 1) Then
      BombaturaMAX = 0
      For k = 1 To 4
        If (iPd(k) <> 0) Then
          If (BombaturaMAX <= Abs(Bombatura(k))) Then BombaturaMAX = Abs(Bombatura(k))
        End If
      Next k
      For k = 1 To 4
        If (iPd(k) <> 0) Then
          j = 3
          X1 = (MLeft + 3 + k * (Box1W / 10))
          Y1 = MTop + 13.5 + j * iRG
          TESTO = Abs(frmt(Bombatura(k) * 1000, 1))
          X.CurrentX = X1 - X.TextWidth(TESTO) / 2
          X.CurrentY = Y1
          X.FontBold = (Abs(Bombatura(k)) = BombaturaMAX)
          X.Print TESTO
          X.FontBold = False
        End If
      Next k
    End If
    '
    For j = 0 To 2
      X1 = MLeft
      Y1 = MTop + 13.5 + j * iRG
      TESTO = comVisPROELI(4 + j, 1)
      X.CurrentX = X1 + (1.5 - X.TextWidth(TESTO)) / 2
      X.CurrentY = Y1
      X.Print TESTO
      X.Line (X1, Y1 + iRG)-(X1 + Box0W, Y1 + iRG), QBColor(0)
    Next j
    '
    'VISUALIZZAZIONE DELLE TOLLERANZE DIN
    j = -1
    X1 = MLeft + 3
    Y1 = MTop + 13.5 + j * iRG
    X.Line (X1 + Box1W / 2, Y1 + iRG)-(X1 + Box1W / 2, Y1 + 4 * iRG)
    TESTO = "Act."
    X.CurrentX = X1 + (Box1W - X.TextWidth(TESTO)) / 2
    X.CurrentY = (MTop + 4 + Box1H)
    X.Font.Bold = True
    X.Print TESTO
    X.Font.Bold = False
    For j = 0 To 2
      X.Font.Bold = True
      X1 = MLeft + 3
      Y1 = MTop + 13.5 + j * iRG
      'DIN F2
      Select Case j
        Case 0:    TESTO = Fa2Q(ModNor, MaxVett(faF2d, iPd))
        Case 1:    TESTO = ffa2Q(ModNor, MaxVett(ffaF2d, iPd))
        Case 2:    TESTO = fha2Q(ModNor, MaxVett(fhaF2d, iPd))
        Case Else: TESTO = "-"
      End Select
      X.CurrentX = X1 - X.TextWidth(TESTO & " ") + (Box1W) / 2
      X.CurrentY = Y1
      X.Print TESTO
      X1 = MLeft + 3
      Y1 = MTop + 13.5 + j * iRG
      'DIN F1
      Select Case j
        Case 0:    TESTO = Fa2Q(ModNor, MaxVett(FaF1d, iPd))
        Case 1:    TESTO = ffa2Q(ModNor, MaxVett(ffaF1d, iPd))
        Case 2:    TESTO = fha2Q(ModNor, MaxVett(fhaF1d, iPd))
        Case Else: TESTO = "-"
      End Select
      X.CurrentX = X1 + X.TextWidth(" ") + (Box1W) / 2
      X.CurrentY = Y1
      X.Print TESTO
      X.Font.Bold = False
    Next j
    '
    If (SiSTIMA_BOMBATURA = 1) Then
      j = 3
        X1 = MLeft
        Y1 = MTop + 13.5 + j * iRG
        TESTO = "Ca" 'comVisPROELI(7, 1)
        X.CurrentX = X1 + (1.5 - X.TextWidth(TESTO)) / 2
        X.CurrentY = Y1
        X.Print TESTO
        X.Line (X1, Y1 + iRG)-(X1 + Box0W, Y1 + iRG), QBColor(0)
    End If
    '
    X1 = (MLeft + 3 + Box1W / 10) - 1
    X2 = X1 + 2
    Y1 = (MTop + 4 + Box1H / 2) + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
    Y2 = Y1
    'x.Line (X1, Y1)-(X2, Y2), QBColor(0)
    '
    TESTO = frmt(SAP, 3)
    X.CurrentX = X1 - X.TextWidth(TESTO) - iCl
    X.CurrentY = Y1 - iRG / 2
    X.Print TESTO
    '
    X1 = (MLeft + 3 + Box1W / 10) - 1
    X2 = X1 + 2
    Y1 = (MTop + 4 + Box1H / 2) - (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
    Y2 = Y1
    'x.Line (X1, Y1)-(X2, Y2), QBColor(0)
    '
    TESTO = frmt(EAP, 3)
    X.CurrentX = X1 - X.TextWidth(TESTO) - iCl
    X.CurrentY = Y1 - iRG / 2
    X.Print TESTO
    '
    X1 = MLeft + 3 + 3 * Box1W / 4: Y1 = MTop + 4
    TESTO = "F1"
    'VISUALIZZAZIONE QUOTA WILDHABER
    If (Len(sQUOTA_WILDHABER) > 0) Then
      X.ForeColor = QBColor(12)
      TESTO = sQUOTA_WILDHABER & " " & TESTO
      X.CurrentX = X1 - X.TextWidth(TESTO) / 2
      X.CurrentY = Y1 - iRG
      X.Print sQUOTA_WILDHABER
    End If
    X.ForeColor = QBColor(0)
    X.CurrentX = X1 + X.TextWidth(TESTO) / 2 - X.TextWidth("F1")
    X.CurrentY = Y1 - iRG
    X.FontBold = True
    X.Print "F1"
    X.FontBold = False
    '
    'Inizio e fine valutazione
    If (CHK_Ini_PROFILO > 1) Then
      X1 = (MLeft + 3 + Box1W / 10) - 1
      X2 = X1 + 2
      Y1 = (MTop + 4 + Box1H / 2) + (HyP(Np) - 2 * HyP(CHK_Ini_PROFILO)) * Sc1Y / 10 / 2
      Y2 = Y1
      X.Line (X1, Y1)-(X2, Y2), vbBlue
      tDia = Idx2Dia(CHK_Ini_PROFILO)
      TESTO = frmt(tDia, 3)
      X.CurrentX = X1 - X.TextWidth(TESTO) - iCl
      X.CurrentY = Y1 - iRG / 2
      X.Print TESTO
    End If
    If (CHK_Fin_PROFILO < Np) Then
      X1 = (MLeft + 3 + Box1W / 10) - 1
      X2 = X1 + 2
      Y1 = (MTop + 4 + Box1H / 2) - (-HyP(Np) + 2 * HyP(CHK_Fin_PROFILO)) * Sc1Y / 10 / 2
      Y2 = Y1
      X.Line (X1, Y1)-(X2, Y2), vbBlue
      tDia = Idx2Dia(CHK_Fin_PROFILO)
      TESTO = frmt(tDia, 3)
      X.CurrentX = X1 - X.TextWidth(TESTO) - iCl
      X.CurrentY = Y1 - iRG / 2
      X.Print TESTO
    End If
    '
   If (SiOpt(4, 0) = 1) Then
        k = 1
          X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
          Y1 = (MTop + 4 + Box1H / 2)
          Y1 = Y1 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
          Y1 = Y1 - (HyP(1) - HyP(1)) * Sc1Y / 10
          X2 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
          Y2 = (MTop + 4 + Box1H / 2)
          Y2 = Y2 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
          Y2 = Y2 - (HyP(Np) - HyP(1)) * Sc1Y / 10
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
          X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10))) - 0.5
          X2 = X1 + 1
          Y1 = (MTop + 4 + Box1H / 2) + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
          Y2 = Y1
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
          X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10))) - 0.5
          X2 = X1 + 1
          Y1 = (MTop + 4 + Box1H / 2) - (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
          Y2 = Y1
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
      For k = 2 To 4
        If (iPd(k) <> 0) Then
          X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
          Y1 = (MTop + 4 + Box1H / 2)
          Y1 = Y1 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
          Y1 = Y1 - (HyP(1) - HyP(1)) * Sc1Y / 10
          X2 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
          Y2 = (MTop + 4 + Box1H / 2)
          Y2 = Y2 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
          Y2 = Y2 - (HyP(Np) - HyP(1)) * Sc1Y / 10
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
          X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10))) - 0.5
          X2 = X1 + 1
          Y1 = (MTop + 4 + Box1H / 2) + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
          Y2 = Y1
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
          X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10))) - 0.5
          X2 = X1 + 1
          Y1 = (MTop + 4 + Box1H / 2) - (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
          Y2 = Y1
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
        End If
      Next
    End If
    For k = 1 To 4
     SiDisegna = False
      Select Case k
        Case 1
          For i = 1 To Np: Pd0f1(i) = Pd1f1(i): Next i
            GoSub STIMA_BOMBATURA_PROFILO_FIANCO1
            SiDisegna = True
        Case 2
          For i = 1 To Np: Pd0f1(i) = Pd2f1(i): Next i
          If (iPd(2) <> 0) Then
            GoSub STIMA_BOMBATURA_PROFILO_FIANCO1
            SiDisegna = True
          End If
        Case 3
          For i = 1 To Np: Pd0f1(i) = Pd3f1(i): Next i
          If (iPd(3) <> 0) Then
            GoSub STIMA_BOMBATURA_PROFILO_FIANCO1
            SiDisegna = True
          End If
        Case 4
          For i = 1 To Np: Pd0f1(i) = Pd4f1(i): Next i
          If (iPd(4) <> 0) Then
            GoSub STIMA_BOMBATURA_PROFILO_FIANCO1
            SiDisegna = True
          End If
      End Select
      '
      If SiDisegna Then
        For i = 1 To Np
          X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
          X1 = X1 + (Pd0f1(i) - MV(Pd0f1, CHK_Ini_PROFILO, CHK_Fin_PROFILO)) * Sc1X / 10
          Y1 = (MTop + 4 + Box1H / 2)
          Y1 = Y1 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
          Y1 = Y1 - (HyP(i) - HyP(1)) * Sc1Y / 10
          'If SiOpt(0, 0) = 1 Then
          '  R1 = 0.025
          '  X.Circle (X1, Y1), R1, QBColor(0)
          'End If
          'If SiOpt(1, 0) = 1 Then
          '  X.Line ((MLeft + 3 + (Box1W - k * (Box1W / 10))), Y1)-(X1, Y1), QBColor(0)
          'End If
          If (i < Np) And (SiOpt(2, 0) = 1) Then
            X2 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
            X2 = X2 + (Pd0f1(i + 1) - MV(Pd0f1, CHK_Ini_PROFILO, CHK_Fin_PROFILO)) * Sc1X / 10
            Y2 = (MTop + 4 + Box1H / 2)
            Y2 = Y2 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
            Y2 = Y2 - (HyP(i + 1) - HyP(1)) * Sc1Y / 10
            X.Line (X1, Y1)-(X2, Y2), QBColor(0)
          End If
        Next i
        X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
        Y1 = (MTop + 4 + Box1H)
        TESTO = Format$(iPd(k), "#")
        X.CurrentX = X1 - X.TextWidth(TESTO) / 2 '+ ICl
        X.CurrentY = Y1
        X.Print TESTO
        j = 0
        X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
        Y1 = MTop + 13.5 + j * iRG
        TESTO = frmt(FaF1d(k) * 1000, 1)
        X.CurrentX = X1 - X.TextWidth(TESTO) / 2 '+ ICl
        X.CurrentY = Y1
        X.FontBold = (Abs(FaF1d(k)) = faMAX(1))
        X.Print TESTO
        X.FontBold = False
        j = 1
        X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
        Y1 = MTop + 13.5 + j * iRG
        TESTO = frmt(ffaF1d(k) * 1000, 1)
        X.CurrentX = X1 - X.TextWidth(TESTO) / 2 '+ ICl
        X.CurrentY = Y1
        X.FontBold = (Abs(ffaF1d(k)) = ffaMAX(1))
        X.Print TESTO
        X.FontBold = False
        j = 2
        X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
        Y1 = MTop + 13.5 + j * iRG
        TESTO = frmt(SEGNO_fha * fhaF1d(k) * 1000, 1)
        X.CurrentX = X1 - X.TextWidth(TESTO) / 2 '+ ICl
        X.CurrentY = Y1
        X.FontBold = (Abs(fhaF1d(k)) = fhaMAX(1))
        X.Print TESTO
        X.FontBold = False
      End If
    Next k
    '
    If (SiSTIMA_BOMBATURA = 1) Then
      BombaturaMAX = 0
      For k = 1 To 4
        If (iPd(k) <> 0) Then
          If (BombaturaMAX <= Abs(Bombatura(k))) Then BombaturaMAX = Abs(Bombatura(k))
        End If
      Next k
      For k = 1 To 4
        If (iPd(k) <> 0) Then
          j = 3
          X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
          Y1 = MTop + 13.5 + j * iRG
          TESTO = Abs(frmt(Bombatura(k) * 1000, 1))
          X.CurrentX = X1 - X.TextWidth(TESTO) / 2
          X.CurrentY = Y1
          X.FontBold = (Abs(Bombatura(k)) = BombaturaMAX)
          X.Print TESTO
          X.FontBold = False
        End If
      Next k
    End If
    '
    X1 = (MLeft + 3 + (Box1W - Box1W / 10)) - 1
    X2 = X1 + 2
    Y1 = (MTop + 4 + Box1H / 2) + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
    Y2 = Y1
    'x.Line (X1, Y1)-(X2, Y2), QBColor(0)
    TESTO = frmt(SAP, 3)
    X.CurrentX = X2 + iCl
    X.CurrentY = Y2 - iRG / 2
    X.Print TESTO
    X1 = (MLeft + 3 + (Box1W - Box1W / 10)) - 1
    X2 = X1 + 2
    Y1 = (MTop + 4 + Box1H / 2) - (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
    Y2 = Y1
    'x.Line (X1, Y1)-(X2, Y2), QBColor(0)
    TESTO = frmt(EAP, 3)
    X.CurrentX = X2 + iCl
    X.CurrentY = Y2 - iRG / 2
    X.Print TESTO
    '
    'Inizio e fine valutazione
    If (CHK_Ini_PROFILO > 1) Then
      X1 = (MLeft + 3 + (Box1W - Box1W / 10)) - 1
      X2 = X1 + 2
      Y1 = (MTop + 4 + Box1H / 2) + (HyP(Np) - 2 * HyP(CHK_Ini_PROFILO)) * Sc1Y / 10 / 2
      Y2 = Y1
      X.Line (X1, Y1)-(X2, Y2), vbBlue
      tDia = Idx2Dia(CHK_Ini_PROFILO)
      TESTO = frmt(tDia, 3)
      X.CurrentX = X2 + iCl
      X.CurrentY = Y1 - iRG / 2
      X.Print TESTO
    End If
    If (CHK_Fin_PROFILO < Np) Then
      X1 = (MLeft + 3 + (Box1W - Box1W / 10)) - 1
      X2 = X1 + 2
      Y1 = (MTop + 4 + Box1H / 2) - (-HyP(Np) + 2 * HyP(CHK_Fin_PROFILO)) * Sc1Y / 10 / 2
      Y2 = Y1
      X.Line (X1, Y1)-(X2, Y2), vbBlue
      tDia = Idx2Dia(CHK_Fin_PROFILO)
      TESTO = frmt(tDia, 3)
      X.CurrentX = X2 + iCl
      X.CurrentY = Y1 - iRG / 2
      X.Print TESTO
    End If
    '
  End If
  '
  'Scale X e Y del controllo PROFILO
  X.DrawStyle = 0
  X1 = MLeft: Y1 = MTop + 4
  X2 = MLeft: Y2 = MTop + 13.5
  If (SEGNO_fha = 1) Then
    Testa = comVisPROELI(2, 1)
    Piede = comVisPROELI(3, 1)
  End If
  If (SEGNO_fha = -1) Then
    Testa = comVisPROELI(3, 1)
    Piede = comVisPROELI(2, 1)
  End If
  Call VisScXY(X, Sc1X, Sc1Y, 1, 1)
  X.DrawStyle = 0
  '
  stmp = GetInfo("CHK0", "TOLLERANZA_PROFILO", Path_LAVORAZIONE_INI): stmp = UCase$(Trim$(stmp))
  If (stmp <> "Y") Then Exit Sub
  '
  'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  Open PathMIS & "\" & "DEFAULT" For Input As 1
    Line Input #1, stmp
    Line Input #1, stmp
    Line Input #1, stmp
    Input #1, ftmp, rsp
    If (rsp = 2) Then
      Input #1, k1, ftmp, k1, k1, k1, k1, CODICE_PEZZO
    Else
      CODICE_PEZZO = GetInfo("CHK0", "FileMisura", PathFILEINI): CODICE_PEZZO = UCase$(Trim$(CODICE_PEZZO))
      'LEGGO COMUNQUE FINO AL PRIMO SPAZIO
      rsp = InStr(CODICE_PEZZO, " "): If rsp > 0 Then CODICE_PEZZO = Left$(CODICE_PEZZO, rsp - 1)
      'LEGGO COMUNQUE FINO AL PRIMO PUNTO
      rsp = InStr(CODICE_PEZZO, "."): If rsp > 0 Then CODICE_PEZZO = Left$(CODICE_PEZZO, rsp - 1)
    End If
  Close #1
  '
  'LETTURA DELLA TOLLERANZA DAL DATABASE
  Set DBB = OpenDatabase(PathDB, True, False)
  Set RSS = DBB.OpenRecordset("ARCHIVIO_" & TIPO_LAVORAZIONE)
    RSS.Index = "NOME_PEZZO"
    RSS.Seek "=", CODICE_PEZZO
    If Not RSS.NoMatch Then
      If (Len(RSS.Fields("TOLLERANZA_PROFILO").Value) > 0) Then
        lstTOLLERANZA = UCase$(Trim$(RSS.Fields("TOLLERANZA_PROFILO").Value))
      Else
        lstTOLLERANZA = ""
      End If
    End If
  RSS.Close
  DBB.Close
  '
  'RICAVO I SINGOLI VALORI DALLA LISTA
  If (Len(lstTOLLERANZA) > 0) Then
    Dim NumeroPuntiUsatiF(2)  As Integer
    Dim DDp                   As Double
    Dim DDb                   As Double
    Dim EAP_TIF               As Double
    Dim SAP_TIF               As Double
    DDp = ModNor * NZ / Cos(Abs(AngEli) * PG / 180)
    DDb = DDp * Cos(Atn(Tan(AngPreNor * PG / 180) / Cos(Abs(AngEli) * PG / 180)))
    SAP_TIF = Sqr(SAP ^ 2 - DDb ^ 2)
    EAP_TIF = Sqr(EAP ^ 2 - DDb ^ 2)
    k1 = InStr(lstTOLLERANZA, ";;")
    If (k1 <= 0) Then
      k1 = InStr(lstTOLLERANZA, ";")
      k2 = InStr(k1 + 1, lstTOLLERANZA, ";")
      k3 = InStr(k2 + 1, lstTOLLERANZA, ";")
      k4 = 1
      For j = 1 To 8
        pXX(1, j) = val(Mid$(lstTOLLERANZA, k4, k1 - k4))
        pYY(1, j) = val(Mid$(lstTOLLERANZA, k1 + 1, k2 - k1 - 1))
        pBB(1, j) = val(Mid$(lstTOLLERANZA, k2 + 1, k3 - k2 - 1))
        k4 = k3 + 1
        k1 = InStr(k4, lstTOLLERANZA, ";")
        k2 = InStr(k1 + 1, lstTOLLERANZA, ";")
        k3 = InStr(k2 + 1, lstTOLLERANZA, ";")
      Next j
      For j = 1 To 8
        pXX(2, j) = val(Mid$(lstTOLLERANZA, k4, k1 - k4))
        pYY(2, j) = val(Mid$(lstTOLLERANZA, k1 + 1, k2 - k1 - 1))
        pBB(2, j) = val(Mid$(lstTOLLERANZA, k2 + 1, k3 - k2 - 1))
        k4 = k3 + 1
        k1 = InStr(k4, lstTOLLERANZA, ";")
        k2 = InStr(k1 + 1, lstTOLLERANZA, ";")
        k3 = InStr(k2 + 1, lstTOLLERANZA, ";")
      Next j
    End If
    For j = 1 To 8
      If pYY(1, j) > DDb Then pYY(1, j) = Sqr(pYY(1, j) ^ 2 - DDb ^ 2)
      If pYY(2, j) > DDb Then pYY(2, j) = Sqr(pYY(2, j) ^ 2 - DDb ^ 2)
    Next
    i = 1
    NumeroPuntiUsatiF(1) = 8
      For j = 1 To 8
        If (pYY(i, j) = 0) And (pXX(i, j) = 0) Then
          For k1 = j To 8
            pYY(i, k1) = pYY(i, 1):  pXX(i, k1) = pXX(i, 1)
            NumeroPuntiUsatiF(1) = NumeroPuntiUsatiF(1) - 1
          Next k1
          Exit For
        End If
      Next j
    i = 2
    NumeroPuntiUsatiF(2) = 8
      For j = 1 To 8
        If (pYY(i, j) = 0) And (pXX(i, j) = 0) Then
          For k1 = j To 8
            pYY(i, k1) = pYY(i, 1):  pXX(i, k1) = pXX(i, 1)
            NumeroPuntiUsatiF(2) = NumeroPuntiUsatiF(2) - 1
          Next k1
          Exit For
        End If
      Next j
    For j = 1 To 2
      pXX(j, 9) = pXX(j, 1)
      pYY(j, 9) = pYY(j, 1)
    Next j
    'FIANCO 2 TOLLERANZA --> FIANCO 1 PEZZO
    If NumeroPuntiUsatiF(2) > 1 Then
      For k = 1 To 4
        SiDisegna = False
        Select Case k
          Case 1:                     SiDisegna = True
          Case 2: If iPd(2) <> 0 Then SiDisegna = True
          Case 3: If iPd(3) <> 0 Then SiDisegna = True
          Case 4: If iPd(4) <> 0 Then SiDisegna = True
        End Select
        If SiDisegna Then
          For i = 1 To 8
          If (i <> 8) And (pBB(2, i) = 0) Then
            X1 = (MLeft + 3 + k * Box1W / 10)
            X1 = X1 + pXX(2, i) * Sc1X / 10
            Y1 = (MTop + 4 + Box1H / 2)
            Y1 = Y1 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
            Y1 = Y1 - (pYY(2, i) / 2 - SAP_TIF / 2) * Sc1Y / 10
            X2 = (MLeft + 3 + k * Box1W / 10)
            X2 = X2 + pXX(2, i + 1) * Sc1X / 10
            Y2 = (MTop + 4 + Box1H / 2)
            Y2 = Y2 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
            Y2 = Y2 - (pYY(2, i + 1) / 2 - SAP_TIF / 2) * Sc1Y / 10
            X.Line (X1, Y1)-(X2, Y2), QBColor(9)
          Else
            Call visRACCORDA_DUE_PUNTI(X, pXX(2, i), (HyP(Np) - HyP(1)) / 2 - (pYY(2, i) / 2 - SAP_TIF / 2), pXX(2, i + 1), (HyP(Np) - HyP(1)) / 2 - (pYY(2, i + 1) / 2 - SAP_TIF / 2), pBB(2, i), "SX", (MLeft + 3 + k * Box1W / 10), (MTop + 4 + Box1H / 2), Sc1X / 10, Sc1Y / 10)
          End If
          Next i
        End If
      Next k
      i = 1
      X.FillStyle = 0
      X.FillColor = QBColor(12)
      X1 = (MLeft + 3 + Box1W / 10)
      X1 = X1 + pXX(2, i) * Sc1X / 10
      Y1 = (MTop + 4 + Box1H / 2)
      Y1 = Y1 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
      Y1 = Y1 - (pYY(2, i) / 2 - SAP_TIF / 2) * Sc1Y / 10
      X.Circle (X1, Y1), 0.05
      X.FillStyle = 1
      X.FillColor = QBColor(0)
      X.Line (X1 - 0.05, Y1)-(X1 + 0.05, Y1), QBColor(0)
      X.Line (X1, Y1 - 0.05)-(X1, Y1 + 0.05), QBColor(0)
    End If
    'FIANCO 1 TOLLERANZA --> FIANCO 2 PEZZO
    If NumeroPuntiUsatiF(1) > 1 Then
      For k = 1 To 4
        SiDisegna = False
        Select Case k
          Case 1:                     SiDisegna = True
          Case 2: If iPd(2) <> 0 Then SiDisegna = True
          Case 3: If iPd(3) <> 0 Then SiDisegna = True
          Case 4: If iPd(4) <> 0 Then SiDisegna = True
        End Select
        If SiDisegna Then
          For i = 1 To 8
          If (i <> 8) And (pBB(1, i) = 0) Then
            X1 = (MLeft + 3 + (Box1W - k * Box1W / 10))
            X1 = X1 + pXX(1, i) * Sc1X / 10
            Y1 = (MTop + 4 + Box1H / 2)
            Y1 = Y1 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
            Y1 = Y1 - (pYY(1, i) / 2 - SAP_TIF / 2) * Sc1Y / 10
            X2 = (MLeft + 3 + (Box1W - k * Box1W / 10))
            X2 = X2 + pXX(1, i + 1) * Sc1X / 10
            Y2 = (MTop + 4 + Box1H / 2)
            Y2 = Y2 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
            Y2 = Y2 - (pYY(1, i + 1) / 2 - SAP_TIF / 2) * Sc1Y / 10
            X.Line (X1, Y1)-(X2, Y2), QBColor(9)
          Else
            Call visRACCORDA_DUE_PUNTI(X, pXX(1, i), (HyP(Np) - HyP(1)) / 2 - (pYY(1, i) / 2 - SAP_TIF / 2), pXX(1, i + 1), (HyP(Np) - HyP(1)) / 2 - (pYY(1, i + 1) / 2 - SAP_TIF / 2), pBB(1, i), "SX", (MLeft + 3 + (Box1W - k * Box1W / 10)), (MTop + 4 + Box1H / 2), Sc1X / 10, Sc1Y / 10)
          End If
          Next i
        End If
      Next k
      i = 1
      X.FillStyle = 0
      X.FillColor = QBColor(12)
      X1 = (MLeft + 3 + (Box1W - Box1W / 10))
      X1 = X1 + pXX(1, i) * Sc1X / 10
      Y1 = (MTop + 4 + Box1H / 2)
      Y1 = Y1 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
      Y1 = Y1 - (pYY(1, i) / 2 - SAP_TIF / 2) * Sc1Y / 10
      X.Circle (X1, Y1), 0.05
      X.FillStyle = 1
      X.FillColor = QBColor(0)
      X.Line (X1 - 0.05, Y1)-(X1 + 0.05, Y1), QBColor(0)
      X.Line (X1, Y1 - 0.05)-(X1, Y1 + 0.05), QBColor(0)
    End If
  End If
  '
Exit Sub

STIMA_BOMBATURA_PROFILO_FIANCO1:
  'VISUALIZZAZIONE RETTA DI REGRESSIONE
  If (SiOpt(4, 0) = 1) Then
    'Call CalcoloMretta(HyP, Pd0f1, M, Q, CHK_Ini_PROFILO, CHK_Fin_PROFILO)
    M = fhaF1d(k) / (HyP(CHK_Fin_PROFILO) - HyP(CHK_Ini_PROFILO))
    X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
    X1 = X1 - M * (HyP(CHK_Fin_PROFILO) - HyP(CHK_Ini_PROFILO)) * Sc1X / 10 / 2
    Y1 = (MTop + 4 + Box1H / 2)
    Y1 = Y1 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
    Y1 = Y1 - (HyP(CHK_Ini_PROFILO) - HyP(1)) * Sc1Y / 10
    X2 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
    X2 = X2 + M * (HyP(CHK_Fin_PROFILO) - HyP(CHK_Ini_PROFILO)) * Sc1X / 10 / 2
    Y2 = (MTop + 4 + Box1H / 2)
    Y2 = Y2 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
    Y2 = Y2 - (HyP(CHK_Fin_PROFILO) - HyP(1)) * Sc1Y / 10
    X.DrawWidth = 2
    X.Line (X1, Y1)-(X2, Y2), QBColor(12)
    X.DrawWidth = 1
  End If
  '
  'STIMA BOMBATURA CON VISUALIZZAZIONE DELL' ARCO DI CERCHIO SVG 07/11/09
  If (SiSTIMA_BOMBATURA = 1) Then
    Call CalcoloCerchio(HyP, Pd0f1, AM, BM, RM)
    '
    X1 = AM + Sqr(RM ^ 2 - (Pd0f1(1) - BM) ^ 2):  Y1 = HyP(1)
    X2 = AM + Sqr(RM ^ 2 - (Pd0f1(Np) - BM) ^ 2): Y2 = HyP(Np)
    ANGOLO = Sqr((X2 - X1) ^ 2 + (Y2 - Y1) ^ 2)
    ANGOLO = FnASN(ANGOLO / 2 / RM)
    '
    Bombatura(k) = Abs(RM * (1 - Cos(ANGOLO)))
    '
    ffaF1d(k) = 0: RRB_max = -1E+99: RRB_min = 1E+99
    Select Case k
    Case 1
      For i = 1 To Np
        RRB_cur = Sqr((Pd1f1(i) - AM) ^ 2 + (HyP(i) - BM) ^ 2)
        If (RRB_cur > RRB_max) Then RRB_max = RRB_cur
        If (RRB_cur < RRB_min) Then RRB_min = RRB_cur
      Next i
    Case 2
      For i = 1 To Np
        RRB_cur = Sqr((Pd2f1(i) - AM) ^ 2 + (HyP(i) - BM) ^ 2)
        If (RRB_cur > RRB_max) Then RRB_max = RRB_cur
        If (RRB_cur < RRB_min) Then RRB_min = RRB_cur
      Next i
    Case 3
      For i = 1 To Np
        RRB_cur = Sqr((Pd3f1(i) - AM) ^ 2 + (HyP(i) - BM) ^ 2)
        If (RRB_cur > RRB_max) Then RRB_max = RRB_cur
        If (RRB_cur < RRB_min) Then RRB_min = RRB_cur
      Next i
    Case 4
      For i = 1 To Np
        RRB_cur = Sqr((Pd4f1(i) - AM) ^ 2 + (HyP(i) - BM) ^ 2)
        If (RRB_cur > RRB_max) Then RRB_max = RRB_cur
        If (RRB_cur < RRB_min) Then RRB_min = RRB_cur
      Next i
    End Select
    ffaF1d(k) = RRB_max - RRB_min
    '
    X.DrawWidth = 2
    If (AM > 0) Then
      For i = 1 To Np - 1
        X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
        X1 = X1 + (AM - Sqr(RM ^ 2 - (HyP(i) - BM) ^ 2) - MV(Pd0f1, CHK_Ini_PROFILO, CHK_Fin_PROFILO)) * Sc1X / 10
        Y1 = (MTop + 4 + Box1H / 2)
        Y1 = Y1 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
        Y1 = Y1 - HyP(i) * Sc1Y / 10
        '
        X2 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
        X2 = X2 + (AM - Sqr(RM ^ 2 - (HyP(i + 1) - BM) ^ 2) - MV(Pd0f1, CHK_Ini_PROFILO, CHK_Fin_PROFILO)) * Sc1X / 10
        Y2 = (MTop + 4 + Box1H / 2)
        Y2 = Y2 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
        Y2 = Y2 - HyP(i + 1) * Sc1Y / 10
        '
        X.Line (X1, Y1)-(X2, Y2), QBColor(12)
      Next i
    Else
      For i = 1 To Np - 1
        X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
        X1 = X1 + (AM + Sqr(RM ^ 2 - (HyP(i) - BM) ^ 2) - MV(Pd0f1, CHK_Ini_PROFILO, CHK_Fin_PROFILO)) * Sc1X / 10
        Y1 = (MTop + 4 + Box1H / 2)
        Y1 = Y1 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
        Y1 = Y1 - HyP(i) * Sc1Y / 10
        '
        X2 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
        X2 = X2 + (AM + Sqr(RM ^ 2 - (HyP(i + 1) - BM) ^ 2) - MV(Pd0f1, CHK_Ini_PROFILO, CHK_Fin_PROFILO)) * Sc1X / 10
        Y2 = (MTop + 4 + Box1H / 2)
        Y2 = Y2 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
        Y2 = Y2 - HyP(i + 1) * Sc1Y / 10
        '
        X.Line (X1, Y1)-(X2, Y2), QBColor(12)
      Next i
    End If
    X.DrawWidth = 1
    '
  End If
  '
Return

STIMA_BOMBATURA_PROFILO_FIANCO2:
  'VISUALIZZAZIONE RETTA DI REGRESSIONE
  If (SiOpt(4, 0) = 1) Then
    'Call CalcoloMretta(HyP, Pd0f2, M, Q, CHK_Ini_PROFILO, CHK_Fin_PROFILO)
    M = fhaF2d(k) / (HyP(CHK_Fin_PROFILO) - HyP(CHK_Ini_PROFILO))
    X1 = (MLeft + 3 + k * (Box1W / 10))
    X1 = X1 + M * (HyP(CHK_Fin_PROFILO) - HyP(CHK_Ini_PROFILO)) * Sc1X / 10 / 2
    Y1 = (MTop + 4 + Box1H / 2)
    Y1 = Y1 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
    Y1 = Y1 - (HyP(CHK_Ini_PROFILO) - HyP(1)) * Sc1Y / 10
    X2 = (MLeft + 3 + k * (Box1W / 10))
    X2 = X2 - M * (HyP(CHK_Fin_PROFILO) - HyP(CHK_Ini_PROFILO)) * Sc1X / 10 / 2
    Y2 = (MTop + 4 + Box1H / 2)
    Y2 = Y2 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
    Y2 = Y2 - (HyP(CHK_Fin_PROFILO) - HyP(1)) * Sc1Y / 10
    X.DrawWidth = 2
    X.Line (X1, Y1)-(X2, Y2), QBColor(12)
    X.DrawWidth = 1
  End If
  '
  'STIMA BOMBATURA CON VISUALIZZAZIONE DELL' ARCO DI CERCHIO SVG 07/11/09
  If (SiSTIMA_BOMBATURA = 1) Then
    Call CalcoloCerchio(HyP, Pd0f2, AM, BM, RM)
    '
    X1 = AM + Sqr(RM ^ 2 - (Pd0f2(1) - BM) ^ 2):  Y1 = HyP(1)
    X2 = AM + Sqr(RM ^ 2 - (Pd0f2(Np) - BM) ^ 2): Y2 = HyP(Np)
    ANGOLO = Sqr((X2 - X1) ^ 2 + (Y2 - Y1) ^ 2)
    ANGOLO = FnASN(ANGOLO / 2 / RM)
    '
    If AM > 0 Then RM = -RM
    Bombatura(k) = Abs(RM * (1 - Cos(ANGOLO)))
    '
    ffaF2d(k) = 0: RRB_max = -1E+99: RRB_min = 1E+99
    Select Case k
    Case 1
      For i = 1 To Np
        RRB_cur = Sqr((Pd1f2(i) - AM) ^ 2 + (HyP(i) - BM) ^ 2)
        If (RRB_cur > RRB_max) Then RRB_max = RRB_cur
        If (RRB_cur < RRB_min) Then RRB_min = RRB_cur
      Next i
    Case 2
      For i = 1 To Np
        RRB_cur = Sqr((Pd2f2(i) - AM) ^ 2 + (HyP(i) - BM) ^ 2)
        If (RRB_cur > RRB_max) Then RRB_max = RRB_cur
        If (RRB_cur < RRB_min) Then RRB_min = RRB_cur
      Next i
    Case 3
      For i = 1 To Np
        RRB_cur = Sqr((Pd3f2(i) - AM) ^ 2 + (HyP(i) - BM) ^ 2)
        If (RRB_cur > RRB_max) Then RRB_max = RRB_cur
        If (RRB_cur < RRB_min) Then RRB_min = RRB_cur
      Next i
    Case 4
      For i = 1 To Np
        RRB_cur = Sqr((Pd4f2(i) - AM) ^ 2 + (HyP(i) - BM) ^ 2)
        If (RRB_cur > RRB_max) Then RRB_max = RRB_cur
        If (RRB_cur < RRB_min) Then RRB_min = RRB_cur
      Next i
    End Select
    ffaF2d(k) = RRB_max - RRB_min
    '
    X.DrawWidth = 2
    If (AM > 0) Then
      For i = 1 To Np - 1
        X1 = (MLeft + 3 + k * (Box1W / 10))
        X1 = X1 - (AM - Sqr(RM ^ 2 - (HyP(i) - BM) ^ 2) - MV(Pd0f2, CHK_Ini_PROFILO, CHK_Fin_PROFILO)) * Sc1X / 10
        Y1 = (MTop + 4 + Box1H / 2)
        Y1 = Y1 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
        Y1 = Y1 - HyP(i) * Sc1Y / 10
        '
        X2 = (MLeft + 3 + k * (Box1W / 10))
        X2 = X2 - (AM - Sqr(RM ^ 2 - (HyP(i + 1) - BM) ^ 2) - MV(Pd0f2, CHK_Ini_PROFILO, CHK_Fin_PROFILO)) * Sc1X / 10
        Y2 = (MTop + 4 + Box1H / 2)
        Y2 = Y2 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
        Y2 = Y2 - HyP(i + 1) * Sc1Y / 10
        '
        X.Line (X1, Y1)-(X2, Y2), QBColor(12)
      Next i
    Else
      For i = 1 To Np - 1
        X1 = (MLeft + 3 + k * (Box1W / 10))
        X1 = X1 - (AM + Sqr(RM ^ 2 - (HyP(i) - BM) ^ 2) - MV(Pd0f2, CHK_Ini_PROFILO, CHK_Fin_PROFILO)) * Sc1X / 10
        Y1 = (MTop + 4 + Box1H / 2)
        Y1 = Y1 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
        Y1 = Y1 - HyP(i) * Sc1Y / 10
        '
        X2 = (MLeft + 3 + k * (Box1W / 10))
        X2 = X2 - (AM + Sqr(RM ^ 2 - (HyP(i + 1) - BM) ^ 2) - MV(Pd0f2, CHK_Ini_PROFILO, CHK_Fin_PROFILO)) * Sc1X / 10
        Y2 = (MTop + 4 + Box1H / 2)
        Y2 = Y2 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
        Y2 = Y2 - HyP(i + 1) * Sc1Y / 10
        '
        X.Line (X1, Y1)-(X2, Y2), QBColor(12)
      Next i
    End If
    X.DrawWidth = 1
    '
  End If
  '
Return

errVisPROFILO:
  WRITE_DIALOG "SUB VisPROFILO ERROR " & str(Err)
  Resume Next

End Sub

Sub visRACCORDA_DUE_PUNTI(ByVal OGGETTO As Object, ByVal X1 As Double, ByVal Y1 As Double, ByVal X2 As Double, ByVal Y2 As Double, ByVal Bombatura As Double, ByVal TIPO As String, ByVal OX As Double, ByVal OY As Double, ByVal SCx As Double, ByVal SCy As Double)

Dim j     As Integer
Dim stmp  As String
Dim dx    As Double
Dim Dy    As Double
Dim pX1   As Double
Dim pX2   As Double
Dim pY1   As Double
Dim pY2   As Double
Dim ColoreCurva As Integer
Dim AngEnd   As Double
Dim AngStart As Double
Dim DeltaAng As Double
Dim Ang      As Double
Dim R164 As Double
Dim R165 As Double
Dim R166 As Double
Dim R167 As Double
Dim R168 As Double
Dim R169 As Double
Dim R160 As Double
Dim R161 As Double
Dim R162 As Double
Dim R163 As Double
Dim R170 As Double
Dim R171 As Double
Dim R172 As Double
Dim R173 As Double
Dim R174 As Double
Dim NumeroArchi As Integer
'
On Error GoTo errvisRACCORDA_DUE_PUNTI
  '
  If (Bombatura = 0) Then Exit Sub
  'RISOLUZIONE DELL'ARCO DI CERCHIO
  NumeroArchi = 30
  'COLORE DEL TRATTO
  Select Case TIPO
    Case "SX": ColoreCurva = 12
    Case "DX": ColoreCurva = 9
  End Select
  'COORD. X             COORD. Y
  R164 = X1:            R165 = Y1
  pX1 = (X1 + X2) / 2:  pY1 = (Y1 + Y2) / 2
  R168 = X2:            R169 = Y2
  If (Bombatura <> 0) Then
    'BETA
    If (X2 <> X1) Then
      Ang = Abs(Atn((Y2 - Y1) / (X2 - X1)))
    Else
      Ang = PG / 2
    End If
  End If
  '
  If (X1 - pX1 >= 0) And ((Y1 - pY1 >= 0)) Then 'QUADRANTE 1
    Select Case TIPO
      Case "SX": R166 = pX1 + Bombatura * Sin(Ang): R167 = pY1 - Bombatura * Cos(Ang)
      Case "DX": R166 = pX1 - Bombatura * Sin(Ang): R167 = pY1 + Bombatura * Cos(Ang)
    End Select
  End If
  If (X1 - pX1 < 0) And ((Y1 - pY1 >= 0)) Then 'QUADRANTE 2
    Select Case TIPO
      Case "SX": R166 = pX1 + Bombatura * Sin(Ang): R167 = pY1 + Bombatura * Cos(Ang)
      Case "DX": R166 = pX1 - Bombatura * Sin(Ang): R167 = pY1 - Bombatura * Cos(Ang)
    End Select
  End If
  If (X1 - pX1 < 0) And ((Y1 - pY1 <= 0)) Then 'QUADRANTE 3
    Select Case TIPO
      Case "SX": R166 = pX1 - Bombatura * Sin(Ang): R167 = pY1 + Bombatura * Cos(Ang)
      Case "DX": R166 = pX1 + Bombatura * Sin(Ang): R167 = pY1 - Bombatura * Cos(Ang)
    End Select
  End If
  If (X1 - pX1 >= 0) And ((Y1 - pY1 <= 0)) Then 'QUADRANTE 4
    Select Case TIPO
      Case "SX": R166 = pX1 - Bombatura * Sin(Ang): R167 = pY1 - Bombatura * Cos(Ang)
      Case "DX": R166 = pX1 + Bombatura * Sin(Ang): R167 = pY1 + Bombatura * Cos(Ang)
    End Select
  End If
  pX2 = R166: pY2 = R167
  '
  'LINEA TRA I PUNTI MEDI
  pX1 = OX + pX1 * SCx: pY1 = OY + pY1 * SCy
  pX2 = OX + pX2 * SCx: pY2 = OY + pY2 * SCy
  'OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
  '
  'DISTANZA PER TRE PUNTI
  R162 = Abs(Bombatura)
  If (R162 < 0.0005) Then
    'TRATTO RETTILINEO
    pX1 = OX + R164 * SCx: pY1 = OY + R165 * SCy
    pX2 = OX + R168 * SCx: pY2 = OY + R169 * SCy
    OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
  Else
    'TRATTO CURVILINEO
    If R169 = R167 Then R169 = R167 + 0.00001
    If R165 = R167 Then R165 = R167 + 0.00001
    R160 = (R164 - R166) / (R167 - R165)
    R171 = (R166 - R168) / (R169 - R167)
    R172 = (R165 + R167) / 2 - R160 * (R164 + R166) / 2
    R173 = (R169 + R167) / 2 - R171 * (R168 + R166) / 2
    If R160 = R171 Then R160 = R171 + 0.00001
    'coordinate del centro
    R174 = (R173 - R172) / (R160 - R171)
    R170 = R160 * R174 + R172
    'raggio del cerchio
    R161 = Sqr((R174 - R166) * (R174 - R166) + (R170 - R167) * (R170 - R167))
    'prodotto misto per senso di rotazione
    R160 = (R166 - R164) * (R169 - R165) - (R168 - R164) * (R167 - R165)
    'Calcolo angoli compresi tra 0 e 2 * Pigreco. --------------------------
    AngStart = CALCOLO_ANGOLO(R164 - R174, R165 - R170)
    AngEnd = CALCOLO_ANGOLO(R168 - R174, R169 - R170)
    If (R160 > 0) Then
      If AngEnd >= AngStart Then Ang = AngEnd - AngStart Else Ang = 2 * PG - (AngStart - AngEnd)
      DeltaAng = Ang / NumeroArchi
      pX1 = R164: pY1 = R165
      For j = 1 To NumeroArchi - 1
        Ang = (AngStart + j * DeltaAng)
        pX2 = R174 + R161 * Cos(Ang): pY2 = R170 + R161 * Sin(Ang)
        '
        pX1 = OX + pX1 * SCx: pY1 = OY + pY1 * SCy
        pX2 = OX + pX2 * SCx: pY2 = OY + pY2 * SCy
        OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
        'Aggiorno Px1 e Py1
        pX1 = R174 + R161 * Cos(Ang): pY1 = R170 + R161 * Sin(Ang)
      Next j
      pX2 = R168: pY2 = R169
      '
      pX1 = OX + pX1 * SCx: pY1 = OY + pY1 * SCy
      pX2 = OX + pX2 * SCx: pY2 = OY + pY2 * SCy
      OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
    Else
      If AngStart >= AngEnd Then Ang = AngStart - AngEnd Else Ang = 2 * PG - (AngEnd - AngStart)
      DeltaAng = Ang / NumeroArchi
      pX1 = R164: pY1 = R165
      For j = 1 To NumeroArchi - 1
        Ang = (AngStart - j * DeltaAng)
        pX2 = R174 + R161 * Cos(Ang): pY2 = R170 + R161 * Sin(Ang)
        '
        pX1 = OX + pX1 * SCx: pY1 = OY + pY1 * SCy
        pX2 = OX + pX2 * SCx: pY2 = OY + pY2 * SCy
        OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
        'Aggiorno Px1 e Py1
        pX1 = R174 + R161 * Cos(Ang): pY1 = R170 + R161 * Sin(Ang)
      Next j
      pX2 = R168: pY2 = R169
      '
      pX1 = OX + pX1 * SCx: pY1 = OY + pY1 * SCy
      pX2 = OX + pX2 * SCx: pY2 = OY + pY2 * SCy
      OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
    End If
  End If
  
Exit Sub

errvisRACCORDA_DUE_PUNTI:
  WRITE_DIALOG "SUB visRACCORDA_DUE_PUNTI: ERROR --> " & Error(Err)
  Resume Next
  
End Sub

Sub CHK0_AZIONI(Action As String, ByVal TIPO_LAVORAZIONE As String)
'
Dim Atmp                  As String * 255
Dim itmp                  As Integer
Dim dtmp                  As Double
Dim stmp1                 As String
Dim stmp                  As String
Dim riga                  As String
Dim i                     As Integer
Dim k                     As Integer
Dim rsp                   As Integer
Dim ret                   As Integer
Dim NomeFile              As String
Dim NomeDIR               As String
Dim INCREMENTO_AUTOMATICO As String
Dim RADICE                As String
Dim DISCHETTO             As String
Dim LISTA_FILE()          As String
'
Dim COORDINATE         As String
Dim PERCORSO           As String
Dim PERCORSO_CONTROLLI As String
Dim PERCORSO_PROFILI   As String
Dim sequenza           As String
Dim npt                As Integer
Dim TY_ROT             As String
Dim PASSO              As Double
Dim CONTROLLORE        As String
Dim DM_ROT             As String
Dim INVERSIONE         As Integer


On Error GoTo errCHK0_AZIONI
  '
  Select Case Action
    '
    Case "CORREZIONE"
      Select Case TIPO_LAVORAZIONE
        Case "ROTORI_ESTERNI": Call ROTORI1.CALCOLO_CORRETTORI_MOLA
        Case "PROFILO_RAB_ESTERNI": Call ROTORI1.CALCOLO_CORRETTORI_MOLA_RAB
      End Select
      'Call CONTROLLO_CREATORI_MODIFICA_CORRETTORI_MOLA
      
    Case "SELEZIONA_DATA":  Call CHK0_SELEZIONA_PER("DATA")
    Case "SELEZIONA_NOME":  Call CHK0_SELEZIONA_PER("NOME")
    Case "SELEZIONA_TUTTI": Call CHK0_SELEZIONA_PER("TUTTI")
    Case "TOLLERANZA":      Call MODIFICA_TOLLERANZA_CONTROLLO(TIPO_LAVORAZIONE)
    
    Case "SALVARE"
      Select Case TIPO_LAVORAZIONE
      
        Case "ROTORI_ESTERNI"
          WRITE_DIALOG "ARCHIVIAZIONE ROTORE"
          
        Case Else
          stmp = CHK0.Text2.Text
          Call ELIMINA_CARATTERI(stmp)
          CHK0.Text2.Text = stmp
          If (CHK0.Text2.Text <> "DEFAULT") Then
            If (Len(CHK0.Text2.Text) > 0) Then
              INCREMENTO_AUTOMATICO = GetInfo("CHK0", "INCREMENTO_AUTOMATICO", Path_LAVORAZIONE_INI)
              INCREMENTO_AUTOMATICO = UCase$(Trim$(INCREMENTO_AUTOMATICO))
              If (INCREMENTO_AUTOMATICO = "Y") Then
                'RICAVO LA RADICE
                ret = InStr(CHK0.Text2.Text, ".")
                If (ret <= 0) Then
                  RADICE = CHK0.Text2.Text
                Else
                  RADICE = Left$(CHK0.Text2.Text, ret - 1)
                End If
                'RICERCA DELL'INDICE PIU' GRANDE
                stmp = Dir(PathMIS & "\")
                stmp = UCase(Trim$(stmp))
                k = 0
                Do While (stmp <> "")
                  stmp = UCase(Trim$(stmp))
                  i = InStr(stmp, RADICE)
                  If (i > 0) Then
                    ret = InStr(stmp, ".")
                    If (ret > 0) Then
                      k = val(Right$(stmp, Len(stmp) - ret))
                      If (k > rsp) Then rsp = k
                    End If
                  End If
                  stmp = Dir
                Loop
                If (rsp >= 998) Then rsp = 998
                'COSTRUISCO IL NOME COMPLETO DEL FILE
                CHK0.Text2.Text = RADICE & "." & Format$(rsp + 1, "000")
              End If
              'COSTRUISCO LA DOMANDA
              ret = LoadString(g_hLanguageLibHandle, 1002, Atmp, 255)
              If (ret > 0) Then riga = Left$(Atmp, ret)
              StopRegieEvents
              rsp = MsgBox(riga & " " & CHK0.Text2.Text & " ? ", 4 + 32, "WARNING: " & CHK0.INTESTAZIONE.Caption)
              ResumeRegieEvents
              If (rsp = 6) Then
                WRITE_DIALOG ""
                'NOME DEL FILE SORGENTE
                stmp = PathMIS & "\" & "DEFAULT"
                If (Dir$(stmp) <> "") Then
                  'NOME DEL FILE DESTINAZIONE
                  NomeFile = PathMIS & "\" & CHK0.Text2.Text
                  If (Dir$(NomeFile) <> "") Then
                    'LA DESTINAZIONE ESISTE E CHIEDE CONFERMA PER SOVRASCRIVERE
                    ret = LoadString(g_hLanguageLibHandle, 1002, Atmp, 255)
                    If (ret > 0) Then riga = Left$(Atmp, ret)
                    StopRegieEvents
                    rsp = MsgBox(riga & " " & CHK0.Text2.Text & " ? ", 256 + 4 + 32, "WARNING: " & Error(58))
                    ResumeRegieEvents
                    'CANCELLO LA DESTINAZIONE PRIMA DI SOVRASCRIVERE
                    If (rsp = 6) Then Kill NomeFile
                  Else
                    'LA DESTINAZIONE NON ESISTE
                    rsp = 6
                  End If
                  If (rsp = 6) Then
                    FileCopy stmp, NomeFile
                    CHK0.File1.Refresh
                    ret = WritePrivateProfileString("CHK0", "FileMisura", CHK0.Text2.Text, Path_LAVORAZIONE_INI)
                  Else
                    WRITE_DIALOG "Operation aborted by user!!!"
                  End If
                Else
                  WRITE_DIALOG "Operation cannot be performed!!!"
                End If
              Else
                WRITE_DIALOG "Operation aborted by user!!!"
              End If
            Else
              WRITE_DIALOG "Operation cannot be performed!!!"
            End If
          Else
            WRITE_DIALOG Error(58)
          End If
      End Select
      '
    Case "CARICARE"
      ret = LoadString(g_hLanguageLibHandle, 1002, Atmp, 255)
      If (ret > 0) Then riga = Left$(Atmp, ret)
      'CARICAMENTO DEL CONTROLLO DAL CNC
      stmp = LEGGI_NomePezzo(TIPO_LAVORAZIONE)
      StopRegieEvents
      'rsp = MsgBox(riga & " " & stmp & " --> DEFAULT ? ", 256 + 4 + 32, "WARNING: " & CHK0.Intestazione.Caption)
      WRITE_DIALOG riga & " " & stmp & " --> DEFAULT"
      ResumeRegieEvents
      rsp = 6
      If (rsp = 6) Then
        CHK0.Text2.Text = "DEFAULT"
        ret = WritePrivateProfileString("CHK0", "FileMisura", CHK0.Text2.Text, Path_LAVORAZIONE_INI)
        'Leggo il controllo dal CNC e lo copio su DEFAULT
        stmp = PathMIS & "\" & CHK0.Text2.Text
        WRITE_DIALOG "Reading data from CNC."
        Select Case TIPO_LAVORAZIONE
          '
'          Case "PROFILO_RAB_ESTERNI"
'            If LeggiDDE_INGRANAGGI(0) Then
'              Call ScriviFile_INGRANAGGI(stmp)
'              CHK0.File1.Refresh
'              Call VISUALIZZA_CONTROLLO
'            End If
'            For i = 0 To CHK0.File1.ListCount - 1
'              If CHK0.File1.Selected(i) Then CHK0.File1.Selected(i) = False
'            Next i
'            CHK0.File1.Refresh
            '
          Case "INGR380_ESTERNI", "SCANALATIEVO", "DENTATURA", "MOLAVITE", "MOLAVITE_SG", "MOLAVITEG160", "INGRANAGGI_ESTERNIV", "INGRANAGGI_ESTERNIO"
            If LeggiDDE_INGRANAGGI(0) Then
              If (TIPO_LAVORAZIONE = "INGR380_ESTERNI") Then
                 stmp1 = GetInfo("INGR380_ESTERNI", "SOVRAMETALLO", Path_LAVORAZIONE_INI)
              End If
              If (TIPO_LAVORAZIONE = "INGRANAGGI_ESTERNIV") Then
                 stmp1 = GetInfo("INGRANAGGI_ESTERNIV", "SOVRAMETALLO", Path_LAVORAZIONE_INI)
              End If
              If (TIPO_LAVORAZIONE = "SCANALATIEVO") Then
                 stmp1 = GetInfo("SCANALATIEVO", "SOVRAMETALLO", Path_LAVORAZIONE_INI)
              End If
              If (TIPO_LAVORAZIONE = "INGRANAGGI_ESTERNIO") Then
                 stmp1 = GetInfo("INGRANAGGI_ESTERNIO", "SOVRAMETALLO", Path_LAVORAZIONE_INI)
              End If
              If (stmp1 = "Y") Then
                'ACQUISIZIONE CONTROLLO SOVRAMETALLO
                NomeDIR = LEGGI_DIRETTORIO_WKS(TIPO_LAVORAZIONE)
                Set Session = GetObject("@SinHMIMCDomain.MCDomain")
                Session.CopyNC "/NC/WKS.DIR/" & NomeDIR & ".WPD/SOVRAMETALLO.SPF", PathMIS & "\SOVRAMETALLO", MCDOMAIN_COPY_NC
                Set Session = Nothing
              End If
              Call ScriviFile_INGRANAGGI(stmp)
              CHK0.File1.Refresh
              Call VISUALIZZA_CONTROLLO
            End If
            For i = 0 To CHK0.File1.ListCount - 1
              If CHK0.File1.Selected(i) Then CHK0.File1.Selected(i) = False
            Next i
            CHK0.File1.Refresh
            '
          Case "INGRANAGGI_INTERNI"
            If LeggiDDE_INGRANAGGI(1) Then
              Call ScriviFile_INGRANAGGI(stmp)
              CHK0.File1.Refresh
              Call VISUALIZZA_CONTROLLO
            End If
            For i = 0 To CHK0.File1.ListCount - 1
              If CHK0.File1.Selected(i) Then CHK0.File1.Selected(i) = False
            Next i
            CHK0.File1.Refresh
            '
          Case "PROFILO_RAB_ESTERNI"
            CHK0.Picture1.Cls
            'centro il grafico
            CHK0.Text1(0).Text = "0"
            CHK0.Text1(1).Text = "0"
            'annullo la scala profilo
            ret = WritePrivateProfileString("CHK0", "ScalaProfiloROTORI", "0", Path_LAVORAZIONE_INI)
            '
            Call LEGGI_INFO_RAB(RADICE, npt, NP1, TY_ROT, PASSO, CONTROLLORE, DM_ROT)
            
            'stmp = GetInfo("PROFILO_RAB_ESTERNI", "NomePezzo(1)", Path_LAVORAZIONE_INI)
            'RADICE = g_chOemPATH & "\RAB_ESTERNI\" & stmp
            'TY_ROT = "FRONT"
            'CONTROLLORE = "INTERNAL"
             
            Select Case CONTROLLORE
              Case "INTERNAL"
                  k = CHK0.Combo1.ListIndex
                  stmp = GetInfo("CONTROLLO", "itemCOD(" & Format$(k, "#0") & ")", Path_LAVORAZIONE_INI)
                  stmp = UCase$(Trim$(stmp))
                  If (stmp = "LEAD") Then
'                    'LETTURA RISULTATO CONTROLLO ELICA
'                    stmp = GetInfo("ELICA", "Abilitazione", Path_LAVORAZIONE_INI): stmp = UCase$(Trim$(stmp))
'                    If (stmp = "Y") Then
'                      stmp1 = GetInfo("ELICA", "LetturaDirettaMGUD", Path_LAVORAZIONE_INI): stmp1 = UCase$(Trim$(stmp1))
'                      If (stmp1 = "Y") Then
'                        'LETTURA DIRETTA DEI PARAMETRI MGUD
'                        Call LEGGI_CNC_CONTROLLO_ROTORI_ELICA(RADICE)
'                      Else
'                        'LETTURA DEL SOTTOPROGRAMMA RESULTS CREATO DAL PROGRAMMA DI CONTROLLO
'                        If (Dir$(RADICE & "\RESULTS.SPF") <> "") Then Kill RADICE & "\RESULTS.SPF"
'                        Set Session = GetObject("@SinHMIMCDomain.MCDomain")
'                        Session.CopyNC "/NC/WKS.DIR/PARAMETRI" & Format$(INDICE_LAVORAZIONE_SINGOLA, "###0") & ".WPD/RESULTS.SPF", RADICE & "\RESULTS.SPF", MCDOMAIN_COPY_NC
'                        Set Session = Nothing
'                      End If
'                    End If
                  Else
                    'LETTURA PARAMETRI MGUD DEL CONTROLLO PROFILO
                    Call LEGGI_CNC_CONTROLLO_RAB(RADICE)
                    Call ROTORI1.ACQUISIZIONE_CONTROLLO_GAPP4(RADICE)
                  End If
                  Call VISUALIZZA_CONTROLLO
'              Case "KLINGELNBERG"
'                  Call ROTORI1.ACQUISIZIONE_CONTROLLO_KLINGELNBERG
'                  Call VISUALIZZA_CONTROLLO
            End Select
          
          
          Case "ROTORI_ESTERNI"
'            Dim COORDINATE         As String
'            'Dim RADICE             As String
'            Dim PERCORSO           As String
'            Dim PERCORSO_CONTROLLI As String
'            Dim PERCORSO_PROFILI   As String
'            Dim sequenza           As String
'            Dim npt                As Integer
'            Dim TY_ROT             As String
'            Dim PASSO              As Double
'            Dim CONTROLLORE        As String
'            Dim DM_ROT             As String
'            Dim INVERSIONE         As Integer
            CHK0.Picture1.Cls
            'centro il grafico
            CHK0.Text1(0).Text = "0"
            CHK0.Text1(1).Text = "0"
            'annullo la scala profilo
            ret = WritePrivateProfileString("CHK0", "ScalaProfiloROTORI", "0", Path_LAVORAZIONE_INI)
            Call LEGGI_INFO_ROTORI(PERCORSO, COORDINATE, RADICE, PERCORSO_CONTROLLI, PERCORSO_PROFILI, sequenza, npt, TY_ROT, PASSO, CONTROLLORE, DM_ROT, INVERSIONE)
            Select Case CONTROLLORE
              Case "INTERNAL"
                  k = CHK0.Combo1.ListIndex
                  stmp = GetInfo("CONTROLLO", "itemCOD(" & Format$(k, "#0") & ")", Path_LAVORAZIONE_INI)
                  stmp = UCase$(Trim$(stmp))
                  If (stmp = "LEAD") Then
                    'LETTURA RISULTATO CONTROLLO ELICA
                    stmp = GetInfo("ELICA", "Abilitazione", Path_LAVORAZIONE_INI): stmp = UCase$(Trim$(stmp))
                    If (stmp = "Y") Then
                      stmp1 = GetInfo("ELICA", "LetturaDirettaMGUD", Path_LAVORAZIONE_INI): stmp1 = UCase$(Trim$(stmp1))
                      If (stmp1 = "Y") Then
                        'LETTURA DIRETTA DEI PARAMETRI MGUD
                        Call LEGGI_CNC_CONTROLLO_ROTORI_ELICA(RADICE)
                      Else
                        'LETTURA DEL SOTTOPROGRAMMA RESULTS CREATO DAL PROGRAMMA DI CONTROLLO
                        If (Dir$(RADICE & "\RESULTS.SPF") <> "") Then Kill RADICE & "\RESULTS.SPF"
                        Set Session = GetObject("@SinHMIMCDomain.MCDomain")
                        Session.CopyNC "/NC/WKS.DIR/PARAMETRI" & Format$(INDICE_LAVORAZIONE_SINGOLA, "###0") & ".WPD/RESULTS.SPF", RADICE & "\RESULTS.SPF", MCDOMAIN_COPY_NC
                        Set Session = Nothing
                      End If
                    End If
                  Else
                    'LETTURA PARAMETRI MGUD DEL CONTROLLO PROFILO
                    Call LEGGI_CNC_CONTROLLO_ROTORI(RADICE)
                    Call ROTORI1.ACQUISIZIONE_CONTROLLO_GAPP4(RADICE)
                  End If
                  Call VISUALIZZA_CONTROLLO
              Case "KLINGELNBERG"
                  Call ROTORI1.ACQUISIZIONE_CONTROLLO_KLINGELNBERG
                  Call VISUALIZZA_CONTROLLO
              Case "ACCURA"
                  Call ROTORI1.ACQUISIZIONE_CONTROLLO_ZEISS_ACCURA
                  Call VISUALIZZA_CONTROLLO
              Case "DEA"
                  Call ROTORI1.ACQUISIZIONE_CONTROLLO_DEA_IMAGE
                  Call VISUALIZZA_CONTROLLO
              Case "QUINDOS"
                  Call ROTORI1.ACQUISIZIONE_CONTROLLO_QUINDOS
                  Call VISUALIZZA_CONTROLLO
            End Select
            '
          Case "CREATORI_STANDARD"
            CHK0.Picture1.Cls
            Call LEGGI_CNC_CONTROLLO_CREATORI(stmp)
            Call CONTROLLO_CREATORI_VISUALIZZAZIONE(0, 0)
            '
          Case "CREATORI_FORMA"  'FlagFORMA 008
            CHK0.Picture1.Cls
            Call LEGGI_CNC_CONTROLLO_CRFORMA(stmp)
            Call CONTROLLO_CRFORMA_VISUALIZZAZIONE(0, 0)
            '
          Case "VITI_ESTERNE", "VITI_MULTIFILETTO", "VITIV_ESTERNE"
            CHK0.Picture1.Cls
            NomeDIR = LEGGI_DIRETTORIO_WKS(TIPO_LAVORAZIONE)
            If (Len(NomeDIR) > 0) Then
              'ACQUISIZIONE DEL RISULTATO DEL CONTROLLO VITI
              Set Session = GetObject("@SinHMIMCDomain.MCDomain")
              Session.CopyNC "/NC/WKS.DIR/" & NomeDIR & ".WPD/RESULTS.SPF", PathMIS & "\DEFAULT", MCDOMAIN_COPY_NC
              Session.CopyNC "/NC/WKS.DIR/" & NomeDIR & ".WPD/DIAMETRI.SPF", PathMIS & "\DIAMETRI", MCDOMAIN_COPY_NC
              Set Session = Nothing
              'AGGIUNGO IL NOME DEL PEZZO ALL'INTERNO DEL FILE
              Dim RIGHE() As String
              If (Dir$(PathMIS & "\DEFAULT") <> "") Then
                Open PathMIS & "\DEFAULT" For Input As #1
                  For i = 1 To 5
                    ReDim Preserve RIGHE(i)
                    Line Input #1, RIGHE(i)
                  Next i
                  ReDim Preserve RIGHE(i)
                  RIGHE(i) = LEGGI_NomePezzo(TIPO_LAVORAZIONE)
                  While Not EOF(1)
                    i = i + 1
                    ReDim Preserve RIGHE(i)
                    Line Input #1, RIGHE(i)
                  Wend
                Close #1
                Open PathMIS & "\DEFAULT" For Output As #1
                  For i = 1 To UBound(RIGHE)
                    Print #1, RIGHE(i)
                  Next i
                Close #1
                CHK0.File1.Refresh
                Call VISUALIZZA_CONTROLLO
              End If
            End If
            '
          Case Else: WRITE_DIALOG "SOFTKEY DISENABLED"
            '
        End Select
        '
      Else
        WRITE_DIALOG "Operation aborted by user!!!"
      End If
      '
    Case "CANCELLARE"
      WRITE_DIALOG ""
      k = 0
      stmp = ""
      For i = 0 To CHK0.File1.ListCount - 1
        If (CHK0.File1.Selected(i) = True) Then
          k = k + 1
          stmp = stmp & Format$(k, "#0) - ") & UCase$(CHK0.File1.List(i))
          stmp = stmp & Chr$(13)
        End If
      Next i
      If (k > 10) Then
        'VISUALIZZO SOLO QUANTI NE SONO SELEZIONATI
        stmp = Format$(k, "########") & "/" & Format$(CHK0.File1.ListCount, "########")
      End If
      If (k > 0) Then
        ret = LoadString(g_hLanguageLibHandle, 1004, Atmp, 255)
        If (ret > 0) Then riga = Left$(Atmp, ret)
        StopRegieEvents
        rsp = MsgBox(riga & " " & Chr$(13) & Chr$(13) & stmp & " ?", 256 + 4 + 32, "WARNING: " & CHK0.INTESTAZIONE.Caption)
        ResumeRegieEvents
        If (rsp = 6) Then
          For i = 0 To CHK0.File1.ListCount - 1
            If CHK0.File1.Selected(i) = True Then
              ret = LoadString(g_hLanguageLibHandle, 1004, Atmp, 255)
              If (ret > 0) Then riga = Left$(Atmp, ret)
              Call Kill(CHK0.File1.Path & "\" & CHK0.File1.List(i))
              CHK0.File1.Selected(i) = False
              Call WRITE_DIALOG(riga & " " & CHK0.File1.Path & "\" & CHK0.File1.List(i) & " OK!!")
            End If
          Next i
          CHK0.File1.Refresh
        Else
          WRITE_DIALOG "Operation aborted by user!!!"
        End If
      Else
        WRITE_DIALOG "Operation cannot be performed!!!"
      End If
      '
    Case "ESPORTARE"
      WRITE_DIALOG "": k = 0: stmp = ""
      For i = 0 To CHK0.File1.ListCount - 1
        If (CHK0.File1.Selected(i) = True) Then
          k = k + 1
          stmp = stmp & Format$(k, "#0) - ") & UCase$(CHK0.File1.List(i))
          stmp = stmp & Chr$(13)
        End If
      Next i
      If (k > 10) Then
        'VISUALIZZO SOLO QUANTI NE SONO SELEZIONATI
        stmp = Format$(k, "########") & "/" & Format$(CHK0.File1.ListCount, "########")
      End If
      If (k > 0) Then
        'LETTURA DEL DRIVE A O PENDRIVE
        DISCHETTO = GetInfo("Configurazione", "DISCHETTO", PathFILEINI)
        DISCHETTO = UCase$(Trim$(DISCHETTO))
        If (Len(DISCHETTO) <= 0) Then DISCHETTO = "A:"
        ret = LoadString(g_hLanguageLibHandle, 1007, Atmp, 255)
        If (ret > 0) Then riga = Left$(Atmp, ret)
        StopRegieEvents
        rsp = MsgBox(riga & Chr$(13) & Chr$(13) & stmp & Chr$(13) & " --> " & DISCHETTO & "\ ", 256 + 4 + 32, "WARNING: " & CHK0.INTESTAZIONE.Caption)
        ResumeRegieEvents
        If (rsp = 6) Then
          For i = 0 To CHK0.File1.ListCount - 1
            If CHK0.File1.Selected(i) = True Then
              WRITE_DIALOG CHK0.File1.Path & "\" & CHK0.File1.List(i) & " --> " & DISCHETTO & ":\"
              Call FileCopy(CHK0.File1.Path & "\" & CHK0.File1.List(i), DISCHETTO & "\" & CHK0.File1.List(i))
              CHK0.File1.Selected(i) = False
            End If
          Next i
          CHK0.File1.Refresh
        Else
          WRITE_DIALOG "Operation aborted by user!!!"
        End If
      Else
        WRITE_DIALOG "Operation cannot be performed!!!"
      End If
      '
    Case "IMPORTARE"
      WRITE_DIALOG "": k = 0: stmp = ""
      'LETTURA DEL DRIVE A O PENDRIVE
      DISCHETTO = GetInfo("Configurazione", "DISCHETTO", PathFILEINI)
      DISCHETTO = UCase$(Trim$(DISCHETTO))
      If (Len(DISCHETTO) <= 0) Then DISCHETTO = "A:"
      'COSTRUISCO LA LISTA DEI FILE SUL DISCHETTO
      riga = Dir(DISCHETTO & "\", vbNormal)
      Do While (riga <> "")
        If riga <> "." And riga <> ".." Then
          If (GetAttr(DISCHETTO & "\" & riga) And vbNormal) = vbNormal Then
            k = k + 1
            ReDim Preserve LISTA_FILE(k)
            LISTA_FILE(k) = riga
          End If
        End If
        riga = Dir
      Loop
      'COSTRUISCO LA LISTA LA STRINGA PER LA DOMANDA DI CONFERMA
      For i = 1 To k
        stmp = stmp & Format$(i, "#0) - ") & UCase$(LISTA_FILE(i))
        stmp = stmp & Chr$(13)
      Next i
      If (k > 10) Then
        'VISUALIZZO SOLO QUANTI CE NE SONO
        stmp = Format$(k, "########")
      End If
      If (k > 0) Then
        ret = LoadString(g_hLanguageLibHandle, 1008, Atmp, 255)
        If (ret > 0) Then riga = Left$(Atmp, ret)
        StopRegieEvents
        rsp = MsgBox(riga & Chr$(13) & Chr$(13) & stmp & Chr$(13), 256 + 4 + 32, "WARNING: " & CHK0.INTESTAZIONE.Caption)
        ResumeRegieEvents
        If (rsp = 6) Then
          For i = 1 To k
            Call WRITE_DIALOG(DISCHETTO & "\" & LISTA_FILE(i) & " --> " & CHK0.File1.Path)
            Call FileCopy(DISCHETTO & "\" & LISTA_FILE(i), CHK0.File1.Path & "\" & LISTA_FILE(i))
          Next i
          CHK0.File1.Refresh
        Else
          WRITE_DIALOG "Operation aborted by user!!!"
        End If
      Else
        WRITE_DIALOG Error(53)
      End If
      '
    Case "STAMPANTE"
      'MODIFICA PER LA STAMPA DEL FILE
      If (CHK0.txtHELP.Visible = True) And (CHK0.Combo1.ListIndex <> 4) Then
        Call STAMPA_CONTENUTO_txtHELP
        Exit Sub
      End If
      'GESTIONE SENZA CONTROLLO DELL'ESISTENZA DI DEFAULT
      If (TIPO_LAVORAZIONE = "ROTORI_ESTERNI") Then
        StopRegieEvents
        stmp = LEGGI_NomePezzo("ROTORI_ESTERNI")
        rsp = MsgBox(stmp & " --> " & Printer.DeviceName & " ? ", 4 + 32, "WARNING: " & CHK0.INTESTAZIONE.Caption)
        ResumeRegieEvents
        If (rsp = 6) Then
          CHK0.txtHELP.Visible = False
          k = CHK0.Combo1.ListIndex
          stmp = GetInfo("CONTROLLO", "itemCOD(" & Format$(k, "#0") & ")", Path_LAVORAZIONE_INI)
          stmp = UCase$(Trim$(stmp))
          If (stmp = "LEAD") Then
            Call VISUALIZZA_CONTROLLO_ELICA(val(CHK0.txtScX.Text), val(CHK0.txtScY.Text), "Y")
          Else
            'PULISCO LO SCHERMO
            Call StampaChkG4
          End If
        Else
          WRITE_DIALOG "Operation aborted by user!!!"
        End If
        Exit Sub
      End If
      If (TIPO_LAVORAZIONE = "PROFILO_RAB_ESTERNI") Then
        StopRegieEvents
        stmp = LEGGI_NomePezzo("PROFILO_RAB_ESTERNI")
        rsp = MsgBox(stmp & " --> " & Printer.DeviceName & " ? ", 4 + 32, "WARNING: " & CHK0.INTESTAZIONE.Caption)
        ResumeRegieEvents
        If (rsp = 6) Then
          CHK0.txtHELP.Visible = False
          k = CHK0.Combo1.ListIndex
          stmp = GetInfo("CONTROLLO", "itemCOD(" & Format$(k, "#0") & ")", Path_LAVORAZIONE_INI)
          stmp = UCase$(Trim$(stmp))
          If (stmp = "LEAD") Then
            Call VISUALIZZA_CONTROLLO_ELICA(val(CHK0.txtScX.Text), val(CHK0.txtScY.Text), "Y")
          Else
            'PULISCO LO SCHERMO
            Call StampaChkG4
          End If
        Else
          WRITE_DIALOG "Operation aborted by user!!!"
        End If
        Exit Sub
      End If
      
      
      stmp = CHK0.Text2.Text
      Call ELIMINA_CARATTERI(stmp)
      CHK0.Text2.Text = stmp
      riga = PathMIS & "\" & CHK0.Text2.Text
      If (Dir$(riga) <> "") And (Len(CHK0.Text2.Text) > 0) Then
        Select Case TIPO_LAVORAZIONE
            '
          Case "CREATORI_STANDARD"
            StopRegieEvents
            rsp = MsgBox(CHK0.Text2.Text & " --> " & Printer.DeviceName & " ? ", 4 + 32, "WARNING: " & CHK0.INTESTAZIONE.Caption)
            ResumeRegieEvents
            If (rsp = 6) Then
              Call CONTROLLO_CREATORI_STAMPA(val(CHK0.Text1(0).Text), val(CHK0.Text1(1).Text))
            Else
              WRITE_DIALOG "Operation aborted by user!!!"
            End If
            '
          Case "VITI_ESTERNE", "VITI_MULTIFILETTO", "VITIV_ESTERNE"
            If (CHK0.Combo1.ListIndex = 0) Or (CHK0.Combo1.ListIndex = 1) Then
              StopRegieEvents
              rsp = MsgBox(CHK0.Text2.Text & Chr(13) & Chr(13) & TitoloPROELI & " --> " & Printer.DeviceName & " ? ", 4 + 32, "WARNING: " & CHK0.INTESTAZIONE.Caption)
              ResumeRegieEvents
              If (rsp = 6) Then
                Call StampaMisuraPeE_VITI
              Else
                WRITE_DIALOG "Operation aborted by user!!!"
              End If
            End If
            If (CHK0.Combo1.ListIndex = 2) Or (CHK0.Combo1.ListIndex = 3) Then
              StopRegieEvents
              rsp = MsgBox(CHK0.Text2.Text & Chr(13) & Chr(13) & TitoloDIV & " --> " & Printer.DeviceName & " ? ", 4 + 32, "WARNING: " & CHK0.INTESTAZIONE.Caption)
              ResumeRegieEvents
              If (rsp = 6) Then
                Call StampaMisuraDIV_VITI
              Else
                WRITE_DIALOG "Operation aborted by user!!!"
              End If
            End If
            If (CHK0.Combo1.ListIndex = 4) Then Call STAMPA_DIAMETRI
            '
          Case Else
            If (CHK0.Combo1.ListIndex = 0) Or (CHK0.Combo1.ListIndex = 1) Then
              StopRegieEvents
              rsp = MsgBox(CHK0.Text2.Text & Chr(13) & Chr(13) & TitoloPROELI & " --> " & Printer.DeviceName & " ? ", 4 + 32, "WARNING: " & CHK0.INTESTAZIONE.Caption)
              ResumeRegieEvents
              If (rsp = 6) Then
                Call StampaMisuraPeE
              Else
                WRITE_DIALOG "Operation aborted by user!!!"
              End If
            End If
            If (CHK0.Combo1.ListIndex = 2) Or (CHK0.Combo1.ListIndex = 3) Or (CHK0.Combo1.ListIndex = 4) Then
              StopRegieEvents
              rsp = MsgBox(CHK0.Text2.Text & Chr(13) & Chr(13) & TitoloDIV & " --> " & Printer.DeviceName & " ? ", 4 + 32, "WARNING: " & CHK0.INTESTAZIONE.Caption)
              ResumeRegieEvents
              If (rsp = 6) Then
                Call StampaMisuraDIV
              Else
                WRITE_DIALOG "Operation aborted by user!!!"
              End If
            End If
            '
        End Select
        '
      Else
        WRITE_DIALOG Error(53) & " " & CHK0.Text2.Text
      End If
      '
    End Select
    '
Exit Sub
      
errCHK0_AZIONI:
  WRITE_DIALOG "Sub CHK0_AZIONI: ERROR -> " & Err
  Resume Next

End Sub

Sub CHK0_SELEZIONA_PER(ByVal TIPO_SELEZIONE As String)
'
Dim i               As Integer
Dim j               As Integer
Dim k               As Integer
Dim DataRiferimento As Date
Dim NomeRiferimento As String
Dim stmp            As String
Dim Atmp            As String * 255
'
On Error Resume Next
  '
  WRITE_DIALOG ""
  Select Case TIPO_SELEZIONE
    '
    Case "DATA"
      'COSTRUISCO LA DOMANDA
      'SELEZIONE PER DATA
      i = LoadString(g_hLanguageLibHandle, 134, Atmp, 255)
      If i > 0 Then stmp = Left$(Atmp, i)
      stmp = stmp & Chr(13) & Chr(13)
      stmp = stmp & CHK0.INTESTAZIONE.Caption & Chr(13) & Chr(13)
      'CONSENTO L'INSERIMENTO DELLA DATA
      StopRegieEvents
      DataRiferimento = InputBox(stmp, "INPUT", Now)
      ResumeRegieEvents
      If DateValue(DataRiferimento) > 0 Then
        i = 0
        For j = 0 To CHK0.File1.ListCount - 1
          If DateValue(FileDateTime(CHK0.File1.Path & "\" & CHK0.File1.List(j))) <= DateValue(DataRiferimento) Then
            CHK0.File1.Selected(j) = True
            i = i + 1
            WRITE_DIALOG "SELECTED " & Format$(i, "########0") & "/" & Format$(CHK0.File1.ListCount, "########0")
          Else
            CHK0.File1.Selected(j) = False
          End If
        Next j
      Else
        WRITE_DIALOG "Operation aborted by user!!!"
      End If
      '
    Case "NOME"
      'COSTRUISCO LA DOMANDA
      stmp = "NOME"
      stmp = stmp & Chr(13) & Chr(13)
      stmp = stmp & CHK0.INTESTAZIONE.Caption & Chr(13) & Chr(13)
      'CONSENTO L'INSERIMENTO DELLA DATA
      StopRegieEvents
      NomeRiferimento = InputBox(stmp, "INPUT", "")
      NomeRiferimento = UCase$(Trim$(NomeRiferimento))
      ResumeRegieEvents
      If Len(NomeRiferimento) > 0 Then
        i = 0
        For j = 0 To CHK0.File1.ListCount - 1
          k = InStr(Left(UCase$(CHK0.File1.List(j)), Len(NomeRiferimento)), NomeRiferimento)
          If k > 0 Then
            CHK0.File1.Selected(j) = True
            i = i + 1
            WRITE_DIALOG "SELECTED " & Format$(i, "########0") & "/" & Format$(CHK0.File1.ListCount, "########0")
          Else
            CHK0.File1.Selected(j) = False
          End If
        Next j
      Else
        WRITE_DIALOG "Operation aborted by user!!!"
      End If
      '
    Case "TUTTI"
      DataRiferimento = Now
      If DateValue(DataRiferimento) > 0 Then
        i = 0
        For j = 0 To CHK0.File1.ListCount - 1
          If DateValue(FileDateTime(CHK0.File1.Path & "\" & CHK0.File1.List(j))) <= DateValue(DataRiferimento) Then
            CHK0.File1.Selected(j) = True
            i = i + 1
            WRITE_DIALOG "SELECTED " & Format$(i, "########0") & "/" & Format$(CHK0.File1.ListCount, "########0")
          Else
            CHK0.File1.Selected(j) = False
          End If
        Next j
      Else
        WRITE_DIALOG "Operation aborted by user!!!"
      End If
      '
  End Select
  '
End Sub

Sub CARICAMENTO_CONTROLLO_DA_ARCHIVIO()
'
Dim i                As Integer
Dim ret              As Integer
Dim NomeFileMisura   As String
Dim TIPO_LAVORAZIONE As String
'
On Error GoTo errCARICAMENTO_CONTROLLO_DA_ARCHIVIO
  '
  'CARICAMENTO DEL CONTROLLO DALL'ARCHIVIO
  WRITE_DIALOG ""
  '
  If CHK0.fraTOLLERANZA.Visible = True Then CHK0.fraTOLLERANZA.Visible = False
  '
  'LEGGO IL NOME DEL CONTROLLO SELEZIONATO
  NomeFileMisura = UCase$(CHK0.File1.FileName)
  If (NomeFileMisura <> "DEFAULT") Then
    'VISUALIZZO NELLA CASELLA
    CHK0.Text2.Text = NomeFileMisura
    'Pulizia Textbox inizio e fine valutazione
    CHK0.Text1(0) = ""
    CHK0.Text1(1) = ""
    CHK0.Text1(2) = ""
    CHK0.Text1(3) = ""
    'SCRIVO NEL FILE INI IL NOME DEL FILE DI MISURA
    ret = WritePrivateProfileString("CHK0", "FileMisura", NomeFileMisura, Path_LAVORAZIONE_INI)
    'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
    TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
    TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
    Select Case TIPO_LAVORAZIONE
      '
      Case "ROTORI_ESTERNI"
        FileCopy PathMIS & "\" & NomeFileMisura, PathMIS & "\" & "DEFAULT"
        CHK0.Text1(0).Text = "0"
        CHK0.Text1(1).Text = "0"
        ret = WritePrivateProfileString("CHK0", "ScalaProfiloROTORI", "0", Path_LAVORAZIONE_INI)
        Call VISUALIZZA_CONTROLLO
      
      Case "CREATORI_STANDARD"
        'COPIO NEL FILE DI DEFAULT
        FileCopy PathMIS & "\" & NomeFileMisura, PathMIS & "\" & "DEFAULT"
        Call CONTROLLO_CREATORI_VISUALIZZAZIONE(0, 0)
        '
      Case "CREATORI_FORMA"   'FlagFORMA 009
        'COPIO NEL FILE DI DEFAULT
        FileCopy PathMIS & "\" & NomeFileMisura, PathMIS & "\" & "DEFAULT"
        Call CONTROLLO_CRFORMA_VISUALIZZAZIONE(0, 0)
        '
      Case "VITI_ESTERNE", "VITI_MULTIFILETTO", "VITIV_ESTERNE"
        If LeggiFile_VITI(PathMIS & "\" & NomeFileMisura) Then
          'COPIO NEL FILE DI DEFAULT
          FileCopy PathMIS & "\" & NomeFileMisura, PathMIS & "\" & "DEFAULT"
          Call VISUALIZZA_CONTROLLO
        End If
        '
      Case Else
        If (NomeFileMisura <> "SOVRAMETALLO") Then
          If LeggiFile_INGRANAGGI(PathMIS & "\" & NomeFileMisura) Then
            'COPIO NEL FILE DI DEFAULT
            FileCopy PathMIS & "\" & NomeFileMisura, PathMIS & "\" & "DEFAULT"
            Call VISUALIZZA_CONTROLLO
          End If
        Else
           'Forzo indice 5 (Sovrametallo) nella combo
           'Nota: Genera evento combo1.click che richiama VISUALIZZA_CONTROLLO
           CHK0.Combo1.ListIndex = 5
        End If
        '
    End Select
    '
    'RIMUOVO LA SELEZIONE
    For i = 0 To CHK0.File1.ListCount - 1
      If CHK0.File1.Selected(i) Then CHK0.File1.Selected(i) = False
    Next i
    '
    'RINFRESCO L'ELENCO DEI CONTROLLI
    CHK0.File1.Refresh
    '
    'PUNTO AL TIPO
    If CHK0.Combo1.Visible = True Then CHK0.Combo1.SetFocus
    '
  Else
    WRITE_DIALOG "Operation cannot be performed!!!"
  End If
    
Exit Sub

errCARICAMENTO_CONTROLLO_DA_ARCHIVIO:
  WRITE_DIALOG "Sub CARICAMENTO_CONTROLLO_DA_ARCHIVIO: ERROR -> " & str(Err)
  Exit Sub

End Sub

Sub CalcoloErrori(INDICE As Integer)
'
Dim Appoggio As Double
Dim Pmedio   As Integer
Dim Kitem    As Integer
Dim E30      As Double
Dim E31      As Double
Dim E33      As Double
Dim E35      As Double
Dim E37      As Double
Dim E82      As Double
Dim ZMax     As Double
Dim Zmin     As Double
Dim Zlen     As Double
Dim iMax     As Integer
Dim iMIN     As Integer
'
On Error Resume Next
  '
  Dim TipoCalcolo As String
  TipoCalcolo = GetInfo("CHK0", "TipoCalcolo", Path_LAVORAZIONE_INI): TipoCalcolo = UCase$(Trim$(TipoCalcolo))
  If (TipoCalcolo = "") Then TipoCalcolo = "SVG"
  '
  If (Np <> 0) And (iPd(INDICE) <> 0) Then
    Select Case INDICE
      Case 1
        For i = 1 To Np
          Pd0f1(i) = Pd1f1(i)
          Pd0f2(i) = Pd1f2(i)
        Next i
      Case 2
        For i = 1 To Np
          Pd0f1(i) = Pd2f1(i)
          Pd0f2(i) = Pd2f2(i)
        Next i
      Case 3
        For i = 1 To Np
          Pd0f1(i) = Pd3f1(i)
          Pd0f2(i) = Pd3f2(i)
        Next i
      Case 4
        For i = 1 To Np
          Pd0f1(i) = Pd4f1(i)
          Pd0f2(i) = Pd4f2(i)
        Next i
    End Select
    If (TipoCalcolo <> "SVG") Then
      'Fianco 1: Calcolo FHa --------------------------------------------------------------
      E31 = 0
      Pmedio = Int((Np + 1) / 2)
      Kitem = 2
      Do
        E31 = E31 + 3 * Pd0f1(Kitem)
        Kitem = Kitem + 1
      Loop While Kitem < Pmedio
      E35 = (Pd0f1(1) * 3 + Pd0f1(Pmedio) + E31) / (1 + 3 * (Pmedio - 1))
      E31 = 0
      Kitem = Pmedio + 1
      Do
        i = Kitem
        E31 = E31 + 3 * (Pd0f1(i))
        Kitem = Kitem + 1
      Loop While Kitem < Np
      E33 = (E33 + Pd0f1(Np) + Pd0f1(Pmedio)) / (2 + 3 * (Pmedio - 2))
      fhaF1d(INDICE) = (E33 - E35) * 2
      Appoggio = Pd0f1(Np) - Pd0f1(1)
      If Abs(fhaF1d(INDICE)) > Abs(Appoggio) Then fhaF1d(INDICE) = Appoggio
      'Fianco 1: Calcolo FFa, Fa ---------------------------------------------------------
      i = 1
      E30 = -999
      E31 = 999
      E82 = -999
      E33 = 999
      Do
        E37 = Pd0f1(i)
        E35 = E37 - fhaF1d(INDICE) * (i - 1) / (Np - 1)
        If E35 >= E82 Then E82 = E35
        If E35 <= E33 Then E33 = E35
        If E37 >= E30 Then E30 = E37
        If E37 <= E31 Then E31 = E37
        i = i + 1
      Loop While i <= Np
      FaF1d(INDICE) = E30 - E31
      ffaF1d(INDICE) = E82 - E33
      'Fianco 2: Calcolo FHa --------------------------------------------------------------
      E31 = 0
      Pmedio = Int((Np + 1) / 2)
      Kitem = 2
      Do
        E31 = E31 + 3 * Pd0f2(Kitem)
        Kitem = Kitem + 1
      Loop While Kitem < Pmedio
      E35 = (Pd0f2(1) * 3 + Pd0f2(Pmedio) + E31) / (1 + 3 * (Pmedio - 1))
      E31 = 0
      Kitem = Pmedio + 1
      Do
        i = Kitem
        E31 = E31 + 3 * (Pd0f2(i))
        Kitem = Kitem + 1
      Loop While Kitem < Np
      E33 = (E33 + Pd0f2(Np) + Pd0f2(Pmedio)) / (2 + 3 * (Pmedio - 2))
      fhaF2d(INDICE) = (E33 - E35) * 2
      Appoggio = Pd0f2(Np) - Pd0f2(1)
      If Abs(fhaF2d(INDICE)) > Abs(Appoggio) Then fhaF2d(INDICE) = Appoggio
      'Fianco 2: Calcolo FFa, Fa ---------------------------------------------------------
      i = 1
      E30 = -999
      E31 = 999
      E82 = -999
      E33 = 999
      Do
        E37 = Pd0f2(i)
        E35 = E37 - fhaF2d(INDICE) * (i - 1) / (Np - 1)
        If E35 >= E82 Then E82 = E35
        If E35 <= E33 Then E33 = E35
        If E37 >= E30 Then E30 = E37
        If E37 <= E31 Then E31 = E37
        i = i + 1
      Loop While i <= Np
      faF2d(INDICE) = E30 - E31
      ffaF2d(INDICE) = E82 - E33
    Else
      Call CalcoloFaFHaFFa_Int(INDICE, 1, Np)
    End If
  End If
  If (NE <> 0) And (iEd(INDICE) <> 0) Then
    Select Case INDICE
      Case 1
        For i = 1 To NE
          Ed0f1(i) = Ed1f1(i)
          Ed0f2(i) = Ed1f2(i)
        Next i
      Case 2
        For i = 1 To NE
          Ed0f1(i) = Ed2f1(i)
          Ed0f2(i) = Ed2f2(i)
        Next i
      Case 3
        For i = 1 To NE
          Ed0f1(i) = Ed3f1(i)
          Ed0f2(i) = Ed3f2(i)
        Next i
      Case 4
        For i = 1 To NE
          Ed0f1(i) = Ed4f1(i)
          Ed0f2(i) = Ed4f2(i)
        Next i
    End Select
    If (TipoCalcolo <> "SVG") Then
      'Fianco 1: CALCOLO FHB --------------------------------------------------------------
      E31 = 0
      Pmedio = Int((NE + 1) / 2)
      Kitem = 2
      Do
        E31 = E31 + 3 * Ed0f1(Kitem)
        Kitem = Kitem + 1
      Loop While Kitem < Pmedio
      E35 = (Ed0f1(1) * 3 + Ed0f1(Pmedio) + E31) / (1 + 3 * (Pmedio - 1))
      E31 = 0
      Kitem = Pmedio + 1
      Do
        i = Kitem
        E31 = E31 + 3 * (Ed0f1(i))
        Kitem = Kitem + 1
      Loop While Kitem < NE
      E33 = (E33 + Ed0f1(NE) + Ed0f1(Pmedio)) / (2 + 3 * (Pmedio - 2))
      fhbF1d(INDICE) = (E33 - E35) * 2
      Appoggio = Ed0f1(NE) - Ed0f1(1)
      If Abs(fhbF1d(INDICE)) > Abs(Appoggio) Then fhbF1d(INDICE) = Appoggio
      '
      'PER ELICA SINISTRA CAMBIO IL SEGNO DI fhb DI F1
      If (AngEli < 0) Then fhbF1d(INDICE) = -fhbF1d(INDICE)
      '
      'Fianco 1: Calcolo FFB, FB ----------------------------------------------------------
      i = 1
      E30 = -999
      E31 = 999
      E82 = -999
      E33 = 999
      Do
        E37 = Ed0f1(i)
        E35 = E37 - fhbF1d(INDICE) * (i - 1) / (NE - 1)
        If E35 >= E82 Then E82 = E35
        If E35 <= E33 Then E33 = E35
        If E37 >= E30 Then E30 = E37
        If E37 <= E31 Then E31 = E37
        i = i + 1
      Loop While i <= NE
      fbF1d(INDICE) = E30 - E31
      ffbF1d(INDICE) = E82 - E33
      'Fianco 2: Calcolo FHB --------------------------------------------------------------
      E31 = 0
      Pmedio = Int((NE + 1) / 2)
      Kitem = 2
      Do
        E31 = E31 + 3 * Ed0f2(Kitem)
        Kitem = Kitem + 1
      Loop While Kitem < Pmedio
      E35 = (Ed0f2(1) * 3 + Ed0f2(Pmedio) + E31) / (1 + 3 * (Pmedio - 1))
      E31 = 0
      Kitem = Pmedio + 1
      Do
        i = Kitem
        E31 = E31 + 3 * (Ed0f2(i))
        Kitem = Kitem + 1
      Loop While Kitem < NE
      E33 = (E33 + Ed0f2(NE) + Ed0f2(Pmedio)) / (2 + 3 * (Pmedio - 2))
      fhbF2d(INDICE) = (E33 - E35) * 2
      Appoggio = Ed0f2(NE) - Ed0f2(1)
      If Abs(fhbF2d(INDICE)) > Abs(Appoggio) Then fhbF2d(INDICE) = Appoggio
      '
      'PER ELICA DESTRA CAMBIO IL SEGNO DI fhb DI F1
      If (AngEli > 0) Then fhbF2d(INDICE) = -fhbF2d(INDICE)
      '
      'Fianco 2: Calcolo FFB, FB ----------------------------------------------------------
      i = 1
      E30 = -999
      E31 = 999
      E82 = -999
      E33 = 999
      Do
        E37 = Ed0f2(i)
        E35 = E37 - fhbF2d(INDICE) * (i - 1) / (NE - 1)
        If E35 >= E82 Then E82 = E35
        If E35 <= E33 Then E33 = E35
        If E37 >= E30 Then E30 = E37
        If E37 <= E31 Then E31 = E37
        i = i + 1
      Loop While i <= NE
      fbF2d(INDICE) = E30 - E31
      ffbF2d(INDICE) = E82 - E33
    Else
      Call CalcoloFbFHbFFb_Int(INDICE, 1, NE)
    End If
  End If
  If DivCon = 1 Then
    'FIANCO 1
    ZMax = Divf1(1): Zmin = Divf1(1)
    For i = 1 To NZ
      If Divf1(i) >= ZMax Then
        ZMax = Divf1(i)
        iMax = i
      End If
      If Divf1(i) <= Zmin Then
        Zmin = Divf1(i)
        iMIN = i
      End If
    Next i
    NZMaxF1 = iMax: ErrMaxF1 = ZMax
    NZMinF1 = iMIN: ErrMinF1 = Zmin
    'FIANCO 2
    ZMax = Divf2(1): Zmin = Divf2(1)
    For i = 1 To NZ
      If Divf2(i) >= ZMax Then
        ZMax = Divf2(i)
        iMax = i
      End If
      If Divf2(i) <= Zmin Then
        Zmin = Divf2(i)
        iMIN = i
      End If
    Next i
    NZMaxF2 = iMax: ErrMaxF2 = ZMax
    NZMinF2 = iMIN: ErrMinF2 = Zmin
    Zlen = ZMax - Zmin
  End If

End Sub

Sub VISUALIZZA_CONTROLLO()
'
Dim i                     As Integer
Dim MLeft                 As Single
Dim MTop                  As Single
Dim TIPO_LAVORAZIONE      As String
Dim stmp                  As String
'
On Error GoTo errVISUALIZZA_CONTROLLO
  '
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  For i = 0 To 5
    CHK0.Text1(i).Visible = False
    CHK0.Label2(i).Visible = False
  Next i
  Select Case TIPO_LAVORAZIONE
    '
    Case "ROTORI_ESTERNI": Call CONTROLLO_ROTORI_VISUALIZZAZIONE
    '
    Case "PROFILO_RAB_ESTERNI": Call CONTROLLO_RAB_VISUALIZZAZIONE
    '
    Case "INGR380_ESTERNI", "SCANALATIEVO", "DENTATURA", "MOLAVITE", "MOLAVITEG160", "INGRANAGGI_ESTERNIV", "INGRANAGGI_ESTERNIO"
      MLeft = val(GetInfo("CHK0", "MLeft", Path_LAVORAZIONE_INI))
      MTop = val(GetInfo("CHK0", "MTop", Path_LAVORAZIONE_INI))
      WRITE_DIALOG ""
      CHK0.Picture1.Cls
      iRG = CHK0.Picture1.TextHeight("A")
      iCl = CHK0.Picture1.TextWidth("A")
      If LeggiFile_INGRANAGGI(PathMIS & "\DEFAULT") Then
        '
        Select Case CHK0.Combo1.ListIndex
          '
          Case 0  'PROFILO
            CHK0.txtScX.Text = GetInfo("CHK0", "ScalaPROFILOx", Path_LAVORAZIONE_INI)
            CHK0.txtScY.Text = GetInfo("CHK0", "ScalaPROFILOy", Path_LAVORAZIONE_INI)
            CHK0.lblScX.Visible = True: CHK0.lblScY.Visible = True
            CHK0.txtScX.Visible = True: CHK0.txtScY.Visible = True
            CHK0.Picture1.ScaleTop = 3
            CHK0.Picture1.ScaleLeft = -0.3 + MLeft
            CHK0.txtHELP.Visible = False
            CHK0.Text1(2).Visible = True
            CHK0.Text1(3).Visible = True
            CHK0.Text1(4).Visible = True
            CHK0.Text1(5).Visible = True
            CHK0.Label2(2).Visible = True
            CHK0.Label2(3).Visible = True
            CHK0.Label2(4).Visible = True
            CHK0.Label2(5).Visible = True
            CHK0.Label2(2) = "D2"
            CHK0.Label2(3) = "D1"
            CHK0.Label2(4) = "fhaF1"
            CHK0.Label2(5) = "fhaF2"
            CHK0.Text1(4).Text = CorPrF1T
            CHK0.Text1(5).Text = CorPrF2T
            Call VisPROFILO(CHK0.Picture1, 1)
            '
          Case 1  'ELICA
            CHK0.txtScX.Text = GetInfo("CHK0", "ScalaELICAx", Path_LAVORAZIONE_INI)
            CHK0.txtScY.Text = GetInfo("CHK0", "ScalaELICAy", Path_LAVORAZIONE_INI)
            CHK0.lblScX.Visible = True
            CHK0.lblScY.Visible = True
            CHK0.txtScX.Visible = True
            CHK0.txtScY.Visible = True
            CHK0.Picture1.ScaleTop = 14.5
            CHK0.Picture1.ScaleLeft = -0.3 + MLeft
            CHK0.txtHELP.Visible = False
            CHK0.Text1(0).Visible = True
            CHK0.Label2(0).Visible = True
            CHK0.Label2(0) = "%"
            CHK0.Label2(4) = "fhbF1"
            CHK0.Label2(5) = "fhbF2"
            CHK0.Text1(4).Text = CorElF1T
            CHK0.Text1(5).Text = CorElF2T
            CHK0.Label2(4).Visible = True
            CHK0.Label2(5).Visible = True
            CHK0.Text1(4).Visible = True
            CHK0.Text1(5).Visible = True
            Call VisELICA(CHK0.Picture1)
            '
          Case 2  'fp DIVISIONE
            CHK0.txtScX.Text = GetInfo("CHK0", "ScalaDIVISIONEx", Path_LAVORAZIONE_INI)
            CHK0.txtScY.Text = GetInfo("CHK0", "ScalaDIVISIONEy", Path_LAVORAZIONE_INI)
            CHK0.lblScX.Visible = True
            CHK0.lblScY.Visible = True
            CHK0.txtScX.Visible = True
            CHK0.txtScY.Visible = True
            CHK0.Picture1.ScaleTop = 3.5
            CHK0.Picture1.ScaleLeft = -0.3 + MLeft
            CHK0.txtHELP.Visible = False
            Call VISUALIZZA_DERIVATA_DIVISIONE(CHK0.Picture1)
            '
          Case 3  'FP DIVISIONE
            CHK0.txtScX.Text = GetInfo("CHK0", "ScalaDIVISIONEx", Path_LAVORAZIONE_INI)
            CHK0.txtScY.Text = GetInfo("CHK0", "ScalaDIVISIONEy", Path_LAVORAZIONE_INI)
            CHK0.lblScX.Visible = True
            CHK0.lblScY.Visible = True
            CHK0.txtScX.Visible = True
            CHK0.txtScY.Visible = True
            CHK0.Picture1.ScaleTop = 11.5
            CHK0.Picture1.ScaleLeft = -0.3 + MLeft
            CHK0.txtHELP.Visible = False
            Call VISUALIZZA_DIVISIONE(CHK0.Picture1)
            '
          Case 4  'Fr DIVISIONE
            CHK0.txtScX.Text = GetInfo("CHK0", "ScalaDIVISIONEx", Path_LAVORAZIONE_INI)
            CHK0.txtScY.Text = GetInfo("CHK0", "ScalaDIVISIONEy", Path_LAVORAZIONE_INI)
            CHK0.lblScX.Visible = True
            CHK0.lblScY.Visible = True
            CHK0.txtScX.Visible = True
            CHK0.txtScY.Visible = True
            CHK0.Picture1.ScaleTop = 19.5
            CHK0.Picture1.ScaleLeft = -0.3 + MLeft
            CHK0.txtHELP.Visible = False
            Call VISUALIZZA_CONCENTRICITA(CHK0.Picture1)
            '
          Case 5 'VISUALIZZA SOVRAMETALLO
           If (TIPO_LAVORAZIONE = "INGR380_ESTERNI") Then stmp = GetInfo("INGR380_ESTERNI", "SOVRAMETALLO", Path_LAVORAZIONE_INI)
           If (TIPO_LAVORAZIONE = "INGRANAGGI_ESTERNIV") Then stmp = GetInfo("INGRANAGGI_ESTERNIV", "SOVRAMETALLO", Path_LAVORAZIONE_INI)
           If (TIPO_LAVORAZIONE = "SCANALATIEVO") Then stmp = GetInfo("SCANALATIEVO", "SOVRAMETALLO", Path_LAVORAZIONE_INI)
           If (TIPO_LAVORAZIONE = "INGRANAGGI_ESTERNIO") Then stmp = GetInfo("INGRANAGGI_ESTERNIO", "SOVRAMETALLO", Path_LAVORAZIONE_INI)
           If (stmp = "Y") Then
              CHK0.txtHELP.BackColor = &H0&
              CHK0.txtHELP.ForeColor = &HFFFFFF
              CHK0.txtHELP.Visible = True
              CHK0.txtHELP.Text = ""
              If Dir$(PathMIS & "\" & "SOVRAMETALLO") <> "" Then
                Open PathMIS & "\" & "SOVRAMETALLO" For Input As #1
                CHK0.txtHELP.Text = Input$(LOF(1), 1)
                Close #1
              End If
           End If
            '
        End Select
        '
      End If
      
    Case "INGRANAGGI_INTERNI"
      MLeft = val(GetInfo("CHK0", "MLeft", Path_LAVORAZIONE_INI))
      MTop = val(GetInfo("CHK0", "MTop", Path_LAVORAZIONE_INI))
      WRITE_DIALOG ""
      CHK0.Picture1.Cls
      iRG = CHK0.Picture1.TextHeight("A")
      iCl = CHK0.Picture1.TextWidth("A")
      If LeggiFile_INGRANAGGI(PathMIS & "\DEFAULT") Then
        '
        Select Case CHK0.Combo1.ListIndex
          '
          Case 0  'PROFILO
            CHK0.txtScX.Text = GetInfo("CHK0", "ScalaPROFILOx", Path_LAVORAZIONE_INI)
            CHK0.txtScY.Text = GetInfo("CHK0", "ScalaPROFILOy", Path_LAVORAZIONE_INI)
            CHK0.lblScX.Visible = True
            CHK0.lblScY.Visible = True
            CHK0.txtScX.Visible = True
            CHK0.txtScY.Visible = True
            CHK0.Picture1.ScaleTop = 3.5
            CHK0.Picture1.ScaleLeft = -0.3 + MLeft
            CHK0.txtHELP.Visible = False
            CHK0.Text1(2).Visible = True
            CHK0.Text1(3).Visible = True
            CHK0.Label2(2).Visible = True
            CHK0.Label2(3).Visible = True
            CHK0.Label2(2) = "D2"
            CHK0.Label2(3) = "D1"
            Call VisPROFILO(CHK0.Picture1, -1)
            '
          Case 1  'ELICA
            CHK0.txtScX.Text = GetInfo("CHK0", "ScalaELICAx", Path_LAVORAZIONE_INI)
            CHK0.txtScY.Text = GetInfo("CHK0", "ScalaELICAy", Path_LAVORAZIONE_INI)
            CHK0.lblScX.Visible = True
            CHK0.lblScY.Visible = True
            CHK0.txtScX.Visible = True
            CHK0.txtScY.Visible = True
            CHK0.Picture1.ScaleTop = 15.5
            CHK0.Picture1.ScaleLeft = -0.3 + MLeft
            CHK0.txtHELP.Visible = False
            CHK0.Text1(0).Visible = True
            CHK0.Label2(0).Visible = True
            CHK0.Label2(0) = "%"
            Call VisELICA(CHK0.Picture1)
            '
          Case 2  'fp DIVISIONE
            CHK0.txtScX.Text = GetInfo("CHK0", "ScalaDIVISIONEx", Path_LAVORAZIONE_INI)
            CHK0.txtScY.Text = GetInfo("CHK0", "ScalaDIVISIONEy", Path_LAVORAZIONE_INI)
            CHK0.lblScX.Visible = True
            CHK0.lblScY.Visible = True
            CHK0.txtScX.Visible = True
            CHK0.txtScY.Visible = True
            CHK0.Picture1.ScaleTop = 3.5
            CHK0.Picture1.ScaleLeft = -0.3 + MLeft
            CHK0.txtHELP.Visible = False
            Call VISUALIZZA_DERIVATA_DIVISIONE(CHK0.Picture1)
            '
          Case 3  'FP DIVISIONE
            CHK0.txtScX.Text = GetInfo("CHK0", "ScalaDIVISIONEx", Path_LAVORAZIONE_INI)
            CHK0.txtScY.Text = GetInfo("CHK0", "ScalaDIVISIONEy", Path_LAVORAZIONE_INI)
            CHK0.lblScX.Visible = True
            CHK0.lblScY.Visible = True
            CHK0.txtScX.Visible = True
            CHK0.txtScY.Visible = True
            CHK0.Picture1.ScaleTop = 11.5
            CHK0.Picture1.ScaleLeft = -0.3 + MLeft
            CHK0.txtHELP.Visible = False
            Call VISUALIZZA_DIVISIONE(CHK0.Picture1)
            '
          Case 4  'Fr DIVISIONE
            CHK0.txtScX.Text = GetInfo("CHK0", "ScalaDIVISIONEx", Path_LAVORAZIONE_INI)
            CHK0.txtScY.Text = GetInfo("CHK0", "ScalaDIVISIONEy", Path_LAVORAZIONE_INI)
            CHK0.lblScX.Visible = True
            CHK0.lblScY.Visible = True
            CHK0.txtScX.Visible = True
            CHK0.txtScY.Visible = True
            CHK0.Picture1.ScaleTop = 19.5
            CHK0.Picture1.ScaleLeft = -0.3 + MLeft
            CHK0.txtHELP.Visible = False
            Call VISUALIZZA_CONCENTRICITA(CHK0.Picture1)
            '
        End Select
      End If
      '
    Case "CREATORI_STANDARD":  Call CONTROLLO_CREATORI_VISUALIZZAZIONE(val(CHK0.Text1(0).Text), val(CHK0.Text1(1).Text))
    Case "CREATORI_FORMA":     Call CONTROLLO_CRFORMA_VISUALIZZAZIONE(val(CHK0.Text1(0).Text), val(CHK0.Text1(1).Text))
      '
    Case "VITI_ESTERNE", "VITI_MULTIFILETTO", "VITIV_ESTERNE"
      MLeft = val(GetInfo("CHK0", "MLeft", Path_LAVORAZIONE_INI))
      MTop = val(GetInfo("CHK0", "MTop", Path_LAVORAZIONE_INI))
      WRITE_DIALOG ""
      CHK0.Picture1.Cls
      iRG = CHK0.Picture1.TextHeight("A")
      iCl = CHK0.Picture1.TextWidth("A")
      If LeggiFile_VITI(PathMIS & "\DEFAULT") Then
        '
        Select Case CHK0.Combo1.ListIndex
          '
          Case 0  'PROFILO
            CHK0.txtScX.Text = GetInfo("CHK0", "ScalaPROFILOx", Path_LAVORAZIONE_INI)
            CHK0.txtScY.Text = GetInfo("CHK0", "ScalaPROFILOy", Path_LAVORAZIONE_INI)
            CHK0.lblScX.Visible = True
            CHK0.lblScY.Visible = True
            CHK0.txtScX.Visible = True
            CHK0.txtScY.Visible = True
            CHK0.Picture1.ScaleTop = 3
            CHK0.Picture1.ScaleLeft = -0.3 + MLeft
            CHK0.txtHELP.Visible = False
            Call VisPROFILO_VITI(CHK0.Picture1, 1)
            '
          Case 1  'ELICA
            CHK0.txtScX.Text = GetInfo("CHK0", "ScalaELICAx", Path_LAVORAZIONE_INI)
            CHK0.txtScY.Text = GetInfo("CHK0", "ScalaELICAy", Path_LAVORAZIONE_INI)
            CHK0.lblScX.Visible = True
            CHK0.lblScY.Visible = True
            CHK0.txtScX.Visible = True
            CHK0.txtScY.Visible = True
            CHK0.Picture1.ScaleTop = 14.5
            CHK0.Picture1.ScaleLeft = -0.3 + MLeft
            CHK0.txtHELP.Visible = False
            Call VisELICA_VITI(CHK0.Picture1)
            '
          Case 2  'fp DIVISIONE
            CHK0.txtScX.Text = GetInfo("CHK0", "ScalaDIVISIONEx", Path_LAVORAZIONE_INI)
            CHK0.txtScY.Text = GetInfo("CHK0", "ScalaDIVISIONEy", Path_LAVORAZIONE_INI)
            CHK0.lblScX.Visible = True
            CHK0.lblScY.Visible = True
            CHK0.txtScX.Visible = True
            CHK0.txtScY.Visible = True
            CHK0.Picture1.ScaleTop = 3.5
            CHK0.Picture1.ScaleLeft = -0.3 + MLeft
            CHK0.txtHELP.Visible = False
            Call VISUALIZZA_DERIVATA_DIVISIONE_VITI(CHK0.Picture1)
            '
          Case 3  'FP DIVISIONE
            CHK0.txtScX.Text = GetInfo("CHK0", "ScalaDIVISIONEx", Path_LAVORAZIONE_INI)
            CHK0.txtScY.Text = GetInfo("CHK0", "ScalaDIVISIONEy", Path_LAVORAZIONE_INI)
            CHK0.lblScX.Visible = True
            CHK0.lblScY.Visible = True
            CHK0.txtScX.Visible = True
            CHK0.txtScY.Visible = True
            CHK0.Picture1.ScaleTop = 11.5
            CHK0.Picture1.ScaleLeft = -0.3 + MLeft
            CHK0.txtHELP.Visible = False
            Call VISUALIZZA_DIVISIONE_VITI(CHK0.Picture1)
            '
          Case 4  'Fr DIVISIONE
            CHK0.txtScX.Text = GetInfo("CHK0", "ScalaDIVISIONEx", Path_LAVORAZIONE_INI)
            CHK0.txtScY.Text = GetInfo("CHK0", "ScalaDIVISIONEy", Path_LAVORAZIONE_INI)
            CHK0.lblScX.Visible = True
            CHK0.lblScY.Visible = True
            CHK0.txtScX.Visible = True
            CHK0.txtScY.Visible = True
            CHK0.Picture1.ScaleTop = 19
            CHK0.Picture1.ScaleLeft = -0.3 + MLeft
            CHK0.txtHELP.Visible = False
            Call VISUALIZZA_CONCENTRICITA_VITI(CHK0.Picture1)
            '
          Case 5 'CONTROLLO DIAMETRI
            CHK0.txtHELP.BackColor = &H0&
            CHK0.txtHELP.ForeColor = &HFFFFFF
            CHK0.txtHELP.Visible = True
            CHK0.txtHELP.Text = ""
            If Dir$(PathMIS & "\" & "DIAMETRI") <> "" Then
              Open PathMIS & "\" & "DIAMETRI" For Input As #1
                CHK0.txtHELP.Text = Input$(LOF(1), 1)
              Close #1
            End If
            '
        End Select
        '
      End If
      '
    Case Else: WRITE_DIALOG ""
    '
  End Select
  '
Exit Sub

errVISUALIZZA_CONTROLLO:
  WRITE_DIALOG "SUB: VISUALIZZA_CONTROLLO " & Error(Err)
  Resume Next

End Sub

Sub INIZIALIZZA_CHK0_PARTE2()
'
Dim i                 As Integer
Dim j                 As Integer
Dim ret               As Integer
Dim stmp              As String
Dim Atmp              As String * 255
Dim TIPO_LAVORAZIONE  As String
'
On Error GoTo errINIZIALIZZA_CHK0_PARTE2
  '
  ret = WritePrivateProfileString("Configurazione", "Modalita", "SPF", PathFILEINI)
  CHK0.Image1.Left = 0
  CHK0.Image1.Top = 0
  CHK0.INTESTAZIONE.Top = CHK0.Image1.Top
  CHK0.INTESTAZIONE.Left = CHK0.Image1.Left + CHK0.Image1.Width
  CHK0.INTESTAZIONE.Width = CHK0.Width - CHK0.INTESTAZIONE.Left
  CHK0.INTESTAZIONE.Height = CHK0.Image1.Height - CHK0.Combo1.Height
  CHK0.Text2.Height = CHK0.Combo1.Height
  CHK0.Picture1.Height = CHK0.Height - CHK0.Image1.Top - CHK0.Image1.Height
  CHK0.File1.Width = 2600
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  Select Case TIPO_LAVORAZIONE
  Case "ROTORI_ESTERNI", "PROFILO_RAB_ESTERNI"
      CHK0.Combo1.Width = 7600
  Case Else
      CHK0.Combo1.Width = 4000
  End Select
  CHK0.Picture1.ScaleWidth = 21
  CHK0.Picture1.ScaleHeight = 15
  CHK0.Picture1.Width = CHK0.Width - CHK0.File1.Width
  CHK0.Combo1.Left = CHK0.Image1.Width
  CHK0.Combo1.Top = CHK0.INTESTAZIONE.Top + CHK0.INTESTAZIONE.Height
  CHK0.Text2.Left = CHK0.Image1.Width + CHK0.Combo1.Width
  CHK0.Text2.Top = CHK0.INTESTAZIONE.Top + CHK0.INTESTAZIONE.Height
  CHK0.Text2.Width = CHK0.INTESTAZIONE.Width - CHK0.Combo1.Width - 5
  CHK0.Picture1.Left = CHK0.Image1.Left
  CHK0.Picture1.Top = CHK0.Image1.Top + CHK0.Image1.Height
  CHK0.Picture1.BackColor = vbWhite
  CHK0.Picture1.ForeColor = vbBlue
  CHK0.txtHELP.Left = 0
  CHK0.txtHELP.Top = CHK0.Picture1.Top
  CHK0.txtHELP.Height = CHK0.Picture1.Height
  If (CHK0.File1.Visible = True) Then
    CHK0.txtHELP.Width = CHK0.Picture1.Width
  Else
    CHK0.txtHELP.Width = CHK0.Picture1.Width + CHK0.File1.Width
  End If
  CHK0.INTESTAZIONE.Caption = RICERCA_NOME_TIPO(TIPO_LAVORAZIONE)
  CHK0.Image2.Left = CHK0.Picture1.Left + CHK0.Picture1.Width
  CHK0.Label1.Left = CHK0.Image2.Left
  CHK0.File1.Left = CHK0.Image2.Left
  CHK0.File1.Width = CHK0.Width - CHK0.Picture1.Width - 5
  CHK0.Label1.Width = CHK0.File1.Width
  CHK0.Image2.Top = CHK0.Picture1.Top + CHK0.Picture1.Height - CHK0.File1.Height - CHK0.Image2.Height
  CHK0.Label1.Top = CHK0.Image2.Top
  CHK0.File1.Top = CHK0.Image2.Top + CHK0.Image2.Height
  CHK0.ChkGRID1.Left = CHK0.Image2.Left
  CHK0.ChkGRID1.Top = CHK0.Image2.Top - CHK0.ChkGRID1.Height
  CHK0.ChkGRID2.Left = CHK0.ChkGRID1.Left + CHK0.ChkGRID1.Width
  CHK0.ChkGRID2.Top = CHK0.ChkGRID1.Top
  CHK0.ChkTAN.Left = CHK0.ChkGRID2.Left + CHK0.ChkGRID2.Width
  CHK0.ChkTAN.Top = CHK0.ChkGRID2.Top
  'INIZIALIZZO LE CASELLE DELLE SCALE
  CHK0.lblScX.Width = (3 / 5) * CHK0.File1.Width
  CHK0.lblScY.Width = CHK0.lblScX.Width
  CHK0.txtScX.Width = CHK0.File1.Width - CHK0.lblScX.Width - 5
  CHK0.txtScY.Width = CHK0.txtScX.Width
  CHK0.lblScX.Left = CHK0.File1.Left
  CHK0.lblScY.Left = CHK0.File1.Left
  CHK0.txtScX.Left = CHK0.lblScX.Left + CHK0.lblScX.Width
  CHK0.txtScY.Left = CHK0.lblScY.Left + CHK0.lblScY.Width
  CHK0.txtScY.Height = CHK0.txtScX.Height
  CHK0.lblScX.Height = CHK0.txtScX.Height
  CHK0.lblScY.Height = CHK0.txtScY.Height
  CHK0.txtScX.Top = CHK0.Text2.Top + CHK0.Text2.Height
  CHK0.txtScY.Top = CHK0.txtScX.Top + CHK0.txtScX.Height
  CHK0.lblScX.Top = CHK0.txtScX.Top
  CHK0.lblScY.Top = CHK0.txtScX.Top + CHK0.lblScX.Height
  For i = 0 To 5
    CHK0.Label2(i).Left = CHK0.lblScX.Left
    CHK0.Label2(i).Width = CHK0.lblScX.Width
    CHK0.Text1(i).Left = CHK0.txtScX.Left
    CHK0.Text1(i).Width = CHK0.txtScX.Width
  Next i
  CHK0.Label2(0).Top = CHK0.lblScY.Top + CHK0.lblScY.Height
  CHK0.Text1(0).Top = CHK0.txtScY.Top + CHK0.txtScY.Height
  For i = 1 To 5
    CHK0.Label2(i).Top = CHK0.Label2(i - 1).Top + CHK0.Label2(i - 1).Height
    CHK0.Text1(i).Top = CHK0.Text1(i - 1).Top + CHK0.Text1(i - 1).Height
  Next i
  i = LoadString(g_hLanguageLibHandle, 1000, Atmp, 255)
  If i > 0 Then CHK0.lblScX.Caption = Left$(Atmp, i)
  i = LoadString(g_hLanguageLibHandle, 1001, Atmp, 255)
  If i > 0 Then CHK0.lblScY.Caption = Left$(Atmp, i)
  'IMPOSTAZIONE CARATTERE
      If (LINGUA = "CH") Then
      stmp = "MS Song"
  ElseIf (LINGUA = "RU") Then
      stmp = "Arial Cyr"
  Else
      stmp = "Arial"
  End If
  For i = 1 To 2
    CHK0.Picture1.FontName = stmp
    CHK0.Picture1.FontSize = 8
    CHK0.Picture1.FontBold = False 'True
  Next i
  'SE CAMBIO IL COLORE NON VISUALIZZA PIU' NIENTE
  CHK0.Picture1.BackColor = &HFFFFFF
  CHK0.Picture1.ForeColor = &H80000008
  CHK0.INTESTAZIONE.BackColor = &HFF&
  CHK0.INTESTAZIONE.ForeColor = &HFFFFFF
  CHK0.Text2.BackColor = &HFFFFFF
  CHK0.lblScX.BackColor = &HFFFFFF
  CHK0.txtScX.BackColor = &HFFFFFF
  CHK0.lblScY.BackColor = &HFFFFFF
  CHK0.txtScY.BackColor = &HFFFFFF
  CHK0.Label2(0).BackColor = &HFFFFFF
  CHK0.Label2(1).BackColor = &HFFFFFF
  CHK0.Label2(2).BackColor = &HFFFFFF
  CHK0.Label2(3).BackColor = &HFFFFFF
  CHK0.Label2(4).BackColor = &HFFFFFF
  CHK0.Label2(5).BackColor = &HFFFFFF
  CHK0.Text1(0).BackColor = &HFFFFFF
  CHK0.Text1(1).BackColor = &HFFFFFF
  CHK0.Text1(2).BackColor = &HFFFFFF
  CHK0.Text1(3).BackColor = &HFFFFFF
  CHK0.Text1(4).BackColor = &HFFFFFF
  CHK0.Text1(5).BackColor = &HFFFFFF
  CHK0.BackColor = &HEBE1D7
  CHK0.Label1.BackColor = &HFFFF&
  CHK0.File1.BackColor = &HFFFFFF
  CHK0.lstARCHIVIO.BackColor = &HFFFFFF
  CHK0.txtHELP.BackColor = &HFFFFFF
  For i = 0 To 7
    CHK0.txtPXXF1(i).BackColor = &HFFFFFF
    CHK0.txtPYYF1(i).BackColor = &HFFFFFF
    CHK0.txtPBBF1(i).BackColor = &HFFFFFF
    CHK0.txtPXXF2(i).BackColor = &HFFFFFF
    CHK0.txtPYYF2(i).BackColor = &HFFFFFF
    CHK0.txtPBBF2(i).BackColor = &HFFFFFF
  Next i
  CHK0.ChkGRID1.BackColor = &HEBE1D7:  CHK0.ChkGRID1.Value = val(GetInfo("CHK0", "Griglia1", Path_LAVORAZIONE_INI))
  CHK0.ChkGRID2.BackColor = &HEBE1D7:  CHK0.ChkGRID2.Value = val(GetInfo("CHK0", "Griglia2", Path_LAVORAZIONE_INI))
  CHK0.ChkTAN.BackColor = &HEBE1D7:    CHK0.ChkTAN.Value = val(GetInfo("CHK0", "SiOpt(4,0)", Path_LAVORAZIONE_INI))
  '
Exit Sub

errINIZIALIZZA_CHK0_PARTE2:
  WRITE_DIALOG "SUB: INIZIALIZZA_CHK0_PARTE2 ERROR --> " & str(Err)
  Resume Next
  
End Sub

Sub INIZIALIZZA_CHK0_PARTE1()
'
Dim i                 As Integer
Dim j                 As Integer
Dim k                 As Integer
Dim riga              As String
Dim ret               As Integer
Dim stmp              As String
Dim Atmp              As String * 255
Dim TIPO_LAVORAZIONE  As String
Dim SiStampaControlli As String * 1
'
On Error GoTo errINIZIALIZZA_CHK0_PARTE1
  '
  'NASCONDO I TASTI DELLA SECONDA RIGA
  For i = 0 To 7
    MDIForm1.Picture3(i).Visible = False
  Next i
  'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI): TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  'LEGGO IL PERCORSO PER IL DIRETTORIO DEI CONTROLLI
  PathMIS = GetInfo(TIPO_LAVORAZIONE, "PathMIS", Path_LAVORAZIONE_INI): PathMIS = UCase$(Trim$(PathMIS))
  If (TIPO_LAVORAZIONE = "ROTORI_ESTERNI") Then
    stmp = GetInfo("Configurazione", "PERCORSO_CONTROLLI", Path_LAVORAZIONE_INI)
    stmp = UCase$(Trim$(stmp))
    If (Len(stmp) > 0) Then CHK0.cmdABILITA.Visible = True Else CHK0.cmdABILITA.Visible = False
    Dim COORDINATE         As String
    Dim RADICE             As String
    Dim PERCORSO           As String
    Dim PERCORSO_CONTROLLI As String
    Dim PERCORSO_PROFILI   As String
    Dim sequenza           As String
    Dim npt                As Integer
    Dim TY_ROT             As String
    Dim PASSO              As Double
    Dim CONTROLLORE        As String
    Dim DM_ROT             As String
    Dim INVERSIONE         As Integer
    Call LEGGI_INFO_ROTORI(PERCORSO, COORDINATE, RADICE, PERCORSO_CONTROLLI, PERCORSO_PROFILI, sequenza, npt, TY_ROT, PASSO, CONTROLLORE, DM_ROT, INVERSIONE)
    'CONTROLLO SE E' SPECIFICATO UNA PERIFERICA
    k = InStr(PathMIS, "LOCALE")
    If (k > 0) Then
      PathMIS = RADICE & "\MEASURE"
    Else
      i = InStr(PathMIS, "\\"): j = InStr(PathMIS, ":")
      If i + j <= 0 Then PathMIS = g_chOemPATH & PathMIS
    End If
  Else
    'CONTROLLO SE E' SPECIFICATO UNA PERIFERICA
    i = InStr(PathMIS, "\\"): j = InStr(PathMIS, ":")
    If i + j <= 0 Then PathMIS = g_chOemPATH & PathMIS
  End If
  For i = 0 To 3
    CHK0.Text1(i).Text = ""
  Next i
  'SE NON ESISTE LO CREO
  If Dir$(PathMIS, 16) = "" Then
    i = InStr(PathMIS, "\")
    While (i <> 0)
      MkDir Left$(PathMIS, i - 1)
      'MsgBox Left$(PathMIS, i - 1)
      i = InStr(i + 1, PathMIS, "\")
    Wend
    Call MkDir(PathMIS)
  End If
  'VISUALIZZO L'ELENCO DEI CONTROLLI
  CHK0.File1.Path = PathMIS
  'IMMAGINE DELL'AZIENDA
  CHK0.PicLogo.Picture = LoadPicture(g_chOemPATH & "\" & "logo2.bmp")
  'IMMAGINE DELLA QUALITA'
  If (Dir$(g_chOemPATH & "\" & "QUALITA.bmp") <> "") Then
    CHK0.PicLogo2.Picture = LoadPicture(g_chOemPATH & "\" & "QUALITA.bmp")
  Else
    CHK0.PicLogo2.Picture = LoadPicture("")
  End If
  Select Case TIPO_LAVORAZIONE
  Case "CREATORI_STANDARD"
      'NON VISUALIZZO L'IMMAGINE DI AUTOCORREZIONE
      Call Change_SkTextOnScr(6, "")
      'NON VISUALIZZO L'IMMAGINE DI TOLLERANZA
      Call Change_SkTextOnScr(12, "")
      CHK0.Label2(0).Left = CHK0.lblScX.Left
      CHK0.Text1(0).Left = CHK0.txtScX.Left
      CHK0.Label2(1).Left = CHK0.lblScX.Left
      CHK0.Text1(1).Left = CHK0.txtScX.Left
      CHK0.Label2(0).Width = CHK0.lblScX.Width
      CHK0.Text1(0).Width = CHK0.txtScX.Width
      CHK0.Label2(1).Width = CHK0.lblScX.Width
      CHK0.Text1(1).Width = CHK0.txtScX.Width
      CHK0.Label2(0).Height = CHK0.Text2.Height
      CHK0.Label2(1).Height = CHK0.Text2.Height
      CHK0.Text1(0).Height = CHK0.Text2.Height
      CHK0.Text1(1).Height = CHK0.Text2.Height
      CHK0.Label2(0).Top = CHK0.lblScY.Top + CHK0.lblScY.Height
      CHK0.Label2(1).Top = CHK0.Label2(0).Top + CHK0.Label2(0).Height
      CHK0.Text1(0).Top = CHK0.txtScX.Top + CHK0.txtScX.Height + CHK0.txtScY.Height
      CHK0.Text1(1).Top = CHK0.txtScX.Top + CHK0.txtScX.Height + CHK0.txtScY.Height + CHK0.Text1(0).Height
      CHK0.lblScX.Caption = Prendi(247, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
      CHK0.lblScY.Caption = Prendi(248, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
      CHK0.Label2(0).Caption = "Ox"
      CHK0.Label2(1).Caption = "Oy"
      CHK0.Label2(0).Visible = True
      CHK0.Label2(1).Visible = True
      CHK0.Text1(0).Visible = True
      CHK0.Text1(1).Visible = True
      CHK0.ChkBOMBATURA.Visible = False
      'INIZIALIZZO GLI INDICI DEI PUNTI DEL PROFILO
      CHK0.Text1(0).Text = "0"
      CHK0.Text1(1).Text = "0"
      'CONTROLLO SE DEVO LEGGERE I RISULATATI DELLA MISURA
      stmp = GetInfo("CHK0", "LeggiMisura", Path_LAVORAZIONE_INI): stmp = UCase$(Trim$(stmp))
      If (stmp = "Y") Then
        If (Len(CHK0.Text2.Text) <= 0) Then CHK0.Text2.Text = "DEFAULT"
        ret = WritePrivateProfileString("CHK0", "FileMisura", CHK0.Text2.Text, Path_LAVORAZIONE_INI)
        Call LEGGI_CNC_CONTROLLO_CREATORI(PathMIS & " \ " & CHK0.Text2.Text)
      End If
      '--------------------------------------------------------------------------
      CHK0.Combo1.Visible = True
      CHK0.Combo1.Clear
      Call CHK0.Combo1.AddItem(Prendi(220, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA))
      Call CHK0.Combo1.AddItem(Prendi(249, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA))
      Call CHK0.Combo1.AddItem(Prendi(236, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA))
      CHK0.Combo1.ListIndex = 0 '-----> Call CONTROLLO_CREATORI_VISUALIZZAZIONE da Combo1_Click
      '--------------------------------------------------------------------------
      'LEGGO IL NOME DEL FILE DI MISURA
      CHK0.Text2.Text = GetInfo("CHK0", "FileMisura", Path_LAVORAZIONE_INI)
      'LEGGO LA MATRICOLA
      stmp = GetInfo("CONFIGURAZIONE", "MATRICOLA", Path_LAVORAZIONE_INI)
      If (Len(stmp) > 0) Then CHK0.Text2.Text = stmp
      'SELEZIONE PER DATA
      i = LoadString(g_hLanguageLibHandle, 134, Atmp, 255)
      If (i > 0) Then stmp = Left$(Atmp, i)
      Call Change_SkTextOnScr(10, stmp)
      'SELEZIONA TUTTI
      i = LoadString(g_hLanguageLibHandle, 135, Atmp, 255)
      If (i > 0) Then stmp = Left$(Atmp, i)
      Call Change_SkTextOnScr(11, stmp)
  Case "CREATORI_FORMA"
      'NON VISUALIZZO L'IMMAGINE DI AUTOCORREZIONE
      Call Change_SkTextOnScr(6, "")
      'NON VISUALIZZO L'IMMAGINE DI TOLLERANZA
      Call Change_SkTextOnScr(12, "")
      CHK0.Label2(0).Left = CHK0.lblScX.Left
      CHK0.Text1(0).Left = CHK0.txtScX.Left
      CHK0.Label2(1).Left = CHK0.lblScX.Left
      CHK0.Text1(1).Left = CHK0.txtScX.Left
      CHK0.Label2(0).Width = CHK0.lblScX.Width
      CHK0.Text1(0).Width = CHK0.txtScX.Width
      CHK0.Label2(1).Width = CHK0.lblScX.Width
      CHK0.Text1(1).Width = CHK0.txtScX.Width
      CHK0.Label2(0).Height = CHK0.Text2.Height
      CHK0.Label2(1).Height = CHK0.Text2.Height
      CHK0.Text1(0).Height = CHK0.Text2.Height
      CHK0.Text1(1).Height = CHK0.Text2.Height
      CHK0.Label2(0).Top = CHK0.lblScY.Top + CHK0.lblScY.Height
      CHK0.Label2(1).Top = CHK0.Label2(0).Top + CHK0.Label2(0).Height
      CHK0.Text1(0).Top = CHK0.txtScX.Top + CHK0.txtScX.Height + CHK0.txtScY.Height
      CHK0.Text1(1).Top = CHK0.txtScX.Top + CHK0.txtScX.Height + CHK0.txtScY.Height + CHK0.Text1(0).Height
      'correggo Caption Label scale leggendo da DB
      CHK0.lblScX.Caption = Prendi(247, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
      CHK0.lblScY.Caption = Prendi(248, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
      CHK0.Label2(0).Caption = "Ox"
      CHK0.Label2(1).Caption = "Oy"
      CHK0.Label2(0).Visible = True
      CHK0.Label2(1).Visible = True
      CHK0.Text1(0).Visible = True
      CHK0.Text1(1).Visible = True
      CHK0.ChkBOMBATURA.Visible = False
      'INIZIALIZZO GLI INDICI DEI PUNTI DEL PROFILO
      CHK0.Text1(0).Text = "0"
      CHK0.Text1(1).Text = "0"
      'CONTROLLO SE DEVO LEGGERE I RISULTATI DELLA MISURA
      stmp = GetInfo("CHK0", "LeggiMisura", Path_LAVORAZIONE_INI): stmp = UCase$(Trim$(stmp))
      If (stmp = "Y") Then
        If (Len(CHK0.Text2.Text) <= 0) Then CHK0.Text2.Text = "DEFAULT"
        ret = WritePrivateProfileString("CHK0", "FileMisura", CHK0.Text2.Text, Path_LAVORAZIONE_INI)
        Call LEGGI_CNC_CONTROLLO_CRFORMA(PathMIS & " \ " & Trim$(CHK0.Text2.Text))
      End If
      '--------------------------------------------------------------------------
      CHK0.Combo1.Visible = True
      CHK0.Combo1.Clear
      Call CHK0.Combo1.AddItem(Prendi(220, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA))
      Call CHK0.Combo1.AddItem(Prendi(249, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA))
      Call CHK0.Combo1.AddItem(Prendi(236, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA))
      CHK0.Combo1.ListIndex = 0 '-----> Call CONTROLLO_CREATORI_VISUALIZZAZIONE da Combo1_Click
      '--------------------------------------------------------------------------
      'LEGGO IL NOME DEL FILE DI MISURA
      CHK0.Text2.Text = GetInfo("CHK0", "FileMisura", Path_LAVORAZIONE_INI)
      'LEGGO LA MATRICOLA
      stmp = GetInfo("CONFIGURAZIONE", "MATRICOLA", Path_LAVORAZIONE_INI)
      If (Len(stmp) > 0) Then CHK0.Text2.Text = stmp
      'SELEZIONE PER DATA
      i = LoadString(g_hLanguageLibHandle, 134, Atmp, 255)
      If (i > 0) Then stmp = Left$(Atmp, i)
      Call Change_SkTextOnScr(10, stmp)
      'SELEZIONA TUTTI
      i = LoadString(g_hLanguageLibHandle, 135, Atmp, 255)
      If (i > 0) Then stmp = Left$(Atmp, i)
      Call Change_SkTextOnScr(11, stmp)
    Case "ROTORI_ESTERNI"
      CHK0.Image1.Picture = LoadPicture(g_chOemPATH & "\Gapp4\images\ROTORS.bmp")
      ret = WritePrivateProfileString("CHK0", "FileMisura", "DEFAULT", Path_LAVORAZIONE_INI)
      'NON VISUALIZZO L'IMMAGINE DI AUTOCORREZIONE
      Call Change_SkTextOnScr(6, "\AGGIUSTA.BMP")
      Call Change_SkTextOnScr(1, "")
      Call Change_SkTextOnScr(2, "")
      CHK0.ChkGRID1.Visible = False
      CHK0.ChkGRID2.Visible = False
      CHK0.ChkTAN.Visible = False
      '
      CHK0.lblScX.Visible = False
      CHK0.txtScX.Visible = False
      CHK0.Label2(0).Left = CHK0.lblScX.Left
      CHK0.Text1(0).Left = CHK0.txtScX.Left
      CHK0.Label2(1).Left = CHK0.lblScX.Left
      CHK0.Text1(1).Left = CHK0.txtScX.Left
      CHK0.Label2(0).Width = CHK0.lblScX.Width
      CHK0.Text1(0).Width = CHK0.txtScX.Width
      CHK0.Label2(1).Width = CHK0.lblScX.Width
      CHK0.Text1(1).Width = CHK0.txtScX.Width
      CHK0.Label2(0).Height = CHK0.Text2.Height
      CHK0.Label2(1).Height = CHK0.Text2.Height
      CHK0.Text1(0).Height = CHK0.Text2.Height
      CHK0.Text1(1).Height = CHK0.Text2.Height
      CHK0.Label2(0).Top = CHK0.lblScY.Top + CHK0.lblScY.Height
      CHK0.Label2(1).Top = CHK0.Label2(0).Top + CHK0.Label2(0).Height
      CHK0.Text1(0).Top = CHK0.txtScX.Top + CHK0.txtScX.Height + CHK0.txtScY.Height
      CHK0.Text1(1).Top = CHK0.txtScX.Top + CHK0.txtScX.Height + CHK0.txtScY.Height + CHK0.Text1(0).Height
      'correggo Caption Label scale leggendo da DB
      CHK0.Label2(0).Caption = "Ox"
      CHK0.Label2(1).Caption = "Oy"
      CHK0.Label2(0).Visible = False
      CHK0.Label2(1).Visible = False
      CHK0.Text1(0).Visible = False
      CHK0.Text1(1).Visible = False
      CHK0.ChkBOMBATURA.Visible = False
      'INIZIALIZZO GLI INDICI DEI PUNTI DEL PROFILO
      CHK0.Text1(0).Text = "0"
      CHK0.Text1(1).Text = "0"
      '--------------------------------------------------------------------------
      'LEGGO IL NOME DEL FILE DI MISURA
      CHK0.Text2.Text = GetInfo("CHK0", "FileMisura", Path_LAVORAZIONE_INI)
      'LEGGO LA MATRICOLA
      stmp = GetInfo("CONFIGURAZIONE", "MATRICOLA", Path_LAVORAZIONE_INI)
      If (Len(stmp) > 0) Then CHK0.Text2.Text = stmp
      CHK0.Image2.Visible = False
      CHK0.Label1.Visible = False
      CHK0.File1.Visible = False
      Call Change_SkTextOnScr(10, "")
      Call Change_SkTextOnScr(11, "")
      Call Change_SkTextOnScr(13, "")
      Call Change_SkTextOnScr(15, "")
      '--------------------------------------------------------------------------
      CHK0.Combo1.Visible = True
      CHK0.Combo1.Clear
      i = 0
      Do
        stmp = GetInfo("CONTROLLO", "itemSTR(" & Format$(i, "#0") & ")", Path_LAVORAZIONE_INI)
        stmp = Trim$(stmp)
        If (stmp <> "") Then
          Call CHK0.Combo1.AddItem("(" & Format$(i + 1, "#0") & ") " & stmp)
        Else
          Exit Do
        End If
        i = i + 1
      Loop
      If (i = 0) Then
        Call CHK0.Combo1.AddItem("PROFILE")
        Call CHK0.Combo1.AddItem("CORROT")
        Call CHK0.Combo1.AddItem("CORRPROP")
        Call CHK0.Combo1.AddItem("PROPOSED")
        Call CHK0.Combo1.AddItem("PRFTOL")
        Call CHK0.Combo1.AddItem("CHKRAB")
      End If
      'VISUALIZZO L'IMMAGINE DI TOLLERANZA
      Call Change_SkTextOnScr(12, "TOLL.")
      CHK0.Combo1.ListIndex = 0 '-----> Call VISUALIZZA_CONTROLLO da Combo1_Click
      '--------------------------------------------------------------------------
    Case "PROFILO_RAB_ESTERNI"
      CHK0.Image1.Picture = LoadPicture(g_chOemPATH & "\Gapp4\images\ROTORS.bmp")
      ret = WritePrivateProfileString("CHK0", "FileMisura", "DEFAULT", Path_LAVORAZIONE_INI)
      'NON VISUALIZZO L'IMMAGINE DI AUTOCORREZIONE
      Call Change_SkTextOnScr(6, "\AGGIUSTA.BMP")
      Call Change_SkTextOnScr(1, "")
      Call Change_SkTextOnScr(2, "")
      CHK0.ChkGRID1.Visible = False
      CHK0.ChkGRID2.Visible = False
      CHK0.ChkTAN.Visible = False
      '
      CHK0.lblScX.Visible = False
      CHK0.txtScX.Visible = False
      CHK0.Label2(0).Left = CHK0.lblScX.Left
      CHK0.Text1(0).Left = CHK0.txtScX.Left
      CHK0.Label2(1).Left = CHK0.lblScX.Left
      CHK0.Text1(1).Left = CHK0.txtScX.Left
      CHK0.Label2(0).Width = CHK0.lblScX.Width
      CHK0.Text1(0).Width = CHK0.txtScX.Width
      CHK0.Label2(1).Width = CHK0.lblScX.Width
      CHK0.Text1(1).Width = CHK0.txtScX.Width
      CHK0.Label2(0).Height = CHK0.Text2.Height
      CHK0.Label2(1).Height = CHK0.Text2.Height
      CHK0.Text1(0).Height = CHK0.Text2.Height
      CHK0.Text1(1).Height = CHK0.Text2.Height
      CHK0.Label2(0).Top = CHK0.lblScY.Top + CHK0.lblScY.Height
      CHK0.Label2(1).Top = CHK0.Label2(0).Top + CHK0.Label2(0).Height
      CHK0.Text1(0).Top = CHK0.txtScX.Top + CHK0.txtScX.Height + CHK0.txtScY.Height
      CHK0.Text1(1).Top = CHK0.txtScX.Top + CHK0.txtScX.Height + CHK0.txtScY.Height + CHK0.Text1(0).Height
      'correggo Caption Label scale leggendo da DB
      CHK0.Label2(0).Caption = "Ox"
      CHK0.Label2(1).Caption = "Oy"
      CHK0.Label2(0).Visible = False
      CHK0.Label2(1).Visible = False
      CHK0.Text1(0).Visible = False
      CHK0.Text1(1).Visible = False
      CHK0.ChkBOMBATURA.Visible = False
      'INIZIALIZZO GLI INDICI DEI PUNTI DEL PROFILO
      CHK0.Text1(0).Text = "0"
      CHK0.Text1(1).Text = "0"
      '--------------------------------------------------------------------------
      'LEGGO IL NOME DEL FILE DI MISURA
      CHK0.Text2.Text = GetInfo("CHK0", "FileMisura", Path_LAVORAZIONE_INI)
      'LEGGO LA MATRICOLA
      stmp = GetInfo("CONFIGURAZIONE", "MATRICOLA", Path_LAVORAZIONE_INI)
      If (Len(stmp) > 0) Then CHK0.Text2.Text = stmp
      CHK0.Image2.Visible = False
      CHK0.Label1.Visible = False
      CHK0.File1.Visible = False
      Call Change_SkTextOnScr(10, "")
      Call Change_SkTextOnScr(11, "")
      Call Change_SkTextOnScr(13, "")
      Call Change_SkTextOnScr(15, "")
      '--------------------------------------------------------------------------
      CHK0.Combo1.Visible = True
      CHK0.Combo1.Clear
      i = 0
      Do
        stmp = GetInfo("CONTROLLO", "itemSTR(" & Format$(i, "#0") & ")", Path_LAVORAZIONE_INI)
        stmp = Trim$(stmp)
        If (stmp <> "") Then
          Call CHK0.Combo1.AddItem("(" & Format$(i + 1, "#0") & ") " & stmp)
        Else
          Exit Do
        End If
        i = i + 1
      Loop
      If (i = 0) Then
        Call CHK0.Combo1.AddItem("PROFILE")
        Call CHK0.Combo1.AddItem("CORROT")
        Call CHK0.Combo1.AddItem("CORRPROP")
        Call CHK0.Combo1.AddItem("PROPOSED")
        Call CHK0.Combo1.AddItem("PRFTOL")
        Call CHK0.Combo1.AddItem("CHKRAB")
      End If
      'VISUALIZZO L'IMMAGINE DI TOLLERANZA
      Call Change_SkTextOnScr(12, "TOLL.")
      CHK0.Combo1.ListIndex = 0 '-----> Call VISUALIZZA_CONTROLLO da Combo1_Click
      '--------------------------------------------------------------------------
  Case "INGRANAGGI_INTERNI", "INGR380_ESTERNI", "SCANALATIEVO", "DENTATURA", "MOLAVITE", "MOLAVITEG160", "INGRANAGGI_ESTERNIV", "INGRANAGGI_ESTERNIO"
      'NON VISUALIZZO L'IMMAGINE DI AUTOCORREZIONE
      Call Change_SkTextOnScr(6, "")
      stmp = GetInfo("CHK0", "TOLLERANZA_PROFILO", Path_LAVORAZIONE_INI): stmp = UCase$(Trim$(stmp))
      If (stmp = "Y") Then
        'VISUALIZZO L'IMMAGINE DI TOLLERANZA
        Call Change_SkTextOnScr(12, "TOLL.")
      Else
        'NON VISUALIZZO L'IMMAGINE DI TOLLERANZA
        Call Change_SkTextOnScr(12, "")
      End If
      '--------------------------------------------------------------------------
      INTESTAZIONE = Prendi(200, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
      TitoloPROELI = Prendi(201, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
      TitoloDIV = Prendi(202, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
      i = 3
      For j = 1 To 4
        ComPROELI1(j) = Prendi(200 + i + j, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
      Next j
      i = 7
      For j = 1 To 4
        ComPROELI2(j) = Prendi(200 + i + j, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
      Next j
      i = 19
      For j = 1 To 6
        comVisPROELI(j, 1) = Prendi(200 + i + j, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
      Next j
      comVisPROELI(7, 1) = Prendi(227, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
      i = 25
      For j = 1 To 6
        comVisPROELI(j, 2) = Prendi(200 + i + j, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
      Next j
      comVisPROELI(7, 2) = Prendi(228, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
      '--------------------------------------------------------------------------
      Logo1 = Prendi(256, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
      Logo2 = Prendi(257, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
      INTESTAZIONE = Logo1 & "     " & INTESTAZIONE
      '--------------------------------------------------------------------------
      SINISTRA = Prendi(258, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
      DESTRA = Prendi(259, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
      '--------------------------------------------------------------------------
      logo3 = Prendi(260, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
      '--------------------------------------------------------------------------
      For j = 0 To 1
        For i = 0 To 4
          stmp = "SiOpt(" & Format$(i, "0") & "," & Format$(j, "0") & ")"
          riga = GetInfo("CHK0", stmp, Path_LAVORAZIONE_INI): riga = Trim$(riga)
          If (Len(riga) > 0) Then
            ret = InStr(riga, ";")
            If (ret > 0) Then riga = Trim$(Left$(riga, ret - 1))
          Else
            riga = "1"
            ret = WritePrivateProfileString("CHK0", stmp, riga, Path_LAVORAZIONE_INI)
          End If
          SiOpt(i, j) = val(riga)
        Next i
      Next j
      'COPIO L'ULTIMO CONTROLLO NEL FILE DI DEFAULT
      stmp = GetInfo("CHK0", "FileMisura", Path_LAVORAZIONE_INI)
      stmp = PathMIS & "\" & stmp
      If (Dir$(stmp) <> "") And ((stmp) <> PathMIS & "\DEFAULT") Then
        'se ultimo controllo e' SOVRAMETALLO carico DEFAULT 14.12.06
        If (stmp) <> PathMIS & "\SOVRAMETALLO" Then
           FileCopy stmp, PathMIS & "\DEFAULT"
        Else
           ret = WritePrivateProfileString("CHK0", "FileMisura", "DEFAULT", Path_LAVORAZIONE_INI)
           CHK0.Text2.Text = "DEFAULT"
        End If
      Else
        ret = WritePrivateProfileString("CHK0", "FileMisura", "DEFAULT", Path_LAVORAZIONE_INI)
        CHK0.Text2.Text = "DEFAULT"
      End If
      '--------------------------------------------------------------------------
      CHK0.Combo1.Visible = True
      CHK0.Combo1.Clear
      Call CHK0.Combo1.AddItem(Prendi(351, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA))
      Call CHK0.Combo1.AddItem(Prendi(352, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA))
      Call CHK0.Combo1.AddItem(Prendi(353, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA))
      Call CHK0.Combo1.AddItem(Prendi(354, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA))
      Call CHK0.Combo1.AddItem(Prendi(355, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA))
      If (TIPO_LAVORAZIONE = "INGR380_ESTERNI") Or (TIPO_LAVORAZIONE = "SCANALATIEVO") Or (TIPO_LAVORAZIONE = "INGRANAGGI_ESTERNIV") Or (TIPO_LAVORAZIONE = "INGRANAGGI_ESTERNIO") Then
        stmp = GetInfo(TIPO_LAVORAZIONE, "SOVRAMETALLO", Path_LAVORAZIONE_INI)
        If (stmp = "Y") Then Call CHK0.Combo1.AddItem(Prendi(921, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA))
      End If
      CHK0.Combo1.ListIndex = 0 '-----> Call VISUALIZZA_CONTROLLO da Combo1_Click
      '--------------------------------------------------------------------------
      CHK0.Label2(0).Visible = False
      CHK0.Label2(1).Visible = False
      CHK0.Text1(0).Visible = False
      CHK0.Text1(1).Visible = False
      CHK0.ChkBOMBATURA.Visible = True
      'LEGGO IL NOME DEL FILE DI MISURA
      CHK0.Text2.Text = GetInfo("CHK0", "FileMisura", Path_LAVORAZIONE_INI)
      'LEGGO LA MATRICOLA
      stmp = GetInfo("CONFIGURAZIONE", "MATRICOLA", Path_LAVORAZIONE_INI)
      If (Len(stmp) > 0) Then CHK0.Text2.Text = stmp
      'SELEZIONE PER DATA
      i = LoadString(g_hLanguageLibHandle, 134, Atmp, 255)
      If (i > 0) Then stmp = Left$(Atmp, i)
      Call Change_SkTextOnScr(10, stmp)
      'SELEZIONA TUTTI
      i = LoadString(g_hLanguageLibHandle, 135, Atmp, 255)
      If (i > 0) Then stmp = Left$(Atmp, i)
      Call Change_SkTextOnScr(11, stmp)
  Case "VITI_MULTIFILETTO", "VITI_ESTERNE", "VITIV_ESTERNE"
      'NON VISUALIZZO L'IMMAGINE DI AUTOCORREZIONE
      Call Change_SkTextOnScr(6, "")
      stmp = GetInfo("CHK0", "TOLLERANZA_PROFILO", Path_LAVORAZIONE_INI): stmp = UCase$(Trim$(stmp))
      If (stmp = "Y") Then
        'VISUALIZZO L'IMMAGINE DI TOLLERANZA
        Call Change_SkTextOnScr(12, "TOLL.")
      Else
        'NON VISUALIZZO L'IMMAGINE DI TOLLERANZA
        Call Change_SkTextOnScr(12, "")
      End If
      '--------------------------------------------------------------------------
      INTESTAZIONE = Prendi(200, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
      TitoloPROELI = Prendi(201, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
      TitoloDIV = Prendi(202, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
      i = 3
      For j = 1 To 4
        ComPROELI1(j) = Prendi(200 + i + j, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
      Next j
      i = 7
      For j = 1 To 4
        ComPROELI2(j) = Prendi(200 + i + j, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
      Next j
      i = 19
      For j = 1 To 6
        comVisPROELI(j, 1) = Prendi(200 + i + j, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
      Next j
      comVisPROELI(7, 1) = Prendi(227, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
      i = 25
      For j = 1 To 6
        comVisPROELI(j, 2) = Prendi(200 + i + j, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
      Next j
      comVisPROELI(7, 2) = Prendi(228, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
      '--------------------------------------------------------------------------
      Logo1 = Prendi(256, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
      Logo2 = Prendi(257, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
      INTESTAZIONE = Logo1 & "     " & INTESTAZIONE
      '--------------------------------------------------------------------------
      SINISTRA = Prendi(258, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
      DESTRA = Prendi(259, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
      '--------------------------------------------------------------------------
      logo3 = Prendi(260, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
      '--------------------------------------------------------------------------
      For j = 0 To 1
        For i = 0 To 4
          stmp = "SiOpt(" & Format$(i, "0") & "," & Format$(j, "0") & ")"
          riga = GetInfo("CHK0", stmp, Path_LAVORAZIONE_INI): riga = Trim$(riga)
          If (Len(riga) > 0) Then
            ret = InStr(riga, ";")
            If (ret > 0) Then riga = Trim$(Left$(riga, ret - 1))
          Else
            riga = "1"
            ret = WritePrivateProfileString("CHK0", stmp, riga, Path_LAVORAZIONE_INI)
          End If
          SiOpt(i, j) = val(riga)
        Next i
      Next j
      'COPIO L'ULTIMO CONTROLLO NEL FILE DI DEFAULT
      stmp = GetInfo("CHK0", "FileMisura", Path_LAVORAZIONE_INI)
      stmp = PathMIS & "\" & stmp
      If (Dir$(stmp) <> "") Then
        Call FileCopy(stmp, PathMIS & "\DEFAULT")
      Else
        ret = WritePrivateProfileString("CHK0", "FileMisura", "DEFAULT", Path_LAVORAZIONE_INI)
        CHK0.Text2.Text = "DEFAULT"
      End If
      '--------------------------------------------------------------------------
      CHK0.Combo1.Visible = True
      CHK0.Combo1.Clear
      Call CHK0.Combo1.AddItem(Prendi(351, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA))
      Call CHK0.Combo1.AddItem(Prendi(352, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA))
      Call CHK0.Combo1.AddItem(Prendi(353, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA))
      Call CHK0.Combo1.AddItem(Prendi(354, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA))
      Call CHK0.Combo1.AddItem(Prendi(355, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA))
      Call CHK0.Combo1.AddItem(Prendi(918, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA))
      CHK0.Combo1.ListIndex = 0 '-----> Call VISUALIZZA_CONTROLLO da Combo1_Click
      '--------------------------------------------------------------------------
      CHK0.Label2(0).Visible = False
      CHK0.Label2(1).Visible = False
      CHK0.Text1(0).Visible = False
      CHK0.Text1(1).Visible = False
      CHK0.ChkBOMBATURA.Visible = True
      'LEGGO IL NOME DEL FILE DI MISURA
      CHK0.Text2.Text = GetInfo("CHK0", "FileMisura", Path_LAVORAZIONE_INI)
      'LEGGO LA MATRICOLA
      stmp = GetInfo("CONFIGURAZIONE", "MATRICOLA", Path_LAVORAZIONE_INI)
      If (Len(stmp) > 0) Then CHK0.Text2.Text = stmp
      'SELEZIONE PER DATA
      i = LoadString(g_hLanguageLibHandle, 134, Atmp, 255)
      If (i > 0) Then stmp = Left$(Atmp, i)
      Call Change_SkTextOnScr(10, stmp)
      'SELEZIONA TUTTI
      i = LoadString(g_hLanguageLibHandle, 135, Atmp, 255)
      If (i > 0) Then stmp = Left$(Atmp, i)
      Call Change_SkTextOnScr(11, stmp)
  End Select
  '
Exit Sub

errINIZIALIZZA_CHK0_PARTE1:
  WRITE_DIALOG "SUB: INIZIALIZZA_CHK0_PARTE1 ERROR --> " & str(Err)
  Resume Next
  
End Sub

Function LeggiDDE_INGRANAGGI(ByVal INVERTI_PROFILO As Integer) As Integer
'
Dim iB              As Integer
Dim i               As Integer
Dim j               As Integer
Dim INVERTI_FIANCHI As Integer 'PER OPERARARE L'INVERSIONE DEI FIANCHI A RICHIESTA DEL PROGRAMMA PEZZO
'
On Error GoTo errLeggiDDE_INGRANAGGI
  '
  LeggiDDE_INGRANAGGI = False
  'Lettura dei parametri principali
  SAP = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/GEN[1]"))
  EAP = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/GEN[2]"))
  AngPreNor = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/GEN[3]"))
  AngEli = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/GEN[4]"))
  NZ = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/GEN[5]"))
  ModNor = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/GEN[6]"))
  SpeCirNorMis = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/GEN[7]"))
  DivCon = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/GEN[8]"))
  Np = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/GEN[9]"))
  LunCon = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/GEN[10]"))
  '
  i = 1: iPd(1) = 0: iPd(2) = 0: iPd(3) = 0: iPd(4) = 0
  Do
    'Lettura dell'indice del dente controllato
    iPd(i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/GEN[" & Format$(10 + i, "00") & "]"))
    i = i + 1 'Incremento il contatore
  Loop While (i <= 4) And (iPd(i - 1) <> 0)
  '
  NE = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/GEN[15]"))
  FasCon = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/GEN[16]"))
  '
  '------------------------------------------------------------------------
  'LEGGO SE DEVO INVERTIRE I FIANCHI
  INVERTI_FIANCHI = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/GEN[19]"))
  'SONO SICURO DI CONSIDERARE SOLO 0 e 1
  If (INVERTI_FIANCHI <> 1) Then INVERTI_FIANCHI = 0
  '------------------------------------------------------------------------
  For i = 1 To 4: iEd(i) = iPd(i): Next i
  '------------------------------------------------------------------------
  ReDim Pd0f1(Np): ReDim Pd0f2(Np): ReDim HyP(Np)
  For i = 1 To Np: HyP(i) = (i - 1) * LunCon / (Np - 1): Next i
  '------------------------------------------------------------------------
  ReDim Ed0f1(NE): ReDim Ed0f2(NE): ReDim HyE(NE)
  For i = 1 To NE: HyE(i) = (i - 1) * FasCon / (NE - 1): Next i
  '------------------------------------------------------------------------
  j = 0: iB = 0
  'PROFILO DENTE 1 ********************************************************
  ReDim Pd1f1(Np): ReDim Pd1f2(Np)
  If (iPd(1) <> 0) And (Np <> 0) Then
    If INVERTI_PROFILO = 1 Then
      If INVERTI_FIANCHI = 1 Then
        'CONTROLLO PROFILO INTERNI DI TIPO 2
        For i = 1 To Np
          j = iB + i
          Pd1f2(Np + 1 - i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF1[" & Format$(j, "##0") & "]"))
          Pd1f1(Np + 1 - i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF2[" & Format$(j, "##0") & "]"))
        Next i
      Else
        'CONTROLLO PROFILO INTERNI DI TIPO 1
        For i = 1 To Np
          j = iB + i
          Pd1f1(Np + 1 - i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF1[" & Format$(j, "##0") & "]"))
          Pd1f2(Np + 1 - i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF2[" & Format$(j, "##0") & "]"))
        Next i
      End If
    Else
      If INVERTI_FIANCHI = 1 Then
        'CONTROLLO PROFILO ESTERNI CON INVERSIONE FIANCHI
        For i = 1 To Np
          j = iB + i
          Pd1f2(i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF1[" & Format$(j, "##0") & "]"))
          Pd1f1(i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF2[" & Format$(j, "##0") & "]"))
        Next i
      Else
        'CONTROLLO PROFILO ESTERNI
        For i = 1 To Np
          j = iB + i
          Pd1f1(i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF1[" & Format$(j, "##0") & "]"))
          Pd1f2(i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF2[" & Format$(j, "##0") & "]"))
        Next i
      End If
    End If
    iB = j
  End If
  'ELICA DENTE 1 **********************************************************
  ReDim Ed1f1(NE): ReDim Ed1f2(NE)
  If (iEd(1) <> 0) And (NE <> 0) Then
    If INVERTI_FIANCHI = 1 Then
      'CONTROLLO ELICA ESTERNI CON INVERSIONE FIANCHI
      'CONTROLLO ELICA INTERNI DI TIPO 2
      For i = 1 To NE
        j = iB + i
        Ed1f2(i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF1[" & Format$(j, "##0") & "]"))
        Ed1f1(i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF2[" & Format$(j, "##0") & "]"))
      Next i
    Else
      'CONTROLLO ELICA ESTERNI
      'CONTROLLO ELICA INTERNI DI TIPO 1
      For i = 1 To NE
        j = iB + i
        Ed1f1(i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF1[" & Format$(j, "##0") & "]"))
        Ed1f2(i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF2[" & Format$(j, "##0") & "]"))
      Next i
    End If
    iB = j
  End If
  'PROFILO DENTE 2 ********************************************************
  ReDim Pd2f1(Np): ReDim Pd2f2(Np)
  If (iPd(2) <> 0) Then
    If INVERTI_PROFILO = 1 Then
      If INVERTI_FIANCHI = 1 Then
        'CONTROLLO PROFILO INTERNI DI TIPO 2
        For i = 1 To Np
          j = iB + i
          Pd2f2(Np + 1 - i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF1[" & Format$(j, "##0") & "]"))
          Pd2f1(Np + 1 - i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF2[" & Format$(j, "##0") & "]"))
        Next i
      Else
        'CONTROLLO PROFILO INTERNI DI TIPO 1
        For i = 1 To Np
          j = iB + i
          Pd2f1(Np + 1 - i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF1[" & Format$(j, "##0") & "]"))
          Pd2f2(Np + 1 - i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF2[" & Format$(j, "##0") & "]"))
        Next i
      End If
    Else
      If INVERTI_FIANCHI = 1 Then
        'CONTROLLO PROFILO ESTERNI CON INVERSIONE FIANCHI
        For i = 1 To Np
          j = iB + i
          Pd2f2(i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF1[" & Format$(j, "##0") & "]"))
          Pd2f1(i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF2[" & Format$(j, "##0") & "]"))
        Next i
      Else
        'CONTROLLO PROFILO ESTERNI
        For i = 1 To Np
          j = iB + i
          Pd2f1(i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF1[" & Format$(j, "##0") & "]"))
          Pd2f2(i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF2[" & Format$(j, "##0") & "]"))
        Next i
      End If
    End If
    iB = j
  End If
  'ELICA DENTE 2 **********************************************************
  ReDim Ed2f1(NE): ReDim Ed2f2(NE)
  If (iEd(2) <> 0) Then
    If INVERTI_FIANCHI = 1 Then
      'CONTROLLO ELICA ESTERNI CON INVERSIONE FIANCHI
      'CONTROLLO ELICA INTERNI DI TIPO 2
      For i = 1 To NE
        j = iB + i
        Ed2f2(i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF1[" & Format$(j, "##0") & "]"))
        Ed2f1(i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF2[" & Format$(j, "##0") & "]"))
      Next i
    Else
      'CONTROLLO ELICA ESTERNI
      'CONTROLLO ELICA INTERNI DI TIPO 1
      For i = 1 To NE
        j = iB + i
        Ed2f1(i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF1[" & Format$(j, "##0") & "]"))
        Ed2f2(i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF2[" & Format$(j, "##0") & "]"))
      Next i
    End If
    iB = j
  End If
  'PROFILO DENTE 3 ********************************************************
  ReDim Pd3f1(Np): ReDim Pd3f2(Np)
  If (iPd(3) <> 0) Then
    If INVERTI_PROFILO = 1 Then
      If INVERTI_FIANCHI = 1 Then
        'CONTROLLO PROFILO INTERNI DI TIPO 2
        For i = 1 To Np
          j = iB + i
          Pd3f2(Np + 1 - i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF1[" & Format$(j, "##0") & "]"))
          Pd3f1(Np + 1 - i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF2[" & Format$(j, "##0") & "]"))
        Next i
      Else
        'CONTROLLO PROFILO INTERNI DI TIPO 1
        For i = 1 To Np
          j = iB + i
          Pd3f1(Np + 1 - i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF1[" & Format$(j, "##0") & "]"))
          Pd3f2(Np + 1 - i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF2[" & Format$(j, "##0") & "]"))
        Next i
      End If
    Else
      If INVERTI_FIANCHI = 1 Then
        'CONTROLLO PROFILO ESTERNI CON INVERSIONE FIANCHI
        For i = 1 To Np
          j = iB + i
          Pd3f2(i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF1[" & Format$(j, "##0") & "]"))
          Pd3f1(i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF2[" & Format$(j, "##0") & "]"))
        Next i
      Else
        'CONTROLLO PROFILO ESTERNI
        For i = 1 To Np
          j = iB + i
          Pd3f1(i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF1[" & Format$(j, "##0") & "]"))
          Pd3f2(i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF2[" & Format$(j, "##0") & "]"))
        Next i
      End If
    End If
    iB = j
  End If
  'ELICA DENTE 3 **********************************************************
  ReDim Ed3f1(NE): ReDim Ed3f2(NE)
  If (iEd(3) <> 0) Then
    If INVERTI_FIANCHI = 1 Then
      'CONTROLLO ELICA ESTERNI CON INVERSIONE FIANCHI
      'CONTROLLO ELICA INTERNI DI TIPO 2
      For i = 1 To NE
        j = iB + i
        Ed3f2(i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF1[" & Format$(j, "##0") & "]"))
        Ed3f1(i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF2[" & Format$(j, "##0") & "]"))
      Next i
    Else
      'CONTROLLO ELICA ESTERNI
      'CONTROLLO ELICA INTERNI DI TIPO 1
      For i = 1 To NE
        j = iB + i
        Ed3f1(i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF1[" & Format$(j, "##0") & "]"))
        Ed3f2(i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF2[" & Format$(j, "##0") & "]"))
      Next i
    End If
    iB = j
  End If
  'PROFILO DENTE 4 ********************************************************
  ReDim Pd4f1(Np): ReDim Pd4f2(Np)
  If (iPd(4) <> 0) Then
    If INVERTI_PROFILO = 1 Then
      If INVERTI_FIANCHI = 1 Then
        'CONTROLLO PROFILO INTERNI DI TIPO 2
        For i = 1 To Np
          j = iB + i
          Pd4f2(Np + 1 - i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF1[" & Format$(j, "##0") & "]"))
          Pd4f1(Np + 1 - i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF2[" & Format$(j, "##0") & "]"))
        Next i
      Else
        'CONTROLLO PROFILO INTERNI DI TIPO 1
        For i = 1 To Np
          j = iB + i
          Pd4f1(Np + 1 - i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF1[" & Format$(j, "##0") & "]"))
          Pd4f2(Np + 1 - i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF2[" & Format$(j, "##0") & "]"))
        Next i
      End If
    Else
      If INVERTI_FIANCHI = 1 Then
        'CONTROLLO PROFILO ESTERNI CON INVERSIONE FIANCHI
        For i = 1 To Np
          j = iB + i
          Pd4f2(i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF1[" & Format$(j, "##0") & "]"))
          Pd4f1(i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF2[" & Format$(j, "##0") & "]"))
        Next i
      Else
        'CONTROLLO PROFILO ESTERNI
        For i = 1 To Np
          j = iB + i
          Pd4f1(i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF1[" & Format$(j, "##0") & "]"))
          Pd4f2(i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF2[" & Format$(j, "##0") & "]"))
        Next i
      End If
    End If
    iB = j
  End If
  'ELICA DENTE 4 **********************************************************
  ReDim Ed4f1(NE): ReDim Ed4f2(NE)
  If (iEd(4) <> 0) Then
    If INVERTI_FIANCHI = 1 Then
      'CONTROLLO ELICA ESTERNI CON INVERSIONE FIANCHI
      'CONTROLLO ELICA INTERNI DI TIPO 2
      For i = 1 To NE
        j = iB + i
        Ed4f2(i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF1[" & Format$(j, "##0") & "]"))
        Ed4f1(i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF2[" & Format$(j, "##0") & "]"))
      Next i
    Else
      'CONTROLLO ELICA ESTERNI
      'CONTROLLO ELICA INTERNI DI TIPO 1
      For i = 1 To NE
        j = iB + i
        Ed4f1(i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF1[" & Format$(j, "##0") & "]"))
        Ed4f2(i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF2[" & Format$(j, "##0") & "]"))
      Next i
    End If
    iB = j
  End If
  'DIVISIONE **************************************************************
  If DivCon = 1 Then
    ReDim Divf1(NZ): ReDim Divf2(NZ): ReDim HxD(NZ)
    For i = 1 To NZ: HxD(i) = (i - 0.5) * Box1W / NZ: Next i
    If INVERTI_FIANCHI = 1 Then
      'CONTROLLO DIVISIONE ESTERNI CON INVERSIONE FIANCHI
      'CONTROLLO DIVISIONE INTERNI DI TIPO 2
      For i = 1 To NZ
        j = iB + i
        Divf2(i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF1[" & Format$(j, "##0") & "]"))
        Divf1(i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF2[" & Format$(j, "##0") & "]"))
      Next i
    Else
      'CONTROLLO DIVISIONE ESTERNI
      'CONTROLLO DIVISIONE INTERNI DI TIPO 1
      For i = 1 To NZ
        j = iB + i
        Divf1(i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF1[" & Format$(j, "##0") & "]"))
        Divf2(i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF2[" & Format$(j, "##0") & "]"))
      Next i
    End If
    iB = j
  End If
  '--------------------------------------------------------------------------
  Call CalcoloErrori(1)
  Call CalcoloErrori(2)
  Call CalcoloErrori(3)
  Call CalcoloErrori(4)
  Call CalcoloErrori(0)
  '--------------------------------------------------------------------------
  LeggiDDE_INGRANAGGI = True
  '
Exit Function

errLeggiDDE_INGRANAGGI:
  LeggiDDE_INGRANAGGI = False
  WRITE_DIALOG "SUB: LeggiDDE_INGRANAGGI ERROR --> " & str(Err)
  Exit Function

End Function

Sub Calcolo_Qr_Int(DPG As Double, Qmola As Double, AngPreNor As Double, AngEli As Double, NZ As Integer, ModNor As Double, SpeCirNorMis As Double)
'
Dim ALFAn     As Double
Dim ALFAt     As Double
Dim BetaR     As Double
Dim Betab     As Double
Dim AM        As Double
Dim Z         As Integer
Dim SONmola   As Double
Dim SONtmola  As Double
'
Dim T1  As Double
Dim CS1 As Double
Dim CS2 As Double
Dim F3  As Double
Dim RX  As Double
Dim R   As Double
Dim Rb  As Double
Dim PB  As Double
Dim DB  As Double
Dim Eb  As Double
Dim iB  As Double
Dim DP  As Double
Dim PG  As Double
Dim QrulliInt     As Double
Dim Drulli        As Double
Dim stmp          As String
'
On Error Resume Next
  '
  If (AngEli = 0) Then AngEli = 0.000001
  '
  AM = ModNor
  Z = NZ
  PG = 3.141592654
  '
  'GRADI -> RADIANTI
  ALFAn = FnGR(AngPreNor)
  BetaR = FnGR(Abs(AngEli))
  '
  'CALCOLO COSTANTI
  ALFAt = Atn(Tan(ALFAn) / Cos(BetaR))
  T1 = Tan(ALFAt)
  CS1 = Cos(ALFAt)
  F3 = T1 - Atn(T1)             'INVOLUTA(ALFAON)
  R = AM * Z / 2 / Cos(BetaR)   'R primitivo
  Betab = Atn(Tan(BetaR) * CS1) 'elica base
  Rb = R * CS1                  'R base
  PB = (Rb * 2 * PG) / Z        'Passo di base
  DB = 2 * Rb                   'diam. base
  '
  SONmola = SpeCirNorMis
  Eb = Rb * (SONmola / Cos(BetaR) / R - 2 * F3)
  iB = PB - Eb
  Drulli = DPG
  Call QUOTA_RULLI(Drulli, Z, DB, Betab, iB, QrulliInt, "K")
  Qmola = QrulliInt
  '
End Sub

'Calcolo quota rulli da SON misurato
Sub Calcolo_Qr(DPG As Double, Qmola As Double, AngPreNor As Double, AngEli As Double, NZ As Integer, ModNor As Double, SpeCirNorMis As Double)
'
Dim ALFAn     As Double
Dim ALFAt     As Double
Dim Beta      As Double
Dim Betab     As Double
Dim AM        As Double
Dim Z         As Integer
Dim SONmola   As Double
Dim SONtmola  As Double
Dim T1        As Double
Dim CS1       As Double
Dim CS2       As Double
Dim F3        As Double
Dim RX        As Double
Dim R         As Double
Dim Rb        As Double
Dim PB        As Double
Dim DB        As Double
Dim Sb        As Double
Dim DP        As Double
Dim PG        As Double
'
Dim Wx        As Double
Dim We        As Double
Dim X         As Double
Dim Ymini     As Double
Dim Ymaxi     As Double
Dim Ymoyen    As Double
'
On Error Resume Next
  '
  AM = ModNor
  Z = NZ
  PG = 3.141592654
  '
  'GRADI -> RADIANTI
  ALFAn = FnGR(AngPreNor)
  Beta = FnGR(Abs(AngEli))
  '
  'CALCOLO COSTANTI
  ALFAt = Atn(Tan(ALFAn) / Cos(Beta))
  T1 = Tan(ALFAt)
  CS1 = Cos(ALFAt)
  CS2 = Cos(Beta)
  F3 = T1 - Atn(T1)            'INVOLUTA(ALFAON)
  R = AM * Z / 2 / Cos(Beta)   'R primitivo
  Betab = Atn(Tan(Beta) * CS1) 'Elica base
  Rb = R * CS1                 'R base
  PB = (Rb * 2 * PG) / Z       'Passo di base
  DB = 2 * Rb                  'diam. base
  DP = 2 * R                   'diam. primitivo
  '
  SONmola = SpeCirNorMis
  '
  SONtmola = SONmola / Cos(Beta)
  Sb = Rb * (SONtmola / R + 2 * F3)
  '
  Wx = Cos(PG / (2 * Z)) * DB
  If Int(Z / 2) = Z / 2 Then Wx = DB
  '
  We = Sb - PB
  X = (We + DPG / Cos(Betab)) / DB
  Ymini = 0: Ymaxi = PG / 2
  For i = 1 To 25
    Ymoyen = (Ymini + Ymaxi) / 2: X1 = Tan(Ymoyen) - Ymoyen
    If X1 < X Then Ymini = Ymoyen Else Ymaxi = Ymoyen
  Next i
  Qmola = (Wx / Cos(Ymoyen)) + DPG
  '
End Sub

'Calcolo misura Wildabler da SON misurato
Sub Calcolo_Wk(NW As Double, ByRef Wmola As Double, AngPreNor As Double, AngEli As Double, NZ As Integer, ModNor As Double, SpeCirNorMis As Double)

Dim ALFAn    As Double
Dim ALFAt    As Double
Dim Beta     As Double
Dim Betab    As Double
Dim AM       As Double
Dim Z        As Integer
Dim SONmola  As Double
Dim SONtmola As Double
Dim T1       As Double
Dim CS1      As Double
Dim CS2      As Double
Dim F3       As Double
Dim RX       As Double
Dim R        As Double
Dim Rb       As Double
Dim PB       As Double
Dim DB       As Double
Dim Sb       As Double
Dim DP       As Double
Dim PG       As Double
'
On Error Resume Next
  '
  AM = ModNor
  Z = NZ
  PG = 3.141592654
  '
  'GRADI -> RADIANTI
  ALFAn = FnGR(AngPreNor)
  Beta = FnGR(Abs(AngEli))
  '
  'CALCOLO COSTANTI
  ALFAt = Atn(Tan(ALFAn) / Cos(Beta))
  T1 = Tan(ALFAt)
  CS1 = Cos(ALFAt)
  CS2 = Cos(Beta)
  F3 = T1 - Atn(T1)            'INVOLUTA(ALFAON)
  R = AM * Z / 2 / Cos(Beta)   'R primitivo
  Betab = Atn(Tan(Beta) * CS1) 'Elica base
  Rb = R * CS1                 'R base
  PB = (Rb * 2 * PG) / Z       'Passo di base
  DB = 2 * Rb                  'diam. base
  DP = 2 * R                   'diam. primitivo
  '
  SONmola = SpeCirNorMis
  '
  If (NW > 0) Then
    SONtmola = SONmola / Cos(Beta)
    Sb = Rb * (SONtmola / R + 2 * F3)
    Wmola = -Cos(Betab) * (PB - Sb - NW * PB)
  End If
  '
End Sub

Function LeggiFile_INGRANAGGI(ByVal PathFILE As String) As Integer
'
Dim i           As Integer
Dim j           As Integer
Dim k           As Integer
Dim riga        As String
Dim ZMax        As Single
Dim Zmin        As Single
Dim Zlen        As Single
Dim AB_SMS      As Integer
Dim stmp        As String
Dim SAP_C       As Double
Dim EAP_C       As Double
Dim PIni        As Integer
Dim PFin        As Integer
Dim sDATI       As String
Dim DBB         As Database
Dim RSS         As Recordset
Const CIFRE = 8
'
Dim vQUOTA_RULLI     As Double
Dim vDIAMETRO_RULLI  As Double
Dim sDIAMETRO_RULLI  As String
Dim vQUOTA_WILDHABER As Double
Dim vDENTI_WILDHABER As Double
Dim sDENTI_WILDHABER As String
Dim TIPO_LAVORAZIONE As String
'
On Error GoTo errLeggiFile_INGRANAGGI
  '
  LeggiFile_INGRANAGGI = False
  '
  Dim TipoCalcolo As String
  TipoCalcolo = GetInfo("CHK0", "TipoCalcolo", Path_LAVORAZIONE_INI): TipoCalcolo = UCase$(Trim$(TipoCalcolo))
  If (TipoCalcolo = "") Then TipoCalcolo = "SVG"
  '
  'LETTURA DEL TIPO DI LAVORAZIONE
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  'Inizio lettura del file dei risultati di controllo
  Open PathFILE For Input As 1
    Input #1, SAP, EAP
    Input #1, AngPreNor, AngEli
    'Input #1, NZ, ModNor
    Line Input #1, stmp: stmp = Trim$(stmp)
    If (Len(stmp) > 0) Then
      'AGGIORNO L'INDICE
      i = InStr(stmp, " ")
      'RICAVO NZ
      NZ = val(Left$(stmp, i - 1))
      'AGGIORNO LA RIGA
      stmp = Trim$(Right$(stmp, Len(stmp) - i))
      
      'AGGIORNO L'INDICE
      i = InStr(stmp, " ")
      If (i > 0) Then
        'RICAVO ModNor
         ModNor = val(Left$(stmp, i - 1))
        'AGGIORNO LA RIGA
         stmp = Trim$(Right$(stmp, Len(stmp) - i))
         
        'AGGIORNO L'INDICE
         i = InStr(stmp, " ")
        'RICAVO CorPrF1T
         CorPrF1T = val(Left$(stmp, i - 1))
        'AGGIORNO LA RIGA
         stmp = Trim$(Right$(stmp, Len(stmp) - i))
         
        'AGGIORNO L'INDICE
         i = InStr(stmp, " ")
        'RICAVO CorPrF1T
         CorPrF2T = val(Left$(stmp, i - 1))
        'AGGIORNO LA RIGA
         stmp = Trim$(Right$(stmp, Len(stmp) - i))
         
        'AGGIORNO L'INDICE
         i = InStr(stmp, " ")
        'RICAVO CorElF1T
         CorElF1T = val(Left$(stmp, i - 1))
        'AGGIORNO LA RIGA
         stmp = Trim$(Right$(stmp, Len(stmp) - i))
         
        'RICAVO CorElF2T
         CorElF2T = val(stmp)
      Else
        'RICAVO ModNor
         ModNor = val(stmp)
        'ASSEGNO I VALORI DI DEFAULT
         CorPrF1T = 0: CorPrF2T = 0: CorElF1T = 0: CorElF2T = 0
      End If
    End If
        
    Input #1, SpeCirNorMis, AB_SMS
    
    Line Input #1, stmp: stmp = Trim$(stmp)
    
    If (Len(stmp) > 0) Then
      '
      'AGGIORNO L'INDICE
      i = InStr(stmp, " ")
      'RICAVO NP
      Np = val(Left$(stmp, i - 1))
      'AGGIORNO LA RIGA
      stmp = Trim$(Right$(stmp, Len(stmp) - i))
      '
      'AGGIORNO L'INDICE
      i = InStr(stmp, " ")
      'RICAVO LunCon
      LunCon = val(Left$(stmp, i - 1))
      'AGGIORNO LA RIGA
      stmp = Trim$(Right$(stmp, Len(stmp) - i))
      '
      If (Len(stmp) > 0) Then
        'AGGIORNO L'INDICE
        i = InStr(stmp, " ")
        'RICAVO iPd(1)
        iPd(1) = val(Left$(stmp, i - 1))
        'AGGIORNO LA RIGA
        stmp = Trim$(Right$(stmp, Len(stmp) - i))
        'AGGIORNO L'INDICE
        i = InStr(stmp, " ")
        'RICAVO iPd(2)
        iPd(2) = val(Left$(stmp, i - 1))
        'AGGIORNO LA RIGA
        stmp = Trim$(Right$(stmp, Len(stmp) - i))
        'AGGIORNO L'INDICE
        i = InStr(stmp, " ")
        'RICAVO iPd(3)
        iPd(3) = val(Left$(stmp, i - 1))
        'AGGIORNO LA RIGA
        stmp = Trim$(Right$(stmp, Len(stmp) - i))
        'AGGIORNO L'INDICE
        i = InStr(stmp, " ")
        If (i > 0) Then
          'RICAVO iPd(4)
          iPd(4) = val(Left$(stmp, i - 1))
          'AGGIORNO LA RIGA
          riga = Trim$(Right$(stmp, Len(stmp) - i))
          '
          'CALCOLO QUOTA RULLI E WILDHABER
          If (Len(riga) > 0) And (TIPO_LAVORAZIONE = "INGR380_ESTERNI") Then
            'LETTURA DEI DATI DAL DATABASE
            Set DBB = OpenDatabase(PathDB, True, False)
            Set RSS = DBB.OpenRecordset("ARCHIVIO_INGR380_ESTERNI")
            RSS.Index = "NOME_PEZZO"
            RSS.Seek "=", riga
            If RSS.NoMatch Then
              'SEGNALAZIONE UTENTE
              WRITE_DIALOG "WARNING: " & riga & " not found."
              'CHIUDO IL DATABASE
              RSS.Close
              DBB.Close
              'LISTA VUOTA --> NON ESEGUO IL CALCOLO DELLA QUOTA RULLI
              sDATI = "": sQUOTA_RULLI = "": sQUOTA_WILDHABER = ""
            Else
              If Not IsNull(RSS.Fields("DATI").Value) Then
                sDATI = RSS.Fields("DATI").Value
                If (Len(sDATI) <= 0) Then
                  'SEGNALAZIONE UTENTE
                  WRITE_DIALOG riga & "--> WARNING: INVALID PROFILE DATA"
                  'CHIUDO IL DATABASE
                  RSS.Close
                  DBB.Close
                  'LISTA VUOTA --> NON ESEGUO IL CALCOLO DELLA QUOTA RULLI
                  sDATI = "": sQUOTA_RULLI = "": sQUOTA_WILDHABER = ""
                Else
                  'CHIUDO IL DATABASE
                  RSS.Close
                  DBB.Close
                End If
              Else
                'SEGNALAZIONE UTENTE
                WRITE_DIALOG riga & "--> WARNING: INVALID PROFILE DATA"
                'CHIUDO IL DATABASE
                RSS.Close
                DBB.Close
                'LISTA VUOTA --> NON ESEGUO IL CALCOLO DELLA QUOTA RULLI
                sDATI = "": sQUOTA_RULLI = "": sQUOTA_WILDHABER = ""
              End If
            End If
            If (Len(sDATI) > 0) Then
                'Estrai denti Wk
                i = InStr(sDATI, "R[48]")
                k = InStr(i, sDATI, "=")
                j = InStr(i, sDATI, Chr(13))
                sDENTI_WILDHABER = Trim$(Mid(sDATI, k + 1, j - k - 1))
                vDENTI_WILDHABER = val(sDENTI_WILDHABER)
                'Estrai Diametro rulli
                i = InStr(sDATI, "R[50]")
                k = InStr(i, sDATI, "=")
                j = InStr(i, sDATI, Chr(13))
                sDIAMETRO_RULLI = Trim$(Mid(sDATI, k + 1, j - k - 1))
                vDIAMETRO_RULLI = val(sDIAMETRO_RULLI)
            End If
            If (vDIAMETRO_RULLI > 0) Then
              Call Calcolo_Qr(vDIAMETRO_RULLI, vQUOTA_RULLI, AngPreNor, AngEli, NZ, ModNor, SpeCirNorMis)
              sQUOTA_RULLI = "Q(" & frmt(vDIAMETRO_RULLI, 4) & ")=" & frmt(vQUOTA_RULLI, 3)
            Else
              sQUOTA_RULLI = ""
            End If
            If (vDENTI_WILDHABER > 0) Then
              Call Calcolo_Wk(vDENTI_WILDHABER, vQUOTA_WILDHABER, AngPreNor, AngEli, NZ, ModNor, SpeCirNorMis)
              sQUOTA_WILDHABER = "W(" & Format$(vDENTI_WILDHABER, "###0") & ")=" & frmt(vQUOTA_WILDHABER, 3)
            Else
              sQUOTA_WILDHABER = ""
            End If
          End If
          '
          'CALCOLO QUOTA RULLI E WILDHABER
          If (Len(riga) > 0) And (TIPO_LAVORAZIONE = "INGRANAGGI_ESTERNIV") Then
            'LETTURA DEI DATI DAL DATABASE
            Set DBB = OpenDatabase(PathDB, True, False)
            Set RSS = DBB.OpenRecordset("ARCHIVIO_INGRANAGGI_ESTERNIV")
            RSS.Index = "NOME_PEZZO"
            RSS.Seek "=", riga
            If RSS.NoMatch Then
              'SEGNALAZIONE UTENTE
              WRITE_DIALOG "WARNING: " & riga & " not found."
              'CHIUDO IL DATABASE
              RSS.Close
              DBB.Close
              'LISTA VUOTA --> NON ESEGUO IL CALCOLO DELLA QUOTA RULLI
              sDATI = "": sQUOTA_RULLI = "": sQUOTA_WILDHABER = ""
            Else
              If Not IsNull(RSS.Fields("DATI").Value) Then
                sDATI = RSS.Fields("DATI").Value
                If (Len(sDATI) <= 0) Then
                  'SEGNALAZIONE UTENTE
                  WRITE_DIALOG riga & "--> WARNING: INVALID PROFILE DATA"
                  'CHIUDO IL DATABASE
                  RSS.Close
                  DBB.Close
                  'LISTA VUOTA --> NON ESEGUO IL CALCOLO DELLA QUOTA RULLI
                  sDATI = "": sQUOTA_RULLI = "": sQUOTA_WILDHABER = ""
                Else
                  'CHIUDO IL DATABASE
                  RSS.Close
                  DBB.Close
                End If
              Else
                'SEGNALAZIONE UTENTE
                WRITE_DIALOG riga & "--> WARNING: INVALID PROFILE DATA"
                'CHIUDO IL DATABASE
                RSS.Close
                DBB.Close
                'LISTA VUOTA --> NON ESEGUO IL CALCOLO DELLA QUOTA RULLI
                sDATI = "": sQUOTA_RULLI = "": sQUOTA_WILDHABER = ""
              End If
            End If
            If (Len(sDATI) > 0) Then
                'Estrai denti Wk
                i = InStr(sDATI, "R[48]")
                k = InStr(i, sDATI, "=")
                j = InStr(i, sDATI, Chr(13))
                sDENTI_WILDHABER = Trim$(Mid(sDATI, k + 1, j - k - 1))
                vDENTI_WILDHABER = val(sDENTI_WILDHABER)
                'Estrai Diametro rulli
                i = InStr(sDATI, "R[50]")
                k = InStr(i, sDATI, "=")
                j = InStr(i, sDATI, Chr(13))
                sDIAMETRO_RULLI = Trim$(Mid(sDATI, k + 1, j - k - 1))
                vDIAMETRO_RULLI = val(sDIAMETRO_RULLI)
            End If
            If (vDIAMETRO_RULLI > 0) Then
              Call Calcolo_Qr(vDIAMETRO_RULLI, vQUOTA_RULLI, AngPreNor, AngEli, NZ, ModNor, SpeCirNorMis)
              sQUOTA_RULLI = "Q(" & frmt(vDIAMETRO_RULLI, 4) & ")=" & frmt(vQUOTA_RULLI, 3)
            Else
              sQUOTA_RULLI = ""
            End If
            If (vDENTI_WILDHABER > 0) Then
              Call Calcolo_Wk(vDENTI_WILDHABER, vQUOTA_WILDHABER, AngPreNor, AngEli, NZ, ModNor, SpeCirNorMis)
              sQUOTA_WILDHABER = "W(" & Format$(vDENTI_WILDHABER, "###0") & ")=" & frmt(vQUOTA_WILDHABER, 3)
            Else
              sQUOTA_WILDHABER = ""
            End If
          End If
          '
          'CALCOLO QUOTA RULLI E WILDHABER
          If (Len(riga) > 0) And (TIPO_LAVORAZIONE = "INGRANAGGI_ESTERNIO") Then
            'LETTURA DEI DATI DAL DATABASE
            Set DBB = OpenDatabase(PathDB, True, False)
            Set RSS = DBB.OpenRecordset("ARCHIVIO_INGRANAGGI_ESTERNIO")
            RSS.Index = "NOME_PEZZO"
            RSS.Seek "=", riga
            If RSS.NoMatch Then
              'SEGNALAZIONE UTENTE
              WRITE_DIALOG "WARNING: " & riga & " not found."
              'CHIUDO IL DATABASE
              RSS.Close
              DBB.Close
              'LISTA VUOTA --> NON ESEGUO IL CALCOLO DELLA QUOTA RULLI
              sDATI = "": sQUOTA_RULLI = "": sQUOTA_WILDHABER = ""
            Else
              If Not IsNull(RSS.Fields("DATI").Value) Then
                sDATI = RSS.Fields("DATI").Value
                If (Len(sDATI) <= 0) Then
                  'SEGNALAZIONE UTENTE
                  WRITE_DIALOG riga & "--> WARNING: INVALID PROFILE DATA"
                  'CHIUDO IL DATABASE
                  RSS.Close
                  DBB.Close
                  'LISTA VUOTA --> NON ESEGUO IL CALCOLO DELLA QUOTA RULLI
                  sDATI = "": sQUOTA_RULLI = "": sQUOTA_WILDHABER = ""
                Else
                  'CHIUDO IL DATABASE
                  RSS.Close
                  DBB.Close
                End If
              Else
                'SEGNALAZIONE UTENTE
                WRITE_DIALOG riga & "--> WARNING: INVALID PROFILE DATA"
                'CHIUDO IL DATABASE
                RSS.Close
                DBB.Close
                'LISTA VUOTA --> NON ESEGUO IL CALCOLO DELLA QUOTA RULLI
                sDATI = "": sQUOTA_RULLI = "": sQUOTA_WILDHABER = ""
              End If
            End If
            If (Len(sDATI) > 0) Then
                'Estrai denti Wk
                i = InStr(sDATI, "R[48]")
                k = InStr(i, sDATI, "=")
                j = InStr(i, sDATI, Chr(13))
                sDENTI_WILDHABER = Trim$(Mid(sDATI, k + 1, j - k - 1))
                vDENTI_WILDHABER = val(sDENTI_WILDHABER)
                'Estrai Diametro rulli
                i = InStr(sDATI, "R[50]")
                k = InStr(i, sDATI, "=")
                j = InStr(i, sDATI, Chr(13))
                sDIAMETRO_RULLI = Trim$(Mid(sDATI, k + 1, j - k - 1))
                vDIAMETRO_RULLI = val(sDIAMETRO_RULLI)
            End If
            If (vDIAMETRO_RULLI > 0) Then
              Call Calcolo_Qr(vDIAMETRO_RULLI, vQUOTA_RULLI, AngPreNor, AngEli, NZ, ModNor, SpeCirNorMis)
              sQUOTA_RULLI = "Q(" & frmt(vDIAMETRO_RULLI, 4) & ")=" & frmt(vQUOTA_RULLI, 3)
            Else
              sQUOTA_RULLI = ""
            End If
            If (vDENTI_WILDHABER > 0) Then
              Call Calcolo_Wk(vDENTI_WILDHABER, vQUOTA_WILDHABER, AngPreNor, AngEli, NZ, ModNor, SpeCirNorMis)
              sQUOTA_WILDHABER = "W(" & Format$(vDENTI_WILDHABER, "###0") & ")=" & frmt(vQUOTA_WILDHABER, 3)
            Else
              sQUOTA_WILDHABER = ""
            End If
          End If
          '
          'CALCOLO QUOTA RULLI E WILDHABER
          If (Len(riga) > 0) And (TIPO_LAVORAZIONE = "INGRANAGGI_INTERNI") Then
            'LETTURA DEI DATI DAL DATABASE
            Set DBB = OpenDatabase(PathDB, True, False)
            Set RSS = DBB.OpenRecordset("ARCHIVIO_INGRANAGGI_INTERNI")
            RSS.Index = "NOME_PEZZO"
            RSS.Seek "=", riga
            If RSS.NoMatch Then
              'SEGNALAZIONE UTENTE
              WRITE_DIALOG "WARNING: " & riga & " not found."
              'CHIUDO IL DATABASE
              RSS.Close
              DBB.Close
              'LISTA VUOTA --> NON ESEGUO IL CALCOLO DELLA QUOTA RULLI
              sDATI = "": sQUOTA_RULLI = "": sQUOTA_WILDHABER = ""
            Else
              If Not IsNull(RSS.Fields("DATI").Value) Then
                sDATI = RSS.Fields("DATI").Value
                If (Len(sDATI) <= 0) Then
                  'SEGNALAZIONE UTENTE
                  WRITE_DIALOG riga & "--> WARNING: INVALID PROFILE DATA"
                  'CHIUDO IL DATABASE
                  RSS.Close
                  DBB.Close
                  'LISTA VUOTA --> NON ESEGUO IL CALCOLO DELLA QUOTA RULLI
                  sDATI = "": sQUOTA_RULLI = "": sQUOTA_WILDHABER = ""
                Else
                  'CHIUDO IL DATABASE
                  RSS.Close
                  DBB.Close
                End If
              Else
                'SEGNALAZIONE UTENTE
                WRITE_DIALOG riga & "--> WARNING: INVALID PROFILE DATA"
                'CHIUDO IL DATABASE
                RSS.Close
                DBB.Close
                'LISTA VUOTA --> NON ESEGUO IL CALCOLO DELLA QUOTA RULLI
                sDATI = "": sQUOTA_RULLI = "": sQUOTA_WILDHABER = ""
              End If
            End If
            If (Len(sDATI) > 0) Then
                'Estrai Diametro rulli
                i = InStr(sDATI, "R[50]")
                k = InStr(i, sDATI, "=")
                j = InStr(i, sDATI, Chr(13))
                sDIAMETRO_RULLI = Trim$(Mid(sDATI, k + 1, j - k - 1))
                vDIAMETRO_RULLI = val(sDIAMETRO_RULLI)
            End If
            If (vDIAMETRO_RULLI > 0) Then
              Call Calcolo_Qr_Int(vDIAMETRO_RULLI, vQUOTA_RULLI, AngPreNor, AngEli, NZ, ModNor, SpeCirNorMis)
              sQUOTA_RULLI = "Q(" & frmt(vDIAMETRO_RULLI, 4) & ")=" & frmt(vQUOTA_RULLI, 3)
            Else
              sQUOTA_RULLI = ""
            End If
          End If
          '
        Else
          'con controllo su 4 denti
          'RICAVO iPd(4)
          iPd(4) = val(stmp)
        End If
        '
      Else
        'un principio per SERIE10
        iPd(1) = 0: iPd(2) = 0: iPd(3) = 0: iPd(4) = 0
      End If
      '
    Else
      '
      WRITE_DIALOG "ERROR on FILE"
      Close #1
      Exit Function
      '
    End If
    '
    'Select Case AB_SMS
    '  '
    '  Case 1    'con controllo su 4 denti
    '    Input #1, Np, LunCon, iPd(1), iPd(2), iPd(3), iPd(4)
    '  '
    '  Case 2    'con nome pezzo per tolleranze
    '    Input #1, Np, LunCon, iPd(1), iPd(2), iPd(3), iPd(4), riga
    '  '
    '  Case Else 'un principio per SERIE10
    '    Input #1, Np, LunCon
    '    iPd(1) = 0: iPd(2) = 0: iPd(3) = 0: iPd(4) = 0
    '  '
    'End Select
    '
    If (Np <> 0) Then
      '
      ReDim Pd0f1(Np): ReDim Pd0f2(Np): ReDim HyP(Np)
      For i = 1 To Np: HyP(i) = (i - 1) * LunCon / (Np - 1): Next i
      '
      If Not IsNumeric(CHK0.Text1(2)) Then CHK0.Text1(2).Text = frmt(Idx2Dia(Np), 3)
      If Not IsNumeric(CHK0.Text1(3)) Then CHK0.Text1(3).Text = frmt(Idx2Dia(1), 3)
      '
      If (val(CHK0.Text1(3).Text) > val(CHK0.Text1(2).Text)) Then
        CHK0.Text1(3).Text = frmt(Idx2Dia(1), 3)
        CHK0.Text1(2).Text = frmt(Idx2Dia(Np), 3)
      End If
      If val(CHK0.Text1(2).Text) > Idx2Dia(Np) Then CHK0.Text1(2).Text = frmt(Idx2Dia(Np), 3)
      If val(CHK0.Text1(3).Text) > Idx2Dia(Np) Then CHK0.Text1(3).Text = frmt(Idx2Dia(Np), 3)
      If val(CHK0.Text1(2).Text) < Idx2Dia(1) Then CHK0.Text1(2).Text = frmt(Idx2Dia(Np), 3)
      If val(CHK0.Text1(3).Text) < Idx2Dia(1) Then CHK0.Text1(3).Text = frmt(Idx2Dia(1), 3)
      '
      CHK_Fin_PROFILO = Dia2Idx(val(CHK0.Text1(2).Text))
      CHK_Ini_PROFILO = Dia2Idx(val(CHK0.Text1(3).Text))
      '
      PIni = CHK_Ini_PROFILO:  PFin = CHK_Fin_PROFILO
      '
      ReDim Pd1f1(Np): ReDim Pd1f2(Np)
      For i = 1 To Np
        Input #1, Pd1f1(i), Pd1f2(i)
        Pd1f1(i) = Pd1f1(i) + CorPrF1T / (Np - 1) * (i - 1)
        Pd1f2(i) = Pd1f2(i) + CorPrF2T / (Np - 1) * (i - 1)
      Next i
      Input #1, FaF1d(1), faF2d(1)
      Input #1, ffaF1d(1), ffaF2d(1)
      Input #1, fhaF1d(1), fhaF2d(1)
      If (TipoCalcolo = "SVG") Then
        For i = 1 To Np
          Pd0f1(i) = Pd1f1(i)
          Pd0f2(i) = Pd1f2(i)
        Next i
        Call CalcoloFaFHaFFa_Int(1, PIni, PFin)
      End If
      ReDim Pd2f1(Np): ReDim Pd2f2(Np)
      If (iPd(2) <> 0) Then
        For i = 1 To Np
          Input #1, Pd2f1(i), Pd2f2(i)
          Pd2f1(i) = Pd2f1(i) + CorPrF1T / (Np - 1) * (i - 1)
          Pd2f2(i) = Pd2f2(i) + CorPrF2T / (Np - 1) * (i - 1)
        Next i
        Input #1, FaF1d(2), faF2d(2)
        Input #1, ffaF1d(2), ffaF2d(2)
        Input #1, fhaF1d(2), fhaF2d(2)
        If (TipoCalcolo = "SVG") Then
          For i = 1 To Np
            Pd0f1(i) = Pd2f1(i)
            Pd0f2(i) = Pd2f2(i)
          Next i
          Call CalcoloFaFHaFFa_Int(2, PIni, PFin)
        End If
      End If
      ReDim Pd3f1(Np): ReDim Pd3f2(Np)
      If (iPd(3) <> 0) Then
        For i = 1 To Np
          Input #1, Pd3f1(i), Pd3f2(i)
          Pd3f1(i) = Pd3f1(i) + CorPrF1T / (Np - 1) * (i - 1)
          Pd3f2(i) = Pd3f2(i) + CorPrF2T / (Np - 1) * (i - 1)
        Next i
        Input #1, FaF1d(3), faF2d(3)
        Input #1, ffaF1d(3), ffaF2d(3)
        Input #1, fhaF1d(3), fhaF2d(3)
        If (TipoCalcolo = "SVG") Then
          For i = 1 To Np
            Pd0f1(i) = Pd3f1(i)
            Pd0f2(i) = Pd3f2(i)
          Next i
          Call CalcoloFaFHaFFa_Int(3, PIni, PFin)
        End If
      End If
      ReDim Pd4f1(Np): ReDim Pd4f2(Np)
      If (iPd(4) <> 0) Then
        For i = 1 To Np
          Input #1, Pd4f1(i), Pd4f2(i)
          Pd4f1(i) = Pd4f1(i) + CorPrF1T / (Np - 1) * (i - 1)
          Pd4f2(i) = Pd4f2(i) + CorPrF2T / (Np - 1) * (i - 1)
        Next i
        Input #1, FaF1d(4), faF2d(4)
        Input #1, ffaF1d(4), ffaF2d(4)
        Input #1, fhaF1d(4), fhaF2d(4)
        If (TipoCalcolo = "SVG") Then
          For i = 1 To Np
            Pd0f1(i) = Pd4f1(i)
            Pd0f2(i) = Pd4f2(i)
          Next i
          Call CalcoloFaFHaFFa_Int(4, PIni, PFin)
        End If
      End If
      ZMax = Pd1f1(1): Zmin = Pd1f1(1)
      For i = 1 To Np
        If Pd1f1(i) > ZMax Then ZMax = Pd1f1(i)
        If Pd1f1(i) < Zmin Then Zmin = Pd1f1(i)
      Next i
      For i = 1 To Np
        If Pd1f2(i) > ZMax Then ZMax = Pd1f2(i)
        If Pd1f2(i) < Zmin Then Zmin = Pd1f2(i)
      Next i
      If (iPd(2) <> 0) Then
        For i = 1 To Np
          If Pd2f1(i) > ZMax Then ZMax = Pd2f1(i)
          If Pd2f1(i) < Zmin Then Zmin = Pd2f1(i)
        Next i
        For i = 1 To Np
          If Pd2f2(i) > ZMax Then ZMax = Pd2f2(i)
          If Pd2f2(i) < Zmin Then Zmin = Pd2f2(i)
        Next i
      End If
      If (iPd(3) <> 0) Then
        For i = 1 To Np
          If Pd3f1(i) > ZMax Then ZMax = Pd3f1(i)
          If Pd3f1(i) < Zmin Then Zmin = Pd3f1(i)
        Next i
        For i = 1 To Np
          If Pd3f2(i) > ZMax Then ZMax = Pd3f2(i)
          If Pd3f2(i) < Zmin Then Zmin = Pd3f2(i)
        Next i
      End If
      If (iPd(4) <> 0) Then
        For i = 1 To Np
          If Pd4f1(i) > ZMax Then ZMax = Pd4f1(i)
          If Pd4f1(i) < Zmin Then Zmin = Pd4f1(i)
        Next i
        For i = 1 To Np
          If Pd4f2(i) > ZMax Then ZMax = Pd4f2(i)
          If Pd4f2(i) < Zmin Then Zmin = Pd4f2(i)
        Next i
      End If
      Zlen = ZMax - Zmin
    End If
    '
    Select Case AB_SMS
      '
      Case 1    'con controllo su 4 denti
        Input #1, NE, FasCon, iEd(1), iEd(2), iEd(3), iEd(4)
        '
      Case 2    'con nome pezzo per tolleranze
        Input #1, NE, FasCon, iEd(1), iEd(2), iEd(3), iEd(4)
        '
      Case Else 'un principio per SERIE10
        Input #1, NE, FasCon
        iEd(1) = 0: iEd(2) = 0: iEd(3) = 0: iEd(4) = 0
        '
    End Select
    '
    If (NE > 1) Then
      ReDim Ed0f1(NE): ReDim Ed0f2(NE): ReDim HyE(NE)
      For i = 1 To NE: HyE(i) = (i - 1) * FasCon / (NE - 1): Next i
      '
      If (val(CHK0.Text1(0).Text) <= 0) Or (val(CHK0.Text1(0).Text) >= 100) Then
        PIni = 1: PFin = NE: CHK0.Text1(0).Text = "100"
      Else
        '
        Dim PERCENTUALE     As Single
        Dim FASCIA_RIDOTTA  As Single
        Dim FASCIA          As Single
        Dim Delta           As Single
        '
        PERCENTUALE = val(CHK0.Text1(0).Text) / 100
        FASCIA_RIDOTTA = FasCon * PERCENTUALE
        Delta = FasCon / (NE - 1)
        '
        PIni = 1: PFin = NE
        FASCIA = Delta * (PFin - PIni)
        While (FASCIA > FASCIA_RIDOTTA)
          PFin = PFin - 1: PIni = PIni + 1
          FASCIA = Delta * (PFin - PIni)
        Wend
        If PIni = PFin Then PIni = 1: PFin = NE
        '
      End If
      CHK_Ini_ELICA = PIni: CHK_Fin_ELICA = PFin
      '
      ReDim Ed1f1(NE): ReDim Ed1f2(NE)
      For i = 1 To NE
        Input #1, Ed1f1(i), Ed1f2(i)
        If AngEli < 0 Then
          Ed1f1(i) = Ed1f1(i) - CorElF1T / (NE - 1) * (i - 1)
          Ed1f2(i) = Ed1f2(i) + CorElF2T / (NE - 1) * (i - 1)
        Else
          Ed1f1(i) = Ed1f1(i) + CorElF1T / (NE - 1) * (i - 1)
          Ed1f2(i) = Ed1f2(i) - CorElF2T / (NE - 1) * (i - 1)
        End If
      Next i
      Input #1, fbF1d(1), fbF2d(1)
        Input #1, ffbF1d(1), ffbF2d(1)
        Input #1, fhbF1d(1), fhbF2d(1)
        If (TipoCalcolo = "SVG") Then
          For i = 1 To NE
            Ed0f1(i) = Ed1f1(i)
            Ed0f2(i) = Ed1f2(i)
          Next i
          Call CalcoloFbFHbFFb_Int(1, PIni, PFin)
        End If
      ReDim Ed2f1(NE): ReDim Ed2f2(NE)
      If (iEd(2) <> 0) Then
        For i = 1 To NE
          Input #1, Ed2f1(i), Ed2f2(i)
          If AngEli < 0 Then
            Ed2f1(i) = Ed2f1(i) - CorElF1T / (NE - 1) * (i - 1)
            Ed2f2(i) = Ed2f2(i) + CorElF2T / (NE - 1) * (i - 1)
          Else
            Ed2f1(i) = Ed2f1(i) + CorElF1T / (NE - 1) * (i - 1)
            Ed2f2(i) = Ed2f2(i) - CorElF2T / (NE - 1) * (i - 1)
          End If
        Next i
        Input #1, fbF1d(2), fbF2d(2)
        Input #1, ffbF1d(2), ffbF2d(2)
        Input #1, fhbF1d(2), fhbF2d(2)
        If (TipoCalcolo = "SVG") Then
          For i = 1 To NE
            Ed0f1(i) = Ed2f1(i)
            Ed0f2(i) = Ed2f2(i)
          Next i
          Call CalcoloFbFHbFFb_Int(2, PIni, PFin)
        End If
      End If
      ReDim Ed3f1(NE): ReDim Ed3f2(NE)
      If (iEd(3) <> 0) Then
        For i = 1 To NE
          Input #1, Ed3f1(i), Ed3f2(i)
          If AngEli < 0 Then
             Ed3f1(i) = Ed3f1(i) - CorElF1T / (NE - 1) * (i - 1)
             Ed3f2(i) = Ed3f2(i) + CorElF2T / (NE - 1) * (i - 1)
          Else
             Ed3f1(i) = Ed3f1(i) + CorElF1T / (NE - 1) * (i - 1)
             Ed3f2(i) = Ed3f2(i) - CorElF2T / (NE - 1) * (i - 1)
          End If
        Next i
        Input #1, fbF1d(3), fbF2d(3)
        Input #1, ffbF1d(3), ffbF2d(3)
        Input #1, fhbF1d(3), fhbF2d(3)
        If (TipoCalcolo = "SVG") Then
          For i = 1 To NE
            Ed0f1(i) = Ed3f1(i)
            Ed0f2(i) = Ed3f2(i)
          Next i
          Call CalcoloFbFHbFFb_Int(3, PIni, PFin)
        End If
      End If
      ReDim Ed4f1(NE): ReDim Ed4f2(NE)
      If (iEd(4) <> 0) Then
        For i = 1 To NE
          Input #1, Ed4f1(i), Ed4f2(i)
          If AngEli < 0 Then
            Ed4f1(i) = Ed4f1(i) - CorElF1T / (NE - 1) * (i - 1)
            Ed4f2(i) = Ed4f2(i) + CorElF2T / (NE - 1) * (i - 1)
          Else
            Ed4f1(i) = Ed4f1(i) + CorElF1T / (NE - 1) * (i - 1)
            Ed4f2(i) = Ed4f2(i) - CorElF2T / (NE - 1) * (i - 1)
          End If
        Next i
        Input #1, fbF1d(4), fbF2d(4)
        Input #1, ffbF1d(4), ffbF2d(4)
        Input #1, fhbF1d(4), fhbF2d(4)
        If (TipoCalcolo = "SVG") Then
          For i = 1 To NE
            Ed0f1(i) = Ed4f1(i)
            Ed0f2(i) = Ed4f2(i)
          Next i
          Call CalcoloFbFHbFFb_Int(4, PIni, PFin)
        End If
      End If
      ZMax = Ed1f1(1): Zmin = Ed1f1(1)
      For i = 1 To NE
        If Ed1f1(i) > ZMax Then ZMax = Ed1f1(i)
        If Ed1f1(i) < Zmin Then Zmin = Ed1f1(i)
      Next i
      For i = 1 To NE
        If Ed1f2(i) > ZMax Then ZMax = Ed1f2(i)
        If Ed1f2(i) < Zmin Then Zmin = Ed1f2(i)
      Next i
      If (iEd(2) <> 0) Then
        For i = 1 To NE
          If Ed2f1(i) > ZMax Then ZMax = Ed2f1(i)
          If Ed2f1(i) < Zmin Then Zmin = Ed2f1(i)
        Next i
        For i = 1 To NE
          If Ed2f2(i) > ZMax Then ZMax = Ed2f2(i)
          If Ed2f2(i) < Zmin Then Zmin = Ed2f2(i)
        Next i
      End If
      If (iEd(3) <> 0) Then
        For i = 1 To NE
          If Ed3f1(i) > ZMax Then ZMax = Ed3f1(i)
          If Ed3f1(i) < Zmin Then Zmin = Ed3f1(i)
        Next i
        For i = 1 To NE
          If Ed3f2(i) > ZMax Then ZMax = Ed3f2(i)
          If Ed3f2(i) < Zmin Then Zmin = Ed3f2(i)
        Next i
      End If
      If (iEd(4) <> 0) Then
        For i = 1 To NE
          If Ed4f1(i) > ZMax Then ZMax = Ed4f1(i)
          If Ed4f1(i) < Zmin Then Zmin = Ed4f1(i)
        Next i
        For i = 1 To NE
          If Ed4f2(i) > ZMax Then ZMax = Ed4f2(i)
          If Ed4f2(i) < Zmin Then Zmin = Ed4f2(i)
        Next i
      End If
      Zlen = ZMax - Zmin
    End If
    '------------------------------------------------------------------------
    Input #1, DivCon, MoreUse
    DivCon = Int(DivCon)
    If (DivCon = 1) Then
      ReDim Divf1(NZ): ReDim Divf2(NZ): ReDim HxD(NZ)
      For i = 1 To NZ
        Input #1, Divf1(i), Divf2(i)
        HxD(i) = (i - 0.5) * Box1W / NZ
      Next i
      Input #1, NZMinF1, ErrMinF1
      Input #1, NZMaxF1, ErrMaxF1
      Input #1, NZMinF2, ErrMinF2
      Input #1, NZMaxF2, ErrMaxF2
      If (TipoCalcolo = "SVG") Then
        Dim iMax As Integer
        Dim iMIN As Integer
        'FIANCO 1
        ZMax = Divf1(1): Zmin = Divf1(1)
        For i = 1 To NZ
          If Divf1(i) >= ZMax Then ZMax = Divf1(i): iMax = i
          If Divf1(i) <= Zmin Then Zmin = Divf1(i): iMIN = i
        Next i
        NZMaxF1 = iMax: ErrMaxF1 = ZMax
        NZMinF1 = iMIN: ErrMinF1 = Zmin
        'FIANCO 2
        ZMax = Divf2(1): Zmin = Divf2(1)
        For i = 1 To NZ
          If Divf2(i) >= ZMax Then ZMax = Divf2(i): iMax = i
          If Divf2(i) <= Zmin Then Zmin = Divf2(i): iMIN = i
        Next i
        NZMaxF2 = iMax: ErrMaxF2 = ZMax
        NZMinF2 = iMIN: ErrMinF2 = Zmin
        'FIANCO 1
        FPmaxF(1) = (Divf1(2) - Divf1(1)): FPminF(1) = (Divf1(2) - Divf1(1))
        For i = 1 To NZ - 1
          If (Divf1(i + 1) - Divf1(i)) >= FPmaxF(1) Then FPmaxF(1) = (Divf1(i + 1) - Divf1(i))
          If (Divf1(i + 1) - Divf1(i)) <= FPminF(1) Then FPminF(1) = (Divf1(i + 1) - Divf1(i))
        Next i
        'FIANCO 2
        FPmaxF(2) = (Divf2(2) - Divf2(1)): FPminF(2) = (Divf2(2) - Divf2(1))
        For i = 1 To NZ - 1
          If (Divf2(i + 1) - Divf2(i)) >= FPmaxF(2) Then FPmaxF(2) = (Divf2(i + 1) - Divf2(i))
          If (Divf2(i + 1) - Divf2(i)) <= FPminF(2) Then FPminF(2) = (Divf2(i + 1) - Divf2(i))
        Next i
      End If
      ZMax = Divf1(1): Zmin = Divf1(1)
      For i = 1 To NZ
        If Divf1(i) > ZMax Then ZMax = Divf1(i)
        If Divf1(i) < Zmin Then Zmin = Divf1(i)
      Next i
      For i = 1 To NZ
        If Divf2(i) > ZMax Then ZMax = Divf2(i)
        If Divf2(i) < Zmin Then Zmin = Divf2(i)
      Next i
      Zlen = ZMax - Zmin
    End If
  If Not EOF(1) Then
    Input #1, stmp
    If stmp Like "EVAL*" Then
    
    End If
  End If
  Close #1
  LeggiFile_INGRANAGGI = True
  '
Exit Function

errLeggiFile_INGRANAGGI:
  If FreeFile > 0 Then Close
  LeggiFile_INGRANAGGI = False
  WRITE_DIALOG "SUB LeggiFile_INGRANAGGI ERROR " & str(Err)
  'Exit Function
  Resume Next

End Function

Sub CalcoloFbFHbFFb(ByVal j As Integer)
'
Dim sXh   As Double
Dim sYh   As Double
Dim sXhXh As Double
Dim sXhYh As Double
Dim B     As Double
Dim A     As Double
Dim qMAX  As Double
Dim qMIN  As Double
'
On Error GoTo errCalcoloFbFHbFFb
  '
  'FIANCO 1 -----------------------------------------------------------
  sXh = Ed0f1(1)
  sYh = Ed0f1(1)
  For i = 1 To NE
    If Ed0f1(i) <= sXh Then sXh = Ed0f1(i)
    If Ed0f1(i) >= sYh Then sYh = Ed0f1(i)
  Next i
  fbF1d(j) = sYh - sXh
  '--------------------------------------------------------------------
  sXh = 0:   For i = 1 To NE: sXh = sXh + HyE(i):                Next i
  sYh = 0:   For i = 1 To NE: sYh = sYh + Ed0f1(i):              Next i
  sXhXh = 0: For i = 1 To NE: sXhXh = sXhXh + HyE(i) * HyE(i):   Next i
  sXhYh = 0: For i = 1 To NE: sXhYh = sXhYh + HyE(i) * Ed0f1(i): Next i
  'Coefficiente angolare della retta di regressione -------------------
  B = (NE * sXhYh - sXh * sYh) / (NE * sXhXh - sXh * sXh)
  fhbF1d(j) = B * HyE(NE)
  'PER ELICA SINISTRA CAMBIO IL SEGNO DI fhb DI F1
  If (AngEli < 0) Then fhbF1d(j) = -fhbF1d(j)
  'Costante minima delle rette parallele a quella di regressione ------
  qMIN = B * HyE(1) - Ed0f1(1)
  qMAX = B * HyE(1) - Ed0f1(1)
  For i = 1 To NE
    sXh = B * HyE(i) - Ed0f1(i)
    If sXh <= qMIN Then qMIN = sXh
    If sXh >= qMAX Then qMAX = sXh
  Next i
  'Calcolo della distanza massima dalla retta -------------------------
  ffbF1d(j) = Abs(qMIN - qMAX) / Sqr(1 + B * B)
  'FIANCO 2 -----------------------------------------------------------
  sXh = Ed0f2(1)
  sYh = Ed0f2(1)
  For i = 1 To NE
    If Ed0f2(i) <= sXh Then sXh = Ed0f2(i)
    If Ed0f2(i) >= sYh Then sYh = Ed0f2(i)
  Next i
  fbF2d(j) = sYh - sXh
  '--------------------------------------------------------------------
  sXh = 0:   For i = 1 To NE: sXh = sXh + HyE(i):                Next i
  sYh = 0:   For i = 1 To NE: sYh = sYh + Ed0f2(i):              Next i
  sXhXh = 0: For i = 1 To NE: sXhXh = sXhXh + HyE(i) * HyE(i):   Next i
  sXhYh = 0: For i = 1 To NE: sXhYh = sXhYh + HyE(i) * Ed0f2(i): Next i
  'Coefficiente angolare della retta di regressione -------------------
  B = (NE * sXhYh - sXh * sYh) / (NE * sXhXh - sXh * sXh)
  fhbF2d(j) = B * HyE(NE)
  'PER ELICA DESTRA CAMBIO IL SEGNO DI fhb DI F2
  If (AngEli > 0) Then fhbF2d(j) = -fhbF2d(j)
  'Costante minima delle rette parallele a quella di regressione ------
  qMIN = B * HyE(1) - Ed0f2(1)
  qMAX = B * HyE(1) - Ed0f2(1)
  For i = 1 To NE
    sXh = B * HyE(i) - Ed0f2(i)
    If sXh <= qMIN Then qMIN = sXh
    If sXh >= qMAX Then qMAX = sXh
  Next i
  'Calcolo della distanza massima dalla retta -------------------------
  ffbF2d(j) = Abs(qMIN - qMAX) / Sqr(1 + B * B)
  '
Exit Sub

errCalcoloFbFHbFFb:
  WRITE_DIALOG "Sub CalcoloFbFHbFFb: ERROR -> " & Err
  If FreeFile > 0 Then Close
  Exit Sub

End Sub

Sub CalcoloFaFHaFFa(ByVal j As Integer)
'
Dim sXh   As Double
Dim sYh   As Double
Dim sXhXh As Double
Dim sXhYh As Double
Dim B     As Double
Dim A     As Double
Dim qMAX  As Double
Dim qMIN  As Double
'
On Error GoTo errCalcoloFaFHaFFa
  '
  'FIANCO 1 -----------------------------------------------------------
  sXh = Pd0f1(1)
  sYh = Pd0f1(1)
  For i = 1 To Np
    If Pd0f1(i) <= sXh Then sXh = Pd0f1(i)
    If Pd0f1(i) >= sYh Then sYh = Pd0f1(i)
  Next i
  FaF1d(j) = sYh - sXh
  '--------------------------------------------------------------------
  sXh = 0:   For i = 1 To Np: sXh = sXh + HyP(i):                Next i
  sYh = 0:   For i = 1 To Np: sYh = sYh + Pd0f1(i):              Next i
  sXhXh = 0: For i = 1 To Np: sXhXh = sXhXh + HyP(i) * HyP(i):   Next i
  sXhYh = 0: For i = 1 To Np: sXhYh = sXhYh + HyP(i) * Pd0f1(i): Next i
  'Coefficiente angolare della retta di regressione
  B = (Np * sXhYh - sXh * sYh) / (Np * sXhXh - sXh * sXh)
  fhaF1d(j) = B * HyP(Np)
  'Costante minima delle rette parallele a quella di regressione ------
  qMIN = B * HyP(1) - Pd0f1(1)
  qMAX = B * HyP(1) - Pd0f1(1)
  For i = 1 To Np
    sXh = B * HyP(i) - Pd0f1(i)
    If sXh <= qMIN Then qMIN = sXh
    If sXh >= qMAX Then qMAX = sXh
  Next i
  'Calcolo della distanza massima dalla retta -------------------------
  ffaF1d(j) = Abs(qMIN - qMAX) / Sqr(1 + B * B)
  'FIANCO 2 -----------------------------------------------------------
  sXh = Pd0f2(1)
  sYh = Pd0f2(1)
  For i = 1 To Np
    If Pd0f2(i) <= sXh Then sXh = Pd0f2(i)
    If Pd0f2(i) >= sYh Then sYh = Pd0f2(i)
  Next i
  faF2d(j) = sYh - sXh
  '--------------------------------------------------------------------
  sXh = 0:   For i = 1 To Np: sXh = sXh + HyP(i):                Next i
  sYh = 0:   For i = 1 To Np: sYh = sYh + Pd0f2(i):              Next i
  sXhXh = 0: For i = 1 To Np: sXhXh = sXhXh + HyP(i) * HyP(i):   Next i
  sXhYh = 0: For i = 1 To Np: sXhYh = sXhYh + HyP(i) * Pd0f2(i): Next i
  'Coefficiente angolare della retta di regressione -------------------
  B = (Np * sXhYh - sXh * sYh) / (Np * sXhXh - sXh * sXh)
  fhaF2d(j) = B * HyP(Np)
  'Costante minima delle rette parallele a quella di regressione ------
  qMIN = B * HyP(1) - Pd0f2(1)
  qMAX = B * HyP(1) - Pd0f2(1)
  For i = 1 To Np
    sXh = B * HyP(i) - Pd0f2(i)
    If sXh <= qMIN Then qMIN = sXh
    If sXh >= qMAX Then qMAX = sXh
  Next i
  'Calcolo della distanza massima dalla retta -------------------------
  ffaF2d(j) = Abs(qMIN - qMAX) / Sqr(1 + B * B)
  
Exit Sub

errCalcoloFaFHaFFa:
  WRITE_DIALOG "Sub CalcoloFaFHaFFa: ERROR -> " & Err
  If FreeFile > 0 Then Close
  Exit Sub

End Sub

Sub CalcoloFaFHaFFa_Int(ByVal j As Integer, PIni As Integer, PFin As Integer)
'
Dim sXh   As Double
Dim sYh   As Double
Dim sXhXh As Double
Dim sXhYh As Double
Dim B     As Double
Dim DD    As Double
Dim NpINT As Integer
Dim ii    As Integer
Dim jj    As Integer
'
On Error GoTo errCalcoloFaFHaFFa_Int
  '
  NpINT = PFin - PIni + 1
  'FIANCO 1
  sXh = Pd0f1(PIni): sYh = Pd0f1(PIni)
  For i = PIni To PFin
    If (Pd0f1(i) <= sXh) Then sXh = Pd0f1(i) 'minimo
    If (Pd0f1(i) >= sYh) Then sYh = Pd0f1(i) 'massimo
  Next i
  FaF1d(j) = sYh - sXh 'massimo - minimo
  'Coefficiente angolare della retta di regressione
  sXh = 0:   For i = PIni To PFin: sXh = sXh + HyP(i):                Next i
  sYh = 0:   For i = PIni To PFin: sYh = sYh + Pd0f1(i):              Next i
  sXhXh = 0: For i = PIni To PFin: sXhXh = sXhXh + HyP(i) * HyP(i):   Next i
  sXhYh = 0: For i = PIni To PFin: sXhYh = sXhYh + HyP(i) * Pd0f1(i): Next i
  B = (NpINT * sXhYh - sXh * sYh) / (NpINT * sXhXh - sXh * sXh)
  fhaF1d(j) = B * (HyP(PFin) - HyP(PIni))
  'Calcolo della distanza massima dalla retta
  ffaF1d(j) = 0: DD = 0
  For ii = PIni To PFin
    For jj = PIni To PFin
      DD = Abs(B * (HyP(ii) - HyP(jj)) + (Pd0f1(jj) - Pd0f1(ii))) * Sqr(1 + B * B)
      If (DD >= ffaF1d(j)) Then ffaF1d(j) = DD
    Next jj
  Next ii
  'Calcolo di Fa come somma di ffa e fha
  DD = Abs(ffaF1d(j)) + Abs(fhaF1d(j))
  'If (DD > FaF1d(j)) Then FaF1d(j) = DD
  'FIANCO 2
  sXh = Pd0f2(PIni): sYh = Pd0f2(PIni)
  For i = PIni To PFin
    If Pd0f2(i) <= sXh Then sXh = Pd0f2(i) 'minimo
    If Pd0f2(i) >= sYh Then sYh = Pd0f2(i) 'massimo
  Next i
  faF2d(j) = sYh - sXh 'massimo - minimo
  'Coefficiente angolare della retta di regressione
  sXh = 0:   For i = PIni To PFin: sXh = sXh + HyP(i):                Next i
  sYh = 0:   For i = PIni To PFin: sYh = sYh + Pd0f2(i):              Next i
  sXhXh = 0: For i = PIni To PFin: sXhXh = sXhXh + HyP(i) * HyP(i):   Next i
  sXhYh = 0: For i = PIni To PFin: sXhYh = sXhYh + HyP(i) * Pd0f2(i): Next i
  B = (NpINT * sXhYh - sXh * sYh) / (NpINT * sXhXh - sXh * sXh)
  fhaF2d(j) = B * (HyP(PFin) - HyP(PIni))
  'Calcolo della distanza massima dalla retta
  ffaF2d(j) = 0: DD = 0
  For ii = PIni To PFin
    For jj = PIni To PFin
      DD = Abs(B * (HyP(ii) - HyP(jj)) + (Pd0f2(jj) - Pd0f2(ii))) * Sqr(1 + B * B)
      If (DD >= ffaF2d(j)) Then ffaF2d(j) = DD
    Next jj
  Next ii
  'Calcolo di Fa come somma di ffa e fha
  'DD = Abs(ffaF2d(j)) + Abs(fhaF2d(j))
  'If (DD > faF2d(j)) Then faF2d(j) = DD
  '
Exit Sub

errCalcoloFaFHaFFa_Int:
  WRITE_DIALOG "Sub CalcoloFaFHaFFa_Int: ERROR -> " & Err
  If FreeFile > 0 Then Close
  Exit Sub

End Sub

Sub CalcoloFbFHbFFb_Int(ByVal j As Integer, PIni As Integer, PFin As Integer)
'
Dim sXh   As Double
Dim sYh   As Double
Dim sXhXh As Double
Dim sXhYh As Double
Dim B     As Double
Dim DD    As Double
Dim NpINT As Integer
Dim ii    As Integer
Dim jj    As Integer
'
On Error GoTo errCalcoloFbFHbFFb_Int
  '
  NpINT = PFin - PIni + 1
  'FIANCO 1
  sXh = Ed0f1(PIni): sYh = Ed0f1(PIni)
  For i = PIni To PFin
    If Ed0f1(i) <= sXh Then sXh = Ed0f1(i) 'minimo
    If Ed0f1(i) >= sYh Then sYh = Ed0f1(i) 'massimo
  Next i
  fbF1d(j) = sYh - sXh 'massimo - minimo
  'Coefficiente angolare della retta di regressione
  sXh = 0:   For i = PIni To PFin: sXh = sXh + HyE(i):                Next i
  sYh = 0:   For i = PIni To PFin: sYh = sYh + Ed0f1(i):              Next i
  sXhXh = 0: For i = PIni To PFin: sXhXh = sXhXh + HyE(i) * HyE(i):   Next i
  sXhYh = 0: For i = PIni To PFin: sXhYh = sXhYh + HyE(i) * Ed0f1(i): Next i
  B = (NpINT * sXhYh - sXh * sYh) / (NpINT * sXhXh - sXh * sXh)
  fhbF1d(j) = B * (HyE(PFin) - HyE(PIni))
  'PER ELICA SINISTRA CAMBIO IL SEGNO DI fhb DI F1
  If (AngEli < 0) Then fhbF1d(j) = -fhbF1d(j)
  'Calcolo della distanza massima dalla retta
  ffbF1d(j) = 0: DD = 0
  For ii = PIni To PFin
    For jj = PIni To PFin
      DD = Abs(B * (HyE(ii) - HyE(jj)) + (Ed0f1(jj) - Ed0f1(ii))) * Sqr(1 + B * B)
      If (DD >= ffbF1d(j)) Then ffbF1d(j) = DD
    Next jj
  Next ii
  'FIANCO 2
  sXh = Ed0f2(PIni): sYh = Ed0f2(PIni)
  For i = PIni To PFin
    If Ed0f2(i) <= sXh Then sXh = Ed0f2(i) 'minimo
    If Ed0f2(i) >= sYh Then sYh = Ed0f2(i) 'massimo
  Next i
  fbF2d(j) = sYh - sXh 'massimo - minimo
  'Coefficiente angolare della retta di regressione
  sXh = 0:   For i = PIni To PFin: sXh = sXh + HyE(i):                Next i
  sYh = 0:   For i = PIni To PFin: sYh = sYh + Ed0f2(i):              Next i
  sXhXh = 0: For i = PIni To PFin: sXhXh = sXhXh + HyE(i) * HyE(i):   Next i
  sXhYh = 0: For i = PIni To PFin: sXhYh = sXhYh + HyE(i) * Ed0f2(i): Next i
  B = (NpINT * sXhYh - sXh * sYh) / (NpINT * sXhXh - sXh * sXh)
  fhbF2d(j) = B * (HyE(PFin) - HyE(PIni))
  'PER ELICA DESTRA CAMBIO IL SEGNO DI fhb DI F2
  If (AngEli >= 0) Then fhbF2d(j) = -fhbF2d(j)
  'Calcolo della distanza massima dalla retta
  ffbF2d(j) = 0: DD = 0
  For ii = PIni To PFin
    For jj = PIni To PFin
      DD = Abs(B * (HyE(ii) - HyE(jj)) + (Ed0f2(jj) - Ed0f2(ii))) * Sqr(1 + B * B)
      If (DD >= ffbF2d(j)) Then ffbF2d(j) = DD
    Next jj
  Next ii
  '
Exit Sub

errCalcoloFbFHbFFb_Int:
  WRITE_DIALOG "Sub CalcoloFbFHbFFb_Int: ERROR -> " & Err
  If FreeFile > 0 Then Close
  Exit Sub

End Sub

Sub CalcoloCerchio(Hy() As Double, Xd0() As Double, AM As Double, BM As Double, RM As Double)
'
Dim i     As Long
Dim A     As Double
Dim B     As Double
Dim C     As Double
Dim D     As Double
Dim E     As Double
Dim Sx    As Double
Dim Sy    As Double
Dim Sxy   As Double
Dim Sxy2  As Double
Dim Sx2y  As Double
Dim Sx2   As Double
Dim Sy2   As Double
Dim Sx3   As Double
Dim Sy3   As Double
Dim NT    As Integer
'
On Error GoTo errCalcoloCerchio
  '
  NT = UBound(Xd0)
  Sx = 0:   For i = 1 To NT: Sx = Sx + Xd0(i):                      Next i 'SOMMA X
  Sy = 0:   For i = 1 To NT: Sy = Sy + Hy(i):                       Next i 'SOMMA Y
  Sxy = 0:  For i = 1 To NT: Sxy = Sxy + Xd0(i) * Hy(i):            Next i 'SOMMA X Y
  Sx2 = 0:  For i = 1 To NT: Sx2 = Sx2 + Xd0(i) * Xd0(i):           Next i 'SOMMA X X
  Sx3 = 0:  For i = 1 To NT: Sx3 = Sx3 + Xd0(i) * Xd0(i) * Xd0(i):  Next i 'SOMMA X X X
  Sx2y = 0: For i = 1 To NT: Sx2y = Sx2y + Xd0(i) * Xd0(i) * Hy(i): Next i 'SOMMA X X Y
  Sxy2 = 0: For i = 1 To NT: Sxy2 = Sxy2 + Xd0(i) * Hy(i) * Hy(i):  Next i 'SOMMA X Y Y
  Sy2 = 0:  For i = 1 To NT: Sy2 = Sy2 + Hy(i) * Hy(i):             Next i 'SOMMA Y Y
  Sy3 = 0:  For i = 1 To NT: Sy3 = Sy3 + Hy(i) * Hy(i) * Hy(i):     Next i 'SOMMA Y Y Y
  A = NT * Sx2 - Sx ^ 2  'CALCOLO A
  B = NT * Sxy - Sx * Sy 'CALCOLO B
  C = NT * Sy2 - Sy ^ 2  'CALCOLO C
  D = 0.5 * (NT * Sxy2 - Sx * Sy2 + NT * Sx3 - Sx * Sx2) 'CALCOLO D
  E = 0.5 * (NT * Sx2y - Sy * Sx2 + NT * Sy3 - Sy * Sy2) 'CALCOLO E
  AM = (D * C - B * E) / (A * C - B * B) 'CALCOLO aM
  BM = (A * E - B * D) / (A * C - B * B) 'CALCOLO bM
  'CALCOLO rM
  RM = 0
  For i = 1 To NT
    RM = RM + Sqr((Xd0(i) - AM) ^ 2 + (Hy(i) - BM) ^ 2)
  Next i
  RM = RM / NT
  '
Exit Sub

errCalcoloCerchio:
  WRITE_DIALOG "Sub CalcoloCerchio: ERROR -> " & Err
  Exit Sub

End Sub

Sub ScriviFile_INGRANAGGI(PathFILE As String)
'
Dim i       As Integer
Dim riga    As String
Dim ZMax    As Single
Dim Zmin    As Single
Dim Zlen    As Single
Dim stmp As String
Dim TIPO_LAVORAZIONE As String
'
On Error GoTo errScriviFile_INGRANAGGI
  '
  'LETTURA DEL TIPO DI LAVORAZIONE
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  '
  stmp = LEGGI_NomePezzo(TIPO_LAVORAZIONE)
  '
'
CorPrF1T = LEP("CORRFHA1")
CorPrF2T = LEP("CORRFHA2")
CorElF1T = LEP("CORRFHB1")
CorElF2T = LEP("CORRFHB2")

'*** Inizio scrittura del file dei risultati di controllo *****************
Open PathFILE For Output As 1
  Print #1, SAP, EAP
  Print #1, AngPreNor, AngEli
  Print #1, NZ, ModNor, CorPrF1T, CorPrF2T, CorElF1T, CorElF2T
  Print #1, SpeCirNorMis, "2" 'MoreUse
  '------------------------------------------------------------------------
  Print #1, Np, LunCon, iPd(1), iPd(2), iPd(3), iPd(4), stmp
  '------------------------------------------------------------------------
  If (Np <> 0) Then
    For i = 1 To Np
      Print #1, Pd1f1(i), Pd1f2(i)
    Next i
    Print #1, FaF1d(1), faF2d(1)
    Print #1, ffaF1d(1), ffaF2d(1)
    Print #1, fhaF1d(1), fhaF2d(1)
    If (iPd(2) <> 0) Then
      For i = 1 To Np
        Print #1, Pd2f1(i), Pd2f2(i)
      Next i
      Print #1, FaF1d(2), faF2d(2)
      Print #1, ffaF1d(2), ffaF2d(2)
      Print #1, fhaF1d(2), fhaF2d(2)
    End If
    If (iPd(3) <> 0) Then
      For i = 1 To Np
        Print #1, Pd3f1(i), Pd3f2(i)
      Next i
      Print #1, FaF1d(3), faF2d(3)
      Print #1, ffaF1d(3), ffaF2d(3)
      Print #1, fhaF1d(3), fhaF2d(3)
    End If
    If (iPd(4) <> 0) Then
      For i = 1 To Np
        Print #1, Pd4f1(i), Pd4f2(i)
      Next i
      Print #1, FaF1d(4), faF2d(4)
      Print #1, ffaF1d(4), ffaF2d(4)
      Print #1, fhaF1d(4), fhaF2d(4)
    End If
  End If
  '------------------------------------------------------------------------
  Print #1, NE, FasCon, iEd(1), iEd(2), iEd(3), iEd(4)
  '------------------------------------------------------------------------
  If (NE <> 0) Then
    For i = 1 To NE
      Print #1, Ed1f1(i), Ed1f2(i)
    Next i
    Print #1, fbF1d(1), fbF2d(1)
    Print #1, ffbF1d(1), ffbF2d(1)
    Print #1, fhbF1d(1), fhbF2d(1)
    If (iEd(2) <> 0) Then
      For i = 1 To NE
        Print #1, Ed2f1(i), Ed2f2(i)
      Next i
      Print #1, fbF1d(2), fbF2d(2)
      Print #1, ffbF1d(2), ffbF2d(2)
      Print #1, fhbF1d(2), fhbF2d(2)
    End If
    If (iEd(3) <> 0) Then
      For i = 1 To NE
        Print #1, Ed3f1(i), Ed3f2(i)
      Next i
      Print #1, fbF1d(3), fbF2d(3)
      Print #1, ffbF1d(3), ffbF2d(3)
      Print #1, fhbF1d(3), fhbF2d(3)
    End If
    If (iEd(4) <> 0) Then
      For i = 1 To NE
        Print #1, Ed4f1(i), Ed4f2(i)
      Next i
      Print #1, fbF1d(4), fbF2d(4)
      Print #1, ffbF1d(4), ffbF2d(4)
      Print #1, fhbF1d(4), fhbF2d(4)
    End If
  End If
  '------------------------------------------------------------------------
  Print #1, DivCon, MoreUse
  If DivCon = 1 Then
    For i = 1 To NZ
      Print #1, Divf1(i), Divf2(i)
    Next i
    Print #1, NZMinF1, ErrMinF1
    Print #1, NZMaxF1, ErrMaxF1
    Print #1, NZMinF2, ErrMinF2
    Print #1, NZMaxF2, ErrMaxF2
  End If
  'Print #1, "EVAL", SAP, EAP
Close #1

Exit Sub

errScriviFile_INGRANAGGI:
  Close
  WRITE_DIALOG "SUB ScriviFile_INGRANAGGI: ERROR --> " & str(Err)
  Exit Sub

End Sub

Sub StampaMisuraDIV()
'
Static Com1(4)       As String
Static Com2(4)       As String
Dim TabX             As Single
Dim TabY             As Single
Dim stmp             As String
Dim TextFont         As String
Dim MLeft            As Single
Dim MTop             As Single
Dim NomeControllo    As String
Dim DataControllo    As String
Dim NomeLavorazione  As String
Dim NomePezzo        As String
Dim NomePezzoDEFAULT As String
Dim TIPO_LAVORAZIONE As String
Dim rsp              As Long
Dim k1               As Long
Dim ftmp             As Double
Dim ERRORE           As String
Dim DP               As Double
'
On Error Resume Next
  '
  'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  'RICAVO IL NOME DEL TIPO DI LAVORAZIONE
  NomeLavorazione = RICERCA_NOME_TIPO(TIPO_LAVORAZIONE)
  'LEGGO IL NOME DEL PEZZO
  NomePezzo = LEGGI_NomePezzo(TIPO_LAVORAZIONE)
  'LEGGO IL NOME DEL CONTROLLO
  NomeControllo = GetInfo("CHK0", "FileMisura", Path_LAVORAZIONE_INI)
  'RICAVO LA DATA DAL FILE
  If (Len(NomeControllo) > 0) And (Dir(CHK0.File1.Path & "\" & NomeControllo) <> "") Then
    DataControllo = FileDateTime(CHK0.File1.Path & "\" & NomeControllo)
  Else
    DataControllo = FileDateTime(CHK0.File1.Path & "\DEFAULT")
  End If
  'LETTURA DEI VALORI DAL FILE DEAFULT
  Open PathMIS & "\" & "DEFAULT" For Input As 1
    Line Input #1, stmp
    Line Input #1, stmp
    Line Input #1, stmp
    Input #1, ftmp, rsp
    If (rsp = 2) Then
      Input #1, k1, ftmp, k1, k1, k1, k1, NomePezzoDEFAULT
    End If
  Close #1
  'SEGNALAZIONE UTENTE
  WRITE_DIALOG TitoloDIV & " --> " & Printer.DeviceName
  'CARATTERI DI STAMPA
      If (LINGUA = "CH") Then
    TextFont = "MS Song"
  ElseIf (LINGUA = "RU") Then
    TextFont = "Arial Cyr"
  Else
    TextFont = "Arial"
  End If
  'IMPOSTAZIONE STAMPANTE
  Printer.ScaleMode = 7
  Printer.DrawWidth = 1
  'MARGINI
  MLeft = val(GetInfo("CHK0", "MLeft", Path_LAVORAZIONE_INI))
  MTop = val(GetInfo("CHK0", "MTop", Path_LAVORAZIONE_INI))
  'Linea di separazione
  X1 = MLeft: Y1 = (MTop + 1) + 2.5 + 0 * 8
  X2 = MLeft + Box0W: Y2 = Y1
  Printer.Line (X1, Y1)-(X2, Y2), QBColor(0)
  'Linea di separazione
  X1 = MLeft: Y1 = (MTop + 1) + 2.5 + 1 * 8
  X2 = MLeft + Box0W: Y2 = Y1
  Printer.Line (X1, Y1)-(X2, Y2), QBColor(0)
  'Linea di separazione
  X1 = MLeft: Y1 = (MTop + 1) + 2.5 + 2 * 8
  X2 = MLeft + Box0W: Y2 = Y1
  Printer.Line (X1, Y1)-(X2, Y2), QBColor(0)
  'Linea di separazione verticale
  X1 = MLeft + 2: Y1 = MTop + 1 + 2.5
  X2 = MLeft + 2: Y2 = MTop + Box0H - 3
  Printer.Line (X1, Y1)-(X2, Y2), QBColor(0)
  'Intestazione della pagina
  X1 = MLeft: Y1 = MTop
  'IMMAGINE BITMAP
  Printer.PaintPicture CHK0.PicLogo.Picture, X1, Y1, 2, 2
  Printer.PaintPicture CHK0.PicLogo2.Picture, X1, Y1 + 2, 2, 2
  For i = 1 To 2
    Printer.FontName = TextFont
    Printer.FontSize = 14
  Next i
  'INTESTAZIONE DELLA PAGINA
  TESTO = logo3 & " " & TitoloDIV
  Printer.CurrentX = X1 + Box0W - 0.25 - Printer.TextWidth(TESTO)
  Printer.CurrentY = Y1 + (1 - Printer.TextHeight(TESTO)) / 2
  Printer.Print TESTO
  'STAMPA DEI VALORI PRINCIPALI
  For i = 1 To 2
    Printer.FontName = TextFont
    Printer.FontSize = 10
  Next i
  iRG = Printer.TextHeight("A")
  iCl = Printer.TextWidth("A")
  '*********
  Com1(1) = ""
  Com1(2) = Prendi(211 + 1, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
  Com1(3) = Prendi(211 + 2, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
  Com1(4) = Prendi(211 + 3, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
  '*********
  If Len(NomePezzoDEFAULT) > 0 Then
    Com2(1) = NomeLavorazione & ": " & NomePezzoDEFAULT
  Else
    If NomeControllo = "DEFAULT" Then
      Com2(1) = NomeLavorazione & ": " & NomePezzo
    Else
      Com2(1) = NomeLavorazione
    End If
  End If
  Com2(2) = " : " & frmt(ModNor, 4)
  Com2(3) = " : " & frmt(AngPreNor, 3)
  Com2(4) = " : " & frmt(SpeCirNorMis, 3)
  '*********
  X1 = MLeft + 2 + iCl: Y1 = MTop + 1
  TabX = 0
  For i = 1 To 4
    Printer.CurrentX = X1
    Printer.CurrentY = Y1 + i * iRG
    Printer.Print Com1(i)
    If TabX < Printer.TextWidth(Com1(i)) Then
      TabX = Printer.TextWidth(Com1(i))
    End If
  Next i
  i = 1
    Printer.CurrentX = X1
    Printer.CurrentY = Y1 + i * iRG
    Printer.Print Com2(i)
  For i = 2 To 4
    Printer.CurrentX = X1 + TabX
    Printer.CurrentY = Y1 + i * iRG
    Printer.Print Com2(i)
  Next i
  '*********
  Com1(1) = Prendi(215 + 1, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
  Com1(2) = Prendi(215 + 2, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
  Com1(3) = Prendi(203, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
  Com1(4) = "Date"
  '*********
  Com2(1) = " : " & Format$(NZ, "##0")
  Com2(2) = " : " & frmt(Abs(AngEli), 3)
  If (AngEli < 0) Then
    Com2(2) = Com2(2) & "    (" & SINISTRA & ")"
  Else
    Com2(2) = Com2(2) & "    (" & DESTRA & ")"
  End If
  Com2(3) = " : " & NomeControllo
  Com2(4) = " : " & DataControllo
  '*********
  X1 = MLeft + 2 + 3 + (Box0W - 2) / 2 + iCl: Y1 = MTop + 1
  TabX = 0
  i = 1
    Printer.CurrentX = X1
    Printer.CurrentY = Y1 + (i + 1) * iRG
    Printer.Print Com1(i)
    If TabX < Printer.TextWidth(Com1(i)) Then
      TabX = Printer.TextWidth(Com1(i))
    End If
  For i = 2 To 4
    Printer.CurrentX = X1 - 6
    Printer.CurrentY = Y1 + i * iRG
    Printer.Print Com1(i)
    If TabX < Printer.TextWidth(Com1(i)) Then
      TabX = Printer.TextWidth(Com1(i))
    End If
  Next i
  i = 1
    Printer.CurrentX = X1 + TabX
    Printer.CurrentY = Y1 + (i + 1) * iRG
    Printer.Print Com2(i)
  For i = 2 To 4
    Printer.CurrentX = X1 + TabX - 6
    Printer.CurrentY = Y1 + i * iRG
    Printer.Print Com2(i)
  Next i
  'STAMPA DEI GRAFICI
  For i = 1 To 2
    Printer.FontName = TextFont
    Printer.FontSize = 8
  Next i
  iRG = Printer.TextHeight("A")
  iCl = Printer.TextWidth("A")
  Call VISUALIZZA_DERIVATA_DIVISIONE(Printer)
  Call VISUALIZZA_DIVISIONE(Printer)
  Call VISUALIZZA_CONCENTRICITA(Printer)
  'STAMPA DEI VALORI IMPORTANTI
  For i = 1 To 2
    Printer.FontName = TextFont
    Printer.FontSize = 10
  Next i
  'Linea di separazione
  X1 = MLeft + 0:           Y1 = MTop + 1 + 2.5 + 2 * 8 + 5
  X2 = MLeft + Box0W - 3.5: Y2 = MTop + 1 + 2.5 + 2 * 8 + 5
  Printer.Line (X1, Y1)-(X2, Y2), QBColor(0)
  For i = 1 To 3
    'Linea di separazione
    X1 = MLeft + 0:     Y1 = MTop + 1 + 2.5 + 2 * 8 + 5 + i * 0.5
    X2 = MLeft + Box0W: Y2 = Y1
    Printer.Line (X1, Y1)-(X2, Y2), QBColor(0)
  Next i
  For i = 0 To 2
    'Linea di separazione
    X1 = MLeft + i * 7.5: Y1 = MTop + 1 + 2.5 + 2 * 8 + 5 + 0 * 0.5
    X2 = MLeft + i * 7.5: Y2 = MTop + 1 + 2.5 + 2 * 8 + 5 + 3 * 0.5
    Printer.Line (X1, Y1)-(X2, Y2), QBColor(0)
  Next i
  For i = 1 To 6
    'Linea di separazione
    X1 = MLeft + i * 2.5: Y1 = MTop + 1 + 2.5 + 2 * 8 + 5 + 1 * 0.5
    X2 = MLeft + i * 2.5: Y2 = MTop + 1 + 2.5 + 2 * 8 + 5 + 3 * 0.5
    Printer.Line (X1, Y1)-(X2, Y2), QBColor(0)
  Next i
    'Linea di separazione
    X1 = MLeft + Box0W: Y1 = MTop + 1 + 2.5 + 2 * 8 + 5 + 1 * 0.5
    X2 = MLeft + Box0W: Y2 = MTop + 1 + 2.5 + 2 * 8 + 5 + 3 * 0.5
    Printer.Line (X1, Y1)-(X2, Y2), QBColor(0)
  'CALCOLO DIAMETRO PRIMITIVO
  DP = ModNor * NZ / Cos(Abs(AngEli) * PG / 180)
  For i = 1 To 2
    stmp = " F" & Format$(i, "0") & " "
    Printer.CurrentX = MLeft + (i - 1) * 7.5 + 7.5 / 2 - Printer.TextWidth(stmp) / 2
    Printer.CurrentY = MTop + 1 + 2.5 + 2 * 8 + 5 + 0.25 - Printer.TextHeight(stmp) / 2
    Printer.Print stmp
    ERRORE = GetInfo("CHK0", "DERIVATA_DIVISIONE(" & Format$(i, "0") & ")", Path_LAVORAZIONE_INI)
    stmp = " fp (" & DIN_fpe(ModNor, DP, val(ERRORE)) & ")"
    Printer.CurrentX = MLeft + (i - 1) * 7.5 + 0 * 2.5 + (2.5 / 2) - Printer.TextWidth(stmp) / 2
    Printer.CurrentY = MTop + 1 + 2.5 + 2 * 8 + 5 + 0.75 - Printer.TextHeight(stmp) / 2
    Printer.Print stmp
    stmp = ERRORE
    Printer.CurrentX = MLeft + (i - 1) * 7.5 + 0 * 2.5 + (2.5 / 2) - Printer.TextWidth(stmp) / 2
    Printer.CurrentY = MTop + 1 + 2.5 + 2 * 8 + 5 + 1.25 - Printer.TextHeight(stmp) / 2
    Printer.Print stmp
    ERRORE = GetInfo("CHK0", "fu(" & Format$(i, "0") & ")", Path_LAVORAZIONE_INI)
    stmp = " fu (" & DIN_fu(ModNor, DP, val(ERRORE)) & ")"
    Printer.CurrentX = MLeft + (i - 1) * 7.5 + 1 * 2.5 + (2.5 / 2) - Printer.TextWidth(stmp) / 2
    Printer.CurrentY = MTop + 1 + 2.5 + 2 * 8 + 5 + 0.75 - Printer.TextHeight(stmp) / 2
    Printer.Print stmp
    stmp = ERRORE
    Printer.CurrentX = MLeft + (i - 1) * 7.5 + 1 * 2.5 + (2.5 / 2) - Printer.TextWidth(stmp) / 2
    Printer.CurrentY = MTop + 1 + 2.5 + 2 * 8 + 5 + 1.25 - Printer.TextHeight(stmp) / 2
    Printer.Print stmp
    ERRORE = GetInfo("CHK0", "DIVISIONE(" & Format$(i, "0") & ")", Path_LAVORAZIONE_INI)
    stmp = " Fp (" & DIN_Fp(ModNor, DP, val(ERRORE)) & ")"
    Printer.CurrentX = MLeft + (i - 1) * 7.5 + 2 * 2.5 + (2.5 / 2) - Printer.TextWidth(stmp) / 2
    Printer.CurrentY = MTop + 1 + 2.5 + 2 * 8 + 5 + 0.75 - Printer.TextHeight(stmp) / 2
    Printer.Print stmp
    stmp = ERRORE
    Printer.CurrentX = MLeft + (i - 1) * 7.5 + 2 * 2.5 + (2.5 / 2) - Printer.TextWidth(stmp) / 2
    Printer.CurrentY = MTop + 1 + 2.5 + 2 * 8 + 5 + 1.25 - Printer.TextHeight(stmp) / 2
    Printer.Print stmp
  Next i
    ERRORE = GetInfo("CHK0", "Fr", Path_LAVORAZIONE_INI)
    stmp = " Fr (" & DIN_Fr(ModNor, DP, val(ERRORE)) & ")"
    Printer.CurrentX = MLeft + Box0W - 3.5 / 2 - Printer.TextWidth(stmp) / 2
    Printer.CurrentY = MTop + 1 + 2.5 + 2 * 8 + 5 + 0.75 - Printer.TextHeight(stmp) / 2
    Printer.Print stmp
    stmp = ERRORE
    Printer.CurrentX = MLeft + Box0W - 3.5 / 2 - Printer.TextWidth(stmp) / 2
    Printer.CurrentY = MTop + 1 + 2.5 + 2 * 8 + 5 + 1.25 - Printer.TextHeight(stmp) / 2
    Printer.Print stmp
  Printer.EndDoc
  'SEGNALAZIONE UTENTE
  WRITE_DIALOG TitoloDIV & " --> " & Printer.DeviceName & " OK!!"
  '
End Sub

Sub StampaMisuraDIV_VITI()
'
Static Com1(4)       As String
Static Com2(4)       As String
Dim TabX             As Single
Dim TabY             As Single
Dim stmp             As String
Dim TextFont         As String
Dim MLeft            As Single
Dim MTop             As Single
Dim NomeControllo    As String
Dim DataControllo    As String
Dim NomeLavorazione  As String
Dim NomePezzo        As String
Dim NomePezzoDEFAULT As String
Dim TIPO_LAVORAZIONE As String
Dim rsp              As Long
Dim k1               As Long
Dim ftmp             As Double
'
On Error Resume Next
  '
  'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  'RICAVO IL NOME DEL TIPO DI LAVORAZIONE
  NomeLavorazione = RICERCA_NOME_TIPO(TIPO_LAVORAZIONE)
  'LEGGO IL NOME DEL PEZZO
  NomePezzo = LEGGI_NomePezzo(TIPO_LAVORAZIONE)
  'LEGGO IL NOME DEL CONTROLLO
  NomeControllo = GetInfo("CHK0", "FileMisura", Path_LAVORAZIONE_INI)
  'RICAVO LA DATA DAL FILE
  If (Len(NomeControllo) > 0) And (Dir(CHK0.File1.Path & "\" & NomeControllo) <> "") Then
    DataControllo = FileDateTime(CHK0.File1.Path & "\" & NomeControllo)
  Else
    DataControllo = FileDateTime(CHK0.File1.Path & "\DEFAULT")
  End If
  Open PathMIS & "\" & "DEFAULT" For Input As 1
    For i = 1 To 6
      Line Input #1, NomePezzoDEFAULT
    Next i
  Close #1
  'SEGNALAZIONE UTENTE
  WRITE_DIALOG TitoloDIV & " --> " & Printer.DeviceName
  'CARATTERI DI STAMPA
      If (LINGUA = "CH") Then
    TextFont = "MS Song"
  ElseIf (LINGUA = "RU") Then
    TextFont = "Arial Cyr"
  Else
    TextFont = "Arial"
  End If
  'IMPOSTAZIONE STAMPANTE
  Printer.ScaleMode = 7
  Printer.DrawWidth = 1
  'MARGINI
  MLeft = val(GetInfo("CHK0", "MLeft", Path_LAVORAZIONE_INI))
  MTop = val(GetInfo("CHK0", "MTop", Path_LAVORAZIONE_INI))
  'Linea di separazione
  X1 = MLeft: Y1 = (MTop + 1) + 2.5 + 0 * 8
  X2 = MLeft + Box0W: Y2 = Y1
  Printer.Line (X1, Y1)-(X2, Y2), QBColor(0)
  'Linea di separazione
  X1 = MLeft: Y1 = (MTop + 1) + 2.5 + 1 * 8
  X2 = MLeft + Box0W: Y2 = Y1
  Printer.Line (X1, Y1)-(X2, Y2), QBColor(0)
  'Linea di separazione
  X1 = MLeft: Y1 = (MTop + 1) + 2.5 + 2 * 8
  X2 = MLeft + Box0W: Y2 = Y1
  Printer.Line (X1, Y1)-(X2, Y2), QBColor(0)
  'Linea di separazione verticale
  X1 = MLeft + 2: Y1 = MTop + 1 + 2.5
  X2 = MLeft + 2: Y2 = MTop + Box0H - 3
  Printer.Line (X1, Y1)-(X2, Y2), QBColor(0)
  'Intestazione della pagina
  X1 = MLeft: Y1 = MTop
  'IMMAGINE BITMAP
  Printer.PaintPicture CHK0.PicLogo.Picture, X1, Y1, 2, 2
  Printer.PaintPicture CHK0.PicLogo2.Picture, X1, Y1 + 2, 2, 2
  For i = 1 To 2
    Printer.FontName = TextFont
    Printer.FontSize = 14
  Next i
  'INTESTAZIONE DELLA PAGINA
  TESTO = logo3 & " " & TitoloDIV
  Printer.CurrentX = X1 + Box0W - 0.25 - Printer.TextWidth(TESTO)
  Printer.CurrentY = Y1 + (1 - Printer.TextHeight(TESTO)) / 2
  Printer.Print TESTO
  'STAMPA DEI VALORI PRINCIPALI
  For i = 1 To 2
    Printer.FontName = TextFont
    Printer.FontSize = 10
  Next i
  iRG = Printer.TextHeight("A")
  iCl = Printer.TextWidth("A")
  '*********
  Com1(1) = ""
  Com1(2) = Prendi(211 + 1, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
  Com1(3) = Prendi(211 + 2, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
  Com1(4) = Prendi(211 + 3, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
  '*********
  If Len(NomePezzoDEFAULT) > 0 Then
    Com2(1) = NomeLavorazione & ": " & NomePezzoDEFAULT
  Else
    If NomeControllo = "DEFAULT" Then
      Com2(1) = NomeLavorazione & ": " & NomePezzo
    Else
      Com2(1) = NomeLavorazione
    End If
  End If
  Com2(2) = " : " & frmt(ModNor, 4)
  Com2(3) = " : " & frmt(AngPreNor, 3)
  Com2(4) = " : " & frmt(SpeCirNorMis, 3)
  '*********
  X1 = MLeft + 2 + iCl: Y1 = MTop + 1
  TabX = 0
  For i = 1 To 4
    Printer.CurrentX = X1
    Printer.CurrentY = Y1 + i * iRG
    Printer.Print Com1(i)
    If TabX < Printer.TextWidth(Com1(i)) Then
      TabX = Printer.TextWidth(Com1(i))
    End If
  Next i
  i = 1
    Printer.CurrentX = X1
    Printer.CurrentY = Y1 + i * iRG
    Printer.Print Com2(i)
  For i = 2 To 4
    Printer.CurrentX = X1 + TabX
    Printer.CurrentY = Y1 + i * iRG
    Printer.Print Com2(i)
  Next i
  '*********
  Com1(1) = Prendi(215 + 1, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
  Com1(2) = Prendi(215 + 2, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
  Com1(3) = Prendi(203, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
  Com1(4) = "Date"
  '*********
  Com2(1) = " : " & Format$(NZ, "##0")
  Com2(2) = " : " & frmt(90 - Abs(AngEli), 3)
  If (AngEli < 0) Then
    Com2(2) = Com2(2) & "    (" & SINISTRA & ")"
  Else
    Com2(2) = Com2(2) & "    (" & DESTRA & ")"
  End If
  Com2(3) = " : " & NomeControllo
  Com2(4) = " : " & DataControllo
  '*********
  X1 = MLeft + 2 + 3 + (Box0W - 2) / 2 + iCl: Y1 = MTop + 1
  TabX = 0
  i = 1
    Printer.CurrentX = X1
    Printer.CurrentY = Y1 + (i + 1) * iRG
    Printer.Print Com1(i)
    If TabX < Printer.TextWidth(Com1(i)) Then
      TabX = Printer.TextWidth(Com1(i))
    End If
  For i = 2 To 4
    Printer.CurrentX = X1 - 6
    Printer.CurrentY = Y1 + i * iRG
    Printer.Print Com1(i)
    If TabX < Printer.TextWidth(Com1(i)) Then
      TabX = Printer.TextWidth(Com1(i))
    End If
  Next i
  i = 1
    Printer.CurrentX = X1 + TabX
    Printer.CurrentY = Y1 + (i + 1) * iRG
    Printer.Print Com2(i)
  For i = 2 To 4
    Printer.CurrentX = X1 + TabX - 6
    Printer.CurrentY = Y1 + i * iRG
    Printer.Print Com2(i)
  Next i
  'STAMPA DEI GRAFICI
  For i = 1 To 2
    Printer.FontName = TextFont
    Printer.FontSize = 8
  Next i
  iRG = Printer.TextHeight("A")
  iCl = Printer.TextWidth("A")
  Call VISUALIZZA_DERIVATA_DIVISIONE_VITI(Printer)
  Call VISUALIZZA_DIVISIONE_VITI(Printer)
  Call VISUALIZZA_CONCENTRICITA_VITI(Printer)
  'STAMPA DEI VALORI IMPORTANTI
  For i = 1 To 2
    Printer.FontName = TextFont
    Printer.FontSize = 10
  Next i
  'Linea di separazione
  X1 = MLeft + 0:           Y1 = MTop + 1 + 2.5 + 2 * 8 + 5
  X2 = MLeft + Box0W - 3.5: Y2 = MTop + 1 + 2.5 + 2 * 8 + 5
  Printer.Line (X1, Y1)-(X2, Y2), QBColor(0)
  For i = 1 To 3
    'Linea di separazione
    X1 = MLeft + 0:     Y1 = MTop + 1 + 2.5 + 2 * 8 + 5 + i * 0.5
    X2 = MLeft + Box0W: Y2 = Y1
    Printer.Line (X1, Y1)-(X2, Y2), QBColor(0)
  Next i
  For i = 0 To 2
    'Linea di separazione
    X1 = MLeft + i * 7.5: Y1 = MTop + 1 + 2.5 + 2 * 8 + 5 + 0 * 0.5
    X2 = MLeft + i * 7.5: Y2 = MTop + 1 + 2.5 + 2 * 8 + 5 + 3 * 0.5
    Printer.Line (X1, Y1)-(X2, Y2), QBColor(0)
  Next i
  For i = 1 To 6
    'Linea di separazione
    X1 = MLeft + i * 2.5: Y1 = MTop + 1 + 2.5 + 2 * 8 + 5 + 1 * 0.5
    X2 = MLeft + i * 2.5: Y2 = MTop + 1 + 2.5 + 2 * 8 + 5 + 3 * 0.5
    Printer.Line (X1, Y1)-(X2, Y2), QBColor(0)
  Next i
    'Linea di separazione
    X1 = MLeft + Box0W: Y1 = MTop + 1 + 2.5 + 2 * 8 + 5 + 1 * 0.5
    X2 = MLeft + Box0W: Y2 = MTop + 1 + 2.5 + 2 * 8 + 5 + 3 * 0.5
    Printer.Line (X1, Y1)-(X2, Y2), QBColor(0)
  For i = 1 To 2
    stmp = " F" & Format$(i, "0") & " "
    Printer.CurrentX = MLeft + (i - 1) * 7.5 + 7.5 / 2 - Printer.TextWidth(stmp) / 2
    Printer.CurrentY = MTop + 1 + 2.5 + 2 * 8 + 5 + 0.25 - Printer.TextHeight(stmp) / 2
    Printer.Print stmp
    stmp = " fp "
    Printer.CurrentX = MLeft + (i - 1) * 7.5 + 0 * 2.5 + (2.5 / 2) - Printer.TextWidth(stmp) / 2
    Printer.CurrentY = MTop + 1 + 2.5 + 2 * 8 + 5 + 0.75 - Printer.TextHeight(stmp) / 2
    Printer.Print stmp
    stmp = GetInfo("CHK0", "DERIVATA_DIVISIONE(" & Format$(i, "0") & ")", Path_LAVORAZIONE_INI)
    Printer.CurrentX = MLeft + (i - 1) * 7.5 + 0 * 2.5 + (2.5 / 2) - Printer.TextWidth(stmp) / 2
    Printer.CurrentY = MTop + 1 + 2.5 + 2 * 8 + 5 + 1.25 - Printer.TextHeight(stmp) / 2
    Printer.Print stmp
    stmp = " fu "
    Printer.CurrentX = MLeft + (i - 1) * 7.5 + 1 * 2.5 + (2.5 / 2) - Printer.TextWidth(stmp) / 2
    Printer.CurrentY = MTop + 1 + 2.5 + 2 * 8 + 5 + 0.75 - Printer.TextHeight(stmp) / 2
    Printer.Print stmp
    stmp = GetInfo("CHK0", "fu(" & Format$(i, "0") & ")", Path_LAVORAZIONE_INI)
    Printer.CurrentX = MLeft + (i - 1) * 7.5 + 1 * 2.5 + (2.5 / 2) - Printer.TextWidth(stmp) / 2
    Printer.CurrentY = MTop + 1 + 2.5 + 2 * 8 + 5 + 1.25 - Printer.TextHeight(stmp) / 2
    Printer.Print stmp
    stmp = " Fp "
    Printer.CurrentX = MLeft + (i - 1) * 7.5 + 2 * 2.5 + (2.5 / 2) - Printer.TextWidth(stmp) / 2
    Printer.CurrentY = MTop + 1 + 2.5 + 2 * 8 + 5 + 0.75 - Printer.TextHeight(stmp) / 2
    Printer.Print stmp
    stmp = GetInfo("CHK0", "DIVISIONE(" & Format$(i, "0") & ")", Path_LAVORAZIONE_INI)
    Printer.CurrentX = MLeft + (i - 1) * 7.5 + 2 * 2.5 + (2.5 / 2) - Printer.TextWidth(stmp) / 2
    Printer.CurrentY = MTop + 1 + 2.5 + 2 * 8 + 5 + 1.25 - Printer.TextHeight(stmp) / 2
    Printer.Print stmp
  Next i
    stmp = " Fr "
    Printer.CurrentX = MLeft + Box0W - 3.5 / 2 - Printer.TextWidth(stmp) / 2
    Printer.CurrentY = MTop + 1 + 2.5 + 2 * 8 + 5 + 0.75 - Printer.TextHeight(stmp) / 2
    Printer.Print stmp
    stmp = GetInfo("CHK0", "fr", Path_LAVORAZIONE_INI)
    Printer.CurrentX = MLeft + Box0W - 3.5 / 2 - Printer.TextWidth(stmp) / 2
    Printer.CurrentY = MTop + 1 + 2.5 + 2 * 8 + 5 + 1.25 - Printer.TextHeight(stmp) / 2
    Printer.Print stmp
  'CHIUSURA DEL DOCUMENTO
  Printer.EndDoc
  'SEGNALAZIONE UTENTE
  WRITE_DIALOG TitoloDIV & " --> " & Printer.DeviceName & " OK!!"
  '
End Sub

Sub VisELICA_VITI(X As Control)
'
Dim SiSTIMA_BOMBATURA As Integer
Dim SiGriglia1        As Integer
Dim SiGriglia2        As Integer
Dim Sc1X              As Single
Dim Sc1Y              As Single
Dim k                 As Integer
Dim stmp              As String
Dim SiDisegna         As Integer
Dim MLeft             As Single
Dim MTop              As Single
Dim DBB               As Database
Dim RSS               As Recordset
Dim pXX(2, 9)         As Double
Dim pYY(2, 9)         As Double
Dim pBB(2, 9)         As Double
Dim k1                As Long
Dim k2                As Long
Dim k3                As Long
Dim k4                As Long
Dim rsp               As Long
Dim CODICE_PEZZO      As String
Dim lstTOLLERANZA     As String
Dim ftmp              As Double
Dim AM                As Double
Dim BM                As Double
Dim RM                As Double
Dim ANGOLO            As Double
Dim Bombatura(5)      As Double
Dim Abgn              As Double
Dim Aend              As Double
Dim RRB_cur           As Double
Dim RRB_max           As Double
Dim RRB_min           As Double
Dim M                 As Double
Dim TIPO_MACCHINA     As String
'
On Error GoTo errVisELICA_VITI
  '
  TIPO_MACCHINA = GetInfo("Configurazione", "TIPO_MACCHINA", Path_LAVORAZIONE_INI)
  TIPO_MACCHINA = UCase$(Trim$(TIPO_MACCHINA))
  
  SiGriglia1 = val(GetInfo("CHK0", "Griglia1", Path_LAVORAZIONE_INI))
  SiGriglia2 = val(GetInfo("CHK0", "Griglia2", Path_LAVORAZIONE_INI))
  '
  SiSTIMA_BOMBATURA = val(GetInfo("CHK0", "BOMBATURA_ELICA", Path_LAVORAZIONE_INI))
  stmp = GetInfo("CHK0", "TOLLERANZA_ELICA", Path_LAVORAZIONE_INI):  stmp = UCase$(Trim$(stmp))
  If (stmp <> "Y") Then SiSTIMA_BOMBATURA = 0
  '
  MLeft = val(GetInfo("CHK0", "MLeft", Path_LAVORAZIONE_INI))
  MTop = val(GetInfo("CHK0", "MTop", Path_LAVORAZIONE_INI))
  stmp = GetInfo("CHK0", "ScalaELICAx", Path_LAVORAZIONE_INI):  Sc1X = val(stmp)
  stmp = GetInfo("CHK0", "ScalaELICAy", Path_LAVORAZIONE_INI):  Sc1Y = val(stmp)
  '
  X1 = MLeft + 2: Y1 = (MTop - 0.5 + 1) + 15
  X.DrawWidth = 2
  If (AngEli >= 0) Then
    'SEGNO -
    X.Circle (X1, Y1), 0.25, QBColor(0)
    X.Line (X1 - 0.15, Y1)-(X1 + 0.15, Y1)
  Else
    'SEGNO +
    X.Circle (X1, Y1), 0.25, QBColor(0)
    X.Line (X1, Y1 + 0.15)-(X1, Y1 - 0.15)
    X.Line (X1 - 0.15, Y1)-(X1 + 0.15, Y1)
  End If
  X1 = MLeft + 18: Y1 = (MTop - 0.5 + 1) + 15
  If (AngEli >= 0) Then
    'SEGNO +
    X.Circle (X1, Y1), 0.25, QBColor(0)
    X.Line (X1, Y1 + 0.15)-(X1, Y1 - 0.15)
    X.Line (X1 - 0.15, Y1)-(X1 + 0.15, Y1)
  Else
    'SEGNO -
    X.Circle (X1, Y1), 0.25, QBColor(0)
    X.Line (X1 - 0.15, Y1)-(X1 + 0.15, Y1)
  End If
  X.DrawWidth = 1
  'Sinistra e Destra per zona grafica ELICA
  X1 = MLeft + 2: Y1 = (MTop - 0.5 + 1) + 15 + 5 - 3 - 1
  X.CurrentX = X1 - iCl / 2: X.CurrentY = Y1 - iRG / 2
  X.Print DESTRA
  X1 = MLeft + 2: Y1 = (MTop - 0.5 + 1) + 15 + 5 + 3
  X.CurrentX = X1 - iCl / 2: X.CurrentY = Y1 - iRG / 2
  X.Print SINISTRA
  If (SiGriglia1 = 1) Then
    X1 = MLeft + 3: Y1 = MTop - 0.5 + 16
    Call Disegna_Griglia1(X, CDbl(X1), CDbl(Y1))
  End If
  If (SiGriglia2 = 1) Then
    X1 = MLeft + 3: Y1 = MTop - 0.5 + 16
    Call Disegna_Griglia2(X, CDbl(X1), CDbl(Y1))
  End If
  X1 = MLeft + 3: Y1 = MTop - 0.5 + 16
  X1 = X1 + Box1W / 2: Y1 = Y1
  TESTO = comVisPROELI(1, 2)
  X.CurrentX = X1 - X.TextWidth(TESTO) / 2
  X.CurrentY = Y1 - iRG
  X.FontBold = True
  X.Print TESTO
  X.FontBold = False
  '
  If (SiOpt(4, 1) = 1) Then
    X.DrawStyle = 1
    X1 = MLeft + 3: Y1 = MTop - 0.5 + 16
    X1 = X1 + Box1W / 2: Y1 = Y1
    X2 = X1: Y2 = Y1 + Box1H
    X.Line (X1, Y1)-(X2, Y2), QBColor(0)
    X1 = MLeft + 3: Y1 = MTop - 0.5 + 16
    X1 = X1: Y1 = Y1 + Box1H / 2
    X2 = X1 + Box1W: Y2 = Y1
    X.Line (X1, Y1)-(X2, Y2), QBColor(0)
    X.DrawStyle = 0
  End If
  '
  If (NE <> 0) Then
    X1 = MLeft + 3 + Box1W / 4: Y1 = MTop - 0.5 + 16
    TESTO = "F2"
    X.CurrentX = X1 - X.TextWidth(TESTO) / 2
    X.CurrentY = Y1 - iRG
    X.FontBold = True
    X.Print TESTO
    X.FontBold = False
    If (SiOpt(4, 1) = 1) Then
        k = 1
          X1 = (MLeft + 3 + k * (Box1W / 10))
          Y1 = (MTop - 0.5 + 16 + Box1H / 2)
          Y1 = Y1 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
          Y1 = Y1 - (HyE(1) - HyE(1)) * Sc1Y / 10
          X2 = (MLeft + 3 + k * (Box1W / 10))
          Y2 = (MTop - 0.5 + 16 + Box1H / 2)
          Y2 = Y2 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
          Y2 = Y2 - (HyE(NE) - HyE(1)) * Sc1Y / 10
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
          X1 = (MLeft + 3 + Box1W / 10) - 0.5
          X2 = X1 + 1
          Y1 = (MTop - 0.5 + 16 + Box1H / 2) + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
          Y2 = Y1
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
          X1 = (MLeft + 3 + Box1W / 10) - 0.5
          X2 = X1 + 1
          Y1 = (MTop - 0.5 + 16 + Box1H / 2) - (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
          Y2 = Y1
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
      For k = 2 To 4
        If (iEd(k) <> 0) Then
          X1 = (MLeft + 3 + k * (Box1W / 10))
          Y1 = (MTop - 0.5 + 16 + Box1H / 2)
          Y1 = Y1 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
          Y1 = Y1 - (HyE(1) - HyE(1)) * Sc1Y / 10
          X2 = (MLeft + 3 + k * (Box1W / 10))
          Y2 = (MTop - 0.5 + 16 + Box1H / 2)
          Y2 = Y2 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
          Y2 = Y2 - (HyE(NE) - HyE(1)) * Sc1Y / 10
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
          X1 = (MLeft + 3 + k * (Box1W / 10)) - 0.5
          X2 = X1 + 1
          Y1 = (MTop - 0.5 + 16 + Box1H / 2) + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
          Y2 = Y1
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
          X1 = (MLeft + 3 + k * (Box1W / 10)) - 0.5
          X2 = X1 + 1
          Y1 = (MTop - 0.5 + 16 + Box1H / 2) - (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
          Y2 = Y1
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
        End If
      Next
    End If
    For k = 1 To 4
      SiDisegna = False
      Select Case k
        Case 1
          For i = 1 To NE: Ed0f2(i) = Ed1f2(i): Next i
            GoSub STIMA_BOMBATURA_ELICA_FIANCO2
            SiDisegna = True
        Case 2
          For i = 1 To NE: Ed0f2(i) = Ed2f2(i): Next i
          If (iEd(2) <> 0) Then
            GoSub STIMA_BOMBATURA_ELICA_FIANCO2
            SiDisegna = True
          End If
        Case 3
          For i = 1 To NE: Ed0f2(i) = Ed3f2(i): Next i
          If (iEd(3) <> 0) Then
            GoSub STIMA_BOMBATURA_ELICA_FIANCO2
            SiDisegna = True
          End If
        Case 4
          For i = 1 To NE: Ed0f2(i) = Ed4f2(i): Next i
          If (iEd(4) <> 0) Then
            GoSub STIMA_BOMBATURA_ELICA_FIANCO2
            SiDisegna = True
          End If
      End Select
      If SiDisegna Then
        For i = 1 To NE
          X1 = (MLeft + 3 + k * Box1W / 10)
          X1 = X1 - (Ed0f2(i) - MV(Ed0f2, 1, NE)) * Sc1X / 10
          Y1 = (MTop - 0.5 + 16 + Box1H / 2)
          Y1 = Y1 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
          Y1 = Y1 - (HyE(i) - HyE(1)) * Sc1Y / 10
          'If (SiOpt(0, 1) = 1) Then
          '  R1 = 0.025
          '  X.Circle (X1, Y1), R1, QBColor(0)
          'End If
          'If (SiOpt(1, 1) = 1) Then
          '  X.Line ((MLeft + 3 + k * Box1W / 10), Y1)-(X1, Y1), QBColor(0)
          'End If
          If (i < NE) And (SiOpt(2, 1) = 1) Then
            X2 = (MLeft + 3 + k * Box1W / 10)
            X2 = X2 - (Ed0f2(i + 1) - MV(Ed0f2, 1, NE)) * Sc1X / 10
            Y2 = (MTop - 0.5 + 16 + Box1H / 2)
            Y2 = Y2 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
            Y2 = Y2 - (HyE(i + 1) - HyE(1)) * Sc1Y / 10
            X.Line (X1, Y1)-(X2, Y2), QBColor(0)
          End If
        Next i
        X1 = (MLeft + 3 + k * (Box1W / 10))
        Y1 = (MTop - 0.5 + 16 + Box1H)
        TESTO = Format$(iEd(k), "#")
        X.CurrentX = X1
        X.CurrentY = Y1
        X.Print TESTO
        j = 0
        X1 = (MLeft + 3 + k * (Box1W / 10))
        Y1 = MTop - 0.5 + 25.5 + j * iRG
        TESTO = frmt(fbF2d(k) * 1000, 1)
        X.CurrentX = X1 - X.TextWidth(TESTO) / 2 + iCl
        X.CurrentY = Y1
        X.Print TESTO
        j = 1
        X1 = (MLeft + 3 + k * (Box1W / 10))
        Y1 = MTop - 0.5 + 25.5 + j * iRG
        TESTO = frmt(ffbF2d(k) * 1000, 1)
        X.CurrentX = X1 - X.TextWidth(TESTO) / 2 + iCl
        X.CurrentY = Y1
        X.Print TESTO
        j = 2
        X1 = (MLeft + 3 + k * (Box1W / 10))
        Y1 = MTop - 0.5 + 25.5 + j * iRG
        TESTO = frmt(fhbF2d(k) * 1000, 1)
        X.CurrentX = X1 - X.TextWidth(TESTO) / 2 + iCl
        X.CurrentY = Y1
        X.Print TESTO
        If (SiSTIMA_BOMBATURA = 1) Then
          j = 3
          X1 = (MLeft + 3 + k * (Box1W / 10))
          Y1 = MTop - 0.5 + 25.5 + j * iRG
          TESTO = frmt(Bombatura(k) * 1000, 1)
          X.CurrentX = X1 - X.TextWidth(TESTO) / 2 + iCl
          X.CurrentY = Y1
          X.Print TESTO
        End If
      End If
    Next k
    For j = 0 To 2
      X1 = MLeft
      Y1 = MTop - 0.5 + 25.5 + j * iRG
      TESTO = comVisPROELI(4 + j, 2)
      X.CurrentX = X1 + (1.5 - X.TextWidth(TESTO)) / 2
      X.CurrentY = Y1
      X.Print TESTO
      X.Line (X1, Y1 + iRG)-(X1 + Box0W, Y1 + iRG), QBColor(0)
    Next j
    '
    If (SiSTIMA_BOMBATURA = 1) Then
      j = 3
        X1 = MLeft
        Y1 = MTop - 0.5 + 25.5 + j * iRG
        TESTO = "Cb" 'comVisPROELI(7, 2)
        X.CurrentX = X1 + (1.5 - X.TextWidth(TESTO)) / 2
        X.CurrentY = Y1
        X.Print TESTO
        X.Line (X1, Y1 + iRG)-(X1 + Box0W, Y1 + iRG), QBColor(0)
    End If
    '
    X1 = (MLeft + 3 + Box1W / 10) - 1
    X2 = X1 + 2
    Y1 = (MTop - 0.5 + 16 + Box1H / 2) + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
    Y2 = Y1
    'x.Line (X1, Y1)-(X2, Y2), QBColor(0)
    '
    TESTO = frmt(-FasCon / 2, 3)
    X.CurrentX = X1 - X.TextWidth(TESTO) - iCl
    X.CurrentY = Y1 - iRG / 2
    X.Print TESTO
    '
    X1 = (MLeft + 3 + Box1W / 10) - 1
    X2 = X1 + 2
    Y1 = (MTop - 0.5 + 16 + Box1H / 2) - (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
    Y2 = Y1
    'x.Line (X1, Y1)-(X2, Y2), QBColor(0)
    '
    TESTO = frmt(FasCon / 2, 3)
    X.CurrentX = X1 - X.TextWidth(TESTO) - iCl
    X.CurrentY = Y1 - iRG / 2
    X.Print TESTO
    '
    X1 = MLeft + 3 + 3 * Box1W / 4: Y1 = MTop - 0.5 + 16
    TESTO = "F1"
    X.CurrentX = X1 - X.TextWidth(TESTO) / 2
    X.CurrentY = Y1 - iRG
    X.FontBold = True
    X.Print TESTO
    X.FontBold = False
    '
    If (SiOpt(4, 1) = 1) Then
        k = 1
          X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
          Y1 = (MTop - 0.5 + 16 + Box1H / 2)
          Y1 = Y1 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
          Y1 = Y1 - (HyE(1) - HyE(1)) * Sc1Y / 10
          X2 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
          Y2 = (MTop - 0.5 + 16 + Box1H / 2)
          Y2 = Y2 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
          Y2 = Y2 - (HyE(NE) - HyE(1)) * Sc1Y / 10
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
          X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10))) - 0.5
          X2 = X1 + 1
          Y1 = (MTop - 0.5 + 16 + Box1H / 2) + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
          Y2 = Y1
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
          X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10))) - 0.5
          X2 = X1 + 1
          Y1 = (MTop - 0.5 + 16 + Box1H / 2) - (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
          Y2 = Y1
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
      For k = 2 To 4
        If (iEd(k) <> 0) Then
          X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
          Y1 = (MTop - 0.5 + 16 + Box1H / 2)
          Y1 = Y1 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
          Y1 = Y1 - (HyE(1) - HyE(1)) * Sc1Y / 10
          X2 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
          Y2 = (MTop - 0.5 + 16 + Box1H / 2)
          Y2 = Y2 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
          Y2 = Y2 - (HyE(NE) - HyE(1)) * Sc1Y / 10
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
          X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10))) - 0.5
          X2 = X1 + 1
          Y1 = (MTop - 0.5 + 16 + Box1H / 2) + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
          Y2 = Y1
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
          X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10))) - 0.5
          X2 = X1 + 1
          Y1 = (MTop - 0.5 + 16 + Box1H / 2) - (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
          Y2 = Y1
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
        End If
      Next
    End If
    For k = 1 To 4
      SiDisegna = False
      Select Case k
        Case 1
          For i = 1 To NE: Ed0f1(i) = Ed1f1(i): Next i
            GoSub STIMA_BOMBATURA_ELICA_FIANCO1
            SiDisegna = True
        Case 2
          For i = 1 To NE: Ed0f1(i) = Ed2f1(i): Next i
          If (iEd(2) <> 0) Then
            GoSub STIMA_BOMBATURA_ELICA_FIANCO1
            SiDisegna = True
          End If
        Case 3
          For i = 1 To NE: Ed0f1(i) = Ed3f1(i): Next i
          If (iEd(3) <> 0) Then
            GoSub STIMA_BOMBATURA_ELICA_FIANCO1
            SiDisegna = True
          End If
        Case 4
          For i = 1 To NE: Ed0f1(i) = Ed4f1(i): Next i
          If (iEd(4) <> 0) Then
            GoSub STIMA_BOMBATURA_ELICA_FIANCO1
            SiDisegna = True
          End If
      End Select
      If SiDisegna Then
        For i = 1 To NE
          X1 = (MLeft + 3 + (Box1W - k * Box1W / 10))
          X1 = X1 + (Ed0f1(i) - MV(Ed0f1, 1, NE)) * Sc1X / 10
          Y1 = (MTop - 0.5 + 16 + Box1H / 2)
          Y1 = Y1 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
          Y1 = Y1 - (HyE(i) - HyE(1)) * Sc1Y / 10
          'If (SiOpt(0, 1) = 1) Then
          '  R1 = 0.025
          '  X.Circle (X1, Y1), R1, QBColor(0)
          'End If
          'If (SiOpt(1, 1) = 1) Then
          '  X.Line ((MLeft + 3 + (Box1W - k * Box1W / 10)), Y1)-(X1, Y1), QBColor(0)
          'End If
          If (i < NE) And (SiOpt(2, 1) = 1) Then
            X2 = (MLeft + 3 + (Box1W - k * Box1W / 10))
            X2 = X2 + (Ed0f1(i + 1) - MV(Ed0f1, 1, NE)) * Sc1X / 10
            Y2 = (MTop - 0.5 + 16 + Box1H / 2)
            Y2 = Y2 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
            Y2 = Y2 - (HyE(i + 1) - HyE(1)) * Sc1Y / 10
            X.Line (X1, Y1)-(X2, Y2), QBColor(0)
          End If
        Next i
        X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
        Y1 = (MTop - 0.5 + 16 + Box1H)
        TESTO = Format$(iEd(k), "#")
        X.CurrentX = X1
        X.CurrentY = Y1
        X.Print TESTO
        j = 0
        X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
        Y1 = MTop - 0.5 + 25.5 + j * iRG
        TESTO = frmt(fbF1d(k) * 1000, 1)
        X.CurrentX = X1 - X.TextWidth(TESTO) / 2 + iCl
        X.CurrentY = Y1
        X.Print TESTO
        j = 1
        X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
        Y1 = MTop - 0.5 + 25.5 + j * iRG
        TESTO = frmt(ffbF1d(k) * 1000, 1)
        X.CurrentX = X1 - X.TextWidth(TESTO) / 2 + iCl
        X.CurrentY = Y1
        X.Print TESTO
        j = 2
        X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
        Y1 = MTop - 0.5 + 25.5 + j * iRG
        TESTO = frmt(fhbF1d(k) * 1000, 1)
        X.CurrentX = X1 - X.TextWidth(TESTO) / 2 + iCl
        X.CurrentY = Y1
        X.Print TESTO
        If (SiSTIMA_BOMBATURA = 1) Then
          j = 3
          X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
          Y1 = MTop - 0.5 + 25.5 + j * iRG
          TESTO = frmt(Bombatura(k) * 1000, 1)
          X.CurrentX = X1 - X.TextWidth(TESTO) / 2 + iCl
          X.CurrentY = Y1
          X.Print TESTO
        End If
      End If
    Next k
    X1 = (MLeft + 3 + (Box1W - Box1W / 10)) - 1
    X2 = X1 + 2
    Y1 = (MTop - 0.5 + 16 + Box1H / 2) + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
    Y2 = Y1
    'x.Line (X1, Y1)-(X2, Y2), QBColor(0)
    TESTO = frmt(-FasCon / 2, 3)
    X.CurrentX = X2 + iCl
    X.CurrentY = Y1 - iRG / 2
    X.Print TESTO
    X1 = (MLeft + 3 + (Box1W - Box1W / 10)) - 1
    X2 = X1 + 2
    Y1 = (MTop - 0.5 + 16 + Box1H / 2) - (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
    Y2 = Y1
    'x.Line (X1, Y1)-(X2, Y2), QBColor(0)
    TESTO = frmt(FasCon / 2, 3)
    X.CurrentX = X2 + iCl
    X.CurrentY = Y1 - iRG / 2
    X.Print TESTO
  End If
  '
  'Scale X e Y del controllo ELICA
  X.DrawStyle = 0
  X1 = MLeft: Y1 = MTop - 0.5 + 16
  X2 = MLeft: Y2 = MTop - 0.5 + 25.5
  Testa = comVisPROELI(2, 2)
  Piede = comVisPROELI(3, 2)
  Call VisScXY(X, Sc1X, Sc1Y, 1, 1)
  X.DrawStyle = 0
  '
  stmp = GetInfo("CHK0", "TOLLERANZA_ELICA", Path_LAVORAZIONE_INI): stmp = UCase$(Trim$(stmp))
  If (stmp <> "Y") Then Exit Sub
  '
  'LETTURA DEL NOME DEL PEZZO ASSOCIATO AL CONTROLLO
  Open PathMIS & "\" & "DEFAULT" For Input As 1
    For i = 1 To 6
      Line Input #1, CODICE_PEZZO
    Next i
  Close #1
  '
  'LETTURA DELLA TOLLERANZA DAL DATABASE
  Set DBB = OpenDatabase(PathDB, True, False)
  If (TIPO_MACCHINA = "GR250") Then
     Set RSS = DBB.OpenRecordset("ARCHIVIO_VITIV_ESTERNE")
  Else
     Set RSS = DBB.OpenRecordset("ARCHIVIO_VITI_ESTERNE")
  End If
    RSS.Index = "NOME_PEZZO"
    RSS.Seek "=", CODICE_PEZZO
    If Not RSS.NoMatch Then
      If Len(RSS.Fields("TOLLERANZA_ELICA").Value) > 0 Then
        lstTOLLERANZA = UCase$(Trim$(RSS.Fields("TOLLERANZA_ELICA").Value))
      Else
        lstTOLLERANZA = ""
      End If
    End If
  RSS.Close
  DBB.Close
  '
  'RICAVO I SINGOLI VALORI DALLA LISTA
  If (Len(lstTOLLERANZA) > 0) Then
    Dim NumeroPuntiUsatiF(2) As Integer
      k1 = InStr(lstTOLLERANZA, ";;")
      If (k1 <= 0) Then
        k1 = InStr(lstTOLLERANZA, ";")
        k2 = InStr(k1 + 1, lstTOLLERANZA, ";")
        k3 = InStr(k2 + 1, lstTOLLERANZA, ";")
        k4 = 1
        For j = 1 To 8
          pXX(1, j) = val(Mid$(lstTOLLERANZA, k4, k1 - k4))
          pYY(1, j) = val(Mid$(lstTOLLERANZA, k1 + 1, k2 - k1 - 1))
          pBB(1, j) = val(Mid$(lstTOLLERANZA, k2 + 1, k3 - k2 - 1))
          k4 = k3 + 1
          k1 = InStr(k4, lstTOLLERANZA, ";")
          k2 = InStr(k1 + 1, lstTOLLERANZA, ";")
          k3 = InStr(k2 + 1, lstTOLLERANZA, ";")
        Next j
        For j = 1 To 8
          pXX(2, j) = val(Mid$(lstTOLLERANZA, k4, k1 - k4))
          pYY(2, j) = val(Mid$(lstTOLLERANZA, k1 + 1, k2 - k1 - 1))
          pBB(2, j) = val(Mid$(lstTOLLERANZA, k2 + 1, k3 - k2 - 1))
          k4 = k3 + 1
          k1 = InStr(k4, lstTOLLERANZA, ";")
          k2 = InStr(k1 + 1, lstTOLLERANZA, ";")
          k3 = InStr(k2 + 1, lstTOLLERANZA, ";")
        Next j
      End If
      i = 1
      NumeroPuntiUsatiF(1) = 8
        For j = 1 To 8
          If (pYY(i, j) = 0) And (pXX(i, j) = 0) Then
            For k1 = j To 8
              pYY(i, k1) = pYY(i, 1):  pXX(i, k1) = pXX(i, 1)
              NumeroPuntiUsatiF(1) = NumeroPuntiUsatiF(1) - 1
            Next k1
            Exit For
          End If
        Next j
      i = 2
      NumeroPuntiUsatiF(2) = 8
        For j = 1 To 8
          If (pYY(i, j) = 0) And (pXX(i, j) = 0) Then
            For k1 = j To 8
              pYY(i, k1) = pYY(i, 1):  pXX(i, k1) = pXX(i, 1)
              NumeroPuntiUsatiF(2) = NumeroPuntiUsatiF(2) - 1
            Next k1
            Exit For
          End If
        Next j
      For j = 1 To 2
        pXX(j, 9) = pXX(j, 1)
        pYY(j, 9) = pYY(j, 1)
      Next j
      'FIANCO 2 TOLLERANZA ELICA --> FIANCO 1 PEZZO
      If NumeroPuntiUsatiF(2) > 1 Then
        For k = 1 To 4
          SiDisegna = False
          Select Case k
            Case 1:                       SiDisegna = True
            Case 2: If (iEd(2) <> 0) Then SiDisegna = True
            Case 3: If (iEd(3) <> 0) Then SiDisegna = True
            Case 4: If (iEd(4) <> 0) Then SiDisegna = True
          End Select
          If SiDisegna Then
            For i = 1 To 8
            If (i <> 8) And (pBB(2, i) = 0) Then
              X1 = (MLeft + 3 + k * Box1W / 10)
              X1 = X1 + pXX(2, i) * Sc1X / 10
              Y1 = (MTop - 0.5 + 16 + Box1H / 2)
              Y1 = Y1 - (pYY(2, i) - HyE(1)) * Sc1Y / 10
              X2 = (MLeft + 3 + k * Box1W / 10)
              X2 = X2 + pXX(2, i + 1) * Sc1X / 10
              Y2 = (MTop - 0.5 + 16 + Box1H / 2)
              Y2 = Y2 - (pYY(2, i + 1) - HyE(1)) * Sc1Y / 10
              X.Line (X1, Y1)-(X2, Y2), QBColor(9)
            Else
              Call visRACCORDA_DUE_PUNTI(X, pXX(2, i), (pYY(2, i) - HyE(1)), pXX(2, i + 1), (pYY(2, i + 1) - HyE(1)), pBB(2, i), "SX", (MLeft + 3 + k * Box1W / 10), (MTop - 0.5 + 16 + Box1H / 2), Sc1X / 10, Sc1Y / 10)
            End If
            Next i
          End If
        Next k
        i = 1
        X.FillStyle = 0
        X.FillColor = QBColor(12)
        X1 = (MLeft + 3 + Box1W / 10)
        X1 = X1 + pXX(2, i) * Sc1X / 10
        Y1 = (MTop - 0.5 + 16 + Box1H / 2)
        Y1 = Y1 - (pYY(2, i) - HyE(1)) * Sc1Y / 10
        X.Circle (X1, Y1), 0.05
        X.FillStyle = 1
        X.FillColor = QBColor(0)
        X.Line (X1 - 0.05, Y1)-(X1 + 0.05, Y1), QBColor(0)
        X.Line (X1, Y1 - 0.05)-(X1, Y1 + 0.05), QBColor(0)
      End If
      'FIANCO 1 TOLLERANZA ELICA --> FIANCO 2 PEZZO
      If NumeroPuntiUsatiF(1) > 1 Then
        For k = 1 To 4
          SiDisegna = False
          Select Case k
            Case 1:                       SiDisegna = True
            Case 2: If (iEd(2) <> 0) Then SiDisegna = True
            Case 3: If (iEd(3) <> 0) Then SiDisegna = True
            Case 4: If (iEd(4) <> 0) Then SiDisegna = True
          End Select
          If SiDisegna Then
            For i = 1 To 8
            If (i <> 8) And (pBB(1, i) = 0) Then
              X1 = (MLeft + 3 + (Box1W - k * Box1W / 10))
              X1 = X1 + pXX(1, i) * Sc1X / 10
              Y1 = (MTop - 0.5 + 16 + Box1H / 2)
              Y1 = Y1 - (pYY(1, i) - HyE(1)) * Sc1Y / 10
              '
              X2 = (MLeft + 3 + (Box1W - k * Box1W / 10))
              X2 = X2 + pXX(1, i + 1) * Sc1X / 10
              Y2 = (MTop - 0.5 + 16 + Box1H / 2)
              Y2 = Y2 - (pYY(1, i + 1) - HyE(1)) * Sc1Y / 10
              X.Line (X1, Y1)-(X2, Y2), QBColor(9)
            Else
              Call visRACCORDA_DUE_PUNTI(X, pXX(1, i), (pYY(1, i) - HyE(1)), pXX(1, i + 1), (pYY(1, i + 1) - HyE(1)), pBB(1, i), "SX", (MLeft + 3 + (Box1W - k * Box1W / 10)), (MTop - 0.5 + 16 + Box1H / 2), Sc1X / 10, Sc1Y / 10)
            End If
            Next i
          End If
        Next k
        i = 1
        X.FillStyle = 0
        X.FillColor = QBColor(12)
        X1 = (MLeft + 3 + (Box1W - Box1W / 10))
        X1 = X1 + pXX(1, i) * Sc1X / 10
        Y1 = (MTop - 0.5 + 16 + Box1H / 2)
        Y1 = Y1 - (pYY(1, i) - HyE(1)) * Sc1Y / 10
        X.Circle (X1, Y1), 0.05
        X.FillStyle = 1
        X.FillColor = QBColor(0)
        X.Line (X1 - 0.05, Y1)-(X1 + 0.05, Y1), QBColor(0)
        X.Line (X1, Y1 - 0.05)-(X1, Y1 + 0.05), QBColor(0)
      End If
  End If
  '
Exit Sub

STIMA_BOMBATURA_ELICA_FIANCO1:
  'VISUALIZZAZIONE RETTA DI REGRESSIONE
  If (SiOpt(4, 1) = 1) Then
    M = fhbF1d(k) / (HyE(NE) - HyE(1))
    'PER ELICA SINISTRA CAMBIO IL SEGNO DI fhb DI F1
    If (AngEli < 0) Then M = -M
    X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
    X1 = X1 - M * (HyE(NE) - HyE(1)) * Sc1X / 10 / 2
    Y1 = (MTop - 0.5 + 16 + Box1H / 2)
    Y1 = Y1 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
    Y1 = Y1 - (HyE(1) - HyE(1)) * Sc1Y / 10
    X2 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
    X2 = X2 + M * (HyE(NE) - HyE(1)) * Sc1X / 10 / 2
    Y2 = (MTop - 0.5 + 16 + Box1H / 2)
    Y2 = Y2 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
    Y2 = Y2 - (HyE(NE) - HyE(1)) * Sc1Y / 10
    X.DrawWidth = 2
    X.Line (X1, Y1)-(X2, Y2), QBColor(12)
    X.DrawWidth = 1
  End If
  '
  'STIMA BOMBATURA CON VISUALIZZAZIONE DELL' ARCO DI CERCHIO SVG 07/11/09
  If (SiSTIMA_BOMBATURA = 1) Then
    Call CalcoloCerchio(HyE, Ed0f1, AM, BM, RM)
    '
    X1 = AM + Sqr(RM ^ 2 - (Ed0f1(1) - BM) ^ 2):  Y1 = HyE(1)
    X2 = AM + Sqr(RM ^ 2 - (Ed0f1(NE) - BM) ^ 2): Y2 = HyE(NE)
    ANGOLO = Sqr((X2 - X1) ^ 2 + (Y2 - Y1) ^ 2)
    ANGOLO = FnASN(ANGOLO / 2 / RM)
    '
    Bombatura(k) = RM * (1 - Cos(ANGOLO))
    '
    ffbF1d(k) = 0: RRB_max = -1E+99: RRB_min = 1E+99
    Select Case k
    Case 1
      For i = 1 To NE
        RRB_cur = Sqr((Ed1f1(i) - AM) ^ 2 + (HyE(i) - BM) ^ 2)
        If (RRB_cur > RRB_max) Then RRB_max = RRB_cur
        If (RRB_cur < RRB_min) Then RRB_min = RRB_cur
      Next i
    Case 2
      For i = 1 To NE
        RRB_cur = Sqr((Ed2f1(i) - AM) ^ 2 + (HyE(i) - BM) ^ 2)
        If (RRB_cur > RRB_max) Then RRB_max = RRB_cur
        If (RRB_cur < RRB_min) Then RRB_min = RRB_cur
      Next i
    Case 3
      For i = 1 To NE
        RRB_cur = Sqr((Ed3f1(i) - AM) ^ 2 + (HyE(i) - BM) ^ 2)
        If (RRB_cur > RRB_max) Then RRB_max = RRB_cur
        If (RRB_cur < RRB_min) Then RRB_min = RRB_cur
      Next i
    Case 4
      For i = 1 To NE
        RRB_cur = Sqr((Ed4f1(i) - AM) ^ 2 + (HyE(i) - BM) ^ 2)
        If (RRB_cur > RRB_max) Then RRB_max = RRB_cur
        If (RRB_cur < RRB_min) Then RRB_min = RRB_cur
      Next i
    End Select
    ffbF1d(k) = RRB_max - RRB_min
    '
    X.DrawWidth = 2
    If (AM > 0) Then
      For i = 1 To NE - 1
        X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
        X1 = X1 + (AM - Sqr(RM ^ 2 - (HyE(i) - BM) ^ 2) - MV(Ed0f1, 1, NE)) * Sc1X / 10
        Y1 = (MTop - 0.5 + 16 + Box1H / 2)
        Y1 = Y1 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
        Y1 = Y1 - HyE(i) * Sc1Y / 10
        '
        X2 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
        X2 = X2 + (AM - Sqr(RM ^ 2 - (HyE(i + 1) - BM) ^ 2) - MV(Ed0f1, 1, NE)) * Sc1X / 10
        Y2 = (MTop - 0.5 + 16 + Box1H / 2)
        Y2 = Y2 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
        Y2 = Y2 - HyE(i + 1) * Sc1Y / 10
        '
        X.Line (X1, Y1)-(X2, Y2), QBColor(12)
      Next i
    Else
      For i = 1 To NE - 1
        X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
        X1 = X1 + (AM + Sqr(RM ^ 2 - (HyE(i) - BM) ^ 2) - MV(Ed0f1, 1, NE)) * Sc1X / 10
        Y1 = (MTop - 0.5 + 16 + Box1H / 2)
        Y1 = Y1 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
        Y1 = Y1 - HyE(i) * Sc1Y / 10
        '
        X2 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
        X2 = X2 + (AM + Sqr(RM ^ 2 - (HyE(i + 1) - BM) ^ 2) - MV(Ed0f1, 1, NE)) * Sc1X / 10
        Y2 = (MTop - 0.5 + 16 + Box1H / 2)
        Y2 = Y2 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
        Y2 = Y2 - HyE(i + 1) * Sc1Y / 10
        '
        X.Line (X1, Y1)-(X2, Y2), QBColor(12)
      Next i
    End If
    X.DrawWidth = 1
    '
  End If
  '
Return

STIMA_BOMBATURA_ELICA_FIANCO2:
  'VISUALIZZAZIONE RETTA DI REGRESSIONE
  If (SiOpt(4, 1) = 1) Then
    M = fhbF2d(k) / (HyE(NE) - HyE(1))
    'PER ELICA DESTRA CAMBIO IL SEGNO DI fhb DI F2
    If (AngEli > 0) Then M = -M
    X1 = (MLeft + 3 + k * (Box1W / 10))
    X1 = X1 + M * (HyE(NE) - HyE(1)) * Sc1X / 10 / 2
    Y1 = (MTop - 0.5 + 16 + Box1H / 2)
    Y1 = Y1 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
    Y1 = Y1 - (HyE(1) - HyE(1)) * Sc1Y / 10
    X2 = (MLeft + 3 + k * (Box1W / 10))
    X2 = X2 - M * (HyE(NE) - HyE(1)) * Sc1X / 10 / 2
    Y2 = (MTop - 0.5 + 16 + Box1H / 2)
    Y2 = Y2 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
    Y2 = Y2 - (HyE(NE) - HyE(1)) * Sc1Y / 10
    X.DrawWidth = 2
    X.Line (X1, Y1)-(X2, Y2), QBColor(12)
    X.DrawWidth = 1
  End If
  '
  'STIMA BOMBATURA CON VISUALIZZAZIONE DELL' ARCO DI CERCHIO SVG 07/11/09
  If (SiSTIMA_BOMBATURA = 1) Then
    Call CalcoloCerchio(HyE, Ed0f2, AM, BM, RM)
    '
    X1 = AM + Sqr(RM ^ 2 - (Ed0f2(1) - BM) ^ 2):  Y1 = HyE(1)
    X2 = AM + Sqr(RM ^ 2 - (Ed0f2(NE) - BM) ^ 2): Y2 = HyE(NE)
    ANGOLO = Sqr((X2 - X1) ^ 2 + (Y2 - Y1) ^ 2)
    ANGOLO = FnASN(ANGOLO / 2 / RM)
    '
    Bombatura(k) = RM * (1 - Cos(ANGOLO))
    '
    ffbF2d(k) = 0: RRB_max = -1E+99: RRB_min = 1E+99
    Select Case k
    Case 1
      For i = 1 To NE
        RRB_cur = Sqr((Ed1f2(i) - AM) ^ 2 + (HyE(i) - BM) ^ 2)
        If (RRB_cur > RRB_max) Then RRB_max = RRB_cur
        If (RRB_cur < RRB_min) Then RRB_min = RRB_cur
      Next i
    Case 2
      For i = 1 To NE
        RRB_cur = Sqr((Ed2f2(i) - AM) ^ 2 + (HyE(i) - BM) ^ 2)
        If (RRB_cur > RRB_max) Then RRB_max = RRB_cur
        If (RRB_cur < RRB_min) Then RRB_min = RRB_cur
      Next i
    Case 3
      For i = 1 To NE
        RRB_cur = Sqr((Ed3f2(i) - AM) ^ 2 + (HyE(i) - BM) ^ 2)
        If (RRB_cur > RRB_max) Then RRB_max = RRB_cur
        If (RRB_cur < RRB_min) Then RRB_min = RRB_cur
      Next i
    Case 4
      For i = 1 To NE
        RRB_cur = Sqr((Ed4f2(i) - AM) ^ 2 + (HyE(i) - BM) ^ 2)
        If (RRB_cur > RRB_max) Then RRB_max = RRB_cur
        If (RRB_cur < RRB_min) Then RRB_min = RRB_cur
      Next i
    End Select
    ffbF2d(k) = RRB_max - RRB_min
    '
    X.DrawWidth = 2
    If (AM < 0) Then
      For i = 1 To NE - 1
        X1 = (MLeft + 3 + k * (Box1W / 10))
        X1 = X1 - (AM + Sqr(RM ^ 2 - (HyE(i) - BM) ^ 2) - MV(Ed0f2, 1, NE)) * Sc1X / 10
        Y1 = (MTop - 0.5 + 16 + Box1H / 2)
        Y1 = Y1 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
        Y1 = Y1 - HyE(i) * Sc1Y / 10
        '
        X2 = (MLeft + 3 + k * (Box1W / 10))
        X2 = X2 - (AM + Sqr(RM ^ 2 - (HyE(i + 1) - BM) ^ 2) - MV(Ed0f2, 1, NE)) * Sc1X / 10
        Y2 = (MTop - 0.5 + 16 + Box1H / 2)
        Y2 = Y2 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
        Y2 = Y2 - HyE(i + 1) * Sc1Y / 10
        '
        X.Line (X1, Y1)-(X2, Y2), QBColor(12)
      Next i
    Else
      For i = 1 To NE - 1
        X1 = (MLeft + 3 + k * (Box1W / 10))
        X1 = X1 - (AM - Sqr(RM ^ 2 - (HyE(i) - BM) ^ 2) - MV(Ed0f2, 1, NE)) * Sc1X / 10
        Y1 = (MTop - 0.5 + 16 + Box1H / 2)
        Y1 = Y1 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
        Y1 = Y1 - HyE(i) * Sc1Y / 10
        '
        X2 = (MLeft + 3 + k * (Box1W / 10))
        X2 = X2 - (AM - Sqr(RM ^ 2 - (HyE(i + 1) - BM) ^ 2) - MV(Ed0f2, 1, NE)) * Sc1X / 10
        Y2 = (MTop - 0.5 + 16 + Box1H / 2)
        Y2 = Y2 + (HyE(NE) - HyE(1)) * Sc1Y / 10 / 2
        Y2 = Y2 - HyE(i + 1) * Sc1Y / 10
        '
        X.Line (X1, Y1)-(X2, Y2), QBColor(12)
      Next i
    End If
    X.DrawWidth = 1
    '
  End If
  '
Return

errVisELICA_VITI:
  WRITE_DIALOG "SUB VisELICA_VITI ERROR " & str(Err)
  Exit Sub

End Sub

Sub VisPROFILO_VITI(X As Control, ByVal SEGNO_fha As Integer)
'
Dim Sc1X              As Single
Dim Sc1Y              As Single
Dim i                 As Long
Dim k                 As Long
Dim stmp              As String
Dim SiDisegna         As Integer
Dim MLeft             As Single
Dim MTop              As Single
Dim DBB               As Database
Dim RSS               As Recordset
Dim pXX(2, 9)         As Double
Dim pYY(2, 9)         As Double
Dim pBB(2, 9)         As Double
Dim k1                As Long
Dim k2                As Long
Dim k3                As Long
Dim k4                As Long
Dim rsp               As Long
Dim CODICE_PEZZO      As String
Dim lstTOLLERANZA     As String
Dim ftmp              As Double
Dim SiSTIMA_BOMBATURA As Integer
Dim SiGriglia1        As Integer
Dim SiGriglia2        As Integer
Dim AM                As Double
Dim BM                As Double
Dim RM                As Double
Dim ANGOLO            As Double
Dim Bombatura(5)      As Double
Dim Abgn              As Double
Dim Aend              As Double
Dim RRB_cur           As Double
Dim RRB_max           As Double
Dim RRB_min           As Double
Dim M                 As Double
Dim Q                 As Double
Dim TIPO_MACCHINA     As String
'
On Error GoTo errVisPROFILO_VITI
  '
  TIPO_MACCHINA = GetInfo("Configurazione", "TIPO_MACCHINA", Path_LAVORAZIONE_INI)
  TIPO_MACCHINA = UCase$(Trim$(TIPO_MACCHINA))
  '
  SiGriglia1 = val(GetInfo("CHK0", "Griglia1", Path_LAVORAZIONE_INI))
  SiGriglia2 = val(GetInfo("CHK0", "Griglia2", Path_LAVORAZIONE_INI))
  '
  SiSTIMA_BOMBATURA = val(GetInfo("CHK0", "BOMBATURA_PROFILO", Path_LAVORAZIONE_INI))
  stmp = GetInfo("CHK0", "TOLLERANZA_PROFILO", Path_LAVORAZIONE_INI): stmp = UCase$(Trim$(stmp))
  If (stmp <> "Y") Then SiSTIMA_BOMBATURA = 0
  '
  MLeft = val(GetInfo("CHK0", "MLeft", Path_LAVORAZIONE_INI))
  MTop = val(GetInfo("CHK0", "MTop", Path_LAVORAZIONE_INI))
  stmp = GetInfo("CHK0", "ScalaPROFILOx", Path_LAVORAZIONE_INI): Sc1X = val(stmp)
  stmp = GetInfo("CHK0", "ScalaPROFILOy", Path_LAVORAZIONE_INI): Sc1Y = val(stmp)
  '
  X.DrawWidth = 2
  'SEGNO +
  X1 = MLeft + 2: Y1 = (MTop + 1) + 3
  X.Circle (X1, Y1), 0.25, QBColor(0)
  X.Line (X1, Y1 + 0.15)-(X1, Y1 - 0.15)
  X.Line (X1 - 0.15, Y1)-(X1 + 0.15, Y1)
  'SEGNO -
  X1 = MLeft + 10: Y1 = (MTop + 1) + 7.5
  X.Circle (X1, Y1), 0.25, QBColor(0)
  X.Line (X1 - 0.15, Y1)-(X1 + 0.15, Y1)
  'SEGNO +
  X1 = MLeft + 18: Y1 = (MTop + 1) + 3
  X.Circle (X1, Y1), 0.25, QBColor(0)
  X.Line (X1, Y1 + 0.15)-(X1, Y1 - 0.15)
  X.Line (X1 - 0.15, Y1)-(X1 + 0.15, Y1)
  X.DrawWidth = 1
  If (QRviteMis > 0) Then
    stmp = Prendi(920, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
    stmp = stmp & ": " & frmt(QRviteMis, 3) & "(" & frmt(DiamRulli, 4) & ")"
    X.CurrentX = MLeft + 10 - X.TextWidth(stmp) / 2
    X.CurrentY = Y1 + iRG * 1
    X.Print stmp
  End If
  If (SiGriglia1 = 1) Then
    X1 = MLeft + 3: Y1 = MTop + 4
    Call Disegna_Griglia1(X, CDbl(X1), CDbl(Y1))
  End If
  If (SiGriglia2 = 1) Then
    X1 = MLeft + 3: Y1 = MTop + 4
    Call Disegna_Griglia2(X, CDbl(X1), CDbl(Y1))
  End If
  TESTO = comVisPROELI(1, 1)
  X1 = MLeft + 3: Y1 = MTop + 4
  X1 = X1 + Box1W / 2: Y1 = Y1
  X.CurrentX = X1 - X.TextWidth(TESTO) / 2
  X.CurrentY = Y1 - iRG
  X.FontBold = True
  X.Print TESTO
  X.FontBold = False
  '
  If (SiOpt(4, 0) = 1) Then
    X.DrawStyle = 1
    R1 = 1.2 * Sqr(iRG * iRG + iCl * iCl)
    X1 = MLeft + 3: Y1 = MTop + 4
    X1 = X1 + Box1W / 2: Y1 = Y1
    X2 = X1: Y2 = Y1 + Box1H / 2
    X.Line (X1, Y1)-(X2, Y2 - R1), QBColor(0)
    X1 = MLeft + 3: Y1 = MTop + 4
    X1 = X1 + Box1W / 2: Y1 = Y1 + Box1H
    X2 = X1: Y2 = Y1 - Box1H / 2
    X.Line (X1, Y1)-(X2, Y2 + R1), QBColor(0)
    X1 = MLeft + 3: Y1 = MTop + 4
    X1 = X1: Y1 = Y1 + Box1H / 2
    X2 = X1 + Box1W / 2: Y2 = Y1
    X.Line (X1, Y1)-(X2 - R1, Y2), QBColor(0)
    X1 = MLeft + 3: Y1 = MTop + 4
    X1 = X1 + Box1W: Y1 = Y1 + Box1H / 2
    X2 = X1 - Box1W / 2: Y2 = Y1
    X.Line (X1, Y1)-(X2 + R1, Y2), QBColor(0)
    X.DrawStyle = 0
  End If
  '
  'Grafico del controllo PROFILO
  If (Np <> 0) Then
    X1 = MLeft + 3 + Box1W / 4: Y1 = MTop + 4
    TESTO = "F2"
    X.CurrentX = X1 - X.TextWidth(TESTO) / 2
    X.CurrentY = Y1 - iRG
    X.FontBold = True
    X.Print TESTO
    X.FontBold = False
    If (SiOpt(4, 0) = 1) Then
        k = 1
          X1 = (MLeft + 3 + k * (Box1W / 10))
          Y1 = (MTop + 4 + Box1H / 2)
          Y1 = Y1 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
          Y1 = Y1 - (HyP(1) - HyP(1)) * Sc1Y / 10
          X2 = (MLeft + 3 + k * (Box1W / 10))
          Y2 = (MTop + 4 + Box1H / 2)
          Y2 = Y2 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
          Y2 = Y2 - (HyP(Np) - HyP(1)) * Sc1Y / 10
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
          X1 = (MLeft + 3 + Box1W / 10) - 0.5
          X2 = X1 + 1
          Y1 = (MTop + 4 + Box1H / 2) + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
          Y2 = Y1
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
          X1 = (MLeft + 3 + Box1W / 10) - 0.5
          X2 = X1 + 1
          Y1 = (MTop + 4 + Box1H / 2) - (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
          Y2 = Y1
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
      For k = 2 To 4
        If (iPd(k) <> 0) Then
          X1 = (MLeft + 3 + k * (Box1W / 10))
          Y1 = (MTop + 4 + Box1H / 2)
          Y1 = Y1 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
          Y1 = Y1 - (HyP(1) - HyP(1)) * Sc1Y / 10
          X2 = (MLeft + 3 + k * (Box1W / 10))
          Y2 = (MTop + 4 + Box1H / 2)
          Y2 = Y2 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
          Y2 = Y2 - (HyP(Np) - HyP(1)) * Sc1Y / 10
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
          X1 = (MLeft + 3 + k * (Box1W / 10)) - 0.5
          X2 = X1 + 1
          Y1 = (MTop + 4 + Box1H / 2) + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
          Y2 = Y1
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
          X1 = (MLeft + 3 + k * (Box1W / 10)) - 0.5
          X2 = X1 + 1
          Y1 = (MTop + 4 + Box1H / 2) - (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
          Y2 = Y1
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
        End If
      Next
    End If
    For k = 1 To 4
      SiDisegna = False
      Select Case k
        Case 1
          For i = 1 To Np: Pd0f2(i) = Pd1f2(i): Next i
            GoSub STIMA_BOMBATURA_PROFILO_FIANCO2
            SiDisegna = True
        Case 2
          For i = 1 To Np: Pd0f2(i) = Pd2f2(i): Next i
          If (iPd(2) <> 0) Then
            GoSub STIMA_BOMBATURA_PROFILO_FIANCO2
            SiDisegna = True
          End If
        Case 3
          For i = 1 To Np: Pd0f2(i) = Pd3f2(i): Next i
          If (iPd(3) <> 0) Then
            GoSub STIMA_BOMBATURA_PROFILO_FIANCO2
            SiDisegna = True
          End If
        Case 4
          For i = 1 To Np: Pd0f2(i) = Pd4f2(i): Next i
          If (iPd(4) <> 0) Then
            GoSub STIMA_BOMBATURA_PROFILO_FIANCO2
            SiDisegna = True
          End If
      End Select
      '
      If SiDisegna Then
        For i = 1 To Np
          X1 = (MLeft + 3 + k * (Box1W / 10))
          X1 = X1 - (Pd0f2(i) - MV(Pd0f2, 1, Np)) * Sc1X / 10
          Y1 = (MTop + 4 + Box1H / 2)
          Y1 = Y1 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
          Y1 = Y1 - (HyP(i) - HyP(1)) * Sc1Y / 10
          'If SiOpt(0, 0) = 1 Then
          '  R1 = 0.025
          '  X.Circle (X1, Y1), R1, QBColor(0)
          'End If
          'If SiOpt(1, 0) = 1 Then
          '  X.Line ((MLeft + 3 + k * (Box1W / 10)), Y1)-(X1, Y1), QBColor(0)
          'End If
          If (i < Np) And (SiOpt(2, 0) = 1) Then
            X2 = (MLeft + 3 + k * (Box1W / 10))
            X2 = X2 - (Pd0f2(i + 1) - MV(Pd0f2, 1, Np)) * Sc1X / 10
            Y2 = (MTop + 4 + Box1H / 2)
            Y2 = Y2 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
            Y2 = Y2 - (HyP(i + 1) - HyP(1)) * Sc1Y / 10
            X.Line (X1, Y1)-(X2, Y2), QBColor(0)
          End If
        Next i
        X1 = (MLeft + 3 + k * (Box1W / 10))
        Y1 = (MTop + 4 + Box1H)
        TESTO = Format$(iPd(k), "#")
        X.CurrentX = X1
        X.CurrentY = Y1
        X.Print TESTO
        j = 0
        X1 = (MLeft + 3 + k * (Box1W / 10))
        Y1 = MTop + 13.5 + j * iRG
        TESTO = frmt(faF2d(k) * 1000, 1)
        X.CurrentX = X1 - X.TextWidth(TESTO) / 2 + iCl
        X.CurrentY = Y1
        X.Print TESTO
        j = 1
        X1 = (MLeft + 3 + k * (Box1W / 10))
        Y1 = MTop + 13.5 + j * iRG
        TESTO = frmt(ffaF2d(k) * 1000, 1)
        X.CurrentX = X1 - X.TextWidth(TESTO) / 2 + iCl
        X.CurrentY = Y1
        X.Print TESTO
        j = 2
        X1 = (MLeft + 3 + k * (Box1W / 10))
        Y1 = MTop + 13.5 + j * iRG
        TESTO = frmt(SEGNO_fha * fhaF2d(k) * 1000, 1)
        X.CurrentX = X1 - X.TextWidth(TESTO) / 2 + iCl
        X.CurrentY = Y1
        X.Print TESTO
        If (SiSTIMA_BOMBATURA = 1) Then
          j = 3
          X1 = (MLeft + 3 + k * (Box1W / 10))
          Y1 = MTop + 13.5 + j * iRG
          TESTO = frmt(Bombatura(k) * 1000, 1)
          X.CurrentX = X1 - X.TextWidth(TESTO) / 2 + iCl
          X.CurrentY = Y1
          X.Print TESTO
        End If
      End If
    Next k
    '
    For j = 0 To 2
      X1 = MLeft
      Y1 = MTop + 13.5 + j * iRG
      TESTO = comVisPROELI(4 + j, 1)
      X.CurrentX = X1 + (1.5 - X.TextWidth(TESTO)) / 2
      X.CurrentY = Y1
      X.Print TESTO
      X.Line (X1, Y1 + iRG)-(X1 + Box0W, Y1 + iRG), QBColor(0)
    Next j
    '
    If (SiSTIMA_BOMBATURA = 1) Then
      j = 3
        X1 = MLeft
        Y1 = MTop + 13.5 + j * iRG
        TESTO = "Ca" 'comVisPROELI(7, 1)
        X.CurrentX = X1 + (1.5 - X.TextWidth(TESTO)) / 2
        X.CurrentY = Y1
        X.Print TESTO
        X.Line (X1, Y1 + iRG)-(X1 + Box0W, Y1 + iRG), QBColor(0)
    End If
    '
    X1 = (MLeft + 3 + Box1W / 10) - 1
    X2 = X1 + 2
    Y1 = (MTop + 4 + Box1H / 2) + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
    Y2 = Y1
    X.Line (X1, Y1)-(X2, Y2), QBColor(0)
    '
    TESTO = frmt(SAP, 3)
    X.CurrentX = X1 - X.TextWidth(TESTO) - iCl
    X.CurrentY = Y1 - iRG / 2
    X.Print TESTO
    '
    X1 = (MLeft + 3 + Box1W / 10) - 1
    X2 = X1 + 2
    Y1 = (MTop + 4 + Box1H / 2) - (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
    Y2 = Y1
    X.Line (X1, Y1)-(X2, Y2), QBColor(0)
    '
    TESTO = frmt(EAP, 3)
    X.CurrentX = X1 - X.TextWidth(TESTO) - iCl
    X.CurrentY = Y1 - iRG / 2
    X.Print TESTO
    '
    X1 = MLeft + 3 + 3 * Box1W / 4: Y1 = MTop + 4
    TESTO = "F1"
    X.CurrentX = X1 - X.TextWidth(TESTO) / 2
    X.CurrentY = Y1 - iRG
    X.FontBold = True
    X.Print TESTO
    X.FontBold = False
    '
   If (SiOpt(4, 0) = 1) Then
        k = 1
          X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
          Y1 = (MTop + 4 + Box1H / 2)
          Y1 = Y1 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
          Y1 = Y1 - (HyP(1) - HyP(1)) * Sc1Y / 10
          X2 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
          Y2 = (MTop + 4 + Box1H / 2)
          Y2 = Y2 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
          Y2 = Y2 - (HyP(Np) - HyP(1)) * Sc1Y / 10
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
          X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10))) - 0.5
          X2 = X1 + 1
          Y1 = (MTop + 4 + Box1H / 2) + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
          Y2 = Y1
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
          X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10))) - 0.5
          X2 = X1 + 1
          Y1 = (MTop + 4 + Box1H / 2) - (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
          Y2 = Y1
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
      For k = 2 To 4
        If (iPd(k) <> 0) Then
          X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
          Y1 = (MTop + 4 + Box1H / 2)
          Y1 = Y1 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
          Y1 = Y1 - (HyP(1) - HyP(1)) * Sc1Y / 10
          X2 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
          Y2 = (MTop + 4 + Box1H / 2)
          Y2 = Y2 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
          Y2 = Y2 - (HyP(Np) - HyP(1)) * Sc1Y / 10
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
          X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10))) - 0.5
          X2 = X1 + 1
          Y1 = (MTop + 4 + Box1H / 2) + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
          Y2 = Y1
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
          X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10))) - 0.5
          X2 = X1 + 1
          Y1 = (MTop + 4 + Box1H / 2) - (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
          Y2 = Y1
          X.Line (X1, Y1)-(X2, Y2), QBColor(0)
        End If
      Next
    End If
    For k = 1 To 4
     SiDisegna = False
      Select Case k
        Case 1
          For i = 1 To Np: Pd0f1(i) = Pd1f1(i): Next i
            GoSub STIMA_BOMBATURA_PROFILO_FIANCO1
            SiDisegna = True
        Case 2
          For i = 1 To Np: Pd0f1(i) = Pd2f1(i): Next i
          If (iPd(2) <> 0) Then
            GoSub STIMA_BOMBATURA_PROFILO_FIANCO1
            SiDisegna = True
          End If
        Case 3
          For i = 1 To Np: Pd0f1(i) = Pd3f1(i): Next i
          If (iPd(3) <> 0) Then
            GoSub STIMA_BOMBATURA_PROFILO_FIANCO1
            SiDisegna = True
          End If
        Case 4
          For i = 1 To Np: Pd0f1(i) = Pd4f1(i): Next i
          If (iPd(4) <> 0) Then
            GoSub STIMA_BOMBATURA_PROFILO_FIANCO1
            SiDisegna = True
          End If
      End Select
      '
      If SiDisegna Then
        For i = 1 To Np
          X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
          X1 = X1 + (Pd0f1(i) - MV(Pd0f1, 1, Np)) * Sc1X / 10
          Y1 = (MTop + 4 + Box1H / 2)
          Y1 = Y1 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
          Y1 = Y1 - (HyP(i) - HyP(1)) * Sc1Y / 10
          'If SiOpt(0, 0) = 1 Then
          '  R1 = 0.025
          '  X.Circle (X1, Y1), R1, QBColor(0)
          'End If
          'If SiOpt(1, 0) = 1 Then
          '  X.Line ((MLeft + 3 + (Box1W - k * (Box1W / 10))), Y1)-(X1, Y1), QBColor(0)
          'End If
          If (i < Np) And (SiOpt(2, 0) = 1) Then
            X2 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
            X2 = X2 + (Pd0f1(i + 1) - MV(Pd0f1, 1, Np)) * Sc1X / 10
            Y2 = (MTop + 4 + Box1H / 2)
            Y2 = Y2 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
            Y2 = Y2 - (HyP(i + 1) - HyP(1)) * Sc1Y / 10
            X.Line (X1, Y1)-(X2, Y2), QBColor(0)
          End If
        Next i
        X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
        Y1 = (MTop + 4 + Box1H)
        TESTO = Format$(iPd(k), "#")
        X.CurrentX = X1
        X.CurrentY = Y1
        X.Print TESTO
        j = 0
        X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
        Y1 = MTop + 13.5 + j * iRG
        TESTO = frmt(FaF1d(k) * 1000, 1)
        X.CurrentX = X1 - X.TextWidth(TESTO) / 2 + iCl
        X.CurrentY = Y1
        X.Print TESTO
        j = 1
        X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
        Y1 = MTop + 13.5 + j * iRG
        TESTO = frmt(ffaF1d(k) * 1000, 1)
        X.CurrentX = X1 - X.TextWidth(TESTO) / 2 + iCl
        X.CurrentY = Y1
        X.Print TESTO
        j = 2
        X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
        Y1 = MTop + 13.5 + j * iRG
        TESTO = frmt(SEGNO_fha * fhaF1d(k) * 1000, 1)
        X.CurrentX = X1 - X.TextWidth(TESTO) / 2 + iCl
        X.CurrentY = Y1
        X.Print TESTO
        If (SiSTIMA_BOMBATURA = 1) Then
          j = 3
          X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
          Y1 = MTop + 13.5 + j * iRG
          TESTO = frmt(Bombatura(k) * 1000, 1)
          X.CurrentX = X1 - X.TextWidth(TESTO) / 2 + iCl
          X.CurrentY = Y1
          X.Print TESTO
        End If
      End If
    Next k
    X1 = (MLeft + 3 + (Box1W - Box1W / 10)) - 1
    X2 = X1 + 2
    Y1 = (MTop + 4 + Box1H / 2) + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
    Y2 = Y1
    X.Line (X1, Y1)-(X2, Y2), QBColor(0)
    TESTO = frmt(SAP, 3)
    X.CurrentX = X2 + iCl
    X.CurrentY = Y2 - iRG / 2
    X.Print TESTO
    X1 = (MLeft + 3 + (Box1W - Box1W / 10)) - 1
    X2 = X1 + 2
    Y1 = (MTop + 4 + Box1H / 2) - (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
    Y2 = Y1
    X.Line (X1, Y1)-(X2, Y2), QBColor(0)
    TESTO = frmt(EAP, 3)
    X.CurrentX = X2 + iCl
    X.CurrentY = Y2 - iRG / 2
    X.Print TESTO
  End If
  '
  'Scale X e Y del controllo PROFILO
  X.DrawStyle = 0
  X1 = MLeft: Y1 = MTop + 4
  X2 = MLeft: Y2 = MTop + 13.5
  If SEGNO_fha = 1 Then
    Testa = comVisPROELI(2, 1)
    Piede = comVisPROELI(3, 1)
  End If
  If SEGNO_fha = -1 Then
    Testa = comVisPROELI(3, 1)
    Piede = comVisPROELI(2, 1)
  End If
  Call VisScXY(X, Sc1X, Sc1Y, 1, 1)
  X.DrawStyle = 0
  '
  stmp = GetInfo("CHK0", "TOLLERANZA_PROFILO", Path_LAVORAZIONE_INI): stmp = UCase$(Trim$(stmp))
  If (stmp <> "Y") Then Exit Sub
  '
  'LETTURA DEL NOME DEL PEZZO ASSOCIATO AL CONTROLLO
  Open PathMIS & "\" & "DEFAULT" For Input As 1
    For i = 1 To 6
      Line Input #1, CODICE_PEZZO
    Next i
  Close #1
  '
  'LETTURA DELLA TOLLERANZA DAL DATABASE
  Set DBB = OpenDatabase(PathDB, True, False)
  If (TIPO_MACCHINA = "GR250") Then
     Set RSS = DBB.OpenRecordset("ARCHIVIO_VITIV_ESTERNE")
  Else
     Set RSS = DBB.OpenRecordset("ARCHIVIO_VITI_ESTERNE")
  End If
    RSS.Index = "NOME_PEZZO"
    RSS.Seek "=", CODICE_PEZZO
    If Not RSS.NoMatch Then
      If (Len(RSS.Fields("TOLLERANZA_PROFILO").Value) > 0) Then
        lstTOLLERANZA = UCase$(Trim$(RSS.Fields("TOLLERANZA_PROFILO").Value))
      Else
        lstTOLLERANZA = ""
      End If
    End If
  RSS.Close
  DBB.Close
  '
  'RICAVO I SINGOLI VALORI DALLA LISTA
  If (Len(lstTOLLERANZA) > 0) Then
    Dim NumeroPuntiUsatiF(2) As Integer
      k1 = InStr(lstTOLLERANZA, ";;")
      If (k1 <= 0) Then
        k1 = InStr(lstTOLLERANZA, ";")
        k2 = InStr(k1 + 1, lstTOLLERANZA, ";")
        k3 = InStr(k2 + 1, lstTOLLERANZA, ";")
        k4 = 1
        For j = 1 To 8
          pXX(1, j) = val(Mid$(lstTOLLERANZA, k4, k1 - k4))
          pYY(1, j) = val(Mid$(lstTOLLERANZA, k1 + 1, k2 - k1 - 1))
          pBB(1, j) = val(Mid$(lstTOLLERANZA, k2 + 1, k3 - k2 - 1))
          k4 = k3 + 1
          k1 = InStr(k4, lstTOLLERANZA, ";")
          k2 = InStr(k1 + 1, lstTOLLERANZA, ";")
          k3 = InStr(k2 + 1, lstTOLLERANZA, ";")
        Next j
        For j = 1 To 8
          pXX(2, j) = val(Mid$(lstTOLLERANZA, k4, k1 - k4))
          pYY(2, j) = val(Mid$(lstTOLLERANZA, k1 + 1, k2 - k1 - 1))
          pBB(2, j) = val(Mid$(lstTOLLERANZA, k2 + 1, k3 - k2 - 1))
          k4 = k3 + 1
          k1 = InStr(k4, lstTOLLERANZA, ";")
          k2 = InStr(k1 + 1, lstTOLLERANZA, ";")
          k3 = InStr(k2 + 1, lstTOLLERANZA, ";")
        Next j
      End If
      i = 1
      NumeroPuntiUsatiF(1) = 8
        For j = 1 To 8
          If (pYY(i, j) = 0) And (pXX(i, j) = 0) Then
            For k1 = j To 8
              pYY(i, k1) = pYY(i, 1):  pXX(i, k1) = pXX(i, 1)
              NumeroPuntiUsatiF(1) = NumeroPuntiUsatiF(1) - 1
            Next k1
            Exit For
          End If
        Next j
      i = 2
      NumeroPuntiUsatiF(2) = 8
        For j = 1 To 8
          If (pYY(i, j) = 0) And (pXX(i, j) = 0) Then
            For k1 = j To 8
              pYY(i, k1) = pYY(i, 1):  pXX(i, k1) = pXX(i, 1)
              NumeroPuntiUsatiF(2) = NumeroPuntiUsatiF(2) - 1
            Next k1
            Exit For
          End If
        Next j
      For j = 1 To 2
        pXX(j, 9) = pXX(j, 1)
        pYY(j, 9) = pYY(j, 1)
      Next j
      'FIANCO 2 TOLLERANZA --> FIANCO 1 PEZZO
      If NumeroPuntiUsatiF(2) > 1 Then
        For k = 1 To 4
          SiDisegna = False
          Select Case k
            Case 1:                       SiDisegna = True
            Case 2: If (iPd(2) <> 0) Then SiDisegna = True
            Case 3: If (iPd(3) <> 0) Then SiDisegna = True
            Case 4: If (iPd(4) <> 0) Then SiDisegna = True
          End Select
          If SiDisegna Then
            For i = 1 To 8
            If (i <> 8) And (pBB(2, i) = 0) Then
              X1 = (MLeft + 3 + k * Box1W / 10)
              X1 = X1 + pXX(2, i) * Sc1X / 10
              Y1 = (MTop + 4 + Box1H / 2)
              Y1 = Y1 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
              Y1 = Y1 - (pYY(2, i) - SAP) * Sc1Y / 10
              X2 = (MLeft + 3 + k * Box1W / 10)
              X2 = X2 + pXX(2, i + 1) * Sc1X / 10
              Y2 = (MTop + 4 + Box1H / 2)
              Y2 = Y2 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
              Y2 = Y2 - (pYY(2, i + 1) - SAP) * Sc1Y / 10
              X.Line (X1, Y1)-(X2, Y2), QBColor(9)
            Else
              Call visRACCORDA_DUE_PUNTI(X, pXX(2, i), (HyP(Np) - HyP(1)) / 2 - (pYY(2, i) - SAP), pXX(2, i + 1), (HyP(Np) - HyP(1)) / 2 - (pYY(2, i + 1) - SAP), pBB(2, i), "SX", (MLeft + 3 + k * Box1W / 10), (MTop + 4 + Box1H / 2), Sc1X / 10, Sc1Y / 10)
            End If
            Next i
          End If
        Next k
        i = 1
        X.FillStyle = 0
        X.FillColor = QBColor(12)
        X1 = (MLeft + 3 + Box1W / 10)
        X1 = X1 + pXX(2, i) * Sc1X / 10
        Y1 = (MTop + 4 + Box1H / 2)
        Y1 = Y1 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
        Y1 = Y1 - (pYY(2, i) - SAP) * Sc1Y / 10
        X.Circle (X1, Y1), 0.05
        X.FillStyle = 1
        X.FillColor = QBColor(0)
        X.Line (X1 - 0.05, Y1)-(X1 + 0.05, Y1), QBColor(0)
        X.Line (X1, Y1 - 0.05)-(X1, Y1 + 0.05), QBColor(0)
      End If
      'FIANCO 1 TOLLERANZA --> FIANCO 2 PEZZO
      If NumeroPuntiUsatiF(1) > 1 Then
        For k = 1 To 4
          SiDisegna = False
          Select Case k
            Case 1:                       SiDisegna = True
            Case 2: If (iPd(2) <> 0) Then SiDisegna = True
            Case 3: If (iPd(3) <> 0) Then SiDisegna = True
            Case 4: If (iPd(4) <> 0) Then SiDisegna = True
          End Select
          If SiDisegna Then
            For i = 1 To 8
            If (i <> 8) And (pBB(1, i) = 0) Then
              X1 = (MLeft + 3 + (Box1W - k * Box1W / 10))
              X1 = X1 + pXX(1, i) * Sc1X / 10
              Y1 = (MTop + 4 + Box1H / 2)
              Y1 = Y1 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
              Y1 = Y1 - (pYY(1, i) - SAP) * Sc1Y / 10
              '
              X2 = (MLeft + 3 + (Box1W - k * Box1W / 10))
              X2 = X2 + pXX(1, i + 1) * Sc1X / 10
              Y2 = (MTop + 4 + Box1H / 2)
              Y2 = Y2 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
              Y2 = Y2 - (pYY(1, i + 1) - SAP) * Sc1Y / 10
              X.Line (X1, Y1)-(X2, Y2), QBColor(9)
            Else
              Call visRACCORDA_DUE_PUNTI(X, pXX(1, i), (HyP(Np) - HyP(1)) / 2 - (pYY(1, i) - SAP), pXX(1, i + 1), (HyP(Np) - HyP(1)) / 2 - (pYY(1, i + 1) - SAP), pBB(1, i), "SX", (MLeft + 3 + (Box1W - k * Box1W / 10)), (MTop + 4 + Box1H / 2), Sc1X / 10, Sc1Y / 10)
            End If
            Next i
          End If
        Next k
        i = 1
        X.FillStyle = 0
        X.FillColor = QBColor(12)
        X1 = (MLeft + 3 + (Box1W - Box1W / 10))
        X1 = X1 + pXX(1, i) * Sc1X / 10
        Y1 = (MTop + 4 + Box1H / 2)
        Y1 = Y1 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
        Y1 = Y1 - (pYY(1, i) - SAP) * Sc1Y / 10
        X.Circle (X1, Y1), 0.05
        X.FillStyle = 1
        X.FillColor = QBColor(0)
        X.Line (X1 - 0.05, Y1)-(X1 + 0.05, Y1), QBColor(0)
        X.Line (X1, Y1 - 0.05)-(X1, Y1 + 0.05), QBColor(0)
      End If
  End If
  '
Exit Sub

STIMA_BOMBATURA_PROFILO_FIANCO1:
  'VISUALIZZAZIONE RETTA DI REGRESSIONE
  If (SiOpt(4, 0) = 1) Then
    'Call CalcoloMretta(HyP, Pd0f1, M, Q, CHK_Ini_PROFILO, CHK_Fin_PROFILO)
    M = fhaF1d(k) / (HyP(Np) - HyP(1))
    X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
    X1 = X1 - M * (HyP(Np) - HyP(1)) * Sc1X / 10 / 2
    Y1 = (MTop + 4 + Box1H / 2)
    Y1 = Y1 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
    Y1 = Y1 - (HyP(1) - HyP(1)) * Sc1Y / 10
    X2 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
    X2 = X2 + M * (HyP(Np) - HyP(1)) * Sc1X / 10 / 2
    Y2 = (MTop + 4 + Box1H / 2)
    Y2 = Y2 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
    Y2 = Y2 - (HyP(Np) - HyP(1)) * Sc1Y / 10
    X.DrawWidth = 2
    X.Line (X1, Y1)-(X2, Y2), QBColor(12)
    X.DrawWidth = 1
  End If
  '
  'STIMA BOMBATURA CON VISUALIZZAZIONE DELL' ARCO DI CERCHIO SVG 07/11/09
  If (SiSTIMA_BOMBATURA = 1) Then
    Call CalcoloCerchio(HyP, Pd0f1, AM, BM, RM)
    '
    X1 = AM + Sqr(RM ^ 2 - (Pd0f1(1) - BM) ^ 2):  Y1 = HyP(1)
    X2 = AM + Sqr(RM ^ 2 - (Pd0f1(Np) - BM) ^ 2): Y2 = HyP(Np)
    ANGOLO = Sqr((X2 - X1) ^ 2 + (Y2 - Y1) ^ 2)
    ANGOLO = FnASN(ANGOLO / 2 / RM)
    '
    Bombatura(k) = Abs(RM * (1 - Cos(ANGOLO)))
    '
    ffaF1d(k) = 0: RRB_max = -1E+99: RRB_min = 1E+99
    Select Case k
    Case 1
      For i = 1 To Np
        RRB_cur = Sqr((Pd1f1(i) - AM) ^ 2 + (HyP(i) - BM) ^ 2)
        If (RRB_cur > RRB_max) Then RRB_max = RRB_cur
        If (RRB_cur < RRB_min) Then RRB_min = RRB_cur
      Next i
    Case 2
      For i = 1 To Np
        RRB_cur = Sqr((Pd2f1(i) - AM) ^ 2 + (HyP(i) - BM) ^ 2)
        If (RRB_cur > RRB_max) Then RRB_max = RRB_cur
        If (RRB_cur < RRB_min) Then RRB_min = RRB_cur
      Next i
    Case 3
      For i = 1 To Np
        RRB_cur = Sqr((Pd3f1(i) - AM) ^ 2 + (HyP(i) - BM) ^ 2)
        If (RRB_cur > RRB_max) Then RRB_max = RRB_cur
        If (RRB_cur < RRB_min) Then RRB_min = RRB_cur
      Next i
    Case 4
      For i = 1 To Np
        RRB_cur = Sqr((Pd4f1(i) - AM) ^ 2 + (HyP(i) - BM) ^ 2)
        If (RRB_cur > RRB_max) Then RRB_max = RRB_cur
        If (RRB_cur < RRB_min) Then RRB_min = RRB_cur
      Next i
    End Select
    ffaF1d(k) = RRB_max - RRB_min
    '
    X.DrawWidth = 2
    If (AM > 0) Then
      For i = 1 To Np - 1
        X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
        X1 = X1 + (AM - Sqr(RM ^ 2 - (HyP(i) - BM) ^ 2) - MV(Pd0f1, 1, Np)) * Sc1X / 10
        Y1 = (MTop + 4 + Box1H / 2)
        Y1 = Y1 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
        Y1 = Y1 - HyP(i) * Sc1Y / 10
        '
        X2 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
        X2 = X2 + (AM - Sqr(RM ^ 2 - (HyP(i + 1) - BM) ^ 2) - MV(Pd0f1, 1, Np)) * Sc1X / 10
        Y2 = (MTop + 4 + Box1H / 2)
        Y2 = Y2 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
        Y2 = Y2 - HyP(i + 1) * Sc1Y / 10
        '
        X.Line (X1, Y1)-(X2, Y2), QBColor(12)
      Next i
    Else
      For i = 1 To Np - 1
        X1 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
        X1 = X1 + (AM + Sqr(RM ^ 2 - (HyP(i) - BM) ^ 2) - MV(Pd0f1, 1, Np)) * Sc1X / 10
        Y1 = (MTop + 4 + Box1H / 2)
        Y1 = Y1 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
        Y1 = Y1 - HyP(i) * Sc1Y / 10
        '
        X2 = (MLeft + 3 + (Box1W - k * (Box1W / 10)))
        X2 = X2 + (AM + Sqr(RM ^ 2 - (HyP(i + 1) - BM) ^ 2) - MV(Pd0f1, 1, Np)) * Sc1X / 10
        Y2 = (MTop + 4 + Box1H / 2)
        Y2 = Y2 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
        Y2 = Y2 - HyP(i + 1) * Sc1Y / 10
        '
        X.Line (X1, Y1)-(X2, Y2), QBColor(12)
      Next i
    End If
    X.DrawWidth = 1
    '
  End If
  '
Return

STIMA_BOMBATURA_PROFILO_FIANCO2:
  'VISUALIZZAZIONE RETTA DI REGRESSIONE
  If (SiOpt(4, 0) = 1) Then
    'Call CalcoloMretta(HyP, Pd0f2, M, Q, CHK_Ini_PROFILO, CHK_Fin_PROFILO)
    M = fhaF2d(k) / (HyP(Np) - HyP(1))
    X1 = (MLeft + 3 + k * (Box1W / 10))
    X1 = X1 + M * (HyP(Np) - HyP(1)) * Sc1X / 10 / 2
    Y1 = (MTop + 4 + Box1H / 2)
    Y1 = Y1 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
    Y1 = Y1 - (HyP(1) - HyP(1)) * Sc1Y / 10
    X2 = (MLeft + 3 + k * (Box1W / 10))
    X2 = X2 - M * (HyP(Np) - HyP(1)) * Sc1X / 10 / 2
    Y2 = (MTop + 4 + Box1H / 2)
    Y2 = Y2 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
    Y2 = Y2 - (HyP(Np) - HyP(1)) * Sc1Y / 10
    X.DrawWidth = 2
    X.Line (X1, Y1)-(X2, Y2), QBColor(12)
    X.DrawWidth = 1
  End If
  '
  'STIMA BOMBATURA CON VISUALIZZAZIONE DELL' ARCO DI CERCHIO SVG 07/11/09
  If (SiSTIMA_BOMBATURA = 1) Then
    Call CalcoloCerchio(HyP, Pd0f2, AM, BM, RM)
    '
    X1 = AM + Sqr(RM ^ 2 - (Pd0f2(1) - BM) ^ 2):  Y1 = HyP(1)
    X2 = AM + Sqr(RM ^ 2 - (Pd0f2(Np) - BM) ^ 2): Y2 = HyP(Np)
    ANGOLO = Sqr((X2 - X1) ^ 2 + (Y2 - Y1) ^ 2)
    ANGOLO = FnASN(ANGOLO / 2 / RM)
    '
    If AM > 0 Then RM = -RM
    Bombatura(k) = Abs(RM * (1 - Cos(ANGOLO)))
    '
    ffaF2d(k) = 0: RRB_max = -1E+99: RRB_min = 1E+99
    Select Case k
    Case 1
      For i = 1 To Np
        RRB_cur = Sqr((Pd1f2(i) - AM) ^ 2 + (HyP(i) - BM) ^ 2)
        If (RRB_cur > RRB_max) Then RRB_max = RRB_cur
        If (RRB_cur < RRB_min) Then RRB_min = RRB_cur
      Next i
    Case 2
      For i = 1 To Np
        RRB_cur = Sqr((Pd2f2(i) - AM) ^ 2 + (HyP(i) - BM) ^ 2)
        If (RRB_cur > RRB_max) Then RRB_max = RRB_cur
        If (RRB_cur < RRB_min) Then RRB_min = RRB_cur
      Next i
    Case 3
      For i = 1 To Np
        RRB_cur = Sqr((Pd3f2(i) - AM) ^ 2 + (HyP(i) - BM) ^ 2)
        If (RRB_cur > RRB_max) Then RRB_max = RRB_cur
        If (RRB_cur < RRB_min) Then RRB_min = RRB_cur
      Next i
    Case 4
      For i = 1 To Np
        RRB_cur = Sqr((Pd4f2(i) - AM) ^ 2 + (HyP(i) - BM) ^ 2)
        If (RRB_cur > RRB_max) Then RRB_max = RRB_cur
        If (RRB_cur < RRB_min) Then RRB_min = RRB_cur
      Next i
    End Select
    ffaF2d(k) = RRB_max - RRB_min
    '
    X.DrawWidth = 2
    If (AM > 0) Then
      For i = 1 To Np - 1
        X1 = (MLeft + 3 + k * (Box1W / 10))
        X1 = X1 - (AM - Sqr(RM ^ 2 - (HyP(i) - BM) ^ 2) - MV(Pd0f2, 1, Np)) * Sc1X / 10
        Y1 = (MTop + 4 + Box1H / 2)
        Y1 = Y1 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
        Y1 = Y1 - HyP(i) * Sc1Y / 10
        '
        X2 = (MLeft + 3 + k * (Box1W / 10))
        X2 = X2 - (AM - Sqr(RM ^ 2 - (HyP(i + 1) - BM) ^ 2) - MV(Pd0f2, 1, Np)) * Sc1X / 10
        Y2 = (MTop + 4 + Box1H / 2)
        Y2 = Y2 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
        Y2 = Y2 - HyP(i + 1) * Sc1Y / 10
        '
        X.Line (X1, Y1)-(X2, Y2), QBColor(12)
      Next i
    Else
      For i = 1 To Np - 1
        X1 = (MLeft + 3 + k * (Box1W / 10))
        X1 = X1 - (AM + Sqr(RM ^ 2 - (HyP(i) - BM) ^ 2) - MV(Pd0f2, 1, Np)) * Sc1X / 10
        Y1 = (MTop + 4 + Box1H / 2)
        Y1 = Y1 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
        Y1 = Y1 - HyP(i) * Sc1Y / 10
        '
        X2 = (MLeft + 3 + k * (Box1W / 10))
        X2 = X2 - (AM + Sqr(RM ^ 2 - (HyP(i + 1) - BM) ^ 2) - MV(Pd0f2, 1, Np)) * Sc1X / 10
        Y2 = (MTop + 4 + Box1H / 2)
        Y2 = Y2 + (HyP(Np) - HyP(1)) * Sc1Y / 10 / 2
        Y2 = Y2 - HyP(i + 1) * Sc1Y / 10
        '
        X.Line (X1, Y1)-(X2, Y2), QBColor(12)
      Next i
    End If
    X.DrawWidth = 1
    '
  End If
  '
Return

errVisPROFILO_VITI:
  WRITE_DIALOG "SUB VisPROFILO_VITI ERROR " & str(Err)
  Resume Next
  'Exit Sub

End Sub

Sub StampaMisuraPeE()
'
Static Com1(4)       As String
Static Com2(4)       As String
Dim TabX             As Single
Dim TabY             As Single
Dim stmp             As String
Dim TextFont         As String
Dim MLeft            As Single
Dim MTop             As Single
Dim NomeControllo    As String
Dim DataControllo    As String
Dim NomeLavorazione  As String
Dim NomePezzo        As String
Dim NomePezzoDEFAULT As String
Dim TIPO_LAVORAZIONE As String
Dim rsp              As Long
Dim k1               As Long
Dim ftmp             As Double
'
On Error Resume Next
  '
  'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  'RICAVO IL NOME DEL TIPO DI LAVORAZIONE
  NomeLavorazione = RICERCA_NOME_TIPO(TIPO_LAVORAZIONE)
  'LEGGO IL NOME DEL PEZZO
  NomePezzo = LEGGI_NomePezzo(TIPO_LAVORAZIONE)
  'LEGGO IL NOME DEL CONTROLLO
  NomeControllo = GetInfo("CHK0", "FileMisura", Path_LAVORAZIONE_INI)
  'RICAVO LA DATA DAL FILE
  If (Len(NomeControllo) > 0) And (Dir(CHK0.File1.Path & "\" & NomeControllo) <> "") Then
    DataControllo = FileDateTime(CHK0.File1.Path & "\" & NomeControllo)
  Else
    DataControllo = FileDateTime(CHK0.File1.Path & "\DEFAULT")
  End If
  Open PathMIS & "\" & "DEFAULT" For Input As 1
    Line Input #1, stmp
    Line Input #1, stmp
    Line Input #1, stmp
    Input #1, ftmp, rsp
    If (rsp = 2) Then
      Input #1, k1, ftmp, k1, k1, k1, k1, NomePezzoDEFAULT
    End If
  Close #1
  'SEGNALAZIONE UTENTE
  WRITE_DIALOG TitoloPROELI & " --> " & Printer.DeviceName
  'CARATTERI DI STAMPA
      If (LINGUA = "CH") Then
    TextFont = "MS Song"
  ElseIf (LINGUA = "RU") Then
    TextFont = "Arial Cyr"
  Else
    TextFont = "Arial"
  End If
  'IMPOSTAZIONE STAMPANTE
  Printer.ScaleMode = 7
  Printer.DrawWidth = 1
  'MARGINI
  MLeft = val(GetInfo("CHK0", "MLeft", Path_LAVORAZIONE_INI))
  MTop = val(GetInfo("CHK0", "MTop", Path_LAVORAZIONE_INI))
  'Squadratura della pagina
  X1 = MLeft: Y1 = MTop
  X2 = MLeft + Box0W: Y2 = MTop + Box0H
  'Printer.Line (X1, Y1)-(X2, Y2), QBColor(0), B
  'Linea di separazione per titolo
  X1 = MLeft: Y1 = (MTop + 1)
  X2 = MLeft + Box0W: Y2 = Y1
  Printer.Line (X1, Y1)-(X2, Y2), QBColor(0)
  'Linea di separazione: primo gruppo
  X1 = MLeft: Y1 = (MTop + 1) + 2.5
  X2 = MLeft + Box0W: Y2 = Y1
  Printer.Line (X1, Y1)-(X2, Y2), QBColor(0)
  'Linea di separazione: secondo gruppo
  X1 = MLeft: Y1 = (MTop + 1) + 2.5 + 10
  X2 = MLeft + Box0W: Y2 = Y1
  'Printer.Line (X1, Y1)-(X2, Y2), QBColor(0)
  X1 = MLeft: Y1 = (MTop + 1) + 2.5 + 10 + 2
  X2 = MLeft + Box0W: Y2 = Y1
  'Printer.Line (X1, Y1)-(X2, Y2), QBColor(0)
  'Linea di separazione: terzo gruppo
  X1 = MLeft: Y1 = (MTop + 1) + 2.5 + 10 + 2 + 10
  X2 = MLeft + Box0W: Y2 = Y1
  'Printer.Line (X1, Y1)-(X2, Y2), QBColor(0)
  'Linea di separazione verticale
  X1 = MLeft + 1.5: Y1 = (MTop + 1) + 2.5
  X2 = X1: Y2 = Y1 + Box0H - 1 - 2.5
  'Printer.Line (X1, Y1)-(X2, Y2), QBColor(0)
  'Intestazione della pagina
  X1 = MLeft: Y1 = MTop
  'IMMAGINE BITMAP
  Printer.PaintPicture CHK0.PicLogo.Picture, X1, Y1, 2, 2
  Printer.PaintPicture CHK0.PicLogo2.Picture, X1, Y1 + 2, 2, 2
  For i = 1 To 2
    Printer.FontName = TextFont
    Printer.FontSize = 14
  Next i
  'SCRIVO IL TITOLO DELLA PAGINA
  TESTO = logo3 & " " & TitoloPROELI
  Printer.CurrentX = X1 + Box0W - 0.25 - Printer.TextWidth(TESTO)
  Printer.CurrentY = Y1 + (1 - Printer.TextHeight(TESTO)) / 2
  Printer.Print TESTO
  'Leggi VBhelp di FontSize (1440/567) [cm] = 1 [inch]
  For i = 1 To 2
    Printer.FontName = TextFont
    Printer.FontSize = 10
  Next i
  iRG = Printer.TextHeight("A")
  iCl = Printer.TextWidth("A")
  '*********
  Com1(1) = ""
  Com1(2) = Prendi(211 + 1, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
  Com1(3) = Prendi(211 + 2, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
  Com1(4) = Prendi(211 + 3, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
  '*********
  If Len(NomePezzoDEFAULT) > 0 Then
    Com2(1) = NomeLavorazione & ": " & NomePezzoDEFAULT
  Else
    If NomeControllo = "DEFAULT" Then
      Com2(1) = NomeLavorazione & ": " & NomePezzo
    Else
      Com2(1) = NomeLavorazione
    End If
  End If
  Com2(2) = " : " & frmt(ModNor, 4)
  Com2(3) = " : " & frmt(AngPreNor, 3)
  Com2(4) = " : " & frmt(SpeCirNorMis, 3)
  '*********
  X1 = MLeft + 2 + iCl: Y1 = MTop + 1
  TabX = 0
  For i = 1 To 4
    Printer.CurrentX = X1
    Printer.CurrentY = Y1 + i * iRG
    Printer.Print Com1(i)
    If TabX < Printer.TextWidth(Com1(i)) Then
      TabX = Printer.TextWidth(Com1(i))
    End If
  Next i
  i = 1
    Printer.CurrentX = X1
    Printer.CurrentY = Y1 + i * iRG
    Printer.Print Com2(i)
  For i = 2 To 4
    Printer.CurrentX = X1 + TabX
    Printer.CurrentY = Y1 + i * iRG
    Printer.Print Com2(i)
  Next i
  '*********
  Com1(1) = Prendi(215 + 1, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
  Com1(2) = Prendi(215 + 2, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
  Com1(3) = Prendi(203, "MESSAGGI_CONTROLLO", "NOME_" & LINGUA)
  Com1(4) = "Date"
  '*********
  Com2(1) = " : " & Format$(NZ, "##0")
  Com2(2) = " : " & frmt(Abs(AngEli), 3)
  If (AngEli < 0) Then
    Com2(2) = Com2(2) & "    (" & SINISTRA & ")"
  Else
    Com2(2) = Com2(2) & "    (" & DESTRA & ")"
  End If
  Com2(3) = " : " & NomeControllo
  Com2(4) = " : " & DataControllo
  '*********
  X1 = MLeft + 2 + 3 + (Box0W - 2) / 2 + iCl: Y1 = MTop + 1
  TabX = 0
  i = 1
    Printer.CurrentX = X1
    Printer.CurrentY = Y1 + (i + 1) * iRG
    Printer.Print Com1(i)
    If TabX < Printer.TextWidth(Com1(i)) Then
      TabX = Printer.TextWidth(Com1(i))
    End If
  For i = 2 To 4
    Printer.CurrentX = X1 - 6
    Printer.CurrentY = Y1 + i * iRG
    Printer.Print Com1(i)
    If TabX < Printer.TextWidth(Com1(i)) Then
      TabX = Printer.TextWidth(Com1(i))
    End If
  Next i
  i = 1
    Printer.CurrentX = X1 + TabX
    Printer.CurrentY = Y1 + (i + 1) * iRG
    Printer.Print Com2(i)
  For i = 2 To 4
    Printer.CurrentX = X1 + TabX - 6
    Printer.CurrentY = Y1 + i * iRG
    Printer.Print Com2(i)
  Next i
  'STAMPA DEI GRAFICI
  For i = 1 To 2
    Printer.FontName = TextFont
    Printer.FontSize = 8
  Next i
  iRG = Printer.TextHeight("A")
  iCl = Printer.TextWidth("A")
  '*************************************************************************
  Select Case TIPO_LAVORAZIONE
    Case "PROFILO_RAB_ESTERNI", "INGR380_ESTERNI", "SCANALATIEVO", "DENTATURA", "MOLAVITE", "MOLAVITEG160", "INGRANAGGI_ESTERNIV", "INGRANAGGI_ESTERNIO"
      Call VisPROFILO(Printer, 1)
    Case "INGRANAGGI_INTERNI"
      Call VisPROFILO(Printer, -1)
    Case Else
      WRITE_DIALOG "SOFTKEY DISENABLED"
  End Select
  Call VisELICA(Printer)
  Printer.EndDoc
  'SEGNALAZIONE UTENTE
  WRITE_DIALOG TitoloPROELI & " --> " & Printer.DeviceName & " OK!!"
  '
Exit Sub

StampaHeaderPeE:
  TabX = 0
  For i = 1 To 4
    Printer.CurrentX = X1
    Printer.CurrentY = Y1 + i * iRG
    Printer.Print Com1(i)
    If TabX < Printer.TextWidth(Com1(i)) Then
      TabX = Printer.TextWidth(Com1(i))
    End If
  Next i
  i = 1
    Printer.CurrentX = X1
    Printer.CurrentY = Y1 + i * iRG
    Printer.Print Com2(i)
  For i = 2 To 4
    Printer.CurrentX = X1 + TabX
    Printer.CurrentY = Y1 + i * iRG
    Printer.Print Com2(i)
  Next i
Return

End Sub

Sub VISUALIZZA_DERIVATA_DIVISIONE(X As Control)
'
Dim Sc1X                As Single
Dim Sc1Y                As Single
Dim stmp                As String
Dim MLeft               As Single
Dim MTop                As Single
Dim XX1                 As Single
Dim XX2                 As Single
Dim YY1                 As Single
Dim YY2                 As Single
Dim wt                  As Single
Dim w1                  As Single
Dim FasciaTolleranza(2) As Single
Dim MaxFp(2, 2)         As Single
Dim MinFp(2, 2)         As Single
Dim fu                  As Single
Dim ifu                 As Integer
Dim ftmp1               As Single
Dim ftmp2               As Single
Const NumeroRette = 15
Const ANGOLO = 20
Const Box2H = 7
Const Box2W = 14
Const DELTA_Y = 4
'
On Error GoTo errVISUALIZZA_DERIVATA_DIVISIONE
  '
  'LARGHEZZA TOTALE
  wt = 1
  'LARGHEZZA LIVELLO ON
  w1 = 1 '0.5
  'FASCIA DI TOLLERANZA
  FasciaTolleranza(1) = val(GetInfo("CHK0", "TOLLERANZA_DIV(1)", Path_LAVORAZIONE_INI))
  FasciaTolleranza(2) = val(GetInfo("CHK0", "TOLLERANZA_DIV(2)", Path_LAVORAZIONE_INI))
  '
  MLeft = val(GetInfo("CHK0", "MLeft", Path_LAVORAZIONE_INI))
  MTop = val(GetInfo("CHK0", "MTop", Path_LAVORAZIONE_INI))
  MTop = MTop + DELTA_Y
  stmp = GetInfo("CHK0", "ScalaDIVISIONEx", Path_LAVORAZIONE_INI): Sc1X = val(stmp)
  stmp = GetInfo("CHK0", "ScalaDIVISIONEy", Path_LAVORAZIONE_INI): Sc1Y = val(stmp)
  '
  XX1 = (MLeft + 3) - 2
  YY1 = MTop + 1 * Box2H / 4
  stmp = frmt(Sc1Y, 4) & ":1"
  X.CurrentX = XX1 - X.TextWidth(stmp) / 2
  X.CurrentY = YY1 - 0.5 - 2 * X.TextHeight(stmp)
  X.Print stmp
  X.Line (XX1 - 0.5, YY1 - 0.5)-(XX1 + 0.5, YY1 - 0.5), QBColor(9)
  'FRECCIA SUPERIORE
  ftmp1 = (ANGOLO / 180 * PG) / NumeroRette
  For i = -NumeroRette To NumeroRette
    XX2 = XX1 + 0.3 * Sin(i * ftmp1)
    YY2 = YY1 - 0.5 - 0.25 * Cos((ANGOLO / 180 * PG))
    X.Line (XX1, YY1 - 0.5)-(XX2, YY2), QBColor(9)
  Next i
  'FRECCIA INFERIORE
  ftmp1 = (ANGOLO / 180 * PG) / NumeroRette
  For i = -NumeroRette To NumeroRette
    XX2 = XX1 + 0.3 * Sin(i * ftmp1)
    YY2 = YY1 + 0.5 + 0.25 * Cos((ANGOLO / 180 * PG))
    X.Line (XX1, YY1 + 0.5)-(XX2, YY2), QBColor(9)
  Next i
  X.Line (XX1 - 0.5, YY1 + 0.5)-(XX1 + 0.5, YY1 + 0.5), QBColor(9)
  stmp = frmt(10 / Sc1Y, 4) & "mm"
  X.CurrentX = XX1 - X.TextWidth(stmp) / 2
  X.CurrentY = YY1 - X.TextHeight(stmp) / 2
  X.Print stmp
  'SEGNO POSITIVO
  stmp = " + "
  XX1 = (MLeft + 3)
  YY1 = MTop + 0 * Box2H / 4 - X.TextHeight(stmp) / 2
  X.Line (XX1, YY1)-(XX1 + X.TextWidth(stmp), YY1 + X.TextHeight(stmp)), , B
  X.CurrentX = XX1
  X.CurrentY = YY1
  X.Print stmp
  'NOME DEL GRAFICO
  stmp = " fp "
  X.CurrentX = (MLeft + 3) - X.TextWidth(stmp)
  X.CurrentY = MTop + 2 * Box2H / 4 - X.TextHeight(stmp) / 2
  X.Print stmp
  'SEGNO NEGATIVO
  stmp = " - "
  XX1 = (MLeft + 3)
  YY1 = MTop + 2 * Box2H / 4 - X.TextHeight(stmp) / 2
  X.Line (XX1, YY1)-(XX1 + X.TextWidth(stmp), YY1 + X.TextHeight(stmp)), , B
  X.CurrentX = XX1
  X.CurrentY = YY1
  X.Print stmp
  'SEGNO POSITIVO
  stmp = " + "
  XX1 = (MLeft + 3)
  YY1 = MTop + 4 * Box2H / 4 - X.TextHeight(stmp) / 2
  X.Line (XX1, YY1)-(XX1 + X.TextWidth(stmp), YY1 + X.TextHeight(stmp)), , B
  X.CurrentX = XX1
  X.CurrentY = YY1
  X.Print stmp
  'GRAFICO DI CONTROLLO DIVISIONE
  If (DivCon = 1) Then
    'FIANCO 1
    XX1 = (MLeft + 3)
    YY1 = MTop + 1 * Box2H / 4
    'NOME DEL GRAFICO
    stmp = " F1 "
    X.CurrentX = XX1 - X.TextWidth(stmp)
    X.CurrentY = YY1 - X.TextHeight(stmp) / 2
    X.Print stmp
    'FASCIA DI TOLLERANZA
    If FasciaTolleranza(1) > 0 Then
      X.Line (XX1 + 0.5 * wt * Sc1X / 10, YY1 - (FasciaTolleranza(1) / 2) * Sc1Y / 10)-(XX1 + (NZ + 0.5) * wt * Sc1X / 10, YY1 - (FasciaTolleranza(1) / 2) * Sc1Y / 10), QBColor(12)
      X.Line (XX1 + 0.5 * wt * Sc1X / 10, YY1 + (FasciaTolleranza(1) / 2) * Sc1Y / 10)-(XX1 + (NZ + 0.5) * wt * Sc1X / 10, YY1 + (FasciaTolleranza(1) / 2) * Sc1Y / 10), QBColor(12)
      '
      'VALORE
      stmp = CInt(FasciaTolleranza(1) * 1000)
      X.CurrentX = XX1 - X.TextWidth(stmp) / 2
      X.CurrentY = YY1 - (FasciaTolleranza(1) / 2) * Sc1Y / 10 - 0.3 - X.TextHeight(stmp)
      X.Print stmp
      'FRECCIA SUPERIORE
      ftmp1 = (ANGOLO / 180 * PG) / NumeroRette
      For i = -NumeroRette To NumeroRette
        XX2 = XX1 + 0.3 * Sin(i * ftmp1)
        YY2 = YY1 - (FasciaTolleranza(1) / 2) * Sc1Y / 10 - 0.25 * Cos((ANGOLO / 180 * PG))
        X.Line (XX1, YY1 - (FasciaTolleranza(1) / 2) * Sc1Y / 10)-(XX2, YY2), QBColor(12)
      Next i
      X.Line (XX1 - 0.25, YY1 - (FasciaTolleranza(1) / 2) * Sc1Y / 10)-(XX1 + 0.25, YY1 - (FasciaTolleranza(1) / 2) * Sc1Y / 10), QBColor(12)
      'FRECCIA INFERIORE
      ftmp1 = (ANGOLO / 180 * PG) / NumeroRette
      For i = -NumeroRette To NumeroRette
        XX2 = XX1 + 0.3 * Sin(i * ftmp1)
        YY2 = YY1 + (FasciaTolleranza(1) / 2) * Sc1Y / 10 + 0.25 * Cos((ANGOLO / 180 * PG))
        X.Line (XX1, YY1 + (FasciaTolleranza(1) / 2) * Sc1Y / 10)-(XX2, YY2), QBColor(12)
      Next i
      X.Line (XX1 - 0.25, YY1 + (FasciaTolleranza(1) / 2) * Sc1Y / 10)-(XX1 + 0.25, YY1 + (FasciaTolleranza(1) / 2) * Sc1Y / 10), QBColor(12)
      '
    End If
    'RIFERIMENTO
    X.Line (XX1 + 0.5 * wt * Sc1X / 10, YY1)-(XX1 + (NZ + 0.5) * wt * Sc1X / 10, YY1), QBColor(0)
    MaxFp(1, 1) = 1: MaxFp(1, 2) = Divf1(2) - Divf1(1)
    MinFp(2, 1) = 1: MinFp(2, 2) = Divf1(2) - Divf1(1)
    ifu = 0: fu = 0
    For i = 1 To NZ - 1
      'Valore corrente
      ftmp1 = Divf1(i + 1) - Divf1(i)
      'Ricerca del massimo e minimo
      If MaxFp(1, 2) <= Abs(ftmp1) Then MaxFp(1, 1) = i: MaxFp(1, 2) = Abs(ftmp1)
      If MinFp(2, 2) >= Abs(ftmp1) Then MinFp(2, 1) = i: MinFp(2, 2) = Abs(ftmp1)
      'Ricerca fu
      If (i < NZ - 1) Then
        ftmp2 = Divf1(i + 1 + 1) - Divf1(i + 1)
        ftmp2 = Abs(ftmp1 - ftmp2)
        If fu <= ftmp2 Then ifu = i: fu = ftmp2
      End If
      'Errore divisione
      XX2 = XX1 + i * wt * Sc1X / 10
      YY2 = YY1 - ftmp1 * Sc1Y / 10
      'Livello ON : ALTEZZA RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YY2)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(9)
      'Livello OFF: LATI RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YY1)-(XX2 - (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      X.Line (XX2 + (w1 / 2) * Sc1X / 10, YY1)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      'Livello OFF: BASE RETTANGOLO
      X.Line (XX2 - (wt / 2) * Sc1X / 10, YY1)-(XX2 - (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
      X.Line (XX2 + (wt / 2) * Sc1X / 10, YY1)-(XX2 + (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
    Next i
    i = NZ
      'Valore corrente
      ftmp1 = Divf1(1) - Divf1(i)
      'Ricerca del massimo e minimo
      If MaxFp(1, 2) <= Abs(ftmp1) Then MaxFp(1, 1) = i: MaxFp(1, 2) = Abs(ftmp1)
      If MinFp(2, 2) >= Abs(ftmp1) Then MinFp(2, 1) = i: MinFp(2, 2) = Abs(ftmp1)
      'Ricerca fu
      If (i < NZ - 1) Then
        ftmp2 = Divf1(i + 1 + 1) - Divf1(i + 1)
        ftmp2 = Abs(ftmp1 - ftmp2)
        If fu <= ftmp2 Then ifu = i: fu = ftmp2
      End If
      'Errore divisione
      XX2 = XX1 + i * wt * Sc1X / 10
      YY2 = YY1 - ftmp1 * Sc1Y / 10
      'Livello ON : ALTEZZA RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YY2)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(9)
      'Livello OFF: LATI RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YY1)-(XX2 - (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      X.Line (XX2 + (w1 / 2) * Sc1X / 10, YY1)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      'Livello OFF: BASE RETTANGOLO
      X.Line (XX2 - (wt / 2) * Sc1X / 10, YY1)-(XX2 - (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
      X.Line (XX2 + (wt / 2) * Sc1X / 10, YY1)-(XX2 + (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
    'fp MASSIMO di F1
    stmp = "fp = " & CInt(MaxFp(1, 2) * 1000) & "/" & Format$(MaxFp(1, 1), "##0")
    X.CurrentX = (MLeft + 3) + 1 * Box2W / 4
    X.CurrentY = MTop + 0 * Box2H / 4 - X.TextHeight(stmp) / 2
    X.Print stmp
    i = WritePrivateProfileString("CHK0", "DERIVATA_DIVISIONE(1)", CInt(MaxFp(1, 2) * 1000), Path_LAVORAZIONE_INI)
    'fu di F1: asterisco
    ftmp1 = Divf1(ifu + 1) - Divf1(ifu)
    stmp = "*"
    If ftmp1 >= 0 Then
      X.CurrentX = XX1 + ifu * wt * Sc1X / 10 - X.TextWidth(stmp) / 2
      X.CurrentY = YY1 - ftmp1 * Sc1Y / 10 - X.TextHeight(stmp)
    Else
      X.CurrentX = XX1 + ifu * wt * Sc1X / 10 - X.TextWidth(stmp) / 2
      X.CurrentY = YY1 - ftmp1 * Sc1Y / 10
    End If
    X.Print stmp
    'fu di F1: valore
    stmp = " fu = " & CInt(fu * 1000) & "/" & Format$(ifu, "##0") & " "
    XX1 = (MLeft + 3) + 3 * Box2W / 4
    YY1 = MTop + 0 * Box2H / 4 - X.TextHeight(stmp) / 2
    X.Line (XX1, YY1)-(XX1 + X.TextWidth(stmp), YY1 + X.TextHeight(stmp)), , B
    X.CurrentX = XX1
    X.CurrentY = YY1
    X.Print stmp
    i = WritePrivateProfileString("CHK0", "fu(1)", CInt(fu * 1000), Path_LAVORAZIONE_INI)
    'FIANCO 2
    XX1 = (MLeft + 3)
    YY1 = MTop + 3 * Box2H / 4
    'NOME DEL GRAFICO
    stmp = " F2 "
    X.CurrentX = XX1 - X.TextWidth(stmp)
    X.CurrentY = YY1 - X.TextHeight(stmp) / 2
    X.Print stmp
    'FASCIA DI TOLLERANZA
    If FasciaTolleranza(2) > 0 Then
      X.Line (XX1 + 0.5 * wt * Sc1X / 10, YY1 - (FasciaTolleranza(2) / 2) * Sc1Y / 10)-(XX1 + (NZ + 0.5) * wt * Sc1X / 10, YY1 - (FasciaTolleranza(2) / 2) * Sc1Y / 10), QBColor(12)
      X.Line (XX1 + 0.5 * wt * Sc1X / 10, YY1 + (FasciaTolleranza(2) / 2) * Sc1Y / 10)-(XX1 + (NZ + 0.5) * wt * Sc1X / 10, YY1 + (FasciaTolleranza(2) / 2) * Sc1Y / 10), QBColor(12)
      'VALORE
      stmp = CInt(FasciaTolleranza(2) * 1000)
      X.CurrentX = XX1 - X.TextWidth(stmp) / 2
      X.CurrentY = YY1 - (FasciaTolleranza(2) / 2) * Sc1Y / 10 - 0.3 - X.TextHeight(stmp)
      X.Print stmp
      'FRECCIA SUPERIORE
      ftmp1 = (ANGOLO / 180 * PG) / NumeroRette
      For i = -NumeroRette To NumeroRette
        XX2 = XX1 + 0.3 * Sin(i * ftmp1)
        YY2 = YY1 - (FasciaTolleranza(2) / 2) * Sc1Y / 10 - 0.25 * Cos((ANGOLO / 180 * PG))
        X.Line (XX1, YY1 - (FasciaTolleranza(2) / 2) * Sc1Y / 10)-(XX2, YY2), QBColor(12)
      Next i
      X.Line (XX1 - 0.25, YY1 - (FasciaTolleranza(2) / 2) * Sc1Y / 10)-(XX1 + 0.25, YY1 - (FasciaTolleranza(2) / 2) * Sc1Y / 10), QBColor(12)
      'FRECCIA INFERIORE
      ftmp1 = (ANGOLO / 180 * PG) / NumeroRette
      For i = -NumeroRette To NumeroRette
        XX2 = XX1 + 0.3 * Sin(i * ftmp1)
        YY2 = YY1 + (FasciaTolleranza(2) / 2) * Sc1Y / 10 + 0.25 * Cos((ANGOLO / 180 * PG))
        X.Line (XX1, YY1 + (FasciaTolleranza(2) / 2) * Sc1Y / 10)-(XX2, YY2), QBColor(12)
      Next i
      X.Line (XX1 - 0.25, YY1 + (FasciaTolleranza(2) / 2) * Sc1Y / 10)-(XX1 + 0.25, YY1 + (FasciaTolleranza(2) / 2) * Sc1Y / 10), QBColor(12)
    End If
    'RIFERIMENTO 0
    X.Line (XX1 + 0.5 * wt * Sc1X / 10, YY1)-(XX1 + (NZ + 0.5) * wt * Sc1X / 10, YY1), QBColor(0)
    MaxFp(1, 1) = 1: MaxFp(1, 2) = Divf2(2) - Divf2(1)
    MinFp(2, 1) = 1: MinFp(2, 2) = Divf2(2) - Divf2(1)
    ifu = 0: fu = 0
    For i = 1 To NZ - 1
      'Valore corrente
      ftmp1 = Divf2(i + 1) - Divf2(i)
      'Ricerca del massimo e minimo
      If MaxFp(1, 2) <= Abs(ftmp1) Then MaxFp(1, 1) = i: MaxFp(1, 2) = Abs(ftmp1)
      If MinFp(2, 2) >= Abs(ftmp1) Then MinFp(2, 1) = i: MinFp(2, 2) = Abs(ftmp1)
      If (i < NZ - 1) Then
        ftmp2 = Divf2(i + 1 + 1) - Divf2(i + 1)
        ftmp2 = Abs(ftmp1 - ftmp2)
        If fu <= ftmp2 Then ifu = i: fu = ftmp2
      End If
      'Errore divisione
      XX2 = XX1 + i * wt * Sc1X / 10
      YY2 = YY1 + ftmp1 * Sc1Y / 10
      'Livello ON : ALTEZZA RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YY2)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(9)
      'Livello OFF: LATI RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YY1)-(XX2 - (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      X.Line (XX2 + (w1 / 2) * Sc1X / 10, YY1)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      'Livello OFF: BASE RETTANGOLO
      X.Line (XX2 - (wt / 2) * Sc1X / 10, YY1)-(XX2 - (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
      X.Line (XX2 + (wt / 2) * Sc1X / 10, YY1)-(XX2 + (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
    Next i
    i = NZ
      'Valore corrente
      ftmp1 = Divf2(1) - Divf2(i)
      'Ricerca del massimo e minimo
      If MaxFp(1, 2) <= Abs(ftmp1) Then MaxFp(1, 1) = i: MaxFp(1, 2) = Abs(ftmp1)
      If MinFp(2, 2) >= Abs(ftmp1) Then MinFp(2, 1) = i: MinFp(2, 2) = Abs(ftmp1)
      If (i < NZ - 1) Then
        ftmp2 = Divf2(i + 1 + 1) - Divf2(i + 1)
        ftmp2 = Abs(ftmp1 - ftmp2)
        If fu <= ftmp2 Then ifu = i: fu = ftmp2
      End If
      'Errore divisione
      XX2 = XX1 + i * wt * Sc1X / 10
      YY2 = YY1 + ftmp1 * Sc1Y / 10
      'Livello ON : ALTEZZA RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YY2)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(9)
      'Livello OFF: LATI RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YY1)-(XX2 - (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      X.Line (XX2 + (w1 / 2) * Sc1X / 10, YY1)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      'Livello OFF: BASE RETTANGOLO
      X.Line (XX2 - (wt / 2) * Sc1X / 10, YY1)-(XX2 - (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
      X.Line (XX2 + (wt / 2) * Sc1X / 10, YY1)-(XX2 + (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
    'fp MASSIMO di F2
    stmp = "fp = " & CInt(MaxFp(1, 2) * 1000) & "/" & Format$(MaxFp(1, 1), "##0")
    X.CurrentX = (MLeft + 3) + 1 * Box2W / 4
    X.CurrentY = MTop + 4 * Box2H / 4 - X.TextHeight(stmp) / 2
    X.Print stmp
    i = WritePrivateProfileString("CHK0", "DERIVATA_DIVISIONE(2)", CInt(MaxFp(1, 2) * 1000), Path_LAVORAZIONE_INI)
    'fu di F2: asterisco
    ftmp1 = Divf2(ifu + 1) - Divf2(ifu)
    stmp = "*"
    If ftmp1 > 0 Then
      X.CurrentX = XX1 + ifu * wt * Sc1X / 10 - X.TextWidth(stmp) / 2
      X.CurrentY = YY1 + ftmp1 * Sc1Y / 10
      X.Print stmp
    Else
      X.CurrentX = XX1 + ifu * wt * Sc1X / 10 - X.TextWidth(stmp) / 2
      X.CurrentY = YY1 + ftmp1 * Sc1Y / 10 - X.TextHeight(stmp)
      X.Print stmp
    End If
    'fu di F2: valore
    stmp = " fu = " & CInt(fu * 1000) & "/" & Format$(ifu, "##0") & " "
    XX1 = (MLeft + 3) + 3 * Box2W / 4
    YY1 = MTop + 4 * Box2H / 4 - X.TextHeight(stmp) / 2
    X.Line (XX1, YY1)-(XX1 + X.TextWidth(stmp), YY1 + X.TextHeight(stmp)), , B
    X.CurrentX = XX1
    X.CurrentY = YY1
    X.Print stmp
    i = WritePrivateProfileString("CHK0", "fu(2)", CInt(fu * 1000), Path_LAVORAZIONE_INI)
  End If

Exit Sub

errVISUALIZZA_DERIVATA_DIVISIONE:
  WRITE_DIALOG "SUB: VISUALIZZA_DERIVATA_DIVISIONE ERROR " & str(Err)
  'Exit Sub
  Resume Next

End Sub

Sub VISUALIZZA_DERIVATA_DIVISIONE_VITI(X As Control)
'
Dim Sc1X                As Single
Dim Sc1Y                As Single
Dim stmp                As String
Dim MLeft               As Single
Dim MTop                As Single
Dim XX1                 As Single
Dim XX2                 As Single
Dim YY1                 As Single
Dim YY2                 As Single
Dim wt                  As Single
Dim w1                  As Single
Dim FasciaTolleranza(2) As Single
Dim MaxFp(2, 2)         As Single
Dim MinFp(2, 2)         As Single
Dim fu                  As Single
Dim ifu                 As Integer
Dim ftmp1               As Single
Dim ftmp2               As Single
Const NumeroRette = 15
Const ANGOLO = 20
Const Box2H = 7
Const Box2W = 14
Const DELTA_Y = 4
'
On Error GoTo errVISUALIZZA_DERIVATA_DIVISIONE_VITI
  '
  'LARGHEZZA TOTALE
  wt = 1
  'LARGHEZZA LIVELLO ON
  w1 = 0.5
  'FASCIA DI TOLLERANZA
  FasciaTolleranza(1) = val(GetInfo("CHK0", "TOLLERANZA_DIV(1)", Path_LAVORAZIONE_INI))
  FasciaTolleranza(2) = val(GetInfo("CHK0", "TOLLERANZA_DIV(2)", Path_LAVORAZIONE_INI))
  '
  MLeft = val(GetInfo("CHK0", "MLeft", Path_LAVORAZIONE_INI))
  MTop = val(GetInfo("CHK0", "MTop", Path_LAVORAZIONE_INI))
  MTop = MTop + DELTA_Y
  stmp = GetInfo("CHK0", "ScalaDIVISIONEx", Path_LAVORAZIONE_INI): Sc1X = val(stmp)
  stmp = GetInfo("CHK0", "ScalaDIVISIONEy", Path_LAVORAZIONE_INI): Sc1Y = val(stmp)
  '
  XX1 = (MLeft + 3) - 2
  YY1 = MTop + 1 * Box2H / 4
  stmp = frmt(Sc1Y, 4) & ":1"
  X.CurrentX = XX1 - X.TextWidth(stmp) / 2
  X.CurrentY = YY1 - 0.5 - 2 * X.TextHeight(stmp)
  X.Print stmp
  X.Line (XX1 - 0.5, YY1 - 0.5)-(XX1 + 0.5, YY1 - 0.5), QBColor(9)
  'FRECCIA SUPERIORE
  ftmp1 = (ANGOLO / 180 * PG) / NumeroRette
  For i = -NumeroRette To NumeroRette
    XX2 = XX1 + 0.3 * Sin(i * ftmp1)
    YY2 = YY1 - 0.5 - 0.25 * Cos((ANGOLO / 180 * PG))
    X.Line (XX1, YY1 - 0.5)-(XX2, YY2), QBColor(9)
  Next i
  'FRECCIA INFERIORE
  ftmp1 = (ANGOLO / 180 * PG) / NumeroRette
  For i = -NumeroRette To NumeroRette
    XX2 = XX1 + 0.3 * Sin(i * ftmp1)
    YY2 = YY1 + 0.5 + 0.25 * Cos((ANGOLO / 180 * PG))
    X.Line (XX1, YY1 + 0.5)-(XX2, YY2), QBColor(9)
  Next i
  X.Line (XX1 - 0.5, YY1 + 0.5)-(XX1 + 0.5, YY1 + 0.5), QBColor(9)
  stmp = frmt(10 / Sc1Y, 4) & "mm"
  X.CurrentX = XX1 - X.TextWidth(stmp) / 2
  X.CurrentY = YY1 - X.TextHeight(stmp) / 2
  X.Print stmp
  'SEGNO POSITIVO
  stmp = " + "
  XX1 = (MLeft + 3)
  YY1 = MTop + 0 * Box2H / 4 - X.TextHeight(stmp) / 2
  X.Line (XX1, YY1)-(XX1 + X.TextWidth(stmp), YY1 + X.TextHeight(stmp)), , B
  X.CurrentX = XX1
  X.CurrentY = YY1
  X.Print stmp
  'NOME DEL GRAFICO
  stmp = " fp "
  X.CurrentX = (MLeft + 3) - X.TextWidth(stmp)
  X.CurrentY = MTop + 2 * Box2H / 4 - X.TextHeight(stmp) / 2
  X.Print stmp
  'SEGNO NEGATIVO
  stmp = " - "
  XX1 = (MLeft + 3)
  YY1 = MTop + 2 * Box2H / 4 - X.TextHeight(stmp) / 2
  X.Line (XX1, YY1)-(XX1 + X.TextWidth(stmp), YY1 + X.TextHeight(stmp)), , B
  X.CurrentX = XX1
  X.CurrentY = YY1
  X.Print stmp
  'SEGNO POSITIVO
  stmp = " + "
  XX1 = (MLeft + 3)
  YY1 = MTop + 4 * Box2H / 4 - X.TextHeight(stmp) / 2
  X.Line (XX1, YY1)-(XX1 + X.TextWidth(stmp), YY1 + X.TextHeight(stmp)), , B
  X.CurrentX = XX1
  X.CurrentY = YY1
  X.Print stmp
  'GRAFICO DI CONTROLLO DIVISIONE
  If (DivCon > 0) Then
    'FIANCO 1
    XX1 = (MLeft + 3)
    YY1 = MTop + 1 * Box2H / 4
    'NOME DEL GRAFICO
    stmp = " F1 "
    X.CurrentX = XX1 - X.TextWidth(stmp)
    X.CurrentY = YY1 - X.TextHeight(stmp) / 2
    X.Print stmp
    'FASCIA DI TOLLERANZA
    If FasciaTolleranza(1) > 0 Then
      X.Line (XX1 + 0.5 * wt * Sc1X / 10, YY1 - (FasciaTolleranza(1) / 2) * Sc1Y / 10)-(XX1 + (DivCon + 0.5) * wt * Sc1X / 10, YY1 - (FasciaTolleranza(1) / 2) * Sc1Y / 10), QBColor(12)
      X.Line (XX1 + 0.5 * wt * Sc1X / 10, YY1 + (FasciaTolleranza(1) / 2) * Sc1Y / 10)-(XX1 + (DivCon + 0.5) * wt * Sc1X / 10, YY1 + (FasciaTolleranza(1) / 2) * Sc1Y / 10), QBColor(12)
      'VALORE
      stmp = CInt(FasciaTolleranza(1) * 1000)
      X.CurrentX = XX1 - X.TextWidth(stmp) / 2
      X.CurrentY = YY1 - (FasciaTolleranza(1) / 2) * Sc1Y / 10 - 0.3 - X.TextHeight(stmp)
      X.Print stmp
      'FRECCIA SUPERIORE
      ftmp1 = (ANGOLO / 180 * PG) / NumeroRette
      For i = -NumeroRette To NumeroRette
        XX2 = XX1 + 0.3 * Sin(i * ftmp1)
        YY2 = YY1 - (FasciaTolleranza(1) / 2) * Sc1Y / 10 - 0.25 * Cos((ANGOLO / 180 * PG))
        X.Line (XX1, YY1 - (FasciaTolleranza(1) / 2) * Sc1Y / 10)-(XX2, YY2), QBColor(12)
      Next i
      X.Line (XX1 - 0.25, YY1 - (FasciaTolleranza(1) / 2) * Sc1Y / 10)-(XX1 + 0.25, YY1 - (FasciaTolleranza(1) / 2) * Sc1Y / 10), QBColor(12)
      'FRECCIA INFERIORE
      ftmp1 = (ANGOLO / 180 * PG) / NumeroRette
      For i = -NumeroRette To NumeroRette
        XX2 = XX1 + 0.3 * Sin(i * ftmp1)
        YY2 = YY1 + (FasciaTolleranza(1) / 2) * Sc1Y / 10 + 0.25 * Cos((ANGOLO / 180 * PG))
        X.Line (XX1, YY1 + (FasciaTolleranza(1) / 2) * Sc1Y / 10)-(XX2, YY2), QBColor(12)
      Next i
      X.Line (XX1 - 0.25, YY1 + (FasciaTolleranza(1) / 2) * Sc1Y / 10)-(XX1 + 0.25, YY1 + (FasciaTolleranza(1) / 2) * Sc1Y / 10), QBColor(12)
    End If
    'RIFERIMENTO
    X.Line (XX1 + 0.5 * wt * Sc1X / 10, YY1)-(XX1 + (DivCon + 0.5) * wt * Sc1X / 10, YY1), QBColor(0)
    MaxFp(1, 1) = 1: MaxFp(1, 2) = Divf1(2) - Divf1(1)
    MinFp(2, 1) = 1: MinFp(2, 2) = Divf1(2) - Divf1(1)
    ifu = 0: fu = 0
    For i = 1 To DivCon - 1
      'Valore corrente
      ftmp1 = Divf1(i + 1) - Divf1(i)
      'Ricerca del massimo e minimo
      If MaxFp(1, 2) <= Abs(ftmp1) Then MaxFp(1, 1) = i: MaxFp(1, 2) = Abs(ftmp1)
      If MinFp(2, 2) >= Abs(ftmp1) Then MinFp(2, 1) = i: MinFp(2, 2) = Abs(ftmp1)
      'Ricerca fu
      If (i < DivCon - 1) Then
        ftmp2 = Divf1(i + 1 + 1) - Divf1(i + 1)
        ftmp2 = Abs(ftmp1 - ftmp2)
        If fu <= ftmp2 Then ifu = i: fu = ftmp2
      End If
      'Errore divisione
      XX2 = XX1 + i * wt * Sc1X / 10
      YY2 = YY1 - ftmp1 * Sc1Y / 10
      'Livello ON : ALTEZZA RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YY2)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(9)
      'Livello OFF: LATI RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YY1)-(XX2 - (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      X.Line (XX2 + (w1 / 2) * Sc1X / 10, YY1)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      'Livello OFF: BASE RETTANGOLO
      X.Line (XX2 - (wt / 2) * Sc1X / 10, YY1)-(XX2 - (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
      X.Line (XX2 + (wt / 2) * Sc1X / 10, YY1)-(XX2 + (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
    Next i
    i = DivCon
      'Valore corrente
      ftmp1 = Divf1(1) - Divf1(i)
      'Ricerca del massimo e minimo
      If MaxFp(1, 2) <= Abs(ftmp1) Then MaxFp(1, 1) = i: MaxFp(1, 2) = Abs(ftmp1)
      If MinFp(2, 2) >= Abs(ftmp1) Then MinFp(2, 1) = i: MinFp(2, 2) = Abs(ftmp1)
      'Ricerca fu
      If (i < DivCon - 1) Then
        ftmp2 = Divf1(i + 1 + 1) - Divf1(i + 1)
        ftmp2 = Abs(ftmp1 - ftmp2)
        If fu <= ftmp2 Then ifu = i: fu = ftmp2
      End If
      'Errore divisione
      XX2 = XX1 + i * wt * Sc1X / 10
      YY2 = YY1 - ftmp1 * Sc1Y / 10
      'Livello ON : ALTEZZA RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YY2)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(9)
      'Livello OFF: LATI RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YY1)-(XX2 - (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      X.Line (XX2 + (w1 / 2) * Sc1X / 10, YY1)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      'Livello OFF: BASE RETTANGOLO
      X.Line (XX2 - (wt / 2) * Sc1X / 10, YY1)-(XX2 - (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
      X.Line (XX2 + (wt / 2) * Sc1X / 10, YY1)-(XX2 + (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
    'fp MASSIMO di F1
    stmp = "fp = " & CInt(MaxFp(1, 2) * 1000) & "/" & Format$(MaxFp(1, 1), "##0")
    X.CurrentX = (MLeft + 3) + 1 * Box2W / 4
    X.CurrentY = MTop + 0 * Box2H / 4 - X.TextHeight(stmp) / 2
    X.Print stmp
    i = WritePrivateProfileString("CHK0", "DERIVATA_DIVISIONE(1)", CInt(MaxFp(1, 2) * 1000), Path_LAVORAZIONE_INI)
    'fu di F1: asterisco
    ftmp1 = Divf1(ifu + 1) - Divf1(ifu)
    stmp = "*"
    If (ifu > 0) Then
      If (ftmp1 >= 0) Then
        X.CurrentX = XX1 + ifu * wt * Sc1X / 10 - X.TextWidth(stmp) / 2
        X.CurrentY = YY1 - ftmp1 * Sc1Y / 10 - X.TextHeight(stmp)
      Else
        X.CurrentX = XX1 + ifu * wt * Sc1X / 10 - X.TextWidth(stmp) / 2
        X.CurrentY = YY1 - ftmp1 * Sc1Y / 10
      End If
    End If
    X.Print stmp
    'fu di F1: valore
    stmp = " fu = " & CInt(fu * 1000) & "/" & Format$(ifu, "##0") & " "
    XX1 = (MLeft + 3) + 3 * Box2W / 4
    YY1 = MTop + 0 * Box2H / 4 - X.TextHeight(stmp) / 2
    X.Line (XX1, YY1)-(XX1 + X.TextWidth(stmp), YY1 + X.TextHeight(stmp)), , B
    X.CurrentX = XX1
    X.CurrentY = YY1
    X.Print stmp
    i = WritePrivateProfileString("CHK0", "fu(1)", CInt(fu * 1000), Path_LAVORAZIONE_INI)
    'FIANCO 2
    XX1 = (MLeft + 3)
    YY1 = MTop + 3 * Box2H / 4
    'NOME DEL GRAFICO
    stmp = " F2 "
    X.CurrentX = XX1 - X.TextWidth(stmp)
    X.CurrentY = YY1 - X.TextHeight(stmp) / 2
    X.Print stmp
    'FASCIA DI TOLLERANZA
    If FasciaTolleranza(2) > 0 Then
      X.Line (XX1 + 0.5 * wt * Sc1X / 10, YY1 - (FasciaTolleranza(2) / 2) * Sc1Y / 10)-(XX1 + (DivCon + 0.5) * wt * Sc1X / 10, YY1 - (FasciaTolleranza(2) / 2) * Sc1Y / 10), QBColor(12)
      X.Line (XX1 + 0.5 * wt * Sc1X / 10, YY1 + (FasciaTolleranza(2) / 2) * Sc1Y / 10)-(XX1 + (DivCon + 0.5) * wt * Sc1X / 10, YY1 + (FasciaTolleranza(2) / 2) * Sc1Y / 10), QBColor(12)
      'VALORE
      stmp = CInt(FasciaTolleranza(2) * 1000)
      X.CurrentX = XX1 - X.TextWidth(stmp) / 2
      X.CurrentY = YY1 - (FasciaTolleranza(2) / 2) * Sc1Y / 10 - 0.3 - X.TextHeight(stmp)
      X.Print stmp
      'FRECCIA SUPERIORE
      ftmp1 = (ANGOLO / 180 * PG) / NumeroRette
      For i = -NumeroRette To NumeroRette
        XX2 = XX1 + 0.3 * Sin(i * ftmp1)
        YY2 = YY1 - (FasciaTolleranza(2) / 2) * Sc1Y / 10 - 0.25 * Cos((ANGOLO / 180 * PG))
        X.Line (XX1, YY1 - (FasciaTolleranza(2) / 2) * Sc1Y / 10)-(XX2, YY2), QBColor(12)
      Next i
      X.Line (XX1 - 0.25, YY1 - (FasciaTolleranza(2) / 2) * Sc1Y / 10)-(XX1 + 0.25, YY1 - (FasciaTolleranza(2) / 2) * Sc1Y / 10), QBColor(12)
      'FRECCIA INFERIORE
      ftmp1 = (ANGOLO / 180 * PG) / NumeroRette
      For i = -NumeroRette To NumeroRette
        XX2 = XX1 + 0.3 * Sin(i * ftmp1)
        YY2 = YY1 + (FasciaTolleranza(2) / 2) * Sc1Y / 10 + 0.25 * Cos((ANGOLO / 180 * PG))
        X.Line (XX1, YY1 + (FasciaTolleranza(2) / 2) * Sc1Y / 10)-(XX2, YY2), QBColor(12)
      Next i
      X.Line (XX1 - 0.25, YY1 + (FasciaTolleranza(2) / 2) * Sc1Y / 10)-(XX1 + 0.25, YY1 + (FasciaTolleranza(2) / 2) * Sc1Y / 10), QBColor(12)
    End If
    'RIFERIMENTO 0
    X.Line (XX1 + 0.5 * wt * Sc1X / 10, YY1)-(XX1 + (DivCon + 0.5) * wt * Sc1X / 10, YY1), QBColor(0)
    MaxFp(1, 1) = 1: MaxFp(1, 2) = Divf2(2) - Divf2(1)
    MinFp(2, 1) = 1: MinFp(2, 2) = Divf2(2) - Divf2(1)
    ifu = 0: fu = 0
    For i = 1 To DivCon - 1
      'Valore corrente
      ftmp1 = Divf2(i + 1) - Divf2(i)
      'Ricerca del massimo e minimo
      If MaxFp(1, 2) <= Abs(ftmp1) Then MaxFp(1, 1) = i: MaxFp(1, 2) = Abs(ftmp1)
      If MinFp(2, 2) >= Abs(ftmp1) Then MinFp(2, 1) = i: MinFp(2, 2) = Abs(ftmp1)
      If (i < DivCon - 1) Then
        ftmp2 = Divf2(i + 1 + 1) - Divf2(i + 1)
        ftmp2 = Abs(ftmp1 - ftmp2)
        If fu <= ftmp2 Then ifu = i: fu = ftmp2
      End If
      'Errore divisione
      XX2 = XX1 + i * wt * Sc1X / 10
      YY2 = YY1 + ftmp1 * Sc1Y / 10
      'Livello ON : ALTEZZA RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YY2)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(9)
      'Livello OFF: LATI RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YY1)-(XX2 - (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      X.Line (XX2 + (w1 / 2) * Sc1X / 10, YY1)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      'Livello OFF: BASE RETTANGOLO
      X.Line (XX2 - (wt / 2) * Sc1X / 10, YY1)-(XX2 - (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
      X.Line (XX2 + (wt / 2) * Sc1X / 10, YY1)-(XX2 + (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
    Next i
    i = DivCon
      'Valore corrente
      ftmp1 = Divf2(1) - Divf2(i)
      'Ricerca del massimo e minimo
      If MaxFp(1, 2) <= Abs(ftmp1) Then MaxFp(1, 1) = i: MaxFp(1, 2) = Abs(ftmp1)
      If MinFp(2, 2) >= Abs(ftmp1) Then MinFp(2, 1) = i: MinFp(2, 2) = Abs(ftmp1)
      If (i < DivCon - 1) Then
        ftmp2 = Divf2(i + 1 + 1) - Divf2(i + 1)
        ftmp2 = Abs(ftmp1 - ftmp2)
        If fu <= ftmp2 Then ifu = i: fu = ftmp2
      End If
      'Errore divisione
      XX2 = XX1 + i * wt * Sc1X / 10
      YY2 = YY1 + ftmp1 * Sc1Y / 10
      'Livello ON : ALTEZZA RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YY2)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(9)
      'Livello OFF: LATI RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YY1)-(XX2 - (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      X.Line (XX2 + (w1 / 2) * Sc1X / 10, YY1)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      'Livello OFF: BASE RETTANGOLO
      X.Line (XX2 - (wt / 2) * Sc1X / 10, YY1)-(XX2 - (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
      X.Line (XX2 + (wt / 2) * Sc1X / 10, YY1)-(XX2 + (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
    'fp MASSIMO di F2
    stmp = "fp = " & CInt(MaxFp(1, 2) * 1000) & "/" & Format$(MaxFp(1, 1), "##0")
    X.CurrentX = (MLeft + 3) + 1 * Box2W / 4
    X.CurrentY = MTop + 4 * Box2H / 4 - X.TextHeight(stmp) / 2
    X.Print stmp
    i = WritePrivateProfileString("CHK0", "DERIVATA_DIVISIONE(2)", CInt(MaxFp(1, 2) * 1000), Path_LAVORAZIONE_INI)
    'fu di F2: asterisco
    ftmp1 = Divf2(ifu + 1) - Divf2(ifu)
    stmp = "*"
    If (ifu > 0) Then
      If (ftmp1 > 0) Then
        X.CurrentX = XX1 + ifu * wt * Sc1X / 10 - X.TextWidth(stmp) / 2
        X.CurrentY = YY1 + ftmp1 * Sc1Y / 10
        X.Print stmp
      Else
        X.CurrentX = XX1 + ifu * wt * Sc1X / 10 - X.TextWidth(stmp) / 2
        X.CurrentY = YY1 + ftmp1 * Sc1Y / 10 - X.TextHeight(stmp)
        X.Print stmp
      End If
    End If
    'fu di F2: valore
    stmp = " fu = " & CInt(fu * 1000) & "/" & Format$(ifu, "##0") & " "
    XX1 = (MLeft + 3) + 3 * Box2W / 4
    YY1 = MTop + 4 * Box2H / 4 - X.TextHeight(stmp) / 2
    X.Line (XX1, YY1)-(XX1 + X.TextWidth(stmp), YY1 + X.TextHeight(stmp)), , B
    X.CurrentX = XX1
    X.CurrentY = YY1
    X.Print stmp
    i = WritePrivateProfileString("CHK0", "fu(2)", CInt(fu * 1000), Path_LAVORAZIONE_INI)
  End If

Exit Sub

errVISUALIZZA_DERIVATA_DIVISIONE_VITI:
  WRITE_DIALOG "SUB: VISUALIZZA_DERIVATA_DIVISIONE_VITI ERROR " & str(Err)
  'Exit Sub
  Resume Next

End Sub

Sub VISUALIZZA_DIVISIONE(X As Control)
'
Dim Sc1X                As Single
Dim Sc1Y                As Single
Dim stmp                As String
Dim MLeft               As Single
Dim MTop                As Single
Dim XX1                 As Single
Dim XX2                 As Single
Dim YY1                 As Single
Dim YY2                 As Single
Dim YYpre               As Single
Dim YYsuc               As Single
Dim wt                  As Single
Dim w1                  As Single
Dim FasciaTolleranza(2) As Single
Dim MaxFp(2, 2)         As Single
Dim MinFp(2, 2)         As Single
Dim ftmp                As Single
Const NumeroRette = 15
Const ANGOLO = 20
Const Box2H = 7
Const Box2W = 14
Const DELTA_Y = 12
'
On Error GoTo errVISUALIZZA_DIVISIONE
  '
  'LARGHEZZA TOTALE
  wt = 1
  'LARGHEZZA LIVELLO ON
  w1 = 1
  'FASCIA DI TOLLERANZA
  FasciaTolleranza(1) = val(GetInfo("CHK0", "TOLLERANZA_DIV(1)", Path_LAVORAZIONE_INI))
  FasciaTolleranza(2) = val(GetInfo("CHK0", "TOLLERANZA_DIV(2)", Path_LAVORAZIONE_INI))
  '
  MLeft = val(GetInfo("CHK0", "MLeft", Path_LAVORAZIONE_INI))
  MTop = val(GetInfo("CHK0", "MTop", Path_LAVORAZIONE_INI))
  MTop = MTop + DELTA_Y
  stmp = GetInfo("CHK0", "ScalaDIVISIONEx", Path_LAVORAZIONE_INI): Sc1X = val(stmp)
  stmp = GetInfo("CHK0", "ScalaDIVISIONEy", Path_LAVORAZIONE_INI): Sc1Y = val(stmp)
  '
  XX1 = (MLeft + 3) - 2
  YY1 = MTop + 1 * Box2H / 4
  stmp = frmt(Sc1Y, 4) & ":1"
  X.CurrentX = XX1 - X.TextWidth(stmp) / 2
  X.CurrentY = YY1 - 0.5 - 2 * X.TextHeight(stmp)
  X.Print stmp
  X.Line (XX1 - 0.5, YY1 - 0.5)-(XX1 + 0.5, YY1 - 0.5), QBColor(9)
  'FRECCIA SUPERIORE
  ftmp = (ANGOLO / 180 * PG) / NumeroRette
  For i = -NumeroRette To NumeroRette
    XX2 = XX1 + 0.3 * Sin(i * ftmp)
    YY2 = YY1 - 0.5 - 0.25 * Cos((ANGOLO / 180 * PG))
    X.Line (XX1, YY1 - 0.5)-(XX2, YY2), QBColor(9)
  Next i
  'FRECCIA INFERIORE
  ftmp = (ANGOLO / 180 * PG) / NumeroRette
  For i = -NumeroRette To NumeroRette
    XX2 = XX1 + 0.3 * Sin(i * ftmp)
    YY2 = YY1 + 0.5 + 0.25 * Cos((ANGOLO / 180 * PG))
    X.Line (XX1, YY1 + 0.5)-(XX2, YY2), QBColor(9)
  Next i
  X.Line (XX1 - 0.5, YY1 + 0.5)-(XX1 + 0.5, YY1 + 0.5), QBColor(9)
  stmp = frmt(10 / Sc1Y, 4) & "mm"
  X.CurrentX = XX1 - X.TextWidth(stmp) / 2
  X.CurrentY = YY1 - X.TextHeight(stmp) / 2
  X.Print stmp
  'SEGNO POSITIVO
  stmp = " + "
  XX1 = (MLeft + 3)
  YY1 = MTop + 0 * Box2H / 4 - X.TextHeight(stmp) / 2
  X.Line (XX1, YY1)-(XX1 + X.TextWidth(stmp), YY1 + X.TextHeight(stmp)), , B
  X.CurrentX = XX1
  X.CurrentY = YY1
  X.Print stmp
  'NOME DEL GRAFICO
  stmp = " Fp "
  X.CurrentX = (MLeft + 3) - X.TextWidth(stmp)
  X.CurrentY = MTop + 2 * Box2H / 4 - X.TextHeight(stmp) / 2
  X.Print stmp
  'SEGNO NEGATIVO
  stmp = " - "
  XX1 = (MLeft + 3)
  YY1 = MTop + 2 * Box2H / 4 - X.TextHeight(stmp) / 2
  X.Line (XX1, YY1)-(XX1 + X.TextWidth(stmp), YY1 + X.TextHeight(stmp)), , B
  X.CurrentX = XX1
  X.CurrentY = YY1
  X.Print stmp
  'SEGNO POSITIVO
  stmp = " + "
  XX1 = (MLeft + 3)
  YY1 = MTop + 4 * Box2H / 4 - X.TextHeight(stmp) / 2
  X.Line (XX1, YY1)-(XX1 + X.TextWidth(stmp), YY1 + X.TextHeight(stmp)), , B
  X.CurrentX = XX1
  X.CurrentY = YY1
  X.Print stmp
  'GRAFICO DI CONTROLLO DIVISIONE
  If DivCon = 1 Then
    'FIANCO 1
    XX1 = (MLeft + 3)
    YY1 = MTop + 1 * Box2H / 4
    'NOME DEL GRAFICO
    stmp = " F1 "
    X.CurrentX = XX1 - X.TextWidth(stmp)
    X.CurrentY = YY1 - X.TextHeight(stmp) / 2
    X.Print stmp
    'FASCIA DI TOLLERANZA
    If FasciaTolleranza(1) > 0 Then
      X.Line (XX1 + 0.5 * wt * Sc1X / 10, YY1 - (FasciaTolleranza(1) / 2) * Sc1Y / 10)-(XX1 + (NZ + 0.5) * wt * Sc1X / 10, YY1 - (FasciaTolleranza(1) / 2) * Sc1Y / 10), QBColor(12)
      X.Line (XX1 + 0.5 * wt * Sc1X / 10, YY1 + (FasciaTolleranza(1) / 2) * Sc1Y / 10)-(XX1 + (NZ + 0.5) * wt * Sc1X / 10, YY1 + (FasciaTolleranza(1) / 2) * Sc1Y / 10), QBColor(12)
      'VALORE
      stmp = CInt(FasciaTolleranza(1) * 1000)
      X.CurrentX = XX1 - X.TextWidth(stmp) / 2
      X.CurrentY = YY1 - (FasciaTolleranza(1) / 2) * Sc1Y / 10 - 0.3 - X.TextHeight(stmp)
      X.Print stmp
      'FRECCIA SUPERIORE
      ftmp = (ANGOLO / 180 * PG) / NumeroRette
      For i = -NumeroRette To NumeroRette
        XX2 = XX1 + 0.3 * Sin(i * ftmp)
        YY2 = YY1 - (FasciaTolleranza(1) / 2) * Sc1Y / 10 - 0.25 * Cos((ANGOLO / 180 * PG))
        X.Line (XX1, YY1 - (FasciaTolleranza(1) / 2) * Sc1Y / 10)-(XX2, YY2), QBColor(12)
      Next i
      X.Line (XX1 - 0.25, YY1 - (FasciaTolleranza(1) / 2) * Sc1Y / 10)-(XX1 + 0.25, YY1 - (FasciaTolleranza(1) / 2) * Sc1Y / 10), QBColor(12)
      'FRECCIA INFERIORE
      ftmp = (ANGOLO / 180 * PG) / NumeroRette
      For i = -NumeroRette To NumeroRette
        XX2 = XX1 + 0.3 * Sin(i * ftmp)
        YY2 = YY1 + (FasciaTolleranza(1) / 2) * Sc1Y / 10 + 0.25 * Cos((ANGOLO / 180 * PG))
        X.Line (XX1, YY1 + (FasciaTolleranza(1) / 2) * Sc1Y / 10)-(XX2, YY2), QBColor(12)
      Next i
      X.Line (XX1 - 0.25, YY1 + (FasciaTolleranza(1) / 2) * Sc1Y / 10)-(XX1 + 0.25, YY1 + (FasciaTolleranza(1) / 2) * Sc1Y / 10), QBColor(12)
    End If
    MaxFp(1, 1) = 1: MaxFp(1, 2) = Divf1(1)
    MinFp(2, 1) = 1: MinFp(2, 2) = Divf1(1)
    i = 1
      'Ricerca del massimo e minimo
      If MaxFp(1, 2) <= Divf1(i) Then MaxFp(1, 1) = i: MaxFp(1, 2) = Divf1(i)
      If MinFp(2, 2) >= Divf1(i) Then MinFp(2, 1) = i: MinFp(2, 2) = Divf1(i)
      'Errore divisione
      XX2 = XX1 + i * wt * Sc1X / 10
      YY2 = YY1 - Divf1(i) * Sc1Y / 10
      YYpre = YY1
      YYsuc = YY1 - Divf1(i + 1) * Sc1Y / 10
      'Livello ON : ALTEZZA RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YY2)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(9)
      'Livello OFF: LATI RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YYpre)-(XX2 - (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      X.Line (XX2 + (w1 / 2) * Sc1X / 10, YYsuc)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      'Livello OFF: BASE RETTANGOLO
      X.Line (XX2 - (wt / 2) * Sc1X / 10, YY1)-(XX2 - (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
      X.Line (XX2 + (wt / 2) * Sc1X / 10, YY1)-(XX2 + (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
    For i = 2 To NZ - 1
      'Ricerca del massimo e minimo
      If MaxFp(1, 2) <= Divf1(i) Then MaxFp(1, 1) = i: MaxFp(1, 2) = Divf1(i)
      If MinFp(2, 2) >= Divf1(i) Then MinFp(2, 1) = i: MinFp(2, 2) = Divf1(i)
      'Errore divisione
      XX2 = XX1 + i * wt * Sc1X / 10
      YY2 = YY1 - Divf1(i) * Sc1Y / 10
      YYpre = YY1 - Divf1(i - 1) * Sc1Y / 10
      YYsuc = YY1 - Divf1(i + 1) * Sc1Y / 10
      'Livello ON : ALTEZZA RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YY2)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(9)
      'Livello OFF: LATI RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YYpre)-(XX2 - (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      X.Line (XX2 + (w1 / 2) * Sc1X / 10, YYsuc)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      'Livello OFF: BASE RETTANGOLO
      X.Line (XX2 - (wt / 2) * Sc1X / 10, YY1)-(XX2 - (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
      X.Line (XX2 + (wt / 2) * Sc1X / 10, YY1)-(XX2 + (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
    Next i
    i = NZ
      'Ricerca del massimo e minimo
      If MaxFp(1, 2) <= Divf1(i) Then MaxFp(1, 1) = i: MaxFp(1, 2) = Divf1(i)
      If MinFp(2, 2) >= Divf1(i) Then MinFp(2, 1) = i: MinFp(2, 2) = Divf1(i)
      'Errore divisione
      XX2 = XX1 + i * wt * Sc1X / 10
      YY2 = YY1 - Divf1(i) * Sc1Y / 10
      YYpre = YY1 - Divf1(i - 1) * Sc1Y / 10
      YYsuc = YY1
      'Livello ON : ALTEZZA RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YY2)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(9)
      'Livello OFF: LATI RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YYpre)-(XX2 - (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      X.Line (XX2 + (w1 / 2) * Sc1X / 10, YYsuc)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      'Livello OFF: BASE RETTANGOLO
      X.Line (XX2 - (wt / 2) * Sc1X / 10, YY1)-(XX2 - (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
      X.Line (XX2 + (wt / 2) * Sc1X / 10, YY1)-(XX2 + (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
    'Fp MASSIMO di F1
    stmp = "Fp Max = " & CInt(MaxFp(1, 2) * 1000) & "/" & Format$(MaxFp(1, 1), "##0")
    X.CurrentX = (MLeft + 3) + 1 * Box2W / 4
    X.CurrentY = MTop + 0 * Box2H / 4 - X.TextHeight(stmp) / 2
    X.Print stmp
    'Fp MINIMO di F1
    stmp = "Fp Min = " & CInt(MinFp(2, 2) * 1000) & "/" & Format$(MinFp(2, 1), "##0")
    X.CurrentX = (MLeft + 3) + 3 * Box2W / 4
    X.CurrentY = MTop + 0 * Box2H / 4 - X.TextHeight(stmp) / 2
    X.Print stmp
    'Fp MASSIMA ESCURSIONE di F1
    ftmp = MaxFp(1, 2) - MinFp(2, 2)
    stmp = " Fp(F1) = " & CInt(ftmp * 1000) & " "
    XX1 = (MLeft + 3) + 1 * Box2W / 4
    YY1 = MTop + 2 * Box2H / 4 - X.TextHeight(stmp) / 2
    X.Line (XX1, YY1)-(XX1 + X.TextWidth(stmp), YY1 + X.TextHeight(stmp)), , B
    X.CurrentX = XX1
    X.CurrentY = YY1
    X.Print stmp
    i = WritePrivateProfileString("CHK0", "DIVISIONE(1)", CInt(ftmp * 1000), Path_LAVORAZIONE_INI)
    'FIANCO 2
    XX1 = (MLeft + 3)
    YY1 = MTop + 3 * Box2H / 4
    'NOME DEL GRAFICO
    stmp = " F2 "
    X.CurrentX = XX1 - X.TextWidth(stmp)
    X.CurrentY = YY1 - X.TextHeight(stmp) / 2
    X.Print stmp
    'FASCIA DI TOLLERANZA
    If FasciaTolleranza(2) > 0 Then
      X.Line (XX1 + 0.5 * wt * Sc1X / 10, YY1 - (FasciaTolleranza(2) / 2) * Sc1Y / 10)-(XX1 + (NZ + 0.5) * wt * Sc1X / 10, YY1 - (FasciaTolleranza(2) / 2) * Sc1Y / 10), QBColor(12)
      X.Line (XX1 + 0.5 * wt * Sc1X / 10, YY1 + (FasciaTolleranza(2) / 2) * Sc1Y / 10)-(XX1 + (NZ + 0.5) * wt * Sc1X / 10, YY1 + (FasciaTolleranza(2) / 2) * Sc1Y / 10), QBColor(12)
      'VALORE
      stmp = CInt(FasciaTolleranza(2) * 1000)
      X.CurrentX = XX1 - X.TextWidth(stmp) / 2
      X.CurrentY = YY1 - (FasciaTolleranza(2) / 2) * Sc1Y / 10 - 0.3 - X.TextHeight(stmp)
      X.Print stmp
      'FRECCIA SUPERIORE
      ftmp = (ANGOLO / 180 * PG) / NumeroRette
      For i = -NumeroRette To NumeroRette
        XX2 = XX1 + 0.3 * Sin(i * ftmp)
        YY2 = YY1 - (FasciaTolleranza(2) / 2) * Sc1Y / 10 - 0.25 * Cos((ANGOLO / 180 * PG))
        X.Line (XX1, YY1 - (FasciaTolleranza(2) / 2) * Sc1Y / 10)-(XX2, YY2), QBColor(12)
      Next i
      X.Line (XX1 - 0.25, YY1 - (FasciaTolleranza(2) / 2) * Sc1Y / 10)-(XX1 + 0.25, YY1 - (FasciaTolleranza(2) / 2) * Sc1Y / 10), QBColor(12)
      'FRECCIA INFERIORE
      ftmp = (ANGOLO / 180 * PG) / NumeroRette
      For i = -NumeroRette To NumeroRette
        XX2 = XX1 + 0.3 * Sin(i * ftmp)
        YY2 = YY1 + (FasciaTolleranza(2) / 2) * Sc1Y / 10 + 0.25 * Cos((ANGOLO / 180 * PG))
        X.Line (XX1, YY1 + (FasciaTolleranza(2) / 2) * Sc1Y / 10)-(XX2, YY2), QBColor(12)
      Next i
      X.Line (XX1 - 0.25, YY1 + (FasciaTolleranza(2) / 2) * Sc1Y / 10)-(XX1 + 0.25, YY1 + (FasciaTolleranza(2) / 2) * Sc1Y / 10), QBColor(12)
    End If
    MaxFp(1, 1) = 1: MaxFp(1, 2) = Divf2(1)
    MinFp(2, 1) = 1: MinFp(2, 2) = Divf2(1)
    i = 1
      'Ricerca del massimo e minimo
      If MaxFp(1, 2) <= Divf2(i) Then MaxFp(1, 1) = i: MaxFp(1, 2) = Divf2(i)
      If MinFp(2, 2) >= Divf2(i) Then MinFp(2, 1) = i: MinFp(2, 2) = Divf2(i)
      'Errore divisione
      XX2 = XX1 + i * wt * Sc1X / 10
      YY2 = YY1 + Divf2(i) * Sc1Y / 10
      YYpre = YY1
      YYsuc = YY1 + Divf2(i + 1) * Sc1Y / 10
      'Livello ON : ALTEZZA RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YY2)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(9)
      'Livello OFF: LATI RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YYpre)-(XX2 - (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      X.Line (XX2 + (w1 / 2) * Sc1X / 10, YYsuc)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      'Livello OFF: BASE RETTANGOLO
      X.Line (XX2 - (wt / 2) * Sc1X / 10, YY1)-(XX2 - (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
      X.Line (XX2 + (wt / 2) * Sc1X / 10, YY1)-(XX2 + (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
    For i = 2 To NZ - 1
      'Ricerca del massimo e minimo
      If MaxFp(1, 2) <= Divf2(i) Then MaxFp(1, 1) = i: MaxFp(1, 2) = Divf2(i)
      If MinFp(2, 2) >= Divf2(i) Then MinFp(2, 1) = i: MinFp(2, 2) = Divf2(i)
      'Errore divisione
      XX2 = XX1 + i * wt * Sc1X / 10
      YY2 = YY1 + Divf2(i) * Sc1Y / 10
      YYpre = YY1 + Divf2(i - 1) * Sc1Y / 10
      YYsuc = YY1 + Divf2(i + 1) * Sc1Y / 10
      'Livello ON : ALTEZZA RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YY2)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(9)
      'Livello OFF: LATI RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YYpre)-(XX2 - (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      X.Line (XX2 + (w1 / 2) * Sc1X / 10, YYsuc)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      'Livello OFF: BASE RETTANGOLO
      X.Line (XX2 - (wt / 2) * Sc1X / 10, YY1)-(XX2 - (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
      X.Line (XX2 + (wt / 2) * Sc1X / 10, YY1)-(XX2 + (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
    Next i
    i = NZ
      'Ricerca del massimo e minimo
      If MaxFp(1, 2) <= Divf2(i) Then MaxFp(1, 1) = i: MaxFp(1, 2) = Divf2(i)
      If MinFp(2, 2) >= Divf2(i) Then MinFp(2, 1) = i: MinFp(2, 2) = Divf2(i)
      'Errore divisione
      XX2 = XX1 + i * wt * Sc1X / 10
      YY2 = YY1 + Divf2(i) * Sc1Y / 10
      YYpre = YY1 + Divf2(i - 1) * Sc1Y / 10
      YYsuc = YY1
      'Livello ON : ALTEZZA RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YY2)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(9)
      'Livello OFF: LATI RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YYpre)-(XX2 - (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      X.Line (XX2 + (w1 / 2) * Sc1X / 10, YYsuc)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      'Livello OFF: BASE RETTANGOLO
      X.Line (XX2 - (wt / 2) * Sc1X / 10, YY1)-(XX2 - (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
      X.Line (XX2 + (wt / 2) * Sc1X / 10, YY1)-(XX2 + (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
    'Fp MASSIMO di F2
    stmp = "Fp Max = " & CInt(MaxFp(1, 2) * 1000) & "/" & Format$(MaxFp(1, 1), "##0")
    X.CurrentX = (MLeft + 3) + 1 * Box2W / 4
    X.CurrentY = MTop + 4 * Box2H / 4 - X.TextHeight(stmp) / 2
    X.Print stmp
    'Fp MINIMO di F2
    stmp = "Fp Min = " & CInt(MinFp(2, 2) * 1000) & "/" & Format$(MinFp(2, 1), "##0")
    X.CurrentX = (MLeft + 3) + 3 * Box2W / 4
    X.CurrentY = MTop + 4 * Box2H / 4 - X.TextHeight(stmp) / 2
    X.Print stmp
    'Fp MASSIMA ESCURSIONE di F2
    ftmp = MaxFp(1, 2) - MinFp(2, 2)
    stmp = " Fp(F2) = " & CInt(ftmp * 1000) & " "
    XX1 = (MLeft + 3) + 3 * Box2W / 4
    YY1 = MTop + 2 * Box2H / 4 - X.TextHeight(stmp) / 2
    X.Line (XX1, YY1)-(XX1 + X.TextWidth(stmp), YY1 + X.TextHeight(stmp)), , B
    X.CurrentX = XX1
    X.CurrentY = YY1
    X.Print stmp
    i = WritePrivateProfileString("CHK0", "DIVISIONE(2)", CInt(ftmp * 1000), Path_LAVORAZIONE_INI)
  End If
  '
Exit Sub

errVISUALIZZA_DIVISIONE:
  WRITE_DIALOG "SUB: VISUALIZZA_DIVISIONE ERROR " & str(Err)
  Exit Sub

End Sub

Sub VISUALIZZA_DIVISIONE_VITI(X As Control)
'
Dim Sc1X                As Single
Dim Sc1Y                As Single
Dim stmp                As String
Dim MLeft               As Single
Dim MTop                As Single
Dim XX1                 As Single
Dim XX2                 As Single
Dim YY1                 As Single
Dim YY2                 As Single
Dim YYpre               As Single
Dim YYsuc               As Single
Dim wt                  As Single
Dim w1                  As Single
Dim FasciaTolleranza(2) As Single
Dim MaxFp(2, 2)         As Single
Dim MinFp(2, 2)         As Single
Dim ftmp                As Single
Const NumeroRette = 15
Const ANGOLO = 20
Const Box2H = 7
Const Box2W = 14
Const DELTA_Y = 12
'
On Error GoTo errVISUALIZZA_DIVISIONE_VITI
  '
  'LARGHEZZA TOTALE
  wt = 1
  'LARGHEZZA LIVELLO ON
  w1 = 1
  'FASCIA DI TOLLERANZA
  FasciaTolleranza(1) = val(GetInfo("CHK0", "TOLLERANZA_DIV(1)", Path_LAVORAZIONE_INI))
  FasciaTolleranza(2) = val(GetInfo("CHK0", "TOLLERANZA_DIV(2)", Path_LAVORAZIONE_INI))
  '
  MLeft = val(GetInfo("CHK0", "MLeft", Path_LAVORAZIONE_INI))
  MTop = val(GetInfo("CHK0", "MTop", Path_LAVORAZIONE_INI))
  MTop = MTop + DELTA_Y
  stmp = GetInfo("CHK0", "ScalaDIVISIONEx", Path_LAVORAZIONE_INI): Sc1X = val(stmp)
  stmp = GetInfo("CHK0", "ScalaDIVISIONEy", Path_LAVORAZIONE_INI): Sc1Y = val(stmp)
  '
  XX1 = (MLeft + 3) - 2
  YY1 = MTop + 1 * Box2H / 4
  stmp = frmt(Sc1Y, 4) & ":1"
  X.CurrentX = XX1 - X.TextWidth(stmp) / 2
  X.CurrentY = YY1 - 0.5 - 2 * X.TextHeight(stmp)
  X.Print stmp
  X.Line (XX1 - 0.5, YY1 - 0.5)-(XX1 + 0.5, YY1 - 0.5), QBColor(9)
  'FRECCIA SUPERIORE
  ftmp = (ANGOLO / 180 * PG) / NumeroRette
  For i = -NumeroRette To NumeroRette
    XX2 = XX1 + 0.3 * Sin(i * ftmp)
    YY2 = YY1 - 0.5 - 0.25 * Cos((ANGOLO / 180 * PG))
    X.Line (XX1, YY1 - 0.5)-(XX2, YY2), QBColor(9)
  Next i
  'FRECCIA INFERIORE
  ftmp = (ANGOLO / 180 * PG) / NumeroRette
  For i = -NumeroRette To NumeroRette
    XX2 = XX1 + 0.3 * Sin(i * ftmp)
    YY2 = YY1 + 0.5 + 0.25 * Cos((ANGOLO / 180 * PG))
    X.Line (XX1, YY1 + 0.5)-(XX2, YY2), QBColor(9)
  Next i
  X.Line (XX1 - 0.5, YY1 + 0.5)-(XX1 + 0.5, YY1 + 0.5), QBColor(9)
  stmp = frmt(10 / Sc1Y, 4) & "mm"
  X.CurrentX = XX1 - X.TextWidth(stmp) / 2
  X.CurrentY = YY1 - X.TextHeight(stmp) / 2
  X.Print stmp
  'SEGNO POSITIVO
  stmp = " + "
  XX1 = (MLeft + 3)
  YY1 = MTop + 0 * Box2H / 4 - X.TextHeight(stmp) / 2
  X.Line (XX1, YY1)-(XX1 + X.TextWidth(stmp), YY1 + X.TextHeight(stmp)), , B
  X.CurrentX = XX1
  X.CurrentY = YY1
  X.Print stmp
  'NOME DEL GRAFICO
  stmp = " Fp "
  X.CurrentX = (MLeft + 3) - X.TextWidth(stmp)
  X.CurrentY = MTop + 2 * Box2H / 4 - X.TextHeight(stmp) / 2
  X.Print stmp
  'SEGNO NEGATIVO
  stmp = " - "
  XX1 = (MLeft + 3)
  YY1 = MTop + 2 * Box2H / 4 - X.TextHeight(stmp) / 2
  X.Line (XX1, YY1)-(XX1 + X.TextWidth(stmp), YY1 + X.TextHeight(stmp)), , B
  X.CurrentX = XX1
  X.CurrentY = YY1
  X.Print stmp
  'SEGNO POSITIVO
  stmp = " + "
  XX1 = (MLeft + 3)
  YY1 = MTop + 4 * Box2H / 4 - X.TextHeight(stmp) / 2
  X.Line (XX1, YY1)-(XX1 + X.TextWidth(stmp), YY1 + X.TextHeight(stmp)), , B
  X.CurrentX = XX1
  X.CurrentY = YY1
  X.Print stmp
  'GRAFICO DI CONTROLLO DIVISIONE
  If (DivCon > 0) Then
    'FIANCO 1
    XX1 = (MLeft + 3)
    YY1 = MTop + 1 * Box2H / 4
    'NOME DEL GRAFICO
    stmp = " F1 "
    X.CurrentX = XX1 - X.TextWidth(stmp)
    X.CurrentY = YY1 - X.TextHeight(stmp) / 2
    X.Print stmp
    'FASCIA DI TOLLERANZA
    If FasciaTolleranza(1) > 0 Then
      X.Line (XX1 + 0.5 * wt * Sc1X / 10, YY1 - (FasciaTolleranza(1) / 2) * Sc1Y / 10)-(XX1 + (DivCon + 0.5) * wt * Sc1X / 10, YY1 - (FasciaTolleranza(1) / 2) * Sc1Y / 10), QBColor(12)
      X.Line (XX1 + 0.5 * wt * Sc1X / 10, YY1 + (FasciaTolleranza(1) / 2) * Sc1Y / 10)-(XX1 + (DivCon + 0.5) * wt * Sc1X / 10, YY1 + (FasciaTolleranza(1) / 2) * Sc1Y / 10), QBColor(12)
      'VALORE
      stmp = CInt(FasciaTolleranza(1) * 1000)
      X.CurrentX = XX1 - X.TextWidth(stmp) / 2
      X.CurrentY = YY1 - (FasciaTolleranza(1) / 2) * Sc1Y / 10 - 0.3 - X.TextHeight(stmp)
      X.Print stmp
      'FRECCIA SUPERIORE
      ftmp = (ANGOLO / 180 * PG) / NumeroRette
      For i = -NumeroRette To NumeroRette
        XX2 = XX1 + 0.3 * Sin(i * ftmp)
        YY2 = YY1 - (FasciaTolleranza(1) / 2) * Sc1Y / 10 - 0.25 * Cos((ANGOLO / 180 * PG))
        X.Line (XX1, YY1 - (FasciaTolleranza(1) / 2) * Sc1Y / 10)-(XX2, YY2), QBColor(12)
      Next i
      X.Line (XX1 - 0.25, YY1 - (FasciaTolleranza(1) / 2) * Sc1Y / 10)-(XX1 + 0.25, YY1 - (FasciaTolleranza(1) / 2) * Sc1Y / 10), QBColor(12)
      'FRECCIA INFERIORE
      ftmp = (ANGOLO / 180 * PG) / NumeroRette
      For i = -NumeroRette To NumeroRette
        XX2 = XX1 + 0.3 * Sin(i * ftmp)
        YY2 = YY1 + (FasciaTolleranza(1) / 2) * Sc1Y / 10 + 0.25 * Cos((ANGOLO / 180 * PG))
        X.Line (XX1, YY1 + (FasciaTolleranza(1) / 2) * Sc1Y / 10)-(XX2, YY2), QBColor(12)
      Next i
      X.Line (XX1 - 0.25, YY1 + (FasciaTolleranza(1) / 2) * Sc1Y / 10)-(XX1 + 0.25, YY1 + (FasciaTolleranza(1) / 2) * Sc1Y / 10), QBColor(12)
    End If
    MaxFp(1, 1) = 1: MaxFp(1, 2) = Divf1(1)
    MinFp(2, 1) = 1: MinFp(2, 2) = Divf1(1)
    i = 1
      'Ricerca del massimo e minimo
      If MaxFp(1, 2) <= Divf1(i) Then MaxFp(1, 1) = i: MaxFp(1, 2) = Divf1(i)
      If MinFp(2, 2) >= Divf1(i) Then MinFp(2, 1) = i: MinFp(2, 2) = Divf1(i)
      'Errore divisione
      XX2 = XX1 + i * wt * Sc1X / 10
      YY2 = YY1 - Divf1(i) * Sc1Y / 10
      YYpre = YY1
      YYsuc = YY1 - Divf1(i + 1) * Sc1Y / 10
      'Livello ON : ALTEZZA RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YY2)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(9)
      'Livello OFF: LATI RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YYpre)-(XX2 - (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      X.Line (XX2 + (w1 / 2) * Sc1X / 10, YYsuc)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      'Livello OFF: BASE RETTANGOLO
      X.Line (XX2 - (wt / 2) * Sc1X / 10, YY1)-(XX2 - (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
      X.Line (XX2 + (wt / 2) * Sc1X / 10, YY1)-(XX2 + (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
    For i = 2 To DivCon - 1
      'Ricerca del massimo e minimo
      If MaxFp(1, 2) <= Divf1(i) Then MaxFp(1, 1) = i: MaxFp(1, 2) = Divf1(i)
      If MinFp(2, 2) >= Divf1(i) Then MinFp(2, 1) = i: MinFp(2, 2) = Divf1(i)
      'Errore divisione
      XX2 = XX1 + i * wt * Sc1X / 10
      YY2 = YY1 - Divf1(i) * Sc1Y / 10
      YYpre = YY1 - Divf1(i - 1) * Sc1Y / 10
      YYsuc = YY1 - Divf1(i + 1) * Sc1Y / 10
      'Livello ON : ALTEZZA RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YY2)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(9)
      'Livello OFF: LATI RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YYpre)-(XX2 - (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      X.Line (XX2 + (w1 / 2) * Sc1X / 10, YYsuc)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      'Livello OFF: BASE RETTANGOLO
      X.Line (XX2 - (wt / 2) * Sc1X / 10, YY1)-(XX2 - (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
      X.Line (XX2 + (wt / 2) * Sc1X / 10, YY1)-(XX2 + (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
    Next i
    i = DivCon
      'Ricerca del massimo e minimo
      If MaxFp(1, 2) <= Divf1(i) Then MaxFp(1, 1) = i: MaxFp(1, 2) = Divf1(i)
      If MinFp(2, 2) >= Divf1(i) Then MinFp(2, 1) = i: MinFp(2, 2) = Divf1(i)
      'Errore divisione
      XX2 = XX1 + i * wt * Sc1X / 10
      YY2 = YY1 - Divf1(i) * Sc1Y / 10
      YYpre = YY1 - Divf1(i - 1) * Sc1Y / 10
      YYsuc = YY1
      'Livello ON : ALTEZZA RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YY2)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(9)
      'Livello OFF: LATI RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YYpre)-(XX2 - (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      X.Line (XX2 + (w1 / 2) * Sc1X / 10, YYsuc)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      'Livello OFF: BASE RETTANGOLO
      X.Line (XX2 - (wt / 2) * Sc1X / 10, YY1)-(XX2 - (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
      X.Line (XX2 + (wt / 2) * Sc1X / 10, YY1)-(XX2 + (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
    'Fp MASSIMO di F1
    stmp = "Fp Max = " & CInt(MaxFp(1, 2) * 1000) & "/" & Format$(MaxFp(1, 1), "##0")
    X.CurrentX = (MLeft + 3) + 1 * Box2W / 4
    X.CurrentY = MTop + 0 * Box2H / 4 - X.TextHeight(stmp) / 2
    X.Print stmp
    'Fp MINIMO di F1
    stmp = "Fp Min = " & CInt(MinFp(2, 2) * 1000) & "/" & Format$(MinFp(2, 1), "##0")
    X.CurrentX = (MLeft + 3) + 3 * Box2W / 4
    X.CurrentY = MTop + 0 * Box2H / 4 - X.TextHeight(stmp) / 2
    X.Print stmp
    'Fp MASSIMA ESCURSIONE di F1
    ftmp = MaxFp(1, 2) - MinFp(2, 2)
    stmp = " Fp(F1) = " & CInt(ftmp * 1000) & " "
    XX1 = (MLeft + 3) + 1 * Box2W / 4
    YY1 = MTop + 2 * Box2H / 4 - X.TextHeight(stmp) / 2
    X.Line (XX1, YY1)-(XX1 + X.TextWidth(stmp), YY1 + X.TextHeight(stmp)), , B
    X.CurrentX = XX1
    X.CurrentY = YY1
    X.Print stmp
    i = WritePrivateProfileString("CHK0", "DIVISIONE(1)", CInt(ftmp * 1000), Path_LAVORAZIONE_INI)
    'FIANCO 2
    XX1 = (MLeft + 3)
    YY1 = MTop + 3 * Box2H / 4
    'NOME DEL GRAFICO
    stmp = " F2 "
    X.CurrentX = XX1 - X.TextWidth(stmp)
    X.CurrentY = YY1 - X.TextHeight(stmp) / 2
    X.Print stmp
    'FASCIA DI TOLLERANZA
    If FasciaTolleranza(2) > 0 Then
      X.Line (XX1 + 0.5 * wt * Sc1X / 10, YY1 - (FasciaTolleranza(2) / 2) * Sc1Y / 10)-(XX1 + (DivCon + 0.5) * wt * Sc1X / 10, YY1 - (FasciaTolleranza(2) / 2) * Sc1Y / 10), QBColor(12)
      X.Line (XX1 + 0.5 * wt * Sc1X / 10, YY1 + (FasciaTolleranza(2) / 2) * Sc1Y / 10)-(XX1 + (DivCon + 0.5) * wt * Sc1X / 10, YY1 + (FasciaTolleranza(2) / 2) * Sc1Y / 10), QBColor(12)
      'VALORE
      stmp = CInt(FasciaTolleranza(2) * 1000)
      X.CurrentX = XX1 - X.TextWidth(stmp) / 2
      X.CurrentY = YY1 - (FasciaTolleranza(2) / 2) * Sc1Y / 10 - 0.3 - X.TextHeight(stmp)
      X.Print stmp
      'FRECCIA SUPERIORE
      ftmp = (ANGOLO / 180 * PG) / NumeroRette
      For i = -NumeroRette To NumeroRette
        XX2 = XX1 + 0.3 * Sin(i * ftmp)
        YY2 = YY1 - (FasciaTolleranza(2) / 2) * Sc1Y / 10 - 0.25 * Cos((ANGOLO / 180 * PG))
        X.Line (XX1, YY1 - (FasciaTolleranza(2) / 2) * Sc1Y / 10)-(XX2, YY2), QBColor(12)
      Next i
      X.Line (XX1 - 0.25, YY1 - (FasciaTolleranza(2) / 2) * Sc1Y / 10)-(XX1 + 0.25, YY1 - (FasciaTolleranza(2) / 2) * Sc1Y / 10), QBColor(12)
      'FRECCIA INFERIORE
      ftmp = (ANGOLO / 180 * PG) / NumeroRette
      For i = -NumeroRette To NumeroRette
        XX2 = XX1 + 0.3 * Sin(i * ftmp)
        YY2 = YY1 + (FasciaTolleranza(2) / 2) * Sc1Y / 10 + 0.25 * Cos((ANGOLO / 180 * PG))
        X.Line (XX1, YY1 + (FasciaTolleranza(2) / 2) * Sc1Y / 10)-(XX2, YY2), QBColor(12)
      Next i
      X.Line (XX1 - 0.25, YY1 + (FasciaTolleranza(2) / 2) * Sc1Y / 10)-(XX1 + 0.25, YY1 + (FasciaTolleranza(2) / 2) * Sc1Y / 10), QBColor(12)
    End If
    MaxFp(1, 1) = 1: MaxFp(1, 2) = Divf2(1)
    MinFp(2, 1) = 1: MinFp(2, 2) = Divf2(1)
    i = 1
      'Ricerca del massimo e minimo
      If MaxFp(1, 2) <= Divf2(i) Then MaxFp(1, 1) = i: MaxFp(1, 2) = Divf2(i)
      If MinFp(2, 2) >= Divf2(i) Then MinFp(2, 1) = i: MinFp(2, 2) = Divf2(i)
      'Errore divisione
      XX2 = XX1 + i * wt * Sc1X / 10
      YY2 = YY1 + Divf2(i) * Sc1Y / 10
      YYpre = YY1
      YYsuc = YY1 + Divf2(i + 1) * Sc1Y / 10
      'Livello ON : ALTEZZA RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YY2)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(9)
      'Livello OFF: LATI RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YYpre)-(XX2 - (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      X.Line (XX2 + (w1 / 2) * Sc1X / 10, YYsuc)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      'Livello OFF: BASE RETTANGOLO
      X.Line (XX2 - (wt / 2) * Sc1X / 10, YY1)-(XX2 - (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
      X.Line (XX2 + (wt / 2) * Sc1X / 10, YY1)-(XX2 + (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
    For i = 2 To DivCon - 1
      'Ricerca del massimo e minimo
      If MaxFp(1, 2) <= Divf2(i) Then MaxFp(1, 1) = i: MaxFp(1, 2) = Divf2(i)
      If MinFp(2, 2) >= Divf2(i) Then MinFp(2, 1) = i: MinFp(2, 2) = Divf2(i)
      'Errore divisione
      XX2 = XX1 + i * wt * Sc1X / 10
      YY2 = YY1 + Divf2(i) * Sc1Y / 10
      YYpre = YY1 + Divf2(i - 1) * Sc1Y / 10
      YYsuc = YY1 + Divf2(i + 1) * Sc1Y / 10
      'Livello ON : ALTEZZA RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YY2)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(9)
      'Livello OFF: LATI RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YYpre)-(XX2 - (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      X.Line (XX2 + (w1 / 2) * Sc1X / 10, YYsuc)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      'Livello OFF: BASE RETTANGOLO
      X.Line (XX2 - (wt / 2) * Sc1X / 10, YY1)-(XX2 - (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
      X.Line (XX2 + (wt / 2) * Sc1X / 10, YY1)-(XX2 + (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
    Next i
    i = DivCon
      'Ricerca del massimo e minimo
      If MaxFp(1, 2) <= Divf2(i) Then MaxFp(1, 1) = i: MaxFp(1, 2) = Divf2(i)
      If MinFp(2, 2) >= Divf2(i) Then MinFp(2, 1) = i: MinFp(2, 2) = Divf2(i)
      'Errore divisione
      XX2 = XX1 + i * wt * Sc1X / 10
      YY2 = YY1 + Divf2(i) * Sc1Y / 10
      YYpre = YY1 + Divf2(i - 1) * Sc1Y / 10
      YYsuc = YY1
      'Livello ON : ALTEZZA RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YY2)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(9)
      'Livello OFF: LATI RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YYpre)-(XX2 - (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      X.Line (XX2 + (w1 / 2) * Sc1X / 10, YYsuc)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      'Livello OFF: BASE RETTANGOLO
      X.Line (XX2 - (wt / 2) * Sc1X / 10, YY1)-(XX2 - (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
      X.Line (XX2 + (wt / 2) * Sc1X / 10, YY1)-(XX2 + (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
    'Fp MASSIMO di F2
    stmp = "Fp Max = " & CInt(MaxFp(1, 2) * 1000) & "/" & Format$(MaxFp(1, 1), "##0")
    X.CurrentX = (MLeft + 3) + 1 * Box2W / 4
    X.CurrentY = MTop + 4 * Box2H / 4 - X.TextHeight(stmp) / 2
    X.Print stmp
    'Fp MINIMO di F2
    stmp = "Fp Min = " & CInt(MinFp(2, 2) * 1000) & "/" & Format$(MinFp(2, 1), "##0")
    X.CurrentX = (MLeft + 3) + 3 * Box2W / 4
    X.CurrentY = MTop + 4 * Box2H / 4 - X.TextHeight(stmp) / 2
    X.Print stmp
    'Fp MASSIMA ESCURSIONE di F2
    ftmp = MaxFp(1, 2) - MinFp(2, 2)
    stmp = " Fp(F2) = " & CInt(ftmp * 1000) & " "
    XX1 = (MLeft + 3) + 3 * Box2W / 4
    YY1 = MTop + 2 * Box2H / 4 - X.TextHeight(stmp) / 2
    X.Line (XX1, YY1)-(XX1 + X.TextWidth(stmp), YY1 + X.TextHeight(stmp)), , B
    X.CurrentX = XX1
    X.CurrentY = YY1
    X.Print stmp
    i = WritePrivateProfileString("CHK0", "DIVISIONE(2)", CInt(ftmp * 1000), Path_LAVORAZIONE_INI)
  End If
  '
Exit Sub

errVISUALIZZA_DIVISIONE_VITI:
  WRITE_DIALOG "SUB: VISUALIZZA_DIVISIONE_VITI ERROR " & str(Err)
  Exit Sub

End Sub

Sub VISUALIZZA_CONCENTRICITA(X As Control)
'
Dim Sc1X              As Single
Dim Sc1Y              As Single
Dim stmp              As String
Dim MLeft             As Single
Dim MTop              As Single
Dim XX1               As Single
Dim XX2               As Single
Dim YY1               As Single
Dim YY2               As Single
Dim wt                As Single
Dim w1                As Single
Dim FasciaTolleranza  As Single
Dim iMax              As Integer
Dim vMax              As Single
Dim iMIN              As Integer
Dim vMin              As Single
Dim ftmp              As Single
Const NumeroRette = 15
Const ANGOLO = 20
Const Box2H = 4
Const Box2W = 14
Const DELTA_Y = 20
'
On Error GoTo errVISUALIZZA_CONCENTRICITA
  '
  'LARGHEZZA TOTALE
  wt = 1
  'LARGHEZZA LIVELLO ON
  w1 = 1
  'FASCIA DI TOLLERANZA
  FasciaTolleranza = val(GetInfo("CHK0", "TOLLERANZA_CON", Path_LAVORAZIONE_INI))
  '
  MLeft = val(GetInfo("CHK0", "MLeft", Path_LAVORAZIONE_INI))
  MTop = val(GetInfo("CHK0", "MTop", Path_LAVORAZIONE_INI))
  MTop = MTop + DELTA_Y
  stmp = GetInfo("CHK0", "ScalaDIVISIONEx", Path_LAVORAZIONE_INI): Sc1X = val(stmp)
  stmp = GetInfo("CHK0", "ScalaDIVISIONEy", Path_LAVORAZIONE_INI): Sc1Y = val(stmp)
  '
  XX1 = (MLeft + 3) - 2
  YY1 = MTop + 1 * Box2H / 4
  stmp = frmt(Sc1Y, 4) & ":1"
  X.CurrentX = XX1 - X.TextWidth(stmp) / 2
  X.CurrentY = YY1 - 0.5 - 2 * X.TextHeight(stmp)
  X.Print stmp
  X.Line (XX1 - 0.5, YY1 - 0.5)-(XX1 + 0.5, YY1 - 0.5), QBColor(9)
  'FRECCIA SUPERIORE
  ftmp = (ANGOLO / 180 * PG) / NumeroRette
  For i = -NumeroRette To NumeroRette
    XX2 = XX1 + 0.3 * Sin(i * ftmp)
    YY2 = YY1 - 0.5 - 0.25 * Cos((ANGOLO / 180 * PG))
    X.Line (XX1, YY1 - 0.5)-(XX2, YY2), QBColor(9)
  Next i
  'FRECCIA INFERIORE
  ftmp = (ANGOLO / 180 * PG) / NumeroRette
  For i = -NumeroRette To NumeroRette
    XX2 = XX1 + 0.3 * Sin(i * ftmp)
    YY2 = YY1 + 0.5 + 0.25 * Cos((ANGOLO / 180 * PG))
    X.Line (XX1, YY1 + 0.5)-(XX2, YY2), QBColor(9)
  Next i
  X.Line (XX1 - 0.5, YY1 + 0.5)-(XX1 + 0.5, YY1 + 0.5), QBColor(9)
  stmp = frmt(10 / Sc1Y, 4) & "mm"
  X.CurrentX = XX1 - X.TextWidth(stmp) / 2
  X.CurrentY = YY1 - X.TextHeight(stmp) / 2
  X.Print stmp
  'SEGNO POSITIVO
  stmp = " + "
  XX1 = (MLeft + 3)
  YY1 = MTop + 0 * Box2H / 4 - X.TextHeight(stmp) / 2
  X.Line (XX1, YY1)-(XX1 + X.TextWidth(stmp), YY1 + X.TextHeight(stmp)), , B
  X.CurrentX = XX1
  X.CurrentY = YY1
  X.Print stmp
  'NOME DEL GRAFICO
  stmp = " Fr "
  X.CurrentX = (MLeft + 3) - X.TextWidth(stmp)
  X.CurrentY = MTop + 2 * Box2H / 4 - X.TextHeight(stmp) / 2
  X.Print stmp
  'SEGNO NEGATIVO
  stmp = " - "
  XX1 = (MLeft + 3)
  YY1 = MTop + 4 * Box2H / 4 - X.TextHeight(stmp) / 2
  X.Line (XX1, YY1)-(XX1 + X.TextWidth(stmp), YY1 + X.TextHeight(stmp)), , B
  X.CurrentX = XX1
  X.CurrentY = YY1
  X.Print stmp
  'GRAFICO CONCENTRICITA
  If DivCon = 1 Then
    XX1 = (MLeft + 3)
    YY1 = MTop + 2 * Box2H / 4
    'FASCIA DI TOLLERANZA
    If FasciaTolleranza > 0 Then
      X.Line (XX1 + 0.5 * wt * Sc1X / 10, YY1 - (FasciaTolleranza / 2) * Sc1Y / 10)-(XX1 + (NZ + 0.5) * wt * Sc1X / 10, YY1 - (FasciaTolleranza / 2) * Sc1Y / 10), QBColor(12)
      X.Line (XX1 + 0.5 * wt * Sc1X / 10, YY1 + (FasciaTolleranza / 2) * Sc1Y / 10)-(XX1 + (NZ + 0.5) * wt * Sc1X / 10, YY1 + (FasciaTolleranza / 2) * Sc1Y / 10), QBColor(12)
      'VALORE
      stmp = CInt(FasciaTolleranza * 1000)
      X.CurrentX = XX1 - X.TextWidth(stmp) / 2
      X.CurrentY = YY1 - (FasciaTolleranza / 2) * Sc1Y / 10 - 0.3 - X.TextHeight(stmp)
      X.Print stmp
      'FRECCIA SUPERIORE
      ftmp = (ANGOLO / 180 * PG) / NumeroRette
      For i = -NumeroRette To NumeroRette
        XX2 = XX1 + 0.3 * Sin(i * ftmp)
        YY2 = YY1 - (FasciaTolleranza / 2) * Sc1Y / 10 - 0.25 * Cos((ANGOLO / 180 * PG))
        X.Line (XX1, YY1 - (FasciaTolleranza / 2) * Sc1Y / 10)-(XX2, YY2), QBColor(12)
      Next i
      X.Line (XX1 - 0.25, YY1 - (FasciaTolleranza / 2) * Sc1Y / 10)-(XX1 + 0.25, YY1 - (FasciaTolleranza / 2) * Sc1Y / 10), QBColor(12)
      'FRECCIA INFERIORE
      ftmp = (ANGOLO / 180 * PG) / NumeroRette
      For i = -NumeroRette To NumeroRette
        XX2 = XX1 + 0.3 * Sin(i * ftmp)
        YY2 = YY1 + (FasciaTolleranza / 2) * Sc1Y / 10 + 0.25 * Cos((ANGOLO / 180 * PG))
        X.Line (XX1, YY1 + (FasciaTolleranza / 2) * Sc1Y / 10)-(XX2, YY2), QBColor(12)
      Next i
      X.Line (XX1 - 0.25, YY1 + (FasciaTolleranza / 2) * Sc1Y / 10)-(XX1 + 0.25, YY1 + (FasciaTolleranza / 2) * Sc1Y / 10), QBColor(12)
    End If
    'RIFERIMENTO 0
    X.Line (XX1 + 0.5 * wt * Sc1X / 10, YY1)-(XX1 + (NZ + 0.5) * wt * Sc1X / 10, YY1), QBColor(0)
    iMax = 1: vMax = Divf1(1) + Divf2(1)
    iMIN = 1: vMin = Divf1(1) + Divf2(1)
    For i = 1 To NZ
      'Ricerca del massimo e minimo
      ftmp = (Divf1(i) + Divf2(i))
      If vMax <= ftmp Then vMax = ftmp: iMax = i
      If vMin >= ftmp Then vMin = ftmp: iMIN = i
      'Errore divisione
      XX2 = XX1 + i * wt * Sc1X / 10
      YY2 = YY1 - ftmp * Sc1Y / 10
      'Livello ON : ALTEZZA RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YY2)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(9)
      'Livello OFF: LATI RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YY1)-(XX2 - (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      X.Line (XX2 + (w1 / 2) * Sc1X / 10, YY1)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      'Livello OFF: BASE RETTANGOLO
      X.Line (XX2 - (wt / 2) * Sc1X / 10, YY1)-(XX2 - (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
      X.Line (XX2 + (wt / 2) * Sc1X / 10, YY1)-(XX2 + (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
    Next i
    'MASSIMO di F1 + F2
    stmp = "Fr Max = " & CInt(vMax * 1000) & "/" & Format$(iMax, "##0")
    X.CurrentX = (MLeft + 3) + 1 * Box2W / 4
    X.CurrentY = MTop + 0 * Box2H / 4 - X.TextHeight(stmp) / 2
    X.Print stmp
    'MINIMO di F1 + F2
    stmp = "Fr Min = " & CInt(vMin * 1000) & "/" & Format$(iMIN, "##0")
    X.CurrentX = (MLeft + 3) + 3 * Box2W / 4
    X.CurrentY = MTop + 0 * Box2H / 4 - X.TextHeight(stmp) / 2
    X.Print stmp
    'ESCURSIONE MASSIMA di F1 + F2
    stmp = " Fr = " & CInt((vMax - vMin) * 1000) & " "
    XX1 = (MLeft + 3) + 2 * Box2W / 4
    YY1 = MTop + 4 * Box2H / 4 - X.TextHeight(stmp) / 2
    X.Line (XX1, YY1)-(XX1 + X.TextWidth(stmp), YY1 + X.TextHeight(stmp)), , B
    X.CurrentX = XX1
    X.CurrentY = YY1
    X.Print stmp
    i = WritePrivateProfileString("CHK0", "Fr", CInt((vMax - vMin) * 1000), Path_LAVORAZIONE_INI)
  End If

Exit Sub

errVISUALIZZA_CONCENTRICITA:
  WRITE_DIALOG "SUB: VISUALIZZA_CONCENTRICITA ERROR " & str(Err)
  Exit Sub

End Sub

Sub VISUALIZZA_CONCENTRICITA_VITI(X As Control)
'
Dim Sc1X              As Single
Dim Sc1Y              As Single
Dim stmp              As String
Dim MLeft             As Single
Dim MTop              As Single
Dim XX1               As Single
Dim XX2               As Single
Dim YY1               As Single
Dim YY2               As Single
Dim wt                As Single
Dim w1                As Single
Dim FasciaTolleranza  As Single
Dim iMax              As Integer
Dim vMax              As Single
Dim iMIN              As Integer
Dim vMin              As Single
Dim ftmp              As Single
Const NumeroRette = 15
Const ANGOLO = 20
Const Box2H = 4
Const Box2W = 14
Const DELTA_Y = 20
'
On Error GoTo errVISUALIZZA_CONCENTRICITA_VITI
  '
  'LARGHEZZA TOTALE
  wt = 1
  'LARGHEZZA LIVELLO ON
  w1 = 1
  'FASCIA DI TOLLERANZA
  FasciaTolleranza = val(GetInfo("CHK0", "TOLLERANZA_CON", Path_LAVORAZIONE_INI))
  '
  MLeft = val(GetInfo("CHK0", "MLeft", Path_LAVORAZIONE_INI))
  MTop = val(GetInfo("CHK0", "MTop", Path_LAVORAZIONE_INI))
  MTop = MTop + DELTA_Y
  stmp = GetInfo("CHK0", "ScalaDIVISIONEx", Path_LAVORAZIONE_INI): Sc1X = val(stmp)
  stmp = GetInfo("CHK0", "ScalaDIVISIONEy", Path_LAVORAZIONE_INI): Sc1Y = val(stmp)
  '
  XX1 = (MLeft + 3) - 2
  YY1 = MTop + 1 * Box2H / 4
  stmp = frmt(Sc1Y, 4) & ":1"
  X.CurrentX = XX1 - X.TextWidth(stmp) / 2
  X.CurrentY = YY1 - 0.5 - 2 * X.TextHeight(stmp)
  X.Print stmp
  X.Line (XX1 - 0.5, YY1 - 0.5)-(XX1 + 0.5, YY1 - 0.5), QBColor(9)
  'FRECCIA SUPERIORE
  ftmp = (ANGOLO / 180 * PG) / NumeroRette
  For i = -NumeroRette To NumeroRette
    XX2 = XX1 + 0.3 * Sin(i * ftmp)
    YY2 = YY1 - 0.5 - 0.25 * Cos((ANGOLO / 180 * PG))
    X.Line (XX1, YY1 - 0.5)-(XX2, YY2), QBColor(9)
  Next i
  'FRECCIA INFERIORE
  ftmp = (ANGOLO / 180 * PG) / NumeroRette
  For i = -NumeroRette To NumeroRette
    XX2 = XX1 + 0.3 * Sin(i * ftmp)
    YY2 = YY1 + 0.5 + 0.25 * Cos((ANGOLO / 180 * PG))
    X.Line (XX1, YY1 + 0.5)-(XX2, YY2), QBColor(9)
  Next i
  X.Line (XX1 - 0.5, YY1 + 0.5)-(XX1 + 0.5, YY1 + 0.5), QBColor(9)
  stmp = frmt(10 / Sc1Y, 4) & "mm"
  X.CurrentX = XX1 - X.TextWidth(stmp) / 2
  X.CurrentY = YY1 - X.TextHeight(stmp) / 2
  X.Print stmp
  'SEGNO POSITIVO
  stmp = " + "
  XX1 = (MLeft + 3)
  YY1 = MTop + 0 * Box2H / 4 - X.TextHeight(stmp) / 2
  X.Line (XX1, YY1)-(XX1 + X.TextWidth(stmp), YY1 + X.TextHeight(stmp)), , B
  X.CurrentX = XX1
  X.CurrentY = YY1
  X.Print stmp
  'NOME DEL GRAFICO
  stmp = " Fr "
  X.CurrentX = (MLeft + 3) - X.TextWidth(stmp)
  X.CurrentY = MTop + 2 * Box2H / 4 - X.TextHeight(stmp) / 2
  X.Print stmp
  'SEGNO NEGATIVO
  stmp = " - "
  XX1 = (MLeft + 3)
  YY1 = MTop + 4 * Box2H / 4 - X.TextHeight(stmp) / 2
  X.Line (XX1, YY1)-(XX1 + X.TextWidth(stmp), YY1 + X.TextHeight(stmp)), , B
  X.CurrentX = XX1
  X.CurrentY = YY1
  X.Print stmp
  'GRAFICO CONCENTRICITA
  If (DivCon > 0) Then
    XX1 = (MLeft + 3)
    YY1 = MTop + 2 * Box2H / 4
    'FASCIA DI TOLLERANZA
    If FasciaTolleranza > 0 Then
      X.Line (XX1 + 0.5 * wt * Sc1X / 10, YY1 - (FasciaTolleranza / 2) * Sc1Y / 10)-(XX1 + (DivCon + 0.5) * wt * Sc1X / 10, YY1 - (FasciaTolleranza / 2) * Sc1Y / 10), QBColor(12)
      X.Line (XX1 + 0.5 * wt * Sc1X / 10, YY1 + (FasciaTolleranza / 2) * Sc1Y / 10)-(XX1 + (DivCon + 0.5) * wt * Sc1X / 10, YY1 + (FasciaTolleranza / 2) * Sc1Y / 10), QBColor(12)
      'VALORE
      stmp = CInt(FasciaTolleranza * 1000)
      X.CurrentX = XX1 - X.TextWidth(stmp) / 2
      X.CurrentY = YY1 - (FasciaTolleranza / 2) * Sc1Y / 10 - 0.3 - X.TextHeight(stmp)
      X.Print stmp
      'FRECCIA SUPERIORE
      ftmp = (ANGOLO / 180 * PG) / NumeroRette
      For i = -NumeroRette To NumeroRette
        XX2 = XX1 + 0.3 * Sin(i * ftmp)
        YY2 = YY1 - (FasciaTolleranza / 2) * Sc1Y / 10 - 0.25 * Cos((ANGOLO / 180 * PG))
        X.Line (XX1, YY1 - (FasciaTolleranza / 2) * Sc1Y / 10)-(XX2, YY2), QBColor(12)
      Next i
      X.Line (XX1 - 0.25, YY1 - (FasciaTolleranza / 2) * Sc1Y / 10)-(XX1 + 0.25, YY1 - (FasciaTolleranza / 2) * Sc1Y / 10), QBColor(12)
      'FRECCIA INFERIORE
      ftmp = (ANGOLO / 180 * PG) / NumeroRette
      For i = -NumeroRette To NumeroRette
        XX2 = XX1 + 0.3 * Sin(i * ftmp)
        YY2 = YY1 + (FasciaTolleranza / 2) * Sc1Y / 10 + 0.25 * Cos((ANGOLO / 180 * PG))
        X.Line (XX1, YY1 + (FasciaTolleranza / 2) * Sc1Y / 10)-(XX2, YY2), QBColor(12)
      Next i
      X.Line (XX1 - 0.25, YY1 + (FasciaTolleranza / 2) * Sc1Y / 10)-(XX1 + 0.25, YY1 + (FasciaTolleranza / 2) * Sc1Y / 10), QBColor(12)
    End If
    'RIFERIMENTO 0
    X.Line (XX1 + 0.5 * wt * Sc1X / 10, YY1)-(XX1 + (DivCon + 0.5) * wt * Sc1X / 10, YY1), QBColor(0)
    iMax = 1: vMax = Divf1(1) + Divf2(1)
    iMIN = 1: vMin = Divf1(1) + Divf2(1)
    For i = 1 To DivCon
      'Ricerca del massimo e minimo
      ftmp = (Divf1(i) + Divf2(i))
      If vMax <= ftmp Then vMax = ftmp: iMax = i
      If vMin >= ftmp Then vMin = ftmp: iMIN = i
      'Errore divisione
      XX2 = XX1 + i * wt * Sc1X / 10
      YY2 = YY1 - ftmp * Sc1Y / 10
      'Livello ON : ALTEZZA RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YY2)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(9)
      'Livello OFF: LATI RETTANGOLO
      X.Line (XX2 - (w1 / 2) * Sc1X / 10, YY1)-(XX2 - (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      X.Line (XX2 + (w1 / 2) * Sc1X / 10, YY1)-(XX2 + (w1 / 2) * Sc1X / 10, YY2), QBColor(0)
      'Livello OFF: BASE RETTANGOLO
      X.Line (XX2 - (wt / 2) * Sc1X / 10, YY1)-(XX2 - (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
      X.Line (XX2 + (wt / 2) * Sc1X / 10, YY1)-(XX2 + (w1 / 2) * Sc1X / 10, YY1), QBColor(9)
    Next i
    'MASSIMO di F1 + F2
    stmp = "Fr Max = " & CInt(vMax * 1000) & "/" & Format$(iMax, "##0")
    X.CurrentX = (MLeft + 3) + 1 * Box2W / 4
    X.CurrentY = MTop + 0 * Box2H / 4 - X.TextHeight(stmp) / 2
    X.Print stmp
    'MINIMO di F1 + F2
    stmp = "Fr Min = " & CInt(vMin * 1000) & "/" & Format$(iMIN, "##0")
    X.CurrentX = (MLeft + 3) + 3 * Box2W / 4
    X.CurrentY = MTop + 0 * Box2H / 4 - X.TextHeight(stmp) / 2
    X.Print stmp
    'ESCURSIONE MASSIMA di F1 + F2
    stmp = " Fr = " & CInt((vMax - vMin) * 1000) & " "
    XX1 = (MLeft + 3) + 2 * Box2W / 4
    YY1 = MTop + 4 * Box2H / 4 - X.TextHeight(stmp) / 2
    X.Line (XX1, YY1)-(XX1 + X.TextWidth(stmp), YY1 + X.TextHeight(stmp)), , B
    X.CurrentX = XX1
    X.CurrentY = YY1
    X.Print stmp
    i = WritePrivateProfileString("CHK0", "Fr", CInt((vMax - vMin) * 1000), Path_LAVORAZIONE_INI)
  End If

Exit Sub

errVISUALIZZA_CONCENTRICITA_VITI:
  WRITE_DIALOG "SUB: VISUALIZZA_CONCENTRICITA_VITI ERROR " & str(Err)
  Resume Next
  'Exit Sub

End Sub
