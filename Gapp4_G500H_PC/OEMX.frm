VERSION 5.00
Object = "{F220A985-D80A-11D5-BA79-00105AAFF7A0}#1.0#0"; "ListBoxMod.dll"
Object = "{0BA686C6-F7D3-101A-993E-0000C0EF6F5E}#1.0#0"; "threed32.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form OEMX 
   Appearance      =   0  'Flat
   BackColor       =   &H00EBE1D7&
   BorderStyle     =   0  'None
   Caption         =   "OEMX"
   ClientHeight    =   15264
   ClientLeft      =   1716
   ClientTop       =   1956
   ClientWidth     =   33000
   ForeColor       =   &H80000008&
   Icon            =   "OEMX.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   15264
   ScaleWidth      =   33000
   ShowInTaskbar   =   0   'False
   Tag             =   "OEMX"
   Visible         =   0   'False
   Begin VB.Timer Timer2 
      Enabled         =   0   'False
      Left            =   10320
      Top             =   10440
   End
   Begin VB.TextBox MNG0EditorTesto 
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   10.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1800
      Left            =   2640
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   117
      Text            =   "OEMX.frx":030A
      Top             =   12960
      Visible         =   0   'False
      Width           =   2655
   End
   Begin VB.Frame MNG0fraVOITH 
      Caption         =   "Safety Control"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.2
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4572
      Left            =   5640
      TabIndex        =   148
      Top             =   1680
      Visible         =   0   'False
      Width           =   5040
      Begin VB.CheckBox chkV2 
         Caption         =   "chkV2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   10.8
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Index           =   9
         Left            =   2520
         TabIndex        =   168
         TabStop         =   0   'False
         Top             =   4000
         Visible         =   0   'False
         Width           =   2000
      End
      Begin VB.CheckBox chkV2 
         Caption         =   "chkV2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   10.8
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Index           =   8
         Left            =   2520
         TabIndex        =   167
         TabStop         =   0   'False
         Top             =   3600
         Visible         =   0   'False
         Width           =   2000
      End
      Begin VB.CheckBox chkV2 
         Caption         =   "chkV2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   10.8
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Index           =   7
         Left            =   2520
         TabIndex        =   166
         TabStop         =   0   'False
         Top             =   3200
         Visible         =   0   'False
         Width           =   2000
      End
      Begin VB.CheckBox chkV2 
         Caption         =   "chkV2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   10.8
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Index           =   6
         Left            =   2520
         TabIndex        =   165
         TabStop         =   0   'False
         Top             =   2800
         Visible         =   0   'False
         Width           =   2000
      End
      Begin VB.CheckBox chkV2 
         Caption         =   "chkV2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   10.8
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Index           =   5
         Left            =   2520
         TabIndex        =   164
         TabStop         =   0   'False
         Top             =   2400
         Visible         =   0   'False
         Width           =   2000
      End
      Begin VB.CheckBox chkV2 
         Caption         =   "chkV2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   10.8
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Index           =   4
         Left            =   2520
         TabIndex        =   163
         TabStop         =   0   'False
         Top             =   2000
         Visible         =   0   'False
         Width           =   2000
      End
      Begin VB.CheckBox chkV2 
         Caption         =   "chkV2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   10.8
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Index           =   3
         Left            =   2520
         TabIndex        =   162
         TabStop         =   0   'False
         Top             =   1600
         Visible         =   0   'False
         Width           =   2000
      End
      Begin VB.CheckBox chkV2 
         Caption         =   "chkV2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   10.8
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Index           =   2
         Left            =   2520
         TabIndex        =   161
         TabStop         =   0   'False
         Top             =   1200
         Visible         =   0   'False
         Width           =   2000
      End
      Begin VB.CheckBox chkV2 
         Caption         =   "chkV2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   10.8
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Index           =   1
         Left            =   2520
         TabIndex        =   160
         TabStop         =   0   'False
         Top             =   800
         Visible         =   0   'False
         Width           =   2000
      End
      Begin VB.CheckBox chkV2 
         Caption         =   "chkV2"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   10.8
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Index           =   0
         Left            =   2520
         TabIndex        =   159
         TabStop         =   0   'False
         Top             =   400
         Visible         =   0   'False
         Width           =   2000
      End
      Begin VB.CheckBox chkV1 
         Caption         =   "chkV1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   10.8
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Index           =   9
         Left            =   120
         TabIndex        =   158
         TabStop         =   0   'False
         Top             =   4000
         Visible         =   0   'False
         Width           =   2000
      End
      Begin VB.CheckBox chkV1 
         Caption         =   "chkV1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   10.8
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Index           =   8
         Left            =   120
         TabIndex        =   157
         TabStop         =   0   'False
         Top             =   3600
         Visible         =   0   'False
         Width           =   2000
      End
      Begin VB.CheckBox chkV1 
         Caption         =   "chkV1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   10.8
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Index           =   7
         Left            =   120
         TabIndex        =   156
         TabStop         =   0   'False
         Top             =   3200
         Visible         =   0   'False
         Width           =   2000
      End
      Begin VB.CheckBox chkV1 
         Caption         =   "chkV1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   10.8
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Index           =   6
         Left            =   120
         TabIndex        =   155
         TabStop         =   0   'False
         Top             =   2800
         Visible         =   0   'False
         Width           =   2000
      End
      Begin VB.CheckBox chkV1 
         Caption         =   "chkV1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   10.8
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Index           =   5
         Left            =   120
         TabIndex        =   154
         TabStop         =   0   'False
         Top             =   2400
         Visible         =   0   'False
         Width           =   2000
      End
      Begin VB.CheckBox chkV1 
         Caption         =   "chkV1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   10.8
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Index           =   4
         Left            =   120
         TabIndex        =   153
         TabStop         =   0   'False
         Top             =   2000
         Visible         =   0   'False
         Width           =   2000
      End
      Begin VB.CheckBox chkV1 
         Caption         =   "chkV1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   10.8
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Index           =   3
         Left            =   120
         TabIndex        =   152
         TabStop         =   0   'False
         Top             =   1600
         Visible         =   0   'False
         Width           =   2000
      End
      Begin VB.CheckBox chkV1 
         Caption         =   "chkV1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   10.8
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Index           =   2
         Left            =   120
         TabIndex        =   151
         TabStop         =   0   'False
         Top             =   1200
         Visible         =   0   'False
         Width           =   2000
      End
      Begin VB.CheckBox chkV1 
         Caption         =   "chkV1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   10.8
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Index           =   1
         Left            =   120
         TabIndex        =   150
         TabStop         =   0   'False
         Top             =   800
         Visible         =   0   'False
         Width           =   2000
      End
      Begin VB.CheckBox chkV1 
         Caption         =   "chkV1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   10.8
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Index           =   0
         Left            =   120
         TabIndex        =   149
         TabStop         =   0   'False
         Top             =   400
         Visible         =   0   'False
         Width           =   2000
      End
   End
   Begin VB.Frame MIX1Pannello 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBE1D7&
      Caption         =   "Frame1"
      ForeColor       =   &H80000008&
      Height          =   8076
      Left            =   11040
      TabIndex        =   128
      Top             =   7200
      Width           =   10284
      Begin LISTBOXMODLibCtl.McVBListBox MIX1List1 
         Height          =   2364
         Index           =   1
         Left            =   4500
         TabIndex        =   137
         Top             =   3888
         Visible         =   0   'False
         Width           =   5196
         McVBListBox-InterfaceVersion=   1
         _cx             =   9165
         _cy             =   4170
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Song"
            Size            =   10.8
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BackColor       =   16777215
         ForeColor       =   -2147483640
         Appearance      =   0
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         RightToLeft     =   0   'False
         Sorted          =   0   'False
         Style           =   0
         IntegralHeight  =   0   'False
         MultiSelect     =   0
         Columns         =   0
      End
      Begin VB.TextBox MIX1TxtHELP 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   615
         Left            =   3360
         MultiLine       =   -1  'True
         TabIndex        =   143
         TabStop         =   0   'False
         Top             =   6600
         Visible         =   0   'False
         Width           =   2625
      End
      Begin VB.TextBox MIX1Text2 
         Appearance      =   0  'Flat
         BackColor       =   &H00000000&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   615
         Left            =   4800
         MultiLine       =   -1  'True
         TabIndex        =   142
         Text            =   "OEMX.frx":0313
         Top             =   6360
         Width           =   2625
      End
      Begin LISTBOXMODLibCtl.McVBListBox MIX1lstIMPOSTAZIONI 
         Height          =   900
         Index           =   1
         Left            =   5520
         TabIndex        =   145
         Top             =   6720
         Visible         =   0   'False
         Width           =   1548
         McVBListBox-InterfaceVersion=   1
         _cx             =   2725
         _cy             =   1587
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Song"
            Size            =   8.4
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BackColor       =   16777215
         ForeColor       =   -2147483640
         Appearance      =   0
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         RightToLeft     =   0   'False
         Sorted          =   0   'False
         Style           =   0
         IntegralHeight  =   0   'False
         MultiSelect     =   0
         Columns         =   0
      End
      Begin VB.ComboBox MIX1cmbMacroLav 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   384
         ItemData        =   "OEMX.frx":031F
         Left            =   360
         List            =   "OEMX.frx":0321
         TabIndex        =   134
         Text            =   "Selezione Macro"
         Top             =   1440
         Width           =   9030
      End
      Begin VB.TextBox MIX1Text1 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   10.8
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   800
         TabIndex        =   131
         Text            =   "Text1"
         Top             =   396
         Width           =   3500
      End
      Begin VB.PictureBox MIX1PicHELP 
         Appearance      =   0  'Flat
         BackColor       =   &H00EBE1D7&
         ForeColor       =   &H80000008&
         Height          =   735
         Left            =   256
         ScaleHeight     =   59
         ScaleMode       =   3  'Pixel
         ScaleWidth      =   164
         TabIndex        =   129
         TabStop         =   0   'False
         Top             =   4920
         Visible         =   0   'False
         Width           =   1995
      End
      Begin LISTBOXMODLibCtl.McVBListBox MIX1lstIMPOSTAZIONI 
         Height          =   900
         Index           =   0
         Left            =   1510
         TabIndex        =   130
         TabStop         =   0   'False
         Top             =   5454
         Visible         =   0   'False
         Width           =   2610
         McVBListBox-InterfaceVersion=   1
         _cx             =   4604
         _cy             =   1587
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Song"
            Size            =   8.4
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BackColor       =   16777215
         ForeColor       =   -2147483640
         Appearance      =   0
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         RightToLeft     =   0   'False
         Sorted          =   0   'False
         Style           =   0
         IntegralHeight  =   0   'False
         MultiSelect     =   0
         Columns         =   0
      End
      Begin TabDlg.SSTab MIX1SSTab1 
         Height          =   2904
         Left            =   4800
         TabIndex        =   132
         Top             =   4800
         Width           =   4500
         _ExtentX        =   7938
         _ExtentY        =   5144
         _Version        =   393216
         Style           =   1
         Tabs            =   2
         Tab             =   1
         TabHeight       =   520
         BackColor       =   15458775
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.4
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         TabCaption(0)   =   "MEMO"
         TabPicture(0)   =   "OEMX.frx":0323
         Tab(0).ControlEnabled=   0   'False
         Tab(0).ControlCount=   0
         TabCaption(1)   =   "Settings"
         TabPicture(1)   =   "OEMX.frx":07F9
         Tab(1).ControlEnabled=   -1  'True
         Tab(1).ControlCount=   0
      End
      Begin LISTBOXMODLibCtl.McVBListBox MIX1List1 
         Height          =   2364
         Index           =   3
         Left            =   0
         TabIndex        =   133
         Top             =   3888
         Width           =   4500
         McVBListBox-InterfaceVersion=   1
         _cx             =   7937
         _cy             =   4170
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Song"
            Size            =   7.8
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BackColor       =   65535
         ForeColor       =   -2147483640
         Appearance      =   0
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         RightToLeft     =   0   'False
         Sorted          =   0   'False
         Style           =   0
         IntegralHeight  =   0   'False
         MultiSelect     =   0
         Columns         =   0
      End
      Begin LISTBOXMODLibCtl.McVBListBox MIX1List1 
         Height          =   2400
         Index           =   0
         Left            =   4500
         TabIndex        =   135
         Top             =   792
         Width           =   5200
         McVBListBox-InterfaceVersion=   1
         _cx             =   9172
         _cy             =   4233
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Song"
            Size            =   7.8
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BackColor       =   65535
         ForeColor       =   -2147483640
         Appearance      =   0
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         RightToLeft     =   0   'False
         Sorted          =   0   'False
         Style           =   0
         IntegralHeight  =   0   'False
         MultiSelect     =   0
         Columns         =   0
      End
      Begin LISTBOXMODLibCtl.McVBListBox MIX1List1 
         Height          =   2400
         Index           =   2
         Left            =   0
         TabIndex        =   136
         TabStop         =   0   'False
         Top             =   792
         Width           =   4500
         McVBListBox-InterfaceVersion=   1
         _cx             =   7937
         _cy             =   4233
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Song"
            Size            =   12
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BackColor       =   16777215
         ForeColor       =   -2147483640
         Appearance      =   0
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         RightToLeft     =   0   'False
         Sorted          =   -1  'True
         Style           =   0
         IntegralHeight  =   0   'False
         MultiSelect     =   0
         Columns         =   0
      End
      Begin VB.Image MIX1Image1 
         Appearance      =   0  'Flat
         BorderStyle     =   1  'Fixed Single
         Height          =   400
         Left            =   120
         Picture         =   "OEMX.frx":0C8B
         Stretch         =   -1  'True
         Top             =   3360
         Visible         =   0   'False
         Width           =   312
      End
      Begin VB.Image MIX1Image5 
         Appearance      =   0  'Flat
         BorderStyle     =   1  'Fixed Single
         Height          =   400
         Left            =   0
         Picture         =   "OEMX.frx":3C0D
         Stretch         =   -1  'True
         Top             =   360
         Visible         =   0   'False
         Width           =   792
      End
      Begin VB.Image MIX1Image7 
         Appearance      =   0  'Flat
         BorderStyle     =   1  'Fixed Single
         Height          =   400
         Left            =   4440
         Picture         =   "OEMX.frx":6B8F
         Stretch         =   -1  'True
         Top             =   3360
         Visible         =   0   'False
         Width           =   708
      End
      Begin VB.Label MIX1Label1 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H0000FFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "MACRO LAVORAZIONE"
         BeginProperty Font 
            Name            =   "MS Song"
            Size            =   12
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   408
         Index           =   2
         Left            =   1200
         TabIndex        =   141
         Top             =   120
         Width           =   7500
      End
      Begin VB.Image MIX1Image3 
         Appearance      =   0  'Flat
         BorderStyle     =   1  'Fixed Single
         Height          =   400
         Left            =   0
         Picture         =   "OEMX.frx":9B11
         Stretch         =   -1  'True
         Top             =   0
         Width           =   792
      End
      Begin VB.Label MIX1Label1 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "En/Disenable Workpieces"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   10.8
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   408
         Index           =   3
         Left            =   628
         TabIndex        =   140
         Top             =   3384
         Visible         =   0   'False
         Width           =   2832
      End
      Begin VB.Image MIX1Image6 
         Appearance      =   0  'Flat
         BorderStyle     =   1  'Fixed Single
         Height          =   400
         Left            =   9000
         Picture         =   "OEMX.frx":CA93
         Stretch         =   -1  'True
         Top             =   396
         Width           =   700
      End
      Begin VB.Image MIX1Image2 
         Appearance      =   0  'Flat
         BorderStyle     =   1  'Fixed Single
         Height          =   400
         Left            =   0
         Picture         =   "OEMX.frx":FA15
         Stretch         =   -1  'True
         Top             =   0
         Width           =   792
      End
      Begin VB.Image MIX1Image4 
         Appearance      =   0  'Flat
         BorderStyle     =   1  'Fixed Single
         Height          =   400
         Left            =   4368
         Picture         =   "OEMX.frx":12997
         Stretch         =   -1  'True
         Top             =   396
         Width           =   312
      End
      Begin VB.Label MIX1Label1 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Archive"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   10.8
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   408
         Index           =   1
         Left            =   5200
         TabIndex        =   139
         Top             =   3384
         Visible         =   0   'False
         Width           =   3648
      End
      Begin VB.Label MIX1Label1 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Workpieces list"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   10.8
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FF0000&
         Height          =   408
         Index           =   0
         Left            =   4700
         TabIndex        =   138
         Top             =   396
         Width           =   4280
      End
   End
   Begin VB.CommandButton PRG0Intestazione 
      BackColor       =   &H00EBE1D7&
      Caption         =   "Work Type"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   400
      Left            =   950
      Style           =   1  'Graphical
      TabIndex        =   124
      Top             =   8500
      Width           =   2000
   End
   Begin LISTBOXMODLibCtl.McVBListBox PRG0SelezioneLavorazioni 
      Height          =   2592
      Left            =   8760
      TabIndex        =   122
      TabStop         =   0   'False
      Top             =   8520
      Visible         =   0   'False
      Width           =   2052
      McVBListBox-InterfaceVersion=   1
      _cx             =   3619
      _cy             =   4572
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Appearance      =   0
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      RightToLeft     =   0   'False
      Sorted          =   0   'False
      Style           =   0
      IntegralHeight  =   0   'False
      MultiSelect     =   0
      Columns         =   0
   End
   Begin VB.FileListBox MNG0FileList 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   10.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   744
      Left            =   8040
      MultiSelect     =   2  'Extended
      TabIndex        =   116
      Top             =   12000
      Width           =   1500
   End
   Begin VB.TextBox MNG0Text2 
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   10.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   1800
      Left            =   50
      MultiLine       =   -1  'True
      TabIndex        =   111
      Top             =   11000
      Width           =   2655
   End
   Begin VB.Timer Timer1 
      Left            =   10320
      Top             =   11400
   End
   Begin LISTBOXMODLibCtl.McVBListBox MNG0lstIMPOSTAZIONI 
      Height          =   780
      Index           =   1
      Left            =   8040
      TabIndex        =   109
      Top             =   13000
      Width           =   1500
      McVBListBox-InterfaceVersion=   1
      _cx             =   2646
      _cy             =   1376
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Song"
         Size            =   9
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Appearance      =   0
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      RightToLeft     =   0   'False
      Sorted          =   0   'False
      Style           =   0
      IntegralHeight  =   0   'False
      MultiSelect     =   0
      Columns         =   0
   End
   Begin LISTBOXMODLibCtl.McVBListBox MNG0lstIMPOSTAZIONI 
      Height          =   768
      Index           =   0
      Left            =   5400
      TabIndex        =   110
      TabStop         =   0   'False
      Top             =   13000
      Width           =   2500
      McVBListBox-InterfaceVersion=   1
      _cx             =   4410
      _cy             =   1355
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Song"
         Size            =   9
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BackColor       =   16777215
      ForeColor       =   -2147483640
      Appearance      =   0
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      RightToLeft     =   0   'False
      Sorted          =   0   'False
      Style           =   0
      IntegralHeight  =   0   'False
      MultiSelect     =   0
      Columns         =   0
   End
   Begin VB.TextBox MNG0Text1 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   16.8
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   528
      Left            =   1050
      TabIndex        =   102
      Top             =   10500
      Visible         =   0   'False
      Width           =   6240
   End
   Begin VB.ListBox MNG0List1 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3792
      Index           =   0
      ItemData        =   "OEMX.frx":15919
      Left            =   50
      List            =   "OEMX.frx":1591B
      Sorted          =   -1  'True
      TabIndex        =   105
      TabStop         =   0   'False
      Top             =   11000
      Visible         =   0   'False
      Width           =   4500
   End
   Begin VB.PictureBox MNG0PicLogo 
      BorderStyle     =   0  'None
      Height          =   585
      Left            =   7440
      Picture         =   "OEMX.frx":1591D
      ScaleHeight     =   10.372
      ScaleMode       =   6  'Millimeter
      ScaleWidth      =   13.335
      TabIndex        =   104
      TabStop         =   0   'False
      Top             =   10320
      Visible         =   0   'False
      Width           =   750
   End
   Begin VB.Timer MNG0Timer1 
      Left            =   8208
      Top             =   15540
   End
   Begin VB.TextBox PRG0txtNomePezzo 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   10.8
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   400
      Left            =   3000
      TabIndex        =   97
      Text            =   "txtNomePezzo"
      Top             =   9000
      Width           =   5600
   End
   Begin VB.TextBox PRG0txtMATRICOLA 
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   400
      Left            =   3000
      TabIndex        =   95
      Text            =   "txtMATRICOLA"
      Top             =   9500
      Visible         =   0   'False
      Width           =   5600
   End
   Begin VB.Frame PRG0Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBE1D7&
      Caption         =   "Selezione programmi pezzo"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   4800
      Left            =   11400
      TabIndex        =   93
      Top             =   1320
      Width           =   9750
      Begin LISTBOXMODLibCtl.McVBListBox McListAzioni 
         Height          =   3600
         Index           =   0
         Left            =   5950
         TabIndex        =   169
         TabStop         =   0   'False
         Top             =   900
         Width           =   3710
         McVBListBox-InterfaceVersion=   1
         _cx             =   6544
         _cy             =   6350
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Times New Roman"
            Size            =   10.8
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BackColor       =   15458775
         ForeColor       =   -2147483640
         Appearance      =   0
         Enabled         =   0   'False
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         RightToLeft     =   0   'False
         Sorted          =   0   'False
         Style           =   1
         IntegralHeight  =   0   'False
         MultiSelect     =   0
         Columns         =   0
      End
      Begin LISTBOXMODLibCtl.McVBListBox McListPrg 
         Height          =   3600
         Left            =   100
         TabIndex        =   0
         Top             =   900
         Width           =   3800
         McVBListBox-InterfaceVersion=   1
         _cx             =   6703
         _cy             =   6350
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BackColor       =   16777215
         ForeColor       =   -2147483640
         Appearance      =   0
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         RightToLeft     =   0   'False
         Sorted          =   0   'False
         Style           =   0
         IntegralHeight  =   0   'False
         MultiSelect     =   0
         Columns         =   0
      End
      Begin VB.Label lblABILITAZIONI 
         Alignment       =   2  'Center
         BackColor       =   &H0000FFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "ON-LINE Optionen"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         Height          =   400
         Left            =   4560
         TabIndex        =   193
         Top             =   4320
         Visible         =   0   'False
         Width           =   2004
      End
      Begin VB.Image Picture1 
         Appearance      =   0  'Flat
         BorderStyle     =   1  'Fixed Single
         Height          =   3600
         Left            =   3900
         Stretch         =   -1  'True
         Top             =   900
         Width           =   3710
      End
      Begin VB.Label lblVERSIONE 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   300
         Index           =   1
         Left            =   1300
         TabIndex        =   120
         Top             =   600
         Width           =   8360
      End
      Begin VB.Image Image1 
         Appearance      =   0  'Flat
         BorderStyle     =   1  'Fixed Single
         Height          =   600
         Left            =   700
         Picture         =   "OEMX.frx":246C7
         Stretch         =   -1  'True
         Top             =   300
         Width           =   600
      End
      Begin VB.Image Image2 
         Appearance      =   0  'Flat
         BorderStyle     =   1  'Fixed Single
         Height          =   600
         Left            =   100
         Picture         =   "OEMX.frx":27649
         Stretch         =   -1  'True
         Top             =   300
         Width           =   600
      End
      Begin VB.Label lblVERSIONE 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H0080FFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "VERSIONE"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   300
         Index           =   0
         Left            =   1300
         TabIndex        =   94
         Top             =   300
         Width           =   8360
      End
   End
   Begin Threed.SSFrame OEM1frameCalcolo 
      Height          =   6000
      Left            =   21480
      TabIndex        =   16
      Top             =   360
      Visible         =   0   'False
      Width           =   10000
      _Version        =   65536
      _ExtentX        =   17639
      _ExtentY        =   10583
      _StockProps     =   14
      Caption         =   "OEM1frameCalcolo"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin VB.Frame OEM1fraOpzioni 
         BackColor       =   &H00FFFFFF&
         Caption         =   "OEM1fraOpzioni"
         BeginProperty Font 
            Name            =   "MS Song"
            Size            =   8.4
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3529
         Left            =   3120
         TabIndex        =   170
         Top             =   1440
         Visible         =   0   'False
         Width           =   3684
         Begin VB.TextBox txtDonnee 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "MS Song"
               Size            =   9.6
               Charset         =   134
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   1
            Left            =   2650
            TabIndex        =   171
            Text            =   "txtDonnee"
            Top             =   270
            Width           =   1000
         End
         Begin VB.TextBox txtDonnee 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "MS Song"
               Size            =   9.6
               Charset         =   134
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   2
            Left            =   2650
            TabIndex        =   172
            Text            =   "txtDonnee"
            Top             =   560
            Width           =   1000
         End
         Begin VB.TextBox txtDonnee 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "MS Song"
               Size            =   9.6
               Charset         =   134
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   3
            Left            =   2650
            TabIndex        =   173
            Text            =   "txtDonnee"
            Top             =   850
            Width           =   1000
         End
         Begin VB.TextBox txtDonnee 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "MS Song"
               Size            =   9.6
               Charset         =   134
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   4
            Left            =   2650
            TabIndex        =   174
            Text            =   "txtDonnee"
            Top             =   1140
            Width           =   1000
         End
         Begin VB.TextBox txtDonnee 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "MS Song"
               Size            =   9.6
               Charset         =   134
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   5
            Left            =   2650
            TabIndex        =   175
            Text            =   "txtDonnee"
            Top             =   1430
            Width           =   1000
         End
         Begin VB.TextBox txtDonnee 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "MS Song"
               Size            =   9.6
               Charset         =   134
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   6
            Left            =   2650
            TabIndex        =   176
            Text            =   "txtDonnee"
            Top             =   1710
            Width           =   1000
         End
         Begin VB.TextBox txtDonnee 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "MS Song"
               Size            =   9.6
               Charset         =   134
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   7
            Left            =   2650
            TabIndex        =   177
            Text            =   "txtDonnee"
            Top             =   2000
            Width           =   1000
         End
         Begin VB.TextBox txtDonnee 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "MS Song"
               Size            =   9.6
               Charset         =   134
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   8
            Left            =   2650
            TabIndex        =   178
            Text            =   "txtDonnee"
            Top             =   2290
            Width           =   1000
         End
         Begin VB.TextBox txtDonnee 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "MS Song"
               Size            =   9.6
               Charset         =   134
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   9
            Left            =   2650
            TabIndex        =   179
            Text            =   "txtDonnee"
            Top             =   2580
            Width           =   1000
         End
         Begin VB.TextBox txtDonnee 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "MS Song"
               Size            =   9.6
               Charset         =   134
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Index           =   10
            Left            =   2650
            TabIndex        =   180
            Text            =   "txtDonnee"
            Top             =   2870
            Width           =   1000
         End
         Begin VB.CheckBox chkDonnee 
            Alignment       =   1  'Right Justify
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            Caption         =   "REVERSE FLANK"
            BeginProperty Font 
               Name            =   "MS Song"
               Size            =   10.2
               Charset         =   134
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   350
            Left            =   150
            TabIndex        =   181
            Top             =   3150
            Width           =   3300
         End
         Begin VB.Label lblDonnee 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            Caption         =   "lblDonnee"
            BeginProperty Font 
               Name            =   "MS Song"
               Size            =   9.6
               Charset         =   134
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Index           =   1
            Left            =   50
            TabIndex        =   191
            Top             =   270
            Width           =   2600
         End
         Begin VB.Label lblDonnee 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            Caption         =   "lblDonnee"
            BeginProperty Font 
               Name            =   "MS Song"
               Size            =   9.6
               Charset         =   134
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Index           =   2
            Left            =   50
            TabIndex        =   190
            Top             =   560
            Width           =   2600
         End
         Begin VB.Label lblDonnee 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            Caption         =   "lblDonnee"
            BeginProperty Font 
               Name            =   "MS Song"
               Size            =   9.6
               Charset         =   134
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Index           =   3
            Left            =   50
            TabIndex        =   189
            Top             =   850
            Width           =   2600
         End
         Begin VB.Label lblDonnee 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            Caption         =   "lblDonnee"
            BeginProperty Font 
               Name            =   "MS Song"
               Size            =   9.6
               Charset         =   134
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Index           =   4
            Left            =   50
            TabIndex        =   188
            Top             =   1140
            Width           =   2600
         End
         Begin VB.Label lblDonnee 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            Caption         =   "lblDonnee"
            BeginProperty Font 
               Name            =   "MS Song"
               Size            =   9.6
               Charset         =   134
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Index           =   5
            Left            =   50
            TabIndex        =   187
            Top             =   1430
            Width           =   2600
         End
         Begin VB.Label lblDonnee 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            Caption         =   "lblDonnee"
            BeginProperty Font 
               Name            =   "MS Song"
               Size            =   9.6
               Charset         =   134
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Index           =   6
            Left            =   50
            TabIndex        =   186
            Top             =   1710
            Width           =   2600
         End
         Begin VB.Label lblDonnee 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            Caption         =   "lblDonnee"
            BeginProperty Font 
               Name            =   "MS Song"
               Size            =   9.6
               Charset         =   134
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Index           =   7
            Left            =   50
            TabIndex        =   185
            Top             =   2000
            Width           =   2600
         End
         Begin VB.Label lblDonnee 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            Caption         =   "lblDonnee"
            BeginProperty Font 
               Name            =   "MS Song"
               Size            =   9.6
               Charset         =   134
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   285
            Index           =   8
            Left            =   50
            TabIndex        =   184
            Top             =   2290
            Width           =   2600
         End
         Begin VB.Label lblDonnee 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            Caption         =   "lblDonnee"
            BeginProperty Font 
               Name            =   "MS Song"
               Size            =   9.6
               Charset         =   134
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   288
            Index           =   9
            Left            =   50
            TabIndex        =   183
            Top             =   2580
            Width           =   2604
         End
         Begin VB.Label lblDonnee 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            Caption         =   "lblDonnee"
            BeginProperty Font 
               Name            =   "MS Song"
               Size            =   9.6
               Charset         =   134
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H80000008&
            Height          =   288
            Index           =   10
            Left            =   50
            TabIndex        =   182
            Top             =   2870
            Width           =   2604
         End
      End
      Begin VB.Frame HGkineticprep 
         BackColor       =   &H80000009&
         Height          =   732
         Left            =   1200
         TabIndex        =   147
         Top             =   1080
         Visible         =   0   'False
         Width           =   2364
      End
      Begin VB.CheckBox HGkineticF 
         Appearance      =   0  'Flat
         Caption         =   "LQ"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   7.8
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   1
         Left            =   8000
         TabIndex        =   144
         Top             =   320
         Visible         =   0   'False
         Width           =   800
      End
      Begin VB.CheckBox HGkineticF 
         Appearance      =   0  'Flat
         Caption         =   "WOC"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   7.8
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Index           =   0
         Left            =   8000
         TabIndex        =   146
         Top             =   100
         Visible         =   0   'False
         Width           =   800
      End
      Begin LISTBOXMODLibCtl.McVBListBox OEM1ListaCombo1 
         Height          =   2496
         Left            =   1200
         TabIndex        =   125
         Top             =   1320
         Visible         =   0   'False
         Width           =   2100
         McVBListBox-InterfaceVersion=   1
         _cx             =   3704
         _cy             =   4410
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         BackColor       =   16777215
         ForeColor       =   -2147483640
         Appearance      =   0
         Enabled         =   -1  'True
         MousePointer    =   0
         Object.TabStop         =   -1  'True
         RightToLeft     =   0   'False
         Sorted          =   0   'False
         Style           =   0
         IntegralHeight  =   0   'False
         MultiSelect     =   0
         Columns         =   0
      End
      Begin VB.TextBox OEM1txtHELP 
         Appearance      =   0  'Flat
         BackColor       =   &H00000000&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   10.8
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   1185
         Left            =   6360
         MultiLine       =   -1  'True
         ScrollBars      =   3  'Both
         TabIndex        =   26
         Text            =   "OEMX.frx":2A5CB
         Top             =   1560
         Visible         =   0   'False
         Width           =   3420
      End
      Begin VB.CommandButton OEM1Combo1 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3900
         Style           =   1  'Graphical
         TabIndex        =   126
         Top             =   240
         Width           =   4000
      End
      Begin VB.HScrollBar OEM1hsMOLA1 
         Height          =   330
         LargeChange     =   10
         Left            =   3600
         TabIndex        =   121
         Top             =   560
         Visible         =   0   'False
         Width           =   1665
      End
      Begin VB.HScrollBar OEM1hsROTORE 
         Height          =   330
         LargeChange     =   10
         Left            =   400
         TabIndex        =   119
         Top             =   2500
         Visible         =   0   'False
         Width           =   2500
      End
      Begin VB.HScrollBar OEM1hsMOLA 
         Height          =   330
         LargeChange     =   10
         Left            =   400
         TabIndex        =   118
         Top             =   2130
         Visible         =   0   'False
         Width           =   2500
      End
      Begin VB.HScrollBar OEM1hsPROFILO 
         Height          =   330
         LargeChange     =   10
         Left            =   400
         TabIndex        =   115
         Top             =   1750
         Visible         =   0   'False
         Width           =   2500
      End
      Begin VB.Frame fraTOLLERANZA 
         Caption         =   "fraTOLLERANZA"
         BeginProperty Font 
            Name            =   "MS Song"
            Size            =   8.4
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3825
         Left            =   7440
         TabIndex        =   27
         Top             =   3600
         Visible         =   0   'False
         Width           =   6405
         Begin VB.TextBox txtPXXF1 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   0
            Left            =   720
            TabIndex        =   59
            Text            =   "txtPXXF1"
            Top             =   3060
            Width           =   915
         End
         Begin VB.TextBox txtPXXF1 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   1
            Left            =   720
            TabIndex        =   58
            TabStop         =   0   'False
            Text            =   "txtPXXF1"
            Top             =   2700
            Width           =   915
         End
         Begin VB.TextBox txtPXXF1 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   2
            Left            =   720
            TabIndex        =   57
            TabStop         =   0   'False
            Text            =   "txtPXXF1"
            Top             =   2340
            Width           =   915
         End
         Begin VB.TextBox txtPXXF1 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   3
            Left            =   720
            TabIndex        =   56
            TabStop         =   0   'False
            Text            =   "txtPXXF1"
            Top             =   1980
            Width           =   915
         End
         Begin VB.TextBox txtPXXF1 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   4
            Left            =   720
            TabIndex        =   55
            TabStop         =   0   'False
            Text            =   "txtPXXF1"
            Top             =   1620
            Width           =   915
         End
         Begin VB.TextBox txtPXXF1 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   5
            Left            =   720
            TabIndex        =   54
            TabStop         =   0   'False
            Text            =   "txtPXXF1"
            Top             =   1260
            Width           =   915
         End
         Begin VB.TextBox txtPXXF1 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   6
            Left            =   720
            TabIndex        =   53
            TabStop         =   0   'False
            Text            =   "txtPXXF1"
            Top             =   900
            Width           =   915
         End
         Begin VB.TextBox txtPXXF1 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   7
            Left            =   720
            TabIndex        =   52
            TabStop         =   0   'False
            Text            =   "txtPXXF1"
            Top             =   540
            Width           =   915
         End
         Begin VB.TextBox txtPYYF1 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   0
            Left            =   1665
            TabIndex        =   51
            Text            =   "txtPYYF1"
            Top             =   3060
            Width           =   915
         End
         Begin VB.TextBox txtPYYF1 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   1
            Left            =   1665
            TabIndex        =   50
            TabStop         =   0   'False
            Text            =   "txtPYYF1"
            Top             =   2700
            Width           =   915
         End
         Begin VB.TextBox txtPYYF1 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   2
            Left            =   1665
            TabIndex        =   49
            TabStop         =   0   'False
            Text            =   "txtPYYF1"
            Top             =   2340
            Width           =   915
         End
         Begin VB.TextBox txtPYYF1 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   3
            Left            =   1665
            TabIndex        =   48
            TabStop         =   0   'False
            Text            =   "txtPYYF1"
            Top             =   1980
            Width           =   915
         End
         Begin VB.TextBox txtPYYF1 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   4
            Left            =   1665
            TabIndex        =   47
            TabStop         =   0   'False
            Text            =   "txtPYYF1"
            Top             =   1620
            Width           =   915
         End
         Begin VB.TextBox txtPYYF1 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   5
            Left            =   1665
            TabIndex        =   46
            TabStop         =   0   'False
            Text            =   "txtPYYF1"
            Top             =   1260
            Width           =   915
         End
         Begin VB.TextBox txtPYYF1 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   6
            Left            =   1665
            TabIndex        =   45
            TabStop         =   0   'False
            Text            =   "txtPYYF1"
            Top             =   900
            Width           =   915
         End
         Begin VB.TextBox txtPYYF1 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   7
            Left            =   1665
            TabIndex        =   44
            TabStop         =   0   'False
            Text            =   "txtPYYF1"
            Top             =   540
            Width           =   915
         End
         Begin VB.TextBox txtPXXF2 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   0
            Left            =   3600
            TabIndex        =   43
            Text            =   "txtPXXF2"
            Top             =   3060
            Width           =   915
         End
         Begin VB.TextBox txtPXXF2 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   1
            Left            =   3600
            TabIndex        =   42
            TabStop         =   0   'False
            Text            =   "txtPXXF2"
            Top             =   2700
            Width           =   915
         End
         Begin VB.TextBox txtPXXF2 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   2
            Left            =   3600
            TabIndex        =   41
            TabStop         =   0   'False
            Text            =   "txtPXXF2"
            Top             =   2340
            Width           =   915
         End
         Begin VB.TextBox txtPXXF2 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   3
            Left            =   3600
            TabIndex        =   40
            TabStop         =   0   'False
            Text            =   "txtPXXF2"
            Top             =   1980
            Width           =   915
         End
         Begin VB.TextBox txtPXXF2 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   4
            Left            =   3600
            TabIndex        =   39
            TabStop         =   0   'False
            Text            =   "txtPXXF2"
            Top             =   1620
            Width           =   915
         End
         Begin VB.TextBox txtPXXF2 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   5
            Left            =   3600
            TabIndex        =   38
            TabStop         =   0   'False
            Text            =   "txtPXXF2"
            Top             =   1260
            Width           =   915
         End
         Begin VB.TextBox txtPXXF2 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   6
            Left            =   3600
            TabIndex        =   37
            TabStop         =   0   'False
            Text            =   "txtPXXF2"
            Top             =   900
            Width           =   915
         End
         Begin VB.TextBox txtPXXF2 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   7
            Left            =   3600
            TabIndex        =   36
            TabStop         =   0   'False
            Text            =   "txtPXXF2"
            Top             =   540
            Width           =   915
         End
         Begin VB.TextBox txtPYYF2 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   0
            Left            =   4550
            TabIndex        =   35
            Text            =   "txtPYYF2"
            Top             =   3060
            Width           =   915
         End
         Begin VB.TextBox txtPYYF2 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   1
            Left            =   4550
            TabIndex        =   34
            TabStop         =   0   'False
            Text            =   "txtPYYF2"
            Top             =   2700
            Width           =   915
         End
         Begin VB.TextBox txtPYYF2 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   2
            Left            =   4550
            TabIndex        =   33
            TabStop         =   0   'False
            Text            =   "txtPYYF2"
            Top             =   2340
            Width           =   915
         End
         Begin VB.TextBox txtPYYF2 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   3
            Left            =   4550
            TabIndex        =   32
            TabStop         =   0   'False
            Text            =   "txtPYYF2"
            Top             =   1980
            Width           =   915
         End
         Begin VB.TextBox txtPYYF2 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   4
            Left            =   4550
            TabIndex        =   31
            TabStop         =   0   'False
            Text            =   "txtPYYF2"
            Top             =   1620
            Width           =   915
         End
         Begin VB.TextBox txtPYYF2 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   5
            Left            =   4550
            TabIndex        =   30
            TabStop         =   0   'False
            Text            =   "txtPYYF2"
            Top             =   1260
            Width           =   915
         End
         Begin VB.TextBox txtPYYF2 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   6
            Left            =   4550
            TabIndex        =   29
            TabStop         =   0   'False
            Text            =   "txtPYYF2"
            Top             =   900
            Width           =   915
         End
         Begin VB.TextBox txtPYYF2 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   7
            Left            =   4550
            TabIndex        =   28
            TabStop         =   0   'False
            Text            =   "txtPYYF2"
            Top             =   540
            Width           =   915
         End
         Begin VB.Label IF2 
            Caption         =   "IF2"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   7
            Left            =   3000
            TabIndex        =   81
            Top             =   555
            Width           =   2595
         End
         Begin VB.Label IF2 
            Caption         =   "IF2"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   6
            Left            =   3000
            TabIndex        =   80
            Top             =   900
            Width           =   2595
         End
         Begin VB.Label IF2 
            Caption         =   "IF2"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   5
            Left            =   3000
            TabIndex        =   79
            Top             =   1260
            Width           =   2595
         End
         Begin VB.Label IF2 
            Caption         =   "IF2"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   4
            Left            =   3000
            TabIndex        =   78
            Top             =   1605
            Width           =   2595
         End
         Begin VB.Label IF2 
            Caption         =   "IF2"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   3
            Left            =   3000
            TabIndex        =   77
            Top             =   2000
            Width           =   2600
         End
         Begin VB.Label IF2 
            Caption         =   "IF2"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   2
            Left            =   3000
            TabIndex        =   76
            Top             =   2350
            Width           =   2600
         End
         Begin VB.Label IF2 
            Caption         =   "IF2"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   1
            Left            =   3000
            TabIndex        =   75
            Top             =   2700
            Width           =   2600
         End
         Begin VB.Label IF2 
            Caption         =   "IF2"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   0
            Left            =   3000
            TabIndex        =   74
            Top             =   3050
            Width           =   2600
         End
         Begin VB.Label IF1 
            Caption         =   "IF1"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   7
            Left            =   105
            TabIndex        =   73
            Top             =   555
            Width           =   2595
         End
         Begin VB.Label IF1 
            Caption         =   "IF1"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   6
            Left            =   105
            TabIndex        =   72
            Top             =   900
            Width           =   2595
         End
         Begin VB.Label IF1 
            Caption         =   "IF1"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   5
            Left            =   105
            TabIndex        =   71
            Top             =   1260
            Width           =   2595
         End
         Begin VB.Label IF1 
            Caption         =   "IF1"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   4
            Left            =   105
            TabIndex        =   70
            Top             =   1605
            Width           =   2595
         End
         Begin VB.Label IF1 
            Caption         =   "IF1"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   3
            Left            =   100
            TabIndex        =   69
            Top             =   2000
            Width           =   2600
         End
         Begin VB.Label IF1 
            Caption         =   "IF1"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   2
            Left            =   100
            TabIndex        =   68
            Top             =   2350
            Width           =   2600
         End
         Begin VB.Label IF1 
            Caption         =   "IF1"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   1
            Left            =   100
            TabIndex        =   67
            Top             =   2700
            Width           =   2600
         End
         Begin VB.Label IF1 
            Caption         =   "IF1"
            BeginProperty Font 
               Name            =   "MS Song"
               Size            =   12
               Charset         =   134
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   0
            Left            =   100
            TabIndex        =   66
            Top             =   3050
            Width           =   2600
         End
         Begin VB.Label lblFIANCO 
            Alignment       =   2  'Center
            Caption         =   "F2"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   1
            Left            =   3000
            TabIndex        =   65
            Top             =   165
            Width           =   2595
         End
         Begin VB.Label lblFIANCO 
            Alignment       =   2  'Center
            Caption         =   "F1"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   0
            Left            =   105
            TabIndex        =   64
            Top             =   165
            Width           =   2595
         End
         Begin VB.Label lblXX 
            Alignment       =   2  'Center
            Caption         =   "X"
            BeginProperty Font 
               Name            =   "MS Song"
               Size            =   14.4
               Charset         =   134
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   0
            Left            =   705
            TabIndex        =   63
            Top             =   3435
            Width           =   900
         End
         Begin VB.Label lblXX 
            Alignment       =   2  'Center
            Caption         =   "X"
            BeginProperty Font 
               Name            =   "MS Song"
               Size            =   12
               Charset         =   134
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   1
            Left            =   3600
            TabIndex        =   62
            Top             =   3435
            Width           =   900
         End
         Begin VB.Label lblYY 
            Alignment       =   2  'Center
            Caption         =   "Y"
            BeginProperty Font 
               Name            =   "MS Song"
               Size            =   14.4
               Charset         =   134
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   0
            Left            =   1650
            TabIndex        =   61
            Top             =   3435
            Width           =   900
         End
         Begin VB.Label lblYY 
            Alignment       =   2  'Center
            Caption         =   "Y"
            BeginProperty Font 
               Name            =   "MS Song"
               Size            =   14.4
               Charset         =   134
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   1
            Left            =   4545
            TabIndex        =   60
            Top             =   3435
            Width           =   900
         End
      End
      Begin VB.CheckBox OEM1chkINPUT 
         Appearance      =   0  'Flat
         Caption         =   "LINES"
         BeginProperty Font 
            Name            =   "MS Song"
            Size            =   8.4
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   300
         Index           =   2
         Left            =   8000
         TabIndex        =   92
         Top             =   580
         Visible         =   0   'False
         Width           =   1600
      End
      Begin VB.CheckBox OEM1chkINPUT 
         Appearance      =   0  'Flat
         Caption         =   "GRID"
         BeginProperty Font 
            Name            =   "MS Song"
            Size            =   8.4
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   300
         Index           =   1
         Left            =   8000
         TabIndex        =   83
         Top             =   240
         Visible         =   0   'False
         Width           =   1600
      End
      Begin VB.CheckBox OEM1chkINPUT 
         Appearance      =   0  'Flat
         Caption         =   "POINTS"
         BeginProperty Font 
            Name            =   "MS Song"
            Size            =   8.4
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   300
         Index           =   0
         Left            =   6120
         TabIndex        =   82
         Top             =   580
         Visible         =   0   'False
         Width           =   1600
      End
      Begin VB.TextBox txtINP 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Song"
            Size            =   9.6
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   0
         Left            =   1130
         TabIndex        =   19
         Text            =   "txtINP"
         Top             =   240
         Visible         =   0   'False
         Width           =   800
      End
      Begin VB.TextBox txtINP 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Song"
            Size            =   9.6
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   1
         Left            =   2925
         TabIndex        =   20
         Text            =   "txtINP"
         Top             =   240
         Visible         =   0   'False
         Width           =   800
      End
      Begin VB.CommandButton OEM1Command2 
         Caption         =   "Options"
         BeginProperty Font 
            Name            =   "MS Song"
            Size            =   8.4
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   320
         Left            =   290
         TabIndex        =   18
         Top             =   575
         Visible         =   0   'False
         Width           =   1550
      End
      Begin VB.PictureBox OEM1PicCALCOLO 
         Appearance      =   0  'Flat
         AutoRedraw      =   -1  'True
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Song"
            Size            =   8.4
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   4700
         Left            =   180
         ScaleHeight     =   4680
         ScaleMode       =   0  'User
         ScaleWidth      =   9372
         TabIndex        =   17
         TabStop         =   0   'False
         Top             =   900
         Width           =   9400
         Begin VB.Line OEM1Line1 
            BorderColor     =   &H000000FF&
            Visible         =   0   'False
            X1              =   150
            X2              =   150
            Y1              =   0
            Y2              =   797
         End
         Begin VB.Line OEM1Line2 
            BorderColor     =   &H000000FF&
            Visible         =   0   'False
            X1              =   0
            X2              =   600
            Y1              =   406
            Y2              =   406
         End
         Begin VB.Line OEM1Line3 
            BorderColor     =   &H00000000&
            Visible         =   0   'False
            X1              =   660
            X2              =   660
            Y1              =   0
            Y2              =   797
         End
      End
      Begin VB.TextBox txtINP 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Song"
            Size            =   9.6
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   2
         Left            =   1130
         TabIndex        =   23
         Text            =   "txtINP"
         Top             =   560
         Visible         =   0   'False
         Width           =   800
      End
      Begin VB.TextBox txtINP 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Song"
            Size            =   9.6
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   3
         Left            =   2925
         TabIndex        =   24
         Text            =   "txtINP"
         Top             =   560
         Visible         =   0   'False
         Width           =   800
      End
      Begin VB.TextBox txtINP 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Song"
            Size            =   9.6
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   4
         Left            =   4725
         TabIndex        =   25
         Text            =   "txtINP"
         Top             =   560
         Visible         =   0   'False
         Width           =   800
      End
      Begin VB.TextBox OEM1txtOX 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Song"
            Size            =   9.6
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   3645
         TabIndex        =   22
         Text            =   "0"
         Top             =   560
         Visible         =   0   'False
         Width           =   600
      End
      Begin VB.TextBox OEM1txtOY 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Song"
            Size            =   9.6
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   5445
         TabIndex        =   21
         Text            =   "0"
         Top             =   560
         Visible         =   0   'False
         Width           =   600
      End
      Begin VB.Label OEM1lblOY 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H0080FFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Oy"
         BeginProperty Font 
            Name            =   "MS Song"
            Size            =   9.6
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   330
         Left            =   4725
         TabIndex        =   91
         Top             =   560
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.Label lblINP 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "lblINP"
         BeginProperty Font 
            Name            =   "MS Song"
            Size            =   9.6
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   330
         Index           =   4
         Left            =   3960
         TabIndex        =   90
         Top             =   560
         Visible         =   0   'False
         Width           =   780
      End
      Begin VB.Label lblINP 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "lblINP"
         BeginProperty Font 
            Name            =   "MS Song"
            Size            =   9.6
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   330
         Index           =   3
         Left            =   1980
         TabIndex        =   89
         Top             =   560
         Visible         =   0   'False
         Width           =   960
      End
      Begin VB.Label lblINP 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "lblINP"
         BeginProperty Font 
            Name            =   "MS Song"
            Size            =   9.6
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   330
         Index           =   2
         Left            =   180
         TabIndex        =   88
         Top             =   560
         Visible         =   0   'False
         Width           =   960
      End
      Begin VB.Label lblINP 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "lblINP"
         BeginProperty Font 
            Name            =   "MS Song"
            Size            =   9.6
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   330
         Index           =   0
         Left            =   180
         TabIndex        =   87
         Top             =   240
         Visible         =   0   'False
         Width           =   960
      End
      Begin VB.Label lblINP 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "lblINP"
         BeginProperty Font 
            Name            =   "MS Song"
            Size            =   9.6
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   330
         Index           =   1
         Left            =   1980
         TabIndex        =   86
         Top             =   240
         Visible         =   0   'False
         Width           =   960
      End
      Begin VB.Label OEM1Label2 
         Appearance      =   0  'Flat
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "MS Song"
            Size            =   9
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   330
         Left            =   180
         TabIndex        =   85
         Top             =   560
         Visible         =   0   'False
         Width           =   1750
      End
      Begin VB.Label OEM1lblOX 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H0080FFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Ox"
         BeginProperty Font 
            Name            =   "MS Song"
            Size            =   9.6
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   330
         Left            =   2925
         TabIndex        =   84
         Top             =   560
         Visible         =   0   'False
         Width           =   735
      End
   End
   Begin VB.Timer Polling 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   50
      Top             =   10500
   End
   Begin Threed.SSFrame frameCalcolo 
      Height          =   6000
      Left            =   21600
      TabIndex        =   2
      Top             =   6360
      Visible         =   0   'False
      Width           =   10000
      _Version        =   65536
      _ExtentX        =   17639
      _ExtentY        =   10583
      _StockProps     =   14
      Caption         =   "frameCalcolo"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Song"
         Size            =   10.8
         Charset         =   134
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Begin VB.ComboBox McCombo1 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "MS Song"
            Size            =   16.8
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   420
         ItemData        =   "OEMX.frx":2A5F6
         Left            =   3900
         List            =   "OEMX.frx":2A5F8
         TabIndex        =   11
         Text            =   "Combo1"
         Top             =   300
         Width           =   750
      End
      Begin VB.CheckBox chkINPUT 
         Appearance      =   0  'Flat
         Caption         =   "POINTS"
         ForeColor       =   &H80000008&
         Height          =   240
         Index           =   0
         Left            =   135
         TabIndex        =   10
         Top             =   525
         Visible         =   0   'False
         Width           =   1050
      End
      Begin VB.TextBox txtHELP 
         Appearance      =   0  'Flat
         BackColor       =   &H00000000&
         ForeColor       =   &H00FFFFFF&
         Height          =   1185
         Left            =   270
         MultiLine       =   -1  'True
         ScrollBars      =   3  'Both
         TabIndex        =   9
         Text            =   "OEMX.frx":2A5FA
         Top             =   3465
         Visible         =   0   'False
         Width           =   3420
      End
      Begin VB.TextBox txtINPUT 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "MS Song"
            Size            =   16.8
            Charset         =   134
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Left            =   8725
         TabIndex        =   8
         Text            =   "txtINPUT"
         Top             =   300
         Width           =   800
      End
      Begin VB.CheckBox chkINPUT 
         Appearance      =   0  'Flat
         Caption         =   "GRID"
         ForeColor       =   &H80000008&
         Height          =   240
         Index           =   1
         Left            =   1170
         TabIndex        =   7
         Top             =   525
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.TextBox txtOY 
         Appearance      =   0  'Flat
         Height          =   330
         Left            =   2565
         TabIndex        =   6
         Text            =   "0"
         Top             =   300
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.TextBox txtOX 
         Appearance      =   0  'Flat
         Height          =   330
         Left            =   855
         TabIndex        =   5
         Text            =   "0"
         Top             =   300
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.CheckBox chkINPUT 
         Appearance      =   0  'Flat
         Caption         =   "AXIAL"
         ForeColor       =   &H80000008&
         Height          =   240
         Index           =   2
         Left            =   2250
         TabIndex        =   4
         Top             =   525
         Visible         =   0   'False
         Width           =   1050
      End
      Begin VB.PictureBox PicCALCOLO 
         Appearance      =   0  'Flat
         AutoRedraw      =   -1  'True
         BackColor       =   &H0080FF80&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   4700
         Left            =   135
         ScaleHeight     =   4666.071
         ScaleMode       =   0  'User
         ScaleWidth      =   9365.037
         TabIndex        =   3
         TabStop         =   0   'False
         Top             =   800
         Width           =   9400
         Begin GAPP4PC.SuGrid SuGridTempi 
            Height          =   1000
            Index           =   0
            Left            =   4000
            TabIndex        =   112
            TabStop         =   0   'False
            Top             =   135
            Width           =   4000
            _ExtentX        =   7070
            _ExtentY        =   1778
         End
         Begin GAPP4PC.SuGrid SuGridTempi 
            Height          =   1000
            Index           =   1
            Left            =   4000
            TabIndex        =   113
            TabStop         =   0   'False
            Top             =   1335
            Width           =   4000
            _ExtentX        =   7070
            _ExtentY        =   1778
         End
         Begin GAPP4PC.SuGrid SuGridTempi 
            Height          =   1000
            Index           =   2
            Left            =   4000
            TabIndex        =   114
            TabStop         =   0   'False
            Top             =   2610
            Width           =   4000
            _ExtentX        =   7070
            _ExtentY        =   1778
         End
      End
      Begin VB.Label lblINPUT 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H0080FFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "lblINPUT"
         BeginProperty Font 
            Name            =   "MS Song"
            Size            =   13.8
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   450
         Left            =   4750
         TabIndex        =   15
         Top             =   300
         Width           =   3970
      End
      Begin VB.Label lblMANDRINI 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H0080FFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "lblMANDRINI"
         BeginProperty Font 
            Name            =   "MS Song"
            Size            =   13.8
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   450
         Left            =   135
         TabIndex        =   14
         Top             =   300
         Width           =   3750
      End
      Begin VB.Label lblOY 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H0080FFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Oy"
         ForeColor       =   &H80000008&
         Height          =   330
         Left            =   1800
         TabIndex        =   13
         Top             =   300
         Visible         =   0   'False
         Width           =   780
      End
      Begin VB.Label lblOX 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H0080FFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Ox"
         ForeColor       =   &H80000008&
         Height          =   330
         Left            =   135
         TabIndex        =   12
         Top             =   300
         Visible         =   0   'False
         Width           =   780
      End
   End
   Begin VB.PictureBox PRG0PicLogo 
      BorderStyle     =   0  'None
      Height          =   405
      Left            =   9000
      Picture         =   "OEMX.frx":2A625
      ScaleHeight     =   7.197
      ScaleMode       =   6  'Millimeter
      ScaleWidth      =   7.62
      TabIndex        =   96
      TabStop         =   0   'False
      Top             =   11500
      Visible         =   0   'False
      Width           =   435
   End
   Begin GAPP4PC.SuOEM SuOEM1 
      Height          =   492
      Left            =   10200
      TabIndex        =   1
      Top             =   11880
      Visible         =   0   'False
      Width           =   672
      _ExtentX        =   1185
      _ExtentY        =   868
   End
   Begin LISTBOXMODLibCtl.McVBListBox MNG0lstPRODUZIONE 
      Height          =   780
      Left            =   5400
      TabIndex        =   192
      Top             =   12000
      Width           =   2500
      McVBListBox-InterfaceVersion=   1
      _cx             =   4403
      _cy             =   1376
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Song"
         Size            =   9
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BackColor       =   8454016
      ForeColor       =   -2147483640
      Appearance      =   0
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      RightToLeft     =   0   'False
      Sorted          =   0   'False
      Style           =   0
      IntegralHeight  =   0   'False
      MultiSelect     =   0
      Columns         =   0
   End
   Begin TabDlg.SSTab MNG0SSTab1 
      Height          =   3312
      Left            =   5280
      TabIndex        =   103
      Top             =   11040
      Visible         =   0   'False
      Width           =   4620
      _ExtentX        =   8149
      _ExtentY        =   5842
      _Version        =   393216
      Style           =   1
      Tabs            =   4
      Tab             =   3
      TabsPerRow      =   4
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.4
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "MEMO"
      TabPicture(0)   =   "OEMX.frx":4284F
      Tab(0).ControlEnabled=   0   'False
      Tab(0).ControlCount=   0
      TabCaption(1)   =   "Settings"
      TabPicture(1)   =   "OEMX.frx":42D25
      Tab(1).ControlEnabled=   0   'False
      Tab(1).ControlCount=   0
      TabCaption(2)   =   "Rotor Directory"
      TabPicture(2)   =   "OEMX.frx":431B7
      Tab(2).ControlEnabled=   0   'False
      Tab(2).ControlCount=   0
      TabCaption(3)   =   "PIANO PRODUZIONE"
      TabPicture(3)   =   "OEMX.frx":431D3
      Tab(3).ControlEnabled=   -1  'True
      Tab(3).ControlCount=   0
   End
   Begin VB.Label MNG0Combo3 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "MNG0Combo3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   372
      Left            =   1050
      TabIndex        =   127
      Top             =   10000
      Width           =   6252
   End
   Begin VB.Label PRG0TipoLavorazione 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   396
      Left            =   3000
      TabIndex        =   123
      Top             =   8500
      Width           =   5604
   End
   Begin VB.Image PRG0Image1 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   900
      Left            =   50
      Stretch         =   -1  'True
      Top             =   8500
      Width           =   900
   End
   Begin VB.Image MNG0Image3 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   492
      Left            =   50
      Picture         =   "OEMX.frx":431EF
      Stretch         =   -1  'True
      Top             =   10500
      Visible         =   0   'False
      Width           =   1008
   End
   Begin VB.Image MNG0Image4 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   492
      Left            =   50
      Picture         =   "OEMX.frx":46171
      Stretch         =   -1  'True
      Top             =   10000
      Visible         =   0   'False
      Width           =   1008
   End
   Begin VB.Image MNG0Image1 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   972
      Left            =   8280
      Picture         =   "OEMX.frx":490F3
      Stretch         =   -1  'True
      Top             =   14040
      Visible         =   0   'False
      Width           =   972
   End
   Begin VB.Label MNG0INTESTAZIONE 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Song"
         Size            =   9.6
         Charset         =   134
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   408
      Left            =   -72
      TabIndex        =   108
      Top             =   15540
      Visible         =   0   'False
      Width           =   4500
   End
   Begin VB.Label MNG0Label2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Label2"
      BeginProperty Font 
         Name            =   "MS Song"
         Size            =   9.6
         Charset         =   134
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   408
      Left            =   3756
      TabIndex        =   107
      Top             =   15540
      Visible         =   0   'False
      Width           =   3612
   End
   Begin VB.Image MNG0Image2 
      Appearance      =   0  'Flat
      Height          =   492
      Left            =   8280
      Stretch         =   -1  'True
      Top             =   10440
      Visible         =   0   'False
      Width           =   1000
   End
   Begin VB.Label MNG0lblINDICE 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H000000FF&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "MS Song"
         Size            =   20.4
         Charset         =   134
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   468
      Left            =   7392
      TabIndex        =   106
      Top             =   15540
      Visible         =   0   'False
      Width           =   720
   End
   Begin VB.Label PRG0lblINDICE 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H000000FF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "A"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   16.8
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   396
      Left            =   50
      TabIndex        =   99
      Top             =   9500
      Width           =   888
   End
   Begin VB.Label PRG0Label5 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   396
      Left            =   950
      TabIndex        =   101
      Top             =   9000
      Width           =   2004
   End
   Begin VB.Label PRG0lblMATRICOLA 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H0000FFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "MATR."
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   396
      Left            =   950
      TabIndex        =   100
      Top             =   9500
      Visible         =   0   'False
      Width           =   2004
   End
   Begin VB.Label PRG0lblLOGIN 
      AutoSize        =   -1  'True
      BackColor       =   &H00EBE1D7&
      Caption         =   "LOGIN"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   13.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   312
      Left            =   4500
      TabIndex        =   98
      Top             =   11500
      Width           =   864
   End
End
Attribute VB_Name = "OEMX"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim OldtxtDonnee  As String
Dim Sposta        As Boolean
Dim tempX         As Single
Dim tempY         As Single

Sub AGGIORNA_OEM1FrameCalcolo()
'
Dim k     As Integer
Dim ret   As Integer
Dim stmp  As String
Dim XX    As Double
Dim sXX   As String
Dim PUNTI As Integer
'
On Error GoTo errAGGIORNA_OEM1FrameCalcolo
  '
  If fraTOLLERANZA.Visible = True Then fraTOLLERANZA.Visible = False
  OEMX.OEM1Combo1.Caption = OEMX.OEM1ListaCombo1.List(OEMX.OEM1ListaCombo1.ListIndex)
  '
  Select Case ACTUAL_PARAMETER_GROUP
    
    Case "OEM1_MOLA86", "OEM1_RULLO86", "OEM1_PROFILO86", "OEM1_LAVORO86", "OEM1_CORRELICA86", "OEM1_CORRETTORI86" ' MOLA_V
      Select Case OEMX.OEM1ListaCombo1.ListIndex
        Case 0, 1, 2, 3, 4:     Call CALCOLO_INGRANAGGIO_ESTERNO_380_MV(0) ' 0=MOLAVITE
      End Select
    
    Case "OEM1_MOLA85", "OEM1_RULLO85", "OEM1_PROFILO85", "OEM1_LAVORO85", "OEM1_CORRELICA85", "OEM1_CORRETTORI85" ' MOLA_V
      Select Case OEMX.OEM1ListaCombo1.ListIndex
        Case 0, 1, 2, 3, 4:     Call CALCOLO_INGRANAGGIO_ESTERNO_380_MV(0) ' 0=MOLAVITE
      End Select
    '
    'VISUALIZZAZIONE CANALE PER LE VITI CONICHE
    Case "OEM1_MOLA41", "OEM1_MOLA42", "OEM1_MOLA43", "OEM1_MOLA44", "OEM1_MOLA45", "OEM1_MOLA46", "OEM1_MOLA47", "OEM1_MOLA47A", "OEM1_MOLA47B", "OEM1_MOLA47C", "OEM1_MOLA49", "OEM1_MOLA50", "OEM1_MOLA50A", "OEM1_MOLA50B", "OEM1_MOLA50C"
      k = OEMX.OEM1ListaCombo1.ListIndex
      ret = WritePrivateProfileString("MOLA_VITI_CONICHE", "INDICE_COMBO", Format$(k, "##0"), Path_LAVORAZIONE_INI)
      OEMX.OEM1PicCALCOLO.BackColor = &HFFFFFF
      OEMX.OEM1PicCALCOLO.Cls
      Call VISUALIZZA_CANALE("N")
      '
    Case "OEM1_PROFILO6", "OEM1_PROFILO91", "OEM1_PROFILOK91", "OEM1_LAVORO91", "OEM1_CORRETTORI91", "OEM1_MOLA91", "OEM1_MOLAB91"
      Select Case OEMX.OEM1ListaCombo1.ListIndex
        Case 0, 1, 2, 3, 4:     Call CALCOLO_INGRANAGGIO_ESTERNO_380(0)
        Case 5:                 Call VERIFICA_PROFILO_INGRANAGGIO_ESTERNO_380(0)
        Case 6:                 Call CALCOLO_INGRANAGGIO_ESTERNO_SAP_380(0)
      End Select
    
    Case "OEM1_PROFILO92", "OEM1_PROFILOK92", "OEM1_LAVORO92", "OEM1_CORRETTORI92", "OEM1_MOLA92", "OEM1_MOLAB92"
      Select Case OEMX.OEM1ListaCombo1.ListIndex
        Case 0, 1, 2, 3, 4:     Call CALCOLO_INGRANAGGIO_ESTERNO_380(0)
        Case 5:                 Call VERIFICA_PROFILO_INGRANAGGIO_ESTERNO_380(0)
      End Select
    
    Case "OEM1_PROFILO13", "OEM1_PROFILOK13", "OEM1_LAVORO13", "OEM1_CORRETTORI13", "OEM1_MOLA13", "OEM1_MOLAB13"
      Select Case OEMX.OEM1ListaCombo1.ListIndex
        Case 0, 1, 2, 3, 4:     Call CALCOLO_INGRANAGGIO_ESTERNO_380(0)
        Case 5:                 Call VERIFICA_PROFILO_INGRANAGGIO_ESTERNO_380(0)
        Case 6:                 Call CALCOLO_INGRANAGGIO_ESTERNO_SAP_380(0)
      End Select
      '
    Case "OEM1_PROFILO33"
      Select Case OEMX.OEM1ListaCombo1.ListIndex
        Case 0:                 Call CALCOLO_SCANALATO(0)
      End Select
      '
    Case "OEM1_PROFILO2", "OEM1_PROFILOK2", "OEM1_MOLA2", "OEM1_MOLAB2"
                                Call CALCOLO_INGRANAGGIO_INTERNO(0)
      '
    Case "OEM1_MOLA7"
      Select Case OEMX.OEM1ListaCombo1.ListIndex
          '
        Case 0
          OEMX.OEM1hsROTORE.Visible = False
          OEMX.OEM1hsMOLA.Visible = False
          OEMX.OEM1hsMOLA1.Visible = False
          OEMX.OEM1hsPROFILO.Visible = False
          '
          OEMX.OEM1Label2.Visible = False
          OEMX.OEM1Command2.Visible = False
          OEMX.OEM1fraOpzioni.Visible = False
          '
          OEMX.OEM1lblOX.Visible = True
          OEMX.OEM1lblOY.Visible = True
          OEMX.OEM1txtOX.Visible = True
          OEMX.OEM1txtOY.Visible = True
          '
          OEMX.lblINP(0).Caption = "SCALE"
          OEMX.lblINP(1).Caption = "Rext"
          OEMX.lblINP(3).Visible = False
          '
          OEMX.txtINP(0).Text = GetInfo("DIS0", "ScalaMOLA", Path_LAVORAZIONE_INI)
          OEMX.txtINP(1).Text = GetInfo("DIS0", "RaggioMola", Path_LAVORAZIONE_INI)
          OEMX.txtINP(3).Visible = False
          '
          If val(OEMX.txtINP(1).Text) <= 0 Then OEMX.txtINP(1).Text = "125"
          '
          Call CALCOLO_MOLA_PROFILO_RAB_ESTERNI(2 * val(OEMX.txtINP(1).Text), 0)
          Call VISUALIZZA_MOLA_RAB(OEMX, val(OEMX.txtINP(0).Text), "N", OEMX.OEM1chkINPUT(0).Value, OEMX.OEM1chkINPUT(1).Value, val(OEMX.OEM1txtOX.Text), val(OEMX.OEM1txtOY.Text))
          '
        Case 1
          '
          OEMX.OEM1hsROTORE.Visible = False
          OEMX.OEM1hsMOLA.Visible = False
          OEMX.OEM1hsMOLA1.Visible = False
          OEMX.OEM1hsPROFILO.Visible = False
          '
          OEMX.OEM1Label2.Visible = True
          OEMX.OEM1Command2.Visible = True
          OEMX.OEM1fraOpzioni.Visible = False
          '
          OEMX.OEM1lblOX.Visible = False
          OEMX.OEM1lblOY.Visible = False
          OEMX.OEM1txtOX.Visible = False
          OEMX.OEM1txtOY.Visible = False
          '
          OEMX.lblINP(0).Caption = "Scale X"
          OEMX.lblINP(1).Caption = "Scale Y"
          OEMX.lblINP(3).Caption = "Res"
          '
          OEMX.txtINP(0).Text = GetInfo("DIS0", "ScalaCORREZIONIX", Path_LAVORAZIONE_INI)
          OEMX.txtINP(1).Text = GetInfo("DIS0", "ScalaCORREZIONIY", Path_LAVORAZIONE_INI)
          OEMX.txtINP(3).Text = GetInfo("DIS0", "RISOLUZIONE", Path_LAVORAZIONE_INI)
          '
          stmp = GetInfo("DIS0", "RaggioMola", Path_LAVORAZIONE_INI)
          Call CALCOLO_MOLA_PROFILO_RAB_ESTERNI(val(stmp) * 2, 0)
          Call VERIFICA_PROFILO_RAB_INGRANAGGIO(val(stmp), "N")
          '
      End Select
      '
    Case "OEM1_LAVORO71"
      Call txtINP_KeyUp(0, 13, 0)
      '
    Case "OEM1_PROFILO71"
      Select Case OEMX.OEM1ListaCombo1.ListIndex
        '
        Case 0
          OEMX.OEM1chkINPUT(0).Visible = True
          OEMX.OEM1chkINPUT(1).Visible = True
          OEMX.OEM1chkINPUT(2).Visible = True
          OEMX.OEM1hsROTORE.Visible = False
          OEMX.OEM1hsMOLA.Visible = False
          OEMX.OEM1hsMOLA1.Visible = False
          OEMX.OEM1hsPROFILO.Visible = False
          OEMX.Timer1.Enabled = True
          '
        Case 1
          OEMX.Timer1.Enabled = False
          OEMX.OEM1frameCalcolo.Caption = OEMX.OEM1Combo1.Caption
          OEMX.OEM1PicCALCOLO.Cls
          OEMX.OEM1chkINPUT(0).Visible = False
          OEMX.OEM1chkINPUT(1).Visible = False
          OEMX.OEM1chkINPUT(2).Visible = False
          Call VISUALIZZA_SOVRAMETALLO_ROTORI("N")
          '
      End Select
      '
    Case "OEM1_MOLA71"
          
      Select Case OEMX.OEM1ListaCombo1.ListIndex
        '
        Case 0
        OEMX.OEM1txtHELP.Visible = False
        OEMX.OEM1PicCALCOLO.Visible = True
        OEMX.OEM1chkINPUT(0).Visible = True
        OEMX.OEM1chkINPUT(1).Visible = True
        OEMX.OEM1chkINPUT(2).Visible = False
        OEMX.lblINP(0).Visible = True
        OEMX.txtINP(0).Visible = True
        OEMX.lblINP(1).Visible = True
        OEMX.txtINP(1).Visible = True
        OEMX.OEM1lblOX.Visible = True
        OEMX.OEM1lblOY.Visible = True
        OEMX.OEM1txtOX.Visible = True
        OEMX.OEM1txtOY.Visible = True
        OEMX.OEM1hsMOLA.Visible = True
        Call CALCOLO_PROFILO_MOLA_ROTORI(2 * val(OEMX.txtINP(1).Text), 0)
        Call VISUALIZZA_MOLA_ROTORI(OEMX.OEM1PicCALCOLO, val(OEMX.txtINP(0).Text), OEMX.OEM1chkINPUT(0).Value, OEMX.OEM1chkINPUT(1).Value, val(OEMX.OEM1txtOX.Text), val(OEMX.OEM1txtOY.Text))
        
        Case 1
        OEMX.OEM1PicCALCOLO.Visible = False
        OEMX.OEM1chkINPUT(0).Visible = False
        OEMX.OEM1chkINPUT(1).Visible = False
        OEMX.OEM1chkINPUT(2).Visible = False
        OEMX.lblINP(0).Visible = False
        OEMX.txtINP(0).Visible = False
        OEMX.lblINP(1).Visible = False
        OEMX.txtINP(1).Visible = False
        OEMX.OEM1chkINPUT(0).Visible = False
        OEMX.OEM1chkINPUT(1).Visible = False
        OEMX.OEM1lblOX.Visible = False
        OEMX.OEM1lblOY.Visible = False
        OEMX.OEM1txtOX.Visible = False
        OEMX.OEM1txtOY.Visible = False
        OEMX.OEM1hsMOLA.Visible = False
        OEMX.OEM1hsMOLA1.Visible = False
        'CARATTERE DI STAMPA TESTO
        For i = 1 To 2
          OEMX.OEM1txtHELP.FontName = "Courier New"
          OEMX.OEM1txtHELP.FontSize = 10
          OEMX.OEM1txtHELP.FontBold = False
          OEMX.OEM1txtHELP.BackColor = &H0&
          OEMX.OEM1txtHELP.ForeColor = &HFFFFFF
        Next i
        OEMX.OEM1txtHELP.Visible = True
        Call OEMX.OEM1txtHELP.Move(OEMX.OEM1PicCALCOLO.Left, OEMX.OEM1PicCALCOLO.Top, OEMX.OEM1PicCALCOLO.Width, OEMX.OEM1PicCALCOLO.Height + OEMX.OEM1Combo1.Height)
        OEMX.OEM1txtHELP.Text = ""
        PUNTI = Lp("R[147]")
          stmp = Left$("ii]" & String(5, " "), 5) & " "
          stmp = stmp & Left$(" XX " & String(10, " "), 10)
          stmp = stmp & " "
          stmp = stmp & Left$(" YY " & String(10, " "), 10)
          OEMX.OEM1txtHELP.Text = OEMX.OEM1txtHELP.Text & stmp & vbCrLf
          OEMX.OEM1txtHELP.Text = OEMX.OEM1txtHELP.Text & String(27, "-") & vbCrLf
        For i = 0 To PUNTI + 1
          stmp = Left$(Format$(i, "##0") & "]" & String(5, " "), 5) & " "
          sXX = GetInfo("CORREZIONI", "COXX[0," & Format$(i, "##0") & "]", g_chOemPATH & "\SEIM.INI")
          XX = val(sXX)
          If (XX >= 0) Then
            stmp = stmp & Left$("+" & frmt(Abs(XX), 4) & String(10, " "), 10)
          Else
            stmp = stmp & Left$("-" & frmt(Abs(XX), 4) & String(10, " "), 10)
          End If
          stmp = stmp & " "
          sXX = GetInfo("CORREZIONI", "COYY[0," & Format$(i, "##0") & "]", g_chOemPATH & "\SEIM.INI")
          XX = val(sXX)
          If (XX >= 0) Then
            stmp = stmp & Left$("+" & frmt(Abs(XX), 4) & String(10, " "), 10)
          Else
            stmp = stmp & Left$("-" & frmt(Abs(XX), 4) & String(10, " "), 10)
          End If
          OEMX.OEM1txtHELP.Text = OEMX.OEM1txtHELP.Text & stmp & vbCrLf
        Next i
      End Select
      '
  End Select
  '
Exit Sub

errAGGIORNA_OEM1FrameCalcolo:
  WRITE_DIALOG "Sub AGGIORNA_OEM1FrameCalcolo: ERROR -> " & Err
  Exit Sub

End Sub

Public Sub EditorTesto_STAMPA()
'
Dim X    As Single
Dim i    As Integer
Dim k1   As Integer
Dim k2   As Integer
Dim k3   As Integer
Dim k4   As Integer
Dim K5   As Integer
Dim j    As Integer
Dim rsp  As Integer
'
Dim MLeft As Single
Dim MTop  As Single
Dim Box0W As Single
Dim Box0H As Single
'
'Parametri per la stampa di linee
Dim X1 As Single
Dim Y1 As Single
Dim X2 As Single
Dim Y2 As Single
'
'Area di stampa dei grafici
Dim Box1L As Single
Dim Box1T As Single
Dim Box1W As Single
Dim Box1H As Single
'
Dim iRG As Single
Dim iCl As Single
'
Dim riga As String
Dim stmp As String
'
On Error Resume Next
  '
  Printer.ScaleMode = 7
  Call ImpostaPrinter("Courier New", 10)
  MLeft = 2: MTop = 1
  Box0W = 17: Box0H = 27
  iRG = Printer.TextHeight("A")
  iCl = Printer.TextWidth("A")
  Box1L = 5: Box1T = 1
  Box1W = 12: Box1H = 12
  '
  GoSub STAMPA_INTESTAZIONE
  '
  Dim NrigheMax As Integer
  Dim iiRG      As Integer
  Dim iiPG      As Integer
  Dim NrigheTot As Integer
  '
  NrigheTot = NoOfLines(OEMX.MNG0EditorTesto)
  NrigheMax = (Box0H - 3) / iRG
  iiPG = 1: riga = "": i = 0: k1 = 1: k4 = 1
  For K5 = 1 To NrigheTot - 1
    'terminatore di riga
    k2 = InStr(k1, OEMX.MNG0EditorTesto.Text, Chr(13))
    k3 = InStr(k1, OEMX.MNG0EditorTesto.Text, Chr(10))
    'prendo il minimo
    If (k3 >= k2) Then k4 = k2 Else k4 = k3
    riga = Mid$(OEMX.MNG0EditorTesto.Text, k1, k4 - k1)
    If (k3 >= k2) Then k1 = k3 + 1 Else k1 = k2 + 1
    '
    i = i + 1
    '
    iiRG = (i - 1) Mod NrigheMax
    Printer.CurrentX = X1 + iCl
    Printer.CurrentY = Y1 + (iiRG + 3) * iRG
    riga = Right(String(6, " ") & Format$(i - 1, "#####0"), 6) & "] " & riga
    Printer.Print riga
    '
    If (i = iiPG * NrigheMax) Then
      Printer.NewPage
      GoSub STAMPA_INTESTAZIONE
      iiPG = iiPG + 1
    End If
    '
  Next K5
  Printer.EndDoc
  '
Exit Sub

STAMPA_INTESTAZIONE:
  'Squadratura della pagina
  X1 = MLeft: Y1 = MTop
  X2 = MLeft + Box0W: Y2 = MTop + Box0H
  'Intestazione della pagina
  Printer.CurrentX = X1 + iCl
  Printer.CurrentY = Y1 + iRG
  stmp = ""
  stmp = stmp & " Date: " & Format$(Now, "dd/mm/yy")
  stmp = stmp & " Time: " & Format$(Now, "hh:mm:ss")
  Printer.Print OEMX.MNG0INTESTAZIONE.Caption & " [" & OEMX.MNG0Text1.Text & "] " & stmp
  'Linea di separazione
  X1 = MLeft: Y1 = MTop + 1
  X2 = MLeft + Box0W: Y2 = MTop + 1
  Printer.Line (X1, Y1)-(X2, Y2), QBColor(0)
  Printer.CurrentX = X1 + iCl
  Printer.CurrentY = Y1 + 1 * iRG
  Printer.Print OEMX.MNG0Combo3.Caption
Return

End Sub

Private Sub chkDonnee_Click()
'
Dim ret As Long
'
On Error Resume Next
  '
  ret = val(GetInfo("CALCOLO", "vREVERSE", Path_LAVORAZIONE_INI))
  If (chkDonnee.Value <> ret) Then
    ret = WritePrivateProfileString("CALCOLO", "vREVERSE", Format$(chkDonnee.Value, "#0"), Path_LAVORAZIONE_INI)
    Call VERIFICA_PROFILO_INGRANAGGIO_ESTERNO_380(0)
  End If

End Sub

Private Sub chkDonnee_GotFocus()
'
On Error Resume Next
  '
  chkDonnee.BackColor = QBColor(12)
  chkDonnee.ForeColor = QBColor(15)

End Sub

Private Sub chkDonnee_LostFocus()
'
On Error Resume Next
  '
  chkDonnee.BackColor = vbWhite
  chkDonnee.ForeColor = vbBlack
  '
End Sub

Private Sub chkV1_KeyUp(Index As Integer, KeyCode As Integer, Shift As Integer)
'
Dim stmp1 As String
Dim stmp2 As String
Dim stmp3 As String
  '
  If (KeyCode = 32) Or (KeyCode = 13) Then
    stmp1 = GetInfo("SEQUENZA_CONTROLLO", "chkDDE(" & Format$(Index + 1) & ")", Path_LAVORAZIONE_INI)
    stmp1 = Trim$(stmp1)
    If (Len(stmp1) > 0) Then
      '
      If (OEMX.chkV1(Index).Value > 0) Then
        Call OPC_SCRIVI_DATO(stmp1, "0")
        OEMX.chkV1(Index).BackColor = QBColor(12)
        OEMX.chkV1(Index).ForeColor = &HFFFFFF
      Else
        Call OPC_SCRIVI_DATO(stmp1, "1")
        OEMX.chkV1(Index).BackColor = &HFFFFFF
        OEMX.chkV1(Index).ForeColor = &H80000008
      End If
      '
      If (KeyCode = 13) Then
        stmp3 = OPC_LEGGI_DATO(stmp1)
        If (val(stmp3) > 0) Then
          OEMX.chkV1(Index).Value = 1
          OEMX.chkV1(Index).BackColor = &HFFFFFF
          OEMX.chkV1(Index).ForeColor = &H80000008
        Else
          OEMX.chkV1(Index).Value = 0
          OEMX.chkV1(Index).BackColor = QBColor(12)
          OEMX.chkV1(Index).ForeColor = &HFFFFFF
        End If
      End If
      '
    End If
  End If
  
End Sub

Private Sub chkV1_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)

  Call chkV1_KeyUp(Index, 13, Shift)
  
End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
'
Dim Index As Integer
'
On Error Resume Next
  '
  If (Shift = 2) Then
    Index = KeyCode - 112
    If (Len(MDIForm1.Picture3(Index).Caption) > 0) Or (MDIForm1.Picture3(Index).Picture <> 0) Then
      Call MDIForm1.Picture3_Click(Index)
    End If
  End If
  '
End Sub

Private Sub HGkineticF_Click(Index As Integer)

 If (OEMX.HGkineticF(Index).Visible) Then
 Select Case (Index)
 Case (0)
  If (OEMX.HGkineticF(Index).Value = 1) Then
   OEMX.HGkineticF(Index).Caption = "WC": OEMX.HGkineticF(Index).ToolTipText = "With Correction" ':  OEMX.HGkineticF(1).Left = 7400 'OEMX.HGkineticF(1).Top = 360
  Else
   OEMX.HGkineticF(Index).Caption = "WOC": OEMX.HGkineticF(Index).ToolTipText = "WithOut Correction" ': OEMX.HGkineticF(1).Left = 7400  'OEMX.HGkineticF(1).Top = 360
  End If
  
 Case (1)
  If (OEMX.HGkineticF(Index).Value = 1) Then
   OEMX.HGkineticF(Index).Caption = "HQS": OEMX.HGkineticF(Index).ToolTipText = "Hight Quality Simulation" ': OEMX.HGkineticF(0).Left = 7400 'OEMX.HGkineticF(1).Top = 240
  Else
   OEMX.HGkineticF(Index).Caption = "LQS": OEMX.HGkineticF(Index).ToolTipText = "Low Quality Simulation" ': OEMX.HGkineticF(0).Left = 7400  'OEMX.HGkineticF(1).Top = 120
  End If

 End Select
  If (hndHGkinetic <> 0) Then
   HGkinetic_cmdCloseWindow
  End If
 '-------------------------------------
  OEMX.HGkineticF(0).Visible = True
 OEMX.HGkineticF(1).Visible = True
 '-------------------------------------
  '-------------------------------------
      HGkinetic_XMLpopulate
      HGkinetic_cmdOpenWindow
'-------------------------------------
 End If

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  '
  Call GENERA_EVENTO_CLICK(KeyCode, Shift)
  '
End Sub

Private Sub McListAzioni_DblClick(Index As Integer)
'
Dim k      As Integer
Dim stmp   As String
Dim Valore As String
Dim ret    As Long
'
On Error Resume Next
  '
  If (Index = 0) Then
    k = McListAzioni(Index).ListIndex
    stmp = GetInfo("OPZIONI_ONLINE", "OPZIONE(" & Format$(k, "###0") & ")", Path_LAVORAZIONE_INI)
    Valore = OPC_LEGGI_DATO(stmp)
    If (Valore = 1) Then
      Call OPC_SCRIVI_DATO(stmp, "0")
    Else
      Call OPC_SCRIVI_DATO(stmp, "1")
    End If
  End If

End Sub

Private Sub McListAzioni_GotFocus(Index As Integer)
'
On Error Resume Next
  '
  McListAzioni(0).BackColor = &HFFFFFF
  McListAzioni(Index).ListIndex = 0
  '
End Sub

Private Sub McListAzioni_KeyUp(Index As Integer, KeyCode As Integer, Shift As Integer)
'
Dim k      As Integer
Dim stmp   As String
Dim Valore As String
Dim ret    As Long
'
On Error Resume Next
  '
  If (Index = 0) And ((KeyCode = 13) Or (KeyCode = 32)) Then
    k = McListAzioni(Index).ListIndex
    stmp = GetInfo("OPZIONI_ONLINE", "OPZIONE(" & Format$(k, "###0") & ")", Path_LAVORAZIONE_INI)
    Valore = OPC_LEGGI_DATO(stmp)
    If (Valore = 1) Then
        Call OPC_SCRIVI_DATO(stmp, "0")
        McListAzioni(Index).Selected(k) = False
    Else
        Call OPC_SCRIVI_DATO(stmp, "1")
        McListAzioni(Index).Selected(k) = True
    End If
  End If

End Sub

Private Sub McListAzioni_LostFocus(Index As Integer)
'
On Error Resume Next
  '
  McListAzioni(0).BackColor = &HEBE1D7
  OEMX.McListAzioni(0).Enabled = False
  OEMX.McListAzioni(0).TabStop = False
  '
End Sub

Private Sub McListPrg_KeyUp(KeyCode As Integer, Shift As Integer)
'
On Error Resume Next
  '
  If (KeyCode = 13) Then
    OEMX.McListAzioni(0).Enabled = True
    OEMX.McListAzioni(0).TabStop = True
    OEMX.McListAzioni(0).SetFocus
  End If
  '
End Sub

Private Sub MIX1cmbMacroLav_Click()
'
On Error Resume Next
  '
  Call MIX1cmbMacroLav_OnSelChange(MIX1cmbMacroLav.ListIndex)
  '
End Sub

Private Sub MNG0EditorTesto_DblClick()
'
Static LARGO As Integer
  '
  If (LARGO = 0) Then
    LARGO = 1
    OEMX.MNG0EditorTesto.Width = OEMX.MNG0List1(0).Width + OEMX.MNG0SSTab1.Width
  Else
    LARGO = 0
    OEMX.MNG0EditorTesto.Width = OEMX.MNG0List1(0).Width
  End If
  '
End Sub

Private Sub MNG0lstPRODUZIONE_DblClick()
'
On Error Resume Next
  '
  Call MNG0lstPRODUZIONE_KeyDown(13, 0)
  
End Sub

Private Sub MNG0lstPRODUZIONE_KeyDown(KeyCode As Integer, Shift As Integer)
'
Dim NomeFilePIANO As String
Dim CODICE_LOTTO  As String
Dim CODICE_PEZZO  As String
Dim RICHIESTA     As String
Dim riga          As String
Dim stmp          As String
Dim i             As Integer
Dim iLISTA        As Integer
Dim k1            As Integer
Dim k2            As Integer
Dim j             As Integer
Dim LUNGHEZZA()   As String
Dim iSEQ()        As String
Dim stmp1         As String
Dim stmp2         As String
Dim k2I           As Integer
Dim k2D           As Integer
Dim TIPO_LAVORAZIONE As String
'
On Error Resume Next
  '
  If (KeyCode = 13) Then
    NomeFilePIANO = GetInfo("PRODUZIONE", "NomeFilePIANO", Path_LAVORAZIONE_INI)
    NomeFilePIANO = UCase$(Trim$(NomeFilePIANO))
    If (Len(NomeFilePIANO) > 0) Then
      TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
      TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
      NomeFilePIANO = g_chOemPATH & "\PRODUZIONE\" & TIPO_LAVORAZIONE & "\" & NomeFilePIANO
      If (Dir$(NomeFilePIANO) <> "") Then
        iLISTA = OEMX.MNG0lstPRODUZIONE.ListIndex
        If (iLISTA > 0) Then
          i = 0
          Open NomeFilePIANO For Input As #1
          Do While Not EOF(1)
            i = i + 1
            Line Input #1, stmp
            If (i = iLISTA) Then
              stmp1 = GetInfo("PRODUZIONE", "LUNGHEZZE", Path_LAVORAZIONE_INI)
              LUNGHEZZA = Split(stmp1, ";")
              stmp1 = GetInfo("PRODUZIONE", "lblINPUT", Path_LAVORAZIONE_INI)
              iSEQ = Split(stmp1, ";")
              k1 = 1: riga = ""
              For j = 0 To UBound(iSEQ)
                If (j = val(iSEQ(j))) Then
                  stmp1 = GetInfo("PRODUZIONE", "ETICHETTA(" & Format$(j, "#0") & ")", Path_LAVORAZIONE_INI)
                  stmp1 = Trim$(stmp1)
                  If (Len(stmp1) > 0) Then
                    riga = riga & " " & stmp1
                    Call RICAVO_INDICI(LUNGHEZZA(j), k2I, k2D)
                    k2 = k2I + k2D
                    stmp2 = Mid$(stmp, k1, k2)
                    k1 = k1 + k2
                    If (k2D <= 0) Then
                      riga = riga & ": " & stmp2 & Chr(13)
                    Else
                      riga = riga & ": " & Left$(stmp2, k2I) & "." & Right$(stmp2, k2D) & Chr(13)
                    End If
                  End If
                End If
              Next j
              Call MsgBox(riga, vbInformation, OEMX.MNG0SSTab1.TabCaption(3) & ": item " & Format$(iLISTA, "#######0"))
              Call SetFocusFRM
              Exit Do
            End If
          Loop
          Close #1
        End If
      End If
    End If
  End If
  
End Sub

Private Sub OEM1Combo1_Click()

On Error Resume Next

  If OEMX.OEM1ListaCombo1.Visible = False Then
    OEMX.OEM1ListaCombo1.Visible = True
  Else
    OEMX.OEM1ListaCombo1.Visible = False
  End If

End Sub

Private Sub OEM1ListaCombo1_Click()

On Error Resume Next

  Call AGGIORNA_OEM1FrameCalcolo

End Sub

Private Sub OEM1ListaCombo1_DblClick()

On Error Resume Next

  Call OEM1Combo1_Click
  
End Sub

Private Sub OEM1ListaCombo1_KeyDown(KeyCode As Integer, Shift As Integer)

On Error Resume Next

  If (KeyCode = 13) Then
    OEMX.OEM1ListaCombo1.Visible = False
    OEMX.OEM1Combo1.SetFocus
  End If
  
End Sub

Private Sub MNG0Image4_Click()
'
Dim NomeFileProduzione As String
Dim TIPO_LAVORAZIONE   As String
Dim ret                As Long
Dim stmp               As String
Dim Atmp               As String * 255
'
On Error Resume Next
  '
  NomeFileProduzione = GetInfo("Configurazione", "RISULTATI", Path_LAVORAZIONE_INI)
  NomeFileProduzione = UCase$(Trim$(NomeFileProduzione))
  If (Len(NomeFileProduzione) <= 0) Then Exit Sub
  If (Dir$(g_chOemPATH & "\" & NomeFileProduzione) = "") Then Exit Sub
  '
  If (OEMX.MNG0EditorTesto.Visible = True) Then
    OEMX.MNG0EditorTesto.Visible = False
    'RIMETTO A POSTO IL TESTO
    ret = LoadString(g_hLanguageLibHandle, 1211, Atmp, 255)
    If (ret > 0) Then stmp = Left$(Atmp, ret) Else stmp = "No name at " & stmp
    OEMX.MNG0INTESTAZIONE.Caption = stmp
    'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
    TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
    TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
    'NOME PEZZO CARICATO
    OEMX.MNG0Label2.Caption = LEGGI_NomePezzo(TIPO_LAVORAZIONE)
    OEMX.MNG0Label2.BackColor = &HFFFF&
    OEMX.MNG0Label2.ForeColor = &HFF0000
    OEMX.MNG0INTESTAZIONE.BackColor = &HFFFF&
    OEMX.MNG0INTESTAZIONE.ForeColor = &HFF0000
  Else
    OEMX.MNG0EditorTesto.Text = ""
    OEMX.MNG0EditorTesto.Font = "Ms Song"
    OEMX.MNG0EditorTesto.Left = 0
    OEMX.MNG0EditorTesto.Top = OEMX.MNG0Image3.Top
    OEMX.MNG0EditorTesto.Width = OEMX.MNG0List1(0).Width + OEMX.MNG0SSTab1.Width
    OEMX.MNG0EditorTesto.Height = OEMX.MNG0Image3.Height + OEMX.MNG0List1(0).Height
    Open g_chOemPATH & "\" & NomeFileProduzione For Input As #1
        OEMX.MNG0EditorTesto.Text = Input$(LOF(1), 1)
    Close #1
    OEMX.MNG0EditorTesto.Visible = True
    OEMX.MNG0Label2.Caption = CStr(CurrentLine(MNG0EditorTesto)) & "/" & CStr(NoOfLines(MNG0EditorTesto))
    OEMX.MNG0INTESTAZIONE.Caption = NomeFileProduzione
    OEMX.MNG0EditorTesto.SetFocus
    OEMX.MNG0Label2.BackColor = &HFF&
    OEMX.MNG0Label2.ForeColor = &HFFFFFF
    OEMX.MNG0INTESTAZIONE.BackColor = &HFF&
    OEMX.MNG0INTESTAZIONE.ForeColor = &HFFFFFF
  End If
  
End Sub

Private Sub OEM1hsMola_Change()
'
On Error Resume Next
  '
  'LETTURA DEGLI INDICI DEI PUNTI DI RIFERIMENTO PER LO ZERO PEZZO
  OEMX.OEM1txtOX.Text = Format$(OEMX.OEM1hsMOLA.Value, "######0")
  OEMX.OEM1txtOY.Text = Format$(OEMX.OEM1hsMOLA.Value, "######0")
  '
  Call VISUALIZZA_MOLA_ROTORI(OEMX.OEM1PicCALCOLO, val(OEMX.txtINP(0).Text), OEMX.OEM1chkINPUT(0).Value, OEMX.OEM1chkINPUT(1).Value, val(OEMX.OEM1txtOX.Text), val(OEMX.OEM1txtOY.Text))
  '
End Sub

Private Sub OEM1hsMOLA1_Change()
  '
  Call Riduci_Gioco_Testa_Rullo_MOLAVITE
  '
End Sub

Private Sub OEM1hsPROFILO_Change()
'
On Error Resume Next
  '
  Call VISUALIZZA_SOVRAMETALLO_ROTORI("N")
  '
End Sub

Private Sub MIX1List1_Click(Index As Integer)
'
Dim iCNC As Integer
Dim stmp As String
'
On Error Resume Next
  '
  Select Case Index
    '
    Case 0: INDICE_LAVORAZIONE_SINGOLA = MIX1List1(0).ListIndex + 1
      '
    Case 1:
      '
    Case 2
      iCNC = OEMX.MIX1List1(Index).ListIndex
      OEMX.MIX1Text1.Text = Trim$(OEMX.MIX1List1(Index).List(iCNC))
      Call MIX1_TROVA_MEMO(OEMX.MIX1Text1.Text)
      Call MIX1_AggiornaListaCNC
      OEMX.MIX1Text1.SetFocus
      '
    Case 3
      iCNC = OEMX.MIX1List1(Index).ListIndex
      stmp = Chr$(64 + iCNC + 1)
      Call Change_SkTextOnScr(3, "CNC " & stmp)
      '
  End Select
  '
End Sub

Private Sub MIX1List1_DblClick(Index As Integer)
'
On Error Resume Next
  '
  Select Case Index
      '
    Case 0
      '
    Case 1
      '
    Case 2
      OEMX.MIX1Text1.Text = Trim$(OEMX.MIX1List1(Index).List(OEMX.MIX1List1(Index).ListIndex))
      Call MIX1_TROVA_MEMO(OEMX.MIX1Text1.Text)
      OEMX.MIX1Text1.SetFocus
      '
    Case 3
      '
  End Select
  '
End Sub

Private Sub MIX1List1_GotFocus(Index As Integer)
'
Dim i     As Integer
Dim tCol  As Long
'
On Error Resume Next
  '
  Select Case Index
    '
    Case 0
      i = OEMX.MIX1List1(0).ListIndex
      If (i = -1) And (OEMX.MIX1List1(0).ListCount > 0) Then
        OEMX.MIX1List1(0).ListIndex = 0
        i = OEMX.MIX1List1(0).ListIndex
      End If
      Call MIX1_TROVA_MEMO(OEMX.MIX1Text1.Text)
      OEMX.MIX1List1(0).ListIndex = i

  End Select
  If (i <> 2) Then
    tCol = MIX1Label1(Index).ForeColor
    MIX1Label1(Index).ForeColor = MIX1Label1(Index).BackColor
    MIX1Label1(Index).BackColor = tCol
  End If
  '
End Sub

Private Sub MIX1List1_KeyUp(Index As Integer, KeyCode As Integer, Shift As Integer)
'
Dim i                 As Integer
Dim INDICE            As Integer
Dim PathTEMP          As String
Dim TipoTEMP          As String
Dim NomiPezzoTEMP     As String
Dim NomiTipoTEMP      As String
Dim Atmp              As String * 255
Dim stmp              As String
Dim riga              As String
Dim ret               As String
Dim DB                As Database
Dim TB                As Recordset
Dim NomeDB            As String
Dim NOME_MACRO        As String
Dim NuovoNomePezzo    As String
Dim COMBINAZIONI_DB   As String
Dim NomePezzoCORRENTE As String
'
On Error Resume Next
  '
  Select Case Index
    '
    Case 0
      '
    Case 1
      'LETTURA DEL NOME DEL PEZZO SELEZIONATO
      NuovoNomePezzo = Trim$(OEMX.MIX1List1(1).List(OEMX.MIX1List1(1).ListIndex))
      'CAMBIO NOME
      OEMX.MIX1List1(0).List(OEMX.MIX1List1(0).ListIndex) = " [" & Chr$(64 + OEMX.MIX1List1(0).ListIndex + 1) & "] " & NuovoNomePezzo
      If (KeyCode = 13) Then
        'NASCONDO IL TASTO FUNZIONE DI VISUALIZZAZIONE
        Call Change_SkTextOnScr(2, " ")
        'AGGIORNAMENTO ELENCO NELL'ARCHIVIO DELLE MACRO
        'NOME PEZZO
        NomePezzoCORRENTE = UCase$(Trim$(OEMX.MIX1Text1.Text))
        'LETTURA INDICE ULTIMA COMBINAZIONE SELEZIONATA
        stmp = GetInfo("Configurazione", "UltimaCOMBINAZIONE", PathFILEINI)
        INDICE = val(stmp)
        'NOME DELLA COMBINAZIONE
        NOME_MACRO = GetInfo("COMBINAZIONE(" & Format$(INDICE, "##0") & ")", "NOME", PathFILEINI)
        NOME_MACRO = UCase$(Trim$(NOME_MACRO))
        'LETTURA DEL NOME PEZZO DELLA COMBINAZIONE
        COMBINAZIONI_DB = GetInfo("COMBINAZIONE(" & Format$(INDICE, "##0") & ")", "NomeDB", PathFILEINI)
        COMBINAZIONI_DB = g_chOemPATH & "\" & UCase$(Trim$(COMBINAZIONI_DB)) & ".MDB"
        'APRO IL DATABASE
        Set DB = OpenDatabase(COMBINAZIONI_DB, True, False)
        Set TB = DB.OpenRecordset("ARCHIVIO_" & NOME_MACRO)
        'RICERCO NELLA TABELLA ARCHIVIO SE ESISTE UNA ARCHIVIAZIONE
        TB.Index = "NOME_PEZZO"
        TB.Seek "=", NomePezzoCORRENTE
        If (TB.NoMatch) Then
          'AGGIUNGE UN NUOVO ELEMENTO
          TB.AddNew
          Call OEMX.MIX1List1(2).AddItem(" " & NomePezzoCORRENTE)
        Else
          'AGGIORNA UN VECCHIO ELEMENTO
          TB.Edit
        End If
        'SALVO IL NOME E DATA
        TB.Fields("NOME_PEZZO").Value = NomePezzoCORRENTE
        TB.Fields("DATA_ARCHIVIAZIONE").Value = Now
        'VISUALIZZAZIONE DEI PEZZI SINGOLI
        Dim TIPO()      As String
        Dim NomiPezzo() As String
        Open g_chOemPATH & "\ELENCO.TXT" For Output As #1
          Print #1, TB.Fields("ELENCO").Value
        Close #1
        Open g_chOemPATH & "\ELENCO.TXT" For Input As #1
        i = 0
        While Not EOF(1)
          i = i + 1
          Line Input #1, riga: riga = UCase$(Trim$(riga))
          If (Len(riga) > 0) Then
            ReDim Preserve TIPO(i): ReDim Preserve NomiPezzo(i)
            TIPO(i) = Left$(riga, InStr(riga, "\") - 1)
            NomiPezzo(i) = Right$(riga, Len(riga) - Len(TIPO(i)) - 1)
          End If
        Wend
        Close #1
        'COSTRUZIONE ELENCO
        TB.Fields("ELENCO").Value = ""
        For i = 1 To UBound(NomiPezzo())
          If (i = OEMX.MIX1List1(0).ListIndex + 1) Then
            TB.Fields("ELENCO").Value = TB.Fields("ELENCO").Value & TIPO(i) & "\" & NuovoNomePezzo & Chr(13)
          Else
            TB.Fields("ELENCO").Value = TB.Fields("ELENCO").Value & TIPO(i) & "\" & NomiPezzo(i) & Chr(13)
          End If
        Next i
        'AGGIORNO IL CAMPO MEMO
        TB.Fields("MEMO").Value = OEMX.MIX1Text2.Text
        'AGGIORNO LA TABELLA
        TB.Update
        'CHIUDO LA TABELLA
        TB.Close
        'CHIUDO IL DATABASE
        DB.Close
        'SEGNALAZIONE UTENTE
        WRITE_DIALOG NomePezzoCORRENTE & ": OK!!!"
        'CHIUDO LA LISTA DI SELEZIONE
        OEMX.MIX1Text2.Width = OEMX.MIX1List1(0).Width + OEMX.MIX1List1(2).Width
        OEMX.MIX1List1(1).Visible = False
        OEMX.MIX1Label1(1).Visible = False
        OEMX.MIX1Image5.Visible = False
        OEMX.MIX1SSTab1.Visible = True
        OEMX.MIX1Text2.Visible = True
        'Se non ci sono parametri da visualizzare nascondo il tab setting
        If (OEMX.MIX1lstIMPOSTAZIONI(0).ListCount = 0) Then
          OEMX.MIX1SSTab1.TabVisible(1) = False
          OEMX.MIX1lstIMPOSTAZIONI(0).Visible = False
          OEMX.MIX1lstIMPOSTAZIONI(1).Visible = False
        Else
          OEMX.MIX1SSTab1.TabVisible(1) = True
          OEMX.MIX1lstIMPOSTAZIONI(0).Visible = True
          OEMX.MIX1lstIMPOSTAZIONI(1).Visible = True
        End If
        OEMX.MIX1List1(0).SetFocus
        DoEvents
      Else
        'SEGNALAZIONE UTENTE
        WRITE_DIALOG "PRESS " & Chr(34) & "INPUT" & Chr(34) & " to CONFIRM or " & Chr(34) & "TAB" & Chr(34) & " to ABORT"
      End If
      '
    Case 2
      '
    Case 3
      If (KeyCode = 13) Then
            If (Left(OEMX.MIX1List1(3).Text, 4) = " [_]") Then
          OEMX.MIX1List1(3).Text = " [X]" & Right(OEMX.MIX1List1(3).Text, Len(OEMX.MIX1List1(3).Text) - 4)
          Call OPC_SCRIVI_DATO("/ACC/NCK/UGUD/ABLAV_G[" & Trim(OEMX.MIX1List1(3).ListIndex + 2) & "]", 1)
        ElseIf Left(OEMX.MIX1List1(3).Text, 5) = " [X] " Then
          OEMX.MIX1List1(3).Text = " [_]" & Right(OEMX.MIX1List1(3).Text, Len(OEMX.MIX1List1(3).Text) - 4)
          Call OPC_SCRIVI_DATO("/ACC/NCK/UGUD/ABLAV_G[" & Trim(OEMX.MIX1List1(3).ListIndex + 2) & "]", 0)
        End If
      End If
      '
  End Select

End Sub

Private Sub MIX1lstIMPOSTAZIONI_Click(Index As Integer)
'
On Error Resume Next
  '
  MIX1lstIMPOSTAZIONI(0).ListIndex = MIX1lstIMPOSTAZIONI(1).ListIndex
  '
End Sub

Private Sub MIX1lstIMPOSTAZIONI_GotFocus(Index As Integer)
'
On Error Resume Next
  '
  If (MIX1lstIMPOSTAZIONI(0).ListIndex <= -1) Then
    MIX1lstIMPOSTAZIONI(0).ListIndex = 0
    MIX1lstIMPOSTAZIONI(1).ListIndex = 0
  End If
  
End Sub

Private Sub MIX1lstIMPOSTAZIONI_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
'
Dim stmp            As String
Dim INDICE          As Integer
Dim NOME_MACRO      As String
Dim i               As Integer
Dim DB              As Database
Dim TB              As Recordset
Dim COMBINAZIONI_DB As String
Dim LLinf           As Integer
Dim LLsup           As Integer
Dim LLbas           As Integer
Dim Valore          As String
'
On Error Resume Next
  '
  If (Index = 1) Then
    If (KeyCode = 13) Then
      'MsgBox Index
      'LETTURA INDICE ULTIMA COMBINAZIONE SELEZIONATA
      stmp = GetInfo("Configurazione", "UltimaCOMBINAZIONE", PathFILEINI)
      INDICE = val(stmp)
      'NOME DELLA COMBINAZIONE
      NOME_MACRO = GetInfo("COMBINAZIONE(" & Format$(INDICE, "##0") & ")", "NOME", PathFILEINI)
      NOME_MACRO = UCase$(Trim$(NOME_MACRO))
      'NOME DEL DATABASE
      COMBINAZIONI_DB = GetInfo("COMBINAZIONE(" & Format$(INDICE, "##0") & ")", "NomeDB", PathFILEINI)
      COMBINAZIONI_DB = g_chOemPATH & "\" & UCase$(Trim$(COMBINAZIONI_DB)) & ".MDB"
      'INDICE DELLA TABELLA
      i = OEMX.MIX1lstIMPOSTAZIONI(1).ListIndex + 1
      'APRO IL DATABASE
      Set DB = OpenDatabase(COMBINAZIONI_DB, True, False)
      'APRO LA TABELLA DEL GRUPPO DI PARAMETRI
      Set TB = DB.OpenRecordset("IMPOSTAZIONI_" & NOME_MACRO, dbOpenTable)
      'RICERCO L'ITEMDDE DEL PARAMETRO
      TB.Index = "INDICE"
      TB.Seek "=", i
      'MODIFICO SOLO SE TROVO IL PARAMETRO NELLA TABELLA
      If Not TB.NoMatch Then
        If (TB.Fields("ABILITATO").Value = "Y") Then
          'LEGGO IL VALORE DEL PARAMETRO SUL CNC
          stmp = TB.Fields("ITEMDDE").Value
          'INDICE DELLA LAVORAZIONE CON SHIFT
          LLinf = InStr(stmp, "*")
          If (LLinf > 0) Then
            'CHIUSURA TABELLA
            TB.Close
            'CHIUSURA DATABASE
            DB.Close
            'SEGNALAZIONE UTENTE
            StopRegieEvents
            MsgBox "ITEM NOT ALLOWED: " & Chr(13) & stmp, vbCritical, "WARNING"
            ResumeRegieEvents
          Else
            Dim TIPO As String
            Dim Limiti As String
            '
            Dim k     As Integer
            Dim swp   As Single
            Dim INF   As Single
            Dim sup   As Single
            '
            TIPO = TB.Fields("TIPO").Value
            Limiti = UCase$(Trim$(TB.Fields("LIMITI").Value))
            '
            'CHIUSURA TABELLA
            TB.Close
            'CHIUSURA DATABASE
            DB.Close
            'CASELLA DI MODIFICA
            StopRegieEvents
            Valore = InputBox("DDE item:" & Chr(13) & Chr$(13) & stmp, Trim$(OEMX.MIX1lstIMPOSTAZIONI(0).List(i - 1)), Trim$(OEMX.MIX1lstIMPOSTAZIONI(1).List(i - 1)))
            Valore = Trim$(Valore)
            ResumeRegieEvents
            If (Len(Valore) > 0) Then
              Call CALCOLA_ESPRESSIONE_ESTESA(Valore)
              If (Len(Limiti) > 0) Then
                'SCRIVO SE E' COMPRESO TRA IL MAX ED IL MIN
                k = InStr(Limiti, ",")
                If (k > 0) Then
                  INF = val(Trim$(Left$(Limiti, k)))
                  sup = val(Trim$(Right$(Limiti, Len(Limiti) - k)))
                  If (INF > sup) Then swp = sup: sup = INF: INF = swp
                End If
                If (val(Valore) >= INF) And (val(Valore) <= sup) Then
                  'AGGIORNAMENTO DEL VALORE SUL CNC
                  Call OPC_SCRIVI_DATO(stmp, Valore)
                  'AGGIORNAMENTO DEL VALORE SULLA LISTA
                  Call OEMX.MIX1lstIMPOSTAZIONI(1).RemoveItem(i - 1)
                  Call OEMX.MIX1lstIMPOSTAZIONI(1).AddItem(" " & Valore, (i - 1))
                  WRITE_DIALOG OEMX.MIX1lstIMPOSTAZIONI(0).List(i - 1) & " MODIFIED!!"
                Else
                  WRITE_DIALOG INF & "<=" & OEMX.MIX1lstIMPOSTAZIONI(0).List(i - 1) & " <= " & sup
                End If
              Else
                WRITE_DIALOG "Operation Aborted by user."
              End If
            End If
          End If
        Else
          'CHIUSURA TABELLA
          TB.Close
          'CHIUSURA DATABASE
          DB.Close
        End If
      Else
        'CHIUSURA TABELLA
        TB.Close
        'CHIUSURA DATABASE
        DB.Close
      End If
      OEMX.MIX1lstIMPOSTAZIONI(0).ListIndex = (i - 1)
      OEMX.MIX1lstIMPOSTAZIONI(1).ListIndex = (i - 1)
      OEMX.MIX1lstIMPOSTAZIONI(1).SetFocus
      '
    End If
  End If
  
End Sub

Private Sub MIX1SSTab1_Click(PreviousTab As Integer)
'
Dim stmp            As String
Dim INDICE          As Integer
Dim NOME_MACRO      As String
Dim i               As Integer
Dim DB              As Database
Dim TB              As Recordset
Dim COMBINAZIONI_DB As String
Dim LLinf           As Integer
Dim LLsup           As Integer
Dim LLbas           As Integer
'
On Error Resume Next
  '
  If (PreviousTab = 0) Then
    'VISUALIZZAZIONE CONTROLLI
    MIX1lstIMPOSTAZIONI(0).Visible = True
    MIX1lstIMPOSTAZIONI(1).Visible = True
    MIX1Text2.Visible = False
    'LETTURA INDICE ULTIMA COMBINAZIONE SELEZIONATA
    stmp = GetInfo("Configurazione", "UltimaCOMBINAZIONE", PathFILEINI)
    INDICE = val(stmp)
    'NOME DELLA COMBINAZIONE
    NOME_MACRO = GetInfo("COMBINAZIONE(" & Format$(INDICE, "##0") & ")", "NOME", PathFILEINI)
    NOME_MACRO = UCase$(Trim$(NOME_MACRO))
    'NOME DEL DATABASE
    COMBINAZIONI_DB = GetInfo("COMBINAZIONE(" & Format$(INDICE, "##0") & ")", "NomeDB", PathFILEINI)
    COMBINAZIONI_DB = g_chOemPATH & "\" & UCase$(Trim$(COMBINAZIONI_DB)) & ".MDB"
    'APRO IL DATABASE
    Set DB = OpenDatabase(COMBINAZIONI_DB, True, False)
    'CONTROLLO SE LA TABELLA DI DESTINAZIONE ESISTE NEL DATABASE
    For i = 0 To DB.TableDefs.count - 1
      If DB.TableDefs(i).Name = "IMPOSTAZIONI_" & NOME_MACRO Then
        'APRO LA TABELLA DEL GRUPPO DI PARAMETRI
        Set TB = DB.OpenRecordset("IMPOSTAZIONI_" & NOME_MACRO, dbOpenTable)
        OEMX.MIX1lstIMPOSTAZIONI(1).Clear
        Do Until TB.EOF
          If (TB.Fields("ABILITATO").Value = "Y") Then
            'LEGGO IL VALORE DEL PARAMETRO SUL CNC
            stmp = TB.Fields("ITEMDDE").Value
            'INDICE DELLA LAVORAZIONE CON SHIFT
            LLinf = InStr(stmp, "*")
            If (LLinf > 0) Then
              StopRegieEvents
              MsgBox "ITEM NOT ALLOWED: " & Chr(13) & stmp, vbCritical, "WARNING"
              ResumeRegieEvents
              stmp = "?"
            Else
              stmp = OPC_LEGGI_DATO(stmp)
            End If
            stmp = Trim$(stmp)
            'SE E' NUMERICO FORMATTO IL VALORE
            If IsNumeric(stmp) Then stmp = frmt(val(stmp), 8)
            Call OEMX.MIX1lstIMPOSTAZIONI(1).AddItem(" " & stmp)
          End If
          'SPOSTO IN AVANTI IL PUNTATORE AL RECORD
          TB.MoveNext
        Loop
        'CHIUSURA TABELLA DEI CICLI
        TB.Close
        Exit For
      End If
    Next i
    'CHIUSURA DATABASE
    DB.Close
  Else
    MIX1lstIMPOSTAZIONI(0).Visible = False
    MIX1lstIMPOSTAZIONI(1).Visible = False
    MIX1Text2.Visible = True
    MIX1Text2.SetFocus
  End If
  '
End Sub

Private Sub MIX1Text1_GotFocus()
  '
  OEMX.MIX1List1(0).ListIndex = -1
  OEMX.MIX1List1(3).ListIndex = -1
  Call SELEZIONA_TESTO
  '
End Sub

Private Sub MIX1Text1_KeyUp(KeyCode As Integer, Shift As Integer)
'
Dim NomePezzo As String
Dim rsp       As Integer
Dim stmp      As String
Dim i         As Integer
Dim nMAX      As Integer
Dim ret       As Integer
'
On Error GoTo errText1_KeyUp
  '
  Select Case KeyCode
    '
    Case 33
      If (OEMX.MIX1List1(2).ListCount > 0) Then
        WRITE_DIALOG ""
        'INIZIO LISTA
        i = 0
        OEMX.MIX1List1(2).ListIndex = i
        OEMX.MIX1Text1.Text = Trim$(OEMX.MIX1List1(2).List(i))
        Call SELEZIONA_TESTO
        Call MIX1_TROVA_MEMO(OEMX.MIX1Text1.Text)
      End If
      '
    Case 34
      If (OEMX.MIX1List1(2).ListCount > 0) Then
        WRITE_DIALOG ""
        'FINE LISTA
        nMAX = OEMX.MIX1List1(2).ListCount
        If (nMAX > 10) Then
          i = OEMX.MIX1List1(2).ListIndex + 10
          If i >= nMAX Then i = nMAX - 1
        Else
          'VADO SULL'ULTIMO
          i = nMAX - 1
        End If
        OEMX.MIX1List1(2).ListIndex = i
        OEMX.MIX1Text1.Text = Trim$(OEMX.MIX1List1(2).List(i))
        Call SELEZIONA_TESTO
        Call MIX1_TROVA_MEMO(OEMX.MIX1Text1.Text)
      End If
      '
    Case 40
      If (OEMX.MIX1List1(2).ListCount > 0) Then
        WRITE_DIALOG ""
        'AVANTI
        i = OEMX.MIX1List1(2).ListIndex
        nMAX = OEMX.MIX1List1(2).ListCount
        i = (i + 1) Mod nMAX
        OEMX.MIX1List1(2).ListIndex = i
        OEMX.MIX1Text1.Text = Trim$(OEMX.MIX1List1(2).List(i))
        Call SELEZIONA_TESTO
        Call MIX1_TROVA_MEMO(OEMX.MIX1Text1.Text)
      End If
      '
    Case 38
      If (OEMX.MIX1List1(2).ListCount > 0) Then
        WRITE_DIALOG ""
        'INDIETRO
        i = OEMX.MIX1List1(2).ListIndex
        nMAX = OEMX.MIX1List1(2).ListCount
        If (i > 0) Then
          i = (i - 1) Mod nMAX
        Else
          i = nMAX - 1
        End If
        OEMX.MIX1List1(2).ListIndex = i
        OEMX.MIX1Text1.Text = Trim$(OEMX.MIX1List1(2).List(i))
        Call SELEZIONA_TESTO
        Call MIX1_TROVA_MEMO(OEMX.MIX1Text1.Text)
      End If
      '
    Case 13: 'Ricerca della casella di testo
      stmp = UCase$(Trim$(OEMX.MIX1Text1.Text))
      If (Len(stmp) > 0) Then
        WRITE_DIALOG ""
        For i = 0 To OEMX.MIX1List1(2).ListCount - 1
          If (InStr(Left(Trim$(OEMX.MIX1List1(2).List(i)), Len(stmp)), stmp) > 0) Then
            OEMX.MIX1List1(2).ListIndex = i
            Call SELEZIONA_TESTO
            Call MIX1_TROVA_MEMO(OEMX.MIX1Text1.Text)
            'Write_Dialog stmp & " --> " & OEMX.MIX1LIST1(2).List(i)
            Exit For
          Else
            WRITE_DIALOG stmp & " not found!!!"
          End If
        Next i
      End If
      '
  End Select
  '
Exit Sub

errText1_KeyUp:
  WRITE_DIALOG Error(Err)
  Exit Sub

End Sub

Private Sub MIX1Text2_GotFocus()
  
Dim tCol As Long
  
On Error Resume Next

  tCol = MIX1Text2.ForeColor
  MIX1Text2.ForeColor = MIX1Text2.BackColor
  MIX1Text2.BackColor = tCol

End Sub

Private Sub MIX1Text2_LostFocus()
'
Dim tCol As Long
'
On Error Resume Next
  '
  tCol = MIX1Text2.ForeColor
  MIX1Text2.ForeColor = MIX1Text2.BackColor
  MIX1Text2.BackColor = tCol
  '
End Sub

Private Sub MIX1cmbMacroLav_OnSelChange(ByVal itemIndex As Long)
'
Dim INDICE          As Integer
Dim COMBINAZIONI_DB As String
Dim i               As Integer
Dim COMBINAZIONE    As String
Dim NomePezzo       As String
Dim DB              As Database
Dim RS              As Recordset
Dim ret             As String
Dim stmp            As String
'
On Error Resume Next
  '
  ret = WritePrivateProfileString("Configurazione", "UltimaCOMBINAZIONE", Format$(itemIndex + 1, "##0"), PathFILEINI)
  'LETTURA INDICE ULTIMA COMBINAZIONE SELEZIONATA
  stmp = GetInfo("Configurazione", "UltimaCOMBINAZIONE", PathFILEINI)
  INDICE = val(stmp)
  'NOME TIPO DELLA COMBINAZIONE
  COMBINAZIONE = GetInfo("COMBINAZIONE(" & Format$(INDICE, "##0") & ")", "NOME", PathFILEINI)
  'LETTURA DEL NOME PEZZO DELLA COMBINAZIONE
  NomePezzo = GetInfo("COMBINAZIONE(" & Format$(INDICE, "##0") & ")", "NomePezzo", PathFILEINI)
  NomePezzo = UCase$(Trim$(NomePezzo))
  'PERCORSO DEL DATABASE
  COMBINAZIONI_DB = GetInfo("COMBINAZIONE(" & Format$(INDICE, "##0") & ")", "NomeDB", PathFILEINI)
  COMBINAZIONI_DB = g_chOemPATH & "\" & UCase$(Trim$(COMBINAZIONI_DB)) & ".MDB"
  'APRO IL DATABASE
  Set DB = OpenDatabase(COMBINAZIONI_DB, True, False)
  Set RS = DB.OpenRecordset("PROGRAMMI", dbOpenTable)
  RS.Index = "NOME"
  RS.Seek "=", COMBINAZIONE
  OEMX.MIX1Label1(2).Caption = RS.Fields("NOME_" & LINGUA).Value
  'CHIUSURA TABELLA E DATABASE
  RS.Close
  DB.Close
  Call MIX1_VISUALIZZA_COMBINAZIONE
  Call MIX1_TROVA_MEMO(NomePezzo)
  Call MIX1_AggiornaListaCNC
  '
End Sub

Private Sub MNG0EditorTesto_GotFocus()
'
On Error Resume Next
  '
  OEMX.MNG0Label2.Caption = CStr(CurrentLine(MNG0EditorTesto)) & "/" & CStr(NoOfLines(MNG0EditorTesto))
  '
End Sub

Private Sub MNG0EditorTesto_KeyUp(KeyCode As Integer, Shift As Integer)
'
On Error Resume Next
  '
  OEMX.MNG0Label2.Caption = CStr(CurrentLine(MNG0EditorTesto)) & "/" & CStr(NoOfLines(MNG0EditorTesto))
  '
End Sub

Private Sub MNG0EditorTesto_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
'
On Error Resume Next
  '
  OEMX.MNG0Label2.Caption = CStr(CurrentLine(MNG0EditorTesto)) & "/" & CStr(NoOfLines(MNG0EditorTesto))
  '
End Sub

Private Sub MNG0FileList_DblClick()
'
On Error Resume Next
  '
  Call MNG0FileList_KeyDown(13, 0)
  '
End Sub

Private Sub MNG0FileList_KeyDown(KeyCode As Integer, Shift As Integer)
'
Dim k                 As Integer
Dim stmp              As String
Dim riga              As String
Dim i                 As Integer
Dim nMAX              As Long
Dim ret               As Long
Dim rsp               As Long
Dim Atmp              As String * 255
Dim NomeFile          As String
Dim TIPO_LAVORAZIONE  As String
'
Dim ABILITAZIONE      As String
Dim Utente            As String
Dim UTENTI()          As String
'
On Error Resume Next
  '
  WRITE_DIALOG ""
  If (KeyCode = 46) Then
      'VERIFICA ABILITAZIONE UTENTE
      i = 0: ReDim UTENTI(i)
      Do
        i = i + 1
        stmp = GetInfo("UTENTI", "UTENTE(" & Format$(i, "#0") & ")", Path_LAVORAZIONE_INI)
        stmp = UCase$(Trim$(stmp))
        If (stmp <> "") Then
          ReDim Preserve UTENTI(i)
          UTENTI(i) = stmp
        Else
          Exit Do
        End If
      Loop
      If (UBound(UTENTI) > 0) Then
        Utente = InputBox("INPUT USERNAME!!!!", "ROTOR DATA", "")
        Utente = UCase$(Trim$(Utente))
        For i = 1 To UBound(UTENTI)
          If (UTENTI(i) = Utente) Then
            ABILITAZIONE = "Y" 'UTENTE ABILITATO
            Exit For
          End If
        Next i
        If (ABILITAZIONE <> "Y") Then
          If (Len(Utente) <= 0) Then Utente = "?????"
          stmp = GetInfo("UTENTI", "MESSAGGIO", Path_LAVORAZIONE_INI)
          WRITE_DIALOG Utente & ": " & stmp
          Exit Sub
        End If
      End If
      '
      WRITE_DIALOG ""
      k = 0
      stmp = ""
      For i = 0 To OEMX.MNG0FileList.ListCount - 1
        If (OEMX.MNG0FileList.Selected(i) = True) Then
          k = k + 1
          stmp = stmp & Format$(k, "#0) - ") & UCase$(OEMX.MNG0FileList.List(i))
          stmp = stmp & Chr$(13)
        End If
      Next i
      If (k > 10) Then
        'VISUALIZZO SOLO QUANTI NE SONO SELEZIONATI
        stmp = Format$(k, "########") & "/" & Format$(OEMX.MNG0FileList.ListCount, "########")
      End If
      If (k > 0) Then
        ret = LoadString(g_hLanguageLibHandle, 1004, Atmp, 255)
        If (ret > 0) Then riga = Left$(Atmp, ret)
        StopRegieEvents
        rsp = MsgBox(riga & " " & Chr$(13) & Chr$(13) & stmp & " ?", 256 + 4 + 32, "WARNING: " & OEMX.MNG0FileList.Path)
        ResumeRegieEvents
        If (rsp = 6) Then
          For i = 0 To OEMX.MNG0FileList.ListCount - 1
            If (OEMX.MNG0FileList.Selected(i) = True) Then
              ret = LoadString(g_hLanguageLibHandle, 1004, Atmp, 255)
              If (ret > 0) Then riga = Left$(Atmp, ret)
              Call Kill(OEMX.MNG0FileList.Path & "\" & OEMX.MNG0FileList.List(i))
              OEMX.MNG0FileList.Selected(i) = False
              Call WRITE_DIALOG(riga & " " & OEMX.MNG0FileList.List(i) & " OK!!")
            End If
          Next i
          'AGGIORNO VISUALIZZAZIONE FILE
          OEMX.MNG0FileList.Refresh
        Else
          WRITE_DIALOG "Operation aborted by user!!!"
        End If
      Else
        WRITE_DIALOG "Operation cannot be performed!!!"
      End If
  End If
  '
  If (KeyCode = 13) Then
    'VERIFICA ABILITAZIONE UTENTE
    i = 0: ReDim UTENTI(i)
    Do
      i = i + 1
      stmp = GetInfo("UTENTI", "UTENTE(" & Format$(i, "#0") & ")", Path_LAVORAZIONE_INI)
      stmp = UCase$(Trim$(stmp))
      If (stmp <> "") Then
        ReDim Preserve UTENTI(i)
        UTENTI(i) = stmp
      Else
        Exit Do
      End If
    Loop
    If (UBound(UTENTI) > 0) Then
      Utente = InputBox("INPUT USERNAME!!!!", "ROTOR DATA", "")
      Utente = UCase$(Trim$(Utente))
      For i = 1 To UBound(UTENTI)
        If (UTENTI(i) = Utente) Then
          ABILITAZIONE = "Y" 'UTENTE ABILITATO
          Exit For
        End If
      Next i
      If (ABILITAZIONE <> "Y") Then
        If (Len(Utente) <= 0) Then Utente = "?????"
        stmp = GetInfo("UTENTI", "MESSAGGIO", Path_LAVORAZIONE_INI)
        WRITE_DIALOG Utente & ": " & stmp
        Exit Sub
      End If
    End If
    If (OEMX.MNG0EditorTesto.Visible = True) Then
      'SALVATAGGIO FILE
      NomeFile = OEMX.MNG0FileList.Path & "\" & OEMX.MNG0INTESTAZIONE.Caption
      'SE IL FILE ESISTE OFFRE LA POSSIBILITA' DI CAMBIARE NOME
      StopRegieEvents
      NomeFile = InputBox("MODIFY FILE NAME", "FILE " & OEMX.MNG0INTESTAZIONE.Caption & " already exist!!!", OEMX.MNG0INTESTAZIONE.Caption)
      ResumeRegieEvents
      NomeFile = UCase$(Trim$(NomeFile))
      If (Len(NomeFile) > 0) Then
        'NUOVO NOME
        NomeFile = OEMX.MNG0FileList.Path & "\" & NomeFile
        If (Dir$(NomeFile) <> "") Then
          'FILE ESISTENTE E CHIEDO CONFERMA
          StopRegieEvents
          rsp = MsgBox(Error(58) & Chr(13) & Chr(13) & "SAVE " & NomeFile & " ?", vbDefaultButton2 + vbQuestion + vbYesNo, "WARNING")
          ResumeRegieEvents
        Else
          'FILE NON ESISTENTE
          rsp = vbYes
        End If
      Else
        rsp = vbNo
        'SEGNALAZIONE UTENTE
        WRITE_DIALOG "Operation aborted by user!!!"
      End If
      OEMX.MNG0EditorTesto.Visible = False
      If (rsp = vbYes) Then
        'SCRIVO IL CONTENUTO DELLA CASELLA DI TESTO
        stmp = OEMX.MNG0EditorTesto.Text
        Do
          If Right$(stmp, 1) = Chr$(10) Or Right$(stmp, 1) = Chr$(13) Then
            stmp = Left$(stmp, Len(stmp) - 1)
          Else
            Exit Do
          End If
        Loop
        Open NomeFile For Output As #1
          Print #1, Trim$(stmp)
        Close #1
        'SEGNALAZIONE UTENTE
        WRITE_DIALOG "SAVED!!!" & NomeFile
      End If
      'RIMETTO A POSTO IL TESTO
      ret = LoadString(g_hLanguageLibHandle, 1211, Atmp, 255)
      If (ret > 0) Then stmp = Left$(Atmp, ret) Else stmp = "No name at " & stmp
      OEMX.MNG0INTESTAZIONE.Caption = stmp
      'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
      TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
      TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
      'NOME PEZZO CARICATO
      OEMX.MNG0Label2.Caption = LEGGI_NomePezzo(TIPO_LAVORAZIONE)
      OEMX.MNG0Label2.BackColor = &HFFFF&
      OEMX.MNG0Label2.ForeColor = &HFF0000
      OEMX.MNG0INTESTAZIONE.BackColor = &HFFFF&
      OEMX.MNG0INTESTAZIONE.ForeColor = &HFF0000
      'AGGIORNO LA VISUALIZZAZIONE DELLA LISTA
      OEMX.MNG0FileList.Refresh
    Else
      'VISUALIZZAZIONE FILE
      NomeFile = OEMX.MNG0FileList.Path & "\" & OEMX.MNG0FileList.FileName
      OEMX.MNG0EditorTesto.Visible = True
      OEMX.MNG0EditorTesto.Left = OEMX.MNG0List1(0).Left
      OEMX.MNG0EditorTesto.Top = OEMX.MNG0List1(0).Top
      OEMX.MNG0EditorTesto.Height = OEMX.MNG0List1(0).Height
      OEMX.MNG0EditorTesto.Width = OEMX.MNG0List1(0).Width
      OEMX.MNG0EditorTesto.Font.Name = "Courier New"
      OEMX.MNG0EditorTesto.Text = ""
      Open NomeFile For Input As #1
        OEMX.MNG0EditorTesto.Text = Input$(LOF(1), 1)
      Close #1
      OEMX.MNG0INTESTAZIONE.Caption = OEMX.MNG0FileList.FileName
      OEMX.MNG0EditorTesto.SetFocus
      OEMX.MNG0Label2.BackColor = &HFF&
      OEMX.MNG0Label2.ForeColor = &HFFFFFF
      OEMX.MNG0INTESTAZIONE.BackColor = &HFF&
      OEMX.MNG0INTESTAZIONE.ForeColor = &HFFFFFF
    End If
  End If
  '
End Sub

Private Sub MNG0List1_Click(Index As Integer)
'
Dim NomePezzo As String
Dim ret       As Integer
'
On Error Resume Next
  '
  NomePezzo = Trim$(OEMX.MNG0List1(0).List(OEMX.MNG0List1(0).ListIndex))
  ret = InStr(NomePezzo, "(")
  If (ret > 1) Then NomePezzo = Trim$(Left$(NomePezzo, ret - 1))
  OEMX.MNG0Text1.Text = Trim$(NomePezzo)
  OEMX.MNG0Text1.SetFocus
  Call MNG0_TROVA_MEMO
  '
End Sub

Private Sub MNG0List1_DblClick(Index As Integer)
'
Dim NomePezzo As String
Dim ret       As Integer
'
On Error Resume Next
  '
  NomePezzo = Trim$(OEMX.MNG0List1(0).List(OEMX.MNG0List1(0).ListIndex))
  ret = InStr(NomePezzo, "(")
  If (ret > 1) Then NomePezzo = Trim$(Left$(NomePezzo, ret - 1))
  OEMX.MNG0Text1.Text = Trim$(NomePezzo)
  Call MNG0_TROVA_MEMO
  '
End Sub

Private Sub MNG0lstIMPOSTAZIONI_Click(Index As Integer)

  Select Case Index
    Case 1:  MNG0lstIMPOSTAZIONI(0).ListIndex = MNG0lstIMPOSTAZIONI(1).ListIndex
    Case 0:  MNG0lstIMPOSTAZIONI(1).ListIndex = MNG0lstIMPOSTAZIONI(0).ListIndex
  End Select
  
End Sub

Private Sub MNG0SSTab1_Click(PreviousTab As Integer)
'
Dim stmp              As String
Dim TIPO_LAVORAZIONE  As String
Dim i                 As Integer
Dim DB                As Database
Dim TB                As Recordset
'
Dim LLinf As Integer
Dim LLsup As Integer
Dim LLbas As Integer
'
On Error Resume Next
  '
  Select Case OEMX.MNG0SSTab1.Tab
  Case 0
      OEMX.MNG0Text2.Visible = True
      OEMX.MNG0lstIMPOSTAZIONI(1).Visible = False
      OEMX.MNG0lstIMPOSTAZIONI(0).Visible = False
      OEMX.MNG0FileList.Visible = False
      OEMX.MNG0lstPRODUZIONE.Visible = False
  Case 1
      OEMX.MNG0Text2.Visible = False
      OEMX.MNG0lstIMPOSTAZIONI(1).Visible = True
      OEMX.MNG0lstIMPOSTAZIONI(0).Visible = True
      OEMX.MNG0FileList.Visible = False
      OEMX.MNG0lstPRODUZIONE.Visible = False
      TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
      TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
      Set DB = OpenDatabase(PathDB, True, False)
      'CONTROLLO SE LA TABELLA DI DESTINAZIONE ESISTE NEL DATABASE
      For i = 0 To DB.TableDefs.count - 1
        If DB.TableDefs(i).Name = "IMPOSTAZIONI_" & TIPO_LAVORAZIONE Then
          'APRO LA TABELLA DEL GRUPPO DI PARAMETRI
          Set TB = DB.OpenRecordset("IMPOSTAZIONI_" & TIPO_LAVORAZIONE, dbOpenTable)
          OEMX.MNG0lstIMPOSTAZIONI(1).Clear
          Do Until TB.EOF
            If (TB.Fields("ABILITATO").Value = "Y") Then
              'LEGGO IL VALORE DEL PARAMETRO SUL CNC
              stmp = TB.Fields("ITEMDDE").Value
              'INDICE DELLA LAVORAZIONE CON SHIFT
              LLinf = InStr(stmp, "*")
              If (LLinf > 0) Then
                LLsup = InStr(stmp, "]")
                LLbas = val(Mid$(stmp, LLinf + 1, LLsup - LLinf - 1))
                LLbas = LLbas + 1 + INDICE_LAVORAZIONE_SINGOLA
                stmp = Left$(stmp, LLinf - 1) & Format$(LLbas, "####0") & "]"
              End If
              stmp = OPC_LEGGI_DATO(stmp)
              stmp = Trim$(stmp)
              'SE E' NUMERICO FORMATTO IL VALORE
              If IsNumeric(stmp) Then stmp = frmt(val(stmp), 8)
              Call OEMX.MNG0lstIMPOSTAZIONI(1).AddItem(stmp)
            End If
            TB.MoveNext
          Loop
          TB.Close
          Exit For
        End If
      Next i
      DB.Close
  Case 2
      OEMX.MNG0Text2.Visible = False
      OEMX.MNG0lstIMPOSTAZIONI(1).Visible = False
      OEMX.MNG0lstIMPOSTAZIONI(0).Visible = False
      OEMX.MNG0lstPRODUZIONE.Visible = False
      Call MNG0_TROVA_ROTORDIRECTORY
  Case 3
      OEMX.MNG0Text2.Visible = False
      OEMX.MNG0lstIMPOSTAZIONI(1).Visible = False
      OEMX.MNG0lstIMPOSTAZIONI(0).Visible = False
      OEMX.MNG0FileList.Visible = False
      Call MNG0_LEGGI_PIANO_PRODUZIONE
  End Select
  '
End Sub

Private Sub MNG0Text1_Change()
'
On Error Resume Next
  '
  WRITE_DIALOG ""
  '
End Sub

Private Sub MNG0Text1_GotFocus()
'
On Error Resume Next
  '
  Call SELEZIONA_TESTO
  '
End Sub

Private Sub MNG0Text1_KeyUp(KeyCode As Integer, Shift As Integer)
'
Dim NomePezzo As String
Dim rsp       As Integer
Dim stmp      As String
Dim i         As Integer
Dim nMAX      As Integer
Dim ret       As Integer
'
On Error GoTo errText1_KeyUp
  '
  Select Case KeyCode
    '
    Case 33
      If (OEMX.MNG0List1(0).ListCount > 0) Then
        WRITE_DIALOG ""
        'INIZIO LISTA
        i = 0
        OEMX.MNG0List1(0).ListIndex = i
        '
        NomePezzo = Trim$(OEMX.MNG0List1(0).List(i))
        ret = InStr(NomePezzo, "(")
        If (ret > 1) Then NomePezzo = Trim$(Left$(NomePezzo, ret - 1))
        OEMX.MNG0Text1.Text = NomePezzo
        '
        Call SELEZIONA_TESTO
        Call MNG0_TROVA_MEMO
      End If
      '
    Case 34
      If (OEMX.MNG0List1(0).ListCount > 0) Then
        WRITE_DIALOG ""
        'FINE LISTA
        nMAX = OEMX.MNG0List1(0).ListCount
        If (nMAX > 10) Then
          i = OEMX.MNG0List1(0).ListIndex + 10
          If i >= nMAX Then i = nMAX - 1
        Else
          'VADO SULL'ULTIMO
          i = nMAX - 1
        End If
        OEMX.MNG0List1(0).ListIndex = i
        '
        NomePezzo = Trim$(OEMX.MNG0List1(0).List(i))
        ret = InStr(NomePezzo, "(")
        If (ret > 1) Then NomePezzo = Trim$(Left$(NomePezzo, ret - 1))
        OEMX.MNG0Text1.Text = NomePezzo
        '
        Call SELEZIONA_TESTO
        Call MNG0_TROVA_MEMO
      End If
      '
    Case 40
      If (OEMX.MNG0List1(0).ListCount > 0) Then
        WRITE_DIALOG ""
        'AVANTI
        i = OEMX.MNG0List1(0).ListIndex
        nMAX = OEMX.MNG0List1(0).ListCount
        i = (i + 1) Mod nMAX
        OEMX.MNG0List1(0).ListIndex = i
        '
        NomePezzo = Trim$(OEMX.MNG0List1(0).List(i))
        ret = InStr(NomePezzo, "(")
        If (ret > 1) Then NomePezzo = Trim$(Left$(NomePezzo, ret - 1))
        OEMX.MNG0Text1.Text = NomePezzo
        '
        Call SELEZIONA_TESTO
        Call MNG0_TROVA_MEMO
      End If
      '
    Case 38
      If (OEMX.MNG0List1(0).ListCount > 0) Then
        WRITE_DIALOG ""
        'INDIETRO
        i = OEMX.MNG0List1(0).ListIndex
        nMAX = OEMX.MNG0List1(0).ListCount
        If (i > 0) Then
          i = (i - 1) Mod nMAX
        Else
          i = nMAX - 1
        End If
        OEMX.MNG0List1(0).ListIndex = i
        '
        NomePezzo = Trim$(OEMX.MNG0List1(0).List(i))
        ret = InStr(NomePezzo, "(")
        If (ret > 1) Then NomePezzo = Trim$(Left$(NomePezzo, ret - 1))
        OEMX.MNG0Text1.Text = NomePezzo
        '
        Call SELEZIONA_TESTO
        Call MNG0_TROVA_MEMO
      End If
      '
    Case 13
      'Ricerca della casella di testo
      stmp = UCase$(Trim$(OEMX.MNG0Text1.Text))
      If (Len(stmp) > 0) Then
        WRITE_DIALOG ""
        For i = 0 To OEMX.MNG0List1(0).ListCount - 1
          If (InStr(Left(Trim$(OEMX.MNG0List1(0).List(i)), Len(stmp)), stmp) > 0) Then
            OEMX.MNG0List1(0).ListIndex = i
            Call SELEZIONA_TESTO
            Call MNG0_TROVA_MEMO(Shift)
            '
            NomePezzo = Trim$(OEMX.MNG0List1(0).List(i))
            ret = InStr(NomePezzo, "(")
            If (ret > 1) Then NomePezzo = Trim$(Left$(NomePezzo, ret - 1))
            '
            WRITE_DIALOG stmp & " --> " & NomePezzo
            Exit For
          Else
            WRITE_DIALOG stmp & " not found!!!"
          End If
        Next i
      End If
      '
  End Select
  '
Exit Sub

errText1_KeyUp:
  WRITE_DIALOG Error(Err)
  Exit Sub

End Sub

Private Sub MNG0Text2_LostFocus()
'
On Error Resume Next
  '
  Open PathFileMemo For Output As #1
    Print #1, OEMX.MNG0Text2.Text;
  Close #1
  '
End Sub

Private Sub MNG0Timer1_Timer()
'
Static ii                      As Integer
Dim INDICE_LAVORAZIONE_LOCALE  As Integer
Dim k                          As Integer
Dim NomePezzo                  As String
Dim Azione                     As String
Dim ret                        As Integer
Dim stmp                       As String
Dim Atmp                       As String * 255
Dim TIPO_LAVORAZIONE           As String
Dim COMANDO()                  As String
Dim INDICE                     As Integer
'
On Error GoTo errTimer1_Timer
  '
  If (OEMX.MNG0Timer1.Enabled = True) Then
    'VISUALIZZAZIONE TIMER
    ii = ii + 1
    ii = ii Mod 14
    WRITE_DIALOG "POLLING ON " & String(ii, ".")
    'ANIMAZIONE DELL'INTESTAZIONE
    'OEMX.INTESTAZIONE.Caption = "CICLO DI ATTESA COMANDI DA PROGRAMMA ESTERNO"
    OEMX.MNG0INTESTAZIONE.Caption = "IDLE CYCLE WAITING FOR COMMAND!!"
    If (OEMX.MNG0INTESTAZIONE.BackColor = &HFFFF&) Then
      OEMX.MNG0INTESTAZIONE.BackColor = QBColor(12)
      OEMX.MNG0INTESTAZIONE.ForeColor = QBColor(15)
    Else
      OEMX.MNG0INTESTAZIONE.BackColor = &HFFFF&
      OEMX.MNG0INTESTAZIONE.ForeColor = &HFF0000
    End If
    'RIMUOVO IL NOME DEL CODICE PEZZO
    OEMX.MNG0Label2.Caption = ""
    'SEGNALAZIONE STATO MNG0 PER PROGRAMMI ESTERNI
    k = WritePrivateProfileString("Configurazione", "MNG0_STATO", "POLLING", Path_LAVORAZIONE_INI)
    'LETTURA DEL TIPO DI LAVORAZIONE
    TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
    TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
    '
    'INIZIALIZZO LA LISTA DEI PEZZI SINGOLI
    INDICE_LAVORAZIONE_LOCALE = 0
    Do
      'INCREMENTO IL CONTATORE
      INDICE_LAVORAZIONE_LOCALE = INDICE_LAVORAZIONE_LOCALE + 1
      'LEGGO IL NOME DEL PEZZO PER L'INDICE DI LAVORAZIONE
      NomePezzo = GetInfo(TIPO_LAVORAZIONE, "NomePezzo(" & Format$(INDICE_LAVORAZIONE_LOCALE, "##0") & ")", Path_LAVORAZIONE_INI)
      NomePezzo = UCase$(Trim$(NomePezzo))
      If (Len(NomePezzo) > 0) Then
        'CONTROLLO SE C'E' QUALCOSA DA FARE NELLA LAVORAZIONE INDICE_LAVORAZIONE_LOCALE
        Azione = GetInfo("Configurazione", "AZIONE(" & Format$(INDICE_LAVORAZIONE_LOCALE, "##0") & ")", Path_LAVORAZIONE_INI)
        Azione = UCase$(Trim$(Azione))
        'DETERMINO L'AZIONE DA COMPIERE
        Select Case Azione
        Case "CARICA"
            'SEGNALAZIONE STATO MNG0 PER PROGRAMMI ESTERNI
            k = WritePrivateProfileString("Configurazione", "MNG0_STATO", "UPLOADING", Path_LAVORAZIONE_INI)
            'IMPOSTO I COLORI
            OEMX.MNG0INTESTAZIONE.BackColor = QBColor(12)
            OEMX.MNG0INTESTAZIONE.ForeColor = QBColor(15)
            'SEGNALO IL CARICAMENTO DEL NOME PEZZO
            OEMX.MNG0INTESTAZIONE.Caption = "UPLOADING " & NomePezzo
            'EVIDENZIO IL NOME PEZZO DA CARICARE NELLA CASELLA DI TESTO
            OEMX.MNG0Text1.Text = NomePezzo
            Call SELEZIONA_TESTO
            'EVIDENZIO IL NOME PEZZO SULLA LA LISTA
            stmp = UCase$(Trim$(OEMX.MNG0Text1.Text))
            For k = 0 To OEMX.MNG0List1(0).ListCount - 1
              If (Trim$(OEMX.MNG0List1(0).List(k)) = stmp) Then
                OEMX.MNG0List1(0).ListIndex = k
                Exit For
              End If
            Next k
            'ALLINEO GLI INDICI
            INDICE_LAVORAZIONE_SINGOLA = INDICE_LAVORAZIONE_LOCALE
            stmp = Format$(INDICE_LAVORAZIONE_SINGOLA, "##0")
            k = WritePrivateProfileString("Configurazione", "INDICE_LAVORAZIONE_SINGOLA", stmp, PathFILEINI)
            k = WritePrivateProfileString(TIPO_LAVORAZIONE, "INDICE_LAVORAZIONE_SINGOLA", stmp, Path_LAVORAZIONE_INI)
            OEMX.MNG0lblINDICE.Caption = LETTERA_LAVORAZIONE
            DoEvents
            Call MNG0_CARICARE(NomePezzo)
            ret = WritePrivateProfileString("Configurazione", "AZIONE(" & Format$(INDICE_LAVORAZIONE_LOCALE, "##0") & ")", "", Path_LAVORAZIONE_INI)
        Case "SCARICA"
            'SEGNALAZIONE STATO MNG0 PER PROGRAMMI ESTERNI
            k = WritePrivateProfileString("Configurazione", "MNG0_STATO", "DOWNLOADING", Path_LAVORAZIONE_INI)
            'IMPOSTO I COLORI
            OEMX.MNG0INTESTAZIONE.BackColor = QBColor(12)
            OEMX.MNG0INTESTAZIONE.ForeColor = QBColor(15)
            'SEGNALO IL CARICAMENTO DEL NOME PEZZO
            OEMX.MNG0INTESTAZIONE.Caption = "DOWNLOADING " & NomePezzo
            'EVIDENZIO IL NOME PEZZO DA CARICARE NELLA CASELLA DI TESTO
            OEMX.MNG0Text1.Text = NomePezzo
            Call SELEZIONA_TESTO
            'EVIDENZIO IL NOME PEZZO SULLA LA LISTA
            stmp = UCase$(Trim$(OEMX.MNG0Text1.Text))
            For k = 0 To OEMX.MNG0List1(0).ListCount - 1
              If (Trim$(OEMX.MNG0List1(0).List(k)) = stmp) Then
                OEMX.MNG0List1(0).ListIndex = k
                Exit For
              End If
            Next k
            'ALLINEO GLI INDICI
            INDICE_LAVORAZIONE_SINGOLA = INDICE_LAVORAZIONE_LOCALE
            stmp = Format$(INDICE_LAVORAZIONE_SINGOLA, "##0")
            k = WritePrivateProfileString("Configurazione", "INDICE_LAVORAZIONE_SINGOLA", stmp, PathFILEINI)
            k = WritePrivateProfileString(TIPO_LAVORAZIONE, "INDICE_LAVORAZIONE_SINGOLA", stmp, Path_LAVORAZIONE_INI)
            OEMX.MNG0lblINDICE.Caption = LETTERA_LAVORAZIONE
            DoEvents
            '>>>>>>>>>LETTURA DEI VALORI DAL CNC<<<<<<<<<<<<<<<<< da riscrivere SVG300516
            Call MNG0_SALVARE(NomePezzo, NomePezzo)
            ret = WritePrivateProfileString("Configurazione", "AZIONE(" & Format$(INDICE_LAVORAZIONE_LOCALE, "##0") & ")", "", Path_LAVORAZIONE_INI)
        End Select
      Else
        Exit Do
      End If
    Loop
    '
    'CONTROLLO COS'E' STATO COMANDATO
    Azione = GetInfo("Configurazione", "AZIONE(" & Format$(INDICE_LAVORAZIONE_SINGOLA, "##0") & ")", Path_LAVORAZIONE_INI)
    Azione = UCase$(Trim$(Azione))
    COMANDO = Split(Azione, ",")
    If (COMANDO(0) = "ESEGUI") Or (COMANDO(0) = "RUN") Then
      Select Case COMANDO(1)
      Case "V"
        Call MDIForm1.Picture2_Click(val(COMANDO(2)))
        ret = WritePrivateProfileString("Configurazione", "AZIONE(" & Format$(INDICE_LAVORAZIONE_SINGOLA, "##0") & ")", "", Path_LAVORAZIONE_INI)
      Case "O", "H"
        Call MDIForm1.Picture1_Click(val(COMANDO(2)))
        ret = WritePrivateProfileString("Configurazione", "AZIONE(" & Format$(INDICE_LAVORAZIONE_SINGOLA, "##0") & ")", "", Path_LAVORAZIONE_INI)
      End Select
    End If
  End If
  '
Exit Sub

errTimer1_Timer:

End Sub

Sub MNG0_TROVA_MEMO(Optional Shift As Integer = 0)
'
Dim Atmp As String * 255
Dim riga As String
Dim stmp As String
'
Dim DB   As Database
Dim TB   As Recordset
'
Dim TIPO_LAVORAZIONE As String
Dim DATA_SALVATAGGIO As String
Dim TABELLA_ARCHIVIO As String
'
Dim ret         As Integer
Dim i           As Integer
Dim k1          As Integer
Dim k2          As Integer
'
Dim DATI        As String
Dim PERCORSO    As String
Dim COORDINATE  As String
Dim RADICE      As String
Dim ELENCO()    As String
Dim sELENCO     As String
Dim NomePezzo   As String
'
On Error Resume Next
  '
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  OEMX.MNG0fraVOITH.Visible = False
  TABELLA_ARCHIVIO = TIPO_LAVORAZIONE
  TABELLA_ARCHIVIO = "ARCHIVIO_" & TABELLA_ARCHIVIO
  NomePezzo = Trim$(OEMX.MNG0List1(0).List(OEMX.MNG0List1(0).ListIndex))
  ret = InStr(NomePezzo, "(")
  If (ret > 1) Then NomePezzo = Trim$(Left$(NomePezzo, ret - 1))
  Set DB = OpenDatabase(PathDB, True, False)
  Set TB = DB.OpenRecordset(TABELLA_ARCHIVIO)
  TB.Index = "NOME_PEZZO"
  TB.Seek "=", NomePezzo
  If Not TB.NoMatch Then
    sELENCO = TB.Fields("ELENCO").Value
    DATA_SALVATAGGIO = TB.Fields("DATA_ARCHIVIAZIONE").Value
    'VISUALIZZAZIONE DEL CAMPO MEMO
    If Not IsNull(TB.Fields("MEMO").Value) Then
      If (Len(TB.Fields("MEMO").Value) > 0) Then
        OEMX.MNG0Text2.Text = TB.Fields("MEMO").Value
      Else
        OEMX.MNG0Text2.Text = ""
      End If
    Else
      OEMX.MNG0Text2.Text = ""
    End If
    'VISUALIZZAZIONE DELLE IMPOSTAZIONI
    If (OEMX.MNG0SSTab1.Tab = 1) Then
      OEMX.MNG0lstIMPOSTAZIONI(1).Clear
      If Not IsNull(TB.Fields("IMPOSTAZIONI").Value) Then
        If (Len(TB.Fields("IMPOSTAZIONI").Value) > 0) Then
          i = 0: k1 = 1: k2 = 1
          For i = 0 To OEMX.MNG0lstIMPOSTAZIONI(0).ListCount - 1
            k2 = InStr(k1, TB.Fields("IMPOSTAZIONI").Value, ";")
            If (k2 > k1) Then
              stmp = Mid$(TB.Fields("IMPOSTAZIONI").Value, k1, k2 - k1)
              stmp = Trim$(stmp)
            Else
              'ELEMENTO NULLO DELLA LISTA: CASO DI DUE SEPARATORI CONSECUTIVI.
              stmp = ""
            End If
            k1 = k2 + 1
            Call OEMX.MNG0lstIMPOSTAZIONI(1).AddItem(stmp)
          Next i
        Else
          For i = 0 To OEMX.MNG0lstIMPOSTAZIONI(0).ListCount - 1
            Call OEMX.MNG0lstIMPOSTAZIONI(1).AddItem("?")
          Next i
        End If
      Else
        For i = 0 To OEMX.MNG0lstIMPOSTAZIONI(0).ListCount - 1
          Call OEMX.MNG0lstIMPOSTAZIONI(1).AddItem("?")
        Next i
      End If
    End If
    'VISUALIZZAZIONE FILE COLLEGATI
    If (TIPO_LAVORAZIONE = "ROTORI_ESTERNI") And (OEMX.MNG0SSTab1.Tab = 2) Then
      DATI = TB.Fields("DATI").Value
      'LETTURA DEL PERCORSO
      k1 = InStr(DATI, "PERCORSO=")
      k2 = InStr(k1, DATI, Chr(10))
      stmp = Mid(DATI, k1, k2 - k1 - 1)
      i = InStr(stmp, "=")
      PERCORSO = Right$(stmp, Len(stmp) - i)
      PERCORSO = g_chOemPATH & "\" & PERCORSO
      'RICAVO IL NOME DEL ROTORE DAL PERCORSO
      COORDINATE = PERCORSO
      If (Len(COORDINATE) > 0) Then
        i = InStr(COORDINATE, "\")
        While (i <> 0)
          j = i
          i = InStr(i + 1, COORDINATE, "\")
        Wend
        'RICAVO IL NOME DEL ROTORE
        COORDINATE = Right$(COORDINATE, Len(COORDINATE) - j)
      End If
      'RICAVO IL PERCORSO DI LAVORO
      RADICE = PERCORSO
      i = InStr(RADICE, COORDINATE)
      RADICE = Left$(RADICE, i - 2)
      RADICE = RADICE & "\"
      If (Dir$(RADICE) <> "") Then
        If OEMX.MNG0FileList.Visible = False Then OEMX.MNG0FileList.Visible = True
        OEMX.MNG0FileList.Path = RADICE
      Else
        OEMX.MNG0FileList.Visible = False
        OEMX.MNG0FileList.Path = ""
      End If
    End If
  Else
    OEMX.MNG0Text2.Text = ""
    OEMX.MNG0Text2.Text = OEMX.MNG0Text2.Text & TABELLA_ARCHIVIO & " "
    OEMX.MNG0Text2.Text = OEMX.MNG0Text2.Text & OEMX.MNG0Text1.Text & " "
    OEMX.MNG0Text2.Text = OEMX.MNG0Text2.Text & " NOT FOUND!!!"
  End If
  TB.Close
  DB.Close
  ret = LoadString(g_hLanguageLibHandle, 1003, Atmp, 255)
  If (ret > 0) Then riga = Left$(Atmp, ret)
  WRITE_DIALOG riga & " " & NomePezzo & " : " & DATA_SALVATAGGIO
  '***************************************************************************************
  If (Len(sELENCO) > 0) And (Shift = 2) Then
    ELENCO = Split(sELENCO, ";")
    stmp = " CNC --> HD:" & Chr(13)
    For i = 0 To UBound(ELENCO) - 1
      stmp = stmp & Format$(i + 1, "##0") & ") " & ELENCO(i) & Chr(13)
    Next i
    Call MsgBox(stmp, vbInformation, "WARNING: " & NomePezzo)
  End If
  '***************************************************************************************
  '
End Sub

Sub MNG0_TROVA_ROTORDIRECTORY()
'
Dim Atmp  As String * 255
Dim riga  As String
Dim stmp As String
'
Dim DB   As Database
Dim TB   As Recordset
'
Dim DATI        As String
Dim PERCORSO    As String
Dim COORDINATE  As String
Dim RADICE      As String
'
Dim ret              As Integer
Dim i                As Integer
Dim k1               As Integer
Dim k2               As Integer
'
On Error Resume Next
  '
  'APRO IL DATABASE
  Set DB = OpenDatabase(PathDB, True, False)
  Set TB = DB.OpenRecordset("ARCHIVIO_ROTORI_ESTERNI")
  'RICERCO NELLA TABELLA ARCHIVIO SE ESISTE UNA ARCHIVIAZIONE
  TB.Index = "NOME_PEZZO"
  TB.Seek "=", OEMX.MNG0Text1.Text
  If Not TB.NoMatch Then
    DATI = TB.Fields("DATI").Value
    'LETTURA DEL PERCORSO
    k1 = InStr(DATI, "PERCORSO=")
    k2 = InStr(k1, DATI, Chr(10))
    stmp = Mid(DATI, k1, k2 - k1 - 1)
    i = InStr(stmp, "=")
    PERCORSO = Right$(stmp, Len(stmp) - i)
    PERCORSO = g_chOemPATH & "\" & PERCORSO
    'RICAVO IL NOME DEL ROTORE DAL PERCORSO
    COORDINATE = PERCORSO
    If (Len(COORDINATE) > 0) Then
      i = InStr(COORDINATE, "\")
      While (i <> 0)
        j = i
        i = InStr(i + 1, COORDINATE, "\")
      Wend
      'RICAVO IL NOME DEL ROTORE
      COORDINATE = Right$(COORDINATE, Len(COORDINATE) - j)
    End If
    'RICAVO IL PERCORSO DI LAVORO
    RADICE = PERCORSO
    i = InStr(RADICE, COORDINATE)
    RADICE = Left$(RADICE, i - 2)
    RADICE = RADICE & "\"
    If (Dir$(RADICE) <> "") Then
      If OEMX.MNG0FileList.Visible = False Then OEMX.MNG0FileList.Visible = True
      OEMX.MNG0FileList.Path = RADICE
    Else
      OEMX.MNG0FileList.Visible = False
      OEMX.MNG0FileList.Path = ""
    End If
  Else
    OEMX.MNG0FileList.Visible = False
    OEMX.MNG0FileList.Path = ""
    OEMX.MNG0Text2.Text = ""
    OEMX.MNG0Text2.Text = OEMX.MNG0Text2.Text & OEMX.MNG0Text1.Text & " "
    OEMX.MNG0Text2.Text = OEMX.MNG0Text2.Text & " NOT FOUND!!!"
  End If
  'CHIUDO IL DATABASE
  TB.Close
  DB.Close
  '
End Sub

Private Sub AggiungiZeriInArchivio(ByRef stmp As String, ByVal idxZero As Integer)
'
Dim kk1   As Integer
Dim kk2   As Integer
Dim j     As Integer
Dim i     As Integer
Dim VV()  As Double
'
On Error Resume Next
  '
  kk1 = 0
  kk2 = InStr(kk1 + 1, stmp, ";")
  j = 1
  Do
    If kk2 > kk1 Then
      ReDim Preserve VV(j)
      VV(j) = val(Mid$(stmp, kk1 + 1, kk2 - kk1 - 1))
      j = j + 1
      kk1 = kk2
      kk2 = InStr(kk1 + 1, stmp, ";")
    End If
  Loop While (kk2 > kk1)
  
  stmp = ""
  For i = 1 To j - 1
    If i = idxZero Then stmp = stmp & "0;"
    stmp = stmp & CStr(VV(i)) & ";"
  Next i
  
End Sub

Sub SbloccaPRG0()
'
Dim i As Integer
'
On Error Resume Next
  '
  'Blocco SK
  For i = 0 To 11
    Call Unlock_Softkey(i)
  Next i
  '
  Call Change_SkTextOnScr(12, " ")  '"LOGIN"
  OEMX.PRG0lblLOGIN.Caption = ""
  '
  For i = 13 To 15
    Call Unlock_Softkey(i)
  Next i
  Call INIZIALIZZA_PRG0
  OEMX.PRG0Frame1.Visible = True
  OEMX.PRG0lblLOGIN.Visible = False
  '
End Sub

Private Sub Form_Unload(Cancel As Integer)
'
Dim i                 As Integer
Dim stmp              As String
Dim COMMENTO          As String
Dim TIPO_LAVORAZIONE  As String
'
On Error Resume Next
  '
  'SEGNALAZIONE STATO MNG0 PER PROGRAMMI ESTERNI
  i = WritePrivateProfileString("Configurazione", "MNG0_STATO", "", Path_LAVORAZIONE_INI)
  'LETTURA DEL TIPO DI LAVORAZIONE
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  'CONSIDERO SOLO I CARATTERI MAIUSCOLI
  stmp = UCase$(Trim$(OEMX.MNG0Text1.Text))
  'LEGGO COMUNQUE FINO AL PRIMO SPAZIO
  i = InStr(stmp, " "): If i > 0 Then stmp = Left$(stmp, i - 1)
  i = WritePrivateProfileString(TIPO_LAVORAZIONE, "NomePezzoInserimento", stmp, Path_LAVORAZIONE_INI)
  'AGGIORNO IL FILE MEMO
  COMMENTO = OEMX.MNG0Text2.Text
  Do
    If Right$(COMMENTO, 1) = Chr$(10) Or Right$(COMMENTO, 1) = Chr$(13) Then
      COMMENTO = Left$(COMMENTO, Len(COMMENTO) - 1)
    Else
      Exit Do
    End If
  Loop
  'SALVO IL CONTENUTO DEL CAMPO MEMO
  Open PathFileMemo For Output As #1
    Print #1, Trim$(COMMENTO);
  Close #1
  '
End Sub

Private Sub OEM1hsROTORE_Change()
'
On Error Resume Next
  '
  'LETTURA DEGLI INDICI DEI PUNTI DI RIFERIMENTO PER LO ZERO PEZZO
  OEMX.OEM1txtOX.Text = Format$(OEMX.OEM1hsROTORE.Value, "######0")
  OEMX.OEM1txtOY.Text = Format$(OEMX.OEM1hsROTORE.Value, "######0")
  Call VISUALIZZA_PROFILO_RAB_ROTORI(OEMX, "N", val(OEMX.txtINP(0).Text), val(OEMX.OEM1txtOX.Text), val(OEMX.OEM1txtOY.Text), OEMX.OEM1chkINPUT(0).Value, OEMX.OEM1chkINPUT(1).Value, OEMX.OEM1chkINPUT(2).Value)
  '
End Sub

Private Sub OEM1ListaCombo1_LostFocus()

  OEMX.OEM1ListaCombo1.Visible = False

End Sub

Private Sub OEM1txtOX_LostFocus()
'
On Error Resume Next
  '
  OEMX.OEM1lblOX.BackColor = &H80FFFF
  OEMX.OEM1lblOX.ForeColor = QBColor(0)

End Sub

Private Sub OEM1txtOY_LostFocus()
'
On Error Resume Next
  '
  OEMX.OEM1lblOY.BackColor = &H80FFFF
  OEMX.OEM1lblOY.ForeColor = QBColor(0)

End Sub

Private Sub PRG0Image1_Click()
'PROGRAMMA: CICLO14.SPF
'VERSIONE : 04.11.15
'
'Dim RTMP  As Double
'Const NUMSET = 2
'Dim ND1   As Integer
'Dim ND2   As Integer
'Dim ND3   As Integer
'Dim ii    As Integer
'Dim jj    As Integer
'Dim BA1   As Double
'Dim BA2   As Double
'Dim R144  As Double
'Dim stmp  As String
'Const R5 = 26
'Const NUMDIV = 3
'Dim CONTATORE As Integer
''
'R144 = 0
'BA1 = R144
'ND1 = TRUNC(R5 / NUMSET)
'ND2 = TRUNC(ND1 / NUMDIV) + 1
'ND3 = NUMSET * NUMDIV * ND2
'
'   CONTATORE = 0
'   stmp = "R5=" & R5 & " NUMDIV=" & NUMDIV & " NUMSET=" & NUMSET & Chr(13)
'   stmp = stmp & "ND1=" & ND1 & " ND2=" & ND2 & " ND3=" & ND3 & Chr(13)
'   For jj = 1 To ND2
'     BA2 = BA1 + (jj - 1) * (360 / R5) * NUMDIV
'     For ii = 1 To NUMSET
'      CONTATORE = CONTATORE + 1
'       R144 = BA2 + (ii - 1) * (360 / R5) * ND1
'       stmp = stmp & Format$(CONTATORE * NUMDIV, "##0") & "  (" & Format$(jj, "#0") & "," & Format$(ii, "#0") & ") -> " & frmt(R144, 4) & Chr(13)
'       If (CONTATORE * NUMDIV >= R5) Then Exit For
'     Next ii
'   Next jj
'   MsgBox stmp, vbCritical, "CICLO14"
'
End Sub

Private Sub PRG0Intestazione_Click()
'
On Error Resume Next
  '
  If (OEMX.PRG0SelezioneLavorazioni.Visible = False) Then
    OEMX.PRG0SelezioneLavorazioni.Visible = True
    OEMX.PRG0TipoLavorazione.Visible = False
    OEMX.PRG0SelezioneLavorazioni.Top = 0
    OEMX.PRG0SelezioneLavorazioni.Left = OEMX.PRG0txtNomePezzo.Left
    OEMX.PRG0SelezioneLavorazioni.Width = OEMX.PRG0txtNomePezzo.Width
  Else
    OEMX.PRG0SelezioneLavorazioni.Visible = False
    OEMX.PRG0TipoLavorazione.Visible = True
  End If
  '
End Sub

Private Sub PRG0Intestazione_GotFocus()
'
On Error Resume Next
  '
  'SISTEMO I COLORI
  OEMX.PRG0TipoLavorazione.BackColor = QBColor(12)
  OEMX.PRG0TipoLavorazione.ForeColor = &HFFFFFF
  WRITE_DIALOG ""
  '
End Sub

Private Sub PRG0Intestazione_LostFocus()
'
On Error Resume Next
  '
  'SISTEMO I COLORI
  OEMX.PRG0TipoLavorazione.BackColor = &HFFFFFF
  OEMX.PRG0TipoLavorazione.ForeColor = &H80000008

End Sub

Private Sub PRG0SelezioneLavorazioni_GotFocus()
'
Dim i                As Integer
Dim L                As Integer
Dim LL               As Integer
Dim stmp1            As String
Dim stmp3            As String
Dim TIPO_LAVORAZIONE As String
Dim ret              As Long
Dim Atmp             As String * 255
'
On Error Resume Next
  '
  OEMX.PRG0SelezioneLavorazioni.Clear
  'LETTURA DEL TIPO DI LAVORAZIONE
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  'CONTROLLO SE E' UN TIPO DI LAVORAZIONE CONSENTITO
  i = 1: L = 1
  Do
    'TIPO DI LAVORAZIONE
    stmp1 = GetInfo("TIPO_LAVORAZIONE", "TIPO(" & Format$(i, "####0") & ")", PathFILEINI)
    stmp1 = UCase$(Trim$(stmp1))
    If (Len(stmp1) > 0) Then
      stmp3 = GetInfo("TIPO_LAVORAZIONE", "NOMETIPO(" & Format$(i, "####0") & ")", PathFILEINI)
      If IsNumeric(stmp3) Then
        'LEGGO DALLA DLL
        ret = val(stmp3)
        ret = LoadString(g_hLanguageLibHandle, ret, Atmp, 255)
        If ret > 0 Then stmp3 = Left$(Atmp, ret) Else stmp3 = "No name at " & stmp3
      End If
      Call OEMX.PRG0SelezioneLavorazioni.AddItem(" " & stmp3)
      INDICE_TIPO(L) = i
      L = L + 1
      If (TIPO_LAVORAZIONE = stmp1) Then LL = i - 1
    Else
      Exit Do
    End If
    'INCREMENTO IL CONTATORE
    i = i + 1
  Loop
  OEMX.PRG0SelezioneLavorazioni.ListIndex = LL
  OEMX.PRG0SelezioneLavorazioni.Height = OEMX.PRG0TipoLavorazione.Height * OEMX.PRG0SelezioneLavorazioni.ListCount
  '
End Sub

Private Sub PRG0SelezioneLavorazioni_KeyDown(KeyCode As Integer, Shift As Integer)
  
  If (KeyCode = 13) Then Call PRG0SelezioneLavorazioni_DblClick
  
End Sub

Private Sub PRG0SelezioneLavorazioni_LostFocus()
'
On Error Resume Next
  '
  OEMX.PRG0SelezioneLavorazioni.Visible = False
  OEMX.PRG0TipoLavorazione.Visible = True
  'SISTEMO I COLORI
  OEMX.PRG0TipoLavorazione.BackColor = &HFFFFFF
  OEMX.PRG0TipoLavorazione.ForeColor = &H80000008
  OEMX.PRG0Intestazione.SetFocus
  
End Sub

Private Sub PRG0txtMATRICOLA_GotFocus()
'
On Error Resume Next
  '
  Call SELEZIONA_TESTO
  '
End Sub

Private Sub PRG0txtMATRICOLA_KeyUp(KeyCode As Integer, Shift As Integer)
'
Dim stmp1 As String
Dim stmp2 As String
Dim stmp3 As String
Dim ret   As Integer
'
On Error Resume Next
  '
  If (KeyCode = 13) Then
    'MATRICOLA VECCHIA
    stmp1 = GetInfo("Configurazione", "MATRICOLA", PathFILEINI)
    stmp1 = UCase$(Trim$(stmp1))
    'MATRICOLA NUOVA
    stmp2 = UCase$(Trim$(OEMX.PRG0txtMATRICOLA.Text))
    'AGGIORNO SOLO SE SONO DIVERSI
    If (stmp1 <> stmp2) Then
      'LEGGO COMUNQUE FINO AL PRIMO SPAZIO
      ret = InStr(stmp2, " "): If ret > 0 Then stmp2 = Left$(stmp2, ret - 1)
      OEMX.PRG0txtMATRICOLA.Text = stmp2
      ret = WritePrivateProfileString("Configurazione", "MATRICOLA", stmp2, PathFILEINI)
      WRITE_DIALOG OEMX.PRG0lblMATRICOLA.Caption & ": " & stmp1 & " --> " & stmp2
      'SCRIVO NEL PARAMETRO GUD
      stmp3 = GetInfo("Configurazione", "MATRICOLA_DDE", PathFILEINI)
      Call OPC_SCRIVI_DATO(stmp3, stmp2)
    End If
  End If
  '
End Sub

Private Sub PRG0txtNomePezzo_GotFocus()
'
On Error Resume Next
  '
  Call SELEZIONA_TESTO
  '
End Sub

Private Sub PRG0txtNomePezzo_KeyUp(KeyCode As Integer, Shift As Integer)
'
Dim stmp1             As String
Dim stmp2             As String
Dim ret               As Integer
Dim TIPO_LAVORAZIONE  As String
'
On Error Resume Next
  '
  If (KeyCode = 13) Then
    'LETTURA DEL TIPO DI LAVORAZIONE
    TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
    TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
    'NOME PEZZO VECCHIO
    stmp1 = LEGGI_NomePezzo(TIPO_LAVORAZIONE)
    'NOME PEZZO NUOVO
    stmp2 = UCase$(Trim$(OEMX.PRG0txtNomePezzo.Text))
    'AGGIORNO SOLO SE SONO DIVERSI
    If (stmp1 <> stmp2) Then
      'LEGGO COMUNQUE FINO AL PRIMO SPAZIO
      ret = InStr(stmp2, " "): If ret > 0 Then stmp2 = Left$(stmp2, ret - 1)
      OEMX.PRG0txtNomePezzo.Text = " " & stmp2
      ret = WritePrivateProfileString("Configurazione", "NomePezzo", stmp2, PathFILEINI)
      Call SCRIVI_NomePezzo(TIPO_LAVORAZIONE, stmp2)
      WRITE_DIALOG OEMX.PRG0Label5.Caption & ": " & stmp1 & " --> " & stmp2
    End If
    '
  End If
  '
End Sub

Private Sub EstraiItem(Strcmd As String, SORG As String, sDest As String, wArea As Byte, wData As Byte, wTout As Long, tipGud As String)
'
Dim Virgola   As String
Dim INIZIO    As Integer
Dim Pos       As Integer
Dim elem(10)  As String
Dim i         As Integer
Dim lungstr   As Integer
Dim ret       As Long
Dim Atmp      As String * 255
Dim stmp      As String
'
On Error GoTo errEstraiItem
  '
  Virgola = ","
  INIZIO = 1
  '
  i = 0
  Pos = InStr(INIZIO, Strcmd, "/")
  elem(i) = Mid(Strcmd, INIZIO, Pos - 1)
  INIZIO = Pos
  For i = 1 To 6
    Pos = InStr(INIZIO, Strcmd, Virgola)
    lungstr = Pos - INIZIO
    elem(i) = Mid(Strcmd, INIZIO, lungstr)
    INIZIO = Pos + 1
  Next i
  '
  SORG = LTrim(elem(1))
  sDest = LTrim(elem(2))
  wArea = CByte(elem(3))
  wData = CByte(elem(4))
  wTout = CLng(elem(5))
  tipGud = LTrim(elem(6))
  '
Exit Sub

errEstraiItem:
  ret = LoadString(g_hLanguageLibHandle, 316, Atmp, 255)
  stmp = Left$(Atmp, ret) & Chr(13)
  stmp = stmp & "Errore " & str(Err.Number) & " generato da " & Err.Source & Chr(13) & Err.Description
  StopRegieEvents
  MsgBox stmp, , "Errore", Err.HelpFile, Err.HelpContext
  ResumeRegieEvents
  Resume Next

End Sub

Private Sub McListPrg_Click()
'
On Error Resume Next
  '
  Call PRG0_AIUTO_PROGRAMMI
  If (McListPrg.ListIndex > -1) Then
    WRITE_DIALOG McListPrg.List(McListPrg.ListIndex) & " --> CNC"
    PRG0Frame1.BackColor = &HEBE1D7
    PRG0Frame1.ForeColor = QBColor(12)
  End If
  '
End Sub

Private Sub McListPrg_GotFocus()
'
Dim i                As Integer
Dim stmp             As String
Dim TIPO_LAVORAZIONE As String
'
On Error Resume Next
  '
  'SISTEMO I COLORI
  PRG0Frame1.BackColor = &HEBE1D7
  PRG0Frame1.ForeColor = QBColor(12)
  'POSIZIONAMENTO SULL'ULTIMO PROGRAMMA SELEZIONATO
  If (McListPrg.ListCount > 0) Then
    'LETTURA DEL TIPO DI LAVORAZIONE
    TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
    TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
    'LETTURA INDICE PROGRAMMA SELEZIONATO PER ULTIMO
    stmp = GetInfo(TIPO_LAVORAZIONE, "UltimaSelezione", Path_LAVORAZIONE_INI)
    i = val(stmp)
    If (i <= McListPrg.ListCount) Then McListPrg.ListIndex = i - 1
  End If
  '
End Sub

Private Sub McListPrg_LostFocus()
'
On Error Resume Next
  '
  PRG0Frame1.BackColor = &HEBE1D7
  PRG0Frame1.ForeColor = &H80000008
  '
End Sub

Private Sub lblINP_Click(Index As Integer)
'
Dim DBB               As Database
Dim RSS               As Recordset
Dim pXX(2, 9)         As Double
Dim pYY(2, 9)         As Double
Dim k1                As Long
Dim k2                As Long
Dim k3                As Long
Dim rsp               As Long
Dim CODICE_PEZZO      As String
Dim lstTOLLERANZA     As String
Dim MODALITA          As String
Dim TIPO_LAVORAZIONE  As String
'
On Error Resume Next
  '
  'LEGGO LA MODALITA DI TRASFERIMENTO DATI
  MODALITA = GetInfo("Configurazione", "MODALITA", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))
  'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  'Calcolo spostamento rullo
  If (TIPO_LAVORAZIONE = "MOLAVITE") Or (TIPO_LAVORAZIONE = "MOLAVITE_SG") Or (TIPO_LAVORAZIONE = "MOLAVITEG160") Then
    If (Index = 0) Then
      Call calcolaspostamentorullo
    ElseIf (Index = 2) Then ' get correction
      Dim ret As Long
      Dim Specs(1 To 18)
      '
      Call Get_WormWheel_Specs(Specs)
      '
      ret = MsgBox("Acquire dressing correction? (Spr, Ccd, Aput) = (" & frmt(Specs(2), 4) & ", " & frmt(Specs(18), 4) & ", " & frmt(Specs(5), 4) & ")", vbQuestion + vbYesNo, "WARNING")
      If (ret = vbYes) Then
        '
        Dim NumLAV  As Integer
        Dim tsez    As String
        Dim Tvar    As String
        Dim tDat    As String
        Dim stmp    As String
        '
        stmp = frmt(Specs(2), 4)
        NumLAV = CInt(Trim(Asc(LETTERA_LAVORAZIONE) - 64))
        tsez = "CNC_" & Chr(Asc("A") - 1 + NumLAV)  ' nuovo, vecchio
        Tvar = "CORSPESSOREMV_G"
        tDat = frmt(Specs(2), 4)
        Dim iCORSPESSOREMV_G      As Double
        Dim iCORSPESSOREMV_G_CORR As Double
        Dim CORSPESSOREMV_G       As Double
        iCORSPESSOREMV_G = Specs(2)
        Call SP("CORSPESSOREMV_G", Round(Specs(2), 4)) ', True
        Call SP("ALFAUT_G", Round(Specs(5), 4)) ', True
        Call SP("MNUT_G", Round(Specs(4), 4))
        stmp = frmt(Specs(18), 4)
        Dim iCORINTLAV_G11, iCORINTLAV_G_CORR, CORINTLAV_G11 As Double
        iCORINTLAV_G11 = Specs(18)
        'Agg.4.12.2013
        Call SP("CORINTERASSE", Round(iCORINTLAV_G11, 3)) ', True
        'Angolo di pressione mola a vite
        If (Tabella1.MODALITA = "SPF") Then Call SuOEM1.CreaSPF
          For i = 0 To 8
            Call Lock_Softkey(i)
            If (i <> 3) Then
              'VSoftkey.McsText(i).State = mcSKSLocked
            End If
          Next i
        End If
        Call CALCOLO_INGRANAGGIO_ESTERNO_380_MV(0)
      Else
        Call CALCOLO_INGRANAGGIO_ESTERNO_380_MV(0)
      End If
      GoTo ESCI
    End If
    If (MODALITA = "OFF-LINE") Then
      CODICE_PEZZO = GetInfo(TIPO_LAVORAZIONE, "NomePezzoInserimento", Path_LAVORAZIONE_INI)
    Else
      CODICE_PEZZO = LEGGI_NomePezzo(TIPO_LAVORAZIONE)
    End If
    CODICE_PEZZO = UCase$(Trim$(CODICE_PEZZO))
    'LEGGO COMUNQUE FINO AL PRIMO SPAZIO
    rsp = InStr(CODICE_PEZZO, " "): If rsp > 0 Then CODICE_PEZZO = Left$(CODICE_PEZZO, rsp - 1)
    'LEGGO COMUNQUE FINO AL PRIMO PUNTO
    rsp = InStr(CODICE_PEZZO, "."): If rsp > 0 Then CODICE_PEZZO = Left$(CODICE_PEZZO, rsp - 1)
    Set DBB = OpenDatabase(PathDB, True, False)
    Set RSS = DBB.OpenRecordset("ARCHIVIO_" & TIPO_LAVORAZIONE)
    RSS.Index = "NOME_PEZZO"
    RSS.Seek "=", CODICE_PEZZO
    If Not RSS.NoMatch Then
      If (fraTOLLERANZA.Visible = True) Then
        fraTOLLERANZA.Visible = False
        'SALVATAGGIO
        lstTOLLERANZA = ""
        For j = 1 To 8
          lstTOLLERANZA = lstTOLLERANZA & frmt(val(txtPXXF1(j - 1)), 8) & ";"
          lstTOLLERANZA = lstTOLLERANZA & frmt(val(txtPYYF1(j - 1)), 8) & ";"
        Next j
        For j = 1 To 8
          lstTOLLERANZA = lstTOLLERANZA & frmt(val(txtPXXF2(j - 1)), 8) & ";"
          lstTOLLERANZA = lstTOLLERANZA & frmt(val(txtPYYF2(j - 1)), 8) & ";"
        Next j
        RSS.Edit
        RSS.Fields("TOLLERANZA_PROFILO").Value = lstTOLLERANZA
        RSS.Update
        WRITE_DIALOG CODICE_PEZZO & " PROFILE TOLLERANCE HAVE BEEN SAVED"
      Else
        'LETTURA PER MODIFICA
        fraTOLLERANZA.Visible = True
        fraTOLLERANZA.Move OEM1PicCALCOLO.Left, OEM1PicCALCOLO.Top, OEM1PicCALCOLO.Width, OEM1PicCALCOLO.Height
        For i = 0 To 7
          IF1(i) = "p" & Format$(i + 1, "0")
          IF2(i) = "p" & Format$(i + 1, "0")
        Next i
        fraTOLLERANZA.Caption = ""
        fraTOLLERANZA.Visible = True
        If Len(RSS.Fields("TOLLERANZA_PROFILO").Value) > 0 Then
          lstTOLLERANZA = UCase$(Trim$(RSS.Fields("TOLLERANZA_PROFILO").Value))
        Else
          lstTOLLERANZA = ""
        End If
        '
        'RICAVO I SINGOLI VALORI DALLA LISTA
        If (Len(lstTOLLERANZA) > 0) Then
          k1 = InStr(lstTOLLERANZA, ";;")
          If (k1 <= 0) Then
            k1 = InStr(lstTOLLERANZA, ";")
            k2 = InStr(k1 + 1, lstTOLLERANZA, ";")
            k3 = 1
            For j = 1 To 8
              txtPXXF1(j - 1) = Mid$(lstTOLLERANZA, k3, k1 - k3)
              txtPYYF1(j - 1) = Mid$(lstTOLLERANZA, k1 + 1, k2 - k1 - 1)
              k3 = k2 + 1
              k1 = InStr(k3, lstTOLLERANZA, ";")
              k2 = InStr(k1 + 1, lstTOLLERANZA, ";")
            Next j
            For j = 1 To 8
              txtPXXF2(j - 1) = Mid$(lstTOLLERANZA, k3, k1 - k3)
              txtPYYF2(j - 1) = Mid$(lstTOLLERANZA, k1 + 1, k2 - k1 - 1)
              k3 = k2 + 1
              k1 = InStr(k3, lstTOLLERANZA, ";")
              k2 = InStr(k1 + 1, lstTOLLERANZA, ";")
            Next j
          End If
        Else
          For j = 1 To 8
            txtPXXF1(j - 1) = "0"
            txtPYYF1(j - 1) = "0"
          Next j
          For j = 1 To 8
            txtPXXF2(j - 1) = "0"
            txtPYYF2(j - 1) = "0"
          Next j
        End If
        WRITE_DIALOG CODICE_PEZZO & " PROFILE TOLLERANCE HAVE BEEN READ"
      End If
  Else
    WRITE_DIALOG CODICE_PEZZO & " NOT FOUND!!!"
  End If
  RSS.Close
  DBB.Close
  '
ESCI:
Exit Sub

End Sub

Private Sub OEM1Command2_Click()
'
Dim stmp  As String
Dim Atmp  As String * 255
Dim ret   As Integer
Dim i     As Integer
'
On Error Resume Next
  '
  If fraTOLLERANZA.Visible = True Then fraTOLLERANZA.Visible = False
  If OEMX.OEM1fraOpzioni.Visible = True Then
    OEMX.OEM1fraOpzioni.Visible = False
  Else
    Select Case ACTUAL_PARAMETER_GROUP
      '
      'ELABORAZONI DATI INGRANAGGI ESTERNI
      Case "OEM1_PROFILO91", "OEM1_PROFILOK91", "OEM1_LAVORO91", "OEM1_CORRELICA91", "OEM1_MOLA91", "OEM1_MOLAB91"
        OEMX.OEM1fraOpzioni.Visible = True
        OEMX.OEM1fraOpzioni.Left = OEMX.OEM1PicCALCOLO.Left + OEMX.OEM1PicCALCOLO.Width / 2 - OEMX.OEM1fraOpzioni.Width / 2
        OEMX.OEM1fraOpzioni.Top = OEMX.OEM1PicCALCOLO.Top + OEMX.OEM1PicCALCOLO.Height / 2 - OEMX.OEM1fraOpzioni.Height / 2
        OEMX.OEM1fraOpzioni.Caption = OEMX.OEM1Command2.Caption
        OEMX.OEM1fraOpzioni.BackColor = &HFFFFFF
        For i = 1 To 10
          ret = LoadString(g_hLanguageLibHandle, 1120 + (i - 1), Atmp, 255)
          If ret > 0 Then stmp = Left$(Atmp, ret) Else stmp = ""
          OEMX.lblDonnee(i).Caption = stmp
          OEMX.lblDonnee(i).BackColor = &HFFFFFF
          OEMX.txtDonnee(i).BackColor = &HFFFFFF
          OEMX.lblDonnee(i).Visible = True
          OEMX.txtDonnee(i).Visible = True
        Next i
        OEMX.txtDonnee(1).Text = GetInfo("CALCOLO", "CorINT", Path_LAVORAZIONE_INI)
        OEMX.txtDonnee(2).Text = GetInfo("CALCOLO", "CorINCL", Path_LAVORAZIONE_INI)
        OEMX.txtDonnee(3).Text = GetInfo("CALCOLO", "DeltaX", Path_LAVORAZIONE_INI)
        OEMX.txtDonnee(4).Text = GetInfo("CALCOLO", "ConCorr", Path_LAVORAZIONE_INI)
        OEMX.txtDonnee(5).Text = GetInfo("CALCOLO", "SAP", Path_LAVORAZIONE_INI)
        OEMX.txtDonnee(6).Text = GetInfo("CALCOLO", "EAP", Path_LAVORAZIONE_INI)
        OEMX.txtDonnee(7).Text = GetInfo("CALCOLO", "NW", Path_LAVORAZIONE_INI)
        OEMX.txtDonnee(8).Text = GetInfo("CALCOLO", "DPG", Path_LAVORAZIONE_INI)
        If Len(OEMX.lblDonnee(9).Caption) <= 0 Then OEMX.lblDonnee(9).Caption = "efHa1"
        If Len(OEMX.lblDonnee(10).Caption) <= 0 Then OEMX.lblDonnee(10).Caption = "efHa2"
        OEMX.txtDonnee(9).Text = GetInfo("CALCOLO", "efHa1", Path_LAVORAZIONE_INI)
        OEMX.txtDonnee(10).Text = GetInfo("CALCOLO", "efHa2", Path_LAVORAZIONE_INI)
        OEMX.lblDonnee(4).Visible = False
        OEMX.txtDonnee(4).Visible = False
        OEMX.txtDonnee(1).SetFocus
        '
      'ELABORAZONI DATI INGRANAGGI ESTERNIO
      Case "OEM1_PROFILO92", "OEM1_PROFILOK92", "OEM1_LAVORO92", "OEM1_CORRELICA92", "OEM1_MOLA92", "OEM1_MOLAB92"
        OEMX.OEM1fraOpzioni.Visible = True
        OEMX.OEM1fraOpzioni.Left = OEMX.OEM1PicCALCOLO.Left + OEMX.OEM1PicCALCOLO.Width / 2 - OEMX.OEM1fraOpzioni.Width / 2
        OEMX.OEM1fraOpzioni.Top = OEMX.OEM1PicCALCOLO.Top + OEMX.OEM1PicCALCOLO.Height / 2 - OEMX.OEM1fraOpzioni.Height / 2
        OEMX.OEM1fraOpzioni.Caption = OEMX.OEM1Command2.Caption
        OEMX.OEM1fraOpzioni.BackColor = &HFFFFFF
        For i = 1 To 10
          ret = LoadString(g_hLanguageLibHandle, 1120 + (i - 1), Atmp, 255)
          If ret > 0 Then stmp = Left$(Atmp, ret) Else stmp = ""
          OEMX.lblDonnee(i).Caption = stmp
          OEMX.lblDonnee(i).BackColor = &HFFFFFF
          OEMX.txtDonnee(i).BackColor = &HFFFFFF
          OEMX.lblDonnee(i).Visible = True
          OEMX.txtDonnee(i).Visible = True
        Next i
        OEMX.txtDonnee(1).Text = GetInfo("CALCOLO", "CorINT", Path_LAVORAZIONE_INI)
        OEMX.txtDonnee(2).Text = GetInfo("CALCOLO", "CorINCL", Path_LAVORAZIONE_INI)
        OEMX.txtDonnee(3).Text = GetInfo("CALCOLO", "DeltaX", Path_LAVORAZIONE_INI)
        OEMX.txtDonnee(4).Text = GetInfo("CALCOLO", "ConCorr", Path_LAVORAZIONE_INI)
        OEMX.txtDonnee(5).Text = GetInfo("CALCOLO", "SAP", Path_LAVORAZIONE_INI)
        OEMX.txtDonnee(6).Text = GetInfo("CALCOLO", "EAP", Path_LAVORAZIONE_INI)
        OEMX.txtDonnee(7).Text = GetInfo("CALCOLO", "NW", Path_LAVORAZIONE_INI)
        OEMX.txtDonnee(8).Text = GetInfo("CALCOLO", "DPG", Path_LAVORAZIONE_INI)
        If Len(OEMX.lblDonnee(9).Caption) <= 0 Then OEMX.lblDonnee(9).Caption = "efHa1"
        If Len(OEMX.lblDonnee(10).Caption) <= 0 Then OEMX.lblDonnee(10).Caption = "efHa2"
        OEMX.txtDonnee(9).Text = GetInfo("CALCOLO", "efHa1", Path_LAVORAZIONE_INI)
        OEMX.txtDonnee(10).Text = GetInfo("CALCOLO", "efHa2", Path_LAVORAZIONE_INI)
        OEMX.lblDonnee(4).Visible = False
        OEMX.txtDonnee(4).Visible = False
        OEMX.txtDonnee(1).SetFocus
        '
      'ELABORAZONI DATI SCANALATI EVOLVENTE
      Case "OEM1_PROFILO13", "OEM1_PROFILOK13", "OEM1_LAVORO13", "OEM1_CORRELICA13", "OEM1_MOLA13", "OEM1_MOLAB13"
        OEMX.OEM1fraOpzioni.Visible = True
        OEMX.OEM1fraOpzioni.Left = OEMX.OEM1PicCALCOLO.Left + OEMX.OEM1PicCALCOLO.Width / 2 - OEMX.OEM1fraOpzioni.Width / 2
        OEMX.OEM1fraOpzioni.Top = OEMX.OEM1PicCALCOLO.Top + OEMX.OEM1PicCALCOLO.Height / 2 - OEMX.OEM1fraOpzioni.Height / 2
        OEMX.OEM1fraOpzioni.Caption = OEMX.OEM1Command2.Caption
        OEMX.OEM1fraOpzioni.BackColor = &HFFFFFF
        For i = 1 To 10
          ret = LoadString(g_hLanguageLibHandle, 1120 + (i - 1), Atmp, 255)
          If ret > 0 Then stmp = Left$(Atmp, ret) Else stmp = ""
          OEMX.lblDonnee(i).Caption = stmp
          OEMX.lblDonnee(i).BackColor = &HFFFFFF
          OEMX.txtDonnee(i).BackColor = &HFFFFFF
          OEMX.lblDonnee(i).Visible = True
          OEMX.txtDonnee(i).Visible = True
        Next i
        OEMX.txtDonnee(1).Text = GetInfo("CALCOLO", "CorINT", Path_LAVORAZIONE_INI)
        OEMX.txtDonnee(2).Text = GetInfo("CALCOLO", "CorINCL", Path_LAVORAZIONE_INI)
        OEMX.txtDonnee(3).Text = GetInfo("CALCOLO", "DeltaX", Path_LAVORAZIONE_INI)
        OEMX.txtDonnee(4).Text = GetInfo("CALCOLO", "ConCorr", Path_LAVORAZIONE_INI)
        OEMX.txtDonnee(5).Text = GetInfo("CALCOLO", "SAP", Path_LAVORAZIONE_INI)
        OEMX.txtDonnee(6).Text = GetInfo("CALCOLO", "EAP", Path_LAVORAZIONE_INI)
        OEMX.txtDonnee(7).Text = GetInfo("CALCOLO", "NW", Path_LAVORAZIONE_INI)
        OEMX.txtDonnee(8).Text = GetInfo("CALCOLO", "DPG", Path_LAVORAZIONE_INI)
        If Len(OEMX.lblDonnee(9).Caption) <= 0 Then OEMX.lblDonnee(9).Caption = "efHa1"
        If Len(OEMX.lblDonnee(10).Caption) <= 0 Then OEMX.lblDonnee(10).Caption = "efHa2"
        OEMX.txtDonnee(9).Text = GetInfo("CALCOLO", "efHa1", Path_LAVORAZIONE_INI)
        OEMX.txtDonnee(10).Text = GetInfo("CALCOLO", "efHa2", Path_LAVORAZIONE_INI)
        OEMX.lblDonnee(4).Visible = False
        OEMX.txtDonnee(4).Visible = False
        OEMX.txtDonnee(1).SetFocus
        '
      'ELABORAZONI DATI INGRANAGGI INTERNI
      Case "OEM1_PROFILO2", "OEM1_PROFILOK2", "OEM1_LAVORO2", "OEM1_CORRELICA2", "OEM1_MOLA2", "OEM1_MOLAB2"
        OEMX.OEM1fraOpzioni.Visible = True
        OEMX.OEM1fraOpzioni.Left = OEMX.OEM1PicCALCOLO.Left + OEMX.OEM1PicCALCOLO.Width / 2 - OEMX.OEM1fraOpzioni.Width / 2
        OEMX.OEM1fraOpzioni.Top = OEMX.OEM1PicCALCOLO.Top + OEMX.OEM1PicCALCOLO.Height / 2 - OEMX.OEM1fraOpzioni.Height / 2
        OEMX.OEM1fraOpzioni.Caption = OEMX.OEM1Command2.Caption
        OEMX.OEM1fraOpzioni.BackColor = &HFFFFFF
        OEMX.chkDonnee.Visible = False
        For i = 1 To 8
          ret = LoadString(g_hLanguageLibHandle, 1120 + (i - 1), Atmp, 255)
          If ret > 0 Then stmp = Left$(Atmp, ret) Else stmp = ""
          OEMX.lblDonnee(i).Caption = stmp
          OEMX.lblDonnee(i).BackColor = &HFFFFFF
          OEMX.txtDonnee(i).BackColor = &HFFFFFF
          OEMX.lblDonnee(i).Visible = True
          OEMX.txtDonnee(i).Visible = True
        Next i
        OEMX.txtDonnee(1).Text = GetInfo("CALCOLO", "CorINT", Path_LAVORAZIONE_INI)
        OEMX.txtDonnee(2).Text = GetInfo("CALCOLO", "CorINCL", Path_LAVORAZIONE_INI)
        OEMX.txtDonnee(3).Text = GetInfo("CALCOLO", "DeltaX", Path_LAVORAZIONE_INI)
        OEMX.txtDonnee(4).Text = GetInfo("CALCOLO", "ConCorr", Path_LAVORAZIONE_INI)
        OEMX.txtDonnee(5).Text = GetInfo("CALCOLO", "SAP", Path_LAVORAZIONE_INI)
        OEMX.txtDonnee(6).Text = GetInfo("CALCOLO", "EAP", Path_LAVORAZIONE_INI)
        OEMX.txtDonnee(7).Text = GetInfo("CALCOLO", "NW", Path_LAVORAZIONE_INI)
        OEMX.txtDonnee(8).Text = GetInfo("CALCOLO", "DPG", Path_LAVORAZIONE_INI)
        OEMX.lblDonnee(7).Visible = False
        OEMX.txtDonnee(7).Visible = False
        OEMX.lblDonnee(9).Visible = False
        OEMX.txtDonnee(9).Visible = False
        OEMX.lblDonnee(10).Visible = False
        OEMX.txtDonnee(10).Visible = False
        OEMX.lblDonnee(4).Visible = False
        OEMX.txtDonnee(4).Visible = False
        OEMX.txtDonnee(1).SetFocus
        '
      Case "OEM1_MOLA40"
        If OEMX.txtHELP.Visible = True Then
          'VISUALIZZO I CONTROLLI DEL GRAFICO
          OEMX.OEM1chkINPUT(2).Visible = True
          OEMX.OEM1lblOX.Visible = True
          OEMX.OEM1lblOY.Visible = True
          OEMX.OEM1txtOX.Visible = True
          OEMX.OEM1txtOY.Visible = True
          OEMX.lblINP(0).Visible = True
          OEMX.txtINP(0).Visible = True
          OEMX.chkDonnee.Visible = False
          OEMX.txtHELP.Visible = False
          Call SOTTOPROGRAMMA_CAD_SCRITTURA
          Call SOTTOPROGRAMMA_CAD_VISUALIZZA(val(OEMX.txtINP(0).Text), val(OEMX.OEM1txtOX.Text), val(OEMX.OEM1txtOY.Text), "N")
        Else
          'NASCONDO I CONTROLLI DEL GRAFICO
          OEMX.OEM1chkINPUT(2).Visible = False
          OEMX.OEM1lblOX.Visible = False
          OEMX.OEM1lblOY.Visible = False
          OEMX.OEM1txtOX.Visible = False
          OEMX.OEM1txtOY.Visible = False
          OEMX.lblINP(0).Visible = False
          OEMX.txtINP(0).Visible = False
          OEMX.chkDonnee.Visible = False
          OEMX.txtHELP.Move OEMX.OEM1PicCALCOLO.Left, OEMX.OEM1PicCALCOLO.Top, OEMX.OEM1PicCALCOLO.Width, OEMX.OEM1PicCALCOLO.Height
          Call SOTTOPROGRAMMA_CAD_LETTURA
          Open g_chOemPATH & "\CAD.TXT" For Input As #1
            OEMX.txtHELP.Text = ""
            OEMX.txtHELP.Text = Input$(LOF(1), 1)
          Close #1
          OEMX.txtHELP.Visible = True
        End If
        '
      Case "OEM1_MOLA7"
        OEMX.OEM1fraOpzioni.Visible = True
        OEMX.OEM1fraOpzioni.Left = OEMX.OEM1PicCALCOLO.Left + OEMX.OEM1PicCALCOLO.Width / 2 - OEMX.OEM1fraOpzioni.Width / 2
        OEMX.OEM1fraOpzioni.Top = OEMX.OEM1PicCALCOLO.Top + OEMX.OEM1PicCALCOLO.Height / 2 - OEMX.OEM1fraOpzioni.Height / 2
        OEMX.OEM1fraOpzioni.Caption = OEMX.OEM1Command2.Caption
        OEMX.chkDonnee.Visible = False
        ret = LoadString(g_hLanguageLibHandle, 1122, Atmp, 255)
        If ret > 0 Then stmp = Left$(Atmp, ret) Else stmp = ""
        OEMX.lblDonnee(1).Caption = stmp
        '
        ret = LoadString(g_hLanguageLibHandle, 1120, Atmp, 255)
        If ret > 0 Then stmp = Left$(Atmp, ret) Else stmp = ""
        OEMX.lblDonnee(2).Caption = stmp
        '
        OEMX.lblDonnee(3).Caption = "Toll. +"
        OEMX.lblDonnee(4).Caption = "Toll. -"
        '
        ret = LoadString(g_hLanguageLibHandle, 1121, Atmp, 255)
        If ret > 0 Then stmp = Left$(Atmp, ret) Else stmp = ""
        OEMX.lblDonnee(5).Caption = stmp
        '
        ret = LoadString(g_hLanguageLibHandle, 1126, Atmp, 255)
        If ret > 0 Then stmp = Left$(Atmp, ret) Else stmp = ""
        OEMX.lblDonnee(6).Caption = stmp
        '
        ret = LoadString(g_hLanguageLibHandle, 1127, Atmp, 255)
        If ret > 0 Then stmp = Left$(Atmp, ret) Else stmp = ""
        OEMX.lblDonnee(7).Caption = stmp
        '
        OEMX.txtDonnee(1).Text = GetInfo("DIS0", "SpostaX", Path_LAVORAZIONE_INI)
        OEMX.txtDonnee(2).Text = GetInfo("DIS0", "SpostaY", Path_LAVORAZIONE_INI)
        OEMX.txtDonnee(3).Text = GetInfo("DIS0", "TolleranzaPositiva", Path_LAVORAZIONE_INI)
        OEMX.txtDonnee(4).Text = GetInfo("DIS0", "TolleranzaNegativa", Path_LAVORAZIONE_INI)
        OEMX.txtDonnee(5).Text = GetInfo("DIS0", "InclinazioneMola", Path_LAVORAZIONE_INI)
        OEMX.txtDonnee(6).Text = GetInfo("DIS0", "NW", Path_LAVORAZIONE_INI)
        OEMX.txtDonnee(7).Text = GetInfo("DIS0", "DPG", Path_LAVORAZIONE_INI)
        For i = 1 To 8
          OEMX.lblDonnee(i).BackColor = &HFFFFFF
          OEMX.txtDonnee(i).BackColor = &HFFFFFF
        Next i
        Select Case OEMX.OEM1ListaCombo1.ListIndex
          Case 3, 2
            For i = 3 To 4
              OEMX.lblDonnee(i).Visible = True
              OEMX.txtDonnee(i).Visible = True
            Next i
            For i = 6 To 8
              OEMX.lblDonnee(i).Visible = False
              OEMX.txtDonnee(i).Visible = False
            Next i
          Case 1
            For i = 3 To 4
              OEMX.lblDonnee(i).Visible = False
              OEMX.txtDonnee(i).Visible = False
            Next i
            For i = 6 To 7
              OEMX.lblDonnee(i).Visible = True
              OEMX.txtDonnee(i).Visible = True
            Next i
            For i = 8 To 8
              OEMX.lblDonnee(i).Visible = False
              OEMX.txtDonnee(i).Visible = False
            Next i
        End Select
        OEMX.lblDonnee(9).Visible = False
        OEMX.txtDonnee(9).Visible = False
        OEMX.lblDonnee(10).Visible = False
        OEMX.txtDonnee(10).Visible = False
        OEMX.txtDonnee(1).SetFocus
    End Select
    '
  End If
  '
End Sub

Private Sub OEM1Command2_GotFocus()
'
On Error Resume Next
  '
  OEMX.OEM1Label2.BackColor = QBColor(12)
  '
End Sub

Private Sub OEM1Command2_LostFocus()
'
On Error Resume Next
  '
  OEMX.OEM1Label2.BackColor = &H8000000F
  '
End Sub

Private Sub OEM1PicCalcolo_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
'
On Error Resume Next
  '
  If (Button = 1) Then
    '
    If (OEM1Line1.Visible = True) Then
      OEM1Line1.Visible = False
      OEM1Line2.Visible = False
      OEM1Line3.Visible = False
      '
    Else
      '
      OEM1Line1.Visible = True
      OEM1Line2.Visible = True
      OEM1Line3.Visible = True
      OEM1Line3.Y1 = 0
      OEM1Line3.Y2 = OEM1PicCALCOLO.ScaleHeight
      OEM1Line3.X1 = X
      OEM1Line3.X2 = X
      tempX = X
      tempY = Y
      '
    End If
    '
    'If Line2.Visible = True Then Line2.Visible = False Else Line2.Visible = True
  End If
  '
    If (Button = 2) And OEM1Line3.Visible Then
      OEM1Line3.Y1 = 0
      OEM1Line3.Y2 = OEM1PicCALCOLO.ScaleHeight
      OEM1Line3.X1 = X
      OEM1Line3.X2 = X
      tempX = X
      tempY = Y
    End If
  '
End Sub

Private Sub OEM1PicCalcolo_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
'
Dim tstr          As String
Dim picLARGHEZZA  As Single
Dim picSCALA      As Single
Dim picALTEZZA    As Single
Dim DeltaX        As Single
Dim DeltaY        As Single
'
On Error Resume Next
  '
  picLARGHEZZA = OEM1PicCALCOLO.ScaleWidth
  picALTEZZA = OEM1PicCALCOLO.ScaleHeight
  picSCALA = val(OEMX.txtINP(0).Text)
  If (picSCALA > 0) Then
    '
    tstr = "X= " & frmt((X - picLARGHEZZA / 2) / picSCALA, 4) & " Y= " & frmt(-(Y - picALTEZZA / 2) / picSCALA, 4)
    If OEM1Line1.Visible Then
      DeltaX = Abs((tempX - X) / picSCALA)
      DeltaY = Abs((tempY - Y) / picSCALA)
      tstr = tstr & " (dX: " & frmt(DeltaX, 4) & " dY: " & frmt(DeltaY, 4) & ")"
    End If
    '
    WRITE_DIALOG tstr
    '
    OEM1Line1.X1 = X
    OEM1Line1.X2 = X
    OEM1Line1.Y1 = 0
    OEM1Line1.Y2 = picALTEZZA
    '
    OEM1Line2.X1 = 0
    OEM1Line2.X2 = picLARGHEZZA
    OEM1Line2.Y1 = Y
    OEM1Line2.Y2 = Y
  End If
  '
End Sub

Private Sub OEM1txtOX_GotFocus()
'
On Error Resume Next
  '
  OEMX.OEM1lblOX.BackColor = QBColor(12)
  OEMX.OEM1lblOX.ForeColor = QBColor(15)
  Call SELEZIONA_TESTO
  '
End Sub

Private Sub OEM1txtOX_KeyUp(KeyCode As Integer, Shift As Integer)
'
Dim k       As Integer
Dim stmp    As String
Dim ret     As Long
Dim Valore  As Double
'
On Error Resume Next
  '
  If fraTOLLERANZA.Visible = True Then fraTOLLERANZA.Visible = False
  'GESTIONE DELLE CASELLE PER PROFILO RAB ESTERNI
  k = 0
  If ACTUAL_PARAMETER_GROUP = "OEM1_MOLA7" Then k = 1
  If (k > 0) Then
    stmp = GetInfo("DIS0", "OX", Path_LAVORAZIONE_INI)
    Valore = val(OEMX.OEM1txtOX.Text)
    If (val(stmp) <> Valore) Then
    ret = WritePrivateProfileString("DIS0", "OX", OEMX.OEM1txtOX.Text, Path_LAVORAZIONE_INI)
    Select Case OEMX.OEM1ListaCombo1.ListIndex
      Case 0, -1
        Call VISUALIZZA_MOLA_RAB(OEMX, val(OEMX.txtINP(0).Text), "N", OEMX.OEM1chkINPUT(0).Value, OEMX.OEM1chkINPUT(1).Value, val(OEMX.OEM1txtOX.Text), val(OEMX.OEM1txtOY.Text))
    End Select
    End If
  End If
  'GESTIONE DELLE CASELLE PER VITI ESTERNE
  k = 0
  If (ACTUAL_PARAMETER_GROUP = "OEM1_MOLA4") Or (ACTUAL_PARAMETER_GROUP = "OEM1_MOLA84") Then k = 1
  If k > 0 Then Call VITI_ESTERNE_VISUALIZZA_MOLA(OEMX.OEM1PicCALCOLO, val(OEMX.txtINP(0).Text), val(OEMX.OEM1txtOX.Text), val(OEMX.OEM1txtOY.Text), "N", val(OEMX.OEM1chkINPUT(2).Value))
  'GESTIONE DELLE CASELLE PER VITI MULTIFILETTO
  k = 0
  If ACTUAL_PARAMETER_GROUP = "OEM1_MOLA40" Then k = 1
  If k > 0 Then Call SOTTOPROGRAMMA_CAD_VISUALIZZA(val(OEMX.txtINP(0).Text), val(OEMX.OEM1txtOX.Text), val(OEMX.OEM1txtOY.Text), "N")
  'GESTIONE DELLE CASELLE PER CREATORI STANDARD
  k = 0
  If ACTUAL_PARAMETER_GROUP = "OEM1_PROFILO10" Then k = 1
  If k > 0 Then
    stmp = GetInfo("DIS0", "ZeroPezzoX", Path_LAVORAZIONE_INI)
    Valore = val(OEMX.OEM1txtOX.Text)
    If (val(stmp) <> Valore) Then
      ret = WritePrivateProfileString("DIS0", "ZeroPezzoX", frmt(Valore, 4), Path_LAVORAZIONE_INI)
      Call VISUALIZZAZIONE_PROFILO_CREATORE(val(OEMX.txtINP(0).Text), "N")
      Call SELEZIONA_TESTO
    End If
  End If
  'GESTIONE DELLE CASELLE PER CREATORI FORMA  'FlagFORMA 029
  k = 0
  If ACTUAL_PARAMETER_GROUP = "OEM1_PROFILO15" Then k = 1
  If k > 0 Then
    stmp = GetInfo("DIS0", "ZeroPezzoX", Path_LAVORAZIONE_INI)
    Valore = val(OEMX.OEM1txtOX.Text)
    If (val(stmp) <> Valore) Then
      ret = WritePrivateProfileString("DIS0", "ZeroPezzoX", frmt(Valore, 4), Path_LAVORAZIONE_INI)
      Call VISUALIZZAZIONE_PROFILO_CRFORMA(val(OEMX.txtINP(0).Text), "N")
      Call SELEZIONA_TESTO
    End If
  End If
  'GESTIONE DELLE CASELLE PER ROTORI
  k = 0
  If ACTUAL_PARAMETER_GROUP = "OEM1_MOLA71" Then k = 1
  If (k > 0) Then
    stmp = GetInfo("DIS0", "OX", Path_LAVORAZIONE_INI)
    Valore = val(OEMX.OEM1txtOX.Text)
    If (val(stmp) <> Valore) Then
    ret = WritePrivateProfileString("DIS0", "OX", OEMX.OEM1txtOX.Text, Path_LAVORAZIONE_INI)
    Call ROTORI1.CALCOLO_PROFILO_MOLA_ROTORI(2 * val(OEMX.txtINP(1).Text), 0)
    Call VISUALIZZA_MOLA_ROTORI(OEMX.OEM1PicCALCOLO, val(OEMX.txtINP(0).Text), OEMX.OEM1chkINPUT(0).Value, OEMX.OEM1chkINPUT(1).Value, val(OEMX.OEM1txtOX.Text), val(OEMX.OEM1txtOY.Text))
    End If
  End If
  '
End Sub

Private Sub OEM1txtOY_GotFocus()
'
On Error Resume Next
  '
  OEMX.OEM1lblOY.BackColor = QBColor(12)
  OEMX.OEM1lblOY.ForeColor = QBColor(15)
  Call SELEZIONA_TESTO
  '
End Sub

Private Sub OEM1txtOY_KeyUp(KeyCode As Integer, Shift As Integer)
'
Dim k       As Integer
Dim stmp    As String
Dim ret     As Long
Dim Valore  As Double
Dim MODALITA As String
'
On Error Resume Next
  '
  If fraTOLLERANZA.Visible = True Then fraTOLLERANZA.Visible = False
  'GESTIONE DELLE CASELLE PER PROFILO RAB ESTERNI
  k = 0
  If ACTUAL_PARAMETER_GROUP = "OEM1_MOLA7" Then k = 1
  If k > 0 Then
    stmp = GetInfo("DIS0", "OY", Path_LAVORAZIONE_INI)
    Valore = val(OEMX.OEM1txtOY.Text)
    If (val(stmp) <> Valore) Then
    ret = WritePrivateProfileString("DIS0", "OY", OEMX.OEM1txtOY.Text, Path_LAVORAZIONE_INI)
    Select Case OEMX.OEM1ListaCombo1.ListIndex
      Case 0, -1
        Call VISUALIZZA_MOLA_RAB(OEMX, val(OEMX.txtINP(0).Text), "N", OEMX.OEM1chkINPUT(0).Value, OEMX.OEM1chkINPUT(1).Value, val(OEMX.OEM1txtOX.Text), val(OEMX.OEM1txtOY.Text))
    End Select
    End If
  End If
  'GESTIONE DELLE CASELLE PER VITI ESTERNE
  k = 0
  If (ACTUAL_PARAMETER_GROUP = "OEM1_MOLA4") Then k = 1
  If k > 0 Then Call VITI_ESTERNE_VISUALIZZA_MOLA(OEMX.OEM1PicCALCOLO, val(OEMX.txtINP(0).Text), val(OEMX.OEM1txtOX.Text), val(OEMX.OEM1txtOY.Text), "N", val(OEMX.OEM1chkINPUT(2).Value))
  k = 0
  If (ACTUAL_PARAMETER_GROUP = "OEM1_MOLA84") Then k = 1
  If k > 0 Then Call VITI_ESTERNE_VISUALIZZA_MOLA(OEMX.OEM1PicCALCOLO, val(OEMX.txtINP(0).Text), val(OEMX.OEM1txtOX.Text), val("0"), "N", val(OEMX.OEM1chkINPUT(2).Value))
  k = 0
  If (ACTUAL_PARAMETER_GROUP = "OEM1_MOLAB84") And (KeyCode = 13) Then k = 1
  If k > 0 Then
     MODALITA = GetInfo("Configurazione", "MODALITA", PathFILEINI)
     MODALITA = UCase$(Trim$(MODALITA))
     Call VITI_ESTERNE_VISUALIZZA_MOLA(OEMX.OEM1PicCALCOLO, val(OEMX.txtINP(0).Text), val(OEMX.OEM1txtOX.Text), val("0"), "N", val(OEMX.OEM1chkINPUT(2).Value))
     If MODALITA = "SPF" Then
        Call VITI_ESTERNE_VISUALIZZA_MOLA_ADDIN(OEMX.OEM1PicCALCOLO, val(OEMX.txtINP(0).Text), val(OEMX.OEM1txtOX.Text), val(OEMX.OEM1txtOY.Text), "N", val(OEMX.OEM1chkINPUT(2).Value), "ADDIN", False, True)
     End If
  End If
  'GESTIONE DELLE CASELLE PER VITI MULTIFILETTO
  k = 0
  If ACTUAL_PARAMETER_GROUP = "OEM1_MOLA40" Then k = 1
  If k > 0 Then Call SOTTOPROGRAMMA_CAD_VISUALIZZA(val(OEMX.txtINP(0).Text), val(OEMX.OEM1txtOX.Text), val(OEMX.OEM1txtOY.Text), "N")
  'GESTIONE DELLE CASELLE PER CREATORI STANDARD
  k = 0
  If ACTUAL_PARAMETER_GROUP = "OEM1_PROFILO10" Then k = 1
  If k > 0 Then
    stmp = GetInfo("DIS0", "ZeroPezzoY", Path_LAVORAZIONE_INI)
    Valore = val(OEMX.OEM1txtOY.Text)
    If (val(stmp) <> Valore) Then
      ret = WritePrivateProfileString("DIS0", "ZeroPezzoY", frmt(Valore, 4), Path_LAVORAZIONE_INI)
      Call VISUALIZZAZIONE_PROFILO_CREATORE(val(OEMX.txtINP(0).Text), "N")
      Call SELEZIONA_TESTO
    End If
  End If
  'GESTIONE DELLE CASELLE PER CREATORI FORMA
  k = 0
  If ACTUAL_PARAMETER_GROUP = "OEM1_PROFILO15" Then k = 1
  If k > 0 Then
    stmp = GetInfo("DIS0", "ZeroPezzoY", Path_LAVORAZIONE_INI)
    Valore = val(OEMX.OEM1txtOY.Text)
    If (val(stmp) <> Valore) Then
      ret = WritePrivateProfileString("DIS0", "ZeroPezzoY", frmt(Valore, 4), Path_LAVORAZIONE_INI)
      Call VISUALIZZAZIONE_PROFILO_CRFORMA(val(OEMX.txtINP(0).Text), "N")
      Call SELEZIONA_TESTO
    End If
  End If
  'GESTIONE DELLE CASELLE PER ROTORI
  k = 0
  If ACTUAL_PARAMETER_GROUP = "OEM1_MOLA71" Then k = 1
  If k > 0 Then
    stmp = GetInfo("DIS0", "OY", Path_LAVORAZIONE_INI)
    Valore = val(OEMX.OEM1txtOY.Text)
    If (val(stmp) <> Valore) Then
    ret = WritePrivateProfileString("DIS0", "OY", OEMX.OEM1txtOY.Text, Path_LAVORAZIONE_INI)
    Call ROTORI1.CALCOLO_PROFILO_MOLA_ROTORI(2 * val(OEMX.txtINP(1).Text), 0)
    Call VISUALIZZA_MOLA_ROTORI(OEMX.OEM1PicCALCOLO, val(OEMX.txtINP(0).Text), OEMX.OEM1chkINPUT(0).Value, OEMX.OEM1chkINPUT(1).Value, val(OEMX.OEM1txtOX.Text), val(OEMX.OEM1txtOY.Text))
    End If
  End If
  '
End Sub

Private Sub PRG0SelezioneLavorazioni_Click()
'
On Error Resume Next

  WRITE_DIALOG OEMX.PRG0SelezioneLavorazioni.List(OEMX.PRG0SelezioneLavorazioni.ListIndex)
  
End Sub

Private Sub PRG0SelezioneLavorazioni_DblClick()
'
Dim DIMENSIONE       As Integer
Dim i                As Integer
Dim ret              As Integer
Dim stmp             As String
Dim stmp1            As String
Dim stmp2            As String
Dim stmp3            As String
Dim stmp4            As String
Dim sDDE             As String
Dim TIPO_LAVORAZIONE As String
'
On Error Resume Next
  '
  'COSTRUISCO L'INDICE DEL TIPO DI LAVORAZIONE SELEZIONATO
  i = INDICE_TIPO(OEMX.PRG0SelezioneLavorazioni.ListIndex + 1)
  If (i >= 1) Then
    OEMX.PRG0TipoLavorazione.Caption = OEMX.PRG0SelezioneLavorazioni.List(i - 1)
    'NUOVO TIPO DI LAVORAZIONE
    stmp = GetInfo("TIPO_LAVORAZIONE", "TIPO(" & Format$(i, "####0") & ")", PathFILEINI)
    stmp = UCase$(Trim$(stmp))
    TIPO_LAVORAZIONE = stmp
    'NUOVO FILE INI DI RIFERIMENTO
    stmp1 = GetInfo("TIPO_LAVORAZIONE", "FileINI(" & Format$(i, "####0") & ")", PathFILEINI)
    stmp1 = UCase$(Trim$(stmp1))
    'PERCORSO COMPLETO DEL FILE INI DI RIFERIMENTO LAVORAZIONE
    stmp2 = UCase$(Trim$(stmp1)) & ".INI"
    stmp2 = g_chOemPATH & "\" & stmp2
    Path_LAVORAZIONE_INI = stmp2
    'NUOVO NOME PEZZO
    stmp4 = GetInfo(stmp, "INDICE_LAVORAZIONE_SINGOLA", stmp2)
    If Len(stmp4) <= 0 Then stmp4 = "1"
    INDICE_LAVORAZIONE_SINGOLA = val(stmp4)
    stmp3 = LEGGI_NomePezzo(stmp)
    OEMX.PRG0txtNomePezzo.Text = " " & stmp3
    stmp4 = Format$(INDICE_LAVORAZIONE_SINGOLA, "##0")
    ret = WritePrivateProfileString("Configurazione", "INDICE_LAVORAZIONE_SINGOLA", stmp4, PathFILEINI)
    ret = WritePrivateProfileString(stmp, "INDICE_LAVORAZIONE_SINGOLA", stmp4, stmp2)
    OEMX.PRG0lblINDICE.Caption = LETTERA_LAVORAZIONE
    'SCRITTURA NEL FILE GAPP4.INI
    ret = WritePrivateProfileString("Configurazione", "TIPO_LAVORAZIONE", stmp, PathFILEINI)
    ret = WritePrivateProfileString("Configurazione", "FileINI", stmp1, PathFILEINI)
    ret = WritePrivateProfileString("Configurazione", "NomePezzo", stmp3, PathFILEINI)
    Call INIZIALIZZA_GLOBALI
    DIMENSIONE = val(GetInfo("Configurazione", "DIMENSIONE", Path_LAVORAZIONE_INI))
    If (DIMENSIONE <= 0) Then DIMENSIONE = 12
    Call IMPOSTA_FONT(Me, DIMENSIONE)
    'VISUALIZZAZIONE DEI PROGRAMMI ASSOCIATI AL TIPO DI LAVORAZIONE SELEZIONATO
    Call VISUALIZZA_PROGRAMMI_DISPONIBILI(stmp)
    Call VISUALIZZA_AZIONI_DISPONIBILI(TIPO_LAVORAZIONE)
    OEMX.PRG0SelezioneLavorazioni.Visible = False
    OEMX.PRG0TipoLavorazione.Visible = True
  Else
    WRITE_DIALOG "NO WORK MODE SELECTED"
  End If
  '
End Sub

Private Sub SuOEM1_OFFLINEChanged(ByVal TABELLA As String, ByVal VARIABILE As String, ByVal CARICA_CALCOLO As Boolean)
'
On Error Resume Next
  '
  If (CARICA_CALCOLO = True) Then
    Select Case TABELLA
      '
      Case "OEM1_PROFILO71": Call CARICA_DATI_CONTROLLO
      Case "OEM1_MACCHINA7": Call CARICA_DATI_CONTROLLO_RAB
      
      '
    End Select
  End If
  '
End Sub

Private Sub Timer1_Timer()
'
On Error Resume Next
  '
  Call ROTOLA("N")
  '
End Sub

Private Sub Timer2_Timer()
Dim ComandoOK As Boolean
Dim stmp      As String

On Error Resume Next

If Timer2.Tag = "W" Then
    'Esito del comando
     stmp = OPC_LEGGI_DATO("DB1300.DBX100.1")
     ComandoOK = val(stmp)
    'MsgBox ("Scrittura Eseguita")
    Call ChkCmdOnChip(Timer2.Tag, ComandoOK)
Else
  If Timer2.Tag = "R" Then
    'Esito del comando
     stmp = OPC_LEGGI_DATO("DB1300.DBX100.0")
     ComandoOK = val(stmp)
     'MsgBox ("Lettura Eseguita")
     Call ChkCmdOnChip(Timer2.Tag, ComandoOK)
  End If
End If

Timer2.Interval = 0
Timer2.Enabled = False
Timer2.Tag = ""

End Sub

Private Sub txtDonnee_GotFocus(Index As Integer)
'
On Error Resume Next
  '
  OldtxtDonnee = txtDonnee(Index).Text
  lblDonnee(Index).BackColor = QBColor(12)
  lblDonnee(Index).ForeColor = QBColor(15)
  Call SELEZIONA_TESTO
  '
End Sub

Private Sub txtDonnee_KeyUp(Index As Integer, KeyCode As Integer, Shift As Integer)
'
Dim ret  As Integer
Dim stmp As String
'
On Error Resume Next
  '
  If (KeyCode = 13) Then
    '
    stmp = Trim$(OEMX.txtDonnee(Index).Text)
    If IsNumeric(stmp) And stmp <> OldtxtDonnee Then
      'ELABORAZIONI DATI INGRANAGGI ESTERNI
      ret = 0
      If (ACTUAL_PARAMETER_GROUP = "OEM1_PROFILO91") Then ret = 1
      If (ACTUAL_PARAMETER_GROUP = "OEM1_PROFILOK91") Then ret = 1
      If (ACTUAL_PARAMETER_GROUP = "OEM1_LAVORO91") Then ret = 1
      If (ACTUAL_PARAMETER_GROUP = "OEM1_CORRELICA91") Then ret = 1
      If (ACTUAL_PARAMETER_GROUP = "OEM1_MOLA91") Then ret = 1
      If (ACTUAL_PARAMETER_GROUP = "OEM1_MOLAB91") Then ret = 1
      'SCANALATI EVOLVENTE
      If (ACTUAL_PARAMETER_GROUP = "OEM1_PROFILO13") Then ret = 1
      If (ACTUAL_PARAMETER_GROUP = "OEM1_PROFILOK13") Then ret = 1
      If (ACTUAL_PARAMETER_GROUP = "OEM1_LAVORO13") Then ret = 1
      If (ACTUAL_PARAMETER_GROUP = "OEM1_CORRELICA13") Then ret = 1
      If (ACTUAL_PARAMETER_GROUP = "OEM1_MOLA13") Then ret = 1
      If (ACTUAL_PARAMETER_GROUP = "OEM1_MOLAB13") Then ret = 1
      'INGRANAGGI ESTERNIO
      If (ACTUAL_PARAMETER_GROUP = "OEM1_PROFILO92") Then ret = 1
      If (ACTUAL_PARAMETER_GROUP = "OEM1_PROFILOK92") Then ret = 1
      If (ACTUAL_PARAMETER_GROUP = "OEM1_LAVORO92") Then ret = 1
      If (ACTUAL_PARAMETER_GROUP = "OEM1_CORRELICA92") Then ret = 1
      If (ACTUAL_PARAMETER_GROUP = "OEM1_MOLA92") Then ret = 1
      If (ACTUAL_PARAMETER_GROUP = "OEM1_MOLAB92") Then ret = 1
      If (ret = 1) Then
        Select Case Index
          Case 1: ret = WritePrivateProfileString("CALCOLO", "CorINT", stmp, Path_LAVORAZIONE_INI)
          Case 2: ret = WritePrivateProfileString("CALCOLO", "CorINCL", stmp, Path_LAVORAZIONE_INI)
          Case 3: ret = WritePrivateProfileString("CALCOLO", "DeltaX", stmp, Path_LAVORAZIONE_INI)
          Case 4: ret = WritePrivateProfileString("CALCOLO", "ConCorr", stmp, Path_LAVORAZIONE_INI)
          Case 5: ret = WritePrivateProfileString("CALCOLO", "SAP", stmp, Path_LAVORAZIONE_INI)
          Case 6: ret = WritePrivateProfileString("CALCOLO", "EAP", stmp, Path_LAVORAZIONE_INI)
          Case 7: ret = WritePrivateProfileString("CALCOLO", "NW", stmp, Path_LAVORAZIONE_INI)
          Case 8: ret = WritePrivateProfileString("CALCOLO", "DPG", stmp, Path_LAVORAZIONE_INI)
          Case 9: ret = WritePrivateProfileString("CALCOLO", "efha1", stmp, Path_LAVORAZIONE_INI)
          Case 10: ret = WritePrivateProfileString("CALCOLO", "efHa2", stmp, Path_LAVORAZIONE_INI)
        End Select
        OldtxtDonnee = stmp
        Call VERIFICA_PROFILO_INGRANAGGIO_ESTERNO_380(0)
      End If
      'ELABORAZONI DATI INGRANAGGI INTERNI
      ret = 0
      If (ACTUAL_PARAMETER_GROUP = "OEM1_PROFILO2") Then ret = 1
      If (ACTUAL_PARAMETER_GROUP = "OEM1_PROFILOK2") Then ret = 1
      If (ACTUAL_PARAMETER_GROUP = "OEM1_LAVORO2") Then ret = 1
      If (ACTUAL_PARAMETER_GROUP = "OEM1_CORRELICA2") Then ret = 1
      If (ACTUAL_PARAMETER_GROUP = "OEM1_MOLA2") Then ret = 1
      If (ACTUAL_PARAMETER_GROUP = "OEM1_MOLAB2") Then ret = 1
      If (ret = 1) Then
        Select Case Index
          Case 1: ret = WritePrivateProfileString("CALCOLO", "CorINT", stmp, Path_LAVORAZIONE_INI)
          Case 2: ret = WritePrivateProfileString("CALCOLO", "CorINCL", stmp, Path_LAVORAZIONE_INI)
          Case 3: ret = WritePrivateProfileString("CALCOLO", "DeltaX", stmp, Path_LAVORAZIONE_INI)
          Case 4: ret = WritePrivateProfileString("CALCOLO", "ConCorr", stmp, Path_LAVORAZIONE_INI)
          Case 5: ret = WritePrivateProfileString("CALCOLO", "SAP", stmp, Path_LAVORAZIONE_INI)
          Case 6: ret = WritePrivateProfileString("CALCOLO", "EAP", stmp, Path_LAVORAZIONE_INI)
          Case 8: ret = WritePrivateProfileString("CALCOLO", "DPG", stmp, Path_LAVORAZIONE_INI)
        End Select
        OldtxtDonnee = stmp
        Call CALCOLO_INGRANAGGIO_INTERNO(0)
      End If
      'ELABORAZIONI PROFILO RAB
      If ACTUAL_PARAMETER_GROUP = "OEM1_MOLA7" Then
        Select Case Index
          Case 1: ret = WritePrivateProfileString("DIS0", "SpostaX", stmp, Path_LAVORAZIONE_INI)
          Case 2: ret = WritePrivateProfileString("DIS0", "SpostaY", stmp, Path_LAVORAZIONE_INI)
          Case 3: ret = WritePrivateProfileString("DIS0", "TolleranzaPositiva", stmp, Path_LAVORAZIONE_INI)
          Case 4: ret = WritePrivateProfileString("DIS0", "TolleranzaNegativa", stmp, Path_LAVORAZIONE_INI)
          Case 5: ret = WritePrivateProfileString("DIS0", "InclinazioneMola", stmp, Path_LAVORAZIONE_INI)
          Case 6: ret = WritePrivateProfileString("DIS0", "NW", stmp, Path_LAVORAZIONE_INI)
          Case 7: ret = WritePrivateProfileString("DIS0", "DPG", stmp, Path_LAVORAZIONE_INI)
        End Select
        OldtxtDonnee = stmp
        Select Case OEM1ListaCombo1.ListIndex
            '
          Case 1
            stmp = GetInfo("DIS0", "RaggioMola", Path_LAVORAZIONE_INI)
            Call CALCOLO_MOLA_PROFILO_RAB_ESTERNI(val(stmp) * 2, 0)
            Call VERIFICA_PROFILO_RAB_INGRANAGGIO(val(stmp), "N")
            '
        End Select
      End If
      '
    End If
    '
  ElseIf (KeyCode = 38) And (Index >= 2) Then
    SendKeys "+{TAB}"
    '
  ElseIf (KeyCode = 40) And (Index <= 10) Then
    SendKeys "{TAB}"
    '
  ElseIf (KeyCode = 33) Then
    OEMX.txtDonnee(1).SetFocus
    '
  ElseIf (KeyCode = 34) Then
    If OEMX.txtDonnee(10).Visible = True Then
      OEMX.txtDonnee(10).SetFocus
    ElseIf OEMX.txtDonnee(9).Visible = True Then
      OEMX.txtDonnee(9).SetFocus
    ElseIf OEMX.txtDonnee(8).Visible = True Then
      OEMX.txtDonnee(8).SetFocus
    ElseIf OEMX.txtDonnee(7).Visible = True Then
      OEMX.txtDonnee(7).SetFocus
    End If
    '
  End If
  '
End Sub

Private Sub txtDonnee_LostFocus(Index As Integer)
'
On Error Resume Next
  '
  txtDonnee(Index).Text = OldtxtDonnee
  lblDonnee(Index).BackColor = vbWhite
  lblDonnee(Index).ForeColor = vbBlack
  '
End Sub

Private Sub txtINP_GotFocus(Index As Integer)
'
On Error Resume Next
  '
  OEMX.lblINP(Index).BackColor = QBColor(12)
  OEMX.lblINP(Index).ForeColor = QBColor(15)
  Call SELEZIONA_TESTO
  '
End Sub

Private Sub txtINP_KeyUp(Index As Integer, KeyCode As Integer, Shift As Integer)
'
Dim k     As Integer
Dim kk    As Integer
Dim i     As Integer
Dim ret   As Integer
Dim stmp  As String
Dim Scala As Single
Dim MODALITA As String
'
On Error Resume Next
  '
  If (KeyCode = 13) Then
    stmp = OEMX.txtINP(Index).Text
    Call CALCOLA_ESPRESSIONE_ESTESA(stmp)
    OEMX.txtINP(Index).Text = stmp
    If fraTOLLERANZA.Visible = True Then fraTOLLERANZA.Visible = False
    'GESTIONE DELLE CASELLE PROFILO ROTORE
    k = 0
    If ACTUAL_PARAMETER_GROUP = "OEM1_PROFILO71" Then k = 1
    If (k > 0) Then
      Select Case OEMX.OEM1ListaCombo1.ListIndex
        Case 0
          OEMX.OEM1hsROTORE.Visible = False
          OEMX.OEM1hsMOLA.Visible = False
          OEMX.OEM1hsMOLA1.Visible = False
          OEMX.OEM1hsPROFILO.Visible = False
          OEMX.Timer1.Enabled = True
        Case 1
          OEMX.Timer1.Enabled = False
          Call VISUALIZZA_SOVRAMETALLO_ROTORI("N")
      End Select
      OEMX.txtINP(Index).SetFocus
      Call SELEZIONA_TESTO
      Exit Sub
    End If
    'GESTIONE DELLE CASELLE PER ELICA
    k = 0
    If ACTUAL_PARAMETER_GROUP = "OEM1_CORRELICA85" Then k = 1
    If ACTUAL_PARAMETER_GROUP = "OEM1_CORRELICA86" Then k = 1
    If ACTUAL_PARAMETER_GROUP = "OEM1_CORRELICA81" Then k = 1
    'If ACTUAL_PARAMETER_GROUP = "OEM1_LAVORO91" Then k = 1
    If ACTUAL_PARAMETER_GROUP = "OEM1_CORRELICA91" Then k = 1
    If ACTUAL_PARAMETER_GROUP = "OEM1_CORRELICA92" Then k = 1
    If ACTUAL_PARAMETER_GROUP = "OEM1_LAVORO13" Then k = 1
    If ACTUAL_PARAMETER_GROUP = "OEM1_CORRELICA13" Then k = 1
    If ACTUAL_PARAMETER_GROUP = "OEM1_LAVORO2" Then k = 1
    If ACTUAL_PARAMETER_GROUP = "OEM1_CORRELICA2" Then k = 1
    If ACTUAL_PARAMETER_GROUP = "OEM1_CORRELICA4" Then k = 1
    If ACTUAL_PARAMETER_GROUP = "OEM1_CORRELICA84" Then k = 1
    If ACTUAL_PARAMETER_GROUP = "OEM1_LAVORO3" Then k = 1
    If ACTUAL_PARAMETER_GROUP = "OEM1_LAVORO4" Then k = 1
    If ACTUAL_PARAMETER_GROUP = "OEM1_LAVORO6" Then k = 1
    If (k > 0) Then
      Select Case Index
        Case 0: stmp = GetInfo("ELICA", "ScalaX", Path_LAVORAZIONE_INI)
          If (val(stmp) <> val(OEMX.txtINP(0).Text)) Then
            ret = WritePrivateProfileString("ELICA", "ScalaX", OEMX.txtINP(0).Text, Path_LAVORAZIONE_INI)
          End If
        Case 1: stmp = GetInfo("ELICA", "ScalaY", Path_LAVORAZIONE_INI)
          If (val(stmp) <> val(OEMX.txtINP(1).Text)) Then
            ret = WritePrivateProfileString("ELICA", "ScalaY", OEMX.txtINP(1).Text, Path_LAVORAZIONE_INI)
          End If
      End Select
      Call VISUALIZZA_ELICA(OEMX, "N")
      OEMX.txtINP(Index).SetFocus
      Call SELEZIONA_TESTO
      Exit Sub
    End If
    'GESTIONE DELLE CASELLE PER ELICA MULTIFILETTO
    k = 0
    If ACTUAL_PARAMETER_GROUP = "OEM1_LAVORO40" Then k = 1
    If (k > 0) Then
      Select Case Index
        Case 0
          stmp = GetInfo("ELICA", "ScalaX", Path_LAVORAZIONE_INI)
          If (val(stmp) <> val(OEMX.txtINP(0).Text)) Then
            ret = WritePrivateProfileString("ELICA", "ScalaX", OEMX.txtINP(0).Text, Path_LAVORAZIONE_INI)
          End If
        Case 1
          stmp = GetInfo("ELICA", "ScalaY", Path_LAVORAZIONE_INI)
          If (val(stmp) <> val(OEMX.txtINP(1).Text)) Then
            ret = WritePrivateProfileString("ELICA", "ScalaY", OEMX.txtINP(1).Text, Path_LAVORAZIONE_INI)
          End If
      End Select
      Call VISUALIZZA_ELICA_MULTIFILETTO(OEMX, "N")
      OEMX.txtINP(Index).SetFocus
      Call SELEZIONA_TESTO
      Exit Sub
    End If
    'GESTIONE DELLE CASELLE PER SCANALATO ESTERNO
    k = 0
    If ACTUAL_PARAMETER_GROUP = "OEM1_PROFILO33" Then k = 1
    If (k > 0) Then
      kk = OEMX.OEM1ListaCombo1.ListIndex
      If (kk < 0) Then kk = 0
      Select Case kk
        Case 0  'MOLA
          Select Case Index
            Case 0:  stmp = GetInfo("CALCOLO", "ScalaMOLA", Path_LAVORAZIONE_INI)
              If (val(stmp) <> val(OEMX.txtINP(0).Text)) Then
                ret = WritePrivateProfileString("CALCOLO", "ScalaMOLA", OEMX.txtINP(0).Text, Path_LAVORAZIONE_INI)
              End If
            Case 1
          End Select
          Call CALCOLO_SCANALATO(0)
      End Select
      Call SELEZIONA_TESTO
      Exit Sub
    End If
    'ELABORAZIONI DATI MOLAVITE
    k = 0
    If (ACTUAL_PARAMETER_GROUP = "OEM1_PROFILO85") Or (ACTUAL_PARAMETER_GROUP = "OEM1_PROFILO86") Then k = 1
    If (ACTUAL_PARAMETER_GROUP = "OEM1_MOLA85") Or (ACTUAL_PARAMETER_GROUP = "OEM1_MOLA86") Then k = 1
    If (ACTUAL_PARAMETER_GROUP = "OEM1_RULLO85") Or (ACTUAL_PARAMETER_GROUP = "OEM1_RULLO86") Then k = 1
    If (k > 0) Then
      kk = OEMX.OEM1ListaCombo1.ListIndex
      If (kk < 0) Then kk = 0
      Select Case kk
        Case 3  'Grafico evolvente
          Select Case Index
            Case 1
              stmp = GetInfo("CALCOLO", "fHaToll", Path_LAVORAZIONE_INI)
              If (val(stmp) <> val(OEMX.txtINP(1).Text)) Then
                ret = WritePrivateProfileString("CALCOLO", "fHaToll", OEMX.txtINP(1).Text, Path_LAVORAZIONE_INI)
              End If
            Case 4
              stmp = GetInfo("CALCOLO", "ScalafHA", Path_LAVORAZIONE_INI)
              If (val(stmp) <> val(OEMX.txtINP(4).Text)) Then
                ret = WritePrivateProfileString("CALCOLO", "ScalafHA", OEMX.txtINP(4).Text, Path_LAVORAZIONE_INI)
              End If
          End Select
          Call CALCOLO_INGRANAGGIO_ESTERNO_380_MV(0)
      End Select
      Call SELEZIONA_TESTO
      Exit Sub
    End If
    'ELABORAZONI DATI INGRANAGGI ESTERNI
    k = 0
    If (ACTUAL_PARAMETER_GROUP = "OEM1_PROFILO91") Then k = 1
    If (ACTUAL_PARAMETER_GROUP = "OEM1_PROFILOK91") Then k = 1
    If (ACTUAL_PARAMETER_GROUP = "OEM1_LAVORO91") Then k = 1
    If (ACTUAL_PARAMETER_GROUP = "OEM1_CORRELICA91") Then k = 1
    If (ACTUAL_PARAMETER_GROUP = "OEM1_MOLA91") Then k = 1
    If (ACTUAL_PARAMETER_GROUP = "OEM1_MOLAB91") Then k = 1
    'SCANALATI EVOLVENTE
    If (ACTUAL_PARAMETER_GROUP = "OEM1_PROFILO13") Then k = 1
    If (ACTUAL_PARAMETER_GROUP = "OEM1_PROFILOK13") Then k = 1
    If (ACTUAL_PARAMETER_GROUP = "OEM1_LAVORO13") Then k = 1
    If (ACTUAL_PARAMETER_GROUP = "OEM1_CORRELICA13") Then k = 1
    If (ACTUAL_PARAMETER_GROUP = "OEM1_MOLA13") Then k = 1
    If (ACTUAL_PARAMETER_GROUP = "OEM1_MOLAB13") Then k = 1
    'INGRANAGGI ESTERNIO
    If (ACTUAL_PARAMETER_GROUP = "OEM1_PROFILO92") Then k = 1
    If (ACTUAL_PARAMETER_GROUP = "OEM1_PROFILOK92") Then k = 1
    If (ACTUAL_PARAMETER_GROUP = "OEM1_LAVORO92") Then k = 1
    If (ACTUAL_PARAMETER_GROUP = "OEM1_CORRELICA92") Then k = 1
    If (ACTUAL_PARAMETER_GROUP = "OEM1_MOLA92") Then k = 1
    If (ACTUAL_PARAMETER_GROUP = "OEM1_MOLAB92") Then k = 1
    If (k > 0) Then
      kk = OEMX.OEM1ListaCombo1.ListIndex
      If (kk < 0) Then kk = 0
      Select Case kk
        Case 0  'MOLA
          Select Case Index
            Case 0
              stmp = GetInfo("CALCOLO", "ScalaMOLA", Path_LAVORAZIONE_INI)
              If val(stmp) <> val(OEMX.txtINP(0).Text) Then
                ret = WritePrivateProfileString("CALCOLO", "ScalaMOLA", OEMX.txtINP(0).Text, Path_LAVORAZIONE_INI)
              End If
            Case 1
          End Select
          Call CALCOLO_INGRANAGGIO_ESTERNO_380(0)
        Case 1  'INGRANAGGIO
          Select Case Index
            Case 0
              stmp = GetInfo("CALCOLO", "ScalaINGRANAGGIO", Path_LAVORAZIONE_INI)
              If val(stmp) <> val(OEMX.txtINP(0).Text) Then
                ret = WritePrivateProfileString("CALCOLO", "ScalaINGRANAGGIO", OEMX.txtINP(0).Text, Path_LAVORAZIONE_INI)
              End If
            Case 1
          End Select
          Call CALCOLO_INGRANAGGIO_ESTERNO_380(0)
        Case 2  'LINEA DI CONTATTO
          Select Case Index
            Case 0
              stmp = GetInfo("CALCOLO", "ScalaCONTATTOx", Path_LAVORAZIONE_INI)
              If val(stmp) <> val(OEMX.txtINP(0).Text) Then
                ret = WritePrivateProfileString("CALCOLO", "ScalaCONTATTOx", OEMX.txtINP(0).Text, Path_LAVORAZIONE_INI)
              End If
            Case 1
              stmp = GetInfo("CALCOLO", "ScalaCONTATTOy", Path_LAVORAZIONE_INI)
              If val(stmp) <> val(OEMX.txtINP(1).Text) Then
                ret = WritePrivateProfileString("CALCOLO", "ScalaCONTATTOy", OEMX.txtINP(1).Text, Path_LAVORAZIONE_INI)
              End If
          End Select
          Call CALCOLO_INGRANAGGIO_ESTERNO_380(0)
        Case 3  'INFORMAZIONI1
          Select Case Index
            Case 0
              stmp = GetInfo("CALCOLO", "DiametroEsternoMola", Path_LAVORAZIONE_INI)
              If val(stmp) <> val(OEMX.txtINP(0).Text) Then
                ret = WritePrivateProfileString("CALCOLO", "DiametroEsternoMola", OEMX.txtINP(0).Text, Path_LAVORAZIONE_INI)
              End If
            Case 1
              stmp = GetInfo("CALCOLO", "InclinazioneMola", Path_LAVORAZIONE_INI)
              If val(stmp) <> val(OEMX.txtINP(1).Text) Then
                ret = WritePrivateProfileString("CALCOLO", "InclinazioneMola", OEMX.txtINP(1).Text, Path_LAVORAZIONE_INI)
              End If
          End Select
          Call CALCOLO_INGRANAGGIO_ESTERNO_380(0)
        Case 5 'VERIFICA PROFILO
          Select Case Index
            Case 0
              stmp = GetInfo("CALCOLO", "ScalaCORREZIONIx", Path_LAVORAZIONE_INI)
              If val(stmp) <> val(OEMX.txtINP(0).Text) Then
                ret = WritePrivateProfileString("CALCOLO", "ScalaCORREZIONIx", OEMX.txtINP(0).Text, Path_LAVORAZIONE_INI)
              End If
            Case 1
              stmp = GetInfo("CALCOLO", "ScalaCORREZIONIy", Path_LAVORAZIONE_INI)
              If val(stmp) <> val(OEMX.txtINP(1).Text) Then
                ret = WritePrivateProfileString("CALCOLO", "ScalaCORREZIONIy", OEMX.txtINP(1).Text, Path_LAVORAZIONE_INI)
              End If
            Case 3
              stmp = GetInfo("CALCOLO", "RISOLUZIONE", Path_LAVORAZIONE_INI)
              If val(stmp) <> val(OEMX.txtINP(3).Text) Then
                ret = WritePrivateProfileString("CALCOLO", "RISOLUZIONE", OEMX.txtINP(3).Text, Path_LAVORAZIONE_INI)
              End If
            Case 4
              stmp = GetInfo("CALCOLO", "CorrSPESSORE", Path_LAVORAZIONE_INI)
              If val(stmp) <> val(OEMX.txtINP(4).Text) Then
                ret = WritePrivateProfileString("CALCOLO", "CorrSPESSORE", OEMX.txtINP(4).Text, Path_LAVORAZIONE_INI)
                Call CALCOLO_INGRANAGGIO_ESTERNO_380(0)
              End If
          End Select
          Call VERIFICA_PROFILO_INGRANAGGIO_ESTERNO_380(0)
        Case 6 'CALCOLO DIAMETRO SAP
          stmp = GetInfo("CALCOLO", "txtINP(" & Format$(Index, "0") & ")", Path_LAVORAZIONE_INI)
          If val(stmp) <> val(OEMX.txtINP(Index).Text) Then
            ret = WritePrivateProfileString("CALCOLO", "txtINP(" & Format$(Index, "0") & ")", OEMX.txtINP(Index).Text, Path_LAVORAZIONE_INI)
            Call CALCOLO_INGRANAGGIO_ESTERNO_SAP_380(0)
          End If
      End Select
      Call SELEZIONA_TESTO
      Exit Sub
    End If
    'ELABORAZONI DATI INGRANAGGI INTERNI
    k = 0
    If (ACTUAL_PARAMETER_GROUP = "OEM1_PROFILO2") Then k = 1
    If (ACTUAL_PARAMETER_GROUP = "OEM1_PROFILOK2") Then k = 1
    If (ACTUAL_PARAMETER_GROUP = "OEM1_LAVORO2") Then k = 1
    If (ACTUAL_PARAMETER_GROUP = "OEM1_CORRELICA2") Then k = 1
    If (ACTUAL_PARAMETER_GROUP = "OEM1_MOLA2") Then k = 1
    If (ACTUAL_PARAMETER_GROUP = "OEM1_MOLAB2") Then k = 1
    If (k > 0) Then
      kk = OEMX.OEM1ListaCombo1.ListIndex
      If (kk < 0) Then kk = 0
      Select Case kk
        Case 0  'MOLA
          Select Case Index
            Case 0
              stmp = GetInfo("CALCOLO", "ScalaMOLA", Path_LAVORAZIONE_INI)
              If val(stmp) <> val(OEMX.txtINP(0).Text) Then
                ret = WritePrivateProfileString("CALCOLO", "ScalaMOLA", OEMX.txtINP(0).Text, Path_LAVORAZIONE_INI)
              End If
            Case 1
          End Select
        Case 1  'INGRANAGGIO
          Select Case Index
            Case 0
              stmp = GetInfo("CALCOLO", "ScalaINGRANAGGIO", Path_LAVORAZIONE_INI)
              If val(stmp) <> val(OEMX.txtINP(0).Text) Then
                ret = WritePrivateProfileString("CALCOLO", "ScalaINGRANAGGIO", OEMX.txtINP(0).Text, Path_LAVORAZIONE_INI)
              End If
            Case 1
          End Select
        Case 2  'LINEA DI CONTATTO
          Select Case Index
            Case 0
              stmp = GetInfo("CALCOLO", "ScalaCONTATTOx", Path_LAVORAZIONE_INI)
              If val(stmp) <> val(OEMX.txtINP(0).Text) Then
                ret = WritePrivateProfileString("CALCOLO", "ScalaCONTATTOx", OEMX.txtINP(0).Text, Path_LAVORAZIONE_INI)
              End If
            Case 1
              stmp = GetInfo("CALCOLO", "ScalaCONTATTOy", Path_LAVORAZIONE_INI)
              If val(stmp) <> val(OEMX.txtINP(1).Text) Then
                ret = WritePrivateProfileString("CALCOLO", "ScalaCONTATTOy", OEMX.txtINP(1).Text, Path_LAVORAZIONE_INI)
              End If
          End Select
        Case 3 'CORREZIONI
          Select Case Index
            Case 0
              stmp = GetInfo("CALCOLO", "ScalaCORREZIONIx", Path_LAVORAZIONE_INI)
              If val(stmp) <> val(OEMX.txtINP(0).Text) Then
                ret = WritePrivateProfileString("CALCOLO", "ScalaCORREZIONIx", OEMX.txtINP(0).Text, Path_LAVORAZIONE_INI)
              End If
            Case 1
              stmp = GetInfo("CALCOLO", "ScalaCORREZIONIy", Path_LAVORAZIONE_INI)
              If val(stmp) <> val(OEMX.txtINP(1).Text) Then
                ret = WritePrivateProfileString("CALCOLO", "ScalaCORREZIONIy", OEMX.txtINP(1).Text, Path_LAVORAZIONE_INI)
              End If
          End Select
        Case 4  'INFORMAZIONI1
          Select Case Index
            Case 0
              stmp = GetInfo("CALCOLO", "DiametroEsternoMolaINT", Path_LAVORAZIONE_INI)
              If val(stmp) <> val(OEMX.txtINP(0).Text) Then
                ret = WritePrivateProfileString("CALCOLO", "DiametroEsternoMolaINT", OEMX.txtINP(0).Text, Path_LAVORAZIONE_INI)
              End If
            Case 1
              stmp = GetInfo("CALCOLO", "InclinazioneMolaINT", Path_LAVORAZIONE_INI)
              If val(stmp) <> val(OEMX.txtINP(1).Text) Then
                ret = WritePrivateProfileString("CALCOLO", "InclinazioneMolaINT", OEMX.txtINP(1).Text, Path_LAVORAZIONE_INI)
              End If
          End Select
        Case 6 'VERIFICA PROFILO
          Select Case Index
            Case 0
              stmp = GetInfo("CALCOLO", "ScalaCORREZIONIx", Path_LAVORAZIONE_INI)
              If val(stmp) <> val(OEMX.txtINP(0).Text) Then
                ret = WritePrivateProfileString("CALCOLO", "ScalaCORREZIONIx", OEMX.txtINP(0).Text, Path_LAVORAZIONE_INI)
              End If
            Case 1
              stmp = GetInfo("CALCOLO", "ScalaCORREZIONIy", Path_LAVORAZIONE_INI)
              If val(stmp) <> val(OEMX.txtINP(1).Text) Then
                ret = WritePrivateProfileString("CALCOLO", "ScalaCORREZIONIy", OEMX.txtINP(1).Text, Path_LAVORAZIONE_INI)
              End If
            Case 3
              stmp = GetInfo("CALCOLO", "RISOLUZIONE", Path_LAVORAZIONE_INI)
              If val(stmp) <> val(OEMX.txtINP(3).Text) Then
                ret = WritePrivateProfileString("CALCOLO", "RISOLUZIONE", OEMX.txtINP(3).Text, Path_LAVORAZIONE_INI)
              End If
            Case 4
              stmp = GetInfo("CALCOLO", "CorrSPESSORE", Path_LAVORAZIONE_INI)
              If val(stmp) <> val(OEMX.txtINP(4).Text) Then
                ret = WritePrivateProfileString("CALCOLO", "CorrSPESSORE", OEMX.txtINP(4).Text, Path_LAVORAZIONE_INI)
              End If
          End Select
      End Select
      Call CALCOLO_INGRANAGGIO_INTERNO(0)
      Call SELEZIONA_TESTO
      Exit Sub
    End If
    'GESTIONE DELLE CASELLE PER VITI ESTERNE
    k = 0
    If (ACTUAL_PARAMETER_GROUP = "OEM1_MOLA4") Or (ACTUAL_PARAMETER_GROUP = "OEM1_MOLA84") Then k = 1
    If (k > 0) Then
      stmp = GetInfo("DIS0", "ScalaMOLA", Path_LAVORAZIONE_INI)
      If stmp <> OEMX.txtINP(0).Text Then
        stmp = Trim$(OEMX.txtINP(0).Text)
        If val(stmp) <= 0 Then stmp = "1": OEMX.txtINP(0).Text = "1"
        ret = WritePrivateProfileString("DIS0", "ScalaMOLA", stmp, Path_LAVORAZIONE_INI)
        Call VITI_ESTERNE_VISUALIZZA_MOLA(OEMX.OEM1PicCALCOLO, val(OEMX.txtINP(0).Text), val(OEMX.OEM1txtOX.Text), val(OEMX.OEM1txtOY.Text), "N", val(OEMX.OEM1chkINPUT(2).Value))
        Call SELEZIONA_TESTO
      End If
    End If
    
    k = 0
    If (ACTUAL_PARAMETER_GROUP = "OEM1_MOLAB84") Then k = 1
    If (k > 0) Then
      MODALITA = GetInfo("Configurazione", "MODALITA", PathFILEINI)
      MODALITA = UCase$(Trim$(MODALITA))
      stmp = GetInfo("DIS0", "ScalaMOLA", Path_LAVORAZIONE_INI)
      If stmp <> OEMX.txtINP(0).Text Then
        stmp = Trim$(OEMX.txtINP(0).Text)
        If val(stmp) <= 0 Then stmp = "1": OEMX.txtINP(0).Text = "1"
        ret = WritePrivateProfileString("DIS0", "ScalaMOLA", stmp, Path_LAVORAZIONE_INI)
        Call VITI_ESTERNE_VISUALIZZA_MOLA(OEMX.OEM1PicCALCOLO, val(OEMX.txtINP(0).Text), val(OEMX.OEM1txtOX.Text), val("0"), "N", val(OEMX.OEM1chkINPUT(2).Value))
        If MODALITA = "SPF" Then
           Call VITI_ESTERNE_VISUALIZZA_MOLA_ADDIN(OEMX.OEM1PicCALCOLO, val(OEMX.txtINP(0).Text), val(OEMX.OEM1txtOX.Text), val(OEMX.OEM1txtOY.Text), "N", val(OEMX.OEM1chkINPUT(2).Value), "ADDIN", False, True)
        End If
        Call SELEZIONA_TESTO
      End If
    End If
    
    k = 0
    If (ACTUAL_PARAMETER_GROUP = "OEM1_PROFILO4") Or (ACTUAL_PARAMETER_GROUP = "OEM1_LAVORO84") Then k = 1
    If (k > 0) Then
      stmp = GetInfo("DIS0", "ScalaMOLA", Path_LAVORAZIONE_INI)
      If stmp <> OEMX.txtINP(0).Text Then
        stmp = Trim$(OEMX.txtINP(0).Text)
        If val(stmp) <= 0 Then stmp = "1": OEMX.txtINP(0).Text = "1"
        ret = WritePrivateProfileString("DIS0", "ScalaMOLA", stmp, Path_LAVORAZIONE_INI)
        Call VITI_ESTERNE_VISUALIZZA_PROFILO(OEMX.OEM1PicCALCOLO, "N")
        Call SELEZIONA_TESTO
      End If
    End If
    'GESTIONE DELLE CASELLE PER VITI MULTIFILETTO
    k = 0
    If ACTUAL_PARAMETER_GROUP = "OEM1_MOLA40" Then k = 1
    If (k > 0) Then
      stmp = GetInfo("DIS0", "ScalaMOLA", Path_LAVORAZIONE_INI)
      If stmp <> OEMX.txtINP(0).Text Then
        stmp = Trim$(OEMX.txtINP(0).Text)
        If val(stmp) <= 0 Then stmp = "1": OEMX.txtINP(0).Text = "1"
        ret = WritePrivateProfileString("DIS0", "ScalaMOLA", stmp, Path_LAVORAZIONE_INI)
        Call SOTTOPROGRAMMA_CAD_VISUALIZZA(val(OEMX.txtINP(0).Text), val(OEMX.OEM1txtOX.Text), val(OEMX.OEM1txtOY.Text), "N")
        Call SELEZIONA_TESTO
      End If
      Exit Sub
    End If
    'GESTIONE DELLE CASELLE PER CREATORI STANDARD
    k = 0
    If ACTUAL_PARAMETER_GROUP = "OEM1_MOLA10" Then k = 1
    If (k > 0) Then
      stmp = GetInfo("DIS0", "Scala", Path_LAVORAZIONE_INI)
      If stmp <> OEMX.txtINP(0).Text Then
        stmp = Trim$(OEMX.txtINP(0).Text)
        If val(stmp) <= 0 Then stmp = "1": OEMX.txtINP(0).Text = "1"
        ret = WritePrivateProfileString("DIS0", "Scala", stmp, Path_LAVORAZIONE_INI)
        Scala = val(stmp)
        Call VISUALIZZAZIONE_PROFILO_MOLA_CREATORI(Scala, "N")
        Call SELEZIONA_TESTO
      End If
      Exit Sub
    End If
    'GESTIONE DELLE CASELLE PER CREATORI FORMA
    k = 0
    If ACTUAL_PARAMETER_GROUP = "OEM1_MOLA15" Then k = 1
    If (k > 0) Then
      stmp = GetInfo("DIS0", "Scala", Path_LAVORAZIONE_INI)
      If stmp <> OEMX.txtINP(0).Text Then
        stmp = Trim$(OEMX.txtINP(0).Text)
        If val(stmp) <= 0 Then stmp = "1": OEMX.txtINP(0).Text = "1"
        ret = WritePrivateProfileString("DIS0", "Scala", stmp, Path_LAVORAZIONE_INI)
        Scala = val(stmp)
        Call VISUALIZZAZIONE_PROFILO_MOLA_CRFORMA(Scala, "N")
        Call SELEZIONA_TESTO
      End If
      Exit Sub
    End If
    'GESTIONE DELLE CASELLE PER PROFILO PER PUNTI RAB
    k = 0
    If ACTUAL_PARAMETER_GROUP = "OEM1_MOLA7" Then k = 1
    If (k > 0) Then
      Select Case OEMX.OEM1ListaCombo1.ListIndex
        Case 0, -1
          Select Case Index
            Case 0
              stmp = GetInfo("DIS0", "ScalaMOLA", Path_LAVORAZIONE_INI)
              If (val(stmp) <> val(OEMX.txtINP(0).Text)) Then
                ret = WritePrivateProfileString("DIS0", "ScalaMOLA", OEMX.txtINP(0).Text, Path_LAVORAZIONE_INI)
                Call CALCOLO_MOLA_PROFILO_RAB_ESTERNI(2 * val(OEMX.txtINP(1).Text), 0)
                Call VISUALIZZA_MOLA_RAB(OEMX, val(OEMX.txtINP(0).Text), "N", OEMX.OEM1chkINPUT(0).Value, OEMX.OEM1chkINPUT(1).Value, val(OEMX.OEM1txtOX.Text), val(OEMX.OEM1txtOY.Text))
              End If
            Case 1
              stmp = GetInfo("DIS0", "RaggioMola", Path_LAVORAZIONE_INI)
              If (val(stmp) <> val(OEMX.txtINP(1).Text)) Then
                ret = WritePrivateProfileString("DIS0", "RaggioMola", OEMX.txtINP(1).Text, Path_LAVORAZIONE_INI)
                Call CALCOLO_MOLA_PROFILO_RAB_ESTERNI(2 * val(OEMX.txtINP(1).Text), 0)
                Call VISUALIZZA_MOLA_RAB(OEMX, val(OEMX.txtINP(0).Text), "N", OEMX.OEM1chkINPUT(0).Value, OEMX.OEM1chkINPUT(1).Value, val(OEMX.OEM1txtOX.Text), val(OEMX.OEM1txtOY.Text))
              End If
          End Select
        Case 1
          Select Case Index
            Case 0
              stmp = GetInfo("DIS0", "ScalaCORREZIONIX", Path_LAVORAZIONE_INI)
              If (val(stmp) <> val(OEMX.txtINP(0).Text)) Then
                ret = WritePrivateProfileString("DIS0", "ScalaCORREZIONIX", OEMX.txtINP(0).Text, Path_LAVORAZIONE_INI)
                stmp = GetInfo("DIS0", "RaggioMola", Path_LAVORAZIONE_INI)
                Call VERIFICA_PROFILO_RAB_INGRANAGGIO(val(stmp), "N")
              End If
            Case 1
              stmp = GetInfo("DIS0", "ScalaCORREZIONIY", Path_LAVORAZIONE_INI)
              If (val(stmp) <> val(OEMX.txtINP(1).Text)) Then
                ret = WritePrivateProfileString("DIS0", "ScalaCORREZIONIY", OEMX.txtINP(1).Text, Path_LAVORAZIONE_INI)
                stmp = GetInfo("DIS0", "RaggioMola", Path_LAVORAZIONE_INI)
                Call VERIFICA_PROFILO_RAB_INGRANAGGIO(val(stmp), "N")
              End If
            Case 3
              stmp = GetInfo("DIS0", "RISOLUZIONE", Path_LAVORAZIONE_INI)
              If (val(stmp) <> val(OEMX.txtINP(3).Text)) Then
                ret = WritePrivateProfileString("DIS0", "RISOLUZIONE", OEMX.txtINP(3).Text, Path_LAVORAZIONE_INI)
                stmp = GetInfo("DIS0", "RaggioMola", Path_LAVORAZIONE_INI)
                Call VERIFICA_PROFILO_RAB_INGRANAGGIO(val(stmp), "N")
              End If
          End Select
      End Select
      Exit Sub
    End If
    'GESTIONE DELLE CASELLE CORREZIONI PROFILO
    k = 0
    If ACTUAL_PARAMETER_GROUP = "OEM1_PROFILO7" Then k = 1
    If (k > 0) Then
      Select Case Index
        Case 0
          stmp = GetInfo("DIS0", "ScalaCORREZIONIX", Path_LAVORAZIONE_INI)
          If val(stmp) <> val(OEMX.txtINP(0).Text) Then
            ret = WritePrivateProfileString("DIS0", "ScalaCORREZIONIX", OEMX.txtINP(0).Text, Path_LAVORAZIONE_INI)
          End If
        Case 1
          stmp = GetInfo("DIS0", "ScalaCORREZIONIY", Path_LAVORAZIONE_INI)
          If val(stmp) <> val(OEMX.txtINP(1).Text) Then
            ret = WritePrivateProfileString("DIS0", "ScalaCORREZIONIY", OEMX.txtINP(1).Text, Path_LAVORAZIONE_INI)
          End If
      End Select
      Call VISUALIZZA_CORREZIONI_PROFILO_RAB("N")
      Call SELEZIONA_TESTO
      Exit Sub
    End If
    'GESTIONE DELLE CASELLE PROFILO RAB
    k = 0
    If ACTUAL_PARAMETER_GROUP = "OEM0_CORRETTORI7" Then k = 1
    If (k > 0) Then
      Select Case Index
        Case 0
          stmp = GetInfo("DIS0", "ScalaCORREZIONIX", Path_LAVORAZIONE_INI)
          If val(stmp) <> val(OEMX.txtINP(0).Text) Then
            ret = WritePrivateProfileString("DIS0", "ScalaCORREZIONIX", OEMX.txtINP(0).Text, Path_LAVORAZIONE_INI)
          End If
        Case 1
          stmp = GetInfo("DIS0", "ScalaCORREZIONIY", Path_LAVORAZIONE_INI)
          If val(stmp) <> val(OEMX.txtINP(1).Text) Then
            ret = WritePrivateProfileString("DIS0", "ScalaCORREZIONIY", OEMX.txtINP(1).Text, Path_LAVORAZIONE_INI)
          End If
      End Select
      Call VISUALIZZA_PROFILO_RAB_ESTERNI(OEMX, "N", val(OEMX.txtINP(0).Text), val(OEMX.OEM1txtOX.Text), val(OEMX.OEM1txtOY.Text), OEMX.OEM1chkINPUT(0).Value, OEMX.OEM1chkINPUT(1).Value, OEMX.OEM1chkINPUT(2).Value)
      Call SELEZIONA_TESTO
      Exit Sub
    End If
    '
    'GESTIONE DELLA SCALA PER IL PROFILO DEL ROTORE
    k = 0
    If ACTUAL_PARAMETER_GROUP = "OEM1_LAVORO71" Then k = 1
    If ACTUAL_PARAMETER_GROUP = "OEM0_CORRETTORI71" Then k = 1
    If (k > 0) Then
      Call VISUALIZZA_PROFILO_RAB_ROTORI(OEMX, "N", val(OEMX.txtINP(0).Text), val(OEMX.OEM1txtOX.Text), val(OEMX.OEM1txtOY.Text), OEMX.OEM1chkINPUT(0).Value, OEMX.OEM1chkINPUT(1).Value, OEMX.OEM1chkINPUT(2).Value)
      Call SELEZIONA_TESTO
      Exit Sub
    End If
    k = 0
    If ACTUAL_PARAMETER_GROUP = "OEM1_MOLA71" Then k = 1
    If (k > 0) Then
      ret = WritePrivateProfileString("DIS0", "ScalaMola", OEMX.txtINP(0).Text, Path_LAVORAZIONE_INI)
      ret = WritePrivateProfileString("DIS0", "RaggioMola", OEMX.txtINP(1).Text, Path_LAVORAZIONE_INI)
      Call ROTORI1.CALCOLO_PROFILO_MOLA_ROTORI(2 * val(OEMX.txtINP(1).Text), 0)
      Call VISUALIZZA_MOLA_ROTORI(OEMX.OEM1PicCALCOLO, val(OEMX.txtINP(0).Text), OEMX.OEM1chkINPUT(0).Value, OEMX.OEM1chkINPUT(1).Value, val(OEMX.OEM1txtOX.Text), val(OEMX.OEM1txtOY.Text))
      Call SELEZIONA_TESTO
      Exit Sub
    End If
    '
    'GESTIONE DELLA SCALA PER IL PROFILO DEL CREATORE
    k = 0
    If ACTUAL_PARAMETER_GROUP = "OEM1_PROFILO10" Then k = 1
    If (k > 0) Then
      Call VISUALIZZAZIONE_PROFILO_CREATORE(val(OEMX.txtINP(0).Text), "N")
      Call SELEZIONA_TESTO
      Exit Sub
    End If
    k = 0  'FlagFORMA 028
    If ACTUAL_PARAMETER_GROUP = "OEM1_PROFILO15" Then k = 1
    If (k > 0) Then
      Call VISUALIZZAZIONE_PROFILO_CRFORMA(val(OEMX.txtINP(0).Text), "N")
      Call SELEZIONA_TESTO
      Exit Sub
    End If
    'VISUALIZZAZIONE VITI CONICHE
    k = 0
    If ACTUAL_PARAMETER_GROUP = "OEM1_MOLA41" Then k = 1
    If ACTUAL_PARAMETER_GROUP = "OEM1_MOLA42" Then k = 1
    If ACTUAL_PARAMETER_GROUP = "OEM1_MOLA43" Then k = 1
    If ACTUAL_PARAMETER_GROUP = "OEM1_MOLA44" Then k = 1
    If ACTUAL_PARAMETER_GROUP = "OEM1_MOLA45" Then k = 1
    If ACTUAL_PARAMETER_GROUP = "OEM1_MOLA46" Then k = 1
    If ACTUAL_PARAMETER_GROUP = "OEM1_MOLA47" Then k = 1
    If ACTUAL_PARAMETER_GROUP = "OEM1_MOLA47A" Then k = 1
    If ACTUAL_PARAMETER_GROUP = "OEM1_MOLA47B" Then k = 1
    If ACTUAL_PARAMETER_GROUP = "OEM1_MOLA47C" Then k = 1
    If ACTUAL_PARAMETER_GROUP = "OEM1_MOLA48" Then k = 1
    If ACTUAL_PARAMETER_GROUP = "OEM1_MOLA49" Then k = 1
    If ACTUAL_PARAMETER_GROUP = "OEM1_MOLA50" Then k = 1
    If ACTUAL_PARAMETER_GROUP = "OEM1_MOLA50A" Then k = 1
    If ACTUAL_PARAMETER_GROUP = "OEM1_MOLA50B" Then k = 1
    If ACTUAL_PARAMETER_GROUP = "OEM1_MOLA50C" Then k = 1
    If (k > 0) Then
      stmp = OEMX.SuOEM1.VARIABILE_SELEZIONATA
      If (stmp = "CODICE_MOLA") Or (stmp = "CODICE_M1") Or (stmp = "CODICE_M2") Or (stmp = "CODICE_M3") Then
      'CONFRONTO MOLE PER VITI CONICHE
      Select Case Index
        Case 0
          stmp = GetInfo("MOLA_VITI_CONICHE", "Scala", Path_LAVORAZIONE_INI)
          If (val(stmp) <> val(OEMX.txtINP(0).Text)) Then
            ret = WritePrivateProfileString("MOLA_VITI_CONICHE", "Scala", OEMX.txtINP(0).Text, Path_LAVORAZIONE_INI)
            ret = WritePrivateProfileString("MOLA_VITI_CONICHE", "ZeroPezzoX", OEMX.txtINP(2).Text, Path_LAVORAZIONE_INI)
            ret = WritePrivateProfileString("MOLA_VITI_CONICHE", "ZeroPezzoY", OEMX.txtINP(3).Text, Path_LAVORAZIONE_INI)
            OEMX.OEM1PicCALCOLO.BackColor = &HFFFFFF
            OEMX.OEM1PicCALCOLO.Cls
            Call CONFRONTA_MOLE_VITI_CONICHE("N")
            Call SELEZIONA_TESTO
          End If
        Case 2
          stmp = GetInfo("MOLA_VITI_CONICHE", "ZeroPezzoX", Path_LAVORAZIONE_INI)
          If (val(stmp) <> val(OEMX.txtINP(2).Text)) Then
            ret = WritePrivateProfileString("MOLA_VITI_CONICHE", "Scala", OEMX.txtINP(0).Text, Path_LAVORAZIONE_INI)
            ret = WritePrivateProfileString("MOLA_VITI_CONICHE", "ZeroPezzoX", OEMX.txtINP(2).Text, Path_LAVORAZIONE_INI)
            ret = WritePrivateProfileString("MOLA_VITI_CONICHE", "ZeroPezzoY", OEMX.txtINP(3).Text, Path_LAVORAZIONE_INI)
            OEMX.OEM1PicCALCOLO.BackColor = &HFFFFFF
            OEMX.OEM1PicCALCOLO.Cls
            Call CONFRONTA_MOLE_VITI_CONICHE("N")
            Call SELEZIONA_TESTO
          End If
        Case 3
          stmp = GetInfo("MOLA_VITI_CONICHE", "ZeroPezzoY", Path_LAVORAZIONE_INI)
          If (val(stmp) <> val(OEMX.txtINP(3).Text)) Then
            ret = WritePrivateProfileString("MOLA_VITI_CONICHE", "Scala", OEMX.txtINP(0).Text, Path_LAVORAZIONE_INI)
            ret = WritePrivateProfileString("MOLA_VITI_CONICHE", "ZeroPezzoX", OEMX.txtINP(2).Text, Path_LAVORAZIONE_INI)
            ret = WritePrivateProfileString("MOLA_VITI_CONICHE", "ZeroPezzoY", OEMX.txtINP(3).Text, Path_LAVORAZIONE_INI)
            OEMX.OEM1PicCALCOLO.BackColor = &HFFFFFF
            OEMX.OEM1PicCALCOLO.Cls
            Call CONFRONTA_MOLE_VITI_CONICHE("N")
            Call SELEZIONA_TESTO
          End If
      End Select
      Else
      'VISUALIZZAZIONE CANALE PER VITI CONICHE
      Select Case Index
        Case 0
          stmp = GetInfo("MOLA_VITI_CONICHE", "Scala", Path_LAVORAZIONE_INI)
          If val(stmp) <> val(OEMX.txtINP(0).Text) Then
            ret = WritePrivateProfileString("MOLA_VITI_CONICHE", "Scala", OEMX.txtINP(0).Text, Path_LAVORAZIONE_INI)
            ret = WritePrivateProfileString("MOLA_VITI_CONICHE", "ZeroPezzoX", "0", Path_LAVORAZIONE_INI)
            ret = WritePrivateProfileString("MOLA_VITI_CONICHE", "ZeroPezzoY", "0", Path_LAVORAZIONE_INI)
            OEMX.OEM1PicCALCOLO.BackColor = &HFFFFFF
            OEMX.OEM1PicCALCOLO.Cls
            Call VISUALIZZA_CANALE("N")
            Call SELEZIONA_TESTO
          End If
        Case 1
          stmp = GetInfo("MOLA_VITI_CONICHE", "RAGGIO", Path_LAVORAZIONE_INI)
          If val(stmp) <> val(OEMX.txtINP(1).Text) Then
            ret = WritePrivateProfileString("MOLA_VITI_CONICHE", "RAGGIO", OEMX.txtINP(1).Text, Path_LAVORAZIONE_INI)
            ret = WritePrivateProfileString("MOLA_VITI_CONICHE", "ZeroPezzoX", "0", Path_LAVORAZIONE_INI)
            ret = WritePrivateProfileString("MOLA_VITI_CONICHE", "ZeroPezzoY", "0", Path_LAVORAZIONE_INI)
            OEMX.OEM1PicCALCOLO.BackColor = &HFFFFFF
            OEMX.OEM1PicCALCOLO.Cls
            Call VISUALIZZA_CANALE("N")
            Call SELEZIONA_TESTO
          End If
        Case 2
          stmp = GetInfo("MOLA_VITI_CONICHE", "CORREZIONE_INTERASSE", Path_LAVORAZIONE_INI)
          If val(stmp) <> val(OEMX.txtINP(2).Text) Then
            ret = WritePrivateProfileString("MOLA_VITI_CONICHE", "CORREZIONE_INTERASSE", OEMX.txtINP(2).Text, Path_LAVORAZIONE_INI)
            ret = WritePrivateProfileString("MOLA_VITI_CONICHE", "ZeroPezzoX", "0", Path_LAVORAZIONE_INI)
            ret = WritePrivateProfileString("MOLA_VITI_CONICHE", "ZeroPezzoY", "0", Path_LAVORAZIONE_INI)
            OEMX.OEM1PicCALCOLO.BackColor = &HFFFFFF
            OEMX.OEM1PicCALCOLO.Cls
            Call VISUALIZZA_CANALE("N")
            Call SELEZIONA_TESTO
          End If
        Case 3
          stmp = GetInfo("MOLA_VITI_CONICHE", "DhF(1)", Path_LAVORAZIONE_INI)
          If val(stmp) <> val(OEMX.txtINP(3).Text) Then
            ret = WritePrivateProfileString("MOLA_VITI_CONICHE", "DhF(1)", OEMX.txtINP(3).Text, Path_LAVORAZIONE_INI)
            OEMX.OEM1PicCALCOLO.BackColor = &HFFFFFF
            OEMX.OEM1PicCALCOLO.Cls
            Call VISUALIZZA_MOLA
            Call VISUALIZZA_CANALE("N")
            Call SELEZIONA_TESTO
          End If
        Case 4
          stmp = GetInfo("MOLA_VITI_CONICHE", "DhF(2)", Path_LAVORAZIONE_INI)
          If val(stmp) <> val(OEMX.txtINP(4).Text) Then
            ret = WritePrivateProfileString("MOLA_VITI_CONICHE", "DhF(2)", OEMX.txtINP(4).Text, Path_LAVORAZIONE_INI)
            OEMX.OEM1PicCALCOLO.BackColor = &HFFFFFF
            OEMX.OEM1PicCALCOLO.Cls
            Call VISUALIZZA_MOLA
            Call VISUALIZZA_CANALE("N")
            Call SELEZIONA_TESTO
          End If
      End Select
      End If
      Exit Sub
    End If
  End If
  '
End Sub

Private Sub txtINP_LostFocus(Index As Integer)
'
Dim i As Integer
'
On Error Resume Next
  '
  For i = 0 To 4
    OEMX.lblINP(i).BackColor = &HFFFFFF
    OEMX.lblINP(i).ForeColor = &H80000008
  Next i
  '
End Sub

Private Sub Form_Activate()
'
Dim stmp  As String
Dim vtmp  As Single
Dim ret   As Long
Dim wTOP  As Single
Dim wLEFT As Single
'
On Error GoTo Form_Activate_Error
  '
  Call ChildActivate(Me)
  If (OEMX.Width <> g_nBeArtWidth) Or (OEMX.Height <> g_nBeArtHeight) Then
    Call AlMakeBorder(hwnd)
    '***********************************************************************
    wTOP = frmHeader.Top + frmHeader.Height
    wLEFT = 500
    '***********************************************************************
    Call OEMX.Move(g_nBeArtLeft, g_nBeArtTop, g_nBeArtWidth, g_nBeArtHeight)
    Call InitEnvironment(Me)
    OEMX.Left = wLEFT: OEMX.Top = wTOP
    'DIMENSIONAMENTO IN FUNZIONE DELLA GRANDEZZA FISSATA
    Call OEMX.OEM1frameCalcolo.Move(0, 0, g_nBeArtWidth, g_nBeArtHeight)
    OEMX.OEM1PicCALCOLO.Width = OEMX.OEM1frameCalcolo.Width - 50
    OEMX.OEM1PicCALCOLO.Height = OEMX.OEM1PicCALCOLO.Width / 2
    OEMX.OEM1PicCALCOLO.Left = OEMX.OEM1frameCalcolo.Width / 2 - OEMX.OEM1PicCALCOLO.Width / 2
    Call OEMX.frameCalcolo.Move(0, 0, g_nBeArtWidth, g_nBeArtHeight)
    OEMX.PicCALCOLO.Width = OEMX.frameCalcolo.Width - 50
    OEMX.PicCALCOLO.Height = OEMX.PicCALCOLO.Width / 2
    OEMX.PicCALCOLO.Left = OEMX.frameCalcolo.Width / 2 - OEMX.PicCALCOLO.Width / 2
    OEMX.SuOEM1.Top = g_nBeArtTop
    OEMX.SuOEM1.Left = g_nBeArtLeft
    OEMX.SuOEM1.Height = g_nBeArtHeight
    OEMX.SuOEM1.Width = g_nBeArtWidth
    OEMX.ScaleWidth = g_nBeArtWidth
    OEMX.ScaleHeight = g_nBeArtHeight
  End If
  stmp = GetInfo("CONFIGURAZIONE", "STATO_OEMX", PathFILEINI)
  Select Case stmp
  Case "OEMX"
      Call MIX1_NASCONDI
      Call MNG0_NASCONDI
      Call PRG0_NASCONDI
      OEMX.SuOEM1.Top = g_nBeArtTop
      OEMX.SuOEM1.Left = g_nBeArtLeft
      OEMX.SuOEM1.Height = g_nBeArtHeight
      OEMX.SuOEM1.Width = g_nBeArtWidth
      OEMX.ScaleWidth = g_nBeArtWidth
      OEMX.ScaleHeight = g_nBeArtHeight
      Call OEMX_AZIONI("RECALL", PathDB)
      OEMX.lblMANDRINI.BackColor = QBColor(14)
      OEMX.McCombo1.BackColor = QBColor(14)
      OEMX.lblINPUT.BackColor = QBColor(14)
      OEMX.txtINPUT.BackColor = QBColor(14)
      For i = 0 To 4
        OEMX.lblINP(i).BackColor = &HFFFFFF
        OEMX.txtINP(i).BackColor = &HFFFFFF
      Next i
      OEMX.OEM1lblOX.BackColor = &HFFFFFF
      OEMX.OEM1lblOY.BackColor = &HFFFFFF
      OEMX.OEM1txtOX.BackColor = &HFFFFFF
      OEMX.OEM1txtOY.BackColor = &HFFFFFF
      OEMX.OEM1ListaCombo1.Left = OEMX.OEM1Combo1.Left
      OEMX.OEM1ListaCombo1.Width = OEMX.OEM1Combo1.Width
      OEMX.OEM1ListaCombo1.Top = OEMX.OEM1Combo1.Top + OEMX.OEM1Combo1.Height
      OEMX.OEM1ListaCombo1.Visible = False
  Case "PROGRAMMI"
      Call OEMX_NASCONDI
      Call MIX1_NASCONDI
      Call MNG0_NASCONDI
      Call PRG0_VISUALIZZA
      OEMX.PRG0Image1.Picture = LoadPicture(g_chOemPATH & "\" & "logo1.bmp")
      ACTUAL_PARAMETER_GROUP = ""
      Call LEGGI_LINGUA
      Call INIZIALIZZA_PRG0
      OEMX.PRG0lblINDICE.BackColor = &HFF&
      OEMX.PRG0lblINDICE.ForeColor = &HFFFFFF
      OEMX.McListPrg.BackColor = &HFFFFFF
      OEMX.PRG0Label5.BackColor = &HFFFFFF
      OEMX.PRG0txtNomePezzo.BackColor = &HFFFFFF
      OEMX.PRG0lblMATRICOLA.BackColor = &HFFFFFF
      OEMX.PRG0txtMATRICOLA.BackColor = &HFFFFFF
  Case "ARCHIVIO"
      Call OEMX_NASCONDI
      Call MIX1_NASCONDI
      Call PRG0_NASCONDI
      Call MNG0_VISUALIZZA
      Call INIZIALIZZA_MNG0
  Case "COMBINAZIONI"
      Call OEMX_NASCONDI
      Call PRG0_NASCONDI
      Call MNG0_NASCONDI
      Call MIX1_VISUALIZZA
  End Select
  '
Exit Sub

Form_Activate_Error:
  MsgBox "Errore " & Err.Number & " (" & Err.Description & ") in Sub: Form_Activate"
  '
End Sub

Private Sub Form_Deactivate()
  '
  OEMX.Timer1.Enabled = False
  OEMX.Polling.Enabled = False
  OEMX.SuOEM1.InfoBkColor = vbGreen
  '
  Call ChildDeActivate(Me)
  '
End Sub

Private Sub Form_Load()
'
Dim stmp       As String
Dim vtmp       As Single
Dim ret        As Long
Dim DIMENSIONE As Integer
'
On Error GoTo errForm_Load
  '
  Call AlMakeBorder(hwnd)
  Call OEMX.Move(g_nBeArtLeft, g_nBeArtTop, g_nBeArtWidth, g_nBeArtHeight)
  Call InitEnvironment(Me)
  OEMX.SuOEM1.Top = g_nBeArtTop
  OEMX.SuOEM1.Left = g_nBeArtLeft
  OEMX.SuOEM1.Height = g_nBeArtHeight
  OEMX.SuOEM1.Width = g_nBeArtWidth
  OEMX.ScaleWidth = g_nBeArtWidth
  OEMX.ScaleHeight = g_nBeArtHeight
  Call LEGGI_LINGUA
  DIMENSIONE = val(GetInfo("Configurazione", "DIMENSIONE", Path_LAVORAZIONE_INI))
  If (DIMENSIONE <= 0) Then DIMENSIONE = 12
  Call IMPOSTA_FONT(Me, DIMENSIONE)
  'DIMENSIONAMENTO IN FUNZIONE DELLA GRANDEZZA FISSATA
  Call OEMX.OEM1frameCalcolo.Move(0, 0, g_nBeArtWidth, g_nBeArtHeight)
  OEMX.OEM1PicCALCOLO.Width = OEMX.OEM1frameCalcolo.Width - 50
  OEMX.OEM1PicCALCOLO.Height = OEMX.OEM1PicCALCOLO.Width / 2
  OEMX.OEM1PicCALCOLO.Left = OEMX.OEM1frameCalcolo.Width / 2 - OEMX.OEM1PicCALCOLO.Width / 2
  Call OEMX.frameCalcolo.Move(0, 0, g_nBeArtWidth, g_nBeArtHeight)
  OEMX.PicCALCOLO.Width = OEMX.frameCalcolo.Width - 50
  OEMX.PicCALCOLO.Height = OEMX.PicCALCOLO.Width / 2
  OEMX.PicCALCOLO.Left = OEMX.frameCalcolo.Width / 2 - OEMX.PicCALCOLO.Width / 2
  '
Exit Sub

errForm_Load:
  WRITE_DIALOG Error(Err)
  Resume Next

End Sub

Private Sub McCombo1_Click()
'
On Error Resume Next
  '
  Call CALCOLO_TEMPI_DI_RETTIFICA_380(OEMX.McCombo1.Text, "N")
  '
End Sub

Private Sub McCombo1_KeyUp(KeyCode As Integer, Shift As Integer)
'
Dim TIPO_LAVORAZIONE  As String
'
On Error Resume Next
  '
  'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  If (KeyCode = 13) Then
   If (TIPO_LAVORAZIONE = "MOLAVITEG160") Then
      Call CALCOLO_TEMPI_DI_RETTIFICA_MOLAVITEG160(OEMX.McCombo1.Text, "N")
   Else
   If (TIPO_LAVORAZIONE = "MOLAVITE_SG") Then
      Call CALCOLO_TEMPI_DI_RETTIFICA_MOLAVITE_SG(OEMX.McCombo1.Text, "N")
   Else
    If (TIPO_LAVORAZIONE = "MOLAVITE") Then
       Call CALCOLO_TEMPI_DI_RETTIFICA_MOLAVITE(OEMX.McCombo1.Text, "N")
    Else
       Call CALCOLO_TEMPI_DI_RETTIFICA_380(OEMX.McCombo1.Text, "N")
    End If
   End If
   End If
   OEMX.McCombo1.SetFocus
  End If
  '
End Sub

Private Sub Polling_Timer()
'
Static ii           As Integer
Dim i               As Integer
Dim stmp            As String
Dim NuoviCorrettori As String
Dim NuovoProfilo    As String
Dim tstr            As String
Dim iRESET          As Integer
'
Dim PERCORSO    As String
Dim COORDINATE  As String
Dim RADICE      As String
Dim npt         As Integer

On Error GoTo errPolling_Timer
  '
  'VISUALIZZAZIONE TIMER
  ii = ii + 1
  ii = ii Mod 4
  tstr = Mid("-\|/", (ii Mod 4) + 1, 1)
  If (SuOEM1.InfoBkColor = vbYellow) Then
    SuOEM1.InfoBkColor = vbRed
  Else
    SuOEM1.InfoBkColor = vbYellow
  End If
  'GESTIONE CORREZIONI PER ROTORI ESTERNI
  If (ActualGroup = "OEM1_MOLA71") Then
    WRITE_DIALOG "Waiting for wheel points correctors from PC " & tstr
    'LEGGO SE SONO DISPONIBILI DEI CORRETTORI
    NuoviCorrettori = GetInfo("ROTORI_ESTERNI", "NuoviCorrettori", Path_LAVORAZIONE_INI)
    NuoviCorrettori = Trim$(UCase$(NuoviCorrettori))
    If (NuoviCorrettori = "Y") Then
      iRESET = CONTROLLO_STATO_RESET
      If (iRESET <> 1) Then
        'AVVERTO CHE NON FACCIO NIENTE
        WRITE_DIALOG "Data cannot be modified: CNC is not on RESET state"
        Exit Sub
      End If
      Call ROTORI1.CARICA_CORRETTORI_MOLA
      'RIPRISTINO CONDIZIONI INIZIALI
      i = WritePrivateProfileString("ROTORI_ESTERNI", "NuoviCorrettori", "N", Path_LAVORAZIONE_INI)
      'INCREMENTO CONTATORE CORREZIONI
      Dim CONTATORE_DDE_MOLA_ROTORI As String
      CONTATORE_DDE_MOLA_ROTORI = GetInfo("ROTORI_ESTERNI", "CONTATORE_DDE_MOLA_ROTORI", Path_LAVORAZIONE_INI)
      CONTATORE_DDE_MOLA_ROTORI = Trim$(UCase$(CONTATORE_DDE_MOLA_ROTORI))
      If (Len(CONTATORE_DDE_MOLA_ROTORI) > 0) Then
        stmp = OPC_LEGGI_DATO(CONTATORE_DDE_MOLA_ROTORI)
        i = val(stmp)
        i = i + 1
        Call OPC_SCRIVI_DATO(CONTATORE_DDE_MOLA_ROTORI, Format$(i, "##########0"))
      End If
      'ABILITAZIONE DI TUTTI I CORRETTORI
      'LETTURA DEL PERCORSO DI RIFERIMENTO E COORDINATE RAB CON ROTAZIONE
      PERCORSO = Lp_Str("PERCORSO")
      PERCORSO = g_chOemPATH & "\" & PERCORSO
      'LETTURA DEL NUMERO DEI PUNTI
      npt = Lp("R[147]")
      'RICAVO IL NOME DEI PUNTI DEL ROTORE DAL PERCORSO
      COORDINATE = PERCORSO
      If (Len(COORDINATE) > 0) Then
        i = InStr(COORDINATE, "\")
        While (i <> 0)
          j = i
          i = InStr(i + 1, COORDINATE, "\")
        Wend
        'RICAVO IL NOME DEI PUNTI DEL ROTORE
        COORDINATE = Right$(COORDINATE, Len(COORDINATE) - j)
      End If
      'RICAVO IL PERCORSO DI LAVORO
      RADICE = PERCORSO
      i = InStr(RADICE, COORDINATE)
      RADICE = Left$(RADICE, i - 2)
      'CREAZIONE DI ABILITA.COR
      Open RADICE & "\ABILITA.COR" For Output As #1
        For i = 0 To npt + 1
            Print #1, "1"
        Next i
      Close #1
      'SEGNALAZIONE UTENTE
      WRITE_DIALOG "ALL CORRECTORS ENABLED"
    End If
  End If
  '
  'Gestione correzioni per RAB_ESTERNI
  If (ActualGroup = "OEM1_MOLA7") Then
    WRITE_DIALOG "Waiting for wheel points correctors from PC " & tstr
    'LEGGO SE SONO DISPONIBILI DEI CORRETTORI
    NuoviCorrettori = GetInfo("PROFILO_RAB_ESTERNI", "NuoviCorrettori", Path_LAVORAZIONE_INI): NuoviCorrettori = Trim$(UCase$(NuoviCorrettori))
    If (NuoviCorrettori = "Y") Then
      iRESET = CONTROLLO_STATO_RESET
      If (iRESET <> 1) Then
        'AVVERTO CHE NON FACCIO NIENTE
        WRITE_DIALOG "Data cannot be modified: CNC is not on RESET state"
        Exit Sub
      End If
      Call ROTORI1.CARICA_CORRETTORI_MOLA
      'RIPRISTINO CONDIZIONI INIZIALI
      i = WritePrivateProfileString("PROFILO_RAB_ESTERNI", "NuoviCorrettori", "N", Path_LAVORAZIONE_INI)
      'ABILITAZIONE DI TUTTI I CORRETTORI
      'LETTURA DEL PERCORSO DI RIFERIMENTO E COORDINATE RAB CON ROTAZIONE
      PERCORSO = LEP_STR("PERCORSO")
      RADICE = g_chOemPATH & "\RAB_ESTERNI\" & PERCORSO
      
      'LETTURA DEL NUMERO DEI PUNTI
      npt = Lp("R[147]")
      
      'CREAZIONE DI ABILITA.COR
      Open RADICE & "\ABILITA.COR" For Output As #1
        For i = 0 To npt + 1
            Print #1, "1"
        Next i
      Close #1
      'SEGNALAZIONE UTENTE
      WRITE_DIALOG "ALL CORRECTORS ENABLED"
    End If
  End If

Exit Sub

errPolling_Timer:
  WRITE_DIALOG "Sub Polling_Timer: ERROR -> " & Err
  If FreeFile > 1 Then Close
  Exit Sub
  '
End Sub

Private Sub SuOEM1_CNCChanged(ByVal TABELLA As String, ByVal VARIABILE As String, Valore As String, Caricamento As Boolean, ByVal CARICA_CALCOLO As Boolean)
'
On Error Resume Next
  '
  If (CARICA_CALCOLO = True) Then
    Select Case TABELLA
      '
      Case "OEM1_PROFILO71"
        If (Not Caricamento) Then Call CARICA_DATI_CONTROLLO
        '
      Case "OEM1_PROFILO4":       Call VITI_ESTERNE_CALCOLO_CONTROLLO
      
      Case "OEM1_MACCHINA7"
        If (Not Caricamento) Then Call CARICA_DATI_CONTROLLO_RAB
        '
      '''Case "OEM1_PROFILO84":      Call VITIV_ESTERNE_CALCOLO_CONTROLLO
        '
    End Select
  End If
  '
  If (VARIABILE = "MOLA") And (Valore = "MOLA") Then
    Call INVIO_SPF(TABELLA, True)
  Else
    Call INVIO_SPF(TABELLA)
  End If
  '
End Sub

Private Sub txtINPUT_GotFocus()
  '
  Call SELEZIONA_TESTO
  '
End Sub

Private Sub txtINPUT_KeyUp(KeyCode As Integer, Shift As Integer)
'
Dim DEMEULE_NEW       As Single
Dim DEMEULE_OLD       As Single
Dim stmp              As String
Dim ret               As Integer
Dim TIPO_LAVORAZIONE  As String
'
On Error Resume Next
  '
  If (KeyCode = 13) Then
    'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
    TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
    TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
    'Diametro mola per tipo di lavorazione
    DEMEULE_OLD = val(GetInfo("TEMPI", "DiametroEsternoMola(" & OEMX.McCombo1.Text & ")", Path_LAVORAZIONE_INI))
    stmp = Trim$(OEMX.txtINPUT.Text)
    If (Len(stmp) > 0) Then
      'CALCOLO IL NUOVO VALORE
      DEMEULE_NEW = val(OEMX.txtINPUT.Text)
      If (DEMEULE_NEW <> DEMEULE_OLD) Then
        stmp = frmt(DEMEULE_NEW, 4)
        ret = WritePrivateProfileString("TEMPI", "DiametroEsternoMola(" & OEMX.McCombo1.Text & ")", stmp, Path_LAVORAZIONE_INI)
        If (TIPO_LAVORAZIONE = "MOLAVITE") Then
          Call CALCOLO_TEMPI_DI_RETTIFICA_MOLAVITE(OEMX.McCombo1.Text, "N")
        Else
        If (TIPO_LAVORAZIONE = "MOLAVITEG160") Then
          Call CALCOLO_TEMPI_DI_RETTIFICA_MOLAVITEG160(OEMX.McCombo1.Text, "N")
        Else
          Call CALCOLO_TEMPI_DI_RETTIFICA_380(OEMX.McCombo1.Text, "N")
        End If
        End If
      End If
    Else
      'RIMETTO IL VECCHIO VALORE
      OEMX.txtINPUT.Text = frmt(DEMEULE_OLD, 4)
    End If
  End If
  '
End Sub
