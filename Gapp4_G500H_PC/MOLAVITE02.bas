Attribute VB_Name = "MOLAVITE02"
Option Explicit

' modelli antitwist

' 1) calcolo dello svergolamento

' 2) calcolo dello spostamento massimo lungo x e z in profilatura
' Routine CalcoloSpostRullo dentro dontro fonction
'    INPUT: fha(2) da correggere
'   OUTPUT-01: DZ(2) spostamento fianco
'   OUTPUT-02: DX(2) diminuzione di interasse
'   OUTPUT-03: errore di passo
'--------------------------------------
Private PI As Double
Private Phi$, PAR$, Par1$, VPB, TipoZona$
Private NeTot As Integer ' Numero ingranaggi
Private NeTotMf As Integer ' Numero ingranaggi definiti
Private NE As Integer  ' Numero ingranaggio considerato
Private FinIng$        ' "MV" o "MF"  ( mola vite o di forma )
Private SgrIng$        ' "Nulla" o  "MvDaDefinire" o "MvEsistente"
Private BOMB$

Private DisegnoIng$(10)
Private Mr As Single                      ' Ingranaggio : MODULO NORMALE
Private Aprd As Single                    ' Ingranaggio : ANG.DI PRESS.NORMALE in Gradi
Private Apr As Double                     ' Ingranaggio : ANG.DI PRESS.NORMALE in Radianti
Private Z1(10) As Single                  ' Ingranaggio : Numero Denti
Private HelpD(10) As Single               ' Ingranaggio : Elica Primitiva in Gradi
Private Help(10) As Double                ' Ingranaggio : Elica Primitiva in Radianti
Private Di1(10) As Single                 ' Ingranaggio : Diametro Interno
Private DE1(10) As Single                 ' Ingranaggio : Diametro esterno
Private Kdt(10) As Single                 ' Ingranaggio : Numero Denti Per Controllo quota W
Private Wdt(10) As Single                 ' Ingranaggio : Quota W di finitura
Private WdtDent(10) As Single             ' Ingranaggio : Quota W di dentatura
Private DPG(10) As Single                 ' Ingranaggio : Diametro rulli o sfere per controllo
Private Wpg(10) As Single                 ' Ingranaggio : Finitura : Quota rulli o sfere
Private WpgDent(10) As Single             ' Ingranaggio : Dentatura: Quota rulli o sfere
Private FASCIA(10) As Single
Private BombElica(10) As Single
Private Epr1(10) As Single                ' Ingranaggio : Spessore Primitiva normale (DISEGNO E FINITURA)
Private Epr1Dent(10) As Single            ' Ingranaggio : DENTATURA: Spessore Primitivo normale
' Private Eb1(10) As single               ' Ingranaggio : -------- Di Base Apparente
Private Es1(10) As Single                 ' Ingranaggio : -------- Di Testa Apparente
Private Ic1(10) As Single                 ' Ingranaggio : Larghezza del vano Apparente
Private Kdep(10) As Single                ' Ingranaggio : Coefficiente di spostamento
Private SAP(10) As Single                 ' Ingranaggio : Diametro Attivo Richisto
Private Mapp(10) As Single                ' Ingranaggio : Modulo Apparente
Private ApApp(10) As Double               ' Ingranaggio : Ang.Press.Apparente In radianti
Private Dp1(10) As Single                 ' Ingranaggio : Diametro Primitivo
Private DB1(10) As Single                 ' Ingranaggio : Diametro di Base
Private HELB(10) As Double                ' Ingranaggio : Elica di base in radianti
Private NbpRast As Integer
'  Rastremazione Piede Mola ( Testa Ingranaggio )
Private DminiRastp(10) As Single           ' Ingranaggio  Richiesto : Diametro mini
Private DmaxiRastp(10) As Single           ' -----------  --------- : Diametro maxi
Private Erastp(10) As Single               ' -----------  --------- : Entita
Private Brastp(10) As Single                     ' -----------  : Bombatura
Private BombMaxiRastp(10) As Single           ' -----------  : Bombatura massima possibile
' Rastremazione Testa Mola ( Piede Ingranaggio )
Private DmaxiRastt(10) As Single           ' Ingranaggio  Richiesto : Diametro Maxi
Private DminiRastt(10) As Single           ' Ingranaggio  --------- : Diametro Mini
Private Erastt(10) As Single               ' Ingranaggio  --------- : Entita
Private Brastt(10) As Single                     ' Ingranaggio : Bombatura
Private BombMaxiRastt(10) As Single              ' Ingranaggio  : Bombatura massima possibile

' Bombatura Ingranaggio
Private Ebomb(10) As Single
Private DbombMini(10) As Single
Private DbombMaxi(10) As Single
Private DbombMedio(10) As Single
Private ProgettoMvEsistente$

Type SgrFinType
    XpCr(150) As Single
    AddCr(150) As Single
    ApCr(150) As Single
    MolaEsistente As String
    VersioneProgettoMv As Integer
    BombMola As String
    RastpMola As String
    RasttMola As String
    MolaOk As String
    Dr1(10) As Single                   ' Ingranaggio : Diametro di rotolamento
    Helr(10) As Double              ' Ingranaggio : Angolo elica su diametro di rotolamento (in radianti)
    Eb1(10) As Double
    AngTroc(10) As Double           ' Ingranaggio : Ottenuto:  Angolo Per Ottenere Dattivo Dentatura
    GioccoFondoOtt(10) As Single        ' Mola / ing : Gioco Tra R.inter.Ing. e R.Ester.Mola
    DsmussOtt(10) As Single             ' -----------  --------- : D. "smusso"
    DiObt(10) As Single                 ' Ingranaggio : Ottenuto: Diametro interno
    De1Obt(10) As Single
    DactObt(10) As Single               ' Ingranaggio : Ottenuto: Diametro attivo di piede finito
    InterfF(10) As String               ' Ingranaggio : Ottenuto: Interferenza restante su profilo finito ("OUI" "NON")
    '  Rastremazione Piede Mola ( Testa Ingranaggio )
    DminiRastpObt(10) As Single           ' -----------  Ottenuto  : Diametro Mini
    ERastpObt(10, 9) As Double           ' -----------  --------  : Entita Sur R sotto
    RRastpObt(10, 9) As Single            ' -----------  --------  : Rayon
    DbRastp(10) As Single                 ' -----------  --------  : Diametro di base
    EbRastp(10) As Double                 ' -----------  --------  : Spessore di base
    DbApPiede(10) As Single
    EbApPiede(10) As Double
    ERastpObtDe(10) As Double
    ErastpObtDmaxiRastp(10) As Single
    DmaxiRastpObtBomb(10) As Single
    ' ErastpObtDmaxiRastpObtBomb(10) As DOUBLE
    ' Rastremazione Testa Mola ( Piede Ingranaggio )
    DbRastt(10) As Single                 ' Ingranaggio  --------- : Diametro di base
    EbRastt(10) As Double                 ' Ingranaggio  --------- : Spessore di base
    DbApTesta(10) As Single
    EbApTesta(10) As Double
    DmaxiRasttObt(10) As Single           ' Ingranaggio  Ottenuto  : Diametro Maxi
    ERasttObt(10, 9) As Double           ' Ingranaggio  --------  : Entita Sur R sotto
    RRasttObt(10, 9) As Single           ' Ingranaggio  --------  : Rayon
    DminiRasttObtBomb(10) As Single
    ' Sgr.ErasttObtDminiRasttObtBomb(10) As DOUBLE
    ErasttObtDminiRastt(10) As Double
    ErasttObtSap(10) As Double
    ' Bombatura
    DbombMedioOtt(10) As Single
    EbombOtt(10) As Double
    ErrApOtt(10) As Double
    ' Mola
    'LavFondo As String              ' "O" "N" Se la mola lavora il fondo ingranaggio
    GioccoFondo As Single           ' Mola / ing : Gioco Tra R.inter.Ing. e R.Ester.Mola
    DmolaV As Single                ' MOLA VIRE FINITURA: Diametro Esterno
    NFil As Integer                 ' MOLA VIRE FINITURA: Numero filetti
    InFilD As Single                ' MOLA VIRE FINITURA: Inclinazione filetto        [ � ]
    InFil As Single
    DpMola As Single
    PasAxial As Single              ' MOLA VIRE FINITURA: Passo assiale
    S0(10) As Single                ' Mola / ing : Addendum di rotolamento
    MrMola As Single                ' Cre.Rif.Mola : Modulo Normale
    ApMolaD As Single               ' Cre.Rif.Mola : Ang.Di Press.Normale             [ � ]
    Apmola As Double                ' Cre.Rif.Mola : Ang.Di Press.Normale in radianti
    Sw As Single                    ' Cre.Rif.Mola : Spessore dente sulla linea primitiva
    Hkw As Single                   ' Cre.Rif.Mola : Addendum
    RtPieno As Single               ' Cre.Rif.Mola : Raggio di testa pieno
    Rtesta As Single                ' Cre.Rif.Mola : Raggio di testa
    Xrtesta As Single               ' Cre.Rif.Mola : Raggio di testa : Centre / asse pieno
    Yrtesta As Single               ' Cre.Rif.Mola : Raggio di testa : Centre / primitiv
    RfPieno As Single               ' Cre.Rif.Mola : Raggio di fondo pieno
    Rfondo As Single                ' Cre.Rif.Mola : Raggio di fondo
    XrFondo As Single               ' Cre.Rif.Mola : Raggio di fondo : Centre / asse pieno
    Yrfondo As Single               ' Cre.Rif.Mola : Raggio di fondo : Centre / primitiv
    Hfw As Single                   ' Cre.Rif.Mola : Dedendum
    Htotale As Single               ' Cre.Rif.Mola : Altezza totale dente
    HtMini As Single                ' Cre.Rif.Mola : Altezza dente minimo
    HRastp As Double                ' Cre.Rif.Mola : Rastr.Piede : Altezza / Testa
    ApRastpD As Double              ' Cre.Rif.Mola : ----------- : Ang.di press. in gradi
    ApRastp As Double               ' Cre.Rif.Mola : ----------- : Ang.di press. in radianti
    YiRastp As Double               ' Cre.Rif.Mola : ----------- : Inizio / Primitivo
    XiRastp As Double               ' Cre.Rif.Mola : ----------- : Inizio / Asse dente
    YfRastp As Double              ' Cre.Rif.Mola : ----------- : Fine / Primitivo
    Rrastp As Double               ' Cre.Rif.Mola : ----------- : Raggio per Bombatura
    YrRastp As Double              ' Cre.Rif.Mola : ----------- : Centro Raggio / Primitivo
    XrRastp As Double              ' Cre.Rif.Mola : ----------- : Centro Raggio /  Asse dente
    ApPiede As Double              ' Cre.Rif.Mola : ----------- : A.Press tra fondo e rastrem
    HRastt As Double               ' Cre.Rif.Mola : Rastr.Testa : Altezza / testa
    ApRasttD As Double             ' Cre.Rif.Mola : ----------- : Ang.di press. in gradi
    ApRastt As Double              ' Cre.Rif.Mola : ----------- : Ang.di press. in radianti
    YiRastt As Double              ' Cre.Rif.Mola : ----------- : Inizio / Primitivo
    XiRastt As Double              ' Cre.Rif.Mola : ----------- : Inizio / Asse dente
    YfRastt As Double              ' Cre.Rif.Mola : ----------- : Fine / Primitivo
    Rrastt As Double               ' Cre.Rif.Mola : ----------- : Raggio per Bombatura
    YrRastt As Double              ' Cre.Rif.Mola : ----------- : Centro Raggio / Primitivo
    XrRastt As Double              ' Cre.Rif.Mola : ----------- : Centro Raggio /  Asse dente
    ApTesta As Double              ' Cre.Rif.Mola : ----------- : A.Press tra Testa e rastrem
    EsMola As Double               ' Cre.Rif.Mola : Spessore di testa (per verifica)
    Icf As Double                  ' Cre.Rif.Mola : larghezza vano sul fondo (senza R.Fondo)
    '  Dati Seguenti  : Ex MolRullo
    SvergolamentoOtt(10) As Single
    TitoloRul As String
    DatRul As String
    FormRullo As String     ' = "S2F" o "SFF" o "COPSR" o "COPCR" o "MUL"
    Profilatura As String   ' = "NORM" o "ASS"
    NbpTotale As Integer   ' Numero punti della cremagliera Mv
    NptEvolvMini As Integer
    NptEvolvMaxi As Integer
    NptFiancoMini As Integer ' della cremagliera Mv
    NptFiancoMaxi As Integer ' della cremagliera Mv
    Drullo As Single
    IncRulloD As Single                 ' RULLO: inclinazione rullo [gradi]
    ApRulMedioD As Single               ' RULLO: angolo di pressione medio [gradi]
    SpTotControlloRullo As Single       ' RULLO: spessore
    AddRullo As Single
    BombRullo As Single                 ' RULLO: bombatura
    SpControlloRulloSing As Single
    AddControlloRullo As Single         ' RULLO: addendum di controllo
    SpRulloCentrale As Single
    DrulloCentrale As Single
    DrulliFianchi As Single
    XaxMola(100) As Single
    RaxMola(100) As Single
    ApAxMola(100) As Single
    Rrull(100) As Single
    Xrull(100) As Single
    ApRullD(100) As Single
    RrullCent(30) As Single
    XrullCent(30) As Single
    ApRullCentD(30) As Single
    NbpTotRulCent As Integer
    NumIniRtesta As Integer
    NumFineRfondo As Integer
    NptIniRastt As Integer
    NptIniRastp As Integer
    Aprul(200) As Single
    Xrul(200) As Single
    Rrul(200) As Single
    NbpCorpRul As Integer
    XcorpRullo(200) As Single
    RcorpRullo(200) As Single
    SovramRulloTorn As Single
    XcorpRulloTorn(200) As Single
    RcorpRulloTorn(200) As Single
    LcorpRullo As Single
    EquCorpRul As Single ' Minorazione profilo (micron)
    EquidCorpMola As Single ' Minorazione profilo (micron)
    IdYmax As Single
End Type
Private Sgr As SgrFinType ' mola a sgrossare
Private FIN As SgrFinType ' mola a finire
Private Ing As SgrFinType ' mola per ingranaggio teorico

Private AprDdefault As Single
Private ApAppDdefault(10) As Single
Private InMolaFdDefault(10) As Single

'  Sovrametallo per mola finitrice

Private SovramMv(10) As Single

' Mola di forma

Private DmolaF(10) As Single
Private GioccoFondoMf(10) As Single ' Gioco di fondo mola di forma (usato per calcolare il diametro interno ottenuto sull'ingranaggio)
Private InMolaFd(10) As Single       ' MOLA DI FORMA: inclinazione [�]
Private SapMf(10) As Single          ' MOLA DI FORMA: sap pratico, RAGGI DI FONDO OTTENUTI
Private SapMiniMf As Single
Private RfondoMf(10) As Single
Private RtestaMf(10) As Single
Private RfRtMf(10) As Single        ' MOLA DI FORMA: R. SAP / R.Testa
Private RtMaxiMf(10) As Single
Private RtMax_Y As Single
Private RtMax_X As Single
Private NpTotMf As Integer
Private ErrEvolvMf(10) As Single    ' MOLA DI FORMA: ERRORE EVOLVENTE [ mm ]
Private PntIniFondoMf As Integer
Private PntTgRettaRtMf As Integer  ' Numero del punto di tangenza tra R.testa e fianco ( se c'� un raggio solo )
Private PntTg2raggiMf As Integer  ' Numero del punto di tangenza tra i 2 raggi
Private PntFineRaggMf As Integer  '  Numero del punto fine raggi
Private XottMf(99) As Single ' Coordinate vano ottenuto
Private YottMf(99) As Single ' Coordinate vano ottenuto
Private ApOttMf(99) As Single ' Coordinate vano ottenuto
Private RfRtOttAppMf(10) As Single ' Raggio fondo ottenuto con RfRtMf
Private ErrRfRtOttAppMf(10) As Single ' Errore
Private RfRtOttNormMf(10) As Single ' Raggio fondo ottenuto con RfRtMf
Private ErrRfRtOttNormMf(10) As Single ' Errore
Private RtestaOttAppMf(10, 2) As Single        ' MOLA DI FORMA: Raggio fondo ottenuto con RtestaMf
Private ErrRtestaOttAppMf(10, 2) As Single ' Errore
Private RtestaOttNormMf(10, 2) As Single       ' MOLA DI FORMA: Raggio testa ottenuto su piano normale
Private ErrRtestaOttNormMf(10, 2) As Single ' Errore
Private DrtgFondo(10) As Single ' Diametro ottenuto con PntTg2raggiMf
Private DmedioRtesta(10) As Single ' Diametro medio ottenuto con RtestaMf
Private XmF(99) As Single ' Coordinate mola di forma
Private YmF(99) As Single ' Coordinate mola di forma
Private ApMf(99) As Single ' Coordinate mola di forma

' Dati seguenti : Creatore per dentatura

Private SovramDent(10) As Single ' Creatore
Private Eb1Dent(10) As Single ' Creatore
Private ApFmD(10) As Single ' Creatore
Private ApFm(10) As Single ' Creatore
Private ApProtuD(10) As Single ' Creatore
Private ApProtu(10) As Single ' Creatore
Private EpProtu(10) As Single ' Creatore
Private RtFm(10) As Single ' Creatore
Private DrFm(10) As Single ' Creatore
Private AddFm(10) As Single ' Creatore
Private DedSt(10) As Single ' Creatore
Private ApStD(10) As Single ' Creatore
Private ApSt(10) As Single ' Creatore
Private DedStTh(10) As Single ' Creatore teorico per ottenere uno smusso H = 0.1 mod.
Private DedStMaxi(10) As Single ' Creatore teorico per ottenere uno smusso H = 0.02
Private AddFmTh(10) As Single ' Creatore teorico per rispettare D.int.ing.
Private HelrFm(10) As Single ' Creatore
Private MrFm(10) As Single ' Creatore
Private EpFm(10) As Single ' Creatore
Private RtPienoFm(10) As Single ' Creatore
Private YrtFm(10) As Single ' Creatore
Private XrtFm(10) As Single ' Creatore
Private InterferFm$(10) ' Creatore
Private DattivoDentato(10)  As Single ' Creatore
Private AngTrocFm(10) As Single ' Creatore
Private DbProtu(10)  As Single ' Creatore
Private EbProtu(10) As Single   ' Creatore
Private DiniDevProtu(10) As Single   ' Creatore
Private Di1Dent(10) As Single        ' - mm -  Diametro interno di dentatura
Private DbSt(10) As Single
Private EbSt(10) As Single
Private DstDentato(10) As Single
'--------------------------------------------------------------
' Rettifica
Private INTERASSE#
Private FASCIAFHBG#
Private CORR_TW_KIND0%
'--------------------------------------------------------------
Public Sub DB_GUD_AT_Azzera()
    Dim i%
    For i = 1 To 2
     Call SP("LNG_TW[0," & i - 1 & "]", 0, False) '_F
     Call SP("FHA_TW[0," & i - 1 & "]", 0, False) '_F
    Next i
    Call SP("iTW_TUNING", 0, False)
    Call SP("iTW_TUNING_corr", 0, False)
    Call SP("TW_TUNING", 0, False)
    Call SP("CORR_TW_KIND", 0, False)
    Call SP("AT_DRESSING", 0, True)
End Sub
'--------------------------------------------------------------
Public Sub DB_GUD_AT_Aggiorna()
 Dim Profila_ZonaAT_Mola_INOUT As Boolean                   'GB@SAMP28122016
'---------------------------------------------------------------------------
' Poiche il twist realmente corretto risulata minore di quello nominale
' si intende profilare le zone ingresso ed uscita della zona AT di mola.
'---------------------------------------------------------------------------
  Dim i%, j%
  Dim iFHA_TW_CORR(1 To 2) As Double: Dim iFHA_TW(1 To 2) As Double: Dim FHA_TW(1 To 2) As Double: Dim FHA_TW_f(1 To 2) As Double
  Dim iLNG_TW_CORR(1 To 2) As Double: Dim iLNG_TW(1 To 2) As Double: Dim LNG_TW(1 To 2) As Double
  Dim AT_Dx#, AT_Dz#, AT_Dy#
  Dim word As String
'--------------------------------------------------------------------------
  Dim CorrEvolv(1 To 3) As Double:
  Dim iBomb(1 To 2) As Double
  Dim Nf%, Lfw#
'--------------------------------------------------------------------------
  Dim BOE_IN#, BOE_OU#, FW_GEAR#, FW_INSPC#, FW_IN#, FW_OU#, UNIT#, Crown#
  Dim Freeze(1 To 2) As Boolean
'--------------------------------------------------------------------------
  Dim FASCIA#, coefTW#, FASCIAPARZIALE#
'--------------------------------------------------------------------------
  Dim HMI_ADVANCED As Boolean
  Dim side(1 To 2) As String

On Error GoTo Err_DB_GUD_AT_Aggiorna

'--------------------------------------------------------------------------
  GoTo 20161228:
'--------------------------------------------------------------------------
  Profila_ZonaAT_Mola_INOUT = False
  HMI_ADVANCED = False
  side(1) = "sx": side(2) = "dx"
'--------------------------------------------------------------------------
  FASCIA = Lp("FASCIATOTALE_G")
 '--------------------------------------------------------------------------
  For i = 1 To 2
   Nf = Lp("iNZone")
   iBomb(i) = 0
   For j = 1 To Nf
    If (HMI_ADVANCED) Then FASCIAPARZIALE = Lp("iFas" & j & "F" & i) Else FASCIAPARZIALE = Lp("iFas" & j & "F" & side(i))
    If (FASCIA <> 0) Then coefTW = (FASCIAPARZIALE / FASCIA) Else coefTW = 1
    If (HMI_ADVANCED) Then iBomb(i) = iBomb(i) + Lp("iBom" & j & "F" & i) * coefTW Else iBomb(i) = iBomb(i) + Lp("iBom" & j & "F" & side(i)) * coefTW
   Next j
   iBomb(i) = iBomb(i) + Lp("CRW_TW[0," & i - 1 & "]")
  Next i
'--------------------------------------------------------------------------
  If (Profila_ZonaAT_Mola_INOUT) Then
   Nf = Lp("iNZone")
   FW_GEAR = 0
   For i = 1 To Nf
    If (HMI_ADVANCED) Then FW_GEAR = FW_GEAR + (Lp("iFas" & i & "F1") + Lp("iFas" & i & "F2")) / 2# Else FW_GEAR = FW_GEAR + (Lp("iFas" & i & "F" & side(1)) + Lp("iFas" & i & "F" & side(2))) / 2#
   Next i
   FW_GEAR = FW_GEAR / Nf
    BOE_IN = iBomb(1): BOE_OU = iBomb(2)
    
  FW_INSPC = Lp("FASCIAFHB_G")
     FW_IN = LEP("CORSAIN")
     FW_OU = LEP("CORSAOUT")
      UNIT = 1
   Call PRF_VIT_BARRELING_F(BOE_IN, BOE_OU, FW_GEAR, FW_INSPC, FW_IN, FW_OU, UNIT, Crown)
   iBomb(1) = Crown
   iBomb(2) = Crown
  End If
'-------------------------------------------------------------------------
  Call CalcoloSvergolamento(iBomb(1), iBomb(2), CorrEvolv)
  For i = 1 To 2
'--------------------------------------------------------------------------
    Freeze(i) = (Lp("FHA_TW_freeze[0," & i - 1 & "]") = 1)
  FHA_TW_f(i) = Lp("FHA_TW_f[0," & i - 1 & "]")
  'MsgBox "Freeze " & i & " (" & Freeze(i) & ") in Sub: DB_GUD_AT_Aggiorna"
'--------------------------------------------------------------------------
   If (Freeze(i)) Then
    iFHA_TW(i) = CorrEvolv(i) / 1000
    iFHA_TW_CORR(i) = FHA_TW_f(i) - iFHA_TW(i)
    Call SP("FHA_TW_c[0," & i - 1 & "]", Round(iFHA_TW_CORR(i), 4))
   Else
    iFHA_TW_CORR(i) = Lp("FHA_TW_c[0," & i - 1 & "]") 'iFHA_TW_CORR(i) = Lp("iFHA_TW_F" & i & "_CORR")
    iFHA_TW(i) = CorrEvolv(i) / 1000#
   End If
'MsgBox "FHA_TW_f " & i & " (" & FHA_TW_f(i) & ") in Sub: DB_GUD_AT_Aggiorna"
  Next i
'-------------------------------------------------------------------------
  For i = 1 To 2
'--------------------------------------------------------------------------
   ' (1) leggo le correzioni utente
    iFHA_TW_CORR(i) = Lp("FHA_TW_c[0," & i - 1 & "]") 'iFHA_TW_CORR(i) = Lp("iFHA_TW_F" & i & "_CORR")
   ' Considero ! lunghezza (profilatura 2 fianchi alla volta prioritaria)
   iLNG_TW_CORR(i) = Lp("iLNG_TW_F1_CORR") ' iLNG_TW_CORR(i) = Lp("iLNG_TW_F" & i & "_CORR")
   ' (2) calcolo le correzioni normali assi (obsoleto) ora solo per lunghezza mola
    Call CalcoloMovimentiXZ2(AT_Dx, AT_Dz, iLNG_TW(i), AT_Dy, iFHA_TW_CORR(i), iFHA_TW(i))
   ' (3) scrivo le correzioni normali
    Call SP("iLNG_TW_F" & i, Round(iLNG_TW(i), 4))
    Call SP("FHA_TW_n[0," & i - 1 & "]", Round(iFHA_TW(i), 4)) 'Call SP("iFHA_TW_F" & i, Round(iFHA_TW(i), 4))
   ' (4) calcolo le correzioni somma
     LNG_TW(i) = iLNG_TW(i) + iLNG_TW_CORR(i)
     FHA_TW(i) = iFHA_TW(i) + iFHA_TW_CORR(i)
   ' (5) scrivo le correzioni somma
    Call SP("LNG_TW[0," & i - 1 & "]", Round(LNG_TW(i), 4)) '_F
    Call SP("FHA_TW[0," & i - 1 & "]", Round(FHA_TW(i), 4)) '_F
    ' Cache for freeze
    If (Not Freeze(i)) Then Call SP("FHA_TW_f[0," & i - 1 & "]", Round(FHA_TW(i), 4))
'--------------------------------------------------------------------------
    Next i
'--------------------------------------------------------------------------
20161228:
 Dim CORR_TW_KIND%, BoE_N#, BoE_M#, LatoPA_aZ%, DIRSHIFT_aY%, SensoElica%
 Dim iTW_TUNING#, iTW_TUNING_corr#, TW_TUNING#
 Dim OOC(0 To 2, 0 To 2) As Double
 Dim SPK(0 To 2, 0 To 2) As Double
 Dim OOI(0 To 2, 0 To 2) As Double
'--------------------------------------------------------------------------
 BoE_N = Lp("BoE")
'--------------------------------------------------------------------------

 CORR_TW_KIND = Lp("CORR_TW_KIND")
 If (CORR_TW_KIND = 0) Then
  Call SP("AT_DRESSING", 0)
 Else
  Call SP("AT_DRESSING", 1)
 End If
'--------------------------------------------------------------------------
'Call SP("CORR_TW_KIND", CORR_TW_KIND)
'--------------------------------------------------------------------------
 Select Case (CORR_TW_KIND)
 Case (0) ' DISABLE
  iTW_TUNING = 0#: iTW_TUNING_corr = 0#: TW_TUNING = iTW_TUNING + iTW_TUNING_corr
  For i = 0 To 2: For j = 0 To 2: SPK(i, j) = 0#: Next j: Next i
  For i = 0 To 2: For j = 0 To 2: OOI(i, j) = 0#: Next j: Next i
  For i = 0 To 2: For j = 0 To 2: OOC(i, j) = 0#: Next j: Next i
  For i = 0 To 2: For j = 1 To 2: Call SP("OOC_TW[" & i & "," & j & "]", Round(OOC(i, j), 4)): Next j: Next i
 Case (1) ' TWIST FREE
  iTW_TUNING = 0.43: iTW_TUNING_corr = Lp("iTW_TUNING_corr"): TW_TUNING = iTW_TUNING + iTW_TUNING_corr
  For i = 0 To 2: For j = 0 To 2: SPK(i, j) = 0#: Next j: Next i
  For i = 0 To 2: For j = 0 To 2: OOI(i, j) = 0#: Next j: Next i
  For i = 0 To 2: For j = 0 To 2: OOC(i, j) = 0#: Next j: Next i
  For i = 0 To 2: For j = 1 To 2: Call SP("OOC_TW[" & i & "," & j & "]", Round(OOC(i, j), 4)): Next j: Next i
 Case (2) ' OPPOSTO AL NATURALE
  iTW_TUNING = 1#: iTW_TUNING_corr = Lp("iTW_TUNING_corr"): TW_TUNING = iTW_TUNING + iTW_TUNING_corr
  For i = 0 To 2: For j = 0 To 2: SPK(i, j) = 0#: Next j: Next i
  For i = 0 To 2: For j = 0 To 2: OOI(i, j) = 0#: Next j: Next i
  For i = 0 To 2: For j = 0 To 2: OOC(i, j) = 0#: Next j: Next i
  For i = 0 To 2: For j = 1 To 2: Call SP("OOC_TW[" & i & "," & j & "]", Round(OOC(i, j), 4)): Next j: Next i
 Case (3) ' Simmetrico
   If (CORR_TW_KIND0 = 3) Then
    iTW_TUNING = Lp("iTW_TUNING"): iTW_TUNING_corr = Lp("iTW_TUNING_corr"): TW_TUNING = iTW_TUNING + iTW_TUNING_corr
   Else
    iTW_TUNING = 0.43: iTW_TUNING_corr = 0: TW_TUNING = iTW_TUNING + iTW_TUNING_corr
   End If
'-------------------------------------------------------------
  SPK(0, 1) = Lp("SPC_TW[" & "0" & "," & "1" & "]")
  OOI(0, 1) = Lp("OOI_TW[" & "0" & "," & "1" & "]")
  OOC(0, 1) = SPK(0, 1) + OOI(0, 1)
'-------------------------------------------------------------OUT OF CENTER
                                                          Call SP("OOI_TW[0," & "2" & "]", Round(-OOI(0, 1), 4))
                     Call SP("OOI_TW[1," & "1" & "]", 0): Call SP("OOI_TW[1," & "2" & "]", 0)
  Call SP("OOI_TW[2," & "1" & "]", Round(-OOI(0, 1), 4)): Call SP("OOI_TW[2," & "2" & "]", Round(OOI(0, 1), 4))
'------------------------------------------------------------- OUT OF CENTER
   Call SP("OOC_TW[0," & "1" & "]", Round(OOC(0, 1), 4)): Call SP("OOC_TW[0," & "2" & "]", Round(-OOC(0, 1), 4))
                     Call SP("OOC_TW[1," & "1" & "]", 0): Call SP("OOC_TW[1," & "2" & "]", 0)
  Call SP("OOC_TW[2," & "1" & "]", Round(-OOC(0, 1), 4)): Call SP("OOC_TW[2," & "2" & "]", Round(OOC(0, 1), 4))
'------------------------------------------------------------- SIMMETRIZZO
  For i = 0 To 2: For j = 1 To 2: OOC(i, j) = Lp("OOC_TW[" & i & "," & j & "]"): Next j: Next i
'-------------------------------------------------------------
 Case Else
  MsgBox "Error " & "0" & " (" & "CORR_TW_KIND Out Of Range" & ") in Sub: DB_GUD_AT_Aggiorna"
 End Select
'-------------------------------------------------------------
 CORR_TW_KIND0 = CORR_TW_KIND
'-------------------------------------------------------------
 SPK(0, 1) = Lp("SPC_TW[" & "0" & "," & "1" & "]")
'------------------------------------------------------------- Simmetria input RUOTA
   Call SP("SPC_TW[0," & "1" & "]", Round(SPK(0, 1), 4)): Call SP("SPC_TW[0," & "2" & "]", Round(-SPK(0, 1), 4))
                     Call SP("SPC_TW[1," & "1" & "]", 0): Call SP("SPC_TW[1," & "2" & "]", 0)
  Call SP("SPC_TW[2," & "1" & "]", Round(-SPK(0, 1), 4)): Call SP("SPC_TW[2," & "2" & "]", Round(SPK(0, 1), 4))
'------------------------------------------------------------- PUNTI FSCIA
 For i = 0 To 2: Call SP("OOC_TW[" & i & ",0]", Lp("SPC_TW[" & i & ",0]")): Next i
 For i = 0 To 2: Call SP("OOI_TW[" & i & ",0]", Lp("SPC_TW[" & i & ",0]")): Next i
'-----------------------------------------------------------------BARRELING
    BoE_M = TW_TUNING * BoE_N
 Call SP("TW_TUNING", TW_TUNING): Call SP("iTW_TUNING", iTW_TUNING) ': Call SP("iTW_TUNING", iTW_TUNING_corr)
 For i = 1 To 2: Call SP("CRW_TW[0," & i - 1 & "]", Round(BoE_M, 8)): Next i
'----------------------------------------------------------------SEGNI ASSI
 If (TW_TUNING <> 0 And Lp("AT_DRESSING") = 1) Then
  LatoPA_aZ = Lp("LatoPA") ' (-1,+1) = (Basso, Alto)
 SensoElica = Lp("iSensoElica")
'DIRSHIFT_aY = Lp("DIRSHIFT") ' (-1,+1) = (Negativa, Positiva)
  Call SP("DIRSHIFT", -SensoElica * LatoPA_aZ) ' Call SP("DIRSHIFT", LatoPA_aZ) ORIGINALE TEST PER "SX"
 End If
'-------------------------------------------------------------OUT OF CENTER
 Call SP("FHA_TW[0," & "0" & "]", Round(OOC(0, 1), 4))
 Call SP("FHA_TW[0," & "1" & "]", Round(-OOC(0, 2), 4))
'-------------------------------------------------------------WORM LENGHT
  For i = 1 To 2
'--------------------------------------------------------------------------
   ' (1) Considero ! lunghezza (profilatura 2 fianchi alla volta prioritaria)
   iLNG_TW_CORR(i) = Lp("iLNG_TW_F1_CORR") ' iLNG_TW_CORR(i) = Lp("iLNG_TW_F" & i & "_CORR")
   ' (2) calcolo le correzioni normali assi (obsoleto) ora solo per lunghezza mola
    Call CalcoloMovimentiXZ2(AT_Dx, AT_Dz, iLNG_TW(i), AT_Dy, iFHA_TW_CORR(i), iFHA_TW(i))
   ' (3) scrivo le correzioni normali
    Call SP("iLNG_TW_F" & i, Round(iLNG_TW(i), 4))
   ' (4) calcolo le correzioni somma
     LNG_TW(i) = iLNG_TW(i) + iLNG_TW_CORR(i)
   ' (5) scrivo le correzioni somma
    Call SP("LNG_TW[0," & i - 1 & "]", Round(LNG_TW(i), 4)) '_F
    ' Cache for freeze
'--------------------------------------------------------------------------
 Next i
 
Exit Sub

Err_DB_GUD_AT_Aggiorna:
  MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Sub: Molavite02.DB_GUD_AT_Aggiorna"

 Exit Sub
 
End Sub
'--------------------------------------------------------------
' Antitwist
'--------------------------------------------------------------
Public Sub Calcolo_Parametri_AT()
'
Dim INCLINAZIONE_MOLA_GRD As Double
Dim INCLINAZIONE_MOLA_RAD As Double
Dim MODULO_MOLA           As Double
Dim PRINCIPI_MOLA         As Double
Dim DIAMETRO_MOLA         As Double
Dim ARGOMENTO             As Double
Dim DIAMETRO_MOLA_EXT_EFF As Double ' DIametro mola esterno durante la rettifica
Dim DIAMETRO_MOLA_INT_EFF As Double ' Diametro mola di fondo usato durante la rettifica
Dim DIAMETRO_MOLA_MED_EFF As Double ' Diametro mola medio usato durante la rettifica
Dim PASSO_ASSIALE_MOLA    As Double
Dim INCOMINGS(1 To 21)
Dim AT_Dx                 As Double
Dim AT_Dz                 As Double
Dim AT_DL                 As Double
Dim AT_Dy                 As Double
Dim iAT_DL                As Double
Dim iAT_DL_CORR           As Double
Dim AT_DAP#, iAT_DAP#, iAT_DAP_CORR#
'
On Error GoTo errCalcolo_Parametri_AT
  '
  MODULO_MOLA = Lp("MNUT_G")       'MnUtensileMV
  PRINCIPI_MOLA = Lp("NUMPRINC_G") 'NPrincipiMV
  DIAMETRO_MOLA = Lp("DIAMMOLA_G") 'Diametro Mola Nuova
  If (DIAMETRO_MOLA <= 0) Then
     DIAMETRO_MOLA = 250 ' Leggere da file INI
     Call SP("DIAMMOLA_G", DIAMETRO_MOLA)
  End If
  '
  DIAMETRO_MOLA_EXT_EFF = 2 * val(OPC_LEGGI_DATO("/ACC/NCK/GUD7/REXTMOLA_G"))
  If (((DIAMETRO_MOLA_EXT_EFF <= 0) And (DIAMETRO_MOLA > 0)) Or (DIAMETRO_MOLA_EXT_EFF > DIAMETRO_MOLA)) Then
    DIAMETRO_MOLA_EXT_EFF = DIAMETRO_MOLA
  End If
  '
  DIAMETRO_MOLA_MED_EFF = DIAMETRO_MOLA_EXT_EFF - 2 * MODULO_MOLA
  '
  'CALCOLO INCLINAZIONE FILETTO
  ARGOMENTO = MODULO_MOLA * PRINCIPI_MOLA / DIAMETRO_MOLA_MED_EFF
  INCLINAZIONE_MOLA_RAD = Atn(ARGOMENTO / Sqr(1 - ARGOMENTO * ARGOMENTO))  'RADIANTI
  INCLINAZIONE_MOLA_GRD = INCLINAZIONE_MOLA_RAD / PG * 180                 'GRADI
  '
  'CALCOLO PASSO ASSIALE
  PASSO_ASSIALE_MOLA = MODULO_MOLA * PRINCIPI_MOLA * PG / Cos(INCLINAZIONE_MOLA_RAD)
'--------------------------------------------------------------
  If (Lp("BETA_G") <> 0) Then '(Lp("AT_DRESSING") <> 0) And (Lp("BETA_G") <> 0)
'--------------------------------------------------------------
   Dim Nf%, Lfw#, FASCIA#, FASCIAPARZIALE#, coefTW#
   Dim HMI_ADVANCED As Boolean
   Dim BOE_INPUT As Boolean
   Dim side(1 To 2) As String
'--------------------------------------------------------------
   HMI_ADVANCED = False
   BOE_INPUT = True
   side(1) = "sx": side(2) = "dx"
'--------------------------------------------------------------
     Nf = Lp("iNZone")
     FASCIA = Lp("FASCIATOTALE_G")
'-------------------------------------------------------- RULLO
     INCOMINGS(1) = Lp("DRULLOPROF_G")
     INCOMINGS(2) = INCLINAZIONE_MOLA_GRD
'---------------------------------------------------- Mola VITE
     INCOMINGS(3) = DIAMETRO_MOLA_EXT_EFF
     INCOMINGS(4) = INCLINAZIONE_MOLA_GRD
     INCOMINGS(5) = PRINCIPI_MOLA
     INCOMINGS(6) = PASSO_ASSIALE_MOLA
     INCOMINGS(7) = Lp("ALFAUT_G")
     INCOMINGS(8) = MODULO_MOLA
'------------------------------------------------------ DISEGNO
     INCOMINGS(9) = Lp("ALFA_G")
     INCOMINGS(10) = Lp("BETA_G")
     INCOMINGS(11) = Lp("MN_G")
     INCOMINGS(12) = Lp("MN_G")
     INCOMINGS(13) = Lp("NumDenti_G")
     INCOMINGS(14) = Lp("QW")
     INCOMINGS(15) = Lp("ZW")
     INCOMINGS(16) = Lp("DEXT_G")
     INCOMINGS(17) = Lp("DINT_G")
     INCOMINGS(18) = Lp("DSAP_G")
     INCOMINGS(21) = Lp("FASCIAFHB_G")
     INCOMINGS(19) = 0#
     INCOMINGS(20) = 0#
'--------------------------------------------------------------
   If (BOE_INPUT) Then
   coefTW = Lp("iFASCIALB_G") / 100
   INCOMINGS(19) = FASCIA * coefTW
   INCOMINGS(20) = Lp("BoE")
   Else
     For i = 1 To Nf
      If (HMI_ADVANCED) Then FASCIAPARZIALE = (Lp("iFas" & i & "F1") + Lp("iFas" & i & "F2")) / 2# Else FASCIAPARZIALE = (Lp("iFas" & i & "F" & side(1)) + Lp("iFas" & i & "F" & side(2))) / 2#
      If (FASCIA <> 0) Then coefTW = (FASCIAPARZIALE / FASCIA) Else coefTW = 1
      If (HMI_ADVANCED) Then INCOMINGS(19) = INCOMINGS(19) + (Lp("iFas" & i & "F1") + Lp("iFas" & i & "F2")) / 2# Else INCOMINGS(19) = INCOMINGS(19) + (Lp("iFas" & i & "F" & side(1)) + Lp("iFas" & i & "F" & side(2))) / 2#
      If (HMI_ADVANCED) Then INCOMINGS(20) = INCOMINGS(20) + coefTW * (Lp("iBom" & i & "F1") + Lp("iBom" & i & "F2")) / 2# Else INCOMINGS(20) = INCOMINGS(20) + coefTW * (Lp("iBom" & i & "F" & side(1)) + Lp("iBom" & i & "F" & side(2))) / 2#
     Next i
'--------------------------------------------------------------
    INCOMINGS(20) = INCOMINGS(20) + (Lp("CRW_TW[0,0]") + Lp("CRW_TW[0,1]")) / 2
   End If
'--------------------------------------------------------------
    Call Fill_AntiTwist_MV_paramenter(INCOMINGS)
'--------------------------------------------------------------
    Call DB_GUD_AT_Aggiorna
'--------------------------------------------------------------
  Else
'--------------------------------------------------------------
    Call DB_GUD_AT_Azzera
'--------------------------------------------------------------
  End If
'--------------------------------------------------------------
  On Error GoTo 0
  Exit Sub
'--------------------------------------------------------------
errCalcolo_Parametri_AT:
Dim errNum         As Long
Dim errDescription As Long
  '
  errNum = Err.Number
  errDescription = Err.Description
  '
  StopRegieEvents
  MsgBox "Error " & errNum & " (" & errDescription & ") in Sub: Calcolo_Parametri_AT"
  ResumeRegieEvents
  
  Exit Sub

End Sub

Private Function Acs(Aux)

Dim Aux1
   
   Aux1 = (1 - Aux * Aux)
   If Aux1 < 0 Then
   Rem   MsgBox Parola("1", TabUtente$), 16 ' "Coseno > 1 : Il programma si ferma : Controllare i dati"
 '   MsgBox ("Coseno > 1 : Il programma si ferma : Controllare i dati")
    Write_Dialog "ERROR in MOLAVITE02 : Cos > 1 " '& Err
    Acs = 0.1  'End
    'Exit Function
    '  End
   Else
      If Aux = 0 Then
         Acs = PI / 2
      Else
         If Aux > 0 Then
            Acs = Atn(Sqr(1 - Aux * Aux) / Aux)
         Else
            Aux = Abs(Aux)
            Acs = Atn(Aux / Sqr(1 - Aux * Aux)) + 1.570796
         End If
      End If
   End If

End Function

Private Function Asn(Aux)
        Dim Aux1
  Aux1 = (1 - Aux * Aux)
  If Aux1 < 0 Then
    Rem MsgBox Parola("2", TabUtente$), 16 ' "Seno > 1 : Il programma si ferma : Controllare i dati"
  '   MsgBox ("Seno > 1 : Il programma si ferma : Controllare i dati")
     Write_Dialog "ERROR in MOLAVITE02 : Seno > 1" '& Err
     Asn = 0.2
 '    Exit Function 'End
  Else
     If Aux = 1 Then
        Asn = PI / 2
     Else
        Asn = Atn(Aux / Sqr(Aux1))
     End If
  End If
End Function


Private Function Fnrd(A)
   
   Fnrd = A * 180 / PI

End Function

Private Function Fndr(A)
   
   Fndr = A * PI / 180

End Function

Private Function inv(Aux)

  inv = Tan(Aux) - Aux

End Function

Private Function Ainv(Aux)

Dim i, Ymini, Ymaxi, Ymoyen, X1
    
    Ymini = 0: Ymaxi = 1.570796
    For i = 1 To 25
       Ymoyen = (Ymini + Ymaxi) / 2: X1 = inv(Ymoyen)
       If X1 < Aux Then Ymini = Ymoyen Else: Ymaxi = Ymoyen
    Next i
    Ainv = Ymoyen
    
End Function

Private Function SpessSurDiam(Eb, db, dx)

Dim Aux
   
   If dx < db Then
   Rem   MsgBox Parola("24", TabUtente$), 16 ' "Errore in 'SpessSurDiam' : Il programma si ferma : Controllare i dati"
  '    MsgBox ("Errore in 'SpessSurDiam' : Il programma si ferma : Controllare i dati")
    SpessSurDiam = 0
      Write_Dialog "ERROR in MOLAVITE02 : Can't measure thickness above base diameter" '& Err
      Exit Function
   Else
      Aux = Acs(db / dx)
      SpessSurDiam = (Eb / db - inv(Aux)) * dx
   End If

End Function

Private Function DcroisDev(Eb3, DB3, Eb2, DB2)

Dim D1, E1, D2, E2
Dim PAS, Dc, Ec1, EC2
   
   If DB3 > DB2 Then
      D1 = DB3: E1 = Eb3: D2 = DB2: E2 = Eb2
   Else
      D1 = DB2: E1 = Eb2: D2 = DB3: E2 = Eb3
   End If
   PAS = 1: Dc = D1 - PAS
3110
   Dc = Dc + PAS
   Ec1 = (E1 / D1 - inv(Acs(D1 / Dc))) * Dc
   EC2 = ((E2 / D2) - inv(Acs(D2 / Dc))) * Dc
   If EC2 > Ec1 Then GoTo 3110
   If Dc = D1 Then DcroisDev = 0: Exit Function
   Dc = Dc - PAS: PAS = PAS / 10
   If PAS > 0.00001 Then GoTo 3110
   DcroisDev = Dc
   '   " EP.CIRC.SUR CE DIAMETRE :"; EC1
End Function

Private Sub Devel2diametri(E1, E2, D1, D2, db, Eb)

' E1 ; E2 : Epaisseurs circulaires sur diametre D1 ; D2
Dim Aux, Emn, Dmn, Emx, Dmx, PAS, Amn, Amx, X

   If D2 > D1 Then
      Emn = E1: Emx = E2: Dmn = D1: Dmx = D2
   Else
      Emn = E2: Emx = E1: Dmn = D2: Dmx = D1
   End If
   Amn = Emn / Dmn: Amx = Emx / Dmx
   PAS = 1: db = Dmn - PAS
2121
   X = -1: db = db + PAS: PAS = PAS / 10
   Do While X < 0
     db = db - PAS
     Aux = Amx + inv(Acs(db / Dmx))
     X = (Aux - inv(Acs(db / Dmn))) - Amn
   Loop
   If Abs(X) < 0.000001 Then
      Eb = Aux * db
      Exit Sub
   End If
   If db = Dmn Then
      db = 0
      Exit Sub
   End If
   GoTo 2121

End Sub

Private Sub CalcPuntoIngDaMf(Ent, INC, PAS, Rayon, ALFA, Beta, Eaxial, EXF, RXF, APF, Ltang)
    
Dim SENS As Integer, Aux As Double
Dim V7 As Double, V12 As Double, V13 As Double, V14 As Double, V15 As Double, V16 As Double
Dim X1 As Double, Y1 As Double, ang1 As Double
Dim XAPP As Double, YAPP As Double, Helx As Double
Dim ApAxial As Single
      
   If PAS = 0 Then
        Ltang = 0
        ALFA = Atn(EXF / (Ent - RXF))
        Rayon = (Ent - RXF) / Cos(ALFA)
        Beta = APF - ALFA
        Exit Sub
   End If
   SENS = 1: ' If Exf < 0 Then Sens = -1
   EXF = EXF * SENS: APF = APF * SENS: V7 = PAS / 2 / PI
  
   RXF = RXF + 0.02 * Cos(APF): EXF = EXF - 0.02 * Sin(APF): GoSub 2050
   Y1 = YAPP: X1 = XAPP
   RXF = RXF - 0.04 * Cos(APF): EXF = EXF + 0.04 * Sin(APF): GoSub 2050
   If Abs(YAPP - Y1) > 0.000001 Then
      ang1 = Atn((X1 - XAPP) / (YAPP - Y1))
   Else
      ang1 = PI / 2
   End If
   RXF = RXF + 0.02 * Cos(APF): EXF = EXF - 0.02 * Sin(APF)
   GoSub 2050
   Beta = (ang1 - ALFA) * SENS
   ALFA = ALFA * SENS
   Helx = Atn(PAS / PI / Rayon / 2)
   ApAxial = Atn(Tan(Beta) * Tan(Helx))
   Eaxial = Eaxial * SENS
        Exit Sub
2050
     V12 = -Tan(APF) * (Ent * Sin(INC) + V7 * Cos(INC))
     V13 = V7 * Sin(INC) - Ent * Cos(INC)
     V14 = Cos(INC) * ((-Tan(APF) * EXF) + RXF)
     V15 = (V14 ^ 2) + (V12 ^ 2) - (V13 ^ 2)
     If V15 > 0 Then
         V15 = Sqr(V15)
         V16 = -(V15 + V12) / (V13 - V14)
         V15 = (V15 - V12) / (V13 - V14)
         Aux = V16: If Abs(V15) < Abs(V16) Then Aux = V15
         V12 = 2 * Atn(Aux)
         V13 = (RXF * Sin(INC) * Sin(V12)) + EXF * Cos(INC)
         V13 = Atn(V13 / ((RXF * Cos(V12)) - Ent))
         Rayon = (Ent - RXF * Cos(V12)) / Cos(V13)
         Ltang = EXF * Sin(INC) - RXF * Sin(V12) * Cos(INC)
         ' Ltang : Distance axe meule / plan tangent ( type Programme 'JPG' )
         Eaxial = Ltang - V7 * V13
         ALFA = 2 * PI * Eaxial / PAS
         YAPP = Rayon * Cos(ALFA): XAPP = -Rayon * Sin(ALFA)
    Else
       'CALC$ = "IMPOSS"
       Rayon = 999: Exit Sub
    End If
Return
End Sub

'--------------------------------------
' quota rulli
'--------------------------------------
Private Sub CotePiges(Dpige, Z, Dbx, Helbx, Eb, Kpig, Code$)
  
' Code$ = "E": Calcul de Eb
' Code$ = "K": Calcul de Wpg
Dim Aux, Pba, W, We, X
  
  Pba = PI * Dbx / Z
  If Int(Z / 2) = Z / 2 Then W = Dbx Else W = Cos(PI / (2 * Z)) * Dbx
  If Dpige = 0 Then Kpig = 0: Exit Sub
  If Kpig = 0 And Code$ = "E" Then Exit Sub
  If Code$ = "K" Then
     We = Eb - Pba
     X = (We + Dpige / Cos(Helbx)) / Dbx
     Kpig = W / Cos(Ainv(X)) + Dpige
  Else
     Aux = Acs(W / (Kpig - Dpige))
     Eb = Dbx * inv(Aux) - Dpige / Cos(Helbx) + Pba
  End If

End Sub

'--------------------------------------
' Lunghezza vs angolo di rotolamento
'--------------------------------------
Private Sub LdevAdev(db, dx, Ldev, Adev, Entita)

Dim Aux
   
   If dx > db Then
      ' Connu : Dx
      Ldev = Sqr((dx / 2) ^ 2 - (db / 2) ^ 2)
      Adev = Ldev / (db / 2)
   Else
      If Ldev > 0 Then
         ' Connu : Ldev
         dx = 2 * Sqr((db / 2) ^ 2 + Ldev ^ 2)
         Adev = Ldev / (db / 2)
      Else
         ' Connu : Adev
         Ldev = Adev * (db / 2)
         dx = 2 * Sqr((db / 2) ^ 2 + Ldev ^ 2)
      End If
   End If
   Par1$ = " " & Phi$ & Format(dx, "0.00")
   If Entita <> 0 Then Par1$ = Par1$ & " E=" & Format(Entita, "0.000")
   PAR$ = " L=" & Format(Ldev, "0.00") & "  A=" & Format(Fnrd(Adev), "0.00") & "�"

End Sub

Private Sub CalcoloIngranaggioPiuFm(NE)

Dim Aux, MappFm, DemiPas
Dim DR, Ap, ApAppSt, Dst, EpStFm

    Help(NE) = Fndr(HelpD(NE)): Apr = Fndr(Aprd)
    Mapp(NE) = Mr / Cos(Help(NE)): Dp1(NE) = Mapp(NE) * Z1(NE)
    ApApp(NE) = Atn(Tan(Apr) / Cos(Help(NE)))
    DB1(NE) = Dp1(NE) * Cos(ApApp(NE))
    HELB(NE) = Atn(Tan(Help(NE)) * Cos(ApApp(NE)))
    If Z1(NE) < 2 Or Aprd < 0 Or Mr < 0.1 Then Exit Sub
        
    If Aprd < 5 Then
        If Di1(NE) > DB1(NE) Then Aux = Di1(NE) Else Aux = DB1(NE)
        DR = (DE1(NE) + Aux) / 2
        Ap = Acs(DB1(NE) / DR)
        InMolaFdDefault(NE) = Int(Fnrd(Atn(Tan(HELB(NE)) * DR / DB1(NE))) + 1)
        If NE = 1 Then
            AprDdefault = Int(Fnrd(Atn(Tan(Ap) * Cos(Fndr(InMolaFdDefault(NE))))) + 1)
        End If
        Aux = (DB1(NE) * Cos(HELB(NE))) / Sqr(Cos(Fndr(AprDdefault) + HELB(NE)) * Cos(Fndr(AprDdefault) - HELB(NE)))
        ApAppDdefault(NE) = Acs(DB1(NE) / Aux)
    Else
        AprDdefault = Aprd
        InMolaFdDefault(NE) = HelpD(NE)
        ApAppDdefault(NE) = ApApp(NE)
    End If
    '                           *** calcul ep. base
    If Wdt(NE) <> 0 Then
       Aux = PI * DB1(NE) / Z1(NE)
       FIN.Eb1(NE) = (Wdt(NE) / Cos(HELB(NE))) - (Kdt(NE) - 1) * Aux
    End If
    If Wpg(NE) <> 0 Then
       Call CotePiges(DPG(NE), Z1(NE), DB1(NE), HELB(NE), FIN.Eb1(NE), Wpg(NE), "E")
    End If
    If Epr1(NE) <> 0 Then
       Aux = Epr1(NE) / Cos(Help(NE))
       FIN.Eb1(NE) = (Aux / Dp1(NE) + inv(ApApp(NE))) * DB1(NE)
    End If
    Eb1Dent(NE) = FIN.Eb1(NE) + SovramDent(NE) * 2 / Cos(HELB(NE))
    Sgr.Eb1(NE) = FIN.Eb1(NE) + SovramMv(NE) * 2 / Cos(HELB(NE))
    Ing.Eb1(NE) = FIN.Eb1(NE)
    
    '                 *** calcul cotes de controle
   
   Epr1(NE) = ((FIN.Eb1(NE) / DB1(NE)) - inv(ApApp(NE))) * Dp1(NE) * Cos(Help(NE))
   Epr1Dent(NE) = ((Eb1Dent(NE) / DB1(NE)) - inv(ApApp(NE))) * Dp1(NE) * Cos(Help(NE))
   If Apr > 0 Then
      Aux = (Epr1(NE) - (PI * Mr / 2)) / (2 * Tan(Apr))
   End If
   Kdep(NE) = Aux / Mr
   If Abs(Kdep(NE)) < 0.0001 Then
      Kdep(NE) = 0
   End If
   If Kdt(NE) > 0 Then
      Aux = PI * DB1(NE) / Z1(NE)
      Wdt(NE) = ((Kdt(NE) - 1) * Aux + FIN.Eb1(NE)) * Cos(HELB(NE))
      WdtDent(NE) = ((Kdt(NE) - 1) * Aux + Eb1Dent(NE)) * Cos(HELB(NE))
   End If
   If DPG(NE) <> 0 Then
      Call CotePiges(DPG(NE), Z1(NE), DB1(NE), HELB(NE), FIN.Eb1(NE), Wpg(NE), "K")
      Call CotePiges(DPG(NE), Z1(NE), DB1(NE), HELB(NE), Eb1Dent(NE), WpgDent(NE), "K")
   End If
   If DE1(NE) > DB1(NE) Then Es1(NE) = SpessSurDiam(FIN.Eb1(NE), DB1(NE), DE1(NE))
   If Di1(NE) < DB1(NE) Then
      Aux = 0
   Else
      Aux = Acs(DB1(NE) / Di1(NE))
   End If
   Ic1(NE) = (PI / Z1(NE) - FIN.Eb1(NE) / DB1(NE) + inv(Aux)) * Di1(NE)
      
   ' Calcul Fraise-mere taillage
' Aprd,Helpd(Ne),Mr,Z1(Ne),Di1(Ne),De1(Ne),Wdt(Ne),Kdt(Ne),SovramDent(Ne),SovramMv(Ne),ApFmD(Ne),ApProtuD(Ne),ApStD(Ne)
    If ApFmD(NE) < 5 Then Exit Sub
    
    ApFm(NE) = Fndr(ApFmD(NE))
    ApProtu(NE) = Fndr(ApProtuD(NE))
    DrFm(NE) = (DB1(NE) * Cos(HELB(NE))) / Sqr(Cos(ApFm(NE) + HELB(NE)) * Cos(ApFm(NE) - HELB(NE)))
    HelrFm(NE) = Atn(Tan(HELB(NE)) * DrFm(NE) / DB1(NE))
    MappFm = DrFm(NE) / Z1(NE)
    MrFm(NE) = MappFm * Cos(HelrFm(NE))
    Aux = SpessSurDiam(Eb1Dent(NE), DB1(NE), DrFm(NE))
    EpFm(NE) = (PI * MappFm - Aux) * Cos(HelrFm(NE))
    Aux = (DrFm(NE) - Di1(NE)) / 2
    DemiPas = PI * MrFm(NE) / 2
    AddFmTh(NE) = Aux + (DemiPas - EpFm(NE)) / 2 / Tan(ApFm(NE))
    If ApStD(NE) < ApFmD(NE) + 0.1 Then ApStD(NE) = ApFmD(NE) / 2 + 45
    ApSt(NE) = Fndr(ApStD(NE))
    ApAppSt = Atn(Tan(ApSt(NE)) / Cos(HelrFm(NE)))
    DbSt(NE) = DrFm(NE) * Cos(ApAppSt)
    
    ' S.T. 1 : calcolo Dedendum semi-topping massi
    
    Dst = DE1(NE) - 0.04
    Aux = FIN.Eb1(NE) - Erastp(NE) * 2 - Ebomb(NE) * 2
    Aux = SpessSurDiam(Aux, DB1(NE), Dst)
    EbSt(NE) = (Aux / Dst + inv(Acs(DbSt(NE) / Dst))) * DbSt(NE)
    Aux = SpessSurDiam(EbSt(NE), DbSt(NE), DrFm(NE))
    EpStFm = (PI * MappFm - Aux) * Cos(HelrFm(NE))
    Aux = (EpFm(NE) - EpStFm) / 2 / (Tan(ApSt(NE)) - Tan(ApFm(NE)))
    DedStMaxi(NE) = Aux - (DemiPas - EpFm(NE)) / 2 / Tan(ApFm(NE))
    If DedSt(NE) > DedStMaxi(NE) Then DedSt(NE) = DedStMaxi(NE)
    
    ' S.T. 2 : calcolo Dedendum semi-topping di default
    
    Dst = DE1(NE) - Mr * 0.2
    Aux = FIN.Eb1(NE) - Erastp(NE) * 2 - Ebomb(NE) * 2
    Aux = SpessSurDiam(Aux, DB1(NE), Dst)
    EbSt(NE) = (Aux / Dst + inv(Acs(DbSt(NE) / Dst))) * DbSt(NE)
    Aux = SpessSurDiam(EbSt(NE), DbSt(NE), DrFm(NE))
    EpStFm = (PI * MappFm - Aux) * Cos(HelrFm(NE))
    Aux = (EpFm(NE) - EpStFm) / 2 / (Tan(ApSt(NE)) - Tan(ApFm(NE)))
    DedStTh(NE) = Aux - (DemiPas - EpFm(NE)) / 2 / Tan(ApFm(NE))
    If DedSt(NE) = 0 Then DedSt(NE) = DedStTh(NE)
    
    ' S.T. 3 : calcolo Diametro semi-topping ottenuto
    
    Aux = DedSt(NE) + (DemiPas - EpFm(NE)) / 2 / Tan(ApFm(NE))
    EpStFm = EpFm(NE) - 2 * Aux * (Tan(ApSt(NE)) - Tan(ApFm(NE)))
    EbSt(NE) = DbSt(NE) * ((PI * MappFm - EpStFm / Cos(HelrFm(NE))) / DrFm(NE) + inv(ApAppSt))
    DstDentato(NE) = DcroisDev(EbSt(NE), DbSt(NE), Eb1Dent(NE), DB1(NE))
    
    
    
    Aux = DemiPas - AddFm(NE) * 2 * Tan(ApFm(NE)) + 2 * EpProtu(NE) / Cos(ApFm(NE))
    RtPienoFm(NE) = Aux / 2 / Tan(PI / 4 - ApFm(NE) / 2)
    Aux = (DemiPas - EpFm(NE)) / 2 / Tan(ApFm(NE))
    YrtFm(NE) = AddFm(NE) - RtFm(NE) - Aux
    'If Ne = 5 Then Stop
    Aux = DemiPas / 2 - AddFm(NE) * Tan(ApFm(NE)) + EpProtu(NE) / Cos(ApFm(NE))
    XrtFm(NE) = Aux - RtFm(NE) * Tan((PI / 2 - ApFm(NE)) / 2)
    Di1Dent(NE) = Di1(NE) - (AddFm(NE) - AddFmTh(NE)) * 2
    
End Sub


Private Sub PuntiCrem(ZONA As SgrFinType, TipoZona$)

' Coordinate Cremagliera   .XpCr (), .AddCr(), .ApCr()
Dim Aux, i, Ydebut, Xdebut, Yfin, Xfin
Dim Nbp, NpIni, PAS, Xmaxi As Single, Xmini As Single
Dim RastpProv$, RasttProv$, BombProv$

With ZONA
    If TipoZona$ = "Sgr" And SgrIng$ = "MvEsistente" Then
        RastpProv$ = .RastpMola: RasttProv$ = .RasttMola: BombProv$ = .BombMola
    Else
        RastpProv$ = Rastp$: RasttProv$ = Rastt$: BombProv$ = BOMB$
    End If

   Nbp = 5                                    ' Testa Cr
   PAS = .Xrtesta / (Nbp - 1)
   For i = 1 To Nbp
      .XpCr(i) = PAS * (i - 1)
      .AddCr(i) = .Hkw
      .ApCr(i) = PI / 2
   Next i
   NpIni = i - 1
   .NumIniRtesta = NpIni
   
   Nbp = 15                                     ' Raggio di Testa Cr
   If RasttProv$ = "N" And BombProv$ = "N" Then
      Aux = .Apmola
   Else
      If .Rrastt = 0 Then Aux = .ApRastt
      If .Rrastt > 0 Then Aux = .ApTesta
   End If
   Aux = PI / 2 - Aux
   PAS = Aux / (Nbp - 1)
   For i = NpIni To NpIni + Nbp - 1
      Aux = PAS * (i - NpIni)
      .XpCr(i) = .Xrtesta + .Rtesta * Sin(Aux)
      .AddCr(i) = .Yrtesta + .Rtesta * Cos(Aux)
      .ApCr(i) = PI / 2 - Aux
   Next i
   NpIni = i - 1: .NptFiancoMini = NpIni
   .NptIniRastt = NpIni ' modificato sotto se c'� Rastt
   Ydebut = .AddCr(NpIni): Xdebut = .XpCr(NpIni)
   
   If RasttProv$ = "O" Or BombProv$ = "O" Then
      '  A.P. Rastremazione Testa  Cr
      If .Rrastt = 0 Then Nbp = 15 Else Nbp = 9
      If .Rrastt = 0 Then
         Aux = .ApRastt: Yfin = .YiRastt
      Else
         Aux = .ApTesta: Yfin = .YfRastt
      End If
      PAS = (Ydebut - Yfin) / (Nbp - 1)
      For i = NpIni To NpIni + Nbp - 1
         .AddCr(i) = Ydebut - PAS * (i - NpIni)
         .XpCr(i) = Xdebut + (Ydebut - .AddCr(i)) * Tan(Aux)
         .ApCr(i) = Aux
      Next i
      NpIni = i - 1
      .NptIniRastt = NpIni
      If .Rrastt > 0 Then
         Nbp = 15        ' Bombatura Rastremazione Testa CR or bombatura solo
         PAS = (.YfRastt - .YiRastt) / (Nbp - 1)
         For i = NpIni To NpIni + Nbp - 1
            .AddCr(i) = .YfRastt - PAS * (i - NpIni)
            .ApCr(i) = Asn((.YrRastt - .AddCr(i)) / .Rrastt)
            .XpCr(i) = .XrRastt - .Rrastt * Cos(.ApCr(i))
         Next i
         NpIni = i - 1
      End If
   End If
   .NptEvolvMini = NpIni
   
   Nbp = 15             ' A.P. Fianco  Cr
   If RasttProv$ = "O" Or BombProv$ = "O" Then
      Ydebut = .YiRastt
   Else
      Ydebut = .Yrtesta + .Rtesta * Sin(.Apmola)
   End If
   Xdebut = .Sw / 2 - Ydebut * Tan(.Apmola)
   If RastpProv$ = "O" Or BombProv$ = "O" Then
      Yfin = -.YiRastp
   Else
      Yfin = -.Yrfondo - .Rfondo * Sin(.Apmola)
   End If
   PAS = (Ydebut - Yfin) / (Nbp - 1)
   For i = NpIni To NpIni + Nbp - 1
      .AddCr(i) = Ydebut - PAS * (i - NpIni)
      .XpCr(i) = Xdebut + (Ydebut - .AddCr(i)) * Tan(.Apmola)
      .ApCr(i) = .Apmola
   Next i
   NpIni = i - 1
   .NptEvolvMaxi = NpIni
   .NptIniRastp = NpIni  ' modificato sotto se c'� Rastp
   If RastpProv$ = "O" Or BombProv$ = "O" Then
      If .Rrastp > 0 Then
         ' Bombatura o Rastremazione Piede CR
         If .Rrastp = 0 Then Nbp = 15 Else Nbp = 9
         PAS = (.YfRastp - .YiRastp) / (Nbp - 1)
         For i = NpIni To NpIni + Nbp - 1
            .AddCr(i) = -.YiRastp - PAS * (i - NpIni)
            .ApCr(i) = Asn((-.YrRastp - .AddCr(i)) / .Rrastp)
            .XpCr(i) = .XrRastp - .Rrastp * Cos(.ApCr(i))
         Next i
         NpIni = i - 1
      End If
      .NptIniRastp = NpIni
      Nbp = 15             ' A.P. Rastremazione Piede  Cr
      If .Rrastp = 0 Then
         Aux = .ApRastp: Ydebut = -.YiRastp
         Xdebut = .XiRastp
      Else
         Aux = .ApPiede: Ydebut = -.YfRastp
         Xdebut = .XpCr(NpIni)
      End If
      Yfin = -.Yrfondo - .Rfondo * Sin(Aux)
      PAS = (Ydebut - Yfin) / (Nbp - 1)
      For i = NpIni To NpIni + Nbp - 1
         .AddCr(i) = Ydebut - PAS * (i - NpIni)
         .XpCr(i) = Xdebut + (Ydebut - .AddCr(i)) * Tan(Aux)
         .ApCr(i) = Aux
      Next i
      NpIni = i - 1
   End If
   
   .NptFiancoMaxi = NpIni
   Nbp = 15                        ' Raggio di Fondo Cr
   If RastpProv$ = "N" And BombProv$ = "N" Then
      Aux = .Apmola
   Else
      If .Rrastp = 0 Then Aux = .ApRastp
      If .Rrastp > 0 Then Aux = .ApPiede
   End If
   PAS = (PI / 2 - Aux) / (Nbp - 1)
   For i = NpIni To NpIni + Nbp - 1
      .ApCr(i) = Aux + PAS * (i - NpIni)
      .XpCr(i) = .XrFondo - .Rfondo * Cos(.ApCr(i))
      .AddCr(i) = -.Yrfondo - .Rfondo * Sin(.ApCr(i))
   Next i
   NpIni = i - 1: ' PRINT
   .NumFineRfondo = NpIni
   Ydebut = .AddCr(NpIni): Xdebut = .XpCr(NpIni)
   
   Nbp = 5                       ' Fondo Cr
   PAS = (.MrMola * PI / 2 - .XrFondo) / (Nbp - 1):
   For i = NpIni To NpIni + Nbp - 1
      .XpCr(i) = .XrFondo + PAS * (i - NpIni)
      .AddCr(i) = -.Hfw
      .ApCr(i) = PI / 2
   Next i
   .NbpTotale = i - 1
   If .FormRullo = "COPSR" Or .FormRullo = "COPCR" Then
      .NbpTotale = .NbpTotale + 1
      Xmaxi = .XpCr(.NbpTotale - 1) * 2 - .XrFondo
      Xmini = .XpCr(.NbpTotale - 1)
      If Xmaxi - Xmini > 0.25 Then
         .XpCr(.NbpTotale) = Xmini + 0.25
      Else
         .XpCr(.NbpTotale) = Xmaxi
      End If
      .AddCr(.NbpTotale) = .AddCr(.NbpTotale - 1)
      .ApCr(.NbpTotale) = PI / 2
   End If
   'NbpTotIni = NbpTotale
End With
End Sub

Private Sub CalcoloMolaAssiale(ZONA As SgrFinType)

Dim Rref As Single, i As Integer, A$
Dim Intf$ ' non utilise
   
With ZONA
    For i = 1 To .NbpTotale
        ' pour chaque point :
        ' XpCr() 1/2  Sp.pieno --> Sgr.XaxMola() 1/2 Sp.ass.pieno --> Xrull()
        ' AddCr() Addendum     --> Sgr.RaxMola() Raggio della mola  --> Rrull()
        ' ApCr() A.Press.rad.  --> Sgr.ApAxMola() Ang. press.assiale --> ApRullD()
        
        Rref = .DmolaV / 2 - .Hkw + .AddCr(i)
        If .ApCr(i) = 0 Then
            A$ = "Errore in CalcoloMolaAssiale" & vbCrLf & vbCrLf
            A$ = A$ & "Programma da mettrere a posto"
      '      MsgBox A$, vbCritical
            Write_Dialog "ERROR in MOLAVITE02 : 01" '& Err
            Exit Sub
        End If
        Call CalcUnPuntoAxial(ZONA, .XpCr(i), Rref, .ApCr(i), .XaxMola(i), .RaxMola(i), .ApAxMola(i))
    Next i
End With
End Sub

Private Sub CalcUnPuntoAxial(ZONA As SgrFinType, Xcrem, Rref, ApCrem, Xax, Rax, ApAx)
   ' Calcul de 1 point dans le plan axial
   ' obtenu en considerant le profil du plan axial de la fraise-mere
   ' comme etant le profil conjugue de la cremaillere de reference
   ' roulant sur le diametre primitif correspondant a l'inclinaison
   ' Ceci pour une inclinaison < 2 degre

   ' Donnees :  Pi
   ' + pour chaque point du profil de la cremaillere de reference :
   '           Xcrem  : Distance point considere a l'axe du plein
   '           Rref   : Distance point considere a l'axe de la fraise-mere
   '           Apcrem : Angle de pression sur le point considere en radians
   ' + Mola : InFild , InFil , DpMola , PasAxial
   Dim A, Aux, InEngr, RpMola, Ycrem, Apa, Rb, Helx, ApApp
   Dim Intf$ ' non utilise
With ZONA
   If .InFilD < 3 Then
      Xax = Xcrem / Cos(.InFil)
      Rax = Rref
      ApAx = Atn(Tan(ApCrem) / Cos(.InFil))
   Else
      InEngr = PI / 2 - .InFil
      RpMola = .DpMola / 2
      Ycrem = RpMola - Rref
      If Fnrd(ApCrem) < 89 Then
         Apa = Atn(Tan(ApCrem) / Cos(InEngr))
         Rb = RpMola * Cos(Apa)
         A = Rb * Tan(Apa) - Ycrem / Sin(Apa)
         If A < 0 Then Intf$ = "OUI" Else Intf$ = "NON"
         Rax = Sqr(Rb ^ 2 + A ^ 2)
         Aux = (Ycrem / Tan(Apa) + Xcrem / Cos(InEngr)) / RpMola
         Aux = Aux - Apa + Atn(A / Rb)
         ApApp = Acs(Rb / Rax)
         Helx = Atn(.PasAxial / PI / (Rax * 2))
         ApAx = Atn(Tan(ApApp) * Tan(Helx))
      Else
         Intf$ = "NON"
         Rax = Rref
         Aux = Xcrem / Cos(InEngr) / RpMola
         ApApp = PI / 2
         Helx = Atn(.PasAxial / PI / (Rax * 2))
         ApAx = PI / 2
      End If
      Xax = Aux * .PasAxial / PI / 2
   End If
End With
End Sub
'-------------------------------PRINCIPALE
'------------------------------------------
Private Sub CalcoloSpostRullo(ZONA As SgrFinType, CorrEvolv1, CorrEvolv2, DZ1, DX1, EP1, DZ2, DX2, EP2)
   Dim LungCorrEvolv, LdevDmaxi, LdevDmini
   Dim Aux, Aux1, A$, i, Ent, INC, Adev
   Dim ALFA, Beta, eX, gama2
   Dim RmolaMaxi, ImolaMaxi
   Dim RmolaMini, ImolaMini
   Dim ApMolaOtt, DbOtt, ApMolaTh, DbTh
   Dim Errore1, Errore2, ApAr
   Dim Dmaxi, Dmini, PAS
   Dim XrullMaxi, RrullMaxi, XrullMini, RrullMini
   Dim SpostRu1, SpostRu2, SpostRuM, ErrPasso1, ErrPasso2
   Dim EntCorr1, EntCorr2, EntCorrM

   Call LdevAdev(DB1(NE), DE1(NE), LdevDmaxi, VPB, VPB)
   Call LdevAdev(DB1(NE), SAP(NE), LdevDmini, VPB, VPB)
   LungCorrEvolv = LdevDmaxi - LdevDmini

        With ZONA
   Ent = (.DmolaV - (DE1(NE) - Di1(NE)) + .Drullo) / 2
   INC = PI / 2 - Fndr(.IncRulloD)
   .DpMola = .DmolaV - .MrMola * 2
   .InFil = Atn(.PasAxial / PI / .DpMola)
   .MrMola = .PasAxial * Cos(.InFil) / .NFil / PI
   .Apmola = Acs(Mr * Cos(Apr) / .MrMola)
   .Helr(NE) = Asn(Sin(HELB(NE)) / Cos(.Apmola))
   .Dr1(NE) = .MrMola / Cos(.Helr(NE)) * Z1(NE)
   ApAr = Acs(DB1(NE) / .Dr1(NE))
   Aux = Fnrd(Atn(CorrEvolv1 / 1000 / (LungCorrEvolv * Sin(.Apmola))))
   
        If Abs(Aux) > 1 Then
      '6042="LA CORREZIONE RICHIESTA  E ESAGERATA"
    Rem  MsgBox Parola("6042", TabUtente$) & vbCrLf & vbCrLf, 16
    'MsgBox ("6042")
    Write_Dialog "ERROR in MOLAVITE02 : correction not possible (Err02)" '& Err
      Exit Sub
   End If
   Aux = Fnrd(Atn(CorrEvolv2 / 1000 / (LungCorrEvolv * Sin(.Apmola))))
   
        If Abs(Aux) > 1 Then
      '6042="LA CORREZIONE RICHIESTA  E ESAGERATA"
    Rem  MsgBox Parola("6042", TabUtente$) & vbCrLf & vbCrLf, 16
   ' MsgBox ("6042")
    Write_Dialog "ERROR in MOLAVITE02 : correction not possible (Err03)" '& Err
      Exit Sub
   End If
   ' Fianchi senza spostamento : Si dovrebbe ritrovare
   ' l'angolo di pressione mola teorico - infatti � cosi
   
   ' Poi, l'ho modificato per essere utilizzato in macchina
   
   RrullMaxi = .Drullo / 2
   XrullMaxi = .Xrull(.NbpTotale - .NptFiancoMini + 1)
   'XrullMaxi = 0 ' sembra che non influisce
   RrullMini = RrullMaxi - LungCorrEvolv * Sin(ApAr)
   XrullMini = XrullMaxi + (RrullMaxi - RrullMini) * Tan(.Apmola)
   Call CalcPuntoIngDaMf(Ent, INC, .PasAxial, RmolaMaxi, VPB, VPB, ImolaMaxi, XrullMini, RrullMini, .Apmola, VPB)
   Call CalcPuntoIngDaMf(Ent, INC, .PasAxial, RmolaMini, VPB, VPB, ImolaMini, XrullMaxi, RrullMaxi, .Apmola, VPB)
   If RmolaMaxi = 999 Or RmolaMini = 999 Then Stop
   ApMolaTh = Atn((ImolaMaxi - ImolaMini) / (RmolaMaxi - RmolaMini) * Cos(.InFil))
   
   ' Primo fianco con spostamento
   SpostRu1 = 0
   If CorrEvolv1 > 0 Then PAS = -0.1 Else PAS = 0.1
   Do
      If CorrEvolv1 = 0 Then Exit Do
      SpostRu1 = SpostRu1 + PAS
'      If Abs(Abs(SpostRu1) - 3.3) < 0.01 Then Stop
      EntCorr1 = Sqr(Ent ^ 2 - SpostRu1 ^ 2)
      eX = XrullMini + SpostRu1 * Tan(INC)
      Call CalcPuntoIngDaMf(EntCorr1, INC, .PasAxial, RmolaMaxi, VPB, VPB, ImolaMaxi, eX, RrullMini, .Apmola, VPB)
      eX = XrullMaxi + SpostRu1 * Tan(INC)
      Call CalcPuntoIngDaMf(EntCorr1, INC, .PasAxial, RmolaMini, VPB, VPB, ImolaMini, eX, RrullMaxi, .Apmola, VPB)
      If RmolaMaxi = 999 Or RmolaMini = 999 Then Stop
      ApMolaOtt = Atn((ImolaMaxi - ImolaMini) / (RmolaMaxi - RmolaMini) * Cos(.InFil))
      Errore1 = LungCorrEvolv * Tan(ApAppDdefault(NE)) * Tan(ApMolaOtt - ApMolaTh) / Cos(HELB(NE)) * 1000
   Loop While Abs(Errore1) < Abs(CorrEvolv1)

   Aux = Asn(SpostRu1 / Ent)
   Aux1 = .DpMola / 2 * Sin(Aux) / Tan(INC)
   Aux = .PasAxial * Aux / 2 / PI
   ErrPasso1 = Aux1 - Aux
   
   ' Secondo fianco con spostamento
   SpostRu2 = 0
   If CorrEvolv2 < 0 Then PAS = -0.1 Else PAS = 0.1
   Do
      If CorrEvolv2 = 0 Then EntCorr2 = Ent: Exit Do
      SpostRu2 = SpostRu2 + PAS
      'If Abs(Abs(SpostRu2) - 7.7) < 0.01 Then Stop
      EntCorr2 = Sqr(Ent ^ 2 - SpostRu2 ^ 2)
      eX = XrullMini - SpostRu2 * Tan(INC)
      Call CalcPuntoIngDaMf(EntCorr2, INC, .PasAxial, RmolaMaxi, VPB, VPB, ImolaMaxi, eX, RrullMini, .Apmola, VPB)
      eX = XrullMaxi - SpostRu2 * Tan(INC)
      Call CalcPuntoIngDaMf(EntCorr2, INC, .PasAxial, RmolaMini, VPB, VPB, ImolaMini, eX, RrullMaxi, .Apmola, VPB)
      If RmolaMaxi = 999 Or RmolaMini = 999 Then Stop
      ApMolaOtt = Atn((ImolaMaxi - ImolaMini) / (RmolaMaxi - RmolaMini) * Cos(.InFil))
      Errore2 = LungCorrEvolv * Tan(ApAppDdefault(NE)) * Tan(ApMolaOtt - ApMolaTh) / Cos(HELB(NE)) * 1000
   Loop While Abs(Errore2) < Abs(CorrEvolv2)
   Aux = Asn(SpostRu2 / Ent)
   Aux1 = .DpMola / 2 * Sin(Aux) / Tan(INC)
   Aux = .PasAxial * Aux / 2 / PI
   ErrPasso2 = Aux1 - Aux
      
   ' Correzione ottenute con spostamento medio
   SpostRuM = (SpostRu2 + SpostRu1) / 2
   EntCorrM = Sqr(Ent ^ 2 - SpostRuM ^ 2)
   eX = XrullMini + SpostRuM * Tan(INC)
   Call CalcPuntoIngDaMf(EntCorrM, INC, .PasAxial, RmolaMaxi, VPB, VPB, ImolaMaxi, eX, RrullMini, .Apmola, VPB)
   eX = XrullMaxi + SpostRuM * Tan(INC)
   Call CalcPuntoIngDaMf(EntCorrM, INC, .PasAxial, RmolaMini, VPB, VPB, ImolaMini, eX, RrullMaxi, .Apmola, VPB)
   If RmolaMaxi = 999 Or RmolaMini = 999 Then Stop
   ApMolaOtt = Atn((ImolaMaxi - ImolaMini) / (RmolaMaxi - RmolaMini) * Cos(.InFil))
   Errore1 = LungCorrEvolv * Tan(ApAppDdefault(NE)) * Tan(ApMolaOtt - ApMolaTh) / Cos(HELB(NE)) * 1000
   eX = XrullMini - SpostRuM * Tan(INC)
   Call CalcPuntoIngDaMf(EntCorrM, INC, .PasAxial, RmolaMaxi, VPB, VPB, ImolaMaxi, eX, RrullMini, .Apmola, VPB)
   eX = XrullMaxi - SpostRuM * Tan(INC)
   Call CalcPuntoIngDaMf(EntCorrM, INC, .PasAxial, RmolaMini, VPB, VPB, ImolaMini, eX, RrullMaxi, .Apmola, VPB)
   If RmolaMaxi = 999 Or RmolaMini = 999 Then Stop
   ApMolaOtt = Atn((ImolaMaxi - ImolaMini) / (RmolaMaxi - RmolaMini) * Cos(.InFil))
   Errore2 = LungCorrEvolv * Tan(ApAppDdefault(NE)) * Tan(ApMolaOtt - ApMolaTh) / Cos(HELB(NE)) * 1000
   
      DZ1 = SpostRu1            ' Spostamento asse Z profilatura F1
          DX1 = Ent - EntCorr1  ' Spostamento asse X profilatura F1
          EP1 = ErrPasso1
      DZ2 = SpostRu2            ' Spostamento asse Z profilatura F2
          DX2 = Ent - EntCorr2  ' Spostamento asse X profilatura F2
      EP2 = ErrPasso2

   Rem Uscita.Cls
   Rem Uscita.Print: Uscita.Print: Uscita.Print: Uscita.Print: Uscita.Print: Uscita.Print
   Rem If DisegnoIng(Ne) <> "" Then
      Rem Uscita.Print " "; "INGRANAGGIO DISEGNO "; " "; DisegnoIng(Ne)
   Rem End If
   Rem Uscita.Print " "; "NUMERO DENTI ....... :"; Z1(Ne)
   Rem Uscita.Print " "; "ELICA PRIMITIVA .... :"; Helpd(Ne)
   Rem Uscita.Print " "; "LUNGHEZZA EVOLVENTE  :"; " "; Format(LungCorrEvolv, "#.##")
   Rem Uscita.Print
   Rem Uscita.Print " "; "SPOSTAMENTO PRIMO FIANCO   :"; " "; Format(SpostRu1, "##.#0") Asse Z
   Rem Uscita.Print " "; "CON INTERASSE DIMINUITO DI :"; " "; Format(Ent - EntCorr1, "#0.##0")
   Rem Uscita.Print " "; "CON PASSO ERRATO DI        :"; " "; Format(ErrPasso1, "#0.###0") & vbCrLf
      
   Rem Uscita.Print " "; "SPOSTAMENTO SECONDO FIANCO :"; " "; Format(SpostRu2, "##.#0")
   Rem Uscita.Print " "; "CON INTERASSE DIMINUITO DI :"; " "; Format(Ent - EntCorr2, "#0.##0")
   Rem Uscita.Print " "; "CON PASSO ERRATO DI        :"; " "; Format(ErrPasso2, "#0.###0")
   Rem Uscita.Print
   Rem Uscita.Print " "; "CON SPOSTAMENTO MEDIO      :"; " "; Format(SpostRuM, "##.#0")
   Rem Uscita.Print " "; "CON INTERASSE DIMINUITO DI :"; " "; Format(Ent - EntCorrM, "#0.##0")
   Rem Uscita.Print " "; "OTTENUTO SUL FIANCO 1      :"; " "; Format(Errore1, "##0.0")
   Rem Uscita.Print " "; "OTTENUTO SUL FIANCO 2      :"; " "; Format(Errore2, "##0.0")
End With
End Sub

Private Sub Azzerare_Ing_Cr_Mf()
    Dim i As Integer, j As Integer
    Rem VersioneProgetto = 0
    Rem Cliente$ = "": Cartella$ = "": LgCliente = 0
    Rem Rastt$ = "": Rastp$ = "": Bomb$ = ""
    Rem SgrIng$ = "": ProgettoMvEsistente$ = "": FinIng$ = ""
    Rem Commessa$ = ""
    Rem Sgr.MolaOk = ""
    Rem Fin.MolaOk = ""
    NeTotMf = 0
    Rem For i = 1 To 3: Note$(i) = "": Next i
    NeTot = 0
    Mr = 0
    Aprd = 0
    For i = 1 To 99
        XottMf(i) = 0: YottMf(i) = 0
    Next i
    For i = 0 To 10
        Rem For j = 1 To 5: TabCli$(i, j) = "": Next j
        Rem DisegnoIng$(i) = ""
        Rem Titolo$(i) = ""
        Z1(i) = 0
        HelpD(i) = 0
        Di1(i) = 0
        DE1(i) = 0
        FASCIA(i) = 0
        BombElica(i) = 0
        Epr1(i) = 0
        DPG(i) = 0
        Wpg(i) = 0
        Kdt(i) = 0
        Wdt(i) = 0
        SAP(i) = 0
        Erastp(i) = 0
        Sgr.ERastpObtDe(i) = 0: FIN.ERastpObtDe(i) = 0
        Sgr.ErastpObtDmaxiRastp(i) = 0: FIN.ErastpObtDmaxiRastp(i) = 0
        DminiRastp(i) = 0
        Sgr.DminiRastpObt(i) = 0: FIN.DminiRastpObt(i) = 0
        DmaxiRastp(i) = 0
        Sgr.DmaxiRastpObtBomb(i) = 0: FIN.DmaxiRastpObtBomb(i) = 0
        Brastp(i) = 0
        DmaxiRastt(i) = 0
        Sgr.DmaxiRasttObt(i) = 0: FIN.DmaxiRasttObt(i) = 0
        DminiRastt(i) = 0
        Sgr.DminiRasttObtBomb(i) = 0: FIN.DminiRasttObtBomb(i) = 0
        Erastt(i) = 0
        Sgr.ErasttObtDminiRastt(i) = 0: FIN.ErasttObtDminiRastt(i) = 0
        Sgr.ErasttObtSap(i) = 0: FIN.ErasttObtSap(i) = 0
        Brastt(i) = 0
        Ebomb(i) = 0
        Sgr.EbombOtt(i) = 0: FIN.EbombOtt(i) = 0
        DbombMini(i) = 0
        DbombMaxi(i) = 0
        DbombMedio(i) = 0
        Sgr.DbombMedioOtt(i) = 0: FIN.DbombMedioOtt(i) = 0
        Sgr.ErrApOtt(i) = 0: FIN.ErrApOtt(i) = 0
        ' Creatore
        SovramDent(i) = 0
        ApFmD(i) = 0
        ApProtuD(i) = 0
        EpProtu(i) = 0
        RtFm(i) = 0
        AddFm(i) = 0
        DedSt(i) = 0
        ApStD(i) = 0
        ' Mola di forma
        DmolaF(i) = 0
        GioccoFondoMf(i) = 0
        InMolaFd(i) = 0
        SapMf(i) = 0
        RfondoMf(i) = 0
        RtestaMf(i) = 0
        RfRtMf(i) = 0
        ' Sovrametallo per finitura
        SovramMv(i) = 0
    Next i
    
End Sub

Private Sub AzzerareCremRefMola(ZONA As SgrFinType)
Dim i As Integer
With ZONA
    '.LavFondo = ""
    Rem .RasttMola = "": .RastpMola = "": .BombMola = ""
    .Htotale = 0:
    .DmolaV = 0: .NFil = 0: .InFilD = 0: .PasAxial = 0
    .GioccoFondo = 0: .MrMola = 0
    .ApMolaD = 0: .Sw = 0: .ApMolaD = 0: .Hkw = 0
    .Hfw = 0: .Rfondo = 0: .RtPieno = 0: .RfPieno = 0
    .Rtesta = 0: .HRastp = 0: .ApRastpD = 0: .Rrastp = 0
    .YrRastp = 0: .XrRastp = 0: .ApPiede = 0: .HRastt = 0
    .ApRasttD = 0: .Rrastt = 0: .YrRastt = 0: .ApTesta = 0
    Rem .TitoloRul = "": .FormRullo = "": .Profilatura = ""
    .NbpTotale = 0: .NptEvolvMini = 0: .NptEvolvMaxi = 0
    .NptFiancoMini = 0: .NptFiancoMaxi = 0: .Drullo = 0
    .IncRulloD = 0: .TitoloRul = ""
    Rem ModifMola$ = "": ModifRul$ = ""
    For i = 1 To 100
        .XaxMola(i) = 0: .RaxMola(i) = 0: .ApAxMola(i) = 0
        .Rrull(i) = 0: .Xrull(i) = 0: .ApRullD(i) = 0
    Next i
End With
End Sub

Private Sub MeuleJpg(Ent, INC, PAS, Rayon, ALFA, Beta, X, Y, gama2, Ltang)
   '                      Entrees   Variables : RAYON , ALFA , BETA
   '                      Entrees   Constantes : ENT , INC , PAS
   '                      Sorties : X ,  Y , GAMA2 ,LTANG
   '           programme style JPG
   
   Dim Aux, SENS, PL, L, PasL, Ymini, Lmini, Y1, Y2, X3, Y3
   Dim Xmini, GamaL, AlfaL
   If Abs(ALFA) < 0.000001 Then ALFA = 0
   SENS = Sgn(ALFA): If SENS = 0 Then SENS = 1
   SENS = 1 ' le 24.07.01
   ALFA = ALFA * SENS: Beta = Beta * SENS
   If INC > 0 Then
      Aux = (ALFA * PAS / 2 / PI) '/ Tan(Inc)
   Else
      Aux = 0
   End If
   PL = PAS / 100
  
   L = -PL + Aux: PasL = PL: Ymini = 100000
   '                                  recherche y mini
Varierl:
   L = L + PasL
   GoSub Calcul_Xm_Ym
   If Y < Ymini Then
     Ymini = Y
     GoTo Varierl
   End If
   Ymini = Y
   PasL = -PasL / 2
   If Abs(PasL) > PL / 100000 Then GoTo Varierl
   'Debug.Print "    "; Format(l, "###.###");

   Lmini = L: Xmini = X
   L = L + PL: GoSub Calcul_Xm_Ym
   If X < Xmini Then
      PasL = PL
   Else
      PasL = -PL
   End If
   L = Lmini
5350
   L = L + PasL: '                          derivee de la courbe atn(GAMAL)
   L = L + PL * 0.1: GoSub Calcul_Xm_Ym: X3 = X: Y3 = Y
   L = L - PL * 0.2: GoSub Calcul_Xm_Ym
   If Abs(Y - Y3) > 0.000001 Then
      GamaL = (X3 - X) / (Y - Y3)
   Else
      GamaL = 10000 * Sgn(X3 - X)
   End If
   '                                     angle reel du profil atn(GAMA2)
   L = L + PL * 0.1: GoSub Calcul_Xm_Ym
   gama2 = 1 / (Abs(Y1 / Y) / (Tan(Beta + AlfaL) * Cos(INC)) - (-Y2 / Y) * Tan(INC))
     
   If gama2 < GamaL And gama2 > 0 Then GoTo 5350
      
   L = L - PasL: PasL = PasL / 2
   If Abs(PasL) > PL / 1000000 Then
      GoTo 5350
   Else
      GoSub Calcul_Xm_Ym
      gama2 = Atn(1 / (Abs(Y1 / Y) / (Tan(Beta + AlfaL) * Cos(INC)) - (-Y2 / Y) * Tan(INC)))
   End If
   'Debug.Print "    "; Format(FnRd(Atn(GamaL)), "###.###"); "    "; Format(FnRd(Gama2), "###.###")
   Ltang = L
   X = X * SENS: gama2 = gama2 * SENS
   ALFA = ALFA * SENS: Beta = Beta * SENS
   'Debug.Print "    "; Format(X, "###.###");
   'Debug.Print "    "; Format(Y, "###.###");
   'Debug.Print "    "; Format(FnRd(Gama2), "###.###");
   'Debug.Print "    "; Format(Ltang, "###.###")
   
Exit Sub

Calcul_Xm_Ym:
   AlfaL = ALFA - PI * 2 * L / PAS
   X = L * Sin(INC) + Rayon * Sin(AlfaL) * Cos(INC)
   Y1 = Ent - Rayon * Cos(AlfaL)
   Y2 = L * Cos(INC) - Rayon * Sin(AlfaL) * Sin(INC)
   Y = Sqr(Y1 ^ 2 + Y2 ^ 2)
Return
End Sub

Private Function DiObtenu(Z1x, Helbx, db, Eb, ModFm, SpCr, AddFmx)
   
   Dim Aux, ApMl, Helrx, DR, E0r, E1r, s
   
   ApMl = Acs(db / Z1x * Cos(Helbx) / ModFm)
   Helrx = Asn(Sin(Helbx) / Cos(ApMl))
   DR = ModFm / Cos(Helrx) * Z1x
   E1r = (Eb / db - inv(Acs(db / DR))) * DR * Cos(Helrx)
   E0r = ModFm * PI - E1r
   s = AddFmx - (SpCr - E0r) / 2 / Tan(ApMl)
   DiObtenu = ModFm / Cos(Helrx) * Z1x - (s * 2)

End Function

Private Sub CalcRtPieno(ZONA As SgrFinType, TipoZona$)

Dim XfRastt
Dim RastpProv$, RasttProv$, BombProv$

With ZONA
    
    If TipoZona$ = "Sgr" And SgrIng$ = "MvEsistente" Then
        RastpProv$ = .RastpMola: RasttProv$ = .RasttMola: BombProv$ = .BombMola
    Else
        RastpProv$ = Rastp$: RasttProv$ = Rastt$: BombProv$ = BOMB$
    End If

   If RasttProv$ = "O" Or BombProv$ = "O" Then
      If .Rrastt = 0 Then
         .XiRastt = .Sw / 2 - .YiRastt * Tan(.Apmola)
         .EsMola = .XiRastt * 2 - (.HRastt * 2 * Tan(.ApRastt))
         .RtPieno = .EsMola / 2 / Tan(PI / 4 - .ApRastt / 2)
      Else
         XfRastt = .XrRastt - Sqr(.Rrastt ^ 2 - (.YrRastt - .YfRastt) ^ 2)
         .EsMola = XfRastt * 2 - ((.Hkw - .YfRastt) * 2 * Tan(.ApTesta))
         .RtPieno = .EsMola / 2 / Tan(PI / 4 - .ApTesta / 2)
      End If
   Else
      .EsMola = .Sw - .Hkw * 2 * Tan(.Apmola)
      .RtPieno = .EsMola / 2 / Tan(PI / 4 - .Apmola / 2)
   End If
End With
End Sub

Private Function DcObtenu(ZONA As SgrFinType, HstFm, ApStFm, DbSt, EbSt)

' Dbst, Ebst sont des resultats
Dim Aux, ApMl, Helrx, DR, Cst, E0r
Dim ErSt0, EaSt1, ApStA

With ZONA
   ApMl = Acs((DB1(NE) / Z1(NE) * Cos(HELB(NE))) / .MrMola)
   Helrx = Asn(Sin(HELB(NE)) / Cos(ApMl))
   DR = .MrMola / Cos(Helrx) * Z1(NE)
   Aux = (.Eb1(NE) / DB1(NE) - inv(Acs(DB1(NE) / DR))) * DR * Cos(Helrx)
   E0r = .MrMola * PI - Aux
   Cst = HstFm + (.Sw - E0r) / 2 / Tan(ApMl)
   ErSt0 = .Sw + 2 * HstFm * Tan(ApMl) - 2 * Cst * Tan(ApStFm)
   EaSt1 = (.MrMola * PI - ErSt0) / Cos(Helrx)
   ApStA = Atn(Tan(ApStFm) / Cos(Helrx))
   DbSt = DR * Cos(ApStA)
   EbSt = (EaSt1 / DR + inv(ApStA)) * DbSt
   DcObtenu = DcroisDev(.Eb1(NE), DB1(NE), EbSt, DbSt)
End With

End Function

Private Sub PntEvolv(Eb, db, dx, X, Y, dis$)
   '  CALCUL UN POINT DE LA DEVELOPPANTE
   '  Donnees : Eb , Db , Dx
   '  Dis$ : "P" pour Pieno
   '          "V" pour Vano
   Dim Aux
   
   Aux = Eb / db - inv(Acs(db / dx))
   If dis$ = "P" Then
        X = dx * Sin(Aux) / 2
        Y = dx * Cos(Aux) / 2
   Else
        X = dx * Sin(PI / Z1(NE) - Aux) / 2
        Y = dx * Cos(PI / Z1(NE) - Aux) / 2
   End If
End Sub

Private Sub CalcDsmussoOttenutoDaMola(ZONA As SgrFinType, TipoZona$)

Dim R0, Xr0, Yr0, Ap, db, Aux, AlfaMaxi, i, EbDb, AlfaMini
Dim X, Y, ALFA, RX
Dim RastpProv$, RasttProv$, BombProv$

With ZONA
    If TipoZona$ = "Sgr" And SgrIng$ = "MvEsistente" Then
        RastpProv$ = .RastpMola: RasttProv$ = .RasttMola: BombProv$ = .BombMola
    Else
        RastpProv$ = Rastp$: RasttProv$ = Rastt$: BombProv$ = BOMB$
    End If
   .Yrtesta = .Hkw - .Rtesta
   If RasttProv$ = "O" Or BombProv$ = "O" Then
      If .Rrastt = 0 Then
        .Xrtesta = .EsMola / 2 - .Rtesta * Tan((PI / 2 - .ApRastt) / 2)
      Else
        .Xrtesta = .EsMola / 2 - .Rtesta * Tan((PI / 2 - .ApTesta) / 2)
      End If
   Else
      .Xrtesta = .EsMola / 2 - .Rtesta * Tan((PI / 2 - .Apmola) / 2)
   End If
   
   .Yrfondo = .Hfw - .Rfondo
   If RastpProv$ = "N" And BombProv$ = "N" Then
       .XrFondo = .Sw - .Icf / 2 + .Rfondo * Tan(PI / 4 - .Apmola / 2)
   Else
      If .Rrastp = 0 Then
         .XrFondo = .Sw - .Icf / 2 + .Rfondo * Tan(PI / 4 - .ApRastp / 2)
      Else
         .XrFondo = .Sw - .Icf / 2 + .Rfondo * Tan(PI / 4 - .ApPiede / 2)
      End If
   End If

   '  Calcolo Diametro "smusso" ottenuto e Diametro esterno ottenuto
   
   R0 = -.Rfondo: Yr0 = -.Yrfondo - .Hkw + .S0(NE): Xr0 = .XrFondo
   Call CalcPntConjRayon(.Dr1(NE), .Helr(NE), R0, Yr0, Xr0, 0.001, X, Y, "P", RX)
   .De1Obt(NE) = RX * 2
   
   AlfaMaxi = 0
Rem   If RastpProv$ = "N" And BombProv$ = "N" Then
      Ap = .Apmola
Rem   Else
Rem      If .Rrastp = 0 Then Ap = .ApRastp Else Ap = .ApPiede
Rem   End If
   AlfaMini = PI / 2 - Ap
   db = .Dr1(NE) * Cos(Atn(Tan(Ap) / Cos(.Helr(NE))))
   Call CalcPntConjRayon(.Dr1(NE), .Helr(NE), R0, Yr0, Xr0, AlfaMini, X, Y, "P", RX)
   EbDb = Atn(X / Y) + inv(Acs(db / 2 / RX))
   .DsmussOtt(NE) = RX * 2
   
   For i = 1 To 500
      ALFA = AlfaMini - (AlfaMini * i / 500)
      If ALFA < 0.0001 Then ALFA = 0.0001
      Call CalcPntConjRayon(.Dr1(NE), .Helr(NE), R0, Yr0, Xr0, ALFA, X, Y, "P", RX)
'      If Rx < De1(Ne) / 2 Then
         Aux = RX * Sin(EbDb - inv(Acs(db / RX / 2)))
         If X < Aux Then
            If RX * 2 < .DsmussOtt(NE) Then .DsmussOtt(NE) = RX * 2
         End If
'      End If
   Next i
End With
End Sub

Private Sub CalcoloDAttivoDaMola(ZONA As SgrFinType, Eb9, Db9, INTERF$, Dactif, AngTr)
'IN:  Eb9 , Db9
'OUT: Interf$, DACTIF, AngTr
Dim Apa9, Ap9, R0, Yr0, Xr0, SAC, Aux
Dim RXMINI, ALFA, VALFA, XPD, DincrDevTroc
Dim Xpt, Ypt, RX

With ZONA
   Apa9 = Acs(Db9 / .Dr1(NE))
   Ap9 = Atn(Tan(Apa9) * Cos(.Helr(NE)))
   R0 = .Rtesta
   Yr0 = .S0(NE) - R0
   Xr0 = .EsMola / 2 - R0 * Tan((PI / 2 - Ap9) / 2)
   SAC = R0 * Sin(Ap9) + Yr0
   Aux = Db9 / 2 * Tan(Apa9) - SAC / Sin(Apa9)
   If Aux > 0 Then
      RXMINI = Sqr((Db9 / 2) ^ 2 + Aux ^ 2)
   Else
      RXMINI = Db9 / 2
   End If
   If Aux > 0 Then
      INTERF$ = "NON"
      AngTr = PI / 2 - Ap9
      Dactif = RXMINI * 2
   Else
      INTERF$ = "OUI"
      '  DIAMETRE croisement trochoide / developpante
      ALFA = 0: VALFA = 0.19
5110
      ALFA = ALFA + VALFA
      Call CalcPntConjRayon(.Dr1(NE), .Helr(NE), R0, Yr0, Xr0, ALFA, Xpt, Ypt, "P", RX)
      Aux = Eb9 / Db9
      If (Db9 / 2) < RX Then
         Aux = Aux - inv(Acs(Db9 / 2 / RX))
      End If
      XPD = RX * Sin(Aux)
      If RX < RXMINI Or Xpt < XPD Then GoTo 5110
      ALFA = ALFA - VALFA: VALFA = VALFA / 10
      If VALFA > 0.000019 Then GoTo 5110
      DincrDevTroc = RX * 2
      AngTr = ALFA
      Dactif = DincrDevTroc
   End If
End With
End Sub

Private Sub CalcProfiloOttenutoDaMv(ZONA As SgrFinType, TipoZona$) ' Con mola esistente
   
Dim A$, Aux, i, dx, E1, E2, Y, xe, ye
Dim ALFA, Xt, Yt, RX, Nbp
Dim ApPiedeApp, ApTestaApp, Eappr1
Dim YfRastpMola, YfRasttMola
Dim DeFianco, INIZIO
Dim Dprovis As Single, DbOtt, Apa
Dim YfRastpProvis, YfRasttProvis
Dim RastpProv$, RasttProv$, BombProv$
Dim XfRastp, XfRastt

With ZONA
Rem    If TipoZona$ = "Sgr" And SgrIng$ = "MvEsistente" Then
Rem        RastpProv$ = .RastpMola: RasttProv$ = .RasttMola: BombProv$ = .BombMola
Rem    Else
Rem        RastpProv$ = Rastp$: RasttProv$ = Rastt$: BombProv$ = BOMB$
Rem    End If
    .Apmola = Fndr(.ApMolaD)
    .ApRastp = Fndr(.ApRastpD)
    .ApRastt = Fndr(.ApRasttD)
    .Dr1(NE) = (DB1(NE) * Cos(HELB(NE))) / Sqr(Cos(.Apmola + HELB(NE)) * Cos(.Apmola - HELB(NE)))
    .Helr(NE) = Atn(Tan(HELB(NE)) * .Dr1(NE) / DB1(NE))
    .MrMola = .Dr1(NE) / Z1(NE) * Cos(.Helr(NE))
    .HtMini = 0
    For i = 1 To NeTot
         Aux = DiObtenu(Z1(i), HELB(i), DB1(i), .Eb1(i), .MrMola, .Sw, .Hkw)
         Aux = (DE1(i) - Aux) / 2
         If Aux > .HtMini Then .HtMini = Aux
    Next i
    .DiObt(NE) = DiObtenu(Z1(NE), HELB(NE), DB1(NE), .Eb1(NE), .MrMola, .Sw, .Hkw)
    .GioccoFondoOtt(NE) = (.DiObt(NE) - Di1(NE)) / 2
    .S0(NE) = (.Dr1(NE) - Di1(NE)) / 2 - .GioccoFondoOtt(NE)
    ' Calcul .YiRastp, .XiRastp, .YfRastp, XfRastp
    ' .Icf, .RfPieno, .Yrfondo, .XrFondo
    ' .YiRastt, .XiRastt, .YfRastt, XfRastt, .XrRastt
    ' .EsMola, .RtPieno, .YrTesta, .Xrtesta
Rem    If TipoZona$ = "Sgr" And SgrIng$ = "MvEsistente" Then
Rem        RastpProv$ = .RastpMola: RasttProv$ = .RasttMola: BombProv$ = .BombMola
Rem    Else
Rem        RastpProv$ = Rastp$: RasttProv$ = Rastt$: BombProv$ = BOMB$
Rem    End If
    If RastpProv$ = "O" Or BombProv$ = "O" Then
        .YiRastp = .HRastp - .Hkw
        .XiRastp = .Sw / 2 + .YiRastp * Tan(.Apmola)
        Aux = .HRastp - .Hkw
        If .Rrastp = 0 Then
           .Icf = .Sw - Aux * 2 * Tan(.Apmola) - 2 * (.Hfw - Aux) * Tan(.ApRastp)
        Else
           .YfRastp = .YrRastp + .Rrastp * Sin(.ApPiede)
           XfRastp = .XrRastp - Sqr(.Rrastp ^ 2 - (.YfRastp - .YrRastp) ^ 2)
           .Icf = .Sw * 2 - XfRastp * 2 - (.Hfw - .YfRastp) * 2 * Tan(.ApPiede)
        End If
    Else
        .Icf = .Sw - .Hfw * 2 * Tan(.Apmola)
    End If
    If RastpProv$ = "O" Or BombProv$ = "O" Then
        If .Rrastp = 0 Then
           .RfPieno = .Icf / 2 / Tan(PI / 4 - .ApRastp / 2)
        Else
           .RfPieno = .Icf / 2 / Tan(PI / 4 - .ApPiede / 2)
        End If
    Else
        .RfPieno = .Icf / 2 / Tan(PI / 4 - .Apmola / 2)
    End If
    .Yrfondo = .Hfw - .Rfondo
    If RastpProv$ = "N" And BombProv$ = "N" Then
        .XrFondo = .Sw - .Icf / 2 + .Rfondo * Tan(PI / 4 - .Apmola / 2)
    Else
        .XrFondo = .Sw - .Icf / 2 + .Rfondo * Tan(PI / 4 - .ApPiede / 2)
    End If
    If RasttProv$ = "O" Or BombProv$ = "O" Then
        .YiRastt = .Hkw - .HRastt
        .XiRastt = .Sw / 2 - .YiRastt * Tan(.Apmola)
        If .Rrastt = 0 Then
          .XiRastt = .Sw / 2 - .YiRastt * Tan(.Apmola)
          .EsMola = .XiRastt * 2 - (.HRastt * 2 * Tan(.ApRastt))
        Else
          .XrRastt = .XiRastt + Sqr(.Rrastt ^ 2 - (.YrRastt - .YiRastt) ^ 2)
          .YfRastt = .YrRastt - .Rrastt * Sin(.ApTesta)
          XfRastt = .XrRastt - Sqr(.Rrastt ^ 2 - (.YrRastt - .YfRastt) ^ 2)
          .EsMola = XfRastt * 2 - ((.Hkw - .YfRastt) * 2 * Tan(.ApTesta))
        End If
    Else
        .EsMola = .Sw - .Hkw * 2 * Tan(.Apmola)
    End If
    If RasttProv$ = "O" Or BombProv$ = "O" Then
        If .Rrastt = 0 Then
          .Xrtesta = .EsMola / 2 - .Rtesta * Tan((PI / 2 - .ApRastt) / 2)
        Else
          .Xrtesta = .EsMola / 2 - .Rtesta * Tan((PI / 2 - .ApTesta) / 2)
        End If
    Else
        .Xrtesta = .EsMola / 2 - .Rtesta * Tan((PI / 2 - .Apmola) / 2)
    End If
    .Yrtesta = .Hkw - .Rtesta
    .RtPieno = .RtPieno
    Call CalcRtPieno(ZONA, TipoZona$)
    Call CalcDsmussoOttenutoDaMola(ZONA, TipoZona$)
    If .DsmussOtt(NE) > DE1(NE) Then
       DeFianco = DE1(NE)
    Else
       DeFianco = .DsmussOtt(NE)
    End If
    If RastpProv$ = "O" Or BombProv$ = "O" Then
        .DminiRastpObt(NE) = DcObtenu(ZONA, .YiRastp, .ApRastp, .DbRastp(NE), .EbRastp(NE))
        ApPiedeApp = Atn(Tan(.ApPiede) / Cos(.Helr(NE)))
        .DbApPiede(NE) = .Dr1(NE) * Cos(ApPiedeApp)
        Aux = (.MrMola * PI - 2 * (.XrFondo - .Rfondo * Tan(PI / 4 - .ApPiede / 2))) / Cos(.Helr(NE))
        Eappr1 = Aux + 2 * Tan(ApPiedeApp) * (.Hfw + .Hkw - .S0(NE))
        .EbApPiede(NE) = ((Eappr1 / .Dr1(NE)) + inv(ApPiedeApp)) * .DbApPiede(NE)
        If .Rrastp = 0 Then
            For i = 1 To NbpRast
               dx = .DminiRastpObt(NE) + (DeFianco - .DminiRastpObt(NE)) / (NbpRast - 1) * (i - 1)
               E1 = SpessSurDiam(.Eb1(NE), DB1(NE), dx)
               E2 = SpessSurDiam(.EbRastp(NE), .DbRastp(NE), dx)
               .RRastpObt(NE, i) = dx / 2
               .ERastpObt(NE, i) = (E1 - E2) / 2 * DB1(NE) / dx
            Next i
            .ERastpObtDe(NE) = .ERastpObt(NE, NbpRast)
            Dprovis = 0
            If Rastp$ = "O" Then Dprovis = DmaxiRastp(NE)
            If BOMB$ = "O" Then Dprovis = DbombMaxi(NE)
            If Dprovis = 0 Then Dprovis = DE1(NE)
            E1 = SpessSurDiam(.Eb1(NE), DB1(NE), Dprovis)
            E2 = SpessSurDiam(.EbRastp(NE), .DbRastp(NE), Dprovis)
            .ErastpObtDmaxiRastp(NE) = (E1 - E2) / 2 * DB1(NE) / Dprovis
        Else
            YfRastpMola = .YrRastp + .Rrastp * Sin(.ApPiede)
            Call CalcPntConjRayon(.Dr1(NE), .Helr(NE), -.Rrastp, -(.YrRastp + .Hkw - .S0(NE)), .XrRastp, PI / 2 - .ApPiede, Xt, Yt, "P", RX)
            .DmaxiRastpObtBomb(NE) = RX * 2
            Call PntEvolv(.Eb1(NE), DB1(NE), .DmaxiRastpObtBomb(NE), xe, ye, "P")
            ' ErastpObtDmaxiRastpObtBomb(Ne) = (XE - Xt) * Db1(Ne) / (Rx * 2)
            ' Calcolo rastremazione o Bombatura Ottenuta
            Dprovis = 0
            If Rastp$ = "O" Then Dprovis = DmaxiRastp(NE)
            If BOMB$ = "O" Then Dprovis = DbombMaxi(NE)
            If Dprovis = 0 Then Dprovis = DE1(NE)
            If .DmaxiRastpObtBomb(NE) < Dprovis - 0.01 Then
                ' Dprovis Ottenuto con .ApPiede
                E1 = SpessSurDiam(.Eb1(NE), DB1(NE), Dprovis)
                E2 = SpessSurDiam(.EbApPiede(NE), .DbApPiede(NE), Dprovis)
                .ErastpObtDmaxiRastp(NE) = (E1 - E2) / 2 * DB1(NE) / Dprovis
                E1 = SpessSurDiam(.Eb1(NE), DB1(NE), DeFianco)
                E2 = SpessSurDiam(.EbApPiede(NE), .DbApPiede(NE), DeFianco) ' idem avec .EbRastp(Ne) et .DbRastp(Ne)
                .ERastpObtDe(NE) = (E1 - E2) / 2 * DB1(NE) / DeFianco
                For i = 1 To 10
                   ' Definition de YfRastpProvis correspondant Dprovis
                   ALFA = Acs((YfRastpProvis - .YrRastp) / .Rrastp)
                   Call CalcPntConjRayon(.Dr1(NE), .Helr(NE), -.Rrastp, -(.YrRastp + .Hkw - .S0(NE)), .XrRastp, ALFA, Xt, Yt, "P", RX)
                   YfRastpProvis = YfRastpProvis + Dprovis / 2 - RX
                Next i
                Nbp = NbpRast - 1
                YfRastpMola = YfRastpMola
                For i = 1 To Nbp
                   Y = .YiRastp + (YfRastpProvis - .YiRastp) / (Nbp - 1) * (i - 1)
                   ALFA = Acs((Y - .YrRastp) / .Rrastp)
                   Call CalcPntConjRayon(.Dr1(NE), .Helr(NE), -.Rrastp, -(.YrRastp + .Hkw - .S0(NE)), .XrRastp, ALFA, Xt, Yt, "P", RX)
                   If RX * 2 < DB1(NE) Then Stop
                   Call PntEvolv(.Eb1(NE), DB1(NE), RX * 2, xe, ye, "P")
                   .ERastpObt(NE, i) = (xe - Xt) * DB1(NE) / (RX * 2)
                   .RRastpObt(NE, i) = RX
                Next i
                .ERastpObt(NE, NbpRast) = .ERastpObtDe(NE)
                .RRastpObt(NE, NbpRast) = DeFianco / 2
            Else
                For i = 1 To 10
                   ' Definition de YfRastpProvis correspondant Dprovis
                   ALFA = Acs((YfRastpProvis - .YrRastp) / .Rrastp)
                   Call CalcPntConjRayon(.Dr1(NE), .Helr(NE), -.Rrastp, -(.YrRastp + .Hkw - .S0(NE)), .XrRastp, ALFA, Xt, Yt, "P", RX)
                   YfRastpProvis = YfRastpProvis + Dprovis / 2 - RX
                Next i
                ALFA = PI / 2 - Asn((YfRastpProvis - .YrRastp) / .Rrastp)
                Call CalcPntConjRayon(.Dr1(NE), .Helr(NE), -.Rrastp, -(.YrRastp + .Hkw - .S0(NE)), .XrRastp, ALFA, Xt, Yt, "P", RX)
                Call PntEvolv(.Eb1(NE), DB1(NE), RX * 2, xe, ye, "P")
                .ErastpObtDmaxiRastp(NE) = (xe - Xt) * DB1(NE) / (RX * 2)
                If .DmaxiRastpObtBomb(NE) > DeFianco Then
                   ' DeFianco lavorato con .Rrastp
                   For i = 1 To 10
                      ' Definition de YfRastpProvis correspondant DeFianco
                      ALFA = Acs((YfRastpProvis - .YrRastp) / .Rrastp)
                      Call CalcPntConjRayon(.Dr1(NE), .Helr(NE), -.Rrastp, -(.YrRastp + .Hkw - .S0(NE)), .XrRastp, ALFA, Xt, Yt, "P", RX)
                      YfRastpProvis = YfRastpProvis + DeFianco / 2 - RX
                   Next i
                   Call PntEvolv(.Eb1(NE), DB1(NE), DeFianco, xe, ye, "P")
                   .ERastpObtDe(NE) = (xe - Xt) * DB1(NE) / DeFianco
                   For i = 1 To NbpRast
                      Y = .YiRastp + (YfRastpProvis - .YiRastp) / (NbpRast - 1) * (i - 1)
                      ALFA = Acs((Y - .YrRastp) / .Rrastp)
                      Call CalcPntConjRayon(.Dr1(NE), .Helr(NE), -.Rrastp, -(.YrRastp + .Hkw - .S0(NE)), .XrRastp, ALFA, Xt, Yt, "P", RX)
                      Call PntEvolv(.Eb1(NE), DB1(NE), RX * 2, xe, ye, "P")
                      .ERastpObt(NE, i) = (xe - Xt) * DB1(NE) / (RX * 2)
                      .RRastpObt(NE, i) = RX
                   Next i
                Else
                    ' DeFianco Lavorato con .ApPiede
                    E1 = SpessSurDiam(.Eb1(NE), DB1(NE), DeFianco)
                    E2 = SpessSurDiam(.EbApPiede(NE), .DbApPiede(NE), DeFianco) ' idem avec .EbRastp(Ne) et .DbRastp(Ne)
                    .ERastpObtDe(NE) = (E1 - E2) / 2 * DB1(NE) / DeFianco
                    Nbp = NbpRast - 1
                    For i = 1 To Nbp
                       Y = .YiRastp + (YfRastpMola - .YiRastp) / (Nbp - 1) * (i - 1)
                       ALFA = Acs((Y - .YrRastp) / .Rrastp)
                       Call CalcPntConjRayon(.Dr1(NE), .Helr(NE), -.Rrastp, -(.YrRastp + .Hkw - .S0(NE)), .XrRastp, ALFA, Xt, Yt, "P", RX)
                       Call PntEvolv(.Eb1(NE), DB1(NE), RX * 2, xe, ye, "P")
                       .ERastpObt(NE, i) = (xe - Xt) * DB1(NE) / (RX * 2)
                       .RRastpObt(NE, i) = RX
                    Next i
                    .ERastpObt(NE, NbpRast) = .ERastpObtDe(NE)
                    .RRastpObt(NE, NbpRast) = DeFianco / 2
                End If
            End If
            
        End If
    Else
        For i = 1 To NbpRast
            .RRastpObt(NE, i) = 0
            .ERastpObt(NE, i) = 0
        Next i
    End If
                    
    If RasttProv$ = "O" Or BombProv$ = "O" Then
       ' ???  Il diametro attivo e considerato sulla rastremazione
       ApTestaApp = Atn(Tan(.ApTesta) / Cos(.Helr(NE)))
       .DbApTesta(NE) = .Dr1(NE) * Cos(ApTestaApp)
       Aux = (.MrMola * PI - 2 * (.Xrtesta + .Rtesta * Tan(PI / 4 - .ApTesta / 2))) / Cos(.Helr(NE))
       Eappr1 = Aux - 2 * Tan(ApTestaApp) * .S0(NE)
       .EbApTesta(NE) = ((Eappr1 / .Dr1(NE)) + inv(ApTestaApp)) * .DbApTesta(NE)
       .DmaxiRasttObt(NE) = DcObtenu(ZONA, -.YiRastt, .ApRastt, .DbRastt(NE), .EbRastt(NE))
       Call CalcoloDAttivoDaMola(ZONA, .Eb1(NE), DB1(NE), .InterfF(NE), .DactObt(NE), .AngTroc(NE))
       If .Rrastt = 0 Then
          Dprovis = 0
          If Rastt$ = "O" Then Dprovis = DminiRastt(NE)
          If BOMB$ = "O" Then Dprovis = DbombMini(NE)
          If Dprovis = 0 Then Dprovis = SAP(NE)
          If Dprovis < .DactObt(NE) Then Dprovis = .DactObt(NE)
          If Dprovis < .DbApTesta(NE) Then Dprovis = .DbApTesta(NE) + 0.001
          For i = 1 To NbpRast
                dx = Dprovis + (.DmaxiRasttObt(NE) - Dprovis) / (NbpRast - 1) * (i - 1)
                E1 = SpessSurDiam(.Eb1(NE), DB1(NE), dx)
                E2 = SpessSurDiam(.EbRastt(NE), .DbRastt(NE), dx)
                .RRasttObt(NE, i) = dx / 2
                .ERasttObt(NE, i) = (E1 - E2) / 2 * DB1(NE) / dx
          Next i
          If SAP(NE) > .DbRastt(NE) Then
                E1 = SpessSurDiam(.Eb1(NE), DB1(NE), SAP(NE))
                E2 = SpessSurDiam(.EbRastt(NE), .DbRastt(NE), SAP(NE))
                .ErasttObtSap(NE) = (E1 - E2) / 2 * DB1(NE) / SAP(NE)
          Else
             .ErasttObtSap(NE) = 0
          End If
          If Rastt$ = "O" Then
             E1 = SpessSurDiam(.Eb1(NE), DB1(NE), DminiRastt(NE))
             E2 = SpessSurDiam(.EbRastt(NE), .DbRastt(NE), DminiRastt(NE))
             .ErasttObtDminiRastt(NE) = (E1 - E2) / 2 * DB1(NE) / DminiRastt(NE)
          End If
       Else
          E1 = (.YrRastt - .Rrastt * Sin(.ApTesta)) - (.Hkw - .S0(NE))
          Apa = Atn(Tan(.ApTesta) / Cos(.Helr(NE)))
          Aux = .Dr1(NE) / 2 * Sin(Apa) - E1 / Sin(Apa)
          If Aux < 0 Then
             'MsgBox "IL Y A INTERFERENCE", 16
             .InterfF(NE) = "OUI"
          End If
          YfRasttMola = .YrRastt - .Rrastt * Sin(.ApTesta)
          Call CalcPntConjRayon(.Dr1(NE), .Helr(NE), -.Rrastt, .YrRastt - .Hkw + .S0(NE), .XrRastt, PI / 2 - .ApTesta, Xt, Yt, "P", RX)
          .DminiRasttObtBomb(NE) = RX * 2
          If .DminiRasttObtBomb(NE) > DB1(NE) Then
             Call PntEvolv(.Eb1(NE), DB1(NE), .DminiRasttObtBomb(NE), xe, ye, "P")
             ' .ErasttObtDminiRasttObtBomb(Ne) = (XE - Xt) * Db1(Ne) / (Rx * 2)
          End If
          
          ' Calcolo rastremazione o Bombatura Ottenuta
          Dprovis = 0
          If Rastt$ = "O" Then Dprovis = DminiRastt(NE)
          If BOMB$ = "O" Then Dprovis = DbombMini(NE)
          If Dprovis = 0 Then Dprovis = SAP(NE)
          
          If Dprovis < .DbApTesta(NE) Then Dprovis = .DbApTesta(NE) + 0.001
          If .DminiRasttObtBomb(NE) > Dprovis + 0.001 Then
             ' Dprovis Ottenuto con .ApTesta
             E1 = SpessSurDiam(.Eb1(NE), DB1(NE), Dprovis)
             E2 = SpessSurDiam(.EbApTesta(NE), .DbApTesta(NE), Dprovis)
             .ErasttObtDminiRastt(NE) = (E1 - E2) / 2 * DB1(NE) / Dprovis
          Else
             For i = 1 To 10
                ' Definition de YfRasttProvis correspondant Dprovis
                ALFA = Acs((.YrRastt - YfRasttProvis) / .Rrastt)
                Call CalcPntConjRayon(.Dr1(NE), .Helr(NE), -.Rrastt, .YrRastt - .Hkw + .S0(NE), .XrRastt, ALFA, Xt, Yt, "P", RX)
                YfRasttProvis = YfRasttProvis - Dprovis / 2 + RX
             Next i
             ALFA = PI / 2 - Asn((.YrRastt - YfRasttProvis) / .Rrastt)
             Call CalcPntConjRayon(.Dr1(NE), .Helr(NE), -.Rrastt, .YrRastt - .Hkw + .S0(NE), .XrRastt, ALFA, Xt, Yt, "P", RX)
             Call PntEvolv(.Eb1(NE), DB1(NE), RX * 2, xe, ye, "P")
             .ErasttObtDminiRastt(NE) = (xe - Xt) * DB1(NE) / (RX * 2)
          End If
          If SAP(NE) < .DminiRasttObtBomb(NE) And SAP(NE) > .DbApTesta(NE) Then
             E1 = SpessSurDiam(.Eb1(NE), DB1(NE), SAP(NE))
             E2 = SpessSurDiam(.EbApTesta(NE), .DbApTesta(NE), SAP(NE))
             .ErasttObtSap(NE) = (E1 - E2) / 2 * DB1(NE) / SAP(NE)
          Else
             .ErasttObtSap(NE) = 0
          End If
          If BOMB$ = "O" Then ' If BombProv$ = "O" Then
             ' Ap e bombatura ottenuti
             .DbombMedioOtt(NE) = (.DmaxiRasttObt(NE) + .DminiRastpObt(NE)) / 2
             E1 = SpessSurDiam(.Eb1(NE) - 2 * .ErasttObtDminiRastt(NE), DB1(NE), DbombMini(NE))
             E2 = SpessSurDiam(.Eb1(NE) - 2 * .ErastpObtDmaxiRastp(NE), DB1(NE), DbombMaxi(NE))
             Dim Eb
             Call Devel2diametri(E1, E2, DbombMini(NE), DbombMaxi(NE), DbOtt, Eb)
             .ErrApOtt(NE) = (.ErastpObtDmaxiRastp(NE) - .ErasttObtDminiRastt(NE))
             .EbombOtt(NE) = (.ErasttObtDminiRastt(NE) + .ErastpObtDmaxiRastp(NE)) / 2
          End If
          ' Calcolo Profilo Ottenuto
          If .DminiRasttObtBomb(NE) < SAP(NE) Then
             INIZIO = 1
          Else
             If .ErasttObtSap(NE) > 0 Then
                INIZIO = 2
                .ERasttObt(NE, 1) = .ErasttObtSap(NE)
                .RRasttObt(NE, 1) = SAP(NE) / 2
             Else
                INIZIO = 1
             End If
          End If
          For i = INIZIO To NbpRast
             ' .Yirastt definito dalla mola
             ' YfRasttProvis lavora  .DminiRastt
             ' Modif 02.03.05  Y = .YfRastt - (YfRasttMola - .YiRastt) / (NbpRast - Inizio) * (i - Inizio)
             Y = YfRasttMola - (YfRasttMola - .YiRastt) / (NbpRast - INIZIO) * (i - INIZIO)
             ALFA = Acs((.YrRastt - Y) / .Rrastt)
             Call CalcPntConjRayon(.Dr1(NE), .Helr(NE), -.Rrastt, .YrRastt - .Hkw + .S0(NE), .XrRastt, ALFA, Xt, Yt, "P", RX)
             Call PntEvolv(.Eb1(NE), DB1(NE), RX * 2, xe, ye, "P")
             .ERasttObt(NE, i) = (xe - Xt) * DB1(NE) / (RX * 2)
             .RRasttObt(NE, i) = RX
             'Debug.Print Ne; i; Round(Y, 2); Round(.RRasttObt(Ne, i), 2); Round(.ERasttObt(Ne, i), 4)
          Next i
       End If
    Else
       For i = 1 To NbpRast
           .RRasttObt(NE, i) = 0
           .ERasttObt(NE, i) = 0
       Next i
       Call CalcoloDAttivoDaMola(ZONA, .Eb1(NE), DB1(NE), .InterfF(NE), .DactObt(NE), .AngTroc(NE))
    End If
    
    If Abs(FASCIA(NE)) > 2 And BombElica(NE) > 0 Then
        Dim Aux1, BombRadiale, RBomb, Depf
        Dim Aph, APB, ErroreH, ErroreB, ApAr
        Dim LdevDmini, LdevDmaxi, LungEvolv, LongAvancePourGenerer
        ApAr = Acs(DB1(NE) / .Dr1(NE))
        Call LdevAdev(DB1(NE), DE1(NE), LdevDmaxi, VPB, VPB)
        Call LdevAdev(DB1(NE), SAP(NE), LdevDmini, VPB, VPB)
        LungEvolv = Int((LdevDmaxi - LdevDmini) * 0.95)
        LongAvancePourGenerer = LungEvolv * Tan(HELB(NE))
        BombRadiale = BombElica(NE) / Sin(.Apmola)
        RBomb = (FASCIA(NE) / 2) ^ 2 / 2 / BombRadiale
        Depf = Atn((FASCIA(NE) / 2) / RBomb)
'        Depf = Atn((6) / Rbomb)
        Call DbHelObt(.Apmola, .Helr(NE), Depf, .Dr1(NE), Aph, APB, Aux, Aux, Aux)
        ErroreH = LungEvolv * Tan(ApAr) * Tan(Aph - ApAr)
        ErroreB = LungEvolv * Tan(ApAr) * Tan(APB - ApAr)
        .SvergolamentoOtt(NE) = ErroreH
        'Aux1 = Abs(ErroreH - ErroreB) / 2
        'Dim Deplp
        'Deplp = Atn(Tan(ApAr) * Tan(Depf))
        'Aux = LongAvancePourGenerer * Tan(Deplp) * Cos(ApAr) ' idem ErroreB ErroreH
    Else
        .SvergolamentoOtt(NE) = 0
    End If
    
End With
End Sub

Private Sub DbHelObt(ApMol, HelRot, Depf, Drot, Aph, APB, PashOtt, PasbOtt, Bascul)
     ' Calcul des diametres de base et des pas des helices de base obtenus
     ' en fonction des donnees reelles de la fraise-mere
     '
     ' Donnees : ApMol ; HelRot ; depf ; Drot
     ' Calculs : DbhOtt ; DbbOtt ; PashOtt ; PasbOtt ; Bascul
     Dim AZ1, AZ2, AZ3, AZ4
     Dim HelNil, DbhOtt, DbbOtt
     HelNil = Atn(Tan(HelRot) * Cos(Depf))
     Bascul = Asn(Sin(HelRot) * Sin(Depf))
     AZ1 = Sin(Depf) * Tan(HelNil)
     AZ2 = Cos(HelNil) / Cos(Depf)
     Aph = Atn(Tan(ApMol) / AZ2 + AZ1)
     DbhOtt = Drot * Cos(Aph)
     APB = Atn(Tan(ApMol) / AZ2 - AZ1)
     DbbOtt = Drot * Cos(APB)
     AZ3 = Tan(Depf) * Sin(APB)
     AZ4 = Tan(Depf) * Sin(Aph)
     PasbOtt = DbbOtt * PI / (Tan(HelRot) * Cos(APB) + AZ3)
     PashOtt = DbhOtt * PI / (Tan(HelRot) * Cos(Aph) - AZ4)
End Sub

Private Sub CalcoloPntConjugSuIngDaMv(DR, Hel, Apr, XR, Sx, RX, X, Y, dis$)
   
   ' Calcul d'un point sur l'engrenage d'apres les coordonnees
   ' de la cremaillere
   ' Donnees : Dr  : Diametre de roulement
   '           Apr : Angle de pression cremaillere reel
   '           E0  : 1/2 Epaisseur Dent cremaillere
   '           Sx  : Addendum cremaillere rispetto al primitivo di rotolamento
   '           Z1   : Nombre de dent engrenage a generer
   '           Hel : helice sur Dr
   ' Sorties : Profil conjugue : Rx  : Rayon
   '           --------------- : XcRx et YcRx : coordonnees rectangulaires
   '                             du point par rapport axe creux et axe ing.
   '           --------------- : XpRx et YpRx : coordonnees rectangulaires
   '                             du point par rapport axe plein et axe ing.
   Dim Rp, E0, Apa, A, B, C, D, AngC, Angp, XcRx, YcRx, XpRx, YpRx
   Dim Aux
   Rp = DR / 2
   E0 = XR / Cos(Hel)
   Apa = Atn(Tan(Apr) / Cos(Hel))
   A = Sx / Sin(Apa)
   Aux = Rp * Sin(Apa) - A
   
  ' If Aux < 0 Then Stop '  interfere
   
  '    Rx = Rp * Cos(Apa) + 0.0001
  '    X = 0: Y = Rx
  ' Else
      B = PI / 2 - Apa
      RX = Sqr(A ^ 2 + Rp ^ 2 - 2 * A * Rp * Cos(B))
      C = Asn(Sin(B) * A / RX)
      D = (Sx / Tan(Apa) - E0) / Rp
      AngC = C - D
      Angp = PI / Z1(NE) - AngC
   'End If
   XcRx = RX * Sin(AngC): YcRx = RX * Cos(AngC)
   If Abs(XcRx) < 0.001 Then XcRx = 0
   XpRx = RX * Sin(Angp): YpRx = RX * Cos(Angp)
   If Abs(XpRx) < 0.001 Then XpRx = 0
   If dis$ = "P" Then
    X = XpRx: Y = YpRx
   Else
    X = XcRx: Y = YcRx
   End If

End Sub

Private Sub CalcPntConjRayon(DR, Hel, R0, Yr0, Xr0, ALFA, X, Y, dis$, RX)

Dim Yp, Xp, Apr
   
   Yp = Yr0 + R0 * Cos(ALFA)
   Xp = Xr0 + R0 * Sin(ALFA)
   Apr = PI / 2 - ALFA
   Call CalcoloPntConjugSuIngDaMv(DR, Hel, Apr, Xp, Yp, RX, X, Y, dis$)

End Sub

Private Sub CalcoloRullo(ZONA As SgrFinType)
    ' Donnees : .DmolaV; .Nfil; .InFil; .PasAxial; .Drullo
    '           .RaxMola() ; .XaxMola() ; .ApAxMola()
    '           .NptFiancoMini ; .NbpTotale
    Dim Aux, i, Ent, INC, Lvano, ALFA, Helx, Beta, Nmedio
    Dim X, Y, gama2, Ltang, PAS, Prolunga
    Dim ApRulMedio As Single
    
With ZONA
    If .Drullo < .Htotale * 2 + 10 Then
        For i = 1 To 100
            .Rrull(i) = 0: .Xrull(i) = 0: .ApRullD(i) = 0
        Next i
        Exit Sub
    End If
    INC = PI / 2 - Fndr(.IncRulloD)
    Ent = .DmolaV / 2 - (.Hkw + .Hfw) + .Drullo / 2
    For i = 1 To .NbpTotale
        Lvano = .PasAxial / .NFil / 2 - .XaxMola(i)
        ' If i = 30 Then Stop
        If .FormRullo = "COPSR" Or .FormRullo = "COPCR" Then
           ' Pour depassement de l'axe, voir .XpCr(.NbpTotale) =
           Lvano = Lvano + .PasAxial / .NFil / 2
        End If
        ALFA = 2 * PI * Lvano / .PasAxial
        Helx = Atn(.PasAxial / PI / .RaxMola(i) / 2)
        Beta = Atn(Tan(.ApAxMola(i)) / Tan(Helx))
        If Abs(Fnrd(Beta)) > 89.9999 Then Beta = Fndr(89.9999)
        ' le programme normal ne fonctionne pas lorsque ( alfa + beta ) > 180
        ' ce qui est souvent le cas surtout si Lvano est spostato
        If (ALFA + Beta) > PI Then
           Call MeuleJpg(Ent, INC, .PasAxial, .RaxMola(i), ALFA, Beta, X, Y, gama2, Ltang)
        Else
           ' Call PuntoMolaF(Ent, Inc, .PasAxial, .RaxMola(i), Alfa, Beta, X, Y, Gama2, Ltang)
           ' Stop
           Call MeuleJpg(Ent, INC, .PasAxial, .RaxMola(i), ALFA, Beta, X, Y, gama2, Ltang)
        End If
        .Xrull(i) = X
        .Rrull(i) = Y
        .ApRullD(i) = Fnrd(gama2)
        'If i = 80 Then Stop
    Next i
    
    ' Sotto : Definizione rullo centrale
    
    If .FormRullo = "COPSR" Or .FormRullo = "COPCR" Then
        If .FormRullo = "COPSR" Then
            .DrulloCentrale = (Ent - .DmolaV / 2) * 2
        Else
            For i = 1 To .NptFiancoMini - 1
                ALFA = -2 * PI * .XaxMola(i) / .PasAxial
                Helx = Atn(.PasAxial / PI / .RaxMola(i) / 2)
                Beta = Atn(Tan(.ApAxMola(i)) / Tan(Helx))
                If Abs(Fnrd(Beta)) > 89.9 Then Beta = Fndr(89.9999)
                Call MeuleJpg(Ent, INC, .PasAxial, .RaxMola(i), ALFA, Beta, X, Y, gama2, Ltang)
                ' Call PuntoMolaF(Ent, Inc, .PasAxial, .RaxMola(i), Alfa, Beta, X, Y, Gama2, Ltang)
                .XrullCent(i) = -X
                .RrullCent(i) = Y
                .ApRullCentD(i) = Fnrd(gama2)
            Next i
            ' prolunga di .5 mm
            Prolunga = 0.5
            .RrullCent(i) = .RrullCent(i - 1) + Prolunga * Cos(gama2)
            .XrullCent(i) = .XrullCent(i - 1) + Prolunga * Sin(gama2)
            .ApRullCentD(i) = .ApRullCentD(i - 1)
            .NbpTotRulCent = i
            .DrulloCentrale = .RrullCent(i) * 2
            Rem Debug.Print Progetto$; "   RULLO CENTRALE "
            Rem Debug.Print " XrullCent   RrullCent   ApRullCentD"
            For i = 1 To .NbpTotRulCent
                Debug.Print i; "  "; Format(.XrullCent(i), "###.###0");
                Debug.Print "  "; Format(.RrullCent(i), "###.###0");
                Debug.Print "  "; Format(.ApRullCentD(i), "###.###0")
            Next i
        End If
    End If
    
    ' Sotto  : Prolunga mola de .5 mm se non lavora il raggio testa mola
    
    If .FormRullo = "S2F" Or .FormRullo = "COPSR" Or .FormRullo = "COPCR" Then
        Prolunga = 0.5 + .Rrull(.NptFiancoMini) - .Rrull(1)
        For i = 1 To .NptFiancoMini
            .Rrull(i) = .Rrull(.NptFiancoMini) - (Prolunga / (.NptFiancoMini - 1)) * (.NptFiancoMini - i)
            Aux = Tan(Fndr(.ApRullD(.NptFiancoMini)))
            .Xrull(i) = .Xrull(.NptFiancoMini) + (Prolunga / (.NptFiancoMini - 1)) * (.NptFiancoMini - i) * Aux
            .ApRullD(i) = .ApRullD(.NptFiancoMini)
        Next i
    End If
       
    ApRulMedio = Atn((.Xrull(.NptFiancoMini) - .Xrull(.NptFiancoMaxi)) / (.Rrull(.NptFiancoMaxi) - .Rrull(.NptFiancoMini)))
    .ApRulMedioD = Fnrd(ApRulMedio)
    
    ' .AddRullo considerato sulla mola afinche sia costante per confronti
    
    Nmedio = Int((.NptEvolvMini + .NptEvolvMaxi) / 2)
    .AddRullo = .RaxMola(Nmedio) - (.DmolaV / 2 - .Htotale)
    Aux = Tan(ApRulMedio) * (.Rrull(Nmedio) - .Rrull(.NptFiancoMaxi))
    .BombRullo = (.Xrull(Nmedio) - .Xrull(.NptFiancoMaxi) + Aux) * Cos(ApRulMedio)
    If .FormRullo = "MUL" Then
        Stop ' da fare
    Else
        .SpTotControlloRullo = 2 * (.Xrull(Nmedio) + Tan(ApRulMedio) * (.AddRullo - (.Drullo / 2 - .Rrull(Nmedio))))
        If .FormRullo = "COPSR" Or .FormRullo = "COPCR" Then
            .SpControlloRulloSing = .SpTotControlloRullo / 2 - .Xrull(.NbpTotale)
            .SpRulloCentrale = .Xrull(.NbpTotale) * 2
            .DrulliFianchi = .Rrull(.NbpTotale) * 2
            .AddControlloRullo = .AddRullo + (.DrulliFianchi - .Drullo) / 2
        Else
            .AddControlloRullo = .AddRullo
            .SpControlloRulloSing = 0
            .SpRulloCentrale = 0
            .DrulloCentrale = 0
            .DrulliFianchi = 0
        End If
    End If
End With
  
End Sub

Private Sub CalcoloCremTeoric(ZONA As SgrFinType, APD)

Dim Aux, Errastt, Ear1, Err1, ERR0
Dim E, D1, D2, E1, E2
Dim Apa9, Lrast, Rmini

With ZONA
    .Apmola = Fndr(APD)
    .Dr1(NE) = (DB1(NE) * Cos(HELB(NE))) / Sqr(Cos(.Apmola + HELB(NE)) * Cos(.Apmola - HELB(NE)))
    .Helr(NE) = Atn(Tan(HELB(NE)) * .Dr1(NE) / DB1(NE))
    .MrMola = .Dr1(NE) / Z1(NE) * Cos(.Helr(NE))
    Aux = Acs(DB1(NE) / .Dr1(NE))
    Ear1 = (.Eb1(NE) / DB1(NE) - inv(Aux)) * .Dr1(NE)
    Err1 = Ear1 * Cos(.Helr(NE))
    ERR0 = (PI * .Dr1(NE) / Z1(NE) - Ear1) * Cos(.Helr(NE))
    .S0(NE) = (.Dr1(NE) - Di1(NE)) / 2 - .GioccoFondo
    .EsMola = ERR0 - (2 * .S0(NE) * Tan(.Apmola))
    .Sw = PI * .MrMola / 2
    .Hkw = ((.Sw - .EsMola) / 2) / Tan(.Apmola)
    If (Rastt$ = "O" Or BOMB$ = "O") Then
        ' Calcolo Rastremazione Piede Ing. o Bombatura piede
        If Rastt$ = "O" Then
            D1 = DmaxiRastt(NE): D2 = DminiRastt(NE)
            E1 = SpessSurDiam(.Eb1(NE), DB1(NE), D1)
            E2 = SpessSurDiam(.Eb1(NE), DB1(NE), D2)
            E2 = E2 - Erastt(NE) * 2 * D2 / DB1(NE)
        Else
            If DbombMedio(NE) < DB1(NE) Then Exit Sub
            D1 = DbombMedio(NE): D2 = DbombMini(NE)
            E1 = SpessSurDiam(.Eb1(NE), DB1(NE), D1)
            E2 = SpessSurDiam(.Eb1(NE), DB1(NE), D2)
            E2 = E2 - Ebomb(NE) * 2 * D2 / DB1(NE)
        End If
        Call Devel2diametri(E1, E2, D1, D2, .DbRastt(NE), .EbRastt(NE))
        If .DbRastt(NE) = 0 Then
            '36="Errore in 'CalcoloCremTeoric' ( Calcolo Rastremazione Piede Ing. ) : Il programma si ferma : Controllare i dati"
            MsgBox ("DbRastt(Ne) Errore in CalcoloCremTeoric")
            Write_Dialog "ERROR in MOLAVITE02 : basic rack evaluation (Err03)" '& Err
            End
        End If
        E = SpessSurDiam(.Eb1(NE), DB1(NE), D1)
        .EbRastt(NE) = (E / D1 + inv(Acs(.DbRastt(NE) / D1))) * .DbRastt(NE)
        ' Calcolo Rastremazione Testa Mola iniziale
        .ApRastt = Atn(Tan(Acs(.DbRastt(NE) / .Dr1(NE))) * Cos(.Helr(NE)))
        ' .ApTesta = .ApRastt
        .ApRasttD = Fnrd(.ApRastt)
        Errastt = (.EbRastt(NE) / .DbRastt(NE) - inv(Acs(.DbRastt(NE) / .Dr1(NE)))) * .Dr1(NE) * Cos(.Helr(NE))
        Aux = (Errastt - Err1) / 2 / (Tan(.Apmola) - Tan(.ApRastt))
        .HRastt = .S0(NE) - Aux
        
         ' Calcolo Bombatura Maxi Rastt ( RASTREMAZIONE PIEDE INGRANAGGIO )
        
        .ApRastt = Fndr(.ApRasttD)
        .YiRastt = .Hkw - .HRastt
        .XiRastt = .Sw / 2 - .YiRastt * Tan(.Apmola)
        .DmaxiRasttObt(NE) = DcObtenu(ZONA, -.YiRastt, .ApRastt, .DbRastt(NE), .EbRastt(NE))
        Apa9 = Acs(.DbRastt(NE) / .Dr1(NE))
        If Rastt$ = "O" Then
           Call LdevAdev(.DbRastt(NE), DminiRastt(NE), Aux, VPB, 0)
        Else
           Call LdevAdev(.DbRastt(NE), DbombMini(NE), Aux, VPB, 0)
        End If
        .YfRastt = -(Sin(Apa9) * (Aux - .DbRastt(NE) / 2 * Tan(Apa9)) + .S0(NE) - .Hkw)
        ' Calcolo : Bombatura massimo ( BombMaxiRastt(Ne) per .Rrastt tangent in .YiRastt )
        
        'Lrast = ((.YfRastt - .YiRastt) / Cos(.ApRastt)) / 2     - Modif 11.04.05
        'Rmini = Abs(Lrast / Sin((.ApMola - .ApRastt)))          - Modif 11.04.05
        'BombMaxiRastt(Ne) = Rmini - Sqr(Rmini ^ 2 - Lrast ^ 2)  - Modif 11.04.05
        
        Lrast = ((.YfRastt - .YiRastt) / Cos(Apa9)) / 2
        Rmini = Abs(Lrast / Sin((Apa9 - Acs(DB1(NE) / .Dr1(NE)))))
        BombMaxiRastt(NE) = Rmini - Sqr(Rmini ^ 2 - Lrast ^ 2)
        If BOMB$ = "O" Then
           Brastt(NE) = BombMaxiRastt(NE) - 0.0001
           If Brastt(NE) < 0.0001 Then Brastt(NE) = 0
        End If
    Else
        '.ApTesta = .ApMola
    End If
   
    If (Rastp$ = "O" Or BOMB$ = "O") Then
        ' Calcolo Rastremazione testa Ing. o bombatura testa
        If Rastp$ = "O" Then
           D1 = DminiRastp(NE): E1 = SpessSurDiam(.Eb1(NE), DB1(NE), D1)
           D2 = DmaxiRastp(NE): E2 = SpessSurDiam(.Eb1(NE), DB1(NE), D2)
           E2 = E2 - Erastp(NE) * 2 * D2 / DB1(NE)
        Else
           D1 = DbombMedio(NE): E1 = SpessSurDiam(.Eb1(NE), DB1(NE), D1)
           D2 = DbombMaxi(NE): E2 = SpessSurDiam(.Eb1(NE), DB1(NE), D2)
           E2 = E2 - Ebomb(NE) * 2 * D2 / DB1(NE)
        End If
        Call Devel2diametri(E1, E2, D1, D2, .DbRastp(NE), .EbRastp(NE))
        E = SpessSurDiam(.Eb1(NE), DB1(NE), D1)
        Aux = Acs(.DbRastp(NE) / D1)
        .EbRastp(NE) = (E / D1 + inv(Aux)) * .DbRastp(NE)
        ' Calcolo Rastremazione Piede Mola Iniziale
        Aux = Acs(.DbRastp(NE) / .Dr1(NE))
        .ApRastp = Atn(Tan(Aux) * Cos(.Helr(NE)))
        '.ApPiede = .ApRastp
        .ApRastpD = Fnrd(.ApRastp)
        Aux = (.EbRastp(NE) / .DbRastp(NE) - inv(Aux)) * .Dr1(NE)
        Aux = (Aux * Cos(.Helr(NE)) - Err1) / 2
        .HRastp = Aux / (Tan(.ApRastp) - Tan(.Apmola)) + .S0(NE)
        
        ' Calcolo Bombatura Maxi Rastp ( RASTREMAZIONE TESTA INGRANAGGIO )
        
        .ApRastp = Fndr(.ApRastpD)
        .YiRastp = .HRastp - .Hkw
        .XiRastp = .Sw / 2 + .YiRastp * Tan(.Apmola)
        .DminiRastpObt(NE) = DcObtenu(ZONA, .YiRastp, .ApRastp, .DbRastp(NE), .EbRastp(NE))
        Apa9 = Acs(.DbRastp(NE) / .Dr1(NE))
        If Rastp$ = "O" Then
            Call LdevAdev(.DbRastp(NE), DmaxiRastp(NE), Aux, VPB, 0)
        Else
            Call LdevAdev(.DbRastp(NE), DbombMaxi(NE), Aux, VPB, 0)
        End If
        .YfRastp = Sin(Apa9) * (Aux - .DbRastp(NE) / 2 * Tan(Apa9)) + .S0(NE) - .Hkw
        
        ' Calcolo : Bombatura massimo ( BombMaxiRastp(Ne) per .Rrastp tangent in .YiRastt )
        'Lrast = ((.YfRastp - .YiRastp) / Cos(.ApRastp)) / 2      - Modif 11.04.05
        'Rmini = Lrast / Sin((.ApRastp - .ApMola))                - Modif 11.04.05
        'BombMaxiRastp(Ne) = Rmini - Sqr(Rmini ^ 2 - Lrast ^ 2)   - Modif 11.04.05
        Lrast = ((.YfRastp - .YiRastp) / Cos(Apa9)) / 2 ' 11.04.05
        Rmini = Lrast / Sin((Apa9 - Acs(DB1(NE) / .Dr1(NE))))
        BombMaxiRastp(NE) = Rmini - Sqr(Rmini ^ 2 - Lrast ^ 2)
        If BOMB$ = "O" Then
           Brastp(NE) = BombMaxiRastp(NE) - 0.0001
           If Brastp(NE) < 0.0001 Then Brastp(NE) = 0
        End If
    Else
        '.ApPiede = .ApMola
    End If
End With
End Sub

Private Sub CalcPiedeDenteMv(ZONA As SgrFinType)

' Utilizzato solo da DefinizioneMola
Dim Aux, i, Coeff, XfRastp

With ZONA
   .HtMini = 0
   For i = 1 To NeTot
        Aux = DiObtenu(Z1(i), HELB(i), DB1(i), .Eb1(i), .MrMola, .Sw, .Hkw)
        Aux = (DE1(i) - Aux) / 2
        If Aux > .HtMini Then .HtMini = Aux
   Next i
   .ApRastp = Fndr(.ApRastpD)
   .DiObt(NE) = DiObtenu(Z1(NE), HELB(NE), DB1(NE), .Eb1(NE), .MrMola, .Sw, .Hkw)
   If .Htotale > .HtMini * 0.8 Then
      GoSub CalcIcfRpieno
   Else
      Coeff = 0.2
      Do
         .Htotale = Int((.HtMini + Coeff * Mr + 0.1) * 10) / 10
         GoSub CalcIcfRpieno
         If Coeff = 0 Or .Icf > 0 Then
            '
         Else
            Coeff = Coeff - 0.01
            If Coeff < 0 Then
               Coeff = 0
            End If
         End If
      Loop While Not (Coeff = 0 Or .Icf > 0)
   End If
   
   Exit Sub
   
CalcIcfRpieno:
   .Hfw = .Htotale - .Hkw
   If Rastp$ = "O" Or BOMB$ = "O" Then
      Aux = .HRastp - .Hkw
      If .Rrastp = 0 Then
         .Icf = .Sw - Aux * 2 * Tan(.Apmola) - 2 * (.Hfw - Aux) * Tan(.ApRastp)
      Else
         XfRastp = .XrRastp - Sqr(.Rrastp ^ 2 - (.YfRastp - .YrRastp) ^ 2)
         .Icf = .Sw * 2 - XfRastp * 2 - (.Hfw - .YfRastp) * 2 * Tan(.ApPiede)
      End If
   Else
      .Icf = .Sw - .Hfw * 2 * Tan(.Apmola)
   End If
   If Rastp$ = "O" Or BOMB$ = "O" Then
      If .Rrastp = 0 Then
         .RfPieno = .Icf / 2 / Tan(PI / 4 - .ApRastp / 2)
      Else
         .RfPieno = .Icf / 2 / Tan(PI / 4 - .ApPiede / 2)
      End If
   Else
      .RfPieno = .Icf / 2 / Tan(PI / 4 - .Apmola / 2)
   End If

Return
End With
End Sub

Private Sub DefinizioneMola(ZONA As SgrFinType, TipoZona$)
' Utilizzato solo da Sub Definizione
Dim A$, Aux
Dim i, Lrast
Dim dx, E1, E2, R0, Xr0, Yr0, ALFA, Xt, Yt, RX, Y, xe, ye


With ZONA
    Call CalcoloCremTeoric(ZONA, .ApMolaD)
    ' In CalcoloCremTeoric si calcolano i dati di rotolamento
    ' + i dati della cremagliera di base
    ' + rastremazione testa ( in MolaVit : solo se .Hrastt = 0 )
    ' + rastremazione piede ( in MolaVit : solo se .Hrastp = 0 )
    
    If Rastt$ = "O" Or BOMB$ = "O" Then
        ' RASTREMAZIONE PIEDE INGRANAGGIO  ( TESTA MOLA )
        ' 1 ) Calcolo mola : Raggio Bombatura , A.P.tra testa e Rastr
        ' 2 ) Calcolo profilo ottenuto
        .ERasttObt(NE, 0) = Erastt(NE)
        If Brastt(NE) = 0 Then
            .Rrastt = 0: .YrRastt = 0: .XrRastt = 0
            ' .DbRastt(Ne) et .EbRastt(Ne) sont Calcules dans Sub CalcoloCremTeoric
            For i = 1 To NbpRast
                dx = DminiRastt(NE) + (.DmaxiRasttObt(NE) - DminiRastt(NE)) / (NbpRast - 1) * (i - 1)
                E1 = SpessSurDiam(.Eb1(NE), DB1(NE), dx)
                E2 = SpessSurDiam(.EbRastt(NE), .DbRastt(NE), dx)
                .RRasttObt(NE, i) = dx / 2
                .ERasttObt(NE, i) = (E1 - E2) / 2 * DB1(NE) / dx
            Next i
            .ApTesta = .ApRastt
        Else
            ' Calcolo Raggio della bombatura ( .Rrastt , .YrRastt , .XrRastt )
            Lrast = ((.YfRastt - .YiRastt) / Cos(.ApRastt)) / 2
            .Rrastt = (Lrast ^ 2 + Brastt(NE) ^ 2) / 2 / Brastt(NE)
            .YrRastt = .YiRastt + Lrast * Cos(.ApRastt) + (.Rrastt - Brastt(NE)) * Sin(.ApRastt)
            .XrRastt = .XiRastt - Lrast * Sin(.ApRastt) + (.Rrastt - Brastt(NE)) * Cos(.ApRastt)
            R0 = -.Rrastt
            Yr0 = .YrRastt - .Hkw + .S0(NE)
            Xr0 = .XrRastt
            For i = 1 To 10
                ' Definition de .YfRastt
                ALFA = Acs((.YrRastt - .YfRastt) / .Rrastt)
                Call CalcPntConjRayon(.Dr1(NE), .Helr(NE), R0, Yr0, Xr0, ALFA, Xt, Yt, "P", RX)
                If Rastt$ = "O" Then
                   .YfRastt = .YfRastt - DminiRastt(NE) / 2 + RX
                Else
                   .YfRastt = .YfRastt - DbombMini(NE) / 2 + RX
                End If
            Next i
            .ApTesta = Asn((.YrRastt - .YfRastt) / .Rrastt)
            
            ' Calcolo Profilo Ottenuto
            
            For i = 1 To NbpRast
                Y = .YfRastt - (.YfRastt - .YiRastt) / (NbpRast - 1) * (i - 1)
                ALFA = Acs((.YrRastt - Y) / .Rrastt)
                R0 = -.Rrastt
                Yr0 = .YrRastt - .Hkw + .S0(NE)
                Xr0 = .XrRastt
                Call CalcPntConjRayon(.Dr1(NE), .Helr(NE), R0, Yr0, Xr0, ALFA, Xt, Yt, "P", RX)
                Call PntEvolv(.Eb1(NE), DB1(NE), RX * 2, xe, ye, "P")
                .ERasttObt(NE, i) = (xe - Xt) * DB1(NE) / (RX * 2)
                .RRasttObt(NE, i) = RX
            Next i
        End If
        For i = 1 To NbpRast
            'Debug.Print i; "    "; Format(.RRasttObt(Ne,i) * 2, "#.###0"); "    ";
            'Debug.Print Format(.ERasttObt(Ne,i), "0.###0")
        Next i
    End If
    If Rastp$ = "O" Or BOMB$ = "O" Then
        ' RASTREMAZIONE TESTA INGRANAGGIO  ( PIEDE MOLA )
        ' Calcolo Raggio Bombatura , A.P.tra fondo e Rastr
        ' Calcolo profilo ottenuto con mola specifica
        .ErastpObtDmaxiRastp(NE) = Erastp(NE)
        If Brastp(NE) = 0 Then
            .Rrastp = 0: .YrRastp = 0: .XrRastp = 0
            ' .DbRastp(Ne) et .EbRastp(Ne) sont Calcules dans Sub CalcoloCremTeoric
            For i = 1 To NbpRast
                dx = .DminiRastpObt(NE) + (DmaxiRastp(NE) - .DminiRastpObt(NE)) / (NbpRast - 1) * (i - 1)
                E1 = SpessSurDiam(.Eb1(NE), DB1(NE), dx)
                E2 = SpessSurDiam(.EbRastp(NE), .DbRastp(NE), dx)
                .RRastpObt(NE, i) = dx / 2
                .ERastpObt(NE, i) = (E1 - E2) / 2 * DB1(NE) / dx
            Next i
            .ApPiede = .ApRastp
        Else
            ' Calcolo Raggio della bombatura ( .Rrastp , .YrRastp , .XrRastp )
            Lrast = ((.YfRastp - .YiRastp) / Cos(.ApRastp)) / 2
            .Rrastp = (Lrast ^ 2 + Brastp(NE) ^ 2) / 2 / Brastp(NE)
            .YrRastp = .YiRastp + Lrast * Cos(.ApRastp) - (.Rrastp - Brastp(NE)) * Sin(.ApRastp)
            .XrRastp = .XiRastp + Lrast * Sin(.ApRastp) + (.Rrastp - Brastp(NE)) * Cos(.ApRastp)
            R0 = -.Rrastp: Yr0 = -(.YrRastp + .Hkw - .S0(NE)): Xr0 = .XrRastp
            For i = 1 To 10
                ' Definition de .YfRastp
                ALFA = Acs((.YfRastp - .YrRastp) / .Rrastp)
                Call CalcPntConjRayon(.Dr1(NE), .Helr(NE), R0, Yr0, Xr0, ALFA, Xt, Yt, "P", RX)
                If Rastp$ = "O" Then
                   .YfRastp = .YfRastp + DmaxiRastp(NE) / 2 - RX
                Else
                   .YfRastp = .YfRastp + DbombMaxi(NE) / 2 - RX
                End If
            Next i
            .ApPiede = Asn((.YfRastp - .YrRastp) / .Rrastp)
            ' Calcolo Profilo Ottenuto
            For i = 1 To NbpRast
                Y = .YiRastp + (.YfRastp - .YiRastp) / (NbpRast - 1) * (i - 1)
                ALFA = Acs((Y - .YrRastp) / .Rrastp)
                R0 = -.Rrastp: Yr0 = -(.YrRastp + .Hkw - .S0(NE)): Xr0 = .XrRastp
                Call CalcPntConjRayon(.Dr1(NE), .Helr(NE), R0, Yr0, Xr0, ALFA, Xt, Yt, "P", RX)
                Call PntEvolv(.Eb1(NE), DB1(NE), RX * 2, xe, ye, "P")
                .ERastpObt(NE, i) = (xe - Xt) * DB1(NE) / (RX * 2)
                .RRastpObt(NE, i) = RX
            Next i
        End If
        For i = 1 To NbpRast
            'Debug.Print i; "    "; Format(.RRastpObt(Ne,i) * 2, "#.###0"); "    ";
            'Debug.Print Format(.ERastpObt(Ne,i), "0.###0")
        Next i
    End If
    If BOMB$ = "O" Then
       .DbombMedioOtt(NE) = (.DmaxiRasttObt(NE) + .DminiRastpObt(NE)) / 2
    End If
    Call CalcRtPieno(ZONA, TipoZona$)
    If .Rtesta = 0 Then .Rtesta = Int(Mr + 1) / 5
    If .Rtesta > .RtPieno Then .Rtesta = .RtPieno - 0.001
    If .Rtesta < 0 Then .Rtesta = 0
    ' Calcul du diametre actif
    If Rastt$ = "O" Or BOMB$ = "O" Then
       ' NO : HO CAMBIATO IDEA
       ' Call CalcoloDAttivoDaMola(Zona,.EbRastt(Ne), .DbRastt(Ne), .InterfF(Ne), .DactObt(Ne), .AngTroc(Ne))
       Call CalcoloDAttivoDaMola(ZONA, .Eb1(NE), DB1(NE), .InterfF(NE), .DactObt(NE), .AngTroc(NE))
    Else
       Call CalcoloDAttivoDaMola(ZONA, .Eb1(NE), DB1(NE), .InterfF(NE), .DactObt(NE), .AngTroc(NE))
    End If
    Call CalcPiedeDenteMv(ZONA)
    If .RfPieno < 0.2 Then
Rem       A$ = Parola("6031", TabUtente$) & vbCrLf & vbCrLf & "     = " ' "ATTENZIONE : RAGGIO FONDO PIENO"
Rem       A$ = A$ & Format(.RfPieno, "##0.##0")
       If .RfPieno < 0 Then
Rem          A$ = A$ & "    " & Parola("6032", TabUtente$) ' "(  NEGATIVO  )"
        '  MsgBox ("RfPieno NEGATIVO")
        Write_Dialog "ERROR in MOLAVITE02 : Root diameter < 0 (Err04)" '& Err
          Exit Sub
       Else
Rem          A$ = A$ & "     " & Parola("6033", TabUtente$) ' "(  TROPPO PICCOLO  )"
          'MsgBox ("RfPieno TROPPO PICCOLO")
          Write_Dialog "ERROR in MOLAVITE02 : Root diameter too small (Err05)" '& Err
       End If
    End If
    If .Rfondo = 0 Then .Rfondo = Int(Mr + 1) / 5
    If .Rfondo > .RfPieno Then .Rfondo = .RfPieno - 0.001
    If .Rfondo < 0 Then .Rfondo = 0
    Call CalcDsmussoOttenutoDaMola(ZONA, TipoZona$)
End With
End Sub
Public Sub Init_AntiTwist_MV()
'-------------------------------------
Dim INCOMINGS(1 To 20)
'-------------------------------------
' RULLO
 INCOMINGS(1) = LEP("DRULLOPROF_G") 'FIN.Drullo     '= 120                     ' Diametro rullo
 INCOMINGS(2) = Get_Worm_Tilt("GRADI") 'LEP("INCLMOLA_G") 'FIN.IncRulloD  '= 1.8356               ' Inclinazione rullo
' Mola VITE
 INCOMINGS(3) = 2 * val(OPC_LEGGI_DATO("/ACC/NCK/GUD7/REXTMOLA_G"))   ' 176 'Lp("REXTMOLA_G")
 INCOMINGS(4) = Get_Worm_Tilt("GRADI") 'LEP("INCLMOLA_G")    '= 1.836 '
 INCOMINGS(5) = LEP("NUMPRINC_G")  'FIN.Nfil       '= 2                    ' Numero filetti
 INCOMINGS(6) = LEP("PassoMola_G")   '= 17.1705 '
 INCOMINGS(7) = LEP("ALFAUT_G") 'FIN.ApMolaD    '= 21
 INCOMINGS(8) = LEP("MNUT_G")    'FIN.MrMola     '= 2.73137                 ' Cre.Rif.Mola : Modulo Normale MODULO_MOLA
' DISEGNO
 INCOMINGS(9) = LEP("ALFA_G") ' Aprd           '= 21
 INCOMINGS(10) = LEP("BETA_G") 'HelpD(NE)     '= 33.5
 INCOMINGS(11) = LEP("MN_G")    'LEP("MN_G") 'Mr            '= 2.73137 MODULO NORMALE
 INCOMINGS(12) = LEP("MN_G") 'LEP("ALFA_G") 'Mapp(NE)      '= 2.73137 MODULO APPARENTE
 INCOMINGS(13) = LEP("NumDenti_G")  'Z1(NE)        '= 67
 INCOMINGS(14) = LEP("QW") 'Wdt(NE)       '= 31.7252
 INCOMINGS(15) = LEP("ZW") 'Kdt(NE)       '= 4
 INCOMINGS(16) = LEP("DEXT_G")       '= 224.52
 INCOMINGS(17) = LEP("DINT_G")       '= 208.55
 INCOMINGS(18) = LEP("DSAP_G") 'SAP(NE)       '= 211
' Dim Fascia1  As Double: Dim Fascia2 As Double: Dim Bomb1 As Double: Dim Bomb2 As Double
' Call CALCOLA_BOMBATURA_EQUIVALENTE(Fascia1, Fascia2, Bomb1, Bomb2)
' INCOMINGS(19) = (Fascia1 + Fascia2) / 2
' INCOMINGS(20) = (Bomb1 + Bomb2) / 2
 
INCOMINGS(19) = (LEP("iFas1Fsx") + LEP("iFas1Fdx")) / 2
INCOMINGS(20) = (LEP("iBom1Fsx") + LEP("iBom1Fdx")) / 2
'-------------------------------------
Call Fill_AntiTwist_MV_paramenter(INCOMINGS)
End Sub
'--------------------------------------
' ROUTINE per popolare il modulo calcolo spostamenti profilatura
' RULLO
' INCOMINGS(01) = FIN.Drullo    '= 120                     ' Diametro rullo
' INCOMINGS(02) = FIN.IncRulloD '= 1.8356               ' Inclinazione rullo
' MOLA VITE
' INCOMINGS(03) = FIN.DmolaV    '= 176 '
' INCOMINGS(04) = FIN.InFilD    '= 1.836 '
' INCOMINGS(05) = FIN.Nfil      '= 2                    ' Numero filetti
' INCOMINGS(06) = FIN.PasAxial  '= 17.1705 '
' INCOMINGS(07) = FIN.ApMolaD   '= 21
' INCOMINGS(08) = FIN.MrMola    '= 2.73137                 ' Cre.Rif.Mola : Modulo Normale
' DISEGNO
' INCOMINGS(09) = Aprd          '= 21
' INCOMINGS(10) = HelpD(NE)     '= 33.5
' INCOMINGS(11) = Mr            '= 2.73137 ' MODULO NORMALE
' INCOMINGS(12) = Mapp(NE)      '= 2.73137
' INCOMINGS(13) = Z1(NE)        '= 67
' INCOMINGS(14) = Wdt(NE)       '= 31.7252
' INCOMINGS(15) = Kdt(NE)       '= 4
' INCOMINGS(16) = DE1(NE)       '= 224.52
' INCOMINGS(17) = Di1(NE)       '= 208.55
' INCOMINGS(18) = SAP(NE)       '= 211
' INCOMINGS(19) = FASCIA(NE)
' INCOMINGS(20) = BombElica(NE)
'-------------------------------------
Public Sub Fill_AntiTwist_MV_paramenter(ByRef INCOMINGS())

NE = 1
NeTot = 1
PI = 4 * Atn(1)

' CORREZIONE ANGOLO PRESSIONE
' CorrEvolv1 = 31 ' micron
' CorrEvolv2 = 31 ' micron

Call Azzerare_Ing_Cr_Mf
Call AzzerareCremRefMola(FIN)

' RULLO
FIN.Drullo = INCOMINGS(1)       '120                     ' Diametro rullo ?
FIN.IncRulloD = INCOMINGS(2)  '1.8356        2.36 '       ' Inclinazione rullo
' MOLA VITE
FIN.DmolaV = INCOMINGS(3)  '176 '
FIN.InFilD = INCOMINGS(4)  '1.836 '2.36 '
FIN.InFil = FIN.InFilD * PI / 180
FIN.NFil = INCOMINGS(5)  '2                    ' Numero filetti
FIN.PasAxial = INCOMINGS(6)  '17.1705 '
FIN.ApMolaD = INCOMINGS(7)  '21
FIN.MrMola = INCOMINGS(8)  '2.73137                 ' Cre.Rif.Mola : Modulo Normale
' DISEGNO
Aprd = INCOMINGS(9)  '21
HelpD(NE) = INCOMINGS(10)  '33.5
Mr = INCOMINGS(11)  '2.73137
Mapp(NE) = INCOMINGS(12)  '2.73137
Z1(NE) = INCOMINGS(13)  '67
Wdt(NE) = INCOMINGS(14)  '31.7252
Kdt(NE) = INCOMINGS(15)  '4
DE1(NE) = INCOMINGS(16)  '224.52
Di1(NE) = INCOMINGS(17)  '208.55
SAP(NE) = INCOMINGS(18)  '211

'lamdaRad, betaRad, RextMV, RextG, I
'FIN.InFil,Fndr(HelpD(NE)), FIN.DmolaV/2,DE1(NE)/2, Interasse
'If Rastt$ <> "O" Then Rastt$ = "N"
'If Rastp$ <> "O" Then Rastp$ = "N"
'If BOMB$ <> "O" Then BOMB$ = "N"
'TipoZona$ = "Fin"
'-----------------calcolo
'Call CalcoloIngranaggioPiuFm(NE)
'Call DefinizioneMola(FIN, TipoZona$)
FIN.DpMola = FIN.DmolaV - FIN.MrMola * 2
FASCIA(NE) = INCOMINGS(19)
BombElica(NE) = INCOMINGS(20)
INTERASSE = (FIN.DmolaV + SAP(NE)) / 2

End Sub
'------------------------------------------------------------------
Private Function POT(arg): POT = arg ^ 2: End Function
'------------------------------------------------------------------
Private Sub PRF_VIT_PINOU_F(ByVal FW_GEAR As Double, ByVal FW_INSPC As Double, ByRef P_IN As Double, ByRef P_OU As Double)
 Dim P_I#
 Dim DISTRIBUZ#
 P_I = FW_INSPC / FW_GEAR
 ' * CAMBIA (DISTRIBUZ) PER ISPEZIONI A QUOTE FASCIA ASIMEMTRICHE  *
 DISTRIBUZ = 0.5
 P_IN = (1 - P_I) * DISTRIBUZ
 P_OU = P_IN
End Sub
'------------------------------------------------------------------
Private Sub PRF_VIT_BARRELING_BOE(ByVal BOE_IN As Double, ByVal BOE_OU As Double, ByVal FW_GEAR As Double, ByVal FW_INSPC As Double, ByRef rBOE As Double, ByRef pBOEa As Double, ByRef pBOEr As Double)
';*****************************************************************
';* Calcola Raggio e la %fascia di Bombatura d'Elica asimmetrica  *
';* GBERTACCHI@SAMP@2016.07.25                                    *
';* DESCRIZIONE:                                                  *
';* Nota la lunghez. di fascia e  d'ispezione (FW_INSPC), l'enti= *
';* t� di bombatura che si vuole realizzare all'inizio (BOE_IN) e *
';* alla fine (BOE_OUT) � possibile calcolare la %fascia di max   *
';* bombatura (pBOE) e il raggio di curvatura (rBOE)              *
';* VERSIONE: v1                                                  *
';*  |<            FW_GEAR                                    >|  *
';*         |<       FW_INSPC                          >|         *
';*  |<P_IN>|<      PA                      >|<  PR    >|<P_OU>|  *
';*  |------|--------------------------------|----------|------|  *
';*         ^                                           ^         *
';*       BOE_IN                                      BOE_OU      *
';*                                                               *
';* REFERENCE:                                                    *
';* [https://it.wikipedia.org/wiki/Equazione_di_secondo_grado]    *
';*****************************************************************
 Dim Pa(2) As Double
 Dim Pr(2) As Double
 Dim P_IN#, P_OU#
 Dim i%, ix%
 Dim B_sus_A#, C_sus_A#, DTA#
 
 Call PRF_VIT_PINOU_F(FW_GEAR, FW_INSPC, P_IN, P_OU)
 ix = 0
 If (BOE_IN = BOE_OU) Then
  Pa(0) = (1 - (P_IN + P_OU)) / 2: Pa(1) = Pa(0)
  Pr(0) = 1 - (P_IN + P_OU + Pa(0)): Pr(1) = Pr(0)
 Else
  B_sus_A = BOE_IN * (1 - (P_IN + P_OU)) / (BOE_IN - BOE_OU)
  C_sus_A = -BOE_IN * BOE_OU / POT(FW_GEAR) + B_sus_A * (1 - (P_IN + P_OU))
  DTA = POT(B_sus_A) - C_sus_A
  If DTA < 0 Then
   MsgBox vbCritical, "BARRELING: CROWN CONBINATION NOT ADMITTED!!"
   Pa(0) = (1 - (P_IN + P_OU)) / 2: Pa(1) = Pa(0)
   Pr(0) = 1 - (P_IN + P_OU + Pa(0)): Pr(1) = Pr(0)
  Else
   For i = 0 To 1
    Pa(i) = B_sus_A + (1 - 2 * i) * Sqr(DTA): Pr(i) = 1 - (P_IN + P_OU + Pa(i))
    If (Pa(i) > 0) And (Pa(i) < 1) Then ix = i
   Next i
  End If
 End If
 pBOEa = Pa(ix)
 pBOEr = Pr(ix)
 If (BOE_IN <> 0) Then
  rBOE = POT(pBOEa * FW_GEAR) / (2 * BOE_IN) + BOE_IN / 2
 Else
  rBOE = POT(pBOEr * FW_GEAR) / (2 * BOE_OU) + BOE_OU / 2
 End If
End Sub
'------------------------------------------------------------------
Private Sub PRF_VIT_BARRELING_F(ByVal BOE_IN As Double, ByVal BOE_OU As Double, ByVal FW_GEAR As Double, ByVal FW_INSPC As Double, ByVal FW_IN As Double, ByVal FW_OU As Double, ByVal UNIT As Double, _
ByRef Crown As Double)
';*****************************************************************
';* Calcola Entita Bombatura di Elica secondo la corsa dell'asseZ *
';* GBERTACCHI@SAMP@2016.07.25                                    *
';* DESCRIZIONE:                                                  *
';* Da sperimentazione [2016/07/21] si � osservato che la varia = *
';* zione dentro valori nominali non � sufficiente per generare la*
';* modifica di elica voluta.                                   *
';* Si estrae valore di bobmatura di elica al variare della corsa *
';* asse Z adimensionale                                          *
';*****************************************************************
 Dim rBOE#, pBOEa#, pBOEr#
 Dim P_IN#, P_OU#
 Dim cIN#, cOU#
 Dim TH_S#, TH_F#
 Dim cZ# ' Corsa asse Z
 Dim GRD#
 
 Call PRF_VIT_BARRELING_BOE(BOE_IN, BOE_OU, FW_GEAR, FW_INSPC, rBOE, pBOEa, pBOEr)
 Call PRF_VIT_PINOU_F(FW_GEAR, FW_INSPC, P_IN, P_OU)
 
 cIN = FW_IN + P_IN * FW_GEAR + pBOEa * FW_GEAR
 cOU = pBOEr * FW_GEAR + P_OU * FW_GEAR + FW_OU

 TH_S = ASIN(-cIN / rBOE)
 TH_F = ASIN(cOU / rBOE)

 Crown = -rBOE * (Cos(TH_S + (TH_F - TH_S) * UNIT) - 1)
End Sub
'------------------------------------------------------------------
Sub CalcoloMovimentiXZ()

Dim ZONA As SgrFinType
Dim CorrEvolv1 As Double
Dim CorrEvolv2 As Double
Dim DZ1 As Double
Dim DX1 As Double
Dim EP1 As Double
Dim DZ2 As Double
Dim DX2 As Double
Dim EP2 As Double

' Call AzzerareCremRefMola(Sgr)
' Call AzzerareCremRefMola(Ing)
' Call AzzerareCremRefMola(Zona)
Rem Rastt$ = "O" Rastremazione Piede Ottenuta
Rem BOMB$ = "O" Bombatura piede Ottenuta

NE = 1
NeTot = 1
PI = 4 * Atn(1)

' CORREZIONE ANGOLO PRESSIONE
' CorrEvolv1 = 31 ' micron
' CorrEvolv2 = 31 ' micron

Call Azzerare_Ing_Cr_Mf
Call AzzerareCremRefMola(FIN)

' RULLO
FIN.Drullo = 120                     ' Diametro rullo ?
FIN.IncRulloD = 1.8356               ' Inclinazione rullo
' MOLA VITE
FIN.DmolaV = 176 '
FIN.InFilD = 1.836 '
FIN.InFil = FIN.InFilD * PI / 180
FIN.NFil = 2                    ' Numero filetti
FIN.PasAxial = 17.1705 '
FIN.ApMolaD = 21
FIN.MrMola = 2.73137                 ' Cre.Rif.Mola : Modulo Normale
' DISEGNO
Aprd = 21
HelpD(NE) = 33.5
Mr = 2.73137
Mapp(NE) = 2.73137
Z1(NE) = 67
Wdt(NE) = 31.7252
Kdt(NE) = 4
DE1(NE) = 224.52
Di1(NE) = 208.55
SAP(NE) = 211

' Non usato

' Fin.Sw = 2.9476 '4.29
' Fin.Helr(Ne) = 0.584   ' Ingranaggio : Angolo elica su diametro di rotolamento (in radianti)
' Fin.Dr1(Ne) = 219.4566 ' Ingranaggio : Diametro di rotolamento (OUTPUT)
' Fin.NptFiancoMini = 19 ' della cremagliera Mv ' ininfluente su calcolo
' Fin.Eb1(Ne) = 0.1
' Fin.Profilatura = "NORM"
' Fin.RastpMola = "N"
' Fin.RasttMola = "N"
' Fin.FormRullo = "SFF"
' Zona.InFil = 1.2 '
' Fin.NbpTotale = 32 ' Numero punti della cremagliera Mv
' Zona.DpMola = 210 '
' Fin.ApMola = 0.3663                  ' Cre.Rif.Mola : Ang.Di Press.Normale in radianti
' Fin.Rrull(Ne) = 0                    ' Raggio rullo
' Fin.Xrull(Ne) = 0                    ' X rullo?
' Fin.ApRullD(Ne) = 0                  ' Angolo pressione rullo D?
' Fin.Hkw = 3.404                      ' Cre.Rif.Mola : Addendum
' Fin.Hfw = 4.896                      ' Cre.Rif.Mola : Dedendum
' Fin.Htotale = Zona.Hkw + Zona.Hfw    ' Cre.Rif.Mola : Altezza totale dente
' SovramDent(Ne) = 0.04 ' ininfluente su calcolo
' SovramMv(Ne) = 0 ' ininfluente su calcolo
' BombElica(Ne) = 0.02  ' ininfluente su calcolo
'-----------------------
If Rastt$ <> "O" Then Rastt$ = "N"
If Rastp$ <> "O" Then Rastp$ = "N"
If BOMB$ <> "O" Then BOMB$ = "N"
TipoZona$ = "Fin"
'-----------------calcolo
Call CalcoloIngranaggioPiuFm(NE)
Call DefinizioneMola(FIN, TipoZona$)

FASCIA(NE) = 28
BombElica(NE) = 0.02

Call CalcProfiloOttenutoDaMv(FIN, TipoZona$)

CorrEvolv1 = FIN.SvergolamentoOtt(NE) * 1000 ' 32 ' micron
CorrEvolv2 = CorrEvolv1

Call CalcoloCremTeoric(FIN, AprDdefault)
Call PuntiCrem(FIN, TipoZona$)
Call CalcoloMolaAssiale(FIN)
Call CalcoloRullo(FIN)

Call CalcoloSpostRullo(FIN, CorrEvolv1, CorrEvolv2, DZ1, DX1, EP1, DZ2, DX2, EP2)

End Sub
'--------------------------------------
Sub CalcoloMovimentiXZ1(ByRef dx, ByRef dz, ByRef Lu, ByRef Dy, ByVal iAT_DAP_CORR, ByRef iAT_DAP)

Dim ZONA As SgrFinType
Dim CorrEvolv1 As Double
Dim CorrEvolv2 As Double
Dim DZ1 As Double
Dim DX1 As Double
Dim EP1 As Double
Dim DZ2 As Double
Dim DX2 As Double
Dim EP2 As Double
Dim Lumin#, SF%
' Call AzzerareCremRefMola(Sgr)
' Call AzzerareCremRefMola(Ing)
' Call AzzerareCremRefMola(Zona)
Rem Rastt$ = "O" Rastremazione Piede Ottenuta
Rem BOMB$ = "O" Bombatura piede Ottenuta

NE = 1
NeTot = 1
PI = 4 * Atn(1)

' CORREZIONE ANGOLO PRESSIONE
' CorrEvolv1 = 31 ' micron
' CorrEvolv2 = 31 ' micron

'Call Azzerare_Ing_Cr_Mf
'Call AzzerareCremRefMola(FIN)

If Rastt$ <> "O" Then Rastt$ = "N"
If Rastp$ <> "O" Then Rastp$ = "N"
If BOMB$ <> "O" Then BOMB$ = "N"
TipoZona$ = "Fin"
'-----------------calcolo
Call CalcoloIngranaggioPiuFm(NE)
Call DefinizioneMola(FIN, TipoZona$)

'FASCIA(NE) = 28
'BombElica(NE) = 0.02

Call CalcProfiloOttenutoDaMv(FIN, TipoZona$)
iAT_DAP = FIN.SvergolamentoOtt(NE)
CorrEvolv1 = (iAT_DAP + iAT_DAP_CORR) * 1000 ' 32 ' micron
CorrEvolv2 = CorrEvolv1

Call CalcoloCremTeoric(FIN, AprDdefault)
Call PuntiCrem(FIN, TipoZona$)
Call CalcoloMolaAssiale(FIN)
Call CalcoloRullo(FIN)

Call CalcoloSpostRullo(FIN, CorrEvolv1, CorrEvolv2, DZ1, DX1, EP1, DZ2, DX2, EP2)

dx = DX2
dz = DZ2
Dy = EP2
'FIN.InFil,Fndr(HelpD(NE)), FIN.DmolaV/2,DE1(NE)/2, Interasse
SF = Lp("SensoFil_G"): If (SF <> 0 And SF <> 1) Then Call MsgBox("Worm Verse is not set", "WARNING")
Lumin = LUminima(FIN.InFil, Fndr(HelpD(NE)), FIN.DmolaV / 2, DE1(NE) / 2, INTERASSE, SF)
Lu = Lumin
End Sub
'------------------------------------------------------------------
' CALCOLO FUORICENTRO (Dx,Dy,Dz)E LUNGHEzZA ASSIALE MOLA COINVOLTA (Lu)
'------------------------------------------------------------------
Sub CalcoloMovimentiXZ2(ByRef dx, ByRef dz, ByRef Lu, ByRef Dy, ByVal iAT_DAP_CORR, ByVal iAT_DAP)
'------------------------------------------------------------------
 Dim ZONA As SgrFinType
 Dim CorrEvolv1 As Double
 Dim CorrEvolv2 As Double
 Dim DZ1#, DX1#, EP1#
 Dim DZ2#, DX2#, EP2#
 Dim Lumin#, SF%
' Call AzzerareCremRefMola(Sgr)
' Call AzzerareCremRefMola(Ing)
' Call AzzerareCremRefMola(Zona)
Rem Rastt$ = "O" Rastremazione Piede Ottenuta
Rem BOMB$ = "O" Bombatura piede Ottenuta

 NE = 1
 NeTot = 1
 PI = 4 * Atn(1)

' CORREZIONE ANGOLO PRESSIONE
' CorrEvolv1 = 31 ' micron
' CorrEvolv2 = 31 ' micron

'Call Azzerare_Ing_Cr_Mf
'Call AzzerareCremRefMola(FIN)

If Rastt$ <> "O" Then Rastt$ = "N"
If Rastp$ <> "O" Then Rastp$ = "N"
If BOMB$ <> "O" Then BOMB$ = "N"
TipoZona$ = "Fin"
'-----------------calcolo
Call CalcoloIngranaggioPiuFm(NE)
Call DefinizioneMola(FIN, TipoZona$)
'------------------------------------------------------------------
Call CalcProfiloOttenutoDaMv(FIN, TipoZona$)
CorrEvolv1 = (iAT_DAP + iAT_DAP_CORR) * 1000 ' 32 ' micron
CorrEvolv2 = CorrEvolv1
'------------------------------------------------------------------
Call CalcoloCremTeoric(FIN, AprDdefault)
Call PuntiCrem(FIN, TipoZona$)
Call CalcoloMolaAssiale(FIN)
Call CalcoloRullo(FIN)
'------------------------------------------------------------------
Call CalcoloSpostRullo(FIN, CorrEvolv1, CorrEvolv2, DZ1, DX1, EP1, DZ2, DX2, EP2)
'------------------------------------------------------------------
dx = DX2
dz = DZ2
Dy = EP2
'FIN.InFil,Fndr(HelpD(NE)), FIN.DmolaV/2,DE1(NE)/2, Interasse
SF = Lp("SensoFil_G"): If (SF <> 0 And SF <> 1) Then Call MsgBox("Worm Verse is not set", "WARNING")
Lumin = LUminima(FIN.InFil, Fndr(HelpD(NE)), FIN.DmolaV / 2, DE1(NE) / 2, INTERASSE, SF)
Lu = Lumin
'------------------------------------------------------------------
End Sub
'------------------------------------------------------------------
' calcolo fha
'------------------------------------------------------------------
Sub CalcoloSvergolamento(Boom1, Boom2, ByRef CorrEvolv)

'Dim Boom1, Boom2 As Double

Dim ZONA As SgrFinType
'Dim CorrEvolv1 As Double
Dim CEV(1 To 3) As Double


' Call AzzerareCremRefMola(Sgr)
' Call AzzerareCremRefMola(Ing)
' Call AzzerareCremRefMola(Zona)
Rem Rastt$ = "O" Rastremazione Piede Ottenuta
Rem BOMB$ = "O" Bombatura piede Ottenuta

NE = 1
NeTot = 1
PI = 4 * Atn(1)

' CORREZIONE ANGOLO PRESSIONE
' CorrEvolv1 = 31 ' micron
' CorrEvolv2 = 31 ' micron

'Call Azzerare_Ing_Cr_Mf
'Call AzzerareCremRefMola(FIN)

If Rastt$ <> "O" Then Rastt$ = "N"
If Rastp$ <> "O" Then Rastp$ = "N"
If BOMB$ <> "O" Then BOMB$ = "N"
TipoZona$ = "Fin"
'-----------------calcolo
BombElica(NE) = Boom1
  'FASCIA(NE) = 2 * (Lp("FASCIATOTALE_G") / 2 - Lp("SPC_TW[" & i & ",0]"))
Call CalcoloIngranaggioPiuFm(NE)
Call DefinizioneMola(FIN, TipoZona$)
Call CalcProfiloOttenutoDaMv(FIN, TipoZona$)
Dim M#
M = -2# * FIN.SvergolamentoOtt(NE) * 1000 / Lp("FASCIATOTALE_G") ' 32 ' micron
For i = 0 To 2
CEV(i + 1) = M * Lp("SPC_TW[" & i & ",0]") + FIN.SvergolamentoOtt(NE) * 1000'CEV(i + 1) = FIN.SvergolamentoOtt(NE) * 1000 ' 32 ' micron
Next i
CorrEvolv(1) = CEV(1)
CorrEvolv(2) = CEV(2)
CorrEvolv(3) = CEV(3)

End Sub

Private Sub InputDatiMolaVite()
'INPUT 01: Diametro mola
'INPUT 02: Numero filetti
'OUTPUT 01: incl filetto
'OUTPUT 02: Passo assiale

'INPUT 01: gioco tra fondo ingranaggio e mola
'INPUT 02: angolo di pressione rotolamento
'INPUT 03: raggio testa mola
'INPUT 04: altezza totale dente
'INPUT 05: raggio fondo mola
End Sub

Private Sub InputDisegno()
'INPUT 01: Mr            -    - Modulo normale
'INPUT 02: Aprd          - �  - Angolo pressione normale
'INPUT 03: Z1(Ne)        -    - Numero denti
'INPUT 04: Helpd(Ne)     - �  - Elica primitiva
'INPUT 05: De1(Ne)       - mm - Diametro interno
'INPUT 06: Di1(Ne)       - mm - Diametro esterno
'INPUT 07: Sap           - mm - Diametro attivo di piede
'INPUT 08: Epr1(Ne)      - mm - Spessore primitivo normale
'INPUT 09: Kdt(Ne)       -    - Numero Denti Per Controllo quota W
'INPUT 10: quota whildhaber su n denti
'INPUT 10: Dpg(Ne)       - mm - Ingranaggio : Diametro rulli o sfere per controllo
'INPUT 12: Fascia(Ne)    - mm - fascia
'INPUT 13: BombElica(Ne) - mm - bombatura di elica
End Sub
Private Sub OutpuDisegno()
'OUTPUT 01: Mapp(Ne)     - mm - modulo assiale
'OUTPUT 02: ApApp(Ne)    - �  - angolo di pressione assiale
'OUTPUT 03: Dp1(Ne)      - mm -  diametro primitivo
'OUTPUT 04: Db1(Ne)      - mm -  diametro base
'OUTPUT 05: Ing.Eb1(Ne)  - mm -  spessore base
'OUTPUT 06: Helb(Ne)     - �  - elica i base
'OUTPUT 07: Kdep(Ne)    - mm - spostamento (X)
'OUTPUT 08: Es1(Ne) * Cos(Atn(Tan(Helb(Ne)) * De1(Ne) / Db1(Ne)))  - mm -  spessore testa normale
End Sub
Private Sub InputDatiDentaturaCreatore()
' INPUT 01: SovramDent(Ne)    - mm - sovramentrallo normale per fianco
' INPUT 02: ApFmD(Ne)         - �  - CR: angolo pressione a modulo 2.1314
' INPUT 03: EpProtu(Ne)       - mm - CR: entit� di protuberanza: 0
' INPUT 04: ApProtuD(Ne)      - �  - CR: angolo pressione protuberanza: 0
' INPUT 05: RtFm(Ne)          - mm -  CR: raggio di testa (pieno  = 0.990) : 0.4
' INPUT 06: AddFm(Ne)         - mm -  CR: addendum per spessore 4.290: 3.8158
' INPUT 07: Diametro interno / Diametro Semitopping 208.55/
' INPUT 08: DedSt(Ne)         - mm -  CR: Dedeundum semitopping: 3.9805
' INPUT 09: ApStD(Ne)         - �  - CR: Angolo pressione  semitopping: 55.5
End Sub
Private Sub InputIngranaggioEcrsgrossatura()
  Call InputDisegno
  Call OutpuDisegno
  Call InputDatiDentaturaCreatore
End Sub
Private Sub FrmMola()

End Sub
'-----------------------------------------------------------
'
' routine calcolo fHa caso
'
'-----------------------------------------------------------
Private Function LUminima(ByVal lamdaRad#, ByVal betaRad#, ByVal RextMV#, ByVal RextG#, ByVal i#, ByVal SF%) As Double
'RIFERIMENTI: [http://dit1nb38/index.php?option=com_wrapper&view=wrapper&Itemid=484]
 Dim Delta#
 Dim Csi#
 Dim Phi#
 Dim ApSensoFil_G# ' APSENSOFIL_G SENSO FILETTO (-1=SX,+1=DX)
 
 ApSensoFil_G = 1# - SF * 2#  'SensoFil_G SENSO FILETTATURA (1=Sx 0=Dx)
 
 Delta = RextMV + RextG - i
 Csi = Acs(1# - Delta / RextMV)
 Phi = PG / 2# - (lamdaRad + ApSensoFil_G * betaRad)
 LUminima = 2# * RextMV * Sin(Csi) * Sin(Phi)
 
 
End Function
'-----------------------------------------------------------
' CALCOLO BOMBATURA EQUIVALENTE
' OUTPUT: SEMIFASCIA (F1,F2)
' OUTPUT: BOMBATURA (F1,F2)
'-----------------------------------------------------------
Public Sub CALCOLA_BOMBATURA_EQUIVALENTE(ByRef Fascia1 As Double, ByRef Fascia2 As Double, ByRef Bomb1 As Double, ByRef Bomb2 As Double)
'
Dim Stampante As Boolean
Dim LatoPart  As Integer
Dim nZone     As Integer
Dim X()       As Double
Dim Y()       As Double
Dim Con1F1    As Double
Dim Con2F1    As Double
Dim Con3F1    As Double
Dim Con1F2    As Double
Dim Con2F2    As Double
Dim Con3F2    As Double
Dim Bom1F1    As Double
Dim Bom2F1    As Double
Dim Bom3F1    As Double
Dim Bom1F2    As Double
Dim Bom2F2    As Double
Dim Bom3F2    As Double
Dim MaxX1     As Double
Dim MaxX2     As Double
Dim scalaxEl  As Double
Dim Pos       As Double
Dim XF0       As Double
Dim XF1       As Double
Dim XF2       As Double
Dim XF3       As Double
Dim XF0_2     As Double
Dim XF1_2     As Double
Dim XF2_2     As Double
Dim XF3_2     As Double
Dim Ft        As Double
Dim Ft2       As Double
Dim F1        As Double
Dim F2        As Double
Dim F3        As Double
Dim F1_2      As Double
Dim F2_2      As Double
Dim F3_2      As Double
Dim Re        As Double
Dim RS        As Double
Dim LS        As Double
Dim tColore   As Long
Dim tX1       As Double
Dim tX        As Double
Dim tY        As Double
Dim i         As Integer
Dim NotScale  As Boolean
Dim Lin       As Double
Dim CORSA_O   As Double
Dim CORSA_I   As Double
'
On Error Resume Next
  '
'  Call InizializzaVariabili
  '
  NotScale = False
  '
  scalaxEl = 1 'Lp("iScalaX")
'  If (scalaxEl = 0) Then scalaxEl = Lp("iScalaX")
  LatoPart = Lp("LatoPA")
  CORSA_I = Lp("CORSAIN")
  CORSA_O = Lp("CORSAOUT")
  nZone = Lp("iNzone")
  F1 = 0:   F2 = 0:   F3 = 0
  F1_2 = 0: F2_2 = 0: F3_2 = 0
  Select Case nZone
    Case 1
        F1 = CORSA_I + Lp("iFas1Fsx") + CORSA_O: F1_2 = CORSA_I + Lp("iFas1Fdx") + CORSA_O
    Case 2
        F1 = Lp("iFas1Fsx") + CORSA_I: F1_2 = Lp("iFas1Fdx") + CORSA_I
        F2 = Lp("iFas2Fsx") + CORSA_O: F2_2 = Lp("iFas2Fdx") + CORSA_O
    Case 3
        F1 = Lp("iFas1Fsx") + CORSA_I: F1_2 = Lp("iFas1Fdx") + CORSA_I
        F2 = Lp("iFas2Fsx"):           F2_2 = Lp("iFas2Fdx")
        F3 = Lp("iFas3Fsx") + CORSA_O: F3_2 = Lp("iFas3Fdx") + CORSA_O
  End Select
  Re = 10:  RS = 10:  LS = 10
  '
  Ft = F1 + F2 + F3         'Fascia Totale F1
  Ft2 = F1_2 + F2_2 + F3_2  'Fascia Totale F2
' ASCISSE CONTROLLO FASCIA
  XF0 = LS:    XF1 = XF0 + F1:        XF2 = XF1 + F2:        XF3 = XF2 + F3
  XF0_2 = LS:  XF1_2 = XF0_2 + F1_2:  XF2_2 = XF1_2 + F2_2:  XF3_2 = XF2_2 + F3_2
  '
  'Disegno correzioni elica
  ReDim X(45):  ReDim Y(45)
  '
  Con1F1 = Lp("iCon1Fsx"):  Con2F1 = Lp("iCon2Fsx"):  Con3F1 = Lp("iCon3Fsx")
  Con1F2 = Lp("iCon1Fdx"):  Con2F2 = Lp("iCon2Fdx"):  Con3F2 = Lp("iCon3Fdx")
  '
  Bom1F1 = Lp("iBom1Fsx"):  Bom2F1 = Lp("iBom2Fsx"):  Bom3F1 = Lp("iBom3Fsx")
  Bom1F2 = Lp("iBom1Fdx"):  Bom2F2 = Lp("iBom2Fdx"):  Bom3F2 = Lp("iBom3Fdx")
  '
  Pos = 0 'RS
 ' F1
  X(0) = XF0
  X(1) = XF1
  X(2) = XF2
  X(3) = XF3
  X(16) = XF3
  Y(0) = Pos
  Y(1) = Pos + (Con1F1 * 1 + Con2F1 * 0 + Con3F1 * 0) * scalaxEl
  Y(2) = Pos + (Con1F1 * 1 + Con2F1 * 1 + Con3F1 * 0) * scalaxEl
  Y(3) = Pos + (Con1F1 * 1 + Con2F1 * 1 + Con3F1 * 1) * scalaxEl
  Y(16) = Pos
' F2
  X(4) = XF0_2
  X(5) = XF1_2
  X(6) = XF2_2
  X(7) = XF3_2
  X(17) = XF3_2
  Y(4) = -Pos
  Y(5) = -Pos - (Con1F2 * 1 + Con2F2 * 0 + Con3F2 * 0) * scalaxEl
  Y(6) = -Pos - (Con1F2 * 1 + Con2F2 * 1 + Con3F2 * 0) * scalaxEl
  Y(7) = -Pos - (Con1F2 * 1 + Con2F2 * 1 + Con3F2 * 1) * scalaxEl
  Y(17) = -Pos
  'Lineette
'  X(8) = X(1)
'  X(9) = X(1)
'  X(12) = X(5)
'  X(13) = X(5)
'  X(10) = X(2)
'  X(11) = X(2)
'  X(14) = X(6)
'  X(15) = X(6)
'  Lin = 3
'  Y(8) = Y(1) + Lin
'  Y(9) = Y(1) - Lin
'  Y(12) = Y(5) + Lin
'  Y(13) = Y(5) - Lin
'  Y(10) = Y(2) + Lin
'  Y(11) = Y(2) - Lin
'  Y(14) = Y(6) + Lin
'  Y(15) = Y(6) - Lin
'  Dim StepGriglia As Double
'  StepGriglia = 0.05
'  For i = -3 To 3
'    Y(21 + i) = Y(0) + i * StepGriglia * scalaxEl
'    Y(28 + i) = Y(0) + i * StepGriglia * scalaxEl
'    Y(35 + i) = Y(4) + i * StepGriglia * scalaxEl
'    Y(42 + i) = Y(4) + i * StepGriglia * scalaxEl
'    X(21 + i) = XF0
'    X(28 + i) = XF3
'    X(35 + i) = XF0
'    X(42 + i) = XF3
'  Next i
'  PAR.MaxY = Y(0)
'  PAR.MinY = Y(4)
'  For i = 1 To 3
'    If Y(i) > PAR.MaxY Then PAR.MaxY = Y(i)
'    If Y(i + 4) < PAR.MinY Then PAR.MinY = Y(i + 4)
'  Next i
'  If Ft >= Ft2 Then
'    PAR.MaxX = Ft + LS
'  Else
'    PAR.MaxX = Ft2 + LS
'  End If
'  PAR.MinX = LS / 2
'  PAR.ScalaMargine = 0.9
'  PAR.Allineamento = 0
'  Call CalcolaParPic(PAR, True, Stampante)
  'par.Pic.Cls
  'linea XMola, Re + 20 + DiaMola, XMola, Re, vbRed, 1, vbDash
  MaxX1 = X(1)
  MaxX2 = X(5)
'  If Not NotScale Then
'    Select Case nZone 'Disegno linee verticali che delimitano le zone
'      Case 2
'        Call linea(XF1, 0, XF1, Re, RGB(180, 180, 180), 1, vbDash)
' '       Call QuotaH(XF0, Re, XF1, Re, "" & F1, 12, CalColore("iFas1F1"), 3)
' '       Call QuotaH(XF1, Re, XF2, Re, "" & F2, 12, CalColore("iFas2F1"), 3)
'        Call linea(XF1_2, -Re, XF1_2, 0, RGB(180, 180, 180), 1, vbDash)
' '       Call QuotaH(XF0_2, -Re, XF1_2, -Re, "" & F1_2, 12, CalColore("iFas1F2"), 3)
' '       Call QuotaH(XF1_2, -Re, XF2_2, -Re, "" & F2_2, 12, CalColore("iFas2F2"), 3)
'        MaxX1 = X(2)
'        MaxX2 = X(6)
'      Case 3
'        Call linea(XF1, 0, XF1, Re, RGB(180, 180, 180), 1, vbDash)
'        Call linea(XF2, 0, XF2, Re, RGB(180, 180, 180), 1, vbDash)
''        Call QuotaH(XF0, Re, XF1, Re, "" & F1, 12, CalColore("iFas1F1"), 3)
''        Call QuotaH(XF1, Re, XF2, Re, "" & F2, 12, CalColore("iFas2F1"), 3)
''        Call QuotaH(XF2, Re, XF3, Re, "" & F3, 12, CalColore("iFas3F1"), 3)
'        Call linea(XF1_2, -Re, XF1_2, 0, RGB(180, 180, 180), 1, vbDash)
'        Call linea(XF2_2, -Re, XF2_2, 0, RGB(180, 180, 180), 1, vbDash)
''        Call QuotaH(XF0_2, -Re, XF1_2, -Re, "" & F1_2, 12, CalColore("iFas1F2"), 3)
''        Call QuotaH(XF1_2, -Re, XF2_2, -Re, "" & F2_2, 12, CalColore("iFas2F2"), 3)
''        Call QuotaH(XF2_2, -Re, XF3_2, -Re, "" & F3_2, 12, CalColore("iFas3F2"), 3)
'        MaxX1 = X(3)
'        MaxX2 = X(7)
'    End Select
'  End If
  ' Asse pezzo
'  Call linea(LS, 0, LS + Ft, 0, RGB(180, 180, 180), 1, vbDashDot)
'  If Not NotScale Then
'    Call QuotaH(LS, PAR.MaxY + 8, LS + Ft, PAR.MaxY + 8, "" & Ft, 12, , 3)
'  End If
  Dim tidP1   As Integer
  Dim tidP2   As Integer
  Dim tidC    As Long
  Dim tidst   As Integer
  Dim tisp    As Integer
  tColore = vbBlue
  tidst = vbSolid
  'Linee (griglia)
'  For i = -3 To 3
'    tidP1 = 21 + i
'    tidP2 = 28 + i
'    Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), RGB(200, 200, 200), 1, vbDot)
'    tidP1 = 35 + i
'    tidP2 = 42 + i
'    Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), RGB(200, 200, 200), 1, vbDot)
'  Next i
'---------------------
  'Fianco1 Fascia1
  tidP1 = 0
  tidP2 = 1
'  tidC = CalColore("iCon1F1;iBom1F1", , , tColore)
  tisp = IIf(tidC = vbRed, 2, 1)
  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack)
  Dim Xout1F1(1 To 3)
  Dim Yout1F1(1 To 3)
  If Bom1F1 <> 0 Then
'    Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), Bom1F1, scalaxEl, tidC, tisp)
Call CALCOLA_BombaturaElica_MASSIMA(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), Xout1F1, Yout1F1, Bom1F1)
  End If
  'Fianco1 Fascia2
  tidP1 = 1
  tidP2 = 2
'  tidC = CalColore("iCon2F1;iBom2F1", , , tColore)
  tisp = IIf(tidC = vbRed, 2, 1)
'  linea X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack
    Dim Xout2F1(1 To 3)
  Dim Yout2F1(1 To 3)
  If Bom2F1 <> 0 Then
  Call CALCOLA_BombaturaElica_MASSIMA(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), Xout2F1, Yout2F1, Bom2F1)
'    Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), Bom2F1, scalaxEl, tidC, tisp)
  End If
  'Fianco1 Fascia3
  tidP1 = 2
  tidP2 = 3
'  tidC = CalColore("iCon3F1;iBom3F1", , , tColore)
'  tisp = IIf(tidC = vbRed, 2, 1)
'  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack)
    Dim Xout3F1(1 To 3)
  Dim Yout3F1(1 To 3)
  
  If Bom3F1 <> 0 Then
    Call CALCOLA_BombaturaElica_MASSIMA(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), Xout3F1, Yout3F1, Bom3F1)
'    Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), Bom3F1, scalaxEl, tidC, tisp)
  End If
  'Fianco 1 Separazione Zone 1 e 2
  tidC = tColore
  tisp = 1
  tidP1 = 8
  tidP2 = 9
 '     Dim Xout3F1(1 To 3)
 ' Dim Yout3F1(1 To 3)
'  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, tidst)
  'Fianco 1 Separazione Zone 2 e 3
  tidP1 = 10
  tidP2 = 11
'  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, tidst)
  'Fianco 1 Linea tra primo e ultimo punto
  tidP1 = 0
  tidP2 = 16
'  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, vbDot)
  'Fianco2 Fascia1
  tidP1 = 4
  tidP2 = 5
'  tidC = CalColore("iCon1F2;iBom1F2", , , tColore)
  tisp = IIf(tidC = vbRed, 2, 1)
'  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack)
      Dim Xout1F2(1 To 3)
  Dim Yout1F2(1 To 3)
  If Bom1F2 <> 0 Then
  Call CALCOLA_BombaturaElica_MASSIMA(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), Xout1F2, Yout1F2, -Bom1F2)
'    Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), -Bom1F2, scalaxEl, tidC, tisp)
  End If
  'Fianco2 Fascia2
  tidP1 = 5
  tidP2 = 6
'  tidC = CalColore("iCon2F2;iBom2F2", , , tColore)
'  tisp = IIf(tidC = vbRed, 2, 1)
  'linea x(tidP1), Y(tidP1), x(tidP2), Y(tidP2), tidC, tisp, tidst
'  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack)
        Dim Xout2F2(1 To 3)
  Dim Yout2F2(1 To 3)
  If Bom2F2 <> 0 Then
    Call CALCOLA_BombaturaElica_MASSIMA(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), Xout2F2, Yout2F2, -Bom2F2)
'    Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), -Bom2F2, scalaxEl, tidC, tisp)
  End If
  'Fianco2 Fascia3
  tidP1 = 6
  tidP2 = 7
'  tidC = CalColore("iCon3F2;iBom3F2", , , tColore)
'  tisp = IIf(tidC = vbRed, 2, 1)
  'linea x(tidP1), Y(tidP1), x(tidP2), Y(tidP2), tidC, tisp, tidst
'  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack)
        Dim Xout3F2(1 To 3)
  Dim Yout3F2(1 To 3)
  If Bom3F2 <> 0 Then
  Call CALCOLA_BombaturaElica_MASSIMA(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), Xout3F2, Yout3F2, -Bom3F2)
'    Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), -Bom3F2, scalaxEl, tidC, tisp)
  End If
  tidC = tColore
  tisp = 1
  'Fianco 2 Separazione Zone 1 e 2
  tidP1 = 12
  tidP2 = 13
'  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, tidst)
  'Fianco 2 Separazione Zone 2 e 3
  tidP1 = 14
  tidP2 = 15
'  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, tidst)
  'Fianco 1 Linea tra primo e ultimo punto
  tidP1 = 4
  tidP2 = 17
'  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, vbDot)
  'Scritte F1 e F2
'  Call TestoAll(LS, RS, "F1", , True, 1)
'  Call TestoAll(LS, -RS, "F2", , True, 1)
'  If NotScale Then Call TestoInfo("DRAWING NOT IN SCALE ! ", , True, vbRed, True)
'  Call TestoInfo("Corr. Scale: " & scalaxEl, 1)
'  Call TestoInfo("Units: [mm]", 3)

Bomb1 = 0 'Max(Yout1F1(1), Yout1F1(2), Yout1F1(3), Yout2F1(1), Yout2F1(2), Yout2F1(3), Yout3F1(1), Yout3F1(2), Yout3F1(3))
Bomb2 = 0 'Max(Yout1F2(1), Yout1F2(2), Yout1F2(3), Yout2F2(1), Yout2F2(2), Yout2F2(3), Yout3F2(1), Yout3F2(2), Yout3F2(3))
For i = 1 To 3
If (Yout1F1(i) > Bomb1) Then
 Bomb1 = Yout1F1(i)
 Fascia1 = Xout1F1(i)
End If
If (Yout1F2(i) > Bomb2) Then
 Bomb2 = Yout1F2(i)
 Fascia2 = Xout1F2(i)
End If
Next i
For i = 1 To 3
If (Yout2F1(i) > Bomb1) Then
 Bomb1 = Yout2F1(i)
 Fascia1 = Xout2F1(i)
End If
If (Yout2F2(i) > Bomb2) Then
 Bomb2 = Yout2F2(i)
 Fascia2 = Xout2F2(i)
End If
Next i
For i = 1 To 3
If (Yout3F1(i) > Bomb1) Then
 Bomb1 = Yout3F1(i)
 Fascia1 = Xout3F1(i)
End If
If (Yout3F2(i) > Bomb2) Then
 Bomb2 = Yout3F2(i)
 Fascia2 = Xout3F2(i)
End If
Next i
  If Ft2 > Ft Then
'    Call TestoInfo("WARNING: (FT F1 = " & Ft & ") < (FT F2 = " & Ft2 & ")", , True, vbRed, True)
'    Call QuotaH(MaxX1, Pos, MaxX2, Pos, "?", , vbRed, 3)
  ElseIf Ft2 < Ft Then
'    Call TestoInfo("WARNING: (FT F1 = " & Ft & ") > (FT F2 = " & Ft2 & ")", , True, vbRed, True)
'    Call QuotaH(MaxX2, -Pos, MaxX1, -Pos, "?", , vbRed, 3)
  End If
  '
End Sub
Private Sub CALCOLA_BombaturaElica_MASSIMA(ByVal X1 As Double, ByVal Y1 As Double, ByVal X2 As Double, ByVal Y2 As Double, _
                              ByRef Xout(), ByRef Yout(), B As Double)
'
Dim L As Double: Dim R As Double: Dim Ang As Double: Dim Xm As Double: Dim Ym As Double: Dim Xc As Double: Dim YC As Double: Dim Xb As Double: Dim Yb As Double

'
On Error Resume Next
  '
'  If (B = 0) Then Call linea(X1, Y1, X2, Y2, COLORE, Spessore): Exit Sub
'  Y1 = Y1 / ScalaY
'  Y2 = Y2 / ScalaY
  'Mezza fascia
  L = (Sqr((X2 - X1) ^ 2 + (Y2 - Y1) ^ 2)) / 2
  'Raggio
  R = (B ^ 2 + L ^ 2) / (2 * B)
  Ang = AngSeg(X1, Y1, X2, Y2)
  Ang = Ang + PG / 2
  Xm = (X1 + X2) / 2
  Ym = (Y1 + Y2) / 2
  Xb = Xm + B * Cos(Ang)
  Yb = Ym + B * Sin(Ang)
  Call CALCOLO_RAGGIO_TERNA(X1, Y1, Xb, Yb, X2, Y2, Xc, YC, R)
  If (Abs(R) < Abs(B)) Then Exit Sub
'  Call Disegna_Punto(Xb, Yb)
  Call CALCOLA_CP(Xc, YC, R, X1, Y1, X2, Y2, Xout, Yout, B < 0)
  '
End Sub
Private Sub CALCOLA_CP(Xc As Double, YC As Double, R As Double, _
                        X1 As Double, Y1 As Double, X2 As Double, Y2 As Double, _
                        ByRef Xout(), ByRef Yout(), Optional Antiorario As Boolean = False)
'
Dim A1    As Double
Dim A2    As Double
Dim Tp    As Double
Dim Xp    As Double
Dim Yp    As Double
Dim Xq    As Double
Dim Yq    As Double
Dim li_m  As Double
Dim li_q  As Double
Dim li_x  As Double
Dim i     As Integer
Dim npt   As Integer
Dim Delta As Double
Dim Err   As Double
'
On Error GoTo errDisegna_Arco
  '
'  If PAR.AllBlack Then COLORE = vbBlack
  '
  npt = 10 'NumPunti
  '
  If (X1 = X2) And (X2 = Y1) And (Y1 = Y2) Then
     A1 = 0: A2 = 2 * PG
  Else
    If (X1 = Xc) Then
      A1 = PG / 2
    Else
      A1 = Atn((Y1 - YC) / (X1 - Xc))
    End If
    Xp = Xc + R * Cos(A1)
    Yp = YC + R * Sin(A1)
    Err = Sqr((X1 - Xp) ^ 2 + (Y1 - Yp) ^ 2)
    
    Delta = 0
    For i = 1 To 3
      Xp = Xc + R * Cos(A1 + i * (PG / 2))
      Yp = YC + R * Sin(A1 + i * (PG / 2))
      If (Err > Sqr((X1 - Xp) ^ 2 + (Y1 - Yp) ^ 2)) Then
        Delta = i * (PG / 2)
        Err = Sqr((X1 - Xp) ^ 2 + (Y1 - Yp) ^ 2)
      End If
    Next i
    A1 = A1 + Delta
    
    If (X2 = Xc) Then A2 = PG / 2 Else A2 = Atn((Y2 - YC) / (X2 - Xc))
    
    Xp = Xc + R * Cos(A2)
    Yp = YC + R * Sin(A2)
    Err = Sqr((X2 - Xp) ^ 2 + (Y2 - Yp) ^ 2)
    
    Delta = 0
    For i = 1 To 3
      Xp = Xc + R * Cos(A2 + i * (PG / 2))
      Yp = YC + R * Sin(A2 + i * (PG / 2))
      If (Err > Sqr((X2 - Xp) ^ 2 + (Y2 - Yp) ^ 2)) Then
        Delta = i * (PG / 2)
        Err = Sqr((X2 - Xp) ^ 2 + (Y2 - Yp) ^ 2)
      End If
    Next i
    A2 = A2 + Delta
  End If
  
  If (A1 < 0) Then A1 = 2 * PG + A1
  If (A2 < 0) Then A2 = 2 * PG + A2
  
  Tp = -Abs(A2 - A1) / (npt - 1)
  If Antiorario Then Tp = -Tp
  '
  'SVG240111
  'ALTRIMENTI NON DISEGNA BOMBATURE PIU' PICCOLE
  'If Abs(R) > 30000 Then Exit Sub
  '
  Xp = Xc + R * Cos(A1)
  Yp = YC + R * Sin(A1)
  For i = 2 To npt
    Xq = Xc + R * Cos(A1 + Tp * (i - 1))
    Yq = YC + R * Sin(A1 + Tp * (i - 1))
 '   Call linea(Xp * ScalaX, Yp * ScalaY, Xq * ScalaX, Yq * ScalaY, COLORE, Spessore)
    Xp = Xq
    Yp = Yq
  Next i


    'Xqp = -R * Sin(A1 + Tp * (i - 1))
    'Yqp = R * Cos(A1 + Tp * (i - 1))
    
    'Y = Yq / Xq
    Xout(1) = Xc + R * Cos(A1 + Tp)
    Yout(1) = YC + R * Sin(A1 + Tp)
    Xout(2) = Xc + R * Cos(PG / 2)
    Yout(2) = YC + R * Sin(PG / 2)
    Xout(3) = Xc + R * Cos(A2 + (-Abs(A2 - A1)))
    Yout(3) = YC + R * Sin(A2 + (-Abs(A2 - A1)))
Exit Sub

errDisegna_Arco:
 ' MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Sub: Disegna_Arco"
  Exit Sub
  'Resume Next
  
End Sub

Private Function AngSeg(X1 As Double, Y1 As Double, X2 As Double, Y2 As Double) As Double
'
Dim tang  As Double
Dim i     As Integer
Dim R     As Double
Dim tX    As Double
Dim tY    As Double
Dim DIST  As Double
  '
  If (X2 <> X1) Then
    tang = Atn((Y2 - Y1) / (X2 - X1))
  Else
    tang = PG / 2
  End If
  R = Sqr((X2 - X1) ^ 2 + (Y2 - Y1) ^ 2)
  tX = X1 + R * Cos(tang)
  tY = Y1 + R * Sin(tang)
  DIST = Sqr((X2 - tX) ^ 2 + (Y2 - tY) ^ 2)
  i = 1
  While (DIST > R / 10) And (i < 5)
    tang = tang + PG / 2
    tX = X1 + R * Cos(tang)
    tY = Y1 + R * Sin(tang)
    DIST = Sqr((X2 - tX) ^ 2 + (Y2 - tY) ^ 2)
    i = i + 1
  Wend
  AngSeg = tang
  '
End Function
Private Sub CALCOLO_RAGGIO_TERNA(PInX As Double, PInY As Double, _
                                  PMeX As Double, PMeY As Double, _
                                  PFiX As Double, PFiY As Double, _
                                  Xc As Double, YC As Double, R0 As Double)
Dim X1 As Double
Dim X2 As Double
Dim X3 As Double
Dim X4 As Double
Dim X5 As Double
Dim X8 As Double
Dim X9 As Double
Dim Y1 As Double
Dim Y2 As Double
Dim Y3 As Double
Dim Y4 As Double
Dim Y5 As Double
Dim Y8 As Double
Dim Y9 As Double
Dim M1 As Double
Dim M2 As Double
Dim Q1 As Double
Dim Q2 As Double

Dim A1 As Double
Dim A2 As Double


  'CALCOLO RAGGIO
  X1 = PInX: X3 = PMeX: X5 = PFiX
  Y1 = PInY: Y3 = PMeY: Y5 = PFiY
  If Y1 = Y3 Then Y1 = Y3 + 0.00001
  If Y5 = Y3 Then Y5 = Y3 + 0.00001
  M1 = -((X3 - X1) / (Y3 - Y1))
  M2 = -((X5 - X3) / (Y5 - Y3))
  X8 = (X1 + X3) / 2: Y8 = (Y1 + Y3) / 2
  X9 = (X5 + X3) / 2: Y9 = (Y5 + Y3) / 2
  Q1 = Y8 - M1 * X8: Q2 = Y9 - M2 * X9
  If M1 = M2 Then M1 = M1 + 0.00001
  Xc = (Q2 - Q1) / (M1 - M2): YC = M1 * Xc + Q1
  R0 = Sqr((Xc - X3) ^ 2 + (YC - Y3) ^ 2)
'CALCOLO ANGOLO AL CENTRO
  'Q1=quadrante I-2     Q2=quadrante I
  If PInX >= Xc And PInY >= YC Then Q1 = 1
  If PInX >= Xc And PInY < YC Then Q1 = 4
  If PInX < Xc And PInY > YC Then Q1 = 2
  If PInX < Xc And PInY < YC Then Q1 = 3
  If PFiX >= Xc And PFiY >= YC Then Q2 = 1
  If PFiX >= Xc And PFiY < YC Then Q2 = 4
  If PFiX < Xc And PFiY > YC Then Q2 = 2
  If PFiX < Xc And PFiY < YC Then Q2 = 3
  If PInX = Xc Then
    A1 = PG / 2
  Else
    A1 = Atn((PInY - YC) / (PInX - Xc))
  End If
  If Q1 = 3 Then A1 = A1 + PG
  If Q1 = 2 Then A1 = A1 + PG
  If Q1 = 4 Then A1 = A1 + 2 * PG
  If PFiX = Xc Then
    A2 = PG / 2
  Else
    A2 = Atn((PFiY - YC) / (PFiX - Xc))
  End If
  If Q2 = 3 Then A2 = A2 + PG
  If Q2 = 2 Then A2 = A2 + PG
  If Q2 = 4 Then A2 = A2 + 2 * PG
  If A1 < 0 Then A1 = A1 + 2 * PG
  If A2 < 0 Then A2 = A2 + 2 * PG
  A1 = A1 * 180 / PG: A2 = A2 * 180 / PG
  '
  If A1 > A2 Then R0 = -R0
  '

End Sub

Sub CYC_PRFVITNP_AT_TH(RHO, PEIN, AEIN, fHa, THOUT)
'*****************************************************************
'* CALCOLO ANGOLO ASCISSA ANGOLARE                               *
'* NOTO FHALFA                                                   *
'* GBERTACCHI@SAMP@2016.07.04                                    *
'* VERSIONE:                                                     *
'*****************************************************************
'*APSENSOFIL_G=(-1'+1)=(SENSO FILETTO DX' SENSO FILETTO SX)      *
'* INPUTS                                                        *
'*RHO [mm]: ELICOID RADIAL DIMENSION                             *
'*PEIN [deg]: PASSO MOLA                                         *
'*AEIN [deg]: INCLINAZIONE ELICA                                 *
'*DAPN_AT[mm]: CORREZIONE ANGOLO                                 *
'* OUTPUT                                                        *
'*THOUT [deg]: ASCISSA ANGOLARE                                  *
'*NB: inserisci Diametri esatti di inspezione                    *
'*****************************************************************
  Dim SM#, AE#, PE#
  Dim PI#, DEG#
  Dim APt#, BEG#, BEBG#, APNGE#, DAPN#, Ap#, MNG#, Z%
  Dim R_P#, Rb, cRho
  Dim TIFS#, TIFE#, DTIF#, LGEN#
  Dim C_B#, C_C#
  Dim SENSOFIL_G%, ApSensoFil_G%
 PI = 3.14159265358979
 DEG = 180 / PI
 SENSOFIL_G = Lp("SENSOFIL_G") '
 ApSensoFil_G = -1 + SENSOFIL_G * 2
 SM = -ApSensoFil_G
 BEG = -Lp("Beta_G") / DEG
 AE = (-SM) * Abs(AEIN) / DEG
 PE = SM * Abs(PEIN)
 Ap = Lp("Alfa_G") / DEG
 MNG = Lp("Mn_G")
 Z = Lp("NumDenti_G")
 APt = ATAN2(Tan(Ap), Cos(BEG))
 BEBG = ATAN2(Tan(BEG) * Cos(APt), 1)
 R_P = MNG * Z / 2 / Cos(BEG)
 Rb = R_P * Cos(APt)
 TIFS = Sqr(POT(Lp("DSAP_G") / 2) - POT(Rb))
 TIFE = Sqr(POT(Lp("DEAP_G") / 2) - POT(Rb))
 DTIF = (TIFE - TIFS) * 0.95
 LGEN = DTIF * Tan(BEBG)
 DAPN = ATAN2(fHa / LGEN, 1)
 APNGE = Ap + DAPN
 cRho = RHO * Sin(AE) + PE * (1 / (2 * PI)) * Cos(AE)
 C_B = -2 * (Cos(Ap) * Sin(AE) + cRho) / (Sin(Ap) * POT(Sin(AE)))
 C_C = 2 * (Sin(APNGE) - Sin(Ap)) / (Sin(Ap) * POT(Sin(AE)))
 THOUT = -C_B / 2 + SM * Sqr(POT(C_B / 2) - C_C)

 THOUT = THOUT * DEG
End Sub

