Attribute VB_Name = "CalcMolaVitIng"

'*******************************************************************
'       CALCOLO DEL PROFILO DELLA MOLA PER INGRANAGGI E VITI
'*******************************************************************
Option Explicit

Const PI = 3.14159265358979

Public Type StructCalcMola
  'INPUT DA FORM
   ALFA           As Double   'Angolo Pressione (rad)
   Beta           As Double   'Angolo Elica (rad)
   Mn             As Double   'Modulo Normale
   Z              As Double   'Numero Denti
   DEm            As Double   'Diametro Esterno Mola
   DSAP           As Double   'Diametro SAP
   DEAP           As Double   'Diametro EAP
   S0Nn           As Double   'SON Normale
   Wk             As Double   'Quota WILDABER
   k              As Double   'Denti WILDABER
   QR             As Double   'Quota Rulli
   DR             As Double   'Diametro Rulli
   X              As Double   'coefficiente di spostamento
   RT             As Double   'Raggio di Testa
   RF             As Double   'Raggio di Fondo
   ART            As Double   'Angolo Raccordo Testa
   ARF            As Double   'angolo raccordo di fondo
   DInt           As Double   'Diametro Interno
   BetaMola       As Double   'Inclinazione Asse Mola
   ValutazioneDIA As Boolean  'Valutazione (true=DIA; false=TIF)
   NPR1           As Integer  'N� Punti per Raggio
   DIPU           As Double   'Distanza Punti Evolvente
   TifDia(2, 3)   As Double   'TIF/DIA Fine Zona
   Rastr(2, 3)    As Double   'Rastremazione
   BOMB(2, 3)     As Double   'Bombatura
   TipoCORR       As String   'tipo correzione profilo (KSTD=correzioneK, KPARAB=correzioneK tipo parab.)
   Trocoid        As Boolean  'true=trocoide sul fondo
   RCT(2, 2)      As Double   'raggio di raccordo tra le zone (RCT = raggio cerchio tangente)
   'INPUT DA FILEINI
   NPEmin         As Integer
   NPEmax         As Integer
End Type

Public INP As StructCalcMola

Public NP1        As Integer  'Numero punti raggio di testa
Public NPE        As Integer  'Numero punti evolvente
Public NTR        As Integer  'numero punti tratto rettilineo (raccordo con raggio di fondo)
Public NpFIANCO   As Integer  ' max numero punti fianco
Public NPnt(2)    As Integer  'numero punti per fianco
Public NpntEv(2)  As Integer  'numero punti evolvente per fianco
'-----------------------------------------
' - coordinate (polari) profilo pezzo -
    Public D()            As Double  '2R ( dove  R = distanza dal centro del pezzo )
    Public A()            As Double  'Alfa (di RAB) (angolo con asse mediano del vano)
    Public API()          As Double  'Beta (di RAB) (angolo tra tangente al profilo e retta congiungente il punto sul profilo con il centro del pezzo)
' -
    Public RaggioRAB()    As Double    'Raggio (di RAB) - su entrambi i fianchi
    Public AlfaRAB()      As Double      'Alfa (di RAB) - su entrambi i fianchi
    Public BetaRAB()      As Double      'Beta (di RAB) - su entrambi i fianchi
' - vettori d'appoggio - profilo pezzo
    Public Daux(2, 500)   As Double
    Public Aaux(2, 500)   As Double
    Public APIaux(2, 500) As Double
'-----------------------------------------
' - coordinate profilo mola -
    Public X()            As Double
    Public Y()            As Double
    Public AlfaM()        As Double
' -
    Public XMola()        As Double    'ascissa profilo mola - su entrambi i fianchi
    Public Ymola()        As Double    'ordinata profilo mola - su entrambi i fianchi
    Public AMola()        As Double    'ang.inclinazione profilo mola - su entrambi i fianchi
'-----------------------------------------

Public DIntOttenuto As Double
Public DEstOttenuto As Double

Public hu1        As Double
Public RRulloMax  As Double
Public RRulloMax2 As Double
Public XCRullo    As Double
Public XCRullo2   As Double
Public YCRullo    As Double
Public YCRullo2   As Double

Public db As Double     'DIAMETRO DI BASE
Public PAS As Double    'Passo Elica

Public Xass() As Double
Public Yass() As Double
Public AngRastr(2, 2) As Double
Public XCRaccTIF(2, 2) As Double
Public YCRaccTIF(2, 2) As Double

Public Xax() As Double
Public Yax() As Double

'variabili usate per calcolo Raggio di Raccordo su TIF
Public RR1  As Double
Public Xc1  As Double
Public Yc1  As Double
Public RR2  As Double
Public Xc2  As Double
Public Yc2  As Double
Public XT1  As Double
Public YT1  As Double
Public XT2  As Double
Public YT2  As Double

Public R1rac(2, 2)  As Double
Public XC1rac(2, 2) As Double
Public YC1rac(2, 2) As Double
Public R2rac(2, 2)  As Double
Public XC2rac(2, 2) As Double
Public YC2rac(2, 2) As Double
Public XT1rac(2, 2) As Double
Public YT1rac(2, 2) As Double
Public XT2rac(2, 2) As Double
Public YT2rac(2, 2) As Double
'

Private Sub FromCentToGPS(ByVal Valcent As Double, ValGradi As Double, ValPrimi As Double, ValSecondi As Double)
'
On Error Resume Next
  '
  ValGradi = Int(Valcent)
  ValPrimi = Int((Valcent - ValGradi) * 60)
  ValSecondi = Int(100 * (((Valcent - ValGradi) * 60 - ValPrimi) * 60) + 0.5) / 100
  
End Sub

Private Sub FromGPSToCent(Valcent, ByVal ValGradi, Optional ByVal ValPrimi = 0, Optional ByVal ValSecondi = 0)
'
On Error Resume Next
  '
  Valcent = ValGradi + (ValPrimi / 60) + (ValSecondi / 3600)

End Sub

Private Function ToRad(A As Double) As Double
'
On Error Resume Next
  '
  ToRad = A * PI / 180

End Function

Private Function ToDeg(A As Double) As Double
'
On Error Resume Next
  '
  ToDeg = A * 180 / PI

End Function

Private Function inv(Aux As Double) As Double
'
On Error Resume Next
  '
  inv = Tan(Aux) - Aux

End Function

Private Function Ainv(Aux As Double) As Double

Dim i       As Integer
Dim Ymini   As Double
Dim Ymaxi   As Double
Dim Ymoyen  As Double
Dim X1      As Double
'
On Error Resume Next
  '
  Ymini = 0: Ymaxi = PI / 2
  For i = 1 To 25
      Ymoyen = (Ymini + Ymaxi) / 2: X1 = inv(Ymoyen)
      If X1 < Aux Then Ymini = Ymoyen Else: Ymaxi = Ymoyen
  Next i
  Ainv = Ymoyen

End Function

'Input:  Mn, Z, Alfa(rad), Beta(rad)
'Input o Output: S0Nn, K, Wk, Dr, Qr, X
'Output: Dtang
Private Sub CalcoloSpessore(Mn As Double, Z As Double, ALFA As Double, Beta As Double, S0Nn As Double, Optional k As Double = 0, Optional Wk As Double = 0, Optional DR As Double = 0, Optional QR As Double = 0, Optional X As Double = 0, Optional Dtang As Double = 0)
'
Dim Mt        As Double
Dim DP        As Double
Dim ALFAt     As Double
Dim db        As Double
Dim Betab     As Double
Dim SbT       As Double
Dim AlfaPunta As Double
'
On Error GoTo errCalcoloSpessore
  '
  Mt = Mn / Cos(Beta)
  DP = Mt * Z
  ALFAt = Atn(Tan(ALFA) / Cos(Beta))
  db = DP * Cos(ALFAt)
  Betab = Atn(Tan(Beta) * Cos(ALFAt))
  '
  If (QR = 0) And (Wk = 0) And (S0Nn = 0) And (X = 0) Then
    S0Nn = PI * Mn / 2
    Exit Sub
  End If
  If (S0Nn > 0) Then
    'spessore normale inserito da utente 'S0Nn noto
  ElseIf (Wk > 0) And (k > 0) Then
    Call WILDABLER2S0N(S0Nn, Wk, k, Mn, Z, ALFA, Beta)
  ElseIf QR > 0 And DR > 0 Then
    If Beta <= PI / 4 Then Call QUOTARULLI2S0N(S0Nn, QR, DR, Mn, Z, ALFA, Beta)
    If Beta > PI / 4 Then Call QuotaRulli("E", "ZI", Mn, ALFA, DP, Z, DR, QR, S0Nn)
  ElseIf X <> 0 Then
    'trasformazione SpostamentoX2S0Nn
    S0Nn = 2 * Mn * X * Tan(ALFA) + ((PI / 2) * Mn)
  End If
  '
  If (k <> 0) Then
    'calcolo Wk
    SbT = db * ((S0Nn / (Cos(Beta) * DP)) + inv(ALFAt))
    Wk = ((k - 1) * PI * db / Z + SbT) * Cos(Betab)
  End If
  'calcolo X
  If (ALFA <> 0) Then X = (S0Nn - (PI * Mn / 2)) / (2 * Mn * Tan(ALFA))
  If (Abs(X) < 0.0001) Then X = 0
  '
  If (DR <> 0) Then
    'calcolo Qr e Dtang
    If (Beta <= PI / 4) Then QR = QuotaRulliIngEst(Dtang, Mn, ALFA, Z, DR, S0Nn, Beta)
    If (Beta > PI / 4) Then Call QuotaRulli("W", "ZI", Mn, ALFA, DP, Z, DR, QR, S0Nn)
  End If
  '
Exit Sub
'
errCalcoloSpessore:
  StopRegieEvents
  MsgBox "Sub CalcoloSpessore ERROR"
  ResumeRegieEvents

End Sub

'trasformazione WILDABLER -- > SON

'Kmis = numero denti misurati
'Wk = Quota Wildabler (i.e. Quota cordale misurata su Kmis denti)
Private Sub WILDABLER2S0N(ByRef S0Nn As Double, ByVal Wk As Double, ByVal Kmis As Double, ByVal Mn As Double, ByVal Z As Double, ByVal ALFA As Double, ByVal Beta As Double)

Dim ALFAt   As Double
Dim Betab   As Double
Dim Mt      As Double
Dim DP      As Double
Dim Rp      As Double
Dim db      As Double
Dim Rb      As Double
Dim PassoB  As Double
Dim iB      As Double
Dim SbT     As Double
Dim S0Nt    As Double
'
On Error GoTo errWILDABLER2S0N
  '
  ALFAt = Atn(Tan(ALFA) / Cos(Beta))
  Betab = Atn(Tan(Beta) * Cos(ALFAt))
  Mt = Mn / Cos(Beta)
  DP = Mt * Z: Rp = DP / 2
  db = DP * Cos(ALFAt): Rb = db / 2
  PassoB = (Rb * 2 * PI) / Z   'Passo di base
  '
  iB = Kmis * PassoB - Wk / Cos(Betab)  'Vano Base
  SbT = PassoB - iB             'Spessore base
  S0Nt = DP * (SbT / Rb / 2 - inv(ALFAt))
  S0Nn = S0Nt * Cos(Beta)
  S0Nn = Int(S0Nn * 10000 + 0.5) / 10000

Exit Sub
'
errWILDABLER2S0N:
  StopRegieEvents
  MsgBox "Sub WILDABLER2S0N :" & vbCrLf & vbCrLf & Error(Err), vbCritical, "ERROR"
  ResumeRegieEvents

End Sub

'trasformazione Quota Rulli -- > SON
'
'Dr = diametro rullo
'Qr = quota rulli
Private Sub QUOTARULLI2S0N(ByRef S0Nn As Double, ByVal QR As Double, ByVal DR As Double, ByVal Mn As Double, ByVal Z As Double, ByVal ALFA As Double, ByVal Beta As Double)

Dim ALFAt As Double
Dim Betab As Double
Dim Mt    As Double
Dim DP    As Double
Dim Rp    As Double
Dim db    As Double
Dim Rb    As Double
Dim SbT   As Double
Dim Aux1  As Double
Dim Aux2  As Double
Dim Aux3  As Double
Dim Aux4  As Double
'
On Error GoTo errQUOTARULLI2S0N
  '
  ALFAt = Atn(Tan(ALFA) / Cos(Beta))
  '
  Aux1 = PI / 2 / Z
  If Int(Z / 2) = Z / 2 Then Aux1 = 0
  Aux2 = Mn * Z * Cos(ALFAt) * Cos(Aux1) / ((QR - DR) * Cos(Beta))
  Aux3 = Tan(Arccos(Aux2))
  Aux4 = (inv(ALFAt) + DR / (Mn * Z * Cos(ALFA)) - Aux3 + Atn(Aux3)) * Mn * Z
  S0Nn = (PI * Mn - Aux4)
  '
Exit Sub
'
errQUOTARULLI2S0N:
  StopRegieEvents
  MsgBox "Sub QUOTARULLI2S0N :" & vbCrLf & vbCrLf & Error(Err), vbCritical, "ERROR"
  ResumeRegieEvents

End Sub

'INPUT:  Mn, APn, Z, Dr, Sp, Optional Bp = 0
'OUTPUT: QuotaRulliIngEst, Dtang (= diametro punto di tangenza)
Private Function QuotaRulliIngEst(Dtang As Double, Mn As Double, APn As Double, Z As Double, DR As Double, SP As Double, Optional Bp = 0) As Double
'
Dim InvAPq  As Double
Dim Rq      As Double
Dim APq     As Double
Dim APt     As Double
Dim SpT     As Double
Dim Mt      As Double
Dim DP      As Double
Dim db      As Double
Dim BB      As Double
Dim FE      As Double
Dim IE      As Double
'
On Error GoTo ERRORE
  '
  APt = Atn(Tan(APn) / Cos(Bp))
  SpT = SP / Cos(Bp)
  Mt = Mn / Cos(Bp)
  DP = (Mt * Z)
  db = DP * Cos(APt)
  BB = Atn(Tan(Bp) * Cos(APt))
  InvAPq = inv(APt) + (SpT / DP) + (DR / (DP * Cos(APt) * Cos(BB))) - PI / Z
  APq = Ainv(InvAPq)
  Rq = (DP / 2) * (Cos(APt) / Cos(APq))
  If Int(Z / 2) = Z / 2 Then
    QuotaRulliIngEst = 2 * Rq + DR
  Else
    QuotaRulliIngEst = 2 * Rq * Cos(PI / (2 * Z)) + DR
  End If
  FE = Rq * Sin(APq)
  IE = FE - (DR / 2) * Cos(BB)
  Dtang = Sqr((IE * IE) + ((db / 2) * (db / 2))) * 2
  '
Exit Function

ERRORE:
  StopRegieEvents
  MsgBox "check data!!", vbCritical, "SUB: QuotaRulliIngEst"
  ResumeRegieEvents
    
End Function

' Dtang = diametro punto di tangenza
Private Function QuotaRulliIngInt(Dtang As Double, Mn As Double, APn As Double, Z As Double, DR As Double, SP As Double, Optional Bp = 0)

Dim InvAPq  As Double
Dim Rq      As Double
Dim APq     As Double
Dim APt     As Double
Dim SpT     As Double
Dim Mt      As Double
Dim DP      As Double
Dim db      As Double
Dim BB      As Double
Dim FE      As Double
Dim IE      As Double
'
On Error Resume Next
  '
  APt = Atn(Tan(APn) / Cos(Bp))
  SpT = SP / Cos(Bp)
  Mt = Mn / Cos(Bp)
  DP = (Mt * Z)
  db = DP * Cos(APt)
  BB = Atn(Tan(Bp) * Cos(APt))
  InvAPq = inv(APt) - (SpT / DP) - (DR / (DP * Cos(APt) * Cos(BB))) + PI / Z
  APq = Ainv(InvAPq)
  Rq = (DP / 2) * (Cos(APt) / Cos(APq))
  If Int(Z / 2) = Z / 2 Then
    QuotaRulliIngInt = 2 * Rq - DR
  Else
    QuotaRulliIngInt = 2 * Rq * Cos(PI / (2 * Z)) - DR
  End If
  FE = Rq * Sin(APq)
  IE = FE + (DR / 2)
  Dtang = Sqr((IE * IE) + ((db / 2) * (db / 2))) * 2

End Function

' Code$ = "E": Calcul de SpNormV conosciando Wpg
' Code$ = "W": Calcul de Wpg conosciando SpNormV
Private Sub QuotaRulli(Code$, Profil$, MnormV, ApNormV, DprimV, NFil, DPG, Wpg, SpNormV)

Dim IncV      As Double
Dim MAssV     As Double
Dim PassV     As Double
Dim YutProfN  As Double
Dim Rayon     As Double
Dim HelbV     As Double
Dim DbaseV    As Double
Dim EbAppV    As Double
Dim Aux       As Double
Dim Aux1      As Double
Dim Alfa2     As Double
Dim i         As Integer
Dim PasRx     As Double
Dim PasAlfa   As Double
Dim Helx      As Double
Dim Hr        As Double
Dim Alfa1     As Double
Dim XC        As Double
Dim AlfaC     As Double
Dim Xutens    As Double
Dim Yutens    As Double
'
On Error Resume Next
  '
'If Profil$ = "ZN" Then
'   ' Connus : MnormV , ApNormV , DprimV , Nfil , Dpg
'   ' Wpg o SpNormV
'   IncV = Arcsin(Nfil * MnormV / DprimV)
'   MassV = MnormV / Cos(IncV)
'   PassV = MassV * Nfil * PI
'   If Code$ = "W" Then
'      ' Calcul de Wpg connaissant SpNormV
'
'      ' 1 ) calcolo YutProfN ( Distanza vertice fianchi utensile / asse vite )
'
'      Alfa2 = 0.1
'      Aux = (PassV / Nfil - SpNormV / Cos(IncV)) / 2
'      PasAlfa = 0.1
'      Do
'         Alfa2 = Alfa2 - PasAlfa
'         PasAlfa = PasAlfa / 10
'         Do
'            Alfa2 = Alfa2 + PasAlfa
'            Aux1 = DprimV / 2 * Sin(Alfa2) / Tan(IncV) + PassV * Alfa2 / 2 / PI
'         Loop While Aux1 < Aux
'      Loop While PasAlfa > 0.000001
'      Aux = (Aux - PassV * Alfa2 / 2 / PI) / Cos(IncV)
'      YutProfN = DprimV / 2 * Cos(Alfa2) - Aux / Tan(ApNormV)
'
'      ' 2 ) Calcolo Quota
'
'      PasRx = 1
'      Yutens = YutProfN + PasRx
'      Do
'         Yutens = Yutens - PasRx
'         PasRx = PasRx / 10
'         Do
'            Yutens = Yutens + PasRx
'            Xutens = (Yutens - YutProfN) * Tan(ApNormV)
'            Call VisObtN(Yutens, Xutens, ApNormV, IncV, PassV, Rayon, Alfa, BETA, Iax, ApAx)
'            GoSub CalcoloXcAlfaC
'            'Aux = AlfaC: Aux = Xc * 2 + Dpg
'         Loop While AlfaC < 0
'      Loop While PasRx > 0.00001
'      Wpg = Xc * 2 + Dpg ' = Wpg quand AlfaC = 0
'   Else
'      ' Calcul de SpNormV connaissant Wpg
'
'      ' 1 ) Calcolo YutProfN
'
'      YutProfN = (Wpg - Dpg) / 2 - Dpg / 2 / Sin(ApNormV)
'      If YutProfN > (DestV / 2) Or (Wpg - Dpg) < DintV Then
'         MsgBox "Quota su rulli errata", 0
'         SpNormV = 0
'         Exit Sub
'      End If
'      For i = 1 To 10
'         PasRx = 1
'         Yutens = YutProfN
'         Do
'            Yutens = Yutens - PasRx
'            PasRx = PasRx / 10
'            Do
'               Yutens = Yutens + PasRx
'               Xutens = (Yutens - YutProfN) * Tan(ApNormV)
'               Call VisObtN(Yutens, Xutens, ApNormV, IncV, PassV, Rayon, Alfa, BETA, Iax, ApAx)
'               GoSub CalcoloXcAlfaC
'            Loop While AlfaC < 0
'         Loop While PasRx > 0.00001
'         YutProfN = YutProfN + (Wpg / 2 - (Xc + Dpg / 2))
'      Next i
'
'      ' 2 ) Calcolo quota SpNormV
'
'      Dim A, B, C
'      A = Tan(ApNormV) * Sin(IncV)
'      B = A ^ 2 * YutProfN
'      C = (A ^ 2 * YutProfN ^ 2 - (DprimV / 2) ^ 2)
'      Yutens = (B + Sqr(B ^ 2 - (1 + A ^ 2) * C)) / (1 + A ^ 2)
'      Xutens = (Yutens - YutProfN) * Tan(ApNormV)
'      Alfa2 = Arcsin(Xutens * Sin(IncV) / (DprimV / 2))
'      Aux = PassV * Alfa2 / 2 / PI + Xutens * Cos(IncV)
'      SpNormV = (PassV / Nfil - Aux * 2) * Cos(IncV)
'   End If
'End If

If Profil$ = "ZI" Then
   ' Connus : MnormV , ApNormV , DprimV , Nfil , Dpg
   ' Wpg o SpNormV
   Dim Pba
   IncV = Arcsin(NFil * MnormV / DprimV)
   MAssV = MnormV / Cos(IncV)
   HelbV = Arccos(Cos(ApNormV) * Cos(IncV))
   DbaseV = MAssV * NFil / Tan(HelbV)
   Pba = PI * DbaseV / NFil
   If Code$ = "W" Then
      Aux = SpNormV / Sin(IncV) / DprimV
      EbAppV = (Aux + inv(Arccos(DbaseV / DprimV))) * DbaseV
      Aux = ((EbAppV - Pba) + DPG / Sin(HelbV)) / DbaseV
      Wpg = DbaseV / Cos(Ainv(Aux)) + DPG
   Else
      Aux = Arccos(DbaseV / (Wpg - DPG))
      EbAppV = DbaseV * inv(Aux) - DPG / Sin(HelbV) + Pba
      Aux = EbAppV / DbaseV - inv(Arccos(DbaseV / DprimV))
      SpNormV = Aux * DprimV * Sin(IncV)
   End If
End If

'If Profil$ = "ZK" Then
'   ' Connus : MnormV , DintV , DmolaK , DprimV , Nfil , Dpg
'   ' Wpg o SpNormV
'   Dim RxMol, ExMol, InMol, Esm
'   EntMolaVis = (DintV + DmolaK) / 2
'   RmolPrim = EntMolaVis - DprimV / 2
'   IncV = Arcsin(Nfil * MnormV / DprimV)
'   MassV = MnormV / Cos(IncV)
'   PassV = MassV * Nfil * PI
'   If Code$ = "W" Then
'      ' Calcul de Wpg connaissant SpNormV
'      EmolPrim = MnormV * PI - SpNormV
'      For i = 1 To 5
'         ' Calcul de l'epaisseur de la meule
'         Call VisObtK(EntMolaVis, IncV, PassV, Rayon, Alfa, BETA, Iax, ApAx, EmolPrim / 2, RmolPrim, ApNormV, Ltang)
'         Aux = Iax + (DprimV / 2 - Rayon) * Tan(ApAx)
'         Aux = (MassV * PI - Aux * 2) * Cos(IncV)
'         EmolPrim = EmolPrim + Aux - SpNormV
'      Next i
'      Esm = EmolPrim / 2 - (DmolaK / 2 - RmolPrim) * Tan(ApNormV)
'      RxMol = DmolaK / 2: ExMol = Esm
'      Call VisObtK(EntMolaVis, IncV, PassV, Rayon, Alfa, BETA, Iax, ApAx, ExMol, RxMol, ApNormV, Ltang)
'      GoSub CalcoloXcAlfaC
'      If AlfaC > 0 Then
'         Wpg = 0
'      Else
'         PasRx = 0.1
'         Do
'            RxMol = RxMol + PasRx
'            PasRx = PasRx / 10
'            Do
'               RxMol = RxMol - PasRx
'               ExMol = Esm + (DmolaK / 2 - RxMol) * Tan(ApNormV)
'               Call VisObtK(EntMolaVis, IncV, PassV, Rayon, Alfa, BETA, Iax, ApAx, ExMol, RxMol, ApNormV, Ltang)
'               GoSub CalcoloXcAlfaC
'            Loop While AlfaC < 0
'         Loop While PasRx > 0.00001
'         Wpg = Xc * 2 + Dpg ' = Wpg quand AlfaC = 0
'      End If
'   Else
'      ' Calcul de SpNormV connaissant Wpg
'      Aux = (Wpg - DintV) / 2 - Dpg / 2
'      Esm = Dpg / 2 / Cos(ApNormV) - Aux * Tan(ApNormV) - 1
'      Do
'         ' calcul de l'epaisseur de la meule
'         PasRx = 1
'         RxMol = DmolaK / 2 - PasRx
'         Do
'            RxMol = RxMol + PasRx
'            PasRx = PasRx / 10
'            Do
'               RxMol = RxMol - PasRx
'               ExMol = Esm + (DmolaK / 2 - RxMol) * Tan(ApNormV)
'               Call VisObtK(EntMolaVis, IncV, PassV, Rayon, Alfa, BETA, Iax, ApAx, ExMol, RxMol, ApNormV, Ltang)
'               GoSub CalcoloXcAlfaC
'            Loop While AlfaC < 0
'         Loop While PasRx > 0.00001
'         Aux = Xc * 2 + Dpg
'         Esm = Esm + (Aux - Wpg) * Tan(ApNormV) / 2
'      Loop While Abs(Aux - Wpg) > 0.0001
'      RxMol = DmolaK / 2
'      PasRx = 1
'      Do
'         RxMol = RxMol + PasRx
'         PasRx = PasRx / 10
'         Do
'            RxMol = RxMol - PasRx
'            ExMol = Esm + (DmolaK / 2 - RxMol) * Tan(ApNormV)
'            Call VisObtK(EntMolaVis, IncV, PassV, Rayon, Alfa, BETA, Iax, ApAx, ExMol, RxMol, ApNormV, Ltang)
'         Loop While Rayon < DprimV / 2 Or BETA < 0
'      Loop While PasRx > 0.00001
'      Aux = PI / Nfil
'      Aux = (Aux - Alfa) / Aux
'      SpNormV = MnormV * PI * Aux
'   End If
'End If
'
'If Profil$ = "ZA" Then
'   ' Connus : MnormV , ApAssV , DprimV , Nfil , Dpg
'   ' Wpg o SpNormV
'   IncV = Arcsin(Nfil * MnormV / DprimV)
'   MassV = MnormV / Cos(IncV)
'   PassV = MassV * Nfil * PI
'   If Code$ = "W" Then
'      ' Calcul de Wpg connaissant SpNormV
'      Rayon = 10
'      PasRx = 10
'      Do
'         Rayon = Rayon - PasRx
'         PasRx = PasRx / 10
'         Do
'            Rayon = Rayon + PasRx
'            Helx = Atn(PassV / PI / Rayon / 2)
'            BETA = Atn(Tan(ApAssV) / Tan(Helx))
'            Aux = (DprimV / 2 - Rayon) * Tan(ApAssV)
'            Aux = (MassV * PI - SpNormV / Cos(IncV)) / 2 - Aux
'            Alfa = 2 * PI * Aux / PassV
'            GoSub CalcoloXcAlfaC
'         Loop While AlfaC < 0
'      Loop While PasRx > 0.00001
'      Aux = AlfaC ' pour tester
'      Wpg = Xc * 2 + Dpg ' = Wpg quand AlfaC = 0
'   Else
'      ' Calcul de SpNormV connaissant Wpg
'      SpNormV = 0
'      For i = 1 To 10
'         Rayon = 10
'         PasRx = 10
'         Do
'            Rayon = Rayon - PasRx
'            PasRx = PasRx / 10
'            Do
'               Rayon = Rayon + PasRx
'               Helx = Atn(PassV / PI / Rayon / 2)
'               BETA = Atn(Tan(ApAssV) / Tan(Helx))
'               Aux = (DprimV / 2 - Rayon) * Tan(ApAssV)
'               Aux = (MassV * PI - SpNormV / Cos(IncV)) / 2 - Aux
'               Alfa = 2 * PI * Aux / PassV
'               GoSub CalcoloXcAlfaC
'            Loop While AlfaC < 0
'         Loop While PasRx > 0.00001
'         Aux = AlfaC ' pour tester
'         SpNormV = (Wpg - (Xc * 2 + Dpg)) * Tan(ApAssV) + SpNormV ' = Wpg quand AlfaC = 0
'      Next i
'   End If
'End If

Exit Sub

'CalcoloXcAlfaC:
'   ' Il s'agit de la formule que j'avais etablie en 1993
'   ' pour le controle profil de forme sur rectifieuse de profil
'   ' ( Dpg etant la bille du palpeur )
'   Hr = Atn(2 * PI * Rayon * Cos(BETA) / PassV)
'   Aux = Dpg / 2 * Cos(Hr)
'   Alfa1 = Atn(Cos(BETA) / (Sin(BETA) + Rayon / Aux))
'   Aux = Cos(Hr) * Sin(Alfa1 + BETA)
'   Xc = Rayon * Cos(Alfa1) + Dpg / 2 * Aux
'   AlfaC = Alfa - Alfa1 - Dpg * Sin(Hr) * PI / PassV
'Return

End Sub

Private Function Arccos(Aux As Double) As Double
'
Dim Aux1 As Double
'
On Error Resume Next
  '
  Aux1 = (1 - Aux * Aux)
  If (Aux1 < 0) Then
    StopRegieEvents
    MsgBox "Cos > 1" + vbCrLf + vbCrLf + " Please, check input values", vbCritical, "Error"
    ResumeRegieEvents
    Exit Function
  Else
    If (Aux = 0) Then
      Arccos = PI / 2
    Else
      If (Aux > 0) Then
        Arccos = Atn(Sqr(1 - Aux * Aux) / Aux)
      Else
        Aux = Abs(Aux)
        Arccos = Atn(Aux / Sqr(1 - Aux * Aux)) + PI / 2
      End If
    End If
  End If

End Function

Function Arcsin(Aux As Double) As Double
'
Dim Aux1 As Double
'
On Error Resume Next
  '
  Aux1 = (1 - Aux * Aux)
  If (Aux1 < 0) Then
    StopRegieEvents
    MsgBox "Sin > 1" + vbCrLf + vbCrLf + " Check data!!", vbCritical, "Error"
    ResumeRegieEvents
  Else
    If (Aux = 1) Then
      Arcsin = PI / 2
    Else
      Arcsin = Atn(Aux / Sqr(Aux1))
    End If
  End If

End Function

' Dtang = diametro punto di tangenza
Private Function QuotaQg(Dtang As Double, Mn As Double, APn As Double, Z As Double, DR As Double, SP As Double, Dales As Double, Optional Bp As Double = 0) As Double
'
Dim InvAPq  As Double
Dim Rq      As Double
Dim APq     As Double
Dim APt     As Double
Dim SpT     As Double
Dim Mt      As Double
Dim DP      As Double
Dim db      As Double
Dim BB      As Double
Dim FE      As Double
Dim IE      As Double
'
On Error GoTo ERRORE
    
  APt = Atn(Tan(APn) / Cos(Bp))
  SpT = SP / Cos(Bp)
  Mt = Mn / Cos(Bp)
  DP = (Mt * Z)
  db = DP * Cos(APt)
  BB = Atn(Tan(Bp) * Cos(APt))
  InvAPq = inv(APt) + (SpT / DP) + (DR / (DP * Cos(APt) * Cos(BB))) - PI / Z
  APq = Ainv(InvAPq)
  Rq = (DP / 2) * (Cos(APt) / Cos(APq))
  QuotaQg = Rq + DR / 2 - Dales / 2
  FE = Rq * Sin(APq)
  IE = FE - (DR / 2)
  Dtang = Sqr((IE * IE) + ((db / 2) * (db / 2))) * 2
  '
Exit Function

ERRORE:
  StopRegieEvents
  MsgBox "Check data!!", vbCritical, "SUB: QuotaQg"
  ResumeRegieEvents
  
End Function

Private Function SpessoreSx(dx As Double, SbT As Double, db As Double) As Double

Dim AlfaX As Double
'
On Error Resume Next
    
  AlfaX = Arccos(db / dx)
  SpessoreSx = ((SbT / db) - inv(AlfaX)) * dx

End Function

'OUTPUT:    NP1 - Numero punti raggio di testa
'           NPE - Numero punti evolvente
'           D(), A(), API()   - coordinate (polari) profilo pezzo
'           X(), Y(), ALFAM() - coordinate profilo mola
'
Sub CalcolaProfilo(INP As StructCalcMola)
'
Dim IL       As Double  'INTERASSE DI LAVORO MOLA-PEZZO
Dim Mt       As Double
Dim DP       As Double
Dim ALFAt    As Double
Dim SpT      As Double  'Spessore trasversale sul diam. primitivo (nominale)
'
Dim LEV      As Double  'lunghezza evolvente (> profilo attivo)
Dim LEV1     As Double  'lunghezza evolvente (tratto 1)
Dim LEV2     As Double  'lunghezza evolvente (tratto 2)
Dim LEV3     As Double  'lunghezza evolvente (tratto 3)
Dim LEV4     As Double  'lunghezza evolvente residua
'
Dim DIPU1    As Double  'Distanza Punti Evolvente (1� TRATTO)
Dim DIPU2    As Double  'Distanza Punti Evolvente (2� TRATTO)
Dim DIPU3    As Double  'Distanza Punti Evolvente (3� TRATTO)
Dim DIPU4    As Double  'Distanza Punti Evolvente (tratto residuo)
Dim LastDIPU As Double  'Distanza Punti Evolvente (ultimo tratto)
'
'NPE = NUMERO PUNTI EVOLVENTE COMPRESO PUNTO SOTTO IL SAP (MM0)
Dim NPE1     As Integer 'Numero punti evolvente (tratto 1)
Dim NPE2     As Integer 'Numero punti evolvente (tratto 2)
Dim NPE3     As Integer 'Numero punti evolvente (tratto 3)
Dim NPE4     As Integer 'Numero punti evolvente (tratto residuo)
'
Dim MM0      As Double  'TIF inizio evolvente
Dim MM1      As Double  'TIF inizio profilo attivo
Dim MM2      As Double  'TIF fine profilo attivo
Dim MME      As Double  'TIF inizio tratto evolvente senza correzioni
'
Dim Tif(2, 3) As Double 'TIF FINE TRATTO(fianco,tratto)
Dim Fianco    As Integer
'
Dim Corr  As Double
Dim Ti    As Double
'
Dim i     As Integer
Dim RX    As Double
Dim T0    As Double
Dim FI0   As Double
Dim angT  As Double
Dim MM()  As Double      'TIF dei punti dell'evolvente
Dim CB()  As Double      'Correzioni profilo K (Fianco,Correzione)
'
Dim B()   As Double      'Angolo di Pressione Mola
Dim L()   As Double      'LUNGHEZZA TRATTO CORREZIONE
Dim XD()  As Double      'Coordinata X Asse Vano Ingranaggio Trasv
Dim YD()  As Double      'Coordinata Y Asse Vano Ingranaggio Trasv
Dim XCT   As Double      'X CENTRO RTESTA INGRANAGGIO
Dim YCT   As Double      'Y CENTRO RTESTA INGRANAGGIO
Dim ACT   As Double      'ANG ASSE VANO CENTRO RTESTA INGRANAGGIO
Dim AA()  As Double
'
Dim SapMin      As Double  'SAP minimo
Dim EapMin      As Double  'EAP minimo
Dim stmp        As String
Dim Aux         As Integer
Dim Pt          As Double
Dim SbT         As Double
Dim EpT         As Double
Dim AlfaPunta   As Double
Dim DPunta      As Double
'
On Error GoTo ErrCalcolaProfilo
  '
  Call CalcoloSpessore(INP.Mn, INP.Z, INP.ALFA, INP.Beta, INP.S0Nn, INP.k, INP.Wk, INP.DR, INP.QR, INP.X)
  Mt = INP.Mn / Cos(INP.Beta)
  DP = Mt * INP.Z
  ALFAt = Atn(Tan(INP.ALFA) / Cos(INP.Beta))
  db = DP * Cos(ALFAt)
  Betab = Atn(Tan(INP.Beta) * Cos(ALFAt))
  SpT = INP.S0Nn / Cos(INP.Beta)
  '------- passo elica (sez. assiale) -----------
  If INP.Beta = 0 Then
  PAS = 999999
  Else
  PAS = Tan(PI / 2 - INP.Beta) * PI * DP
  End If
  '----------- passo sez. trasversale -----------
  Pt = Mt * PI
  '----------------------------------------------
  EpT = Pt - SpT
  SbT = db * ((INP.S0Nn / (Cos(INP.Beta) * DP)) + inv(ALFAt))
  AlfaPunta = Ainv(SbT / db)
  DPunta = db / Cos(AlfaPunta)
  If (INP.DEm <= 0) Then
    StopRegieEvents
    MsgBox "Wheel diameter shall be > 0", vbExclamation, "SUB: CalcolaProfilo"
    ResumeRegieEvents
    Exit Sub
  End If
  If (INP.DInt <= 0) Then
    StopRegieEvents
    MsgBox "Root diameter shall be > 0", vbExclamation, "SUB: CalcolaProfilo"
    ResumeRegieEvents
    Exit Sub
  End If
  'Interasse mola-ingranaggio
  IL = (INP.DEm + INP.DInt) / 2
  'CONTROLLO DISTANZA PUNTI MININA
  If (INP.DIPU = 0) Then INP.DIPU = 0.5
  'SE IL NUMERO DEI PUNTI PER IL RAGGIO DI FONDO � MINORE DI 3 PRENDO 5
  If (INP.NPR1 < 3) Then INP.NPR1 = 5
  'CALCOLO DEL DIAMETRO SAP MINIMO
  SapMin = 2 * Sqr((db / 2) ^ 2 + INP.DIPU ^ 2)
  If (INP.DSAP < SapMin) Then
    StopRegieEvents
    MsgBox "SAP < SAPmin " & Round(SapMin, 4), vbCritical, "SUB: CalcolaProfilo"
    ResumeRegieEvents
    Exit Sub
  End If
  MM1 = Sqr(INP.DSAP ^ 2 - db ^ 2) / 2
  'CALCOLO DEL DIAMETRO EAP MINIMO
  EapMin = 2 * Sqr((db / 2) ^ 2 + (INP.DIPU * 3 + MM1) ^ 2)
  If (INP.DEAP < EapMin) Then
    StopRegieEvents
    MsgBox "EAP < EAPmin " & Round(EapMin, 4), vbCritical, "SUB: CalcolaProfilo"
    ResumeRegieEvents
    Exit Sub
  End If
  MM2 = Sqr(INP.DEAP ^ 2 - db ^ 2) / 2
  'CONTROLLO NUMERO PUNTI EVOLVENTE
  LEV = MM2 - MM1
  Aux = Int(LEV / INP.DIPU + 0.5) + 1
  If (Int(Aux / 2) <> Aux / 2) Then Aux = Aux + 1
  If (Aux + 1 < INP.NPEmin) Then
    stmp = "Evaluated Envolute points EEp= " & Format$((Aux + 1), "###0") & Chr(13)
    stmp = stmp & "Min points  Mip= " & Format$(INP.NPEmin, "###0") & Chr(13) & Chr(13)
    stmp = stmp & "EEp  < Mip" & Chr(13) & Chr(13)
    INP.DIPU = LEV / (INP.NPEmin - 2)
    stmp = stmp & " --> " & "points distance is " & Format(INP.DIPU, " #0.##0")
    StopRegieEvents
    MsgBox stmp, vbCritical, "SUB: CalcolaProfilo"
    ResumeRegieEvents
  End If
  If (Aux + 1 > INP.NPEmax) Then
    stmp = "Evaluated Envolute points EEp= " & Format$((Aux + 1), "###0") & Chr(13)
    stmp = stmp & "Max points  Map= " & Format$(INP.NPEmax, "###0") & Chr(13) & Chr(13)
    stmp = stmp & "EEp  > Map" & Chr(13) & Chr(13)
    INP.DIPU = LEV / (INP.NPEmax - 2)
    stmp = stmp & vbCrLf & " --> " & "points distance is " & Format(INP.DIPU, " #0.##0")
    StopRegieEvents
    MsgBox stmp, vbCritical, "SUB: CalcolaProfilo"
    ResumeRegieEvents
  End If
  'NUMERO PUNTI PER RAGGIO DI TESTA
  If (INP.RT = 0) Then NP1 = 1 Else NP1 = INP.NPR1
  For Fianco = 1 To 2
    'CONVERSIONE TIF/DIA IN FUNZIONE DELLA VALUTAZIONE
    If INP.ValutazioneDIA Then
      If (INP.TifDia(Fianco, 1) > 0) Then Tif(Fianco, 1) = 0.5 * Sqr(INP.TifDia(Fianco, 1) ^ 2 - db ^ 2)
      If (INP.TifDia(Fianco, 2) > 0) Then Tif(Fianco, 2) = 0.5 * Sqr(INP.TifDia(Fianco, 2) ^ 2 - db ^ 2)
      If (INP.TifDia(Fianco, 3) > 0) Then Tif(Fianco, 3) = 0.5 * Sqr(INP.TifDia(Fianco, 3) ^ 2 - db ^ 2)
    Else
      Tif(Fianco, 1) = INP.TifDia(Fianco, 1)
      Tif(Fianco, 2) = INP.TifDia(Fianco, 2)
      Tif(Fianco, 3) = INP.TifDia(Fianco, 3)
    End If
    '######################################################################################
    '--------------------------------------------------------------------------------------
    ' CONTROLLO I "TIF" E RICALCOLO I DIPU NEI TRATTI INDIVIDUATI DAI "TIF"
    ' IN MODO CHE:
    '    1. VENGANO RISPETTATI I "TIF"
    '    2. SI ABBIA UN NUMERO DISPARI DI PUNTI SUL FIANCO (RT+EVOLV+RACCF+RF)
    '       | Considero  pari i punti del raggio di testa a meno dell'EAP,
    '       |            pari i pti dei tratti 4, 3 e 2
    '       |            pari i pti del tratto 1 (senza MM0)
    '       |            il pto MM0 rende la somma di questi punti dispari (l'indice del pto MM0 � NPE+NP1)
    '       |            pari i punti del racc.fondo (senza MM0)
    '       |            pari i pti del raggio di fondo (senza MM0 o l'ultimo punto del racc.fondo
    '--------------------------------------------------------------------------------------
    NPE1 = 0:   NPE2 = 0:  NPE3 = 0:  NPE4 = 0
    LEV1 = 0:   LEV2 = 0:  LEV3 = 0:  LEV4 = 0
    DIPU1 = 0: DIPU2 = 0: DIPU3 = 0: DIPU4 = 0: LastDIPU = 0
    'Controllo TIF1
    If (Tif(Fianco, 1) > MM1) Then
      If (Tif(Fianco, 1) > MM2) Then Tif(Fianco, 1) = MM2
      'ricalcolo DIPU1
      LEV1 = Tif(Fianco, 1) - MM1
      NPE1 = Int(LEV1 / INP.DIPU + 0.5) + 1
      If (Int(NPE1 / 2) <> NPE1 / 2) Then NPE1 = NPE1 + 1
      DIPU1 = LEV1 / (NPE1 - 1)
      LastDIPU = DIPU1
      MM0 = MM1 - DIPU1
      MME = Tif(Fianco, 1)
      'Controllo TIF2
      If (Tif(Fianco, 2) > Tif(Fianco, 1)) Then
        If (Tif(Fianco, 2) > MM2) Then Tif(Fianco, 2) = MM2
        'ricalcolo DIPU2
        LEV2 = Tif(Fianco, 2) - Tif(Fianco, 1)
        If (LEV2 <> 0) Then
          NPE2 = Int(LEV2 / INP.DIPU + 0.5)
          If (Int(NPE2 / 2) <> NPE2 / 2) Then NPE2 = NPE2 + 1
          DIPU2 = LEV2 / NPE2
          LastDIPU = DIPU2
          MME = Tif(Fianco, 2)
        End If
        ' Controllo TIF3
        If (Tif(Fianco, 3) > Tif(Fianco, 2)) Then
          If (Tif(Fianco, 3) > MM2) Then Tif(Fianco, 3) = MM2
          'ricalcolo DIPU3
          LEV3 = Tif(Fianco, 3) - Tif(Fianco, 2)
          If (LEV3 <> 0) Then
            NPE3 = Int(LEV3 / INP.DIPU + 0.5)
            If (Int(NPE3 / 2) <> NPE3 / 2) Then NPE3 = NPE3 + 1
            DIPU3 = LEV3 / NPE3
            LastDIPU = DIPU3
            MME = Tif(Fianco, 3)
          End If
        End If
      End If
    Else
      LEV1 = MM2 - MM1
      NPE1 = Int(LEV1 / INP.DIPU + 0.5) + 1
      If (Int(NPE1 / 2) <> NPE1 / 2) Then NPE1 = NPE1 + 1
      DIPU1 = LEV1 / (NPE1 - 1)
      LastDIPU = DIPU1
      MM0 = MM1 - DIPU1
      MME = MM2
      LEV2 = 0: LEV3 = 0
    End If
    '------------------------------------------------------------------------
    'SE L'ULTIMO TIF INSERITO (=MME) NON SUPERA L'EAP, BATTEZZO UN TRATTO 4
    'E CALCOLO DIPU4 IN MODO DA RISPETTARE L'EAP
    LEV4 = 0
    If (MME < MM2) Then
      LEV4 = MM2 - MME
      NPE4 = Int(LEV4 / INP.DIPU + 0.5)
      If (Int(NPE4 / 2) <> NPE4 / 2) Then NPE4 = NPE4 + 1
      DIPU4 = LEV4 / NPE4
      LastDIPU = DIPU4
    End If
    '------------------------------------------------------------------------
    '######################################################################################
    '********************************************************
    ' numero punti evolvente da SAP a EAP (profilo attivo)
    ' NB: NPE non comprende MM0 (ma comprende l'ultimo punto del raggio di testa)
    NPE = NPE1 + NPE2 + NPE3 + NPE4
    '********************************************************
    If (Fianco = 1) Then 'SERVE PER DIMENSIONaRE I VETTORI SOLO UNA VOLTA
      '*************************************************************
      ' IL DIMENSIONAMENTO DEI VETTORI DEVE ESSERE ANCORA SISTEMATO
      '*************************************************************
      ReDim MM(NP1 + NPE + 1 + INP.NPR1 * 2)
      ReDim CB(2, NP1 + NPE)
      ReDim A(2, NP1 + NPE + INP.NPR1 + 7)
      ReDim B(NP1 + NPE + INP.NPR1 + 7)
      ReDim L(2, NP1 + NPE + INP.NPR1 + 7)
      ReDim D(2, NP1 + NPE + INP.NPR1 + 7)
      ReDim AlfaM(2, NP1 + NPE + INP.NPR1 + 7)
      ReDim API(2, NP1 + NPE + INP.NPR1 + 7)
      ReDim X(2, NP1 + NPE + INP.NPR1 + 7)
      ReDim Y(2, NP1 + NPE + INP.NPR1 + 7)
      ReDim XD(2, NP1 + NPE + INP.NPR1 + 7)
      ReDim YD(2, NP1 + NPE + INP.NPR1 + 7)
    End If
    '
        If (INP.TipoCORR = "KPARAB") Then
      Call CalcProfK_parabola(Fianco, NPE, DP / 2, db / 2, MM0, MM1, MM2, INP.DIPU, Tif, INP.Rastr, CB)
    ElseIf (INP.TipoCORR = "KSTD") Then
      Call CalcolaProfiloK(Fianco, NPE, DP / 2, db / 2, MM0, MM1, MM2, DIPU1, DIPU2, DIPU3, Tif, INP.Rastr, INP.BOMB, CB)
    Else
      StopRegieEvents
      MsgBox "PROFILE CORRECTORS HAVE NOT BEEN DEFINED!!", vbInformation, "SUB: CalcolaProfilo"
      ResumeRegieEvents
    End If
    '
    '*********************************
    '*********   EVOLVENTE   *********
    '*********************************
    Dim ii        As Integer
    Dim X0        As Double
    Dim Y0        As Double
    Dim R0        As Double 'ULTIMO PUNTO
    Dim LastDb(2) As Double
    Dim D1        As Double
    Dim A1        As Double
    Dim D2        As Double
    Dim A2        As Double
    '--------- TRATTO 4 ---------
    'CALCOLO MOLA x FIANCO
    For ii = 1 To NPE4
      i = ii + NP1 - 1
      'SVILUPPO LINEARE DEL PROFILO INGRANAGGIO
      MM(i) = MM2 - DIPU4 * (ii - 1)
      RX = Sqr(MM(i) ^ 2 + (db / 2) ^ 2)
      'CORREZIONE EVOLVENTE
      Corr = CB(Fianco, NPE - ii + 1) 'PROFILO K
      Call Calcolo_T0FI0(2 * RX, Corr, db, db, DP, INP.Z, INP.ALFA, INP.Beta, SpT, T0, FI0, A(Fianco, i))
      'CALCOLO MOLA
      Call Purea(RX, INP.Beta, DP / 2, INP.BetaMola, T0, FI0, IL, XD(Fianco, i), YD(Fianco, i), Ti, X(Fianco, i), Y(Fianco, i), B(i))
      'CONVERSIONI
      L(Fianco, i) = (Ti * PAS) / (PI * 2)
      D(Fianco, i) = Sqr((2 * MM(i)) ^ 2 + db ^ 2)
      AlfaM(Fianco, i) = B(i) * (PI / 180)
      'API(Fianco, i) = (T0 - FI0) * 180 / PI
      API(Fianco, i) = Atn(MM(i) / (db / 2)) * (180 / PI)
    Next ii
    LastDb(Fianco) = db
    '------------ TRATTO 3 -------------
    Dim DB3 As Double
    If (NPE3 <> 0) Then
      'calcolo Db tratto3
      ii = 1
      i = ii + NP1 + NPE4 - 1
      MM(i) = MM2 - DIPU4 * NPE4 - DIPU3 * (ii - 1)
      D1 = 2 * Sqr(MM(i) ^ 2 + (db / 2) ^ 2)
      Corr = CB(Fianco, NPE - NPE4 - ii + 1) 'PROFILO K
      Call Calcolo_T0FI0(D1, Corr, db, db, DP, INP.Z, INP.ALFA, INP.Beta, SpT, T0, FI0, A1)
      ii = NPE3
      i = ii + NP1 + NPE4 - 1
      MM(i) = MM2 - DIPU4 * NPE4 - DIPU3 * (ii - 1)
      D2 = 2 * Sqr(MM(i) ^ 2 + (db / 2) ^ 2)
      Corr = CB(Fianco, NPE - NPE4 - ii + 1) 'PROFILO K
      Call Calcolo_T0FI0(D2, Corr, db, db, DP, INP.Z, INP.ALFA, INP.Beta, SpT, T0, FI0, A2)
      Call CalcDbase_2punti(INP.Z, D1, A1, D2, A2, DB3)
      If DB3 <= 0 Then DB3 = db
      LastDb(Fianco) = DB3
    End If
    For ii = 1 To NPE3
      i = ii + NP1 + NPE4 - 1
      'SVILUPPO LINEARE DEL PROFILO INGRANAGGIO
      MM(i) = MM2 - DIPU4 * NPE4 - DIPU3 * (ii - 1)
      RX = Sqr(MM(i) ^ 2 + (db / 2) ^ 2)
      'CORREZIONE EVOLVENTE
      Corr = CB(Fianco, NPE - NPE4 - ii + 1) 'PROFILO K
      Call Calcolo_T0FI0(2 * RX, Corr, DB3, db, DP, INP.Z, INP.ALFA, INP.Beta, SpT, T0, FI0, A(Fianco, i))
      'CALCOLO MOLA
      Call Purea(RX, INP.Beta, DP / 2, INP.BetaMola, T0, FI0, IL, XD(Fianco, i), YD(Fianco, i), Ti, X(Fianco, i), Y(Fianco, i), B(i))
      'CONVERSIONI
      L(Fianco, i) = (Ti * PAS) / (PI * 2)
      D(Fianco, i) = Sqr((2 * MM(i)) ^ 2 + db ^ 2)
      AlfaM(Fianco, i) = B(i) * (PI / 180)
      'API(Fianco, i) = (T0 - FI0) * 180 / PI '
      API(Fianco, i) = Atn(MM(i) / (DB3 / 2)) * (180 / PI)
    Next ii
    '--------- TRATTO 2 ---------------
    Dim DB2 As Double
    If (NPE2 <> 0) Then
      'calcolo Db tratto2
      ii = 1
      i = ii + NP1 + NPE4 + NPE3 - 1
      MM(i) = MM2 - DIPU4 * NPE4 - DIPU3 * NPE3 - DIPU2 * (ii - 1)
      D1 = 2 * Sqr(MM(i) ^ 2 + (db / 2) ^ 2)
      Corr = CB(Fianco, NPE - NPE4 - NPE3 - ii + 1) 'PROFILO K
      Call Calcolo_T0FI0(D1, Corr, db, db, DP, INP.Z, INP.ALFA, INP.Beta, SpT, T0, FI0, A1)
      ii = NPE2 + 1
      i = ii + NP1 + NPE4 + NPE3 - 1
      MM(i) = MM2 - DIPU4 * NPE4 - DIPU3 * NPE3 - DIPU2 * (ii - 1)
      D2 = 2 * Sqr(MM(i) ^ 2 + (db / 2) ^ 2)
      Corr = CB(Fianco, NPE - NPE4 - NPE3 - ii + 1) 'PROFILO K
      Call Calcolo_T0FI0(D2, Corr, db, db, DP, INP.Z, INP.ALFA, INP.Beta, SpT, T0, FI0, A2)
      Call CalcDbase_2punti(INP.Z, D2, A2, D1, A1, DB2)
      If DB2 <= 0 Then DB2 = db
      LastDb(Fianco) = DB2
    End If
    For ii = 1 To NPE2
      i = ii + NP1 + NPE4 + NPE3 - 1
      'SVILUPPO LINEARE DEL PROFILO INGRANAGGIO
      MM(i) = MM2 - DIPU4 * NPE4 - DIPU3 * NPE3 - DIPU2 * (ii - 1)
      RX = Sqr(MM(i) ^ 2 + (db / 2) ^ 2)
      'CORREZIONE EVOLVENTE
      Corr = CB(Fianco, NPE - NPE4 - NPE3 - ii + 1) 'PROFILO K
      Call Calcolo_T0FI0(2 * RX, Corr, DB2, db, DP, INP.Z, INP.ALFA, INP.Beta, SpT, T0, FI0, A(Fianco, i))
      'CALCOLO MOLA
      Call Purea(RX, INP.Beta, DP / 2, INP.BetaMola, T0, FI0, IL, XD(Fianco, i), YD(Fianco, i), Ti, X(Fianco, i), Y(Fianco, i), B(i))
      'CONVERSIONI
      L(Fianco, i) = (Ti * PAS) / (PI * 2)
      D(Fianco, i) = Sqr((2 * MM(i)) ^ 2 + db ^ 2)
      AlfaM(Fianco, i) = B(i) * (PI / 180)
      'API(Fianco, i) = (T0 - FI0) * 180 / PI '
      API(Fianco, i) = Atn(MM(i) / (DB2 / 2)) * (180 / PI)
    Next ii
    '--------- TRATTO 1 ---------------
    Dim DB1 As Double
    If (NPE1 <> 0) Then
      'calcolo Db tratto1
      ii = 1
      i = ii + NP1 + NPE4 + NPE3 + NPE2 - 1
      MM(i) = MM2 - DIPU4 * NPE4 - DIPU3 * NPE3 - DIPU2 * NPE2 - DIPU1 * (ii - 1)
      D1 = 2 * Sqr(MM(i) ^ 2 + (db / 2) ^ 2)
      Corr = CB(Fianco, NPE - NPE4 - NPE3 - NPE2 - ii + 1)                      'PROFILO K
      Call Calcolo_T0FI0(D1, Corr, db, db, DP, INP.Z, INP.ALFA, INP.Beta, SpT, T0, FI0, A1)
      ii = NPE1
      i = ii + NP1 + NPE4 + NPE3 + NPE2 - 1
      MM(i) = MM2 - DIPU4 * NPE4 - DIPU3 * NPE3 - DIPU2 * NPE2 - DIPU1 * (ii - 1)
      D2 = 2 * Sqr(MM(i) ^ 2 + (db / 2) ^ 2)
      Corr = CB(Fianco, NPE - NPE4 - NPE3 - NPE2 - ii + 1)                    'PROFILO K
      Call Calcolo_T0FI0(D2, Corr, db, db, DP, INP.Z, INP.ALFA, INP.Beta, SpT, T0, FI0, A2)
      Call CalcDbase_2punti(INP.Z, D1, A1, D2, A2, DB1)
      If DB1 <= 0 Then DB1 = db
      LastDb(Fianco) = DB1
    End If
    For ii = 1 To NPE1
      i = ii + NP1 + NPE4 + NPE3 + NPE2 - 1
      'SVILUPPO LINEARE DEL PROFILO INGRANAGGIO
      MM(i) = MM2 - DIPU4 * NPE4 - DIPU3 * NPE3 - DIPU2 * NPE2 - DIPU1 * (ii - 1)
      RX = Sqr(MM(i) ^ 2 + (db / 2) ^ 2)
      'CORREZIONE EVOLVENTE
      Corr = CB(Fianco, NPE - NPE4 - NPE3 - NPE2 - ii + 1)                   'PROFILO K
      Call Calcolo_T0FI0(2 * RX, Corr, DB1, db, DP, INP.Z, INP.ALFA, INP.Beta, SpT, T0, FI0, A(Fianco, i))
      'CALCOLO MOLA
      Call Purea(RX, INP.Beta, DP / 2, INP.BetaMola, T0, FI0, IL, XD(Fianco, i), YD(Fianco, i), Ti, X(Fianco, i), Y(Fianco, i), B(i))
      'CONVERSIONI
      L(Fianco, i) = (Ti * PAS) / (PI * 2)
      D(Fianco, i) = Sqr((2 * MM(i)) ^ 2 + db ^ 2)
      AlfaM(Fianco, i) = B(i) * (PI / 180)
      API(Fianco, i) = Atn(MM(i) / (DB1 / 2)) * (180 / PI)
    Next ii
    If LastDb(Fianco) = 0 Then LastDb(Fianco) = db
    '.... calcolo correzioni su MM0 ....
    ii = NPE1 + 1
    i = ii + NP1 + NPE4 + NPE3 + NPE2 - 1
    'calcolo dipu minima
    Dim DIPU0 As Double
    DIPU0 = INP.DIPU
    If DIPU1 > 0 And DIPU1 < DIPU0 Then DIPU0 = DIPU1
    If DIPU2 > 0 And DIPU2 < DIPU0 Then DIPU0 = DIPU2
    If DIPU3 > 0 And DIPU3 < DIPU0 Then DIPU0 = DIPU3
    'SVILUPPO LINEARE DEL PROFILO INGRANAGGIO
    MM(i) = MM2 - DIPU4 * NPE4 - DIPU3 * NPE3 - DIPU2 * NPE2 - DIPU1 * NPE1 - DIPU0 'INP.DIPU
    RX = Sqr(MM(i) ^ 2 + (db / 2) ^ 2)
    'CORREZIONE EVOLVENTE
    Corr = CB(Fianco, NPE - NPE4 - NPE3 - NPE2 - ii + 1)                   'PROFILO K
    Call Calcolo_T0FI0(2 * RX, Corr, DB1, db, DP, INP.Z, INP.ALFA, INP.Beta, SpT, T0, FI0, A(Fianco, i))
    'CALCOLO MOLA
    Call Purea(RX, INP.Beta, DP / 2, INP.BetaMola, T0, FI0, IL, XD(Fianco, i), YD(Fianco, i), Ti, X(Fianco, i), Y(Fianco, i), B(i))
    'CONVERSIONI
    L(Fianco, i) = (Ti * PAS) / (PI * 2)
    D(Fianco, i) = Sqr((2 * MM(i)) ^ 2 + db ^ 2)
    AlfaM(Fianco, i) = B(i) * (PI / 180)
    API(Fianco, i) = (T0 - FI0) * 180 / PI
    '*****************************************
    '*********    RAGGIO DI TESTA    *********
    '*****************************************
    'CORRFINE:
    '  '---- CALCOLO DATI MOLA ED INGRANAGGIO ULTIMO PUNTO EVOLVENTE (EAP) ----
    '    MM(NP1) = MM2  'MM(NP1) = Sqr(EAP ^ 2 - Db ^ 2)/2
    '    Rx = Sqr(MM(NP1) ^ 2 + (Db / 2) ^ 2)
    '    Pas = Tan(PI / 2 - INP.BETA) * PI * Dp
    '    Corr = CB(Fianco, NPE)
    '
    '    'Calcolo T0, FI0, angT[=a(Fianco, NP1)]
    '    Call Calcolo_T0FI0(Rx, Corr, Db / 2, Dp / 2, INP.Z, INP.Alfa, INP.BETA, SpT, T0, FI0, A(Fianco, NP1))
    '    'calcolo punto corrrispondente sulla mola
    '    Call Purea(Rx, INP.BETA, Dp / 2, INP.BetaMola, T0, FI0, IL, XD(Fianco, NP1), YD(Fianco, NP1), Ti, X(Fianco, NP1), Y(Fianco, NP1), B(NP1))
    '    L(Fianco, NP1) = (Ti * Pas) / (PI * 2)
    '    D(NP1) = 2 * Sqr(MM(NP1) ^ 2 + (Db / 2) ^ 2)
    '    API(fianco,NP1) = (T0 - FI0) * 180 / PI
    '    'API(fianco,NP1) = Atn(MM(NP1) / (Db / 2)) * 180 / PI
    '    'XD(Fianco, NP1) = D(NP1) / 2 * Sin(a(Fianco, NP1))
    '    'YD(Fianco, NP1) = D(NP1) / 2 * Cos(a(Fianco, NP1))
    '  '------------------------------------------------------------------------
    '***
    Dim dALFA          As Double
    Dim jj             As Double
    Dim Ang            As Double
    ReDim AA(INP.NPR1) As Double
    If (INP.RT > 0) Then
      'Centro R-testa
      YCT = YD(Fianco, NP1) - INP.RT * Sin(ToRad(API(Fianco, NP1)) + A(Fianco, NP1))
      XCT = XD(Fianco, NP1) + INP.RT * Cos(ToRad(API(Fianco, NP1)) + A(Fianco, NP1))
      ACT = Atn(XCT / YCT)
      If (INP.Beta < PI / 4) Then
        'ingranaggio: sez. trasversale
        'Suddivido l'arco descritto dal raggio di testa in "INP.NPR1-1" intervalli.
        'Il punto con indice INP.NPR1 � l'EAP
        'Il punto con indice 1 � l'intersezione tra:
        '  1. l'arco (di cui sopra) e
        '  2. la retta passante per il centro dell'arco e inclinata di inp.art
        '     rispetto alla retta che congiunge il centro del pezzo al centro dell'arco (vedi disegno)
        dALFA = (PI / 2 + ACT - INP.ART - API(Fianco, NP1) * PI / 180 - A(Fianco, NP1)) / (INP.NPR1 - 1) 'delta angolo in rd
        For jj = 1 To INP.NPR1
          i = jj
          Ang = PI / 2 + ACT - INP.ART - dALFA * (jj - 1)
          XD(Fianco, jj) = XCT - INP.RT * Cos(Ang)
          YD(Fianco, jj) = YCT + INP.RT * Sin(Ang)
          AA(jj) = Atn(XD(Fianco, jj) / YD(Fianco, jj))  'alfa (di RAB)
          A(Fianco, jj) = AA(jj) 'mi serve per scrivere alfa nel file (punti profilo)
          API(Fianco, jj) = (Ang - AA(jj)) * (180 / PI)         'beta (di RAB)
          RX = Sqr(XD(Fianco, jj) ^ 2 + YD(Fianco, jj) ^ 2)
          T0 = PI / 2 - AA(jj)
          FI0 = PI / 2 - Ang
          If FI0 > PI Then FI0 = FI0 - PI * 2
          If FI0 < -PI Then FI0 = FI0 + PI * 2
          Call Purea(RX, INP.Beta, DP / 2, INP.BetaMola, T0, FI0, IL, XD(Fianco, i), YD(Fianco, i), Ti, X(Fianco, i), Y(Fianco, i), B(i))
          AlfaM(Fianco, i) = ToRad(B(i))
          L(Fianco, i) = (Ti * PAS) / (PI * 2)
          D(Fianco, i) = 2 * RX
        Next jj
      Else
        'vite: sezione assiale
        Dim Apx As Double
        Dim Xa  As Double
        Dim Ya  As Double
        '------- EAP in coordinate assiali -------
        i = NP1
        'angolo di pressione assiale corrispondente a API(fianco,i)
        Apx = Atn(Tan(API(Fianco, i) * PI / 180) * PAS / (PI * D(Fianco, i)))
        ' Centro R-testa AX
        Ya = D(Fianco, i) / 2: Xa = A(Fianco, i) * PAS / 2 / PI 'coordinate punto EAP assiale
        '-----------------------------------------
        YCT = Ya - INP.RT * Sin(Apx)
        XCT = Xa + INP.RT * Cos(Apx)
        dALFA = (PI / 2 - Apx - INP.ART) / (INP.NPR1 - 1)  'delta angolo in rd
        For jj = 1 To INP.NPR1
          i = jj
          Ang = PI / 2 - INP.ART - dALFA * (jj - 1) 'Angolo AX rispetto Y
          Xa = XCT - INP.RT * Cos(Ang)
          Ya = YCT + INP.RT * Sin(Ang)
          RX = Ya
          'ALFA (di RAB) sulla sezione trasversale
          A(Fianco, i) = Xa / PAS * 2 * PI
          'BETA (di RAB) (sez. assiale - angolo che la tangente al profilo forma con l'asse y di rif.)
          API(Fianco, i) = Atn(Tan(Ang) * 2 * PI * RX / PAS) * 180 / PI
          XD(Fianco, i) = RX * Cos(A(Fianco, i))
          YD(Fianco, i) = RX * Sin(A(Fianco, i))
          T0 = PI / 2 - A(Fianco, i)
          FI0 = PI / 2 - A(Fianco, i) - ToRad(API(Fianco, i))
          If FI0 > PI Then FI0 = FI0 - PI * 2
          If FI0 < -PI Then FI0 = FI0 + PI * 2
          'CALCOLO MOLA
          Call Purea(RX, INP.Beta, DP / 2, INP.BetaMola, T0, FI0, IL, XD(Fianco, i), YD(Fianco, i), Ti, X(Fianco, i), Y(Fianco, i), B(i))
          AlfaM(Fianco, i) = ToRad(B(i))
          L(Fianco, i) = (Ti * PAS) / (PI * 2)
          D(Fianco, i) = 2 * RX
        Next jj
      End If
    End If
    '****************************************
    NPnt(Fianco) = NP1 + NPE
    NpntEv(Fianco) = NPE
    '****************************************
    '---------------- RAGGI DI RACCORDO SUI TIF ----------------
    XCRaccTIF(Fianco, 1) = 0: XCRaccTIF(Fianco, 2) = 0
    YCRaccTIF(Fianco, 1) = 0: YCRaccTIF(Fianco, 2) = 0
    If (INP.TipoCORR = "KSTD") Then
      Dim NPadd As Integer
      If Tif(Fianco, 1) > 0 And INP.RCT(Fianco, 1) > 0 Then
        'raccordo tratto1 - tratto2
        Call CalcRRaccSuTIF(INP.RCT(Fianco, 1), NP1 + NPE4 + NPE3 + NPE2, Fianco, PAS, D(), A(), API(), NPadd, XCRaccTIF(Fianco, 1), YCRaccTIF(Fianco, 1))
        NPnt(Fianco) = NPnt(Fianco) + NPadd
        NpntEv(Fianco) = NpntEv(Fianco) + NPadd
        'per disegno raggio di raccordo
        XC1rac(Fianco, 1) = Xc1: YC1rac(Fianco, 1) = Yc1: R1rac(Fianco, 1) = RR1
        XC2rac(Fianco, 1) = Xc2: YC2rac(Fianco, 1) = Yc2: R2rac(Fianco, 1) = RR2
        XT1rac(Fianco, 1) = XT1: YT1rac(Fianco, 1) = YT1
        XT2rac(Fianco, 1) = XT2: YT2rac(Fianco, 1) = YT2
        'CALCOLO MOLA
        For i = 1 To NPnt(Fianco)
            T0 = PI / 2 - A(Fianco, i)
            FI0 = PI / 2 - A(Fianco, i) - ToRad(API(Fianco, i))
            If FI0 > PI Then FI0 = FI0 - PI * 2
            If FI0 < -PI Then FI0 = FI0 + PI * 2
            Call Purea(D(Fianco, i) / 2, INP.Beta, DP / 2, INP.BetaMola, T0, FI0, IL, XD(Fianco, i), YD(Fianco, i), Ti, X(Fianco, i), Y(Fianco, i), B(i))
            AlfaM(Fianco, i) = ToRad(B(i))
            L(Fianco, i) = (Ti * PAS) / (PI * 2)
        Next i
      End If
      If Tif(Fianco, 2) > 0 And INP.RCT(Fianco, 2) > 0 Then
        'raccordo tratto2 - tratto3
        Call CalcRRaccSuTIF(INP.RCT(Fianco, 2), NP1 + NPE4 + NPE3, Fianco, PAS, D(), A(), API(), NPadd, XCRaccTIF(Fianco, 2), YCRaccTIF(Fianco, 2))
        NPnt(Fianco) = NPnt(Fianco) + NPadd
        NpntEv(Fianco) = NpntEv(Fianco) + NPadd
        'per disegno raggio di raccordo
        XC1rac(Fianco, 2) = Xc1: YC1rac(Fianco, 2) = Yc1: R1rac(Fianco, 2) = RR1
        XC2rac(Fianco, 2) = Xc2: YC2rac(Fianco, 2) = Yc2: R2rac(Fianco, 2) = RR2
        XT1rac(Fianco, 2) = XT1: YT1rac(Fianco, 2) = YT1
        XT2rac(Fianco, 2) = XT2: YT2rac(Fianco, 2) = YT2
        'CALCOLO MOLA
        For i = 1 To NPnt(Fianco)
            T0 = PI / 2 - A(Fianco, i)
            FI0 = PI / 2 - A(Fianco, i) - ToRad(API(Fianco, i))
            If FI0 > PI Then FI0 = FI0 - PI * 2
            If FI0 < -PI Then FI0 = FI0 + PI * 2
            Call Purea(D(Fianco, i) / 2, INP.Beta, DP / 2, INP.BetaMola, T0, FI0, IL, XD(Fianco, i), YD(Fianco, i), Ti, X(Fianco, i), Y(Fianco, i), B(i))
            AlfaM(Fianco, i) = ToRad(B(i))
            L(Fianco, i) = (Ti * PAS) / (PI * 2)
        Next i
      End If
    End If
    '-----------------------------------------------------------
    Dim iSAP  As Integer: iSAP = NP1 + NPE - 1
    Dim iTIF1 As Integer: iTIF1 = NP1 + NPE4 + NPE3 + NPE2
    Dim iTIF2 As Integer: iTIF2 = NP1 + NPE4 + NPE3
    Dim iTIF3 As Integer: iTIF3 = NP1 + NPE4
    Dim iEAP  As Integer: iEAP = NP1
    '
    If (INP.Beta > PI / 4) Then
      'ANGOLO RASTREMAZIONE SUI TIF
      AngRastr(Fianco, 1) = 0: AngRastr(Fianco, 2) = 0
      Dim AP1pre    As Double
      Dim AP1post   As Double
      Dim AP2pre    As Double
      Dim AP2post   As Double
      Dim AP1preAx  As Double
      Dim AP1postAx As Double
      Dim AP2preAx  As Double
      Dim AP2postAx As Double
      Call CalcAngPressSuTIF(INP.Z, Fianco, iSAP, iTIF1, iTIF2, iTIF3, D(), A(), AP1pre, AP1post, AP2pre, AP2post)
      'AP assiale sul TIF corrispondente a quello nella sez.trasversale
      AP1preAx = Atn(Tan(AP1pre) * PAS / (PI * D(Fianco, iTIF1)))
      AP1postAx = Atn(Tan(AP1post) * PAS / (PI * D(Fianco, iTIF1)))
      AP2preAx = Atn(Tan(AP2pre) * PAS / (PI * D(Fianco, iTIF2)))
      AP2postAx = Atn(Tan(AP2post) * PAS / (PI * D(Fianco, iTIF2)))
      AngRastr(Fianco, 1) = AP1postAx - AP1preAx
      AngRastr(Fianco, 2) = AP2postAx - AP2preAx
    End If
    '------------------- PRIMITIVO --------------------
    '       - Calcolo solo per visualizzazione -
    i = 0
    MM(i) = 0.5 * Sqr(DP ^ 2 - db ^ 2)
    RX = DP / 2
    Dim DbCorr As Double
    DbCorr = db
    'CORREZIONE EVOLVENTE
    Corr = 0  'andrebbe ricalcolata - magari approssimata (vedi CalcolaProfiloK  CALCK2:)
    Call Calcolo_T0FI0(DP, Corr, DbCorr, db, DP, INP.Z, INP.ALFA, INP.Beta, SpT, T0, FI0, A(Fianco, i))
    'CALCOLO MOLA
    Call Purea(RX, INP.Beta, DP / 2, INP.BetaMola, T0, FI0, IL, XD(Fianco, i), YD(Fianco, i), Ti, X(Fianco, i), Y(Fianco, i), B(i))
    'CONVERSIONI
    L(Fianco, i) = (Ti * PAS) / (PI * 2)
    D(Fianco, i) = DP
    AlfaM(Fianco, i) = B(i) * (PI / 180)
    API(Fianco, i) = (T0 - FI0) * 180 / PI
    '---------------------------------------------------
  Next Fianco
  '
  Dim Npunti  As Double
  Dim vi      As Double
  Dim Yi      As Double
  Dim Xi      As Double
  Dim Ytr     As Double
  Dim Xtr     As Double
  Dim Ri      As Double
  Dim hu1     As Double
  Dim HU2     As Double
  Dim AngMed  As Double
  Dim Sh      As Double
  Dim PM      As Double
  Dim B1      As Double
  Dim dALFAm  As Double
  Dim MMEst   As Double
  Dim Rbom    As Double
  '
  '*************************************************
  '***  CALCOLO FONDO O MODIFICA RACCORDO FONDO  ***
  '*************************************************
  Dim Ytrmin   As Double
  Dim Ytrmax   As Double
  Dim YiAux(2) As Double
  Dim AngR     As Double
  Yi = 0
  For Fianco = 1 To 2
    NPE = NpntEv(Fianco)
    AngR = INP.ARF
    AlfaM(Fianco, NP1 + NPE) = AlfaM(Fianco, NP1 + NPE) + AngR
    Yi = IL - INP.DInt / 2
    Ytr = Yi - INP.RF * (1 - Sin(AlfaM(Fianco, NP1 + NPE)))
    If Y(Fianco, NP1 + NPE) < Ytr Then
        YiAux(Fianco) = Yi
    Else
        YiAux(Fianco) = Yi + (Y(Fianco, NP1 + NPE) - Ytr)
        'YiAux(Fianco) = Y(Fianco, NP1 + NPE) + INP.RF * (1 - Sin(AlfaM(Fianco, NP1 + NPE)))
    End If
    '---
    '    If Fianco = 1 Then Ytrmin = Ytr
    '    If Ytr < Ytrmin Then Ytrmin = Ytr
    '---
    '    If Fianco = 1 Then Ytrmax = Ytr
    '    If Ytr > Ytrmax Then Ytrmax = Ytr
  Next Fianco
  'Ytr = Ytrmin
  'Ytr = Ytrmax
  Dim YiMod As Double
  If (YiAux(1) > YiAux(2)) Then YiMod = YiAux(1) Else YiMod = YiAux(2)
  '
  For Fianco = 1 To 2
    'CALCOLO FONDO O MODIFICA RACCORDO FONDO (Modifiche del 28/04/03)
    NPE = NpntEv(Fianco)
    NTR = 0
    If INP.Trocoid Then
        Dim ASAP        As Double   'Alfa (di RAB) del SAP
        Dim RSC         As Double
        Dim SAPOTT      As Double
        Dim INTERF$
        Dim Dtroc()     As Double
        Dim Atroc()     As Double
        Dim APItroc()   As Double
        Dim Xtroc()     As Double
        Dim Ytroc()     As Double
        Dim ALFAMtroc() As Double
        'Yi = IL - INP.DInt / 2
        Call TROCOIDE(D(Fianco, NP1 + NPE), A(Fianco, NP1 + NPE), B(NP1 + NPE), INP.RF, INP.NPR1, NP1 + NPE, INP.Z, INP.Mn, INP.ALFA, INP.Beta, INP.DInt, LastDb(Fianco), DP, INP.BetaMola, IL, RSC, SAPOTT, INTERF$, Dtroc, Atroc, APItroc, Xtroc, Ytroc, ALFAMtroc)
        For i = NP1 + NPE + 1 To NP1 + NPE + INP.NPR1
          D(Fianco, i) = Dtroc(i)
          A(Fianco, i) = Atroc(i)
          API(Fianco, i) = APItroc(i)
          X(Fianco, i) = Xtroc(i)
          Y(Fianco, i) = Ytroc(i)
          AlfaM(Fianco, i) = ALFAMtroc(i)
        Next i
        Ri = IL - Yi
        INP.DInt = Ri * 2
        hu1 = Y(Fianco, NP1 + NPE) - Y(Fianco, 1)
        HU2 = hu1 + INP.RF * (1 - Sin(AlfaM(Fianco, NP1 + NPE)))
        AngMed = Atn((X(Fianco, NP1) - X(Fianco, NP1 + NPE)) / hu1)
        Sh = hu1 / Cos(AngMed) / 2
        PM = Int(NPE / 2)
        B1 = X(Fianco, NP1 + NPE) - (Y(Fianco, PM) - Y(Fianco, NP1 + NPE)) * Tan(AngMed) - X(Fianco, PM)
        B1 = B1 * Cos(AngMed)
        Rbom = (Sh ^ 2 + B1 ^ 2) / 2 / B1  'raggio bombatura
        vi = X(Fianco, NP1 + NPE + INP.NPR1) * 2  'NEW 10-11-2005
        NTR = 0  '*** controllare ***
    Else
        'MODIFICA RACCORDO FONDO
        Dim j         As Integer
        Dim Epsilon   As Double
        Dim distMM0TR As Double
        Epsilon = 0.2
        Yi = YiMod
        Ytr = Yi - INP.RF * (1 - Sin(AlfaM(Fianco, NP1 + NPE)))
        If Y(Fianco, NP1 + NPE) <= Ytr Then
          Xtr = X(Fianco, NP1 + NPE) - (Ytr - Y(Fianco, NP1 + NPE)) * Tan(AlfaM(Fianco, NP1 + NPE))
          Xi = Xtr - INP.RF * Cos(AlfaM(Fianco, NP1 + NPE))
          distMM0TR = Sqr((X(Fianco, NP1 + NPE) - Xtr) ^ 2 + (Y(Fianco, NP1 + NPE) - Ytr) ^ 2)
          If (distMM0TR > Epsilon) Then
            'SI AGGIUNGE UN TRATTO RETTILINEO DI RACCORDO
            'TRA MM0 ED IL PRIMO PUNTO DEL RAGGIO DI FONDO
            '--- calcolo punto intermedio (fa si che si abbiano tre punti sul tratto rettilineo) ---
            jj = NP1 + NPE + 1
            AlfaM(Fianco, jj) = AlfaM(Fianco, NP1 + NPE)
            X(Fianco, jj) = (Xtr + X(Fianco, NP1 + NPE)) / 2
            Y(Fianco, jj) = (Ytr + Y(Fianco, NP1 + NPE)) / 2
            Call PuntoIngDaMola(X(Fianco, jj), Y(Fianco, jj), AlfaM(Fianco, jj), IL, INP.BetaMola, PAS, L(Fianco, jj), D(Fianco, jj), A(Fianco, jj), XD(Fianco, jj), YD(Fianco, jj), API(Fianco, jj))
            '---------------------------------------------------------------------------------------
            'RAGGIO DI FONDO
            dALFAm = (PI / 2 - AlfaM(Fianco, NP1 + NPE)) / (INP.NPR1 - 1)
            'PROFILO INGRANAGGIO SEZ. TRASVERSALE
            For j = 2 To INP.NPR1 + 1
                jj = NP1 + NPE + j
                AlfaM(Fianco, jj) = AlfaM(Fianco, NP1 + NPE) + dALFAm * (j - 2)
                X(Fianco, jj) = Xi + INP.RF * Cos(AlfaM(Fianco, jj))
                Y(Fianco, jj) = Yi - INP.RF * (1 - Sin(AlfaM(Fianco, jj)))
                Call PuntoIngDaMola(X(Fianco, jj), Y(Fianco, jj), AlfaM(Fianco, jj), IL, INP.BetaMola, PAS, L(Fianco, jj), D(Fianco, jj), A(Fianco, jj), XD(Fianco, jj), YD(Fianco, jj), API(Fianco, jj))
            Next j
            NTR = 1
          Else
            'se la distanza fra MM0 ed il primo punto del raggio di fondo � < Epsilon...
            'sostituisco MM0 con il primo punto del raggio di fondo
            X(Fianco, NP1 + NPE) = Xtr
            Y(Fianco, NP1 + NPE) = Ytr
            'RAGGIO DI FONDO
            dALFAm = (PI / 2 - AlfaM(Fianco, NP1 + NPE)) / (INP.NPR1 - 1)
            'PROFILO INGRANAGGIO SEZ. TRASVERSALE
            For j = 1 To INP.NPR1
                jj = NP1 + NPE + j - 1
                AlfaM(Fianco, jj) = AlfaM(Fianco, NP1 + NPE) + dALFAm * (j - 1)
                X(Fianco, jj) = Xi + INP.RF * Cos(AlfaM(Fianco, jj))
                Y(Fianco, jj) = Yi - INP.RF * (1 - Sin(AlfaM(Fianco, jj)))
                Call PuntoIngDaMola(X(Fianco, jj), Y(Fianco, jj), AlfaM(Fianco, jj), IL, INP.BetaMola, PAS, L(Fianco, jj), D(Fianco, jj), A(Fianco, jj), XD(Fianco, jj), YD(Fianco, jj), API(Fianco, jj))
            Next j
            NTR = -1
          End If
        Else
          ' CASO:  Y(Fianco, NP1 + NPE) >= Ytr
          'se il raggio di fondo calcolato a partire dal diametro interno supera il punto MM0,
          'impongo che il raggio di fondo parta da MM0 e non rispetto il diametro interno voluto.
          'NOTA: sul pezzo si ottiene un diametro interno pi� piccolo di quello voluto
          Xtr = X(Fianco, NP1 + NPE)
          Ytr = Y(Fianco, NP1 + NPE)
          Yi = Y(Fianco, NP1 + NPE) + INP.RF * (1 - Sin(AlfaM(Fianco, NP1 + NPE)))
          Xi = X(Fianco, NP1 + NPE) - INP.RF * Cos(AlfaM(Fianco, NP1 + NPE))
          StopRegieEvents
          MsgBox "Obtained Root Diameter = " & Format((IL - Yi) * 2, "##0.#0") & vbCrLf & "less than" & vbCrLf & "Required Root Diameter = " & Format(INP.DInt, "##0.#0"), vbCritical, "WARNING"
          ResumeRegieEvents
          'RAGGIO DI FONDO
          dALFAm = (PI / 2 - AlfaM(Fianco, NP1 + NPE)) / (INP.NPR1 - 1)
          'PROFILO INGRANAGGIO SEZ. TRASVERSALE
          For j = 1 To INP.NPR1
              jj = NP1 + NPE + j - 1
              AlfaM(Fianco, jj) = AlfaM(Fianco, NP1 + NPE) + dALFAm * (j - 1)
              X(Fianco, jj) = Xi + INP.RF * Cos(AlfaM(Fianco, jj))
              Y(Fianco, jj) = Yi - INP.RF * (1 - Sin(AlfaM(Fianco, jj)))
              Call PuntoIngDaMola(X(Fianco, jj), Y(Fianco, jj), AlfaM(Fianco, jj), IL, INP.BetaMola, PAS, L(Fianco, jj), D(Fianco, jj), A(Fianco, jj), XD(Fianco, jj), YD(Fianco, jj), API(Fianco, jj))
          Next j
          NTR = -1
        End If
        '***************************************************************************
        Ri = IL - Yi
        vi = Xi * 2
        hu1 = Ytr - Y(Fianco, 1)
        HU2 = hu1 + INP.RF * (1 - Sin(AlfaM(Fianco, NP1 + NPE)))
        AngMed = Atn((X(Fianco, 1) - Xtr) / hu1)
        Sh = hu1 / Cos(AngMed) / 2
        PM = Int(NPE / 2)
        B1 = Xtr - (Y(Fianco, NP1 + PM) - Ytr) * Tan(AngMed) - X(Fianco, NP1 + PM)
        B1 = B1 * Cos(AngMed)
        Rbom = (Sh ^ 2 + B1 ^ 2) / (2 * B1) 'raggio bombatura
        '***************************************************************************
    End If
    '***
    NPnt(Fianco) = NPnt(Fianco) + INP.NPR1 + NTR
  Next Fianco
  '**********************   GESTIONE PIATTINO   **********************
  '**********************  VERSIONE ORIGINALE  ***********************
  '*** sposta l'ultimo punto (Xi,Yi) del raggio di fondo in (0,Yi) ***
  '        jj = NP1 + NPE + INP.NPR1 + NTR + 1
  '        NPunti = jj
  '        AlfaM(Fianco, jj) = PI / 2
  '        X(Fianco, jj) = 0
  '        Y(Fianco, jj) = Yi
  '        Call PuntoIngDaMola(X(Fianco, jj), Y(Fianco, jj), AlfaM(Fianco, jj), IL, INP.BetaMola, Pas, L(Fianco, jj), D(fianco,jj), A(Fianco, jj), XD(Fianco, jj), YD(Fianco, jj), API(fianco,jj))
  '        HU2 = Yi - Y(Fianco, 1)
  '        MMEst = 0.5 * Sqr(INP.DEAP ^ 2 - Db ^ 2)
  '        L(Fianco, jj) = 0
  '*******************************************************************
  '
  '************************  NUOVA VERSIONE  *************************
  'se l'ultimo punto (Xi,Yi)_Fianco1 del raggio di fondo � "vicino" al
  'punto (Xi,Yi)_Fianco2 , spostiamo il punto, altrimenti si aggiunge un
  'tratto rettilineo sul piattino della mola.
  '*******************************************************************
  Dim EpsilonPiattino As Double
  EpsilonPiattino = 0.2
  If Abs(X(1, NPnt(1)) + X(2, NPnt(2))) < EpsilonPiattino Then
    'jj = Npnt(1)
    'X(1, Npnt(1)) = X(2, Npnt(2))
    'Y(1, Npnt(1)) = Y(2, Npnt(2))
    'AlfaM(1, Npnt(1)) = AlfaM(2, Npnt(2))
    'D(1, Npnt(1)) = D(2, Npnt(2))
    'A(1, Npnt(1)) = A(2, Npnt(2))
    'XD(1, Npnt(1)) = XD(2, Npnt(2))
    'YD(1, Npnt(1)) = YD(2, Npnt(2))
    'API(1, Npnt(1)) = API(2, Npnt(2))
    HU2 = Yi - Y(1, 1)
    MMEst = 0.5 * Sqr(INP.DEAP ^ 2 - db ^ 2)
    'L(1, Npnt(1) - 1) = 0
    NTR = -1
  Else
    jj = NPnt(1) + 1
    X(1, jj) = -(X(2, NPnt(2)) - X(1, NPnt(1))) / 2
    Fianco = 1
    Y(Fianco, jj) = Yi
    AlfaM(Fianco, jj) = PI / 2
    Call PuntoIngDaMola(X(Fianco, jj), Y(Fianco, jj), AlfaM(Fianco, jj), IL, INP.BetaMola, PAS, L(Fianco, jj), D(Fianco, jj), A(Fianco, jj), XD(Fianco, jj), YD(Fianco, jj), API(Fianco, jj))
    HU2 = Yi - Y(Fianco, 1)
    MMEst = 0.5 * Sqr(INP.DEAP ^ 2 - db ^ 2)
    L(Fianco, jj) = 0
    NTR = 1
  End If
  NPnt(1) = NPnt(1) + NTR
  '*******************************************************************
  'For i = NP1 + NPE + INP.NPR1 + NTR To 0 Step -1
  '  A(2, i) = -A(2, i)
  '  'API(fianco,i) = -API(fianco,i)
  '  BetaRAB(2, i) = -BetaRAB(2, i)
  '  X(2, i) = -X(2, i)
  '  AlfaM(2, i) = -AlfaM(2, i)
  'Next i
  'rispetto alla convenzione presa in precedenza inverto fianco1 con fianco2 (10-11-2005)
  For i = NPnt(1) To 0 Step -1
    A(1, i) = -A(1, i)
    API(Fianco, i) = -API(Fianco, i)
    X(1, i) = -X(1, i)
    AlfaM(1, i) = -AlfaM(1, i)
  Next i
  '***  VETTORE PUNTI  ***  PROFILO TRASVERSALE PEZZO  ***
  ReDim RaggioRAB(NPnt(1) + NPnt(2) + 1) 'il "+1" serve per includere il primitivo del secondo fianco
  ReDim AlfaRAB(NPnt(1) + NPnt(2) + 1)
  ReDim BetaRAB(NPnt(1) + NPnt(2) + 1)
  Dim iii As Integer
  Fianco = 1
  For i = 0 To NPnt(Fianco)
    iii = i
    RaggioRAB(iii) = D(Fianco, i) / 2
    AlfaRAB(iii) = A(Fianco, i)
    BetaRAB(iii) = API(Fianco, i)
  Next i
  Fianco = 2
  For i = NPnt(Fianco) To 0 Step -1
    iii = NPnt(1) + NPnt(2) + 1 - i
    RaggioRAB(iii) = D(Fianco, i) / 2
    AlfaRAB(iii) = A(Fianco, i)
    BetaRAB(iii) = API(Fianco, i)
  Next i
  '***  VETTORE PUNTI  ***  PROFILO MOLA  ***
  ReDim XMola(NPnt(1) + NPnt(2) + 1) 'il "+1" serve per includere il primitivo del secondo fianco
  ReDim Ymola(NPnt(1) + NPnt(2) + 1)
  ReDim AMola(NPnt(1) + NPnt(2) + 1)
  Fianco = 1
  For i = 0 To NPnt(Fianco)
    iii = i
    XMola(iii) = X(Fianco, i)
    Ymola(iii) = Y(Fianco, i)
    AMola(iii) = AlfaM(Fianco, i)
  Next i
  Fianco = 2
  For i = NPnt(Fianco) To 0 Step -1
      iii = NPnt(1) + NPnt(2) + 1 - i
      XMola(iii) = X(Fianco, i)
      Ymola(iii) = Y(Fianco, i)
      AMola(iii) = AlfaM(Fianco, i)
  Next i
  '--- diametri(interno e esterno) ottenuti ---
  Dim dMin(2) As Double
  Dim dMax As Double
  dMax = D(1, 1)
  For Fianco = 1 To 2
    dMin(Fianco) = D(Fianco, 1)
    For i = 0 To NPnt(Fianco)
      If D(Fianco, i) < dMin(Fianco) Then dMin(Fianco) = D(Fianco, i)
      If D(Fianco, i) > dMax Then dMax = D(Fianco, i)
    Next i
  Next Fianco
  If dMin(1) < dMin(2) Then DIntOttenuto = dMin(1)
  If dMin(2) < dMin(1) Then DIntOttenuto = dMin(2)
  DEstOttenuto = dMax
  '--------------------------------------------
  '------ CONVERSIONE DEI PUNTI DEL PROFILO DA SEZIONE TRASVERSALE A SEZIONE ASSIALE ------
  If NPnt(1) > NPnt(2) Then NpFIANCO = NPnt(1) Else NpFIANCO = NPnt(2)
  ReDim Xax(2, NpFIANCO)
  ReDim Yax(2, NpFIANCO)

  For Fianco = 1 To 2
    For i = 0 To NPnt(Fianco)
      'angolo di pressione assiale corrispondente a API(fianco,i)
      'APx = Atn(Tan(API(fianco,i) * PI / 180) * Pas / (PI * D(fianco,i)))
      'conversione in coordinate assiali punti profilo
      Yax(Fianco, i) = D(Fianco, i) / 2: Xax(Fianco, i) = A(Fianco, i) * PAS / 2 / PI
    Next i
  Next Fianco
  '----------------------------------------------------------------------------------------
  '
  Call RMaxRullo(1, INP.Beta, MM0, NP1 + NPE, X, Y, RRulloMax, XCRullo, YCRullo)
  '
Exit Sub

ErrCalcolaProfilo:
  StopRegieEvents
  MsgBox "ErrCalcolaProfilo: Please, check parameters.", vbCritical, "ERROR"
  ResumeRegieEvents
  
End Sub

'Calcola correzioni profilo  K
Private Sub CalcolaProfiloK(ByVal Fianco As Integer, ByVal Np As Integer, ByVal Rp As Double, ByVal Rb As Double, ByVal MM0 As Double, ByVal MM1 As Double, ByVal MM2 As Double, ByVal DIPU1 As Double, ByVal DIPU2 As Double, ByVal DIPU3 As Double, ByRef Tif() As Double, ByRef Rastremazione() As Double, ByRef Bombatura() As Double, ByRef CB() As Double)
'
Dim i             As Integer
Dim E17           As Integer
Dim LTratto1      As Double   'lunghezza in TIF del 1� tratto
Dim RBomb         As Double   'Raggio di Bombatura
Dim E264          As Double
Dim LTratto2      As Double   'lunghezza in TIF del 2� tratto
Dim LTratto3      As Double   'lunghezza in TIF del 3� tratto
Dim E19           As Integer
Dim NPE1          As Double   'numero punti su cui apportare le correzioni
Dim IDXP          As Double   'indice punto
Dim EntIniz       As Double   'Entit� Rastremazione all'inizio del tratto
Dim NPE2          As Double   'numero punti 2� tratto
Dim E269          As Double
Dim MMp           As Double   'TIF sul primitivo
Dim IDXprimitivo  As Integer  'indice pseudo-primitivo (punto vicino al primitivo teorico)
Dim Corrp         As Double   'correzione sul primitivo
'
    ' 25.10.2005
    '-------------- Tratto 1 --------------
    EntIniz = 0
    E19 = 1
    For i = 1 To Np
        CB(Fianco, i) = 0
    Next i
    ' Controllo TIF1
    If Tif(Fianco, 1) = 0 Then GoTo CALCK2
    If Tif(Fianco, 1) < MM0 Then GoTo CALCK2
    If Tif(Fianco, 1) > MM2 Then Tif(Fianco, 1) = MM2

        LTratto1 = Int((Tif(Fianco, 1) - MM1 + DIPU1 / 2) / DIPU1) * DIPU1
        NPE1 = LTratto1 / DIPU1 + 1

    If Bombatura(Fianco, 1) <> 0 Then
        RBomb = (LTratto1 ^ 2 / 4 + Bombatura(Fianco, 1) ^ 2) / 2 / Bombatura(Fianco, 1) ' RB ( - , + )
    End If
       
    IDXP = 0
    E17 = 0
    For IDXP = 1 To NPE1
        E17 = E17 + 1
        CB(Fianco, E17) = Rastremazione(Fianco, 1) * (IDXP - 1) / (NPE1 - 1)
        
        If Bombatura(Fianco, 1) <> 0 Then
          E264 = Abs(LTratto1 / 2 - (IDXP - 1) * DIPU1)
          CB(Fianco, E17) = CB(Fianco, E17) + Bombatura(Fianco, 1) + Sqr(RBomb ^ 2 - E264 ^ 2) * RBomb / Abs(RBomb) - RBomb
          
        End If
    Next IDXP
        
    '-------------- Tratto 2 --------------
    EntIniz = CB(Fianco, E17)
    E19 = NPE1
    ' Tratto 2:TIF2,RASTR2,BOMB2
    If Tif(Fianco, 2) = 0 Then GoTo AZCK
    If Tif(Fianco, 2) <= Tif(Fianco, 1) Then
        StopRegieEvents
        MsgBox "ERROR: TIF2 <= TIF1", vbExclamation, "SUB: CalcolaProfiloK"
        ResumeRegieEvents
        'pp'    DIS0.Caption = "ERROR: TIF2 <= TIF1"
        Exit Sub            ' ERRORE se TIF2 <= TIF1
    End If
    If Tif(Fianco, 2) > MM2 Then Tif(Fianco, 2) = MM2

    LTratto2 = Int((Tif(Fianco, 2) - MM1 - LTratto1 + DIPU2 / 2) / DIPU2) * DIPU2
    NPE2 = LTratto2 / DIPU2 + 1
    
    If NPE2 = 1 Then
        StopRegieEvents
        MsgBox "Parameter NPE2: WRONG VALUE", vbExclamation, "SUB: CalcolaProfiloK"
        ResumeRegieEvents
        Exit Sub
    End If
    If Bombatura(Fianco, 2) <> 0 Then RBomb = (LTratto2 ^ 2 / 4 + Bombatura(Fianco, 2) ^ 2) / 2 / Bombatura(Fianco, 2)
    IDXP = 1
    For IDXP = 2 To NPE2
        E17 = E17 + 1
        CB(Fianco, E17) = Rastremazione(Fianco, 2) * (IDXP - 1) / (NPE2 - 1) + EntIniz
               
        If Bombatura(Fianco, 2) <> 0 Then
          E264 = Abs(LTratto2 / 2 - (IDXP - 1) * DIPU2)
          CB(Fianco, E17) = CB(Fianco, E17) + Bombatura(Fianco, 2) + Sqr(RBomb ^ 2 - E264 ^ 2) * RBomb / Abs(RBomb) - RBomb
          
        End If
    Next IDXP

        '-------------- Tratto 3 --------------
        EntIniz = CB(Fianco, E17)
        E19 = E17 - 1
        If Tif(Fianco, 3) = 0 Then GoTo AZCK
        If Tif(Fianco, 3) <= Tif(Fianco, 2) Then
            StopRegieEvents
            MsgBox "ERROR: TIF3 <= TIF2", vbExclamation, "Error"
            ResumeRegieEvents
            'pp'    DIS0.Caption = "ERROR: TIF3 <= TIF2"
            Exit Sub
        End If
        If Tif(Fianco, 3) > MM2 Then Tif(Fianco, 3) = MM2

        LTratto3 = Int((Tif(Fianco, 3) - MM1 - LTratto1 - LTratto2 + DIPU3 / 2) / DIPU3) * DIPU3
        E269 = LTratto3 / DIPU3 + 1

        If Bombatura(Fianco, 3) <> 0 Then RBomb = (LTratto3 ^ 2 / 4 + Bombatura(Fianco, 3) ^ 2) / 2 / Bombatura(Fianco, 3)
        IDXP = 1
ALCK3:
        IDXP = IDXP + 1
        E17 = E17 + 1
        CB(Fianco, E17) = Rastremazione(Fianco, 3) * (IDXP - 1) / (E269 - 1) + EntIniz
        If Bombatura(Fianco, 3) = 0 Then GoTo NOB3F1
        E264 = Abs(LTratto3 / 2 - (IDXP - 1) * DIPU3)
        CB(Fianco, E17) = CB(Fianco, E17) + Bombatura(Fianco, 3) + Sqr(RBomb ^ 2 - E264 ^ 2) * RBomb / Abs(RBomb) - RBomb
NOB3F1:
        If IDXP < E269 Then GoTo ALCK3
        E19 = E17 - 1
        EntIniz = CB(Fianco, E17)
AZCK:
        E17 = 1 + E19
        E19 = Np - E19
        If E19 < 1 Then GoTo CALCK2
        For i = 1 To E19
            CB(Fianco, E17) = EntIniz
            E17 = E17 + 1
        Next i
CALCK2:
        MMp = Sqr(Rp * Rp - Rb * Rb)
'-------------- vecchia versione --------------
'        NPE1 = Int((MMp - MM1 + 0.5) / DIPU1)
'        If NPE1 < 0 Then NPE1 = 0
'        If NPE1 >= NP Then NPE1 = NP - 1
'        Corrp = CB(Fianco, NPE1)
'----------------------------------------------
        If MMp < Tif(Fianco, 1) Then IDXprimitivo = Int((MMp - MM1 + 0.5) / DIPU1)
        If (MMp > Tif(Fianco, 1) And MMp < Tif(Fianco, 2)) Then
            If DIPU2 > 0 Then IDXprimitivo = NPE1 + Int((MMp - Tif(Fianco, 1) + 0.5) / DIPU2)
        End If
        If (MMp > Tif(Fianco, 2) And MMp < Tif(Fianco, 3)) Then
            If DIPU3 > 0 Then IDXprimitivo = NPE2 + Int((MMp - Tif(Fianco, 2) + 0.5) / DIPU3)
        End If
        If IDXprimitivo < 0 Then IDXprimitivo = 0
        If IDXprimitivo >= Np Then IDXprimitivo = Np - 1
        Corrp = CB(Fianco, IDXprimitivo)
        For i = 1 To Np
            CB(Fianco, i) = CB(Fianco, i) - Corrp
        Next i
        
        CB(Fianco, 0) = CB(Fianco, 1) + (CB(Fianco, 1) - CB(Fianco, 2))
    
End Sub

'Calcola correzioni profilo K con 2 parabole
'Bomb1, Bomb2 :  non usati
'TIF2: gestito valore < EAP
'RASTR3 = fHa totale del fianco
Private Sub CalcProfK_parabola(ByVal Fianco As Integer, ByVal NPE As Integer, ByVal Rp As Double, ByVal Rb As Double, ByVal MM0 As Double, ByVal MM1 As Double, ByVal MM2 As Double, ByVal DIPU As Double, ByRef Tif() As Double, ByRef Rastremazione() As Double, ByRef CB() As Double)
                                
Dim i As Integer
Dim E17 As Integer
Dim E260 As Double
Dim E262 As Double
Dim E264 As Double
Dim E266 As Double
Dim E268 As Double
Dim E19 As Integer
Dim E261 As Double
Dim E263 As Double
Dim E265 As Double
Dim E267 As Double
Dim E269 As Double
Dim KP1 As Double
Dim KP2 As Double
Dim MMp As Double
                
        E265 = 0
        E19 = 1
        For i = 1 To NPE
            CB(Fianco, i) = 0
        Next i
        ' Controllo TIF1
        If Tif(Fianco, 1) = 0 Then GoTo CALCP2
        If Tif(Fianco, 1) < MM0 Then GoTo CALCP2
        If Tif(Fianco, 1) > MM2 Then GoTo CALCP2
        E260 = Int((Tif(Fianco, 1) - MM1 + DIPU / 2) / DIPU) * DIPU  'L1  = lunghezza Tratto1
        E261 = E260 / DIPU + 1                                       'NP1 = numero punti nel Tratto1
        'calcolo costante KP1
        KP1 = Rastremazione(Fianco, 1) / (E260 ^ 2)
'        DIB:  KP1 = -Rastremazione(Fianco, 1) / (E260 ^ 2)
        '
        E263 = 0
        E17 = 0
        For E263 = 1 To E261
         E17 = E17 + 1
         CB(Fianco, E17) = KP1 * (DIPU * (E261 - E263)) ^ 2
        Next E263
        E265 = CB(Fianco, E17)
        ' Tratto 2
        If Tif(Fianco, 2) = 0 Then GoTo CALCP2
        If Tif(Fianco, 2) <= Tif(Fianco, 1) Then GoTo CALCP2
        If Tif(Fianco, 2) > MM2 Then Tif(Fianco, 2) = MM2

        E266 = Int((Tif(Fianco, 2) - MM1 - E260 + DIPU / 2) / DIPU) * DIPU  'L2  = lunghezza Tratto2
'        DIB:  E267 = (MM2 - TIF(Fianco, 1)) / DIPU + 1
        E267 = NPE - E261  ' PUNTI RESTANTI FINO A EAP
        E17 = E261
        'calcolo costante KP2
        KP2 = Rastremazione(Fianco, 2) / (E266 ^ 2)
        '
        For E263 = 1 To E267
         E17 = E17 + 1
         CB(Fianco, E17) = KP2 * (DIPU * E263) ^ 2
        Next E263
        E265 = CB(Fianco, E17)
CALCP2:
        For i = E17 To NPE
         E17 = E17 + 1
         CB(Fianco, E17) = E265
        Next i
        'gestione fHa
        For i = 1 To NPE
         CB(Fianco, i) = CB(Fianco, i) + Rastremazione(Fianco, 3) * (i - 1) / (NPE - 1)
        Next i
        'normalizzazione spessore su primitivo
        MMp = Sqr(Rp * Rp - Rb * Rb)
        E261 = Int((MMp - MM1 + DIPU / 2) / DIPU) + 1
'        DIB: E261 = Int((MMp - MM1 + 0.5) / DIPU) + 1
        If E261 < 0 Then E261 = 1
        If E261 >= NPE Then E261 = NPE - 1  ' = NPE
'        DIB:  If E261 < 0 Then E261 = 0
        E262 = CB(Fianco, E261)
        E17 = 1
        For i = 1 To NPE
            CB(Fianco, E17) = CB(Fianco, E17) - E262
            E17 = E17 + 1
        Next i
        CB(Fianco, 0) = CB(Fianco, 1) + (CB(Fianco, 1) - CB(Fianco, 2))
End Sub

Private Sub Calcolo_T0FI0(ByVal dx As Double, ByRef Corr As Double, ByVal DbCorr As Double, ByVal db As Double, ByVal Dpr As Double, ByVal Z1 As Double, ByVal ALFA As Double, ByVal Beta As Double, ByVal SpT As Double, ByRef T0 As Double, ByRef FI0 As Double, ByRef angT As Double)

'input : Dx, Corr, DbCorr, Db, SpT, Rpr, Z1, alfa, beta
'output: T0, FI0, AngT

Dim CosA  As Double
Dim SinA  As Double
Dim TanA  As Double
Dim Ax    As Double
Dim InvAx As Double
Dim SxT   As Double  'spessore trasversale sul diam. x
Dim VxT   As Double  'vano trasversale sul diam. x
Dim PAS   As Double  'passo assiale
Dim ALFAt As Double
Dim X0    As Double
Dim Y0    As Double

  '        ********  CALCOLO T0,FI0  *********
  CosA = db / dx: SinA = Sqr(1 - CosA ^ 2): TanA = SinA / CosA
  Ax = Atn(TanA)
  InvAx = TanA - Ax
  ALFAt = Atn(Tan(ALFA) / Cos(Beta))
  SxT = (SpT / Dpr + inv(ALFAt) - InvAx) * dx
  VxT = PI * dx / Z1 - SxT
  If Abs(Beta) < PI / 4 Then
    T0 = PI / 2 - VxT / dx + Atn(Corr / (dx / 2)) ' oppure /Rbase ?
  Else
    'Calcolo del passo assiale
    PAS = Tan(PI / 2 - Beta) * PI * Dpr
    T0 = PI / 2 - VxT / dx + (2 * PI * Corr) / (Sin(Beta) * PAS * Cos(ALFA))
  End If
  FI0 = T0 - Arccos(DbCorr / dx): If T0 < 0 Then FI0 = FI0 - 2 * PI
  While FI0 > PI: FI0 = FI0 - PI * 2: Wend
  While FI0 < -PI: FI0 = FI0 + PI * 2: Wend
  angT = PI / 2 - T0

End Sub

' input: RX, BETA, betam, T0, FI0, IL
'output: Xing(i),Ying(i),TIng(i)
'      : Xmola(i),Ymola(i),APmola(i)
Private Sub Purea(ByVal RX As Double, ByVal Beta As Double, ByVal Rp As Double, ByVal BetaMr As Double, ByVal T0 As Double, ByVal FI0 As Double, ByVal IL As Double, ByRef Xing As Double, ByRef Ying As Double, ByRef Ti As Double, ByRef XMola As Double, ByRef Ymola As Double, ByRef ApMolaG As Double)
'
Dim j       As Integer
Dim PAS     As Double
Dim SIGMA   As Double
Dim Rmin    As Double
Dim Tp      As Double
Dim DRM     As Double
Dim RMPD    As Double
Dim RQC     As Double
Dim AA      As Double
Dim BB      As Double
Dim cc      As Double
Dim RAD     As Double
Dim T1      As Double
Dim T2      As Double
Dim TSI     As Double
Dim fp1     As Double
Dim fp2     As Double
Dim XS      As Double
Dim Ys      As Double
Dim Zs      As Double
Dim Ap      As Double
Dim Bp      As Double
Dim Cp      As Double
Dim Lp      As Double
Dim ArgSeno As Double
Dim SINa2   As Double
Dim Uprof   As Double
Dim Wprof   As Double
Dim Vprof   As Double
Dim Rprof   As Double
Dim TanA    As Double
  '
  PAS = Tan(PI / 2 - Beta) * 2 * PI * Rp
  SIGMA = (90 - BetaMr * 180 / PI) * PI / 180
  Rmin = PAS / ((PI * 2) * Tan(SIGMA))
  Tp = PAS / (PI * 2)
  DRM = IL * Rmin
  RMPD = Rmin + IL
  Ying = RX * Sin(T0)
  Xing = RX * Cos(T0)
  RQC = RX * RX * Cos(T0 - FI0)
  AA = RQC * Sin(T0) + (2 * Tp * Tp + DRM) * Sin(FI0)
  BB = (DRM + Tp * Tp) * Cos(FI0) + RQC * Cos(T0)
  cc = 2 * (-DRM * Sin(FI0) - RQC * Sin(T0) + RX * RMPD * Cos(T0 - FI0))
  RAD = Sqr((BB / AA) * (BB / AA) - (cc / AA))
  T1 = BB / AA + RAD
  T2 = BB / AA - RAD
  Ti = T2
  If Abs(T2) > Abs(T1) Then Ti = T1
  If T0 < 0 Then Ti = T1
  While Ti >= PI: Ti = PI * 0.9: Wend  'Ti = Ti * 0.9: Wend ??????
  TSI = Ti
  For j = 1 To 10
      fp1 = RQC * Sin(T0 + TSI) + DRM * Sin(FI0 + TSI) + Tp * Tp * TSI * Cos(FI0 + TSI) - RX * RMPD * Cos(T0 - FI0)
      fp2 = RQC * Cos(T0 + TSI) + DRM * Cos(FI0 + TSI) - Tp * Tp * TSI * Sin(FI0 + TSI) + Tp * Tp * Cos(FI0 + TSI)
      TSI = TSI - fp1 / fp2
  Next j
  Ti = TSI
  XS = RX * Cos(T0 + Ti)
  Ys = RX * Sin(T0 + Ti)
  Zs = Tp * Ti
  'CALCOLO TANGENTE AL PROFILO MOLA IN TETAS
  Ap = Tan(Ti + FI0) / XS
  Bp = -1 / XS
  Cp = (1 + Ys * Tan(Ti + FI0) / XS) / Tp
  Lp = Tan(SIGMA)
  ArgSeno = (Ap * Lp + Cp) / (Sqr(Ap * Ap + Bp * Bp + Cp * Cp) * Sqr(Lp * Lp + 1))
  SINa2 = ArgSeno * ArgSeno
  TanA = Sqr(SINa2 / (1 - SINa2))
  ApMolaG = 90 - (Atn(TanA) * (180 / PI))
  '---------- aggiunto il 16.11.2005 -----------
  If ApMolaG > 90 Then ApMolaG = ApMolaG - 180
  If ApMolaG < -90 Then ApMolaG = ApMolaG + 180
  '---------------------------------------------
  Uprof = XS * Cos(SIGMA) - Zs * Sin(SIGMA)
  Vprof = Ys - IL
  Wprof = Zs * Cos(SIGMA) + XS * Sin(SIGMA)
  Rprof = Sqr(Uprof ^ 2 + Vprof ^ 2)
  XMola = Wprof
  Ymola = Rprof
  '
End Sub

'MOLA --> INGRANAGGIO (sez.trasversale)
'INPUT : X(JJ), Y(JJ), ALFAM(JJ)
'OUTPUT: XD(JJ), YD(JJ), R(JJ), A(JJ), API(JJ)
Private Sub PuntoIngDaMola(ByVal XMola As Double, ByVal Ymola As Double, ByVal Apmola As Double, ByVal IL As Double, ByVal BetaMr As Double, ByVal PAS As Double, ByRef L As Double, ByRef D As Double, ByRef A As Double, ByRef Xing As Double, ByRef Ying As Double, ByRef ApIng As Double)
'
Dim ENTRX As Double
Dim INF   As Double
Dim EXF   As Double
Dim RXF   As Double
Dim APFD  As Double
Dim APF   As Double
Dim Rayon As Double
Dim ALFAs As Double
Dim BETAi As Double
Dim V17   As Double
'
On Error Resume Next
  '
  ENTRX = IL: INF = BetaMr: EXF = XMola: RXF = Ymola: APFD = Apmola * 180 / PI
  If Abs(APFD) > 89.9 Then APFD = APFD / Abs(APFD) * 89.9
  If EXF < 0 Then APFD = -APFD
  If APFD < 0 Then APF = ((180 + APFD) * (PI / 180)) Else APF = ((APFD) * (PI / 180))
  Call Fun19200(EXF, APF, PAS, ENTRX, INF, RXF, V17, Rayon, ALFAs, BETAi)
  L = V17
  D = Rayon * 2
  A = ALFAs
  ApIng = BETAi * 180 / PI
  '---------- aggiunto il 16.11.2005 -----------
  If ApIng > 90 Then ApIng = ApIng - 180
  If ApIng < -90 Then ApIng = ApIng + 180
  '---------------------------------------------
  Xing = Rayon * Sin(ALFAs)
  Ying = Rayon * Cos(ALFAs)
  '
End Sub

'************** S/PAS ************ CALCUL PIECE D'APRES FRAISE
Private Sub Fun19200(ByVal EXF As Double, ByVal APF As Double, ByVal PAS As Double, ByVal ENTRX As Double, ByVal INF As Double, ByVal RXF As Double, ByRef V17 As Double, ByRef Rayon As Double, ByRef ALFAs As Double, ByRef BETAi As Double)
'
Dim SENS  As Double
Dim V7    As Double
Dim RXV   As Double
Dim Ang   As Double
Dim X1    As Double
Dim Y1    As Double
Dim XAPP  As Double
Dim YAPP  As Double
Dim AngS  As Double
'
On Error Resume Next
  '
  SENS = 1: If EXF < 0 Then SENS = -1
  EXF = EXF * SENS: APF = APF * SENS: V7 = PAS / 2 / PI
  Call FUN20500(APF, ENTRX, INF, V7, EXF, RXF, PAS, RXV, V17, Ang, XAPP, YAPP)
  Rayon = RXV: ALFAs = Ang * SENS: AngS = Ang
  RXF = RXF + 0.01: EXF = EXF - 0.01 * Tan(APF)
  Call FUN20500(APF, ENTRX, INF, V7, EXF, RXF, PAS, RXV, V17, Ang, XAPP, YAPP)
  Y1 = YAPP: X1 = XAPP
  RXF = RXF - 0.02: EXF = EXF + 0.02 * Tan(APF)
  Call FUN20500(APF, ENTRX, INF, V7, EXF, RXF, PAS, RXV, V17, Ang, XAPP, YAPP)
  RXF = RXF + 0.01: EXF = EXF - 0.01 * Tan(APF)
  BETAi = (Atn((X1 - XAPP) / (YAPP - Y1)) - AngS) * SENS
  APF = APF * SENS: EXF = EXF * SENS
  '
End Sub

Private Sub FUN20500(ByVal APF As Double, ByVal ENTRX As Double, ByVal INF As Double, ByVal V7 As Double, ByVal EXF As Double, ByVal RXF As Double, ByVal PAS As Double, ByRef RXV As Double, ByRef V17 As Double, ByRef Ang As Double, ByRef XAPP As Double, ByRef YAPP As Double)
'
Dim V12 As Double
Dim V13 As Double
Dim V14 As Double
Dim V15 As Double
Dim VPB As Double
Dim V16 As Double
Dim EXV As Double
'
On Error Resume Next
  '
  V12 = -Tan(APF) * (ENTRX * Sin(INF) + V7 * Cos(INF))
  V13 = V7 * Sin(INF) - ENTRX * Cos(INF)
  V14 = Cos(INF) * ((-Tan(APF) * EXF) + RXF)
  V15 = (V14 ^ 2) + (V12 ^ 2) - (V13 ^ 2)
  If (V15 > 0) Then
    V15 = Sqr(V15)
    V16 = -(V15 + V12) / (V13 - V14)
    V15 = (V15 - V12) / (V13 - V14)
    VPB = V16: If Abs(V15) < Abs(V16) Then VPB = V15
    V12 = 2 * Atn(VPB)
    V13 = (RXF * Sin(INF) * Sin(V12)) + EXF * Cos(INF)
    V13 = Atn(V13 / ((RXF * Cos(V12)) - ENTRX))
    'DISTANZA ASSE MOLA PIANO TANGENTE --------------
    V17 = EXF * Sin(INF) - RXF * Sin(V12) * Cos(INF)
    '------------------------------------------------
    RXV = (ENTRX - RXF * Cos(V12)) / Cos(V13)
    EXV = EXF * Sin(INF) - V7 * V13 - RXF * Sin(V12) * Cos(INF)
    Ang = 2 * PI * EXV / PAS
    YAPP = RXV * Cos(Ang): XAPP = -RXV * Sin(Ang)
  Else
    StopRegieEvents
    MsgBox "EVALUATION NOT POSSIBLE!!!", vbCritical, "SUB: FUN20500"
    ResumeRegieEvents
    Exit Sub
  End If
    
End Sub

'INPUT: ...
'OUTPUT: RSC, SAPOTT, INTERF$,
'        D(i), A(Fianco, i), API(i),
'        X(Fianco, i), Y(Fianco, i), ALFAM(Fianco, i)

'AlfaSAP = Alfa (di RAB) DEL SAP
'RSC = max raggio CR
Private Sub TROCOIDE(ByVal DSAP As Double, ByVal AlfaSAP As Double, ByVal BSap As Double, ByVal RF As Double, ByVal NPR1 As Integer, ByVal idxSAP As Integer, ByVal Z1 As Double, ByVal Mn As Double, ByVal ALFA As Double, ByVal Beta As Double, ByVal DInt As Double, ByVal DB1 As Double, ByVal Dpr As Double, ByVal BetaMola As Double, ByVal IL As Double, ByRef RSC As Double, ByRef SAPOTT As Double, ByRef INTERF$, ByRef D() As Double, ByRef A() As Double, ByRef API() As Double, ByRef X() As Double, ByRef Y() As Double, AlfaM() As Double)

Dim AlfaCR  As Double:  AlfaCR = ALFA

Dim SONsapT As Double
Dim APsap   As Double
Dim Eb1     As Double
Dim ALFAt   As Double
Dim SONn    As Double
Dim SONt    As Double

Dim ModReel As Double
Dim AprEngD As Double
Dim AprEng  As Double
Dim HelpD   As Double
Dim Help    As Double
Dim Epr1    As Double
Dim Di1     As Double
Dim Ri1     As Double:  Ri1 = Di1 / 2
Dim AprfmD  As Double
Dim Aprfm   As Double
Dim RS      As Double
Dim MA      As Double   'MODULO ASSIALE
Dim DpEng   As Double   'DIAMETRO PRIMITIVO INGRANAGGIO
Dim Apa     As Double
Dim Rb      As Double:  Rb = DB1 / 2
Dim HELB    As Double
Dim Eb1Fin  As Double
Dim DR      As Double
Dim Hr      As Double
Dim Ep0     As Double   'spess prim CR
Dim S0      As Double   'addendum
Dim ES      As Double   'spessore di testa
Dim XR      As Double
Dim YR      As Double
'
Dim RXMINI  As Double
Dim RSI     As Double
Dim SAC     As Double
Dim aTIF    As Double  'TIF OTTENUTO
Dim PAS     As Double
  '
  'risalire al SON
  SONsapT = DSAP * (PI / Z1 - AlfaSAP)
  APsap = Arccos(DB1 / DSAP)    'Ang Press al Sap (Trasv)
  Eb1 = (SONsapT / DSAP + inv(APsap)) * DB1   'spessore di base
  ALFAt = Atn(Tan(ALFA) / Cos(Beta))
  SONt = (Eb1 / DB1 - inv(ALFAt)) * Dpr
  SONn = SONt * Cos(Beta)
  '
  'Profilo trochoide
  ModReel = Mn
  AprEngD = ALFA * 180 / PI
  HelpD = Beta * 180 / PI
  Epr1 = SONn
  Di1 = DInt
  AprfmD = AlfaCR * 180 / PI
  RS = RF
  '
  Help = Beta
  AprEng = ALFA
  Aprfm = AlfaCR
  '
  'calcoli iniziali:
    MA = ModReel / Cos(Help)
    DpEng = MA * Z1
    Apa = Atn(Tan(AprEng) / Cos(Help))
    'Db1 = DpEng * COS(APA)
    HELB = Atn(Tan(Help) * Cos(Apa))
    Eb1Fin = (Epr1 / Cos(Help) / DpEng + inv(Apa)) * DB1
    Ri1 = Di1 / 2
    DR = (DB1 * Cos(HELB)) / Sqr(Cos(Aprfm + HELB) * Cos(Aprfm - HELB))
    Hr = Atn(Tan(HELB) * DR / DB1)                                 'elica rotolam.
    Apa = Arccos(DB1 / DR)
    Ep0 = ((((PI * DB1 / Z1) - Eb1Fin) / DB1) + inv(Apa)) * DR * Cos(Hr)      'spess prim CR
    S0 = (DR - Di1) / 2                                                         'addendum
    ES = Ep0 / 2 - S0 * Tan(Aprfm)                                              'spessore di testa
    RSC = ES / Tan((PI / 2 - Aprfm) / 2)                'max raggio CR
    'CONTROLLO INTERFERENZA
    YR = S0 - RS: XR = ES - (RS * Tan(((PI / 2) - Aprfm) / 2))
    SAC = (RS * Sin(Aprfm)) + YR
    aTIF = (Rb * Tan(Apa)) - (SAC / Sin(Apa))   'TIF OTTENUTO
    If (aTIF > 0) Then
      RXMINI = Sqr(Rb ^ 2 + aTIF ^ 2)
      INTERF$ = "NO"
    Else
      RXMINI = Rb
      INTERF$ = "SI"
    End If
    SAPOTT = RXMINI * 2

    If (SAPOTT > DSAP) And (INTERF$ = "NO") Then
      'RICERCA Raggio Max
      RSI = RS
      While (RSI > 0) And (SAPOTT > DSAP)
        RSI = RSI - 0.01
        YR = S0 - RSI: XR = ES - (RSI * Tan(((PI / 2) - Aprfm) / 2))
        SAC = (RSI * Sin(Aprfm)) + YR
        aTIF = (Rb * Tan(Apa)) - (SAC / Sin(Apa))
        SAPOTT = Sqr(Rb * Rb + aTIF * aTIF) * 2
      Wend
      RS = RSI
    End If
    '
    '--------------- CALCUL DIAMETRE ACTIF MINI THEOR.
    Dim RX        As Double
    Dim BETApnt   As Double
    Dim VALFA     As Double
    Dim XPD       As Double
    Dim RAP       As Double
    Dim APMAX     As Double
    Dim ALFAINT   As Double
    Dim ATR       As Double
    Dim ATR1      As Double
    Dim DV1       As Double
    Dim Fattore   As Double
    Dim NPTR1     As Integer
    Dim STEPALFA  As Double
    
    Dim T()   As Double
    Dim MM()  As Double
    Dim TET() As Double
    Dim L()   As Double
    Dim T0    As Double
    Dim FI0   As Double
    Dim XD()  As Double
    Dim YD()  As Double
    Dim Ti    As Double
    Dim B()   As Double
    
    Dim Aux As Double
    Dim XCT As Double
    Dim YCT As Double
    Dim Xpt As Double
    Dim Ypt As Double
    
    ReDim R(idxSAP + NPR1), D(idxSAP + NPR1), A(idxSAP + NPR1), API(idxSAP + NPR1)
    ReDim T(idxSAP + NPR1), MM(idxSAP + NPR1)
    ReDim TET(idxSAP + NPR1), L(idxSAP + NPR1)
    
    ReDim XD(idxSAP + NPR1), YD(idxSAP + NPR1)
    ReDim X(idxSAP + NPR1), Y(idxSAP + NPR1)
    ReDim B(idxSAP + NPR1), AlfaM(idxSAP + NPR1)
                           
    ALFA = 0: VALFA = 0.9
RIPETI:
    ALFA = ALFA + VALFA
    Call PntTrocFM(Hr, DR, Z1, RS, YR, XR, ALFA, RX, BETApnt, XCT, YCT, Xpt, Ypt)
    Aux = Eb1Fin / DB1
    If Rb < RX Then Aux = Aux - inv(Arccos(Rb / RX))
    XPD = RX * Sin(Aux)
    If RX < RXMINI Then GoTo RIPETI
    If Xpt < XPD Then GoTo RIPETI
    ALFA = ALFA - VALFA: VALFA = VALFA / 10
    If VALFA > 0.000019 Then GoTo RIPETI
    RAP = RX: APMAX = ToDeg(PI / 2 - ALFA): ALFAINT = ALFA
    ' --------------------------------------------------------------
    If INTERF$ = "NO" Then
       ATR = PI / 2 - Aprfm: DV1 = RXMINI * 2
    Else
       ATR = ALFAINT: DV1 = RAP * 2
    End If
    ALFA = 0
    Call PntTrocFM(Hr, DR, Z1, RS, YR, XR, ALFA, RX, BETApnt, XCT, YCT, Xpt, Ypt)
     ' -------------------------------------------trochoide
     ALFA = ATR: Call PntTrocFM(Hr, DR, Z1, RS, YR, XR, ALFA, RX, BETApnt, XCT, YCT, Xpt, Ypt)
     'SPEZZO IN DUE LA TROCOIDE
      'PERCENTUALE ZONA VERSO SAP
     If BSap < 10 Then
            Fattore = 0.7
      Else
            Fattore = 0.6
     End If
    'PRIMO RAGGIO: RFT1
         ATR1 = ATR * Fattore
         NPTR1 = Int(NPR1 / 2) + 1
         STEPALFA = (ATR - ATR1) / (NPTR1)
         Dim NUMR As Integer
         Dim i    As Integer
         For NUMR = 1 To NPTR1
          ALFA = ATR - NUMR * STEPALFA + STEPALFA * 0.5
          Call PntTrocFM(Hr, DR, Z1, RS, YR, XR, ALFA, RX, BETApnt, XCT, YCT, Xpt, Ypt)
          i = idxSAP + NUMR
          R(i) = Sqr(XCT ^ 2 + YCT ^ 2)
          A(i) = (Atn(XCT / YCT))
          T(i) = ToDeg(BETApnt)
          ' MOLA
          MM(i) = 0
          PAS = Tan(PI / 2 - Beta) * PI * Dpr
          T0 = PI / 2 - A(i): FI0 = T0 - ToRad(T(i))
          Call Purea(RX, Beta, Dpr / 2, BetaMola, T0, FI0, IL, XD(i), YD(i), Ti, X(i), Y(i), B(i))
          AlfaM(i) = ToRad(B(i)): TET(i) = ToDeg(Ti)
          'NELLA VERSIONE DIB L'ISTRUZIONE PRECEDENTE ERA:  TET(i) = ToDeg(TH)
          'MA IN PUREA IMPONEVA TH = Ti, QUINDI DOVREBBE ANDARE BENE ANCHE COS�
          L(i) = TET(i) / 360 * PAS
          D(i) = 2 * R(i)
          API(i) = T(i)
          Dim stmp  As String
          Dim stmp1 As String
          Dim stmp2 As String
          Dim sfmt0 As String: sfmt0 = "######0.0000"
          stmp = stmp + vbCrLf
          stmp1 = Format$(i, "####")
          stmp2 = String(4 - Len(stmp1), " ")
          stmp = stmp + stmp1 + stmp2 + (" | ")
          stmp1 = Format$(D(i), sfmt0)
          stmp2 = String(12 - Len(stmp1), " ")
          stmp = stmp + stmp1 + stmp2 + (" | ")
          stmp1 = Format$(MM(i), sfmt0)
          stmp2 = String(12 - Len(stmp1), " ")
          stmp = stmp + stmp1 + stmp2 + (" | ")
          stmp1 = Format$(API(i), sfmt0)
          stmp2 = String(12 - Len(stmp1), " ")
          stmp = stmp + stmp1 + stmp2 + (" | ")
          stmp1 = Format$(X(i), sfmt0)
          stmp2 = String(12 - Len(stmp1), " ")
          stmp = stmp + stmp1 + stmp2 + (" | ")
          stmp1 = Format$(Y(i), sfmt0)
          stmp2 = String(12 - Len(stmp1), " ")
          stmp = stmp + stmp1 + stmp2 + (" | ")
          stmp1 = Format$(B(i), sfmt0)
          stmp2 = String(12 - Len(stmp1), " ")
          stmp = stmp + stmp1 + stmp2 + (" | ")
          stmp1 = Format$(L(i), sfmt0)
          stmp2 = String(12 - Len(stmp1), " ")
          stmp = stmp + stmp1 + stmp2 + (" | ")
        Next NUMR

        Dim X1 As Double
        Dim Y1 As Double
        Dim X2 As Double
        Dim Y2 As Double
        Dim X3 As Double
        Dim Y3 As Double
        Dim XC As Double
        Dim YC As Double
        Dim R0 As Double
 
 'rft1: ingranaggio
        Dim RFT1 As Double
        i = idxSAP + 1
          X1 = R(i) * Sin(A(i))
          Y1 = R(i) * Cos(A(i))
        i = Int(idxSAP + 1 + NPTR1 / 2)
          X2 = R(i) * Sin(A(i))
          Y2 = R(i) * Cos(A(i))
        i = (idxSAP + NPTR1)
          X3 = R(i) * Sin(A(i))
          Y3 = R(i) * Cos(A(i))
        Call RAG3P(X1, Y1, X2, Y2, X3, Y3, XC, YC, R0)
        RFT1 = R0

 'rfm1: mola
        Dim RFM1 As Double
        i = idxSAP + 1
          X1 = X(i): Y1 = Y(i)
        i = Int(idxSAP + 1 + NPTR1 / 2)
          X2 = X(i): Y2 = Y(i)
        i = (idxSAP + NPTR1)
          X3 = X(i): Y3 = Y(i)
        Call RAG3P(X1, Y1, X2, Y2, X3, Y3, XC, YC, R0)
        RFM1 = R0

  stmp = stmp + vbCrLf  'DA CANCELLARE
 
 'SECONDO RAGGIO: RFT2

        STEPALFA = (ATR1 - STEPALFA * 0.2) / (NPR1 - NPTR1)

        For NUMR = NPTR1 + 1 To NPR1
            If NUMR = NPTR1 Then
             ALFA = 0
            Else
              ALFA = (NPR1 - NUMR) * STEPALFA + STEPALFA * 0.25
            End If
            Call PntTrocFM(Hr, DR, Z1, RS, YR, XR, ALFA, RX, BETApnt, XCT, YCT, Xpt, Ypt)
            i = idxSAP + NUMR
            R(i) = Sqr(XCT ^ 2 + YCT ^ 2)
            A(i) = (Atn(XCT / YCT))
            T(i) = ToDeg(BETApnt)
            'MOLA
            MM(i) = 0
            T0 = PI / 2 - A(i): FI0 = T0 - ToRad(T(i))
            Call Purea(RX, Beta, Dpr / 2, BetaMola, T0, FI0, IL, XD(i), YD(i), Ti, X(i), Y(i), B(i))
            AlfaM(i) = ToRad(B(i)): TET(i) = ToDeg(Ti)  'DIB: TET(i) = ToDeg(TH)
            L(i) = TET(i) / 360 * PAS
            D(i) = 2 * R(i)
            API(i) = T(i)
            stmp = stmp + vbCrLf
            stmp1 = Format$(i, "####")
            stmp2 = String(4 - Len(stmp1), " ")
            stmp = stmp + stmp1 + stmp2 + (" | ")
            stmp1 = Format$(D(i), sfmt0)
            stmp2 = String(12 - Len(stmp1), " ")
            stmp = stmp + stmp1 + stmp2 + (" | ")
            stmp1 = Format$(MM(i), sfmt0)
            stmp2 = String(12 - Len(stmp1), " ")
            stmp = stmp + stmp1 + stmp2 + (" | ")
            stmp1 = Format$(API(i), sfmt0)
            stmp2 = String(12 - Len(stmp1), " ")
            stmp = stmp + stmp1 + stmp2 + (" | ")
            stmp1 = Format$(X(i), sfmt0)
            stmp2 = String(12 - Len(stmp1), " ")
            stmp = stmp + stmp1 + stmp2 + (" | ")
            stmp1 = Format$(Y(i), sfmt0)
            stmp2 = String(12 - Len(stmp1), " ")
            stmp = stmp + stmp1 + stmp2 + (" | ")
            stmp1 = Format$(B(i), sfmt0)
            stmp2 = String(12 - Len(stmp1), " ")
            stmp = stmp + stmp1 + stmp2 + (" | ")
            stmp1 = Format$(L(i), sfmt0)
            stmp2 = String(12 - Len(stmp1), " ")
            stmp = stmp + stmp1 + stmp2 + (" | ")
        Next NUMR

 'rft2: ingranaggio
        Dim RFT2 As Double
        i = idxSAP + NPTR1
         X1 = R(i) * Sin(A(i))
         Y1 = R(i) * Cos(A(i))
        i = Int(idxSAP + NPTR1 + NPTR1 / 2)
         X2 = R(i) * Sin(A(i))
         Y2 = R(i) * Cos(A(i))
        i = (idxSAP + NPR1)
         X3 = R(i) * Sin(A(i))
         Y3 = R(i) * Cos(A(i))

        Call RAG3P(X1, Y1, X2, Y2, X3, Y3, XC, YC, R0)
        RFT2 = R0
 
 'rfm2: mola
        Dim RFM2 As Double
        i = idxSAP + NPTR1
         X1 = X(i): Y1 = Y(i)
        i = Int(idxSAP + NPTR1 + NPTR1 / 2)
         X2 = X(i): Y2 = Y(i)
        i = (idxSAP + NPR1)
         X3 = X(i): Y3 = Y(i)

        Call RAG3P(X1, Y1, X2, Y2, X3, Y3, XC, YC, R0)
        RFM2 = R0

End Sub

'  ***************** S/P **** CALCUL UN POINT DE LA TROCHOIDE
Public Sub PntTrocFM(Hel, DR, Z, R0, Yr0, Xr0, ALFA, RX, ApAppx, XCT, YCT, Xpt, Ypt)

Dim Ypx     As Double
Dim Xpx     As Double
Dim AlfaPx  As Double
Dim B       As Double
Dim C       As Double
Dim D       As Double
   
   Ypx = Yr0 + R0 * Cos(ALFA)
   Xpx = (Xr0 + R0 * Sin(ALFA)) / Cos(Hel)
   AlfaPx = Atn(Tan(ALFA) * Cos(Hel))
   C = Atn((Ypx * Tan(AlfaPx)) / (DR / 2 - Ypx))
   B = (Ypx * Tan(AlfaPx) - Xpx) / (DR / 2)
   D = C - B
   RX = (DR / 2 - Ypx) / Cos(C)
   XCT = RX * Sin(D): Xpt = RX * Sin(PI / Z - D)
   YCT = RX * Cos(D): Ypt = RX * Cos(PI / Z - D)
   ApAppx = PI / 2 - AlfaPx - C               ' 19.09.02

End Sub

Private Sub RAG3P(ByVal X1 As Double, ByVal Y1 As Double, _
          ByVal X2 As Double, ByVal Y2 As Double, _
          ByVal X3 As Double, ByVal Y3 As Double, _
          ByRef XC As Double, ByRef YC As Double, ByRef R0 As Double)
'  Calcolo raggio per tre punti

''*** JPG ***
'Dim Scarto As Double
'Call Rayon5pts(X1, 0, X2, 0, X3, Y1, 0, Y2, 0, Y3, R0, XC, YC, Scarto)

''*** MIA ***
'If ((Y3 - Y1) * (X2 - X1) - (X3 - X1) * (Y2 - Y1)) < 0.000001 Then
'    'I tre punti appartengono ad una retta (i.e. circonferenza di raggio infinito)
'Else
'    If X2 = X3 Or Y1 = Y2 Then
'        CalcolaCerchio3P X3, Y3, X2, Y2, X1, Y1, XC, YC, R0
'    Else
'        CalcolaCerchio3P X1, Y1, X2, Y2, X3, Y3, XC, YC, R0
'    End If
'End If

'*** DIB ***
Dim R160 As Double
Dim R171 As Double
Dim R172 As Double
Dim R173 As Double

  If Y3 = Y2 Then Y3 = Y2 + 0.0001
  If Y1 = Y2 Then Y1 = Y2 + 0.0001
  R160 = (X1 - X2) / (Y2 - Y1)
  R171 = (X2 - X3) / (Y3 - Y2)
  R172 = (Y1 + Y2) / 2 - R160 * (X1 + X2) / 2
  R173 = (Y3 + Y2) / 2 - R171 * (X3 + X2) / 2
  If R160 = R171 Then R160 = R171 + 0.0001
  'coordinate del centro
  XC = (R173 - R172) / (R160 - R171)
  YC = R160 * XC + R172
  'raggio del cerchio
  R0 = Sqr((XC - X2) * (XC - X2) + (YC - Y2) * (YC - Y2))
  
End Sub

Private Sub Rayon5pts(L1, L2, L3, L4, L5, H1, H2, H3, H4, H5, R0, Xr0, Yr0, Ecart)
'
Dim L6    As Double
Dim H6    As Double
Dim L7    As Double
Dim H7    As Double
Dim L8    As Double
Dim L9    As Double
Dim A1    As Double
Dim A2    As Double
Dim A3    As Double
Dim A4    As Double
Dim A5    As Double
Dim EC2   As Double
Dim EC4   As Double
Dim Aux   As Double
Dim Aux1  As Double
  '
  If (H1 > H5) Then
     L6 = L3 - L1: H6 = H1 - H3: L7 = L5 - L3: H7 = H3 - H5
  Else
     L6 = L3 - L5: H6 = H5 - H3: L7 = L1 - L3: H7 = H3 - H1
  End If
  A1 = Atn(L6 / H6)
  A2 = Atn(L7 / H7)
  A3 = A1 - A2
  L8 = (H6 / 2) / Cos(A1)
  L9 = (H7 / 2) / Cos(A2)
  Aux = ((L8 * Cos(A3)) + L9) / Sin(A3)
  R0 = Sqr((Aux * Aux) + (L8 * L8))
  A4 = Arcsin(L9 / R0)
  A5 = A2 + (A4 * Sgn(A3))
  Xr0 = L3 - (R0 * Cos(A5) * Sgn(A3))
  Yr0 = H3 - (R0 * Sin(A5) * Sgn(A3))
  Aux = L2 - Xr0
  Aux1 = H2 - Yr0
  Ecart = Abs(Sqr((Aux * Aux) + (Aux1 * Aux1)) - R0)
  Aux = L4 - Xr0: Aux1 = H4 - Yr0
  Aux = Abs(Sqr((Aux * Aux) + (Aux1 * Aux1)) - R0)
  If Aux > Ecart Then Ecart = Aux
  '
End Sub

Private Sub CalcolaCerchio3P(ByVal X1 As Double, ByVal Y1 As Double, ByVal X2 As Double, ByVal Y2 As Double, ByVal X3 As Double, ByVal Y3 As Double, ByRef XC As Double, ByRef YC As Double, ByRef R0 As Double)

Dim Aux   As Double
Dim Aux1  As Double
Dim Aux2  As Double

  Aux = ((X1 - X2) / (Y1 - Y2)) * ((Y2 - Y3) / (X2 - X3))
  Aux1 = ((X1 ^ 2 - X2 ^ 2) + (Y1 ^ 2 - Y2 ^ 2)) / (2 * (Y1 - Y2))
  Aux2 = ((X1 - X2) / (Y1 - Y2)) * ((X2 ^ 2 - X3 ^ 2 + Y2 ^ 2 - Y3 ^ 2) / (2 * (X2 - X3)))
  YC = (1 / (1 - Aux)) * (Aux1 - Aux2)

  Aux = ((X2 ^ 2 - X3 ^ 2) + (Y2 ^ 2 - Y3 ^ 2)) / (2 * (X2 - X3))
  XC = -YC * ((Y2 - Y3) / (X2 - X3)) + Aux

  R0 = Sqr((X1 - XC) ^ 2 + (Y1 - YC) ^ 2)

End Sub

'INPUT:  Fianco, HelAng=beta, MM0, IDXSAP = indice di MM0 (NP1 + NPE)
Private Sub RMaxRullo(ByVal Fianco As Integer, ByVal HelAng As Double, ByVal MM0 As Double, ByVal idxSAP As Integer, ByRef X() As Double, ByRef Y() As Double, ByRef RMaxRulProf As Double, Optional XCRulProf As Double = 0, Optional YCRulProf As Double = 0)

Dim X1 As Double
Dim Y1 As Double
Dim X2 As Double
Dim Y2 As Double
Dim X3 As Double
Dim Y3 As Double
Dim XC As Double
Dim YC As Double
Dim RR As Double

  If HelAng = 0 Then
    RR = MM0
    'XC = Sgn(X(Fianco, idxSAP)) * Abs(X(Fianco, idxSAP) + RR * Cos(AlfaM(Fianco, idxSAP)))
    'YC = Y(Fianco, idxSAP) + RR * Sin(AlfaM(Fianco, idxSAP))
  Else
    X1 = X(Fianco, idxSAP - 2): X2 = X(Fianco, idxSAP - 1): X3 = X(Fianco, idxSAP)
    Y1 = Y(Fianco, idxSAP - 2): Y2 = Y(Fianco, idxSAP - 1): Y3 = Y(Fianco, idxSAP)
    Call RAG3P(X1, Y1, X2, Y2, X3, Y3, XC, YC, RR)
  End If
  RMaxRulProf = RR
  XCRulProf = XC
  YCRulProf = YC

End Sub

'INPUT:  Fianco, HelAng=beta, MM0, IDXSAP = indice di MM0 (NP1 + NPE)
Private Sub RMaxRullo2(ByVal Fianco As Integer, ByVal HelAng As Double, ByVal MM0 As Double, ByVal idxSAP As Integer, ByRef X() As Double, ByRef Y() As Double, ByRef AlfaM() As Double, ByRef RMaxRulProf As Double, Optional XCRulProf As Double = 0, Optional YCRulProf As Double = 0)

Dim m0      As Double
Dim q0      As Double
Dim M1      As Double
Dim Q1      As Double
Dim M2      As Double
Dim Q2      As Double
Dim m0norm  As Double
Dim q0norm  As Double
Dim X0      As Double
Dim Y0      As Double  'coordinate P0
Dim X1      As Double
Dim Y1      As Double  'coordinate P1
Dim X2      As Double
Dim Y2      As Double  'coordinate P2
Dim Xmed    As Double
Dim Ymed    As Double  'coordinate intersezione retta0 con retta1
Dim Rmed    As Double  'distanza intersezione da P0
Dim XC      As Double
Dim YC      As Double
Dim RR      As Double

  If HelAng = 0 Then
    RR = MM0
    XC = Sgn(X(Fianco, idxSAP)) * Abs(X(Fianco, idxSAP) + RR * Cos(AlfaM(Fianco, idxSAP)))
    YC = Y(Fianco, idxSAP) + RR * Sin(AlfaM(Fianco, idxSAP))
  Else
    X0 = X(Fianco, idxSAP): Y0 = Y(Fianco, idxSAP)
    X1 = X(Fianco, idxSAP - 1): Y1 = Y(Fianco, idxSAP - 1)
    m0 = -1 / Tan(AlfaM(Fianco, idxSAP))
    M1 = -1 / Tan(AlfaM(Fianco, idxSAP - 1))
    
    q0 = Y0 - m0 * X0
    Q1 = Y1 - M1 * X1
    
    Xmed = (Q1 - q0) / (m0 - M1)
    Ymed = m0 * Xmed + q0
    Rmed = Sqr((X0 - Xmed) ^ 2 + (Y0 - Ymed) ^ 2)
    
    X2 = Xmed + Rmed * Sin(AlfaM(Fianco, idxSAP - 1))
    Y2 = Ymed - Rmed * Cos(AlfaM(Fianco, idxSAP - 1))
    M2 = -1 / M1
    Q2 = Y2 - M2 * X2
    m0norm = -1 / m0
    q0norm = Y0 - m0norm * X0
    
    XC = (Q2 - q0norm) / (m0norm - M2)
    YC = m0norm * XC + q0norm
    RR = Sqr((X0 - XC) ^ 2 + (Y0 - YC) ^ 2)
  End If
  RMaxRulProf = RR
  XCRulProf = XC
  YCRulProf = YC
    
End Sub

'INPUT:  Fianco, HelAng=beta, MM0, IDXSAP = indice di MM0 (NP1 + NPE)
Private Sub RMaxRullo3(ByVal Fianco As Integer, ByVal HelAng As Double, ByVal MM0 As Double, ByVal idxSAP As Integer, ByRef X() As Double, ByRef Y() As Double, ByRef AlfaM() As Double, ByRef RMaxRulProf As Double, Optional XCRulProf As Double = 0, Optional YCRulProf As Double = 0)

Dim X0      As Double
Dim Y0      As Double
Dim X1      As Double
Dim Y1      As Double
Dim m0      As Double
Dim q0      As Double
Dim AuxNum  As Double
Dim AuxDen  As Double
Dim XC      As Double
Dim YC      As Double
Dim RR      As Double

  If HelAng = 0 Then
    RR = MM0
    XC = Sgn(X(Fianco, idxSAP)) * Abs(X(Fianco, idxSAP) + RR * Cos(AlfaM(Fianco, idxSAP)))
    YC = Y(Fianco, idxSAP) + RR * Sin(AlfaM(Fianco, idxSAP))
  Else
    X0 = X(Fianco, idxSAP - 1): Y0 = Y(Fianco, idxSAP - 1)
    X1 = X(Fianco, idxSAP): Y1 = Y(Fianco, idxSAP)
    
    m0 = Tan(AlfaM(Fianco, idxSAP))
    q0 = Y0 - m0 * X0
    
    AuxNum = (X0 ^ 2 + Y0 ^ 2 - 2 * Y0 * q0) - (X1 ^ 2 + Y1 ^ 2 - 2 * Y1 * q0)
    AuxDen = 2 * ((X0 + Y0 * m0) - (X1 + Y1 * m0))
    XC = AuxNum / AuxDen
    YC = m0 * XC + q0
    RR = Sqr((X0 - XC) ^ 2 + (Y0 - YC) ^ 2)
  End If
  RMaxRulProf = RR
  XCRulProf = XC
  YCRulProf = YC

End Sub

'CrossPosition: LHLow (=in basso a sinistra), LHHigh (=in alto a sinistra)
Private Sub CerchioTgDueCerchi(ByVal Xcross As Double, ByVal Ycross As Double, _
                ByVal RR1 As Double, ByVal Xc1 As Double, ByVal Yc1 As Double, _
                ByVal RR2 As Double, ByVal Xc2 As Double, ByVal Yc2 As Double, _
                ByVal R0 As Double, _
                ByRef XC0 As Double, ByRef YC0 As Double, _
                ByRef XT1 As Double, ByRef YT1 As Double, _
                ByRef XT2 As Double, ByRef YT2 As Double, _
                ByRef sgnRastr As Double)

Dim Rmax  As Double 'Max(RR1,RR2)
Dim L12   As Double 'distanza fra i centri C1 e C2
Dim L01   As Double 'distanza C0-C1 (centro cerchio tangente con centro cerchio 1)
Dim L02   As Double 'distanza C0-C1 (centro cerchio tangente con centro cerchio 1)

Dim TETA1 As Double
Dim TETA2 As Double
Dim m12   As Double
Dim q12   As Double  'retta passante per C1 e C2
Dim m01   As Double
Dim q01   As Double  'retta passante per C0 e C1
Dim m02   As Double
Dim q02   As Double  'retta passante per C0 e C2

Dim FINE  As Boolean

  If RR1 > RR2 Then Rmax = RR1 Else Rmax = RR2
  L12 = Sqr((Xc2 - Xc1) ^ 2 + (Yc2 - Yc1) ^ 2)

  If L12 > Rmax Then      'CASI 8 E 7
    '----------- RASTREMAZIONE POSITIVA ------------
    If Xc1 > Xc2 Then   'CASO 8
        L01 = Abs(RR1 + R0)
        L02 = Abs(RR2 - R0)
    Else                'CASO 7
        L01 = Abs(RR1 - R0)
        L02 = Abs(RR2 + R0)
    End If
    sgnRastr = 1
  Else                    'CASI 2 E 1
     '----------- RASTREMAZIONE NEGATIVA ------------
    L01 = Abs(RR1 - R0)
    L02 = Abs(RR2 - R0)
    sgnRastr = -1
  End If

  'ricavo TETA1 e TETA2 con il teo di Carnot
  TETA1 = Arccos((L01 ^ 2 + L12 ^ 2 - L02 ^ 2) / (2 * L01 * L12))
  TETA2 = Arccos((L02 ^ 2 + L12 ^ 2 - L01 ^ 2) / (2 * L02 * L12))

  If Xc1 > Xc2 Then
    '  *** INCROCIO A SX IN BASSO *** (caso 2 o 8)
    m12 = (Yc2 - Yc1) / (Xc2 - Xc1)
    q12 = Yc1 - m12 * Xc1
    
    m01 = Tan(Atn(m12) + TETA1)
    q01 = Yc1 - m01 * Xc1
    
    m02 = Tan(Atn(m12) + PI - TETA2)
    q02 = Yc2 - m02 * Xc2
    
    
    XT1 = Xc1 + RR1 * Cos(Atn(m12) + PI + TETA1)
    YT1 = Yc1 + RR1 * Sin(Atn(m12) + PI + TETA1)
    
    XT2 = Xc2 + RR2 * Cos(Atn(m12) - TETA2)
    YT2 = Yc2 + RR2 * Sin(Atn(m12) - TETA2)
        
    XC0 = XT1 - R0 * Cos(Atn(m12) + PI + TETA1)
    YC0 = YT1 - R0 * Sin(Atn(m12) + PI + TETA1)
    
  Else
    '  *** INCROCIO A SX IN ALTO *** (caso 1 o 7)
    m12 = (Yc2 - Yc1) / (Xc2 - Xc1)
    q12 = Yc1 - m12 * Xc1
    
    m01 = Tan(Atn(m12) + TETA1)
    q01 = Yc1 - m01 * Xc1
    
    m02 = Tan(Atn(m12) + PI - TETA2)
    q02 = Yc2 - m02 * Xc2
    
    
    XT1 = Xc1 + RR1 * Cos(Atn(m12) + TETA1)
    YT1 = Yc1 + RR1 * Sin(Atn(m12) + TETA1)
    
    XT2 = Xc2 + RR2 * Cos(Atn(m12) + PI - TETA2)
    YT2 = Yc2 + RR2 * Sin(Atn(m12) + PI - TETA2)
    
    XC0 = XT1 - R0 * Cos(Atn(m12) + TETA1)
    YC0 = YT1 - R0 * Sin(Atn(m12) + TETA1)
    
  End If

  '--- CONTROLLO LA POSIZIONE DEL PUNTO DI INTERSEZIONE ---
  If sgnRastr < 0 Then
    If Atn((Ycross - YC0) / (Xcross - XC0)) > Atn((YT1 - YC0) / (XT1 - XC0)) And _
       Atn((Ycross - YC0) / (Xcross - XC0)) < Atn((YT2 - YC0) / (XT2 - XC0)) Then
        FINE = True  'HA CALCOLATO IL CERCHIO DI RACCORDO GIUSTO
    Else
        FINE = False
    End If
  ElseIf sgnRastr > 0 Then
    If Atn((Ycross - YC0) / (Xcross - XC0)) < Atn((YT1 - YC0) / (XT1 - XC0)) And _
       Atn((Ycross - YC0) / (Xcross - XC0)) > Atn((YT2 - YC0) / (XT2 - XC0)) Then
       FINE = True  'HA CALCOLATO IL CERCHIO DI RACCORDO GIUSTO
    Else
       FINE = False
    End If
  End If
  '--------------------------------------------------------

  If FINE = True Then Exit Sub

  If L12 > Rmax Then      'CASI 5 E 6
    '----------- RASTREMAZIONE NEGATIVA ------------
    If Xc1 > Xc2 Then   'CASO 5
        L01 = Abs(RR1 - R0)
        L02 = Abs(RR2 + R0)
    Else                'CASO 6
        L01 = Abs(RR1 + R0)
        L02 = Abs(RR2 - R0)
    End If
    sgnRastr = -1
  Else                    'CASI 3 E 4
    '----------- RASTREMAZIONE POSITIVA ------------
    L01 = Abs(RR1 + R0)
    L02 = Abs(RR2 + R0)
    sgnRastr = 1
  End If

  'ricavo TETA1 e TETA2 con il teo di Carnot
  TETA1 = Arccos((L01 ^ 2 + L12 ^ 2 - L02 ^ 2) / (2 * L01 * L12))
  TETA2 = Arccos((L02 ^ 2 + L12 ^ 2 - L01 ^ 2) / (2 * L02 * L12))

  If Xc1 > Xc2 Then
    '  *** INCROCIO A SX IN ALTO *** (caso 3 o 5)
    m12 = (Yc2 - Yc1) / (Xc2 - Xc1)
    q12 = Yc1 - m12 * Xc1
    
    m01 = Tan(PI + Atn(m12) - TETA1)
    q01 = Yc1 - m01 * Xc1
    
    m02 = Tan(TETA2 + Atn(m12))
    q02 = Yc2 - m02 * Xc2
    
    XC0 = (q01 - q02) / (m02 - m01)
    YC0 = m01 * XC0 + q01
    
    XT1 = Xc1 + RR1 * Cos(PI + Atn(m12) - TETA1)
    YT1 = Yc1 + RR1 * Sin(PI + Atn(m12) - TETA1)
    
    XT2 = Xc2 + RR2 * Cos(Atn(m12) + TETA2)
    YT2 = Yc2 + RR2 * Sin(Atn(m12) + TETA2)
    
    XC0 = XT1 + R0 * Cos(PI + Atn(m12) - TETA1)
    YC0 = YT1 + R0 * Sin(PI + Atn(m12) - TETA1)
  Else
    '  *** INCROCIO A SX IN BASSO *** (caso 4 o 6)
    m12 = (Yc2 - Yc1) / (Xc2 - Xc1)
    q12 = Yc1 - m12 * Xc1
    
    m01 = -Tan(Atn(m12) + PI - TETA1)
    q01 = Yc1 - m01 * Xc1
    
    m02 = Tan(Atn(m12) + TETA2)
    q02 = Yc2 - m02 * Xc2
    
    XC0 = (q01 - q02) / (m02 - m01)
    YC0 = m01 * XC0 + q01
    
    XT1 = Xc1 + RR1 * Cos(Atn(m12) - TETA1)
    YT1 = Yc1 + RR1 * Sin(Atn(m12) - TETA1)
    
    XT2 = Xc2 + RR2 * Cos(Atn(m12) + PI + TETA2)
    YT2 = Yc2 + RR2 * Sin(Atn(m12) + PI + TETA2)
    
    XC0 = XT1 + R0 * Cos(Atn(m12) - TETA1)
    YC0 = YT1 + R0 * Sin(Atn(m12) - TETA1)
  End If

  '--- RI-CONTROLLO LA POSIZIONE DEL PUNTO DI INTERSEZIONE ---
  If sgnRastr < 0 Then
    If Atn((Ycross - YC0) / (Xcross - XC0)) > Atn((YT1 - YC0) / (XT1 - XC0)) And _
       Atn((Ycross - YC0) / (Xcross - XC0)) < Atn((YT2 - YC0) / (XT2 - XC0)) Then
        FINE = True  'HA CALCOLATO IL CERCHIO DI RACCORDO GIUSTO
    Else
        FINE = False
    End If
  ElseIf sgnRastr > 0 Then
    If Atn((Ycross - YC0) / (Xcross - XC0)) < Atn((YT1 - YC0) / (XT1 - XC0)) And _
       Atn((Ycross - YC0) / (Xcross - XC0)) > Atn((YT2 - YC0) / (XT2 - XC0)) Then
       FINE = True  'HA CALCOLATO IL CERCHIO DI RACCORDO GIUSTO
    Else
       FINE = False
    End If
  End If
  If (FINE = False) Then
    StopRegieEvents
    MsgBox "WRONG VALUE FOR ARC ON TIF ", vbCritical, "SUB CerchioTgDueCerchi"
    ResumeRegieEvents
  End If
  
End Sub

'A1,A2 = alfaRAB dei punti (rispetto al vano)
Private Sub CalcDbase_2punti(Z As Double, D1 As Double, A1 As Double, D2 As Double, A2 As Double, DBase As Double)

Dim StD1 As Double
Dim StD2 As Double
Dim SbT  As Double
'
On Error Resume Next
  '
  StD1 = D1 * Sin(PI / Z - A1)
  StD2 = D2 * Sin(PI / Z - A2)
  Call Devel_2diametri(StD1, StD2, D1, D2, DBase, SbT)

End Sub

'OUTPUT: APpreTIF1, APpostTIF1 = AP(TIF1) prima e dopo TIF1 (sez.Trasversale)
'        APpreTIF2, APpostTIF2 = AP(TIF2) prima e dopo TIF2 (sez.Trasversale)
Private Sub CalcAngPressSuTIF(ByVal Z As Integer, ByVal Fianco As Integer, ByVal idxSAP As Integer, ByVal idxTIF1 As Integer, ByVal idxTIF2 As Integer, ByVal idxTIF3 As Integer, ByRef D() As Double, ByRef AlfaRAB() As Double, ByRef APpreTIF1 As Double, ByRef APpostTIF1 As Double, ByRef APpreTIF2 As Double, ByRef APpostTIF2 As Double)
'
Dim StDia(3)      As Double
Dim DbTratto(3)   As Double
Dim SbTTratto(3)  As Double
Dim iTIF(3)       As Integer
Dim i             As Integer
'
On Error Resume Next
  '
  iTIF(0) = idxSAP
  iTIF(1) = idxTIF1
  iTIF(2) = idxTIF2
  iTIF(3) = idxTIF3
  For i = 0 To 3
    'calcolo spessore
    StDia(i) = D(Fianco, iTIF(i)) * Sin((PI / Z - AlfaRAB(Fianco, iTIF(i))))
    If i > 0 Then
      If D(Fianco, iTIF(i - 1)) <> D(Fianco, iTIF(i)) Then
        Call Devel_2diametri(StDia(i - 1), StDia(i), D(Fianco, iTIF(i - 1)), D(Fianco, iTIF(i)), DbTratto(i), SbTTratto(i))
      Else
        DbTratto(i) = DbTratto(i - 1)
      End If
    End If
  Next i
  APpreTIF1 = Arccos(DbTratto(1) / D(Fianco, iTIF(1)))
  APpostTIF1 = Arccos(DbTratto(2) / D(Fianco, iTIF(1)))
  APpreTIF2 = Arccos(DbTratto(2) / D(Fianco, iTIF(2)))
  APpostTIF2 = Arccos(DbTratto(3) / D(Fianco, iTIF(2)))
    
End Sub

'
'OUTPUT: APpreTIF1, APpostTIF1 = AP(TIF1) prima e dopo TIF1 (sez.Trasversale)
'        APpreTIF2, APpostTIF2 = AP(TIF2) prima e dopo TIF2 (sez.Trasversale)
Private Sub CalcAngPressSuEAP(ByVal Z As Integer, ByVal Fianco As Integer, ByVal idxSAP As Integer, ByVal idxTIF1 As Integer, ByVal idxTIF2 As Integer, ByVal idxTIF3 As Integer, ByVal IDXEAP As Integer, ByRef D() As Double, ByRef AlfaRAB() As Double, ByRef APeap As Double)
    
Dim StDia(3)      As Double
Dim DbTratto(3)   As Double
Dim SbTTratto(3)  As Double
Dim iTIF(4)       As Integer
Dim i             As Integer
'
On Error Resume Next
  '
  iTIF(0) = idxSAP
  iTIF(1) = idxTIF1
  iTIF(2) = idxTIF2
  iTIF(3) = idxTIF3
  iTIF(4) = IDXEAP
  For i = 0 To 3
    'calcolo spessore
    StDia(i) = D(iTIF(i)) * Sin((PI / Z - AlfaRAB(Fianco, iTIF(i))))
    If i > 0 Then
      If D(iTIF(i - 1)) <> D(iTIF(i)) Then
        Call Devel_2diametri(StDia(i - 1), StDia(i), D(iTIF(i - 1)), D(iTIF(i)), DbTratto(i), SbTTratto(i))
      Else
        DbTratto(i) = DbTratto(i - 1)
      End If
    End If
  Next i
  APeap = Arccos(DbTratto(3) / D(iTIF(4)))
    
End Sub

Private Sub CalcRRaccSuTIF(ByVal R0 As Double, ByVal idxTIF As Integer, ByVal Fianco As Integer, ByVal PAS As Double, ByRef D() As Double, ByRef A() As Double, ByRef API() As Double, ByRef NPaggiunti As Integer, ByRef XC0 As Double, ByRef YC0 As Double)
    
Dim i     As Integer
Dim j     As Integer
Dim jj    As Integer
Dim NPC1  As Integer
Dim NPC2  As Integer
Dim NPCR  As Integer

Dim Xaux(30)  As Double
Dim Yaux(30)  As Double
Dim APaux(30) As Double
Dim sogliaY   As Double
Dim DeltaY    As Double
Dim T1outside As Boolean
Dim T2outside As Boolean
Dim count     As Integer: count = 0

Dim step1 As Integer
Dim step2 As Integer

Dim idxInizio     As Integer
Dim idxFine       As Integer
Dim NPconsiderati As Integer

Dim YT1min As Double
Dim YT1med As Double
Dim YT1max As Double
Dim YT2min As Double
Dim YT2med As Double
Dim YT2max As Double
Dim T1_OKflag As Boolean
Dim T2_OKflag As Boolean

Dim NPrac As Integer
Dim stmp As String
'
On Error Resume Next
  '
  ReDim Xass(5)
  ReDim Yass(5)
    
  sogliaY = 0.1
  step1 = 1
  step2 = 1
    
CALCRR:
  If T1outside = True Then step1 = step1 + 1
  If T2outside = True Then step2 = step2 + 1
  idxInizio = idxTIF - 2 * step1
  idxFine = idxTIF + 2 * step2
  NPconsiderati = idxFine - (idxInizio - 1) 'il -1 serve per considerare il punto iniziale
  For i = 1 To NPconsiderati
    jj = (idxInizio - 1) + i  'il -1 serve per considerare il punto iniziale
    If INP.Beta > PI / 4 Then
      'conversione in coordinate assiali punti vicini al raccordo
      Yaux(i) = D(Fianco, jj) / 2
      Xaux(i) = A(Fianco, jj) * PAS / 2 / PI
    Else
      'verificare!!!!!
      Yaux(i) = D(Fianco, jj) / 2 * Sin(A(Fianco, jj))
      Xaux(i) = D(Fianco, jj) / 2 * Cos(A(Fianco, jj))
    End If
  Next i
  If INP.Beta > PI / 4 Then
    i = 3
    For jj = idxTIF To idxInizio Step -step1
      Yass(i) = D(Fianco, jj) / 2: Xass(i) = A(Fianco, jj) * PAS / 2 / PI
      i = i - 1
    Next jj
    i = 3
    For jj = idxTIF To idxFine Step step2
      Yass(i) = D(Fianco, jj) / 2: Xass(i) = A(Fianco, jj) * PAS / 2 / PI
      i = i + 1
    Next jj
  Else
    'verificare!!!!!
    For jj = idxTIF To idxInizio Step -step1
      i = (jj - (idxInizio - 1)) / step1
      Yass(i) = D(Fianco, jj) / 2 * Sin(A(Fianco, jj))
      Xass(i) = D(Fianco, jj) / 2 * Cos(A(Fianco, jj))
    Next jj
    For jj = idxTIF To idxFine Step step2
      i = (jj - (idxInizio - 1)) / step2
      Yass(i) = D(Fianco, jj) / 2 * Sin(A(Fianco, jj))
      Xass(i) = D(Fianco, jj) / 2 * Cos(A(Fianco, jj))
    Next jj
  End If
  Call RAG3P(Xass(1), Yass(1), Xass(2), Yass(2), Xass(3), Yass(3), Xc1, Yc1, RR1)
  Call RAG3P(Xass(3), Yass(3), Xass(4), Yass(4), Xass(5), Yass(5), Xc2, Yc2, RR2)
  '---------------- CALCOLO CERCHIO DI RACCORDO C/O TIF ------------------
  Dim posizione As Double ': posizione = -1
  Call CerchioTgDueCerchi(Xass(3), Yass(3), RR1, Xc1, Yc1, RR2, Xc2, Yc2, R0, XC0, YC0, XT1, YT1, XT2, YT2, posizione)
  '-----------------------------------------------------------------------
  T1_OKflag = False: T2_OKflag = False
  YT1max = Yass(1) + sogliaY
  YT1med = (Yass(1) + Yass(2)) / 2
  YT1min = Yass(3) + sogliaY
  YT2max = Yass(3) - sogliaY
  YT2med = (Yass(4) + Yass(5)) / 2
  YT2min = Yass(5) - sogliaY
  NPC1 = 0: NPC2 = 0: NPCR = 0: NPrac = 0
  ' --- T1 ---
  If YT1 > YT1max Then T1outside = True
  If (YT1 < YT1max) And (YT1 > YT1med) Then
      Xaux(1) = XT1
      Yaux(1) = YT1
      NPC1 = 0
      NPCR = 3 + 2 * (step1 - 1)
      T1_OKflag = True
      T1outside = False
  End If
  If (YT1 < YT1med) And (YT1 > YT1min) Then
      NPC1 = 2
      NPCR = 1 + 2 * (step1 - 1)
      T1_OKflag = True
      T1outside = False
  End If
  '----------------- zona centrale - da sistemare?
  If (YT1 < YT1min) And (YT1 > Yass(3)) Then
      If (YT2 < Yass(3)) And (YT2 > YT2max) Then
          'lascio tutto invariato
          T2_OKflag = False
      Else
          Yaux(4) = YT1
          Xaux(4) = XT1
          NPC1 = 2
          NPCR = 1
          T2_OKflag = True
      End If
      T1_OKflag = False
      T1outside = False
  End If
  If (YT2 < Yass(3)) And (YT2 > YT2max) Then
      If (YT1 < YT1min) And (YT1 > Yass(3)) Then
      'lascio tutto invariato
      Else
          NPCR = NPCR + 2
          NPC2 = 2
          T1_OKflag = True
      End If
      T2_OKflag = False
      T2outside = False
  End If
  ' --- T2 ---
  If (YT2 < YT2max) And (YT2 > YT2med) Then
      NPCR = NPCR + 2 + 2 * (step2 - 1)
      NPC2 = 2
      T2_OKflag = True
      T2outside = False
  End If
  If (YT2 < YT2med) And (YT2 > YT2min) Then
      'Xaux(5) = XT2
      'Yaux(5) = YT2
      NPCR = NPCR + 4 + 2 * (step2 - 1)
      NPC2 = 0
      T2_OKflag = True
      T2outside = False
  End If
  If YT2 < YT2min Then T2outside = True
If T1outside = True Or T2outside = True Then count = count + 1 Else count = 6
If (count > 0) And (count < 6) Then GoTo CALCRR
  'creo vettori d'appoggio con i valori originari di D, A e API
  For i = 0 To NPnt(Fianco)
    Daux(Fianco, i) = D(Fianco, i)
    Aaux(Fianco, i) = A(Fianco, i)
    APIaux(Fianco, i) = API(Fianco, i)
  Next i
  Open g_chOemPATH & "\RABold_F" & CStr(Fianco) & ".txt" For Output As #2
    For i = 1 To NPnt(Fianco)
      stmp = Left$(Format$(Daux(Fianco, i), "###0.###0") + "            ", 12)
      stmp = stmp & Left$(Format$(Aaux(Fianco, i) * 180 / PI, "###0.###0") + "            ", 12)
      stmp = stmp & Left$(Format$(APIaux(Fianco, i), "###0.###0") + "            ", 12)
      Print #2, stmp
    Next i
  Close #2
  If (T1_OKflag = True Or T2_OKflag = True) Then
      ' --- cerchio C1 ---
      If NPC1 > 0 Then DeltaY = (Yass(1) - YT1) / NPC1
      For i = 1 To NPC1
        Yaux(i) = Yass(1) - DeltaY * (i - 1)
        Xaux(i) = Xc1 - RR1 * Cos(Arcsin((Yaux(i) - Yc1) / RR1))
        APaux(i) = Abs(Arcsin((Yaux(i) - Yc1) / RR1))
      Next i
      ' --- cerchio Cr ---
      DeltaY = (YT1 - YT2) / (NPCR - 1)
      For i = 1 To NPCR
        If NPC1 > 0 Then j = NPC1 + i Else j = i
        Yaux(j) = YT1 - DeltaY * (i - 1)
        Xaux(j) = XC0 + posizione * R0 * Cos(Arcsin((Yaux(j) - YC0) / R0))
        APaux(j) = Abs(Arcsin((Yaux(j) - YC0) / R0))
      Next i
      ' --- cerchio C2 ---
      If NPC2 > 0 Then DeltaY = (YT2 - Yass(5)) / NPC2
      For i = 1 To NPC2
        j = NPC1 + NPCR + i
        Yaux(j) = YT2 - DeltaY * i
        Xaux(j) = Xc2 - RR2 * Cos(Arcsin((Yaux(j) - Yc2) / RR2))
        APaux(j) = Abs(Arcsin((Yaux(j) - Yc2) / RR2))
      Next i
      NPrac = NPC1 + NPCR + NPC2
      NPaggiunti = NPrac - NPconsiderati
      ' --- profilo trasversale ---
      For i = 1 To NPrac
          j = (idxInizio - 1) + i
          D(Fianco, j) = 2 * Yaux(i)
          A(Fianco, j) = 2 * PI * Xaux(i) / PAS
          API(Fianco, j) = Atn(Tan(APaux(i)) * PI * D(Fianco, j) / PAS) * 180 / PI
      Next i
      For i = idxFine + 1 To NPnt(Fianco)
          j = NPaggiunti + i
          D(Fianco, j) = Daux(Fianco, i)
          A(Fianco, j) = Aaux(Fianco, i)
          API(Fianco, j) = APIaux(Fianco, i)
      Next i
      Open g_chOemPATH & "\RABnew_F" & CStr(Fianco) & ".txt" For Output As #2
       For i = 1 To NPnt(Fianco) + NPaggiunti
          stmp = Left$(Format$(D(Fianco, i), "###0.###0") + "            ", 12)
          stmp = stmp & Left$(Format$(A(Fianco, i) * 180 / PI, "###0.###0") + "            ", 12)
          stmp = stmp & Left$(Format$(API(Fianco, i), "###0.###0") + "            ", 12)
          Print #2, stmp
       Next i
      Close #2
  Else
      NPaggiunti = 0
  End If
    
End Sub

'CALCOLO ANGOLO AL CENTRO  A1-A2
'Q1=quadrante I-2     Q2=quadrante I
Private Sub CalcolaAngoloAlCentro0(ByVal X1 As Double, ByVal Y1 As Double, ByVal X2 As Double, ByVal Y2 As Double, ByVal XC As Double, ByVal YC As Double, ByRef A1 As Double, ByRef A2 As Double)
        
Dim Q1  As Double
Dim Q2  As Double
Dim Aux As Double
'
On Error Resume Next
  '
  If X1 >= XC And Y1 >= YC Then Q1 = 1
  If X1 >= XC And Y1 < YC Then Q1 = 4
  If X1 < XC And Y1 > YC Then Q1 = 2
  If X1 < XC And Y1 < YC Then Q1 = 3
  If X2 >= XC And Y2 >= YC Then Q2 = 1
  If X2 >= XC And Y2 < YC Then Q2 = 4
  If X2 < XC And Y2 > YC Then Q2 = 2
  If X2 < XC And Y2 < YC Then Q2 = 3
  If X1 = XC Then
    A1 = -PI / 2   'pi*3/2
  Else
    A1 = Atn((Y1 - YC) / (X1 - XC))
  End If
  If Q1 = 1 Then A1 = -A1 + 2 * PI
  If Q1 = 3 Then A1 = -A1 + PI  'OK
  If Q1 = 2 Then A1 = -A1 + PI  'OK
  If Q1 = 4 Then A1 = -A1  'OK
  If X2 = XC Then
    A2 = -PI / 2   'pi*3/2
  Else
    A2 = Atn((Y2 - YC) / (X2 - XC))
  End If
  If Q2 = 1 Then A2 = -A2 + 2 * PI
  If Q2 = 3 Then A2 = -A2 + PI  'OK
  If Q2 = 2 Then A2 = -A2 + PI  'OK
  If Q2 = 4 Then A2 = -A2  'OK
  If A1 < 0 Then A1 = A1 + 2 * PI '  -A1 '+ 2 * PI
  If A2 < 0 Then A2 = A2 + 2 * PI '  -A2 '+ 2 * PI
  '----------- fabio --------------
  If A1 > 2 * PI Then A1 = A1 - 2 * PI
  If A2 > 2 * PI Then A2 = A2 - 2 * PI
  '--------------------------------
  If Abs(A1 - A2) > PI And A1 < A2 Then Aux = A1: A1 = A2: A2 = Aux
  If Abs(A1 - A2) < PI And A1 > A2 Then Aux = A1: A1 = A2: A2 = Aux

End Sub

Private Sub CalcolaAngoloAlCentro(ByVal X1 As Double, ByVal Y1 As Double, ByVal X2 As Double, ByVal Y2 As Double, ByVal XC As Double, ByVal YC As Double, ByRef A1 As Double, ByRef A2 As Double)

Dim Q1  As Double
Dim Q2  As Double
Dim Aux As Double
'
On Error Resume Next
  '
  If X1 >= XC And Y1 >= YC Then Q1 = 1
  If X1 >= XC And Y1 < YC Then Q1 = 4
  If X1 < XC And Y1 > YC Then Q1 = 2
  If X1 < XC And Y1 < YC Then Q1 = 3
  If X2 >= XC And Y2 >= YC Then Q2 = 1
  If X2 >= XC And Y2 < YC Then Q2 = 4
  If X2 < XC And Y2 > YC Then Q2 = 2
  If X2 < XC And Y2 < YC Then Q2 = 3
  If X1 = XC Then
    A1 = PI / 2
  Else
    A1 = Atn((Y1 - YC) / (X1 - XC))
  End If
  If Q1 = 3 Then A1 = A1 + PI
  If Q1 = 2 Then A1 = A1 + PI
  If Q1 = 4 Then A1 = A1 + 2 * PI
  If X2 = XC Then
    A2 = PI / 2
  Else
    A2 = Atn((Y2 - YC) / (X2 - XC))
  End If
  If Q2 = 3 Then A2 = A2 + PI
  If Q2 = 2 Then A2 = A2 + PI
  If Q2 = 4 Then A2 = A2 + 2 * PI
  If A1 < 0 Then A1 = A1 + 2 * PI
  If A2 < 0 Then A2 = A2 + 2 * PI
  If Abs(A1 - A2) > PI And A1 < A2 Then Aux = A1: A1 = A2: A2 = Aux
  If Abs(A1 - A2) < PI And A1 > A2 Then Aux = A1: A1 = A2: A2 = Aux
        
End Sub

Private Sub Devel_2diametri(E1 As Double, E2 As Double, D1 As Double, D2 As Double, db As Double, Eb As Double)
   
Dim Emn As Double
Dim Emx As Double
Dim Dmn As Double
Dim Dmx As Double
Dim Amn As Double
Dim Amx As Double
Dim PAS As Double
Dim X   As Double
Dim A   As Double
'
On Error Resume Next
  '
  ' E1 , E2 : cordale
  If D1 < 0.1 Then db = 0: Exit Sub
  If D2 < 0.1 Then db = 0: Exit Sub
  If D1 > D2 Then
    Emn = E2: Dmn = D2: Emx = E1: Dmx = D1
  Else
    Emn = E1: Dmn = D1: Emx = E2: Dmx = D2
  End If
  Amn = Arcsin(Emn / Dmn): Amx = Arcsin(Emx / Dmx)
  'Amn = Emn / Dmn: Amx = Emx / Dmx
  PAS = 1: db = Dmn - PAS
  X = -1
  Do
    db = db + PAS: PAS = PAS / 10
    Do
      db = db - PAS
      A = Amx + inv(Arccos(db / Dmx))
      Eb = A * db
      X = (A - inv(Arccos(db / Dmn))) - Amn
    Loop While X < 0
    If (Abs(db - Dmn) < 0.001) Then
      ' non esiste un'evolvente passando da questi punti
      db = 0: Eb = 0
      Exit Do
    End If
  Loop While Abs(X) > 0.0000001
   
End Sub
