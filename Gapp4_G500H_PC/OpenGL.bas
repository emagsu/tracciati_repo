Attribute VB_Name = "OpenGL"
Option Explicit

Public Type PALETTEENTRY
    peRed As Byte
    peGreen As Byte
    peBlue As Byte
    peFlags As Byte
End Type
Public Type LOGPALETTE
    palVersion As Integer
    palNumEntries As Integer
    palPalEntry(0 To 255) As PALETTEENTRY
End Type
Public Type PIXELFORMATDESCRIPTOR
    nSize As Integer
    nVersion As Integer
    dwFlags As Long
    iPixelType As Byte
    cColorBits As Byte
    cRedBits As Byte
    cRedShift As Byte
    cGreenBits As Byte
    cGreenShift As Byte
    cBlueBits As Byte
    cBlueShift As Byte
    cAlphaBits As Byte
    cAlphaShift As Byte
    cAccumBits As Byte
    cAccumRedBits As Byte
    cAccumGreenBits As Byte
    cAccumBlueBits As Byte
    cAccumAlpgaBits As Byte
    cDepthBits As Byte
    cStencilBits As Byte
    cAuxBuffers As Byte
    iLayerType As Byte
    bReserved As Byte
    dwLayerMask As Long
    dwVisibleMask As Long
    dwDamageMask As Long
End Type

Const PFD_TYPE_RGBA = 0
Const PFD_TYPE_COLORINDEX = 1
Const PFD_MAIN_PLANE = 0
Const PFD_DOUBLEBUFFER = 1
Const PFD_DRAW_TO_WINDOW = &H4
Const PFD_SUPPORT_OPENGL = &H20
Const PFD_NEED_PALETTE = &H80

'Microsoft's OpenGL
Public Declare Function ChoosePixelFormat Lib "gdi32" (ByVal hdc As Long, pfd As PIXELFORMATDESCRIPTOR) As Long
Public Declare Function CreatePalette Lib "gdi32" (pPal As LOGPALETTE) As Long
Public Declare Sub DeleteObject Lib "gdi32" (hObject As Long)
Public Declare Sub DescribePixelFormat Lib "gdi32" (ByVal hdc As Long, ByVal PixelFormat As Long, ByVal nBytes As Long, pfd As PIXELFORMATDESCRIPTOR)
Public Declare Function GetDC Lib "gdi32" (ByVal hWnd As Long) As Long
Public Declare Function GetPixelFormat Lib "gdi32" (ByVal hdc As Long) As Long
Public Declare Sub GetSystemPaletteEntries Lib "gdi32" (ByVal hdc As Long, ByVal start As Long, ByVal entries As Long, ByVal ptrEntries As Long)
Public Declare Sub RealizePalette Lib "gdi32" (ByVal hPalette As Long)
Public Declare Sub SelectPalette Lib "gdi32" (ByVal hdc As Long, ByVal hPalette As Long, ByVal bln As Long)
Public Declare Function SetPixelFormat Lib "gdi32" (ByVal hdc As Long, ByVal i As Long, pfd As PIXELFORMATDESCRIPTOR) As Boolean
Public Declare Sub SwapBuffers Lib "gdi32" (ByVal hdc As Long)
Public Declare Function wglCreateContext Lib "opengl32" (ByVal hdc As Long) As Long
Public Declare Sub wglDeleteContext Lib "opengl32" (ByVal hContext As Long)
Public Declare Sub glDeleteBuffer Lib "opengl32" (i As Integer, ByRef buffer As Long)

Public Declare Sub wglMakeCurrent Lib "opengl32" (ByVal l1 As Long, ByVal l2 As Long)
Public Declare Sub glMultMatrixf Lib "OpenGL32.dll" (M As Single)
Public Declare Function glCallListsString Lib "opengl32" Alias "glCallLists " (ByVal N As Long, ByVal tipe As Long, ByVal lists As String)
'Public Declare sub glMultMatrixf Lib "OpenGL32.dll" (const byval single *m)
Public hPalette As Long
Public hGLRC As Long
Public hdc_l As Long
Public hGLRC1 As Long
Public Ogl_Show As Boolean
'----------------------------------
Public bitmapImage() As GLubyte
Public bitmapHeight As GLfloat
Public bitmapWidth As GLfloat
Public bitmapFilename As String
Public Texes(50) As GLuint
Public Texeq(50) As GLuint
Public FPS%, dwframes%, LastTime%
Public GLMode As String
Private Timer%
'----------------------------------
Private fovy#   ' Specifies the field of view angle, in degrees, in the y direction.
Private aspect# ' Specifies the aspect ratio that determines the field of view in the x direction. The aspect ratio is the ratio of x (width) to y (height).
Private zNear#  ' Specifies the distance from the viewer to the near clipping plane (always positive).
Private zFar#   ' Specifies the distance from the viewer to the far clipping plane (always positive).
Private ScaleGLy#
'----------------------------------
Private PG#, PI#
'----------------------------------
Public Sub LoadFromBMP(ByRef pctPicture As PictureBox)

    Dim X As Double, Y As Double
    Dim C As Long

    bitmapHeight = pctPicture.ScaleHeight
    bitmapWidth = pctPicture.ScaleWidth
    ReDim bitmapImage(2, bitmapHeight - 1, bitmapWidth - 1)
           
    For X = 0 To bitmapWidth - 1
        For Y = 0 To bitmapHeight - 1
            C = pctPicture.Point(X, Y)                ' Returns in long format.
            bitmapImage(0, X, bitmapHeight - Y - 1) = C Mod 256
            bitmapImage(1, X, bitmapHeight - Y - 1) = (C And 65280) / 256
            bitmapImage(2, X, bitmapHeight - Y - 1) = (C And 16711680) / 256 / 256
        Next
    Next
    
End Sub
'----------------------------------
Sub SetUpTexture(Num As Byte, FileName As String, formIN As Form)

  LoadFromBMP formIN.Picture 'pctPicture
  glBindTexture GL_TEXTURE_2D, Texes(Num)
  glPixelStorei GL_UNPACK_ALIGNMENT, 1
  glTexImage2D GL_TEXTURE_2D, 0, 3, bitmapWidth, bitmapHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, bitmapImage(0, 0, 0)
  glTexParameterf GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR
  glTexParameterf GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR
  glTexEnvf GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL
    
End Sub
'----------------------------------
Sub FatalError(ByVal strMessage As String)
 MsgBox "Fatal Error: " & strMessage, vbCritical + vbApplicationModal + vbOKOnly + vbDefaultButton1, "Fatal Error In " & App.title
 End
End Sub
'----------------------------------
Sub SetupPixelFormat(ByVal hdc As Long)
Dim pfd As PIXELFORMATDESCRIPTOR
Dim PixelFormat As Integer
 pfd.nSize = Len(pfd)
 pfd.nVersion = 1
 pfd.dwFlags = PFD_SUPPORT_OPENGL Or PFD_DRAW_TO_WINDOW Or PFD_DOUBLEBUFFER Or PFD_TYPE_RGBA
 pfd.iPixelType = PFD_TYPE_RGBA
 pfd.cColorBits = 16
 pfd.cDepthBits = 16
 pfd.iLayerType = PFD_MAIN_PLANE
 PixelFormat = ChoosePixelFormat(hdc, pfd)
 If PixelFormat = 0 Then FatalError "Could not retrieve pixel format!"
 SetPixelFormat hdc, PixelFormat, pfd
End Sub
'----------------------------------
Sub DeleteWGL()
 On Error Resume Next
 If hGLRC <> 0 Then
  
  wglMakeCurrent 0, 0
  glDeleteBuffer 0, hdc_l
  wglDeleteContext hGLRC
  
 End If
 If hGLRC1 <> 0 Then
  wglMakeCurrent 0, 0
  glDeleteBuffer 0, hdc_l
  wglDeleteContext hGLRC1
  
 End If
End Sub

'-------------------------------------------------------------
Sub OpenGL_Load(ByRef formIN As Form, Optional ByRef hdc_IN As Long, Optional ByVal Width As Integer, Optional ByVal heigh As Integer)   'Optional AttivaB As Boolean = False
'----------------------------------------------------------------
Dim i As Integer
Dim Active_perspective As Boolean
Dim hdc As Long
'Dim aspectRatio As GLfloat
  PG = 4 * Atn(1)
  PI = 4 * Atn(1)
hdc = hdc_IN 'formIN.hdc
'----------------------------------------------------------------
  hdc_l = hdc
  SetupPixelFormat hdc
  Call DeleteWGL
  hGLRC = wglCreateContext(hdc)
  wglMakeCurrent hdc, hGLRC
'----------------------------------------------------------------
' make the system font the device context's selected font
'  SelectObject hdc, GetStockObject(SYSTEM_FONT) '
' create the bitmap display lists  we're making images of glyphs 0 thru 254 the display list numbering starts at 1000, an arbitrary choice
'->  wglUseFontBitmaps hdc, 0, 255, 0 '1000
' display a string:  indicate start of glyph display lists
'->    glListBase 0 '1000
'----------------------------------------------------------------
  glClearColor 1, 1, 1, 1 'glClearColor 0.4, 0.8, 1, 0.9
  glEnable (GL_DEPTH_TEST)
  glShadeModel (GL_SMOOTH) 'GL_FLAT
'----------------------------------------------------------------
  glCullFace (GL_BACK)
  glEnable (GL_CULL_FACE)
  Dim W As Byte
  glGenTextures 21, Texes(0)
'    pctPicture.Picture = LoadPicture(App.Path & "\grey.bmp")
'    SetUpTexture 1, (App.Path & "\grey.bmp"), FormIN
  glHint GL_PERSPECTIVE_CORRECTION_HINT, GL_FLAT 'GL_SMOOTH
  glEnable GL_TEXTURE_2D
  glShadeModel GL_SMOOTH 'GL_FLAT
'----------------------------------------------------------------
  Call glEnable(GL_NORMALIZE)  ' NEW
  Call glEnable(GL_COLOR_MATERIAL)  ' NEW
'----------------------------------------------------------------
  Active_perspective = True
'----------------------------------------------------------------
  If (Active_perspective) Then
  ' Call glViewport(0, 0, formIN.ScaleWidth, formIN.ScaleHeight)
   Call glMatrixMode(GL_PROJECTION)
   
  ' aspectRatio = Width / heigh
   fovy = 10
   aspect = Width / heigh
   ScaleGLy = heigh / HGp.fw_g_ref
   zNear = 1
   zFar = 10000
   glLoadIdentity
  ' glViewport 0, 0, Me.ScaleWidth, Me.ScaleHeight OEMX MDIForm1
   gluPerspective fovy, aspect, zNear, zFar
  Else
  'Call glOrtho(left, right, bottom, Top, zNear, zFar)
   glMatrixMode GL_MODELVIEW
  'glLoadIdentity
   Call glOrtho(0, formIN.ScaleWidth, 0, formIN.ScaleHeight, 1, 1000)
  End If
'----------------------------------------------------------------
   Ogl_Paint
  'OpenGL.Enabled = True
'----------------------------------------------------------------
End Sub
Sub OpenGL_Resize(ByRef formIN As Form, Optional ByRef hdc_IN As Long, Optional ByVal Width As Integer, Optional ByVal heigh As Integer)   'Optional AttivaB As Boolean = False
'----------------------------------------------------------------
Dim i As Integer
Dim Active_perspective As Boolean
Dim hdc As Long
Dim aspectRatio As GLfloat

hdc = hdc_IN 'formIN.hdc
'----------------------------------------------------------------
  hdc_l = hdc
  SetupPixelFormat hdc
  Call DeleteWGL
  hGLRC = wglCreateContext(hdc)
  wglMakeCurrent hdc, hGLRC
'----------------------------------------------------------------
' make the system font the device context's selected font
'  SelectObject hdc, GetStockObject(SYSTEM_FONT) '
' create the bitmap display lists  we're making images of glyphs 0 thru 254 the display list numbering starts at 1000, an arbitrary choice
'->  wglUseFontBitmaps hdc, 0, 255, 0 '1000
' display a string:  indicate start of glyph display lists
'->    glListBase 0 '1000
'----------------------------------------------------------------
  glClearColor 1, 1, 1, 1 'glClearColor 0.4, 0.8, 1, 0.9
  glEnable (GL_DEPTH_TEST)
  glShadeModel (GL_SMOOTH) 'GL_FLAT
'----------------------------------------------------------------
  glCullFace (GL_BACK)
  glEnable (GL_CULL_FACE)
  Dim W As Byte
  glGenTextures 21, Texes(0)
'    pctPicture.Picture = LoadPicture(App.Path & "\grey.bmp")
'    SetUpTexture 1, (App.Path & "\grey.bmp"), FormIN
  glHint GL_PERSPECTIVE_CORRECTION_HINT, GL_FLAT 'GL_SMOOTH
  glEnable GL_TEXTURE_2D
  glShadeModel GL_SMOOTH 'GL_FLAT
'----------------------------------------------------------------
  Call glEnable(GL_NORMALIZE)  ' NEW
  Call glEnable(GL_COLOR_MATERIAL)  ' NEW
'----------------------------------------------------------------
  Active_perspective = True
'----------------------------------------------------------------
  If (Active_perspective) Then
'    Call glViewport(0, 0, Width, heigh)
   Call glMatrixMode(GL_PROJECTION)

   aspectRatio = Width / heigh
   fovy = 10
   aspect = Width / heigh
   zNear = 1
   zFar = 1000
   glLoadIdentity
 ' Call glViewport(0, 0, Width, heigh) ' glViewport 0, 0, Me.ScaleWidth, Me.ScaleHeight OEMX MDIForm1
   gluPerspective fovy, aspect, zNear, zFar
  Else
  'Call glOrtho(left, right, bottom, Top, zNear, zFar)
   glMatrixMode GL_MODELVIEW
  'glLoadIdentity
   Call glOrtho(0, formIN.ScaleWidth, 0, formIN.ScaleHeight, 1, 1000)
  End If
'----------------------------------------------------------------
   Ogl_Paint
  'OpenGL.Enabled = True
'----------------------------------------------------------------
End Sub
 Sub Ogl_Paint()
 'On Error Resume Next
 Dim i%
 Dim Ang#
 Dim upX#, upY#, upZ#, eyeX#, eyeY#, eyeZ#, centerX#, centerY#, centerZ#
'---------------------------------------------------------------------------
'   wglMakeCurrent Mainfm.hdc, hGLRC
 Dim nErr As Integer
 nErr = glGetError()

    wglMakeCurrent hdc_l, hGLRC
Timer = Timer + 1
If (Timer > 360) Then Timer = 0
'------------------------------------------------------------------------
  GL.glPushMatrix
Call GlobalTern_Render
   glClear GL_COLOR_BUFFER_BIT Or GL_DEPTH_BUFFER_BIT   ': Errore = Check_GL_Error  '

'  SwapBuffers hdc_l
'------------------------------------------------------------------------
   upX = 0: upY = 1: upZ = 0
   eyeX = HGp.Deap_ref / 2# + HGp.fw_g_ref / Tan(FnGR(fovy))
   eyeY = 0
   eyeZ = 0
   centerX = HGp.Deap_ref / 2#
   centerY = 0
   centerZ = 0
   gluLookAt eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ
   Call GL.glScalef(0.99 + 0.01 * Cos(FnGR(Timer)), 0.99 + 0.01 * Cos(FnGR(Timer)), 0.99 + 0.01 * Cos(FnGR(Timer)))
'--------------------------------------------------------------------------------------------
' glClearColor 0.4, 0.8, 1, 0.9
' glClear GL_COLOR_BUFFER_BIT Or GL_DEPTH_BUFFER_BIT
' SwapBuffers hdc_l ' Me.hdc
' GL.glFlush
'--------------------------------------------------------------------------------------------
  Call GlobalTern_Render

  Ang = 0# '-360 / HGp.Z_g_ref
  For i = 0 To 2 'HGp.Z_g_ref - 1
   Ang = Ang + (i) * ((-1) ^ i) * 360 / HGp.Z_g_ref
     GL.glPushMatrix
   glRotatef Ang, 0, 1, 0
   Call Tooth_Render
     GL.glPopMatrix
  Next i

 ' GL.glFinish
 GL.glPopMatrix
 SwapBuffers hdc_l ' Me.hdc
'Call GlobalTern_Render
'glFinish
 'Call GlobalTern_Render
  nErr = glGetError()
 ' Sleep (15)
 'glFlush
 'glFinish
 'Call GlobalTern_Render
ESCI:
End Sub

'--------------------------------------------------------
' Terna di riferimento
Public Sub GlobalTern_Render(Optional ByVal displ1 = 0, Optional ByVal dmsn As Integer = 4)
'On Error Resume Next
GLMode = GL_LINE_STRIP
glLineWidth (dmsn)
   glColor3f 1#, 0#, 0#
 glBegin GLMode
    glVertex3f 0, -displ1, 0
    glVertex3f 1, -displ1, 0
 glEnd
    glColor3f 0#, 1#, 0#
 glBegin GLMode
    glVertex3f 0, -displ1, 0
    glVertex3f 0, -displ1 + 1, 0
 glEnd
    glColor3f 0#, 0#, 1#
 glBegin GLMode
    glVertex3f 0, -displ1, 0
    glVertex3f 0, -displ1, 1
 glEnd
 glLineWidth (1)
End Sub
'---------------------------------------------------------------------
Public Sub Tooth_Render(Optional angleGear As Double = 0)
'---------------------------------------------------------------------
 Dim Kx%, Zg_ref%, Zg_render%, iC%, L%, k1%, k%, jn%
 Dim id(0 To 3) As Integer
 Dim angle As Double
 Dim Render As Boolean
 Render = True

 Zg_ref = Abs(HGp.Z_g_ref): Zg_render = Int(HGp.Z_g_ref / 3) ':HGp.Z_g_i  'HGp.Z_g_ref
 
  If Render Then
   Kx = 2
   Zg_render = 0 'HGp.Z_g_i - 1
   If (Zg_render < 0) Then Zg_render = 0 ' Int(2 * (Ang / (2 * Pi)) * HGp.Z_g_ref)
  Else
   Kx = 1
   Zg_render = 0
  End If
'---------------------------------------------------------------------
 angle = 0
'---------------------------------------------------------------------
 For iC = 0 To Zg_render '(Zg - 1) / 4
'---------------------------------------------------------------------
  angle = angle + ((-1#) ^ iC) * iC * 360# / (Zg_ref)
  glPushMatrix
  glRotated angle, 0#, 1#, 0#
'---------------------------------------------------------------------

  For L = 0 To 0 'Zg - 1
   For k1 = 1 To Kx
    Select Case (k1)
    Case (1)
     GLMode = GL_LINE_LOOP
     If (glIsEnabled(GL_TEXTURE_2D)) Then glDisable GL_TEXTURE_2D
     Call glColor3f(100 / 255, 180 / 255, 255 / 255)
    Case (2)
     GLMode = GL_QUADS
     If (glIsEnabled(GL_CULL_FACE)) Then glDisable (GL_CULL_FACE)
     If (Not glIsEnabled(GL_TEXTURE_2D)) Then glEnable GL_TEXTURE_2D
     glBindTexture GL_TEXTURE_2D, Texes(1)
     Call glColor3f(225 / 255, 240 / 255, 255 / 255)
    End Select
    'MousePointer = 1:
    glLineWidth (1)
    If (iC <> 0) Then glLineWidth (1)

    For k = 1 To 2
     If (iC <> 0) Then
    '  glColor3f RGB_Gear(k + 2, k1, 0), RGB_Gear(k + 2, k1, 1), RGB_Gear(k + 2, k1, 2)
     Else
      'glColor3f RGB_Gear(k, k1, 0), RGB_Gear(k, k1, 1), RGB_Gear(k, k1, 2) '225, 240, 255'100 / 255, 180 / 255, 255 / 255
    '  Call glColor3f(100 / 255, 180 / 255, 255 / 255)
     End If
     
     For jn = 0 To 0 ' Orientazione normale
      For j = 1 To (HGp.imXg - 1) ' Lungo fw
       For i = 1 + jn To HGp.inXg - 1 - jn ' Lungo R
        
        id(0 * (-1) ^ jn + 3 * jn) = (i - 1) + (k - 1) * (HGp.inXg) + (j - 1) * HGp.inXg * 2
        id(1 * (-1) ^ jn + 3 * jn) = (i - 1) + (k - 1) * (HGp.inXg) + (j) * HGp.inXg * 2
        id(2 * (-1) ^ jn + 3 * jn) = (i) + (k - 1) * (HGp.inXg) + (j) * HGp.inXg * 2
        id(3 * (-1) ^ jn + 3 * jn) = (i) + (k - 1) * (HGp.inXg) + (j - 1) * HGp.inXg * 2
     
        glBegin GLMode
         glVertex3f Teeth(L).Vert_0(id(0)).X, Teeth(L).Vert_0(id(0)).Y, Teeth(L).Vert_0(id(0)).Z: glTexCoord2f 0, 0
         glVertex3f Teeth(L).Vert_1(id(1)).X, Teeth(L).Vert_1(id(1)).Y, Teeth(L).Vert_1(id(1)).Z: glTexCoord2f 0, Teeth(L).Y_Texture
         glVertex3f Teeth(L).Vert_2(id(2)).X, Teeth(L).Vert_2(id(2)).Y, Teeth(L).Vert_2(id(2)).Z: glTexCoord2f Teeth(L).X_Texture, Teeth(L).Y_Texture
         glVertex3f Teeth(L).Vert_3(id(3)).X, Teeth(L).Vert_3(id(3)).Y, Teeth(L).Vert_3(id(3)).Z: glTexCoord2f Teeth(L).X_Texture, 0
        glEnd
   
       Next i
      Next j
     Next jn
    Next k
   Next k1
  Next L
 glPopMatrix
Next iC
If (Not glIsEnabled(GL_CULL_FACE)) Then glEnable (GL_CULL_FACE)
glLineWidth (1)

End Sub
