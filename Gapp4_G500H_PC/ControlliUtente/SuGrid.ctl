VERSION 5.00
Begin VB.UserControl SuGrid 
   ClientHeight    =   4020
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   6228
   LockControls    =   -1  'True
   ScaleHeight     =   4020
   ScaleWidth      =   6228
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   2385
      Left            =   168
      ScaleHeight     =   2364
      ScaleWidth      =   5052
      TabIndex        =   0
      Top             =   345
      Width           =   5076
      Begin VB.VScrollBar VScroll1 
         Height          =   2145
         LargeChange     =   10
         Left            =   4305
         Max             =   0
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   90
         Width           =   255
      End
   End
End
Attribute VB_Name = "SuGrid"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False

Option Explicit

Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, ByVal lParam As Any) As Long

Private Declare Function GetKeyState Lib "user32" (ByVal nVirtKey As Long) As Integer
Const VK_TAB = &H9

Const LB_GETITEMHEIGHT = &H1A1
Const LB_SETITEMHEIGHT = &H1A0
Const CB_SETITEMHEIGHT = &H153

Private Resizing As Boolean

Public ResizeCol As Boolean

Private UBRighe   As Integer
Private UBColonne As Integer

Public Celle     As New Collection

Private g_Height  As Single
Private g_RigPag  As Integer
Private m_Font    As StdFont

Public Event Selezionata(Key As String, VARIABILE As String)
Public Event EditEnabling(Key As String, CodiceASCII As Integer)
'Public Event EditDisEnabling(Key As String, Variabile As String, NewValue As String)
Public Event GridAction(Key As String, VARIABILE As String, Action As String)
Public Event ListItemSelected(Value As String, VARIABILE As String)
Public Event VarChanged(Variabili As String)
'Public Event NewWidth(Larghezze As String)
Public Event DblClick(Key As String)

Private m_bHasFocus     As Boolean
Private m_sNewItemWidth As String
Private LastSelected    As String
Private LastListValue   As String

Public Sub DISEGNA_NEL_GRUPPO(ByVal NOME_DISEGNO As String)
'
Dim X1   As Double
Dim Y1   As Double
Dim X2   As Double
Dim Y2   As Double
Dim i    As Integer
Dim j    As Integer
Dim k    As Integer
Dim stmp As String
'
On Error Resume Next
  '
  Select Case NOME_DISEGNO
    '
    Case "OEM1_LAVORO72"
      'AREA GRAFICA
      Picture1.DrawWidth = 2
      Picture1.Line (100, Picture1.Height / 2)-(Picture1.Width - 100, Picture1.Height - 100), QBColor(14), BF
      Picture1.Line (100, Picture1.Height / 2)-(Picture1.Width - 100, Picture1.Height - 100), QBColor(0), B
      Picture1.DrawWidth = 1
      'DICHIARAZIONI
      Dim LM     As Double
      Dim LL     As Double
      Dim HH     As Double
      Dim dx     As Double
      Dim Dy     As Double
      Dim SCx    As Double
      Dim SCy    As Double
      Dim AA(10) As Double
      Dim ZZ(10) As Double
      Dim DD(10) As Double
      Dim EE(10) As Double
      Dim sL(10) As Double
      Dim SN(10) As Integer
      Dim SZ(10) As Integer
      Dim UU(10) As Integer
      Dim CD(10) As Integer
      Dim TD(10) As Integer
      Dim DDmax  As Double
      Dim ZZmin  As Double
      Dim ZZmax  As Double
      Dim OX     As Double
      Dim OY     As Double
      Dim lt     As Double
      Dim ht     As Double
      Dim co     As Long
      Dim da     As Double
      'LARGHEZZA MOLA
      LM = LeggiLarghezzaMolaRDE
      If (LM <= 0) Then LM = 50
      'ALTEZZA RETTANGOLO
      HH = Picture1.Height / 4
      'RIFERIMENTO ASSIALE
      dx = 500:  Dy = 3 * HH
      Picture1.Line (dx, Dy)-(dx + Picture1.Width - 2 * dx, Dy), QBColor(0)
      'ACQUISIZIONE DIAMETRI
      DDmax = -10000: ZZmax = -10000: ZZmin = 10000
      For i = 1 To 10
        AA(i) = LEP("RDS[0," & Format$((i - 1) * 10 + 0, "#0") & "]") 'ABILITAZIONE
        If (AA(i) <> 0) Then AA(i) = 1
        ZZ(i) = LEP("RDS[0," & Format$((i - 1) * 10 + 1, "#0") & "]") 'INIZIO LAVORO
        DD(i) = LEP("RDS[0," & Format$((i - 1) * 10 + 2, "#0") & "]") 'DIAMETRO FINALE
        EE(i) = LEP("RDS[0," & Format$((i - 1) * 10 + 3, "#0") & "]") 'CORREZIONE DIAMETRO
        sL(i) = LEP("RDS[0," & Format$((i - 1) * 10 + 4, "#0") & "]") 'INCREMENTO LATERALE
        SN(i) = LEP("RDS[0," & Format$((i - 1) * 10 + 5, "#0") & "]") 'NUMERO SPOSTAMENTI
        SZ(i) = LEP("RDS[0," & Format$((i - 1) * 10 + 6, "#0") & "]") 'SPOSTAMENTO
        UU(i) = LEP("RDS[0," & Format$((i - 1) * 10 + 7, "#0") & "]") 'INDICE LUNETTA
        CD(i) = LEP("RDS[0," & Format$((i - 1) * 10 + 8, "#0") & "]") 'CONTROLLO DIAMETRALE
        TD(i) = LEP("RDS[0," & Format$((i - 1) * 10 + 9, "#0") & "]") 'SENSORE DI TOCCO
        If ((DD(i) + EE(i)) * 2 >= DDmax) Then DDmax = (DD(i) + EE(i)) * 2
        If (ZZ(i) >= ZZmax) Then ZZmax = ZZ(i)
        If (ZZ(i) <= ZZmin) Then ZZmin = ZZ(i)
      Next i
      'CALCOLO SCALA Y
      SCy = HH / DDmax
      If (SCy > 1) Then SCy = Int(SCy)
      'CALCOLO SCALA X
      SCx = (Picture1.Width - 2 * dx) / (ZZmax + LM - ZZmin)
      If (SCx > 1) Then SCx = Int(SCx)
      'COORDINAMENTO DELLE SCALE
      If (SCx > SCy) Then SCx = SCy Else SCy = SCx
      If (LM * SCx < 4 * Picture1.TextWidth("X")) Then LM = 4 * Picture1.TextWidth("X") / SCx
      'DISEGNO POSIZIONAR
      i = val(LEP("CHKPOS_ON"))
      If (i = 1) Then
        lt = Picture1.TextWidth("X") / 2
        ht = Picture1.TextHeight("X") / 2
        X1 = dx:             X2 = dx + 4 * lt
        Y1 = Dy - 1 * ht: Y2 = Dy + 1 * ht
        'TRIANGOLO SX->DX
        Picture1.DrawWidth = 1
        da = Atn(lt / ht) / 50
        For j = -25 To 25
          Picture1.Line (X1 + lt, Y1 - 2 * ht * Tan(da * j))-(X1, Y1), QBColor(12)
        Next j
        'SCANALATURA
        Picture1.DrawWidth = 2
        Picture1.Line (X1, Y1)-(X1 + 2 * lt, Y1), QBColor(12)
        Picture1.Line (X1 + 2 * lt, Y1)-(X1 + 2 * lt, Y1 - 4 * lt), QBColor(12)
        Picture1.DrawWidth = 3
        Picture1.Line (X1, Y1)-(X1, Y2), QBColor(9)
        Picture1.Line (X2, Y1)-(X2, Y2), QBColor(9)
        Picture1.Line (X1, Y2)-(X2, Y2), QBColor(9)
        Picture1.DrawWidth = 1
      End If
      'DISEGNO DEI RETTANGOLI
      Picture1.Font.Bold = True
      For i = 1 To 10
        If (AA(i) = 1) Then
          LL = LM + SN(i) * SZ(i)
          X1 = dx + ZZ(i) * SCx:                X2 = dx + (ZZ(i) + LL) * SCx
          Y1 = Dy - SCy * (EE(i) / 2 + DD(i) / 2): Y2 = Dy + SCy * (EE(i) / 2 + DD(i) / 2)
          If (CD(i) > 0) Then
            'DIAMETRO X CONTROLLO DIAMETRALE
            co = QBColor(0)
            Picture1.DrawWidth = 3
            Picture1.Line (X1, Y1)-(X2, Y2), QBColor(15), BF
            Picture1.Line (X1, Y1)-(X2, Y2), co, B
            Picture1.DrawWidth = 1
            lt = Picture1.TextWidth("X") / 2
            ht = SCy * (EE(i) / 2 + DD(i) / 2)
            OX = dx + (ZZ(i) + LL / 2) * SCx + lt / 2
            OY = Y2
            da = Atn(lt / ht) / 50
            For j = -25 To 25
              Picture1.Line (OX - lt / 2 - 2 * ht * Tan(da * j), OY - 2.25 * ht)-(OX - lt / 2, OY - 2 * ht), co
            Next j
            For j = -25 To 25
              Picture1.Line (OX - lt / 2 - 2 * ht * Tan(da * j), OY + 0.25 * ht)-(OX - lt / 2, OY), co
            Next j
            stmp = " ref " & frmt(Abs(DD(i)), 3)
            Picture1.CurrentX = OX - Picture1.TextWidth(stmp) / 2
            Picture1.CurrentY = OY + 0.25 * ht
            Picture1.Print stmp
          Else
            'DIAMETRO
            Picture1.DrawWidth = 2
            Picture1.Line (X1, Y1)-(X2, Y2), QBColor(15), BF
            Picture1.Line (X1, Y1)-(X2, Y2), QBColor(12), B
            Picture1.DrawWidth = 1
          End If
          'SPALLAMENTO
          If (sL(i) > 0) Then
            Picture1.DrawWidth = 2
            Picture1.Line (X2 + sL(i) * SCx, Y1)-(X2 + sL(i) * SCx, Y2 - HH), QBColor(0)
            Picture1.Line (X2, Y1)-(X2 + sL(i) * SCx, Y1), QBColor(0)
            lt = Picture1.TextWidth("X") / 2
            ht = Picture1.TextHeight("X") / 2
            Picture1.Line (X2 + sL(i) * SCx - 3 * lt, Y2 - HH)-(X2 + sL(i) * SCx, Y2 - HH), QBColor(12)
            Picture1.DrawWidth = 1
            'TRIANGOLO SX->DX
            da = Atn(lt / ht) / 50
            For j = -25 To 25
              Picture1.Line (X2 + sL(i) * SCx - lt, Y2 - HH - 2 * ht * Tan(da * j))-(X2 + sL(i) * SCx, Y2 - HH), QBColor(12)
            Next j
          End If
          If (sL(i) < 0) Then
            Picture1.DrawWidth = 2
            Picture1.Line (X1 + sL(i) * SCx, Y1)-(X1 + sL(i) * SCx, Y2 - HH), QBColor(0)
            Picture1.Line (X1, Y1)-(X1 + sL(i) * SCx, Y1), QBColor(0)
            lt = Picture1.TextWidth("X") / 2
            ht = Picture1.TextHeight("X") / 2
            Picture1.Line (X1 + sL(i) * SCx + 3 * lt, Y2 - HH)-(X1 + sL(i) * SCx, Y2 - HH), QBColor(12)
            Picture1.DrawWidth = 1
            'TRIANGOLO DX->SX
            da = Atn(lt / ht) / 50
            For j = -25 To 25
              Picture1.Line (X1 + sL(i) * SCx + lt, Y2 - HH - 2 * ht * Tan(da * j))-(X1 + sL(i) * SCx, Y2 - HH), QBColor(12)
            Next j
          End If
          'CALCOLO RIGHE IN FUNZIONE DELLA POSIZIONE INIZIALE
          k = 0
          For j = 1 To i
            If (Abs(ZZ(j) - ZZ(i)) <= LL) And (AA(j) = 1) Then k = k + 1
          Next j
          'INDICE
          stmp = " D" & Format$(i, "#0") & " "
          Picture1.CurrentX = X2 - LL / 2 * SCx - Picture1.TextWidth(stmp) / 2
          Picture1.CurrentY = Y1 - k * Picture1.TextHeight(stmp) * 1.2
          Picture1.Print stmp
          'FRECCIA
          If (k > 1) Then
            'FRECCIA SU
            If (sL(i) < 0) Then
            OX = X2 - LL / 2 * SCx + Picture1.TextWidth(stmp) / 2
            Else
            OX = X2 - LL / 2 * SCx - Picture1.TextWidth(stmp) / 2
            End If
            OY = Y1
            lt = Picture1.TextWidth("X") / 2
            ht = Picture1.TextHeight("X")
            co = QBColor(12)
            OY = OY - ht / 2
            Picture1.DrawWidth = 1
            Picture1.Line (OX, OY)-(OX - lt, OY - 1 * ht), co, BF
            da = Atn(lt / ht) / 50
            For j = -25 To 25
              Picture1.Line (OX - lt / 2 - 2 * ht * Tan(da * j), OY - 1 * ht)-(OX - lt / 2, OY - 1.5 * ht), co
            Next j
          End If
          'CHIUSURA LUNETTA
          If (UU(i) > 0) Then
            stmp = " Cls " & Format$(Abs(UU(i)), "0 ")
            Picture1.CurrentX = X2 - LL / 2 * SCx - Picture1.TextWidth(stmp) / 2
            Picture1.CurrentY = Y2
            Picture1.Print stmp
          End If
          'APERTURA LUNETTA
          If (UU(i) < 0) Then
            stmp = "  Opn " & Format$(Abs(UU(i)), "0 ")
            Picture1.CurrentX = X2 - LL / 2 * SCx - Picture1.TextWidth(stmp) / 2
            Picture1.CurrentY = Y2 + Picture1.TextHeight(stmp) * 1.2
            Picture1.Print stmp
            'FRECCIA GIU'
            OX = X2 - LL / 2 * SCx - Picture1.TextWidth(stmp) / 2
            OY = Y2
            lt = Picture1.TextWidth("X") / 2
            ht = Picture1.TextHeight("X")
            co = QBColor(0)
            OY = OY + ht / 2
            Picture1.DrawWidth = 1
            Picture1.Line (OX, OY)-(OX - lt, OY + 1 * ht), co, BF
            da = Atn(lt / ht) / 50
            For j = -25 To 25
              Picture1.Line (OX - lt / 2 - 2 * ht * Tan(da * j), OY + 1 * ht)-(OX - lt / 2, OY + 1.5 * ht), co
            Next j
          End If
          'SENSORE DI TOCCO
          If (TD(i) > 0) Then
            'FRECCIA SU
            OX = X1
            OY = Y1 - Picture1.TextHeight("X") * 1.2
            ht = SCy * (EE(i) / 2 + DD(i) / 2)
            co = QBColor(9)
            OY = 2 * HH
            stmp = g_chOemPATH & "\Gapp4\images\ACUSTICA.bmp"
            If (Dir$(stmp) <> "") Then
              Call Picture1.PaintPicture(LoadPicture(stmp), OX, 2 * HH)
            Else
              Picture1.DrawWidth = 2
              Picture1.Line (OX, OY)-(OX + LM * SCx, OY + 1 * ht), QBColor(15), BF
              Picture1.Line (OX, OY)-(OX + LM * SCx, OY + 1 * ht), co, B
              stmp = " AE "
              Picture1.CurrentX = OX + (LM * SCx - Picture1.TextWidth(stmp)) / 2
              Picture1.CurrentY = OY
              Picture1.Print stmp
            End If
            Picture1.DrawWidth = 1
          End If
        End If
      Next i
      Picture1.DrawWidth = 1
      Picture1.Font.Bold = False
      '
  End Select
  '
End Sub

Public Sub CROCE(tstr As String)
'
Dim i     As Integer
Dim k     As Integer
Dim j     As Integer
Dim tstr1 As String
'
On Error Resume Next
  '
  i = Left(tstr, InStr(1, tstr, ",") - 1)
  j = Right(tstr, Len(tstr) - InStr(1, tstr, ","))
  If Not CellaEsiste(tstr) Then Exit Sub
  '
  tstr1 = ""
  For k = i - 1 To 1 Step -1
    tstr1 = k & "," & j
    If CellaEsiste(tstr1) Then
      GetCella(tstr1).RowSel = True
    Else
      Exit For
    End If
  Next k
  '
  tstr1 = ""
  For k = i + 1 To UBRighe
    tstr1 = k & "," & j
    If CellaEsiste(tstr1) Then
      GetCella(tstr1).RowSel = True
    Else
      Exit For
    End If
  Next k
  '
  tstr1 = ""
  For k = j - 1 To 0 Step -1
    tstr1 = i & "," & k
    If CellaEsiste(tstr1) Then
      GetCella(tstr1).RowSel = True
    Else
      Exit For
    End If
  Next k
  '
  tstr1 = ""
  For k = j + 1 To UBColonne
    tstr1 = i & "," & k
    If CellaEsiste(tstr1) Then
      GetCella(tstr1).RowSel = True
    Else
      Exit For
    End If
  Next k
  
End Sub

Public Property Get CellHeight() As Single
'
On Error Resume Next
  '
  CellHeight = g_Height

End Property

Public Property Let CellHeight(ByVal fCellHeight As Single)
'
On Error Resume Next
  '
  g_Height = fCellHeight
  If (g_Height = 0) Then
    g_RigPag = val(GetInfo("Configurazione", "RIGHE", Path_LAVORAZIONE_INI))
    If (g_RigPag = 0) Then g_RigPag = 24
    g_Height = CInt(Picture1.ScaleHeight / g_RigPag)
  Else
    g_RigPag = CInt(Picture1.ScaleHeight / g_Height)
    g_Height = CInt(Picture1.ScaleHeight / g_RigPag)
  End If
  Call UserControl.PropertyChanged("CellHeight")

End Property

Public Property Get ScaleHeight()
'
On Error Resume Next
  '
  ScaleHeight = Picture1.ScaleHeight

End Property

Public Property Get ScaleTop()
'
On Error Resume Next
  '
  ScaleTop = Picture1.ScaleTop

End Property

Public Sub EVIDENZIA_COLONNA()
'
Dim tstr  As String
Dim i     As Integer
Dim k     As Integer
Dim j     As Integer
Dim Width As Single
'
On Error Resume Next
  '
  tstr = Selected
  '
  i = Left(tstr, InStr(1, tstr, ",") - 1)
  j = Right(tstr, Len(tstr) - InStr(1, tstr, ","))
  If Not CellaEsiste(tstr) Then Exit Sub
  '
  Width = Celle(tstr).Width
  tstr = ""
  For k = i To 1 Step -1
    tstr = k & "," & j
    If CellaEsiste(tstr) Then
      If (Not GetCella(tstr).Bold) And (GetCella(tstr).Width = Width) Then
        GetCella(tstr).RowSel = True
      Else
        Exit For
      End If
    Else
      Exit For
    End If
  Next k
  For k = i To UBRighe
    tstr = k & "," & j
    If CellaEsiste(tstr) Then
      If (Not GetCella(tstr).Bold) And (GetCella(tstr).Width = Width) Then
        GetCella(tstr).RowSel = True
      Else
        Exit For
      End If
    Else
      Exit For
    End If
  Next k
  '
End Sub

Private Sub COPIA_COLONNA(Optional Back As Boolean = False)

Dim tstr    As String
Dim tstr2   As String
Dim i       As Integer
Dim k       As Integer
Dim j       As Integer
Dim chgVAR  As String
  '
On Error Resume Next
  '
  tstr = Selected
  i = Left(tstr, InStr(1, tstr, ",") - 1)
  j = Right(tstr, Len(tstr) - InStr(1, tstr, ","))
  If (Not CellaSelezionabile(i & "," & j + 1)) And (Not Back) Then Exit Sub
  If (Not CellaSelezionabile(i & "," & j - 1)) And (Back) Then Exit Sub
  chgVAR = "": tstr = ""
  For k = 0 To UBRighe
    tstr = k & "," & j
    If Back Then
      tstr2 = k & "," & j - 1
    Else
      tstr2 = k & "," & j + 1
    End If
    GetCella(tstr2).Value = LEP(GetCella(tstr).VARIABILE)
    If (chgVAR <> "") Then chgVAR = chgVAR & ";"
    chgVAR = chgVAR & GetCella(tstr2).VARIABILE
  Next k
  If Back Then tstr = "COPY (" & j & ") -> (" & j - 1 & ")" Else tstr = "COPY (" & j & ") -> (" & j + 1 & ")"
  If (MsgBox(tstr, vbYesNo) = vbYes) Then
    If (chgVAR <> "") Then
      WRITE_DIALOG "copying......"
      RaiseEvent VarChanged(chgVAR)
    Else
      WRITE_DIALOG "REPEAT COMMAD: Error during copy operation!"
      RaiseEvent VarChanged("SALTA")
    End If
  Else
      WRITE_DIALOG "Operation aborted by user!"
      RaiseEvent VarChanged("SALTA")
  End If
  
End Sub

Public Sub STAMPA_GRID(Top As Single, Left As Single, Width As Single)
'
Dim XScala  As Single
Dim YScala  As Single
Dim cell    As Cella
Dim tTop    As Single
'
On Error Resume Next
  '
  If VScroll1.Visible Then
    XScala = Width / VScroll1.Left
  Else
    XScala = Width / UserControl.ScaleWidth
  End If
  YScala = (Printer.TextHeight("X") * 1.07) / GetCella("0,0").Height
  tTop = Top
  For Each cell In Celle
    If (tTop + cell.Top * YScala > 270) Then
      Printer.NewPage
      tTop = Top - cell.Top * YScala
    End If
    Call cell.STAMPA_CELLA(tTop, Left, XScala, YScala)
  Next
  '
End Sub

Public Sub AutoSize()
'
On Error Resume Next
  '
  Height = g_Height * (UBRighe + 1)

End Sub

Private Sub Picture1_DblClick()
'
Dim tstr As String
'
On Error Resume Next
  '
  tstr = Selected
  RaiseEvent DblClick(tstr)

End Sub

Private Sub Picture1_KeyPress(KeyAscii As Integer)
'
Dim tstr As String
'
On Error Resume Next
  '
  'RICHIAMO DEI CONTROLLI DI EDITING
  If ((KeyAscii >= Asc("0")) And (KeyAscii <= Asc("z"))) _
    Or (KeyAscii = Asc("-")) Or (KeyAscii = Asc("(")) _
    Or (KeyAscii = Asc(",")) Or (KeyAscii = Asc(".")) Or (KeyAscii = 32) Then
      tstr = Selected
      RaiseEvent EditEnabling(tstr, KeyAscii)
  End If
  '
End Sub

Private Sub UserControl_Terminate()
'
On Error Resume Next
  '
  Set m_Font = Nothing

If (hndHGkinetic <> 0) Then HGkinetic_cmdCloseWindow

End Sub

Public Function Clear()
'
Dim cell As Cella
'
On Error Resume Next
  '
  While (Celle.count > 0)
    Call Celle.Remove(1)
  Wend
  Picture1.Cls
  UBRighe = -1
  VScroll1.MIN = 0
  VScroll1.Max = 0
  VScroll1 = 0

End Function

Public Property Get HasFocus() As Boolean
'
On Error Resume Next
  '
  HasFocus = m_bHasFocus

End Property

Public Property Get ListCount() As Integer
'
On Error Resume Next
  '
  ListCount = UBRighe + 1

End Property

Function GetCella(Key As String) As Cella
'
On Error Resume Next
  '
  If CellaEsiste(Key) Then Set GetCella = Celle(Key)

End Function

Function GetKeyCellaByVar(VARIABILE As String) As String
'
Dim cell As New Cella
'
On Error Resume Next
  '
  GetKeyCellaByVar = ""
  For Each cell In Celle
    If Trim(UCase(cell.VARIABILE)) = Trim(UCase(VARIABILE)) Then
      GetKeyCellaByVar = cell.Key
    End If
  Next
  Set cell = Nothing

End Function

Function GetCellaByVar(VARIABILE As String) As Cella
'
Dim cell As New Cella
'
On Error Resume Next
  '
  Set GetCellaByVar = Nothing
  For Each cell In Celle
    If Trim(UCase(cell.VARIABILE)) = Trim(UCase(VARIABILE)) Then
      Set GetCellaByVar = cell
    End If
  Next
  Set cell = Nothing

End Function

Public Function Selected(Optional SelLast As Boolean = True) As String
'
Dim i As Integer
Dim j As Integer
'
On Error Resume Next
  '
  For j = 0 To UBColonne
    For i = 0 To UBRighe
      If CellaEsiste(i & "," & j) Then
        If Celle.Item(i & "," & j).Selected Then
          Selected = i & "," & j
          LastSelected = Selected
          'Write_Dialog "Selected: " & Selected & " UBColonne: " & UBColonne & " UBRighe: " & UBRighe & " "
          Exit Function
        End If
      End If
    Next i
  Next j
  If SelLast Then Selected = LastSelected
  
End Function

Private Sub SELEZIONA_COLONNA(Key As String)

Dim tArr()  As String
Dim i       As String
Dim j       As Integer
Dim k       As Integer
Dim tKey    As String
'
On Error Resume Next
  '
  If (Key = "") Then Exit Sub
  '
  tArr = Split(Key, ",")
  i = tArr(0):  k = tArr(1)
  '
  For j = k To 0 Step -1
    tKey = i & "," & j
    If CellaEsiste(tKey) Then
      Celle(tKey).RowSel = True
      If (tKey = Key) Then
        If Not Celle(tKey).Selected Then RaiseEvent Selezionata(tKey, Celle(tKey).VARIABILE)
        Celle(tKey).Selected = True
      End If
    End If
  Next j
  For j = k + 1 To UBColonne
    tKey = i & "," & j
    If CellaEsiste(tKey) Then
      Celle(tKey).RowSel = True
      If (tKey = Key) Then
        If Not Celle(tKey).Selected Then RaiseEvent Selezionata(tKey, Celle(tKey).VARIABILE)
        Celle(tKey).Selected = True
      End If
    End If
  Next j

End Sub

Function CellaByPoint(X As Single, Y As Single) As String

Dim cell As Cella
'
On Error Resume Next
  '
  For Each cell In Celle
    If (Y > cell.Top) And (Y < (cell.Top + cell.Height)) And (X > cell.Left) And (X < (cell.Left + cell.Width)) Then
      CellaByPoint = cell.Key
      Set cell = Nothing
      Exit Function
    End If
  Next
  Set cell = Nothing

End Function

Sub Redraw()

Dim cell As Cella
'
On Error Resume Next
  '
  For Each cell In Celle
    Call cell.Redraw
    Set cell = Nothing
  Next
  Set cell = Nothing

End Sub

Public Sub SelFirst()
'
Dim tstr As String
'
On Error Resume Next
  '
  tstr = Selected
  Call SELEZIONA_COLONNA(CellaFirst)
  Call EVIDENZIA_COLONNA

End Sub

Private Sub Picture1_KeyUP(KeyCode As Integer, Shift As Integer)
'
Dim sposizione  As String
Dim vposizione  As Integer
Dim tstr        As String
Dim tArr()      As String
Dim i           As Integer
'
On Error Resume Next
  '
  'MsgBox "KeyCode " & KeyCode & " Shift " & Shift
  '
  tstr = Selected
  If (tstr = "") Then
    tstr = CellaFirst
    If (tstr <> "") Then
      Call SELEZIONA_COLONNA(tstr)
      Call EVIDENZIA_COLONNA
    End If
    Exit Sub
  End If
  '
  If (Shift = 2) And (KeyCode <> 18) Then 'Ctrl + ....
    Select Case KeyCode
      Case vbKeyUp:     RaiseEvent GridAction(tstr, Celle(tstr).VARIABILE, "MSGUP")
      Case vbKeyDown:   RaiseEvent GridAction(tstr, Celle(tstr).VARIABILE, "MSGDOWN")
      Case 12:          RaiseEvent GridAction(tstr, Celle(tstr).VARIABILE, "LIMITI") 'Tasto select Siemens
      Case 123:         RaiseEvent GridAction(tstr, Celle(tstr).VARIABILE, "INFO")   'Tasto INFO Siemens
      Case vbKeyRight:  Call COPIA_COLONNA
      Case vbKeyLeft:   Call COPIA_COLONNA(True)
      Case vbKeyDelete: RaiseEvent GridAction(tstr, Celle(tstr).VARIABILE, "CANCEL")
      Case 36, vbKeyP:  RaiseEvent GridAction(tstr, Celle(tstr).VARIABILE, "PARAMETRI")
      'NEI CASI IN CUI IL PORTATILE NON DISPONE DEL TASTIERINO NUMERICO
      Case 49:          RaiseEvent GridAction(tstr, Celle(tstr).VARIABILE, "NOME")    'CTRL+1
      Case 50:          RaiseEvent GridAction(tstr, Celle(tstr).VARIABILE, "LIMITI")  'CTRL+2
      Case 51:          RaiseEvent GridAction(tstr, Celle(tstr).VARIABILE, "DEFAULT") 'CTRL+3
      Case 52:          RaiseEvent GridAction(tstr, Celle(tstr).VARIABILE, "TIPO")    'CTRL+4
      'CASO ROTORI CON GRUPPI CON TANTI PARAMETRI
      '
      Case vbKeyPageUp
        sposizione = Split(tstr, ",")(0)
        vposizione = CInt(sposizione) - 50
        If (vposizione <= 1) Then vposizione = 1
        tstr = Format$(vposizione, "#######0") & "," & Split(tstr, ",")(1)
        If Not CellaSelezionabile(tstr) Then tstr = CellaDown(tstr)
        Call SELEZIONA_COLONNA(tstr)
        Call EVIDENZIA_COLONNA
      '
      Case vbKeyPageDown
        sposizione = Split(tstr, ",")(0)
        vposizione = CInt(sposizione) + 50
        If (vposizione >= UBRighe) Then vposizione = UBRighe
        tstr = Format$(vposizione, "#######0") & "," & Split(tstr, ",")(1)
        Call SELEZIONA_COLONNA(tstr)
        Call EVIDENZIA_COLONNA
      '
    End Select
    Call CROCE(tstr)
    Exit Sub
  End If
  '
  If (Shift = 6) And (KeyCode <> 18) Then 'Ctrl + Alt + ....
    Select Case KeyCode
      Case 12:          RaiseEvent GridAction(tstr, Celle(tstr).VARIABILE, "NOME") 'Tasto select Siemens
    End Select
    Call CROCE(tstr)
    Exit Sub
  End If
  '
  Select Case KeyCode
      '
    Case vbKeyHome
      vposizione = 1
      tstr = Format$(vposizione, "#######0") & "," & Split(tstr, ",")(1)
      If Not CellaSelezionabile(tstr) Then tstr = CellaDown(tstr)
      Call SELEZIONA_COLONNA(tstr)
      Call EVIDENZIA_COLONNA
      '
    Case vbKeyEnd
      vposizione = UBRighe
      tstr = Format$(vposizione, "#######0") & "," & Split(tstr, ",")(1)
      Call SELEZIONA_COLONNA(tstr)
      Call EVIDENZIA_COLONNA
      '
    Case vbKeyPageUp
      sposizione = Split(tstr, ",")(0)
      vposizione = CInt(sposizione) - 20
      If (vposizione <= 1) Then vposizione = 1
      tstr = Format$(vposizione, "#######0") & "," & Split(tstr, ",")(1)
      If Not CellaSelezionabile(tstr) Then tstr = CellaDown(tstr)
      Call SELEZIONA_COLONNA(tstr)
      Call EVIDENZIA_COLONNA
      '
    Case vbKeyPageDown
      sposizione = Split(tstr, ",")(0)
      vposizione = CInt(sposizione) + 20
      If (vposizione >= UBRighe) Then vposizione = UBRighe
      tstr = Format$(vposizione, "#######0") & "," & Split(tstr, ",")(1)
      Call SELEZIONA_COLONNA(tstr)
      Call EVIDENZIA_COLONNA
      '
    Case vbKeyDown
      tstr = CellaDown(tstr)
      Call SELEZIONA_COLONNA(tstr)
      Call EVIDENZIA_COLONNA
      '
    Case vbKeyUp
      tstr = CellaUP(tstr)
      Call SELEZIONA_COLONNA(tstr)
      Call EVIDENZIA_COLONNA
      If (CellaUP(tstr) <> "") Then
        If CInt(Split(CellaUP(tstr), ",")(0)) > CInt(Split(tstr, ",")(0)) Then VScroll1 = VScroll1.MIN
      End If
      '
    Case vbKeyRight
      Call SELEZIONA_COLONNA(CellaDx(tstr))
      Call EVIDENZIA_COLONNA
      '
    Case vbKeyLeft
      Call SELEZIONA_COLONNA(CellaSx(tstr))
      Call EVIDENZIA_COLONNA
      '
    Case vbKeyReturn
      If Trim(GetCella(tstr).BoolValues = "") Then
        RaiseEvent EditEnabling(tstr, vbKeyReturn)
      Else
        Call SELEZIONA_COLONNA(CellaDown(tstr))
        Call EVIDENZIA_COLONNA
      End If
      '
    Case 12 'Tasto Select Siemens
      If Trim(GetCella(tstr).BoolValues <> "") Then
        RaiseEvent EditEnabling(tstr, 12)
      End If
      '
    Case vbKeyDelete
        RaiseEvent EditEnabling(tstr, vbKeyDelete)
      
  End Select
  
End Sub

Function CellaEsiste(Key As String) As Boolean

Dim cell As New Cella

On Error Resume Next
  
  Set cell = Celle(Key)
  If Err.Number = 0 Then
    CellaEsiste = True
  Else
    CellaEsiste = False
  End If
  Set cell = Nothing

End Function

Function CellaSelezionabile(Key As String) As Boolean

Dim cell As New Cella

On Error Resume Next
  
  If (Key <> "") Then
    Set cell = Celle(Key)
    If (Err.Number = 0) Then
      CellaSelezionabile = cell.Selectable
    Else
      CellaSelezionabile = False
    End If
  Else
      CellaSelezionabile = False
  End If
  Set cell = Nothing

End Function

Function CellaFirst() As String
  
Dim i     As Integer
Dim j     As Integer
Dim tstr  As String
'
On Error Resume Next
  '
  For i = 0 To UBRighe
    For j = 0 To UBColonne
        tstr = i & "," & j
        If CellaSelezionabile(tstr) Then
          CellaFirst = tstr
          Exit Function
        End If
    Next j
  Next i

End Function

Function CellaDown(Key As String) As String
  
Dim i       As Integer
Dim j       As Integer
Dim tArr()  As String
Dim tKey    As String
Dim cell    As Cella
Dim tX      As Single
Dim tY      As Single
'
'On Error Resume Next
  '
  If (Key = "") Then CellaDown = "": Exit Function
  '
  Set cell = Celle(Key)
  tX = cell.Left + 1
  tY = cell.Top + cell.Height * 3 / 2
  tKey = CellaByPoint(tX, tY)
  While (tKey <> Key) And (Not CellaSelezionabile(tKey))
    tY = tY + cell.Height
    If (tY > (cell.Height * (UBRighe + 1))) Then
      VScroll1 = VScroll1.MIN
      tY = cell.Height / 2
    End If
    tKey = CellaByPoint(tX, tY)
  Wend
  CellaDown = tKey
  Set cell = Nothing
  '
End Function

Function CellaUP(Key As String) As String
  
Dim i       As Integer
Dim j       As Integer
Dim tArr()  As String
Dim tKey    As String
Dim cell    As Cella
Dim tX      As Single
Dim tY      As Single
'
'On Error Resume Next
  '
  If (Key = "") Then CellaUP = "": Exit Function
  '
  Set cell = Celle(Key)
  tX = cell.Left + 1
  tY = cell.Top - cell.Height \ 2
  tKey = CellaByPoint(tX, tY)
  While (tKey <> Key) And Not CellaSelezionabile(tKey)
    tY = tY - cell.Height
    If (tY < 0) Then tY = cell.Height * UBRighe + 1
    tKey = CellaByPoint(tX, tY)
  Wend
  CellaUP = tKey
  Set cell = Nothing
  '
End Function

Function CellaDx(Key As String) As String
  
Dim i       As Integer
Dim j       As Integer
Dim tArr()  As String
Dim tKey    As String
'
'On Error Resume Next
  '
  tArr = Split(Key, ",")
  i = CInt(tArr(0))
  j = CInt(tArr(1)): j = (j + 1) Mod (UBColonne + 1)
  tKey = i & "," & j
  While Not CellaSelezionabile(tKey) And (tKey <> Key)
    j = (j + 1) Mod (UBColonne + 1)
    tKey = i & "," & j
  Wend
  CellaDx = tKey
  
End Function

Function CellaSx(Key As String) As String
  
Dim i       As Integer
Dim j       As Integer
Dim tArr()  As String
Dim tKey    As String
'
'On Error Resume Next
  '
  tArr = Split(Key, ",")
  i = CInt(tArr(0))
  j = CInt(tArr(1)): j = j - 1
  tKey = i & "," & j
  While Not CellaSelezionabile(tKey) And (tKey <> Key)
    j = j - 1
    If j < 0 Then j = UBColonne
    tKey = i & "," & j
  Wend
  CellaSx = tKey

End Function

Private Sub Picture1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
'
Dim tstr  As String
Dim i     As Integer
Dim k     As Integer
Dim j     As Integer
Dim cell  As Cella
'
On Error Resume Next
  '
  For Each cell In Celle
    cell.Selected = False
    cell.RowSel = False
  Next
  For Each cell In Celle
    If (Y > cell.vTop) And (Y < (cell.vTop + cell.Height)) _
   And (X > cell.Left) And (X < (cell.Left + cell.Width)) Then
      If cell.Selectable Then
        If Not cell.Selected Then tstr = cell.Key
        cell.Selected = True
      End If
    End If
  Next
  Set cell = Nothing
  Call CROCE(tstr)
  '
End Sub

Private Sub Picture1_Resize()
'
On Error Resume Next
  '
  VScroll1.Height = Picture1.ScaleHeight
  VScroll1.Top = 0
  VScroll1.Left = Picture1.ScaleWidth - VScroll1.Width

End Sub

Private Sub UserControl_EnterFocus()
'
Dim tstr  As String
'
On Error Resume Next
  '
  m_bHasFocus = True
  '
  tstr = Selected
  If Not CellaSelezionabile(tstr) Then tstr = ""
  If (tstr <> "") Then
    Call SELEZIONA_COLONNA(tstr)
  Else
    Call SELEZIONA_COLONNA(CellaFirst)
  End If

End Sub

Public Function hwnd() As Long
'
On Error Resume Next
  '
  hwnd = UserControl.hwnd

End Function

Private Sub UserControl_ExitFocus()

Dim tstr As String
'
On Error Resume Next
  '
  m_bHasFocus = False
  tstr = Selected
  If (tstr <> "") Then Call DESelRow(Selected)

End Sub
      
Private Sub UserControl_Initialize()
'
Dim i As Integer
'
On Error Resume Next
  '
  UBRighe = -1
  m_sNewItemWidth = "1;1;1"
  g_RigPag = 19
  
End Sub

Private Sub UserControl_Resize()
'
On Error Resume Next
  '
  Picture1.Top = 0
  Picture1.Left = 0
  Picture1.Height = UserControl.ScaleHeight
  Picture1.Width = UserControl.Width
  Call RowSExpand
  
End Sub

Private Sub VScroll1_Change()
'
Dim cell As Cella
'
On Error Resume Next
  '
  Picture1.Cls
  For Each cell In Celle
    Call cell.Redraw
  Next
  Set cell = Nothing

End Sub

Private Sub VScroll1_GotFocus()
'
On Error Resume Next
  '
  Picture1.SetFocus
  
End Sub

Private Sub VScroll1_Scroll()
'
On Error Resume Next
  '
  Call VScroll1_Change

End Sub

Sub RowSExpand()
'
Dim j         As Integer
Dim i         As Integer
Dim tstr      As String
Dim totWidth  As Single
Dim picWidth  As Single
Dim factor    As Single
'
On Error Resume Next
  '
  For j = 0 To UBRighe
    'CALCOLO LARGHEZZA COLONNA TOTALE
    totWidth = 0
    For i = 0 To UBColonne
      tstr = j & "," & i
      If CellaEsiste(tstr) Then
         totWidth = totWidth + Celle(tstr).Width
      End If
    Next i
    picWidth = VScroll1.Left
    If (totWidth <> 0) Then
      factor = picWidth / totWidth
      For i = 0 To UBColonne
        tstr = j & "," & i
        If CellaEsiste(tstr) Then
           Celle(tstr).Left = Celle(tstr).Left * factor
           Celle(tstr).Width = Celle(tstr).Width * factor
        End If
      Next i
    End If
  Next j
  
End Sub

Public Sub AggiornaVscroll()
'
On Error Resume Next
  '
  VScroll1.Max = UBRighe - g_RigPag + 1
  If (VScroll1.Max < 0) Then
    VScroll1 = VScroll1.MIN
    VScroll1.Max = 0
  End If
  'If (VScroll1.Max = 0) Then
  '  If (VScroll1.Left <> Picture1.Width) Then
  '    VScroll1.Left = Picture1.Width
  '    Call RowSExpand
  '  End If
  'Else
  '  If (VScroll1.Left <> (Picture1.Width - VScroll1.Width)) Then
  '    VScroll1.Left = Picture1.Width - VScroll1.Width
      Call RowSExpand
  '  End If
  'End If

End Sub

Public Sub AddItem(Valori As String, Optional title As Boolean = False, Optional BoolValues As String = "")
'
Dim tArr()    As String
Dim tarr2()   As String
Dim tWarr()   As String
Dim tWidth()  As Single
Dim tmp       As Single
Dim i         As Integer
Dim j         As Integer
Dim tstr      As String
Dim tLeft     As Single
Dim cell      As New Cella
'
On Error Resume Next
  '
  tLeft = 0
  UBColonne = 2
  UBRighe = UBRighe + 1
  VScroll1.Max = UBRighe - g_RigPag + 1
  '
  tArr() = Split(Valori, ";")
  i = UBound(tArr)
  If (i > UBColonne) Then UBColonne = i
  '
  If VScroll1.Max < 0 Then
    VScroll1 = VScroll1.MIN
    VScroll1.Max = 0
  End If
  If VScroll1.Max = 0 Then
    If VScroll1.Left <> Picture1.Width Then
      VScroll1.Left = Picture1.Width
      Call RowSExpand
    End If
  Else
    If VScroll1.Left <> (Picture1.Width - VScroll1.Width) Then
      VScroll1.Left = Picture1.Width - VScroll1.Width
      Call RowSExpand
    End If
  End If
  tstr = m_sNewItemWidth
  'Debug.Print UBRighe & " -> " & tstr
  If Right(tstr, 1) = ";" Then
    tstr = tstr & "0;0;0;0;0;"
  Else
    tstr = tstr & ";0;0;0;0;0;"
  End If
  tWarr = Split(tstr, ";")
  '
  ReDim Preserve tWarr(UBColonne)
  ReDim tWidth(UBColonne)
  '
  tmp = 0
  For j = 0 To UBColonne
    tmp = tmp + val(tWarr(j))
  Next j
  For j = 0 To UBColonne
    tWidth(j) = (VScroll1.Left / tmp) * val(tWarr(j))
  Next j
  If i < UBColonne Then
    For j = i + 1 To UBColonne
      tWidth(i) = tWidth(i) + tWidth(j)
    Next j
  End If
  If (i > UBColonne) Then UBColonne = i
  '
  For j = 0 To i
    Set cell.ParentPic = Picture1
    Set cell.VScroll = VScroll1
    cell.AutoRedraw = False
    If title Then
      cell.BackColor = RGB(150, 255, 150)
      cell.Selectable = False
      cell.Allineamento = CentroCentro
    Else
      cell.Allineamento = CentroSx
      cell.BackColor = vbWhite
      cell.Selectable = (j > 0)
    End If
    cell.Bold = title
    'cell.Limiti = Limiti
    'cell.LimitiVis = LimitiLett
    cell.BorderColor = RGB(180, 180, 180)
    cell.RowSelColor = RGB(210, 255, 255)
    cell.SelColor = vbYellow
    cell.BoolValues = BoolValues
    cell.Height = g_Height
    cell.Width = tWidth(j)
    cell.Left = tLeft
    tLeft = tLeft + tWidth(j)
    If (tArr(j) <> "") Then
      If (j = 0) Then
        ReDim tarr2(0)
        tarr2(0) = tArr(0)
      Else
        tarr2() = Split(tArr(j), "=")
      End If
      If UBound(tarr2) = 1 Then
        cell.VARIABILE = tarr2(0)
        cell.Value = tarr2(1)
      Else
        cell.VARIABILE = ""
        cell.Value = tarr2(0)
      End If
    Else
        cell.VARIABILE = ""
        cell.Value = ""
    End If
    If cell.VARIABILE Like "*_CORR" Then
      cell.Correttore = True
    Else
      cell.Correttore = False
    End If
    cell.Top = UBRighe * g_Height
    cell.Key = UBRighe & "," & j
    Celle.Add cell, cell.Key
    'cell.AutoRedraw = True
    Set cell = Nothing
  Next j
  '
End Sub

Public Sub CentraTesti()

Dim cell As Cella
'
On Error Resume Next
  '
  For Each cell In Celle
    cell.Allineamento = CentroCentro
  Next
  Set cell = Nothing

End Sub

Public Sub NonSelezionabili()

Dim cell As Cella
'
On Error Resume Next
  '
  For Each cell In Celle
    cell.Selectable = False
  Next
  Set cell = Nothing

End Sub

Public Sub EnableAutoredraw()

Dim cell As Cella
'
On Error Resume Next
  '
  For Each cell In Celle
    cell.AutoRedraw = True
  Next
  Set cell = Nothing

End Sub

Public Property Get NewItemWidth() As String
'
On Error Resume Next
  '
  NewItemWidth = m_sNewItemWidth

End Property

Public Property Let NewItemWidth(ByVal sNewItemWidth As String)
'
On Error Resume Next
  '
  m_sNewItemWidth = sNewItemWidth
  Call UserControl.PropertyChanged("NewItemWidth")

End Property

Public Property Get Font() As StdFont
'
On Error Resume Next
  '
  If m_Font Is Nothing Then Set m_Font = Picture1.Font
  Set Font = m_Font

End Property

Public Property Set Font(colFont As StdFont)
'
On Error Resume Next
  '
  Set m_Font = colFont
  Set Picture1.Font = m_Font
  Call UserControl.PropertyChanged("Font")

End Property

Public Function TextWidth(stmp As String)
'
On Error Resume Next
  '
  TextWidth = Picture1.TextWidth(stmp)
  
End Function

Public Property Get VScroll1_Value()
'
On Error Resume Next
  '
  VScroll1_Value = VScroll1.Value
  
End Property

Private Sub DESelRow(Key As String)
'
Dim tArr()  As String
Dim i       As String
Dim j       As Integer
Dim tKey    As String
'
On Error Resume Next
  '
  Exit Sub
  '
  If Key = "" Then Exit Sub
  tArr = Split(Key, ",")
  i = tArr(0)
  For j = 0 To UBColonne
    tKey = i & "," & j
    If CellaEsiste(tKey) Then
      Celle(tKey).RowSel = False
      If tKey = Key Then
        Celle(tKey).Selected = False
      End If
    End If
  Next j
  EvidClear

End Sub

Private Sub EvidClear()
  
Dim tstr  As String
Dim i     As Integer
Dim k     As Integer
Dim j     As Integer
'
On Error Resume Next
  '
  tstr = Selected
  i = Left(tstr, InStr(1, tstr, ",") - 1)
  j = Right(tstr, Len(tstr) - InStr(1, tstr, ","))
  '
  tstr = ""
  For k = 0 To UBRighe
    tstr = k & "," & j
    If CellaEsiste(tstr) Then
      If Not GetCella(tstr).Bold Then
        GetCella(tstr).RowSel = False
      End If
    End If
  Next k

End Sub
