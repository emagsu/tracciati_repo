VERSION 5.00
Begin VB.UserControl MHIS 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BackColor       =   &H00EBE1D7&
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   1032
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   4224
   ScaleHeight     =   1032
   ScaleWidth      =   4224
   Begin VB.Image imgMHIS 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   732
      Left            =   0
      Stretch         =   -1  'True
      Top             =   0
      Width           =   852
   End
End
Attribute VB_Name = "MHIS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Function SelectPage(v1 As Integer, v2 As Integer, v3 As Integer, v4 As Integer, v5 As Integer, v6 As Boolean, v7 As Integer, v8 As Integer) As Long
'
Dim PERCORSO As String
'
On Error Resume Next
  '
  SelectPage = 99
  'SENSORE_TOCCO
  If (v1 = 2) And (v2 = 4) And (v3 = 1) And (v4 = -1) And (v5 = -1) And (v6 = False) And (v7 = 0) And (v8 = 0) Then
    SelectPage = 1
    PERCORSO = App.Path & "\MARPOSS\SENSORE_TOCCO.BMP"
    imgMHIS.Picture = LoadPicture(PERCORSO)
  End If
  'TAGLIO_ARIA: PARAMETRI DA VERIFICARE SVG070216
  If (v1 = 3) And (v2 = 4) And (v3 = 1) And (v4 = -1) And (v5 = -1) And (v6 = False) And (v7 = 0) And (v8 = 0) Then
    SelectPage = 2
    PERCORSO = App.Path & "\MARPOSS\TAGLIO_ARIA.BMP"
    imgMHIS.Picture = LoadPicture(PERCORSO)
  End If
  'BILANCIATURA
  If (v1 = 17) And (v2 = 3) And (v3 = 1) And (v4 = -1) And (v5 = -1) And (v6 = False) And (v7 = 0) And (v8 = 0) Then
    SelectPage = 3
    PERCORSO = App.Path & "\MARPOSS\BILANCIATURA.BMP"
    imgMHIS.Picture = LoadPicture(PERCORSO)
  End If
  'DIAMETRAL
  If (v1 = 23) And (v2 = 1) And (v3 = 1) And (v4 = -1) And (v5 = -1) And (v6 = False) And (v7 = 0) And (v8 = 0) Then
    SelectPage = 4
    PERCORSO = App.Path & "\MARPOSS\DIAMETRAL.BMP"
    imgMHIS.Picture = LoadPicture(PERCORSO)
  End If
  UserControl.Refresh
  '
End Function

Public Sub EnableActiveX(v1 As Boolean)
'
'
End Sub

Private Sub UserControl_Resize()
'
On Error Resume Next
  '
  imgMHIS.Left = 0
  imgMHIS.Top = 0
  imgMHIS.Width = UserControl.Width
  imgMHIS.Height = UserControl.Height
  '
End Sub
