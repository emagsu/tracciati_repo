VERSION 5.00
Begin VB.UserControl SuGrid 
   ClientHeight    =   4020
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   6228
   ScaleHeight     =   4020
   ScaleWidth      =   6228
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   2385
      Left            =   525
      ScaleHeight     =   2364
      ScaleWidth      =   4692
      TabIndex        =   0
      Top             =   345
      Width           =   4710
      Begin VB.VScrollBar VScroll1 
         Height          =   2145
         LargeChange     =   10
         Left            =   4305
         Max             =   0
         TabIndex        =   1
         TabStop         =   0   'False
         Top             =   90
         Width           =   255
      End
   End
End
Attribute VB_Name = "SuGrid"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False

Option Explicit

Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, ByVal lParam As Any) As Long

Private Declare Function GetKeyState Lib "user32" (ByVal nVirtKey As Long) As Integer
Const VK_TAB = &H9

Const LB_GETITEMHEIGHT = &H1A1
Const LB_SETITEMHEIGHT = &H1A0
Const CB_SETITEMHEIGHT = &H153

Private Resizing As Boolean

Public ResizeCol As Boolean

Private UBRighe   As Integer
Private UBColonne As Integer

Public Celle     As New Collection

Private g_Height  As Single
Private g_RigPag  As Integer
Private m_Font    As StdFont

Public Event Selezionata(Key As String, Variabile As String)
Public Event EditEnabling(Key As String, CodiceASCII As Integer)
'Public Event EditDisEnabling(Key As String, Variabile As String, NewValue As String)
Public Event GridAction(Key As String, Variabile As String, Action As String)
Public Event ListItemSelected(Value As String, Variabile As String)
Public Event VarChanged(Variabili As String)
'Public Event NewWidth(Larghezze As String)
Public Event DblClick(Key As String)

Private m_bHasFocus     As Boolean
Private m_sNewItemWidth As String
Private LastSelected    As String
Private LastListValue   As String

Public Sub CROCE(tstr As String)
'
Dim i     As Integer
Dim k     As Integer
Dim j     As Integer
Dim tstr1 As String
'
On Error Resume Next
  '
  i = Left(tstr, InStr(1, tstr, ",") - 1)
  j = Right(tstr, Len(tstr) - InStr(1, tstr, ","))
  If Not CellaEsiste(tstr) Then Exit Sub
  '
  tstr1 = ""
  For k = i - 1 To 1 Step -1
    tstr1 = k & "," & j
    If CellaEsiste(tstr1) Then
      GetCella(tstr1).RowSel = True
    Else
      Exit For
    End If
  Next k
  '
  tstr1 = ""
  For k = i + 1 To UBRighe
    tstr1 = k & "," & j
    If CellaEsiste(tstr1) Then
      GetCella(tstr1).RowSel = True
    Else
      Exit For
    End If
  Next k
  '
  tstr1 = ""
  For k = j - 1 To 0 Step -1
    tstr1 = i & "," & k
    If CellaEsiste(tstr1) Then
      GetCella(tstr1).RowSel = True
    Else
      Exit For
    End If
  Next k
  '
  tstr1 = ""
  For k = j + 1 To UBColonne
    tstr1 = i & "," & k
    If CellaEsiste(tstr1) Then
      GetCella(tstr1).RowSel = True
    Else
      Exit For
    End If
  Next k
  
End Sub

Public Property Get CellHeight() As Single
'
On Error Resume Next
  '
  CellHeight = g_Height

End Property

Public Property Let CellHeight(ByVal fCellHeight As Single)
'
On Error Resume Next
  '
  g_Height = fCellHeight
  If g_Height = 0 Then
    g_RigPag = 25
    g_Height = Picture1.ScaleHeight / g_RigPag
  Else
    g_RigPag = CInt(Picture1.ScaleHeight / g_Height)
    g_Height = CInt(Picture1.ScaleHeight / g_RigPag)
  End If
  Call UserControl.PropertyChanged("CellHeight")

End Property

Public Property Get ScaleHeight()
'
On Error Resume Next
  '
  ScaleHeight = Picture1.ScaleHeight

End Property

Public Property Get ScaleTop()
'
On Error Resume Next
  '
  ScaleTop = Picture1.ScaleTop

End Property

Public Sub EVIDENZIA_COLONNA()
'
Dim tstr  As String
Dim i     As Integer
Dim k     As Integer
Dim j     As Integer
Dim Width As Single
'
On Error Resume Next
  '
  tstr = Selected
  '
  i = Left(tstr, InStr(1, tstr, ",") - 1)
  j = Right(tstr, Len(tstr) - InStr(1, tstr, ","))
  If Not CellaEsiste(tstr) Then Exit Sub
  '
  Width = Celle(tstr).Width
  tstr = ""
  For k = i To 1 Step -1
    tstr = k & "," & j
    If CellaEsiste(tstr) Then
      If (Not GetCella(tstr).Bold) And (GetCella(tstr).Width = Width) Then
        GetCella(tstr).RowSel = True
      Else
        Exit For
      End If
    Else
      Exit For
    End If
  Next k
  For k = i To UBRighe
    tstr = k & "," & j
    If CellaEsiste(tstr) Then
      If (Not GetCella(tstr).Bold) And (GetCella(tstr).Width = Width) Then
        GetCella(tstr).RowSel = True
      Else
        Exit For
      End If
    Else
      Exit For
    End If
  Next k
  '
End Sub

Private Sub COPIA_COLONNA(Optional Back As Boolean = False)

Dim tstr    As String
Dim tstr2   As String
Dim i       As Integer
Dim k       As Integer
Dim j       As Integer
Dim chgVAR  As String
  '
On Error Resume Next
  '
  tstr = Selected
  i = Left(tstr, InStr(1, tstr, ",") - 1)
  j = Right(tstr, Len(tstr) - InStr(1, tstr, ","))
  If (Not CellaSelezionabile(i & "," & j + 1)) And (Not Back) Then Exit Sub
  If (Not CellaSelezionabile(i & "," & j - 1)) And (Back) Then Exit Sub
  chgVAR = "": tstr = ""
  For k = 0 To UBRighe
    tstr = k & "," & j
    If Back Then
      tstr2 = k & "," & j - 1
    Else
      tstr2 = k & "," & j + 1
    End If
    GetCella(tstr2).Value = LEP(GetCella(tstr).Variabile)
    If (chgVAR <> "") Then chgVAR = chgVAR & ";"
    chgVAR = chgVAR & GetCella(tstr2).Variabile
  Next k
  If Back Then tstr = "COPY (" & j & ") -> (" & j - 1 & ")" Else tstr = "COPY (" & j & ") -> (" & j + 1 & ")"
  If (MsgBox(tstr, vbYesNo) = vbYes) Then
    If (chgVAR <> "") Then
      WRITE_DIALOG "copying......"
      RaiseEvent VarChanged(chgVAR)
    Else
      WRITE_DIALOG "REPEAT COMMAD: Error during copy operation!"
      RaiseEvent VarChanged("SALTA")
    End If
  Else
      WRITE_DIALOG "Operation aborted by user!"
      RaiseEvent VarChanged("SALTA")
  End If
  
End Sub

Public Sub STAMPA(Top As Single, Left As Single, Width As Single)

Dim XScala  As Single
Dim YScala  As Single
Dim cell    As Cella
Dim tTop    As Single
'
On Error GoTo errSTAMPA_GRID
  '
  If VScroll1.Visible Then
    XScala = Width / VScroll1.Left
  Else
    XScala = Width / UserControl.ScaleWidth
  End If
  YScala = (Printer.TextHeight("X") * 1.2) / GetCella("0,0").Height
  tTop = Top
  For Each cell In Celle
    If (tTop + cell.Top * YScala > 270) Then
      Printer.NewPage
      tTop = Top - cell.Top * YScala
    End If
    Call cell.STAMPA(tTop, Left, XScala, YScala)
  Next
  '
Exit Sub

errSTAMPA_GRID:

End Sub

Public Sub AutoSize()
'
On Error Resume Next
  '
  Height = g_Height * (UBRighe + 1)

End Sub

Private Sub Picture1_DblClick()
'
Dim tstr As String
'
On Error Resume Next
  '
  tstr = Selected
  RaiseEvent DblClick(tstr)
  'RaiseEvent EditEnabling(tstr)

End Sub

Private Sub Picture1_KeyPress(KeyAscii As Integer)
'
Dim tstr As String
'
On Error Resume Next
  '
  'RICHIAMO DEI CONTROLLI DI EDITING
  If ((KeyAscii >= Asc("0")) And (KeyAscii <= Asc("z"))) _
    Or (KeyAscii = Asc("-")) Or (KeyAscii = Asc("(")) _
    Or (KeyAscii = Asc(",")) Or (KeyAscii = Asc(".")) Then
      tstr = Selected
      RaiseEvent EditEnabling(tstr, KeyAscii)
  End If
  '
End Sub

Private Sub UserControl_Terminate()
'
On Error Resume Next
  '
  Set m_Font = Nothing

If (hndHGkinetic <> 0) Then HGkinetic_cmdCloseWindow

End Sub

Public Function Clear()
'
Dim cell As Cella
'
On Error Resume Next
  '
  While (Celle.count > 0)
    Call Celle.Remove(1)
  Wend
  Picture1.Cls
  UBRighe = -1
  VScroll1.MIN = 0
  VScroll1.Max = 0
  VScroll1 = 0

End Function

Public Property Get HasFocus() As Boolean
'
On Error Resume Next
  '
  HasFocus = m_bHasFocus

End Property

Public Function FreeSpaces() As Integer
'
On Error Resume Next
  '
  FreeSpaces = g_RigPag - UBRighe - 1

End Function

Public Property Get CurrVar() As String

Dim tstr As String
'
On Error Resume Next
  '
  tstr = Selected
      If (tstr = "") Then
    CurrVar = ""
  ElseIf CellaEsiste(tstr) Then
    CurrVar = Celle(tstr).Variabile
  End If

End Property

Private Function GetTabState() As Boolean
'
On Error Resume Next
  '
  GetTabState = False
  If GetKeyState(VK_TAB) And -256 Then GetTabState = True

End Function

Public Property Get ListCount() As Integer
'
On Error Resume Next
  '
  ListCount = UBRighe + 1

End Property

Function GetCella(Key As String) As Cella
'
On Error Resume Next
  '
  If CellaEsiste(Key) Then Set GetCella = Celle(Key)

End Function

Function GetKeyCellaByVar(Variabile As String) As String

Dim cell As New Cella
'
On Error Resume Next
  '
  GetKeyCellaByVar = ""
  For Each cell In Celle
    If Trim(UCase(cell.Variabile)) = Trim(UCase(Variabile)) Then
      GetKeyCellaByVar = cell.Key
    End If
  Next
  Set cell = Nothing

End Function

Function GetCellaByVar(Variabile As String) As Cella

Dim cell As New Cella
'
On Error Resume Next
  '
  Set GetCellaByVar = Nothing
  For Each cell In Celle
    If Trim(UCase(cell.Variabile)) = Trim(UCase(Variabile)) Then
      Set GetCellaByVar = cell
    End If
  Next
  Set cell = Nothing

End Function

Public Function Selected(Optional SelLast As Boolean = True) As String
'
Dim i As Integer
Dim j As Integer
'
On Error Resume Next
  '
  For j = 0 To UBColonne
    For i = 0 To UBRighe
      If CellaEsiste(i & "," & j) Then
        If Celle.Item(i & "," & j).Selected Then
          Selected = i & "," & j
          LastSelected = Selected
          'Write_Dialog "Selected: " & Selected & " UBColonne: " & UBColonne & " UBRighe: " & UBRighe & " "
          Exit Function
        End If
      End If
    Next i
  Next j
  If SelLast Then Selected = LastSelected
  
End Function

Private Sub SELEZIONA_COLONNA(Key As String)

Dim tArr()  As String
Dim i       As String
Dim j       As Integer
Dim k       As Integer
Dim tKey    As String
'
On Error Resume Next
  '
  If (Key = "") Then Exit Sub
  '
  tArr = Split(Key, ",")
  i = tArr(0):  k = tArr(1)
  '
  For j = k To 0 Step -1
    tKey = i & "," & j
    If CellaEsiste(tKey) Then
      Celle(tKey).RowSel = True
      If (tKey = Key) Then
        If Not Celle(tKey).Selected Then RaiseEvent Selezionata(tKey, Celle(tKey).Variabile)
        Celle(tKey).Selected = True
      End If
    End If
  Next j
  For j = k + 1 To UBColonne
  'For j = 0 To UBColonne
    tKey = i & "," & j
    If CellaEsiste(tKey) Then
      Celle(tKey).RowSel = True
      If (tKey = Key) Then
        If Not Celle(tKey).Selected Then RaiseEvent Selezionata(tKey, Celle(tKey).Variabile)
        Celle(tKey).Selected = True
      End If
    End If
  Next j

End Sub

Function CellaByPoint(X As Single, Y As Single) As String

Dim cell As Cella
'
On Error Resume Next
  '
  For Each cell In Celle
    If (Y > cell.Top) And (Y < (cell.Top + cell.Height)) And (X > cell.Left) And (X < (cell.Left + cell.Width)) Then
      CellaByPoint = cell.Key
      Set cell = Nothing
      Exit Function
    End If
  Next
  Set cell = Nothing

End Function

Sub Redraw()

Dim cell As Cella
'
On Error Resume Next
  '
  For Each cell In Celle
    Call cell.Redraw
    Set cell = Nothing
  Next
  Set cell = Nothing

End Sub

'Function PuntoSuBordo(X As Single, Y As Single) As Boolean

'Dim Cond As Boolean
'Dim cell As Cella
'
'On Error Resume Next
'  '
'  PuntoSuBordo = False
'  For Each cell In Celle
'    If Abs(X - (cell.Left + cell.Width)) < 100 Then
'    Cond = True
'      If cell.Left + cell.Width = VScroll1.Left Then Cond = False
'      If cell.Left + cell.Width = UserControl.ScaleWidth Then Cond = False
'      PuntoSuBordo = Cond
'      Set cell = Nothing
'      Exit Function
'    End If
'  Next
'  Set cell = Nothing
'
'End Function

Public Sub SelFirst()

Dim tstr As String
'
On Error Resume Next
  '
  tstr = Selected
  Call SELEZIONA_COLONNA(CellaFirst)
  Call EVIDENZIA_COLONNA

End Sub

Private Sub Picture1_KeyUP(KeyCode As Integer, Shift As Integer)
'
Dim sposizione  As String
Dim vposizione  As Integer
Dim tstr        As String
Dim tArr()      As String
Dim i           As Integer
'
On Error Resume Next
  '
  'MsgBox "KeyCode " & KeyCode & " Shift " & Shift
  '
  tstr = Selected
  If (tstr = "") Then
    tstr = CellaFirst
    If (tstr <> "") Then
      Call SELEZIONA_COLONNA(tstr)
      Call EVIDENZIA_COLONNA
    End If
    Exit Sub
  End If
  '
  If (Shift = 2) And (KeyCode <> 18) Then 'Ctrl + ....
    Select Case KeyCode
      Case vbKeyUp:     RaiseEvent GridAction(tstr, Celle(tstr).Variabile, "MSGUP")
      Case vbKeyDown:   RaiseEvent GridAction(tstr, Celle(tstr).Variabile, "MSGDOWN")
      Case 12:          RaiseEvent GridAction(tstr, Celle(tstr).Variabile, "LIMITI") 'Tasto select Siemens
      Case 123:         RaiseEvent GridAction(tstr, Celle(tstr).Variabile, "INFO")   'Tasto INFO Siemens
      Case vbKeyRight:  Call COPIA_COLONNA
      Case vbKeyLeft:   Call COPIA_COLONNA(True)
      Case vbKeyDelete: RaiseEvent GridAction(tstr, Celle(tstr).Variabile, "CANCEL")
      Case 36, vbKeyP:  RaiseEvent GridAction(tstr, Celle(tstr).Variabile, "PARAMETRI")
    End Select
    Call CROCE(tstr)
    Exit Sub
  End If
  '
  Select Case KeyCode
      '
    Case vbKeyHome: VScroll1 = VScroll1.MIN
    Case vbKeyEnd:  VScroll1 = VScroll1.Max
      '
    Case vbKeyPageUp
      sposizione = Split(tstr, ",")(0)
      vposizione = CInt(sposizione) - 10
      If (vposizione <= 1) Then vposizione = 1
      tstr = Format$(vposizione, "#######0") & "," & Split(tstr, ",")(1)
      If Not CellaSelezionabile(tstr) Then tstr = CellaDown(tstr)
      Call SELEZIONA_COLONNA(tstr)
      Call EVIDENZIA_COLONNA
      '
    Case vbKeyPageDown
      sposizione = Split(tstr, ",")(0)
      vposizione = CInt(sposizione) + 10
      If (vposizione >= UBRighe) Then vposizione = UBRighe
      tstr = Format$(vposizione, "#######0") & "," & Split(tstr, ",")(1)
      Call SELEZIONA_COLONNA(tstr)
      Call EVIDENZIA_COLONNA
      '
    Case vbKeyDown
      tstr = CellaDown(tstr)
      Call SELEZIONA_COLONNA(tstr)
      Call EVIDENZIA_COLONNA
      '
    Case vbKeyUp
      tstr = CellaUP(tstr)
      Call SELEZIONA_COLONNA(tstr)
      Call EVIDENZIA_COLONNA
      If (CellaUP(tstr) <> "") Then
        If CInt(Split(CellaUP(tstr), ",")(0)) > CInt(Split(tstr, ",")(0)) Then VScroll1 = VScroll1.MIN
      End If
      '
    Case vbKeyRight
      Call SELEZIONA_COLONNA(CellaDx(tstr))
      Call EVIDENZIA_COLONNA
      '
    Case vbKeyLeft
      Call SELEZIONA_COLONNA(CellaSx(tstr))
      Call EVIDENZIA_COLONNA
      '
    Case vbKeyReturn
      If Trim(GetCella(tstr).BoolValues = "") Then
        RaiseEvent EditEnabling(tstr, vbKeyReturn)
      Else
        Call SELEZIONA_COLONNA(CellaDown(tstr))
        Call EVIDENZIA_COLONNA
      End If
      '
    Case 12 'Tasto Select Siemens
      If Trim(GetCella(tstr).BoolValues <> "") Then
        RaiseEvent EditEnabling(tstr, 12)
      End If
      '
    Case vbKeyDelete
        RaiseEvent EditEnabling(tstr, vbKeyDelete)
      
  End Select
  
End Sub

'Function CalcolaLarg(Key As String) As String
'
'Dim tArr()  As String
'Dim i       As String
'Dim j       As Integer
'Dim tKey    As String
'Dim larg    As String
''
'On Error Resume Next
'  '
'  If Key = "" Then Exit Function
'  tArr = Split(Key, ",")
'  i = tArr(0)
'  larg = ""
'  For j = 0 To UBColonne
'    tKey = i & "," & j
'    If CellaEsiste(tKey) Then
'      larg = larg & CInt(Celle(tKey).Width * 100 / Picture1.ScaleWidth) & ";"
'    End If
'  Next j
'  CalcolaLarg = larg
'  '
'End Function

Function CellaEsiste(Key As String) As Boolean

Dim cell As New Cella

On Error Resume Next
  
  Set cell = Celle(Key)
  If Err.Number = 0 Then
    CellaEsiste = True
  Else
    CellaEsiste = False
  End If
  Set cell = Nothing

End Function

Function CellaSelezionabile(Key As String) As Boolean

Dim cell As New Cella

On Error Resume Next
  
  If (Key <> "") Then
    Set cell = Celle(Key)
    If (Err.Number = 0) Then
      CellaSelezionabile = cell.Selectable
    Else
      CellaSelezionabile = False
    End If
  Else
      CellaSelezionabile = False
  End If
  Set cell = Nothing

End Function

Function CellaFirst() As String
  
Dim i     As Integer
Dim j     As Integer
Dim tstr  As String
'
On Error Resume Next
  '
  For i = 0 To UBRighe
    For j = 0 To UBColonne
        tstr = i & "," & j
        If CellaSelezionabile(tstr) Then
          CellaFirst = tstr
          Exit Function
        End If
    Next j
  Next i

End Function

Function CellaDown(Key As String) As String
  
Dim i       As Integer
Dim j       As Integer
Dim tArr()  As String
Dim tKey    As String
Dim cell    As Cella
Dim tX      As Single
Dim tY      As Single
'
'On Error Resume Next
  '
  If (Key = "") Then CellaDown = "": Exit Function
  '
  Set cell = Celle(Key)
  tX = cell.Left + 1
  tY = cell.Top + cell.Height * 3 / 2
  tKey = CellaByPoint(tX, tY)
  While (tKey <> Key) And (Not CellaSelezionabile(tKey))
    tY = tY + cell.Height
    If (tY > (cell.Height * (UBRighe + 1))) Then
      VScroll1 = VScroll1.MIN
      tY = cell.Height / 2
    End If
    tKey = CellaByPoint(tX, tY)
  Wend
  CellaDown = tKey
  Set cell = Nothing
  '
End Function

Function CellaUP(Key As String) As String
  
Dim i       As Integer
Dim j       As Integer
Dim tArr()  As String
Dim tKey    As String
Dim cell    As Cella
Dim tX      As Single
Dim tY      As Single
'
'On Error Resume Next
  '
  If (Key = "") Then CellaUP = "": Exit Function
  '
  Set cell = Celle(Key)
  tX = cell.Left + 1
  tY = cell.Top - cell.Height \ 2
  tKey = CellaByPoint(tX, tY)
  While (tKey <> Key) And Not CellaSelezionabile(tKey)
    tY = tY - cell.Height
    If (tY < 0) Then tY = cell.Height * UBRighe + 1
    tKey = CellaByPoint(tX, tY)
  Wend
  CellaUP = tKey
  Set cell = Nothing
  '
End Function

Function CellaDx(Key As String) As String
  
Dim i       As Integer
Dim j       As Integer
Dim tArr()  As String
Dim tKey    As String
'
'On Error Resume Next
  '
  tArr = Split(Key, ",")
  i = CInt(tArr(0))
  j = CInt(tArr(1)): j = (j + 1) Mod (UBColonne + 1)
  tKey = i & "," & j
  While Not CellaSelezionabile(tKey) And (tKey <> Key)
    j = (j + 1) Mod (UBColonne + 1)
    tKey = i & "," & j
  Wend
  CellaDx = tKey
  
End Function

Function CellaSx(Key As String) As String
  
Dim i       As Integer
Dim j       As Integer
Dim tArr()  As String
Dim tKey    As String
'
'On Error Resume Next
  '
  tArr = Split(Key, ",")
  i = CInt(tArr(0))
  j = CInt(tArr(1)): j = j - 1
  tKey = i & "," & j
  While Not CellaSelezionabile(tKey) And (tKey <> Key)
    j = j - 1
    If j < 0 Then j = UBColonne
    tKey = i & "," & j
  Wend
  CellaSx = tKey

End Function

Private Sub Picture1_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
'
Dim tstr  As String
Dim i     As Integer
Dim k     As Integer
Dim j     As Integer
Dim cell  As Cella
'
On Error Resume Next
  '
  For Each cell In Celle
    cell.Selected = False
    cell.RowSel = False
  Next
  For Each cell In Celle
    If (Y > cell.vTop) And (Y < (cell.vTop + cell.Height)) _
   And (X > cell.Left) And (X < (cell.Left + cell.Width)) Then
      If cell.Selectable Then
        If Not cell.Selected Then tstr = cell.Key
        cell.Selected = True
      End If
    End If
  Next
  Set cell = Nothing
  Call CROCE(tstr)
  '
End Sub

Private Sub Picture1_Resize()
'
On Error Resume Next
  '
  VScroll1.Height = Picture1.ScaleHeight
  VScroll1.Top = 0
  VScroll1.Left = Picture1.ScaleWidth - VScroll1.Width

End Sub

Private Sub UserControl_EnterFocus()

Dim tstr As String
    '
On Error Resume Next
  '
  m_bHasFocus = True
  '
  tstr = Selected
  If Not CellaSelezionabile(tstr) Then tstr = ""
  If tstr <> "" Then
    Call SELEZIONA_COLONNA(Selected)
  Else
    Call SELEZIONA_COLONNA(CellaFirst)
  End If

End Sub

Public Function hwnd() As Long
'
On Error Resume Next
  '
  hwnd = UserControl.hwnd

End Function

Private Sub UserControl_ExitFocus()

Dim tstr As String
'
On Error Resume Next
  '
  m_bHasFocus = False
  tstr = Selected
  If (tstr <> "") Then Call DESelRow(Selected)

End Sub
      
Private Sub UserControl_Initialize()
'
On Error Resume Next
  '
  UBRighe = -1
  m_sNewItemWidth = "1;1;1"
  g_RigPag = 19
  
End Sub

Private Sub UserControl_Resize()
'
On Error Resume Next
  '
  Picture1.Top = 0
  Picture1.Left = 0
  Picture1.Height = UserControl.ScaleHeight
  Picture1.Width = UserControl.Width
  Call RowSExpand
  
End Sub

Private Sub VScroll1_Change()
'
Dim cell As Cella
'
On Error Resume Next
  '
  Picture1.Cls
  For Each cell In Celle
    Call cell.Redraw
  Next
  Set cell = Nothing

End Sub

Private Sub VScroll1_GotFocus()
'
On Error Resume Next
  '
  Picture1.SetFocus
  
End Sub

Private Sub VScroll1_Scroll()
'
On Error Resume Next
  '
  Call VScroll1_Change

End Sub

Sub RowSExpand()
'
Dim j         As Integer
Dim i         As Integer
Dim tstr      As String
Dim totWidth  As Single
Dim picWidth  As Single
Dim factor    As Single
'
On Error Resume Next
  '
  For j = 0 To UBRighe
    'CALCOLO LARGHEZZA COLONNA TOTALE
    totWidth = 0
    For i = 0 To UBColonne
      tstr = j & "," & i
      If CellaEsiste(tstr) Then
         totWidth = totWidth + Celle(tstr).Width
      End If
    Next i
    picWidth = VScroll1.Left
    If (totWidth <> 0) Then
      factor = picWidth / totWidth
      For i = 0 To UBColonne
        tstr = j & "," & i
        If CellaEsiste(tstr) Then
           Celle(tstr).Left = Celle(tstr).Left * factor
           Celle(tstr).Width = Celle(tstr).Width * factor
        End If
      Next i
    End If
  Next j
  
End Sub

Public Sub AggiornaVscroll()
'
On Error Resume Next
  '
  VScroll1.Max = UBRighe - g_RigPag + 1
  If (VScroll1.Max < 0) Then
    VScroll1 = VScroll1.MIN
    VScroll1.Max = 0
  End If
  If (VScroll1.Max = 0) Then
    If (VScroll1.Left <> Picture1.Width) Then
      VScroll1.Left = Picture1.Width
      Call RowSExpand
    End If
  Else
    If (VScroll1.Left <> (Picture1.Width - VScroll1.Width)) Then
      VScroll1.Left = Picture1.Width - VScroll1.Width
      Call RowSExpand
    End If
  End If

End Sub

Public Sub AddItem(Valori As String, Optional title As Boolean = False, Optional BoolValues As String = "")
'
Dim tArr()    As String
Dim tarr2()   As String
Dim tWarr()   As String
Dim tWidth()  As Single
Dim tmp       As Single
Dim i         As Integer
Dim j         As Integer
Dim tstr      As String
Dim tLeft     As Single
Dim cell      As New Cella
'
On Error Resume Next
  '
  tLeft = 0
  UBColonne = 2
  UBRighe = UBRighe + 1
  VScroll1.Max = UBRighe - g_RigPag + 1
  '
  tArr() = Split(Valori, ";")
  i = UBound(tArr)
  If (i > UBColonne) Then UBColonne = i
  '
  If VScroll1.Max < 0 Then
    VScroll1 = VScroll1.MIN
    VScroll1.Max = 0
  End If
  If VScroll1.Max = 0 Then
    If VScroll1.Left <> Picture1.Width Then
      VScroll1.Left = Picture1.Width
      Call RowSExpand
    End If
  Else
    If VScroll1.Left <> (Picture1.Width - VScroll1.Width) Then
      VScroll1.Left = Picture1.Width - VScroll1.Width
      Call RowSExpand
    End If
  End If
  tstr = m_sNewItemWidth
  'Debug.Print UBRighe & " -> " & tstr
  If Right(tstr, 1) = ";" Then
    tstr = tstr & "0;0;0;0;0;"
  Else
    tstr = tstr & ";0;0;0;0;0;"
  End If
  tWarr = Split(tstr, ";")
  '
  ReDim Preserve tWarr(UBColonne)
  ReDim tWidth(UBColonne)
  '
  tmp = 0
  For j = 0 To UBColonne
    tmp = tmp + val(tWarr(j))
  Next j
  For j = 0 To UBColonne
    tWidth(j) = (VScroll1.Left / tmp) * val(tWarr(j))
  Next j
  If i < UBColonne Then
    For j = i + 1 To UBColonne
      tWidth(i) = tWidth(i) + tWidth(j)
    Next j
  End If
  If (i > UBColonne) Then UBColonne = i
  '
  For j = 0 To i
    Set cell.ParentPic = Picture1
    Set cell.VScroll = VScroll1
    cell.AutoRedraw = False
    If title Then
      cell.BackColor = RGB(150, 255, 150)
      cell.Selectable = False
      cell.Allineamento = CentroCentro
    Else
      cell.Allineamento = CentroSx
      cell.BackColor = vbWhite
      cell.Selectable = (j > 0)
    End If
    cell.Bold = title
    'cell.Limiti = Limiti
    'cell.LimitiVis = LimitiLett
    cell.BorderColor = RGB(180, 180, 180)
    cell.RowSelColor = RGB(210, 255, 255)
    cell.SelColor = vbYellow
    cell.BoolValues = BoolValues
    cell.Height = g_Height
    cell.Width = tWidth(j)
    cell.Left = tLeft
    tLeft = tLeft + tWidth(j)
    If (tArr(j) <> "") Then
      If (j = 0) Then
        ReDim tarr2(0)
        tarr2(0) = tArr(0)
      Else
        tarr2() = Split(tArr(j), "=")
      End If
      If UBound(tarr2) = 1 Then
        cell.Variabile = tarr2(0)
        cell.Value = tarr2(1)
      Else
        cell.Variabile = ""
        cell.Value = tarr2(0)
      End If
    Else
        cell.Variabile = ""
        cell.Value = ""
    End If
    If cell.Variabile Like "*_CORR" Then
      cell.Correttore = True
    Else
      cell.Correttore = False
    End If
    cell.Top = UBRighe * g_Height
    cell.Key = UBRighe & "," & j
    Celle.Add cell, cell.Key
    'cell.AutoRedraw = True
    Set cell = Nothing
  Next j
  '
End Sub

Public Sub CentraTesti()

Dim cell As Cella
'
On Error Resume Next
  '
  For Each cell In Celle
    cell.Allineamento = CentroCentro
  Next
  Set cell = Nothing

End Sub

Public Sub NonSelezionabili()

Dim cell As Cella
'
On Error Resume Next
  '
  For Each cell In Celle
    cell.Selectable = False
  Next
  Set cell = Nothing

End Sub

Public Sub EnableAutoredraw()

Dim cell As Cella
'
On Error Resume Next
  '
  For Each cell In Celle
    cell.AutoRedraw = True
  Next
  Set cell = Nothing

End Sub

Public Property Get NewItemWidth() As String
'
On Error Resume Next
  '
  NewItemWidth = m_sNewItemWidth

End Property

Public Property Let NewItemWidth(ByVal sNewItemWidth As String)
'
On Error Resume Next
  '
  m_sNewItemWidth = sNewItemWidth
  Call UserControl.PropertyChanged("NewItemWidth")

End Property

Public Property Get Font() As StdFont
'
On Error Resume Next
  '
  If m_Font Is Nothing Then Set m_Font = Picture1.Font
  Set Font = m_Font

End Property

Public Property Set Font(colFont As StdFont)
'
On Error Resume Next
  '
  Set m_Font = colFont
  Set Picture1.Font = m_Font
  Call UserControl.PropertyChanged("Font")

End Property

Public Function TextWidth(stmp As String)
'
On Error Resume Next
  '
  TextWidth = Picture1.TextWidth(stmp)
  
End Function

Public Property Get VScroll1_Value()
'
On Error Resume Next
  '
  VScroll1_Value = VScroll1.Value
  
End Property

Private Sub DESelRow(Key As String)
'
Dim tArr()  As String
Dim i       As String
Dim j       As Integer
Dim tKey    As String
'
On Error Resume Next
  '
  Exit Sub
  '
  If Key = "" Then Exit Sub
  tArr = Split(Key, ",")
  i = tArr(0)
  For j = 0 To UBColonne
    tKey = i & "," & j
    If CellaEsiste(tKey) Then
      Celle(tKey).RowSel = False
      If tKey = Key Then
        Celle(tKey).Selected = False
      End If
    End If
  Next j
  EvidClear

End Sub

Private Sub EvidClear()
  
Dim tstr  As String
Dim i     As Integer
Dim k     As Integer
Dim j     As Integer
'
On Error Resume Next
  '
  tstr = Selected
  i = Left(tstr, InStr(1, tstr, ",") - 1)
  j = Right(tstr, Len(tstr) - InStr(1, tstr, ","))
  '
  tstr = ""
  For k = 0 To UBRighe
    tstr = k & "," & j
    If CellaEsiste(tstr) Then
      If Not GetCella(tstr).Bold Then
        GetCella(tstr).RowSel = False
      End If
    End If
  Next k

End Sub
