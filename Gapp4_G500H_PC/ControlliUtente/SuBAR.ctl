VERSION 5.00
Begin VB.UserControl SuBAR 
   ClientHeight    =   1632
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   7512
   ScaleHeight     =   1632
   ScaleWidth      =   7512
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      ForeColor       =   &H80000008&
      Height          =   735
      Left            =   0
      ScaleHeight     =   708
      ScaleWidth      =   3888
      TabIndex        =   0
      Top             =   0
      Width           =   3915
   End
End
Attribute VB_Name = "SuBAR"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private m_dMin      As Double
Private m_dMax      As Double
Private m_dValue    As Double
Private m_dSoglia1  As Double
Private m_dSoglia2  As Double
Private m_sPreText  As String
Private m_sPostText As String
Private m_bPercent  As Boolean

Private Sub AggiornaBarra()

Dim steps   As Integer
Dim COLORE  As OLE_COLOR
Dim i       As Integer
Dim XStep   As Double
Dim XS1     As Double
Dim XS2     As Double
Dim Xc      As Double
Dim tX      As Double
Dim tY      As Double
Dim TX2     As Double
Dim tstr    As String

On Error Resume Next

  steps = 100
  
  Picture1.Move UserControl.ScaleLeft, UserControl.ScaleTop, UserControl.ScaleWidth, UserControl.ScaleHeight
  Picture1.BackColor = vbYellow
  Picture1.Cls
  Picture1.ScaleWidth = 100
  Picture1.ScaleHeight = 10
  XStep = (m_dMax - m_dMin) / steps
  
  Xc = m_dValue * 100 / (m_dMax - m_dMin)
  
  XS1 = m_dSoglia1 * 100 / (m_dMax - m_dMin)
  XS2 = m_dSoglia2 * 100 / (m_dMax - m_dMin)
  
  For i = 1 To steps
    tX = Picture1.Left + XStep * i
    If tX <= Xc Then
      COLORE = vbGreen
      If (tX > XS1) And (m_dSoglia1 > m_dMin) Then COLORE = vbYellow
      If (tX > XS2) And (m_dSoglia2 > m_dMin) Then COLORE = vbRed
      
      Picture1.Line (tX, 0)-(tX - XStep, Picture1.ScaleHeight), COLORE, BF
    End If
  Next i
  
  Picture1.FontBold = True
  Picture1.FontSize = 10
  If m_bPercent Then
    tstr = "" & Xc & "%"
  Else
    tstr = m_sPreText & Value & m_sPostText
  End If
  tX = (Picture1.ScaleWidth - Picture1.TextWidth(tstr)) / 2
  tY = (Picture1.ScaleHeight - Picture1.TextHeight(tstr)) / 2
  
  Picture1.CurrentX = tX
  Picture1.CurrentY = tY
  Picture1.ForeColor = vbRed
  Picture1.Print tstr

End Sub

Private Sub UserControl_Resize()
  
  Call AggiornaBarra

End Sub

Public Property Get MIN() As Double

  MIN = m_dMin

End Property

Public Property Let MIN(ByVal dMin As Double)

  m_dMin = dMin
  Call AggiornaBarra
  Call UserControl.PropertyChanged("Min")

End Property

Public Property Get Max() As Double

  Max = m_dMax

End Property

Public Property Let Max(ByVal dMax As Double)

  m_dMax = dMax
  Call AggiornaBarra
  Call UserControl.PropertyChanged("Max")

End Property

Public Property Get Value() As Double

  Value = m_dValue
 
End Property

Public Property Let Value(ByVal dValue As Double)

  m_dValue = dValue
  Call AggiornaBarra
  Call UserControl.PropertyChanged("Value")

End Property

Public Property Get Soglia1() As Double

  Soglia1 = m_dSoglia1

End Property

Public Property Let Soglia1(ByVal dSoglia1 As Double)

  m_dSoglia1 = dSoglia1
  Call AggiornaBarra
  Call UserControl.PropertyChanged("Soglia1")

End Property

Public Property Get Soglia2() As Double

  Soglia2 = m_dSoglia2

End Property

Public Property Let Soglia2(ByVal dSoglia2 As Double)

  m_dSoglia2 = dSoglia2
  Call AggiornaBarra
  Call UserControl.PropertyChanged("Soglia2")

End Property

Public Property Get PreText() As String

  PreText = m_sPreText

End Property

Public Property Let PreText(ByVal sPreText As String)

  m_sPreText = sPreText
  Call AggiornaBarra
  Call UserControl.PropertyChanged("PreText")

End Property

Public Property Get PostText() As String

  PostText = m_sPostText

End Property

Public Property Let PostText(ByVal sPostText As String)

  m_sPostText = sPostText
  Call AggiornaBarra
  Call UserControl.PropertyChanged("PostText")

End Property

Public Property Get Percent() As Boolean

  Percent = m_bPercent

End Property

Public Property Let Percent(ByVal bPercent As Boolean)

  m_bPercent = bPercent
  Call AggiornaBarra
  Call UserControl.PropertyChanged("Percent")

End Property
