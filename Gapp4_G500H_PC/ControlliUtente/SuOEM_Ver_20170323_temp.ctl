VERSION 5.00
Object = "{F220A985-D80A-11D5-BA79-00105AAFF7A0}#1.0#0"; "ListBoxMod.dll"
Begin VB.UserControl SuOEM 
   Appearance      =   0  'Flat
   BackColor       =   &H00EBE1D7&
   ClientHeight    =   11004
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   15000
   ControlContainer=   -1  'True
   HitBehavior     =   0  'None
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   PropertyPages   =   "SuOEM.ctx":0000
   ScaleHeight     =   11004
   ScaleWidth      =   15000
   Begin LISTBOXMODLibCtl.McVBListBox lstEdit 
      Height          =   465
      Index           =   2
      Left            =   12585
      TabIndex        =   21
      Top             =   7065
      Visible         =   0   'False
      Width           =   1470
      McVBListBox-InterfaceVersion=   1
      _cx             =   2593
      _cy             =   820
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Appearance      =   0
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      RightToLeft     =   0   'False
      Sorted          =   0   'False
      Style           =   0
      IntegralHeight  =   0   'False
      MultiSelect     =   0
      Columns         =   0
   End
   Begin VB.TextBox txtEdit 
      Appearance      =   0  'Flat
      Height          =   375
      Index           =   2
      Left            =   10905
      TabIndex        =   18
      Top             =   7125
      Visible         =   0   'False
      Width           =   1335
   End
   Begin LISTBOXMODLibCtl.McVBListBox lstEdit 
      Height          =   405
      Index           =   1
      Left            =   12555
      TabIndex        =   20
      Top             =   6120
      Visible         =   0   'False
      Width           =   1470
      McVBListBox-InterfaceVersion=   1
      _cx             =   2593
      _cy             =   714
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Appearance      =   0
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      RightToLeft     =   0   'False
      Sorted          =   0   'False
      Style           =   0
      IntegralHeight  =   0   'False
      MultiSelect     =   0
      Columns         =   0
   End
   Begin LISTBOXMODLibCtl.McVBListBox lstEdit 
      Height          =   435
      Index           =   0
      Left            =   12540
      TabIndex        =   19
      Top             =   5205
      Visible         =   0   'False
      Width           =   1470
      McVBListBox-InterfaceVersion=   1
      _cx             =   2593
      _cy             =   767
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Appearance      =   0
      Enabled         =   -1  'True
      MousePointer    =   0
      Object.TabStop         =   -1  'True
      RightToLeft     =   0   'False
      Sorted          =   0   'False
      Style           =   0
      IntegralHeight  =   0   'False
      MultiSelect     =   0
      Columns         =   0
   End
   Begin VB.TextBox txtEdit 
      Appearance      =   0  'Flat
      Height          =   375
      Index           =   0
      Left            =   10965
      TabIndex        =   16
      Top             =   5205
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox txtEdit 
      Appearance      =   0  'Flat
      Height          =   375
      Index           =   1
      Left            =   10935
      TabIndex        =   17
      Top             =   6120
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.PictureBox PicTITOLO 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   612
      Left            =   10500
      ScaleHeight     =   588
      ScaleWidth      =   3972
      TabIndex        =   32
      TabStop         =   0   'False
      Top             =   3840
      Visible         =   0   'False
      Width           =   4000
   End
   Begin VB.CommandButton Command4 
      Caption         =   "Reset Synchro  Wheel - Roller"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   10.8
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   480
      TabIndex        =   30
      Top             =   6960
      Visible         =   0   'False
      Width           =   5055
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Get Load/Unload Pos. Piece "
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Index           =   0
      Left            =   360
      TabIndex        =   28
      TabStop         =   0   'False
      Top             =   9720
      Visible         =   0   'False
      Width           =   5055
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Get Load  Pallet Pos. "
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Index           =   3
      Left            =   5640
      TabIndex        =   25
      Top             =   10200
      Visible         =   0   'False
      Width           =   5055
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Get Unload Pallet Pos."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Index           =   2
      Left            =   5640
      TabIndex        =   26
      TabStop         =   0   'False
      Top             =   9720
      Visible         =   0   'False
      Width           =   5055
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Get Load/Unload Spin-Drier "
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      Index           =   1
      Left            =   360
      TabIndex        =   27
      TabStop         =   0   'False
      Top             =   10200
      Visible         =   0   'False
      Width           =   5055
   End
   Begin VB.PictureBox pctWorking 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   465
      Left            =   14040
      ScaleHeight     =   444
      ScaleWidth      =   672
      TabIndex        =   12
      TabStop         =   0   'False
      Top             =   7800
      Visible         =   0   'False
      Width           =   690
   End
   Begin VB.PictureBox pctMessaggi 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   1110
      Left            =   10500
      ScaleHeight     =   1092
      ScaleWidth      =   3972
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   7920
      Visible         =   0   'False
      Width           =   4000
      Begin VB.VScrollBar scrMessaggi 
         Height          =   735
         Left            =   6300
         Max             =   300
         TabIndex        =   11
         Top             =   75
         Width           =   255
      End
   End
   Begin VB.PictureBox pctDisegni 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H80000005&
      BeginProperty Font 
         Name            =   "MS Song"
         Size            =   8.4
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   2880
      Left            =   11640
      ScaleHeight     =   2856
      ScaleWidth      =   1692
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   4980
      Visible         =   0   'False
      Width           =   1710
      Begin GAPP4PC.SuGrid SuGridDisegni 
         Height          =   2250
         Left            =   405
         TabIndex        =   13
         Top             =   225
         Visible         =   0   'False
         Width           =   1035
         _ExtentX        =   1820
         _ExtentY        =   3979
      End
   End
   Begin VB.CommandButton Command2 
      Caption         =   "IMPORT DATA"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   465
      TabIndex        =   23
      Top             =   7965
      Visible         =   0   'False
      Width           =   5055
   End
   Begin VB.PictureBox Picture2 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   885
      Left            =   375
      ScaleHeight     =   864
      ScaleWidth      =   5232
      TabIndex        =   24
      TabStop         =   0   'False
      Top             =   7800
      Visible         =   0   'False
      Width           =   5250
   End
   Begin VB.CommandButton Command1 
      Caption         =   "LOAD POSITION FOR SENSOR"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   10.8
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   375
      TabIndex        =   22
      Top             =   8880
      Visible         =   0   'False
      Width           =   5055
   End
   Begin VB.Timer OnlinePolling 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   6000
      Top             =   9000
   End
   Begin GAPP4PC.SuGrid SuGRID1 
      Height          =   855
      Index           =   2
      Left            =   10500
      TabIndex        =   2
      Top             =   6960
      Visible         =   0   'False
      Width           =   4000
      _ExtentX        =   7049
      _ExtentY        =   1503
   End
   Begin GAPP4PC.SuGrid SuGRID1 
      Height          =   915
      Index           =   1
      Left            =   10500
      TabIndex        =   1
      Top             =   5970
      Visible         =   0   'False
      Width           =   4000
      _ExtentX        =   7049
      _ExtentY        =   1609
   End
   Begin GAPP4PC.SuGrid SuGRID1 
      Height          =   735
      Index           =   0
      Left            =   10500
      TabIndex        =   0
      Top             =   5025
      Visible         =   0   'False
      Width           =   4000
      _ExtentX        =   7049
      _ExtentY        =   1291
   End
   Begin VB.Label lblSEQUENZA 
      Alignment       =   2  'Center
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "lblSEQUENZA"
      Height          =   252
      Left            =   6000
      TabIndex        =   31
      Top             =   7500
      Visible         =   0   'False
      Width           =   1000
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBE1D7&
      BorderStyle     =   1  'Fixed Single
      ForeColor       =   &H80000008&
      Height          =   852
      Left            =   240
      TabIndex        =   29
      Top             =   8760
      Width           =   5412
   End
   Begin VB.Label lblDefRU 
      Caption         =   "RU"
      BeginProperty Font 
         Name            =   "Arial CYR"
         Size            =   8.4
         Charset         =   204
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7500
      TabIndex        =   15
      Top             =   8500
      Visible         =   0   'False
      Width           =   1000
   End
   Begin VB.Label lblDefRUT 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Titolo RU"
      BeginProperty Font 
         Name            =   "Arial CYR"
         Size            =   8.4
         Charset         =   204
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   9000
      TabIndex        =   14
      Top             =   8500
      Visible         =   0   'False
      Width           =   1000
   End
   Begin VB.Label lblInfo 
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "lblInfo"
      Height          =   255
      Left            =   0
      TabIndex        =   8
      Top             =   7000
      Visible         =   0   'False
      Width           =   2000
   End
   Begin VB.Label lblModalita 
      Alignment       =   2  'Center
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "lblModalita"
      Height          =   255
      Left            =   3000
      TabIndex        =   7
      Top             =   7000
      Visible         =   0   'False
      Width           =   2000
   End
   Begin VB.Label lblDefCHT 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Titolo CH"
      BeginProperty Font 
         Name            =   "MS Song"
         Size            =   8.4
         Charset         =   134
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   9000
      TabIndex        =   6
      Top             =   8000
      Visible         =   0   'False
      Width           =   1000
   End
   Begin VB.Label lblDefCH 
      Caption         =   "CH"
      BeginProperty Font 
         Name            =   "MS Song"
         Size            =   8.4
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7500
      TabIndex        =   5
      Top             =   8000
      Visible         =   0   'False
      Width           =   1000
   End
   Begin VB.Label lblDefItaT 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Titolo"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.4
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   6000
      TabIndex        =   4
      Top             =   8500
      Visible         =   0   'False
      Width           =   1000
   End
   Begin VB.Label lblDefita 
      Caption         =   "Normale"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.4
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   6000
      TabIndex        =   3
      Top             =   8000
      Visible         =   0   'False
      Width           =   1000
   End
End
Attribute VB_Name = "SuOEM"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'SUOem Versione 17-01-11

Option Explicit

Const IniTemp = 1 '1 = Aggiorna il file ini temporaneo in modalit� INPUT
Const VK_TAB = &H9

Const LB_GETITEMHEIGHT = &H1A1
Const LB_SETITEMHEIGHT = &H1A0
Const CB_SETITEMHEIGHT = &H153

Public Event CNCChanged(ByVal TABELLA As String, ByVal VARIABILE As String, Valore As String, Caricamento As Boolean, ByVal CARICA_CALCOLO As Boolean)
Public Event OFFLINEChanged(ByVal TABELLA As String, ByVal VARIABILE As String, ByVal CARICA_CALCOLO As Boolean)

Private Status   As Integer '99=waitfor ack  100=Wait  0=Ok  1=Error 2=Timeout
Private Limiti   As String
Private TIPO     As String
Private TipoED   As String

Private m_lInfoBkColor As Long
Private iBKMode As Long

Private Type defPar
  PAR As Integer
  col As Integer
End Type

Private CaricamentoINCorso  As Boolean
Private VisParGrafico       As Boolean
'
Private Editing             As Boolean
Private Helping             As Boolean
Private SRig                As Integer
Private SCol                As Integer
Private KeyP                As String
Private ETyp                As String
Private MaxPagINS           As Integer
Private AltRiga             As Double
Private NRigPag             As Integer
'
Private FrecciaX            As Double
Private FrecciaY            As Double
Private tTop                As Single

Private MsgLastTitolo       As String
Private MsgLastTesto        As String
Private MsgLastPicture      As String
Private MsgLastTipo         As String

Private ListItemSelected    As String
Private lastFocused         As Integer
Private Livello_I           As String

Private ForzaGruppo         As Integer

Private FontNormale         As New StdFont
Private FontNormaleTitolo   As New StdFont
Private FontCinese          As New StdFont
Private FontCineseTitolo    As New StdFont
Private FontRusso           As New StdFont
Private FontRussoTitolo     As New StdFont

Private CurFontNor          As New StdFont
Private CurFontTit          As New StdFont
Private sequenza            As String

Private ColoreSfondo           As OLE_COLOR
Private ColoreTesto            As OLE_COLOR
Private ColoreRigaSelezionata  As OLE_COLOR
Private ColoreCellaSelezionata As OLE_COLOR
Private ColoreTitolo           As OLE_COLOR
Private ColoreGriglia          As OLE_COLOR
Private ColoreHelp             As OLE_COLOR
Private ColoreLimiti           As OLE_COLOR

Private Command3_Selezionato As Boolean

Private MostraVar            As Boolean

Sub SCRITTURA_VALORI_ONLINE(ByVal ii As Integer, Valore As String)
'
Dim jj        As Integer
Dim tstr      As String
Dim VARIABILE As String
Dim L         As Integer
'
On Error GoTo ErrSCRITTURA_VALORI_ONLINE

  For jj = 1 To Tabella1.Parametri(ii).NActual
    If (Trim((Tabella1.Parametri(ii).ITEMDDE(jj))) <> "") Then
      Dim Controllo        As Long
      Dim DDE_BASE         As String
      Dim VAL_BASE1        As Integer
      Dim VAL_BASE2        As Integer
      Dim N                As Integer
      Dim jj1              As String
      Dim jj2              As String
      Dim LIMITI_VALORI()  As String
      Dim LIMITI_SIMBOLI() As String
      tstr = Tabella1.Parametri(ii).VARIABILE(0)
      tstr = Trim$(UCase$(tstr))
      LIMITI_VALORI = Split(Tabella1.Parametri(ii).Limiti, ",")
      LIMITI_SIMBOLI = Split(Tabella1.Parametri(ii).Limiti_Lett, ",")
      Controllo = InStr(tstr, "BASE")
      If (Controllo > 0) Then
        jj1 = InStr(tstr, "|")
        jj2 = InStr(tstr, ",")
        DDE_BASE = Mid$(tstr, jj1 + 1, jj2 - jj1 - 1)
        VAL_BASE1 = OPC_LEGGI_DATO(DDE_BASE)
        'I VALORI FISSI NEL DATABASE DEVONO ESSERE "0" ED "1"
        If (VAL_BASE1 = 0) Then
          VAL_BASE2 = 0
        Else
          VAL_BASE2 = val(Mid$(tstr, jj2 + 1))
        End If
        If (Tabella1.MODALITA = "SPF") Then
          '+2 perch� l'indirizzamento tramite OPC parte da 1
          N = Asc(UCase(Tabella1.LetteraLavorazione)) - Asc("A") + 2
          N = N + VAL_BASE2
          tstr = Tabella1.Parametri(ii).ITEMDDE(jj)
          'tstr = Replace(tstr, "*", N)
          jj1 = InStr(tstr, "[")
          jj2 = InStr(tstr, "]")
          tstr = Left$(tstr, jj1) & N & Right$(tstr, Len(tstr) - jj2 + 1)
          VARIABILE = tstr
          L = InStr(VARIABILE, "/")
          While (L > 0)
            VARIABILE = Right$(VARIABILE, Len(VARIABILE) - L)
            L = InStr(VARIABILE, "/")
          Wend
          If (Valore = "DEFAULT") Then
            Call OPC_SCRIVI_DATO(tstr, Tabella1.Parametri(ii).Default)
            Tabella1.Parametri(ii).Actual(jj) = Tabella1.Parametri(ii).Default
          Else
            Call OPC_SCRIVI_DATO(tstr, Valore)
            Tabella1.Parametri(ii).Actual(jj) = Valore
          End If
          If (Len(Tabella1.Parametri(ii).VARIABILE(jj)) > 0) Then
          L = WritePrivateProfileString("CNC_" & Tabella1.LetteraLavorazione, Tabella1.Parametri(ii).VARIABILE(jj), Tabella1.Parametri(ii).Actual(jj), Tabella1.PathOEM & Tabella1.NOME & ".INI")
          End If
        End If
      Else
        tstr = DDE_Idx(Tabella1.Parametri(ii).ITEMDDE(jj))
        VARIABILE = tstr
        L = InStr(VARIABILE, "/")
        While (L > 0)
          VARIABILE = Right$(VARIABILE, Len(VARIABILE) - L)
          L = InStr(VARIABILE, "/")
        Wend
        If (Valore = "DEFAULT") Then
          Call OPC_SCRIVI_DATO(tstr, Tabella1.Parametri(ii).Default)
          Tabella1.Parametri(ii).Actual(jj) = Tabella1.Parametri(ii).Default
        Else
          Call OPC_SCRIVI_DATO(tstr, Valore)
          Tabella1.Parametri(ii).Actual(jj) = Valore
        End If
        If (Len(Tabella1.Parametri(ii).VARIABILE(jj)) > 0) Then
        L = WritePrivateProfileString("CNC_" & Tabella1.LetteraLavorazione, Tabella1.Parametri(ii).VARIABILE(jj), Tabella1.Parametri(ii).Actual(jj), Tabella1.PathOEM & Tabella1.NOME & ".INI")
        End If
      End If
      '
      For L = 0 To 2
        tstr = DDE_Idx(Tabella1.Parametri(ii).ITEMDDE(jj))
        If (SuGrid1(L).GetKeyCellaByVar(tstr) <> "") Then
          SuGrid1(L).GetCellaByVar(tstr).Value = Tabella1.Parametri(ii).Actual(jj)
          Exit For
        End If
      Next L
      '
    End If
  Next jj
  '
Exit Sub

ErrSCRITTURA_VALORI_ONLINE:
  WRITE_DIALOG "ERROR " & Err & "ON SUB SCRITTURA_VALORI_ONLINE"
  
End Sub

Sub NUOVA_MOLA(Index As Integer)
'
Dim tstr    As String
Dim cell    As Cella
Dim i       As Integer
Dim VScr    As Double
Dim j       As Integer
Dim tWidth  As Double
'
On Error Resume Next
  '
  tstr = SuGrid1(Index).Selected(False)
  If (tstr = "") Then Exit Sub
  '
  Set cell = SuGrid1(Index).Celle(tstr)
  '
  TipoED = "TEXT"
  '
  txtEdit(Index).Top = SuGrid1(Index).Top + cell.vTop
  txtEdit(Index).Height = cell.Height
  txtEdit(Index).Left = SuGrid1(Index).Left + cell.vLeft
  txtEdit(Index).Width = cell.Width
  txtEdit(Index).BackColor = cell.SelColor
  If cell.Correttore Then
    txtEdit(Index).Text = cell.ValueCorr
  Else
    txtEdit(Index).Text = cell.Value
  End If
  '
  txtEdit(Index).Text = ""
  '
  txtEdit(Index).Visible = True
  txtEdit(Index).SetFocus
  txtEdit(Index).SelStart = 0
  txtEdit(Index).SelLength = Len(txtEdit(Index).Text)
  '
  Set cell = Nothing
  '
  Call DisegnaOEM
  '
End Sub

Sub EditDisEnableDIRETTO(Index As Integer, tNewVal As String)
'
Dim tstr    As String
Dim cell    As Cella
Dim tScroll As Integer
'
On Error Resume Next
  '
  tScroll = SuGrid1(Index).VScroll1_Value
  '
  tstr = SuGrid1(Index).Selected
  If (tstr = "") Then Exit Sub
  '
  Set cell = SuGrid1(Index).Celle(tstr)
  If cell.Correttore Then
    Call SuGrid1_EditDisEnabling(Index, cell.Key, cell.VARIABILE & "_CORR", tNewVal)
  Else
    Call SuGrid1_EditDisEnabling(Index, cell.Key, cell.VARIABILE, tNewVal)
  End If
  Set cell = Nothing
  '
End Sub

Function VARIABILE_SELEZIONATA() As String
'
Dim tstr As String
Dim cell As Cella
'
On Error Resume Next
  '
  VARIABILE_SELEZIONATA = ""
  tstr = SuGrid1(SCon).Selected
  If (tstr = "") Then Exit Function
  '
  Set cell = SuGrid1(SCon).Celle(tstr)
  VARIABILE_SELEZIONATA = cell.VARIABILE
  Set cell = Nothing
  '
End Function

Sub EditDisEnable(Index As Integer)
'
Dim tstr    As String
Dim cell    As Cella
Dim tNewVal As String
Dim tScroll As Integer
'
On Error Resume Next
  '
  Select Case TipoED
    Case "TEXT"
      tNewVal = txtEdit(Index).Text
      Call CALCOLA_ESPRESSIONE_ESTESA(tNewVal)
      
    Case "LIST"
      tNewVal = Trim$(lstEdit(Index).Text)
  End Select
  'tScroll = SuGrid1(Index).VScroll1_Value
  '
  tstr = SuGrid1(Index).Selected
  If (tstr = "") Then Exit Sub
  '
  Set cell = SuGrid1(Index).Celle(tstr)
  If (TipoED = "BOOL") Then tNewVal = cell.Value
  Status = 99
  If cell.Correttore Then
    Call SuGrid1_EditDisEnabling(Index, cell.Key, cell.VARIABILE & "_CORR", tNewVal)
  Else
    Call SuGrid1_EditDisEnabling(Index, cell.Key, cell.VARIABILE, tNewVal)
  End If
  If (Status = 0) Then
    If cell.Correttore Then
      cell.ValueCorr = tNewVal
    Else
      cell.Value = tNewVal
    End If
    SuGrid1(Index).SetFocus
    Select Case TipoED
      Case "TEXT":  txtEdit(Index).Visible = False
      Case "LIST":  lstEdit(Index).Visible = False
    End Select
    'SuGrid1(Index).VScroll1_Value = tScroll
  End If
  Set cell = Nothing
  '
End Sub

Private Sub ListFill(Index As Integer, Limiti As String, Current As String, Optional ListIdx As Integer = -1)
'
Dim tArr()  As String
Dim i       As Integer
Dim j       As Integer
'
On Error Resume Next
  '
  tArr = Split(Limiti, ",")
  lstEdit(Index).Clear
  For i = 0 To UBound(tArr)
    Call lstEdit(Index).AddItem(" " & tArr(i))
    If UCase(Trim(tArr(i))) = UCase(Trim(Current)) Then j = i
  Next i
  lstEdit(Index).ListIndex = j
  ListIdx = j

End Sub

Private Sub RaiseEvent_PaginaChanged(ByVal TABELLA As String, ByVal VARIABILE As String, Valore As String, Pagina As Integer)
'
On Error Resume Next
  '
  If (VARIABILE = "iTipoVis") Then Disegni.CurrVis = Valore
  Select Case Tabella1.NOME
    Case "MOLAVITE":                  Call OEMX_PaginaChanged_MOLAVITE(TABELLA, VARIABILE, Valore, Pagina)
    Case "MOLAVITE_SG":               Call OEMX_PaginaChanged_MOLAVITE_SG(TABELLA, VARIABILE, Valore, Pagina)
    Case "DENTATURA":                 Call OEMX_PaginaChanged_DENTATURA(TABELLA, VARIABILE, Valore, Pagina)
    Case "INGR380_ESTERNI":           Call OEMX_PaginaChanged_INGRANAGGI_ESTERNI(TABELLA, VARIABILE, Valore, Pagina)
    Case "INGRANAGGI_ESTERNIV":       Call OEMX_PaginaChanged_INGRANAGGI_ESTERNIV(TABELLA, VARIABILE, Valore, Pagina)
    Case "INGRANAGGI_ESTERNIO":       Call OEMX_PaginaChanged_INGRANAGGI_ESTERNIO(TABELLA, VARIABILE, Valore, Pagina)
    Case "SCANALATIEVO":              Call OEMX_PaginaChanged_INGRANAGGI_ESTERNI(TABELLA, VARIABILE, Valore, Pagina)
    Case "SCANALATI_ESTERNI":         Call OEMX_PaginaChanged_SCANALATI_ESTERNI(TABELLA, VARIABILE, Valore, Pagina)
    Case "SCANALATI_CBN":             Call OEMX_PaginaChanged_SCANALATI_CBN(TABELLA, VARIABILE, Valore, Pagina)
    Case "SCANALATI_INTERNI_CBN":     Call OEMX_PaginaChanged_SCANALATI_INTERNI_CBN(TABELLA, VARIABILE, Valore, Pagina)
    Case "INGRANAGGI_INTERNI":        Call OEMX_PaginaChanged_INGRANAGGI_INTERNI(TABELLA, VARIABILE, Valore, Pagina)
    Case "ROTORI_ESTERNI":            Call OEMX_PaginaChanged_ROTORI_ESTERNI(TABELLA, VARIABILE, Valore, Pagina)
    Case "VITI_ESTERNE":              Call OEMX_PaginaChanged_VITI_ESTERNE(TABELLA, VARIABILE, Valore, Pagina)
    Case "VITIV_ESTERNE":             Call OEMX_PaginaChanged_VITIV_ESTERNE(TABELLA, VARIABILE, Valore, Pagina)
    Case "PROFILO_PER_PUNTI_ESTERNI": Call OEMX_PaginaChanged_PROFILO_PER_PUNTI_ESTERNI(TABELLA, VARIABILE, Valore, Pagina)
    Case "PROFILO_RAB_ESTERNI":       Call OEMX_PaginaChanged_PROFILO_RAB_ESTERNI(TABELLA, VARIABILE, Valore, Pagina)
    Case "PROFILO_PER_PUNTI_INTERNI": Call OEMX_PaginaChanged_PROFILO_PER_PUNTI_INTERNI(TABELLA, VARIABILE, Valore, Pagina)
  End Select
  '
End Sub

Public Sub STAMPA_TESTO(Optional FineDocumento As Boolean = False, Optional YStart As Single = -1)
'
Dim MLeft             As Single
Dim tstr()            As String
Dim tLeft             As Single
Dim i                 As Integer
Dim tBold             As Boolean
Dim TESTO             As String
Dim Titolo            As String
Dim MODALITA          As String
Dim TIPO_LAVORAZIONE  As String
Dim NOME_PEZZO        As String
Dim DB                As Database
Dim TB                As Recordset
'
On Error Resume Next
  '
  MLeft = 10
  '
  'LEGGO LA MODALITA DI TRASFERIMENTO DATI
  MODALITA = Tabella1.MODALITA
  MODALITA = UCase$(Trim$(MODALITA))
  'LEGGO IL TIPO DI LAVORAZIONE
  TIPO_LAVORAZIONE = Tabella1.NOME
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  'LETTURA DEL NOME PEZZO
  If (MODALITA = "OFF-LINE") Then
    NOME_PEZZO = GetINI(Tabella1.IniCfgLav, Tabella1.NOME, "NomePezzoInserimento")
  Else
    NOME_PEZZO = Tabella1.NomePezzo
  End If
  'APRO IL DATABASE
  Set DB = OpenDatabase(PathDB, True, False)
  Set TB = DB.OpenRecordset("ARCHIVIO_" & TIPO_LAVORAZIONE)
  'RICERCO NELLA TABELLA ARCHIVIO SE ESISTE UNA ARCHIVIAZIONE
  TB.Index = "NOME_PEZZO"
  TB.Seek "=", NOME_PEZZO
  If Not TB.NoMatch Then
    If IsNull(TB.Fields("MEMO").Value) Then
      TESTO = ""
    Else
      TESTO = TB.Fields("MEMO").Value
    End If
  Else
    TESTO = ""
  End If
  'CHIUDO IL DATABASE
  TB.Close
  DB.Close
  '
  Titolo = "MEMO"
  '
  If ("" & UT(TESTO) = "") Then TESTO = vbCrLf & NOME_PEZZO & vbCrLf
  '
  tstr = Split(TESTO, vbCrLf)
  tBold = Printer.FontBold
  Printer.FontBold = True
  Printer.CurrentX = MLeft
  If (YStart <> -1) Then
    Printer.CurrentY = YStart
  Else
    Printer.CurrentY = tTop
  End If
  Printer.Print Titolo
  Printer.FontBold = tBold
  '
  tLeft = MLeft + Printer.TextWidth(vbTab) / 2
  '
  For i = 0 To UBound(tstr)
    Printer.CurrentX = tLeft
    Printer.Print tstr(i)
  Next i
  '
  tTop = Printer.CurrentY
  '
  If (FineDocumento = True) Then Printer.EndDoc
  '
End Sub
'
' Scrive intestazione e restituisce come output
' la posizione Y di inizio della lista (TopLista)
Sub STAMPA_INTESTAZIONE(iPAG As Integer, Npag As Integer, addPAG As Integer, ByRef TopLista As Single, Optional StampaNomePagina As Boolean = True, _
                        Optional PagXdiY As String = "1/1")
'
Dim MLeft             As Single
Dim MTop              As Single
Dim Larghezza         As Single
Dim WLine             As Integer
Dim WLogo             As Single
Dim HLogo             As Single
Dim iRG               As Single
Dim iCl               As Single
Dim stmp              As String
Dim riga              As String
Dim X1                As Single
Dim Y1                As Single
Dim HSpazi            As Double
Dim MODALITA          As String
Dim TIPO_LAVORAZIONE  As String
Dim NOME_TIPO         As String
Dim TABELLA_ARCHIVIO  As String
Dim DB                As Database
Dim TB                As Recordset
Dim DATA_SALVATAGGIO  As String
'
On Error GoTo errSTAMPA_INTESTAZIONE
  '
  'LEGGO LA MODALITA DI TRASFERIMENTO DATI
  MODALITA = Tabella1.MODALITA
  MODALITA = UCase$(Trim$(MODALITA))
  'LEGGO IL TIPO DI LAVORAZIONE
  TIPO_LAVORAZIONE = Tabella1.NOME
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  'RICERCA DEL NOME TIPO DI LAVORAZIONE
  NOME_TIPO = Tabella1.Info
  If (Not StampaNomePagina) And (NOME_TIPO <> "") Then NOME_TIPO = Split(NOME_TIPO, ":")(0)
  'IMPOSTAZIONI PER LA STAMPA
  MLeft = 10
  MTop = 5 + tTop
  Larghezza = 190
  WLine = 10
  'Dimensioni delle rige e colonne per ciascun carattere
  iRG = Printer.TextHeight("A")
  iCl = Printer.TextWidth("A")
  WLogo = 15
  HLogo = 15
  'Posiziono il cursore sulla stampante
  X1 = MLeft
  Y1 = MTop
  If (Dir$(g_chOemPATH & "\" & "Logo2.bmp") <> "") Then
    Printer.PaintPicture LoadPicture(g_chOemPATH & "\" & "Logo2.bmp"), X1, Y1, WLogo, HLogo
  End If
  HSpazi = HLogo - 2 * iRG
  HSpazi = Int(HSpazi / 3 * 1000) / 1000
  'INTESTAZIONE DELLA PAGINA: riga 1
  stmp = NOME_TIPO
  Printer.CurrentX = X1 + iCl + WLogo
  Printer.CurrentY = Y1 + HSpazi
  Printer.FontBold = True
  Printer.Print stmp
  Printer.FontBold = False
  'INTESTAZIONE DELLA PAGINA: riga 2
  riga = GetInfo("Configurazione", "AZIENDA", PathFILEINI)
  stmp = riga & " "
  If (MODALITA = "OFF-LINE") Then
    'LETTURA DEL NOME PEZZO
    riga = GetINI(Tabella1.IniCfgLav, Tabella1.NOME, "NomePezzoInserimento")
    'NOME DELLA TABELLA DI ARCHIVIAZIONE
    TABELLA_ARCHIVIO = TIPO_LAVORAZIONE
    TABELLA_ARCHIVIO = "ARCHIVIO_" & TABELLA_ARCHIVIO
    'APRO IL DATABASE
    Set DB = OpenDatabase(PathDB, True, False)
    Set TB = DB.OpenRecordset(TABELLA_ARCHIVIO)
    'RICERCO NELLA TABELLA ARCHIVIO SE ESISTE UNA ARCHIVIAZIONE
    TB.Index = "NOME_PEZZO"
    TB.Seek "=", riga
    If Not TB.NoMatch Then DATA_SALVATAGGIO = TB.Fields("DATA_ARCHIVIAZIONE").Value
    'CHIUDO IL DATABASE
    TB.Close
    DB.Close
    'INTESTAZIONE
    stmp = stmp & riga & " " & DATA_SALVATAGGIO
  Else
    'LETTURA DEL NOME PEZZO
    riga = Tabella1.NomePezzo
    'INTESTAZIONE
    stmp = stmp & " " & Format$(Now, "dd/mm/yyyy")
    stmp = stmp & " " & Format$(Now, "hh:mm:ss")
  End If
  Printer.CurrentX = X1 + iCl + WLogo
  Printer.CurrentY = Y1 + iRG + 2 * HSpazi
  Printer.FontBold = True
  Printer.Print stmp
  Printer.FontBold = False
  'STAMPA DELLA MODALITA
  If (MODALITA = "OFF-LINE") Then stmp = "OFF-LINE" Else stmp = "CNC " & Tabella1.LetteraLavorazione
  X1 = Larghezza - Printer.TextWidth(stmp) - iCl
  Y1 = Y1 + 0
  Printer.CurrentX = X1
  Printer.CurrentY = Y1 + HSpazi
  Printer.Print stmp
  'STAMPA DEL NUMERO DI PAGINA
  stmp = "pg. " & PagXdiY
  Printer.CurrentX = Larghezza - Printer.TextWidth(stmp) - iCl
  Printer.CurrentY = Y1 + iRG + 2 * HSpazi
  Printer.Print stmp
  'SEPARATORE INTESTAZIONE
  Printer.DrawWidth = WLine
  Printer.Line (MLeft, MTop + HLogo)-(Larghezza, MTop + HLogo)
  Printer.DrawWidth = 1
  'INCREMENTO POSIZIONE INIZIALE
  TopLista = MTop + HLogo + iRG
  '
Exit Sub

errSTAMPA_INTESTAZIONE:
  If (FreeFile > 0) Then Close
  WRITE_DIALOG "Sub STAMPA_INTESTAZIONE: ERROR -> " & Err
  Exit Sub
      
End Sub

Public Sub STAMPA(Optional FineDocumento As Boolean = True, Optional INTESTAZIONE As Boolean = True, Optional StampaNomePagina As Boolean = True, _
                  Optional PagXdiY As String = "1/1", Optional DeltaY As Double = 0)
'
Dim i         As Integer
Dim PrnScale  As Single
Dim MaxY      As Single
Dim MaxYDX    As Single
Dim MaxYSX    As Single
Dim tMax      As Single
'
On Error Resume Next
  '
  For i = 1 To 2
    Printer.ScaleWidth = 200
    Printer.ScaleHeight = 275
    Printer.FontSize = 10
  Next i
  '
  StampaTop = 0
  StampaLeft = 10
  StampaHeight = 0
  StampaWidth = 0
  '
  If (INTESTAZIONE = True) Then
    tTop = DeltaY
    Call STAMPA_INTESTAZIONE(1, 1, 0, tTop, StampaNomePagina, PagXdiY)
  End If
  '
  'Forzata la trasparenza del font in stampa
  'SVG231012: DEVO TROVARE LA LIBRERIA
  'iBKMode = SetBkMode(Printer.hdc, TRANSPARENT)
  '
  PrnScale = (Printer.ScaleWidth - 19) / UserControl.ScaleWidth
  '
  MaxYDX = tTop: MaxYSX = tTop
  '
  i = 0
  If SuGrid1(i).Visible Then
    Call SuGrid1(i).STAMPA_GRID(MaxYSX, 10 + SuGrid1(i).Left * PrnScale, (SuGrid1(i).Width * PrnScale) - 1)
    MaxYSX = MaxYSX + (SuGrid1(i).CellHeight * (SuGrid1(i).ListCount) * PrnScale)
  End If
  '
  i = 1
  If SuGrid1(i).Visible Then
    Call SuGrid1(i).STAMPA_GRID(MaxYDX, 10 + SuGrid1(i).Left * PrnScale, (SuGrid1(i).Width * PrnScale) - 1)
    MaxYDX = MaxYDX + (SuGrid1(i).CellHeight * (SuGrid1(i).ListCount) * PrnScale)
  End If
  '
  'MODIFICA PROVVISORIA PER NON STAMPARE I CORRETTORI
  'SVG150916: DEVO FARE UNA GESTIONE CONFIGURABILE NEL FILA INI
  If (ActualGroup <> "OEM1_MOLA71") Then
  i = 2
  If SuGrid1(i).Visible Then
    Call SuGrid1(i).STAMPA_GRID(MaxYDX, 10 + SuGrid1(i).Left * PrnScale, (SuGrid1(i).Width * PrnScale) - 1)
    MaxYDX = MaxYDX + (SuGrid1(i).CellHeight * (SuGrid1(i).ListCount) * PrnScale)
  End If
  End If
  '
  If (pctDisegni.Visible = True) Then
    StampaTop = MaxYDX
    StampaLeft = 10 + PictureDisegni.Left * PrnScale
    StampaHeight = PictureDisegni.Height * PrnScale
    StampaWidth = (PictureDisegni.Width * PrnScale) - 1
    MaxYDX = MaxYDX + StampaHeight + 2.5 ' 2.5 = Margine dopo la stampa di un disegno
    Call Disegni.RidisegnaPagina(PaginaINS, True)
  End If
  '
  If (MaxYDX > MaxYSX) Then tTop = MaxYDX Else tTop = MaxYSX
  '
  If (FineDocumento = True) Then Printer.EndDoc
  '
End Sub

Public Function EsportaTabella(Optional Separatore As String = ";") As String
'
Dim i     As Integer
Dim j     As Integer
Dim tstr  As String
'
On Error GoTo EsportaTabella_Error
  '
  tstr = ""
  If (Tabella1.Caricata = False) Then
    EsportaTabella = ""
    Exit Function
  End If
  For i = 1 To UBound(Tabella1.Parametri)
    For j = 1 To UBound(Tabella1.Parametri(i).Actual)
      If UCase(Trim(Tabella1.Parametri(i).VARIABILE(j))) <> "" Then
        tstr = tstr & Tabella1.Parametri(i).VARIABILE(j) & "=" & Tabella1.Parametri(i).Actual(j) & ";"
      End If
    Next j
  Next i
  EsportaTabella = tstr
  '
Exit Function

EsportaTabella_Error:
  MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Function: EsportaTabella"
  Resume Next

End Function

'---------------------------------------------------------------------------------------
' Procedura : righe
' Data e ora: 25/07/2007 08.44
' Autore    : Fil
' Note      : Restituisce il numero di righe della sezione (0, 1 ,2) "Compreso il titolo"
'---------------------------------------------------------------------------------------

Public Function RIGHE(Sez012 As Integer) As Integer
'
On Error GoTo Righe_Error
  '
  If (Sez012 > 2) Then
    RIGHE = 0
    Exit Function
  End If
  RIGHE = SuGrid1(Sez012).ListCount
  '
Exit Function

Righe_Error:
  MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Function: Righe"
  Exit Function
  
End Function

Function GetSequenza(Pagina As Integer) As String
'
Dim i       As Integer
Dim j       As Integer
Dim k       As Integer
Dim L       As Integer
Dim tstr2   As String
Dim SeqPag  As String
Dim SubSeg  As String
Dim tstr()  As String
Dim tstr3() As String
'
On Error GoTo GetSequenza_Error
  '
  GetSequenza = ""
  SeqPag = Tabella1.Pagine(Pagina).sequenza
  While SeqPag Like "*{*}*"
    i = InStr(1, SeqPag, "{") + 1
    j = InStr(i, SeqPag, "}")
    k = i - 2
    tstr2 = Mid(SeqPag, k, i - k - 1)
    While IsNumeric(Mid(SeqPag, k, 1)) Or (Mid(SeqPag, k, 1) = "-")
      k = k - 1
    Wend
    k = k + 1
    tstr2 = Mid(SeqPag, k, i - k - 1)
    If tstr2 <> "" Then
      tstr2 = Split(tstr2, "-")(0)
    End If
    k = GetParByINDICE(tstr2)
    tstr = Split(Mid(SeqPag, i, j - i), "|")
    tstr3 = Split(Tabella1.Parametri(k).Limiti, ",")
    tstr2 = ""
    For L = 0 To UBound(tstr3)
      If UCase(tstr3(L)) = UCase(Tabella1.Parametri(k).Actual(1)) Then
        tstr2 = tstr(L)
        Exit For
      End If
    Next L
    SeqPag = Left(SeqPag, i - 2) & tstr2 & Right(SeqPag, Len(SeqPag) - j)
  Wend
  While SeqPag Like "*(*)*"
    i = InStr(1, SeqPag, "(") + 1
    j = InStr(i, SeqPag, ")")
    k = i - 2
    tstr2 = Mid(SeqPag, k, i - k - 1)
    While IsNumeric(Mid(SeqPag, k, 1)) Or (Mid(SeqPag, k, 1) = "-")
      k = k - 1
    Wend
    k = k + 1
    tstr2 = Mid(SeqPag, k, i - k - 1)
    If tstr2 <> "" Then
      tstr2 = Split(tstr2, "-")(0)
    End If
    k = GetParByINDICE(tstr2)
    tstr = Split(Mid(SeqPag, i, j - i), ";")
    tstr2 = ""
    If tstr(0) Like ">=*" Then
      tstr(0) = Right(tstr(0), Len(tstr(0)) - 2)
      If IsNumeric(Tabella1.Parametri(k).Actual(1)) Then
        If (val((Tabella1.Parametri(k).Actual(1))) >= val(tstr(0))) Then tstr2 = tstr(1)
      End If
    ElseIf tstr(0) Like "<=*" Then
      tstr(0) = Right(tstr(0), Len(tstr(0)) - 2)
      If IsNumeric(Tabella1.Parametri(k).Actual(1)) Then
        If (val((Tabella1.Parametri(k).Actual(1))) <= val(tstr(0))) Then tstr2 = tstr(1)
      End If
    ElseIf tstr(0) Like ">*" Then
      tstr(0) = Right(tstr(0), Len(tstr(0)) - 1)
      If IsNumeric(Tabella1.Parametri(k).Actual(1)) Then
        If (val((Tabella1.Parametri(k).Actual(1))) > val(tstr(0))) Then tstr2 = tstr(1)
      End If
    ElseIf tstr(0) Like "<*" Then
      tstr(0) = Right(tstr(0), Len(tstr(0)) - 1)
      If IsNumeric(Tabella1.Parametri(k).Actual(1)) Then
        If (val((Tabella1.Parametri(k).Actual(1))) < val(tstr(0))) Then tstr2 = tstr(1)
      End If
    ElseIf tstr(0) Like "=*" Then
      tstr(0) = Right(tstr(0), Len(tstr(0)) - 1)
      If IsNumeric(Tabella1.Parametri(k).Actual(1)) Then
        If (val((Tabella1.Parametri(k).Actual(1))) = val(tstr(0))) Then tstr2 = tstr(1)
      End If
    End If
    SeqPag = Left(SeqPag, i - 2) & tstr2 & Right(SeqPag, Len(SeqPag) - j)
  Wend
  NRigPag = 0
  For i = 1 To Len(SeqPag)
    If Mid(SeqPag, i, 1) Like ("[A-Z]") Then
      NRigPag = NRigPag + 1
    End If
  Next i
  NPagine = MaxPagINS
  GetSequenza = SeqPag
  '
Exit Function

GetSequenza_Error:
  MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Function: GetSequenza"
  'Resume Next
  Exit Function
  
End Function

Function EstraiDaSeq(ByRef Seq As String) As String
'
Dim tstr As String
'
On Error Resume Next
  '
  tstr = Left(Seq, 1)
  Seq = Right(Seq, Len(Seq) - 1)
  While IsNumeric(Left(Seq, 1))
    tstr = tstr & Left(Seq, 1)
    Seq = Right(Seq, Len(Seq) - 1)
  Wend
  EstraiDaSeq = tstr
  If Left(Seq, 1) = "-" Then
    tstr = ""
    Seq = Right(Seq, Len(Seq) - 1)
    While IsNumeric(Left(Seq, 1))
      tstr = tstr & Left(Seq, 1)
      Seq = Right(Seq, Len(Seq) - 1)
    Wend
    If IsNumeric(tstr) Then
      ForzaGruppo = val(tstr)
    Else
      ForzaGruppo = -1
    End If
  Else
    ForzaGruppo = -1
  End If
  '
End Function

Function GetParByINDICE(INDICE As String) As Integer
'
Dim i As Integer
'
On Error GoTo GetParByINDICE_Error
  '
  GetParByINDICE = 0
  For i = 1 To UBound(Tabella1.Parametri)
    If (Tabella1.Parametri(i).INDICE = INDICE) Then
      GetParByINDICE = i
      Exit Function
    End If
  Next i
  '
Exit Function

GetParByINDICE_Error:
  MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Function: GetParByINDICE"
  
End Function

Public Sub Evidenzia(VARIABILE As String)
'
Dim tpar As defPar
'
On Error Resume Next
  '
  tpar = GetParByVar(VARIABILE)
  If (tpar.PAR <> 0) And (tpar.col <> 0) Then
    Tabella1.Parametri(tpar.PAR).Evidenzia(tpar.col) = True
  End If
  '
End Sub

Private Function GetParByVar(VARIABILE As String) As defPar
'
Dim i       As Integer
Dim j       As Integer
Dim tOnline As Boolean
Dim PAR     As defPar
'
On Error GoTo errGetParByVar
  '
  PAR.PAR = 0: PAR.col = 0
  GetParByVar = PAR
  If (Trim(VARIABILE) = "") Then Exit Function
  '
  If (Left(VARIABILE, 1) = "/") Then tOnline = True Else tOnline = False
  '
  For i = 1 To UBound(Tabella1.Parametri)
    For j = 1 To Tabella1.Parametri(i).NActual
      If tOnline Then
        If UCase(DDE_Idx(Trim(Tabella1.Parametri(i).ITEMDDE(j)))) = UCase(Trim(VARIABILE)) Then
          PAR.PAR = i: PAR.col = j
          GetParByVar = PAR
          Exit Function
        End If
      Else
        If UCase(Trim(Tabella1.Parametri(i).VARIABILE(j))) = UCase(Trim(VARIABILE)) Then
          PAR.PAR = i: PAR.col = j
          GetParByVar = PAR
          Exit Function
        End If
      End If
    Next j
  Next i
  '
Exit Function

errGetParByVar:
  Err = 0
  Exit Function
  
End Function

Private Function GetOptByVar(VARIABILE As String) As defPar
'
Dim i       As Integer
Dim j       As Integer
Dim tOnline As Boolean
Dim PAR     As defPar
'
On Error Resume Next
  '
  PAR.PAR = 0
  PAR.col = 0
  GetOptByVar = PAR
  If Trim(VARIABILE) = "" Then Exit Function
  If Left(VARIABILE, 1) = "/" Then
    tOnline = True
  Else
    tOnline = False
  End If
  For i = 1 To UBound(Tabella1.Opzioni)
    For j = 1 To Tabella1.Opzioni(i).NActual
      If tOnline Then
        If UCase(DDE_Idx(Trim(Tabella1.Opzioni(i).ITEMDDE(j)))) = UCase(Trim(VARIABILE)) Then
          PAR.PAR = i
          PAR.col = j
          GetOptByVar = PAR
          Exit Function
        End If
      Else
        If UCase(Trim(Tabella1.Opzioni(i).VARIABILE(j))) = UCase(Trim(VARIABILE)) Then
          PAR.PAR = i
          PAR.col = j
          GetOptByVar = PAR
          Exit Function
        End If
      End If
    Next j
  Next i
  '
End Function

Private Function GetTabState() As Boolean
'
On Error Resume Next
  '
  GetTabState = False
  'If GetKeyState(VK_TAB) And -256 Then
  '  GetTabState = True
  'End If
  '
End Function

Private Function GetShiftState() As Boolean
'
On Error Resume Next
  '
  GetShiftState = False
  'If GetKeyState(vbKeyShift) And -256 Then
  '  GetShiftState = True
  'End If
  '
End Function

Function Get_Lista(ByVal Gruppo As Integer, ByVal riga As Integer, ByVal COLONNA As Integer) As String
'
Dim tpar    As defPar
Dim tGruppo As Integer
Dim i       As Integer
Dim j       As Integer
'
On Error Resume Next
  '
  tGruppo = IIf(Gruppo = 0, Gruppo, Gruppo - 1)
  i = riga + 1
  j = COLONNA
  Get_Lista = SuGrid1(tGruppo).GetCella(i & "," & j).Value
  '
End Function

Sub InizializzaVariabili()
'
Dim i As Integer
'
On Error GoTo InizializzaVariabili_Error
  '
  SCon = 0
  '
  Tabella1.INTESTAZIONE(0) = ""
  Tabella1.INTESTAZIONE(1) = ""
  Tabella1.INTESTAZIONE(2) = ""
  Tabella1.INTESTAZIONE(3) = ""
  '
  Tabella1.Altezza(0) = 0
  Tabella1.Altezza(1) = 0
  Tabella1.Altezza(2) = 0
  '
Exit Sub

InizializzaVariabili_Error:
  i = Err.Number
  StopRegieEvents
  MsgBox "Error " & i & " (" & Error(i) & ") in Sub: InizializzaVariabili"
  ResumeRegieEvents

End Sub

'Sub PutCfg(Chiave As String, Valore As String)
''
'Dim ret As Integer
''
'On Error Resume Next
'  '
'  ret = WritePrivateProfileString("SUOEM_CONFIGURAZIONE", Chiave, Valore, Tabella1.PathOEM & "GAPP4.INI")
'  '
'End Sub

'Function GetCfg(Chiave As String) As String
''
'Dim ret$, iRet
''
'On Error GoTo GetCfg_Error
'  '
'  ret$ = String$(255, 0)
'  iRet = GetPrivateProfileString("CONFIGURAZIONE", Chiave, "", ret$, 255, Tabella1.PathOEM & "GAPP4.INI")
'  If iRet = 0 Then
'    GetCfg = ""
'  Else
'    GetCfg = Mid$(ret$, 1, iRet)
'  End If
'
'Exit Function
'
'GetCfg_Error:
'  MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Function: GetCfg"
'  Exit Function
'
'End Function

Function ValVis(Valore As String, Parametro As Integer) As String ' Valore visualizzato
'
Dim tstr()  As String
Dim tstr2() As String
Dim i       As Integer
'
On Error Resume Next
  '
  If IsNumeric(Valore) Then
    i = InStr(Valore, ",")
    If (i > 0) Then Valore = Replace(Valore, ",", ".")
  End If
  ValVis = Valore
  If (Tabella1.Parametri(Parametro).Limiti_Lett <> "") Then
    tstr = Split(Tabella1.Parametri(Parametro).Limiti, ",")
    tstr2 = Split(Tabella1.Parametri(Parametro).Limiti_Lett, ",")
    For i = 0 To UBound(tstr)
      If (Trim$(UCase$(Valore)) = Trim$(UCase$(tstr(i)))) Then
        If (i <= UBound(tstr2)) Then ValVis = tstr2(i)
        Exit Function
      End If
    Next i
  End If
  '
End Function

Function ValVisOPT(Valore As String, Parametro As Integer) As String ' Valore visualizzato

ValVisOPT = Valore

Dim tstr()  As String
Dim tstr2() As String
Dim i       As Integer
'
On Error Resume Next
  '
  If (Tabella1.Opzioni(Parametro).Limiti_Lett <> "") Then
    tstr = Split(Tabella1.Opzioni(Parametro).Limiti, ",")
    tstr2 = Split(Tabella1.Opzioni(Parametro).Limiti_Lett, ",")
    For i = 0 To UBound(tstr)
      If (Trim(UCase(Valore)) = Trim(UCase(tstr(i)))) Then
        If (i <= UBound(tstr2)) Then ValVisOPT = tstr2(i)
        Exit Function
      End If
    Next i
  End If

End Function

Function ValMem(Valore As String, Parametro As Integer) As String ' Valore memorizzato

ValMem = Valore

Dim tstr()  As String
Dim tstr2() As String
Dim i       As Integer
'
On Error Resume Next
  '
  If (Tabella1.Parametri(Parametro).Limiti_Lett <> "") Then
    tstr = Split(Tabella1.Parametri(Parametro).Limiti, ",")
    tstr2 = Split(Tabella1.Parametri(Parametro).Limiti_Lett, ",")
    For i = 0 To UBound(tstr2)
      If Trim(UCase(Valore)) = Trim(UCase(tstr2(i))) Then
        If (i <= UBound(tstr)) Then
          ValMem = tstr(i)
        End If
        Exit Function
      End If
    Next i
  End If

End Function

Function ValMemOPT(Valore As String, Parametro As Integer) As String ' Valore memorizzato

ValMemOPT = Valore

Dim tstr()  As String
Dim tstr2() As String
Dim i       As Integer
'
On Error Resume Next
  '
  If (Tabella1.Opzioni(Parametro).Limiti_Lett <> "") Then
    tstr = Split(Tabella1.Opzioni(Parametro).Limiti, ",")
    tstr2 = Split(Tabella1.Opzioni(Parametro).Limiti_Lett, ",")
    For i = 0 To UBound(tstr2)
      If (Trim(UCase(Valore)) = Trim(UCase(tstr2(i)))) Then
        If (i <= UBound(tstr)) Then ValMemOPT = tstr(i)
        Exit Function
      End If
    Next i
  End If

End Function

Private Sub LeggiPAGINE()
'
Dim DBase          As Database
Dim RS             As Recordset
Dim fld            As Field
Dim i              As Integer
Dim j              As Integer
Dim tstr()         As String
Dim Abilita        As Boolean
Dim CONFIGURAZIONE As String
Dim stmp           As String
'
On Error Resume Next
  '
  MaxPagINS = 0
  '
  ReDim Tabella1.Pagine(0)
  Set DBase = OpenDatabase(Tabella1.DBase)
  If (Tabella1.MODALITA = "SPF") Then
    CONFIGURAZIONE = GetInfo("Configurazione", "PRG0_STATO", Tabella1.IniCfgLav)
    CONFIGURAZIONE = Trim$(UCase$(CONFIGURAZIONE))
    If (Len(CONFIGURAZIONE) > 0) Then
      CONFIGURAZIONE = "_" & UCase$(CONFIGURAZIONE)
      'CONTROLLO ESISTENZA DELLA CONFIGURAZIONE
      CONFIGURAZIONE = Tabella1.NOME & CONFIGURAZIONE
      stmp = ""
      For j = 0 To DBase.TableDefs.count - 1
        'MsgBox "[" & DBase.TableDefs(j).Name & "]" & "   " & "[" & CONFIGURAZIONE & "]"
        If DBase.TableDefs(j).Name = CONFIGURAZIONE Then
          stmp = "TROVATO"
          'MsgBox stmp
          Exit For
        End If
      Next j
      If (stmp = "TROVATO") Then
        Set RS = DBase.OpenRecordset("SELECT * FROM " & CONFIGURAZIONE & " ORDER BY [Pagina]")
      Else
        Set RS = DBase.OpenRecordset("SELECT * FROM " & Tabella1.NOME & "_CONFIG" & " ORDER BY [Pagina]")
      End If
    Else
        Set RS = DBase.OpenRecordset("SELECT * FROM " & Tabella1.NOME & "_CONFIG" & " ORDER BY [Pagina]")
    End If
  Else
        Set RS = DBase.OpenRecordset("SELECT * FROM " & Tabella1.NOME & "_CONFIG" & " ORDER BY [Pagina]")
  End If
  If (RS.RecordCount > 0) Then
    RS.MoveFirst
    While Not RS.EOF
      Abilita = True
      For Each fld In RS.Fields
        If (UCase$(fld.Name) = "ABILITA") Then
          If (fld.Type = 1) Then 'Campo ABILITA Booleano (prima versione)
            Abilita = fld.Value
            Exit For
          Else
            Select Case "" & UCase(Trim(fld.Value))
              Case "OFF-LINE"
                If (Tabella1.MODALITA <> "OFF-LINE") Then Abilita = False
              Case "SPF", "CNC"
                If (Tabella1.MODALITA <> "SPF") Then Abilita = False
              Case "", "Y"
                Abilita = True
              Case "N"
                Abilita = False
            End Select
            Exit For
          End If
        End If
      Next
      If (Abilita = True) Then
        i = val(RS.Fields("PAGINA").Value)
        If (i <= 0) Then i = 1
        If (i > MaxPagINS) Then
          ReDim Preserve Tabella1.Pagine(i)
          MaxPagINS = i
        End If
        Tabella1.Pagine(i).SKPos = "-1"
        Tabella1.Pagine(i).Numero = CInt(RS.Fields("PAGINA").Value)
        Tabella1.Pagine(i).NOME1 = "" & RS.Fields("NOME_" & UCase(Tabella1.LINGUA)).Value
        If IsNull(RS.Fields("NOME2_" & UCase(Tabella1.LINGUA)).Value) Then
          stmp = RS.Fields("NOME_" & UCase(Tabella1.LINGUA)).Value
        Else
          stmp = Trim$(RS.Fields("NOME2_" & UCase(Tabella1.LINGUA)).Value)
        End If
        Tabella1.Pagine(i).NOME2 = stmp
        Tabella1.Pagine(i).sequenza = "" & RS.Fields("SEQUENZA").Value
        Tabella1.Pagine(i).Input = RS.Fields("INPUT").Value
        Tabella1.Pagine(i).Nomegruppo = "" & RS.Fields("NOMEGRUPPO").Value
        Tabella1.Pagine(i).Nomegruppo2 = "" & RS.Fields("NOMEGRUPPO2").Value
        Tabella1.Pagine(i).SKPos = RS.Fields("SKPOS").Value
        Tabella1.Pagine(i).SKPos2 = "" & RS.Fields("SKPOS2").Value
        tstr = Split("" & RS.Fields("LARGHEZZACOLONNE").Value, "|")
        ReDim Preserve tstr(2)
        For j = 0 To 2
          Tabella1.Pagine(i).Larghezze(j) = tstr(j)
        Next j
      End If
      RS.MoveNext
    Wend
  End If
  RS.Close
  DBase.Close
  '
End Sub

Public Sub AggiornaONLINE(Optional Redraw As Boolean = False)
'
On Error Resume Next
  '
  If MostraVar Then Exit Sub
  If Not Tabella1.Caricata Then Exit Sub
  If (Tabella1.MODALITA <> "SPF") Then Exit Sub
  If Tabella1.NoPolling Then Exit Sub
  Call LeggiONLINE(Redraw)
  '
End Sub

Public Sub LeggiONLINE(Optional Redraw As Boolean = False)
'
Dim i         As Integer
Dim j         As Integer
Dim L         As Integer
Dim LL        As Integer
Dim tstr      As String
Dim VARIABILE As String
'
On Error Resume Next
  '
  For i = 1 To UBound(Tabella1.Parametri)
    If Tabella1.Parametri(i).Abilitato And Tabella1.Parametri(i).ONLine Then
      For j = 1 To Tabella1.Parametri(i).NActual
        If (Trim((Tabella1.Parametri(i).ITEMDDE(j))) <> "") Then
          Dim Controllo        As Long
          Dim DDE_BASE         As String
          Dim VAL_BASE1        As Integer
          Dim VAL_BASE2        As Integer
          Dim N                As Integer
          Dim jj1              As String
          Dim jj2              As String
          Dim LIMITI_VALORI()  As String
          Dim LIMITI_SIMBOLI() As String
          tstr = Tabella1.Parametri(i).VARIABILE(0)
          tstr = Trim$(UCase$(tstr))
          LIMITI_VALORI = Split(Tabella1.Parametri(i).Limiti, ",")
          If (Len(Tabella1.Parametri(i).Limiti_Lett) > 0) Then
            LIMITI_SIMBOLI = Split(Tabella1.Parametri(i).Limiti_Lett, ",")
          Else
            LIMITI_SIMBOLI = Split(Tabella1.Parametri(i).Limiti, ",")
          End If
          Controllo = InStr(tstr, "BASE")
          If (Controllo > 0) Then
            jj1 = InStr(tstr, "|")
            jj2 = InStr(tstr, ",")
            DDE_BASE = Mid$(tstr, jj1 + 1, jj2 - jj1 - 1)
            VAL_BASE1 = OPC_LEGGI_DATO(DDE_BASE)
            'I VALORI FISSI NEL DATABASE DEVONO ESSERE "0" ED "1"
            If (VAL_BASE1 = 0) Then
              VAL_BASE2 = 0
            Else
              VAL_BASE2 = val(Mid$(tstr, jj2 + 1))
            End If
            If (Tabella1.MODALITA = "SPF") Then
              '+2 perch� l'indirizzamento tramite OPC parte da 1
              N = Asc(UCase(Tabella1.LetteraLavorazione)) - Asc("A") + 2
              N = N + VAL_BASE2
              tstr = Tabella1.Parametri(i).ITEMDDE(j)
              'tstr = Replace(tstr, "*", N)
              jj1 = InStr(tstr, "[")
              jj2 = InStr(tstr, "]")
              tstr = Left$(tstr, jj1) & N & Right$(tstr, Len(tstr) - jj2 + 1)
              VARIABILE = tstr
              L = InStr(VARIABILE, "/")
              While (L > 0)
                VARIABILE = Right$(VARIABILE, Len(VARIABILE) - L)
                L = InStr(VARIABILE, "/")
              Wend
              tstr = OPC_LEGGI_DATO(tstr)
              If IsNumeric(tstr) Then
                Tabella1.Parametri(i).Actual(j) = frmt(val(tstr), 8)
              Else
                Tabella1.Parametri(i).Actual(j) = tstr
              End If
              If (Len(Tabella1.Parametri(i).VARIABILE(j)) > 0) Then
              L = WritePrivateProfileString("CNC_" & Tabella1.LetteraLavorazione, Tabella1.Parametri(i).VARIABILE(j), Tabella1.Parametri(i).Actual(j), Tabella1.PathOEM & Tabella1.NOME & ".INI")
              End If
            End If
          Else
            tstr = DDE_Idx(Tabella1.Parametri(i).ITEMDDE(j))
            VARIABILE = tstr
            L = InStr(VARIABILE, "/")
            While (L > 0)
              VARIABILE = Right$(VARIABILE, Len(VARIABILE) - L)
              L = InStr(VARIABILE, "/")
            Wend
            tstr = OPC_LEGGI_DATO(tstr)
            If IsNumeric(tstr) Then
              Tabella1.Parametri(i).Actual(j) = frmt(val(tstr), 8)
            Else
              Tabella1.Parametri(i).Actual(j) = tstr
            End If
            If (Len(Tabella1.Parametri(i).VARIABILE(j)) > 0) Then
            L = WritePrivateProfileString("CNC_" & Tabella1.LetteraLavorazione, Tabella1.Parametri(i).VARIABILE(j), Tabella1.Parametri(i).Actual(j), Tabella1.PathOEM & Tabella1.NOME & ".INI")
            End If
          End If
          '
          If Redraw Then
            Dim INSERISCI        As Boolean
            Dim SIMBOLO          As String
            For L = 0 To 2
              tstr = DDE_Idx(Tabella1.Parametri(i).ITEMDDE(j))
              If (SuGrid1(L).GetKeyCellaByVar(tstr) <> "") Then
                INSERISCI = False: SIMBOLO = ""
                For LL = 0 To UBound(LIMITI_VALORI)
                  If (LIMITI_VALORI(LL) = Tabella1.Parametri(i).Actual(j)) Then
                    SIMBOLO = LIMITI_SIMBOLI(LL)
                    INSERISCI = True
                    Exit For
                  End If
                Next LL
                If (INSERISCI = True) Then
                SuGrid1(L).GetCellaByVar(tstr).Value = SIMBOLO
                Else
                SuGrid1(L).GetCellaByVar(tstr).Value = Tabella1.Parametri(i).Actual(j)
                End If
                Exit For
              End If
            Next L
          End If
          '
        End If
      Next j
    End If
  Next i

End Sub

'****m* SUOEM/InitSUOEM
' DESCRIZIONE:
' E' il metodo principale che permette di caricare una lavorazione in memoria.
' Dopo il caricamento la gestione delle modifiche nei parametri vengono fatte automaticamente, appoggiando i dati in un file INI.
' DEFINIZIONE:
Public Sub InitSUOEM(ByVal DB As String, ByVal TB As String, _
                     ByVal LINGUA As String, Optional ByVal MODALITA As String = "", _
                     Optional LetteraLavorazione As String = "", Optional NomePezzo As String = "", _
                     Optional IniCfgLav As String = "", Optional NomeLavorazione As String = "", _
                     Optional Temp As Boolean = False, Optional PaginaIniziale As Integer = 1)
'***
Dim DBase         As Database
Dim RS            As Recordset
Dim rs2           As Recordset
Dim tdati         As String
Dim i             As Integer
Dim j             As Integer
Dim k             As Integer
Dim tIdx          As Integer
Dim NActual       As Integer
Dim Campo         As Field
Dim tTitoli()     As String
Dim tLim()        As String
Dim tSoloCarica   As Boolean
Dim tstr          As String
Dim iopt          As Integer
Dim ActPar        As Integer
Dim Atmp          As String * 255
Dim riga          As String
Dim ret           As Long
'
On Error GoTo InitSUOEM_Error
  '
  'Inizializzazione variabili
  Tabella1.Caricata = False
  Call InizializzaVariabili
  '
  If (LINGUA = "") Then LINGUA = "IT"
  CaricamentoINCorso = False
  Tabella1.DBase = DB
  Tabella1.PathOEM = Left(Tabella1.DBase, InStrRev(Tabella1.DBase, "\"))
  Tabella1.NOME = TB
  Tabella1.LINGUA = LINGUA
  Tabella1.MODALITA = IIf(MODALITA <> "", MODALITA, IIf(Tabella1.MODALITA <> "", Tabella1.MODALITA, "OFF-LINE"))
  If (Tabella1.MODALITA = "SPF_STAMPA") Then
    Tabella1.MODALITA = "SPF"
    Tabella1.NoPolling = True
  Else
    Tabella1.NoPolling = False
  End If
  'Tabella1.Livello = Livello
  Tabella1.IniCfgLav = IniCfgLav
  If (NomeLavorazione <> "") Then
    Tabella1.NomeLavorazione = NomeLavorazione
  Else
    Tabella1.NomeLavorazione = TB
  End If
  Tabella1.LetteraLavorazione = LetteraLavorazione
  Tabella1.NomeArchivio = "ARCHIVIO_" & Tabella1.NOME
  If (NomePezzo <> "") Then Tabella1.NomePezzo = NomePezzo
  If (Tabella1.MODALITA <> "") Then
    lblModalita.Visible = True
  Else
    lblModalita.Visible = False
  End If
  i = 1
  While IsNumeric(Right(Tabella1.NOME, i))
    Tabella1.NumeroLavorazione = val(Right(Tabella1.NOME, i))
    i = i + 1
  Wend
  ReDim Tabella1.Parametri(0)
  '
  Set DBase = OpenDatabase(DB)
  Set RS = DBase.OpenRecordset(Tabella1.NOME & "_MESSAGGI", dbOpenSnapshot)
  ReDim Tabella1.MESSAGGI(0)
  i = 0: RS.MoveFirst
  While Not RS.EOF
    i = i + 1
    ReDim Preserve Tabella1.MESSAGGI(i)
    Tabella1.MESSAGGI(i).MESSAGGIO = RS.Fields("MESSAGGIO")
    If (UBound(Split("" & RS.Fields("NOME_" & Tabella1.LINGUA), ";")) <> "-1") Then
      tstr = "" & RS.Fields("NOME_" & Tabella1.LINGUA)
      If (Len(tstr) > 2) Then
        Tabella1.MESSAGGI(i).Gruppo = Trim(Split(tstr, ";")(0))
        Tabella1.MESSAGGI(i).NOME = Right(tstr, Len(tstr) - 2)
      Else
        Tabella1.MESSAGGI(i).Gruppo = "0"
        Tabella1.MESSAGGI(i).NOME = tstr
      End If
    Else
      Tabella1.MESSAGGI(i).NOME = "NOT FOUND"
      Tabella1.MESSAGGI(i).Gruppo = 1
    End If
    RS.MoveNext
  Wend
  RS.Close
  '
  MostraVar = False
  'MostraVar = True
  '
  'Aggiorno l'elenco di parametri
  Set RS = DBase.OpenRecordset("SELECT * FROM " & Tabella1.NOME & " ORDER BY [INDICE]", dbOpenSnapshot)
  i = 0: iopt = 0: RS.MoveFirst
  While Not RS.EOF
    If (UCase(Trim("" & RS.Fields("NOMESPF"))) <> "OPTION") Then
      If (UCase(RS.Fields("ABILITATO")) = "Y") Then
        i = i + 1
        ReDim Preserve Tabella1.Parametri(i)
        tIdx = i
        Tabella1.Parametri(tIdx).DoppiaConf = False
        For Each Campo In RS.Fields
          Select Case UCase(Campo.Name)
            Case "CONFERMA": Tabella1.Parametri(tIdx).DoppiaConf = Campo.Value
          End Select
        Next
        Tabella1.Parametri(tIdx).Abilitato = (UCase(RS.Fields("ABILITATO")) = "Y")
        Tabella1.Parametri(tIdx).Aiuto = "" & RS.Fields("AIUTO_" & Tabella1.LINGUA)
        Tabella1.Parametri(tIdx).Gruppo = RS.Fields("Gruppo")
        Tabella1.Parametri(tIdx).Titolo = False
        If (Tabella1.Parametri(tIdx).Gruppo > 3) Then
          Tabella1.Parametri(tIdx).Titolo = True
          Tabella1.Parametri(tIdx).Gruppo = Tabella1.Parametri(tIdx).Gruppo Mod 4
        End If
        Tabella1.Parametri(tIdx).imgPath = "" & RS.Fields("ImgPath")
        Tabella1.Parametri(tIdx).INDICE = RS.Fields("INDICE")
        Tabella1.Parametri(tIdx).Limiti = "" & RS.Fields("Limiti")
        '
        If RS.Fields("VARIABILE") Like "*CODMOLA_G*" Then
          Tabella1.Parametri(tIdx).CodMola = True
        Else
          Tabella1.Parametri(tIdx).CodMola = False
        End If
        '
        If RS.Fields("VARIABILE") Like "*CODICERULLO_G*" Then
          Tabella1.Parametri(tIdx).CodRullo = True
        Else
          Tabella1.Parametri(tIdx).CodRullo = False
        End If
        '
        'I parametri con ONLINE nel campo NOMESPF vengono aggiornati direttamente sul CN
        'I parametri con MOLA   nel campo NOMESPF vengono inviati al CN nel file PARAMETRI1\"CODICEMOLA".SPF
        'I parametri con RULLO  nel campo NOMESPF vengono inviati al CN nel file \"CODICERULLO".SPF
        '
        Select Case UCase$(Trim$("" & RS.Fields("NOMESPF")))
          Case "ONLINE"
            Tabella1.Parametri(tIdx).ONLine = True
            Tabella1.Parametri(tIdx).Mola = False
            Tabella1.Parametri(tIdx).Rullo = False
            Tabella1.Parametri(tIdx).NomeSPF = ""
          Case "MOLA"
            Tabella1.Parametri(tIdx).ONLine = False
            Tabella1.Parametri(tIdx).Mola = True
            Tabella1.Parametri(tIdx).Rullo = False
            Tabella1.Parametri(tIdx).NomeSPF = ""
          Case "RULLO"
            Tabella1.Parametri(tIdx).ONLine = False
            Tabella1.Parametri(tIdx).Mola = False
            Tabella1.Parametri(tIdx).Rullo = True
            Tabella1.Parametri(tIdx).NomeSPF = ""
          Case Else
            Tabella1.Parametri(tIdx).ONLine = False
            Tabella1.Parametri(tIdx).Mola = False
            Tabella1.Parametri(tIdx).Rullo = False
            Tabella1.Parametri(tIdx).NomeSPF = "" & RS.Fields("NOMESPF")
        End Select
        Tabella1.Parametri(tIdx).LIVELLO = "" & RS.Fields("LIVELLO")
        Tabella1.Parametri(tIdx).Modificabile = (UCase(RS.Fields("MODIFICABILE")) = "Y")
        '
        If MostraVar Then Tabella1.Parametri(tIdx).Modificabile = False
        '
        tLim = Split("" & RS.Fields("NOME_" & Tabella1.LINGUA), "|")
        ReDim Preserve tLim(1)
        Tabella1.Parametri(tIdx).NOME = "" & tLim(0)
        'La seconda parte del nome se presente (dopo "|" ) indica il nome da visualizzare
        'nella ComboBox
        Tabella1.Parametri(tIdx).Limiti_Lett = "" & tLim(1)
        Tabella1.Parametri(tIdx).TIPO = "" & RS.Fields("TIPO")
        If (Tabella1.Parametri(tIdx).TIPO = "S") And (Tabella1.Parametri(tIdx).Limiti = "0,1") And (Tabella1.Parametri(tIdx).Limiti_Lett = "") Then Tabella1.Parametri(tIdx).TIPO = "B"
        Tabella1.Parametri(tIdx).Cambiato = False
        ActPar = UBound(Split(";" & RS.Fields("VARIABILE"), ";"))
        Tabella1.Parametri(tIdx).NActual = ActPar
        ' ";" & serve perch� actual_1 deve essere indice 1 e non 0 dell'array
        Tabella1.Parametri(tIdx).Actual = Split(";" & RS.Fields("ACTUAL"), ";")
        '
        If (Tabella1.Parametri(tIdx).ONLine = True) Then
          Dim Controllo As Long
          Dim DDE_BASE  As String
          Dim VAL_BASE1 As Integer
          Dim VAL_BASE2 As Integer
          Dim N         As Integer
          Dim L         As Integer
          Dim jj1       As Integer
          Dim jj2       As Integer
          Dim k1        As Integer
          Dim k2        As Integer
          Dim VARIABILE As String
          Dim stmp      As String
          Dim INDICE    As String
          tstr = "" & RS.Fields("VARIABILE")
          tstr = Trim$(UCase$(tstr))
          ReDim Tabella1.Parametri(tIdx).VARIABILE(0)
          Controllo = InStr(tstr, "BASE")
          If (Controllo > 0) Then
            jj1 = InStr(tstr, "|")
            jj2 = InStr(tstr, ",")
            DDE_BASE = Mid$(tstr, jj1 + 1, jj2 - jj1 - 1)
            If Temp Then
              'LETTURA DEL VALORE CORRENTE DELLA BASE NEL CN
              VAL_BASE1 = OPC_LEGGI_DATO(DDE_BASE)
            Else
              'LETTURA DEL VALORE SALVATO IN ARCHIVIO DELLA BASE
              Set rs2 = DBase.OpenRecordset(Tabella1.NomeArchivio, dbOpenDynaset)
              If (rs2.RecordCount = 0) Then
                rs2.Close
                DBase.Close
                Exit Sub
              End If
              rs2.FindFirst ("NOME_PEZZO='" & UCase(Trim(NomePezzo)) & "'")
              tdati = rs2.Fields("DATI")
              rs2.Close
              VARIABILE = DDE_BASE
              L = InStr(VARIABILE, "/")
              While (L > 0)
                VARIABILE = Right$(VARIABILE, Len(VARIABILE) - L)
                L = InStr(VARIABILE, "/")
              Wend
              k1 = InStr(tdati, VARIABILE)
              If (k1 > 0) Then
                k2 = InStr(k1, tdati, Chr(13) & Chr(10))
                stmp = Mid$(tdati, k1, k2 - k1)
                L = InStr(stmp, "=")
                VAL_BASE1 = val(Right$(stmp, Len(stmp) - L))
              Else
                VAL_BASE1 = 0
              End If
            End If
            'I VALORI FISSI NEL DATABASE DEVONO ESSERE "0" ED "1"
            If (VAL_BASE1 = 0) Then
              VAL_BASE2 = 0
            Else
              VAL_BASE2 = val(Mid$(tstr, jj2 + 1))
            End If
            '+2 perch� l'indirizzamento tramite OPC parte da 1
            N = Asc(UCase(Tabella1.LetteraLavorazione)) - Asc("A") + 2
            N = N + VAL_BASE2
            tstr = RS.Fields("ItemDDE")
            jj1 = InStr(tstr, "[")
            jj2 = InStr(tstr, "]")
            tstr = Left$(tstr, jj1) & N & Right$(tstr, Len(tstr) - jj2 + 1)
            Tabella1.Parametri(tIdx).ITEMDDE = Split(";" & tstr, ";")
            'SALVATAGGIO DEL VALORE ONLINE LETTO SVG221111
            VARIABILE = tstr
            L = InStr(VARIABILE, "/")
            While (L > 0)
              VARIABILE = Right$(VARIABILE, Len(VARIABILE) - L)
              L = InStr(VARIABILE, "/")
            Wend
            'RIMUOVO L'INDICE
            L = InStr(VARIABILE, "[")
            VARIABILE = Left$(VARIABILE, L - 1)
          Else
            tstr = RS.Fields("ItemDDE")
            L = InStr(tstr, "*")
            If (L > 0) Then
              'SOSTITUISCO CON L'INDICE DELLA LAVORAZIONE CORRENTE
              'CONSIDERO LA SINTASSI VARIABILE[0,LAVORAZIONE] --> DDEVARIABILE[LAVORAZIONE+1]
              If (INDICE_LAVORAZIONE_SINGOLA <= 0) Then INDICE_LAVORAZIONE_SINGOLA = 1
              tstr = Replace(tstr, "*", INDICE_LAVORAZIONE_SINGOLA + 1)
              Tabella1.Parametri(tIdx).ITEMDDE = Split(";" & tstr, ";")
              'SALVATAGGIO DEL VALORE ONLINE LETTO SVG221111
              VARIABILE = tstr
              L = InStr(VARIABILE, "/")
              While (L > 0)
                VARIABILE = Right$(VARIABILE, Len(VARIABILE) - L)
                L = InStr(VARIABILE, "/")
              Wend
              'RIMUOVO L'INDICE
              'L = InStr(Variabile, "[")
              'Variabile = Left$(Variabile, L - 1)
            Else
              k2 = InStr(tstr, "+")
              If (k2 > 0) Then
                'AGGIUNGO L'INDICE DELLA LAVORAZIONE CORRENTE
                If (INDICE_LAVORAZIONE_SINGOLA <= 0) Then INDICE_LAVORAZIONE_SINGOLA = 1
                k1 = InStr(tstr, "[")
                VAL_BASE1 = val(Mid$(tstr, k1 + 1, k2 - k1 - 1))
                INDICE = "[" & Format$(VAL_BASE1 + INDICE_LAVORAZIONE_SINGOLA - 1, "#####0") & "]"
                tstr = Left$(tstr, k1 - 1) & INDICE
                Tabella1.Parametri(tIdx).ITEMDDE = Split(";" & tstr, ";")
                'SALVATAGGIO DEL VALORE ONLINE LETTO SVG221111
                VARIABILE = tstr
                L = InStr(VARIABILE, "/")
                While (L > 0)
                  VARIABILE = Right$(VARIABILE, Len(VARIABILE) - L)
                  L = InStr(VARIABILE, "/")
                Wend
                'RIMUOVO L'INDICE
                L = InStr(VARIABILE, "[")
                VARIABILE = Left$(VARIABILE, L - 1) & INDICE
              Else
                'MANTENGO L'ITEMDDE DEFINITO NEL DATABASE
                Tabella1.Parametri(tIdx).ITEMDDE = Split(";" & RS.Fields("ItemDDE"), ";")
                'SALVATAGGIO DEL VALORE ONLINE LETTO SVG221111
                VARIABILE = tstr
                L = InStr(VARIABILE, "/")
                While (L > 0)
                  VARIABILE = Right$(VARIABILE, Len(VARIABILE) - L)
                  L = InStr(VARIABILE, "/")
                Wend
              End If
            End If
          End If
          Tabella1.Parametri(tIdx).VARIABILE = Split(";" & VARIABILE, ";")
          Tabella1.Parametri(tIdx).VARIABILE(0) = "" & RS.Fields("VARIABILE")
        Else
          Tabella1.Parametri(tIdx).ITEMDDE = Split(";" & RS.Fields("ItemDDE"), ";")
          Tabella1.Parametri(tIdx).VARIABILE = Split(";" & RS.Fields("VARIABILE"), ";")
        End If
        '
        Tabella1.Parametri(tIdx).Default = "" & RS.Fields("DEFAULT")
        '
        ReDim Preserve Tabella1.Parametri(tIdx).Actual(ActPar)
        ReDim Preserve Tabella1.Parametri(tIdx).ITEMDDE(ActPar)
        ReDim Preserve Tabella1.Parametri(tIdx).VARIABILE(ActPar)
        ReDim Tabella1.Parametri(tIdx).Evidenzia(ActPar)
        '
        For j = 1 To ActPar
          Tabella1.Parametri(tIdx).Actual(j) = CalcoloDefaultParametro(tIdx)
        Next j
        '
      End If
    Else
      'Parametro Opzione
      iopt = iopt + 1
      ReDim Preserve Tabella1.Opzioni(iopt)
      tIdx = iopt
      Tabella1.Opzioni(tIdx).Abilitato = (UCase(RS.Fields("ABILITATO")) = "Y")
      Tabella1.Opzioni(tIdx).Aiuto = "" & RS.Fields("AIUTO_" & Tabella1.LINGUA)
      Tabella1.Opzioni(tIdx).Gruppo = RS.Fields("Gruppo")
      Tabella1.Opzioni(tIdx).Titolo = False
      If (Tabella1.Opzioni(tIdx).Gruppo > 3) Then
        Tabella1.Opzioni(tIdx).Titolo = True
        Tabella1.Opzioni(tIdx).Gruppo = Tabella1.Opzioni(tIdx).Gruppo Mod 4
      End If
      Tabella1.Opzioni(tIdx).imgPath = "" & RS.Fields("ImgPath")
      Tabella1.Opzioni(tIdx).INDICE = RS.Fields("INDICE")
      Tabella1.Opzioni(tIdx).Limiti = "" & RS.Fields("Limiti")
      Tabella1.Opzioni(tIdx).LIVELLO = "" & RS.Fields("LIVELLO")
      Tabella1.Opzioni(tIdx).Modificabile = (UCase(RS.Fields("MODIFICABILE")) = "Y")
      tLim = Split("" & RS.Fields("NOME_" & Tabella1.LINGUA), "|")
      ReDim Preserve tLim(1)
      Tabella1.Opzioni(tIdx).NOME = "" & tLim(0)
      'La seconda parte del nome se presente (dopo "|" ) indica il nome da visualizzare
      'nella ComboBox
      Tabella1.Opzioni(tIdx).Limiti_Lett = "" & tLim(1)
      Tabella1.Opzioni(tIdx).TIPO = "" & RS.Fields("TIPO")
      If (Tabella1.Opzioni(tIdx).TIPO = "S") And (Tabella1.Opzioni(tIdx).Limiti = "0,1") And (Tabella1.Opzioni(tIdx).Limiti_Lett = "") Then
        Tabella1.Opzioni(tIdx).TIPO = "B"
      End If
      Tabella1.Opzioni(tIdx).Cambiato = False
      ActPar = UBound(Split(";" & RS.Fields("VARIABILE"), ";"))
      Tabella1.Opzioni(tIdx).NActual = ActPar
      ' ";" & serve perch� actual_1 deve essere indice 1 e non 0 dell'array
      Tabella1.Opzioni(tIdx).Actual = Split(";" & RS.Fields("ACTUAL"), ";")
      Tabella1.Opzioni(tIdx).ITEMDDE = Split(";" & RS.Fields("ItemDDE"), ";")
      Tabella1.Opzioni(tIdx).VARIABILE = Split(";" & RS.Fields("VARIABILE"), ";")
      Tabella1.Opzioni(tIdx).Default = "" & RS.Fields("DEFAULT")
      ReDim Preserve Tabella1.Opzioni(tIdx).Actual(ActPar)
      ReDim Preserve Tabella1.Opzioni(tIdx).ITEMDDE(ActPar)
      ReDim Preserve Tabella1.Opzioni(tIdx).VARIABILE(ActPar)
      ReDim Tabella1.Opzioni(tIdx).Evidenzia(ActPar)
    End If
    RS.MoveNext
  Wend
  RS.Close
  '
  DBase.Close
  '
  Helping = False
  pctMessaggi.Visible = False
  Tabella1.Caricata = True
  '
  'Lettura configurazione pagine
  Call LeggiPAGINE
  '
  If (MaxPagINS <= 0) Then
    lblInfo.Visible = False
    lblModalita.Visible = False
    lblSEQUENZA.Visible = False
    SuGridDisegni.Visible = False
    pctDisegni.Visible = False
    PicTITOLO.Visible = False
    lstEdit(0).Visible = False
    lstEdit(1).Visible = False
    lstEdit(2).Visible = False
    txtEdit(0).Visible = False
    txtEdit(1).Visible = False
    txtEdit(2).Visible = False
    SuGrid1(0).Visible = False
    SuGrid1(1).Visible = False
    SuGrid1(2).Visible = False
    pctWorking.Visible = False
    Exit Sub
  End If
  '
  For i = 1 To MaxPagINS
    If Len(Tabella1.Pagine(i).SKPos) > 0 Then PaginaINS = i: Exit For
  Next i
  If (PaginaIniziale > 1) Then
    PaginaINS = PaginaIniziale
    LastPagina = PaginaIniziale
  Else
    LastPagina = PaginaINS
  End If
    '
    'Caricamento dati pezzo
      If (Tabella1.MODALITA = "OFF-LINE") Then
      If (Not Temp) Then
        Call CaricaPezzo(Tabella1.NomePezzo, Tabella1.NomeArchivio, False)
        Call RaiseEvent_PaginaChanged(Tabella1.NOME, "", "", PaginaINS)
      Else
        Call CaricaPezzoINI(Tabella1.NomePezzo)
      End If
  ElseIf (Tabella1.MODALITA = "SPF") Then
    If (Not Temp) Then
      CaricamentoINCorso = True
      Call CaricaCNC(Tabella1.NomePezzo, Tabella1.LetteraLavorazione, "ARCHIVIO_" & Tabella1.NOME)
      Dim NON_CARICARE_SPF1 As String
      Dim NON_CARICARE_SPF2 As String
      NON_CARICARE_SPF1 = GetInfo(Tabella1.NOME, "NON_CARICARE_SPF1", Tabella1.IniCfgLav)
      NON_CARICARE_SPF1 = UCase$(Trim$(NON_CARICARE_SPF1))
      NON_CARICARE_SPF2 = GetInfo(Tabella1.NOME, "NON_CARICARE_SPF2", Tabella1.IniCfgLav)
      NON_CARICARE_SPF2 = UCase$(Trim$(NON_CARICARE_SPF2))
      Call CreaSPF(True, NON_CARICARE_SPF1, NON_CARICARE_SPF2)
      Dim NON_CARICARE_DDE() As String
      Dim TROVATO            As Boolean
      i = 1: ReDim NON_CARICARE_DDE(0)
      Do
        stmp = GetInfo(Tabella1.NOME, "NON_CARICARE_DDE" & Format$(i, "0"), Tabella1.IniCfgLav)
        stmp = UCase$(Trim$(stmp))
        If (Len(stmp) > 0) Then
          ReDim Preserve NON_CARICARE_DDE(i)
          NON_CARICARE_DDE(i) = stmp
          i = i + 1
        Else
          Exit Do
        End If
      Loop
      'ASSEGNAZIONE VALORI ONLINE DA ARCHIVIO PEZZI
      For i = 1 To UBound(Tabella1.Parametri)
        If (Tabella1.Parametri(i).ONLine) Then
          For j = 1 To UBound(Tabella1.Parametri(i).ITEMDDE)
            'stmp = ""
            'stmp = stmp & Tabella1.Parametri(i).ItemDDE(j) & Chr(13)
            'stmp = stmp & Tabella1.Parametri(i).Variabile(j) & Chr(13)
            'stmp = stmp & Tabella1.Parametri(i).Actual(j) & Chr(13)
            'MsgBox stmp
            TROVATO = False
            For k = 1 To UBound(NON_CARICARE_DDE)
              If (NON_CARICARE_DDE(k) = Tabella1.Parametri(i).ITEMDDE(j)) Then
                TROVATO = True
                ret = LoadString(g_hLanguageLibHandle, 1003, Atmp, 255)
                If (ret > 0) Then riga = Left$(Atmp, ret)
                ret = MsgBox(riga & " " & Tabella1.Parametri(i).NOME, vbCritical + vbDefaultButton2 + vbYesNo, "WARNING")
                If (ret = vbYes) Then
                  Call OPC_SCRIVI_DATO(Tabella1.Parametri(i).ITEMDDE(j), Tabella1.Parametri(i).Actual(j))
                End If
                Exit For
              End If
            Next k
            If (TROVATO = False) Then
              Call OPC_SCRIVI_DATO(Tabella1.Parametri(i).ITEMDDE(j), Tabella1.Parametri(i).Actual(j))
            End If
          Next j
        End If
      Next i
      CaricamentoINCorso = False
    Else
      Call CaricaPezzoINI(Tabella1.NomePezzo)
      Call RaiseEvent_PaginaChanged(Tabella1.NOME, "", "", PaginaINS)
    End If
    '
  End If
  Call ImpostaFont
  Call Riposiziona
  Call Working(False)
  Call DisegnaOEM
  Call SELEZIONA_SuGrid1
  '
  'RaiseEvent PaginaReached(Tabella1.NOME, Tabella1.Pagine(PaginaINS).Nomegruppo, Tabella1.MODALITA)
  '
Exit Sub

InitSUOEM_Error:
  MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Sub: InitSUOEM"
  Exit Sub
   
End Sub

Function ProssimoLivello(idxParametro As Integer) As String
'
Dim tLim()  As String
Dim tNxt()  As String
Dim i       As Integer
'
On Error Resume Next
  '
  Select Case Tabella1.Parametri(idxParametro).TIPO
    
    Case "S"
      If Tabella1.Parametri(idxParametro).Limiti <> "" Then
        tLim() = Split(Tabella1.Parametri(idxParametro).Limiti, ",")
        tNxt() = Split(Tabella1.Parametri(idxParametro).NextLev, "|")
        For i = 0 To UBound(tLim)
          If Trim(UCase(tLim(i))) = Tabella1.Parametri(idxParametro).Actual(1) Then
            ProssimoLivello = tNxt(i)
            Exit Function
          End If
        Next i
        ProssimoLivello = tLim(0)
      End If
    
    Case Else: ProssimoLivello = Tabella1.Parametri(idxParametro).NextLev
    
  End Select
  '
End Function

Function GetLargh(Pagina As Integer, Gruppo As Integer) As String
'
On Error Resume Next
  '
  GetLargh = Tabella1.Pagine(Pagina).Larghezze(Gruppo)

End Function

Sub SetLargh(Pagina As Integer, Gruppo As Integer, Larghezze As String)
'
On Error Resume Next
  '
  Tabella1.Pagine(Pagina).Larghezze(Gruppo) = Larghezze

End Sub

Sub DisegnaOEM()
'
Dim i                 As Integer
Dim j                 As Integer
Dim k                 As Integer
Dim L                 As Integer
Dim K0                As Integer
Dim k1                As Integer
Dim k2                As Integer
Dim INS               As Boolean
Dim Livelli           As String
Dim tGrid             As String
Dim tBool             As String
Dim tstr              As String
Dim ATTIVA_POLLING    As Boolean
Dim stmp              As String
Dim stmp1             As String
Dim stmp2             As String
Dim Stato_Grind       As String
Dim Stato_Grind2      As String
Dim tTIPO_LAVORAZIONE As String
'
On Error GoTo DisegnaOEM_Error
  '
  'LEGGO IL TIPO DI LAVORAZIONE
  tTIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  tTIPO_LAVORAZIONE = UCase$(Trim$(tTIPO_LAVORAZIONE))
  
  If Not Tabella1.Caricata Then Exit Sub
  '
  OnlinePolling.Enabled = False
  '
  'SVG140111: valutare se rimettere
  'If (Tabella1.MODALITA = "SPF") Then Call AggiornaONLINE
  SPFPagina = ""
  WRITE_DIALOG ""
  '
  'SuGRID1(0).Visible = True
  tstr = GetLargh(PaginaINS, 0)
  If (tstr = "") Then
    tstr = "2;0.5;0.5;0.5;0.5;0.5;0.5"
    Call SetLargh(PaginaINS, 0, "2;0.5;0.5;0.5;0.5;0.5;0.5")
  End If
  SuGrid1(0).NewItemWidth = tstr
  '
  tstr = GetLargh(PaginaINS, 1)
  If (tstr = "") Then
    tstr = "5;1;1"
    Call SetLargh(PaginaINS, 1, "5;1;1")
  End If
  SuGrid1(1).NewItemWidth = tstr
  '
  tstr = GetLargh(PaginaINS, 2)
  If (tstr = "") Then
    tstr = "2.5;1;1"
    Call SetLargh(PaginaINS, 2, "2.5;1;1")
  End If
  SuGrid1(2).NewItemWidth = tstr
  '
  'SuGrid1(0).Clear
  'SuGrid1(1).Clear
  'SuGrid1(2).Clear
  '
  SuGrid1(0).ResizeCol = ResizeCol
  SuGrid1(1).ResizeCol = ResizeCol
  SuGrid1(2).ResizeCol = ResizeCol
  '
  SuGrid1(0).CellHeight = 0
  SuGrid1(1).CellHeight = SuGrid1(0).CellHeight
  SuGrid1(2).CellHeight = SuGrid1(0).CellHeight
  '
  If (Tabella1.MODALITA <> "") Then
    lblModalita.Visible = True
    lblModalita = Tabella1.MODALITA
    Tabella1.Info = Tabella1.NomeLavorazione & ": " & Tabella1.NomePezzo
    Select Case Tabella1.MODALITA
      Case "OFF-LINE", "INPUT"
        lblModalita.BackColor = vbGreen
      Case "SPF"
        lblModalita = "CNC" & IIf(Tabella1.LetteraLavorazione <> "", " " & Tabella1.LetteraLavorazione, "")
        lblModalita.BackColor = vbRed
      Case Else
        lblModalita.BackColor = UserControl.BackColor
    End Select
    lblSEQUENZA = PaginaINS & "/" & NPagine
  Else
    lblModalita = ""
    lblModalita.Visible = False
  End If
  '
  If (Left(lblInfo & "    ", 4) <> "SERV") Then
    If (Tabella1.Info <> "") Then
      lblInfo.Visible = True
      lblSEQUENZA.Visible = True
      lblInfo.BackColor = RGB(100, 255, 100)
      lblInfo.ForeColor = vbBlack
      lblInfo = " " & Tabella1.Info
    Else
      lblInfo = ""
      lblInfo.Visible = False
      lblSEQUENZA.Visible = False
    End If
  Else
    lblSEQUENZA.Visible = True
    lblInfo.Visible = True
  End If
  '
  For i = SuGrid1.LBound To SuGrid1.UBound
    SuGrid1(i).Clear
    Tabella1.INTESTAZIONE(i) = ""
  Next i
  '
  Dim tlev As String
  Dim tAbi As Boolean
  Dim ttit As Boolean
  '
  Livelli = GetSequenza(PaginaINS)
  While (Livelli <> "")
    tlev = EstraiDaSeq(Livelli)
    ttit = False
        If UCase(tlev) Like "D*" Then
      tAbi = False
    ElseIf UCase(tlev) Like "A*" Then
      tAbi = True
    ElseIf UCase(tlev) Like "T*" Then
      ttit = True
    End If
    tlev = Right(tlev, Len(tlev) - 1)
    If ttit Then
      i = tlev
      j = Tabella1.MESSAGGI(i).Gruppo - 1
      If (j = -1) Then j = 0
      Call SuGrid1(j).AddItem(Tabella1.MESSAGGI(i).NOME, True)
      If Tabella1.INTESTAZIONE(j) = "" Then Tabella1.INTESTAZIONE(j) = Tabella1.MESSAGGI(i).NOME
    Else
      i = GetParByINDICE(tlev)
      'MODIFICA L'ABILITAZIONE PER I PARAMETERI CONFIGURATI CON "D"
      Tabella1.Parametri(i).Modificabile = tAbi
      If Tabella1.Parametri(i).Abilitato Then
        '
        If (Tabella1.Parametri(i).NomeSPF <> "") And Not (Tabella1.Parametri(i).Rullo Or Tabella1.Parametri(i).Mola Or Tabella1.Parametri(i).ONLine) Then
          If Not ((";" & SPFPagina & ";") Like ("*;" & Tabella1.Parametri(i).NomeSPF & ";*")) Then
            If (SPFPagina <> "") Then SPFPagina = SPFPagina & ";"
            SPFPagina = SPFPagina & Tabella1.Parametri(i).NomeSPF
          End If
        End If
        '
        If Tabella1.Parametri(i).ONLine And Tabella1.MODALITA = "SPF" Then ATTIVA_POLLING = True
        '
        tGrid = Tabella1.Parametri(i).NOME
        If Tabella1.Parametri(i).TIPO = "B" Then
          tBool = Tabella1.Parametri(i).Limiti
        Else
          tBool = ""
        End If
        k = Tabella1.Parametri(i).Gruppo - 1
        If (ForzaGruppo > 0) Then k = ForzaGruppo - 1
        '
        If MostraVar Then
          If (Tabella1.Parametri(i).NActual >= 2) Then
            If Tabella1.Parametri(i).VARIABILE(2) Like "*_CORR" Then
              SuGrid1(k).AddItem Tabella1.Parametri(i).NOME & ";" & Tabella1.Parametri(i).VARIABILE(1) & "=" & ValVis(Tabella1.Parametri(i).VARIABILE(1), i), Tabella1.Parametri(i).Titolo
              SuGrid1(k).GetCellaByVar(Tabella1.Parametri(i).VARIABILE(1)).Correttore = True
              SuGrid1(k).GetCellaByVar(Tabella1.Parametri(i).VARIABILE(1)).ValueCorr = Tabella1.Parametri(i).VARIABILE(2)
            Else
              For j = 1 To Tabella1.Parametri(i).NActual
                If Tabella1.Parametri(i).ONLine Then
                  tGrid = tGrid & ";" & DDE_Idx(Tabella1.Parametri(i).ITEMDDE(j)) & "=" & Tabella1.Parametri(i).VARIABILE(j)
                Else
                  tGrid = tGrid & ";" & Tabella1.Parametri(i).VARIABILE(j) & "=" & ValVis(Tabella1.Parametri(i).VARIABILE(j), i)
                End If
              Next j
              Call SuGrid1(k).AddItem(tGrid, , tBool)
            End If
          Else
            For j = 1 To Tabella1.Parametri(i).NActual
              If Tabella1.Parametri(i).ONLine Then
                tGrid = tGrid & ";" & DDE_Idx(Tabella1.Parametri(i).ITEMDDE(j)) & "=" & Tabella1.Parametri(i).VARIABILE(j)
              Else
                tGrid = tGrid & ";" & Tabella1.Parametri(i).VARIABILE(j) & "=" & ValVis(Tabella1.Parametri(i).VARIABILE(j), i)
              End If
            Next j
            If Not Tabella1.Parametri(i).ONLine Or (Tabella1.Parametri(i).ONLine And (Tabella1.MODALITA = "SPF")) Then
              Call SuGrid1(k).AddItem(tGrid, , tBool)
            End If
          End If
        Else
          If (Tabella1.Parametri(i).NActual >= 2) Then
            If Tabella1.Parametri(i).VARIABILE(2) Like "*_CORR" Then
              SuGrid1(k).AddItem Tabella1.Parametri(i).NOME & ";" & Tabella1.Parametri(i).VARIABILE(1) & "=" & ValVis(Tabella1.Parametri(i).Actual(1), i), Tabella1.Parametri(i).Titolo
              SuGrid1(k).GetCellaByVar(Tabella1.Parametri(i).VARIABILE(1)).Correttore = True
              SuGrid1(k).GetCellaByVar(Tabella1.Parametri(i).VARIABILE(1)).ValueCorr = Tabella1.Parametri(i).Actual(2)
            Else
              For j = 1 To Tabella1.Parametri(i).NActual
                If Tabella1.Parametri(i).ONLine Then
                  tGrid = tGrid & ";" & DDE_Idx(Tabella1.Parametri(i).ITEMDDE(j)) & "=" & Tabella1.Parametri(i).Actual(j)
                Else
                  tGrid = tGrid & ";" & Tabella1.Parametri(i).VARIABILE(j) & "=" & ValVis(Tabella1.Parametri(i).Actual(j), i)
                End If
              Next j
              Call SuGrid1(k).AddItem(tGrid, , tBool)
            End If
          Else
            For j = 1 To Tabella1.Parametri(i).NActual
              'SVG061015: VISUALIZZAZIONE OFFLINE DEI VALORI ONLINE
              If Tabella1.Parametri(i).ONLine And Tabella1.MODALITA = "SPF" Then
                Dim LIMITI_VALORI()  As String
                Dim LIMITI_SIMBOLI() As String
                Dim INSERISCI        As Boolean
                Dim SIMBOLO          As String
                tstr = Tabella1.Parametri(i).VARIABILE(0)
                tstr = Trim$(UCase$(tstr))
                LIMITI_VALORI = Split(Tabella1.Parametri(i).Limiti, ",")
                LIMITI_SIMBOLI = Split(Tabella1.Parametri(i).Limiti_Lett, ",")
                INSERISCI = False: SIMBOLO = ""
                For L = 0 To UBound(LIMITI_VALORI)
                  If (LIMITI_VALORI(L) = Tabella1.Parametri(i).Actual(j)) Then
                    SIMBOLO = LIMITI_SIMBOLI(L)
                    INSERISCI = True
                    Exit For
                  End If
                Next L
                If (INSERISCI = True) Then
                tGrid = tGrid & ";" & DDE_Idx(Tabella1.Parametri(i).ITEMDDE(j)) & "=" & SIMBOLO
                Else
                tGrid = tGrid & ";" & DDE_Idx(Tabella1.Parametri(i).ITEMDDE(j)) & "=" & Tabella1.Parametri(i).Actual(j)
                End If
              Else
                tGrid = tGrid & ";" & Tabella1.Parametri(i).VARIABILE(j) & "=" & ValVis(Tabella1.Parametri(i).Actual(j), i)
              End If
            Next j
            'SVG270111: CONSENTE GESTIRE I PARAMETRI ONLINE IN OFFLINE
            'If Not Tabella1.Parametri(i).ONLine Or (Tabella1.Parametri(i).ONLine And (Tabella1.MODALITA = "SPF")) Then
              Call SuGrid1(k).AddItem(tGrid, , tBool)
            'End If
          End If
        End If
        For L = 1 To Tabella1.Parametri(i).NActual
          If Tabella1.Parametri(i).Evidenzia(L) Then
            SuGrid1(k).GetCellaByVar(Tabella1.Parametri(i).VARIABILE(L)).BackColor = vbRed
            SuGrid1(k).GetCellaByVar(Tabella1.Parametri(i).VARIABILE(L)).RowSelColor = vbRed
          End If
          If (Tabella1.Parametri(i).CodMola Or Tabella1.Parametri(i).CodRullo) And (Tabella1.Parametri(i).Actual(L) <> "") And (Tabella1.Parametri(i).Actual(L) <> "NONE") And (Tabella1.Parametri(i).Actual(L) <> "NOTFOUND") Then
            If Not ((";" & SPFPagina & ";") Like ("*;" & Tabella1.Parametri(i).Actual(L) & ";*")) Then
              If (SPFPagina <> "") Then SPFPagina = SPFPagina & ";"
              SPFPagina = SPFPagina & Tabella1.Parametri(i).Actual(L)
            End If
          End If
        Next L
      End If
    End If
  Wend
  '
  If SuGrid1(0).Visible = True Then Call SuGrid1(0).EnableAutoredraw
  If SuGrid1(1).Visible = True Then Call SuGrid1(1).EnableAutoredraw
  If SuGrid1(2).Visible = True Then Call SuGrid1(2).EnableAutoredraw
  '
  Call SuGrid1(0).DISEGNA_NEL_GRUPPO(UCase(ActualGroup))
  '
  If (UCase(ActualGroup) = "OEM1_MOLA71") And (Tabella1.MODALITA = "SPF") Then Call ABILITA_OEMX_POLLING
  '
  'VISUALIZZAZIONE BOTTONE DI ACQUISIZIONE QUOTE ASSI
  Dim NomeTb As String
  Dim Titolo As String
  Dim nriga  As Long
  Dim nstr   As Long
  '
  NomeTb = GetInfo(Tabella1.NOME, "POSIZIONI_TB", Path_LAVORAZIONE_INI)
  NomeTb = UCase$(Trim$(NomeTb))
  If (Len(NomeTb) > 0) Then
    If (UCase(ActualGroup) = NomeTb) Then
      
        nriga = 1521
        nstr = LoadString(g_hLanguageLibHandle, nriga, Titolo, 255)
        If nstr = 0 Then
           Titolo = "Load sensor position" & Chr(13)
        End If
      
        'Titolo = GetInfo(Tabella1.NOME, "POSIZIONI_TI", Path_LAVORAZIONE_INI)
      If (Len(Titolo) > 0) Then Command1.Caption = Titolo
      '
      If (NomeTb = "OEM1_MACCHINA85") Then
      stmp = GetInfo(Tabella1.NOME, "POSIZIONI_CMD", Path_LAVORAZIONE_INI)
      Command1.Top = SuGrid1(2).Top + val(stmp)
      Command1.Width = SuGrid1(0).Width * 0.95
      Command1.Left = SuGrid1(1).Left + SuGrid1(1).Width * 0.05 / 2
      Command1.Visible = True
      Else
      Command1.Top = SuGrid1(2).Top + 200
      Command1.Width = SuGrid1(0).Width * 0.95
      Command1.Left = SuGrid1(0).Left + SuGrid1(0).Width * 0.05 / 2
      Command1.Visible = True
      End If
      '
      Label1.Top = Command1.Top - 100
      Label1.Height = Command1.Height + 200
      Label1.Width = Command1.Width + 200
      Label1.Left = Command1.Left - 100
      Label1.Visible = True
      '
      'NON POSSIBILE PERCHE' RIMPICCIOLISCE LE CELLE SVG190615
      'If SuGrid1(0).Visible Then SuGrid1(0).Height = SuGrid1(1).Height
      '
    Else
      'CERCO UN'ALTRA CONFIGURAZIONE
      NomeTb = GetInfo(Tabella1.NOME, "POSIZIONI_TB(1)", Path_LAVORAZIONE_INI)
      NomeTb = UCase$(Trim$(NomeTb))
      If (Len(NomeTb) > 0) Then
        If (UCase(ActualGroup) = NomeTb) Then
          Titolo = GetInfo(Tabella1.NOME, "POSIZIONI_TI(1)", Path_LAVORAZIONE_INI)
          If (Len(Titolo) > 0) Then Command1.Caption = Titolo
          '
          Command1.Top = SuGrid1(2).Top + 200
          Command1.Width = SuGrid1(0).Width * 0.95
          Command1.Left = SuGrid1(0).Left + SuGrid1(0).Width * 0.05 / 2
          Command1.Visible = True
          '
          Label1.Top = Command1.Top - 100
          Label1.Height = Command1.Height + 200
          Label1.Width = Command1.Width + 200
          Label1.Left = Command1.Left - 100
          Label1.Visible = True
          '
          'NON POSSIBILE PERCHE' RIMPICCIOLISCE LE CELLE SVG190615
          'If SuGrid1(0).Visible Then SuGrid1(0).Height = SuGrid1(1).Height
          '
        Else
          Command1.Visible = False
          Label1.Visible = False
        End If
      Else
          Command1.Visible = False
          Label1.Visible = False
      End If
    End If
  Else
      Command1.Visible = False
      Label1.Visible = False
  End If
  '
  'VISUALIZZAZIONE BOTTONE DI RESET FASATURA MOLA-RULLO
  NomeTb = GetInfo(Tabella1.NOME, "POSIZIONI_TB0", Path_LAVORAZIONE_INI)
  NomeTb = UCase$(Trim$(NomeTb))
  If (Len(NomeTb) > 0) Then
    If (UCase(ActualGroup) = NomeTb) Then
      'Leggo abilitazione STATI processo
      stmp = GetInfo(Tabella1.NOME, "STATI_DISABILITATI", Path_LAVORAZIONE_INI)
      stmp = UCase$(Trim$(stmp))
      If (stmp = "N") Then
        If ((tTIPO_LAVORAZIONE = "MOLAVITE_SG") Or (tTIPO_LAVORAZIONE = "MOLAVITE")) Then
          'Leggo numero pagina
          stmp = Tabella1.Pagine(PaginaINS).Numero
          stmp = UCase$(Trim$(stmp))
          'If ((tTIPO_LAVORAZIONE = "MOLAVITE_SG") And (stmp = "7")) Or ((tTIPO_LAVORAZIONE = "MOLAVITE") And (stmp = "5")) Then   ' Pagina Utensile Mola
          If ((tTIPO_LAVORAZIONE = "MOLAVITE_SG") And (stmp = "7")) Or ((tTIPO_LAVORAZIONE = "MOLAVITE") And (ActualParGroup2 = "MOLA")) Then   ' Pagina Utensile Mola
                                    
            nriga = 1520
            nstr = LoadString(g_hLanguageLibHandle, nriga, Titolo, 255)
            If nstr = 0 Then
               Titolo = "Delete Synchro Wheel-Roller" & Chr(13)
            End If
            
            'Titolo = GetInfo(Tabella1.NOME, "POSIZIONI_TI0", Path_LAVORAZIONE_INI)
            If (Len(Titolo) > 0) Then Command4.Caption = Titolo
            'Command4.Top = SuGRID1(2).Top + 200 + 700
            Command4.Top = SuGrid1(0).Height - 330
            Command4.Width = SuGrid1(0).Width * 0.95
            Command4.Left = SuGrid1(0).Left + SuGrid1(0).Width * 0.05 / 2
            Command4.Visible = True
            'Else
            '  Command4.Visible = False
            'End If
          Else
            Command4.Visible = False
          End If
        Else
          Command4.Visible = False
        End If
      Else
        Command4.Visible = False
      End If
      '
    Else
      Command4.Visible = False
    End If
  Else
      Command4.Visible = False
  End If
  '
  'VISUALIZZAZIONE BOTTONE DI ACQUISIZIONE QUOTE ASSI ROBOT (12.11.2014)
  Dim NomeTb3   As String
  Dim Titolo3   As String
  Dim IndCmd3   As Integer
  Dim RobotCmd3 As String
  Dim NumTasti  As Integer
  Dim tmp01     As String
  '
  RobotCmd3 = GetInfo(Tabella1.NOME, "ROBOT", Path_LAVORAZIONE_INI)
  tmp01 = GetInfo(Tabella1.NOME, "NUMERO_TASTI", Path_LAVORAZIONE_INI)
  NumTasti = val(Trim(tmp01)) - 1
  If (RobotCmd3 = "Y") And (Helping = False) Then
    NomeTb3 = GetInfo(Tabella1.NOME, "POSIZIONI01_TB", Path_LAVORAZIONE_INI)
    NomeTb3 = UCase$(Trim$(NomeTb3))
    For IndCmd3 = 0 To NumTasti
      If (Len(NomeTb3) > 0) Then
        If (UCase(ActualGroup) = NomeTb3) Then
          Titolo3 = GetInfo(Tabella1.NOME, "POSIZIONI_TI(" & Trim(str(IndCmd3)) & ")", Path_LAVORAZIONE_INI)
          If (Len(Titolo3) > 0) Then Command3(IndCmd3).Caption = Titolo3
          Command3(IndCmd3).Top = SuGrid1(1).Top + 4200 - (IndCmd3 * 350)
          Command3(IndCmd3).Width = SuGrid1(1).Width * 0.98
          Command3(IndCmd3).Left = SuGrid1(1).Left + SuGrid1(1).Width * 0.01 '0.1 / 3
          Command3(IndCmd3).Visible = True
          Command3(IndCmd3).TabStop = True
        Else
          Command3(IndCmd3).Visible = False
        End If
      Else
        Command3(IndCmd3).Visible = False
      End If
    Next IndCmd3
  Else
    For IndCmd3 = 0 To NumTasti
        Command3(IndCmd3).Visible = False
    Next IndCmd3
  End If
  '
  'VISUALIZZAZIONE BOTTONE DI IMPORTAZIONE DATI
  Dim NomeTb2 As String
  Dim Titolo2 As String
  NomeTb2 = GetInfo(Tabella1.NOME, "IMPORTAZIONE_DES", Path_LAVORAZIONE_INI)
  NomeTb2 = UCase$(Trim$(NomeTb2))
  If (Len(NomeTb2) > 0) Then
    If (UCase(ActualGroup) = NomeTb2) Then
      Titolo2 = GetInfo(Tabella1.NOME, "IMPORTAZIONE_TIT", Path_LAVORAZIONE_INI)
      If (Len(Titolo2) > 0) Then Command2.Caption = Titolo2
      Picture2.Top = SuGrid1(0).Height - Command2.Height - 200 - 100
      Picture2.Height = Command2.Height + 200
      Picture2.Width = SuGrid1(0).Width
      Picture2.Left = SuGrid1(0).Left
      Command2.Top = SuGrid1(0).Height - Command2.Height - 200
      Command2.Width = SuGrid1(0).Width * 0.95
      Command2.Left = SuGrid1(0).Left + SuGrid1(0).Width * 0.05 / 2
      Command2.Visible = True
      Picture2.Visible = True
    Else
      Picture2.Visible = False
      Command2.Visible = False
    End If
  Else
      Picture2.Visible = False
      Command2.Visible = False
  End If
  '
  If (ATTIVA_POLLING = True) Then OnlinePolling.Enabled = True
  '
Exit Sub

DisegnaOEM_Error:
  'MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Sub: DisegnaOEM"
  Resume Next

End Sub

Public Property Get InfoBkColor() As Long
'
On Error Resume Next
  '
  m_lInfoBkColor = lblInfo.BackColor
  InfoBkColor = m_lInfoBkColor
  
End Property

Public Property Let InfoBkColor(ByVal lInfoBkColor As Long)
'
On Error Resume Next
  '
  m_lInfoBkColor = lInfoBkColor
  lblInfo.BackColor = m_lInfoBkColor
  Call UserControl.PropertyChanged("InfoBkColor")

End Property

Function GruppoPresente(ByVal Gruppo As Integer) As Boolean
'
On Error Resume Next
  '
  GruppoPresente = False
  Select Case Gruppo
    Case 0:        GruppoPresente = SuGrid1(Gruppo).Visible
    Case 1, 2, 3:  GruppoPresente = SuGrid1(Gruppo - 1).Visible
    Case Else:     GruppoPresente = False
  End Select

End Function

Private Function GruppoEs(db_file As String, Gruppo As Integer, NOME As String) As Boolean

Dim DB As Database
Dim RS As Recordset
'
On Error Resume Next
  '
  Set DB = OpenDatabase(db_file)
  Set RS = DB.OpenRecordset(NOME, dbOpenSnapshot)
  RS.FindFirst ("(GRUPPO=" & Gruppo & ") and (ABILITATO='Y')")
  If RS.NoMatch Then
      GruppoEs = False
  Else
      GruppoEs = True
  End If
  RS.Close
  DB.Close

End Function

Function GetParametroPag(Pagina As Integer, Optional Primo As Boolean = False) As String

Dim tlev As String
'
On Error Resume Next
  '
  If Primo Then
    Livello_I = GetSequenza(PaginaINS)
  End If
  If Livello_I <> "" Then
    GetParametroPag = EstraiDaSeq(Livello_I)
  Else
    GetParametroPag = ""
  End If

End Function

Function LivAbi(LIVELLO As String) As Boolean
'
On Error Resume Next
  '
  If UCase(LIVELLO) Like "D*" Then
    LivAbi = False
  Else
    LivAbi = True
  End If

End Function

Function LivTit(LIVELLO As String) As Boolean
'
On Error Resume Next
  '
  If UCase(LIVELLO) Like "T*" Then
    LivTit = True
  Else
    LivTit = False
  End If

End Function

Function LivPar(LIVELLO As String) As Integer
'
On Error Resume Next
  '
  LivPar = GetParByINDICE(Right(LIVELLO, Len(LIVELLO) - 1))

End Function

Sub InitSuGridDisegni()
'
Dim i     As Integer
Dim j     As Integer
Dim tstr  As String
'
On Error Resume Next
  '
  SuGridDisegni.Width = 2 / 3 * PictureDisegni.ScaleWidth
  SuGridDisegni.Height = 2 / 3 * PictureDisegni.ScaleHeight
  SuGridDisegni.Left = (PictureDisegni.ScaleWidth - SuGridDisegni.Width) / 2
  SuGridDisegni.Top = (PictureDisegni.ScaleHeight - SuGridDisegni.Height) / 2
  SuGridDisegni.Clear
  SuGridDisegni.CellHeight = 250
  SuGridDisegni.NewItemWidth = "1;1"
  Call SuGridDisegni.AddItem("Options", True)
  '
  i = -1
  On Error Resume Next
  '
  i = UBound(Tabella1.Opzioni)
  '
  On Error GoTo 0
  '
  If (i > 0) Then
    For i = 1 To UBound(Tabella1.Opzioni)
      tstr = Tabella1.Opzioni(i).NOME
      For j = 1 To Tabella1.Opzioni(i).NActual
        tstr = tstr & ";" & Tabella1.Opzioni(i).VARIABILE(j) & "=" & ValVisOPT(Tabella1.Opzioni(i).Actual(j), i)
      Next j
      Call SuGridDisegni.AddItem(tstr)
    Next i
  End If
  SuGridDisegni.EnableAutoredraw
  SuGridDisegni.SetFocus
  '
End Sub

Sub Riposiziona()
'
Dim IntSpazio       As Double
Dim ButSpazio       As Double
Dim i               As Integer
Dim SoloG1NoExpand  As Boolean
Dim FORMATO         As String
Dim IF1             As Integer
Dim IF2             As Integer
Dim nG1             As Integer
Dim nG2             As Integer
Dim nG3             As Integer
Dim tstr            As String
Dim INDICI()        As String
Dim ii              As Integer
'
On Error GoTo Riposiziona_Error
  '
  IntSpazio = 50
  PicTITOLO.Visible = False
  lstEdit(0).Visible = False
  lstEdit(1).Visible = False
  lstEdit(2).Visible = False
  txtEdit(0).Visible = False
  txtEdit(1).Visible = False
  txtEdit(2).Visible = False
  '
  SuGrid1(0).Visible = False
  SuGrid1(1).Visible = False
  SuGrid1(2).Visible = False
  '
  If (PaginaINS <> 0) Then
    pctDisegni.Visible = Tabella1.Pagine(PaginaINS).Input
    If pctDisegni.Visible Then pctDisegni.Cls
    SuGridDisegni.Visible = VisParGrafico
    If VisParGrafico Then
      Call InitSuGridDisegni
    End If
  End If
  '
  lblModalita.Height = UserControl.TextHeight("W") * 1.3
  lblInfo.Height = UserControl.TextHeight("W") * 1.3
  lblSEQUENZA.Height = UserControl.TextHeight("W") * 1.3
  lblModalita.Visible = True
  '
  If (Tabella1.NOME <> "") Then
    tstr = GetTuttiParSequenza(Tabella1.Pagine(PaginaINS).sequenza)
    INDICI = Split(tstr, ";")
    nG1 = 0: nG2 = 0: nG3 = 0
    For i = 1 To UBound(Tabella1.Parametri)
      For ii = 0 To UBound(INDICI)
        If (Tabella1.Parametri(i).INDICE = val(INDICI(ii))) And Tabella1.Parametri(i).Abilitato Then
          If (Tabella1.Parametri(i).Gruppo = 1) Then nG1 = nG1 + 1: Exit For
          If (Tabella1.Parametri(i).Gruppo = 2) Then nG2 = nG2 + 1: Exit For
          If (Tabella1.Parametri(i).Gruppo = 3) Then nG3 = nG3 + 1: Exit For
        End If
      Next ii
    Next i
    If (nG1 > 0) Then SuGrid1(0).Visible = True Else SuGrid1(0).Visible = False
    If (nG2 > 0) Then SuGrid1(1).Visible = True Else SuGrid1(1).Visible = False
    If (nG3 > 0) Then SuGrid1(2).Visible = True Else SuGrid1(2).Visible = False
    '
    ButSpazio = 0
    '
    If (SuGrid1(1).Visible Or SuGrid1(2).Visible) Or (Tabella1.Pagine(PaginaINS).Input) Then
      '
      SuGrid1(0).Top = IIf(lblModalita.Visible, lblModalita.Height, 0)
      SuGrid1(0).Left = 0
      SuGrid1(0).Height = UserControl.Height - IIf(lblModalita.Visible, lblModalita.Height, 0)
      SuGrid1(0).Width = (UserControl.Width - IntSpazio) / 2
      If pctDisegni.Visible Then
        pctDisegni.Left = SuGrid1(0).Width + IntSpazio
        pctDisegni.Top = SuGrid1(0).Top
        pctDisegni.Height = UserControl.Height - IIf(lblModalita.Visible, lblModalita.Height, 0)
        pctDisegni.Width = UserControl.Width - SuGrid1(0).Width - IntSpazio
      End If
      SuGrid1(1).Left = SuGrid1(0).Width + IntSpazio
      SuGrid1(1).Width = UserControl.Width - SuGrid1(0).Width - IntSpazio
      '
      SuGrid1(2).Left = SuGrid1(1).Left
      SuGrid1(2).Width = SuGrid1(1).Width
      '
      If SuGrid1(1).Visible And Not SuGrid1(2).Visible Then
          SuGrid1(1).Top = SuGrid1(0).Top
          SuGrid1(1).Height = SuGrid1(0).Height - ButSpazio
      End If
      '
      If Not SuGrid1(1).Visible And SuGrid1(2).Visible Then
          SuGrid1(2).Top = SuGrid1(0).Top
          SuGrid1(2).Height = SuGrid1(0).Height - ButSpazio
      End If
      'SVG141211
      'VERSIONE PROVVISORIA DA SISTEMARE CON IL CALCOLO DEL NUMERO DEI PARAMETRI
      'SVG090212
      'FORMATO SPECIFICATO NEL FILE INI
      'CAMBIA LA VISUALIZZAZIONE DELLA PAGINA
      If SuGrid1(1).Visible And SuGrid1(2).Visible Then
          FORMATO = GetInfo(Tabella1.Pagine(PaginaINS).Nomegruppo, "FORMATO", Path_LAVORAZIONE_INI)
          FORMATO = Trim$(FORMATO)
          If (Len(FORMATO) <= 0) Then
            FORMATO = GetInfo("Configurazione", "FORMATO", Path_LAVORAZIONE_INI)
            FORMATO = Trim$(FORMATO)
            If (Len(FORMATO) <= 0) Then FORMATO = "3/2"
          End If
          IF1 = CInt(Mid$(FORMATO, 1, 1))
          IF2 = CInt(Mid$(FORMATO, 3, 1))
          If (IF1 = 0) Or (IF2 = 0) Then IF1 = 3: IF2 = 2
          SuGrid1(1).Top = SuGrid1(0).Top
          SuGrid1(1).Height = (UserControl.Height - IntSpazio - ButSpazio - IIf(lblModalita.Visible, lblModalita.Height, 0)) * IF1 / (IF1 + IF2)
          SuGrid1(2).Top = SuGrid1(1).Top + SuGrid1(1).Height + IntSpazio
          SuGrid1(2).Height = (UserControl.Height - IntSpazio - ButSpazio - IIf(lblModalita.Visible, lblModalita.Height, 0)) * IF2 / (IF1 + IF2)
      End If
      '
    Else
      '
      SuGrid1(0).Top = IIf(lblModalita.Visible, lblModalita.Height, 0)
      SuGrid1(0).Left = 0
      SuGrid1(0).Height = UserControl.Height - IIf(lblModalita.Visible, lblModalita.Height, 0)
      If (ButSpazio <> 0) Then
        SuGrid1(0).Width = (UserControl.Width - IntSpazio) / 2
      Else
        SuGrid1(0).Width = UserControl.Width
      End If
      '
    End If
    'DIMENSIONAMENTO DELL'INTESTAZIONE
    lblInfo.Top = 0
    lblSEQUENZA.Top = 0
    lblModalita.Top = 0
    lblModalita.Width = UserControl.TextWidth(IIf(Tabella1.MODALITA = "SPF", "XXXX", Tabella1.MODALITA) & "X" & Tabella1.LetteraLavorazione & "X")
    lblModalita.Left = UserControl.Width - lblModalita.Width
    lblSEQUENZA.Width = UserControl.TextWidth("XXX/XXX")
    lblSEQUENZA.Left = lblModalita.Left - lblSEQUENZA.Width
    lblInfo.Left = 0
    lblInfo.Width = lblSEQUENZA.Left
  End If
  '
  If (NPagine > 0) Then
    If Tabella1.Pagine(PaginaINS).Input Then SuGrid1(0).Visible = True
  End If
  '
Exit Sub

Riposiziona_Error:
  WRITE_DIALOG "Error " & Err.Number & " (" & Err.Description & ") in Sub: Riposiziona"
  
End Sub

Public Function SetFocus()
'
Dim i As Integer
'
On Error Resume Next
  '
  For i = 0 To SuGrid1.count - 1
    If SuGrid1(i).Visible Then
      SuGrid1(i).SetFocus
      SuGrid1(i).SelFirst
      Exit Function
    End If
  Next i

End Function

Private Sub Command1_Click()
'
Dim k                 As Integer
Dim ii                As Integer
Dim ret               As Long
Dim stmp              As String
Dim sRESET            As String
Dim iRESET            As Integer
Dim TIPO_LAVORAZIONE  As String
Dim MODALITA          As String
Dim ITEMDDE           As String
Dim tNomePezzo        As String
'
Dim DB As Database
Dim TB As Recordset
Dim INDICI()          As String
Dim VV()              As String
Dim VA()              As String
Dim VN()              As String
Dim POSIZIONI_LST     As String
Dim stmp2             As String
Dim ValStatoG1        As String
Dim ValStatoG2        As String
Dim Stato_OK          As Integer
Dim StatoMicro        As String
Dim PosMicro          As String
'
On Error GoTo errCommand1
'
'Controllo lo Stato
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  stmp = GetInfo(TIPO_LAVORAZIONE, "STATI_DISABILITATI", Path_LAVORAZIONE_INI)
  stmp = UCase$(Trim$(stmp))
  Stato_OK = 0
  If stmp = "N" Then
     'Stati processo abilitati
     stmp2 = GetInfo(TIPO_LAVORAZIONE, "VarStatoG1", Path_LAVORAZIONE_INI)
     stmp2 = UCase$(Trim$(stmp2))
     ValStatoG1 = OPC_LEGGI_DATO(stmp2)
     
     stmp2 = GetInfo(TIPO_LAVORAZIONE, "VarStatoG2", Path_LAVORAZIONE_INI)
     stmp2 = UCase$(Trim$(stmp2))
     ValStatoG2 = OPC_LEGGI_DATO(stmp2)
     
     StatoMicro = GetInfo(TIPO_LAVORAZIONE, "StatoMicroFas", Path_LAVORAZIONE_INI)
     StatoMicro = UCase$(Trim$(StatoMicro))
     
     'Leggo POSMICRO_G[1]
     stmp2 = GetInfo(Tabella1.NOME, "PosMicro", Path_LAVORAZIONE_INI)
     stmp2 = UCase$(Trim$(stmp2))
     PosMicro = OPC_LEGGI_DATO(stmp2)

     If ((ValStatoG1 = StatoMicro) And (ValStatoG2 = StatoMicro)) Then
        'Stato(30) abilita tasto aquisizione quote assi
        'Acquisizione quote Assi x micro fasatura abilitato
        Stato_OK = 1
     Else
        'In tutti gli altri Stati funzione non abilitata
        Stato_OK = 0
        Select Case LINGUA
          Case "IT"
          stmp = "Funzione non abilitata in questo stato"
          Case "GR"
          stmp = "Funktion nicht aktiviert"
          Case Else
          stmp = "Function not enabled in this state"
        End Select
        ret = MsgBox(stmp)
     End If
  Else
     'Stati processo disabilitati
     Stato_OK = 1
  End If
  
  If (Stato_OK = 1) Then
  MODALITA = GetInfo("Configurazione", "Modalita", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))
  If (MODALITA = "SPF") Then
    'TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
    'TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
    iRESET = CONTROLLO_STATO_RESET
    If (iRESET = 1) Then
      'Rilevo le variabili x micro fasatore da settare
      POSIZIONI_LST = GetInfo(TIPO_LAVORAZIONE, "POSIZIONI_LST", Path_LAVORAZIONE_INI)
      POSIZIONI_LST = Trim$(POSIZIONI_LST)
      NomeTb = GetInfo(TIPO_LAVORAZIONE, "POSIZIONI_TB", Path_LAVORAZIONE_INI)
      NomeTb = UCase$(Trim$(NomeTb))
      If (Len(NomeTb) > 0) And (Len(POSIZIONI_LST) > 0) Then
        INDICI = Split(POSIZIONI_LST, ";")
        ReDim VV(UBound(INDICI))
        ReDim VA(UBound(INDICI))
        ReDim VN(UBound(INDICI))
        Set DB = OpenDatabase(PathDB, True, False)
        Set TB = DB.OpenRecordset(TIPO_LAVORAZIONE, dbOpenTable)
        For ii = 0 To UBound(INDICI)
          TB.Index = "INDICE"
          TB.Seek "=", CInt(INDICI(ii))
          VV(ii) = "" & TB.Fields("VARIABILE")
          If (LINGUA = "CH") Or (LINGUA = "RU") Then
          VN(ii) = "" & TB.Fields("NOME_" & "UK")
          Else
          VN(ii) = "" & TB.Fields("NOME_" & LINGUA)
          End If
        Next ii
        TB.Close
        DB.Close
        tNomePezzo = GetInfo(TIPO_LAVORAZIONE, "NomePezzo(" & Trim(Asc(LETTERA_LAVORAZIONE) - 64) & ")", Path_LAVORAZIONE_INI)
        Call OEMX.SuOEM1.InitSUOEM(PathDB, TIPO_LAVORAZIONE, LINGUA, MODALITA, LETTERA_LAVORAZIONE, tNomePezzo, Path_LAVORAZIONE_INI, RICERCA_NOME_TIPO(TIPO_LAVORAZIONE), True, LastPage)
        'Rilevo gli item per lettura quote assi ed effettuo le letture
        For ii = 0 To UBound(INDICI)
          ITEMDDE = GetInfo(TIPO_LAVORAZIONE, "POSIZIONI_DDE(" & Format$(ii + 1, "###0") & ")", Path_LAVORAZIONE_INI)
          If (Len(VV(ii)) > 0) And (Len(ITEMDDE) > 0) Then
            k = val(GetInfo(TIPO_LAVORAZIONE, "POSIZIONI_FMT", Path_LAVORAZIONE_INI))
            If (k > 0) Then
            VA(ii) = OPC_LEGGI_DATO(ITEMDDE)
            VA(ii) = frmt(val(VA(ii)), k)
            Else
            VA(ii) = OPC_LEGGI_DATO(ITEMDDE)
            End If
          End If
        Next ii
        'Preparo domanda con Msgbox
        stmp = Command1.Caption & Chr(13) & Chr(13)
        For ii = 0 To UBound(INDICI)
          stmp = stmp & VN(ii) & "=" & VA(ii) & Chr(13)
        Next ii
        StopRegieEvents
        ret = MsgBox(stmp, vbQuestion + vbYesNo + vbDefaultButton2)
        ResumeRegieEvents
        If (ret = vbYes) Then
          For ii = 0 To UBound(INDICI)
            If (Len(VV(ii)) > 0) Then
              Call SP(VV(ii), VA(ii), False)
            End If
          Next ii
          Call AggiornaDB
          Call OEMX.SuOEM1.CreaSPF

          If (TIPO_LAVORAZIONE = "MOLAVITE") Or (TIPO_LAVORAZIONE = "INGRANAGGI_ESTERNIV") Then
             'Setta POSMICRO_G[1]=1 x Lavorazione MOLAVITE
              stmp = GetInfo(Tabella1.NOME, "PosMicro", Path_LAVORAZIONE_INI)
              stmp = UCase$(Trim$(stmp))
              Call OPC_SCRIVI_DATO(stmp, "1")
          End If
        
        Else
          WRITE_DIALOG "Operation aborted by user!!!"
        End If
        Call DisegnaOEM
      Else
        POSIZIONI_LST = GetInfo(TIPO_LAVORAZIONE, "POSIZIONI_LST(1)", Path_LAVORAZIONE_INI)
        POSIZIONI_LST = Trim$(POSIZIONI_LST)
        NomeTb = GetInfo(TIPO_LAVORAZIONE, "POSIZIONI_TB(1)", Path_LAVORAZIONE_INI)
        NomeTb = UCase$(Trim$(NomeTb))
        If (Len(NomeTb) > 0) And (Len(POSIZIONI_LST) > 0) Then
          INDICI = Split(POSIZIONI_LST, ";")
          ReDim VV(UBound(INDICI))
          ReDim VA(UBound(INDICI))
          ReDim VN(UBound(INDICI))
          Set DB = OpenDatabase(PathDB, True, False)
          Set TB = DB.OpenRecordset(TIPO_LAVORAZIONE, dbOpenTable)
          For ii = 0 To UBound(INDICI)
            TB.Index = "INDICE"
            TB.Seek "=", CInt(INDICI(ii))
            VV(ii) = "" & TB.Fields("VARIABILE")
            If (LINGUA = "CH") Or (LINGUA = "RU") Then
            VN(ii) = "" & TB.Fields("NOME_" & "UK")
            Else
            VN(ii) = "" & TB.Fields("NOME_" & LINGUA)
            End If
          Next ii
          TB.Close
          DB.Close
          tNomePezzo = GetInfo(TIPO_LAVORAZIONE, "NomePezzo(" & Trim(Asc(LETTERA_LAVORAZIONE) - 64) & ")", Path_LAVORAZIONE_INI)
          Call OEMX.SuOEM1.InitSUOEM(PathDB, TIPO_LAVORAZIONE, LINGUA, MODALITA, LETTERA_LAVORAZIONE, tNomePezzo, Path_LAVORAZIONE_INI, RICERCA_NOME_TIPO(TIPO_LAVORAZIONE), True, LastPage)
          For ii = 0 To UBound(INDICI)
            ITEMDDE = GetInfo(TIPO_LAVORAZIONE, "POSIZIONI_DDE(1," & Format$(ii + 1, "###0") & ")", Path_LAVORAZIONE_INI)
            If (Len(VV(ii)) > 0) And (Len(ITEMDDE) > 0) Then
              k = val(GetInfo(TIPO_LAVORAZIONE, "POSIZIONI_FMT", Path_LAVORAZIONE_INI))
              If (k > 0) Then
              VA(ii) = OPC_LEGGI_DATO(ITEMDDE)
              VA(ii) = frmt(val(VA(ii)), k)
              Else
              VA(ii) = OPC_LEGGI_DATO(ITEMDDE)
              End If
            End If
          Next ii
          stmp = Command1.Caption & Chr(13) & Chr(13)
          For ii = 0 To UBound(INDICI)
            stmp = stmp & VN(ii) & "=" & VA(ii) & Chr(13)
          Next ii
          StopRegieEvents
          ret = MsgBox(stmp, vbQuestion + vbYesNo + vbDefaultButton2)
          ResumeRegieEvents
          If (ret = vbYes) Then
            For ii = 0 To UBound(INDICI)
              If (Len(VV(ii)) > 0) Then
                Call SP(VV(ii), VA(ii), False)
              End If
            Next ii
            Call AggiornaDB
            Call OEMX.SuOEM1.CreaSPF
          Else
            WRITE_DIALOG "Operation aborted by user!!!"
          End If
          Call DisegnaOEM
        End If
      End If
    Else
      WRITE_DIALOG "Data cannot be modified: CNC is not on RESET state"
    End If
  Else
    WRITE_DIALOG "Operation not enabled on OFF-LINE!!"
  End If
  Else
    WRITE_DIALOG "Operation not enabled in this state!!"
  End If
  '
Exit Sub

errCommand1:
  WRITE_DIALOG "SUB Command1: ERROR " & Format$(Err)
Exit Sub

End Sub

Private Sub Command1_GotFocus()
'
On Error Resume Next
  '
  Label1.BackColor = QBColor(12)
  '
End Sub

Private Sub Command1_LostFocus()
'
On Error Resume Next
  '
  Label1.BackColor = &HEBE1D7
  '
End Sub

Private Sub Command2_Click()
'
Dim k                 As Integer
Dim ii                As Integer
Dim ret               As Long
Dim stmp              As String
Dim sRESET            As String
Dim iRESET            As Integer
Dim TIPO_LAVORAZIONE  As String
Dim MODALITA          As String
Dim ITEMDDE           As String
Dim tNomePezzo        As String
'
Dim Nomegruppo        As String
Dim NomeTb            As String
'
Dim DB As Database
Dim TB As Recordset
Dim INDICI()          As String
Dim VV()              As String
Dim VA()              As String
Dim VN()              As String
Dim IMPORTAZIONE_LST  As String
Dim IMPORTAZIONE_DBB  As String
Dim IMPORTAZIONE_TAB  As String
'
On Error GoTo errCommand2
  '
  MODALITA = GetInfo("Configurazione", "Modalita", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))
  If (MODALITA = "SPF") Then
    StopRegieEvents
    ret = MsgBox(Command2.Caption & "?", vbInformation + vbYesNo + vbDefaultButton2, "WARNING")
    ResumeRegieEvents
    If (ret = vbYes) Then
      TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
      TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
      iRESET = CONTROLLO_STATO_RESET
      If (iRESET = 1) Then
        NomeTb = GetInfo(TIPO_LAVORAZIONE, "IMPORTAZIONE_DES", Path_LAVORAZIONE_INI)
        NomeTb = UCase$(Trim$(NomeTb))
        If (Len(NomeTb) > 0) Then
          tNomePezzo = GetInfo(TIPO_LAVORAZIONE, "NomePezzo(" & Trim(Asc(LETTERA_LAVORAZIONE) - 64) & ")", Path_LAVORAZIONE_INI)
          Call OEMX.SuOEM1.InitSUOEM(PathDB, TIPO_LAVORAZIONE, LINGUA, MODALITA, LETTERA_LAVORAZIONE, tNomePezzo, Path_LAVORAZIONE_INI, RICERCA_NOME_TIPO(TIPO_LAVORAZIONE), True, LastPage)
          Nomegruppo = GetInfo(TIPO_LAVORAZIONE, "IMPORTAZIONE_SRC", Path_LAVORAZIONE_INI)
          Nomegruppo = UCase$(Trim$(Nomegruppo))
          Call SCARICA_GRUPPO(Nomegruppo)
          Call WritePrivateProfileString(NomeTb, vbNullString, vbNullString, g_chOemPATH & "\" & "SWAPPO.INI.")
          Call SCRIVI_SEZIONE_INI(g_chOemPATH & "\" & Nomegruppo & ".SPF", g_chOemPATH & "\SWAPPO.INI", NomeTb)
          IMPORTAZIONE_LST = GetInfo(TIPO_LAVORAZIONE, "IMPORTAZIONE_LST", Path_LAVORAZIONE_INI)
          IMPORTAZIONE_LST = UCase$(Trim$(IMPORTAZIONE_LST))
          If (Len(IMPORTAZIONE_LST) > 0) Then
            IMPORTAZIONE_DBB = GetInfo(TIPO_LAVORAZIONE, "IMPORTAZIONE_DBB", Path_LAVORAZIONE_INI)
            IMPORTAZIONE_DBB = UCase$(Trim$(IMPORTAZIONE_DBB))
            IMPORTAZIONE_DBB = g_chOemPATH & "\" & IMPORTAZIONE_DBB
            IMPORTAZIONE_TAB = GetInfo(TIPO_LAVORAZIONE, "IMPORTAZIONE_TAB", Path_LAVORAZIONE_INI)
            IMPORTAZIONE_TAB = UCase$(Trim$(IMPORTAZIONE_TAB))
            INDICI = Split(IMPORTAZIONE_LST, ";")
            ReDim VV(UBound(INDICI))
            ReDim VA(UBound(INDICI))
            ReDim VN(UBound(INDICI))
            Set DB = OpenDatabase(IMPORTAZIONE_DBB, True, False)
            Set TB = DB.OpenRecordset(IMPORTAZIONE_TAB, dbOpenTable)
            For ii = 0 To UBound(INDICI)
              TB.Index = "INDICE"
              TB.Seek "=", CInt(INDICI(ii))
              VV(ii) = "" & TB.Fields("ITEMDDE")
              VN(ii) = "" & TB.Fields("VARIABILE")
            Next ii
            TB.Close
            DB.Close
            For ii = 0 To UBound(INDICI)
              VA(ii) = OPC_LEGGI_DATO(VV(ii))
            Next ii
            For ii = 0 To UBound(INDICI)
              If (Len(VV(ii)) > 0) Then
                ret = WritePrivateProfileString(NomeTb, VN(ii), VA(ii), g_chOemPATH & "\SWAPPO.INI")
              End If
            Next ii
          End If
          Call LeggiSWAP
          Call DisegnaOEM
          WRITE_DIALOG "Data have been load by " & Nomegruppo & "!!!"
        End If
      Else
        WRITE_DIALOG "Data cannot be modified: CNC is not on RESET state"
      End If
    Else
      WRITE_DIALOG "Operation aborted by user!!!"
    End If
  Else
    WRITE_DIALOG "Operation not enabled on OFF-LINE!!"
  End If
  '
Exit Sub

errCommand2:
  WRITE_DIALOG "SUB Command2: ERROR " & Format$(Err)
Exit Sub

End Sub

Private Sub Command2_GotFocus()
  
  Picture2.BackColor = QBColor(12)
  
End Sub

Private Sub Command2_LostFocus()

  Picture2.BackColor = &H80000005
  
End Sub

Private Sub Command3_Click(Index As Integer)

Dim k                 As Integer
Dim ii                As Integer
Dim ret               As Long
Dim stmp              As String
Dim sRESET            As String
Dim iRESET            As Integer
Dim TIPO_LAVORAZIONE  As String
Dim MODALITA          As String
Dim ITEMDDE           As String
Dim tNomePezzo        As String
'
Dim DB As Database
Dim TB As Recordset
Dim INDICI()          As String
Dim VV()              As String
Dim VA()              As String
Dim VN()              As String
Dim POSIZIONI_LST     As String
Dim NomeTb3           As String
'
On Error GoTo errCommand3
  '
  MODALITA = GetInfo("Configurazione", "Modalita", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))
  If (MODALITA = "SPF") Then
    TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
    TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
    iRESET = CONTROLLO_STATO_RESET
    If (iRESET = 1) Then
      POSIZIONI_LST = GetInfo(TIPO_LAVORAZIONE, "POSIZIONI_LST(" & Trim(str(Index)) & ")", Path_LAVORAZIONE_INI)
      POSIZIONI_LST = Trim$(POSIZIONI_LST)
      NomeTb3 = GetInfo(TIPO_LAVORAZIONE, "POSIZIONI01_TB", Path_LAVORAZIONE_INI)
      NomeTb3 = UCase$(Trim$(NomeTb3))
      
      If (Len(NomeTb3) > 0) And (Len(POSIZIONI_LST) > 0) Then
        INDICI = Split(POSIZIONI_LST, ";")
        ReDim VV(UBound(INDICI))  'Nome Variabile
        ReDim VA(UBound(INDICI))  'Valore Attuale Variabile
        ReDim VN(UBound(INDICI))  'Nome Parametro
        Set DB = OpenDatabase(PathDB, True, False)
        Set TB = DB.OpenRecordset(TIPO_LAVORAZIONE, dbOpenTable)
        For ii = 0 To UBound(INDICI)
          TB.Index = "INDICE"
          TB.Seek "=", CInt(INDICI(ii))
          VV(ii) = "" & TB.Fields("VARIABILE")
          If (LINGUA = "CH") Or (LINGUA = "RU") Then
          VN(ii) = "" & TB.Fields("NOME_" & "UK")
          Else
          VN(ii) = "" & TB.Fields("NOME_" & LINGUA)
          End If
        Next ii
        TB.Close
        DB.Close
        tNomePezzo = GetInfo(TIPO_LAVORAZIONE, "NomePezzo(" & Trim(Asc(LETTERA_LAVORAZIONE) - 64) & ")", Path_LAVORAZIONE_INI)
        Call OEMX.SuOEM1.InitSUOEM(PathDB, TIPO_LAVORAZIONE, LINGUA, MODALITA, LETTERA_LAVORAZIONE, tNomePezzo, Path_LAVORAZIONE_INI, RICERCA_NOME_TIPO(TIPO_LAVORAZIONE), True, LastPage)
        For ii = 0 To UBound(INDICI)
          ITEMDDE = GetInfo(TIPO_LAVORAZIONE, "POSIZIONI01_DDE(" & Format$(ii + 1, "###0") & ")", Path_LAVORAZIONE_INI)
          If (Len(VV(ii)) > 0) And (Len(ITEMDDE) > 0) Then
            k = val(GetInfo(TIPO_LAVORAZIONE, "POSIZIONI01_FMT", Path_LAVORAZIONE_INI))
            If (k > 0) Then
            VA(ii) = OPC_LEGGI_DATO(ITEMDDE)
            VA(ii) = frmt(val(VA(ii)), k)
            Else
            VA(ii) = OPC_LEGGI_DATO(ITEMDDE)
            End If
          End If
        Next ii
        stmp = Command3(Index).Caption & Chr(13) & Chr(13)
        For ii = 0 To UBound(INDICI)
          stmp = stmp & VN(ii) & "=" & VA(ii) & Chr(13)
        Next ii
        StopRegieEvents
        ret = MsgBox(stmp, vbQuestion + vbYesNo + vbDefaultButton2)
        ResumeRegieEvents
        If (ret = vbYes) Then
          For ii = 0 To UBound(INDICI)
            If (Len(VV(ii)) > 0) Then
              Call SP(VV(ii), VA(ii), False)
            End If
          Next ii
          Call AggiornaDB
          Call OEMX.SuOEM1.CreaSPF
        Else
          WRITE_DIALOG "Operation aborted by user!!!"
        End If
        Call DisegnaOEM
      End If
    
    Else
      WRITE_DIALOG "Data cannot be modified: CNC is not on RESET state"
    End If
  Else
    WRITE_DIALOG "Operation not enabled on OFF-LINE!!"
  End If
  '
Exit Sub

errCommand3:
  WRITE_DIALOG "SUB Command3: ERROR " & Format$(Err)
Exit Sub

End Sub

Private Sub Command3_GotFocus(Index As Integer)

On Error GoTo errCommand3GotFocus

  Command3_Selezionato = True

Exit Sub

errCommand3GotFocus:
  WRITE_DIALOG "SUB Command3_GotFocus: ERROR " & Format$(Err)
Exit Sub

End Sub

Private Sub Command3_LostFocus(Index As Integer)

On Error GoTo errCommand3LostFocus

  Command3_Selezionato = False

Exit Sub

errCommand3LostFocus:
  WRITE_DIALOG "SUB Command3_LostFocus: ERROR " & Format$(Err)
Exit Sub

End Sub

Private Sub Command4_Click()

'Reset  "Setup mola o Fasatura mola"
Dim stmp              As String
Dim riga              As String
Dim rsp               As Integer
Dim Valore            As Integer

Dim ret               As Long
Dim TIPO_LAVORAZIONE  As String
Dim stmp1             As String
Dim stmp2             As String
Dim ValStatoG1        As String
Dim ValStatoG2        As String
Dim Stato_OK          As Integer
Dim StatiReset        As String
Dim Stato_Reset()     As String
Dim ii                As Integer


On Error GoTo errCommand4
  '
 'Controllo lo Stato
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  stmp = GetInfo(TIPO_LAVORAZIONE, "STATI_DISABILITATI", Path_LAVORAZIONE_INI)
  stmp = UCase$(Trim$(stmp))
  
  Stato_OK = 0
  If stmp = "N" Then
     'Stati processo abilitati
     stmp1 = GetInfo(TIPO_LAVORAZIONE, "VarStatoG1", Path_LAVORAZIONE_INI)
     stmp1 = UCase$(Trim$(stmp1))
     ValStatoG1 = OPC_LEGGI_DATO(stmp1)
     stmp2 = GetInfo(TIPO_LAVORAZIONE, "VarStatoG2", Path_LAVORAZIONE_INI)
     stmp2 = UCase$(Trim$(stmp2))
     ValStatoG2 = OPC_LEGGI_DATO(stmp2)
     StatiReset = GetInfo(TIPO_LAVORAZIONE, "StatoResetFas", Path_LAVORAZIONE_INI)
     StatiReset = UCase$(Trim$(StatiReset))
     Stato_Reset = Split(StatiReset, ";")
     For ii = 0 To UBound(Stato_Reset)
         If (ValStatoG1 = Stato_Reset(ii)) And (ValStatoG2 = Stato_Reset(ii)) Then
            'Tasto Reset fasatura Mola-Rullo abilitato
            'Stati (10,20,25,30) abilitano il tasto di reset "Fasatura Mola-Rullo"
             Stato_OK = 1
         End If
     Next ii
     
  Else
     'Stati processo disabilitati
     'Tasto Reset fasatura Mola-Rullo disabilitato
     Stato_OK = 0
  End If
 
If (Stato_OK = 1) Then
   
  If (ValStatoG1 = "10") Then
     
    Select Case LINGUA
       Case "IT"
        riga = "Cancellare Setup Mola-Cilindratore ?"
       Case "GR"
        riga = "Do you want delete Wheel-Cutter Setup ?"
       Case "UK"
        riga = "Do you want delete Wheel-Cutter Setup ?"
       Case Else
        riga = "Do you want delete Wheel-Cutter Setup ?"
    End Select

    StopRegieEvents
    rsp = MsgBox(riga, 256 + 4 + 32, "WARNING")
    ResumeRegieEvents
  
    If (rsp = vbYes) Then
      Call OPC_SCRIVI_DATO(stmp1, "00")
      Call OPC_SCRIVI_DATO(stmp2, "00")
      Select Case LINGUA
       Case "IT"
        riga = "Setup Mola-Cilindratore cancellato"
       Case "GR"
        riga = "Setup Wheel-Cutter has been deleted"
       Case "UK"
        riga = "Setup Wheel-Cutter has been deleted"
       Case Else
        riga = "Setup Wheel-Cutter has been deleted"
      End Select
      StopRegieEvents
      rsp = MsgBox(riga, 64)
      ResumeRegieEvents
    End If
  
  Else
    'STATO 30
    Select Case LINGUA
       Case "IT"
        riga = "Cancellare Fasatura Mola-Rullo?"
       Case "GR"
        riga = "Do you want delete Synchro Wheel-Roller?"
       Case "UK"
        riga = "Do you want delete Synchro Wheel-Roller?"
       Case Else
        riga = "Do you want delete Synchro Wheel-Roller?"
    End Select

    StopRegieEvents
    rsp = MsgBox(riga, 256 + 4 + 32, "WARNING")
    ResumeRegieEvents
  
    If (rsp = vbYes) Then
       'Imposta lo STATO 10
       Call OPC_SCRIVI_DATO(stmp1, "10")
       'Imposta lo STATO 10
       Call OPC_SCRIVI_DATO(stmp2, "10")
   
       stmp = GetInfo(Tabella1.NOME, "FasaMola", Path_LAVORAZIONE_INI)
       stmp = UCase$(Trim$(stmp))
       'Reset Variabile Fasatura Mola-Rullo
       Call OPC_SCRIVI_DATO(stmp, "0")
   
       Valore = 0
       Call SP("FASAMOLARULLO_G", Valore)
   
       Select Case LINGUA
          Case "IT"
           riga = "Fasatura Mola-Rullo � stata cancellata"
          Case "GR"
           riga = "Synchro Wheel-Roller has been deleted"
          Case "UK"
           riga = "Synchro Wheel-Roller has been deleted"
          Case Else
           riga = "Synchro Wheel-Roller has been deleted"
       End Select
       StopRegieEvents
       rsp = MsgBox(riga, 64)
       ResumeRegieEvents
    End If
   
  End If
   

Else

    'In tutti gli altri Stati funzione non abilitata
    Select Case LINGUA
      Case "IT"
      stmp = "Funzione non abilitata in questo stato! Fas. Mola-Rullo non attiva o Cancellare Fas.Mola-Pezzo "
      Case "GR"
      stmp = "Function not enabled in this state! Synchro Wheel-Roller is not done or Delete Synchro Wheel-Piece"
      Case Else
      stmp = "Function not enabled in this state! Synchro Wheel-Roller is not done or Delete Synchro Wheel-Piece"
    End Select
    ret = MsgBox(stmp)

End If

Exit Sub

errCommand4:
  WRITE_DIALOG "SUB Command4: ERROR " & Format$(Err)

End Sub

Private Sub OnlinePolling_Timer()
'
On Error Resume Next
  '
  Call AggiornaONLINE(True)
  '
End Sub

Sub CreaSPF_RULLI(Evento As Boolean)

Dim i               As Integer
Dim j               As Integer
Dim k               As Integer
Dim tstr()          As String
Dim NomiSPF()       As String
Dim TestoSPF()      As String
Dim TestoSPF_OLD()  As String
Dim RigaSPF         As String
Dim NFil            As Integer
Dim tstr2           As String
Dim tPath           As String
Dim CommPath        As String

On Error GoTo CreaSPF_RULLI_Error
  '
  tPath = "RULLI"
  tPath = Tabella1.PathOEM & tPath & "\"
  If Dir(tPath, vbDirectory) = "" Then Call MkDir(tPath)
  ReDim NomiSPF(1)
  '
  NomiSPF(1) = Lp_Str("CODICERULLO_G")
  '
  ReDim TestoSPF(UBound(NomiSPF))
  ReDim TestoSPF_OLD(UBound(NomiSPF))
  For i = 1 To UBound(NomiSPF)
    If Dir(tPath & NomiSPF(i) & ".SPF") <> "" Then
      NFil = FreeFile
      Open tPath & NomiSPF(i) & ".SPF" For Input As #NFil
        While Not EOF(NFil)
          Line Input #1, tstr2
          If TestoSPF_OLD(i) <> "" Then TestoSPF_OLD(i) = TestoSPF_OLD(i) & vbCrLf
          TestoSPF_OLD(i) = TestoSPF_OLD(i) & tstr2
        Wend
      Close #NFil
      Kill tPath & NomiSPF(i) & ".SPF"
    End If
  Next i
  For k = 1 To UBound(NomiSPF)
    TestoSPF(k) = ";PROGRAMMA : " & NomiSPF(k) & vbCrLf
    TestoSPF(k) = TestoSPF(k) & ";VERSIONE  : " & Date & " " & Time()
    For i = 1 To UBound(Tabella1.Parametri)
      If Tabella1.Parametri(i).Abilitato And Tabella1.Parametri(i).Rullo Then
        TestoSPF(k) = TestoSPF(k) & vbCrLf
        TestoSPF(k) = TestoSPF(k) & Tabella1.Parametri(i).VARIABILE(k) & "="
        If Tabella1.Parametri(i).TIPO = "A" Then TestoSPF(k) = TestoSPF(k) & """"
        TestoSPF(k) = TestoSPF(k) & Tabella1.Parametri(i).Actual(k)
        If Tabella1.Parametri(i).TIPO = "A" Then TestoSPF(k) = TestoSPF(k) & """"
        TestoSPF(k) = TestoSPF(k) & " ; " & Tabella1.Parametri(i).NOME
      End If
    Next i
    TestoSPF(k) = TestoSPF(k) & vbCrLf & "M17 ;FINE PROGRAMMA " & NomiSPF(k)
  Next k
  '
  SPF = ""
  For i = 1 To UBound(NomiSPF)
    'If (NomiSPF(i) <> "NOTFOUND") And (NomiSPF(i) <> "NONE") Then
    If (NomiSPF(i) <> "NOTFOUND") Then
      NFil = FreeFile
      Open tPath & NomiSPF(i) & ".SPF" For Output As #NFil
        Print #1, TestoSPF(i)
      Close #NFil
      SPF = SPF & TestoSPF(i) & vbCrLf
    End If
  Next i

Exit Sub

CreaSPF_RULLI_Error:
  MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Sub: CreaSPF_RULLI"

End Sub

Sub CreaSPF_MOLE(Evento As Boolean)

Dim i               As Integer
Dim j               As Integer
Dim k               As Integer
Dim tstr()          As String
Dim NomiSPF()       As String
Dim TestoSPF()      As String
Dim TestoSPF_OLD()  As String
Dim RigaSPF         As String
Dim NFil            As Integer
Dim tstr2           As String
Dim tPath           As String
Dim CommPath        As String

On Error GoTo CreaSPF_MOLE_Error
  '
  tPath = GetInfo("Configurazione", "DIRETTORIO_MOLE", Path_LAVORAZIONE_INI)
  tPath = UCase$(Trim$(tPath))
  If (Len(tPath) <= 0) Then tPath = "PARAMETRI"
  tPath = Tabella1.PathOEM & tPath & "\"
  If Dir(tPath, vbDirectory) = "" Then Call MkDir(tPath)
  ReDim NomiSPF(2)
  '
  NomiSPF(1) = Lp_Str("CODMOLA_G[1]")
  NomiSPF(2) = Lp_Str("CODMOLA_G[2]")
  '
  If NomiSPF(2) = "NOTFOUND" Then ReDim Preserve NomiSPF(1)
  '
  ReDim TestoSPF(UBound(NomiSPF))
  ReDim TestoSPF_OLD(UBound(NomiSPF))
  For i = 1 To UBound(NomiSPF)
    If Dir(tPath & NomiSPF(i) & ".SPF") <> "" Then
      NFil = FreeFile
      Open tPath & NomiSPF(i) & ".SPF" For Input As #NFil
        While Not EOF(NFil)
          Line Input #1, tstr2
          If TestoSPF_OLD(i) <> "" Then TestoSPF_OLD(i) = TestoSPF_OLD(i) & vbCrLf
          TestoSPF_OLD(i) = TestoSPF_OLD(i) & tstr2
        Wend
      Close #NFil
      Kill tPath & NomiSPF(i) & ".SPF"
    End If
  Next i
  For k = 1 To UBound(NomiSPF)
    TestoSPF(k) = ";PROGRAMMA : " & NomiSPF(k) & vbCrLf
    TestoSPF(k) = TestoSPF(k) & ";VERSIONE  : " & Date & " " & Time()
    For i = 1 To UBound(Tabella1.Parametri)
      If Tabella1.Parametri(i).Abilitato And Tabella1.Parametri(i).Mola Then
        TestoSPF(k) = TestoSPF(k) & vbCrLf
        TestoSPF(k) = TestoSPF(k) & Tabella1.Parametri(i).VARIABILE(k) & "="
        If Tabella1.Parametri(i).TIPO = "A" Then TestoSPF(k) = TestoSPF(k) & """"
        TestoSPF(k) = TestoSPF(k) & Tabella1.Parametri(i).Actual(k)
        If Tabella1.Parametri(i).TIPO = "A" Then TestoSPF(k) = TestoSPF(k) & """"
        TestoSPF(k) = TestoSPF(k) & " ; " & Tabella1.Parametri(i).NOME
      End If
    Next i
    TestoSPF(k) = TestoSPF(k) & vbCrLf & "M17 ;FINE PROGRAMMA " & NomiSPF(k)
  Next k
  '
  SPF = ""
  For i = 1 To UBound(NomiSPF)
    If (NomiSPF(i) <> "NOTFOUND") And (NomiSPF(i) <> "NONE") Then
      NFil = FreeFile
      Open tPath & NomiSPF(i) & ".SPF" For Output As #NFil
        Print #1, TestoSPF(i)
      Close #NFil
      SPF = SPF & TestoSPF(i) & vbCrLf
    End If
  Next i

Exit Sub

CreaSPF_MOLE_Error:
  MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Sub: CreaSPF_MOLE"

End Sub

'****m* SuOEM/CreaSPF
' DESCRIZIONE:
' Crea i file SPF sul PC/PCU nella cartella OEM\PARAMETRI1 2 3
' a seconda dell'indice della lavorazione corrente.
' Per tutti gli SPF elaborati, viene generato un evento CNCChanged
' se ci sono differenze rispetto all'ultimo SPF caricato o in qualunque caso
' se il parametro Evento=True
' INGRESSI:
' Evento as Booelan
' DEFINIZIONE:
Sub CreaSPF(Optional Evento As Boolean = False, Optional NON_CARICARE1 As String = "", Optional NON_CARICARE2 As String = "", Optional CARICA As Boolean = True)
'
Dim i               As Integer
Dim j               As Integer
Dim k               As Integer
Dim tstr()          As String
Dim NomiSPF()       As String
Dim TestoSPF()      As String
Dim TestoSPF_OLD()  As String
Dim RigaSPF         As String
Dim NFil            As Integer
Dim tstr2           As String
Dim tPath           As String
Dim CommPath        As String
Dim tMola           As String
Dim SPFMola         As String
Dim AggiungiNomi    As Boolean
Dim tRullo          As String
Dim Atmp            As String * 255
Dim riga            As String
Dim stmp            As String
Dim ret             As Long
'
On Error GoTo errCreaSPF
  '
  tPath = "PARAMETRI"
  If (Tabella1.LetteraLavorazione <> "") Then
    tPath = tPath & Trim(str(Asc(Tabella1.LetteraLavorazione) - Asc("A") + 1))
  End If
  tPath = Tabella1.PathOEM & tPath & "\"
  If (Dir(tPath, vbDirectory) = "") Then Call MkDir(tPath)
  '
  If (Tabella1.IniCfgLav = "") Then
    StopRegieEvents
    MsgBox "LAVORAZIONE INI FILE NOT DEFINED!!!", vbCritical, "SUB CreaSPF: WARNING"
    ResumeRegieEvents
  End If
  '
  SPFMola = GetINI(Tabella1.IniCfgLav, Tabella1.NOME, "NomeTBMola")
  tstr2 = GetINI(Tabella1.IniCfgLav, Tabella1.NOME, "AggiungiNomi")
  If (tstr2 = "Y") Then AggiungiNomi = True Else AggiungiNomi = False
  '
  SPF = ""
  '
  ReDim NomiSPF(0)
  For i = 1 To UBound(Tabella1.Parametri)
    If (Tabella1.Parametri(i).NomeSPF <> "") Then
      For j = 0 To UBound(NomiSPF)
        If (Tabella1.Parametri(i).NomeSPF = NomiSPF(j)) Then
          Exit For
        End If
      Next j
      If (j > UBound(NomiSPF)) Then
        ReDim Preserve NomiSPF(j)
        NomiSPF(j) = Tabella1.Parametri(i).NomeSPF
      End If
    End If
  Next i
  '
  ReDim TestoSPF(UBound(NomiSPF))
  ReDim TestoSPF_OLD(UBound(NomiSPF))
  '
  For i = 1 To UBound(NomiSPF)
    If (Dir(tPath & NomiSPF(i) & ".SPF") <> "") Then
      NFil = FreeFile
      Open tPath & NomiSPF(i) & ".SPF" For Input As #NFil
        While Not EOF(NFil)
          Line Input #1, tstr2
          If (TestoSPF_OLD(i) <> "") Then TestoSPF_OLD(i) = TestoSPF_OLD(i) & vbCrLf
          TestoSPF_OLD(i) = TestoSPF_OLD(i) & tstr2
        Wend
      Close #NFil
      Call Kill(tPath & NomiSPF(i) & ".SPF")
    End If
  Next i
  '
  tstr2 = Tabella1.NomePezzo
  '
  For k = 1 To UBound(NomiSPF)
    TestoSPF(k) = ";PROGRAMMA : " & NomiSPF(k) & " (" & tstr2 & ")" & vbCrLf
    TestoSPF(k) = TestoSPF(k) & ";VERSIONE  : " & Date & " " & Time()
    For i = 1 To UBound(Tabella1.Parametri)
      '
      If Tabella1.Parametri(i).Abilitato And (UCase(Tabella1.Parametri(i).NomeSPF) = UCase(NomiSPF(k))) Then
        For j = 1 To Tabella1.Parametri(i).NActual
          Tabella1.Parametri(i).Evidenzia(j) = False
          TestoSPF(k) = TestoSPF(k) & vbCrLf
          TestoSPF(k) = TestoSPF(k) & Tabella1.Parametri(i).VARIABILE(j) & "="
          If Tabella1.Parametri(i).TIPO = "A" Then TestoSPF(k) = TestoSPF(k) & """"
          TestoSPF(k) = TestoSPF(k) & Tabella1.Parametri(i).Actual(j)
          If Tabella1.Parametri(i).TIPO = "A" Then TestoSPF(k) = TestoSPF(k) & """"
          If AggiungiNomi Then TestoSPF(k) = TestoSPF(k) & " ; " & Tabella1.Parametri(i).NOME
        Next j
      End If
      '
      If (UCase(NomiSPF(k)) = UCase(SPFMola)) Then
        If (Tabella1.Parametri(i).NActual >= 1) Then
          If (Tabella1.Parametri(i).VARIABILE(1) = "CODMOLA_G[1]") Then
            tMola = Tabella1.Parametri(i).Actual(1)
            If (tMola <> "") And (tMola <> "NEW") And (tMola <> "NONE") And (tMola <> "NOTFOUND") Then
              stmp = GetInfo("Configurazione", "DIRETTORIO_MOLE", Path_LAVORAZIONE_INI)
              stmp = UCase$(Trim$(stmp))
              If (Len(stmp) <= 0) Then stmp = "PARAMETRI"
              TestoSPF(k) = TestoSPF(k) & vbCrLf
              TestoSPF(k) = TestoSPF(k) & "EXTCALL(""" & g_chOemPATH & "\" & stmp & "\" & tMola & ".SPF"")"
            End If
            If (tMola = "NONE") Then
              TestoSPF(k) = TestoSPF(k) & vbCrLf
              TestoSPF(k) = TestoSPF(k) & "CODMOLA_G[1]=""NONE"""
            End If
          End If
        End If
        '
        If (Tabella1.Parametri(i).NActual >= 2) Then
          If (Tabella1.Parametri(i).VARIABILE(2) = "CODMOLA_G[2]") Then
            tMola = Tabella1.Parametri(i).Actual(2)
            If (tMola <> "") And (tMola <> "NEW") And (tMola <> "NONE") Then
              stmp = GetInfo("Configurazione", "DIRETTORIO_MOLE", Path_LAVORAZIONE_INI)
              stmp = UCase$(Trim$(stmp))
              If (Len(stmp) <= 0) Then stmp = "PARAMETRI"
              TestoSPF(k) = TestoSPF(k) & vbCrLf
              TestoSPF(k) = TestoSPF(k) & "EXTCALL(""" & g_chOemPATH & "\" & stmp & "\" & tMola & ".SPF"")"
            Else
              TestoSPF(k) = TestoSPF(k) & vbCrLf
              TestoSPF(k) = TestoSPF(k) & "CODMOLA_G[2]=""NONE"""
            End If
          End If
        End If
        '
        If (Tabella1.Parametri(i).VARIABILE(1) = "CODICERULLO_G") Then
            tRullo = Tabella1.Parametri(i).Actual(1)
            If (tRullo <> "") And (tRullo <> "NEW") And (tRullo <> "NOTFOUND") Then
              TestoSPF(k) = TestoSPF(k) & vbCrLf
              TestoSPF(k) = TestoSPF(k) & "EXTCALL(""" & g_chOemPATH & "\RULLI\" & tRullo & ".SPF"")"
            Else
              TestoSPF(k) = TestoSPF(k) & vbCrLf
              tRullo = "DEFAULT"
              Tabella1.Parametri(i).Actual(1) = tRullo
              TestoSPF(k) = TestoSPF(k) & "EXTCALL(""" & g_chOemPATH & "\RULLI\" & tRullo & ".SPF"")"
            End If
          End If
      End If
      '
    Next i
    TestoSPF(k) = TestoSPF(k) & vbCrLf & "M17 ;FINE PROGRAMMA " & NomiSPF(k)
  Next k
  '
  SPF = ""
  For i = 1 To UBound(NomiSPF)
    If (NomiSPF(i) = NON_CARICARE1) Or (NomiSPF(i) = NON_CARICARE2) Then
      For j = 0 To UBound(Tabella1.Pagine)
        If Tabella1.Pagine(j).Nomegruppo = NomiSPF(i) Then stmp = Tabella1.Pagine(j).NOME1
      Next j
      ret = LoadString(g_hLanguageLibHandle, 1003, Atmp, 255)
      If (ret > 0) Then riga = Left$(Atmp, ret)
      stmp = riga & " " & stmp & "?"
      ret = MsgBox(stmp, vbCritical + vbDefaultButton2 + vbYesNo, "WARNING")
    Else
      ret = vbYes
    End If
    If (ret = vbYes) Then
      NFil = FreeFile
      Open tPath & NomiSPF(i) & ".SPF" For Output As #NFil
        Print #1, TestoSPF(i)
      Close #NFil
      SPF = SPF & TestoSPF(i) & vbCrLf
      If (Evento Or ConfrontaSPF(TestoSPF(i), TestoSPF_OLD(i))) Then
        RaiseEvent CNCChanged(NomiSPF(i), "ALL", "ALL", CaricamentoINCorso, CARICA)
      End If
    End If
  Next i
  '
  Call CreaSPF_MOLE(Evento)
  Call CreaSPF_RULLI(Evento)
  '
Exit Sub

errCreaSPF:
  i = Err
  StopRegieEvents
  MsgBox Error(i), vbCritical, "SUB: CreaSPF"
  ResumeRegieEvents
  Resume Next
  
End Sub

Private Function ConfrontaSPF(Spf1 As String, Spf2 As String) As Boolean
'
Dim tSpf1() As String
Dim tSpf2() As String
Dim tstr1   As String
Dim tstr2   As String
Dim i       As Integer
'
On Error Resume Next
  '
  ConfrontaSPF = False
    If (Spf1 = "" And Spf2 <> "") Or (Spf1 <> "" And Spf2 = "") Then
        ConfrontaSPF = True
        Exit Function
  ElseIf (Spf1 = Spf2) Then
        ConfrontaSPF = False
        Exit Function
  End If
  tSpf1 = Split(Spf1, vbCrLf)
  tSpf2 = Split(Spf2, vbCrLf)
  If (UBound(tSpf1) <> UBound(tSpf2)) Then
        ConfrontaSPF = True
        Exit Function
  End If
  For i = 2 To UBound(tSpf1)
    tstr1 = Trim(Split(tSpf1(i), ";")(0))
    tstr2 = Trim(Split(tSpf2(i), ";")(0))
    If (tstr1 <> tstr2) Then
        ConfrontaSPF = True
        Exit Function
    End If
  Next i
  '
End Function

Function EstraiNum(ByRef Stringa As String) As String
'
Dim Top As String
'
On Error Resume Next
  
  Top = Top & Mid(Stringa, 1, 1)
  
  If Not IsNumeric(Top) Then
    Stringa = Right(Stringa, Len(Stringa) - 1)
  End If
  
  Do While IsNumeric(Top & Mid(Stringa, 1, 1))
    Top = Top & Mid(Stringa, 1, 1)
    Stringa = Right(Stringa, Len(Stringa) - 1)
    If Stringa = "" Then Exit Do
  Loop
  
  EstraiNum = Top

End Function

Public Sub AiutoTrue()
'
On Error Resume Next
  '
  Helping = True
  pctMessaggi.Visible = True
  '
End Sub

Public Sub AiutoFalse()
'
On Error Resume Next
  '
  Helping = False
  pctMessaggi.Visible = False
  '
End Sub

Public Sub AiutoPar(Help As Boolean)
'****f* SuOEM/AiutoPar
' DESCRIZIONE:
' Mostra/Nasconde la finestra contenente l'aiuto per il parametro selezionato in funzione del valore del parametro Help
'
Dim PAR     As defPar
'
On Error Resume Next
  '
  If Command3_Selezionato Then
    Call SuGrid1(0).SelFirst
      Exit Sub
  End If
  '
  Helping = Help
  '
  PAR = GetParByVar(VarCorr)
  '
  pctMessaggi.Visible = Help
  If Help Then
    Call PosMsg(Tabella1.Parametri(PAR.PAR).NOME, Tabella1.Parametri(PAR.PAR).Aiuto, Tabella1.Parametri(PAR.PAR).imgPath)
  Else
    Call Disegni.RidisegnaPagina(PaginaINS, False)
    Call DisegnaOEM
  End If
  Call SuGrid1(SCon).SelFirst
  '
End Sub

'****f* SuOEM/Aiuto
' DESCRIZIONE:
' Mostra la finestra contenente l'aiuto per il parametro selezionato.
' Per nascondere l'aiuto, chiamare nuovamente il metodo.
' DEFINIZIONE:
Public Sub Setta_Aiuto()
'
Dim PAR     As defPar
'
On Error Resume Next
  '
  If Command3_Selezionato Then
    Call SuGrid1(0).SelFirst
      Exit Sub
  End If
  '
  Helping = Not Helping
  '
  PAR = GetParByVar(VarCorr)
  '
  pctMessaggi.Visible = Helping
  If Helping Then
    Call PosMsg(Tabella1.Parametri(PAR.PAR).NOME, Tabella1.Parametri(PAR.PAR).Aiuto, Tabella1.Parametri(PAR.PAR).imgPath)
  Else
    Call Disegni.RidisegnaPagina(PaginaINS, False)
    Call DisegnaOEM
  End If
  Call SuGrid1(SCon).SelFirst
  '
End Sub

'****f* SuOEM/Aiuto
' DESCRIZIONE:
' Mostra la finestra contenente l'aiuto per il parametro selezionato.
' Per nascondere l'aiuto, chiamare nuovamente il metodo.
' DEFINIZIONE:
Public Sub Aiuto()
'
Dim PAR     As defPar
'
On Error Resume Next
  '
  If Command3_Selezionato Then
    Call SuGrid1(0).SelFirst
      Exit Sub
  End If
  '
  Helping = Not Helping
  '
  PAR = GetParByVar(VarCorr)
  '
  pctMessaggi.Visible = Helping
  If Helping Then Call PosMsg(Tabella1.Parametri(PAR.PAR).NOME, Tabella1.Parametri(PAR.PAR).Aiuto, Tabella1.Parametri(PAR.PAR).imgPath)
  '
  Call Disegni.RidisegnaPagina(PaginaINS, False)
  Call DisegnaOEM
  Call SuGrid1(SCon).SelFirst
  '
End Sub

Sub ImpostaFont()
'
Dim i As Integer
'
On Error Resume Next
  '
  If (Tabella1.LINGUA = "CH") Then
    '
    Set CurFontNor = FontCinese
    Set CurFontTit = FontCineseTitolo
    For i = 0 To 2
      Set SuGrid1(i).Font = FontCinese
    Next i
    '
  ElseIf (Tabella1.LINGUA = "RU") Then
    Set CurFontNor = FontRusso
    Set CurFontTit = FontRussoTitolo
    For i = 0 To 2
      Set SuGrid1(i).Font = FontRusso
    Next i
    '
  Else
    '
    Set CurFontNor = FontNormale
    Set CurFontTit = FontNormaleTitolo
    For i = 0 To 2
      Set SuGrid1(i).Font = FontNormale
    Next i
    '
  End If
  '
  CurFontNor.Size = UserControl.Parent.Font.Size * g_tAppRes.hFact
  CurFontTit.Size = UserControl.Parent.Font.Size * g_tAppRes.hFact
  '
  For i = SuGrid1.LBound To SuGrid1.UBound
    Set SuGrid1(i).Font = CurFontNor
    'Set PicTitolo(i).Font = CurFontTit
    'Set PicMsg.Font = FontCinese
  '  Set txtEdit(i).Font = CurFontNor
  '  Set lstEdit(i).Font = CurFontNor
  Next i
  Set lblModalita.Font = CurFontTit
  Set lblInfo.Font = CurFontTit
  Set lblSEQUENZA.Font = CurFontTit
  Set UserControl.Font = CurFontNor
  '
End Sub

Private Sub pctDisegni_DblClick()
'
Static Big As Boolean
'
On Error Resume Next
  '
  If Big Then
    pctDisegni.Left = UserControl.ScaleWidth / 2
    pctDisegni.Width = UserControl.ScaleWidth / 2
  Else
    pctDisegni.Left = UserControl.ScaleLeft
    pctDisegni.Width = UserControl.ScaleWidth
  End If
  Big = Not Big
  Call Disegni.RidisegnaPagina(PaginaINS, False)
  Call DisegnaOEM
  '
End Sub

Private Sub pctMessaggi_GotFocus()
'
On Error Resume Next
  '
  SuGrid1(SCon).SetFocus
  
End Sub

Private Sub pctMessaggi_Resize()
'
On Error Resume Next
  '
  scrMessaggi.Top = 0
  scrMessaggi.Height = pctMessaggi.ScaleHeight
  scrMessaggi.Left = pctMessaggi.ScaleWidth - scrMessaggi.Width
  
End Sub

Private Sub scrMessaggi_Change()
'
On Error GoTo errscrMessaggi_Change
  '
  Call PosMsg(MsgLastTitolo, MsgLastTesto, MsgLastPicture, MsgLastTipo)

Exit Sub

errscrMessaggi_Change:
  MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Sub: scrMessaggi_Change"
  
End Sub

Private Sub scrMessaggi_GotFocus()
'
On Error Resume Next
  '
  SuGrid1(SCon).SetFocus

End Sub

Private Sub LeggiArchivioMola(Mola As String)
'
Dim Tvar()  As String
Dim tVal()  As String
Dim tdati() As String
Dim tstr    As String
Dim i       As Integer
Dim j       As Integer
Dim tE      As String
Dim DbM     As Database
Dim rsM     As Recordset
Dim Nuova   As Boolean
'
On Error Resume Next
  '
  ReDim Tvar(0)
  ReDim tVal(0)
  tE = "[0," & Mola & "]"
  If Trim(GetINI(Tabella1.IniCfgLav, Tabella1.NOME, "NomeTBMola")) = "" Then Exit Sub
  '
  For i = 1 To UBound(Tabella1.Parametri)
    If Tabella1.Parametri(i).Mola And Not Tabella1.Parametri(i).CodMola Then
      For j = 1 To Tabella1.Parametri(i).NActual
        If Not Tabella1.Parametri(i).CodMola Then
          ReDim Preserve Tvar(UBound(Tvar) + 1)
          Tvar(UBound(Tvar)) = Tabella1.Parametri(i).VARIABILE(j)
          ReDim Preserve tVal(UBound(tVal) + 1)
          tVal(UBound(tVal)) = Tabella1.Parametri(i).Default
        Else
          If Tabella1.Parametri(i).Actual(j) = "NONE" Then
            Exit Sub
          End If
        End If
      Next j
    End If
  Next i
  '
  Set DbM = OpenDatabase(Tabella1.DBase)
  Set rsM = DbM.OpenRecordset(Tabella1.NOME & "_MOLE", dbOpenDynaset)
  rsM.FindFirst ("[NOME_PEZZO]='" & UCase(Lp_Str("CODMOLA_G" & "[" & Mola & "]")) & "'")
  If Not rsM.NoMatch Then
    tdati = Split(rsM.Fields("DATI"), vbCrLf)
    For i = 0 To UBound(tdati)
    tstr = Split(tdati(i), "=")(0)
      For j = 1 To UBound(Tvar)
        If Tvar(j) = tstr & tE Then
          tVal(j) = Split(tdati(i), "=")(1)
        End If
      Next j
    Next i
    Nuova = False
  Else
    Nuova = True
  End If
  rsM.Close
  DbM.Close
  '
  If Nuova Then
    Call AggiornaArchivioMola(Mola)
  Else
    For j = 1 To UBound(Tvar)
      If Right(Tvar(j), 5) = tE Then Call SP(Tvar(j), tVal(j), False)
    Next j
    Call AggiornaDB
  End If
  '
End Sub

Private Sub LeggiArchivioRullo()
'
Dim Tvar()  As String
Dim tVal()  As String
Dim tdati() As String
Dim tstr    As String
Dim i       As Integer
Dim j       As Integer
Dim tE      As String
Dim DbM     As Database
Dim rsM     As Recordset
Dim Nuova   As Boolean
'
On Error Resume Next
  '
  ReDim Tvar(0)
  ReDim tVal(0)
  '
  If Trim(GetINI(Tabella1.IniCfgLav, Tabella1.NOME, "NomeTBMola")) = "" Then Exit Sub
  '
  For i = 1 To UBound(Tabella1.Parametri)
    If Tabella1.Parametri(i).Rullo And Not Tabella1.Parametri(i).CodRullo Then
      For j = 1 To Tabella1.Parametri(i).NActual
        If Not Tabella1.Parametri(i).CodRullo Then
          ReDim Preserve Tvar(UBound(Tvar) + 1)
          Tvar(UBound(Tvar)) = Tabella1.Parametri(i).VARIABILE(j)
          ReDim Preserve tVal(UBound(tVal) + 1)
          tVal(UBound(tVal)) = Tabella1.Parametri(i).Default
        'Else
        '  If Tabella1.Parametri(i).Actual(j) = "NONE" Then
        '    Exit Sub
        '  End If
        End If
      Next j
    End If
  Next i
  '
  Set DbM = OpenDatabase(Tabella1.DBase)
  Set rsM = DbM.OpenRecordset(Tabella1.NOME & "_RULLI", dbOpenDynaset)
  rsM.FindFirst ("[NOME_PEZZO]='" & UCase(Lp_Str("CODICERULLO_G")) & "'")
  If Not rsM.NoMatch Then
    tdati = Split(rsM.Fields("DATI"), vbCrLf)
    For i = 0 To UBound(tdati)
      tstr = Split(tdati(i), "=")(0)
      For j = 1 To UBound(Tvar)
        If (Tvar(j) = tstr) Then
          tVal(j) = Split(tdati(i), "=")(1)
        End If
      Next j
    Next i
    Nuova = False
  Else
    Nuova = True
  End If
  rsM.Close
  DbM.Close
  '
  If Nuova Then
    Call AggiornaArchivioRullo
  Else
    For j = 1 To UBound(Tvar)
      Call SP(Tvar(j), tVal(j), False)
    Next j
    Call AggiornaDB
  End If
  '
End Sub

Private Sub CancellaDaArchivioMola(Codice As String)
'
Dim DbM   As Database
Dim rsM   As Recordset
Dim tstr  As String
'
On Error Resume Next
  '
  tstr = Codice
  If (tstr = "") Then Exit Sub
  If (tstr = "NONE") Then Exit Sub
  If (tstr = "NEW") Then Exit Sub
  Set DbM = OpenDatabase(Tabella1.DBase)
  Set rsM = DbM.OpenRecordset(Tabella1.NOME & "_MOLE", dbOpenDynaset)
  rsM.FindFirst ("[NOME_PEZZO]='" & UCase(tstr) & "'")
  If Not rsM.NoMatch Then rsM.Delete
  rsM.Close
  DbM.Close

End Sub

Private Sub CancellaDaArchivioRulli(Codice As String)
'
Dim DbM   As Database
Dim rsM   As Recordset
Dim tstr  As String
'
On Error Resume Next
  '
  tstr = Codice
  If (tstr = "") Then Exit Sub
  'If (tstr = "NONE") Then Exit Sub
  If (tstr = "NEW") Then Exit Sub
  Set DbM = OpenDatabase(Tabella1.DBase)
  Set rsM = DbM.OpenRecordset(Tabella1.NOME & "_RULLI", dbOpenDynaset)
  rsM.FindFirst ("[NOME_PEZZO]='" & UCase(tstr) & "'")
  If Not rsM.NoMatch Then rsM.Delete
  rsM.Close
  DbM.Close

End Sub

Private Sub SuGrid1_EditDisEnabling(Index As Integer, Key As String, VARIABILE As String, NewValue As String)
'
Dim PAR             As defPar
Dim Verifica_Limiti As Boolean
Dim tNewValue       As String
Dim tNewValue2      As String
Dim iResponse       As String
Dim stmp            As String
'
Dim i               As Integer
Dim Npar            As Integer
'
On Error GoTo SuGrid1_EditDisEnabling_Error
  '
  Status = 100
  PAR = GetParByVar(VARIABILE)
  Verifica_Limiti = True
  tNewValue = UCase(Trim(ValMem(NewValue, PAR.PAR)))
  tNewValue = Replace(tNewValue, ",", ".")
  If (Trim(tNewValue) = "") And (Tabella1.Parametri(PAR.PAR).CodMola Or Tabella1.Parametri(PAR.PAR).CodRullo) Then
    WRITE_DIALOG "Wrong Value!!!"
    Status = 1
    Exit Sub
  End If
  Status = 0
  If (Trim(UCase(Tabella1.Parametri(PAR.PAR).Actual(PAR.col))) <> tNewValue) Or (Tabella1.Parametri(PAR.PAR).Evidenzia(PAR.col)) Then
    If Tabella1.Parametri(PAR.PAR).VARIABILE(PAR.col) Like "*_CORR" Then
      If (PAR.col > 1) Then
        If Tabella1.Parametri(PAR.PAR).VARIABILE(PAR.col - 1) & "_CORR" = VARIABILE Then
          tNewValue2 = val(Tabella1.Parametri(PAR.PAR).Actual(PAR.col - 1)) + val(tNewValue)
        End If
      End If
    Else
      tNewValue2 = tNewValue
    End If
    If VerLimiti(PAR, tNewValue2) Or Tabella1.Parametri(PAR.PAR).CodMola Then
      If Verifica_Limiti Then
        If Tabella1.Parametri(PAR.PAR).DoppiaConf Then
          'NON USARE DOPPIA CONFERMA PER GRUPPI 2 E 3 QUANDO IL PARAMETRO SI SELEZIONA DA UNA LISTA
          'USO L'INGLESE PER LA VISUALIZZAZIONE DELLA CASELLA DI DIALOGO WINDOWS
          StopRegieEvents
          Select Case Tabella1.LINGUA
          
            Case "CH", "RU"
              stmp = "CHANGE VALUE"
              WRITE_DIALOG Tabella1.Parametri(PAR.PAR).NOME
              stmp = stmp & vbCrLf & vbCrLf & Tabella1.Parametri(PAR.PAR).Actual(PAR.col)
              stmp = stmp & " => "
              stmp = stmp & tNewValue
              
            Case Else
              stmp = Tabella1.Parametri(PAR.PAR).NOME
              If (Tabella1.Parametri(PAR.PAR).TIPO = "S") Or (Tabella1.Parametri(PAR.PAR).TIPO = "B") Then
                Dim Lista_Parole() As String
                Dim Lista_Valori() As String
                Lista_Parole = Split(Tabella1.Parametri(PAR.PAR).Limiti_Lett, ",")
                Lista_Valori = Split(Tabella1.Parametri(PAR.PAR).Limiti, ",")
                Npar = UBound(Lista_Parole)
                For i = 0 To Npar
                  If (Lista_Valori(i) = Tabella1.Parametri(PAR.PAR).Actual(PAR.col)) Then
                    stmp = stmp & vbCrLf & vbCrLf & Lista_Parole(i)
                  End If
                Next i
                stmp = stmp & " => "
                If (Npar > 0) Then
                  For i = 0 To Npar
                    If (Lista_Valori(i) = tNewValue) Then
                      stmp = stmp & Lista_Parole(i)
                    End If
                  Next i
                Else
                  stmp = stmp & tNewValue
                End If
              Else
                stmp = stmp & vbCrLf & vbCrLf & Tabella1.Parametri(PAR.PAR).Actual(PAR.col)
                stmp = stmp & " => "
                stmp = stmp & tNewValue
              End If
              
          End Select
          Dim iRESET As Integer
          iRESET = CONTROLLO_STATO_RESET
          If (iRESET = 1) Then
            iResponse = MsgBox(stmp, vbCritical + vbYesNo + vbDefaultButton2, "REPEAT CONFIRM!!!")
          Else
            stmp = "WARNING: AUTO CYCLE IS RUNNING!!!!" & Chr(13) & Chr(13) & stmp
            iResponse = MsgBox(stmp, vbCritical + vbYesNo + vbDefaultButton2, "REPEAT CONFIRM!!!")
          End If
          ResumeRegieEvents
        Else
          iResponse = vbYes
        End If
        If (iResponse = vbYes) Then
          Tabella1.Parametri(PAR.PAR).Actual(PAR.col) = tNewValue
          Tabella1.Parametri(PAR.PAR).Cambiato = True
          If Tabella1.Parametri(PAR.PAR).ONLine Then
            Dim Controllo As Long
            Dim DDE_BASE  As String
            Dim VAL_BASE1 As Integer
            Dim VAL_BASE2 As Integer
            Dim tstr      As String
            Dim N         As Integer
            Dim jj1       As String
            Dim jj2       As String
            'tstr = Tabella1.Parametri(PAR.PAR).Variabile(PAR.col)
            tstr = Tabella1.Parametri(PAR.PAR).VARIABILE(0)
            tstr = Trim$(UCase$(tstr))
            Controllo = InStr(tstr, "BASE")
            If (Controllo > 0) Then
              jj1 = InStr(tstr, "|")
              jj2 = InStr(tstr, ",")
              DDE_BASE = Mid$(tstr, jj1 + 1, jj2 - jj1 - 1)
              VAL_BASE1 = OPC_LEGGI_DATO(DDE_BASE)
              'I VALORI FISSI NEL DATABASE DEVONO ESSERE "0" ED "1"
              If (VAL_BASE1 = 0) Then
                VAL_BASE2 = 0
              Else
                VAL_BASE2 = val(Mid$(tstr, jj2 + 1))
              End If
              If (Tabella1.MODALITA = "SPF") Then
                '+2 perch� l'indirizzamento tramite OPC parte da 1
                N = Asc(UCase(Tabella1.LetteraLavorazione)) - Asc("A") + 2
                N = N + VAL_BASE2
                tstr = Tabella1.Parametri(PAR.PAR).ITEMDDE(PAR.col)
                'tstr = Replace(tstr, "*", N)
                jj1 = InStr(tstr, "[")
                jj2 = InStr(tstr, "]")
                tstr = Left$(tstr, jj1) & N & Right$(tstr, Len(tstr) - jj2 + 1)
                Call OPC_SCRIVI_DATO(tstr, Tabella1.Parametri(PAR.PAR).Actual(PAR.col))
              End If
            Else
              If (Tabella1.MODALITA = "SPF") Then
                Call OPC_SCRIVI_DATO(DDE_Idx(Tabella1.Parametri(PAR.PAR).ITEMDDE(PAR.col)), Tabella1.Parametri(PAR.PAR).Actual(PAR.col))
              End If
            End If
          End If
          'Aggiorno archivio mole
              If Tabella1.Parametri(PAR.PAR).CodMola Then
            Call LeggiArchivioMola(Trim(CStr(PAR.col)))
          ElseIf Tabella1.Parametri(PAR.PAR).Mola Then
            Call AggiornaArchivioMola(Trim(CStr(PAR.col)))
          End If
          'Aggiorno archivio rulli
              If Tabella1.Parametri(PAR.PAR).CodRullo Then
            Call LeggiArchivioRullo
          ElseIf Tabella1.Parametri(PAR.PAR).Rullo Then
            Call AggiornaArchivioRullo
          End If
          Call AggiornaDB
          If Tabella1.Parametri(PAR.PAR).ONLine Then
             Call RaiseEvent_PaginaChanged(Tabella1.NOME, DDE_Idx(Tabella1.Parametri(PAR.PAR).ITEMDDE(PAR.col)), Tabella1.Parametri(PAR.PAR).Actual(PAR.col), PaginaINS)
          Else
             Call RaiseEvent_PaginaChanged(Tabella1.NOME, Tabella1.Parametri(PAR.PAR).VARIABILE(PAR.col), Tabella1.Parametri(PAR.PAR).Actual(PAR.col), PaginaINS)
          End If
          Select Case Tabella1.MODALITA
            Case "SPF":       Call CreaSPF
            Case "OFF-LINE":  RaiseEvent OFFLINEChanged(ActualGroup, Tabella1.Parametri(PAR.PAR).VARIABILE(PAR.col), True)
          End Select
        End If
      End If
    Else
      If Verifica_Limiti Then
        StopRegieEvents
        MsgBox "POSSIBLE VALUES ARE: " & Tabella1.Parametri(PAR.PAR).Limiti, vbCritical, "INPUT [" & tNewValue2 & "] NOT POSSIBLE!"
        ResumeRegieEvents
      End If
    End If
  End If
  '
Exit Sub

SuGrid1_EditDisEnabling_Error:
  Status = 1

End Sub

Private Sub SuGridDisegni_EditDisEnabling(Key As String, VARIABILE As String, NewValue As String)
'
Dim PAR             As defPar
Dim Verifica_Limiti As Boolean
Dim tNewValue       As String
Dim tNewValue2      As String
'
On Error GoTo SuGridDisegni_EditDisEnabling_Error
  '
  Status = 100
  '
  PAR = GetOptByVar(VARIABILE)
  '
  Verifica_Limiti = True
  '
  tNewValue = UCase(Trim(ValMemOPT(NewValue, PAR.PAR)))
  tNewValue = Replace(tNewValue, ",", ".")
  '
  Status = 0
  '
  If (Trim(UCase(Tabella1.Opzioni(PAR.PAR).Actual(PAR.col))) <> tNewValue) Or (Tabella1.Opzioni(PAR.PAR).Evidenzia(PAR.col)) Then
    If Tabella1.Opzioni(PAR.PAR).VARIABILE(PAR.col) Like "*_CORR" Then
      If (PAR.col > 1) Then
        If Tabella1.Opzioni(PAR.PAR).VARIABILE(PAR.col - 1) & "_CORR" = VARIABILE Then
          tNewValue2 = val(Tabella1.Opzioni(PAR.PAR).Actual(PAR.col - 1)) + val(tNewValue)
        End If
      End If
    Else
      tNewValue2 = tNewValue
    End If
    '
    If VerLimiti(PAR, tNewValue2) Then
       If Verifica_Limiti Then
         Tabella1.Opzioni(PAR.PAR).Actual(PAR.col) = tNewValue
         Tabella1.Opzioni(PAR.PAR).Cambiato = True
      End If
    Else
      If Verifica_Limiti Then
         Status = 1
         Exit Sub
      End If
    End If
  End If
  '
  Call AggiornaDBOPT
  VisParGrafico = False
  '
  Call Riposiziona
  Call DisegnaOEM
  '
  SuGrid1(0).SetFocus
  '
  Status = 0
  '
  Exit Sub
  
SuGridDisegni_EditDisEnabling_Error:
  Status = 1

End Sub
'
'Mola=1 Indica che non voglio la mola 1 nell'elenco
'Mola=2 Indica che non voglio la mola 2 nell'elenco
Function ElencoMole(Mola As String) As String
'
Dim NomeVar As String
Dim DbM     As Database
Dim rsM     As Recordset
Dim tstr    As String
'
On Error GoTo ElencoMole_Err
  '
  'Nome della variabile contenente il codice mola
  'alla mola 1 sar� aggiunto [0,1] e alla 2 [0,2]
  NomeVar = "CODMOLA_G"
  Set DbM = OpenDatabase(Tabella1.DBase)
  Set rsM = DbM.OpenRecordset(Tabella1.NOME & "_MOLE", dbOpenDynaset)
  If (rsM.RecordCount > 0) Then
    rsM.MoveFirst
    'Non metto in elenco la mola nell'altra colonna
    If (Mola = 1) Then
      tstr = UCase(Lp_Str("CODMOLA_G[2]"))
    Else
      tstr = UCase(Lp_Str("CODMOLA_G[1]"))
    End If
    ElencoMole = ""
    While Not rsM.EOF
      If (UCase(rsM.Fields("NOME_PEZZO")) <> tstr) Then
        If (ElencoMole <> "") Then ElencoMole = ElencoMole & ","
        ElencoMole = ElencoMole & rsM.Fields("NOME_PEZZO")
      End If
      rsM.MoveNext
    Wend
  End If
  rsM.Close
  DbM.Close
  If (ElencoMole <> "") Then ElencoMole = "," & ElencoMole
  ElencoMole = "NEW,NONE" & ElencoMole
  '
Exit Function

ElencoMole_Err:
  On Error Resume Next
  ElencoMole = ""
  rsM.Close
  DbM.Close

End Function
'
Function ElencoRulli(Rullo As String) As String
'
Dim NomeVar As String
Dim DbM     As Database
Dim rsM     As Recordset
Dim tstr    As String
'
On Error GoTo ElencoRulli_Err
  '
  'Nome della variabile contenente il codice rullo
  NomeVar = "CODICERULLO_G"
  Set DbM = OpenDatabase(Tabella1.DBase)
  Set rsM = DbM.OpenRecordset(Tabella1.NOME & "_RULLI", dbOpenDynaset)
  If (rsM.RecordCount > 0) Then
    rsM.MoveFirst
    tstr = UCase(Lp_Str("CODICERULLO_G"))
    ElencoRulli = ""
    While Not rsM.EOF
    If (ElencoRulli <> "") Then ElencoRulli = ElencoRulli & ","
        ElencoRulli = ElencoRulli & rsM.Fields("NOME_PEZZO")
    rsM.MoveNext
    Wend
  End If
  rsM.Close
  DbM.Close
  If (ElencoRulli <> "") Then ElencoRulli = "," & ElencoRulli
  'ElencoRulli = "NEW,NONE" & ElencoRulli
  ElencoRulli = "NEW" & ElencoRulli
  '
Exit Function

ElencoRulli_Err:
  On Error Resume Next
  ElencoRulli = ""
  rsM.Close
  DbM.Close

End Function

Private Sub SuGrid1_DblClick(Index As Integer, Key As String)

  Call SuGrid1_EditEnabling(Index, Key, 13)
  
End Sub

Private Sub SuGrid1_EditEnabling(Index As Integer, tstr As String, CodiceASCII As Integer)
'
Dim cell    As Cella
Dim i       As Integer
Dim VScr    As Double
Dim j       As Integer
Dim tWidth  As Double
Dim PAR     As defPar
Dim iRESET  As Integer
Dim ret     As Long
Dim riga    As String
Dim stmp    As String
Dim Atmp    As String * 255
'
On Error GoTo SuGrid1_EditEnabling_Error
  '
  If (tstr = "") Then Exit Sub
  '
  Set cell = SuGrid1(Index).Celle(tstr)
  PAR = GetParByVar(cell.VARIABILE)
  'CONTROLLO DEL LIVELLO DI ACCESSO
  If (g_nAccessLevel > Tabella1.Parametri(PAR.PAR).LIVELLO) Then
    Set cell = Nothing
    WRITE_DIALOG "Operation allowed for access level <= " & Format$(Tabella1.Parametri(PAR.PAR).LIVELLO, "#0") & " ACTUAL -> " & Format$(g_nAccessLevel, "##0")
    Exit Sub
  End If
  'CONTROLLO MOLE
  If Tabella1.Parametri(PAR.PAR).CodMola Then
    Tabella1.Parametri(PAR.PAR).Limiti = ElencoMole(Trim(CStr(PAR.col)))
  End If
  'CONTROLLO RULLI
  If Tabella1.Parametri(PAR.PAR).CodRullo Then
    Tabella1.Parametri(PAR.PAR).Limiti = ElencoRulli(Trim(CStr(PAR.col)))
  End If
  'CONTROLLO SE MODIFICABILE
  If (Tabella1.Parametri(PAR.PAR).Modificabile = False) Then
    WRITE_DIALOG Tabella1.Parametri(PAR.PAR).NOME & ": VALUE CANNOT BE MODIFIED!!!!"
    Exit Sub
  End If
  'If ((Tabella1.Parametri(par.par).Modificabile) Or (cell.Variabile Like "*_CORR")) And (Tabella1.Parametri(par.par).Abilitato) Then
  If (Tabella1.Parametri(PAR.PAR).Abilitato) Then
    'MODIFICA CONSENTITA
    TIPO = "" & Tabella1.Parametri(PAR.PAR).TIPO
    Limiti = "" & Tabella1.Parametri(PAR.PAR).Limiti_Lett
    If (Limiti = "") Then Limiti = "" & Tabella1.Parametri(PAR.PAR).Limiti
  Else
    Set cell = Nothing
    WRITE_DIALOG "Value cannot be modified!!!"
    Exit Sub
  End If
  If (Tabella1.MODALITA = "SPF") Then
    'If (Tabella1.Parametri(PAR.PAR).Livello > 7) And (Tabella1.Parametri(PAR.PAR).ONLine = True) Then
    If (Tabella1.Parametri(PAR.PAR).ONLine = True) Then
      iRESET = 1
    Else
      iRESET = CONTROLLO_STATO_RESET
    End If
  Else
    iRESET = 1
  End If
  If (iRESET <> 1) Then
    WRITE_DIALOG "Data cannot be modified: CNC is not on RESET state"
    Exit Sub
  End If
  'SELEZIONE DEL TIPO DI EDITING
  Select Case UCase(Trim(TIPO))
    Case "N"
        TipoED = "TEXT"
    Case "S", "A"
          If (Limiti = "0,1") Then
        TipoED = "BOOL"
      ElseIf (Limiti = "") Then
        TipoED = "TEXT"
      Else
        TipoED = "LIST"
      End If
    Case "B"
      If (Limiti <> "") Then
        TipoED = "BOOL"
      Else
        TipoED = "TEXT"
      End If
    Case Else
        TipoED = "TEXT"
  End Select
  '
  'SCRITTURA DEL VALORE DI DEFAULT
  If (CodiceASCII = vbKeyDelete) And (TipoED = "TEXT") Then
    Set cell = Nothing
    Call EditDisEnableDIRETTO(Index, CalcoloDefaultParametro(PAR.PAR))
    Call Disegni.RidisegnaPagina(PaginaINS, False)
    Call DisegnaOEM
    Exit Sub
  End If
  '
  'POSIZIONAMENTO DEL CONTROLLO DI EDITING
  Select Case TipoED
    '
    Case "TEXT"
      OnlinePolling.Enabled = False
      OEMX.Polling.Enabled = False
      WRITE_DIALOG ""
      Set txtEdit(Index).Font = SuGrid1(Index).Font
      txtEdit(Index).Height = cell.Height
      txtEdit(Index).Top = cell.vTop + SuGrid1(Index).Top
      txtEdit(Index).Left = SuGrid1(Index).Left + cell.vLeft
      txtEdit(Index).Width = cell.Width
      txtEdit(Index).BackColor = cell.SelColor
      'GESTIONE ARCHIVIO ATTIVO SULLA VARIABILE CODICE_MOLA
      If (cell.VARIABILE = "CODICE_MOLA") Then
        Dim DB      As Database
        Dim TB      As Recordset
        Dim Gruppo  As String
        Dim sGruppo As String
        Dim NomeSK  As String
        'Gruppo = GetInfo("OEM1", "TB_CONFRONTA_MOLE_VITI_CONICHE", Path_LAVORAZIONE_INI)
        Gruppo = GetInfo("OEM1", "TB_CONFRONTA_MOLE_VITIV", Path_LAVORAZIONE_INI)
        Gruppo = UCase$(Trim$(Gruppo)) 'NOME DEL CAMPO
        Gruppo = GetInfo("OEM1", Gruppo, Path_LAVORAZIONE_INI)
        Gruppo = UCase$(Trim$(Gruppo)) 'NOME DELLA TABELLA
        If (Len(Gruppo) > 0) And (Len(PathDB_ADDIN) > 0) Then
          Call OEM1_CREAZIONE_ARCHIVIO(Gruppo)
          'sGruppo = GetInfo("OEM1", "IL_CONFRONTA_MOLE_VITI_CONICHE", Path_LAVORAZIONE_INI)
          sGruppo = GetInfo("OEM1", "IL_CONFRONTA_MOLE_VITIV", Path_LAVORAZIONE_INI)
          sGruppo = UCase$(Trim$(sGruppo)) 'NOME DEL CAMPO
          sGruppo = GetInfo("OEM1", sGruppo, Path_LAVORAZIONE_INI)
          sGruppo = UCase$(Trim$(sGruppo)) 'INTESTAZIONE LISTA
          If (Len(sGruppo) > 0) Then
            If IsNumeric(sGruppo) Then
              ret = val(sGruppo)
              ret = LoadString(g_hLanguageLibHandle, ret, Atmp, 255)
              If (ret > 0) Then sGruppo = Left$(Atmp, ret)
            End If
          Else
            sGruppo = Gruppo
          End If
          txtEdit(Index).Text = ""
          lstEdit(Index).Clear
          Set PicTITOLO.Font = SuGrid1(Index).Font
          PicTITOLO.Top = SuGrid1(Index).Top + cell.vTop + 1 * cell.Height
          PicTITOLO.Left = SuGrid1(Index).Left
          PicTITOLO.Width = SuGrid1(Index).Width
          PicTITOLO.Height = cell.Height * 1.6
          PicTITOLO.BackColor = QBColor(9)
          PicTITOLO.ForeColor = QBColor(15)
          PicTITOLO.Cls
          Set lstEdit(Index).Font = SuGrid1(Index).Font
          lstEdit(Index).Top = PicTITOLO.Top + PicTITOLO.Height
          lstEdit(Index).Left = PicTITOLO.Left
          lstEdit(Index).Width = PicTITOLO.Width
          lstEdit(Index).Height = cell.Height * 19.35
          lstEdit(Index).BackColor = QBColor(14)
          i = 0: NomeSK = ""
          Do
            stmp = GetInfo("OEM1", "GRUPPO(" & Format$(i, "0") & ")", Path_LAVORAZIONE_INI)
            stmp = UCase$(Trim$(stmp))
            If (stmp = Gruppo) Then
              NomeSK = GetInfo("OEM1", "NomeSK(" & Format$(i, "0") & ")", Path_LAVORAZIONE_INI)
              Exit Do
            Else
              i = i + 1
            End If
          Loop
          If (IsNumeric(NomeSK)) Then
            ret = val(NomeSK)
            ret = LoadString(g_hLanguageLibHandle, ret, Atmp, 255)
            If (ret > 0) Then NomeSK = Left$(Atmp, ret)
          End If
          'CONTROLLO SE INDIRIZZO UNA IMMAGINE \\
          ret = InStr(NomeSK, "\")
          If (ret = 1) Then
            NomeSK = Mid$(NomeSK, ret)
            PicTITOLO.Picture = LoadPicture(g_chOemPATH & "\Gapp4\images\" & NomeSK, , , 2000, 100)
          Else
            PicTITOLO.Picture = LoadPicture(g_chOemPATH & "\Gapp4\images\ARCHIVE1.BMP")
          End If
          PicTITOLO.CurrentX = PicTITOLO.Width / 2 - PicTITOLO.TextWidth(sGruppo) / 2
          PicTITOLO.CurrentY = PicTITOLO.Height / 2 - PicTITOLO.TextHeight(sGruppo) / 2
          PicTITOLO.Print sGruppo
          PicTITOLO.Visible = True
          lstEdit(Index).Visible = True
          Call lstEdit(Index).AddItem(" " & "NONE")
          Call lstEdit(Index).AddItem(" " & "NEW")
          Set DB = OpenDatabase(PathDB_ADDIN, True, False)
          Set TB = DB.OpenRecordset(Gruppo & "_ARCHIVIO", dbOpenDynaset)
          TB.MoveFirst
          Do Until TB.EOF
            If IsNull(TB.Fields("NOME_PEZZO").Value) Then
            TB.MoveNext
            Else
            Call lstEdit(Index).AddItem(" " & TB.Fields("NOME_PEZZO").Value)
            TB.MoveNext
            End If
          Loop
          TB.Close
          DB.Close
        End If
      Else
        'PRECEDENTE GESTIONE DELLA CASELLA DI MODIFICA
        If (CodiceASCII = 13) Then
          If cell.Correttore Then
            txtEdit(Index).Text = cell.ValueCorr
          Else
            txtEdit(Index).Text = cell.Value
          End If
          txtEdit(Index).Visible = True
          txtEdit(Index).SetFocus
          If TextWidth(txtEdit(Index).Text) > cell.Width Then txtEdit(Index).Width = TextWidth(txtEdit(Index).Text & "   ")
          txtEdit(Index).SelStart = 0
          txtEdit(Index).SelLength = Len(txtEdit(Index).Text & "   ")
        Else
          txtEdit(Index).Text = Chr$(CodiceASCII)
          txtEdit(Index).Visible = True
          txtEdit(Index).SetFocus
          txtEdit(Index).SelStart = 1
        End If
      End If
      '
    Case "LIST"
      OnlinePolling.Enabled = False
      OEMX.Polling.Enabled = False
      WRITE_DIALOG ""
      VScr = SuGrid1(Index).TextWidth("O ")
      Set lstEdit(Index).Font = SuGrid1(Index).Font
      Call ListFill(Index, Limiti, cell.Value, i)
      Call SetListItemHeight(lstEdit(Index), cell.Height / Screen.TwipsPerPixelY)
      Call AutoSizeLBHeight(lstEdit(Index))
      If (lstEdit(Index).ListCount <= 17) Then
        lstEdit(Index).Height = cell.Height * (lstEdit(Index).ListCount + 2)
      Else
        lstEdit(Index).Height = cell.Height * 17
      End If
      'CONTROLLO MOLE
      If Tabella1.Parametri(PAR.PAR).CodMola Then
        lstEdit(Index).Top = SuGrid1(Index).Top + cell.vTop + cell.Height
        lstEdit(Index).Left = SuGrid1(Index).Left
        lstEdit(Index).Width = SuGrid1(Index).Width
        lstEdit(Index).Height = SuGrid1(Index).Height - (cell.vTop + cell.Height)
      Else
        lstEdit(Index).Top = cell.vTop + SuGrid1(Index).Top
        lstEdit(Index).Left = SuGrid1(Index).Left + cell.vLeft
        lstEdit(Index).Width = cell.Width
      End If
      lstEdit(Index).BackColor = cell.SelColor
      lstEdit(Index).Visible = True
      lstEdit(Index).SetFocus
      lstEdit(Index).TopIndex = i
      tWidth = lstEdit(Index).Width
      For j = 1 To lstEdit(Index).ListCount
        If (SuGrid1(Index).TextWidth(lstEdit(Index).List(j)) + VScr > tWidth) Then
          tWidth = SuGrid1(Index).TextWidth(lstEdit(Index).List(j)) + VScr
        End If
      Next j
      lstEdit(Index).Width = tWidth
      lstEdit(Index).Left = cell.Left + cell.Width - lstEdit(Index).Width
      '
    Case "BOOL"
      OnlinePolling.Enabled = False
      OEMX.Polling.Enabled = False
      WRITE_DIALOG ""
      Call BoolChange(cell)
      Call EditDisEnable(Index)
      '
  End Select
  '
  Set cell = Nothing
  '
Exit Sub

SuGrid1_EditEnabling_Error:
  Status = 1

End Sub

Private Sub SuGrid1_GridAction(Index As Integer, Key As String, VARIABILE As String, Action As String)
'
On Error Resume Next
  '
  Select Case Action
    
    Case "PARAMETRI"
      If PictureDisegni.Visible Then
        VisParGrafico = Not VisParGrafico
        Call Riposiziona
        Call DisegnaOEM
        If VisParGrafico Then
          SuGridDisegni.SetFocus
          SuGridDisegni.SelFirst
        End If
      End If
      
    Case "INFO":      Call ShowInfoParametro(VARIABILE)
        
    Case "NOME":      Call CambiaNOME(VARIABILE)
    Case "LIMITI":    Call CambiaLIMITI(VARIABILE)
    Case "DEFAULT":   Call CambiaDEFAULT(VARIABILE)
    Case "TIPO":      Call CambiaTIPO(VARIABILE)
    
    Case "MSGUP":     If (scrMessaggi.Visible) And (scrMessaggi > scrMessaggi.MIN) Then scrMessaggi = scrMessaggi - 1
      
    Case "MSGDOWN":   If (scrMessaggi.Visible) And (scrMessaggi < scrMessaggi.Max) Then scrMessaggi = scrMessaggi + 1
      
    Case "CANCEL"
      If (VARIABILE Like "CODMOLA_G*") Then
        Select Case MsgBox("DELETE :  " & Lp_Str(VARIABILE) & "  ?", vbYesNo Or vbExclamation Or vbDefaultButton2, "Wheel delete...")
          Case vbYes
            Call CancellaDaArchivioMola(Lp_Str(VARIABILE))
            Call SP(VARIABILE, "NONE")
            If (Right(VARIABILE, 5) = "[0,1]") Then
              Call LeggiArchivioMola(1)
            Else
              Call LeggiArchivioMola(2)
            End If
            Call DisegnaOEM
          Case vbNo
            Call DisegnaOEM
            WRITE_DIALOG "Operation aborted by user!!"
        End Select
      End If
      '
      If (VARIABILE Like "CODICERULLO_G*") Then
        Select Case MsgBox("DELETE :  " & Lp_Str(VARIABILE) & "  ?", vbYesNo Or vbExclamation Or vbDefaultButton2, "Roller delete...")
          Case vbYes
            Call CancellaDaArchivioRulli(Lp_Str(VARIABILE))
            Call SP(VARIABILE, "DEFAULT")
            Call LeggiArchivioRullo
            Call DisegnaOEM
          Case vbNo
            Call DisegnaOEM
            WRITE_DIALOG "Operation aborted by user!!"
        End Select
      End If
      '
  End Select
  SuGrid1(Index).SetFocus
  '
End Sub

Private Sub SuGrid1_ListItemSelected(Index As Integer, Value As String, VARIABILE As String)

Dim PAR As defPar
'
On Error Resume Next
  '
  ListItemSelected = Value
  PAR = GetParByVar(VarCorr)
  '
  pctMessaggi.Visible = Helping
  If Helping Then Call PosMsg(Tabella1.Parametri(PAR.PAR).NOME, Tabella1.Parametri(PAR.PAR).Aiuto, Tabella1.Parametri(PAR.PAR).imgPath)
  '
End Sub

Private Sub SuGrid1_NewWidth(Index As Integer, Larghezze As String)
'
On Error Resume Next
  '
  Call SetLargh(PaginaINS, Index, Larghezze)

End Sub

Private Sub SuGrid1_Selezionata(Index As Integer, Key As String, VARIABILE As String)
'
Dim PAR               As defPar
Dim tArr()            As String
Static PaginaINS_LOC  As Integer
'
On Error Resume Next
  '
  WRITE_DIALOG ""
  VarCorr = VARIABILE
  SCon = Index
  If (Key <> "") Then
    tArr = Split(Key, ",")
    SRig = CInt(tArr(0))
    SCol = CInt(tArr(1))
  End If
  pctMessaggi.Visible = Helping
  If Helping Then
    PAR = GetParByVar(VarCorr)
    Call PosMsg(Tabella1.Parametri(PAR.PAR).NOME, Tabella1.Parametri(PAR.PAR).Aiuto, Tabella1.Parametri(PAR.PAR).imgPath)
  End If
  If (PaginaINS <> PaginaINS_LOC) Then
    PaginaINS_LOC = PaginaINS
    If Tabella1.Pagine(PaginaINS).Input Then Call Disegni.RidisegnaPagina(PaginaINS, False)
  Else
    ' 01.02.2016 Disegna ed evidenzia in rosso il diametro corrispondente al parametro selezionato
    Call Disegni.RidisegnaPagina(PaginaINS, False)
    Call DisegnaOEM
  End If
  SuGrid1(Index).SetFocus
  '
End Sub

Public Sub Cancella_Parametri()
'
Dim tCell      As String
Dim i          As Integer
Dim j          As Integer
Dim tpar       As Integer
Dim Cancellati As String
'
On Error Resume Next
  '
  Cancellati = ";"
  For i = 0 To SuGrid1(SCon).ListCount - 1
    j = 0
    tCell = i & "," & j
    While SuGrid1(SCon).CellaEsiste(tCell)
      If (Trim("" & SuGrid1(SCon).GetCella(tCell).VARIABILE) <> "") Then
        tpar = GetParByVar(SuGrid1(SCon).GetCella(tCell).VARIABILE).PAR
        If (tpar <> 0) Then
          If Not (Cancellati Like ";" & Trim("" & tpar) & ";") Then
            Call DefaultParametro(tpar)
            Cancellati = Cancellati & Trim("" & tpar) & ";"
          End If
          If Tabella1.Parametri(tpar).ONLine Then Call SCRITTURA_VALORI_ONLINE(tpar, "DEFAULT")
        End If
      End If
      j = j + 1
      tCell = i & "," & j
    Wend
  Next i
  Call AggiornaDB(True, True)
  Call DisegnaOEM
  If SuGrid1(SCon).Visible Then
    SuGrid1(SCon).SetFocus
    SuGrid1(SCon).SelFirst
  End If
  If (Tabella1.MODALITA = "SPF") Then Call CreaSPF
  '
End Sub

'****f* SuOEM/Evidenzia_Parametri
' DESCRIZIONE:
' Cambia il colore di fondo del GRUPPO selezionato, � utilizzato ad esempio per evidenziare
' i parametri prima della cancellazione.
' INGRESSI:
' o Stato -- "True" Evidenzia i parametri , "False" ripristina la visualizzazione normale.
' DEFINIZIONE:
Public Sub Evidenzia_Parametri(STATO As Boolean)
'
Dim NomePar    As String
Dim tCell      As String
Dim i          As Integer
Dim j          As Integer
Dim tpar       As Integer
Dim Cancellati As String
'
On Error Resume Next
  '
  If STATO Then
    NomePar = SuGrid1(SCon).GetCella("0,0").Value
    If (NomePar <> "") Then
      Cancellati = ";"
      For i = 1 To SuGrid1(SCon).ListCount - 1
        j = 0
        tCell = i & "," & j
        While SuGrid1(SCon).CellaEsiste(tCell)
          SuGrid1(SCon).GetCella(tCell).BackColor = SuGrid1(SCon).GetCella(tCell).RowSelColor
          j = j + 1
          tCell = i & "," & j
        Wend
      Next i
    End If
  Else
    If (SuGrid1(SCon).Visible) Then
      SuGrid1(SCon).SetFocus
      SuGrid1(SCon).SelFirst
    End If
  End If
  '
End Sub

Private Sub PosMsg(Titolo As String, TESTO As String, Optional PictureFile As String = "", Optional TIPO As String = "H")
'
Dim tPath     As String
Dim tstr()    As String
Dim i         As Integer
Dim tVal      As String
Dim Verticale As Boolean
Dim PAR       As defPar
Dim IntSpazio As Integer
Dim tstr2()   As String
Dim tstr3     As String
Dim tTesto    As String
Dim Pict      As String
Dim j         As Integer
Dim posh      As Double
Dim colh      As Double
Dim talt      As Double
Dim tLarg     As Double
Dim htot      As Double
Dim MaxL      As Double
Dim mypic     As IPictureDisp
Dim tInt      As Integer
'
On Error GoTo PosMsg_Error
  '
  If (Titolo <> MsgLastTitolo) Then scrMessaggi = 0
  '
  MsgLastTipo = TIPO
  MsgLastTesto = TESTO
  MsgLastPicture = PictureFile
  MsgLastTitolo = Titolo
  '
  tstr = Split(PictureFile, ",")
  tPath = ""
  If UBound(tstr) = 0 Then
    tPath = Tabella1.PathOEM & "g4.img\" & tstr(0)
  Else
    If (VarCorr = "") Then Exit Sub
    If (ListItemSelected <> "") Then
      tVal = ListItemSelected
    Else
      PAR = GetParByVar(VarCorr)
      tVal = Tabella1.Parametri(PAR.PAR).Actual(PAR.col)
    End If
    For i = 0 To UBound(tstr)
      If (Trim(tstr(i)) = tVal & ".BMP") Then
        tPath = Tabella1.PathOEM & "g4.img\" & Trim(tstr(i))
      End If
    Next i
  End If
  '
  If Not pctMessaggi.Visible Then Exit Sub
  '
  Set pctMessaggi.Font = CurFontNor
  '
  IntSpazio = 50
  Select Case SCon
    Case 0
      Verticale = True
      pctMessaggi.Top = IIf(lblModalita.Visible, lblModalita.Height, 0)
      pctMessaggi.Height = UserControl.Height - IIf(lblModalita.Visible, lblModalita.Height, 0)
      pctMessaggi.Left = (UserControl.Width - IntSpazio) / 2 + IntSpazio
      pctMessaggi.Width = UserControl.Width - (UserControl.Width - IntSpazio) / 2 - IntSpazio
    Case 1, 2
      pctMessaggi.Top = IIf(lblModalita.Visible, lblModalita.Height, 0)
      pctMessaggi.Left = 0
      pctMessaggi.Height = UserControl.Height - IIf(lblModalita.Visible, lblModalita.Height, 0)
      pctMessaggi.Width = (UserControl.Width - IntSpazio) / 2
      Verticale = True
  End Select
  posh = pctMessaggi.TextWidth("W")
  colh = pctMessaggi.TextHeight("W")
  pctMessaggi.Cls
  pctMessaggi.CurrentY = posh - scrMessaggi * pctMessaggi.TextHeight("W")
  Select Case TIPO
    Case "H":  pctMessaggi.BackColor = vbCyan
  End Select
  'Larghezza massima testo sulla riga
  MaxL = IIf(Verticale, scrMessaggi.Left - (2 * posh), scrMessaggi.Left - pctMessaggi.Height - (2 * posh))
  tTesto = TESTO
  If (tTesto <> "") Then
    If (Titolo <> "") Then
      Set pctMessaggi.Font = CurFontTit
      pctMessaggi.CurrentX = posh
      pctMessaggi.Print Titolo & vbCrLf
      Set pctMessaggi.Font = CurFontNor
    End If
    tstr = Split(tTesto, vbCrLf)
    For i = 0 To UBound(tstr)
      tstr2() = Split(tstr(i), " ")
      For j = 0 To UBound(tstr2)
        tstr3 = tstr3 & tstr2(j) & " "
        If (j + 1) <= UBound(tstr2) Then
          If (pctMessaggi.TextWidth(tstr3 & tstr2(j + 1)) > MaxL) Then
            tstr3 = RTrim(tstr3) '& vbCrLf
            pctMessaggi.CurrentX = posh
            pctMessaggi.Print tstr3
            tstr3 = ""
          End If
        Else
          tstr3 = RTrim(tstr3)
          pctMessaggi.CurrentX = posh
          pctMessaggi.Print tstr3
          tstr3 = ""
        End If
      Next j
    Next i
  End If
  '
  pctMessaggi.CurrentY = IIf(Verticale, pctMessaggi.CurrentY, 0) + (pctMessaggi.TextHeight("W") / 2)
  htot = pctMessaggi.CurrentY
  pctMessaggi.CurrentX = IIf(Verticale, posh, MaxL + (2 * posh))
  '
  Pict = Trim(tPath): i = InStr(Pict, "logo2.bmp") 'NON FACCIO APPARIRE PIU' IL LOGO SU SE ESISTE NEL DATABASE
  If (Pict <> "") And (Dir(Pict) <> "") And (Right(Pict, 1) <> "\") And (i <= 0) Then
    Set mypic = LoadPicture(Pict)
    tLarg = mypic.Width * 0.55
    talt = mypic.Height * 0.55
    Call pctMessaggi.PaintPicture(mypic, 0, pctMessaggi.CurrentY, tLarg, talt)
    htot = htot + talt
  End If
  '
  If (scrMessaggi = 0) Then
    tInt = CInt((htot - pctMessaggi.ScaleHeight) / pctMessaggi.TextHeight("W"))
    If (tInt >= 0) Then
      scrMessaggi.Visible = True
      scrMessaggi.Max = tInt
      WRITE_DIALOG "Use Ctrl + UpArrow or DownArrow to scroll messages"
    Else
      scrMessaggi.Visible = False
      scrMessaggi = 0
      scrMessaggi.Max = 0
      WRITE_DIALOG " "
    End If
  End If
  '
Exit Sub

PosMsg_Error:
  WRITE_DIALOG "Error " & Err.Number & " (" & Err.Description & ") in Sub: PosMsg"

End Sub

'****m* SUOEM/Working
' DESCRIZIONE:
' Mostra (State=True) o nasconde (State=False) un riquadro bianco con scritta "Wait..." che
' serve a nascondere il controllo durante le operazioni di ridimensionamento o aggiornamento
' per evitare di vedere pagine incomplete.
' DEFINIZIONE:
Public Sub Working(State As Boolean)
'
Dim tstr As String
'
On Error GoTo errWorking
  '
  If State Then
    pctWorking.Visible = True
    pctWorking.Cls
    DoEvents
    pctWorking.Move 0, 0, UserControl.ScaleWidth, UserControl.ScaleHeight
    tstr = "Wait..."
    pctWorking.CurrentY = pctWorking.ScaleHeight / 2
    pctWorking.CurrentX = (pctWorking.ScaleWidth - pctWorking.TextWidth(tstr)) / 2
    pctWorking.Print tstr
  Else
    pctWorking.Visible = False
  End If
  '
Exit Sub

errWorking:
  WRITE_DIALOG "Working " & Err.Number
  
End Sub

Public Function GetPaginaDaSK(SKPos As Integer) As Integer
'
Dim i As Integer
'
On Error Resume Next
  '
  GetPaginaDaSK = -1
  If MaxPagINS < 1 Then Exit Function
  '
  For i = 1 To MaxPagINS
    If (Len(Tabella1.Pagine(i).SKPos) > 0) Then
      If (val(Tabella1.Pagine(i).SKPos) = SKPos) Then
        GetPaginaDaSK = i 'Tabella1.Pagine(i).Numero
        Exit For
      End If
    End If
  Next i
  '
End Function

'****m* SUOEM/GoToPaginaNew
' DESCRIZIONE:
' Visualizza la pagina "Numero" in base alla configurazione effettuata tramite
' la tabella LAVORAZIONE_CONFIG nel campo "PAGINA"
' Non chiama DisegnaOEM come GoToPagina()
' DEFINIZIONE:
Public Sub GoToPaginaNew(Numero As Integer)
'***
'
On Error Resume Next
  '
  LastPagina = 0
  If (Numero <= MaxPagINS) Then
    PaginaINS = Numero
  Else
    Exit Sub
  End If
  VisParGrafico = False
  Call SELEZIONA_SuGrid1

End Sub

'****m* SUOEM/GoToPagina
' DESCRIZIONE:
' Visualizza la pagina "Numero" in base alla configurazione effettuata tramite
' la tabella LAVORAZIONE_CONFIG nel campo "PAGINA"
'
' DEFINIZIONE:
Public Sub GoToPagina(Numero As Integer)
'***
'
On Error Resume Next
  '
  LastPagina = 0
  If (Numero <= MaxPagINS) Then
    PaginaINS = Numero
  Else
    Exit Sub
  End If
  VisParGrafico = False
  Call Riposiziona
  Call DisegnaOEM
  Call SELEZIONA_SuGrid1
  'RaiseEvent PaginaReached(Tabella1.NOME, Tabella1.Pagine(i).NOME1, Tabella1.MODALITA)
  '
End Sub

'****f* SuOEM/GoToPaginaSK
' DESCRIZIONE:
' Visualizza la pagina associata alla SoftKey "Numero" in base alla configurazione effettuata tramite
' la tabella LAVORAZIONE_CONFIG nel campo "SKPos"
' DEFINIZIONE:
Public Sub GoToPaginaSK(Numero As Integer)
'***
Dim i As Integer
'
On Error Resume Next
  '
  LastPagina = 0
  i = GetPaginaDaSK(Numero)
  If (i < 0) Then Exit Sub
  If (i <= MaxPagINS) Then
    PaginaINS = i
  Else
    Exit Sub
  End If
  VisParGrafico = False
  Call Riposiziona
  Call Disegni.RidisegnaPagina(PaginaINS, False)
  Call DisegnaOEM
  Call SELEZIONA_SuGrid1
  'RaiseEvent PaginaReached(Tabella1.NOME, Tabella1.Pagine(i).Nomegruppo, Tabella1.MODALITA)
  '
End Sub

'****f* SuOEM/PaginaSeguente
' DESCRIZIONE:
' Mostra la pagina seguente rispetto alla numerazione nella tabella di configurazione
' non in base al numero della SoftKey associata.
' Dopo l'ultima pagina riprende dalla prima.
' DEFINIZIONE:
Public Sub PaginaSeguente()
'
Dim i As Integer
'
On Error Resume Next
  '
  If (MaxPagINS > 0) Then
    LastPagina = PaginaINS
    PaginaINS = PaginaINS + 1
    If (PaginaINS > MaxPagINS) Then
      PaginaINS = 1
      For i = 1 To MaxPagINS
        If Len(Tabella1.Pagine(i).SKPos) > 0 Then PaginaINS = i: Exit For
      Next i
    End If
    If Tabella1.Pagine(PaginaINS).Input Then
      'SCol = 2
      'SRig = 2
      SCon = 0
      Call RaiseEvent_PaginaChanged(Tabella1.NOME, "", "", PaginaINS)
    End If
    VisParGrafico = False
    Call Riposiziona
    Call DisegnaOEM
    Call SELEZIONA_SuGrid1
  Else
    WRITE_DIALOG "Operation cannot be performed!!!"
  End If
  '
End Sub

Public Sub SELEZIONA_SuGrid1()
'
On Error Resume Next
  '
    If SuGrid1(0).Visible Then
      SuGrid1(0).SelFirst
  ElseIf SuGrid1(1).Visible Then
      SuGrid1(1).SelFirst
  ElseIf SuGrid1(2).Visible Then
      SuGrid1(2).SelFirst
  End If
  '
End Sub

Private Sub SuGrid1_VarChanged(Index As Integer, Variabili As String)
'
Dim i       As Integer
Dim PAR     As defPar
Dim tArr()  As String
'
On Error Resume Next
  '
  If (Variabili <> "SALTA") Then
    tArr = Split(Variabili, ";")
    For i = 0 To UBound(tArr)
      PAR = GetParByVar(tArr(i))
      Tabella1.Parametri(PAR.PAR).Actual(PAR.col) = SuGrid1(Index).GetCellaByVar(tArr(i)).Value
      Tabella1.Parametri(PAR.PAR).Cambiato = True
    Next i
    Call AggiornaDB
    If (Tabella1.MODALITA = "SPF") Then Call CreaSPF
  End If
  Call DisegnaOEM
  Call SuGrid1(Index).SetFocus
  '
End Sub

Private Sub SuGridDisegni_GridAction(Key As String, VARIABILE As String, Action As String)
'
On Error Resume Next
  '
  Select Case Action
    '
    Case "PARAMETRI"
      SuGridDisegni.Visible = False
      VisParGrafico = False
      If SuGrid1(0).Visible Then Call SuGrid1(0).SetFocus
  
  End Select

End Sub

Private Sub UserControl_Hide()
'
On Error Resume Next
  '
  'NECESSARIO SE ESCO CON IL TASTO RECALL
  If OnlinePolling.Enabled Then OnlinePolling.Enabled = False

End Sub

Private Sub UserControl_Initialize()
'
'On Error Resume Next
  '
  'Call Working(True)
  '
End Sub

Private Sub UserControl_InitProperties()
'
On Error Resume Next
  '
  ColoreSfondo = vbWhite
  ColoreTesto = vbBlack
  ColoreRigaSelezionata = vbCyan
  ColoreCellaSelezionata = vbYellow
  ColoreTitolo = vbGreen
  ColoreGriglia = RGB(210, 210, 210)
  ColoreHelp = vbCyan
  ColoreLimiti = vbRed
  '
  Set FontNormale = lblDefita.Font
  Set FontNormaleTitolo = lblDefItaT.Font
  '
  Set FontCinese = lblDefCH.Font
  Set FontCineseTitolo = lblDefCHT.Font
  '
  Set FontRusso = lblDefRU.Font
  Set FontRussoTitolo = lblDefRUT.Font
  '
End Sub

'---------------------------------------------------------------------------------------
' Procedura : UserControl_ReadProperties
' Data e ora: 25/07/2007 08.39
' Autore    : Fil
' Note      :
'---------------------------------------------------------------------------------------
Private Sub UserControl_ReadProperties(PropBag As PropertyBag)
'
On Error GoTo UserControl_ReadProperties_Error
  '
  ColoreSfondo = PropBag.ReadProperty("ColoreSfondo", vbWhite)
  ColoreTesto = PropBag.ReadProperty("ColoreTesto", vbBlack)
  ColoreRigaSelezionata = PropBag.ReadProperty("ColoreRigaSelezionata", vbCyan)
  ColoreCellaSelezionata = PropBag.ReadProperty("ColoreCellaSelezionata", vbYellow)
  ColoreTitolo = PropBag.ReadProperty("ColoreTitolo", vbGreen)
  ColoreGriglia = PropBag.ReadProperty("ColoreGriglia", RGB(210, 210, 210))
  ColoreHelp = PropBag.ReadProperty("ColoreHelp", vbCyan)
  ColoreLimiti = PropBag.ReadProperty("ColoreLimiti", vbRed)
  Set FontNormale = PropBag.ReadProperty("FontNormale", lblDefita.Font)
  Set FontNormaleTitolo = PropBag.ReadProperty("FontNormaleTitolo", lblDefItaT.Font)
  Set FontCinese = PropBag.ReadProperty("FontCinese", lblDefCH.Font)
  Set FontCineseTitolo = PropBag.ReadProperty("FontCineseTitolo", lblDefCHT.Font)
  Set FontRusso = PropBag.ReadProperty("FontRusso", lblDefRU.Font)
  Set FontRussoTitolo = PropBag.ReadProperty("FontRussoTitolo", lblDefRUT.Font)
  Set PictureDisegni = pctDisegni
  '
Exit Sub

UserControl_ReadProperties_Error:
  MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Sub: UserControl_ReadProperties"

End Sub

Private Sub UserControl_WriteProperties(PropBag As PropertyBag)
'
On Error GoTo UserControl_WriteProperties_Error
  '
  PropBag.WriteProperty "ColoreSfondo", ColoreSfondo, vbWhite
  PropBag.WriteProperty "ColoreTesto", ColoreTesto, vbBlack
  PropBag.WriteProperty "ColoreRigaSelezionata", ColoreRigaSelezionata, vbCyan
  PropBag.WriteProperty "ColoreCellaSelezionata", ColoreCellaSelezionata, vbYellow
  PropBag.WriteProperty "ColoreTitolo", ColoreTitolo, vbGreen
  PropBag.WriteProperty "ColoreGriglia", ColoreGriglia, RGB(210, 210, 210)
  PropBag.WriteProperty "ColoreHelp", ColoreHelp, vbCyan
  PropBag.WriteProperty "ColoreLimiti", ColoreLimiti, vbRed
  PropBag.WriteProperty "FontNormale", FontNormale, lblDefita.Font
  PropBag.WriteProperty "FontNormaleTitolo", FontNormaleTitolo, lblDefItaT.Font
  PropBag.WriteProperty "FontCinese", FontCinese, lblDefCH.Font
  PropBag.WriteProperty "FontCineseTitolo", FontCineseTitolo, lblDefCHT.Font
  PropBag.WriteProperty "FontRusso", FontRusso, lblDefRU.Font
  PropBag.WriteProperty "FontRussoTitolo", FontRussoTitolo, lblDefRUT.Font
  '
Exit Sub

UserControl_WriteProperties_Error:
  MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Sub: UserControl_WriteProperties"

End Sub

Private Sub ShowInfoParametro(Optional VARIABILE As String = "")
'
Dim PAR   As defPar
Dim tstr  As String
'
On Error Resume Next
  '
  If VARIABILE = "" Then
    'par = MatrPar(SRig + VScorr(SCon), SCol, SCon)
  Else
    PAR = GetParByVar(VARIABILE)
  End If
  tstr = ""
  If Tabella1.Parametri(PAR.PAR).ONLine Then
    tstr = tstr & "NCK Var." & vbTab & ": " & DDE_Idx(Tabella1.Parametri(PAR.PAR).ITEMDDE(PAR.col))
  Else
    tstr = tstr & "Variable" & vbTab & ": " & Tabella1.Parametri(PAR.PAR).VARIABILE(PAR.col)
  End If
  tstr = tstr & vbCrLf & "Value" & vbTab & ": " & Tabella1.Parametri(PAR.PAR).Actual(PAR.col)
  tstr = tstr & vbCrLf & "Range" & vbTab & ": " & Tabella1.Parametri(PAR.PAR).Limiti
  tstr = tstr & vbCrLf & "Index" & vbTab & ": " & Tabella1.Parametri(PAR.PAR).INDICE
  tstr = tstr & vbCrLf & "Type" & vbTab & ": " & Tabella1.Parametri(PAR.PAR).TIPO
  StopRegieEvents
  Call MsgBox(tstr, vbInformation, Tabella1.NOME)
  ResumeRegieEvents
  '
End Sub

Private Sub CambiaLIMITI(Optional VARIABILE As String = "")

Dim PAR   As defPar
Dim tstr  As String
'
On Error Resume Next
  '
  tstr = InputBox("Password?")
  If (UCase(tstr) = "SU") Then
    If (VARIABILE = "") Then
      'par = MatrPar(SRig + VScorr(SCon), SCol, SCon)
    Else
      PAR = GetParByVar(VARIABILE)
    End If
    tstr = InputBox("LIMITI:", , Tabella1.Parametri(PAR.PAR).Limiti)
    If (tstr = "") Then Exit Sub
    Tabella1.Parametri(PAR.PAR).Limiti = tstr
    Tabella1.Parametri(PAR.PAR).Cambiato = True
    Call AggiornaDB
  End If
  '
End Sub

Private Sub CambiaNOME(Optional VARIABILE As String = "")

Dim PAR   As defPar
Dim tstr  As String
Dim NOME  As String
Dim i     As Integer
Dim j     As Integer
Dim DBase As Database
Dim RS    As Recordset
'
On Error Resume Next
  '
  tstr = InputBox("Password?")
  If (UCase(tstr) = "SU") Then
    If (VARIABILE = "") Then
      'par = MatrPar(SRig + VScorr(SCon), SCol, SCon)
    Else
      PAR = GetParByVar(VARIABILE)
    End If
    j = Tabella1.Parametri(PAR.PAR).INDICE
    Set DBase = OpenDatabase(Tabella1.DBase)
    Set RS = DBase.OpenRecordset(Tabella1.NOME, dbOpenDynaset)
      RS.FindFirst ("INDICE=" & Format$(j, "0#####"))
      NOME = RS.Fields("NOME_" & LINGUA)
    RS.Close
    DBase.Close
    tstr = InputBox("NAME:", Format$(j, "0#####"), NOME)
    If (tstr = "") Then Exit Sub
    Set DBase = OpenDatabase(Tabella1.DBase)
    Set RS = DBase.OpenRecordset(Tabella1.NOME, dbOpenDynaset)
      RS.FindFirst ("INDICE=" & Format$(j, "0#####"))
      RS.Edit
      RS.Fields("NOME_" & LINGUA) = tstr
      RS.Update
    RS.Close
    DBase.Close
  End If
  '
End Sub

Private Sub CambiaDEFAULT(Optional VARIABILE As String = "")

Dim PAR   As defPar
Dim tstr  As String
Dim Campo As String
Dim i     As Integer
Dim j     As Integer
Dim DBase As Database
Dim RS    As Recordset
'
On Error Resume Next
  '
  tstr = InputBox("Password?")
  If (UCase(tstr) = "SU") Then
    If (VARIABILE = "") Then
      'par = MatrPar(SRig + VScorr(SCon), SCol, SCon)
    Else
      PAR = GetParByVar(VARIABILE)
    End If
    j = Tabella1.Parametri(PAR.PAR).INDICE
    Set DBase = OpenDatabase(Tabella1.DBase)
    Set RS = DBase.OpenRecordset(Tabella1.NOME, dbOpenDynaset)
      RS.FindFirst ("INDICE=" & Format$(j, "0#####"))
      Campo = RS.Fields("DEFAULT")
    RS.Close
    DBase.Close
    tstr = InputBox("DEFAULT:", Format$(j, "0#####"), Campo)
    If (tstr = "") Then Exit Sub
    Set DBase = OpenDatabase(Tabella1.DBase)
    Set RS = DBase.OpenRecordset(Tabella1.NOME, dbOpenDynaset)
      RS.FindFirst ("INDICE=" & Format$(j, "0#####"))
      RS.Edit
      RS.Fields("DEFAULT") = tstr
      RS.Update
    RS.Close
    DBase.Close
  End If
  '
End Sub

Private Sub CambiaTIPO(Optional VARIABILE As String = "")

Dim PAR   As defPar
Dim tstr  As String
Dim TIPO  As String
Dim i     As Integer
Dim j     As Integer
Dim DBase As Database
Dim RS    As Recordset
'
On Error Resume Next
  '
  tstr = InputBox("Password?")
  If (UCase(tstr) = "SU") Then
    If (VARIABILE = "") Then
      'par = MatrPar(SRig + VScorr(SCon), SCol, SCon)
    Else
      PAR = GetParByVar(VARIABILE)
    End If
    j = Tabella1.Parametri(PAR.PAR).INDICE
    Set DBase = OpenDatabase(Tabella1.DBase)
    Set RS = DBase.OpenRecordset(Tabella1.NOME, dbOpenDynaset)
      RS.FindFirst ("INDICE=" & Format$(j, "0#####"))
      TIPO = RS.Fields("TIPO")
    RS.Close
    DBase.Close
    tstr = InputBox("type:", Format$(j, "0#####"), TIPO)
    If (tstr = "") Then Exit Sub
    Set DBase = OpenDatabase(Tabella1.DBase)
    Set RS = DBase.OpenRecordset(Tabella1.NOME, dbOpenDynaset)
      RS.FindFirst ("INDICE=" & Format$(j, "0#####"))
      RS.Edit
      RS.Fields("TIPO") = tstr
      RS.Update
    RS.Close
    DBase.Close
  End If
  '
End Sub

Private Function VerLimiti(Parametro As defPar, Valore As String) As Boolean
'
Dim tLim    As String
Dim tstr()  As String
Dim tVal    As Double
Dim tL1     As Double
Dim tL2     As Double
Dim tTipo   As String
'
On Error Resume Next
  '
  VerLimiti = True
  tLim = Tabella1.Parametri(Parametro.PAR).Limiti
  tTipo = Tabella1.Parametri(Parametro.PAR).TIPO
  If (tLim = "") Then
    Exit Function
  Else
    tstr = Split(tLim, ",")
    If UBound(tstr) = 1 Then
      'If ((tTipo = "A") Or (tTipo = "N")) And (Not IsNumeric(Valore)) Then
      If (tTipo = "N") And (Not IsNumeric(Valore)) Then
        VerLimiti = False
        Exit Function
      End If
      tVal = val(Valore)
      tL1 = val(tstr(0))
      tL2 = val(tstr(1))
      If (tVal < tL1) Or (tVal > tL2) Then
        VerLimiti = False
      End If
    Else
      'Verifica lista
    End If
  End If
  '
End Function

Public Sub CaricaPezzoINI(NomePezzo As String)
'
Dim i       As Integer
Dim j       As Integer
Dim DBase   As Database
Dim RS      As Recordset
Dim tstr    As String
Dim DATI()  As String
Dim Dato()  As String
Dim NFil    As Integer
Dim TROVATO As Boolean
Dim Finito  As Boolean
Dim tsez    As String
'
On Error Resume Next
  '
  If (Tabella1.Caricata = False) Then Exit Sub
  Tabella1.NomePezzo = NomePezzo
      If (Tabella1.MODALITA = "SPF") Then
        tsez = "[CNC_" & Tabella1.LetteraLavorazione & "]"
  ElseIf (Tabella1.MODALITA = "OFF-LINE") Then
        tsez = "[OFF-LINE]"
  End If
  '
  If (Dir$(Tabella1.PathOEM & Tabella1.NOME & ".INI") <> "") Then
    NFil = FreeFile
    TROVATO = False
    Open Tabella1.PathOEM & Tabella1.NOME & ".INI" For Input As #NFil
    tstr = ""
    While (Not TROVATO) And (Not EOF(NFil))
      Line Input #NFil, tstr
      If UCase(tstr) = tsez Then TROVATO = True
    Wend
    Finito = False
    While (Not Finito) And (Not EOF(NFil))
      Line Input #NFil, tstr
      If Left(tstr, 1) & Right(tstr, 1) = "[]" Then Finito = True
      If (tstr <> "") And (Not Finito) Then
        tstr = Split(tstr, ";")(0)
        Dato = Split(tstr, "=")
        If (Dato(0) <> "") Then
          ReDim Preserve Dato(1)
          Call SP(Dato(0), Dato(1), False)
        End If
      End If
    Wend
    Close NFil
    Call AggiornaDB(False)
    Call LeggiArchivioMola("1")
    Call LeggiArchivioMola("2")
    Call LeggiArchivioRullo
  End If
  '
End Sub

Public Sub CaricaPezzo(NomePezzo As String, TabellaArchivio As String, Optional Temp As Boolean = False)
'
Dim i       As Integer
Dim j       As Integer
Dim DBase   As Database
Dim RS      As Recordset
Dim tstr    As String
Dim DATI()  As String
Dim Dato()  As String
'
On Error Resume Next
  '
  If (Tabella1.Caricata = False) Or ((NomePezzo = "") And (Not Temp)) Or (TabellaArchivio = "") Then
    Exit Sub
  End If
  '
  Set DBase = OpenDatabase(Tabella1.DBase)
  Set RS = DBase.OpenRecordset(TabellaArchivio, dbOpenDynaset)
  Tabella1.NomePezzo = NomePezzo
  RS.FindFirst ("NOME_PEZZO='" & UCase(Trim(Tabella1.NomePezzo)) & "'")
  Call CancellaSezioneIni("OFF-LINE", Tabella1.PathOEM & Tabella1.NOME & ".ini")
  If Not RS.NoMatch Then
    tstr = "" & RS.Fields("DATI")
  Else
    tstr = ""
    For i = 1 To UBound(Tabella1.Parametri)
      If (Tabella1.Parametri(i).Abilitato) And (Not Tabella1.Parametri(i).Titolo) Then
        Call DefaultParametro(i)
      End If
    Next i
    Call DisegnaOEM
    Call AggiornaDB
    RS.Close
    DBase.Close
    'RaiseEvent NuovoPezzo
    Exit Sub
  End If
  RS.Close
  DBase.Close
  '
  DATI = Split(tstr, vbCrLf)
  For i = 0 To UBound(DATI)
    If (DATI(i) <> "") Then
      Dato = Split(DATI(i), "=")
      Dato(1) = Replace(Dato(1), ";", "")
      Call SP(Dato(0), Dato(1), False)
    End If
  Next i
  Call AggiornaDB(True, True)
  Call LeggiArchivioMola("1")
  Call LeggiArchivioMola("2")
  Call LeggiArchivioRullo
  '
End Sub

Public Sub CaricaCNC(NomePezzo As String, CNC As String, TabellaArchivio As String)
'
Dim i       As Integer
Dim j       As Integer
Dim DBase   As Database
Dim RS      As Recordset
Dim rs2     As Recordset
Dim tstr    As String
Dim tdati   As String
Dim fld     As Field
Dim DATI()  As String
Dim Dato()  As String
'
On Error Resume Next
  '
  If (Tabella1.Caricata = False) Or (NomePezzo = "") Or (TabellaArchivio = "") Then Exit Sub
  '
  Set DBase = OpenDatabase(Tabella1.DBase)
  Set rs2 = DBase.OpenRecordset(TabellaArchivio, dbOpenDynaset)
    If (rs2.RecordCount = 0) Then
      rs2.Close
      DBase.Close
      Exit Sub
    End If
    rs2.FindFirst ("NOME_PEZZO='" & UCase(Trim(NomePezzo)) & "'")
    If (rs2.NoMatch) Then
      rs2.Close
      DBase.Close
      StopRegieEvents
      Call MsgBox("NOT FOUND!  ", vbOKOnly + vbCritical + vbApplicationModal + vbDefaultButton1, UCase(Trim(NomePezzo)))
      ResumeRegieEvents
      Exit Sub
    End If
    tdati = rs2.Fields("DATI")
  rs2.Close
  DBase.Close
  '
  Tabella1.LetteraLavorazione = CNC
  Call CancellaSezioneIni("CNC_" & Tabella1.LetteraLavorazione, Tabella1.PathOEM & Tabella1.NOME & ".ini")
  '
  DATI = Split(tdati, vbCrLf)
  For i = 0 To UBound(DATI)
    If (DATI(i) <> "") Then
      Dato = Split(DATI(i), "=")
      Call SP(Dato(0), Dato(1), False)
    End If
  Next i
  Call AggiornaDB(True, True)
  '
End Sub

Public Sub CancellaSezioneIni(NomeSEZIONE As String, NomeIniFile As String)
'
On Error Resume Next
  '
  Call WritePrivateProfileString(NomeSEZIONE, vbNullString, vbNullString, NomeIniFile)

End Sub

Private Function GetTuttiParSequenza(Seq As String) As String

Dim tchr As String
Dim tSeq As String
Dim tnum As String
'
On Error Resume Next
  '
  GetTuttiParSequenza = ""
  tSeq = Seq
  tchr = Left(tSeq, 1)
  tSeq = Right(tSeq, Len(tSeq) - 1)
  While (tSeq <> "")
    If (tchr = "D") Or (tchr = "A") Then
      tchr = Left(tSeq, 1)
      tSeq = Right(tSeq, Len(tSeq) - 1)
      tnum = ""
      While IsNumeric(tchr)
        tnum = tnum & tchr
        If (tSeq <> "") Then
          tchr = Left(tSeq, 1)
          tSeq = Right(tSeq, Len(tSeq) - 1)
        Else
          tchr = ""
        End If
      Wend
      'Controllo che non sia gi� stato inserito
      If Not ((";" & GetTuttiParSequenza & ";") Like ("*;" & tnum & ";*")) Then
        If GetTuttiParSequenza <> "" Then GetTuttiParSequenza = GetTuttiParSequenza & ";"
        GetTuttiParSequenza = GetTuttiParSequenza & tnum
      End If
    Else
      If (tSeq <> "") Then
        tchr = Left(tSeq, 1)
        tSeq = Right(tSeq, Len(tSeq) - 1)
      Else
        tchr = ""
      End If
    End If
  Wend

End Function

'****m* SUOEM/CreaSWAP
' DESCRIZIONE:
' Salva i valori attuali dei parametri della pagina corrente in un file INI (SWAPPO.ini)
' DEFINIZIONE:
Public Sub CreaSWAP()
'***
Dim tstr    As String
Dim Tvar    As String
Dim TPars() As String
Dim i       As Integer
Dim j       As Integer
Dim k       As Integer
'
On Error Resume Next
  '
  tstr = GetTuttiParSequenza(Tabella1.Pagine(PaginaINS).sequenza)
  Tvar = ""
  TPars = Split(tstr, ";")
  For i = 0 To UBound(TPars)
    j = GetParByINDICE(CInt(TPars(i)))
    If Tabella1.Parametri(j).Abilitato Then
      For k = 1 To Tabella1.Parametri(j).NActual
        If (Tabella1.Parametri(j).VARIABILE(k) <> "") Then
          If (Tvar <> "") Then Tvar = Tvar & ";"
          Tvar = Tvar & Tabella1.Parametri(j).VARIABILE(k)
        End If
      Next k
    End If
  Next i
  TPars = Split(Tvar, ";")
  '
  Dim NomeSEZIONE As String
  NomeSEZIONE = Tabella1.Pagine(PaginaINS).Nomegruppo
  Call CancellaSezioneIni(NomeSEZIONE, Tabella1.PathOEM & "SWAPPO.INI")
  Call WritePrivateProfileString(NomeSEZIONE, "DATE", CStr(Now), Tabella1.PathOEM & "SWAPPO.INI")
  Call WritePrivateProfileString(NomeSEZIONE, "NOME", Tabella1.NomePezzo, Tabella1.PathOEM & "SWAPPO.INI")
  For i = 0 To UBound(TPars)
    If (Lp_Str(TPars(i)) <> "NOTFOUND") Then
      Call WritePrivateProfileString(NomeSEZIONE, TPars(i), Lp_Str(TPars(i)), Tabella1.PathOEM & "SWAPPO.INI")
    End If
  Next i
  '
End Sub

Public Sub LeggiCiclo(NomeCiclo As String)
'
Dim tstr    As String
Dim iTab1() As Integer
Dim Tvar    As String
Dim TPars() As String
Dim tpar    As String
Dim i       As Integer
Dim j       As Integer
Dim k       As Integer
'
On Error Resume Next
  '
  tstr = GetTuttiParSequenza(Tabella1.Pagine(PaginaINS).sequenza)
  Tvar = ""
  TPars = Split(tstr, ";")
  ReDim iTab1(UBound(TPars))
  For i = 0 To UBound(TPars)
    j = GetParByINDICE(CInt(TPars(i)))
    If Tabella1.Parametri(j).Abilitato Then
      For k = 1 To Tabella1.Parametri(j).NActual
        If (Tabella1.Parametri(j).VARIABILE(k) <> "") Then
          If (Tvar <> "") Then Tvar = Tvar & ";"
          Tvar = Tvar & Tabella1.Parametri(j).VARIABILE(k)
        End If
        iTab1(i) = j
      Next k
    End If
  Next i
  '
  TPars = Split(Tvar, ";")
  '
  For i = 0 To UBound(TPars)
    tpar = GetINI(Tabella1.PathOEM & NomeCiclo, Tabella1.Pagine(PaginaINS).Nomegruppo, TPars(i))
    If (tpar <> "") Then
      Call SP(TPars(i), tpar, False)
      If Tabella1.Parametri(iTab1(i)).ONLine And Tabella1.Parametri(iTab1(i)).Modificabile And (Tabella1.MODALITA = "SPF") Then
        Call SCRITTURA_VALORI_ONLINE(iTab1(i), tpar)
      End If
    End If
  Next i
  Call AggiornaDB
  Call RaiseEvent_PaginaChanged(Tabella1.NOME, "", "", PaginaINS)
  Call DisegnaOEM
  If SuGrid1(SCon).Visible Then
    Call SuGrid1(SCon).SetFocus
    Call SuGrid1(SCon).SelFirst
  End If
  If (Tabella1.MODALITA = "SPF") Then Call CreaSPF
  '
End Sub

'****m* SUOEM/LeggiSWAP
' DESCRIZIONE:
' Legge i valori dei parametri della pagina corrente da un file INI (SWAPPO.ini)
' Dopo la lettura viene generato l'evento "PaginaChanged"
' Nota: Se tra i parametri letti dal file INI, alcuni sono calcolati,
' pu� capitare che in funzione dei parametri delle altre pagine, che possono essere
' differenti dal disegno originario, i valori vengano ricalcolati.
' Es(Ingranaggi Esterni): Se "incollo" la pagina "Profilo K" sul disegno attuale, e il "Diametro SAP"
' dei dati nello SWAP � minore del SAP minimo del disegno corrente (determinato da parametri
' ad esempio nella pagina "Profilo"), il sap visualizzato sar� il SAP minimo (per l'ingranaggio
' corrente) e non il "Diametro SAP" presente nello SWAP.
' DEFINIZIONE:
Public Sub LeggiSWAP()
'***
Dim tstr      As String
Dim iTab11()  As Integer
Dim Tvar      As String
Dim TPars()   As String
Dim tpar      As String
Dim ii        As Integer
Dim j         As Integer
Dim k         As Integer
'
On Error Resume Next
  '
  tstr = GetTuttiParSequenza(Tabella1.Pagine(PaginaINS).sequenza)
  TPars = Split(tstr, ";")
  ReDim iTab11(UBound(TPars))
  '
  Tvar = ""
  For ii = 0 To UBound(TPars)
    j = GetParByINDICE(Trim$(TPars(ii)))
    If Tabella1.Parametri(j).Abilitato Then
      For k = 1 To Tabella1.Parametri(j).NActual
        If (Tabella1.Parametri(j).VARIABILE(k) <> "") Then
          If (Tvar <> "") Then Tvar = Tvar & ";"
          Tvar = Tvar & Tabella1.Parametri(j).VARIABILE(k)
        End If
        iTab11(ii) = j
      Next k
    End If
  Next ii
  '
Dim iTab1()  As Integer
  j = 0
  For ii = 0 To UBound(TPars)
  If (iTab11(ii) <> 0) Then
    ReDim Preserve iTab1(j)
    iTab1(j) = iTab11(ii)
    j = j + 1
  End If
  Next ii
  '
  TPars = Split(Tvar, ";")
  For ii = 0 To UBound(TPars)
    tpar = GetINI(Tabella1.PathOEM & "SWAPPO.INI", Tabella1.Pagine(PaginaINS).Nomegruppo, TPars(ii))
    If (tpar <> "") Then
      Call SP(TPars(ii), tpar, False)
      'SVG131211: DA COMPLETARE QUANDO C'E' TEMPO
      If Tabella1.Parametri(iTab1(ii)).ONLine And Tabella1.Parametri(iTab1(ii)).Modificabile And (Tabella1.MODALITA = "SPF") Then
        Call SCRITTURA_VALORI_ONLINE(iTab1(ii), tpar)
      End If
    End If
  Next ii
  Call AggiornaDB
  Call RaiseEvent_PaginaChanged(Tabella1.NOME, "", "", PaginaINS)
  Call DisegnaOEM
  If SuGrid1(SCon).Visible Then
    Call SuGrid1(SCon).SetFocus
    Call SuGrid1(SCon).SelFirst
  End If
  If (Tabella1.MODALITA = "SPF") Then Call CreaSPF
  '
End Sub

'SIMILE LeggiSWAP ma gli passo l'indice della pagina ed evito qualche passaggio
'SOLO NELLO STATO OFF-LINE
Public Sub LeggiSWAP2(ByVal Pagina As Integer)
'***
Dim tstr    As String
Dim iTab1() As Integer
Dim Tvar    As String
Dim TPars() As String
Dim tpar    As String
Dim i       As Integer
Dim j       As Integer
Dim k       As Integer
'
On Error Resume Next
  '
  tstr = GetTuttiParSequenza(Tabella1.Pagine(Pagina).sequenza)
  Tvar = ""
  TPars = Split(tstr, ";")
  ReDim iTab1(UBound(TPars))
  For i = 0 To UBound(TPars)
    j = GetParByINDICE(CInt(TPars(i)))
    If Tabella1.Parametri(j).Abilitato Then
      For k = 1 To Tabella1.Parametri(j).NActual
        If (Tabella1.Parametri(j).VARIABILE(k) <> "") Then
          If (Tvar <> "") Then Tvar = Tvar & ";"
          Tvar = Tvar & Tabella1.Parametri(j).VARIABILE(k)
        End If
        iTab1(i) = j
      Next k
    End If
  Next i
  TPars = Split(Tvar, ";")
  For i = 0 To UBound(TPars)
    tpar = GetINI(Tabella1.PathOEM & "SWAPPO.INI", Tabella1.Pagine(Pagina).Nomegruppo, TPars(i))
    If (tpar <> "") Then Call SP(TPars(i), tpar, False)
  Next i
  '
End Sub

Private Sub lstEdit_DblClick(Index As Integer)
'
Dim tstr As String
'
On Error Resume Next
  '
  If (Trim$(lstEdit(Index).Text) = "NEW") Then
    lstEdit(Index).Visible = False
    Call NUOVA_MOLA(Index)
  Else
    If (PicTITOLO.Visible = True) Then TipoED = "LIST"
    lstEdit(Index).Visible = False
    Call EditDisEnable(Index)
    Call Riposiziona
    Call DisegnaOEM
    Call Disegni.RidisegnaPagina(PaginaINS, False)
  End If

End Sub

Private Sub lstEdit_KeyUp(Index As Integer, KeyCode As Integer, Shift As Integer)
'
On Error Resume Next
  '
  Select Case KeyCode
    '
    Case vbKeyReturn
      If (Trim$(lstEdit(Index).Text) = "NEW") Then
        lstEdit(Index).Visible = False
        Call NUOVA_MOLA(Index)
      Else
        If (PicTITOLO.Visible = True) Then TipoED = "LIST"
        lstEdit(Index).Visible = False
        Call EditDisEnable(Index)
        Call Disegni.RidisegnaPagina(PaginaINS, False)
        Call Riposiziona
        Call DisegnaOEM
      End If
      '
    Case vbKeyEscape, vbKeyTab
      lstEdit(Index).Visible = False
      Call Disegni.RidisegnaPagina(PaginaINS, False)
      Call Riposiziona
      Call DisegnaOEM
      '
  End Select
  '
End Sub

Private Sub lstEdit_LostFocus(Index As Integer)
'
Dim i As Integer
'
On Error Resume Next
  '
  PicTITOLO.Visible = False
  For i = 0 To 2
    lstEdit(i).Visible = False
  Next i

End Sub

Private Sub txtEdit_KeyUp(Index As Integer, KeyCode As Integer, Shift As Integer)
'
On Error Resume Next
  '
  Select Case KeyCode
    '
    Case vbKeyReturn
      Call EditDisEnable(Index)
      Call Disegni.RidisegnaPagina(PaginaINS, False)
      Call DisegnaOEM
      '
    Case vbKeyEscape, vbKeyUp, vbKeyDown
      txtEdit(Index).Visible = False
      Call Disegni.RidisegnaPagina(PaginaINS, False)
      Call DisegnaOEM
      '
  End Select

End Sub

Private Sub txtEdit_LostFocus(Index As Integer)
'
Dim i As Integer
'
On Error Resume Next
  '
  For i = 0 To 2
    txtEdit(i).Visible = False
  Next i
  
End Sub

Sub BoolChange(cell As Cella)

Dim tArr() As String
'
On Error Resume Next
  '
  If (Limiti = "") Then Exit Sub
  '
  tArr = Split(Limiti, ",")
  ReDim Preserve tArr(1)
  If UCase(Trim(cell.Value)) = UCase(Trim(tArr(0))) Then
    cell.Value = tArr(1)
  Else
    cell.Value = tArr(0)
  End If
  '
End Sub

Sub SetListItemHeight(ctrl As Control, ByVal newHeight As Long)
'
Dim uMsg As Long
'
On Error Resume Next
  '
  If TypeOf ctrl Is ListBox Then
    uMsg = LB_SETITEMHEIGHT
  ElseIf TypeOf ctrl Is ComboBox Then
    uMsg = CB_SETITEMHEIGHT
  Else
    Exit Sub
  End If
  'only the low-order word of lParam can be used.
  Call SendMessage(ctrl.hwnd, uMsg, 0, ByVal CLng(newHeight And &HFFFF&))
  ' It is necessary to manually refresh the control.
  ctrl.Refresh
  '
End Sub

Public Function AutoSizeLBHeight(LB As Object) As Boolean
'
'PURPOSE    : Will automatically set the height of a
'             list box based on the number and height of entries
'
'PARAMETERS : LB = the ListBox control to autosize
'
'RETURNS    : True if successful, false otherwise
'
'NOTE       : LB's parent's (e.g., form, picturebox)
'             scalemode must be vbTwips, which is the
'             default
'
If Not TypeOf LB Is ListBox Then Exit Function

Dim lItemHeight As Long
Dim lRet        As Long
Dim lItems      As Long
Dim sngTwips    As Single
Dim sngLBHeight As Single

On Error GoTo ErrHandler

  If LB.ListCount = 0 Then
    LB.Height = 125
    AutoSizeLBHeight = True
  Else
    lItems = LB.ListCount
    lItemHeight = SendMessage(LB.hwnd, LB_GETITEMHEIGHT, 0&, 0&)
    If lItemHeight > 0 Then
        sngTwips = lItemHeight * Screen.TwipsPerPixelY
        sngLBHeight = (sngTwips * lItems) + 125
        LB.Height = sngLBHeight
        AutoSizeLBHeight = True
    End If
  End If

ErrHandler:
  MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Sub: AutoSizeLBHeight"

End Function

Public Sub CROCE()
'
Dim tstr As String
Dim i    As Integer
'
On Error Resume Next
  '
  For i = 0 To 2
    If (SuGrid1(i).Visible = True) Then
      tstr = SuGrid1(i).Selected
      Call SuGrid1(i).CROCE(tstr)
      Call SuGrid1(i).SetFocus
      Exit For
    End If
  Next i

End Sub
