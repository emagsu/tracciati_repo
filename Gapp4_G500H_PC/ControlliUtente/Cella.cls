VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Cella"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public ParentPic       As PictureBox
Attribute ParentPic.VB_VarHelpID = -1
Public VScroll         As VScrollBar
Attribute VScroll.VB_VarHelpID = -1
Public Key             As String

Private m_BackColor    As OLE_COLOR
Private m_ForeColor    As OLE_COLOR
Private m_SelColor     As OLE_COLOR
Private m_RowSelColor  As OLE_COLOR
Private m_fTop         As Single
Private m_fLeft        As Single
Private m_fHeight      As Single
Private m_fWidth       As Single

Private m_sValue       As String
Private m_sVariabile   As String

Private m_bAutoRedraw  As Boolean
Private m_bSelected    As Boolean
Private m_bSelectable  As Boolean
Private m_bRowSel      As Boolean

Private X1             As Single
Private Y1             As Single
Private X2             As Single
Private Y2             As Single

Public Enum Allineamenti
  CentroSx = 0
  CentroDx = 1
  CentroCentro = 2
End Enum

Private m_Allineamento  As Allineamenti
Private m_sLimiti       As String
Private m_sTipo         As String
Private m_sLastValue    As String
Private m_sLimitiVis    As String
Private m_lBorderColor  As Long
Private m_bBold         As Boolean
Private m_sBoolValues   As String
Private m_ExBackColor   As OLE_COLOR
Private m_bCorrettore   As Boolean
Private m_sValueCorr    As String

Public Property Get Correttore() As Boolean
'
On Error Resume Next

  Correttore = m_bCorrettore

End Property

Public Property Let Correttore(ByVal bCorrettore As Boolean)
'
On Error Resume Next

  m_bCorrettore = bCorrettore

End Property

Public Property Get BoolValues() As String
'
On Error Resume Next

  BoolValues = m_sBoolValues

End Property

Public Property Let BoolValues(ByVal sBoolValues As String)
'
On Error Resume Next

  m_sBoolValues = sBoolValues

End Property

Public Property Get Selected() As Boolean
'
On Error Resume Next

  Selected = m_bSelected

End Property

Public Property Let Selected(ByVal bSelected As Boolean)
'
On Error Resume Next
  
  m_bSelected = bSelected
  '
  If m_bSelected Then
    If vTop < 0 Then
      VScroll = CInt(m_fTop / m_fHeight)
    ElseIf vTop + m_fHeight > ParentPic.ScaleHeight Then
      VScroll = CInt((m_fTop + m_fHeight - ParentPic.ScaleHeight) / m_fHeight)
    End If
  End If
  If m_bAutoRedraw Then Call Redraw
  
End Property

Public Property Get Value() As String
Attribute Value.VB_MemberFlags = "200"
'
On Error Resume Next

  Value = m_sValue

End Property

Public Property Let Value(ByVal sValue As String)
'
On Error Resume Next

  m_sLastValue = m_sValue
  m_sValue = sValue
  If m_bAutoRedraw Then Call Redraw

End Property

Public Property Get BackColor() As Long
'
On Error Resume Next

  BackColor = m_BackColor

End Property

Public Property Let BackColor(ByVal BackColor As Long)
'
On Error Resume Next
  
  m_ExBackColor = m_BackColor
  m_BackColor = BackColor
  If m_bAutoRedraw Then Call Redraw

End Property

Public Property Get ForeColor() As Long
'
On Error Resume Next

  ForeColor = m_ForeColor

End Property

Public Property Get BorderColor() As Long
'
On Error Resume Next

  BorderColor = m_lBorderColor

End Property

Public Property Let BorderColor(ByVal lBorderColor As Long)
'
On Error Resume Next

  m_lBorderColor = lBorderColor

End Property

Public Property Let ForeColor(ByVal ForeColor As Long)
'
On Error Resume Next

  m_ForeColor = ForeColor
  If m_bAutoRedraw Then Call Redraw

End Property

Public Property Get SelColor() As Long
'
On Error Resume Next

  SelColor = m_SelColor

End Property

Public Property Let SelColor(ByVal SelColor As Long)
'
On Error Resume Next

  m_SelColor = SelColor
  If m_bAutoRedraw Then Call Redraw

End Property

Public Property Get Top() As Single
'
On Error Resume Next

  Top = m_fTop

End Property

Public Property Let Top(ByVal fTop As Single)
'
On Error Resume Next
  
  m_fTop = fTop
  If m_bAutoRedraw Then Call Redraw
  
End Property

Public Property Get Left() As Single
'
On Error Resume Next

  Left = m_fLeft

End Property

Public Property Get vLeft() As Single
'
On Error Resume Next
  
  vLeft = m_fLeft
  If m_bCorrettore Then
    vLeft = m_fLeft + ParentPic.TextWidth(m_sValue & " ") + 2 * ParentPic.TextWidth("O") / 3
  End If

End Property

Public Property Let Left(ByVal fleft As Single)
'
On Error Resume Next

  m_fLeft = fleft
  If m_bAutoRedraw Then Call Redraw
  
End Property

Public Property Get Height() As Single
'
On Error Resume Next

  Height = m_fHeight
  
End Property

Public Property Let Height(ByVal fHeight As Single)
'
On Error Resume Next

  m_fHeight = fHeight
  If m_bAutoRedraw Then Call Redraw
  
End Property

Public Property Get Width() As Single
'
On Error Resume Next

  Width = m_fWidth

End Property

Public Property Let Width(ByVal fWidth As Single)
'
On Error Resume Next
  
  m_fWidth = fWidth
  If m_bAutoRedraw Then Call Redraw
  
End Property

Public Property Get AutoRedraw() As Boolean
'
On Error Resume Next

  AutoRedraw = m_bAutoRedraw

End Property

Public Property Let AutoRedraw(ByVal bAutoRedraw As Boolean)
'
On Error Resume Next

  m_bAutoRedraw = bAutoRedraw
  If m_bAutoRedraw Then Call Redraw

End Property

Public Function vTop() As Single
'
On Error Resume Next
  
  vTop = m_fTop - m_fHeight * VScroll

End Function

Public Sub Redraw()

Dim tBKCol  As Long
Dim tTX     As Single
Dim tTY     As Single
Dim tTX2    As Single
Dim tTY2    As Single
Dim TH      As Single
Dim tArr()  As String
Dim tDW     As Integer
Dim tstr    As String
Dim tValue  As String
'
On Error Resume Next

  If m_bSelected Then
    tBKCol = m_SelColor
  ElseIf m_bRowSel Then
    tBKCol = m_RowSelColor
  Else
    tBKCol = m_BackColor
  End If
  ParentPic.FontBold = m_bBold
  X1 = m_fLeft
  X2 = m_fLeft + m_fWidth
  Y1 = m_fTop + m_fHeight * (1 - VScroll)
  Y2 = m_fTop - (m_fHeight * VScroll)
  'Sfondo cella
  ParentPic.Line (X1, Y1)-(X2, Y2), tBKCol, BF
  'Bordi
  ParentPic.Line (X1, Y1)-(X1, Y2), m_lBorderColor
  ParentPic.Line -(X2, Y2), m_lBorderColor
  ParentPic.Line -(X2, Y1), m_lBorderColor
  ParentPic.Line -(X1, Y1), m_lBorderColor

  If m_bCorrettore Then
    If (m_sValueCorr = "") Then
      m_sValueCorr = "0"
    End If
    If (m_sVariabile <> m_sValue) Then
      If (val(m_sValueCorr) >= 0) Then
        tValue = m_sValue & " + " & Abs(val(m_sValueCorr))
      Else
        tValue = m_sValue & " - " & Abs(val(m_sValueCorr))
      End If
    Else
      tValue = m_sValue & " : " & (m_sValueCorr)
    End If
  Else
    tValue = m_sValue
  End If
  
  If (BoolValues <> "") Then
    tArr = Split(BoolValues, ",")
    TH = ParentPic.TextHeight(tValue)
    tTY = (Y2 + Y1 - TH) / 2
    tTX = (X1 + X2 - TH) / 2
    tTX2 = tTX + TH
    tTY2 = tTY + TH
    If (tArr(0) = Value) Or (tArr(1) = Value) Then
      ParentPic.Line (tTX, tTY)-(tTX2, tTY), m_lBorderColor
      ParentPic.Line (tTX2, tTY)-(tTX2, tTY2), m_lBorderColor
      ParentPic.Line (tTX, tTY2)-(tTX2, tTY2), m_lBorderColor
      ParentPic.Line (tTX, tTY2)-(tTX, tTY), m_lBorderColor
      If (tArr(1) = Value) Then
        tDW = ParentPic.DrawWidth
        ParentPic.DrawWidth = 4
        tTX = tTX + TH / 5
        tTY = tTY + TH / 5
        tTX2 = tTX2 - TH / 5
        tTY2 = tTY2 - TH / 5
        'Disegna Visto
        ParentPic.Line (tTX, (tTY + tTY2) / 2)-(((tTX + tTX2) / 2) - TH / 5, tTY2), m_ForeColor
        ParentPic.Line (((tTX + tTX2) / 2) - TH / 5, tTY2)-(tTX2, tTY), m_ForeColor
        ParentPic.DrawWidth = tDW
      End If
      Exit Sub
    End If
  End If
  
  tTY = (Y2 + Y1 - ParentPic.TextHeight(tValue)) / 2
  Select Case m_Allineamento
    Case CentroSx:      tTX = X1 + ParentPic.TextWidth("O") / 2
    Case CentroDx:      tTX = X2 - ParentPic.TextWidth(tValue) - ParentPic.TextWidth("O") / 2
    Case CentroCentro:  tTX = (X1 + X2 - ParentPic.TextWidth(tValue)) \ 2
  End Select
  ParentPic.CurrentX = tTX
  ParentPic.CurrentY = tTY
  tstr = "" & tValue
  If (ParentPic.CurrentX + ParentPic.TextWidth(tstr) > X2) Then
    Do While (ParentPic.CurrentX + ParentPic.TextWidth(tstr) + ParentPic.TextWidth(">") > X2) And (Len(tstr) > 1)
      If (Len(tstr) <= 1) Then Exit Do
      tstr = Left(tstr, Len(tstr) - 1)
    Loop
    ParentPic.Print "" & tstr
    ParentPic.CurrentX = X2 - ParentPic.TextWidth(">")
    ParentPic.CurrentY = tTY
    ParentPic.Print ">"
  Else
    ParentPic.Print "" & tValue
  End If
  ParentPic.FontBold = False

End Sub

Public Sub STAMPA_CELLA(Top As Single, Left As Single, XScala As Single, YScala As Single)
'
Dim tBKCol  As Long
Dim tTX     As Single
Dim tTY     As Single
Dim tTX2    As Single
Dim tTY2    As Single
Dim TH      As Single
Dim tArr()  As String
Dim tDW     As Integer
Dim tstr    As String
Dim tValue  As String
Dim tYScala As Single
'
On Error GoTo errSTAMPA_CELLA
  '
  X1 = m_fLeft
  X2 = m_fLeft + m_fWidth
  '
  Y1 = m_fTop + m_fHeight
  Y2 = m_fTop
  '
  X1 = (X1 * XScala)
  X2 = (X2 * XScala)
  '
  Y1 = Int(Y1 * YScala * 10) / 10
  Y2 = Int(Y2 * YScala * 10) / 10
  '
  X1 = X1 + Left
  X2 = X2 + Left
  '
  Y1 = Y1 + Top
  Y2 = Y2 + Top
  '
  Printer.FontBold = m_bBold
  '
  'Sfondo cella
  tBKCol = vbBlack
  'Bordi
  Printer.Line (X1, Y1)-(X2, Y2), m_BackColor, BF
  Printer.Line (X1, Y1)-(X1, Y2), tBKCol
  Printer.Line -(X2, Y2), tBKCol
  Printer.Line -(X2, Y1), tBKCol
  Printer.Line -(X1, Y1), tBKCol
  '
  If m_bCorrettore Then
    If (m_sValueCorr = "") Then m_sValueCorr = "0"
    '
    If (m_sVariabile <> m_sValue) Then
      If val(m_sValueCorr) >= 0 Then
        tValue = m_sValue & " + " & Abs(val(m_sValueCorr))
      Else
        tValue = m_sValue & " - " & Abs(val(m_sValueCorr))
      End If
    Else
      tValue = m_sValue & " : " & (m_sValueCorr)
    End If
    '
  Else
    tValue = m_sValue
  End If
  '
  If (BoolValues <> "") Then
    tArr = Split(BoolValues, ",")
    TH = Printer.TextHeight(tValue)
    tTY = (Y2 + Y1 - TH) / 2
    tTX = (X1 + X2 - TH) / 2
    tTX2 = tTX + TH
    tTY2 = tTY + TH
    If (tArr(0) = Value) Or (tArr(1) = Value) Then
      Printer.Line (tTX, tTY)-(tTX2, tTY), m_lBorderColor
      Printer.Line (tTX2, tTY)-(tTX2, tTY2), m_lBorderColor
      Printer.Line (tTX, tTY2)-(tTX2, tTY2), m_lBorderColor
      Printer.Line (tTX, tTY2)-(tTX, tTY), m_lBorderColor
      If (tArr(1) = Value) Then
        tDW = ParentPic.DrawWidth
        Printer.DrawWidth = 3
        tTX = tTX + TH / 10
        tTY = tTY + TH / 10
        tTX2 = tTX2 - TH / 10
        tTY2 = tTY2 - TH / 10
        'Disegna Visto
        Printer.Line (tTX, (tTY + tTY2) / 2)-(((tTX + tTX2) / 2) - TH / 5, tTY2), m_ForeColor
        Printer.Line (((tTX + tTX2) / 2) - TH / 5, tTY2)-(tTX2, tTY), m_ForeColor
        Printer.DrawWidth = tDW
      End If
      Exit Sub
    End If
  End If
  '
  tTY = (Y2 + Y1 - Printer.TextHeight("X")) / 2
  Select Case m_Allineamento
    Case CentroSx:     tTX = X1 + Printer.TextWidth("X") / 2
    Case CentroDx:     tTX = X2 - Printer.TextWidth(tValue) - Printer.TextWidth("X") / 2
    Case CentroCentro: tTX = (X1 + X2 - Printer.TextWidth(tValue)) / 2
  End Select
  Printer.CurrentX = tTX
  Printer.CurrentY = tTY
  '
  tstr = "" & tValue
  If Printer.CurrentX + Printer.TextWidth(tstr) > X2 Then
    While (Printer.CurrentX + Printer.TextWidth(tstr) + Printer.TextWidth(">") > X2) And (Len(tstr) > 1)
      tstr = VBA.Left(tstr, Len(tstr) - 1)
    Wend
    Printer.Print "" & tstr
    Printer.CurrentX = X2 - Printer.TextWidth(">")
    Printer.CurrentY = tTY
    Printer.Print ">"
  Else
    Printer.FontTransparent = True
    Printer.Print "" & tValue
  End If
  '
Exit Sub

errSTAMPA_CELLA:

End Sub

Public Property Get Selectable() As Boolean
'
On Error Resume Next

  Selectable = m_bSelectable

End Property

Public Property Let Selectable(ByVal bSelectable As Boolean)
'
On Error Resume Next

  m_bSelectable = bSelectable

End Property

Public Property Get RowSel() As Boolean
'
On Error Resume Next

  RowSel = m_bRowSel

End Property

Public Property Let RowSel(ByVal bRowSel As Boolean)
'
On Error Resume Next

  m_bRowSel = bRowSel
  If m_bAutoRedraw Then Call Redraw

End Property

Public Property Get RowSelColor() As OLE_COLOR
'
On Error Resume Next

  RowSelColor = m_RowSelColor

End Property

Public Property Let RowSelColor(ByVal RowSelColor As OLE_COLOR)
'
On Error Resume Next

  m_RowSelColor = RowSelColor
  If m_bAutoRedraw Then Call Redraw

End Property

Public Property Get Allineamento() As Allineamenti
'
On Error Resume Next

  Allineamento = m_Allineamento

End Property

Public Property Let Allineamento(ByVal Allineamento As Allineamenti)
'
On Error Resume Next

  m_Allineamento = Allineamento

End Property

Public Property Get Variabile() As String
'
On Error Resume Next

  Variabile = m_sVariabile

End Property

Public Property Let Variabile(ByVal sVariabile As String)
'
On Error Resume Next

  m_sVariabile = sVariabile

End Property

Public Property Get Limiti() As String
'
On Error Resume Next

  Limiti = m_sLimiti

End Property

Public Property Let Limiti(ByVal sLimiti As String)
'
On Error Resume Next

  m_sLimiti = sLimiti

End Property

Public Property Get LastValue() As String
'
On Error Resume Next

  LastValue = m_sLastValue

End Property

Public Property Get LimitiVis() As String
'
On Error Resume Next

  LimitiVis = m_sLimitiVis

End Property

Public Property Let LimitiVis(ByVal sLimitiVis As String)
'
On Error Resume Next

  m_sLimitiVis = sLimitiVis

End Property

Public Property Get Bold() As Boolean
'
On Error Resume Next

  Bold = m_bBold

End Property

Public Property Let Bold(ByVal bBold As Boolean)
'
On Error Resume Next

  m_bBold = bBold

End Property

Public Property Get ValueCorr() As String
'
On Error Resume Next

  ValueCorr = m_sValueCorr

End Property

Public Property Let ValueCorr(ByVal sValueCorr As String)
'
On Error Resume Next

  m_sValueCorr = sValueCorr

End Property
