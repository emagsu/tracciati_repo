VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.UserControl Grafico 
   BackColor       =   &H00EBE1D7&
   ClientHeight    =   4812
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   7356
   ControlContainer=   -1  'True
   LockControls    =   -1  'True
   ScaleHeight     =   4812
   ScaleWidth      =   7356
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   1
      Left            =   5535
      Top             =   4200
   End
   Begin MSComctlLib.Toolbar Toolbar1 
      Height          =   312
      Left            =   48
      TabIndex        =   3
      Top             =   48
      Width           =   1392
      _ExtentX        =   2455
      _ExtentY        =   550
      ButtonWidth     =   487
      ButtonHeight    =   466
      ToolTips        =   0   'False
      Appearance      =   1
      ImageList       =   "ImageList1"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   9
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "zoom"
            ImageIndex      =   1
            Style           =   2
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "pan"
            ImageIndex      =   7
            Style           =   2
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Object.Visible         =   0   'False
            Style           =   3
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Object.Visible         =   0   'False
            Key             =   "aggancia"
            ImageIndex      =   2
            Style           =   1
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Object.Visible         =   0   'False
            Key             =   "griglia"
            ImageIndex      =   3
            Style           =   1
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Object.Visible         =   0   'False
            Style           =   4
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Object.Visible         =   0   'False
            Key             =   "misura"
            ImageIndex      =   4
            Style           =   2
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "aggiungi"
            ImageIndex      =   5
            Style           =   2
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "rimuovi"
            ImageIndex      =   6
            Style           =   2
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList ImageList1 
      Left            =   6180
      Top             =   4080
      _ExtentX        =   995
      _ExtentY        =   995
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Grafico.ctx":0000
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Grafico.ctx":00E2
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Grafico.ctx":01C4
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Grafico.ctx":02A6
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Grafico.ctx":0388
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Grafico.ctx":046A
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Grafico.ctx":054C
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.VScrollBar barScalaErr 
      Height          =   4095
      Left            =   7005
      Max             =   1
      Min             =   100
      TabIndex        =   2
      Top             =   30
      Value           =   1
      Visible         =   0   'False
      Width           =   255
   End
   Begin MSComDlg.CommonDialog Conf1 
      Left            =   4665
      Top             =   4155
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.PictureBox Display 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H00FFFFFF&
      ForeColor       =   &H80000008&
      Height          =   3285
      Left            =   0
      MousePointer    =   4  'Icon
      ScaleHeight     =   57.573
      ScaleMode       =   6  'Millimeter
      ScaleWidth      =   122.343
      TabIndex        =   0
      Top             =   550
      Width           =   6960
      Begin VB.PictureBox picLeg 
         Appearance      =   0  'Flat
         AutoRedraw      =   -1  'True
         BackColor       =   &H00EBE1D7&
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   600
         ScaleHeight     =   4.022
         ScaleMode       =   6  'Millimeter
         ScaleWidth      =   8.255
         TabIndex        =   5
         Top             =   120
         Visible         =   0   'False
         Width           =   495
      End
      Begin VB.Line shpJoinEv 
         BorderColor     =   &H000000FF&
         BorderWidth     =   2
         Visible         =   0   'False
         X1              =   0
         X2              =   2.117
         Y1              =   0
         Y2              =   0
      End
      Begin VB.Label lblLegenda 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.6
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000005&
         Height          =   195
         Left            =   600
         TabIndex        =   4
         Top             =   0
         Visible         =   0   'False
         Width           =   120
      End
      Begin VB.Line shpMisura 
         BorderColor     =   &H0000FF00&
         BorderStyle     =   3  'Dot
         Visible         =   0   'False
         X1              =   63.5
         X2              =   84.667
         Y1              =   59.267
         Y2              =   59.267
      End
      Begin VB.Shape Selez 
         BorderStyle     =   3  'Dot
         Height          =   135
         Left            =   240
         Top             =   3000
         Visible         =   0   'False
         Width           =   135
      End
      Begin VB.Line CursoreY 
         BorderColor     =   &H00C0C0C0&
         Visible         =   0   'False
         X1              =   48.683
         X2              =   48.683
         Y1              =   0
         Y2              =   74.083
      End
      Begin VB.Shape Evid 
         BorderColor     =   &H000000FF&
         BorderWidth     =   2
         Height          =   90
         Left            =   1080
         Shape           =   3  'Circle
         Top             =   1080
         Visible         =   0   'False
         Width           =   90
      End
      Begin VB.Line CursoreX 
         BorderColor     =   &H00C0C0C0&
         Visible         =   0   'False
         X1              =   0
         X2              =   105.833
         Y1              =   31.75
         Y2              =   31.75
      End
   End
   Begin VB.Label Info 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   550
      Left            =   1500
      TabIndex        =   1
      Top             =   0
      Width           =   4000
   End
   Begin VB.Menu DxPop 
      Caption         =   "DxPop"
      Visible         =   0   'False
      Begin VB.Menu mnuZoom 
         Caption         =   "Zoom Tutto"
      End
      Begin VB.Menu mnuZoomOut 
         Caption         =   "Zoom Out"
      End
      Begin VB.Menu mnuZoomIn 
         Caption         =   "Zoom In"
      End
      Begin VB.Menu mnuZoomF 
         Caption         =   "Zoom Fattore"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuLegenda 
         Caption         =   "Legenda"
         Checked         =   -1  'True
         Visible         =   0   'False
      End
      Begin VB.Menu mnuCentra 
         Caption         =   "Centra"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuGriglia 
         Caption         =   "Griglia"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuEsporta 
         Caption         =   "Esporta..."
         Begin VB.Menu mnuDXF 
            Caption         =   "DXF"
         End
         Begin VB.Menu mnuXYT 
            Caption         =   "XY"
         End
      End
      Begin VB.Menu mnuUser1 
         Caption         =   "User1"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuUser2 
         Caption         =   "User2"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuUser3 
         Caption         =   "User3"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuUser4 
         Caption         =   "User4"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuAbout 
         Caption         =   "About"
      End
   End
End
Attribute VB_Name = "Grafico"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "PropPageWizardRun" ,"Yes"
Option Explicit

Const GraficoVersion = "SVG (01.06.12)"

Private Type My_Points
    X As Single
    Y As Single
End Type

Private MyPoints      As My_Points
Private DXF_BodyText  As String
Private DXF_BlockBody As String
Private BlockIndex    As Integer
Private iLayer        As String

Public INIZIO_PUNTI   As Integer
Public TIPO_PROFILO   As String

Public Enum TipoScala
    Lineare = 0
    Semilogaritmica = 1
    Logaritmica = 2
End Enum

Public Enum TipoMarker
    Radiale = 0
    Orizzontale = 1
    Verticale = 2
End Enum

Private Type marker
    NOME    As String
    TIPO    As TipoMarker
    Valore  As Double
End Type

Private Type Serie
    X()         As Double
    Y()         As Double
    T()         As Double
    N()         As Integer
    Visible     As Boolean
    NOME        As String
    MarcaPunti  As Boolean
    COLORE      As FiColors
    Legenda     As String
End Type

Private Type linea
    X1      As Double
    Y1      As Double
    X2      As Double
    Y2      As Double
    Visible As Boolean
    NOME    As String
    COLORE  As FiColors
End Type

Private Type TESTO
    X       As Double
    Y       As Double
    Valore  As String
    Visible As Boolean
    NOME    As String
    COLORE  As FiColors
End Type

Private Type Cerchio
    Xc      As Double
    YC      As Double
    A1      As Double
    A2      As Double
    R       As Double
    Arco    As Integer
    Visible As Boolean
    NOME    As String
    COLORE  As FiColors
End Type

Private Type SJoin
    X1()    As Double
    Y1()    As Double
    X2()    As Double
    Y2()    As Double
    NOME    As String
    COLORE  As FiColors
    SCALA   As Double
    Legenda As String
End Type

Public Enum FiColors
    fiWHITE = &HFFFFFF
    fired = &HFF&
    fiorange = &HA5FF&
    fiyellow = &HFFFF&
    figreen = &H8000&
    fibLUE = &HFF0000
    fiPurple = &H800080
    fiblack = &H0&
    fiAliceBlue = &HFFF8F0
    fiAntiqueWhite = &HD7EBFA
    fiAqua = &HFFFF00
    fiAquamarine = &HD4FF7F
    fiAzure = &HFFFFF0
    fiBeige = &HDCF5F5
    fiBisque = &HC4E4FF
    fiBlanchedAlmond = &HCDEBFF
    fiBlueViolet = &HE22B8A
    fiBrown = &H2A2AA5
    fiBurlyWood = &H87B8DE
    fiCadetBlue = &HA09E5F
    fiChartreuse = &HFF7F&
    fiChocolate = &H1E69D2
    fiCoral = &H507FFF
    fiCornFlowerBlue = &HED9564
    fiCornSilk = &HDCF8FF
    fiCrimson = &H3C14DC
    fiCyan = &HFFFF00
    fiDarkBlue = &H8B0000
    fiDarkCyan = &H8B8B00
    fiDarkGoldenrod = &HB86B8
    fiDarkGray = &HA9A9A9
    fiDarkGreen = &H6400&
    fiDarkKhaki = &H6BB7BD
    fiDarkMagenta = &H8B00BD
    fiDarkOliveGreen = &H2F6B55
    fiDarkOrange = &H8CFF&
    fiDarkOrchid = &HCC3299
    fiDarkRed = &H8B&
    fiDarkSalmon = &H7A96E9
    fiDarkSeaGreen = &H8FBC8F
    fiDarkSlateBlue = &H8B3D48
    fiDarkSlateGray = &H4F4F2F
    fiDarkTurquoise = &HD1CE00
    fiDarkViolet = &HD30094
    fiDeepPink = &H9314FF
    fiDeepSkyBlue = &HFFBF00
    fiDimGray = &H696969
    fiDodgerBlue = &HFF901E
    fiFireBrick = &H2222B2
    fiFloralWhite = &HF0FAFF
    fiForestGreen = &H228B22
    fiFuschia = &HFF00FF
    fiGainsboro = &HDCDCDC
    fiGhostWhite = &HFFF8F8
    fiGold = &HD7FF&
    fiGoldenrod = &H20A5DA
    fiGray = &H808080
    fiGreenYellow = &H2FFFAD
    fiHoneydew = &HF0FFF0
    fiHotPink = &HB469FF
    fiIndianRed = &H5C5CCD
    fiIndigo = &H82004B
    fiIvory = &HF0FFFF
    fiKhaki = &H8CE6F0
    fiLavender = &HFAE6E6
    fiLavenderBlush = &HF5F0FF
    fiLemonChiffon = &HCDFAFF
    fiLightBlue = &HE6D8AD
    fiLightCoral = &H8080F0
    fiLightCyan = &HFFFFE0
    fiLightGoldenrodYellow = &HD2FAFA
    fiLightGreen = &H90EE90
    fiLightGray = &HD3D3D3
    fiLightPink = &HC1B6FF
    fiLightSalmon = &H7AA0FF
    fiLightSeaGreen = &HAAB220
    fiLightSkyBlue = &HFACE87
    fiLightSlateGray = &H998877
    fiLightSteelBlue = &HDEC4B0
    fiLightYellow = &HE0FFFF
    fiLime = &HFF00&
    fiLimeGreen = &H32CD32
    fiLinen = &HE6F0FA
    fiMagenta = &HFF00FF
    fiMaroon = &H80&
    fiMediumAquamarine = &HAACD66
    fiMediumBlue = &HCD0000
    fiMediumOrchid = &HD355BA
    fiMediumPurple = &HDB7093
    fiMediumSeaGreen = &H71B33C
    fiMediumSlateBlue = &HEE687B
    fiMediumSpringGreen = &H9AFA00
    fiMediumTurquoise = &HCCD148
    fiMediumVioletRed = &H8515C7
    fiMidnightBlue = &H701919
    fiMintCream = &HFAFFF5
    fiMistyRose = &HE1E4FF
    fiNavajoWhite = &HADDEFF
    fiNavy = &H800000
    fiOldLace = &HE6F5FD
    fiOlive = &H8080&
    fiOliveDrab = &H238E6B
    fiOrangeRed = &H45FF&
    fiOrchid = &HD670DA
    fiPaleGoldenrod = &HAAE8EE
    fiPaleGreen = &H98FB98
    fiPaleTurquoise = &HEEEEAF
    fiPaleVioletRed = &H9370DB
    fiPapayaWhip = &HD5EFFF
    fiPeachPuff = &HB9DAFF
    fiPeru = &H3F85CD
    fiPink = &HCBC0FF
    fiPlum = &HDDA0DD
    fiPowderBlue = &HE6E0B0
    fiRosyBrown = &H8F8FBC
    fiRoyalBlue = &HE16941
    fiSaddleBrown = &H13458B
    fiSeaGreen = &H578B2E
    fiSeaShell = &HEEF5FF
    fiSienna = &H2D52A0
    fiSilver = &HC0C0C0
    fiSkyBlue = &HEBCE87
    fiSlateBlue = &HCD5A6A
    fiSlateGray = &H908070
    fiSnow = &HFAFAFF
    fiSpringGreen = &H7FFF00
    fiSteelBlue = &HB48246
    fiTan = &H86B4D2
    fiTeal = &H808000
    fiThistle = &HDBBFD8
    fiTomato = &H4763FF
    fiTurquoise = &HD0E040
    fiViolet = &HEE82EE
    fiWheat = &HB3DEF5
    fiWhiteSmoke = &HF5F5F5
    fiYellowGreen = &H32CD9A
End Enum

Private Markers() As marker
Private Series()  As Serie
Private Linee()   As linea
Private Cerchi()  As Cerchio
Private Testi()   As TESTO
Private SJoins()  As SJoin

Dim RABin     As RAB
Dim XYT1      As XYT
Dim GR()      As Double
Dim GA()      As Double
Dim GB()      As Double
Dim GX()      As Double
Dim GY()      As Double
Dim GX2()     As Double
Dim GY2()     As Double
Dim GT()      As Double
Dim GN()      As Integer
Dim Gerr()    As Double
Dim SpostX    As Double
Dim SpostY    As Double
Dim Xm        As Double
Dim Ym        As Double
Dim SCALA     As Double
Dim ScalaX    As Double
Dim ScalaY    As Double
Dim MinX      As Double
Dim MaxX      As Double
Dim MinY      As Double
Dim MaxY      As Double
Dim P         As Integer
Dim Graf      As Boolean
Attribute Graf.VB_VarProcData = "Parametri"
Dim XRet1     As Double
Dim XRet2     As Double
Dim YRet1     As Double
Dim YRet2     As Double
Dim XLin1     As Double
Dim XLin2     As Double
Dim YLin1     As Double
Dim YLin2     As Double
Dim XPan1     As Double
Dim YPan1     As Double
Dim tmpPan    As Boolean
Dim CurrX     As Double
Dim CurrY     As Double
Public TolPos       As Double
Attribute TolPos.VB_VarProcData = "Propriet�"
Public TolNeg       As Double
Attribute TolNeg.VB_VarProcData = "Propriet�"
Public ScalaErr     As Double
Dim blnMisura       As Boolean
Dim blnAggiungi     As Boolean
Dim blnRimuovi      As Boolean
Dim blnZoom         As Boolean
Dim blnPan          As Boolean
Dim col(10)         As FiColors
Dim cnt             As Integer
Dim LinguaMisura    As String

Public ExportPath   As String
Public griglia      As Boolean
Public aggancia     As Boolean
Public ScalaAssi    As TipoScala
Attribute ScalaAssi.VB_VarProcData = "PropertyPage1"
Public StepX        As Double
Attribute StepX.VB_VarProcData = "Propriet�"
Public StepY        As Double
Attribute StepY.VB_VarProcData = "Propriet�"
Public MarcaPunti   As Boolean
Attribute MarcaPunti.VB_VarProcData = "Propriet�"
Public User1        As String
Attribute User1.VB_VarProcData = "Propriet�"
Public User2        As String
Attribute User2.VB_VarProcData = "Propriet�"
Public User3        As String
Attribute User3.VB_VarProcData = "Propriet�"
Public User4        As String
Attribute User4.VB_VarProcData = "Propriet�"
Public PASSO        As Double
Public Visualizza1  As Boolean
Attribute Visualizza1.VB_VarProcData = "Propriet�"
Public Visualizza2  As Boolean
Attribute Visualizza2.VB_VarProcData = "Propriet�"

Public FontName     As String
Public FontSize     As Integer

Public Event Evidenziato()
Public Event Click()
Public Event UserMenu(MenuIndex As Integer, X As Double, Y As Double, PX As Double, PY As Double)
Public Event SelChgByMouse()

Dim PG As Double

Private Type RAB
    R() As Double
    A() As Double
    B() As Double
End Type

Private Type XYT
    X() As Double
    Y() As Double
    T() As Double
End Type

Public Property Get hwnd() As Long
    
  hwnd = UserControl.hwnd

End Property

'****************************************************************************
'*
'* Funzioni Add... per inserimento entit�
'*
'* AddLinea
'* AddMarker
'* AddRaccordo
'* AddSerieXYT
'* AddCerchioCR
'* AddCerchio2PR
'* AddSJoin
'* AddTesto
'*
'****************************************************************************

Public Sub AddLinea(X1 As Double, Y1 As Double, X2 As Double, Y2 As Double, _
                    Optional COLORE As FiColors = fiblack, Optional NOME As String = "")
'
Dim Esistente As Boolean
Dim i         As Integer
  '
  For i = 1 To UBound(Linee)
    Esistente = False
    If UCase(Linee(i).NOME) = UCase(NOME) Then
      Esistente = True
      Exit For
    End If
  Next i
  If Esistente Then
    Linee(i).X1 = X1
    Linee(i).Y1 = Y1
    Linee(i).X2 = X2
    Linee(i).Y2 = Y2
    Linee(i).COLORE = COLORE
  Else
    ReDim Preserve Linee(UBound(Linee) + 1)
    Linee(UBound(Linee)).X1 = X1
    Linee(UBound(Linee)).Y1 = Y1
    Linee(UBound(Linee)).X2 = X2
    Linee(UBound(Linee)).Y2 = Y2
    Linee(UBound(Linee)).COLORE = COLORE
    If (NOME = "") Then NOME = CStr(UBound(Linee))
    Linee(UBound(Linee)).NOME = NOME
  End If
  '
End Sub

Public Sub AddSJoin(Serie1 As String, Serie2 As String, _
                    Optional COLORE As FiColors = fiblack, Optional NOME As String = "", Optional Legenda As String = "", Optional SCALA As Double = 0)
'
Dim Esistente As Boolean
Dim i         As Integer
Dim s1        As Integer
Dim s2        As Integer
  '
  s1 = NumSerie(Serie1)
  s2 = NumSerie(Serie2)
  If (s1 = 0) Or (s2 = 0) Then
    If (s1 = 0) Then
      StopRegieEvents
      MsgBox "Series " & Serie1 & " have not been found!!!", vbInformation, "Method AddSJoin"
      ResumeRegieEvents
    Else
      StopRegieEvents
      MsgBox "Series " & Serie2 & " have not been found!!!", vbInformation, "Method AddSJoin"
      ResumeRegieEvents
    End If
  Else
    For i = 1 To UBound(SJoins)
      Esistente = False
      If UCase(SJoins(i).NOME) = UCase(NOME) Then
        Esistente = True
        Exit For
      End If
    Next i
    If Esistente Then
      SJoins(i).X1 = Series(s1).X
      SJoins(i).Y1 = Series(s1).Y
      SJoins(i).X2 = Series(s2).X
      SJoins(i).Y2 = Series(s2).Y
      SJoins(i).COLORE = COLORE
      SJoins(i).SCALA = SCALA
      SJoins(i).Legenda = Legenda
    Else
      ReDim Preserve SJoins(UBound(SJoins) + 1)
      SJoins(UBound(SJoins)).X1 = Series(s1).X
      SJoins(UBound(SJoins)).Y1 = Series(s1).Y
      SJoins(UBound(SJoins)).X2 = Series(s2).X
      SJoins(UBound(SJoins)).Y2 = Series(s2).Y
      SJoins(UBound(SJoins)).COLORE = COLORE
      SJoins(UBound(SJoins)).SCALA = SCALA
      SJoins(UBound(SJoins)).Legenda = Legenda
      If (NOME = "") Then NOME = CStr(UBound(SJoins))
      SJoins(UBound(SJoins)).NOME = NOME
    End If
    If Graf Then Call Redraw
  End If
  '
End Sub

Public Sub AddRaccordo(L1 As Integer, L2 As Integer, R As Double)
'
Dim A1  As Double
Dim A2  As Double
Dim A   As Double
Dim X1  As Double
Dim Y1  As Double
Dim X2  As Double
Dim Y2  As Double
Dim X3  As Double
Dim Y3  As Double
Dim XP1 As Double
Dim YP1 As Double
Dim XP2 As Double
Dim YP2 As Double
Dim D   As Double
Dim dx  As Double
Dim Dy  As Double
Dim Q   As Integer
  '
  If (L1 = 0) And (L2 = 0) Then
    L1 = UBound(Linee) - 1
    L2 = UBound(Linee)
  End If
  If ((Linee(L1).X2 = Linee(L2).X1) And (Linee(L1).X2 = Linee(L2).X1)) Then
    X1 = Linee(L1).X1
    Y1 = Linee(L1).Y1
    X2 = Linee(L1).X2
    Y2 = Linee(L1).Y2
    X3 = Linee(L2).X2
    Y3 = Linee(L2).Y2
    Dim SP As Double
    Dim Sa As Double
    Dim Sb As Double
    Dim Sc As Double
    Sa = Sqr((X3 - X1) ^ 2 + (Y3 - Y1) ^ 2)
    Sb = Sqr((X2 - X1) ^ 2 + (Y2 - Y1) ^ 2)
    Sc = Sqr((X3 - X2) ^ 2 + (Y3 - Y2) ^ 2)
    SP = (Sa + Sb + Sc) / 2
    A = Sqr((SP - Sb) * (SP - Sc) / (SP * (SP - Sa)))
    D = Abs(R) / A
    Dim ttt As Double
    ttt = Sqr((X2 - X1) ^ 2 + (Y2 - Y1) ^ 2)
    XP1 = ((ttt - D) * ((X2 - X1) / ttt)) + X1
    YP1 = ((ttt - D) * ((Y2 - Y1) / ttt)) + Y1
    ttt = Sqr((X3 - X2) ^ 2 + (Y3 - Y2) ^ 2)
    XP2 = ((ttt - D) * ((X2 - X3) / ttt)) + X3
    YP2 = ((ttt - D) * ((Y2 - Y3) / ttt)) + Y3
    Linee(L1).X2 = XP1
    Linee(L1).Y2 = YP1
    Linee(L2).X1 = XP2
    Linee(L2).Y1 = YP2
    AddCerchio2PR XP1, YP1, XP2, YP2, R, fiyellow
  Else
    StopRegieEvents
    MsgBox "The End of the first line must be the Start of the second!!! ", vbInformation, "Method AddRaccordo"
    ResumeRegieEvents
  End If
  '
End Sub

Public Sub AddCerchio2PR(X1 As Double, Y1 As Double, _
                    X2 As Double, Y2 As Double, R As Double, _
                    Optional COLORE As FiColors = fiblack, Optional NOME As String = "")
Dim Xm As Double
Dim Ym As Double
Dim R1 As Double
Dim R2 As Double
Dim k1 As Double
Dim k2 As Double
Dim k3 As Double
Dim k4 As Double
Dim K5 As Double
Dim inv As Boolean
Dim Xc1 As Double
Dim Yc1 As Double
Dim Xc2 As Double
Dim Yc2 As Double
Dim A1 As Double
Dim A2 As Double
Dim Delta As Double
    
    inv = False
    If Y2 = Y1 Then
        k1 = X1
        k2 = X2
        X1 = Y1
        X2 = Y2
        Y1 = k1
        Y2 = k2
        'R = -R
        inv = True
    End If
    k1 = Abs((X2 - X1) ^ 2 - (Y2 - Y1) ^ 2)
    If Sqr(k1) > Abs(2 * R) Then
        Exit Sub
    End If
    Xm = X1 + (X2 - X1) / 2
    Ym = Y1 + (Y2 - Y1) / 2
        
    k1 = -(X2 - X1) / (Y2 - Y1)
    k2 = (X2 ^ 2 - X1 ^ 2 + Y2 ^ 2 - Y1 ^ 2) / (2 * (Y2 - Y1))
    
    R2 = Sqr((X1 - Xm) ^ 2 + (Y1 - Ym) ^ 2)
    R1 = Sqr(R ^ 2 - R2 ^ 2)
    
    k3 = 1 + k1 ^ 2
    k4 = -2 * Xm + 2 * k1 * k2 - 2 * Ym * k1
    K5 = k2 ^ 2 + Ym ^ 2 - 2 * Ym * k2 - R1 ^ 2 + Xm ^ 2
    
    Delta = k4 ^ 2 - 4 * k3 * K5
    
    If Delta > 0 Then
        Xc1 = Abs(R) * (Sqr(Delta) / (2 * k3)) - (k4 / (2 * k3))
    End If
    
    Yc1 = k1 * Xc1 + k2
     If inv Then
        k1 = X1
        k2 = X2
        X1 = Y1
        X2 = Y2
        Y1 = k1
        Y2 = k2
        k1 = Xc1
        Xc1 = Yc1
        Yc1 = k1
        'R = -R
    End If
    Call ANGOLI_CENTRO(X1, Y1, X2, Y2, Xc1, Yc1, R, A1, A2)
    ReDim Preserve Cerchi(UBound(Cerchi) + 1)
    Cerchi(UBound(Cerchi)).Xc = Xc1
    Cerchi(UBound(Cerchi)).YC = Yc1
    Cerchi(UBound(Cerchi)).R = R
    A1 = A1 * 180 / PG
    A2 = A2 * 180 / PG
    If R < 0 Then
        Cerchi(UBound(Cerchi)).A1 = A2
        Cerchi(UBound(Cerchi)).A2 = A1
    Else
        Cerchi(UBound(Cerchi)).A1 = A1
        Cerchi(UBound(Cerchi)).A2 = A2
    End If
    Cerchi(UBound(Cerchi)).COLORE = COLORE
    Cerchi(UBound(Cerchi)).NOME = NOME
    If Graf Then Call Redraw

End Sub

Public Sub AddCerchioCR(Xc As Double, YC As Double, R As Double, _
                    Optional COLORE As FiColors = fiblack, Optional NOME As String = "")
                    
  ReDim Preserve Cerchi(UBound(Cerchi) + 1)
  Cerchi(UBound(Cerchi)).Xc = Xc
  Cerchi(UBound(Cerchi)).YC = YC
  Cerchi(UBound(Cerchi)).R = R
  Cerchi(UBound(Cerchi)).A1 = 0
  Cerchi(UBound(Cerchi)).A2 = 360
  Cerchi(UBound(Cerchi)).COLORE = COLORE
  Cerchi(UBound(Cerchi)).NOME = NOME
  If Graf Then Call Redraw

End Sub

Public Sub AddMarker(NOME As String, TIPO As TipoMarker, Valore As Double)
    
  ReDim Preserve Markers(UBound(Markers) + 1)
  Markers(UBound(Markers)).NOME = NOME
  Markers(UBound(Markers)).TIPO = TIPO
  Markers(UBound(Markers)).Valore = Valore

End Sub

Public Sub AddSerieXY(X() As Double, Y() As Double, T() As Double, Optional COLORE As FiColors = fiblack, Optional MarcaPunti As Boolean = True, Optional Visibile As Boolean = True, Optional NOME As String = "", Optional Legenda As String = "")
'
Dim tX()      As Double
Dim TT()      As Double
Dim i         As Integer
Dim curser    As Integer
Dim Esistente As Boolean
  '
  If (UBound(X) <> UBound(Y)) Then
    StopRegieEvents
    MsgBox "Dimensions of array X and Y must be the same!!!", vbInformation, "Method AddSerieXY"
    ResumeRegieEvents
  Else
    For i = 1 To UBound(Series)
      Esistente = False
      If UCase(Series(i).NOME) = UCase(NOME) Then
        Esistente = True
        curser = i
        Exit For
      End If
    Next i
    ReDim TT(UBound(X))
    ReDim tX(UBound(X))
    TT = T
    If (ScalaAssi > 0) Then
      For i = 1 To UBound(X)
          tX(i) = DB(X(i))
      Next i
    Else
      tX = X
    End If
    If Esistente Then
      Series(curser).X = tX
      Series(curser).Y = Y
      Series(curser).T = TT
      Series(curser).NOME = NOME
      Series(curser).Visible = Visibile
      Series(curser).MarcaPunti = MarcaPunti
      Series(curser).COLORE = COLORE
      Series(curser).Legenda = Legenda
    Else
      ReDim Preserve Series(UBound(Series) + 1)
      Series(UBound(Series)).X = tX
      Series(UBound(Series)).Y = Y
      Series(UBound(Series)).T = TT
      If (NOME = "") Then NOME = CStr(UBound(Series))
      Series(UBound(Series)).NOME = NOME
      Series(UBound(Series)).Visible = Visibile
      Series(UBound(Series)).MarcaPunti = MarcaPunti
      Series(UBound(Series)).COLORE = COLORE
      Series(UBound(Series)).Legenda = Legenda
      If (NOME = "1") Then
        GX = tX
        GY = Y
        GT = TT
      End If
      Graf = True
      Call CalcolaParametri(Display)
      Call Disegna(Display)
      CursoreX.Visible = Graf
      CursoreY.Visible = Graf
      blnPan = True
      Evid.Visible = aggancia
      Call EvidenziaJoin(0, 0, Sc2X(GX(1)), Sc2X(GY(1)))
      Evid.Left = Sc2X(GX(1)) - Evid.Width / 2
      Evid.Top = Sc2Y(GY(1)) - Evid.Height / 2
      Call AggBottoni
    End If
  End If
  '
End Sub

Public Sub AddTesto(X As Double, Y As Double, Stringa As String, Optional NOME As String = "")
'
Dim Esistente As Boolean
Dim i         As Integer
  '
  For i = 1 To UBound(Testi)
    Esistente = False
    If UCase(Testi(i).NOME) = UCase(NOME) Then
      Esistente = True
      Exit For
    End If
  Next i
  If Esistente Then
    Testi(i).X = X
    Testi(i).Y = Y
    Testi(i).Valore = Stringa
  Else
    ReDim Preserve Testi(UBound(Testi) + 1)
    Testi(UBound(Testi)).X = X
    Testi(UBound(Testi)).Y = Y
    Testi(UBound(Testi)).Valore = Stringa
    If (NOME = "") Then NOME = CStr(UBound(Testi))
    Testi(UBound(Testi)).NOME = NOME
  End If
  If Graf Then Call Redraw
  '
End Sub

'****************************************************************************
'*
'* Funzioni Del... per rimozione entit�
'*
'* DelLinea ok
'* DelMarker da fare
'* DelRaccordo da fare
'* DelSerieRAB da fare
'* DelSerieXYT da fare
'* DelCerchioCR da fare
'* DelSJoin da fare
'* DelTesto ok
'*
'****************************************************************************

Public Sub DelLinea(NOME As String)
'
Dim Esistente As Boolean
Dim i         As Integer
Dim j         As Integer
  '
  For i = 1 To UBound(Linee)
    Esistente = False
    If UCase(Linee(i).NOME) = UCase(NOME) Then
      Esistente = True
      Exit For
    End If
  Next i
  If Esistente Then
    For j = i To UBound(Linee)
      Linee(j - 1) = Linee(j)
    Next j
    ReDim Preserve Linee(UBound(Linee) - 1)
  Else
    StopRegieEvents
    MsgBox "Line " & NOME & " have not been found!!!", vbInformation, "Method DelLinea"
    ResumeRegieEvents
  End If
  If Graf Then Call Redraw
  '
End Sub

Public Sub DelTesto(NOME As String)
'
Dim Esistente As Boolean
Dim i         As Integer
Dim j         As Integer
  '
  For i = 1 To UBound(Testi)
    Esistente = False
    If UCase(Testi(i).NOME) = UCase(NOME) Then
      Esistente = True
      Exit For
    End If
  Next i
  If Esistente Then
    For j = i To UBound(Testi)
      Testi(j - 1) = Testi(j)
    Next j
    ReDim Preserve Testi(UBound(Testi) - 1)
  Else
    StopRegieEvents
    MsgBox "Text " & NOME & " have not been found!!!", vbInformation, "Method DelTesto"
    ResumeRegieEvents
  End If
  If Graf Then Call Redraw
  '
End Sub

Private Sub SalvaDXF(DXF_BodyTextRef As String, savepath As String)
  
  BlockIndex = 0
  DXF_BodyText = DXF_BodyTextRef
  DXF_BlockBody = ""
  Call DXF_save_to_file(savepath)
  
End Sub

Private Sub MainOutput(ByVal StartTime As Variant, ByVal savepath As String)

Dim iDate As String
Dim iHr   As Integer
Dim iMIN  As Integer
Dim iSec  As Integer

  MyPoints.X = 50: MyPoints.Y = 50
  BlockIndex = 0
  DXF_BodyText = ""
  DXF_BlockBody = ""
  iLayer = "CLOCK_FACE"
  'Draw a border
  Call Bordo_DXF(DXF_BodyText, iLayer, 1, 1, 0, 99, 99, 0)
  'Draw a circle
  Call CerchioDXF(DXF_BodyText, iLayer, MyPoints.X, MyPoints.Y, 0, 48)
  'Draw four arcs on clock face
  Call ArcoDXF(DXF_BodyText, iLayer, MyPoints.X, MyPoints.Y, 0, 46, 2, 88)
  Call ArcoDXF(DXF_BodyText, iLayer, MyPoints.X, MyPoints.Y, 0, 46, 92, 178)
  Call ArcoDXF(DXF_BodyText, iLayer, MyPoints.X, MyPoints.Y, 0, 46, 182, 268)
  Call ArcoDXF(DXF_BodyText, iLayer, MyPoints.X, MyPoints.Y, 0, 46, 272, 358)
  'Draw a rectangle on the clock face
  Call RettangoloDXF(DXF_BodyText, iLayer, 50, 70, 0, 55, 80, 0)
  'Dimension the rectangle
  'I know for sure that this works in AutoCAD,
  'but I don't know about other CAD software
  Call DimensioneDXF(DXF_BodyText, iLayer, 55, 70, 55, 80, 60, 80, 60, 75, 90)
  Call DimensioneDXF(DXF_BodyText, iLayer, 50, 70, 55, 70, 55, 65, 52.5, 65)
  Call DimensioneDXF(DXF_BodyText, iLayer, 50, 70, 50, 80, 45, 80, 45, 75, 90, "Height")
  Call DimensioneDXF(DXF_BodyText, iLayer, 50, 80, 55, 80, 55, 85, 52.5, 85, , "Width")
  'Show text
  Call MostraTesto_DXF(DXF_BodyText, 50.5, 80, 315, 12, "This is a rectangle.")
  'Print the date
  iDate = Format(StartTime, "dddd, mmmm d yyyy")
  Call TestoDXF(DXF_BodyText, iLayer, MyPoints.X - Len(iDate), MyPoints.Y - 20, 0, 2, iDate)
  'Get the hours, minutes, seconds
  iHr = IIf(Hour(StartTime) < 12, Hour(StartTime) - 12, Hour(StartTime))
  iMIN = Minute(StartTime)
  iSec = Second(StartTime)
  'Draw the hour hand
  Call DXF_DrawHand((Radians(30 * iHr)) + ((Radians(6 * iMIN)) / 12), 20)
  'Draw the minute hand
  Call DXF_DrawHand((Radians(6 * iMIN)) + ((Radians(6 * iSec)) / 60), 30)
  'Draw the seconds hand
  Call DXF_DrawHand(Radians(6 * iSec), 45)
  'Save the DXF file
  Call DXF_save_to_file(savepath)
    
End Sub

Private Sub DXF_DrawHand(ByVal iRadians As Single, ByVal i As Integer)

Dim X(1) As Single
Dim Y(1) As Single

  'Finds the outer x & y locations of the clock hands
  X(0) = MyPoints.X + (i * (Sin(iRadians)))
  Y(0) = MyPoints.Y + (i * (Cos(iRadians)))
  'Finds the inner x & y locations of the clock hands
  X(1) = MyPoints.X - ((i * 0.25) * (Sin(iRadians)))
  Y(1) = MyPoints.Y - ((i * 0.25) * (Cos(iRadians)))
  'Draw the line with an arrow head
  Call LineaDXF(DXF_BodyText, iLayer, X(0), Y(0), 0, X(1), Y(1), 0)
  Call DXF_ArrowHead(DXF_BodyText, iRadians, X(0), Y(0))
    
End Sub

'Costruisce l'header, questo header � settato con le mie preferenze ma
'pu� essre modificato ricreando un file DXF dal CAD con preferenze diverse
Private Function DXF_Header() As String
Dim HS(19) As String

    HS(0) = "  0|SECTION|  2|HEADER|  9"
        HS(1) = "$ACADVER|  1|AC1009|  9"
        HS(2) = "$INSBASE| 10|0.0| 20|0.0| 30|0.0|  9"
        HS(3) = "$EXTMIN| 10|  0| 20|  0| 30|  0|  9"
        HS(4) = "$EXTMAX| 10|368| 20|326| 30|0.0|  9"
        HS(5) = "$LIMMIN| 10|0.0| 20|0.0|  9"
        HS(6) = "$LIMMAX| 10|100.0| 20|100.0|  9"
        HS(7) = "$ORTHOMODE| 70|     1|  9"
        HS(8) = "$DIMSCALE| 40|8.0|  9"
        HS(9) = "$DIMSTYLE|  2|STANDARD|  9"
        HS(10) = "$FILLETRAD| 40|0.0|  9"
        HS(11) = "$PSLTSCALE| 70|     1|  0"
    HS(12) = "ENDSEC|  0"
    HS(13) = "SECTION|  2|TABLES|  0"
        HS(14) = "TABLE|  2|VPORT| 70|     2|  0|VPORT|  2|*ACTIVE| 70|     0| 10|0.0| 20|0.0| 11|1.0| 21|1.0| 12|50.0| 22|50.0| 13|0.0| 23|0.0| 14|1.0| 24|1.0| 15|0.0| 25|0.0| 16|0.0| 26|0.0| 36|1.0| 17|0.0| 27|0.0| 37|0.0| 40|100.0| 41|1.55| 42|50.0| 43|0.0| 44|0.0| 50|0.0| 51|0.0| 71|     0| 72|   100| 73|     1| 74|     1| 75|     0| 76|     0| 77|     0| 78|     0|  0|ENDTAB|  0"
        HS(15) = "TABLE|  2|LTYPE| 70|     1|  0|LTYPE|  2|CONTINUOUS|  70|     0|  3|Solid Line| 72|    65| 73|     0| 40|0.0|  0|ENDTAB|  0"
        HS(16) = "TABLE|  2|LAYER| 70|     3|  0|LAYER|  2|0| 70|     0| 62|     7|  6|CONTINUOUS|  0|LAYER|  2|CLOCK_FACE| 70|     0| 62|     7|  6|CONTINUOUS|  0|LAYER|  2|DEFPOINTS| 70|     0| 62|     7| 6|CONTINUOUS|  0|ENDTAB|  0"
        HS(17) = "TABLE|  2|VIEW| 70|     0|  0|ENDTAB|  0"
        HS(18) = "TABLE|  2|DIMSTYLE| 70|     1|  0|DIMSTYLE|  2|STANDARD| 70|     0|  3||  4||  5||  6||  7|| 40|1.0| 41|0.18| 42|0.0625| 43|0.38| 44|0.18| 45|0.0| 46|0.0| 47|0.0| 48|0.0|140|0.18|141|0.09|142|0.0|143|25.4|144|1.0|145|0.0|146|1.0|147|0.09| 71|     0| 72|     0| 73|     1| 74|     1| 75|     0| 76|     0| 77|     0| 78|     0|170|     0|171|     2|172|     0|173|     0|174|     0|175|     0|176|     0|177|     0|178|     0|  0|ENDTAB|  0"
    HS(19) = "ENDSEC|  0|"
    DXF_Header = Join$(HS(), "|")

End Function

'The block header, body, and footer are used to append the
'header with any dimensional information added in the body.
Private Function DXF_BlockHeader() As String
  
  DXF_BlockHeader = "SECTION|  2|BLOCKS|  0|"

End Function

Private Sub DXF_BuildBlockBody()
  
  DXF_BlockBody = DXF_BlockBody & "BLOCK|  8|0|  2|*D" & BlockIndex & "|70|     1| 10|0.0| 20|0.0| 30|0.0|  3|*D" & BlockIndex & "|  1||0|ENDBLK|  8|0|0|"
  BlockIndex = BlockIndex + 1
  
End Sub

Private Function DXF_BlockFooter() As String
  
  DXF_BlockFooter = "ENDSEC|  0|"

End Function

'The body header, and footer will always remain the same
Private Function DXF_BodyHeader() As String
    
  DXF_BodyHeader = "SECTION|  2|ENTITIES|  0|"

End Function

Private Function DXF_BodyFooter() As String
    
  DXF_BodyFooter = "ENDSEC|  0|"

End Function

Private Function DXF_Footer() As String
  
  DXF_Footer = "EOF"

End Function

Private Sub DXF_save_to_file(ByVal savepath As String)

Dim varDXF
Dim intDXF        As Integer
Dim strDXF_Output As String

  'Costruisco una stringa full text
  strDXF_Output = DXF_Header & DXF_BlockHeader & DXF_BlockBody & DXF_BlockFooter & DXF_BodyHeader & DXF_BodyText & DXF_BodyFooter & DXF_Footer
  'split la stringa di testo con "|" e scrivo sul file
  varDXF = Split(strDXF_Output, "|")
  Open savepath For Output As #1
    For intDXF = 0 To UBound(varDXF)
      Print #1, varDXF(intDXF)
    Next
  Close #1
  
End Sub

'====================================================
'Tutta la geometria � aggiunta alla stringa: "DXF_BodyText"
'====================================================
Private Sub LineaDXF(ByRef DXF_BodyTextRef As String, ByVal iLayer As String, ByVal X1 As Double, ByVal Y1 As Double, ByVal Z1 As Double, ByVal X2 As Double, ByVal Y2 As Double, ByVal Z2 As Double)
    
  DXF_BodyTextRef = DXF_BodyTextRef & "LINE|8|" & iLayer & "| 10|" & X1 & "| 20|" & Y1 & "| 30|" & Z1 & "| 11|" & X2 & "| 21|" & Y2 & "| 31|" & Z2 & "|0|"
        
End Sub

Private Sub CerchioDXF(ByRef DXF_BodyTextRef As String, ByVal iLayer As String, ByVal X As Single, ByVal Y As Single, ByVal Z As Single, ByVal Radius As Single)
    
  DXF_BodyTextRef = DXF_BodyTextRef & "CIRCLE|8|" & iLayer & "| 10|" & X & "| 20|" & Y & "| 30|" & Z & "| 40|" & Radius & "| 39|  0|0|"
        
End Sub

Private Sub ArcoDXF(ByRef DXF_BodyTextRef As String, ByVal iLayer As String, ByVal X As Single, ByVal Y As Single, ByVal Z As Single, ByVal Radius As Single, ByVal StartAngle As Single, ByVal EndAngle As Single)
    
  '"|62|1|" Aggiunto dopo iLayer setta il colore (Rosso)
  DXF_BodyTextRef = DXF_BodyTextRef & "ARC|8|" & iLayer & "| 10|" & X & "| 20|" & Y & "| 30|" & Z & "| 40|" & Radius & "| 50|" & StartAngle & "| 51|" & EndAngle & "|0|"
        
End Sub

Private Sub TestoDXF(ByRef DXF_BodyTextRef As String, ByVal iLayer As String, ByVal X As Single, ByVal Y As Single, ByVal Z As Single, ByVal Height As Single, ByVal itext As String)

  DXF_BodyTextRef = DXF_BodyTextRef & "TEXT|8|" & iLayer & "| 10|" & X & "| 20|" & Y & "| 30|" & Z & "| 40|" & Height & "|1|" & itext & "| 50|  0" & "|7|" & "Courier New" & "|0|"

End Sub

Private Sub DimensioneDXF(ByRef DXF_BodyTextRef As String, ByVal iLayer As String, ByVal X1 As Single, ByVal Y1 As Single, ByVal X2 As Single, ByVal Y2 As Single, ByVal pX1 As Single, ByVal pY1 As Single, ByVal pX2 As Single, ByVal pY2 As Single, Optional ByVal iAng As Single = 0, Optional ByVal itext As String = "None")

'Funziona sicuramente in AutoCad.
Dim strDim(6) As String

  strDim(0) = "DIMENSION|  8|" & iLayer & "|  6|CONTINUOUS|  2|*D" & BlockIndex
  strDim(1) = " 10|" & pX1 & "| 20|" & pY1 & "| 30|0.0"
  strDim(2) = " 11|" & pX2 & "| 21|" & pY2 & "| 31|0.0"
  strDim(3) = IIf(itext = "None", " 70|     0", " 70|     0|  1|" & itext)
  strDim(4) = " 13|" & X1 & "| 23|" & Y1 & "| 33|0.0"
  strDim(5) = " 14|" & X2 & "| 24|" & Y2 & "| 34|0.0" & IIf(iAng = 0, "", "| 50|" & iAng)
  strDim(6) = "1001|ACAD|1000|DSTYLE|1002|{|1070|   287|1070|     3|1070|    40|1040|8.0|1070|   271|1070|     3|1070|   272|1070|     3|1070|   279|1070|     0|1002|}|  0|"
  DXF_BodyTextRef = DXF_BodyTextRef & Join$(strDim(), "|")
  'All dimensions need to be referenced in the header information
  Call DXF_BuildBlockBody
    
End Sub

Private Sub RettangoloDXF(ByRef DXF_BodyTextRef As String, ByVal iLayer As String, ByVal X1 As Single, ByVal Y1 As Single, ByVal Z1 As Single, ByVal X2 As Single, ByVal Y2 As Single, ByVal Z2 As Single)

Dim strRectangle(5) As String
    
  strRectangle(0) = "POLYLINE|  5|48|  8|" & iLayer & "|66|1| 10|0| 20|0| 30|0| 70|1|0"
  strRectangle(1) = "VERTEX|5|50|8|0| 10|" & X1 & "| 20|" & Y1 & "| 30|" & Z1 & "|  0"
  strRectangle(2) = "VERTEX|5|51|8|0| 10|" & X2 & "| 20|" & Y1 & "| 30|" & Z2 & "|  0"
  strRectangle(3) = "VERTEX|5|52|8|0| 10|" & X2 & "| 20|" & Y2 & "| 30|" & Z2 & "|  0"
  strRectangle(4) = "VERTEX|5|53|8|0| 10|" & X1 & "| 20|" & Y2 & "| 30|" & Z1 & "|  0"
  strRectangle(5) = "SEQEND|0|"
  DXF_BodyTextRef = DXF_BodyTextRef & Join$(strRectangle(), "|")

End Sub

Private Sub PolyLineDXF(ByRef DXF_BodyTextRef As String, ByVal iLayer As String, ByVal Npunti As Long, X1, Y1, Z1)
    
Dim i             As Integer
Dim strPolyLine() As String
    
  ReDim strPolyLine(Npunti + 1)
  'strPolyLine(0) = "POLYLINE|  5|48|  8|" & iLayer & "|66|1| 10|0| 20|0| 30|0| 70|8|0"
  strPolyLine(0) = "POLYLINE| 8|" & iLayer & "|66| 1| 10|0| 20|0| 30|0"
  For i = 1 To Npunti
      strPolyLine(i) = " 0|VERTEX| 8|0| 10|" & X1(i) & "| 20|" & Y1(i) & "| 30|" & Z1(i)
  Next
  'strPolyLine(2) = "VERTEX|5|51|8|0| 10|" & X2 & "| 20|" & Y1 & "| 30|" & Z2 & "|  0"
  'strPolyLine(3) = "VERTEX|5|52|8|0| 10|" & X2 & "| 20|" & Y2 & "| 30|" & Z2 & "|  0"
  'strPolyLine(4) = "VERTEX|5|53|8|0| 10|" & X1 & "| 20|" & Y2 & "| 30|" & Z1 & "|  0"
  strPolyLine(Npunti + 1) = " 0|SEQEND| 8|0|0|"
  DXF_BodyTextRef = DXF_BodyTextRef & Join$(strPolyLine, "|")

End Sub

Private Sub Bordo_DXF(ByRef DXF_BodyTextRef As String, ByVal iLayer As String, ByVal X1 As Single, ByVal Y1 As Single, ByVal Z1 As Single, ByVal X2 As Single, ByVal Y2 As Single, ByVal Z2 As Single)

Dim strBorder(5) As String
    
  strBorder(0) = "POLYLINE|  8|" & iLayer & "| 40|1| 41|1| 66|1| 70|1|0"
  strBorder(1) = "VERTEX|  8|" & iLayer & "| 10|" & X1 & "| 20|" & Y1 & "| 30|" & Z1 & "|  0"
  strBorder(2) = "VERTEX|  8|" & iLayer & "| 10|" & X2 & "| 20|" & Y1 & "| 30|" & Z2 & "|  0"
  strBorder(3) = "VERTEX|  8|" & iLayer & "| 10|" & X2 & "| 20|" & Y2 & "| 30|" & Z2 & "|  0"
  strBorder(4) = "VERTEX|  8|" & iLayer & "| 10|" & X1 & "| 20|" & Y2 & "| 30|" & Z1 & "|  0"
  strBorder(5) = "SEQEND|  0|"
  DXF_BodyTextRef = DXF_BodyTextRef & Join$(strBorder(), "|")

End Sub

Private Sub MostraTesto_DXF(ByRef DXF_BodyTextRef As String, ByVal X As Single, ByVal Y As Single, ByVal eAng As Single, ByVal eRad As Single, ByVal eText As Variant)

Dim eX        As Single
Dim eY        As Single
Dim iRadians  As Single

  iRadians = Radians(eAng)
  'Find the angle at which to draw the arrow head and leader
  eX = X - (eRad * (Cos(iRadians)))
  eY = Y - (eRad * (Sin(iRadians)))
  'Draw an arrow head
  Call DXF_ArrowHead(DXF_BodyText, iRadians + Radians(180), X, Y)
  'Draw the leader lines
  Call LineaDXF(DXF_BodyTextRef, iLayer, X, Y, 0, eX, eY, 0)
  Call LineaDXF(DXF_BodyTextRef, iLayer, eX, eY, 0, eX + 2, eY, 0)
  'Place the text
  Call TestoDXF(DXF_BodyTextRef, iLayer, eX + 2.5, eY - 0.75, 0, 1.5, eText)

End Sub

Private Sub DXF_ArrowHead(ByRef DXF_BodyTextRef As String, iRadians As Single, sngX As Single, sngY As Single)

Dim X(1) As Single
Dim Y(1) As Single

  'The number "3" is the length of the arrow head.
  'Adding or subtracting 170 degrees from the angle
  'gives us a 10 degree angle on the arrow head.
  'Finds the first side of the arrow head
  X(0) = sngX + (3 * (Sin(iRadians + Radians(170))))
  Y(0) = sngY + (3 * (Cos(iRadians + Radians(170))))
  'Finds the second side of the arrow head
  X(1) = sngX + (3 * (Sin(iRadians - Radians(170))))
  Y(1) = sngY + (3 * (Cos(iRadians - Radians(170))))
  'Draw the first side of the arrow head
  Call LineaDXF(DXF_BodyTextRef, iLayer, sngX, sngY, 0, X(0), Y(0), 0)  '/
  'Draw the second side of the arrow head
  Call LineaDXF(DXF_BodyTextRef, iLayer, sngX, sngY, 0, X(1), Y(1), 0)  '\
  'Draw the bottom side of the arrow head
  Call LineaDXF(DXF_BodyTextRef, iLayer, X(0), Y(0), 0, X(1), Y(1), 0)  '_

End Sub

Private Sub Display_KeyDown(KeyCode As Integer, Shift As Integer)

  If KeyCode = 27 Then
    If ((Toolbar1.Buttons("aggiungi").Value = tbrPressed) Or (Toolbar1.Buttons("rimuovi").Value = tbrPressed)) _
          And ((Toolbar1.Buttons("zoom").Value = tbrPressed) Or (Toolbar1.Buttons("pan").Value = tbrPressed)) Then
       Toolbar1.Buttons("zoom").Value = tbrUnpressed
       Toolbar1.Buttons("pan").Value = tbrUnpressed
    Else
       Toolbar1.Buttons("zoom").Value = tbrUnpressed
       Toolbar1.Buttons("pan").Value = tbrPressed
       Toolbar1.Buttons("misura").Value = tbrUnpressed
       Toolbar1.Buttons("aggiungi").Value = tbrUnpressed
       Toolbar1.Buttons("rimuovi").Value = tbrUnpressed
    End If
    blnMisura = Toolbar1.Buttons("misura").Value
    aggancia = Toolbar1.Buttons("aggancia").Value
    griglia = Toolbar1.Buttons("griglia").Value
    blnPan = Toolbar1.Buttons("pan").Value
    blnAggiungi = Toolbar1.Buttons("aggiungi").Value
    blnRimuovi = Toolbar1.Buttons("rimuovi").Value
    blnZoom = Toolbar1.Buttons("zoom").Value
    shpMisura.Visible = False
    Me.Redraw
  End If

End Sub

Private Sub Display_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)

  If (Button = 2) And (ScalaX > 0) Then
  
    CurrX = CDbl(X)
    CurrY = CDbl(Y)
    
    If User1 <> "" Then
        mnuUser1.Caption = User1
        mnuUser1.Visible = True
    End If
    If User2 <> "" Then
        mnuUser2.Caption = User2
        mnuUser2.Visible = True
    End If
    If User3 <> "" Then
        mnuUser3.Caption = User3
        mnuUser3.Visible = True
    End If
    If User4 <> "" Then
        mnuUser4.Caption = User4
        mnuUser4.Visible = True
    End If
    PopupMenu DxPop
  ElseIf (Button = 1) And (ScalaX > 0) Then
    If ((blnZoom = True) Or (blnAggiungi = True) Or (blnRimuovi = True)) And (blnPan = False) Then
        Selez.Visible = True
        Selez.Top = Y
        Selez.Left = X
        Selez.Height = 0
        Selez.Width = 0
        XRet1 = X
        YRet1 = Y
    End If
    If (Shift And vbShiftMask) > 0 Then
        tmpPan = True
    End If
    
    If blnMisura And shpMisura.Visible And Not tmpPan And Not blnZoom Then
        Call AggBottoni
        shpMisura.Visible = False
    End If
    If blnPan Or tmpPan Then
        XPan1 = SCx(X)
        YPan1 = SCy(Y)
    End If
    If blnMisura And Not tmpPan And Not blnZoom Then
        shpMisura.Visible = True
        If aggancia Then
            shpMisura.X1 = Sc2X(GX(P))
            shpMisura.Y1 = Sc2Y(GY(P))
            shpMisura.X2 = Sc2X(GX(P))
            shpMisura.Y2 = Sc2Y(GY(P))
            XLin1 = GX(P)
            YLin1 = GY(P)
        Else
            shpMisura.X1 = X
            shpMisura.Y1 = Y
            shpMisura.X2 = X
            shpMisura.Y2 = Y
            XLin1 = SCx(X)
            YLin1 = SCy(Y)
        End If
    End If
  End If

End Sub

Private Sub Display_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
'
Dim tmpSc1  As Double
Dim tmpSc2  As Double
Dim i       As Integer
Dim j       As Integer
Dim tAggRem As Boolean
  '
  If (Button = 1) And (Selez.Visible = True) Then
    Selez.Visible = False
    XRet2 = X
    YRet2 = Y
    If blnZoom And (ScalaY > 0) Then
      If (Abs(XRet2 - XRet1) > 0) And (Abs(YRet2 - YRet1) > 0) Then
        Xm = SCx(X - CDbl(XRet2 - XRet1) / 2)
        Ym = -(((Y - (YRet2 - YRet1) / 2) + SpostY) / ScalaY) + Ym
        tmpSc1 = Display.ScaleWidth / (Abs(XRet2 - XRet1))
        tmpSc2 = Display.ScaleHeight / (Abs(YRet2 - YRet1))
        If (tmpSc1 < tmpSc2) Then
          If (ScalaX * tmpSc1 < 100000) Then
            Call Zoom(tmpSc1)
          Else
            WRITE_DIALOG "Maximum value for ZOOM"
          End If
        Else
          If (ScalaX * tmpSc2 < 100000) Then
            Call Zoom(tmpSc2)
          Else
            WRITE_DIALOG "Maximum value for ZOOM"
          End If
        End If
      End If
    End If
    If (blnAggiungi Or blnRimuovi) And (blnPan = False) Then
      tAggRem = False
      For j = 1 To UBound(Series)
        For i = 1 To UBound(Series(j).X)
          If (Sc2X(Series(j).X(i)) > Selez.Left) _
          And (Sc2X(Series(j).X(i)) < Selez.Left + Selez.Width) _
          And (Sc2Y(Series(j).Y(i)) > Selez.Top) _
          And (Sc2Y(Series(j).Y(i)) < Selez.Top + Selez.Height) Then
            If blnAggiungi Then
              Call SerieNAgg(i, Series(j).NOME)
              tAggRem = True
            Else
              Call SerieNRem(i, Series(j).NOME)
              tAggRem = True
            End If
          End If
        Next i
      Next j
      Call AggBottoni
    End If
    Display.Cls
    Call Disegna(Display)
  End If
  If tmpPan Then tmpPan = False
  If Not (blnMisura Or blnAggiungi Or blnRimuovi) Then Call AggBottoni
  RaiseEvent SelChgByMouse
  tAggRem = False

End Sub

Private Sub EvidenziaJoin(Button As Integer, Shift As Integer, X As Single, Y As Single)

Dim i     As Integer
Dim j     As Integer
Dim EvI   As Integer
Dim EvJ   As Integer
Dim dx    As Single
Dim tmpDx As Single
Dim Xmed  As Single
Dim Ymed  As Single

  EvI = 0
  EvJ = 0
  dx = 10000
  shpJoinEv.Visible = aggancia
  If (UBound(SJoins) > 0) Then
    For j = 1 To UBound(SJoins)
      Xmed = (SJoins(j).X1(P) + SJoins(j).X2(P)) / 2
      Ymed = (SJoins(j).Y1(P) + SJoins(j).Y2(P)) / 2
      tmpDx = Sqr((Sc2X(Xmed) - X) ^ 2 + (Sc2Y(Ymed) - Y) ^ 2)
      If (tmpDx < dx) And (SJoins(j).X1(P) <> SJoins(j).X2(P)) And (SJoins(j).Y1(P) <> SJoins(j).Y2(P)) Then
        dx = tmpDx
        EvI = P
        EvJ = j
      End If
    Next j
    If (EvJ > 0) Then
      shpJoinEv.X1 = Sc2X(SJoins(EvJ).X1(EvI))
      shpJoinEv.X2 = Sc2X(SJoins(EvJ).X2(EvI))
      shpJoinEv.Y1 = Sc2Y(SJoins(EvJ).Y1(EvI))
      shpJoinEv.Y2 = Sc2Y(SJoins(EvJ).Y2(EvI))
      tmpDx = Sqr((SJoins(EvJ).X2(EvI) - SJoins(EvJ).X1(EvI)) ^ 2 + (SJoins(EvJ).Y2(EvI) - SJoins(EvJ).Y1(EvI)) ^ 2)
      If (SJoins(EvJ).SCALA <> 0) Then tmpDx = tmpDx / SJoins(EvJ).SCALA
    End If
  End If
  
End Sub

Private Sub Display_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
'
Dim tX  As Double
Dim tY  As Double
Dim Tp  As Integer
Dim D   As Double
Dim i   As Integer
Dim dx  As Double
Dim Dy  As Double
Dim da  As Double
Dim DR  As Double
  '
  If Graf Then
    Display.MousePointer = 2
    Evid.Visible = aggancia
    If shpMisura.Visible Then
      If aggancia Then
        shpMisura.X2 = Sc2X(GX(P)): shpMisura.Y2 = Sc2Y(GY(P))
        XLin2 = GX(P):              YLin2 = GY(P)
      Else
        shpMisura.X2 = X: shpMisura.Y2 = Y
        XLin2 = SCx(X):   YLin2 = SCy(Y)
      End If
      dx = XLin2 - XLin1: Dy = YLin2 - YLin1
      If (dx <> 0) Then
        da = Atn(Dy / dx) * 180 / PG
        da = -180 * (dx < 0) - 360 * ((Dy < 0) And (dx > 0)) + da
      End If
      DR = Sqr(dx ^ 2 + Dy ^ 2)
    End If
    If Selez.Visible Then
      XRet2 = X: YRet2 = Y
      If (XRet2 < XRet1) Then
        Selez.Left = XRet2
      Else
        Selez.Left = XRet1
      End If
      If (YRet2 > YRet1) Then
        Selez.Top = YRet1
      Else
        Selez.Top = YRet2
      End If
      Selez.Height = (Abs(YRet2 - YRet1))
      Selez.Width = (Abs(XRet2 - XRet1))
    End If
    Call VisInfo(Button, X, Y)
    CursoreX.Y1 = Y: CursoreX.Y2 = Y
    CursoreY.X1 = X: CursoreY.X2 = X
    If aggancia Then
      Tp = 0
      D = 10000
      For i = 1 To UBound(GX)
        tX = Abs((((X - SpostX) / ScalaX) + Xm) - GX(i))
        tY = Abs((((-Y - SpostY) / ScalaY) + Ym) - GY(i))
        If (D > Sqr(tX ^ 2 + tY ^ 2)) Then
          D = Sqr(tX ^ 2 + tY ^ 2)
          Tp = i
        End If
      Next i
      If (P <> Tp) Then
        P = Tp
        RaiseEvent Evidenziato
      End If
      Call EvidenziaJoin(Button, Shift, X, Y)
      Evid.Left = Sc2X(GX(P)) - Evid.Width / 2
      Evid.Top = Sc2Y(GY(P)) - Evid.Height / 2
    End If
    If (blnPan Or tmpPan) And (Button = 1) Then
      If (Abs(XPan1 - SCx(X)) > (1 / ScalaX)) Or (Abs(YPan1 - SCy(Y)) > (1 / ScalaY)) Then
        Xm = Xm + (XPan1 - SCx(X)): Ym = Ym + (YPan1 - SCy(Y))
      End If
      If shpMisura.Visible Then
        shpMisura.X1 = Sc2X(XLin1): shpMisura.Y1 = Sc2Y(YLin1)
      End If
      DoEvents
      Me.Redraw
    End If
  Else
    Display.MousePointer = 0
  End If

End Sub

Private Sub Display_Click()
  
  RaiseEvent Click
  
End Sub

Public Sub Esporta(file As String)

  Call SavePicture(Display.Image, file)
  
End Sub

Public Sub CopiaSuClipboard()

  Clipboard.Clear
  Clipboard.SetData Display.Image
  
End Sub

Private Function NumSerie(NOME As String)

Dim i As Integer
    
    NumSerie = 0
    For i = 1 To UBound(Series)
        If UCase(Series(i).NOME) = UCase(NOME) Then
            NumSerie = i
            Exit For
        End If
    Next i

End Function

Private Function frmt0(ByVal A As Double, ByVal B As Long) As String
'
Dim AA    As String
Dim sfmt  As String
Dim BB    As Long
'
On Error GoTo errfrmt0
  '
  If (B > 0) Then
    '
    sfmt = "#######0.0" + String(B - 1, "0")
    AA = Format$(A, sfmt)
    BB = InStr(AA, ",")
    If BB = 0 Then BB = InStr(AA, ".")
    frmt0 = Left$(AA, BB - 1) + "." + Mid$(AA, BB + 1)
    '
  Else
    '
    frmt0 = Format$(A, "#######0")
    '
  End If
  '
Exit Function

errfrmt0:
  i = Err
  StopRegieEvents
  MsgBox Error(Err), 48, "WARNING"
  ResumeRegieEvents
  frmt0 = Format$(A, sfmt)
  Exit Function
   
End Function

Private Sub barScalaErr_Change()
  
  ScalaErr = barScalaErr.Value
  Redraw
  
End Sub

Private Sub barScalaErr_Scroll()

  barScalaErr_Change
  
End Sub

Private Sub AggBottoni()
  '
  Toolbar1.Buttons("griglia").Value = Abs(griglia)
  Toolbar1.Buttons("aggancia").Value = Abs(aggancia)
  Toolbar1.Buttons("misura").Value = Abs(blnMisura)
  Toolbar1.Buttons("aggiungi").Value = Abs(blnAggiungi)
  Toolbar1.Buttons("rimuovi").Value = Abs(blnRimuovi)
  Toolbar1.Buttons("zoom").Value = Abs(blnZoom)
  Toolbar1.Buttons("pan").Value = Abs(blnPan)
  '
End Sub

Private Sub VisInfo(Button As Integer, X As Single, Y As Single)

Dim STATO     As String
Dim tX        As Double
Dim TESTO(6)  As String
  '
  Select Case g_chLanguageAbbr
    Case "IT"
      TESTO(0) = "NO"
      TESTO(1) = "SI"
      TESTO(2) = "Punto mola   : "
      TESTO(3) = "Raggio rotore: "
      TESTO(4) = "Punto rotore:"
      TESTO(5) = " raggio: "
      TESTO(6) = " eN: "
    Case "GR"
      TESTO(0) = "NEIN"
      TESTO(1) = "YA"
      TESTO(2) = "Scheibenpunkten: "
      TESTO(3) = "ROTOR radius   : "
      TESTO(4) = "ROTOR punkt: "
      TESTO(5) = " radius: "
      TESTO(6) = " eN: "
    Case Else
      TESTO(0) = "OFF"
      TESTO(1) = "ON"
      TESTO(2) = "WHEEL point : "
      TESTO(3) = "ROTOR radius: "
      TESTO(4) = "ROTOR point: "
      TESTO(5) = " radius: "
      TESTO(6) = " eN: "
  End Select
  '
  Select Case ScalaAssi
    
    Case 0
      Info = ""
      If aggancia Then
        If InN(Series(1).N, P) Then
          STATO = TESTO(0)
        Else
          STATO = TESTO(1)
        End If
        Select Case INIZIO_PUNTI
          Case -1
            If (P > 1) And (P < UBound(GX)) Then
              Info = Info & TESTO(2) & Left$(Format$(P + INIZIO_PUNTI, "##0") & String(3, " "), 3) & " " & STATO & Chr(13)
              If (TIPO_PROFILO = "AXIAL") Then
                Info = Info & Left$(TESTO(3) & frmt0(GY(P), 3) & String(22, " "), 22)
              Else
                Info = Info & Left$(TESTO(3) & frmt0(Sqr(GX(P) ^ 2 + GY(P) ^ 2), 3) & String(22, " "), 22)
              End If
              Info = Info & TESTO(6) & frmt0(errPNT(P), 4)
            Else
              Info = Info & TESTO(2) & Left$(Format$(P + INIZIO_PUNTI, "##0") & String(3, " "), 3) & " " & STATO
            End If
          Case Else
            Info = Info & TESTO(4) & Left$(Format$(P + INIZIO_PUNTI, "##0") & String(3, " "), 3)
            If (TIPO_PROFILO = "AXIAL") Then
              Info = Info & TESTO(5) & Left$(frmt0(GY(P), 3) & String(8, " "), 8) & STATO & Chr(13)
            Else
              Info = Info & TESTO(5) & Left$(frmt0(Sqr(GX(P) ^ 2 + GY(P) ^ 2), 3) & String(8, " "), 8) & STATO & Chr(13)
            End If
            Info = Info & " X: " & Left$(frmt0(GX(P), 3) & String(10, " "), 10) & String(8, " ")
            Info = Info & " Y: " & Left$(frmt0(GY(P), 3) & String(10, " "), 10)
        End Select
      End If
      If UBound(GN) > 0 Then
        Info = Info & " Sel: " & UBound(GN)
      End If
      If ScalaErr > 0 Then
        Info = Info & " Error Scale: " & ScalaErr
      End If
        
    Case 1
      tX = Lin(SCx(CDbl(X)))
      Info = ""
      If aggancia Then
      Info = Info & " I: " & P + INIZIO_PUNTI & " X:" & frmt0(Lin(GX(P)), 3) & " , Y:" & frmt0(GY(P), 3)
      End If
      If UBound(GN) > 0 Then
      Info = Info & " Sel: " & UBound(GN)
      End If
        
  End Select
  
End Sub

Function ANGOLO(X1 As Double, Y1 As Double, X2 As Double, Y2 As Double) As Double

Dim da As Double
Dim dx As Double
Dim Dy As Double
    
    dx = X2 - X1
    Dy = Y2 - Y1
    If (dx <> 0) Then
        da = Atn(Dy / dx)
        da = -PG * (dx < 0) - 2 * PG * ((Dy < 0) And (dx > 0)) + da
        ANGOLO = da
    Else
        ANGOLO = Sgn(Dy) * (PG / 2)
    End If

End Function

Private Sub AggColori()
  '
  Display.BackColor = col(0)
  CursoreX.BorderColor = col(2)
  CursoreY.BorderColor = col(2)
  Selez.BorderColor = col(3)
  '
End Sub

Public Property Get Evidenzia() As Integer
Attribute Evidenzia.VB_ProcData.VB_Invoke_Property = "PropertyPage1"
  '
  Evidenzia = P
  '
End Property

Public Sub EvidPtoSerie(Tp As Integer, Optional NOME As String = "1")
'
Dim i As Integer
  '
  i = NumSerie(NOME)
  If i > 0 Then
    P = Tp
    Evid.Left = ((Series(i).X(P) - Xm) * ScalaX) + SpostX - Evid.Width / 2
    Evid.Top = -(((Series(i).Y(P) - Ym) * ScalaY) + SpostY) - Evid.Height / 2
    Info = "I: " & P & " X:" & frmt0(Series(i).X(P), 3) & " , Y:" & frmt0(Series(i).Y(P), 3)
  End If
  '
End Sub

Public Property Let Evidenzia(Tp As Integer)
  '
  P = Tp
  Evid.Left = ((GX(P) - Xm) * ScalaX) + SpostX - Evid.Width / 2
  Evid.Top = -(((GY(P) - Ym) * ScalaY) + SpostY) - Evid.Height / 2
  '
End Property

Public Property Get N() As Variant
  '
  N = GN
  '
End Property

Public Property Let N(tN As Variant)
  '
  GN = tN
  '
End Property

Public Property Get Errori() As Variant
  '
  Errori = Gerr
  '
End Property

Public Property Let Errori(tX As Variant)
  '
  Gerr = tX
  '
End Property

Public Property Get ScaleWidth() As Double
  '
  ScaleWidth = Display.ScaleWidth
  '
End Property

Public Property Get ScaleHeight() As Double
  '
  ScaleHeight = Display.ScaleHeight
  '
End Property

Public Property Get Visible() As Boolean
Attribute Visible.VB_ProcData.VB_Invoke_Property = "Propriet�"
  '
  Visible = True
  '
End Property

Public Property Let Visible(vis As Boolean)
'
Dim AA As Object
  '
  'Display.ScaleHeight
  For Each AA In UserControl.ContainedControls
    AA.Visible = vis
  Next AA
  '
End Property

Public Property Get Legenda() As String
  '
  Legenda = lblLegenda
  '
End Property

Public Property Let Legenda(txt As String)
  '
  lblLegenda = txt
  If (txt = "") Then
    lblLegenda.Visible = False
  Else
    lblLegenda.Visible = True
  End If
  '
End Property

Public Property Get Grafica() As Boolean
Attribute Grafica.VB_ProcData.VB_Invoke_Property = "PropertyPage1"
  '
  Grafica = Graf
  '
End Property

Public Property Let Grafica(tgr As Boolean)
  '
  Graf = tgr
  If Graf Then
    Call CalcolaParametri(Display)
    Call Disegna(Display)
    CursoreX.Visible = Graf
    CursoreY.Visible = Graf
    blnPan = True
    Evid.Visible = aggancia
    shpJoinEv.Visible = aggancia
    If (UBound(Series) > 0) Then
        Evid.Left = Sc2X(Series(1).X(1)) - Evid.Width / 2
        Evid.Top = Sc2Y(Series(1).Y(1)) - Evid.Height / 2
    End If
  End If
  Call AggBottoni
  '
End Property

Public Sub Pulisci()
  '
  Display.Cls
  ScalaX = 0
  ReDim Series(0)
  ReDim Markers(0)
  ReDim Linee(0)
  ReDim Cerchi(0)
  ReDim Testi(0)
  ReDim SJoins(0)
  Graf = False
  picLeg.Visible = False
  '
End Sub

Public Sub Config_ToolBar(TB_Visibile As Boolean, TB_Zoom As Boolean, TB_Pan As Boolean, _
                          TB_Aggancia As Boolean, TB_Griglia As Boolean, TB_Misura As Boolean, _
                          TB_Aggiungi As Boolean, TB_Rimuovi As Boolean)
  '
  Toolbar1.Visible = TB_Visibile
  Toolbar1.Buttons("zoom").Visible = TB_Zoom
  Toolbar1.Buttons("pan").Visible = TB_Pan
  Toolbar1.Buttons("aggancia").Visible = TB_Aggancia
  Toolbar1.Buttons("griglia").Visible = TB_Griglia
  Toolbar1.Buttons("misura").Visible = TB_Misura
  Toolbar1.Buttons("aggiungi").Visible = TB_Aggiungi
  Toolbar1.Buttons("rimuovi").Visible = TB_Rimuovi
  '
End Sub

Private Sub mnuAbout_Click()
  '
  StopRegieEvents
  MsgBox ("Graf control object ver: " & GraficoVersion)
  ResumeRegieEvents
  '
End Sub

Private Sub mnuCentra_Click()
  '
  If aggancia Then
    Xm = GX(P)
    Ym = GY(P)
  Else
  End If
  Display.Cls
  Call Disegna(Display)
  '
End Sub

Private Sub mnuDXF_Click()
'
Dim DXF_BodyText  As String
Dim tmpstr        As String
Dim Z()           As Double
Dim i             As Integer
Dim j             As Integer
'
On Error GoTo ERRORE
  '
  tmpstr = "Test"
  '
  Conf1.InitDir = ExportPath
  Conf1.DefaultExt = "dxf"
  Conf1.Filter = "File DXF (*.dxf)|*.dxf"
  Conf1.FilterIndex = 1
  Conf1.FLAGS = &H2
  Conf1.CancelError = True
  Conf1.ShowSave
  If (Len(Conf1.FileName) > 0) Then
    tmpstr = Conf1.FileName
    If (tmpstr <> "") Then
      If (UBound(Series) > 0) Then
        For i = 1 To UBound(Series)
          ReDim Z(UBound(Series(i).X))
          Call PolyLineDXF(DXF_BodyText, "0", UBound(Series(i).X), Series(i).X, Series(i).Y, Z)
        Next i
      End If
      If (UBound(SJoins) > 0) Then
        For j = 1 To UBound(SJoins)
          For i = 1 To UBound(SJoins(j).X1)
            Call LineaDXF(DXF_BodyText, "0", SJoins(j).X1(i), SJoins(j).Y1(i), 0, SJoins(j).X2(i), SJoins(j).Y2(i), 0)
          Next i
        Next j
      End If
      Call SalvaDXF(DXF_BodyText, tmpstr)
    End If
  End If
  '
Exit Sub

ERRORE:
  If (Err.Number <> 32755) Then
    i = Err
    StopRegieEvents
    MsgBox Error(i), vbCritical, "SUB: mnuDXF"
    ResumeRegieEvents
  Else
    Exit Sub
  End If

End Sub

Private Sub mnuGriglia_Click()
'
Dim tmpstr As String
  '
  StopRegieEvents
  tmpstr = InputBox("Grid dimension:", "Griglia", StepX)
  ResumeRegieEvents
  If (tmpstr <> "") Then
    StepX = val(tmpstr)
    StepY = StepX
    griglia = True
    Call AggBottoni
    Display.Cls
    Call Disegna(Display)
  End If
  '
End Sub

Private Sub mnuLegenda_Click()
  '
  mnuLegenda.Checked = Not mnuLegenda.Checked
  picLeg.Visible = mnuLegenda.Checked
  If picLeg.Visible Then Call Aggiorna_Leg
  '
End Sub

Private Sub mnuUser1_Click()
  '
  Select Case ScalaAssi
    Case 0: RaiseEvent UserMenu(1, SCx(CurrX), SCy(-CurrY), 0, 0)
    Case 1: RaiseEvent UserMenu(1, Lin(SCx(CurrX)), SCy(-CurrY), 0, 0)
  End Select
  '
End Sub

Private Sub mnuUser2_Click()
  '
  Select Case ScalaAssi
    Case 0:  RaiseEvent UserMenu(2, SCx(CurrX), SCy(-CurrY), 0, 0)
    Case 1:  RaiseEvent UserMenu(2, Lin(SCx(CurrX)), SCy(-CurrY), 0, 0)
  End Select
  '
End Sub

Private Sub mnuUser3_Click()
  '
  Select Case ScalaAssi
    Case 0: RaiseEvent UserMenu(3, SCx(CurrX), SCy(-CurrY), 0, 0)
    Case 1: RaiseEvent UserMenu(3, Lin(SCx(CurrX)), SCy(-CurrY), 0, 0)
  End Select
  '
End Sub

Private Sub mnuUser4_Click()
  '
  Select Case ScalaAssi
    Case 0: RaiseEvent UserMenu(4, SCx(CurrX), SCy(-CurrY), 0, 0)
    Case 1: RaiseEvent UserMenu(4, Lin(SCx(CurrX)), SCy(-CurrY), 0, 0)
  End Select
  '
End Sub

Private Sub mnuXYT_Click()
'
Dim i       As Integer
Dim tmpstr  As String
'
On Error Resume Next
  '
  tmpstr = "XY.TXT"
  StopRegieEvents
  tmpstr = InputBox("Export XY", "File name?", App.Path & "\" & tmpstr)
  ResumeRegieEvents
  If (tmpstr <> "") Then
    ReDim Preserve GT(UBound(GX))
    ReDim Preserve GY(UBound(GX))
    Open tmpstr For Output As 1
    For i = 1 To UBound(GX)
    'Print #1, Right("         " & frmt0(GX(i), 4), 9) & " ," & Right("         " & frmt0(GY(i), 4), 9) & " ," & Right("         " & frmt0(GT(i), 4), 9)
    Print #1, Right("         " & frmt0(GX(i), 4), 9) & " ," & Right("         " & frmt0(GY(i), 4), 9)
    Next i
    Close #1
  End If
  '
End Sub

Private Sub mnuZoom_Click()
  '
  Call Zoom(0)
  '
End Sub

Private Sub Zoom(XFactor As Double, Optional YFactor As Double = 0)
'
Dim tsc As Double
  '
  If XFactor <> 0 Then
    If YFactor = 0 Then YFactor = XFactor
    tsc = ScalaX * XFactor
    If (tsc > 0.01) And (tsc < 100000) Then ScalaX = tsc
    tsc = ScalaY * YFactor
    If (tsc > 0.01) And (tsc < 100000) Then ScalaY = tsc
  Else
    Call CalcolaParametri(Display)
  End If
  Display.Cls
  Call Disegna(Display)
  If shpMisura.Visible Then
    shpMisura.X1 = Sc2X(XLin1)
    shpMisura.Y1 = Sc2Y(YLin1)
  End If
  '
End Sub

Private Sub mnuZoomF_Click()
'
Dim tmpstr As String
  '
  StopRegieEvents
  tmpstr = InputBox("Input the zoom factor", "Zoom", 0.5)
  ResumeRegieEvents
  If (tmpstr <> "") Then Call Zoom(val(tmpstr))
  '
End Sub

Private Sub mnuZoomIn_Click()
  '
  Call Zoom(2)
  '
End Sub

Private Sub mnuZoomOut_Click()
  '
  If (ScalaX > 0.01) Then Call Zoom(0.5)
  '
End Sub

Public Sub Colori()
  '
  col(0) = fiDarkGray     'Sfondo
  col(1) = fiGray         'Griglia
  col(2) = fiLightGray    'CursoreX.BorderColor '2 ColCursore
  col(3) = fiLightGreen   'Zoom
  col(4) = fiCyan         '4 ColEvidenziatore
  col(5) = vbBlack        'Serie1
  col(6) = vbBlack        'Punti1
  col(7) = vbYellow       'Punti evidenziati
  col(8) = vbRed          'Linea +3dB
  col(9) = vbBlack 'fiLightGreen   'Scritte
  col(10) = vbBlue        'Col Serie2
  Call AggColori
  '
End Sub

Public Sub Colori_Stampa()
  '
  col(0) = vbWhite 'Sfondo
  col(1) = 11184810 'Griglia
  col(2) = CursoreX.BorderColor '2 ColCursore
  col(3) = 65280 'Zoom
  col(4) = 16776960 '4 ColEvidenziatore
  col(5) = vbBlack 'Serie1
  col(6) = 59624 'Punti1
  col(7) = 33023
  col(8) = vbRed 'Linea +3dB
  col(9) = fiAliceBlue 'Scritte
  col(10) = vbBlue 'Col Serie2
  Call AggColori
  '
End Sub

Private Sub Timer1_Timer()
'
Dim tmpsize
  '
  tmpsize = UserControl.Height - Display.Top
  If (tmpsize > 0) Then Display.Height = tmpsize
  tmpsize = UserControl.Width - 10
  If (tmpsize > 0) Then Display.Width = tmpsize
  If (barScalaErr.Visible) Then
    tmpsize = Display.Width - barScalaErr.Width
    If (tmpsize > 0) Then Display.Width = tmpsize
    barScalaErr.Left = Display.Width + Display.Left
    barScalaErr.Height = Display.Height
    barScalaErr.Top = Display.Top
  End If
  CursoreY.Y1 = Display.ScaleTop + Display.ScaleHeight
  CursoreY.Y2 = Display.ScaleTop
  CursoreX.X1 = Display.ScaleLeft + Display.ScaleWidth
  CursoreX.X2 = Display.ScaleLeft
  Info.Top = UserControl.ScaleTop
  tmpsize = UserControl.Width - Info.Left
  If (tmpsize > 0) Then Info.Width = tmpsize
  If (Graf) Then
    Call CalcolaParametri(Display)
    Call Redraw
  End If
  Timer1.Enabled = False
  '
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
  '
  WRITE_DIALOG ""
  Select Case Button.Key
    Case "pan"
      Toolbar1.Buttons("aggiungi").Value = tbrUnpressed
      Toolbar1.Buttons("rimuovi").Value = tbrUnpressed
    Case "zoom"
      Toolbar1.Buttons("pan").Value = tbrUnpressed
      Toolbar1.Buttons("aggiungi").Value = tbrUnpressed
      Toolbar1.Buttons("rimuovi").Value = tbrUnpressed
    Case "aggancia"
      If Graf Then
        Evid.Visible = Button.Value
        shpJoinEv.Visible = Button.Value
        If UBound(Series) > 0 Then
          Evid.Left = Sc2X(Series(1).X(1)) - Evid.Width / 2
          Evid.Top = Sc2Y(Series(1).Y(1)) - Evid.Height / 2
        End If
      End If
    Case "misura"
      Toolbar1.Buttons("pan").Value = tbrUnpressed
      Toolbar1.Buttons("zoom").Value = tbrUnpressed
    Case "aggiungi"
      Toolbar1.Buttons("pan").Value = tbrUnpressed
      Toolbar1.Buttons("zoom").Value = tbrUnpressed
    Case "rimuovi"
      Toolbar1.Buttons("pan").Value = tbrUnpressed
      Toolbar1.Buttons("zoom").Value = tbrUnpressed
  End Select
  blnMisura = Toolbar1.Buttons("misura").Value
  aggancia = Toolbar1.Buttons("aggancia").Value
  griglia = Toolbar1.Buttons("griglia").Value
  blnPan = Toolbar1.Buttons("pan").Value
  blnAggiungi = Toolbar1.Buttons("aggiungi").Value
  blnRimuovi = Toolbar1.Buttons("rimuovi").Value
  blnZoom = Toolbar1.Buttons("zoom").Value
  Me.Redraw
  '
End Sub

Private Sub UserControl_Initialize()
  '
  FontName = Display.FontName
  FontSize = Display.FontSize
  Visualizza1 = True
  Visualizza2 = True
  Call Colori
  ExportPath = App.Path
  'MZoom = True
  'MAgg = False
  'MRem = False
  ' ********** COLORI **********
  '0 ColSfondo
  '1 ColGriglia
  '2 ColCursore
  '3 ColZoom
  '4 ColEvidenziatore
  '5 ColSerie1
  '6 ColPunti1
  '8 Linea +3dB
  '9 Scritte
  '10 Col Serie2
  'col(0) = vbWhite 'Sfondo
  'col(1) = 11184810 'Griglia
  'col(2) = CursoreX.BorderColor '2 ColCursore
  'col(3) = 65280 'Zoom
  'col(4) = 16776960 '4 ColEvidenziatore
  'col(5) = vbBlack 'Serie1
  'col(6) = 59624 'Punti1
  'col(7) = 33023
  'col(8) = vbRed 'Linea +3dB
  'col(9) = fiAliceBlue 'Scritte
  'col(10) = vbBlue 'Col Serie2
  'Label1.ForeColor = col(9)
  'Label2.ForeColor = col(9)
  'Selez.BorderColor = col(3)
  'Evid.BorderColor = col(4)
  Graf = False
  aggancia = False
  griglia = False
  '
  ReDim Markers(0)
  ReDim Series(0)
  ReDim Linee(0)
  ReDim Cerchi(0)
  ReDim Testi(0)
  ReDim SJoins(0)
  ReDim GX(0)
  ReDim GY(0)
  ReDim GX2(0)
  ReDim GY2(0)
  ReDim GT(0)
  ReDim GN(0)
  ReDim GR(0)
  ReDim GA(0)
  ReDim GB(0)
  '
  PG = Atn(1) * 4
  TolPos = 0
  TolNeg = 0
  picLeg.BackColor = Display.BackColor
  '
End Sub

Private Sub UserControl_Resize()
  '
  Timer1.Enabled = True
  '
End Sub

Public Sub Redraw()
  '
  Display.Cls
  Call Disegna(Display)
  '
End Sub

Private Sub DisInfo(disp As Object)
'
Dim tX    As Double
Dim tY    As Double
Dim str1  As String
Dim str2  As String
Dim lt1   As Double
Dim lt2   As Double
Dim ht    As Double
Dim lt    As Double
  '
  str1 = StepX
  str2 = "x " & frmt0(ScalaX, 2)
  '
  lt1 = disp.TextWidth(str1)
  lt2 = disp.TextWidth(str2)
  ht = disp.TextHeight("0.0")
  If (lt1 > lt2) Then
    lt = lt1
  Else
    lt = lt2
  End If
  tX = disp.ScaleWidth - 2
  tY = 2
  If griglia Then
    Call linea(SCx(tX - 1.5 * lt), SCy(tY + ht), SCx(tX), SCy(tY + ht), vbWhite, disp)
    Call linea(SCx(tX - 1.5 * lt), SCy(tY + ht - 0.5), SCx(tX - 1.5 * lt), SCy(tY + ht + 1), vbWhite, disp)
    Call linea(SCx(tX), SCy(tY + ht - 0.5), SCx(tX), SCy(tY + ht + 1), vbWhite, disp)
    disp.CurrentX = tX - 0.75 * lt - lt1 / 2
    disp.CurrentY = tY - 0.5
    disp.Print str1
  End If
  disp.CurrentX = tX - 0.75 * lt - lt2 / 2
  disp.CurrentY = tY + ht + 0.5
  disp.Print str2
  '
End Sub

Private Sub DisGriglia(disp As Object)
'
Dim i         As Integer
Dim NX        As Integer
Dim NY        As Integer
Dim tX        As Double
Dim autogrid  As Boolean
  '
  griglia = True
  Call AggBottoni
  If (StepX = 0) Then StepX = 1
  If (StepY = 0) Then StepY = 1
  Select Case ScalaAssi
    Case 0 'Lineare
        disp.ForeColor = col(9)
        disp.Font = "Arial"
        autogrid = True
        If autogrid = True Then
          StepY = 0.5
          NY = (disp.ScaleHeight / (ScalaY * StepY))
          While NY > 10
            StepY = StepY + 0.5
            NY = (disp.ScaleHeight / (ScalaY * StepY))
          Wend
          If NY < 5 Then StepY = 0.1
          StepX = StepY
        End If
        tX = 0
        While tX > SCx(0)
          tX = tX - StepX
        Wend
        While tX < SCx(0)
          tX = tX + StepX
        Wend
        While tX < SCx(disp.ScaleWidth)
          If tX = 0 Then
            disp.DrawWidth = 2
          Else
            disp.DrawWidth = 1
          End If
          If tX > SCx(disp.TextWidth("00000")) Then
            Call linea(tX, SCy(0), tX, SCy(disp.ScaleHeight - disp.TextHeight("0")), col(1), disp)
            disp.CurrentX = Sc2X(tX) - disp.TextWidth(frmt0(tX, 1)) / 2
            disp.CurrentY = disp.ScaleHeight - disp.TextHeight("0.0")
            disp.Print frmt0(tX, 1)
          End If
          tX = tX + StepX
        Wend
        tX = 0
        While tX > SCy(disp.ScaleHeight)
          tX = tX - StepY
        Wend
        While tX < SCy(disp.ScaleHeight - disp.TextHeight("0"))
          tX = tX + StepY
        Wend
        While tX < SCy(0)
          If tX = 0 Then
            disp.DrawWidth = 2
          Else
            disp.DrawWidth = 1
          End If
          If tX < SCy(-disp.ScaleHeight + 2 * disp.TextHeight("0.0")) Then
            disp.CurrentX = disp.TextWidth("000.0") - disp.TextWidth(frmt0(tX, 1))
            disp.CurrentY = Sc2Y(tX) - disp.TextHeight("0.0") / 2
            disp.Print frmt0(tX, 1)
            Call linea(SCx(disp.TextWidth("00000")), tX, SCx(disp.ScaleWidth), tX, col(1), disp)
          End If
          tX = tX + StepY
        Wend
        disp.DrawWidth = 1
    Case 1 'Semilogaritmica
        Call GridSL(disp)
    Case 2 'Logaritmica
  End Select
  
End Sub

Private Sub GridSL(disp As Object)
'
Dim freq  As Double
Dim i     As Integer
Dim j     As Integer
Dim dbg   As Double
  '
  freq = 0
  i = 0
  j = 0
  disp.ForeColor = col(9)
  disp.Font = "Arial"
  While (10 ^ j > Lin(SCx(0)))
    j = j - 1
  Wend
  disp.DrawStyle = 2
  While freq < Int(Lin(SCx(disp.ScaleWidth)))
    If freq > Lin(SCx(disp.ScaleLeft + disp.TextWidth("00000"))) Then
      If i = 1 Then
        disp.CurrentX = Sc2X(DB(freq)) - disp.TextWidth("10^" & j) / 2
        disp.CurrentY = disp.ScaleHeight - disp.TextHeight("0.0")
        disp.Print "10^" & j
      End If
      Call linea(DB(freq), SCy(0), DB(freq), SCy(disp.ScaleHeight - disp.TextHeight("0")), col(1), disp)
    End If
    i = i + 1
    If i = 11 Then
      j = j + 1
      i = 1
      'Testo db(i * 10 ^ j), Disp.ScaleHeight - 5, "10", vbGreen, Disp
      'Testo db(i * 10 ^ j) + 4, Disp.ScaleHeight - 6, CStr(j), vbGreen, Disp
    End If
    freq = i * 10 ^ j
  Wend
  Call linea(SCx(disp.ScaleLeft + disp.TextWidth("00000")), 3, SCx(disp.ScaleWidth), 3, col(8), disp)
  Call linea(SCx(disp.ScaleLeft + disp.TextWidth("00000")), -3, SCx(disp.ScaleWidth), -3, col(8), disp)
  '
  Dim MinDB As Double
  Dim MaxDB As Double
  MinDB = ((-(disp.ScaleHeight - disp.ScaleHeight - SpostY)) / ScalaY) + Ym
  MaxDB = ((-(-disp.ScaleHeight - SpostY)) / ScalaY) + Ym
  MinDB = (CInt(MinDB / 10)) * 10
  MaxDB = (CInt(MaxDB / 10)) * 10
  dbg = MinDB
  While (dbg < MaxDB)
    Call linea(SCx(disp.ScaleLeft + disp.TextWidth("00000")), dbg, SCx(disp.ScaleWidth), dbg, col(1), disp)
    disp.CurrentX = disp.TextWidth("000.0") - disp.TextWidth(frmt0(dbg, 1))
    disp.CurrentY = Sc2Y(dbg) - disp.TextHeight("0.0") / 2
    disp.Print frmt0(dbg, 1)
    dbg = dbg + 10
  Wend
  disp.DrawStyle = 0
  '
End Sub

Private Function DB(Lin As Double) As Double
  '
  If (Lin > 0) Then DB = 20 * Log10(Lin)
  '
End Function

Private Function Lin(logar As Double) As Double
  '
  Lin = 10 ^ (logar / 20)
  
End Function

Private Function Log10(X As Double) As Double
  '
  Log10 = Log(X) / Log(10)
  '
End Function

Private Sub CalcolaParametri(disp As Object)
'
Dim i As Integer
Dim j As Integer
  '
  If Graf Then
    For j = 1 To UBound(Series)
      For i = 1 To UBound(Series(j).X)
        If (i = 1) And (j = 1) Then
          MinX = Series(j).X(i)
          MinY = Series(j).Y(i)
          MaxX = Series(j).X(i)
          MaxY = Series(j).Y(i)
        End If
        ' Nella determinazione del massimo e del minimo non si tengono in considerazione punti
        ' 100 volte pi� grandi in modulo del valore massimo determinato fino al punto precedente
        If (Series(j).X(i) < MinX) And (Abs(Series(j).X(i)) < Abs(1000 + MaxX)) Then MinX = Series(j).X(i)
        If (Series(j).X(i) > MaxX) And (Abs(Series(j).X(i)) < Abs(1000 + MaxX)) Then MaxX = Series(j).X(i)
        If (Series(j).Y(i) < MinY) And (Abs(Series(j).Y(i)) < Abs(1000 + MaxY)) Then MinY = Series(j).Y(i)
        If (Series(j).Y(i) > MaxY) And (Abs(Series(j).Y(i)) < Abs(1000 + MaxY)) Then MaxY = Series(j).Y(i)
      Next i
      SpostX = disp.ScaleWidth / 2
      SpostY = -disp.ScaleHeight / 2
      Xm = MinX + (MaxX - MinX) / 2
      Ym = MinY + (MaxY - MinY) / 2
      ScalaX = disp.ScaleWidth / (MaxX - MinX)
      ScalaY = disp.ScaleHeight / (MaxY - MinY)
      If (ScalaAssi = 0) Then
        If (ScalaX > ScalaY) Then
          ScalaX = ScalaY
        Else
          ScalaY = ScalaX
        End If
      End If
      ScalaX = ScalaX * 0.9
      ScalaY = ScalaY * 0.9
    Next j
  End If
  '
End Sub

Private Sub Disegna(disp As Object)
  '
  Call DisegnaLIN(disp)
  '
End Sub

Private Sub Arco(tC As Cerchio, disp As Object)
'
Dim tA1 As Double
Dim tA2 As Double
Dim da  As Double
Dim i   As Integer
  '
  'CALCOLO_RAGGIO_TERNA_MOLA_JPG PInX, PInY, PMeX, PMeY, PFiX, PFiY, XC, YC, R0
  'ANGOLI_CENTRO PInX, PInY, PMeX, PMeY, XC, YC, R0, A1, A2
  'If EXT Then
  '    A2 = A2 + 0.2
  '    A1 = A1 + 0.2
  'End If
  PG = Atn(1) * 4
  tA1 = tC.A1 * PG / 180
  tA2 = tC.A2 * PG / 180
  da = (tA2 - tA1) / 100
  Select Case tC.Arco
    Case 0
      da = (tA2 - tA1) / 100
      'DA = 2 * PG - DA
      For i = 1 To 100
        Call linea(tC.Xc + ((tC.R) * Cos(tA1 + da * (i - 1))), tC.YC + ((tC.R) * Sin(tA1 + da * (i - 1))), tC.Xc + ((tC.R) * Cos(tA1 + da * i)), tC.YC + ((tC.R) * Sin(tA1 + da * i)), tC.COLORE, disp)
      Next i
    Case 1
      For i = 1 To 100
        Call linea(tC.Xc + (Abs(tC.R) * Cos(tA1 + da * (i - 1))), tC.YC + (Abs(tC.R) * Sin(tA1 + da * (i - 1))), tC.Xc + (Abs(tC.R) * Cos(tA1 + da * i)), tC.YC + (Abs(tC.R) * Sin(tA1 + da * i)), tC.COLORE, disp)
      Next i
  End Select
  'CALL ANGOLI_CENTRO(PMeX, PMeY, PFiX, PFiY, XC, YC, R0, A1, A2)
  'DA = (A2 - A1) / 100
  'For i = 1 To 100
  '    Linea XC + (Abs(R0) * Cos(A1 + DA * (i - 1))), YC + (Abs(R0) * Sin(A1 + DA * (i - 1))), XC + (Abs(R0) * Cos(A1 + DA * i)), YC + (Abs(R0) * Sin(A1 + DA * i)), Colore
  'Next i
  '
End Sub

Private Sub Aggiorna_Leg()
'
Dim i     As Integer
Dim RIGHE As Integer
Dim ttxt  As String
  '
  If mnuLegenda.Checked Then
    RIGHE = 0
    picLeg.Cls
    picLeg.Font = "Arial"
    picLeg.FontBold = True
    picLeg.FontSize = 8
    picLeg.CurrentX = 0
    picLeg.CurrentY = picLeg.TextHeight("A") * 0.2
    For i = 1 To UBound(Series)
      If Series(i).Legenda <> "" Then
        picLeg.Visible = True
        RIGHE = RIGHE + 1
        ttxt = "  --- " & Series(i).Legenda & "  "
        If picLeg.TextWidth(ttxt) > picLeg.Width Then picLeg.Width = picLeg.TextWidth(ttxt)
        picLeg.Height = picLeg.TextHeight("A") * RIGHE + picLeg.TextHeight("A") * 0.4
        picLeg.ForeColor = Series(i).COLORE
        picLeg.Print ttxt
      End If
    Next i
  End If
  '
End Sub

Private Sub DisegnaLIN(disp As Object)
'
Dim i   As Integer
Dim j   As Integer
Dim R   As Double
Dim co  As Long
Dim X1  As Double
Dim X2  As Double
Dim Y1  As Double
Dim Y2  As Double
Dim tfn As String
Dim tfs As Double
  '
  Call AggColori
  Call Aggiorna_Leg
  '
  If ScalaX = 0 Then Call CalcolaParametri(disp)
  If griglia Then Call DisGriglia(disp)
  '
  R = 0.2
  '
  Call DisInfo(disp)
  '
  For i = 1 To UBound(Markers)
    Select Case Markers(i).TIPO
      Case Radiale:    Call Cerchio(0, 0, Markers(i).Valore * ScalaX, col(3), disp, vbFSTransparent)
      Case Orizzontale
      Case Verticale
    End Select
  Next i
  If (UBound(Linee) > 0) Then
    For j = 1 To UBound(Linee)
      If InN(Series(1).N, j) Then
      Else
        Call linea(Linee(j).X1, Linee(j).Y1, Linee(j).X2, Linee(j).Y2, Linee(j).COLORE, disp)
      End If
    Next j
  End If
  If (UBound(Testi) > 0) Then
    For j = 1 To UBound(Testi)
      disp.ForeColor = col(9)
      tfn = disp.FontName
      tfs = disp.FontSize
      disp.FontSize = FontSize
      disp.FontName = FontName
      disp.CurrentX = Testi(j).X
      disp.CurrentY = Testi(j).Y
      disp.FontSize = tfs
      disp.FontName = tfn
      disp.Print Testi(j).Valore
    Next j
  End If
  If (UBound(Cerchi) > 0) Then
    For j = 1 To UBound(Cerchi)
      Call Arco(Cerchi(j), disp)
    Next j
  End If
  If (UBound(Series) > 0) Then
    For j = 1 To UBound(Series)
      For i = 1 To UBound(Series(j).X)
        If (i > 1) Then Call linea(Series(j).X(i - 1), Series(j).Y(i - 1), Series(j).X(i), Series(j).Y(i), Series(j).COLORE, disp)
      Next i
      If (Series(j).NOME = 1) Then
        For i = 1 To UBound(Series(j).X)
          If Series(j).MarcaPunti Then
            co = col(6)
            Call Cerchio(Series(j).X(i), Series(j).Y(i), R, co, disp)
          End If
          If InN(Series(j).N, i) Then
            co = col(7)
            Call Cerchio(Series(j).X(i), Series(j).Y(i), 1.75 * R, co, disp)
          End If
        Next i
      End If
    Next j
  End If
  If UBound(SJoins) > 0 Then
    For j = 1 To UBound(SJoins)
      For i = 1 To UBound(SJoins(j).X1)
        Call linea(SJoins(j).X1(i), SJoins(j).Y1(i), SJoins(j).X2(i), SJoins(j).Y2(i), SJoins(j).COLORE, disp)
      Next i
    Next j
  End If
  '
End Sub

Public Sub SelPunti(NOME As String, N() As Integer)
'
Dim Esistente As Boolean
Dim i         As Integer
Dim j         As Integer
  '
  For i = 1 To UBound(Series)
    Esistente = False
    If UCase(Series(i).NOME) = UCase(NOME) Then
      Esistente = True
      Exit For
    End If
  Next i
  If Esistente Then
    Series(i).N = N
  Else
    StopRegieEvents
    MsgBox "Series " & NOME & " have not been found!!!", vbInformation, "Method SelPunti"
    ResumeRegieEvents
  End If
  If Graf Then Call Redraw
  '
End Sub

Public Function SerieGetN(NOME As String) As Variant
'
Dim j As Integer
  '
  j = NumSerie(NOME)
  If (j > 0) Then
    SerieGetN = Series(j).N
  Else
    StopRegieEvents
    MsgBox "Series " & NOME & " have not been found!!!", vbInformation, "Method SerieGetN"
    ResumeRegieEvents
  End If
  '
End Function

Private Sub DisegnaSLog(disp As PictureBox)

Dim i   As Integer
Dim R   As Double
Dim co  As Long
Dim X1  As Double
Dim X2  As Double
Dim Y1  As Double
Dim Y2  As Double

  If ScalaX = 0 Then Call CalcolaParametri(disp)
  If griglia Then Call DisGriglia(disp)
  R = 0.2
  Call linea(Xm - 0.5, Ym, Xm + 0.5, Ym, col(9), disp)
  Call linea(Xm, Ym - 0.5, Xm, Ym + 0.5, col(9), disp)
  For i = 2 To UBound(GX)
    Call linea(DB(GX(i - 1)), GY(i - 1), DB(GX(i)), GY(i), col(5), disp)
  Next i
  For i = 1 To UBound(GX)
    If InN(GN, i) Then
      co = col(7)
      Call Cerchio(DB(GX(i)), GY(i), 1.75 * R, co, disp)
    Else
      co = col(6)
      Call Cerchio(DB(GX(i)), GY(i), R, co, disp)
    End If
  Next i

End Sub

Private Function TG(i As Integer) As Double

Dim ttg As Double
    
  If (-GA(i) - GB(i)) >= 0 Then
    ttg = -GA(i) - GB(i) + 270
  Else
    ttg = -GA(i) - GB(i) + 90
  End If
  TG = ttg
  
End Function

Private Function Ort(i As Integer) As Double

Dim tor As Double
  
  'If (-GA(i) - GB(i)) >= 0 Then
  '  tor = -GA(i) - GB(i)
  'Else
  '  tor = -GA(i) - GB(i) + 180
  'End If
  Ort = GT(i) + 90
  
End Function

Private Function InN(N() As Integer, tN As Integer) As Boolean

Dim i As Integer

On Error Resume Next

  i = UBound(N)
  If (i > 0) Then
    For i = 1 To UBound(N)
        If (N(i) = tN) Then InN = True
    Next i
  Else
    InN = False
  End If
  
End Function

Private Sub NAgg(tN As Integer)

Dim i As Integer

  For i = 1 To UBound(GN)
    If (GN(i) = tN) Then Exit Sub
  Next i
  
  ReDim Preserve GN(UBound(GN) + 1)
  
  i = UBound(GN) - 1
  While GN(i) > tN
    GN(i + 1) = GN(i)
    i = i - 1
  Wend
  GN(i + 1) = tN
  
End Sub

Private Sub NRem(tN As Integer)

Dim i   As Integer
Dim i2  As Integer
Dim nr  As Boolean

  nr = True
  For i = 1 To UBound(GN)
    If GN(i) = tN Then
        nr = False
        i2 = i
    End If
  Next i
  If nr Then Exit Sub
  For i = i2 To UBound(GN) - 1
    GN(i) = GN(i + 1)
  Next i
  ReDim Preserve GN(UBound(GN) - 1)
  
End Sub

Public Sub SerieNAgg(tN As Integer, Optional NOME As String)

Dim i As Integer
Dim j As Integer

On Error Resume Next

  j = NumSerie(NOME)
  If j > 0 Then
    For i = 1 To UBound(Series(j).N)
      If (Series(j).N(i) = tN) Then Exit Sub
    Next i
    ReDim Preserve Series(j).N(UBound(Series(j).N) + 1)
    i = UBound(Series(j).N) - 1
    While Series(j).N(i) > tN
      Series(j).N(i + 1) = Series(j).N(i)
      i = i - 1
    Wend
    Series(j).N(i + 1) = tN
    Call Redraw
  End If

End Sub

Public Sub SerieNRem(tN As Integer, Optional NOME As String)

Dim i   As Integer
Dim i2  As Integer
Dim nr  As Boolean
Dim j   As Integer

On Error Resume Next

  j = NumSerie(NOME)
  If (j > 0) Then
    nr = True
    For i = 1 To UBound(Series(j).N)
      If Series(j).N(i) = tN Then
        nr = False
        i2 = i
      End If
    Next i
    If nr Then Exit Sub
    For i = i2 To UBound(Series(j).N) - 1
      Series(j).N(i) = Series(j).N(i + 1)
    Next i
    ReDim Preserve Series(j).N(UBound(Series(j).N) - 1)
    Call Redraw
  End If

End Sub

Private Function SCx(Valore As Variant)

  If ScalaX > 0 Then SCx = ((Valore - SpostX) / ScalaX) + Xm

End Function

Private Function SCy(Valore As Variant)
  
  If ScalaY > 0 Then SCy = ((-Valore - SpostY) / ScalaY) + Ym

End Function

Private Function Sc2X(Valore As Variant)

  Sc2X = ((Valore - Xm) * ScalaX) + SpostX

End Function

Private Function Sc2Y(Valore As Variant)
  
  Sc2Y = Display.ScaleHeight - ((Valore - Ym) * ScalaY) + SpostY

End Function

Private Sub linea(X1 As Double, Y1 As Double, X2 As Double, Y2 As Double, COLORE As Long, disp As Object)

  disp.Line (((X1 - Xm) * ScalaX) + SpostX, disp.ScaleHeight - ((Y1 - Ym) * ScalaY) + SpostY)-(((X2 - Xm) * ScalaX) + SpostX, disp.ScaleHeight - ((Y2 - Ym) * ScalaY) + SpostY), COLORE

End Sub

Private Sub LineaAlfa(X1 As Double, Y1 As Double, ALFA As Double, Lung As Double, COLORE As Long, disp As Object)

Dim X2 As Double
Dim Y2 As Double

  X2 = X1 + Lung * Cos(ALFA / 180 * PG)
  Y2 = Y1 + Lung * Sin(ALFA / 180 * PG)
  disp.Line (((X1 - Xm) * ScalaX) + SpostX, disp.ScaleHeight - ((Y1 - Ym) * ScalaY) + SpostY)-(((X2 - Xm) * ScalaX) + SpostX, disp.ScaleHeight - ((Y2 - Ym) * ScalaY) + SpostY), COLORE

End Sub

Private Sub Circ2pt(X1 As Double, Y1 As Double, X2 As Double, Y2 As Double, R As Double)

Dim Xm    As Double
Dim Ym    As Double
Dim R1    As Double
Dim R2    As Double
Dim k1    As Double
Dim k2    As Double
Dim k3    As Double
Dim k4    As Double
Dim K5    As Double
Dim inv   As Boolean
Dim Xc1   As Double
Dim Yc1   As Double
Dim Xc2   As Double
Dim Yc2   As Double
Dim Delta As Double

  inv = False
  If Y2 = Y1 Then
    k1 = X1
    k2 = X2
    X1 = Y1
    X2 = Y2
    Y1 = k1
    Y2 = k2
    inv = True
  End If
  k1 = (X2 - X1) ^ 2 - (Y2 - Y1) ^ 2
  If k1 < 0 Then Exit Sub
  If Sqr(k1) > Abs(2 * R) Then Exit Sub
  Xm = X1 + (X2 - X1) / 2
  Ym = Y1 + (Y2 - Y1) / 2

  k1 = -(X2 - X1) / (Y2 - Y1)
  k2 = (X2 ^ 2 - X1 ^ 2 + Y2 ^ 2 - Y1 ^ 2) / (2 * (Y2 - Y1))

  R2 = Sqr((X1 - Xm) ^ 2 + (Y1 - Ym) ^ 2)
  R1 = Sqr(R ^ 2 - R2 ^ 2)

  k3 = 1 + k1 ^ 2
  k4 = -2 * Xm + 2 * k1 * k2 - 2 * Ym * k1
  K5 = k2 ^ 2 + Ym ^ 2 - 2 * Ym * k2 - R1 ^ 2 + Xm ^ 2

  Delta = k4 ^ 2 - 4 * k3 * K5

  If Delta >= 0 Then
    Xc1 = Sgn(R) * (Sqr(Delta) / (2 * k3)) - (k4 / (2 * k3))
    Yc1 = k1 * Xc1 + k2
  Else
    WRITE_DIALOG "Sub Circ2pt: Delta < 0"
  End If

  If inv Then
    k1 = X1
    k2 = X2
    X1 = Y1
    X2 = Y2
    Y1 = k1
    Y2 = k2
    k1 = Xc1
    Xc1 = Yc1
    Yc1 = k1
  End If

End Sub

Sub ANGOLI_CENTRO(PInX As Double, PInY As Double, PFiX As Double, PFiY As Double, _
                                  Xc As Double, YC As Double, R0 As Double, _
                                  A1 As Double, A2 As Double)
Dim Q1 As Double
Dim Q2 As Double
  
  If PInX >= Xc And PInY >= YC Then Q1 = 1
  If PInX >= Xc And PInY < YC Then Q1 = 4
  If PInX < Xc And PInY > YC Then Q1 = 2
  If PInX < Xc And PInY < YC Then Q1 = 3
  If PFiX >= Xc And PFiY >= YC Then Q2 = 1
  If PFiX >= Xc And PFiY < YC Then Q2 = 4
  If PFiX < Xc And PFiY > YC Then Q2 = 2
  If PFiX < Xc And PFiY < YC Then Q2 = 3
  If PInX = Xc Then
    A1 = PG / 2
  Else
    A1 = Atn((PInY - YC) / (PInX - Xc))
  End If
  If Q1 = 3 Then A1 = A1 + PG
  If Q1 = 2 Then A1 = A1 + PG
  If Q1 = 4 Then A1 = A1 + 2 * PG
  If PFiX = Xc Then
    A2 = PG / 2
  Else
    A2 = Atn((PFiY - YC) / (PFiX - Xc))
  End If
  If Q2 = 3 Then A2 = A2 + PG
  If Q2 = 2 Then A2 = A2 + PG
  If Q2 = 4 Then A2 = A2 + 2 * PG
  If A1 < 0 Then A1 = A1 + 2 * PG
  If A2 < 0 Then A2 = A2 + 2 * PG

End Sub

Private Sub Cerchio(X1 As Double, Y1 As Double, R As Double, COLORE As Long, disp As Object, Optional Riempimento As FillStyleConstants = vbFSSolid)

  disp.FillStyle = Riempimento
  disp.FillColor = COLORE
  disp.Circle (((X1 - Xm) * ScalaX) + SpostX, disp.ScaleHeight - ((Y1 - Ym) * ScalaY) + SpostY), R, COLORE
  
End Sub

Public Sub CopiaSu(Controllo As Object)
  
  Controllo.ScaleMode = vbMillimeters
  If Not TypeOf Controllo Is Printer Then
    Controllo.AutoRedraw = True
  End If
  ScalaX = 0
  Call Disegna(Controllo)
  ScalaX = 0
  Call Disegna(Display)
  
End Sub

Public Sub CopiaSuBN(Controllo As Object)

Dim col2()  As Long
Dim i       As Integer

  Controllo.ScaleMode = vbMillimeters
  If Not TypeOf Controllo Is Printer Then
    Controllo.AutoRedraw = True
  End If
  ScalaX = 0
  col2 = col
  ReDim col2(UBound(col))
  For i = 1 To UBound(col)
    col2(i) = col(i)
    col(i) = vbBlack
  Next i
  Call Disegna(Controllo)
  For i = 1 To UBound(col)
    col(i) = col2(i)
  Next i
  ScalaX = 0
  Call Disegna(Display)
  
End Sub

Private Sub UserControl_Show()
  
  Call UserControl_Resize
  
End Sub

Private Function Radians(ByVal Degree As Single) As Single
  
  'Converts degrees to radians
  Radians = Degree * 0.0174532925
  
End Function

Public Property Get Agganciare() As Boolean

  Agganciare = aggancia

End Property

Public Property Let Agganciare(ByVal bAgganciare As Boolean)
            
  Evid.Visible = bAgganciare
  shpJoinEv.Visible = bAgganciare
  aggancia = bAgganciare
  If Graf Then
    If UBound(Series) > 0 Then
      Evid.Left = Sc2X(Series(1).X(1)) - Evid.Width / 2
      Evid.Top = Sc2Y(Series(1).Y(1)) - Evid.Height / 2
    End If
  End If
  Call UserControl.PropertyChanged("Agganciare")

End Property

Public Sub SetLingua(LINGUA As String)

  Select Case UCase(Trim(LINGUA))
  
  Case "IT"
    mnuZoom.Caption = "Zoom Tutto"
    mnuZoomOut.Caption = "Zoom Out"
    mnuZoomIn.Caption = "Zoom In"
    mnuZoomF.Caption = "Zoom Fattore"
    mnuLegenda.Caption = "Legenda"
    mnuCentra.Caption = "Centra"
    mnuGriglia.Caption = "Griglia"
    mnuEsporta.Caption = "Esporta..."
    mnuDXF.Caption = "DXF"
    mnuXYT.Caption = "XY"
    mnuUser1.Caption = "User1"
    mnuUser2.Caption = "User2"
    mnuUser3.Caption = "User3"
    mnuUser4.Caption = "User4"
    mnuAbout.Caption = "About"
    LinguaMisura = "Misura"
  
  Case "UK", "GR"
    mnuZoom.Caption = "Zoom All"
    mnuZoomOut.Caption = "Zoom Out"
    mnuZoomIn.Caption = "Zoom In"
    mnuZoomF.Caption = "Zoom Factor"
    mnuLegenda.Caption = "Legend"
    mnuCentra.Caption = "Center"
    mnuGriglia.Caption = "Grid"
    mnuEsporta.Caption = "Export..."
    mnuDXF.Caption = "DXF"
    mnuXYT.Caption = "XY"
    mnuUser1.Caption = "User1"
    mnuUser2.Caption = "User2"
    mnuUser3.Caption = "User3"
    mnuUser4.Caption = "User4"
    mnuAbout.Caption = "About"
    LinguaMisura = "Measure"
  
  End Select

End Sub
