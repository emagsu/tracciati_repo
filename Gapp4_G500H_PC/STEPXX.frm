VERSION 5.00
Begin VB.Form STEPXX 
   Appearance      =   0  'Flat
   BackColor       =   &H00EBE1D7&
   BorderStyle     =   0  'None
   ClientHeight    =   7608
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   11100
   ForeColor       =   &H80000008&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7608
   ScaleWidth      =   11100
   ShowInTaskbar   =   0   'False
   Tag             =   "STEPXX"
   Visible         =   0   'False
   WhatsThisButton =   -1  'True
   WhatsThisHelp   =   -1  'True
   Begin VB.CommandButton Cmd07 
      Caption         =   "Cmd07"
      Height          =   500
      Left            =   2160
      TabIndex        =   16
      Top             =   5640
      Width           =   1692
   End
   Begin VB.CommandButton Cmd06 
      Caption         =   "Cmd06"
      Height          =   492
      Left            =   360
      TabIndex        =   15
      Top             =   5640
      Width           =   1692
   End
   Begin VB.CommandButton Cmd05 
      Caption         =   "Cmd05"
      Height          =   500
      Left            =   2160
      TabIndex        =   14
      Top             =   5040
      Width           =   1692
   End
   Begin VB.CommandButton Cmd04 
      Caption         =   "Cmd04"
      Height          =   492
      Left            =   360
      TabIndex        =   13
      Top             =   5040
      Width           =   1692
   End
   Begin VB.Frame Frm_Step 
      BackColor       =   &H00FFFFC0&
      Caption         =   "Montare attrezzatura e utensili"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   10.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5772
      Left            =   4560
      TabIndex        =   6
      Top             =   840
      Width           =   4812
      Begin VB.Label Lbl0 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Codice Rullo cod R1xxxx"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   10.8
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   492
         Index           =   5
         Left            =   240
         TabIndex        =   12
         Top             =   3720
         Width           =   4212
      End
      Begin VB.Label Lbl0 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Codice Mola cod S1xxxx"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   10.8
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   492
         Index           =   4
         Left            =   240
         TabIndex        =   11
         Top             =   3000
         Width           =   4212
      End
      Begin VB.Label Lbl0 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Attrezzatura W2 cod W2xxxx"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   10.8
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   492
         Index           =   3
         Left            =   240
         TabIndex        =   10
         Top             =   2280
         Width           =   4212
      End
      Begin VB.Label Lbl0 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Attrezzatura C2 cod C2xxxx"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   10.8
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   492
         Index           =   2
         Left            =   240
         TabIndex        =   9
         Top             =   1680
         Width           =   4212
      End
      Begin VB.Label Lbl0 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Attrezzatura W1 cod W1xxxx"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   10.8
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   492
         Index           =   1
         Left            =   240
         TabIndex        =   8
         Top             =   960
         Width           =   4212
      End
      Begin VB.Label Lbl0 
         Alignment       =   2  'Center
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Attrezzatura C1 cod C1xxxx"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   10.8
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   492
         Index           =   0
         Left            =   240
         TabIndex        =   7
         Top             =   360
         Width           =   4212
      End
   End
   Begin VB.CommandButton Cmd03 
      Caption         =   "Cmd03"
      Height          =   492
      Left            =   2160
      TabIndex        =   5
      Top             =   4440
      Width           =   1692
   End
   Begin VB.CommandButton Cmd02 
      Caption         =   "Cmd02"
      Height          =   492
      Left            =   360
      TabIndex        =   4
      Top             =   4440
      Width           =   1692
   End
   Begin VB.CommandButton Cmd01 
      Caption         =   "Cmd01"
      Height          =   500
      Left            =   1200
      TabIndex        =   3
      Top             =   3840
      Width           =   1692
   End
   Begin VB.TextBox TESTO 
      BackColor       =   &H00FFFFC0&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.6
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   372
      Left            =   600
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      TabIndex        =   2
      Top             =   840
      Width           =   3252
   End
   Begin VB.Image IMMAGINE4 
      BorderStyle     =   1  'Fixed Single
      Height          =   492
      Left            =   2160
      Stretch         =   -1  'True
      Top             =   2400
      Visible         =   0   'False
      Width           =   1212
   End
   Begin VB.Image IMMAGINE3 
      BorderStyle     =   1  'Fixed Single
      Height          =   492
      Left            =   600
      Stretch         =   -1  'True
      Top             =   2400
      Visible         =   0   'False
      Width           =   1212
   End
   Begin VB.Image IMMAGINE2 
      BorderStyle     =   1  'Fixed Single
      Height          =   492
      Left            =   2160
      Stretch         =   -1  'True
      Top             =   1680
      Visible         =   0   'False
      Width           =   1212
   End
   Begin VB.Image IMMAGINE 
      BorderStyle     =   1  'Fixed Single
      Height          =   492
      Left            =   600
      Stretch         =   -1  'True
      Top             =   1680
      Width           =   1212
   End
   Begin VB.Label INTESTAZIONE 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   13.8
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   396
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   5604
   End
   Begin VB.Label NOME 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   13.8
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   384
      Left            =   5760
      TabIndex        =   1
      Top             =   120
      Width           =   372
   End
End
Attribute VB_Name = "STEPXX"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
  
Dim Controllo   As Boolean
Dim Partenza_x  As Integer
Dim Partenza_y  As Integer
Dim Valore_x    As Integer
Dim Valore_y    As Integer

Private Sub Cmd01_Click()
'
'Tasto Conferma azioni cambio mola
'

Dim ValR896 As Long
Dim sValR896 As String


'LETTURA DEL VALORE
 ValR896 = val(OPC_LEGGI_DATO("/CHANNEL/PARAMETER/R[896]"))
 

  Select Case ValR896
  
  Case 151, 153, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 168, 171
       ValR896 = ValR896 + 1
  Case Else
  
  End Select

 
 
 If (ValR896 >= 151) And (ValR896 <= 172) Then
 Else
    If ValR896 > 172 Then
      ValR896 = 99
    Else
      ValR896 = 151
    End If
 End If
 
 sValR896 = Trim(str(ValR896))
  
 
 Call OPC_SCRIVI_DATO("/CHANNEL/PARAMETER/R[896]", sValR896)
 



End Sub

Private Sub Cmd02_Click()
'Tasto per Selezione Scarico/Carico Mola


Dim ValR896 As Long
Dim sValR896 As String

Dim iRESET              As Integer
Dim iAUTO               As Integer
Dim sCMD_PI             As String
Dim sNOME_PROGRAMMA     As String
Dim sRESET              As String
Dim sAUTO               As String


'/Channel/ProgramPointer/progName[u1]=/_N_WKS_DIR/_N_COMUNEHG_WPD/_N_CARICA_SPINA_MPF

'
'Seleziona Programma Cambio Mola e inizia procedura per cambio mola
'
sNOME_PROGRAMMA = "SchleifScheibe Laden"

'LETTURA DEL VALORE
ValR896 = val(OPC_LEGGI_DATO("/CHANNEL/PARAMETER/R[896]"))

If (ValR896 < 150) Or (ValR896 > 172) Then

'LETTURA DELL'ITEM DDE PER CONTROLLO STATO DI RESET
 sRESET = GetInfo("SELEZIONE_PROGRAMMI", "sRESET", PathFILEINI)
 If (Len(sRESET) > 0) Then iRESET = val(OPC_LEGGI_DATO(sRESET)) Else iRESET = 1
'LETTURA DELL'ITEM DDE PER CONTROLLO MODO AUTOMATICO
 sAUTO = GetInfo("SELEZIONE_PROGRAMMI", "sAUTO", PathFILEINI)
 If Len(sAUTO) > 0 Then iAUTO = val(OPC_LEGGI_DATO(sAUTO)) Else iAUTO = 1

'SELEZIONE DEL PROGRAMMA QUANDO SONO VERIFICATE LE CONDIZIONI
 If (iRESET = 1) Then
   If (iAUTO = 1) Then
      sCMD_PI = "/_N_WKS_DIR/_N_COMUNEHG_WPD/_N_LOAD_MOLA_MPF"
      'SELEZIONE DEL PROGRAMMA PEZZO TRAMITE COMANDO PI DELLA TABELLA
       Call SuOP.SelPrg(sCMD_PI)
       'SEGNALAZIONE UTENTE
       WRITE_DIALOG sNOME_PROGRAMMA & " OK!!!"
   Else
      'SEGNALAZIONE UTENTE
      WRITE_DIALOG "NO AUTO MODE: " & sNOME_PROGRAMMA & " NOT OK!!!"
      Exit Sub
   End If
 Else
    'SEGNALAZIONE UTENTE
    WRITE_DIALOG "NO RESET: " & sNOME_PROGRAMMA & " NOT OK!!!"
    Exit Sub
 End If
 '
End If
 
If (ValR896 >= 151) And (ValR896 <= 172) Then
    'non scrivo
Else
  If (ValR896 < 150) Or (ValR896 > 172) Then
      ValR896 = 150
      sValR896 = Trim(str(ValR896))
      Call OPC_SCRIVI_DATO("/CHANNEL/PARAMETER/R[896]", sValR896)
   End If
End If

Call Set_State(100)
'Serve a tornare sullo stato dato da ValR896 senza avere variazione del valore di R896
Call STEPXX_CAMBIOMOLA(ValR896)


End Sub

Private Sub Cmd03_Click()

Dim ValR896 As Long
Dim sValR896 As String

Dim iRESET              As Integer
Dim iAUTO               As Integer
Dim sCMD_PI             As String
Dim sNOME_PROGRAMMA     As String
Dim sRESET              As String
Dim sAUTO               As String

'Tasto per Selezionare Programma Cambio Rullo
'
sNOME_PROGRAMMA = "Rolle Laden"

'LETTURA DEL VALORE
ValR896 = val(OPC_LEGGI_DATO("/CHANNEL/PARAMETER/R[896]"))


'LETTURA DELL'ITEM DDE PER CONTROLLO STATO DI RESET
 sRESET = GetInfo("SELEZIONE_PROGRAMMI", "sRESET", PathFILEINI)
 If (Len(sRESET) > 0) Then iRESET = val(OPC_LEGGI_DATO(sRESET)) Else iRESET = 1
'LETTURA DELL'ITEM DDE PER CONTROLLO MODO AUTOMATICO
 sAUTO = GetInfo("SELEZIONE_PROGRAMMI", "sAUTO", PathFILEINI)
 If Len(sAUTO) > 0 Then iAUTO = val(OPC_LEGGI_DATO(sAUTO)) Else iAUTO = 1

'SELEZIONE DEL PROGRAMMA QUANDO SONO VERIFICATE LE CONDIZIONI
 If (iRESET = 1) Then
   If (iAUTO = 1) Then
      sCMD_PI = "/_N_WKS_DIR/_N_COMUNEHG_WPD/_N_LOAD_RULLO_MPF"
      'SELEZIONE DEL PROGRAMMA PEZZO TRAMITE COMANDO PI DELLA TABELLA
       Call SuOP.SelPrg(sCMD_PI)
       'SEGNALAZIONE UTENTE
       WRITE_DIALOG sNOME_PROGRAMMA & " OK!!!"
   Else
      'SEGNALAZIONE UTENTE
      WRITE_DIALOG "NO AUTO MODE: " & sNOME_PROGRAMMA & " NOT OK!!!"
      Exit Sub
   End If
 Else
    'SEGNALAZIONE UTENTE
    WRITE_DIALOG "NO RESET: " & sNOME_PROGRAMMA & " NOT OK!!!"
    Exit Sub
 End If
 '
 

ValR896 = 181
sValR896 = Trim(str(ValR896))
Call OPC_SCRIVI_DATO("/CHANNEL/PARAMETER/R[896]", sValR896)

Call Set_State(100)
Call STEPXX_CAMBIORULLO(ValR896)
 

End Sub

Private Sub Cmd04_Click()
Dim ValR896 As Long
Dim sValR896 As String

Dim iRESET              As Integer
Dim iAUTO               As Integer
Dim sCMD_PI             As String
Dim sNOME_PROGRAMMA     As String
Dim sRESET              As String
Dim sAUTO               As String

'Tasto per Selezionare Programma Cambio Attrezzattura
'Posiziona tavola C3 per cambio attrezzatura
'

sNOME_PROGRAMMA = "Spannzange/Reitstock Laden"

'LETTURA DEL VALORE
ValR896 = val(OPC_LEGGI_DATO("/CHANNEL/PARAMETER/R[896]"))


'LETTURA DELL'ITEM DDE PER CONTROLLO STATO DI RESET
 sRESET = GetInfo("SELEZIONE_PROGRAMMI", "sRESET", PathFILEINI)
 If (Len(sRESET) > 0) Then iRESET = val(OPC_LEGGI_DATO(sRESET)) Else iRESET = 1
'LETTURA DELL'ITEM DDE PER CONTROLLO MODO AUTOMATICO
 sAUTO = GetInfo("SELEZIONE_PROGRAMMI", "sAUTO", PathFILEINI)
 If Len(sAUTO) > 0 Then iAUTO = val(OPC_LEGGI_DATO(sAUTO)) Else iAUTO = 1

'SELEZIONE DEL PROGRAMMA QUANDO SONO VERIFICATE LE CONDIZIONI
 If (iRESET = 1) Then
   If (iAUTO = 1) Then
      sCMD_PI = "/_N_WKS_DIR/_N_COMUNEHG_WPD/_N_RUOTAC3_MPF"
      'SELEZIONE DEL PROGRAMMA PEZZO TRAMITE COMANDO PI DELLA TABELLA
       Call SuOP.SelPrg(sCMD_PI)
       'SEGNALAZIONE UTENTE
       WRITE_DIALOG sNOME_PROGRAMMA & " OK!!!"
   Else
      'SEGNALAZIONE UTENTE
      WRITE_DIALOG "NO AUTO MODE: " & sNOME_PROGRAMMA & " NOT OK!!!"
      Exit Sub
   End If
 Else
    'SEGNALAZIONE UTENTE
    WRITE_DIALOG "NO RESET: " & sNOME_PROGRAMMA & " NOT OK!!!"
    Exit Sub
 End If
 '
ValR896 = 141

sValR896 = Trim(str(ValR896))
Call OPC_SCRIVI_DATO("/CHANNEL/PARAMETER/R[896]", sValR896)

'Settare Indice per posizionare tavola C1 per cambio attrezzatura
Call OPC_SCRIVI_DATO("/ACC/NCK/UGUD/INDTAV_G", "0")

'Call Set_State(100)
'Call STEPXX_ATTREZZATURA1(ValR896)
 

End Sub

Private Sub Cmd05_Click()
'
'Tasto Conferma azioni cambio attrezzo Tavola 1
Dim ValR896 As Long
Dim sValR896 As String


'LETTURA DEL VALORE
 ValR896 = val(OPC_LEGGI_DATO("/CHANNEL/PARAMETER/R[896]"))
 
  Select Case ValR896
  
  Case 143
       'Serve per poter selezionare la pagina attrezzatura Tavola 2
       ValR896 = 193
  Case Else
  
  End Select
 
 sValR896 = Trim(str(ValR896))
 Call OPC_SCRIVI_DATO("/CHANNEL/PARAMETER/R[896]", sValR896)
 

End Sub

Private Sub Cmd06_Click()

Dim ValR896 As Long
Dim sValR896 As String

Dim iRESET              As Integer
Dim iAUTO               As Integer
Dim sCMD_PI             As String
Dim sNOME_PROGRAMMA     As String
Dim sRESET              As String
Dim sAUTO               As String

'Tasto per Selezionare Programma Cambio Attrezzattura
'Posiziona tavola C3 per cambio attrezzatura
'

sNOME_PROGRAMMA = "Spannzange/Reitstock Laden"

'LETTURA DEL VALORE
ValR896 = val(OPC_LEGGI_DATO("/CHANNEL/PARAMETER/R[896]"))


'LETTURA DELL'ITEM DDE PER CONTROLLO STATO DI RESET
 sRESET = GetInfo("SELEZIONE_PROGRAMMI", "sRESET", PathFILEINI)
 If (Len(sRESET) > 0) Then iRESET = val(OPC_LEGGI_DATO(sRESET)) Else iRESET = 1
'LETTURA DELL'ITEM DDE PER CONTROLLO MODO AUTOMATICO
 sAUTO = GetInfo("SELEZIONE_PROGRAMMI", "sAUTO", PathFILEINI)
 If Len(sAUTO) > 0 Then iAUTO = val(OPC_LEGGI_DATO(sAUTO)) Else iAUTO = 1

'SELEZIONE DEL PROGRAMMA QUANDO SONO VERIFICATE LE CONDIZIONI
 If (iRESET = 1) Then
   If (iAUTO = 1) Then
      sCMD_PI = "/_N_WKS_DIR/_N_COMUNEHG_WPD/_N_RUOTAC3_MPF"
      'SELEZIONE DEL PROGRAMMA PEZZO TRAMITE COMANDO PI DELLA TABELLA
       Call SuOP.SelPrg(sCMD_PI)
       'SEGNALAZIONE UTENTE
       WRITE_DIALOG sNOME_PROGRAMMA & " OK!!!"
   Else
      'SEGNALAZIONE UTENTE
      WRITE_DIALOG "NO AUTO MODE: " & sNOME_PROGRAMMA & " NOT OK!!!"
      Exit Sub
   End If
 Else
    'SEGNALAZIONE UTENTE
    WRITE_DIALOG "NO RESET: " & sNOME_PROGRAMMA & " NOT OK!!!"
    Exit Sub
 End If
 '
ValR896 = 144

sValR896 = Trim(str(ValR896))
Call OPC_SCRIVI_DATO("/CHANNEL/PARAMETER/R[896]", sValR896)

'Settare Indice per posizionare tavola C2 per cambio attrezzatura
Call OPC_SCRIVI_DATO("/ACC/NCK/UGUD/INDTAV_G", "1")

'Call Set_State(100)
'Call STEPXX_ATTREZZATURA1(ValR896)
 

End Sub

Private Sub Cmd07_Click()

'Tasto Conferma azioni cambio attrezzo Tavola 2
Dim ValR896 As Long
Dim sValR896 As String


'LETTURA DEL VALORE
 ValR896 = val(OPC_LEGGI_DATO("/CHANNEL/PARAMETER/R[896]"))
 
  Select Case ValR896
  
  Case 146
       'Serve per poter selezionare la pagina cambio mola
       ValR896 = 194
  Case Else
  
  End Select
 
 sValR896 = Trim(str(ValR896))
 Call OPC_SCRIVI_DATO("/CHANNEL/PARAMETER/R[896]", sValR896)
 
End Sub

'
Private Sub Form_Activate()
  '
  Call ChildActivate(Me)
  Me.Line (0, 0)-(Me.Width * 0.999, Me.Height * 0.998), , B
  '
End Sub

Private Sub Form_Load()
'
Dim i As Integer
'
On Error GoTo errForm_Load
  '
  'NASCONDO I TASTI DELLA SECONDA RIGA
  MDIForm1.SSPanel1.Height = MDIForm1.Picture1(0).Height
  For i = 0 To 7
    MDIForm1.Picture3(i).Visible = False
  Next i
  '
  Me.Width = g_nBeArtWidth * 0.99
  Me.Height = g_nBeArtHeight
  '
  Call LEGGI_LINGUA
  Call IMPOSTA_FONT(Me)
  '
Exit Sub

errForm_Load:
  WRITE_DIALOG Conversion.Error(Err)
  Resume Next

End Sub

Private Sub Form_Deactivate()
  '
  Call ChildDeActivate(Me)
  '
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  '
  Call GENERA_EVENTO_CLICK(KeyCode, Shift)
  '
End Sub

Private Sub Form_DblClick()
'
On Error Resume Next
  '
  If IMMAGINE.Stretch = True Then
    IMMAGINE.Stretch = False
  Else
    IMMAGINE.Stretch = True
  End If
  '
End Sub

Private Sub IMMAGINE_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
'
On Error Resume Next
  '
  Select Case Button
  Case 1
      Controllo = True
      Partenza_x = X
      Partenza_y = Y
  Case 2
    If (Shift = 1) Then
      IMMAGINE.Width = IMMAGINE.Width * 0.9
      IMMAGINE.Height = IMMAGINE.Height * 0.9
    Else
      IMMAGINE.Width = IMMAGINE.Width * 1.1
      IMMAGINE.Height = IMMAGINE.Height * 1.1
    End If
  End Select
  '
End Sub

Private Sub IMMAGINE_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
'
On Error Resume Next
  '
  If (Controllo = True) Then
    Valore_x = X - Partenza_x
    Valore_y = Y - Partenza_y
    IMMAGINE.Left = IMMAGINE.Left + Valore_x
    IMMAGINE.Top = IMMAGINE.Top + Valore_y
  End If
  '
End Sub

Private Sub IMMAGINE_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
'
On Error Resume Next
  '
  Controllo = False
  '
End Sub

Private Sub TESTO_DblClick()
'
Dim NomePezzo        As String
Dim NomeFile         As String
Dim INDICE           As Integer
Dim ret              As Long
Dim TASTO            As String
Dim PEZZO            As String
Dim TIPO_LAVORAZIONE As String
'
On Error Resume Next
  '
  'INIZIALIZZAZIONE VARIABILI
  INDICE = val(STEPXX.NOME.Caption)
  NomePezzo = GetInfo("NASCONDI", "NomePezzo", Path_LAVORAZIONE_INI)
  'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  PEZZO = g_chOemPATH & "\" & TIPO_LAVORAZIONE & "\PROCEDURE\" & NomePezzo
  'VISUALIZZAZIONE TESTO
  NomeFile = PEZZO & "\" & Format$(INDICE, "0") & "\TT.TXT"
  ret = MsgBox(Conversion.Error(58) & Chr(13) & Chr(13) & "SAVE " & NomeFile & " ?", vbDefaultButton2 + vbQuestion + vbYesNo, "WARNING")
  If (ret = vbYes) Then
    Open NomeFile For Output As #1
    Print #1, STEPXX.TESTO.Text
    Close #1
    WRITE_DIALOG NomeFile & " have been updated!!!"
  Else
    WRITE_DIALOG "Operation aborted by user!!!"
  End If
  '
End Sub
