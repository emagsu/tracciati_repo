VERSION 5.00
Begin VB.Form RIMF15 
   Appearance      =   0  'Flat
   BackColor       =   &H00EBE1D7&
   BorderStyle     =   0  'None
   ClientHeight    =   5316
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   9336
   ForeColor       =   &H80000008&
   Icon            =   "RIMF15.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   5316
   ScaleWidth      =   9336
   ShowInTaskbar   =   0   'False
   Tag             =   "RIMF15"
   Visible         =   0   'False
   WhatsThisButton =   -1  'True
   WhatsThisHelp   =   -1  'True
   Begin VB.Image IMMAGINE 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   1212
      Left            =   600
      Top             =   1680
      Width           =   1212
   End
   Begin VB.Label TESTO 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   10.2
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   300
      Left            =   1080
      TabIndex        =   1
      Top             =   720
      Width           =   4668
   End
   Begin VB.Label INTESTAZIONE 
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   13.8
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   396
      Left            =   0
      TabIndex        =   0
      Top             =   120
      Width           =   5604
   End
   Begin VB.Label NOME 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   13.8
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   384
      Left            =   5760
      TabIndex        =   2
      Top             =   120
      Width           =   372
   End
End
Attribute VB_Name = "RIMF15"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
  
Dim Controllo   As Boolean
Dim Partenza_x  As Integer
Dim Partenza_y  As Integer
Dim Valore_x    As Integer
Dim Valore_y    As Integer
  
Private Sub Form_Activate()
  '
  Call ChildActivate(Me)
  Me.Line (0, 0)-(Me.Width * 0.999, Me.Height * 0.998), , B
  '
End Sub

Private Sub Form_Load()
'
Dim i As Integer
'
On Error GoTo errForm_Load
  '
  'NASCONDO I TASTI DELLA SECONDA RIGA
  MDIForm1.SSPanel1.Height = MDIForm1.Picture1(0).Height
  For i = 0 To 7
    MDIForm1.Picture3(i).Visible = False
  Next i
  Me.Width = g_nBeArtWidth * 0.99
  Me.Height = g_nBeArtHeight
  Call LEGGI_LINGUA
  Call IMPOSTA_FONT(Me)
  Call RIMF_INIZIALIZZA(Me)
  '
Exit Sub

errForm_Load:
  WRITE_DIALOG Error(Err)
  Resume Next

End Sub

Private Sub Form_Deactivate()
  '
  Call ChildDeActivate(Me)
  '
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  '
  Call GENERA_EVENTO_CLICK(KeyCode, Shift)
  '
End Sub

Private Sub Form_DblClick()
'
On Error Resume Next
  '
  If IMMAGINE.Stretch = True Then
    IMMAGINE.Stretch = False
  Else
    IMMAGINE.Stretch = True
  End If
  '
End Sub

Private Sub IMMAGINE_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
'
On Error Resume Next
  '
  Select Case Button
  Case 1
      Controllo = True
      Partenza_x = X
      Partenza_y = Y
  Case 2
    If (Shift = 1) Then
      IMMAGINE.Width = IMMAGINE.Width * 0.9
      IMMAGINE.Height = IMMAGINE.Height * 0.9
    Else
      IMMAGINE.Width = IMMAGINE.Width * 1.1
      IMMAGINE.Height = IMMAGINE.Height * 1.1
    End If
  End Select
  '
End Sub

Private Sub IMMAGINE_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
'
On Error Resume Next
  '
  If (Controllo = True) Then
    Valore_x = X - Partenza_x
    Valore_y = Y - Partenza_y
    IMMAGINE.Left = IMMAGINE.Left + Valore_x
    IMMAGINE.Top = IMMAGINE.Top + Valore_y
  End If
  '
End Sub

Private Sub IMMAGINE_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
'
On Error Resume Next
  '
  Controllo = False
  '
End Sub
