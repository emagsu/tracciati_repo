VERSION 5.00
Begin VB.Form frmDIALOG 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BackColor       =   &H00EBE1D7&
   BorderStyle     =   0  'None
   Caption         =   "WRITE_DIALOG"
   ClientHeight    =   504
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   9996
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   504
   ScaleWidth      =   9996
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton Picture3 
      BackColor       =   &H00EBE1D7&
      Caption         =   " ^ "
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   50
      Style           =   1  'Graphical
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   50
      Width           =   400
   End
   Begin VB.Label lblWRITE_DIALOG 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBE1D7&
      Caption         =   "WRITE_DIALOG"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   500
      Left            =   500
      TabIndex        =   1
      Top             =   0
      Width           =   9000
   End
End
Attribute VB_Name = "frmDIALOG"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  '
  Call GENERA_EVENTO_CLICK(KeyCode, Shift)
  '
End Sub

Sub Picture3_Click()
'
Dim i             As Integer
Dim Nomegruppo    As String
Dim stmp          As String
Dim stmp1         As String
Dim COMANDO       As String
Dim X             As Variant
Dim LIVELLO       As Integer
Dim LIVELLO_MENO  As Integer
Dim ret           As Integer
Dim IndiceTASTO   As Integer
Dim Atmp          As String * 255
Dim NomeMolaAct    As String

'
On Error Resume Next
  '
  'GESTIONE CON OEM1 CARICATA
  stmp = GetInfo("OEM1", "CARICATA", Path_LAVORAZIONE_INI)
  stmp = UCase$(Trim$(stmp))
  If (stmp = "Y") And (Len(PathDB_ADDIN) > 0) Then
    IndiceTASTO = val(GetInfo("OEM1", "IndiceTASTO", Path_LAVORAZIONE_INI))
    LIVELLO = GetInfo("OEM1", "LIVELLO", Path_LAVORAZIONE_INI)
    LIVELLO = val(LIVELLO)
    LIVELLO_MENO = LIVELLO - 1
    '
    stmp1 = GetInfo("OEM1", "LIVELLO(" & Format$(IndiceTASTO, "0") & ",0)", Path_LAVORAZIONE_INI)
    If (Len(stmp1) > 0) Then
    If (LIVELLO_MENO < 0) Then LIVELLO_MENO = 0
    Else
    If (LIVELLO_MENO < 1) Then LIVELLO_MENO = 1
    End If
    '
    If (LIVELLO_MENO <> LIVELLO) Then
      ret = WritePrivateProfileString("OEM1", "LIVELLO", Format$(LIVELLO_MENO, "0"), Path_LAVORAZIONE_INI)
      Call OEM1_VERTICALI(IndiceTASTO, LIVELLO_MENO)
      Select Case LIVELLO_MENO
        Case 2: Call INIZIALIZZA_OEM1_OFFLINE
        Case 1
            'Nomegruppo = GetInfo("OEM1", "NomeGRP(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
            'Nomegruppo = UCase(Trim(Nomegruppo))
            NomeMolaAct = Trim$(UCase$(OEM1.FraPARAMETRI.Caption))
            stmp1 = GetInfo("OEM1", "NomeARC(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
        
            ret = LoadString(g_hLanguageLibHandle, 1002, Atmp, 255)
            If (ret > 0) Then stmp = Left$(Atmp, ret)
            stmp = stmp & " " & NomeMolaAct & "--->" & stmp1 & " ? "
            StopRegieEvents
            ret = MsgBox(stmp, vbYesNoCancel Or vbQuestion Or vbDefaultButton1, "WARNING ")
            ResumeRegieEvents
           
            Select Case ret
              Case vbYes 'CAMBIO DI STATO CON SALVATAGGIO DEI DATI IN ARCHIVIO
                   Call OEM1_SALVARE_OFFLINE(False)
                   Call INIZIALIZZA_OEM1_ARCHIVIO
              
              Case vbNo
                   Call INIZIALIZZA_OEM1_ARCHIVIO
              
              Case vbCancel 'RESTO NELLO STATO OFFLINE DI MODIFICA
                   WRITE_DIALOG "Operation aborted by user!!!"
              
            End Select
        Case 0
          OEM1.FraPARAMETRI.Visible = True
          OEM1.fraARCHIVIO.Visible = False
          OEM1.PicGRUPPO_HD.Visible = False
          Call INIZIALIZZA_OEM1_CNC
      End Select
    Else
      If (frmDIALOG.Picture3.Enabled = True) Then
        'NASCONDO I TASTI DELLA SECONDA RIGA
        MDIForm1.SSPanel1.Height = MDIForm1.Picture1(0).Height
        For i = 0 To 7
          MDIForm1.Picture3(i).Caption = ""
          MDIForm1.Picture3(i).Picture = LoadPicture("")
          MDIForm1.Picture3(i).Visible = False
        Next i
        Call ESEGUI_FUNZIONE(16)
        Call SetFocusFRM
      End If
    End If
  Else
    'GESTIONE NORMALE
    If (frmDIALOG.Picture3.Enabled = True) Then
'      'NASCONDO I TASTI DELLA SECONDA RIGA
'      MDIForm1.SSPanel1.Height = MDIForm1.Picture1(0).Height
'      For i = 0 To 7
'        MDIForm1.Picture3(i).Caption = ""
'        MDIForm1.Picture3(i).Picture = LoadPicture("")
'        MDIForm1.Picture3(i).Visible = False
'      Next i
      Call ESEGUI_FUNZIONE(16)
      Call SetFocusFRM
    End If
  End If
  '
End Sub

