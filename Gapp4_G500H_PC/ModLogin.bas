Attribute VB_Name = "ModLogin"
Option Explicit

Private Declare Function GetVolumeInformation Lib "Kernel32" Alias "GetVolumeInformationA" (ByVal lpRootPathName As String, ByVal lpVolumeNameBuffer As String, ByVal nVolumeNameSize As Long, lpVolumeSerialNumber As Long, lpMaximumComponentLength As Long, lpFileSystemFlags As Long, ByVal lpFileSystemNameBuffer As String, ByVal nFileSystemNameSize As Long) As Long
Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hwnd As Long, ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long
Private Declare Function GetWindowsDirectory Lib "Kernel32" Alias "GetWindowsDirectoryA" (ByVal lpBuffer As String, ByVal nSize As Long) As Long

Const PathLen = 260
Public GiorniPW As Integer

Dim DATA_PRIMO   As String
Dim DATA_ULTIMO  As String
'

Public Function ControllaDataUltimoLogin()
'
Dim MyAnno     As String
Dim MyMese     As String
Dim MyGiorno   As String
Dim MyTime     As String
Dim MyData     As String
Dim GiorniDiff As Long
Dim TimeAct    As String
  '
  ControllaDataUltimoLogin = False
  '
  'File dove viene scritta la data dell'ultimo login
  If (Dir(DATA_ULTIMO) = "") Then
    Call ScriviDataUltimoLogin
    ControllaDataUltimoLogin = True
    Exit Function
  End If
  '
  MyAnno = GetInfo("DATA", "MyAnno", DATA_ULTIMO)
  MyMese = GetInfo("DATA", "MyMese", DATA_ULTIMO)
  MyGiorno = GetInfo("DATA", "MyGiorno", DATA_ULTIMO)
  MyTime = GetInfo("DATA", "MyTime", DATA_ULTIMO)
  '
  MyData = MyGiorno & "/" & MyMese & "/" & MyAnno
  GiorniDiff = DateDiff("y", MyData, Now)
  If (GiorniDiff > 0) Then
      ControllaDataUltimoLogin = True
      Exit Function
  End If
  If (GiorniDiff < 0) Then
      ControllaDataUltimoLogin = False
      Exit Function
  End If
  If (GiorniDiff = 0) Then
    TimeAct = Time
    If (TimeValue(TimeAct) > TimeValue(MyTime)) Then
      ControllaDataUltimoLogin = True
      Exit Function
    Else
      ControllaDataUltimoLogin = False
      Exit Function
    End If
  End If
  '
End Function

Public Function ControllaData6(AppGiorno As String, AppMese As String, AppAnno As String) As Boolean
'
Dim MyData      As String
Dim GiorniDiff  As Long
  '
  MyData = AppGiorno & "/" & AppMese & "/" & AppAnno
  GiorniDiff = DateDiff("y", MyData, Now)
  If (GiorniDiff > 183) Then
    ControllaData6 = False
    GiorniPW = 0
  Else
    ControllaData6 = True
    GiorniPW = 183 - GiorniDiff
  End If
  '
End Function

Public Function ControllaData12(AppGiorno As String, AppMese As String, AppAnno As String) As Boolean
'
Dim MyData      As String
Dim GiorniDiff  As Long
  '
  MyData = AppGiorno & "/" & AppMese & "/" & AppAnno
  GiorniDiff = DateDiff("y", MyData, Now)
  If (GiorniDiff > 365) Then
    ControllaData12 = False
    GiorniPW = 0
  Else
    ControllaData12 = True
    GiorniPW = 365 - GiorniDiff
  End If
  '
End Function

Public Function ControllaData18(AppGiorno As String, AppMese As String, AppAnno As String) As Boolean
'
Dim MyData      As String
Dim GiorniDiff  As Long
  '
  MyData = AppGiorno & "/" & AppMese & "/" & AppAnno
  GiorniDiff = DateDiff("y", MyData, Now)
  If (GiorniDiff > 548) Then
    ControllaData18 = False
    GiorniPW = 0
  Else
    ControllaData18 = True
    GiorniPW = 548 - GiorniDiff
  End If
  '
End Function

Public Function ControlloPwDefinitiva(Codice As String) As Boolean
'
Dim NSeriale     As Long
Dim StrSeriale   As String
Dim Car1         As String
Dim Car2         As String
Dim Num1         As Integer
Dim Num2         As Integer
Dim PWDefinitiva As String
  '
  NSeriale = DammiSerialNumber
  StrSeriale = str(NSeriale)
  StrSeriale = Int(StrSeriale / 2)
  Car1 = Left(StrSeriale, 1)
  Car2 = Right(StrSeriale, 1)
  Num1 = val(Car1) + 3
  If (Num1 > 9) Then Num1 = Num1 - 10
  Num2 = val(Car2) + 4
  If (Num2 > 9) Then Num2 = Num2 - 10
  PWDefinitiva = StrSeriale + Format(Num1, "0") + Format(Num2, "0")
  If (Codice = PWDefinitiva) Then
    ControlloPwDefinitiva = True
    GiorniPW = 9999
  Else
    ControlloPwDefinitiva = False
    GiorniPW = 0
  End If
  '
End Function

Public Function DammiSerialNumber() As Long
'
Dim AppStr     As String
Dim AppValore  As Long
Dim MyIndice   As Integer
Dim MyStringa  As String
  '
  MyStringa = GetSN
  If Len(MyStringa) > 8 Then MyStringa = Right(MyStringa, 8)
  For MyIndice = 1 To Len(MyStringa)
    AppStr = Mid(MyStringa, MyIndice, 1)
    AppValore = AppValore + (Asc(AppStr) * 10 ^ (MyIndice - 1))
  Next
  DammiSerialNumber = AppValore
  '
End Function

Public Sub ScriviDataUltimoLogin()
'
Dim MyAnno          As String
Dim MyMese          As String
Dim MyGiorno        As String
Dim ret             As Long
  '
  MyAnno = Year(Now):  MyMese = Month(Now):  MyGiorno = Day(Now)
  '
  ret = WritePrivateProfileString("DATA", "MyAnno", MyAnno, DATA_ULTIMO)
  ret = WritePrivateProfileString("DATA", "MyMese", MyMese, DATA_ULTIMO)
  ret = WritePrivateProfileString("DATA", "MyGiorno", MyGiorno, DATA_ULTIMO)
  ret = WritePrivateProfileString("DATA", "MyTime", Time, DATA_ULTIMO)
  '
End Sub

Public Function EsaminaPassword(ByVal PwLOCALE As String) As Byte
'
Dim PwDefOk           As Boolean
Dim PwTemp6Ok         As Boolean
Dim PwTemp12Ok        As Boolean
Dim PwTemp18Ok        As Boolean
Dim PwResetOk         As Boolean
'
Dim AppStr            As String
Dim AppStr2           As String
'
Dim ControlloData6Ok  As Boolean
Dim ControlloData12Ok As Boolean
Dim ControlloData18Ok As Boolean
'
Dim MyAnno            As String
Dim MyMese            As String
Dim MyGiorno          As String
'
Dim AppResult         As Boolean
Dim AppControllo
Dim ret               As Long
  '
  'File dove viene scritta la data dell'ultimo login
  DATA_ULTIMO = App.Path & "\htyn.dll"
  'File dove viene scritta la data di primo accesso
  DATA_PRIMO = App.Path & "\wtnj.dll"
  '
  AppControllo = ControllaDataUltimoLogin
  If (AppControllo = False) Then
    EsaminaPassword = 1
    Exit Function
  End If
  EsaminaPassword = 0
  '
  PwDefOk = ControlloPwDefinitiva(PwLOCALE)
  PwTemp6Ok = ControlloPwTemporanea6(PwLOCALE)
  PwTemp12Ok = ControlloPwTemporanea12(PwLOCALE)
  PwTemp18Ok = ControlloPwTemporanea18(PwLOCALE)
  PwResetOk = ControlloPwReset(PwLOCALE)
  '
  If (PwResetOk = True) Then
    '
    If Dir(DATA_PRIMO) <> "" Then Kill DATA_PRIMO
    If Dir(DATA_ULTIMO) <> "" Then Kill DATA_ULTIMO
    '
    'Primo inserimento di una password temporanea
    MyAnno = Year(Now): MyMese = Month(Now): MyGiorno = Day(Now)
    '
    ret = WritePrivateProfileString("DATA", "MyAnno", MyAnno, DATA_PRIMO)
    ret = WritePrivateProfileString("DATA", "MyMese", MyMese, DATA_PRIMO)
    ret = WritePrivateProfileString("DATA", "MyGiorno", MyGiorno, DATA_PRIMO)
    ret = WritePrivateProfileString("DATA", "MyTime", Time, DATA_PRIMO)
    '
    EsaminaPassword = 3
    Exit Function
  End If
  '
  If (PwDefOk = False) And (PwTemp6Ok = False) And (PwTemp12Ok = False) And (PwTemp18Ok = False) Then
    'Password errata
    Exit Function
  End If
  '
  If (PwDefOk = True) Then
    'Password corretta
    EsaminaPassword = 2
    Exit Function
  End If
  '
  If (Dir(DATA_PRIMO) = "") Then
    '
    'Primo inserimento di una password temporanea
    MyAnno = Year(Now): MyMese = Month(Now): MyGiorno = Day(Now)
    '
    ret = WritePrivateProfileString("DATA", "MyAnno", MyAnno, DATA_PRIMO)
    ret = WritePrivateProfileString("DATA", "MyMese", MyMese, DATA_PRIMO)
    ret = WritePrivateProfileString("DATA", "MyGiorno", MyGiorno, DATA_PRIMO)
    '
    EsaminaPassword = 2
    GiorniPW = 9999
    Exit Function
    '
  Else
    '
    MyAnno = GetInfo("DATA", "MyAnno", DATA_PRIMO)
    MyMese = GetInfo("DATA", "MyMese", DATA_PRIMO)
    MyGiorno = GetInfo("DATA", "MyGiorno", DATA_PRIMO)
    '
    If (PwTemp6Ok = True) Then
      AppResult = ControllaData6(MyGiorno, MyMese, MyAnno)
      If (AppResult = True) Then
        'Password 6 mesi non scaduta
        EsaminaPassword = 2
        Exit Function
      Else
        'Password 6 mesi scaduta
        EsaminaPassword = 1
        Exit Function
      End If
    End If
    '
    If (PwTemp12Ok = True) Then
      AppResult = ControllaData12(MyGiorno, MyMese, MyAnno)
      If (AppResult = True) Then
        'Password 12 mesi non scaduta
        EsaminaPassword = 2
        Exit Function
      Else
        'Password 12 mesi scaduta
        EsaminaPassword = 1
        Exit Function
      End If
    End If
    '
    If (PwTemp18Ok = True) Then
      AppResult = ControllaData18(MyGiorno, MyMese, MyAnno)
      If (AppResult = True) Then
        'Password 18 mesi non scaduta
        EsaminaPassword = 2
        Exit Function
      Else
        'Password 18 mesi scaduta
        EsaminaPassword = 1
        Exit Function
      End If
    End If
    '
  End If
  '
End Function

Public Function ControlloPwTemporanea6(Codice As String) As Boolean
'
Dim NSeriale      As Long
Dim StrSeriale    As String
Dim Car1          As String
Dim Car2          As String
Dim Num1          As Integer
Dim Num2          As Integer
Dim PWTemporanea6 As String
  '
  NSeriale = DammiSerialNumber
  StrSeriale = str(NSeriale)
  StrSeriale = Int(StrSeriale / 3)
  Car1 = Left(StrSeriale, 1)
  Car2 = Right(StrSeriale, 1)
  Num1 = val(Car1) + 3
  If (Num1 > 9) Then Num1 = Num1 - 10
  Num2 = val(Car2) + 4
  If (Num2 > 9) Then Num2 = Num2 - 10
  PWTemporanea6 = StrSeriale + Format(Num1, "0") + Format(Num2, "0")
  If (Codice = PWTemporanea6) Then
    ControlloPwTemporanea6 = True
  Else
    ControlloPwTemporanea6 = False
  End If
  '
End Function

Public Function ControlloPwReset(Codice As String) As Boolean
'
Dim NSeriale    As Long
Dim StrSeriale  As String
Dim Car1        As String
Dim Car2        As String
Dim Num1        As Integer
Dim Num2        As Integer
Dim PWReset     As String
  '
  NSeriale = DammiSerialNumber
  StrSeriale = str(NSeriale)
  StrSeriale = Int(StrSeriale / 5)
  Car1 = Left(StrSeriale, 1)
  Car2 = Right(StrSeriale, 1)
  Num1 = val(Car1) + 3
  If (Num1 > 9) Then Num1 = Num1 - 10
  Num2 = val(Car2) + 4
  If (Num2 > 9) Then Num2 = Num2 - 10
  PWReset = StrSeriale + Format(Num1, "0") + Format(Num2, "0")
  If (Codice = PWReset) Then
    ControlloPwReset = True
  Else
    ControlloPwReset = False
  End If
  '
End Function

Public Function ControlloPwTemporanea12(Codice As String) As Boolean
'
Dim NSeriale        As Long
Dim StrSeriale      As String
Dim Car1            As String
Dim Car2            As String
Dim Num1            As Integer
Dim Num2            As Integer
Dim PWTemporanea12  As String
  '
  NSeriale = DammiSerialNumber
  StrSeriale = str(NSeriale)
  StrSeriale = Int(StrSeriale * 1.1)
  Car1 = Left(StrSeriale, 1)
  Car2 = Right(StrSeriale, 1)
  Num1 = val(Car1) + 4
  If (Num1 > 9) Then Num1 = Num1 - 10
  Num2 = val(Car2) + 5
  If (Num2 > 9) Then Num2 = Num2 - 10
  PWTemporanea12 = StrSeriale + Format(Num1, "0") + Format(Num2, "0")
  If (Codice = PWTemporanea12) Then
    ControlloPwTemporanea12 = True
  Else
    ControlloPwTemporanea12 = False
  End If
  '
End Function

Public Function ControlloPwTemporanea18(Codice As String) As Boolean
'
Dim NSeriale        As Long
Dim StrSeriale      As String
Dim Car1            As String
Dim Car2            As String
Dim Num1            As Integer
Dim Num2            As Integer
Dim PWTemporanea18  As String
  '
  NSeriale = DammiSerialNumber
  StrSeriale = str(NSeriale)
  StrSeriale = Int(StrSeriale / 6)
  Car1 = Left(StrSeriale, 1)
  Car2 = Right(StrSeriale, 1)
  Num1 = val(Car1) + 5
  If (Num1 > 9) Then Num1 = Num1 - 10
  Num2 = val(Car2) + 6
  If (Num2 > 9) Then Num2 = Num2 - 10
  PWTemporanea18 = StrSeriale + Format(Num1, "0") + Format(Num2, "0")
  If (Codice = PWTemporanea18) Then
    ControlloPwTemporanea18 = True
  Else
    ControlloPwTemporanea18 = False
  End If
  '
End Function

Public Function WindowsPath() As String

Dim sFolder As String
Dim lResult As Long
    
  sFolder = String(PathLen, 0)
  lResult = GetWindowsDirectory(sFolder, PathLen)
  If (lResult <> 0) Then
    WindowsPath = Left(sFolder, InStr(sFolder, Chr(0)) - 1)
  Else
    WindowsPath = ""
  End If

End Function
