VERSION 5.00
Begin VB.Form MNG2 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BackColor       =   &H00EBE1D7&
   BorderStyle     =   0  'None
   Caption         =   "MNG2"
   ClientHeight    =   7200
   ClientLeft      =   2136
   ClientTop       =   1428
   ClientWidth     =   12000
   ForeColor       =   &H80000008&
   Icon            =   "Mng2.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7200
   ScaleWidth      =   12000
   ShowInTaskbar   =   0   'False
   Tag             =   "MNG2"
   Begin VB.Frame fra_SELEZIONE 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBE1D7&
      Caption         =   "fra_SELEZIONE"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   5300
      Left            =   2760
      TabIndex        =   6
      Top             =   1800
      Visible         =   0   'False
      Width           =   4500
      Begin VB.FileListBox FILE_SELEZIONE 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1164
         Left            =   50
         Pattern         =   "*.mdb"
         TabIndex        =   9
         Top             =   3850
         Width           =   4400
      End
      Begin VB.DirListBox DIRECTORY_SELEZIONE 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3096
         Left            =   50
         TabIndex        =   8
         Top             =   720
         Width           =   4400
      End
      Begin VB.DriveListBox DRIVE_SELEZIONE 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   324
         Left            =   50
         TabIndex        =   7
         Top             =   315
         Width           =   4400
      End
   End
   Begin VB.ComboBox McCombo1 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.4
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   5650
      TabIndex        =   5
      Text            =   "McCombo1"
      Top             =   15
      Width           =   3700
   End
   Begin VB.ListBox List1 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   10.2
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4056
      Index           =   1
      Left            =   4800
      MultiSelect     =   2  'Extended
      Sorted          =   -1  'True
      TabIndex        =   1
      Top             =   1000
      Width           =   4500
   End
   Begin VB.ListBox List1 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   10.2
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4056
      Index           =   0
      Left            =   100
      MultiSelect     =   2  'Extended
      Sorted          =   -1  'True
      TabIndex        =   0
      Top             =   1000
      Width           =   4500
   End
   Begin VB.Image Image4 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   408
      Left            =   108
      Picture         =   "Mng2.frx":030A
      Top             =   5400
      Width           =   408
   End
   Begin VB.Label lblERROR 
      BackColor       =   &H00EBE1D7&
      Caption         =   "lblERROR"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.4
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   400
      Left            =   1100
      TabIndex        =   4
      Top             =   5400
      Width           =   7050
   End
   Begin VB.Image Image3 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   408
      Left            =   108
      Picture         =   "Mng2.frx":074C
      Top             =   36
      Width           =   408
   End
   Begin VB.Image Image2 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   408
      Left            =   4800
      Picture         =   "Mng2.frx":0B8E
      Top             =   12
      Width           =   408
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H000000FF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "MS Song"
         Size            =   8.4
         Charset         =   134
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   415
      Index           =   1
      Left            =   4800
      TabIndex        =   3
      Top             =   600
      Width           =   4500
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H000000FF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "MS Song"
         Size            =   8.4
         Charset         =   134
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   415
      Index           =   0
      Left            =   100
      TabIndex        =   2
      Top             =   600
      Width           =   4500
   End
   Begin VB.Image Image1 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   615
      Left            =   3950
      Picture         =   "Mng2.frx":0FD0
      Stretch         =   -1  'True
      Top             =   0
      Width           =   645
   End
End
Attribute VB_Name = "MNG2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Sub SELEZIONA_PERCORSO(ByVal NuovoPercorso As String)
'
Dim VecchioPercorso  As String
Dim i                As Integer
Dim ret              As Integer
Dim TIPO_LAVORAZIONE As String
Dim DB               As Database
Dim TB               As Recordset
'
On Error GoTo errSELEZIONA_PERCORSO
  '
  'SELEZIONE DEL PERCORSO
  'CONSIDERO SOLO PERCORSI NON NULLI
  If (Len(NuovoPercorso) > 0) Then
    'CONTROLLO L'ESISTENZA DI FILE
    WRITE_DIALOG "Searching for " & NuovoPercorso
    If (Dir$(NuovoPercorso) <> "") Then
      'VISUALIZZO LA LISTA
      MNG2.Label1(1).Visible = True
      MNG2.List1(1).Visible = True
      'SEGNALAZIONE UTENTE
      WRITE_DIALOG "RD --> " & NuovoPercorso
      'SCRIVO IL NUOVO PERCORSO IN ESPORTAZIONE
      ret = WritePrivateProfileString("Configurazione", "Esportazione", NuovoPercorso, PathFILEINI)
      'LEGGO IL TIPO DI LAVORAZIONE
      TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
      TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
      'APRO IL DATABASE
      Set DB = OpenDatabase(NuovoPercorso, True, False)
      Set TB = DB.OpenRecordset("ARCHIVIO_" & TIPO_LAVORAZIONE)
      'LEGGO TUTTI I NOMI DEI PARAMETRI CHE POSSO SALVARE
      MNG2.List1(1).Clear
      Do Until TB.EOF
        Call MNG2.List1(1).AddItem(" " & TB.Fields("NOME_PEZZO").Value)
        TB.MoveNext
      Loop
      'CHIUDO IL DATABASE
      TB.Close
      DB.Close
    Else
      'SEGNALAZIONE ERRORE
      WRITE_DIALOG NuovoPercorso & " " & Error(53)
      'RIMETTO A POSTO LE COSE COME STAVANO
      VecchioPercorso = GetInfo("Configurazione", "Esportazione", PathFILEINI)
      VecchioPercorso = Trim$(UCase$(VecchioPercorso))
      For i = 0 To McCombo1.ListCount - 1
        If (McCombo1.List(i) = VecchioPercorso) Then
          McCombo1.ListIndex = i
          Exit For
        End If
      Next i
    End If
  Else
    WRITE_DIALOG "NULL PATH"
  End If
  '
Exit Sub

errSELEZIONA_PERCORSO:
  MNG2.lblERROR.Caption = Error(Err)
  WRITE_DIALOG "SUB SELEZIONA_PERCORSO: ERROR " & Err
  MNG2.Label1(1).Visible = False
  MNG2.List1(1).Visible = False
  Exit Sub
  '
End Sub

Private Sub DIRECTORY_SELEZIONE_Change()
'
On Error Resume Next
  '
  MNG2.FILE_SELEZIONE.Path = MNG2.DIRECTORY_SELEZIONE.Path
  MNG2.McCombo1.Text = MNG2.DIRECTORY_SELEZIONE.Path
  MNG2.lblERROR.Caption = MNG2.DIRECTORY_SELEZIONE.Path
  'SEGNALAZIONE UTENTE
  WRITE_DIALOG ""
  '
End Sub

Private Sub DRIVE_SELEZIONE_Change()
'
On Error Resume Next
  '
  MNG2.DIRECTORY_SELEZIONE.Path = MNG2.DRIVE_SELEZIONE.Drive
  MNG2.FILE_SELEZIONE.Path = MNG2.DIRECTORY_SELEZIONE.Path
  MNG2.McCombo1.Text = MNG2.DIRECTORY_SELEZIONE.Path
  MNG2.lblERROR.Caption = MNG2.DIRECTORY_SELEZIONE.Path
  'SEGNALAZIONE UTENTE
  WRITE_DIALOG ""
  '
End Sub

Private Sub FILE_SELEZIONE_Click()
'
Dim stmp As String
'
On Error Resume Next
  '
  stmp = MNG2.FILE_SELEZIONE.Path
  If (Right$(stmp, 1) = "\") Then
    MNG2.McCombo1.Text = MNG2.FILE_SELEZIONE.Path & MNG2.FILE_SELEZIONE.List(MNG2.FILE_SELEZIONE.ListIndex)
  Else
    MNG2.McCombo1.Text = MNG2.FILE_SELEZIONE.Path & "\" & MNG2.FILE_SELEZIONE.List(MNG2.FILE_SELEZIONE.ListIndex)
  End If
  MNG2.lblERROR.Caption = MNG2.McCombo1.Text
  MNG2.McCombo1.SetFocus
  'SEGNALAZIONE UTENTE
  WRITE_DIALOG "PRESS INPUT TO SELECT PATH"
  '
End Sub

Private Sub Form_Activate()
  '
  MNG2.List1(0).BackColor = &HFFFFFF
  MNG2.List1(1).BackColor = &HFFFFFF
  MNG2.McCombo1.BackColor = &HFFFFFF
  MNG2.DRIVE_SELEZIONE.BackColor = &HFFFFFF
  MNG2.DIRECTORY_SELEZIONE.BackColor = &HFFFFFF
  MNG2.FILE_SELEZIONE.BackColor = &HFFFFFF
  MNG2.McCombo1.Left = MNG2.Image2.Left + MNG2.Image2.Width
  MNG2.McCombo1.Width = MNG2.List1(1).Width - MNG2.Image2.Width
  Call ChildActivate(Me)
  '
End Sub

Private Sub Form_Deactivate()
  '
  Call ChildDeActivate(Me)
  '
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  '
  Call GENERA_EVENTO_CLICK(KeyCode, Shift)
  '
End Sub

Private Sub Form_Load()
'
Dim stmp As String
Dim i    As Integer
'
On Error GoTo errForm_Load
  '
  'NASCONDO I TASTI DELLA SECONDA RIGA
  MDIForm1.SSPanel1.Height = MDIForm1.Picture1(0).Height
  For i = 0 To 7
    MDIForm1.Picture3(i).Visible = False
  Next i
  Call AlMakeBorder(hwnd)
  Call Move(g_nBeArtLeft, g_nBeArtTop, g_nBeArtWidth, g_nBeArtHeight)
  Call InitEnvironment(Me)
  Image1.Picture = LoadPicture(g_chOemPATH & "\logo1.bmp")
  Image4.Picture = LoadPicture(g_chOemPATH & "\Gapp4\images\SYNCHRO1.bmp")
  Image2.Picture = LoadPicture(g_chOemPATH & "\Gapp4\images\archive3.bmp")
  Image3.Picture = LoadPicture(g_chOemPATH & "\Gapp4\images\archive1.bmp")
  ACTUAL_PARAMETER_GROUP = ""
  Call IMPOSTA_FONT(Me)
  Call INIZIALIZZA_MNG2
  Dim ret       As Integer
  Dim nDATABASE As String
  Dim PERCORSO  As String
  'LEGGO IL PERCORSO DEL DATABASE SORGENTE
  nDATABASE = GetInfo("CONFIGURAZIONE", "Esportazione", PathFILEINI)
  nDATABASE = UCase$(Trim$(nDATABASE))
  MNG2.DRIVE_SELEZIONE.Drive = nDATABASE
  i = InStr(nDATABASE, "\")
  While (i <> 0)
      PERCORSO = Left$(nDATABASE, i - 1)
      'MsgBox Left$(nDATABASE, i - 1)
      i = InStr(i + 1, nDATABASE, "\")
  Wend
  'MsgBox PERCORSO
  MNG2.DIRECTORY_SELEZIONE.Path = PERCORSO
  Call Change_SkTextOnScr(1, "BACKUP")
  Call Change_SkTextOnScr(14, "\\LOAD1.BMP")
  '
Exit Sub

errForm_Load:
  WRITE_DIALOG Error(Err)
  Resume Next

End Sub

Private Sub Image2_Click()
'
Dim Esportazione As String
'
On Error Resume Next
  '
  If (MNG2.fra_SELEZIONE.Visible = False) Then
    MNG2.fra_SELEZIONE.Move MNG2.Label1(1).Left, MNG2.Label1(1).Top
    MNG2.fra_SELEZIONE.Visible = True
    MNG2.fra_SELEZIONE.Caption = "SELECT DATABASE WITH MOUSE"
    Esportazione = GetInfo("Configurazione", "Esportazione", PathFILEINI)
    Esportazione = UCase$(Trim$(Esportazione))
    MNG2.DRIVE_SELEZIONE.Drive = Esportazione
    MNG2.DIRECTORY_SELEZIONE.Path = Esportazione
    MNG2.FILE_SELEZIONE.Path = Esportazione
  Else
    MNG2.fra_SELEZIONE.Visible = False
  End If
  '
End Sub

Private Sub List1_GotFocus(Index As Integer)
'
Dim i As Integer
'
On Error GoTo errList1_GotFocus
  '
  Select Case Index
    '
    Case 0
      'RIMETTO A POSTO L'ALTRA LISTA
      MNG2.Label1(1).BackColor = QBColor(10)
      MNG2.Label1(1).ForeColor = QBColor(0)
      MNG2.List1(1).ListIndex = -1
      For i = 0 To MNG2.List1(1).ListCount - 1
        MNG2.List1(1).Selected(i) = False
      Next i
      'EVIDENZIO LA LISTA
      MNG2.Label1(0).BackColor = QBColor(12)
      MNG2.Label1(0).ForeColor = QBColor(15)
      '
    Case 1
      'RIMETTO A POSTO L'ALTRA LISTA
      MNG2.Label1(0).BackColor = QBColor(10)
      MNG2.Label1(0).ForeColor = QBColor(0)
      MNG2.List1(0).ListIndex = -1
      For i = 0 To MNG2.List1(0).ListCount - 1
        MNG2.List1(0).Selected(i) = False
      Next i
      'EVIDENZIO LA LISTA
      MNG2.Label1(1).BackColor = QBColor(12)
      MNG2.Label1(1).ForeColor = QBColor(15)
      '
  End Select
  '
Exit Sub

errList1_GotFocus:
  WRITE_DIALOG "Sub List1_GotFocus: Error -> " & Err
  Resume Next
  
End Sub

Private Sub List1_KeyUp(Index As Integer, KeyCode As Integer, Shift As Integer)
'
Dim NomePezzo As String
Dim DATA_SALVATAGGIO As String
Dim DB        As Database
Dim TB        As Recordset
Dim stmp      As String
Dim TIPO_LAVORAZIONE As String
Dim nDATABASE As String
'
On Error GoTo errList1_KeyUp
  '
  If MNG2.List1(Index).ListIndex > -1 Then
    Select Case KeyCode
      Case 33
        'INIZIO LISTA
        GoSub SCRIVI_DATA
      Case 34
        'FINE LISTA
        GoSub SCRIVI_DATA
      Case 40
        'AVANTI
        GoSub SCRIVI_DATA
      Case 38
        'INDIETRO
        GoSub SCRIVI_DATA
    End Select
  End If
  '
Exit Sub

errList1_KeyUp:
  WRITE_DIALOG Error(Err)
  Exit Sub

SCRIVI_DATA:
  'LEGGO IL PERCORSO DEL DATABASE
  If Index = 1 Then
    'REMOTO
    nDATABASE = GetInfo("CONFIGURAZIONE", "Esportazione", PathFILEINI)
    nDATABASE = UCase$(Trim$(nDATABASE))
  Else
    'LOCALE
    nDATABASE = PathDB
  End If
  WRITE_DIALOG nDATABASE & " (reading....)"
  'ASSEGNO IL NOME DEL PEZZO SELEZIONATO
  NomePezzo = MNG2.List1(Index).List(MNG2.List1(Index).ListIndex)
  NomePezzo = UCase$(Trim$(NomePezzo))
  'LEGGO IL TIPO DI LAVORAZIONE
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  'COSTRUISCO IL NOME DELL'ARCHIVIO
  stmp = "ARCHIVIO_" & TIPO_LAVORAZIONE
  'APRO IL DATABASE
  Set DB = OpenDatabase(nDATABASE, True, False)
  Set TB = DB.OpenRecordset(stmp)
  'RICERCO NELLA TABELLA ARCHIVIO SE ESISTE
  TB.Index = "NOME_PEZZO"
  TB.Seek "=", NomePezzo
  If (Not TB.NoMatch) Then
    DATA_SALVATAGGIO = TB.Fields("DATA_ARCHIVIAZIONE").Value
    WRITE_DIALOG MNG2.List1(Index).List(MNG2.List1(Index).ListIndex) & " : " & DATA_SALVATAGGIO
  End If
  TB.Close
  DB.Close
Return

End Sub

Private Sub McCombo1_Click()
'
On Error Resume Next
  '
  'SEGNALAZIONE UTENTE
  WRITE_DIALOG "PRESS INPUT TO SELECT PATH"
  MNG2.lblERROR.Caption = UCase$(Trim$(McCombo1.Text))
  '
End Sub

Private Sub McCombo1_KeyUp(KeyCode As Integer, Shift As Integer)
'
On Error Resume Next
  '
  If (KeyCode = 13) Then
    'PULISCO LA BARRA DEI MESSAGGI
    WRITE_DIALOG ""
    'PULISCO LA FINESTRA DEGLI ERRORI
    MNG2.lblERROR.Caption = ""
    Call SELEZIONA_PERCORSO(UCase$(Trim$(McCombo1.Text)))
  Else
    'SEGNALAZIONE UTENTE
    WRITE_DIALOG "PRESS INPUT TO SELECT PATH"
    MNG2.lblERROR.Caption = UCase$(Trim$(McCombo1.Text))
  End If
  '
End Sub
