VERSION 5.00
Begin VB.Form frmHeader 
   AutoRedraw      =   -1  'True
   BackColor       =   &H00EBE1D7&
   BorderStyle     =   0  'None
   Caption         =   "Header"
   ClientHeight    =   900
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   9996
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   900
   ScaleWidth      =   9996
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton cmdBACKUP 
      Caption         =   "RUN BACKUP"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   7.8
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   372
      Left            =   2400
      TabIndex        =   1
      Top             =   480
      Visible         =   0   'False
      Width           =   3012
   End
   Begin VB.CommandButton cmdRESTORE 
      Caption         =   "RESTORE BACKUP"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   7.8
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   372
      Left            =   5520
      TabIndex        =   0
      Top             =   480
      Visible         =   0   'False
      Width           =   3012
   End
End
Attribute VB_Name = "frmHeader"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdBACKUP_Click()
'
Dim ret As Long
'
On Error Resume Next
  '
  ret = MsgBox(frmHeader.cmdBACKUP.Caption & " ?", vbCritical + vbYesNo + vbDefaultButton2, "WARNING")
  If (ret = vbYes) Then
    Call BACKUP_ESEGUI
  Else
    WRITE_DIALOG "Operation aborted by user!!!"
  End If
  '
End Sub

Private Sub cmdRESTORE_Click()
'
Dim ret As Long
'
On Error Resume Next
  '
  ret = MsgBox(frmHeader.cmdRESTORE.Caption & " ?", vbCritical + vbYesNo + vbDefaultButton2, "WARNING")
  If (ret = vbYes) Then
    Call BACKUP_RIPRISTINA
    ret = WritePrivateProfileString("Configurazione", "RESTORE_BACKUP", "N", PathFILEINI)
  Else
    WRITE_DIALOG "Operation aborted by user!!!"
  End If
  '
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  '
  Call GENERA_EVENTO_CLICK(KeyCode, Shift)
  '
End Sub
