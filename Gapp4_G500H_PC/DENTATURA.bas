Attribute VB_Name = "DENTATURA"
Option Explicit

Dim Mn    As Double
Dim ZP    As Double
Dim a0n   As Double
Dim b0n   As Double
Dim LD    As Double
Dim da    As Double
Dim drp   As Double
Dim Dse   As Double
Dim Dpino As Double
Dim DP    As Double
Dim Hr    As Double
Dim D     As Double

Dim Mg  As Double
Dim ag  As Double
Dim g0w As Double
Dim dkw As Double
Dim Hkw As Double
Dim Lu  As Double
Dim N   As Double
Dim N0  As Double
Dim Pn  As Double
Dim Pa  As Double

Dim g0ws  As Double
Dim tC    As Double
Dim TA    As Double
Dim Q     As Double
Dim E     As Double

Dim VcDesb  As Double
Dim VcAcab  As Double
Dim ava_D   As Double
Dim ava_Dm  As Double
Dim ava_A   As Double
Dim ava_Am  As Double
Dim avr_D   As Double
Dim avr_Dm  As Double
Dim avr_A   As Double
Dim avr_Am  As Double

Dim RPMhobD     As Double
Dim RPMhobA     As Double
Dim RPMtavolaD  As Double
Dim RPMtavolaA  As Double

Dim FED   As Double
Dim FEA   As Double
Dim FSD   As Double
Dim FSA   As Double

Dim avFED As Double
Dim avFEA As Double
Dim avFSD As Double
Dim avFSA As Double

Dim b0g   As Double
Dim DPG   As Double
Dim bd    As Double
Dim VPA   As Double
Dim ac    As Double
Dim fz    As Double
Dim ec    As Double
Dim HF    As Double
Dim hp    As Double
Dim X1    As Double
Dim X2    As Double
Dim L     As Double

Dim L1  As Double
Dim L2  As Double
Dim L3  As Double
Dim L3r As Double
Dim Sh  As Double
Dim QSH As Double
Dim i   As Double
Dim QP  As Double
Dim QPP As Double
Dim POT As Double
Dim Fc  As Double
Dim T   As Double

Dim QPAK  As Double
Dim QPA   As Double
Dim k     As Double
Dim Vcr   As Double

Dim TUsind  As Double
Dim TUsina  As Double
Dim Tm      As Double
Dim TCiclo  As Double
Dim Tp      As Double
Dim Ph      As Double
'
'*********************************************************************
'           <<<<<<<<<<        CAL.HOB - SU        >>>>>>>>>>>
'*********************************************************************
'
'   SAMPUTENSILI DO BRASIL Ltda.
'   Software per lo studio delle condizioni di lavorazione
'   Sviluppato da F. Sacchetto
'   Inizio del progetto  07 de junho de 2004
'   Engenheiro de Aplica��es Samputensili
'
'---------------------------------------------------------------------
'          Definizioni dei Termini e Nomenclature Utilizzate
'---------------------------------------------------------------------
'
'   Mn    = Modulo Normale
'   Zp    = Numero dei denti
'   a0n   = Angolo di Pressione
'   b0n   = Angolo dell' Elica
'   Ld    = Larghezza della Dentatura
'   da    = Diametro Esterno
'   drp   = Diametro di Piede
'   Dse   =  Quota rulli/Sfere
'   Dpino = Diametro dei Rulli/Sfere
'   Dp    = Diametro Primitivo
'   hr    = Altezza del dente
'   D     = Durezza in HB del materiale del pezzo
'
'   Mg = Modulo di Generazione
'   ag = Angolo di Pressione di Generazione
'   g0w = Angolo di Elica
'   g0ws = Angolo di Elica Teorico
'   dkw = Diametro esterno dell' utensile
'   hkw = Addendum
'   Lu = Lunghezza utile dell' utensile
'   N = Numero di taglienti
'   N0 = Numero di entrate
'   PN = Passo Normale
'   PA = Passo Assiale
'
'   Tc = Tempo di Caricamento
'   Ta = Tempo di Approssimazione
'   q = Quantit� di pezzi per fissaggio(?)
'   e = Lunghezza del distanziale
'
'   VcDesb  = Velocit� di Taglio  (m/min)  - SGROSSATURA
'   VcAcab  = Velocit� di Taglio  (m/min)  - FINITURA
'   ava_D   = Avanzamento Assiale (mm/rot) - SGROSSATURA
'   ava_Dm  = Avanzamento Assiale (mm/min) - SGROSSATURA
'   ava_A   = Avanzamento Assiale (mm/rot) - FINITURA
'   ava_Am  = Avanzamento Assiale (mm/min) - FINITURA
'   avr_D   = Avanzamento Radiale (mm/rot) - SGROSSATURA
'   avr_Dm  = Avanzamento Radiale (mm/min) - SGROSSATURA
'   avr_A   = Avanzamento Radiale (mm/rot) - FINITURA
'   avr_Am  = Avanzamento Radiale (mm/min) - FINITURA
'
'   RPMhobD = Rotazione dell' Hob - SGROSSATURA
'   RPMhobA = Rotazione dell' Hob - FINITURA
'   RPMtavolaD = Rotazione della tavola - SGROSSATURA
'   RPMtavolaA = Rotazione della tavola - FINITURA
'
'   FED = Fattore di Entrata - SGROSSATURA
'   FEA = Fattore di Entrata - FINITURA
'   FSD = Fattore di Uscita - SGROSSATURA
'   FSA = Fattore di Uscita - FINITURA
'
'   avFED = Avanzamento con Fattore di Entrata - SGROSSATURA
'   avFEA = Avanzamento con Fattore di Entrata - FINITURA
'   avFSD = Avanzamento con Fattore di Uscita - SGROSSATURA
'   avFSA = Avanzamento con Fattore di Uscita - FINITURA
'
'   b0g = Angolo di Elica di Generazione
'   Dpg = Diametro Primitivo di Generazione
'   bd  = Lunghezza lavorata
'   VPA = Lunghezza lavorata Hob
'   AC  = Angolo di incrociamento (Mandrino della Macchina)
'   fz  = Avanzamento per Dente
'   ec  = Spessore del Truciolo
'   hf  = Spessore del sovrametallo
'   hp  = Deviazione nel Profilo
'   X1  = Corsa di Entrata
'   X2  = Corsa di Uscita
'   L   = Corsa Assiale Totale dell' Hob
'
'   L1  = Lunghezza di Entrata
'   L2  = Lunghezza di Uscita
'   L3  = Lunghezza di Shifting
'   L3r = Lunghezza di Shifting Reale
'   SH  = Salto di Shifting
'   QSH = Quantit� di Shifting nella Lunghezza di Shifting
'   I   = Spostamento costante di incremento
'   QP  = Quantit� di Passate
'   QPP = Quantit� di pezzi per Passata
'   P   = Potenza di Taglio
'   Pot = Forza di Taglio
'   T   = Torque
'
'   QPAK = Fattore di Performance
'   QPA  = Quantit� di pezzi per fissaggio(?)
'   K    = Taglio in metri lineari
'
'   XEr = Corsa in mm in Entrata utilizzando Fattore di Avanzamento
'   XSr = Corsa in mm in Uscita utilizzando Fattore di Avanzamento
'   XLd = Corsa in mm percorsa nel taglio completo utilizzando Fattore di Avanzamento
'
'   TFEd   = Tempo di Corsa con Fattore di Entrata nella SGROSSATURA
'   TFEa   = Tempo di Corsa con Fattore di Entrata nella FINITURA
'   TFSd   = Tempo di Corsa con Fattore di Uscita nella SGROSSATURA
'   TFSa   = Tempo di Corsa con Fattore di Uscita nella FINITURA
'   TUsind = Tempo di Lavorazione di SGROSSATURA
'   TUsina = Tempo di Lavorazione di FINITURA
'   TM     = Tempo Macchina
'   TCiclo = Tempo del Ciclo
'   TP     = Tempo Standard
'   Ph     = Pezzi all'ora

Function PAR(valor As Double) As Boolean
'
Dim resultado As Double
Dim RESTO     As Double
  '
  resultado = valor / 2
  RESTO = resultado - Int(resultado)
  '
  If (RESTO = 0) Then
    PAR = True
  Else
    PAR = False
  End If

End Function

Function inv(valor As Double) As Double

  inv = Tan(valor) - valor
  
End Function

Sub CALCOLO_CICLO_DENTATURA()
  '
  'DATI INGRANAGGIO
  Mn = Lp("MN_G")           'Modulo Normale
  ZP = Lp("NUMDENTI_G")     'Numero dei denti
  a0n = Lp("ALFA_G")        'Angolo di Pressione
  b0n = Lp("BETA_G")        'Angolo dell' Elica
  LD = Lp("FASCIATOTALE_G") 'Larghezza della Dentatura
  da = Lp("DEXT_G")         'Diametro Esterno
  drp = Lp("DINT_G")        'Diametro di Piede
  Dse = Lp("QR")            'Quota rulli/Sfere
  Dpino = Lp("DR")          'Diametro dei Rulli/Sfere
  'dp = Mn * ZP / Cos(b0n * 3.14 / 180) 'Diametro Primitivo
  'Hr = (da - drp) / 2       'Altezza del dente
  D = Lp("iDUREZZA")        'Durezza in HB del materiale del pezzo
  '
  '   Mg = Modulo di Generazione
  '   ag = Angolo di Pressione di Generazione
  '   g0w = Angolo di Elica
  '   g0ws = Angolo di Elica Teorico
  '   dkw = Diametro esterno dell' utensile
  '   hkw = Addendum
  '   Lu = Lunghezza utile dell' utensile
  '   N = Numero di taglienti
  '   N0 = Numero di entrate
  '   PN = Passo Normale
  '   PA = Passo Assiale
  '
  Mg = Lp("MNUT_G")         'Modulo di Generazione
  ag = Lp("ALFAUT_G")       'Angolo di Pressione di Generazione
  N0 = Lp("NUMPRINC_G")     'Numero di entrate
  '
Dim DIAMETRO_MOLA          As Double
  '
  DIAMETRO_MOLA = Lp("DIAMMOLA_G")  'Diametro primitivo utensile
  g0w = Mg * N0 / DIAMETRO_MOLA
  g0w = Atn(g0w / Sqr(1 - g0w * g0w)) 'Angolo Elica
  dkw = Lp("DIAMOLAMAX_G")   'Diametro esterno dell' utensile
  '
  'calcolo in fondo SVG271111
  'Hkw = (dkw - DIAMETRO_MOLA) / 2 'Addendum
  '
  Lu = Lp("LARGHMOLA_G")    'Lunghezza utile dell' utensile
  N = Lp("TAGLIENTI")       'Numero di taglienti
  '
  'Set Pn = Cells(32, 10)
  'Set Pa = Cells(34, 10)
  '
  'Set g0ws = Cells(34, 15)
  'Set tC = Cells(36, 15)
  'Set TA = Cells(38, 15)
  'Set Q = Cells(40, 15)
  '
  Q = 1 'Quantit� di pezzi per fissaggio(?)
  '
  'Set E = Cells(42, 15)
  '
Dim Abilitazioni(4) As Integer
  Abilitazioni(1) = Lp("NumPassSGR_G")
  Abilitazioni(2) = Lp("NumPassFIN_G")
  Abilitazioni(3) = Lp("NumPassCiclo3_G")
  Abilitazioni(4) = Lp("NumPassCiclo4_G")
  '
Dim TipoCiclo(4) As Integer
  TipoCiclo(1) = Lp("TipoCicloSG_G")
  TipoCiclo(2) = Lp("TipoCicloFIN_G")
  TipoCiclo(3) = Lp("TipoCicloC3_G")
  TipoCiclo(4) = Lp("TipoCicloC4_G")
  '
Dim VelocitaTaglio(4) As Integer
  VelocitaTaglio(1) = Lp("VelTaglio_G")
  VelocitaTaglio(2) = Lp("VelTaglioFIN_G")
  VelocitaTaglio(3) = Lp("VelTaglioC3_G")
  VelocitaTaglio(4) = Lp("VelTaglioC4_G")
  '
Dim AvaASSIALE(4) As Integer
  AvaASSIALE(1) = Lp("AvanzSGR_G")
  AvaASSIALE(2) = Lp("AvanzFIN_G")
  AvaASSIALE(3) = Lp("AvanzC3_G")
  AvaASSIALE(4) = Lp("AvanzC4_G")
  '
  VcDesb = Lp("VELTAGLIO_G")    'Velocit� di Taglio  (m/min)  - SGROSSATURA
  VcAcab = Lp("VELTAGLIOFIN_G") 'Velocit� di Taglio  (m/min)  - FINITURA
  '
  ava_D = Lp("AVANZSGR_G")  'Avanzamento Assiale (mm/rot) - SGROSSATURA
  ava_A = Lp("AVANZFIN_G")  'Avanzamento Assiale (mm/rot) - FINITURA
  '
  'Set avr_D = Cells(60, 5)
  'Set avr_Dm = Cells(62, 5)
  'Set avr_A = Cells(60, 7)
  'Set avr_Am = Cells(62, 7)
  '
  'Set RPMhobD = Cells(54, 12)
  'Set RPMhobA = Cells(54, 14)
  'Set RPMtavolaD = Cells(56, 12)
  'Set RPMtavolaA = Cells(56, 14)
  '
  'Set FED = Cells(58, 12)
  'Set FEA = Cells(58, 14)
  'Set FSD = Cells(62, 12)
  'Set FSA = Cells(62, 14)
  '
  'Set avFED = Cells(60, 12)
  'Set avFEA = Cells(60, 14)
  'Set avFSD = Cells(64, 12)
  'Set avFSA = Cells(64, 14)
  '
  'Set b0g = Cells(72, 5)
  'Set DPG = Cells(74, 5)
  'Set bd = Cells(76, 5)
  'Set VPA = Cells(78, 5)
  'Set ac = Cells(80, 5)
  'Set fz = Cells(82, 5)
  'Set ec = Cells(84, 5)
  'Set HF = Cells(86, 5)
  'Set hp = Cells(88, 5)
  'Set X1 = Cells(90, 5)
  'Set X2 = Cells(92, 5)
  'Set L = Cells(94, 5)
  '
  'Set L1 = Cells(72, 10)
  'Set L2 = Cells(74, 10)
  'Set L3 = Cells(76, 10)
  'Set L3r = Cells(78, 10)
  'Set Sh = Cells(80, 10)
  '
  'QSH = Lp("QSH_G")
  '
  'Set I = Cells(84, 10)
  'Set QP = Cells(88, 10)
  'Set QPP = Cells(90, 10)
  'Set POT = Cells(92, 10)
  'Set Fc = Cells(94, 10)
  'Set T = Cells(96, 10)
  '
  'Set QPAK = Cells(74, 15)
  'Set QPA = Cells(76, 15)
  '
  'QPAK = Lp("NPMAX_G")
  '
  'Set k = Cells(78, 15)
  'Set Vcr = Cells(80, 15)
  '
  'Set TUsind = Cells(86, 15)
  'Set TUsina = Cells(88, 15)
  'Set Tm = Cells(90, 15)
  'Set TCiclo = Cells(92, 15)
  'Set Tp = Cells(94, 15)
  'Set Ph = Cells(96, 15)
  '
  'COSTANTI E CONVERSIONE DEGLI ANGOLI:
  '
Const PI = 3.14159265358979
  '
  a0n = a0n * (PI / 180)
  b0n = b0n * (PI / 180)
  ag = ag * (PI / 180)
  g0w = g0w * (PI / 180)
  g0ws = g0ws * (PI / 180)
  b0g = b0g * (PI / 180)
  '
  'CONTROLLO DEI DATI PER EFFETTUARE I CALCOLI:
  '
'If Cells(111, 19) = 2 Then
'Cells(133, 18) = FALSO
'End If
  '
Dim msgBASE As String
  '
  msgBASE = "Prego riempire tutti i campi prima di effettuare i calcoli!!" & Chr$(13) & Chr$(13)
  '
If (Mn = 0) Then
  MsgBox msgBASE & " - Modulo normale (Mn) non fornito", vbCritical, "WARNING"
  Exit Sub
End If

If (ZP = 0) Then
  MsgBox msgBASE & " - Numero di denti (Zp) non fornito", vbCritical, "WARNING"
  Exit Sub
End If

If (a0n = 0) Then
  MsgBox msgBASE & " - Angolo de Pressione (a0n) non fornito", vbCritical, "WARNING"
  Exit Sub
End If

'If (b0n = 0) Then
  'MsgBox msgBASE & " - Angolo dell' Elica (b0n) non fornito", vbCritical, "WARNING"
  'Exit Sub
'End If

If (LD = 0) Then
  MsgBox msgBASE & " - Larghezza della dentatura (Ld) non fornito", vbCritical, "WARNING"
  Exit Sub
End If

If (da = 0) Then
  MsgBox msgBASE & " - Diametro esterno (da) non fornito", vbCritical, "WARNING"
  Exit Sub
End If

If (drp = 0) Then
  MsgBox msgBASE & " - Diametro di piede (drp) non fornito", vbCritical, "WARNING"
  Exit Sub
End If

If (D = 0) Then
  MsgBox msgBASE & " - Durezza (D) non fornito", vbCritical, "WARNING"
  Exit Sub
End If

If (ag = 0) Then
  MsgBox msgBASE & " - Ang. Pressione Generazione (ag) non fornito", vbCritical, "WARNING"
  Exit Sub
End If

If (g0w = 0) Then
  MsgBox msgBASE & " - Ang. Elica dell' Hob (g0w) non fornito", vbCritical, "WARNING"
  Exit Sub
End If

If (dkw = 0) Then
  MsgBox msgBASE & " - Diam. esterno dell' Hob (dkw) non fornito", vbCritical, "WARNING"
  Exit Sub
End If

If (Lu = 0) Then
  MsgBox msgBASE & " - Lunghezza Utile (Lu) non fornito", vbCritical, "WARNING"
  Exit Sub
End If

If (N = 0) Then
  MsgBox msgBASE & " - Numeri di Taglienti (N) non fornito", vbCritical, "WARNING"
  Exit Sub
End If

If (N0 = 0) Then
  MsgBox msgBASE & " - Numero di Entrate (N0) non fornito", vbCritical, "WARNING"
  Exit Sub
End If

'If (Q = 0) Then
'MsgBox msgBASE & " - Numero di pezzi por fixa��o (q) non fornito", vbCritical, "WARNING"
'Exit Sub
'End If

'If Cells(54, 5) = "" Then
'MsgBox msgBASE & " - Vc SGROSSATURA non fornito", vbCritical, "WARNING"
'Exit Sub
'End If

'If Cells(111, 19) = 1 Then
'If Cells(54, 7) = "" Then
'MsgBox msgBASE & " - Vc FINITURA non fornito", vbCritical, "WARNING"
'Exit Sub
'End If
'End If

'If Cells(127, 18) = 1 Then
'If ava_D = "" Then
'MsgBox msgBASE & " - av. SGROSSATURA non fornito", vbCritical, "WARNING"
'Exit Sub
'End If
'End If

'If Cells(127, 18) = 2 Then
'If ava_Dm = "" Then
'MsgBox msgBASE & " - av. SGROSSATURA non fornito", vbCritical, "WARNING"
'Exit Sub
'End If
'End If

'If Cells(111, 19) = 1 Then
'If Cells(127, 18) = 1 Then
'If ava_A = "" Then
'MsgBox msgBASE & " - av. FINITURA non fornito", vbCritical, "WARNING"
'Exit Sub
'End If
'End If
'End If

'If Cells(111, 19) = 1 Then
'If Cells(127, 18) = 2 Then
'If ava_Am = "" Then
'MsgBox msgBASE & " - av. FINITURA non fornito", vbCritical, "WARNING"
'Exit Sub
'End If
'End If
'End If

'If Cells(132, 19) = 1 Then
'If avr_D = "" Then
'MsgBox msgBASE & " - av. Radiale sgross. non fornito", vbCritical, "WARNING"
'Exit Sub
'End If
'End If

'If Cells(127, 18) = 2 Then
'If avr_Dm = "" Then
'MsgBox msgBASE & " - av. Radiale sgross. non fornito", vbCritical, "WARNING"
'Exit Sub
'End If
'End If

'If Cells(133, 19) = 1 Then
'If avr_A = "" Then
'MsgBox msgBASE & " - av. Radiale Fin. non fornito", vbCritical, "WARNING"
'Exit Sub
'End If
'End If

'If Cells(127, 18) = 2 Then
'If avr_Am = "" Then
'MsgBox msgBASE & " - av. Radiale Fin. non fornito", vbCritical, "WARNING"
'Exit Sub
'End If
'End If

'If Cells(112, 19) = 2 Then
'FED = 1
'FEA = 1
'End If

'If Cells(112, 19) = 1 Then
'If FED = "" Then
'MsgBox msgBASE & " - Fattore di Entrata Sgross. non fornito", vbCritical, "WARNING"
'Exit Sub
'End If

'If Cells(111, 19) = 1 Then
'If FEA = "" Then
'MsgBox msgBASE & " - Fattore di Entrata Fin. non fornito", vbCritical, "WARNING"
'Exit Sub
'End If
'End If
'End If

'If Cells(113, 19) = 2 Then
'FSD = 1
'FSA = 1
'End If

'If Cells(113, 19) = 1 Then
'If FSD = "" Then
'MsgBox msgBASE & " - Fattore di Uscita Sgross. non fornito", vbCritical, "WARNING"
'Exit Sub
'End If

'If Cells(113, 19) = 1 Then
'If FSA = "" Then
'MsgBox msgBASE & " - Fattore di Uscita Fin. non fornito", vbCritical, "WARNING"
'Exit Sub
'End If
'End If
'End If

'If Cells(114, 19) = 1 Then
'If L3r = "" Then
'MsgBox msgBASE & " - Lungh. Shifting (L3r) non fornito", vbCritical, "WARNING"
'Exit Sub
'End If
'End If

'If Cells(115, 19) = 1 Then
'If QSH = "" Then
'MsgBox msgBASE & " - Qt�. Shifting (QSH) non fornito", vbCritical, "WARNING"
'Exit Sub
'End If
'End If

'If Cells(116, 19) = 1 Then
'If QP = "" Then
'MsgBox msgBASE & " - Qt�. Passate (QP) non fornito", vbCritical, "WARNING"
'Exit Sub
'End If
'End If

'If Cells(135, 19) = 1 Then
'If Dse = "" Then
'MsgBox msgBASE & " - Quota Rulli/Sfere non fornito", vbCritical, "WARNING"
'Exit Sub
'End If
'End If

'If Cells(135, 19) = 1 Then
'If Dpino = "" Then
'MsgBox msgBASE & " - Diametro dei Rulli/Sfere non fornito", vbCritical, "WARNING"
'Exit Sub
'End If
'End If
  '
  'CALCOLI:
  '
Dim X     As Double
Dim Y     As Double
Dim LSV   As Double
Dim G2    As Double
Dim avx   As Double
Dim SH1   As Double
Dim i1    As Double
Dim NC1   As Double
  '
  Hkw = (DPG - drp) / 2
  '
  DP = (Mn * ZP) / Cos(b0n)
  Hr = (da - drp) / 2
  Mg = Mn * (Cos(a0n) / Cos(ag))
  X = Sin(b0n) * Cos(a0n) / Cos(ag)
  Y = ((Mg * N0) / (dkw - (2 * Hkw)))
  g0ws = Atn(Y / Sqr(-Y * Y + 1))
  b0g = Atn(X / Sqr(-X * X + 1))
  DPG = (Mg * ZP) / Cos(b0g)
  '
  Pn = Mg * PI
  Pa = (Pn / Cos(g0w)) * N0
  '
  RPMhobD = (VcDesb * 1000) / (dkw * PI)
  RPMhobA = (VcAcab * 1000) / (dkw * PI)
  RPMtavolaD = Round((RPMhobD * N0 / ZP), 0)
  RPMtavolaA = Round((RPMhobA * N0 / ZP), 0)
  '
'If Cells(127, 18) = 1 Then
  ava_Dm = (ava_D * RPMhobD * N0) / ZP
  ava_Am = (ava_A * RPMhobA * N0) / ZP
'  avr_Dm = (avr_D * RPMhobD * N0) / ZP
'  avr_Am = (avr_A * RPMhobA * N0) / ZP
'Else
'  ava_D = Round(((ava_Dm * ZP) / RPMhobD) / N0, 1)
'  If Cells(111, 19) = 1 Then
'    ava_A = Round(((ava_Am * ZP) / RPMhobA) / N0, 1)
'  End If
'  avr_D = Round(((avr_Dm * ZP) / RPMhobD) / N0, 1)
'  If Cells(133, 19) = 1 Then
'    avr_A = Round(((avr_Am * ZP) / RPMhobA) / N0, 1)
'  End If
'End If
  '
'  avFED = ava_D * FED:  avFEA = ava_A * FEA
'  avFSD = ava_D * FSD:  avFSA = ava_A * FSA
  '
  bd = (Q * LD * ZP) / (1000 * Cos(b0g))
  LSV = Mg * (PI / 2 + Tan(ag) / Cos(g0w))
  G2 = Hkw / Tan(ag)
  ac = Abs(b0g - g0w)
  L1 = (((da - ((da - drp) / 2)) * (da - drp) / 2) ^ 0.5) / Abs(Cos(ac))
  L2 = G2 + (LSV * 1.4)
  L3 = Round(Lu - (L1 + L2))
  '
'If Cells(114, 19) = 1 Then
'  L3 = L3r
'Else
'  L3r = L3
'End If
  '
  'If (QPAK = 0) Then
  '  If Cells(126, 18) = 1 Then
  '    MsgBox ("Inserire quantit� dei pezzi per dentatura")
  '    Exit Sub
  '  Else
  '    If Cells(126, 18) = 2 Then
  '      MsgBox ("Inserire Fattore K")
  '      Exit Sub
  '    End If
  '  End If
  'End If
  '
  'If Cells(126, 18) = 1 Then
  'QPA = QPAK
  'k = ((QPA * bd) / Q / ((L3 * N) / (Mg * PI)))
  'VPA = (L3 * N * k) / (Mg * PI)
  'End If
  '
'If Cells(126, 18) = 2 Then
'  k = QPAK
'  VPA = (L3 * N * k) / (Mg * PI)
'  QPA = (VPA * Q) / bd
'End If
  '
  Dim fz_D As Double
  Dim fz_A As Double
  Dim ec_D As Double
  Dim ec_A As Double
  '
  fz_D = ((ava_D * N0) / ZP) / N
  ec_D = 4.9 * Mg * ZP ^ (9.25 * (10 ^ -3) * b0g - 0.542) * Exp(-0.015 * b0g) * (dkw / 2 / Mg) ^ (-8.25 * (10 ^ -3) * b0g - 0.225) * ((N / N0) ^ -0.877) * ((ava_D / Mg) ^ 0.511) * ((Hr) / Mg) ^ 0.319
  '
  fz_A = ((ava_A * N0) / ZP) / N
  ec_A = 4.9 * Mg * ZP ^ (9.25 * (10 ^ -3) * b0g - 0.542) * Exp(-0.015 * b0g) * (dkw / 2 / Mg) ^ (-8.25 * (10 ^ -3) * b0g - 0.225) * ((N / N0) ^ -0.877) * ((ava_A / Mg) ^ 0.511) * ((Hr) / Mg) ^ 0.319
  '
  If fz_A > fz_D Then fz = fz_A Else fz = fz_D
  If ec_A > ec_D Then ec = ec_A Else ec = ec_D
  '
  'Definizioni per lo  Spessore del Truciolo (Valore Massimo)
  '
Dim CON_OLIO As Integer

  CON_OLIO = Lp("R[130]")
'If Cells(122, 18) = 1 Then
  '
  If (CON_OLIO = 0) Then
    If (ec > 0.25) Then
      'Lavorazione senza Refrigerazione o MQL
      msgBASE = ""
      msgBASE = msgBASE & "DENTATURA SENZA REFRIGERANTE!!!" & Chr$(13)
      msgBASE = msgBASE & "Diminuire Avanzamento Assiale." & Chr$(13)
      msgBASE = msgBASE & "Spessore del Truciolo supera 0,35 mm" & Chr(13)
      msgBASE = msgBASE & "possibile usura prematura" & Chr(13)
      StopRegieEvents
      MsgBox msgBASE, vbCritical, "WARNING"
      ResumeRegieEvents
      'Exit Sub
    End If
  Else
    If (ec > 0.35) Then
      'Lavorazione Umida
      msgBASE = ""
      msgBASE = msgBASE & "DENTATURA CON REFRIGERANTE!!!" & Chr$(13)
      msgBASE = msgBASE & "Diminuire Avanzamento Assiale." & Chr$(13)
      msgBASE = msgBASE & "Spessore del Truciolo supera 0,35 mm" & Chr(13)
      msgBASE = msgBASE & "possibile usura prematura" & Chr(13)
      StopRegieEvents
      MsgBox msgBASE, vbCritical, "WARNING"
      ResumeRegieEvents
      'Exit Sub
    End If
  End If
  '
'End If
  '
'If Cells(122, 18) <> 1 Then
'  If ec > 0.25 Then
'    MsgBox ("Attenzione!!!   Diminuire Avanzamento Assiale. Spessore del Truciolo supera 0,25 mm, che per Lavorazione senza Refrigerazione pu� causare usura prematura")
'    Cells(84, 5).Select
'  End If
'End If
  '
'If Cells(111, 19) = 2 Then
  avx = ava_D
'Else
'  avx = ava_A
'End If
  '
  HF = (Tan(ag)) * ((0.5 * dkw - Hkw) - 0.5 * ((dkw - (2 * Hkw)) ^ 2 - (avx) ^ 2) ^ 0.5)
  hp = (N0 ^ 2) * PI ^ 2 * Sin(ag) * Mg / (N ^ 2 * 4 * DPG / Mg)
  '
  X1 = (((Hr * dkw / ((Sin(ac) ^ 2)) + da - Hr) ^ (0.5)) * Tan(ac))
  X2 = (Hkw / Tan(ag)) * Abs(Sin(ac))
  If (X2 < 2) Then X2 = 2
  '
'If Cells(132, 19) = 1 Then
'  X1 = X2
'End If
  '
  'L = (LD * Q) + X1 + X2 + E
  '
  'SH1 = Mn * PI
  'If Cells(115, 19) = 2 Then
  'If (QSH = 0) Then
  '  QSH = Round(L3 / SH1)
  'End If
  'Sh = L3 / QSH
  '
  'i1 = (Sh / QPA) * QSH
  'NC1 = Round(Sh / i1)
  '
  'If Cells(126, 18) = 1 Then
  'If (NC1 < 1) Then
  'MsgBox ("Aumentare la Qt�. pezzi per dentatura")
  'Exit Sub
  'End If
  'End If
  '
'If Cells(126, 18) = 2 Then
'If NC1 < 1 Then
'MsgBox ("Aumentare il valore del Fattore K")
'Exit Sub
'End If
'End If

  'i = Int(Sh / NC1 * 100000) / 100000
  'Call SP("I_G", i, False)
  'Call SP("QSH_G", QSH, False)
  'Call SP("NPMAX_G", NC1, False)

'If Cells(116, 19) = 2 Then
'QP = 1
'QPP = QPA
'End If

'If Cells(116, 19) = 1 Then
'QPP = Round(QPA / QP, 0)
'i1 = (Sh / QPP) * QSH
'NC1 = Round(Sh / i1)
'I = Sh / NC1
'End If

' Calcolo del Volume del Truciolo Rimosso

'If Cells(135, 19) = 1 Then
'APT = Atn(Tan(a0n) / Cos(b0n))
'DT = Dpino / Cos(Atn(Cos(APT) * Tan(b0n)))
'db = DP * Cos(Atn(Tan(a0n) / Cos(b0n)))
'If PAR(ZP) = True Then
'Dcp = Dse - Dpino
'Else
'If PAR(ZP) = False Then
'Dcp = (Dse - Dpino) / Cos((90 / ZP) * PI / 180)
'End If
'End If

'Y = db / Dcp
'PTpino = Atn(-1 * Y / Sqr(-1 * Y * Y + 1)) + 2 * Atn(1)
'SON = dp * Cos(b0n) * (INV(PTpino) + (PI / ZP) - INV(APT) - (DT / db))

'If (b0n <> 0) Then
'sentido = 2 'Ingranaggio Elicoidale
'Else
'sentido = 3 'Ingranaggio Diritto
'End If

'If (b0n = 0) Then b0n = 0.000001

'T1 = db / drp
'T2 = db / da

'If T1 <= 1 Then
'theta1 = Atn(-T1 / Sqr(-1 * T1 * T1 + 1)) + 2 * Atn(1)
'End If

'If T2 <= 2 Then
'theta2 = Atn(-T2 / Sqr(-1 * T2 * T2 + 1)) + 2 * Atn(1)
'End If

'T13 = ((Tan(theta1)) ^ 3)
'T23 = ((Tan(theta2)) ^ 3)

'If sentido = 3 And db > drp Then
'X3 = (PI / ZP) - (SON / dp) - INV(a0n) + INV(theta2)
'Avao = (X3 / 8) * (da ^ 2 - db ^ 2) + (db ^ 2 / 8) * (INV(theta2) - ((Tan(theta2)) ^ 3) / 3) + (PI / Z - SON / dp - INV(a0n) * (db ^ 2 - drp ^ 2) / 8)
'End If
'If sentido = 3 And db < drp Then
'X3 = (PI / ZP) - (SON / dp) - INV(a0n) + INV(theta2)
'Avao = (X3 / 8) * (da ^ 2 - drp ^ 2) - (db ^ 2 / 8) * ((T23 - T13) / 3) + (drp ^ 2) * (INV(theta2) - INV(theta1)) / 8
'End If

'If sentido = 2 And db > drp Then
'X4 = (PI / ZP) - (SON / (dp * Cos(b0n))) - (INV(APT)) + (INV(theta2))
'Avao = (X4 / 8) * (da ^ 2 - db ^ 2) + (db ^ 2 / 8) * (INV(theta2) - T23 / 3) + ((db ^ 2 - drp ^ 2) / 8) * ((PI / ZP) - (SON / (dp * Cos(b0n))) - INV(APT))
'End If
'If sentido = 2 And db < drp Then
'X4 = (PI / ZP) - SON / (dp * Cos(b0n)) - (INV(APT)) + (INV(theta2))
'Avao = (X4 / 8) * (da ^ 2 - drp ^ 2) - (db ^ 2 / 24) * (T23 - T13) + (drp ^ 2) * (INV(theta2) - INV(theta1)) / 8
'End If

'Avao = (2 * Avao) / 100
'Vcr = Avao * ZP * ((LD * 0.1) / Cos(b0g)) * QPA

'Else
'End If

'Pot = 2000 * Mg ^ 0.95 * (dkw / (2 * Mg)) ^ -0.6 * (N / N0) ^ -0.7 * (ava_D / Mg) ^ 0.8 * (hr / Mg) ^ 0.75 * VcDesb ^ -0.228 * Exp(0.65 * mn * zp ^ -0.35 + 0.012 * b0n) * 9.81
  '
  Dim POT_D As Double
  Dim POT_A As Double
  Dim fc_D  As Double
  Dim fc_A  As Double
  '
  'La potenza dipende da: dkw, D, Mg, ava, N0, N, Vc
  'La Forza di taglio fc dipende da: Mg, ava, N0, D, N
  '
  POT_D = Round(((dkw / 2 / 1000 * (2.16 - (0.0028 * D)) * ((Mg ^ 1.3) * ((ava_D ^ 0.8) * ((N0 ^ 0.8) * 9.81 * D / N)))) * (VcDesb * 1000 / dkw / PI)) / 9550, 1)
  fc_D = (2.16 - (0.0028 * D)) * ((Mg ^ 1.3) * ((ava_D ^ 0.8) * ((N0 ^ 0.8) * 9.81 * D / N)))
  '
  POT_A = Round(((dkw / 2 / 1000 * (2.16 - (0.0028 * D)) * ((Mg ^ 1.3) * ((ava_A ^ 0.8) * ((N0 ^ 0.8) * 9.81 * D / N)))) * (VcAcab * 1000 / dkw / PI)) / 9550, 1)
  fc_A = (2.16 - (0.0028 * D)) * ((Mg ^ 1.3) * ((ava_A ^ 0.8) * ((N0 ^ 0.8) * 9.81 * D / N)))
  '
  If POT_A > POT_D Then POT = POT_A Else POT = POT_D
  If fc_A > fc_D Then Fc = fc_A Else Fc = fc_D
  '
  T = dkw / 2 / 1000 * Fc
  '
'   ******************************************************************

'   Calcolo dei Tempi:

'   Corsa di Entrata, Uscita e Taglio Completo:

'If Cells(136, 18) = 1 Then
'Cells(42, 15) = 0
'End If

'XEd = X1 + 2
'XSd = X2 + 2
'XEa = X2 + 2
'XSa = X2 + 2
'XLd = ((LD * Q) + E) - 4

'TFEd = (XEd / ((avFED * RPMhobD * N0) / ZP))
'TFSd = (XSd / ((avFSD * RPMhobD * N0) / ZP))
'If Cells(111, 19) = 1 Then
'  TFEa = (XEa / ((avFEA * RPMhobA * N0) / ZP))
'  TFSa = (XSa / ((avFSA * RPMhobA * N0) / ZP))
'End If
'TFLd = (XLd / ((ava_D * RPMhobD * N0) / ZP))
'If Cells(111, 19) = 1 Then
'  TFLa = (XLd / ((ava_A * RPMhobA * N0) / ZP))
'End If
'
'If Cells(132, 19) = 1 Then
'  Tavrd = (Hr / ((avr_D * RPMhobD * N0) / ZP))
'End If
'
'If Cells(133, 19) = 1 Then
'  Tavra = (0.5 / ((avr_A * RPMhobA * N0) / ZP))
'End If
'
'If Cells(131, 18) = 2 Then
'  TUsind = (((((LD * Q) + E) + X1 + X2) * ZP) / (Cos(b0g))) / (N0 * RPMhobD * ava_D)
'  If Cells(111, 19) = 1 Then
'    TUsina = (((((LD * Q) + E) + X2 + 2) * ZP) / (Cos(b0g))) / (N0 * RPMhobA * ava_A)
'  End If
'Else
'  TUsind = TFEd + TFSd + TFLd
'  TUsina = TFEa + TFSa + TFLa
'End If
'
'If Cells(132, 19) = 1 Then TUsind = TFEd + TFSd + TFLd + Tavrd
'If Cells(133, 19) = 1 Then TUsina = TFEa + TFSa + TFLa + Tavra
'
'TCiclo = (TUsind + TUsina + TA + tC)
'Tm = (TUsind + TUsina + TA)
'Tp = ((TUsind + TUsina + TA + tC) / 60)
'Ph = (1 / Tp) * Q
'
'If Cells(111, 19) = 2 Then
'  TUsina = TUsind
'  TCiclo = (TUsina + TA + tC)
'  Tm = (TUsina + TA)
'  Tp = ((TUsina + TA + tC) / 60)
'  Ph = Round((1 / Tp) * Q, 1)
'End If

'   *****************************************************************

'   RITORNO DEI RISULTATI:

'Cells(36, 5) = DP
'Cells(38, 5) = Hr
'Cells(16, 10) = Mg
'Cells(24, 10) = Hkw
'Cells(32, 10) = Pn
'Cells(34, 10) = Pa
'Cells(56, 5) = ava_D
'Cells(58, 5) = ava_Dm
'Cells(56, 7) = ava_A
'Cells(58, 7) = ava_Am
'Cells(60, 5) = avr_D
'Cells(62, 5) = avr_Dm
'Cells(60, 7) = avr_A
'Cells(62, 7) = avr_Am
'Cells(54, 12) = RPMhobD
'Cells(54, 14) = RPMhobA
'Cells(56, 12) = RPMtavolaD
'Cells(56, 14) = RPMtavolaA
'Cells(60, 12) = avFED
'Cells(60, 14) = avFEA
'Cells(64, 12) = avFSD
'Cells(64, 14) = avFSA
'Cells(34, 15) = g0ws * (180 / PI)
'Cells(72, 5) = b0g * (180 / PI)
'Cells(74, 5) = DPG
'Cells(76, 5) = bd
'Cells(78, 5) = VPA
'Cells(76, 15) = QPA
'Cells(78, 15) = k
'Cells(80, 15) = Vcr
'Cells(72, 10) = L1
'Cells(74, 10) = L2
'Cells(76, 10) = L3
'Cells(78, 10) = L3r
'Cells(80, 5) = ac * (180 / PI)
'Cells(82, 5) = fz
'Cells(84, 5) = ec
'Cells(86, 5) = HF
'Cells(88, 5) = hp
'Cells(90, 5) = X1
'Cells(92, 5) = X2
'Cells(94, 5) = L
'Cells(80, 10) = Sh
'Cells(82, 10) = QSH
'Cells(84, 10) = I * Q
'Cells(88, 10) = QP
'Cells(90, 10) = QPP
'Cells(92, 10) = POT
'Cells(94, 10) = Fc
'Cells(96, 10) = T
'Cells(86, 15) = TUsind
'Cells(88, 15) = TUsina
'Cells(90, 15) = Tm
'Cells(92, 15) = TCiclo
'Cells(94, 15) = Tp
'Cells(96, 15) = Ph
  '
  'UNA PASSATA
  If (Abilitazioni(1) > 0) And (Abilitazioni(2) <= 0) And (Abilitazioni(3) <= 0) And (Abilitazioni(4) <= 0) Then
    Call SP("NUMPASSSGR_G", 1, False)
    Call SP("NUMPASSFIN_G", 0, False)
    Call SP("NUMPASSCICLO3_G", 0, False)
    Call SP("NUMPASSCICLO4_G", 0, False)
    Call SP("INCPASSSGR_G", Hr, False)
    Call SP("INCPASSFIN_G", 0, False)
    Call SP("INCPASSC3_G", 0, False)
    Call SP("INCPASSC4_G", 0, False)
  End If
  'DUE PASSATE
  If (Abilitazioni(1) > 0) And (Abilitazioni(2) > 0) And (Abilitazioni(3) <= 0) And (Abilitazioni(4) <= 0) Then
    Call SP("NUMPASSSGR_G", 1, False)
    Call SP("NUMPASSFIN_G", 1, False)
    Call SP("NUMPASSCICLO3_G", 0, False)
    Call SP("NUMPASSCICLO4_G", 0, False)
    Call SP("INCPASSSGR_G", Hr / 2, False)
    Call SP("INCPASSFIN_G", Hr / 2, False)
    Call SP("INCPASSC3_G", 0, False)
    Call SP("INCPASSC4_G", 0, False)
  End If
  '
  'AGGIORNAMENTO DEL DATABASE
  Call AggiornaDB
  '
  Write_Dialog "P: " & Format$(POT, "########0") & "Kw F: " & Format$(Fc, "########0") & "N T: " & Format$(T, "########0") & "Nm ec: " & frmt(ec, 4) & "mm fz: " & frmt(fz, 4)

End Sub

' Note e Osservazioni

' 21/03/2005 - Alterazione del Passo Assiale, nella versione precedente il calcolo non considerava il numero di entrate dell'utensile
' 25/04/2005 - Correzione del valore di avanzamento per il calcolo della pot�nza di taglio
' 22/08/2005 - Aggiunto il calcolo del volume del Truciolo per l'analisi del rendimento
' 22/08/2005 - Aggiunto il calcolo della Forza di Taglio (Fc)
' 22/08/2005 - Aggiunto il calcolo del Torque (T)
' 20/03/2006 - Aggiunta l'opzione per il  Calcolo del Volume del Truciolo (s� o no)
' 27/03/2006 - Correzione della formula che calcola l'area del vano utilizzata per il calcolo di V0. Considerato valore di b0n uguala a 0,000001 gradi quando l' Elica � diritta.
' 08/05/2006 - Correzione dell' opzione di C�lcolo per il  Volume di Truciolo rimosso (s� o no)
' 24/07/2006 - Correzione dell' incremento in funzione della quantit� di pezzi per fixa��o (Incremento = I * q)
' 03/12/2007 - Correzione della Lunghezza del distanziatore.
' 05/12/2007 - Correzione della formula del volume del Truciolo rimosso per condizioni di Diametro di base maggiore del Diametro di piede.
'

