Attribute VB_Name = "ContourDressingG450"
Option Explicit
'Modulo per gestione rullo profilatore su G450
'Processo di Contour Dressing e verifica contatto mola-pezzo
'Modulo indipendente

'1.Posizionamento angolare rullo su G450.
      '1.1 Calcolo rotazione e posizionamento di recupero
             '1.1.1 Calcolo inclinazione
             '1.1.2 Verifica recupero per passo mola e spessore
             
      '1.2 Integrazione con correzioni standard
      '1.3 Verifica contatti pari (da programma JPG)


'2.Contour Dressing su G450.
      '2.1.Definizione Profilo
             '2.1.1
      '2.2.Definizione Cremagliera
             '2.2.1 Altezza per rispettare SAP
             '2.2.2 Punto corrispondente EAP per punti extra fondo
      '2.3.Calcolo posizionamenti rullo
             '2.3.1 Calcolo punto di stacco fra zona 1 e zona 2
             '2.3.2 Gestione lunghezza extra rispetto tratto per correzione
             '      (Lunghezza spillo)*n <> tratto per correzione.
             '2.3.3 Gestione fondo mola e raccordo testa-profilo mola
      '2.4.Gestione profilatura
             '2.4.1 Calcolo materiale su mola per eseguire correzioni.
             '2.4.2 Gestione ciclo di profilatura (eventualmente due zone per ridurre tempi)
             '2.4.3 Calcolo numero di posizionamenti
             '2.4.4 Creazione sottoprogramma con dati necessari (routine dedicata)
             


'Variabili di input.
'CONTATTI_MV
Public CodiceRicerca
Public PathDati$, Progetto$, DatPro$
Public Utente$, SepDec$

Public Scala, VPB
Public Jour$
Public SpostX, SpostY
Public Stampante$, SpessoreTratto
Public PrinterStampante(5) As Printer
Public Nstampante
Public Lstamp, Hstamp
Public Tecnico$
Public CodiceOK$
'
Public UltimoTasto
Public Cliente$, Disegno$



Private AddMola, ApMolaD, Apmola, ApMolaApp
Private MrMola, HelrMola, DrMola, RrMola
Private EpMolaApp, EsMolaApp, DedMolaAttivo, DsapOtt
Private DsapRich, AddMolaContatti
Private ArotRxD(50) As Double
Private RxContatto(50) As Double
Private NcontattiRx_F1(50) As Integer
Private NcontattiRx_F2(50) As Integer
Private NcontattiRx(50) As Integer
Private Ddis1, Ddis2, GiocoFondo
'
Private Eb1Fin, Eb1Dent
Private Mr, Mapp, Z1
Private Aprd, Apr, ApApp
Private HelpD, Help, HELB
Private Dp1, DB1, Di1, DE1, Re1
Private Kdt, Wdt, DPG, Wpg, Epr1
Private Spost
'
Private Pr$, EpProtu, DbPr, EbPr, ApProtu, ApProtuD, ApProtuApp, Hprot
Private Helr, Dr1, RR1, AddFm, EpFm
Private MrFm, ApFmD, ApFm, ApFmApp     'Angolo pressione creatore
Private RsFm, Xrs, Yrs
Private Esfm, RPieno
Private HpUtil, InterfDent$
Private R0, Xr0, Yr0
Private SuRep, DactifDent

Private AngRotD ' Angle de rotation du trace de la cremaillere
                ' Rivisto. L'ingranaggio ruota e la cremagliera trasla.
Private RxFin(50), AngRxFin(50)     ' profil du creux de dent theorique fini
Private RX(192), AngRx(192), ApRx(192)     ' profil du creux de dent obtenu taillage
Private Xfm(192) ' profil fraise/mere par points
Private Yfm(192)  ' profil fraise/mere par points  Yfm : rispetto alla testa
Private Aprfm(192)  ' profil fraise-mere apparent par points
Private ENTRX ' Entraxe FM / Engrenage
Private RsFmApp, EsFmApp
Private PntFineFianco, NbpDevProtu
Private PntAxe, PntAxeCr, PntFinRaCr, NbpTotCr, PntIniRaCr
Private NbpDev, NbpTroc, NbpFond, NbPiani, NbpDevFin
Private PntFinTroc, PntIniTroc, PntMedTroc, PntFinDev
Private DactifRect, InterfRettif$

Dim XmIng, YmIng, XmCrem, YmCrem, Xprec, Yprec
Dim Xpt, Ypt, XCT, YCT, XPD, Ypd, Xcd, Ycd, Rx9
Dim KeyCode

'Dati per Contour Dressing
'Definizione profilo ingranaggio
Const Np = 3


Private Type ProfiloK_Ingranaggio
 TIFDIAM As Integer
 SAP(1 To 2) As Double
 'i= numero tif, j= fianco
 'i=0 corrisponde al SAP
 Tif(Np, 1 To 2) As Double
 DIAM(Np, 1 To 2) As Double
 BOMB(Np, 1 To 2) As Double
 Rastr(Np, 1 To 2) As Double
 RastrPT(1 To 2) As Double
 DevAng(Np, 1 To 2) As Double
 ZonaStandard As Boolean
 
      
End Type
'
Private Type DatiGeometrici_Ingranaggio
 Modulo As Double
 NumDenti As Double
 AlfaTrasv As Double
 AlfaNorm  As Double
 AngoloElica As Double
 DiamEXT As Double
 DiamINT As Double
 DiamSAP(1 To 2) As Double
 DiamEAP(1 To 2) As Double
 TipoSpessore As Integer
 SN As Double
 St As Double
 FattoreX As Double
 QuotaWK As Double
 DentiWK As Integer
 QuotaR As Double
 DiamR As Double
End Type

Private Type Ingranaggio
 DatiGeometrici As DatiGeometrici_Ingranaggio
 ProfiloK As ProfiloK_Ingranaggio
 DIAMPRIM As Double
 DiamBASE As Double
 ElicaBASE As Double
 PassoBase As Double
 SpesBaseN As Double
 SpesbaseT As Double
 DiamROT As Double
 AlfatROT As Double
 AlfanROT As Double
 ModuloROT As Double
 SNROT As Double
 STROT As Double
 ElicaROT As Double
End Type

Private Type Punto
 X As Double
 Y As Double
 ANGOLO As Double
End Type


Public TipoRullo As Integer
Private Type Rullo
'CONVENZIONI per Contour Dressing:
'FIANCO si riferisce alla mola
'ZONA si riferisce al rullo
'Indice 1 corrisponde alla zona 1 del rullo (Porzione Standard)
'Indice 2 corrisponde alla zona 2 del rullo (Porzione per correzioni Contour Dressing)
DiametroEsterno As Double

' Considero il diametro esterno del rullo gotico.
' In questo caso devo distinguere i due diametri dei rulli.

DiametroEsternoGotico As Double
Spessore As Double
RaggioRiferimento(1 To 2) As Double
AngoloRiferimento(1 To 2) As Double
SpessoreRiferimento(1 To 2) As Double
RaggioFianco(1 To 2) As Double
AltezzaFianco(1 To 2) As Double
RaggioTesta(1 To 2) As Double

'Correttori da INPUT
'Dati da disegno rullo

RiferimentoTangenziale(1 To 2) As Double
RiferimentoRadiale(1 To 2) As Double


'Movimento Rullo
'Stacco Zona1-Zona2 rullo
StaccoRadiale(1 To 2) As Double
StaccoTangenz(1 To 2) As Double
StaccoInclinazione(1 To 2) As Double

End Type

Private Type Sezione_Mola
  AltezzaTotale As Double
  AltezzaAttiva As Double
  
  ADDENDUM As Double
  DEDENDUM As Double
  Spessore As Double
  
  
  SpessoreTesta As Double
  SpessoreFondo As Double
  GiocoFondo As Double
  GiocoTesta As Double
  
  
  Rif As Punto
  Punto(Np, 1 To 2) As Punto
  PuntiFondo() As Punto
  PuntiTesta() As Punto
End Type

Private Type Mola
  SEZIONE As Sezione_Mola
  AlfaROT As Double
  ModuloROT As Double
  AltezzaDenteMola As Double
End Type


Private Ing As Ingranaggio
Private RulloCD As Rullo
Private MolaCD  As Mola

Private CorrPunto_K(1 To 2) As Double

Private DeltaXRulloZ1(1 To 2) As Double
Private DeltaYRulloZ1(1 To 2) As Double
Private DeltaXRulloZ2(1 To 2) As Double
Private DeltaYRulloZ2(1 To 2) As Double
'
Private DeltaRadialeZ1(1 To 2) As Double
Private DeltaRadialeZ2(1 To 2) As Double

Private DeltaTangenzialeZ1(1 To 2) As Double
Private DeltaTangenzialeZ2(1 To 2) As Double



'
Const PI = 3.141592654
Const RAD = PI / 180
Const DimensioneArray As Integer = 200


Public Sub ContourDressingG450()



'AZZERO I DATI
 Call AzzeraDati

''ACQUISIZIONE DATI

''Dati Geometrici e Profilo K Ingranaggio
Call LeggiDatiGeometrici
Call LeggiDatiProfilo
''Dati Mola
Call LeggiParametriMola
''Dati Rullo
Call LeggiParametriRullo

' Verifico che sia selezioanto un tipo di rullo per eseguire
' la profilatura con Contour Dressing

''RICHIAMO ROUTINE DI CALCOLO
   Call CalcoloContourDressingG450



End Sub


Public Sub CalcoloContourDressingG450()

Dim GearTRASV As Ingranaggio
Dim GearIMMAG As Ingranaggio   'Utilizzo sezione immaginaria, pi� precisa, metodo alla Freddi (vedi GS400 e CD su S250)

Dim TifSAP(1 To 2) As Double
Dim TifROT(1 To 2) As Double
Dim TifROTimm As Double
Dim TifRIF(1 To 2) As Double

Dim TifEAP(1 To 2) As Double
Dim TifEAP_K(1 To 2) As Double
Dim TifEAP_Kimm As Double
Dim DiffSAPEAP(1 To 2) As Double

Dim DedMola As Double 'A la JPG

Dim DressZona(1 To 2, 1 To 2) As Integer


Dim I_Fine As Integer

Dim SpesFondoATT As Double
Dim SpesTestaATT As Double
Dim GiocoFondoATT As Double
Dim DeltaSpessore As Double
Dim DeltaDiametro As Double
Dim SpesFondoVANO As Double
Dim SpesTestaVANO As Double


Dim NumPuntiTOT(1 To 2) As Integer
Dim PuntiProfilaturaZ2(DimensioneArray, 1 To 2) As Punto

Dim k As Integer

Dim X1, Y1, ang1 As Double
Dim X0, Y0, Ang0 As Double

Dim Rastremazione As Boolean
Dim NumRASTR As Integer

'Nuova gestione altezza dente
Dim GiocoFondoMAX As Double
Dim NumPuntiFondo As Integer: NumPuntiFondo = 3
Dim SpostamentoRullo As Double


'Calcoli caratteristiche ingranaggio da dati teorici
Call CalcoloIngranaggioG450(Ing, GearTRASV)




'Conversione dati profilo K da diametro a TIF
'1.
With GearTRASV.ProfiloK
For j = 1 To 2
   If (.TIFDIAM = 2) Then
'       'Dati profilo  - Trasformazione da diametro a Tif
     For i = 0 To Np
        If i = 0 Then
          If .Tif(i, j) <> 0 Then
               .DIAM(i, j) = .Tif(i, j)
               .Tif(i, j) = CalcolaTifG450(.Tif(i, j), GearTRASV.DiamBASE)
               .SAP(j) = .Tif(0, j)
            Else
               Write_Dialog "DEFINIRE INIZIO PROFILO ATTIVO"
          End If
        Else
          If .Tif(i, j) <> 0 Then
               .DIAM(i, j) = .Tif(i, j)
               .Tif(i, j) = CalcolaTifG450(.Tif(i, j), GearTRASV.DiamBASE)
               TifEAP_K(j) = .Tif(i, j)
          End If
        End If
     Next i
     
 Else
    
     'Dati profilo  - Trasformazione da Tif a diametro
     For i = 0 To Np
        If i = 0 Then
          If .Tif(0, i) <> 0 Then
               .DIAM(i, j) = CalcolaDiametroG450(.Tif(i, j), GearTRASV.DiamBASE)
               .SAP(j) = .Tif(0, j)
            Else
               Write_Dialog "DEFINIRE INIZIO PROFILO ATTIVO"
          End If
        Else
          If (.Tif(i, j) <> 0) Then
                .DIAM(i, j) = CalcolaDiametroG450(.Tif(i, j), GearTRASV.DiamBASE)
                TifEAP_K(j) = .Tif(i, j)
          End If
        End If
     Next i
End If

'Controllo congruenza fra dati geometrici e dati profilo K

TifSAP(j) = CalcolaTifG450(GearTRASV.DatiGeometrici.DiamSAP(j), GearTRASV.DiamBASE)
TifEAP(j) = CalcolaTifG450(GearTRASV.DatiGeometrici.DiamEAP(j), GearTRASV.DiamBASE)
If (.SAP(j) <> TifSAP(j)) Then
   Write_Dialog "DATI GEOMETRICI E DI PROFILO FIANCO" & str(j) & "INCONGRUENTI!"
End If
If (TifEAP_K(j) <> TifEAP(j)) Then
   Write_Dialog "DATI GEOMETRICI E DI PROFILO FIANCO" & str(j) & "INCONGRUENTI!"
End If
Next j
'1.
End With

'Calcolo deviazioni profilo
'2.
With GearTRASV.ProfiloK

For j = 1 To 2
   DiffSAPEAP(j) = TifEAP_K(j) - TifSAP(j)
   For i = 1 To Np
     .DevAng(i, j) = (.Tif(i, j) - TifEAP_K(j)) / DiffSAPEAP(j) * .RastrPT(j) + .RastrPT(j)
   Next i
Next j
'2.
End With


'Calcolo dati della sezione immaginaria
'3.
With GearIMMAG
GearIMMAG = GearTRASV
  .DatiGeometrici.NumDenti = GearTRASV.DatiGeometrici.NumDenti / Cos(RAD * GearTRASV.DatiGeometrici.AngoloElica) ^ 3
  .DIAMPRIM = GearTRASV.DatiGeometrici.Modulo * .DatiGeometrici.NumDenti
  .DiamBASE = .DIAMPRIM * Cos(RAD * .DatiGeometrici.AlfaNorm)
  .DiamROT = .DiamBASE / Cos(RAD * MolaCD.AlfaROT)
  .ModuloROT = .DiamROT / .DatiGeometrici.NumDenti
  .STROT = (.DatiGeometrici.St / .DIAMPRIM + InvRAD(Ing.DatiGeometrici.AlfaNorm) - InvRAD(.AlfanROT)) * .DiamROT
  .SNROT = .STROT * Cos(RAD * .ElicaROT)
  .AlfatROT = MolaCD.AlfaROT

For j = 1 To 2
TifROT(j) = CalcolaTifG450(.DiamROT, .DiamBASE)
TifRIF(j) = CalcolaTifG450(.DIAMPRIM, .DiamBASE)
Next j
'3.
End With

'CONVERSIONE DIAMETRI/TIF DALLA SEZIONE TRASVERSALE A QUELLA IMMAGINARIA
'4.
With GearIMMAG
  For j = 1 To 2
            
      For i = 0 To Np
   
         If .ProfiloK.TIFDIAM = 2 Then
         
         If .ProfiloK.DIAM(i, j) > 0 Then
               
               .ProfiloK.DIAM(i, j) = .DIAMPRIM - (GearTRASV.DIAMPRIM - GearTRASV.ProfiloK.DIAM(i, j))
                             .ProfiloK.Tif(i, j) = CalcolaTifG450(.ProfiloK.DIAM(i, j), .DiamBASE)
               TifEAP_Kimm = .ProfiloK.Tif(i, j)
            End If
               
           Else
           If .ProfiloK.Tif(i, j) > 0 Then
              .ProfiloK.DIAM(i, j) = .DIAMPRIM - (GearTRASV.DIAMPRIM - GearTRASV.ProfiloK.DIAM(i, j))
                             .ProfiloK.Tif(i, j) = CalcolaTifG450(.ProfiloK.DIAM(i, j), .DiamBASE)
               TifEAP_Kimm = .ProfiloK.Tif(i, j)
         End If
         End If


Next i
Next j
'4.
End With

TifROTimm = CalcolaTifG450(GearIMMAG.DiamROT, GearIMMAG.DiamBASE)

'CALCOLI GEOMETRIA MOLA.


'5.
With MolaCD.SEZIONE
'SEZIONE NORMALE
.Spessore = PI * (MolaCD.ModuloROT) - GearIMMAG.SNROT
.ADDENDUM = (TifROTimm - GearIMMAG.ProfiloK.Tif(0, 1)) * Sin(RAD * MolaCD.AlfaROT)
.DEDENDUM = (TifEAP_Kimm - TifROTimm) * Sin(RAD * MolaCD.AlfaROT)
.AltezzaAttiva = .ADDENDUM + .DEDENDUM

SpesFondoATT = .Spessore + .DEDENDUM * Tan(RAD * MolaCD.AlfaROT) * 2     'Spessore Fondo (attivo) - PIENO
SpesTestaATT = .Spessore - .ADDENDUM * Tan(RAD * MolaCD.AlfaROT) * 2     'Spessore Testa (attivo) - PIENO
GiocoFondoATT = (GearTRASV.DatiGeometrici.DiamEXT - GearTRASV.DiamROT) / 2 - .DEDENDUM

.GiocoTesta = (GearTRASV.DiamROT - GearTRASV.DatiGeometrici.DiamINT) / 2 - .ADDENDUM

GiocoFondoMAX = RulloCD.AltezzaFianco(2) * Cos(RAD * RulloCD.AngoloRiferimento(2)) * NumPuntiFondo




If TipoRullo = 3 Then
   'CONSIDERO IL RULLO per CONTOUR DRESSING COME UN RULLO STANDARD CON SPESSORE PARI AL DOPPIO
   'DELLO SPESSORE DELLA ZONA 1. HA SENSO PERCHE' LO POSSO RUOTARE DI 180� E USARLO COME UN RULLO STANDARD
   DeltaSpessore = GearIMMAG.SNROT / 2 - RulloCD.SpessoreRiferimento(1)
   DeltaDiametro = RulloCD.RaggioRiferimento(1) - DeltaSpessore / Tan(RAD * MolaCD.AlfaROT) 'Tan(RAD * RulloCD.AngoloRiferimento(1))
   DedMola = RulloCD.DiametroEsterno / 2 - DeltaDiametro                        'Calcolo alla JPG (Riferimento spessore di rotolamento)
   .GiocoFondo = DedMola - (GearTRASV.DatiGeometrici.DiamEXT - GearTRASV.DiamROT) / 2
   SpostamentoRullo = 0
   
'
   If .GiocoFondo > GiocoFondoMAX Then
   Dim Temp(1 To 3) As Double

   DedMola = (GearTRASV.DatiGeometrici.DiamEXT - GearTRASV.DiamROT) / 2 + GiocoFondoMAX
   Temp(1) = (RulloCD.DiametroEsterno / 2 - RulloCD.RaggioRiferimento(1))
   Temp(2) = (Temp(1) - DedMola) * Tan(RAD * MolaCD.AlfaROT)
   Temp(3) = RulloCD.SpessoreRiferimento(1) - Temp(2)
   SpostamentoRullo = 0 'GearIMMAG.SNROT / 2 - Temp(3)

   End If
   





Else
  'RULLO STANDARD + RULLO GOTICO
  
     'CONSIDERO I DATI DEL RULLO SGROSSATORE PER IL CALCOLO DELLA CREMAGLIERA.
     'I PUNTI PER REALIZZARE LE MODIFICHE SONO DETERMINATI DAL RAGGIO DEL RULLO
     'CONTOUR DRESSING.
     'MANTENGO LA STESSA FILOSOFIA DEL RULLO SAMP:
     'IL RULLO SGROSSATORE CORRISPONDE ALLA ZONA 1
     'IL RULLO GOTICO CORRISPONDE ALLA ZONA 2
   
   DeltaSpessore = GearIMMAG.SNROT / 2 - RulloCD.SpessoreRiferimento(1) / 2
   DeltaDiametro = RulloCD.RaggioRiferimento(1) - DeltaSpessore / Tan(RAD * MolaCD.AlfaROT)
   DedMola = RulloCD.DiametroEsterno / 2 - DeltaDiametro                        'Calcolo alla JPG (Riferimento spessore di rotolamento)
   .GiocoFondo = DedMola - (GearTRASV.DatiGeometrici.DiamEXT - GearTRASV.DiamROT) / 2
   SpostamentoRullo = 0
   
   Dim Incremento As Double
   Dim Proiezione As Double
   Dim CordaContatto As Double
   
   
   Incremento = Abs(LEP("APINCSGDIAM_G"))
   Proiezione = Incremento * Sin(RAD * MolaCD.AlfaROT)
   CordaContatto = 2 * Sqr(Proiezione * (RulloCD.AltezzaFianco(2) * 2 - Proiezione))
   
   RulloCD.AltezzaFianco(2) = CordaContatto
   
   

End If

SpesFondoVANO = GearIMMAG.SNROT - DedMola * Tan(RAD * MolaCD.AlfaROT) * 2    'Spessore di fondo della mola - VANO
SpesTestaVANO = GearIMMAG.SNROT + .ADDENDUM * Tan(RAD * MolaCD.AlfaROT) * 2  'Spessore di testa della mola - VANO

.AltezzaTotale = DedMola + .ADDENDUM
.Rif.Y = .Spessore / 2

    If .AltezzaTotale > MolaCD.AltezzaDenteMola Then
     StopRegieEvents
     
     Dim TmpMsg As String
     
     If (LINGUA = "GR") Then
         TmpMsg = MsgBox("BERECHNUNG FUER CONTOUR DRESSING NICHT AUSGEFUEHRT!" & Chr(13) & "MINDESTHOEHE SCHLEIFSCHEIBE ZAHN : " & _
         Format(.AltezzaTotale, "0.000"), vbCritical, "ACHTUNG")
       
       Else
       
        If (LINGUA = "IT") Then
         TmpMsg = MsgBox("CALCOLO PER CONTOUR DRESSING NON ESEGUITO" & Chr(13) & "ALTEZZA MINIMA DENTE MOLA :" & _
         Format(.AltezzaTotale, "0.000"), vbCritical, "ATTENZIONE")
         Else
         
         TmpMsg = MsgBox("CONTOUR DRESSING EVALUATION NOT DONE" & Chr(13) & "MINIMAL WHEEL TOOTH HEIGHT :" & _
         Format(.AltezzaTotale, "0.000"), vbCritical, "WARNING")
         End If
     End If
     
     ResumeRegieEvents
     Exit Sub
    
    End If



'Inizializzo coordinate punti per Contour Dressing (correzione sezione mola attiva dovute a rastremazioni e/o bombature)

For j = 1 To 2
    For i = 0 To Np
        .Punto(i, j).X = 0
        .Punto(i, j).Y = 0
        .Punto(i, j).ANGOLO = 0
    Next i
Next j


For j = 1 To 2
For i = 0 To Np
If GearIMMAG.ProfiloK.Tif(i, j) <> 0 Then
   'Calcolo quote radiali sagoma mola -- Definisce il punto mola che lavora un determinato diametro dell'ingranaggio
   ' "TRAPEZIO DI SUPPORTO" Sagoma mola senza tenere conto delle correzioni
      If (TifROTimm < GearIMMAG.ProfiloK.Tif(i, j)) Then
          .Punto(i, j).X = (GearIMMAG.ProfiloK.Tif(i, j) - TifROTimm) * Sin(RAD * GearIMMAG.AlfatROT) + .ADDENDUM
        ElseIf Abs(TifROTimm - GearIMMAG.ProfiloK.Tif(i, j)) < 0.001 Then
          .Punto(i, j).X = (GearIMMAG.ProfiloK.Tif(i, j) - TifROTimm) * Sin(RAD * GearIMMAG.AlfatROT) + .ADDENDUM
        Else
          .Punto(i, j).X = .ADDENDUM - (TifROTimm - GearIMMAG.ProfiloK.Tif(i, j)) * Sin(RAD * GearIMMAG.AlfatROT)
       End If
   'Calcolo quote tangenziali sagoma mola
   'I punti .Y tengono in considerazione le rastremazioni e definiscono la cremagliera corretta.
      If i = 0 Then
          .Punto(i, j).Y = .Rif.Y - .ADDENDUM * Tan(RAD * MolaCD.AlfaROT)
        Else
          .PUNTO(i, j).Y = .PUNTO(i - 1, j).Y + (.PUNTO(i, j).X - .PUNTO(i - 1, j).X) * Tan(RAD * GearIMMAG.AlfatROT) - GearIMMAG.ProfiloK.Rastr(i, j)
      End If
End If

Next i
Next j

'Gestione deviazione angolare sul profilo.
For j = 1 To 2
For i = 0 To Np
'Ricalcolo coordinata tangenziale per deviazione angolare
     If GearIMMAG.ProfiloK.Tif(i, j) <> 0 Then
       .Punto(i, j).Y = .Punto(i, j).Y + GearIMMAG.ProfiloK.DevAng(i, j)
     End If
Next i
Next j


NumRASTR = 0
I_Fine = 1
'Calcolo angolo punto sezione mola
For j = 1 To 2

For i = 0 To Np
  If i = 0 Then
      .Punto(i, j).ANGOLO = Atn((.Punto(i + 1, j).Y - .Punto(i, j).Y) / (.Punto(i + 1, j).X - .Punto(i, j).X)) / RAD
    Else
        If (.Punto(i, j).X <> 0) And (.Punto(i, j).X - .Punto(i - 1, j).X) <> 0 Then
          .Punto(i, j).ANGOLO = Atn((.Punto(i, j).Y - .Punto(i - 1, j).Y) / (.Punto(i, j).X - .Punto(i - 1, j).X)) / RAD
        End If
  End If
   

'CALCOLI E GESTIONE VARIABILI PER REALIZZAZIONE SAGOMA
'Definisco l'ultimo punto a cui � associata una correzione.
If (GearIMMAG.ProfiloK.Rastr(i, j) <> 0) Or (GearIMMAG.ProfiloK.BOMB(i, j) <> 0) Then
  I_Fine = i
End If
If (GearIMMAG.ProfiloK.Rastr(i, j) <> 0) Then
  NumRASTR = NumRASTR + 1
End If
'
'
Next i

DressZona(1, j) = 1
DressZona(2, j) = 1
NumPuntiTOT(j) = 0

DeltaRadialeZ1(j) = DeltaXRulloZ1(j) + RulloCD.RiferimentoRadiale(1)
DeltaRadialeZ2(j) = DeltaXRulloZ2(j) + RulloCD.RiferimentoRadiale(2)

' Nel caso del rullo gotico devo gestire la differenza fra i diametri dei due rulli
' DIPENDE!!!!
'If TipoRullo = 4 Then
'
'  DeltaRadialeZ2(j) = (RulloCD.DiametroEsternoGotico - RulloCD.DiametroEsterno) / 2
'
'End If


'Spostamento per variazione di spessore, per poter ottimizzare l'altezza dente
DeltaTangenzialeZ1(j) = DeltaYRulloZ1(j) + SpostamentoRullo
DeltaTangenzialeZ2(j) = DeltaYRulloZ2(j) + SpostamentoRullo


If NumRASTR <> 0 Then
    Rastremazione = True
  Else
    Rastremazione = False
End If

 '****************
    Dim Bombatura As Boolean
    Dim NumBOMB As Integer
    NumBOMB = 0
    Bombatura = False
    For i = 1 To Np
      If GearIMMAG.ProfiloK.BOMB(i, j) <> 0 Then
          NumBOMB = NumBOMB + 1
       End If
    Next i
    If NumBOMB <> 0 Then
      Bombatura = True
      GearIMMAG.ProfiloK.ZonaStandard = False
    End If
  '*****************

   


  'Calcolo punto di stacco Zona1 e Zona2 rullo
  
  If (GearIMMAG.ProfiloK.ZonaStandard) Then
  
      DressZona(1, j) = 1
      'Condizioni per stacco: Non c'� bombatura nel primo tratto ed � presente una rastremazione
      If (GearIMMAG.ProfiloK.BOMB(1, j) = 0) And (Rastremazione) Then
           '
           RulloCD.StaccoRadiale(j) = MolaCD.SEZIONE.AltezzaTotale - .PUNTO(1, j).X - DeltaRadialeZ1(j)
           '
         Else
           '
           RulloCD.StaccoRadiale(j) = 0 - DeltaRadialeZ1(j)
           DressZona(2, j) = 0
           
      End If
      If RulloCD.StaccoRadiale(j) < 0 Then
      
           RulloCD.StaccoRadiale(j) = 0
           'Negative Radial Tool Position! Check data and correctors!
           'Negative Radialposition! Werte und Korrekturen kontrollieren!
      End If
      RulloCD.StaccoTangenz(j) = (-1) ^ j * (RulloCD.StaccoRadiale(j) * Tan(RAD * .PUNTO(1, j).ANGOLO) + DeltaTangenzialeZ1(j))
      RulloCD.StaccoInclinazione(j) = (-1) ^ j * (.Punto(1, j).ANGOLO - RulloCD.AngoloRiferimento(1))
      '

           
      
   Else
       
       'CALCOLI PER UTILIZZARE SOLO LA ZONA 2 (DIAMANTINO) IN PROFILATURA
       'Calcolo tratto da realizzare con il diamantino (Zona 2) invece che con la zona standard (Zona 1).
       'Il riferimento � appunto l'altezza impostata da HMI (reale) e non quella teorica ottenuta dai calcoli.
       With MolaCD.SEZIONE
  
           
           'Se c'� una bombatura nel primo tratto considero come punto finale del tratto il SAP. Altrimenti il punto 1.
           If (GearIMMAG.ProfiloK.BOMB(1, j) = 0) Then
                X0 = (.AltezzaTotale - .Punto(1, j).X): Y0 = 0: Ang0 = .Punto(1, j).ANGOLO
             Else
                X0 = (.AltezzaTotale - .Punto(0, j).X): Y0 = 0: Ang0 = .Punto(0, j).ANGOLO
           End If
       End With
      
                X1 = MolaCD.AltezzaDenteMola: Y1 = 0: ang1 = .Punto(1, j).ANGOLO
      
      DressZona(1, j) = 0
      DressZona(2, j) = 1
      '1
      
      Ang0 = MolaCD.AlfaROT
      If (.PUNTO(2, j).X = 0 And GearIMMAG.ProfiloK.BOMB(1, j) = 0) Then X0 = 0: Y0 = 0
                
      Call CalcolaPuntiTrattoLineareG450_PIEDE(j, GearIMMAG.ProfiloK.Rastr(1, j), X1, Y1, ang1, X0, Y0, Ang0, PuntiProfilaturaZ2(), NumPuntiTOT(j))
      
        
        
      End If
Next j
'
'
'5.
End With

'6.




For j = 1 To 2

   For i = 1 To I_Fine

      If (GearIMMAG.ProfiloK.BOMB(i, j) <> 0) Then
   
        'VARIABILE DI CONTROLLO
        Dim DeltaBOMB(DimensioneArray, 1 To 2) As Double
     
            With MolaCD.SEZIONE
                       
              X1 = .PUNTO(i, j).X: Y1 = .PUNTO(i, j).Y: ang1 = .PUNTO(i, j).ANGOLO
              X0 = .PUNTO(i - 1, j).X: Y0 = .PUNTO(i - 1, j).Y: Ang0 = .PUNTO(i - 1, j).ANGOLO

            End With


        Call CalcolaPuntiBombaturaG450(j, X1, Y1, ang1, X0, Y0, Ang0, GearIMMAG.ProfiloK.BOMB(i, j), PuntiProfilaturaZ2, NumPuntiTOT(j), DeltaBOMB)
        
        '
        'FONDO -- Punti da EAP a fondo mola
        
        If i = I_Fine Then
            With MolaCD.SEZIONE

                X1 = .AltezzaTotale - .Punto(i, j).X: Y1 = 0: ang1 = .Punto(i, j).ANGOLO
                X0 = 0: Y0 = 0: Ang0 = 0

            End With
            'Aggiungo i punti che realizzano il fondo della sagoma
            '2
            Ang0 = MolaCD.AlfaROT
            
            Call CalcolaPuntiTrattoLineareG450(j, X1, Y1, ang1, X0, Y0, Ang0, PuntiProfilaturaZ2, NumPuntiTOT(j))
            
        End If
        'Gestione Profilatore
        DressZona(1, j) = 0
        DressZona(2, j) = 1
     
'1.
Else
     ' Inizio da i=2 perch� i punti che realizzano la sagoma dall'altezza dente (da HMI) al punti 1 sono gi� stati calcolati, o il
     ' tratto viene realizzato dalla zona 1 del rullo. Solo per rastremazioni.
     If i <> 1 Then
     With MolaCD.SEZIONE
  
       X1 = .AltezzaTotale - .Punto(i - 1, j).X: Y1 = 0: ang1 = .Punto(i, j).ANGOLO
       'Se i � l'ultimo punto, considero il segmento fino allo 0, per profilare anche il fondo
       If i <> I_Fine Then
         X0 = .AltezzaTotale - .Punto(i, j).X: Y0 = 0: Ang0 = .Punto(i - 1, j).ANGOLO
       Else
          X0 = 0: Y0 = 0: Ang0 = .PUNTO(i - 1, j).ANGOLO
       End If
     End With
     '3
     Ang0 = MolaCD.AlfaROT
     Call CalcolaPuntiTrattoLineareG450(j, X1, Y1, ang1, X0, Y0, Ang0, PuntiProfilaturaZ2, NumPuntiTOT(j))

  End If
  End If
  
  Next i
Next j

'

'TRASFERIMENTO DATI:
'Creo il file DATI_CONTOURDRESSING.SPF per trasmettere i dati al CN.

Dim FileNum As Integer
Dim Z As Integer

FileNum = FreeFile
Open g_chOemPATH & "\DATI_CONTOURDRESSING.SPF" For Output As #FileNum
 Print #FileNum, ";DATI_CONTOURDRESSING.SPF"
 Print #FileNum, ";VERSIONE:" & Date & " " & Time()
 Print #FileNum, ";Programma per trasmissione dati Contour Dressing"
 Print #FileNum, " "
 Print #FileNum, " "
     

 For j = 1 To 2
 Print #FileNum, ";Dati Zona 1 Fianco " & Trim(j)
 Print #FileNum, " "
              Print #FileNum, "DRESSF" & Trim(j) & "Z1_G=" & Format(DressZona(1, j))
              Print #FileNum, " "
              Print #FileNum, "ANGOLOF" & Trim(j) & "Z1_G=" & Format(RulloCD.StaccoInclinazione(j), "0.0000")
              Print #FileNum, "RADIALEF" & Trim(j) & "Z1_G=" & Format(RulloCD.StaccoRadiale(j), "0.0000")
              Print #FileNum, "TANGENZF" & Trim(j) & "Z1_G=" & Format(RulloCD.StaccoTangenz(j), "0.0000")
              Print #FileNum, " "
 
 Next j
 
For j = 1 To 2
 Print #FileNum, " "
 Print #FileNum, ";Dati Zona 2 Fianco " & Trim(j)
 Print #FileNum, " "
 Z = 2
        Print #FileNum, "DRESSF" & Trim(j) & "Z2_G=" & Format(DressZona(2, j))
        Print #FileNum, " "

        
        For k = 0 To NumPuntiTOT(j)
              
              Print #FileNum, "ANGOLOF" & Trim(j) & "Z" & Trim(Z) & "_G[" & Trim(k) & "]=" & Format(PuntiProfilaturaZ2(k, j).ANGOLO, "0.0000")
              Print #FileNum, "RADIALEF" & Trim(j) & "Z" & Trim(Z) & "_G[" & Trim(k) & "]=" & Format(PuntiProfilaturaZ2(k, j).X, "0.0000")
              Print #FileNum, "TANGENZF" & Trim(j) & "Z" & Trim(Z) & "_G[" & Trim(k) & "]=" & Format(PuntiProfilaturaZ2(k, j).Y, "0.0000")
              Print #FileNum, " "
              ' E' SOLO UN CONTROLLO:
              ' permette di conoscere i valori di bombatura di ogni punto direttamente nel file .SPF.
              If (Bombatura) Then Print #FileNum, " " & ";Delta Bombatura:" & Format(DeltaBOMB(k, j), "0.0000")

              Print #FileNum, " "
        Next k
        
         Print #FileNum, "PUNTIF" & Trim(j) & "Z2_G=" & Trim(NumPuntiTOT(j))
         
Next j

Print #FileNum, " "
Print #FileNum, "M17"
Close FileNum

 
 
'TRASMISSIONE DEL SOTTOPROGRAMMA AL CNC
  Dim SORGENTE_CD     As String
  Dim DESTINAZIONE_CD As String
  Dim Session_CD      As IMCDomain
  SORGENTE_CD = g_chOemPATH & "\DATI_CONTOURDRESSING.SPF"
  DESTINAZIONE_CD = "/NC/WKS.DIR/MOLAVITE.WPD/DATI_CONTOURDRESSING.SPF"
  Set Session_CD = GetObject("@SinHMIMCDomain.MCDomain")
  Session_CD.CopyNC SORGENTE_CD, DESTINAZIONE_CD, MCDOMAIN_COPY_NC
  Set Session_CD = Nothing










'
End Sub
Public Sub LeggiDatiGeometrici()
'Routine per acquisizione dati geometrici ingranaggio
With Ing.DatiGeometrici                   'Variabili per CONTATTI_MV
      .Modulo = LEP("MN_G"):               Mr = .Modulo
      .NumDenti = LEP("NUMDENTI_G"):       Z1 = .NumDenti
      .AlfaNorm = LEP("ALFA_G"):           Aprd = .AlfaNorm
      .AngoloElica = LEP("BETA_G"):        HelpD = .AngoloElica
      .DiamEXT = LEP("DEXT_G"):            DE1 = .DiamEXT
      .DiamINT = LEP("DINT_G"):            Di1 = .DiamINT
      .DiamSAP(1) = LEP("DSAP_G"):      .DiamSAP(2) = LEP("DSAP_G"):       DsapRich = LEP("DSAP_G")
      .DiamEAP(1) = LEP("DEAP_G"):      .DiamEAP(2) = LEP("DEAP_G"):
      .TipoSpessore = LEP("iDefSp")
      .SN = LEP("R[938]")
      .QuotaWK = LEP("QW")
      .DentiWK = LEP("ZW")
      .QuotaR = LEP("QR")
      .DiamR = LEP("DR")
      .FattoreX = LEP("FattoreX")
 End With


End Sub
Public Sub LeggiParametriMola()
'Routine per acquisizione dati mola
MolaCD.AlfaROT = LEP("ALFAUT_G")
MolaCD.ModuloROT = LEP("MNUT_G")
MolaCD.AltezzaDenteMola = LEP("HDENTEMOLA_G")
'Acquisizione correttori per Contour Dressing
For j = 1 To 2

              DeltaXRulloZ1(j) = LEP("CORRADIALEF" & Trim(j) & "Z1_G")
              DeltaYRulloZ1(j) = LEP("CORTANGENZF" & Trim(j) & "Z1_G")
              DeltaXRulloZ2(j) = LEP("CORRADIALEF" & Trim(j) & "Z2_G")
              DeltaYRulloZ2(j) = LEP("CORTANGENZF" & Trim(j) & "Z2_G")
              
Next j

End Sub
Public Sub LeggiParametriRullo()

With RulloCD
'Dati Generali
.DiametroEsterno = LEP("DRULLOPROF_G")
.Spessore = LEP("SPESSRULLO_G")

 TipoRullo = LEP("TIPORULLO_G")
 If TipoRullo = 3 Then
 
     'Dati Fianco 1 (Standard)
      .RaggioRiferimento(1) = LEP("RAGGRIFZ1_G")
      .AngoloRiferimento(1) = LEP("ANGOLOPRESSIONEZ1_CD")
      .AltezzaFianco(1) = LEP("ALTEZZARULLOZ1_CD")
      .RaggioFianco(1) = LEP("RAGGIOBOMBZ1_CD")
      .SpessoreRiferimento(1) = LEP("SPESRIFZ1_G")
      .RaggioTesta(1) = LEP("RAGGIOTESTAZ1_CD")
      
      .RiferimentoTangenziale(1) = LEP("RIF_TANGENZZ1_G")
      .RiferimentoRadiale(1) = LEP("RIF_RADIALEZ1_G")
      
     'Dati Fianco 2 (Contour Dressing)
      .RaggioRiferimento(2) = LEP("RAGGRIFZ2_G")
      .AngoloRiferimento(2) = LEP("ANGOLOPRESSIONEZ2_CD")
      .AltezzaFianco(2) = LEP("ALTEZZARULLOZ2_CD")
      .RaggioFianco(2) = LEP("RAGGIOBOMBZ2_CD")
      .SpessoreRiferimento(2) = LEP("SPESRIFZ2_G")
      .RaggioTesta(2) = LEP("RAGGIOTESTAZ2_CD")
      '
      .RiferimentoTangenziale(2) = LEP("RIF_TANGENZZ2_G")
      .RiferimentoRadiale(2) = LEP("RIF_RADIALEZ2_G")
Else
     'CASO RULLO GOTICO.
     'CONSIDERO I DATI DEL RULLO SGROSSATORE PER IL CALCOLO DELLA CREMAGLIERA.
     'I PUNTI PER REALIZZARE LE MODIFICHE SONO DETERMINATI DAL RAGGIO DEL RULLO
     'CONTOUR DRESSING.
     'MANTENGO LA STESSA FILOSOFIA DEL RULLO SAMP:
     'IL RULLO SGROSSATORE CORRISPONDE ALLA ZONA 1
     'IL RULLO GOTICO CORRISPONDE ALLA ZONA 2
     
       .DiametroEsterno = LEP("DRULLOPROF_G")
       .DiametroEsternoGotico = LEP("DRULLOPROF_CONTOUR_G")
     
       Dim RaggioPieno(1 To 2) As Double
       RaggioPieno(1) = LEP("RAGGIOPIENO1_CD")
       RaggioPieno(2) = LEP("RAGGIOPIENO2_CD")
       
       .AltezzaFianco(2) = RaggioPieno(1)
       
       
       .RaggioRiferimento(1) = LEP("RG_RIF_RULLO")
       .SpessoreRiferimento(1) = LEP("SP_RIF_RULLO")
       .AngoloRiferimento(1) = LEP("AP_RULLO")
       
       
       
       
       
       
       
       
       
       
End If

End With
End Sub
Public Sub LeggiDatiProfilo()
Dim ValZonaStandard As Integer
'Routine per acqusizione dati profilo K
' j=fianco i= TIF
With Ing.ProfiloK
  .TIFDIAM = LEP("TIFDIAM_CD")
    .Tif(0, 1) = LEP("SAP_F1_CD")    'SAP Fianco 1
    .Tif(0, 2) = LEP("SAP_F2_CD")    'SAP Fianco 2
    For j = 1 To 2
       For i = 1 To 3
        .Tif(i, j) = LEP("TIF" & Trim(i) & "_F" & Trim(j) & "_CD")
        .Rastr(i, j) = LEP("RASTR" & Trim(i) & "_F" & Trim(j) & "_CD")
        .BOMB(i, j) = LEP("BOMB" & Trim(i) & "_F" & Trim(j) & "_CD")
        Next i
        .RastrPT(j) = LEP("DEVANG_F" & Trim(j) & "_CD")
       ' SPOSTATIF_F1_CD;SPOSTATIF_F2_CD
        CorrPunto_K(j) = LEP("SPOSTATIF_F" & Trim(j) & "_CD")
        
        
    Next j
 ValZonaStandard = LEP("UTILIZZAZONA1_CD")
 If ValZonaStandard = 0 Then
    .ZonaStandard = False
    Else
     .ZonaStandard = True
 End If


End With


End Sub


Public Function InvRAD(ByRef ANGOLO As Double)

On Error Resume Next

InvRAD = Tan(ANGOLO * RAD) - ANGOLO * RAD

End Function
Public Sub CalcolaProfiloG450()

End Sub
Public Function CalcolaTifG450(ByRef DiametroTIF As Double, ByRef DiamBaset) As Double

If DiametroTIF < DiamBaset Then
     Write_Dialog "CONTROLLARE DATI PROFILO K"
     Exit Function
End If
'Conversione da Diametro a TIF
CalcolaTifG450 = Sqr(((DiametroTIF ^ 2) / 4) - ((DiamBaset ^ 2) / 4))

End Function
Public Function CalcolaDiametroG450(ByRef PuntoTIF As Double, ByRef DiamBaset) As Double

CalcolaDiametroG450 = Sqr((PuntoTIF ^ 2) + (DiamBaset ^ 2) / 4) * 2

End Function
Public Function CalcolaSpessoreNormale(ByRef GearData As DatiGeometrici_Ingranaggio)
'Calcolo dello spessore normale dell'ingranaggio a seconda del tipo di input

Dim ALFAt As Single
Dim ALFANRad As Single
Dim betaRad As Single
Dim InvAlfaT As Single
Dim InvAlfaN As Single
Dim MnBase As Single
Dim PnBase As Single
Dim ZnW As Single

Dim E1 As Single
Dim H2 As Single
Dim H3 As Single
Dim i2 As Single
Dim I4 As Single

On Error Resume Next

With GearData
        ALFANRad = .AlfaNorm * PI / 180
        betaRad = .AngoloElica * PI / 180
        ALFAt = Atn(Tan(ALFANRad) / Cos(Abs(betaRad)))
        InvAlfaT = Tan(ALFAt) - ALFAt
        InvAlfaN = Tan(ALFANRad) - ALFANRad
Select Case .TipoSpessore
 Case 1 ' Spessore Normale
        CalcolaSpessoreNormale = .SN
 Case 2 ' Quota Wildhaber
        MnBase = .Modulo * Cos(ALFANRad)
        PnBase = PI * MnBase
        ZnW = InvAlfaT / InvAlfaN * .NumDenti
        CalcolaSpessoreNormale = ((.QuotaWK - PnBase * (.DentiWK - 1)) / (MnBase * ZnW) - InvAlfaN) * .Modulo * ZnW
 Case 3 ' Quota Rulli
        
        If Int(.NumDenti / 2) * 2 <> .NumDenti Then
          E1 = PI / (2 * .NumDenti)
         Else
          E1 = 0
        End If
        H2 = .Modulo * .NumDenti * Cos(ALFAt) * Cos(E1) / ((.QuotaR - .DiamR) * Cos(betaRad))
        H3 = Sqr(1 - H2 ^ 2) / H2
        i2 = (InvAlfaT + .DiamR / (.Modulo * .NumDenti * Cos(ALFANRad)) - H3 + Atn(H3)) * .Modulo * .NumDenti / Cos(betaRad)
        I4 = i2 * Cos(betaRad)
        CalcolaSpessoreNormale = PI * .Modulo - I4
 Case 4  'Fattore di spostamento X
        CalcolaSpessoreNormale = .Modulo * 2 * Tan(ALFANRad) + PI / 2 * .Modulo
 End Select
End With


End Function
Public Function CalcolaRaccordoG450()

 Dim NumPuntiRaccordo As Integer: NumPuntiRaccordo = 3
 
 
 
End Function
Public Function CalcolaPuntiTrattoLineareG450_PIEDE(ByVal T, ByVal Rastremazione, ByVal X1, ByVal Y1, ByVal ang1, ByVal X0, ByVal Y0, ByVal Ang0, CoordinateTRATTO() As PUNTO, ByRef NumPuntiTOT)
      
      
      Dim SegmentoZ2 As Double
      Dim NumpuntiZ2 As Double
      Dim ResiduoZ2 As Double
      Dim DeltaDiamante As Double
      Dim DeltaAngolo As Double
      '
      Dim AngRastr As Double
      

'
'
       Dim K_Z2 As Integer
'
'      'CALCOLI PRELIMINARI
       SegmentoZ2 = X1 - X0
       
       If TipoRullo = 3 Then
            DeltaAngolo = (-1) ^ T * (ang1 - RulloCD.AngoloRiferimento(2))
            'DeltaDiamante = RulloCD.AltezzaFianco(2) * Cos(RAD * RulloCD.AngoloRiferimento(2))
            DeltaDiamante = RulloCD.AltezzaFianco(2) * Cos(RAD * ang1)
          Else
            DeltaAngolo = 0
            DeltaDiamante = RulloCD.AltezzaFianco(2) * Cos(RAD * MolaCD.AlfaROT)
            
       End If
        
       NumpuntiZ2 = Int(SegmentoZ2 / DeltaDiamante)
       ResiduoZ2 = SegmentoZ2 - NumpuntiZ2 * DeltaDiamante
        
        
        
        
        
       CoordinateTRATTO(NumPuntiTOT, T).X = X1 - DeltaRadialeZ2(T) - DeltaDiamante
       CoordinateTRATTO(NumPuntiTOT, T).Y = ((X1 - DeltaRadialeZ2(T) - DeltaDiamante) * Tan(RAD * Ang0) + DeltaTangenzialeZ2(T)) - Rastremazione
       'PUNTO DI RACCORDO INFERIORE
       CoordinateTRATTO(NumPuntiTOT, T).ANGOLO = DeltaAngolo
       
       Dim TestSagoma1(DimensioneArray) As Double
       Dim TestSagoma2(DimensioneArray) As Double
       
       For K_Z2 = NumPuntiTOT + 1 To NumPuntiTOT + NumPuntiZ2
       
         CoordinateTRATTO(K_Z2, T).X = CoordinateTRATTO(K_Z2 - 1, T).X - DeltaDiamante
         CoordinateTRATTO(K_Z2, T).Y = (CoordinateTRATTO(K_Z2 - 1, T).Y - DeltaDiamante * Tan(RAD * ang1))
         Dim TestDelta
         TestSagoma1(K_Z2) = (CoordinateTRATTO(K_Z2 - 1, T).Y - DeltaDiamante * Tan(RAD * MolaCD.AlfaROT))
         TestSagoma2(K_Z2) = CoordinateTRATTO(K_Z2, T).Y - TestSagoma1(K_Z2)
         
         TestDelta = CoordinateTRATTO(K_Z2, T).Y - CoordinateTRATTO(K_Z2 - 1, T).Y
         CoordinateTRATTO(K_Z2, T).ANGOLO = DeltaAngolo
         

       
       Next K_Z2
       'PUNTO AGGIUNTIVO PER COMPLETARE IL TRATTO. UTILIZZO IL RESIDUO CALCOLATO IN PRECEDENZA

       CoordinateTRATTO(K_Z2, T).X = CoordinateTRATTO(K_Z2 - 1, T).X - ResiduoZ2
       CoordinateTRATTO(K_Z2, T).Y = (-1) ^ T * CoordinateTRATTO(K_Z2, T).X * Tan(ang1 * RAD)
       'PUNTO DI RACCORDO SUPERIORE
       CoordinateTRATTO(K_Z2, T).ANGOLO = DeltaAngolo
 
       For K_Z2 = NumPuntiTOT To NumPuntiTOT + NumPuntiZ2
         CoordinateTRATTO(K_Z2, T).Y = (-1) ^ T * CoordinateTRATTO(K_Z2, T).Y
         'RIMUOVO I PUNTI OLTRE LO ZERO DELLA SAGOMA
          If CoordinateTRATTO(K_Z2, T).X < 0.001 Then
               NumPuntiTOT = K_Z2 - 1
               Exit Function
           End If
       Next K_Z2

            
       NumPuntiTOT = NumPuntiTOT + NumPuntiZ2 + 1
    
'
        
      
End Function
Public Function CalcolaPuntiTrattoLineareG450(ByVal T, ByVal X1, ByVal Y1, ByVal ang1, ByVal X0, ByVal Y0, ByVal Ang0, CoordinateTRATTO() As PUNTO, ByRef NumPuntiTOT)
      
      
      Dim SegmentoZ2 As Double
      Dim NumPuntiZ2 As Double
      Dim ResiduoZ2 As Double
      Dim DeltaDiamante As Double
      Dim DeltaAngolo As Double
      '
      Dim AngRastr As Double
      

'
'
       Dim K_Z2 As Integer
'
'      'CALCOLI PRELIMINARI

       SegmentoZ2 = X1 - X0
       
       If TipoRullo = 3 Then
            DeltaAngolo = (-1) ^ T * (ang1 - RulloCD.AngoloRiferimento(2))
            DeltaDiamante = RulloCD.AltezzaFianco(2) * Cos(RAD * ang1)
            'DeltaDiamante = RulloCD.AltezzaFianco(2) * Cos(RAD * RulloCD.AngoloRiferimento(2))
          Else
            DeltaAngolo = 0
            DeltaDiamante = RulloCD.AltezzaFianco(2) * Cos(RAD * MolaCD.AlfaROT)
       End If
       
       NumPuntiZ2 = Int(SegmentoZ2 / DeltaDiamante)
       ResiduoZ2 = SegmentoZ2 - NumPuntiZ2 * DeltaDiamante
       
       
       'PUNTO DI RACCORDO INFERIORE
       'Punto iniziale del tratto ---> Ang0 = angolo di pressione della cremagliera
       
       CoordinateTRATTO(NumPuntiTOT, T).X = X1 - DeltaRadialeZ2(T) - DeltaDiamante
       CoordinateTRATTO(NumPuntiTOT, T).Y = ((X1 - DeltaRadialeZ2(T) - DeltaDiamante) * Tan(RAD * Ang0) + DeltaTangenzialeZ2(T))
       
       CoordinateTRATTO(NumPuntiTOT, T).ANGOLO = DeltaAngolo
       
       Dim TestSagoma1(DimensioneArray) As Double
       Dim TestSagoma2(DimensioneArray) As Double
       
       For K_Z2 = NumPuntiTOT + 1 To NumPuntiTOT + NumpuntiZ2
       
         CoordinateTRATTO(K_Z2, T).X = CoordinateTRATTO(K_Z2 - 1, T).X - DeltaDiamante
         CoordinateTRATTO(K_Z2, T).Y = (CoordinateTRATTO(K_Z2 - 1, T).Y - DeltaDiamante * Tan(RAD * ang1))
         Dim TestDelta As Double
         TestSagoma1(K_Z2) = (CoordinateTRATTO(K_Z2 - 1, T).Y - DeltaDiamante * Tan(RAD * MolaCD.AlfaROT))
         TestSagoma2(K_Z2) = CoordinateTRATTO(K_Z2, T).Y - TestSagoma1(K_Z2)
         
         TestDelta = CoordinateTRATTO(K_Z2, T).Y - CoordinateTRATTO(K_Z2 - 1, T).Y
         CoordinateTRATTO(K_Z2, T).ANGOLO = DeltaAngolo
         

       
       Next K_Z2
       
       'PUNTO AGGIUNTIVO PER COMPLETARE IL TRATTO. UTILIZZO IL RESIDUO CALCOLATO IN PRECEDENZA
       CoordinateTRATTO(K_Z2, T).X = CoordinateTRATTO(K_Z2 - 1, T).X - ResiduoZ2
       CoordinateTRATTO(K_Z2, T).Y = (-1) ^ T * CoordinateTRATTO(K_Z2, T).X * Tan(ang1 * RAD)
       
       'PUNTO DI RACCORDO SUPERIORE
       CoordinateTRATTO(K_Z2, T).ANGOLO = DeltaAngolo
 
       For K_Z2 = NumPuntiTOT To NumPuntiTOT + NumPuntiZ2
         CoordinateTRATTO(K_Z2, T).Y = (-1) ^ T * CoordinateTRATTO(K_Z2, T).Y
         'RIMUOVO I PUNTI OLTRE LO ZERO DELLA SAGOMA
          If CoordinateTRATTO(K_Z2, T).X < 0.001 Then
               NumPuntiTOT = K_Z2 - 1
               Exit Function
          End If
       Next K_Z2

            
       NumPuntiTOT = NumPuntiTOT + NumPuntiZ2 + 1
    
'
        
      
End Function
Public Function CalcolaPuntiBombaturaG450(ByVal T, ByVal X1, ByVal Y1, ByVal ang1, ByVal X0, ByVal Y0, ByVal Ang0, ByVal BombaturaFianco, CoordinateBOMB() As Punto, ByRef NumPuntiTOT, DeltaTangenziale() As Double)

 'Calcolo per bombatura di profilo (circolare)

     Dim k As Integer
   
     Dim Spezzata As Double
     Dim Corda As Double
     Dim AngoloSpezzata As Double
     Dim RaggioBOMB As Double
     Dim CentroBOMB As Punto
     Dim SegmentoBOMB As Double
     Dim ResiduoBOMB As Double
     Dim RisoluzioneBOMB As Double
     Dim NumPuntiBOMB As Integer
     Dim PuntoMedio As Punto
     Dim PuntoBOMB As Punto
     Dim AngoloBOMB As Double
     Dim DeltaAngolo As Double
     '
     Dim NuovaQuotaY(DimensioneArray) As Double
     Dim DeltaAngoloBOMB As Double


     '
     SegmentoBOMB = Sqr((X1 - X0) ^ 2 + (Y1 - Y0) ^ 2)
     Dim Diff As Single: Diff = Y1 - Y0
     
     Spezzata = Sqr(SegmentoBOMB ^ 2 / 4 + (BombaturaFianco) ^ 2)
     AngoloSpezzata = ang1
     '

     NumPuntiBOMB = (Int((Spezzata) / RulloCD.AltezzaFianco(2))) * 2 + 2
     Dim NumPuntiBOMB_2 As Single: NumPuntiBOMB_2 = (Int((X1 - X0) / (RulloCD.AltezzaFianco(2) / Cos(RAD * MolaCD.AlfaROT))))
     Dim ResiduoBomb_2 As Single: ResiduoBomb_2 = SegmentoBOMB - RulloCD.AltezzaFianco(2) * NumPuntiBOMB_2
     ResiduoBOMB = Spezzata - RulloCD.AltezzaFianco(2) * (NumPuntiBOMB) / 2
     RisoluzioneBOMB = Spezzata / (NumPuntiBOMB / 2)
     
     
     RaggioBOMB = SegmentoBOMB ^ 2 / (8 * BombaturaFianco) + BombaturaFianco / 2

     
     '***********************************
     'CALCOLO PUNTO MEDIO DELLA BOMBATURA
     
     
     PuntoMedio.X = X0 + (X1 - X0) / 2
     PuntoMedio.Y = Y0 + (Y1 - Y0) / 2
     
    
     PuntoBOMB.X = PuntoMedio.X + BombaturaFianco * Sin(RAD * ang1)
     PuntoBOMB.Y = PuntoMedio.Y - BombaturaFianco * Cos(RAD * ang1)
     Dim DistBOMB As Single: DistBOMB = Sqr((PuntoBOMB.X - PuntoMedio.X) ^ 2 + (PuntoBOMB.Y - PuntoMedio.Y) ^ 2)
     
     
   
     '***************************************************************
     'CALCOLO IL CENTRO DELLA CIRCONFERENZA CHE DESCRIVE LA BOMBATURA
     
     CentroBOMB.X = PuntoBOMB.X - RaggioBOMB * Sin(RAD * AngoloSpezzata)
     CentroBOMB.Y = PuntoBOMB.Y + RaggioBOMB * Cos(RAD * AngoloSpezzata)
     AngoloBOMB = Atn((SegmentoBOMB / 2) / (RaggioBOMB - BombaturaFianco)) / RAD
     DeltaAngolo = AngoloBOMB / (NumPuntiBOMB / 2)

     
     Dim SagomaBOMB(DimensioneArray) As Punto
     
     '****************************************
     'CALCOLO COORDINATE PUNTI DELLA BOMBATURA
      
      For k = 0 To NumPuntiBOMB
      
         SagomaBOMB(k).ANGOLO = (AngoloBOMB - DeltaAngolo * k)
         SagomaBOMB(k).X = CentroBOMB.X + RaggioBOMB * Sin(RAD * (AngoloSpezzata - SagomaBOMB(k).ANGOLO))
         SagomaBOMB(k).Y = CentroBOMB.Y - RaggioBOMB * Cos(RAD * (AngoloSpezzata - SagomaBOMB(k).ANGOLO))

         

     'RICALCOLO COORDINATE --> CAMBIO DI RIFERIMENTO. IN PROFILATURA COINCIDE CON IL PUNTO DI FASATURA MOLA-RULLO.
      
      NuovaQuotaY(k) = Y0 + (SagomaBOMB(k).X - X0) * Tan(RAD * ang1)
      DeltaTangenziale(NumPuntiTOT + k, T) = (-1) ^ T * (SagomaBOMB(k).Y - NuovaQuotaY(k))
      Dim DIST(DimensioneArray) As Single: DIST(k) = DeltaTangenziale(NumPuntiTOT + k, T) * Cos(RAD * MolaCD.AlfaROT)
      
            
              
        'CALCOLO COORDINATE PER CONTOUR DRESSING. PUNTI DELLA CREMAGLIERA TEORICA + CORREZIONE DOVUTA ALLA BOMBATURA
        'GESTIONE CORREZIONE POSIZIONE RULLO
        
        CoordinateBOMB(NumPuntiTOT + k, T).X = MolaCD.SEZIONE.AltezzaTotale - SagomaBOMB(k).X - DeltaRadialeZ2(T) - RulloCD.AltezzaFianco(2) * Cos(RAD * MolaCD.AlfaROT)
        CoordinateBOMB(NumPuntiTOT + k, T).Y = (-1) ^ T * (CoordinateBOMB(NumPuntiTOT + k, T).X * Tan(RAD * MolaCD.AlfaROT)) - DeltaTangenziale(k, T)
        CoordinateBOMB(NumPuntiTOT + k, T).Y = CoordinateBOMB(NumPuntiTOT + k, T).Y + (-1) ^ T * DeltaTangenzialeZ2(T)

        If TipoRullo = 3 Then
         '
             CoordinateBOMB(NumPuntiTOT + k, T).ANGOLO = (-1) ^ T * ((ang1 - RulloCD.AngoloRiferimento(2)) - SagomaBOMB(k).ANGOLO)
             
           Else
           
             CoordinateBOMB(NumPuntiTOT + k, T).ANGOLO = 0
             
         End If
         

         
     Next k

Dim FileNum As Integer: FileNum = FreeFile
Open "C:\TEST_EXCEL.txt" For Output As #FileNum
For k = 0 To NumPuntiBOMB
Print #FileNum, Format(CoordinateBOMB(NumPuntiTOT + k, T).X, "0.00000") & ";" & Format(CoordinateBOMB(NumPuntiTOT + k, T).Y, "0.00000") & ";" & Format(CoordinateBOMB(NumPuntiTOT + k, T).ANGOLO, "0.00000")
Next
Close FileNum



     NumPuntiTOT = NumPuntiTOT + NumPuntiBOMB + 1
     




End Function
'Public Function CalcolaPuntiBombaturaG450(ByVal T, ByVal X1, ByVal Y1, ByVal ang1, ByVal X0, ByVal Y0, ByVal Ang0, ByVal BombaturaFianco, CoordinateBOMB() As PUNTO, ByRef NumPuntiTOT, DeltaTangenziale() As Double)
'
' 'Calcolo per bombatura di profilo (circolare)
'
'     Dim k As Integer
'
'     Dim Spezzata As Double
'     Dim Corda As Double
'     Dim AngoloSpezzata As Double
'     Dim RaggioBOMB As Double
'     Dim CentroBOMB As PUNTO
'     Dim SegmentoBOMB As Double
'     Dim ResiduoBOMB As Double
'     Dim RisoluzioneBOMB As Double
'     Dim NumPuntiBOMB As Integer
'     Dim PuntoMedio As PUNTO
'     Dim PuntoBOMB As PUNTO
'     Dim AngoloBOMB As Double
'     Dim DeltaAngolo As Double
'     '
'     Dim NuovaQuotaY(DimensioneArray) As Double
'     Dim DeltaAngoloBOMB As Double
'     Dim BombaturaEquivalente As Double
'     Dim SegmentoTEST As Double
'     Dim SegmentoEquivalente As Double
'
'     'GESTIONE BOMBATURA SU TUTTA L'ALTEZZA DEL FILETTO
'
'     '*********************************
'     'CALCOLI PRELIMINARI
'     SegmentoBOMB = Sqr((X1 - X0) ^ 2 + (Y1 - Y0) ^ 2)
'     SegmentoTEST = (X1 - X0) / Cos(RAD * MolaCD.AlfaROT)
'     Spezzata = Sqr(SegmentoBOMB ^ 2 / 4 + (BombaturaFianco) ^ 2)
'     AngoloSpezzata = ang1
'     ' Calcoli come la GS400
'     Dim CatetoAltezza As Single
'     Dim CatetoII As Single
'     Dim Ipotenusa As Single
'     Dim AngoloTest As Single: Dim AngoloTestII As Single
'     Dim E112 As Single: Dim E102 As Single
'     Dim Seno As Single: Dim Coseno As Single
'
'
'     CatetoAltezza = (X1 - X0)
'     CatetoII = CatetoAltezza * Tan(MolaCD.AlfaROT * RAD)
'     Ipotenusa = Sqr(CatetoAltezza ^ 2 + CatetoII ^ 2)
'     '
'     AngoloTest = Atn(CatetoII / CatetoAltezza) / RAD
'     E112 = Ipotenusa * Cos((AngoloTest - MolaCD.AlfaROT) * RAD)
'     E102 = Ipotenusa * Sin((AngoloTest - MolaCD.AlfaROT) * RAD)
'
'     RaggioBOMB = SegmentoBOMB ^ 2 / (8 * BombaturaFianco) + BombaturaFianco / 2
'
'     Coseno = Ipotenusa / (2 * RaggioBOMB)
'     Seno = 1 - Coseno ^ 2
'     AngoloTestII = Atn(Seno / Coseno) / RAD
'
'
'
'     Dim TestAngolo As Double: TestAngolo = Atn((Y1 - Y0) / (X1 - X0)) / RAD
'     Dim TestAngoloII As Double: TestAngoloII = Atn(BombaturaFianco / (SegmentoBOMB / 2)) / RAD
'
'     NumPuntiBOMB = (Int(Spezzata / RulloCD.AltezzaFianco(2))) * 2
'
'     ResiduoBOMB = Spezzata - RulloCD.AltezzaFianco(2) * NumPuntiBOMB / 2
'     RisoluzioneBOMB = Spezzata * 2 / NumPuntiBOMB
'
'     RaggioBOMB = SegmentoBOMB ^ 2 / (8 * BombaturaFianco) + BombaturaFianco / 2
'     SegmentoEquivalente = MolaCD.AltezzaDenteMola / Cos(RAD * MolaCD.AlfaROT)
'     BombaturaEquivalente = Abs(-RaggioBOMB + Sqr(RaggioBOMB ^ 2 - (SegmentoEquivalente ^ 2) / 4))
'
'     '*********************************
'
'     With MolaCD.SEZIONE
'       X1 = MolaCD.AltezzaDenteMola: Y1 = .PUNTO(0, j).Y + MolaCD.AltezzaDenteMola * Tan(RAD * MolaCD.AlfaROT): ang1 = .PUNTO(1, j).ANGOLO
'       X0 = .PUNTO(0, j).X: Y0 = .PUNTO(0, j).Y: Ang0 = .PUNTO(0, j).ANGOLO
'     End With
'
'     SegmentoBOMB = Sqr((X1 - X0) ^ 2 + (Y1 - Y0) ^ 2)
'     SegmentoTEST = (X1 - X0) / Cos(RAD * MolaCD.AlfaROT)
'     Spezzata = Sqr(SegmentoBOMB ^ 2 / 4 + (BombaturaFianco) ^ 2)
'     Dim SegmentoTest2 As Double
'     Dim SegmentoTest3 As Double
'     SegmentoTest2 = Spezzata * 2
'     SegmentoTest3 = SegmentoBOMB - SegmentoTest2
'     AngoloSpezzata = ang1
'
'     NumPuntiBOMB = (Int(Spezzata / RulloCD.AltezzaFianco(2))) * 2
'
'     ResiduoBOMB = Spezzata - RulloCD.AltezzaFianco(2) * NumPuntiBOMB / 2
'     RisoluzioneBOMB = Spezzata * 2 / NumPuntiBOMB
'
'     'CALCOLO PUNTO MEDIO DELLA BOMBATURA
'     PuntoMedio.X = X0 + (X1 - X0) / 2
'     PuntoMedio.Y = Y0 + (Y1 - Y0) / 2
'
'
'     PuntoBOMB.X = PuntoMedio.X + BombaturaFianco * Sin(RAD * ang1)
'     PuntoBOMB.Y = PuntoMedio.Y - BombaturaFianco * Cos(RAD * ang1)
'
'     'PuntoBOMB.X = PuntoMedio.X + BombaturaEquivalente * Sin(RAD * ang1)
'     'PuntoBOMB.Y = PuntoMedio.Y - BombaturaEquivalente * Cos(RAD * ang1)
'
'
'
'
'     'CALCOLO IL CENTRO DELLA CIRCONFERENZA CHE DESCRIVE LA BOMBATURA
'     CentroBOMB.X = PuntoBOMB.X - RaggioBOMB * Sin(RAD * AngoloSpezzata)
'     CentroBOMB.Y = PuntoBOMB.Y + RaggioBOMB * Cos(RAD * AngoloSpezzata)
'     AngoloBOMB = Atn((SegmentoBOMB / 2) / (RaggioBOMB - BombaturaFianco)) / RAD
'     'AngoloBOMB = Atn((SegmentoEquivalente / 2) / (RaggioBOMB - BombaturaEquivalente)) / RAD
'     DeltaAngolo = AngoloBOMB / (NumPuntiBOMB / 2)
'     TestAngoloII = Atn(BombaturaEquivalente / (SegmentoEquivalente / 2)) / RAD
'
'     Dim SagomaBOMB(DimensioneArray) As PUNTO
'     Dim SagomaTEST(DimensioneArray) As PUNTO
'     Dim MyX(DimensioneArray) As Double
'     Dim MyY(DimensioneArray) As Double
'     Dim DeltaTest(DimensioneArray) As Double
'     'CALCOLO COORDINATE PUNTI DELLA BOMBATURA
'      For k = 0 To NumPuntiBOMB
'         SagomaBOMB(k).ANGOLO = (AngoloBOMB - DeltaAngolo * k)
'         SagomaBOMB(k).X = CentroBOMB.X + RaggioBOMB * Sin(RAD * (AngoloSpezzata - SagomaBOMB(k).ANGOLO))
'         SagomaBOMB(k).Y = CentroBOMB.Y - RaggioBOMB * Cos(RAD * (AngoloSpezzata - SagomaBOMB(k).ANGOLO))
'         MyY(k) = SagomaBOMB(k).Y
'
'         If k = 0 Then
'           MyX(k) = MolaCD.AltezzaDenteMola
'          Else
'           MyX(k) = MyX(k - 1) - RulloCD.AltezzaFianco(2) / Cos(RAD * MolaCD.AlfaROT)
'         End If
'
'         Dim MyA As Double: MyA = 1
'         Dim MyB As Double: MyB = -2 * CentroBOMB.Y
'         Dim MyC As Double: MyC = (MyX(k) - CentroBOMB.X) ^ 2 - RaggioBOMB ^ 2 + CentroBOMB.Y ^ 2
'
'         SagomaTEST(k).X = MyX(k)
'         SagomaTEST(k).Y = (-MyB - Sqr(MyB ^ 2 - 4 * MyA * MyC)) / (2 * MyA)
'         DeltaTest(k) = SagomaBOMB(k).Y - SagomaTEST(k).Y
'
'
'
'     'RICALCOLO COORDINATE --> CAMBIO DI RIFERIMENTO. IN PROFILATURA COINCIDE CON IL PUNTO DI FASATURA MOLA-RULLO.
'      NuovaQuotaY(k) = Y0 + (SagomaBOMB(k).X - X0) * Tan(RAD * ang1)
'      DeltaTangenziale(NumPuntiTOT + k, T) = (-1) ^ T * (SagomaBOMB(k).Y - NuovaQuotaY(k))
'
'        'CALCOLO COORDINATE PER CONTOUR DRESSING. PUNTI DELLA CREMAGLIERA TEORICA + CORREZIONE DOVUTA ALLA BOMBATURA
'        'GESTIONE CORREZIONE POSIZIONE RULLO
'        CoordinateBOMB(NumPuntiTOT + k, T).X = MolaCD.AltezzaDenteMola - SagomaBOMB(k).X - DeltaXRulloZ2(T)
'        CoordinateBOMB(NumPuntiTOT + k, T).Y = (-1) ^ T * (CoordinateBOMB(NumPuntiTOT + k, T).X * Tan(RAD * MolaCD.AlfaROT)) - DeltaTangenziale(k, T)
'        CoordinateBOMB(NumPuntiTOT + k, T).Y = CoordinateBOMB(NumPuntiTOT + k, T).Y + (-1) ^ T * DeltaYRulloZ2(T)
'        MyY(k) = CoordinateBOMB(NumPuntiTOT + k, T).Y
'
'         If TipoRullo = 3 Then
'         '
'             CoordinateBOMB(NumPuntiTOT + k, T).ANGOLO = (-1) ^ T * (-SagomaBOMB(k).ANGOLO)
'           Else
'             CoordinateBOMB(NumPuntiTOT + k, T).ANGOLO = 0
'         End If
'     Next k
'
'     NumPuntiTOT = NumPuntiTOT + NumPuntiBOMB
'
'     Dim FileNum As Integer: FileNum = FreeFile
'
'
'Open "C:\TEST_EXCEL.txt" For Output As #FileNum
' For k = 0 To NumPuntiBOMB
'
' Print #FileNum, Format(CoordinateBOMB(k, 2).X, "0.00000") & ";" & Format(CoordinateBOMB(k, 2).Y, "0.00000")
'
' Next
'
'Close FileNum
'
'
'
'End Function
Sub AzzeraDati()
    Dim i As Integer, j As Integer
    
    ' Contatti
    
    Scala = 0: SpostX = 0: SpostY = 0:
    CodiceOK$ = ""
    AddMola = 0: ApMolaD = 0: Apmola = 0: ApMolaApp = 0
    MrMola = 0: HelrMola = 0: DrMola = 0: RrMola = 0
    EpMolaApp = 0: EsMolaApp = 0: DedMolaAttivo = 0: DsapOtt = 0
    For i = 1 To 50
        ArotRxD(i) = 0: RxContatto(i) = 0: NcontattiRx_F1(i) = 0: NcontattiRx_F2(i) = 0: NcontattiRx(i) = 0
    Next i
    Ddis1 = 0: Ddis2 = 0: GiocoFondo = 0: Cliente$ = "": Disegno$ = "": DsapRich = 0
    Eb1Fin = 0: Mr = 0: Mapp = 0: Z1 = 0: Aprd = 0: Apr = 0: ApApp = 0:
    HelpD = 0: Help = 0: HELB = 0: Dp1 = 0: DB1 = 0: Di1 = 0: DE1 = 0: Re1 = 0:
    Kdt = 0: Wdt = 0: DPG = 0: Wpg = 0: Epr1 = 0: Spost = 0:
    Helr = 0: Dr1 = 0: RR1 = 0: AddFm = 0: EpFm = 0: MrFm = 0: ApFmD = 0: ApFm = 0: ApFmApp = 0:
    RsFm = 0: Xrs = 0: Yrs = 0: Esfm = 0: RPieno = 0: HpUtil = 0: AngRotD = 0:
    SuRep = 0: EpProtu = 0: ApProtuD = 0: ApProtu = 0:
    DactifRect = 0: DactifDent = 0
    AddMolaContatti = 0
    For i = 1 To 192
        Xfm(i) = 0: Yfm(i) = 0: Aprfm(i) = 0
        RX(i) = 0: AngRx(i) = 0: ApRx(i) = 0
    Next i
    For i = 1 To 50
        RxFin(i) = 0: AngRxFin(i) = 0
    Next i
    ENTRX = 0: RsFmApp = 0: EsFmApp = 0: PntAxe = 0: PntAxeCr = 0: PntFinRaCr = 0: NbpTotCr = 0: PntIniRaCr = 0:
    NbpDev = 0: NbpTroc = 0: NbpFond = 0: NbPiani = 0: PntFinTroc = 0: PntIniTroc = 0: PntMedTroc = 0: PntFinDev = 0:
    InterfRettif$ = "": InterfDent$ = ""
    
    'Contour Dressing G450
    'Azzero dati ingranaggi
    With Ing
    .AlfanROT = 0: .AlfatROT = 0: .DiamBASE = 0: .DIAMPRIM = 0: .DiamROT = 0: .ElicaBASE = 0: .ElicaROT = 0: .ModuloROT = 0
    .PassoBase = 0: .SNROT = 0: .SpesBaseN = 0: .SpesbaseT = 0: .STROT = 0
    End With
    
    With Ing.DatiGeometrici
    For i = 1 To 2
    .DiamEAP(i) = 0: .DiamSAP(i) = 0
    Next i
       .AlfaNorm = 0: .AlfaTrasv = 0: .AngoloElica = 0: .DentiWK = 0:  .DiamEXT = 0: .DiamINT = 0:
       .DiamR = 0: .FattoreX = 0: .Modulo = 0: .NumDenti = 0: .QuotaR = 0: .QuotaWK = 0: .SN = 0: .St = 0: .TipoSpessore = 0
       
    End With

    
    
    'Azzero dati mola
    With MolaCD
       .AlfaROT = 0: .AltezzaDenteMola = 0: .ModuloROT = 0
    End With
    
    With MolaCD.SEZIONE
       .ADDENDUM = 0: .AltezzaAttiva = 0: .AltezzaTotale = 0: .DEDENDUM = 0: .GiocoFondo = 0: .GiocoTesta = 0
       .Spessore = 0: .SpessoreFondo = 0: .SpessoreTesta = 0
    End With
    'Azzero dati rullo
    With RulloCD
    For i = 1 To 2
    .AltezzaFianco(i) = 0: .AngoloRiferimento(i) = 0: .RaggioFianco(i) = 0: .RaggioRiferimento(i) = 0
    .SpessoreRiferimento(i) = 0: .StaccoInclinazione(i) = 0: .StaccoRadiale(i) = 0: .StaccoTangenz(i) = 0: .RaggioTesta(i) = 0
    Next i
     
       .DiametroEsterno = 0: .Spessore = 0: .DiametroEsternoGotico = 0
    End With
    
    
End Sub


Function Acs(Aux)
   Dim Aux1
   Aux1 = (1 - Aux * Aux)
   If Aux1 < 0 Then
      MsgBox "Coseno > 1 : Il programma si ferma : Controllare i dati ", 16
      End
   Else
      If Aux = 0 Then
         Acs = PI / 2
      Else
         If Aux > 0 Then
            Acs = Atn(Sqr(1 - Aux * Aux) / Aux)
         Else
            Aux = Abs(Aux)
            Acs = Atn(Aux / Sqr(1 - Aux * Aux)) + 1.570796
         End If
      End If
   End If
End Function
'

Function Asn(Aux)
Dim Aux1
  Aux1 = (1 - Aux * Aux)
  If Aux1 < 0 Then
     MsgBox "Seno > 1 : Il programma si ferma : Controllare i dati ", 16
     End
  Else
     If Aux = 1 Then
        Asn = PI / 2
     Else
        Asn = Atn(Aux / Sqr(Aux1))
     End If
  End If
End Function
'

Function Fndr(A)
   Fndr = A * PI / 180
End Function
'

Function inv(Aux)
  inv = Tan(Aux) - Aux
End Function
'

Function Ainv(Aux)
   Dim i, Ymini, Ymaxi, Ymoyen, X1
    Ymini = 0: Ymaxi = 1.570796
    For i = 1 To 25
       Ymoyen = (Ymini + Ymaxi) / 2: X1 = inv(Ymoyen)
       If X1 < Aux Then Ymini = Ymoyen Else: Ymaxi = Ymoyen
    Next i
    Ainv = Ymoyen
End Function

Function Fnrd(A)
   Fnrd = A * 180 / PI
End Function
'

'


Public Sub CalcoloIngranaggioG450(ByRef Ing As Ingranaggio, ByRef GearCALCOLO As Ingranaggio)
'Routine per il calcolo delle caratteristiche geometriche dell'ingranaggio


On Error Resume Next

 With GearCALCOLO
 GearCALCOLO = Ing

'Calcolo diametro primitivo e di base
.DatiGeometrici.AlfaTrasv = Atn(Tan(RAD * Ing.DatiGeometrici.AlfaNorm) / Cos(RAD * Ing.DatiGeometrici.AngoloElica)) / RAD
.ElicaBASE = Atn(Tan(RAD * Ing.DatiGeometrici.AngoloElica) * Cos(RAD * Ing.DatiGeometrici.AlfaTrasv)) / RAD
.DIAMPRIM = Ing.DatiGeometrici.Modulo * Ing.DatiGeometrici.NumDenti / Cos(RAD * Ing.DatiGeometrici.AngoloElica)
.DiamBASE = .DIAMPRIM * Cos(RAD * .DatiGeometrici.AlfaTrasv)

'Calcolo spessore normale.
.DatiGeometrici.SN = CalcolaSpessoreNormale(Ing.DatiGeometrici)


'Calcolo dati di rotolamento (in sezione trasversale).
.ElicaROT = Atn(Tan(RAD * .ElicaBASE) / Cos(RAD * MolaCD.AlfaROT)) / RAD
.AlfatROT = Atn(Tan(RAD * MolaCD.AlfaROT) / Cos(RAD * .ElicaROT)) / RAD
.DiamROT = .DiamBASE / Cos(RAD * .AlfatROT)
.ModuloROT = .DiamROT / .DatiGeometrici.NumDenti

'Definizione angolo di pressione normale di rotolamento.
.AlfanROT = MolaCD.AlfaROT

'Calcolo spessore normale di rotolamento.
.DatiGeometrici.St = .DatiGeometrici.SN / Cos(RAD * .DatiGeometrici.AngoloElica)
.STROT = (.DatiGeometrici.St / .DIAMPRIM + InvRAD(.DatiGeometrici.AlfaTrasv) - InvRAD(.AlfatROT)) * .DiamROT
.SNROT = .STROT * Cos(RAD * .ElicaROT)


'Calcolo spessore e passo base.
.SpesbaseT = .DiamBASE * (.DatiGeometrici.St / .DIAMPRIM + InvRAD(.DatiGeometrici.AlfaTrasv))
.SpesBaseN = .SpesbaseT * Cos(RAD * .ElicaBASE)
.PassoBase = .DiamBASE * PI / .DatiGeometrici.NumDenti


 
   
End With

End Sub


Sub CalcoloCremMolaG450()

Dim Aux As Double, Aux1 As Double, Add As Double
    
    On Error Resume Next
    
    Apmola = Fndr(ApMolaD)
    MrMola = Mr * Cos(Apr) / Cos(Apmola)
    HelrMola = Asn(Sin(Help) * Cos(Apr) / Cos(Apmola))
    DrMola = MrMola / Cos(HelrMola) * Z1
    RrMola = DrMola / 2
    ApMolaApp = Acs(DB1 / DrMola)
    EpMolaApp = PI * DrMola / Z1 - ((Eb1Fin / DB1 - inv(Acs(DB1 / DrMola))) * DrMola)
    Add = (DrMola - Di1) / 2
    Aux = Sqr((DsapRich / 2) ^ 2 - (DB1 / 2) ^ 2)
    AddMola = Sin(ApMolaApp) * (DB1 / 2 * Tan(ApMolaApp) - Aux)
    
    GiocoFondo = (DrMola - Di1) / 2 - AddMola
    
    EsMolaApp = EpMolaApp - (2 * AddMola * Tan(ApMolaApp))
    DedMolaAttivo = (Sqr((DE1 / 2) ^ 2 - (DB1 / 2) ^ 2) - DB1 / 2 * Tan(ApMolaApp)) * Sin(ApMolaApp)
    DsapOtt = DsapRich

   ApFm = Fndr(ApFmD)
   ApProtu = Fndr(ApProtuD)
   Eb1Dent = Eb1Fin + SuRep * 2 / Cos(HELB)
   MrFm = Mr * Cos(Apr) / Cos(ApFm)
   Helr = Asn(Sin(Help) * Cos(Apr) / Cos(ApFm))
   Dr1 = MrFm / Cos(Helr) * Z1
   RR1 = Dr1 / 2
   ApFmApp = Acs(DB1 / Dr1)
   EpFm = PI * MrFm - ((Eb1Dent / DB1 - inv(Acs(DB1 / Dr1))) * Dr1) * Cos(Helr)
   AddFm = (Dr1 - Di1) / 2
   Esfm = EpFm - (2 * AddFm * Tan(ApFm)) + 2 * EpProtu / Cos(ApFm)
   RPieno = Esfm / 2 / Tan(PI / 4 - ApFm / 2)
   Xrs = Esfm / 2 - RsFm * Tan(PI / 4 - ApFm / 2)
   Yrs = AddFm - RsFm
   If Pr$ = "O" Then
        Aux = RsFm * (Tan(PI / 4 - ApProtu / 2) - Tan(PI / 4 - ApFm / 2))
        Hprot = (Aux + EpProtu / Cos(ApFm)) / (Tan(ApFm) - Tan(ApProtu))
   End If
   ' Ci-dessous : pour DefinitionCopeauParZone
   ENTRX = (Di1) / 2
   EsFmApp = Esfm / Cos(Helr)
   RsFmApp = (EsFmApp / 2 - Xrs) / Tan(PI / 4 - ApFmApp / 2)
    
End Sub

