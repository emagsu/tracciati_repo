Attribute VB_Name = "Molcrino"
Option Explicit
'
Private ParametriCR    As StructParametriCR
Private ParametriMola  As StructParametriMola
'
Private RG(30)      As Double
Private Phi(30)     As Double
Private RAPH(30)    As Double
Private VX(30)      As Double
Private RMG(30)     As Double
Private XMG(30)     As Double
Private Rd(30)      As Double
Private RMD(30)     As Double
Private XMD(30)     As Double
Private RX(30)      As Double
Private RM(30)      As Double
Private XM(30)      As Double
Private Oxp(30)     As Double
Private RxP(30)     As Double
Private dx(30)      As Double
Private Ep(30)      As Double
'
'PARAMETRI CR LETTI DA FILE
Private Paxial      As Double
Private Phel        As Double
Private Nf          As Integer
Private Z           As Integer
Private DestCr      As Double
Private Came        As Double
Private DeMola      As Double
Private Ep0         As Double
Private E39         As Double
Private Htotal      As Double
'
Private Apr         As Double
Private E42         As Double
Private ApSt        As Double
Private Add         As Double
Private E44         As Double
Private Hst         As Double
Private E45         As Double
Private EpProtu     As Double
Private E46         As Double
Private E47         As Double
Private Hprotu      As Double
Private E48         As Double
Private Rtesta      As Double
Private E49         As Double
Private Rfondo      As Double
Private E55         As Double
Private E56         As Double
Private E58         As Double
Private E59         As Double
Private E61         As Double
Private E62         As Double
Private E63         As Double
Private Piattino    As Double
Private E64         As Double
Private E71         As Double
Private E72         As Double
Private E73         As Double
Private E74         As Double
Private InMola      As Double
Private INCAX1      As Double
Private Mr          As Double
Private HRastt      As Double
Private ApRastt     As Double
Private HRastp      As Double
Private ApRastp     As Double
'
Private CalcAx$
Private Flanc$
'
Private i           As Integer
Private j           As Integer
Private Sens        As Integer
Private Passage     As Integer
'
Private Ent         As Double
Private InFilet     As Double
Private Rp          As Double
Private SAILLIE     As Double
Private ApaFl       As Double
Private Dbfl        As Double
Private InbFl       As Double
Private EpFl        As Double
Private EbDbFl      As Double
Private ApArastt    As Double
Private DbRastt     As Double
Private InbRastt    As Double
Private Ip          As Double
Private EpRastt     As Double
Private EbDbRastt   As Double
Private ApaRastp    As Double
Private DbRastp     As Double
Private InbRastp    As Double
Private EpRastp     As Double
Private EbDbRastp   As Double
Private SP          As Double
Private DmaxiApr    As Double
Private a           As Double
Private DminiApr    As Double
Private DB          As Double
Private EbDb        As Double
Private R           As Double
Private E           As Double
Private Rax         As Double
Private ApApp       As Double
Private Helx        As Double
Private RAX1        As Double
Private RAPH0       As Double
Private Dmaxi       As Double
Private Dmini       As Double
Private Phi0        As Double
Private D           As Double
Private RMola       As Double
Private XMola       As Double
Private RMmaxi      As Double
Private DRMOL       As Double
Private INCAX       As Double
Private CSINCAX     As Double
Private SNINCAX     As Double
Private TGINCAX     As Double
Private SxP         As Double
Private ApAxial     As Double
Private Apmola      As Double
Private XMA         As Double
Private RMA         As Double
Private PHIA        As Double
Private OP          As Double
Private APM         As Double
Private XM1         As Double
Private Apx         As Double
Private BOMB        As Double
Private APG         As Double
Private BombG       As Double
Private APD         As Double
Private BombD       As Double
Private X1          As Double
Private X2          As Double
Private Y1          As Double
Private Y2          As Double
Private APMG        As Double
Private BombMG      As Double
Private APMD        As Double
Private BombMD      As Double
Private HMrasttG    As Double
Private HMrasttD    As Double
Private APMRasttG   As Double
Private BombMrasttG As Double
Private APMRasttD   As Double
Private BombMrasttD As Double
Private HMrastpG    As Double
Private HMrastpD    As Double
Private APMRastpG   As Double
Private BombMrastpG As Double
Private APMRastpD   As Double
Private BombMrastpD As Double
Private DX0         As Double
Private PasDx       As Double
Private Apdx        As Double
Private posBomD     As Double
Private posBomG     As Double
Private RMprim      As Double
Private INCAX2      As Double
Private P1G         As Double
Private P2G         As Double
Private P3G         As Double
Private P4G         As Double
Private E106D       As Double
Private E106G       As Double
Private P7G         As Double
Private P8G         As Double
Private P1D         As Double
Private P2D         As Double
Private P3D         As Double
Private P4D         As Double
Private P5D         As Double
Private P7D         As Double
Private P8D         As Double
Private E206        As Double
Private E207        As Double
Private E5          As Double
Private E191        As Double
Private E192        As Double
Private E193        As Double
Private E38         As Double
Private DT1         As Double
Private DT2         As Double
Private ATG         As Double
Private ATD         As Double
Private E60         As Double
Private E41         As Double
Private E43         As Double
Private E31         As Double
Private E40         As Double
Private E65         As Double
Private E51         As Double
Private E52         As Double
Private E53         As Double
Private E54         As Double
Private E57         As Double
Private E32         As Double
Private E50         As Double
Private E75         As Double
Private E106        As Double
Private E107        As Double
Private E37         As Double
'
Private RaggioST1   As Double
Private RaggioST2   As Double
'
Private CSIM        As Double
Private SNIM        As Double
Private TGIM        As Double
Private TETAP2      As Double
'
Public Type StructParametriCR
  '
  'PARAMETRI GENERALI CR
  Paxial    As Double
  Phel      As Double
  Nf        As Integer
  Z         As Integer
  DestCr    As Double
  Came      As Double
  DeMola    As Double
  Ep0       As Double
  Htotal    As Double
  InMola    As Double
  Mr        As Double
  CalcAx    As String
  INCAX1    As Double
  '
  'PARAMETRI FIANCO 1
  Apr       As Double
  ApSt      As Double
  Add       As Double
  Hst       As Double
  EpProtu   As Double
  E46       As Double
  E47       As Double
  Rtesta    As Double
  Rfondo    As Double
  E61       As Double
  E62       As Double
  Piattino  As Double
  E64       As Double
  HRastt    As Double
  ApRastt   As Double
  HRastp    As Double
  ApRastp   As Double
  '
  'PARAMETRI FIANCO 2
  Apr2      As Double
  Apst2     As Double
  Add2      As Double
  Hst2      As Double
  E51       As Double
  E52       As Double
  E53       As Double
  E54       As Double
  E55       As Double
  E56       As Double
  E57       As Double
  E58       As Double
  E59       As Double
  E71       As Double
  E72       As Double
  E73       As Double
  E74       As Double
  HRastt2   As Double
  ApRastt2  As Double
  HRastp2   As Double
  ApRastp2  As Double
  '
End Type
'
Public Type StructParametriMola
  '
  'PARAMETRI MOLA
  E206        As Double
  E207        As Double
  E5          As Integer
  E191        As Integer
  E192        As Double
  E193        As Double
  E37         As Double
  E38         As Double
  E39         As Double
  E41         As Double
  E42         As Double
  E43         As Double
  E44         As Double
  E45         As Double
  E46         As Double
  E47         As Double
  E48         As Double
  E49         As Double
  E51         As Double
  E52         As Double
  E53         As Double
  E54         As Double
  E55         As Double
  E56         As Double
  E57         As Double
  E58         As Double
  E59         As Double
  E31         As Double
  E32         As Double
  E40         As Double
  E50         As Double
  E60         As Double
  E61         As Double
  E62         As Double
  E63         As Double
  E64         As Double
  E65         As Double
  E71         As Double
  E72         As Double
  E73         As Double
  E74         As Double
  E75         As Double
  E106        As Double
  E107        As Double
  '
  HMRastt1    As Double
  ApMRastt1   As Double
  BombMRastt1 As Double
  HMRastp1    As Double
  ApMRastp1   As Double
  BombMRastp1 As Double
  HMRastt2    As Double
  ApMRastt2   As Double
  BombMRastt2 As Double
  HMRastp2    As Double
  ApMRastp2   As Double
  BombMRastp2 As Double
  '
End Type
'
Public Sub AZZERAMENTO_VALORI_STRUTTURE()
  '
  'AZZERAMENTO VALORI DELLA STRUTTURA DEI PARAMETRI MOLA
  '
  'PARAMETRI MOLA
  ParametriMola.E206 = 0
  ParametriMola.E207 = 0
  ParametriMola.E5 = 0
  ParametriMola.E191 = 0
  ParametriMola.E192 = 0
  ParametriMola.E193 = 0
  ParametriMola.E37 = 0
  ParametriMola.E38 = 0
  ParametriMola.E39 = 0
  ParametriMola.E41 = 0
  ParametriMola.E42 = 0
  ParametriMola.E43 = 0
  ParametriMola.E44 = 0
  ParametriMola.E45 = 0
  ParametriMola.E46 = 0
  ParametriMola.E47 = 0
  ParametriMola.E48 = 0
  ParametriMola.E49 = 0
  ParametriMola.E51 = 0
  ParametriMola.E52 = 0
  ParametriMola.E53 = 0
  ParametriMola.E54 = 0
  ParametriMola.E55 = 0
  ParametriMola.E56 = 0
  ParametriMola.E57 = 0
  ParametriMola.E58 = 0
  ParametriMola.E59 = 0
  ParametriMola.E31 = 0
  ParametriMola.E32 = 0
  ParametriMola.E40 = 0
  ParametriMola.E50 = 0
  ParametriMola.E60 = 0
  ParametriMola.E61 = 0
  ParametriMola.E62 = 0
  ParametriMola.E63 = 0
  ParametriMola.E64 = 0
  ParametriMola.E65 = 0
  ParametriMola.E71 = 0
  ParametriMola.E72 = 0
  ParametriMola.E73 = 0
  ParametriMola.E74 = 0
  ParametriMola.E75 = 0
  ParametriMola.E106 = 0
  ParametriMola.E107 = 0
  '
  ParametriMola.HMRastt1 = 0
  ParametriMola.ApMRastt1 = 0
  ParametriMola.BombMRastt1 = 0
  ParametriMola.HMRastp1 = 0
  ParametriMola.ApMRastp1 = 0
  ParametriMola.BombMRastp1 = 0
  ParametriMola.HMRastt2 = 0
  ParametriMola.ApMRastt2 = 0
  ParametriMola.BombMRastt2 = 0
  ParametriMola.HMRastp2 = 0
  ParametriMola.ApMRastp2 = 0
  ParametriMola.BombMRastp2 = 0
  '
  'AZZERAMENTO VALORI DELLA STRUTTURA DEI PARAMETRI CREATORE
  '
  'PARAMETRI GENERALI CR
  ParametriCR.Paxial = 0
  ParametriCR.Phel = 0
  ParametriCR.Nf = 0
  ParametriCR.Z = 0
  ParametriCR.DestCr = 0
  ParametriCR.Came = 0
  ParametriCR.DeMola = 0
  ParametriCR.Ep0 = 0
  ParametriCR.Htotal = 0
  ParametriCR.InMola = 0
  ParametriCR.Mr = 0
  ParametriCR.CalcAx = "N"
  ParametriCR.INCAX1 = 0
  '
  'PARAMETRI FIANCO 1
  ParametriCR.Apr = 0
  ParametriCR.ApSt = 0
  ParametriCR.Add = 0
  ParametriCR.Hst = 0
  ParametriCR.EpProtu = 0
  ParametriCR.E46 = 0
  ParametriCR.E47 = 0
  ParametriCR.Rtesta = 0
  ParametriCR.Rfondo = 0
  ParametriCR.E61 = 0
  ParametriCR.E62 = 0
  ParametriCR.Piattino = 0
  ParametriCR.E64 = 0
  ParametriCR.HRastt = 0
  ParametriCR.ApRastt = 0
  ParametriCR.HRastp = 0
  ParametriCR.ApRastp = 0
  '
  'PARAMETRI FIANCO 2
  ParametriCR.Apr2 = 0
  ParametriCR.Apst2 = 0
  ParametriCR.Add2 = 0
  ParametriCR.Hst2 = 0
  ParametriCR.E51 = 0
  ParametriCR.E52 = 0
  ParametriCR.E53 = 0
  ParametriCR.E54 = 0
  ParametriCR.E55 = 0
  ParametriCR.E56 = 0
  ParametriCR.E57 = 0
  ParametriCR.E58 = 0
  ParametriCR.E59 = 0
  ParametriCR.E71 = 0
  ParametriCR.E72 = 0
  ParametriCR.E73 = 0
  ParametriCR.E74 = 0
  ParametriCR.HRastt2 = 0
  ParametriCR.ApRastt2 = 0
  ParametriCR.HRastp2 = 0
  ParametriCR.ApRastp2 = 0
  '
End Sub

Sub CALCOLO_TEMPI_CREATORI(ByVal SiStampa As String)

StopRegieEvents
MsgBox ("Sub: Not S380 ready")
ResumeRegieEvents

''
'Const MAXNumeroCicli = 6
''
'Dim i                           As Integer
'Dim k                           As Integer
'Dim kkk                         As Integer
'Dim j                           As Integer
'Dim stmp                        As String
'Dim riga                        As String
'Dim MLeft                       As Double
'Dim MTop                        As Double
'Dim X1                          As Double
'Dim Y1                          As Double
'Dim X2                          As Double
'Dim Y2                          As Double
'Dim IRg                         As Double
'Dim ICl                         As Double
'Dim TextFont                    As String
'Dim TextSize                    As Double
'Dim Wnome1                      As Double
'Dim Wnome2                      As Double
'Dim Wvalore1                    As Double
'Dim Wvalore2                    As Double
'Dim Fattore                     As Double
'Dim WLine                       As Integer
'Dim DBB                         As Database
'Dim RSS                         As Recordset
'Dim MESSAGGI(5)                 As String
'Dim TPS_ANDATA(MAXNumeroCicli)  As Double
'Dim TPS_RITORNO(MAXNumeroCicli) As Double
'Dim TPS_DIV(MAXNumeroCicli)     As Double
'Dim TPS_PRF(MAXNumeroCicli)     As Double
'Dim TPS_RET(MAXNumeroCicli)     As Double
'Dim TOT_RAD(MAXNumeroCicli)     As Double
'Dim USURA_RAGGIO                As Double
'Dim USURA_DIAMETRO              As Double
'Dim MOLA_PEZZO                  As Double
'Dim MOLA_RULLO                  As Double
'Dim SPOSTAMENTO_ASSIALE_PRF     As Double
'Dim TRATTO_TOTALE               As Double
'Dim TME                         As Double
'Dim TMU                         As Double
''dati macchina
'Dim CENTRO_RULLO_ASSIALE        As Double  'X centro rullo
'Dim VelA                        As Double  'Velocita' max asse A (giri/min->gradi/sec)
'Dim VelB                        As Double  'Velocita' max asse B (giri/min->gradi/sec)
'Dim VelX                        As Double  'Velocita' max asse X (mm/min-> mm/sec)
'Dim VelY                        As Double  'Velocita' max asse Y (mm/min-> mm/sec)
'Dim TPS_FASATURA                As Double  'Tempo fasatura       (sec)
'Dim TFM                         As Double  'Tempo morto ogni mov.(sec)
'Dim DE_MOLA                     As Double  'Diametro mola
''dati profilo
'Dim Mr                          As Double  'MODULO NORMALE
'Dim PRINCIPI                    As Integer 'NUMERO PRINCIPI
'Dim HELPG                       As Double  'ANGOLO ELICA (GRADI)
'Dim PASSO                       As Double  'PASSO
'Dim DI1                         As Double  'DIAMETRO INTERNO
'Dim DE1                         As Double  'DIAMETRO ESTERNO
''dati profilo
'Dim sMR                         As String  'MODULO NORMALE
'Dim sAPRG                       As String  'ANGOLO PRESSIONE NORM. (GRADI)
'Dim sPRINCIPI                   As String  'NUMERO PRINCIPI
'Dim sHELPG                      As String  'ANGOLO ELICA (GRADI)
'Dim sDI1                        As String  'DIAMETRO INTERNO
'Dim sDE1                        As String  'DIAMETRO ESTERNO
'Dim sTRATTO_TOTALE              As String
'Dim sSPOSTAMENTO_ASSIALE_PRF    As String
'Dim sPASSO                      As String
'Dim sDE_MOLA                    As String
''dati lavoro
'Dim INIZIO_LAVORO_ASSIALE       As Double  'X inizio lavoro
'Dim TRATTO_ENTRATA              As Double  'Tratto X entrata
'Dim TRATTO_USCITA               As Double  'Tratto X uscita
'Dim ZONA(2)                     As Double  'SEZIONI
''dati cicli
'Dim TipCic(MAXNumeroCicli)      As String  'Tipo ciclo
'Dim RipCic(MAXNumeroCicli)      As Integer 'Numero ripetizioni
'Dim PasPrf(MAXNumeroCicli)      As Integer 'Numero di passate di diamantatura
'Dim IncPrf(MAXNumeroCicli)      As Double  'Incremento profilatura
'Dim VelPrf(MAXNumeroCicli)      As Double  'Velocit� profilatura  (mm/min)
'Dim PasAnd(MAXNumeroCicli)      As Integer 'N.passate
'Dim IncAnd(MAXNumeroCicli)      As Double  'Incremento andata
'Dim VelAnd(MAXNumeroCicli)      As Double  'Vel.assi and.
'Dim TT                          As Double
'Dim TabX                        As Double
'Dim TABx1                       As Double
'Dim TABx2                       As Double
'Dim TABx3                       As Double
'Dim PicH                        As Double
'Dim PicW                        As Double
'Dim Atmp                        As String * 255
'Dim MODALITA                    As String
'Dim TIPO_LAVORAZIONE            As String
'Dim NOME_TIPO                   As String
'Dim TabellaMANDRINI             As String
'Dim TabellaMACCHINA             As String
'Dim TabellaPROFILO              As String
'Dim TabellaLAVORO               As String
'Dim TabellaMOLA                 As String
'Dim TH                          As Double
'Dim Tm                          As Double
'Dim Ts                          As Double
'Dim ALTEZZA_DENTE               As Double
'Dim TAGLIENTI                   As Integer
'Dim TPS_DIVISIONE_ASSIALE       As Double
'Dim SPOSTAMENTO_RADIALE_PRF     As Double
'Dim LUNGHEZZZA_PRF              As Double
'Dim TPS_PROFILO_PRF             As Double
'Dim TPS_ROTAZIONE               As Double
'Dim TPS_SPOSTAMENTO_ASSIALE_PRF As Double
'Dim TPS_SPOSTAMENTO_RADIALE_PRF As Double
'Dim TPS_SPOSTAMENTO_RULLO_PRF   As Double
'Dim LARGHEZZA_MOLA              As Double
'Dim DIAMETRO_RULLO              As Double
'Dim TEMPO_TOTALE_SECONDI        As Double
''
'Const ASSE_RADIALE = "Y"
'Const ASSE_PEZZO = "A"
''
'On Error GoTo errCALCOLO_TEMPI_CREATORI
'  '
'  Write_Dialog "CALCOLO_TEMPI_CREATORI"
'  '
'  'CONSIDERO FISSO IL DIAMETRO DEL RULLO PROFILATORE: SPOSTAMENTO ASSIALE SOLO PER RCR
'  DIAMETRO_RULLO = 80
'  'LEGGO LA MODALITA DI TRASFERIMENTO DATI
'  MODALITA = GetInfo("Configurazione", "MODALITA", PathFILEINI)
'  MODALITA = UCase$(Trim$(MODALITA))
'  'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
'  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
'  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
'  '
'  OEM0.chkINPUT(0).Visible = False
'  OEM0.chkINPUT(1).Visible = False
'  OEM0.chkINPUT(2).Visible = False
'  OEM0.lblINPUT.Visible = False
'  OEM0.txtINPUT.Visible = False
'  '
'  OEM0.lblOX.Visible = False
'  OEM0.lblOY.Visible = False
'  OEM0.txtOX.Visible = False
'  OEM0.txtOY.Visible = False
'  '
'  OEM0.lblMANDRINI.Visible = False
'  OEM0.McCombo1.Visible = False
'  '
'  'Intestazione frame
'  i = LoadString(g_hLanguageLibHandle, 1013, Atmp, 255)
'  If (i > 0) Then OEM0.frameCalcolo.Caption = Left$(Atmp, i)
'  i = LoadString(g_hLanguageLibHandle, 1014, Atmp, 255)
'  If (i > 0) Then OEM0.lblMANDRINI.Caption = Left$(Atmp, i)
'  '
'  'LARGHEZZA
'  stmp = GetInfo("TEMPI_" & TIPO_LAVORAZIONE, "Larghezza", Path_LAVORAZIONE_INI)
'  If (val(stmp) > 0) Then PicW = val(stmp) Else PicW = 190
'  OEM0.PicCALCOLO.ScaleWidth = PicW
'  'ALTEZZA
'  stmp = GetInfo("TEMPI_" & TIPO_LAVORAZIONE, "Altezza", Path_LAVORAZIONE_INI)
'  If (val(stmp) > 0) Then PicH = val(stmp) Else PicH = 95
'  OEM0.PicCALCOLO.ScaleHeight = PicH
'  '
'  'IMPOSTAZIONE STAMPANTE E STAMPA DEI CICLI
'  TextFont = GetInfo("StampaParametri", "TextFont", PathFILEINI)
'  TextSize = val(GetInfo("StampaParametri", "TextSize", PathFILEINI))
'  '
'  If (SiStampa = "Y") Then
'    '
'    NOME_TIPO = RICERCA_NOME_TIPO(TIPO_LAVORAZIONE)
'    '
'    Write_Dialog OEM0.frameCalcolo.Caption & " --> " & Printer.DeviceName
'    '
'    MLeft = val(GetInfo("StampaParametri", "MLeft", PathFILEINI))
'    MTop = val(GetInfo("StampaParametri", "MTop", PathFILEINI))
'    Fattore = val(GetInfo("StampaParametri", "fattore", PathFILEINI))
'    Wnome1 = val(GetInfo("StampaParametri", "Wnome1", PathFILEINI))
'    Wnome2 = val(GetInfo("StampaParametri", "Wnome2", PathFILEINI))
'    Wvalore1 = val(GetInfo("StampaParametri", "Wvalore1", PathFILEINI))
'    Wvalore2 = val(GetInfo("StampaParametri", "Wvalore2", PathFILEINI))
'    '
'    'CM --> MM
'    MLeft = MLeft * 10
'    MTop = MTop * 10
'    Wnome1 = Wnome1 * 10
'    Wnome2 = Wnome2 * 10
'    Wvalore1 = Wvalore1 * 10
'    Wvalore2 = Wvalore2 * 10
'    WLine = Abs(val(GetInfo("StampaParametri", "Spessore", PathFILEINI)))
'    '
'    'Scala in cm per il foglio
'    Printer.ScaleMode = 6
'    'FontSize (1440/567) [cm] = 1 [inch]
'    For i = 1 To 2
'      Printer.FontName = TextFont
'      Printer.FontSize = TextSize
'      Printer.FontBold = False
'    Next i
'    '
'    'Dimensioni delle rige e colonne per ciascun carattere
'    IRg = Printer.TextHeight("A")
'    ICl = Printer.TextWidth("A")
'    '
'    'Posiziono il cursore sulla stampante
'    X1 = MLeft
'    Y1 = MTop
'    'Logo
'    Printer.PaintPicture OEM0.PicLogo.Picture, X1, Y1, 20, 20
'    GoSub SCRIVI_HEADER
'    '
'    For j = 0 To 5
'      'Posiziono il cursore sulla stampante
'      If (j > 0) Then
'        Printer.CurrentX = MLeft + ICl + (j - 1) * Wvalore1 + Wnome1
'      Else
'        Printer.CurrentX = MLeft + ICl
'      End If
'      Printer.CurrentY = MTop
'      If (OEM0.Label3(j).Visible = True) Then Printer.Print OEM0.Label3(j).Caption
'    Next j
'    '
'    For i = 0 To OEM0.List1(0).ListCount - 1
'      '
'      'Posiziono il cursore sulla stampante
'      X1 = MLeft + ICl
'      Y1 = MTop + (i + 1) * IRg
'      Printer.CurrentX = X1
'      Printer.CurrentY = Y1
'      Printer.Print OEM0.List1(0).List(i)
'      '
'      'Traccio la linea per evidenziare il valore
'      X1 = MLeft + ICl + Printer.TextWidth(OEM0.List1(0).List(i))
'      Y1 = MTop + (i + 1 + Fattore) * IRg
'      X2 = MLeft + ICl + Wnome1
'      Y2 = Y1
'      Printer.Line (X1, Y1)-(X2, Y2)
'      '
'      For j = 1 To MAXNumeroCicli
'        '
'        'LEGGO IL CAMPO RELATIVO AL VALORE DEL PARAMETRO
'        stmp = OEM0.List1(j).List(i)
'        '
'        'Posiziono il cursore sulla stampante
'        X1 = MLeft + ICl + (j - 1) * Wvalore1 + Wnome1
'        Y1 = MTop + (i + 1) * IRg
'        Printer.CurrentX = X1
'        Printer.CurrentY = Y1
'        Printer.Print stmp
'        '
'        If (j < Nlist1) Then
'          'Traccio la linea per evidenziare il valore
'          X1 = MLeft + ICl + Wnome1 + (j - 1) * Wvalore1 + Printer.TextWidth(stmp)
'          Y1 = MTop + (i + 1 + Fattore) * IRg
'          X2 = MLeft + ICl + Wnome1 + j * Wvalore1
'          Y2 = Y1
'          Printer.Line (X1, Y1)-(X2, Y2)
'        End If
'        '
'      Next j
'      '
'    Next i
'    '
'  Else
'    '
'    OEM0.PicCALCOLO.FontName = TextFont
'    OEM0.PicCALCOLO.FontSize = TextSize
'    OEM0.PicCALCOLO.FontBold = False
'    '
'    'Dimensioni delle rige e colonne per ciascun carattere
'    IRg = OEM0.PicCALCOLO.TextHeight("A")
'    ICl = OEM0.PicCALCOLO.TextWidth("A")
'    '
'    OEM0.PicCALCOLO.Cls
'    '
'  End If
'  '
'  'MESSAGGI
'  i = LoadString(g_hLanguageLibHandle, 1010, Atmp, 255)
'  If (i > 0) Then MESSAGGI(0) = Left$(Atmp, i)
'  i = LoadString(g_hLanguageLibHandle, 1011, Atmp, 255)
'  If (i > 0) Then MESSAGGI(1) = Left$(Atmp, i)
'  i = LoadString(g_hLanguageLibHandle, 1012, Atmp, 255)
'  If (i > 0) Then MESSAGGI(2) = Left$(Atmp, i)
'  'i = LoadString(g_hLanguageLibHandle, 1015, Atmp, 255)
'  'If (i > 0) Then sTRATTO_TOTALE = Left$(Atmp, i)
'  i = LoadString(g_hLanguageLibHandle, 1016, Atmp, 255)
'  If (i > 0) Then sSPOSTAMENTO_ASSIALE_PRF = Left$(Atmp, i)
'  i = LoadString(g_hLanguageLibHandle, 1017, Atmp, 255)
'  If (i > 0) Then MESSAGGI(3) = Left$(Atmp, i)
'  i = LoadString(g_hLanguageLibHandle, 1018, Atmp, 255)
'  If (i > 0) Then MESSAGGI(4) = Left$(Atmp, i)
'  i = LoadString(g_hLanguageLibHandle, 1241, Atmp, 255)
'  If (i > 0) Then MESSAGGI(5) = Left$(Atmp, i)
'  '
'  'Velocit� max asse A (giri/min -> gradi/sec)
'  stmp = GetInfo("TEMPI_" & TIPO_LAVORAZIONE, "VelA", Path_LAVORAZIONE_INI)
'  VelA = val(stmp): VelA = VelA * 6
'  '
'  'Velocit� max asse B (giri/min -> gradi/sec)
'  stmp = GetInfo("TEMPI_" & TIPO_LAVORAZIONE, "VelB", Path_LAVORAZIONE_INI)
'  VelB = val(stmp): VelB = VelB * 6
'  '
'  'Velocit� max asse X (mm/min  -> mm/sec)
'  stmp = GetInfo("TEMPI_" & TIPO_LAVORAZIONE, "VelX", Path_LAVORAZIONE_INI)
'  VelX = val(stmp): VelX = VelX / 60
'  '
'  'Velocit� max asse Y (mm/min-> mm/sec)
'  stmp = GetInfo("TEMPI_" & TIPO_LAVORAZIONE, "VelY", Path_LAVORAZIONE_INI)
'  VelY = val(stmp): VelY = VelY / 60
'  '
'  'Tempo fasatura        (sec)
'  stmp = GetInfo("TEMPI_" & TIPO_LAVORAZIONE, "TempoFas", Path_LAVORAZIONE_INI)
'  'TPS_FASATURA = Val(stmp)
'  TPS_FASATURA = 0
'  '
'  'Tempo morto ogni mov. (sec)
'  stmp = GetInfo("TEMPI_" & TIPO_LAVORAZIONE, "TFM", Path_LAVORAZIONE_INI)
'  TFM = val(stmp)
'  '
'  Set DBB = OpenDatabase(PathDB, True, False)
'  '
'  TabellaMACCHINA = GetInfo("TEMPI_" & TIPO_LAVORAZIONE, "TabellaMACCHINA", Path_LAVORAZIONE_INI)
'  If (Len(TabellaMACCHINA) > 0) Then
'    '
'    Set RSS = DBB.OpenRecordset(TabellaMACCHINA)
'    '
'    RSS.Index = "INDICE"
'    '
'    RSS.Seek "=", 1
'    CENTRO_RULLO_ASSIALE = val(RSS.Fields("ACTUAL_1").Value) 'QUOTA ASSIALE CENTRO RULLO
'    '
'    RSS.Seek "=", 3
'    MOLA_RULLO = val(RSS.Fields("ACTUAL_1").Value)    'DISTANZA MOLA RULLO
'    '
'    RSS.Seek "=", 4
'    MOLA_PEZZO = val(RSS.Fields("ACTUAL_1").Value)    'DISTANZA MOLA PEZZO
'    '
'    RSS.Close
'    '
'  End If
'  '
'  TabellaPROFILO = GetInfo("TEMPI_" & TIPO_LAVORAZIONE, "TabellaPROFILO", Path_LAVORAZIONE_INI)
'  If (Len(TabellaPROFILO) > 0) Then
'    '
'    Set RSS = DBB.OpenRecordset(TabellaPROFILO)
'    '
'    RSS.Index = "INDICE"
'    '
'    RSS.Seek "=", 14
'    DE_MOLA = 2 * val(RSS.Fields("ACTUAL_1").Value) 'DIAMETRO MOLA
'    sDE_MOLA = RSS.Fields("NOME_" & Lingua).Value   'RAGGIO MOLA
'    '
'    RSS.Seek "=", 15
'    HELPG = val(RSS.Fields("ACTUAL_1").Value)       'ELICA DI RETTIFICA
'    sHELPG = RSS.Fields("NOME_" & Lingua).Value
'    '
'    RSS.Seek "=", 1
'    PRINCIPI = val(RSS.Fields("ACTUAL_1").Value)    'NUMERO PRINCIPI
'    sPRINCIPI = RSS.Fields("NOME_" & Lingua).Value
'    '
'    RSS.Seek "=", 4
'    DE1 = val(RSS.Fields("ACTUAL_1").Value)         'DIAMETRO ESTERNO
'    sDE1 = RSS.Fields("NOME_" & Lingua).Value
'    '
'    RSS.Seek "=", 20
'    ALTEZZA_DENTE = val(RSS.Fields("ACTUAL_1").Value)       'ALTEZZA TOTALE (<>0 SOLO CON SEMITOPPING)
'    If (ALTEZZA_DENTE <= 0) Then
'      RSS.Seek "=", 26
'      If (val(RSS.Fields("ACTUAL_1").Value) > val(RSS.Fields("ACTUAL_2").Value)) Then
'        ALTEZZA_DENTE = val(RSS.Fields("ACTUAL_1").Value)   'ALTEZZA TOTALE F1
'      Else
'        ALTEZZA_DENTE = val(RSS.Fields("ACTUAL_2").Value)   'ALTEZZA TOTALE F2
'      End If
'    End If
'    '
'    'DIAMETRO INTERNO
'    DI1 = DE1 - 2 * ALTEZZA_DENTE
'    sDI1 = "DI"
'    '
'    RSS.Seek "=", 2
'    PASSO = val(RSS.Fields("ACTUAL_1").Value)        'PASSO
'    sPASSO = RSS.Fields("NOME_" & Lingua).Value      'PASSO
'    '
'    RSS.Seek "=", 5
'    TAGLIENTI = val(RSS.Fields("ACTUAL_1").Value)    'NUMERO SCANALATURE
'    '
'    RSS.Seek "=", 17
'    Mr = val(RSS.Fields("ACTUAL_1").Value)           'MODULO NORMALE
'    sMR = RSS.Fields("NOME_" & Lingua).Value         'MODULO NORMALE
'    '
'    RSS.Close
'    '
'  End If
'  '
'  TabellaLAVORO = GetInfo("TEMPI_" & TIPO_LAVORAZIONE, "TabellaLAVORO", Path_LAVORAZIONE_INI)
'  If (Len(TabellaLAVORO) > 0) Then
'    '
'    Set RSS = DBB.OpenRecordset(TabellaLAVORO)
'    '
'    RSS.Index = "INDICE"
'    '
'    RSS.Seek "=", 1
'    INIZIO_LAVORO_ASSIALE = val(RSS.Fields("ACTUAL_1").Value) 'QUOTA ASSIALE DI INIZIO LAVORO
'    '
'    RSS.Seek "=", 19
'    ZONA(1) = val(RSS.Fields("ACTUAL_1").Value) 'TRATTO 1
'    ZONA(2) = val(RSS.Fields("ACTUAL_2").Value) 'TRATTO 2
'    sTRATTO_TOTALE = RSS.Fields("NOME_" & Lingua).Value
'    '
'    RSS.Close
'    '
'  End If
'  '
'  TabellaMOLA = GetInfo("TEMPI_" & TIPO_LAVORAZIONE, "TabellaMOLA", Path_LAVORAZIONE_INI) 'FlagFORMA 027
'    '
'    Set RSS = DBB.OpenRecordset(TabellaMOLA)
'    '
'    RSS.Index = "INDICE"
'    '
'    RSS.Seek "=", 1
'    LARGHEZZA_MOLA = val(RSS.Fields("ACTUAL_1").Value) 'LARGHEZZA MOLA
'    '
'    RSS.Close
'  '
'  DBB.Close
'  '
'  For i = 1 To MAXNumeroCicli
'    'TIPO CICLO
'    TipCic(i) = Trim$(UCase$(OEM0.List1(i).List(0)))
'    'RIPETIZIONI CICLO
'    RipCic(i) = val(OEM0.List1(i).List(1))
'    'PROFILATURA
'    PasPrf(i) = val(OEM0.List1(i).List(2))  'Numero di passate di diamantatura
'    IncPrf(i) = val(OEM0.List1(i).List(3))  'Incremento profilatura
'    VelPrf(i) = val(OEM0.List1(i).List(4))  'Velocit� profilatura (mm/min)
'    VelPrf(i) = VelPrf(i) / 60              'Velocit� profilatura (mm/s)
'    'RETTIFICA
'    PasAnd(i) = val(OEM0.List1(i).List(9))  'N.passate
'    IncAnd(i) = val(OEM0.List1(i).List(10)) 'Incremento andata (mm/min)
'    VelAnd(i) = val(OEM0.List1(i).List(11)) 'Velocit� passata in giri pezzo
'    'VelAnd(i) = VelAnd(i) * PASSO / 60 '* 0.95    'Velocit� passata (mm/s)
'  Next i
'  '
'  'Lunghezza totale della fascia di rettifica
'  TRATTO_TOTALE = Abs(TRATTO_ENTRATA + TRATTO_USCITA + ZONA(1) + ZONA(2))
'  '
'  'INIDICAZIONE DEL PERCORSO DI PROFILATURA
'  LUNGHEZZZA_PRF = ALTEZZA_DENTE * 2 + LARGHEZZA_MOLA
'  '
'  'TEMPO FISSO PER DIVISIONE RADIALE
'  TPS_DIVISIONE_ASSIALE = (ALTEZZA_DENTE + 2) / VelY * 2
'  '
'  'TPS_SPOSTAMENTO_RADIALE_PRF: tempo per muoversi da posizione Y lavoro a quella di diamantatura
'  SPOSTAMENTO_RADIALE_PRF = MOLA_PEZZO - (DE_MOLA + DI1) / 2 + MOLA_RULLO
'  TPS_SPOSTAMENTO_RADIALE_PRF = SPOSTAMENTO_RADIALE_PRF / VelY + 2 * TFM
'  '
'  'TPS_ROTAZIONE : tempo per rotazione asse B
'  TPS_ROTAZIONE = HELPG / VelB + 2 * TFM
'  '
'  'TPS_SPOSTAMENTO_ASSIALE_PRF : tempo per andare e tornare posizione X diamantatura
'  SPOSTAMENTO_ASSIALE_PRF = Abs(INIZIO_LAVORO_ASSIALE - CENTRO_RULLO_ASSIALE)
'  TPS_SPOSTAMENTO_ASSIALE_PRF = SPOSTAMENTO_ASSIALE_PRF / VelX + 2 * TFM
'  '
'  'TPS_SPOSTAMENTO_RULLO_PRF: tempo per muoversi da F1 a F2 a F1
'  TPS_SPOSTAMENTO_RULLO_PRF = 2 * DIAMETRO_RULLO / VelX
'  '
'  If (TRATTO_ENTRATA = 0) Then TME = 0.2 Else TME = 0    'Sosta entrata
'  If (TRATTO_USCITA = 0) Then TMU = 0.2 Else TMU = 0     'Sosta uscita
'  '
'  'CALCOLO DEL TEMPO TOTALE DI RETTIFICA
'  For i = 1 To MAXNumeroCicli
'    '
'    'CALCOLO TEMPO DEL CICLO PARZIALE
'    If (PasAnd(i) > 0) And (TipCic(i) <> "N") Then
'      '
'      'TPS_ANDATA: Tempo totale andata per 1 dente
'      TPS_ANDATA(i) = TRATTO_TOTALE / (VelAnd(i) * PASSO / 60) + 2 * TFM + TME
'      '
'      'TPS_RITORNO: Tempo totale 1 dente ritorno rapido
'      TPS_RITORNO(i) = TRATTO_TOTALE / VelX + 2 * TFM
'      '
'      'TPS_DIV : Tempo divisione per 1 dente
'      TPS_DIV(i) = TPS_DIVISIONE_ASSIALE
'      '
'      'DEVO MODIFICARE A CAUSA DEL RULLO DI DIAMETRO 80
'      'TPS_PROFILO_PRF : tempo per profilare
'      If (VelPrf(i) > 0) Then TPS_PROFILO_PRF = LUNGHEZZZA_PRF / VelPrf(i) + 8 Else TPS_PROFILO_PRF = 0
'      If (PasPrf(i) > 0) Then
'        TPS_PRF(i) = Int((TPS_SPOSTAMENTO_ASSIALE_PRF + TPS_ROTAZIONE + TPS_SPOSTAMENTO_RADIALE_PRF) * 2 + (TPS_PROFILO_PRF + TPS_SPOSTAMENTO_RULLO_PRF) * PasPrf(i))
'      Else
'        TPS_PRF(i) = 0
'      End If
'      '
'    End If
'    '
'  Next i
'  '
'  USURA_RAGGIO = 0
'  TEMPO_TOTALE_SECONDI = 0
'  For i = 1 To MAXNumeroCicli
'    '
'    TPS_RET(i) = 0
'    If (TipCic(i) <> "N") Then
'      '
'      'Calcolo tempo rettifica
'      TPS_RET(i) = (TPS_DIV(i) + TPS_ANDATA(i) + TPS_RITORNO(i)) * PRINCIPI * PasAnd(i)
'      '
'      'Calcolo Incrementi totali Y/A
'      TOT_RAD(i) = RipCic(i) * PasAnd(i) * IncAnd(i)
'      '
'      'Calcolo consumo mola
'      USURA_RAGGIO = USURA_RAGGIO - (RipCic(i) * PasPrf(i) * IncPrf(i))
'      '
'    End If
'    '
'    TPS_RET(i) = TPS_PRF(i) * RipCic(i) + TPS_RET(i) * RipCic(i)
'    TEMPO_TOTALE_SECONDI = TEMPO_TOTALE_SECONDI + TPS_RET(i)
'    '
'  Next i
'  USURA_DIAMETRO = USURA_RAGGIO * 2
'  '
'  Write_Dialog ""
'  '
'  'Output risultati
'  If (SiStampa = "Y") Then
'    GoSub STAMPA_DATI
'    Write_Dialog OEM0.frameCalcolo.Caption & " --> " & Printer.DeviceName & " OK!!"
'    Printer.EndDoc
'  Else
'    GoSub VISUALIZZA_DATI
'  End If
'  '
'Exit Sub
'
'errCALCOLO_TEMPI_CREATORI:
'  Write_Dialog "SUB CALCOLO_TEMPI_CREATORI: ERROR -> " & Err
'  Resume Next
'  'Exit Sub
'
'VISUALIZZA_DATI:
'  '
'  OEM0.PicCALCOLO.Cls
'  '
'  OEM0.PicCALCOLO.FontName = TextFont
'  OEM0.PicCALCOLO.FontSize = TextSize - 1 'FLAGmod
'  OEM0.PicCALCOLO.FontBold = False 'True
'  '
'  IRg = OEM0.PicCALCOLO.TextHeight("X")
'  TabX = 0
'  '*********
'  i = 0
'  stmp = " " & sMR
'  If OEM0.PicCALCOLO.TextWidth(stmp) > TabX Then TabX = OEM0.PicCALCOLO.TextWidth(stmp)
'  OEM0.PicCALCOLO.CurrentX = 0
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  OEM0.PicCALCOLO.Print stmp
'  '*********
'  i = i + 1
'  stmp = " " & sDE_MOLA
'  If OEM0.PicCALCOLO.TextWidth(stmp) > TabX Then TabX = OEM0.PicCALCOLO.TextWidth(stmp)
'  OEM0.PicCALCOLO.CurrentX = 0
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  OEM0.PicCALCOLO.Print stmp
'  '*********
'  i = i + 1
'  stmp = " " & sPRINCIPI
'  If OEM0.PicCALCOLO.TextWidth(stmp) > TabX Then TabX = OEM0.PicCALCOLO.TextWidth(stmp)
'  OEM0.PicCALCOLO.CurrentX = 0
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  OEM0.PicCALCOLO.Print stmp
'  '*********
'  i = i + 1
'  stmp = " " & sHELPG
'  If OEM0.PicCALCOLO.TextWidth(stmp) > TabX Then TabX = OEM0.PicCALCOLO.TextWidth(stmp)
'  OEM0.PicCALCOLO.CurrentX = 0
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  OEM0.PicCALCOLO.Print stmp
'  '*********
'  i = i + 1
'  stmp = " " & sDI1
'  If OEM0.PicCALCOLO.TextWidth(stmp) > TabX Then TabX = OEM0.PicCALCOLO.TextWidth(stmp)
'  OEM0.PicCALCOLO.CurrentX = 0
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  OEM0.PicCALCOLO.Print stmp
'  '*********
'  i = i + 1
'  stmp = " " & sDE1
'  If OEM0.PicCALCOLO.TextWidth(stmp) > TabX Then TabX = OEM0.PicCALCOLO.TextWidth(stmp)
'  IRg = OEM0.PicCALCOLO.TextHeight(stmp)
'  OEM0.PicCALCOLO.CurrentX = 0
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  OEM0.PicCALCOLO.Print stmp
'  '*********
'  i = i + 1
'  stmp = " " & sSPOSTAMENTO_ASSIALE_PRF
'  If OEM0.PicCALCOLO.TextWidth(stmp) > TabX Then TabX = OEM0.PicCALCOLO.TextWidth(stmp)
'  OEM0.PicCALCOLO.CurrentX = 0
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  OEM0.PicCALCOLO.Print stmp
'  '*********
'  i = i + 1
'  stmp = " " & sTRATTO_TOTALE
'  If OEM0.PicCALCOLO.TextWidth(stmp) > TabX Then TabX = OEM0.PicCALCOLO.TextWidth(stmp)
'  OEM0.PicCALCOLO.CurrentX = 0
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  OEM0.PicCALCOLO.Print stmp
'  '*********
'  i = i + 1
'  stmp = " " & sPASSO
'  If OEM0.PicCALCOLO.TextWidth(stmp) > TabX Then TabX = OEM0.PicCALCOLO.TextWidth(stmp)
'  OEM0.PicCALCOLO.CurrentX = 0
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  OEM0.PicCALCOLO.Print stmp
'  '*********
'  i = i + 1
'  stmp = " " & "CLASS"
'  If OEM0.PicCALCOLO.TextWidth(stmp) > TabX Then TabX = OEM0.PicCALCOLO.TextWidth(stmp)
'  OEM0.PicCALCOLO.CurrentX = 0
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  OEM0.PicCALCOLO.Print stmp
'  '*********
'  i = 0
'  stmp = ": " & frmt$(Mr, 4)
'  OEM0.PicCALCOLO.CurrentX = TabX
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  OEM0.PicCALCOLO.Print stmp
'  '*********
'  i = i + 1
'  stmp = ": " & frmt$(DE_MOLA / 2, 4)
'  OEM0.PicCALCOLO.CurrentX = TabX
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  OEM0.PicCALCOLO.Print stmp
'  '*********
'  i = i + 1
'  stmp = ": " & Format$(PRINCIPI, "###0")
'  OEM0.PicCALCOLO.CurrentX = TabX
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  OEM0.PicCALCOLO.Print stmp
'  '*********
'  i = i + 1
'  stmp = ": " & frmt$(HELPG, 4)
'  OEM0.PicCALCOLO.CurrentX = TabX
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  OEM0.PicCALCOLO.Print stmp
'  '*********
'  i = i + 1
'  stmp = ": " & frmt$(DI1, 4)
'  OEM0.PicCALCOLO.CurrentX = TabX
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  OEM0.PicCALCOLO.Print stmp
'  '*********
'  i = i + 1
'  stmp = ": " & frmt$(DE1, 4)
'  OEM0.PicCALCOLO.CurrentX = TabX
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  OEM0.PicCALCOLO.Print stmp
'  '*********
'  i = i + 1
'  stmp = ": " & frmt$(SPOSTAMENTO_ASSIALE_PRF, 4)
'  OEM0.PicCALCOLO.CurrentX = TabX
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  OEM0.PicCALCOLO.Print stmp
'  '*********
'  i = i + 1
'  stmp = ": " & frmt$(TRATTO_TOTALE, 4)
'  OEM0.PicCALCOLO.CurrentX = TabX
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  OEM0.PicCALCOLO.Print stmp
'  '*********
'  i = i + 1
'  stmp = ": " & frmt$(PASSO, 4)
'  OEM0.PicCALCOLO.CurrentX = TabX
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  OEM0.PicCALCOLO.Print stmp
'  '*********
'  i = i + 1
'      If (TEMPO_TOTALE_SECONDI < 3600) Then
'          stmp = ": " & "(A) < 1h"
'  ElseIf (TEMPO_TOTALE_SECONDI >= 3600) And (TEMPO_TOTALE_SECONDI < 7200) Then
'          stmp = ": " & "1h < (B) < 2h"
'  ElseIf (TEMPO_TOTALE_SECONDI >= 7200) And (TEMPO_TOTALE_SECONDI < 10800) Then
'          stmp = ": " & "2h < (C) < 3h"
'  ElseIf (TEMPO_TOTALE_SECONDI >= 10800) And (TEMPO_TOTALE_SECONDI < 14400) Then
'          stmp = ": " & "3h < (D) < 4h"
'  Else
'          stmp = ": " & "(X) > 4h"
'  End If
'  OEM0.PicCALCOLO.CurrentX = TabX
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  OEM0.PicCALCOLO.Print stmp
'  '
'  'TEMPI DI DIVISIONE
'  j = i
'  '
'  TabX = 0
'  i = j + 2
'  For k = 1 To MAXNumeroCicli
'    If (TipCic(k) <> "N") Then
'      i = i + 1
'      '
'      stmp = " " & Trim$(OEM0.Label3(k).Caption)
'      If (OEM0.PicCALCOLO.TextWidth(stmp) > TabX) Then TabX = OEM0.PicCALCOLO.TextWidth(stmp)
'      OEM0.PicCALCOLO.CurrentX = 0
'      OEM0.PicCALCOLO.CurrentY = i * IRg
'      OEM0.PicCALCOLO.Print stmp
'      '
'    End If
'  Next k
'  '
'  i = j + 2
'  OEM0.PicCALCOLO.CurrentX = TabX
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  stmp = " FORWARD"
'  OEM0.PicCALCOLO.Print stmp
'  OEM0.PicCALCOLO.CurrentX = PicW / 4
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  stmp = " INDEXING"
'  OEM0.PicCALCOLO.Print stmp
'  OEM0.PicCALCOLO.Line (0, (i + 1) * IRg)-(PicW / 2, (i + 1) * IRg)
'  '
'  'MINUTI ANDATA
'  i = j + 2
'  For k = 1 To MAXNumeroCicli
'    If (TipCic(k) <> "N") Then
'      i = i + 1
'      '
'      'MINUTI ANDATA
'      stmp = ": " & Format$(Int(TPS_ANDATA(k) / 60), "#0") & "'"
'      If OEM0.PicCALCOLO.TextWidth(stmp) > TABx1 Then TABx1 = OEM0.PicCALCOLO.TextWidth(stmp)
'      OEM0.PicCALCOLO.CurrentX = TabX
'      OEM0.PicCALCOLO.CurrentY = i * IRg
'      OEM0.PicCALCOLO.Print stmp
'    End If
'  Next k
'  '
'  'SECONDI ANDATA + DIVISIONE SINGOLA + RITORNO
'  i = j + 2
'  For k = 1 To MAXNumeroCicli
'    If (TipCic(k) <> "N") Then
'      i = i + 1
'      '
'      'SECONDI ANDATA
'      stmp = " " & Format(Int(TPS_ANDATA(k) - Int(TPS_ANDATA(k) / 60) * 60), "###0") & Chr$(34)
'      If OEM0.PicCALCOLO.TextWidth(stmp) > TABx2 Then TABx2 = OEM0.PicCALCOLO.TextWidth(stmp)
'      OEM0.PicCALCOLO.CurrentX = TabX + TABx1
'      OEM0.PicCALCOLO.CurrentY = i * IRg
'      OEM0.PicCALCOLO.Print stmp
'      '
'      'DIVISIONE SINGOLA + RITORNO
'      stmp = " " & Format(TPS_DIV(k) + TPS_RITORNO(k), "###0") & Chr$(34)
'      OEM0.PicCALCOLO.CurrentX = PicW / 4
'      OEM0.PicCALCOLO.CurrentY = i * IRg
'      OEM0.PicCALCOLO.Print stmp
'      '
'    End If
'  Next k
'  '
'  'CALCOLO TEMPO TOTALE DI RETTIFICA
'  i = 0
'  OEM0.PicCALCOLO.CurrentX = PicW / 2
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  stmp = " " & MESSAGGI(0)
'  OEM0.PicCALCOLO.Print stmp
'  '
'  i = 1
'  OEM0.PicCALCOLO.Line (PicW / 2, i * IRg)-(PicW, i * IRg)
'  OEM0.PicCALCOLO.CurrentX = PicW / 2
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  '
'  TabX = 0
'  For i = 1 To MAXNumeroCicli
'    If (TipCic(i) <> "N") Then
'      OEM0.PicCALCOLO.CurrentX = PicW / 2
'      OEM0.PicCALCOLO.CurrentY = i * IRg
'      stmp = " " & Trim$(OEM0.Label3(i).Caption)
'      If OEM0.PicCALCOLO.TextWidth(stmp) > TabX Then TabX = OEM0.PicCALCOLO.TextWidth(stmp)
'      OEM0.PicCALCOLO.Print stmp
'    End If
'  Next i
'  '
'  OEM0.PicCALCOLO.CurrentX = PicW / 2
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  If (Lingua = "CH") Then
'    stmp = " " & MESSAGGI(3)
'  Else
'    stmp = " " & Left$(MESSAGGI(3), 7)
'  End If
'  If OEM0.PicCALCOLO.TextWidth(stmp) > TabX Then TabX = OEM0.PicCALCOLO.TextWidth(stmp)
'  'OEM0.PicCALCOLO.Print stmp
'  '
'  i = i + 1
'  OEM0.PicCALCOLO.CurrentX = PicW / 2
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  stmp = " " & MESSAGGI(2)
'  If OEM0.PicCALCOLO.TextWidth(stmp) > TabX Then TabX = OEM0.PicCALCOLO.TextWidth(stmp)
'  OEM0.PicCALCOLO.Print stmp
'  '
'  'ORE
'  TABx1 = 0
'  TT = 0
'  For i = 1 To MAXNumeroCicli
'    If (TipCic(i) <> "N") Then
'      OEM0.PicCALCOLO.CurrentX = PicW / 2 + TabX
'      OEM0.PicCALCOLO.CurrentY = i * IRg
'      TH = Int(TPS_RET(i) / 60 / 60)
'      stmp = " " & Format$(TH, "#0") & "h"
'      If OEM0.PicCALCOLO.TextWidth(stmp) > TABx1 Then TABx1 = OEM0.PicCALCOLO.TextWidth(stmp)
'      '
'      If (TH > 0) Then OEM0.PicCALCOLO.Print stmp
'      '
'      TT = TT + TPS_RET(i)
'    End If
'  Next i
'  TT = TT + TPS_FASATURA
'  '
'  OEM0.PicCALCOLO.CurrentX = PicW / 2 + TabX + TABx1
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  stmp = " " & Format$(Int(TPS_FASATURA / 60), "#0") & "'"
'  If OEM0.PicCALCOLO.TextWidth(stmp) > TABx1 Then TABx1 = OEM0.PicCALCOLO.TextWidth(stmp)
'  'OEM0.PicCALCOLO.Print stmp
'  '
'  'MINUTI
'  TT = 0
'  TABx2 = 0
'  For i = 1 To MAXNumeroCicli
'    If (TipCic(i) <> "N") Then
'      OEM0.PicCALCOLO.CurrentX = PicW / 2 + TabX + TABx1
'      OEM0.PicCALCOLO.CurrentY = i * IRg
'      TH = Int(TPS_RET(i) / 60 / 60)
'      Tm = Int(TPS_RET(i) / 60 - 60 * TH)
'      stmp = " " & Format(Tm, "###0") & "'"
'      If OEM0.PicCALCOLO.TextWidth(stmp) > TABx2 Then TABx2 = OEM0.PicCALCOLO.TextWidth(stmp)
'      '
'      OEM0.PicCALCOLO.Print stmp
'      '
'      TT = TT + TPS_RET(i)
'    End If
'  Next i
'  TT = TT + TPS_FASATURA
'  '
'  'SECONDI
'  TT = 0
'  TABx3 = 0
'  For i = 1 To MAXNumeroCicli
'    If (TipCic(i) <> "N") Then
'      OEM0.PicCALCOLO.CurrentX = PicW / 2 + TabX + TABx1 + TABx2
'      OEM0.PicCALCOLO.CurrentY = i * IRg
'      TH = Int(TPS_RET(i) / 60 / 60)
'      Tm = Int(TPS_RET(i) / 60 - 60 * TH)
'      Ts = TPS_RET(i) - 60 * 60 * TH - 60 * Tm
'      stmp = " " & Format(Ts, "###0") & Chr$(34)
'      If OEM0.PicCALCOLO.TextWidth(stmp) > TABx3 Then TABx3 = OEM0.PicCALCOLO.TextWidth(stmp)
'      '
'      OEM0.PicCALCOLO.Print stmp
'      '
'      TT = TT + TPS_RET(i)
'    End If
'  Next i
'  TT = TT + TPS_FASATURA
'  '
'  'FASATURA
'  OEM0.PicCALCOLO.CurrentX = PicW / 2 + TabX + TABx1 + TABx2
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  stmp = " " & Format(Int(TPS_FASATURA - Int(TPS_FASATURA / 60) * 60), "###0") & Chr$(34)
'  If OEM0.PicCALCOLO.TextWidth(stmp) > TABx3 Then TABx3 = OEM0.PicCALCOLO.TextWidth(stmp)
'  'OEM0.PicCALCOLO.Print stmp
'  '
'  'TOTALE TEMPO
'  i = i + 1
'  OEM0.PicCALCOLO.Line (PicW / 2, i * IRg)-(PicW, i * IRg)
'  OEM0.PicCALCOLO.CurrentX = PicW / 2 + TabX
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  TH = Int(TT / 60 / 60)
'  Tm = Int(TT / 60 - 60 * TH)
'  Ts = TT - 60 * 60 * TH - 60 * Tm
'  stmp = ""
'  If (TH > 0) Then
'    stmp = stmp & " " & Format$(TH, "###0") & "h"
'  Else
'    stmp = stmp & "    "
'  End If
'  stmp = stmp & " " & Format$(Tm, "###0") & "'"
'  stmp = stmp & " " & Format$(Ts, "###0") & Chr$(34)
'  OEM0.PicCALCOLO.Print stmp
'  '
'  'CALCOLO ASPORTAZIONI
'  Dim DELTA_X As Double
'  '
'  i = 0: DELTA_X = 2 * ICl
'  OEM0.PicCALCOLO.CurrentX = 3 * PicW / 4 + DELTA_X
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  stmp = MESSAGGI(1)
'  OEM0.PicCALCOLO.Print stmp
'  '
'  TabX = 0
'  For i = 1 To MAXNumeroCicli
'    If (TipCic(i) <> "N") Then
'      OEM0.PicCALCOLO.CurrentX = 3 * PicW / 4 + DELTA_X
'      OEM0.PicCALCOLO.CurrentY = i * IRg
'      stmp = " " & ASSE_RADIALE & "=" & frmt(TOT_RAD(i), 4)
'      If OEM0.PicCALCOLO.TextWidth(stmp) > TabX Then TabX = OEM0.PicCALCOLO.TextWidth(stmp)
'      OEM0.PicCALCOLO.Print stmp
'    End If
'  Next i
'  '
'  TABx1 = 0
'  For i = 1 To MAXNumeroCicli
'    If (TipCic(i) <> "N") Then
'      OEM0.PicCALCOLO.CurrentX = 3 * PicW / 4 + DELTA_X + TabX
'      OEM0.PicCALCOLO.CurrentY = i * IRg
'      stmp = " CM=" & Format$(INTERO(VelAnd(i) * TAGLIENTI, "MIN"), "##0")
'      If OEM0.PicCALCOLO.TextWidth(stmp) > TABx1 Then TABx1 = OEM0.PicCALCOLO.TextWidth(stmp)
'      OEM0.PicCALCOLO.Print stmp
'    End If
'  Next i
'  '
'  i = i + 1
'  '
'  TOT_RAD(0) = 0
'  For kkk = 1 To MAXNumeroCicli
'    TOT_RAD(0) = TOT_RAD(0) + TOT_RAD(kkk)
'  Next kkk
'  '
'  stmp = " " & ASSE_RADIALE & "=" & frmt(TOT_RAD(0), 4)
'  OEM0.PicCALCOLO.CurrentX = 3 * PicW / 4 + DELTA_X
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  OEM0.PicCALCOLO.Print stmp
'  '
'  OEM0.PicCALCOLO.Line (PicW / 2, 0 * IRg)-(PicW / 2, (i + 2) * IRg)
'  OEM0.PicCALCOLO.Line (3 * PicW / 4 + DELTA_X / 2, 0 * IRg)-(3 * PicW / 4 + DELTA_X / 2, (i + 2) * IRg)
'  '
'  i = i + 2
'  stmp = " " & MESSAGGI(4) & ": " & frmt(USURA_DIAMETRO, 4) & " "
'  OEM0.PicCALCOLO.CurrentX = 3 * PicW / 4 - OEM0.PicCALCOLO.TextWidth(stmp) / 2
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  OEM0.PicCALCOLO.Print stmp
'  TabX = OEM0.PicCALCOLO.TextWidth(stmp)
'  OEM0.PicCALCOLO.Line (PicW / 2, i * IRg)-(PicW, (i + 1) * IRg), , B
'  '
'Return
'
'SCRIVI_HEADER:
'  'Posiziono il cursore sulla stampante
'  X1 = MLeft
'  Y1 = MTop
'  'INTESTAZIONE DELLA PAGINA: riga 1
'  stmp = NOME_TIPO
'  Printer.CurrentX = X1 + ICl + 20
'  Printer.CurrentY = Y1 + 0
'  Printer.Print stmp
'  'INTESTAZIONE DELLA PAGINA: riga 1
'  If (MODALITA = "OFF-LINE") Then stmp = "OFF-LINE" Else stmp = "CNC " & LETTERA_LAVORAZIONE
'  Printer.CurrentX = PicW - Printer.TextWidth(stmp)
'  Printer.CurrentY = Y1 + 0
'  Printer.Print stmp
'  'INTESTAZIONE DELLA PAGINA: riga 2
'  riga = GetInfo("Configurazione", "AZIENDA", PathFILEINI)
'  stmp = riga & " "
'  If (MODALITA = "OFF-LINE") Then
'    riga = GetInfo(TIPO_LAVORAZIONE, "NomePezzoInserimento", Path_LAVORAZIONE_INI)
'  Else
'    riga = LEGGI_NomePezzo(TIPO_LAVORAZIONE)
'  End If
'  stmp = stmp & riga
'  stmp = stmp & " " & Format$(Now, "dd/mm/yyyy")
'  stmp = stmp & " " & Format$(Now, "hh:mm:ss")
'  Printer.CurrentX = X1 + ICl + 20
'  Printer.CurrentY = Y1 + 10
'  Printer.Print stmp
'  'Ritorno linea
'  MTop = Printer.CurrentY + 2 * IRg
'  'Separatore
'  Printer.DrawWidth = WLine
'  Printer.Line (MLeft, MTop)-(PicW, MTop)
'  Printer.DrawWidth = 1
'  'Ritorno linea
'  MTop = MTop + IRg
'Return
'
'STAMPA_DATI:
'  '
'  Const DELTA_Y = 140
'  '
'  'INTESTAZIONE
'  stmp = OEM0.frameCalcolo.Caption
'  Printer.CurrentX = MLeft
'  Printer.CurrentY = -4 * IRg + DELTA_Y
'  Printer.Print stmp
'  'Separatore
'  Printer.DrawWidth = WLine
'  Printer.Line (MLeft, -3 * IRg + DELTA_Y)-(PicW, -3 * IRg + DELTA_Y)
'  Printer.DrawWidth = 1
'  'MANDRINO
'  'stmp = " " & OEM0.lblMANDRINI.Caption & " " & NumeroMandrino & " "
'  'Printer.CurrentX = MLeft
'  'Printer.CurrentY = -2 * IRg + DELTA_Y
'  'Printer.Print stmp
'  'Printer.Line (MLeft, -2 * IRg + DELTA_Y)-(MLeft + Printer.TextWidth(stmp), -1 * IRg + DELTA_Y), , B
'  '
'  TabX = 0
'  '*********
'  i = 0
'  stmp = " " & sMR
'  If Printer.TextWidth(stmp) > TabX Then TabX = Printer.TextWidth(stmp)
'  Printer.CurrentX = MLeft
'  Printer.CurrentY = i * IRg + DELTA_Y
'  Printer.Print stmp
'  '*********
'  i = i + 1
'  stmp = " " & sDE_MOLA
'  If Printer.TextWidth(stmp) > TabX Then TabX = Printer.TextWidth(stmp)
'  Printer.CurrentX = MLeft
'  Printer.CurrentY = i * IRg + DELTA_Y
'  Printer.Print stmp
'  '*********
'  i = i + 1
'  stmp = " " & sPRINCIPI
'  If Printer.TextWidth(stmp) > TabX Then TabX = Printer.TextWidth(stmp)
'  Printer.CurrentX = MLeft
'  Printer.CurrentY = i * IRg + DELTA_Y
'  Printer.Print stmp
'  '*********
'  i = i + 1
'  stmp = " " & sHELPG
'  If Printer.TextWidth(stmp) > TabX Then TabX = Printer.TextWidth(stmp)
'  Printer.CurrentX = MLeft
'  Printer.CurrentY = i * IRg + DELTA_Y
'  Printer.Print stmp
'  '*********
'  i = i + 1
'  stmp = " " & sDI1
'  If Printer.TextWidth(stmp) > TabX Then TabX = Printer.TextWidth(stmp)
'  Printer.CurrentX = MLeft
'  Printer.CurrentY = i * IRg + DELTA_Y
'  Printer.Print stmp
'  '*********
'  i = i + 1
'  stmp = " " & sDE1
'  If Printer.TextWidth(stmp) > TabX Then TabX = Printer.TextWidth(stmp)
'  Printer.CurrentX = MLeft
'  Printer.CurrentY = i * IRg + DELTA_Y
'  Printer.Print stmp
'  '*********
'  i = i + 1
'  stmp = " " & sSPOSTAMENTO_ASSIALE_PRF
'  If Printer.TextWidth(stmp) > TabX Then TabX = Printer.TextWidth(stmp)
'  Printer.CurrentX = MLeft
'  Printer.CurrentY = i * IRg + DELTA_Y
'  Printer.Print stmp
'  '*********
'  i = i + 1
'  stmp = " " & sTRATTO_TOTALE
'  If Printer.TextWidth(stmp) > TabX Then TabX = Printer.TextWidth(stmp)
'  Printer.CurrentX = MLeft
'  Printer.CurrentY = i * IRg + DELTA_Y
'  Printer.Print stmp
'  '*********
'  i = i + 1
'  stmp = " " & sPASSO
'  If Printer.TextWidth(stmp) > TabX Then TabX = Printer.TextWidth(stmp)
'  Printer.CurrentX = MLeft
'  Printer.CurrentY = i * IRg + DELTA_Y
'  Printer.Print stmp
'  '*********
'  i = i + 1
'  stmp = " " & "CLASS"
'  If Printer.TextWidth(stmp) > TabX Then TabX = Printer.TextWidth(stmp)
'  Printer.CurrentX = MLeft
'  Printer.CurrentY = i * IRg + DELTA_Y
'  Printer.Print stmp
'  '*********
'  i = 0
'  stmp = ": " & frmt$(Mr, 4)
'  Printer.CurrentX = MLeft + TabX
'  Printer.CurrentY = i * IRg + DELTA_Y
'  Printer.Print stmp
'  '*********
'  i = i + 1
'  stmp = ": " & frmt$(DE_MOLA / 2, 4)
'  Printer.CurrentX = MLeft + TabX
'  Printer.CurrentY = i * IRg + DELTA_Y
'  Printer.Print stmp
'  '*********
'  i = i + 1
'  stmp = ": " & Format$(PRINCIPI, "###0")
'  Printer.CurrentX = MLeft + TabX
'  Printer.CurrentY = i * IRg + DELTA_Y
'  Printer.Print stmp
'  '*********
'  i = i + 1
'  stmp = ": " & frmt$(HELPG, 4)
'  Printer.CurrentX = MLeft + TabX
'  Printer.CurrentY = i * IRg + DELTA_Y
'  Printer.Print stmp
'  '*********
'  i = i + 1
'  stmp = ": " & frmt$(DI1, 4)
'  Printer.CurrentX = MLeft + TabX
'  Printer.CurrentY = i * IRg + DELTA_Y
'  Printer.Print stmp
'  '*********
'  i = i + 1
'  stmp = ": " & frmt$(DE1, 4)
'  Printer.CurrentX = MLeft + TabX
'  Printer.CurrentY = i * IRg + DELTA_Y
'  Printer.Print stmp
'  '*********
'  i = i + 1
'  stmp = ": " & frmt$(SPOSTAMENTO_ASSIALE_PRF, 4)
'  Printer.CurrentX = MLeft + TabX
'  Printer.CurrentY = i * IRg + DELTA_Y
'  Printer.Print stmp
'  '*********
'  i = i + 1
'  stmp = ": " & frmt$(TRATTO_TOTALE, 4)
'  Printer.CurrentX = MLeft + TabX
'  Printer.CurrentY = i * IRg + DELTA_Y
'  Printer.Print stmp
'  '*********
'  i = i + 1
'  stmp = ": " & frmt$(PASSO, 4)
'  Printer.CurrentX = MLeft + TabX
'  Printer.CurrentY = i * IRg + DELTA_Y
'  Printer.Print stmp
'  '*********
'  i = i + 1
'      If (TEMPO_TOTALE_SECONDI < 3600) Then
'          stmp = ": " & "(A) < 1h"
'  ElseIf (TEMPO_TOTALE_SECONDI >= 3600) And (TEMPO_TOTALE_SECONDI < 7200) Then
'          stmp = ": " & "1h < (B) < 2h"
'  ElseIf (TEMPO_TOTALE_SECONDI >= 7200) And (TEMPO_TOTALE_SECONDI < 10800) Then
'          stmp = ": " & "2h < (C) < 3h"
'  ElseIf (TEMPO_TOTALE_SECONDI >= 10800) And (TEMPO_TOTALE_SECONDI < 14400) Then
'          stmp = ": " & "3h < (D) < 4h"
'  Else
'          stmp = ": " & "(X) > 4h"
'  End If
'  Printer.CurrentX = MLeft + TabX
'  Printer.CurrentY = i * IRg + DELTA_Y
'  Printer.Print stmp
'  '
'  'TEMPI DI DIVISIONE
'  j = i
'  '
'  TabX = 0
'  i = j + 2
'  For k = 1 To MAXNumeroCicli
'    If (TipCic(k) <> "N") Then
'      i = i + 1
'      '
'      stmp = " " & Trim$(OEM0.Label3(k).Caption)
'      If (Printer.TextWidth(stmp) > TabX) Then TabX = Printer.TextWidth(stmp)
'      Printer.CurrentX = MLeft + 0
'      Printer.CurrentY = DELTA_Y + i * IRg
'      Printer.Print stmp
'      '
'    End If
'  Next k
'  '
'  i = j + 2
'  Printer.CurrentX = MLeft + TabX
'  Printer.CurrentY = DELTA_Y + i * IRg
'  stmp = " FORWARD"
'  Printer.Print stmp
'  Printer.CurrentX = MLeft + PicW / 4
'  Printer.CurrentY = DELTA_Y + i * IRg
'  stmp = " INDEXING"
'  Printer.Print stmp
'  Printer.Line (0, DELTA_Y + (i + 1) * IRg)-(PicW / 2, DELTA_Y + (i + 1) * IRg)
'  '
'  'MINUTI ANDATA
'  i = j + 2
'  For k = 1 To MAXNumeroCicli
'    If (TipCic(k) <> "N") Then
'      i = i + 1
'      '
'      'MINUTI ANDATA
'      stmp = ": " & Format$(Int(TPS_ANDATA(k) / 60), "#0") & "'"
'      If Printer.TextWidth(stmp) > TABx1 Then TABx1 = Printer.TextWidth(stmp)
'      Printer.CurrentX = MLeft + TabX
'      Printer.CurrentY = DELTA_Y + i * IRg
'      Printer.Print stmp
'    End If
'  Next k
'  '
'  'SECONDI ANDATA + DIVISIONE SINGOLA + RITORNO
'  i = j + 2
'  For k = 1 To MAXNumeroCicli
'    If (TipCic(k) <> "N") Then
'      i = i + 1
'      '
'      'SECONDI ANDATA
'      stmp = " " & Format(Int(TPS_ANDATA(k) - Int(TPS_ANDATA(k) / 60) * 60), "###0") & Chr$(34)
'      If Printer.TextWidth(stmp) > TABx2 Then TABx2 = Printer.TextWidth(stmp)
'      Printer.CurrentX = MLeft + TabX + TABx1
'      Printer.CurrentY = DELTA_Y + i * IRg
'      Printer.Print stmp
'      '
'      'DIVISIONE SINGOLA + RITORNO
'      stmp = " " & Format(TPS_DIV(k) + TPS_RITORNO(k), "###0") & Chr$(34)
'      Printer.CurrentX = MLeft + PicW / 4
'      Printer.CurrentY = DELTA_Y + i * IRg
'      Printer.Print stmp
'      '
'    End If
'  Next k
'  '
'  'CALCOLO TEMPO TOTALE DI RETTIFICA
'  Printer.CurrentX = MLeft + PicW / 2
'  Printer.CurrentY = DELTA_Y
'  stmp = " " & MESSAGGI(0)
'  Printer.Print stmp
'  i = 1
'  Printer.DrawWidth = WLine
'  Printer.Line (MLeft + PicW / 2, i * IRg + DELTA_Y)-(PicW, i * IRg + DELTA_Y)
'  Printer.DrawWidth = 1
'  Printer.CurrentX = MLeft + PicW / 2
'  Printer.CurrentY = i * IRg + DELTA_Y
'  TabX = 0
'  For i = 1 To MAXNumeroCicli
'    If (TipCic(i) <> "N") Then
'      Printer.CurrentX = MLeft + PicW / 2
'      Printer.CurrentY = i * IRg + DELTA_Y
'      stmp = " " & Trim$(OEM0.Label3(i).Caption)
'      If Printer.TextWidth(stmp) > TabX Then TabX = Printer.TextWidth(stmp)
'      Printer.Print stmp
'    End If
'  Next i
'  Printer.CurrentX = MLeft + PicW / 2
'  Printer.CurrentY = i * IRg + DELTA_Y
'  If (Lingua = "CH") Then
'    stmp = " " & MESSAGGI(3)
'  Else
'    stmp = " " & Left$(MESSAGGI(3), 7)
'  End If
'  If Printer.TextWidth(stmp) > TabX Then TabX = Printer.TextWidth(stmp)
'  'Printer.Print stmp
'  '
'  i = i + 1
'  Printer.CurrentX = MLeft + PicW / 2
'  Printer.CurrentY = i * IRg + DELTA_Y
'  stmp = " " & MESSAGGI(2) & " "
'  If Printer.TextWidth(stmp) > TabX Then TabX = Printer.TextWidth(stmp)
'  Printer.Print stmp
'  '
'  TABx1 = 0
'  TT = 0
'  For i = 1 To MAXNumeroCicli
'    If (TipCic(i) <> "N") Then
'      Printer.CurrentX = MLeft + PicW / 2 + TabX
'      Printer.CurrentY = i * IRg + DELTA_Y
'      TH = Int(TPS_RET(i) / 60 / 60)
'      stmp = " " & Format$(TH, "#0") & "h"
'      If Printer.TextWidth(stmp) > TABx1 Then TABx1 = Printer.TextWidth(stmp)
'      '
'      If (TH > 0) Then Printer.Print stmp
'      '
'      TT = TT + TPS_RET(i)
'    End If
'  Next i
'  TT = TT + TPS_FASATURA
'  '
'  Printer.CurrentX = MLeft + PicW / 2 + TabX + TABx1
'  Printer.CurrentY = i * IRg + DELTA_Y
'  stmp = " " & Format$(Int(TPS_FASATURA / 60), "#0") & "'"
'  If Printer.TextWidth(stmp) > TABx1 Then TABx1 = Printer.TextWidth(stmp)
'  'Printer.Print stmp
'  '
'  TT = 0
'  TABx2 = 0
'  For i = 1 To MAXNumeroCicli
'    If (TipCic(i) <> "N") Then
'      Printer.CurrentX = MLeft + PicW / 2 + TabX + TABx1
'      Printer.CurrentY = i * IRg + DELTA_Y
'      TH = Int(TPS_RET(i) / 60 / 60)
'      Tm = Int(TPS_RET(i) / 60 - 60 * TH)
'      stmp = " " & Format(Tm, "###0") & "'"
'      If Printer.TextWidth(stmp) > TABx2 Then TABx2 = Printer.TextWidth(stmp)
'      Printer.Print stmp
'      TT = TT + TPS_RET(i)
'    End If
'  Next i
'  TT = TT + TPS_FASATURA
'  '
'  TT = 0
'  TABx3 = 0
'  For i = 1 To MAXNumeroCicli
'    If (TipCic(i) <> "N") Then
'      Printer.CurrentX = MLeft + PicW / 2 + TabX + TABx1 + TABx2
'      Printer.CurrentY = i * IRg + DELTA_Y
'      TH = Int(TPS_RET(i) / 60 / 60)
'      Tm = Int(TPS_RET(i) / 60 - 60 * TH)
'      Ts = TPS_RET(i) - 60 * 60 * TH - 60 * Tm
'      stmp = " " & Format(Ts, "###0") & Chr$(34)
'      If Printer.TextWidth(stmp) > TABx3 Then TABx3 = Printer.TextWidth(stmp)
'      Printer.Print stmp
'      TT = TT + TPS_RET(i)
'    End If
'  Next i
'  TT = TT + TPS_FASATURA
'  '
'  Printer.CurrentX = MLeft + PicW / 2 + TabX + TABx1 + TABx2
'  Printer.CurrentY = i * IRg + DELTA_Y
'  stmp = " " & Format(Int(TPS_FASATURA - Int(TPS_FASATURA / 60) * 60), "###0") & Chr$(34)
'  If Printer.TextWidth(stmp) > TABx3 Then TABx3 = Printer.TextWidth(stmp)
'  'Printer.Print stmp
'  '
'  i = i + 1
'  Printer.Line (MLeft + PicW / 2, i * IRg + DELTA_Y)-(MLeft + PicW, i * IRg + DELTA_Y)
'  Printer.CurrentX = PicW / 2 + TabX
'  Printer.CurrentY = i * IRg + DELTA_Y
'  TH = Int(TT / 60 / 60)
'  Tm = Int(TT / 60 - 60 * TH)
'  Ts = TT - 60 * 60 * TH - 60 * Tm
'  stmp = ""
'  If (TH > 0) Then
'    stmp = stmp & " " & Format$(TH, "###0") & "h"
'  Else
'    stmp = stmp & "    "
'  End If
'  stmp = stmp & " " & Format$(Tm, "###0") & "'"
'  stmp = stmp & " " & Format$(Ts, "###0") & Chr$(34)
'  Printer.Print stmp
'  '
'  'CALCOLO ASPORTAZIONI
'  i = 0
'  Printer.CurrentX = MLeft + 3 * PicW / 4
'  Printer.CurrentY = i * IRg + DELTA_Y
'  stmp = MESSAGGI(1)
'  Printer.Print stmp
'  '
'  TabX = 0
'  For i = 1 To MAXNumeroCicli
'    If (TipCic(i) <> "N") Then
'      Printer.CurrentX = MLeft + 3 * PicW / 4
'      Printer.CurrentY = i * IRg + DELTA_Y
'      stmp = " " & ASSE_RADIALE & "=" & frmt(TOT_RAD(i), 4)
'      If Printer.TextWidth(stmp) > TabX Then TabX = Printer.TextWidth(stmp)
'      Printer.Print stmp
'    End If
'  Next i
'  '
'  TABx1 = 0
'  For i = 1 To MAXNumeroCicli
'    If (TipCic(i) <> "N") Then
'      Printer.CurrentX = MLeft + 3 * PicW / 4 + TabX
'      Printer.CurrentY = i * IRg + DELTA_Y
'      stmp = " CM=" & Format$(INTERO(VelAnd(i) * TAGLIENTI, "MIN"), "##0")
'      If Printer.TextWidth(stmp) > TABx1 Then TABx1 = Printer.TextWidth(stmp)
'      Printer.Print stmp
'    End If
'  Next i
'  '
'  i = i + 1
'  '
'  TOT_RAD(0) = 0
'  For kkk = 1 To MAXNumeroCicli
'    TOT_RAD(0) = TOT_RAD(0) + TOT_RAD(kkk)
'  Next kkk
'  '
'  stmp = " X=" & frmt(TOT_RAD(0), 4)
'  Printer.CurrentX = MLeft + 3 * PicW / 4
'  Printer.CurrentY = i * IRg + DELTA_Y
'  Printer.Print stmp
'  '
'  Printer.Line (MLeft + PicW / 2, DELTA_Y + 0 * IRg)-(MLeft + PicW / 2, DELTA_Y + (i + 2) * IRg)
'  Printer.Line (MLeft + 3 * PicW / 4 + DELTA_X / 2, DELTA_Y + 0 * IRg)-(MLeft + 3 * PicW / 4 + DELTA_X / 2, DELTA_Y + (i + 2) * IRg)
'  '
'  i = i + 2
'  stmp = " " & MESSAGGI(4) & ": " & frmt(USURA_DIAMETRO, 4) & " "
'  Printer.CurrentX = MLeft + 3 * PicW / 4 - Printer.TextWidth(stmp) / 2
'  Printer.CurrentY = i * IRg + DELTA_Y
'  Printer.Print stmp
'  TabX = Printer.TextWidth(stmp)
'  Printer.Line (MLeft + PicW / 2, i * IRg + DELTA_Y)-(MLeft + PicW, (i + 1) * IRg + DELTA_Y), , B
'  '
'Return
'
End Sub

Sub CALCOLO_SEQUENZA_CICLI()
'
Dim TIPO_LAVORAZIONE As String
Dim IDTIPO As String
'
Dim MODALITA                    As String
Dim SEZIONE                     As String
Dim SOTTOPROGRAMMA              As String
Dim ret                         As Long
Dim PercorsoINI                 As String
Dim Session                     As IMCDomain
Dim DIRETTORIO_WKS              As String
Dim DBB                         As Database
Dim RSS                         As Recordset
Dim sMR                         As String  'MODULO NORMALE
Dim sPRINCIPI                   As String  'NUMERO PRINCIPI
Dim sTRATTO_TOTALE              As String
Dim sPASSO                      As String
Dim RAGGIO_ESTERNO              As Double
Dim RAGGIO_TESTA                As Double
Dim RAGGIO_RULLO                As Double
Dim ANGOLO_PRESSIONE            As Double
Dim ZONA(2)                     As Double  'SEZIONI
Dim Mr                          As Double  'MODULO NORMALE
Dim PRINCIPI                    As Integer 'NUMERO PRINCIPI
Dim PASSO                       As Double  'PASSO
Dim TAGLIENTI                   As Integer
Dim Camma                       As Double
Dim RAGGIO_RULLO_CORR           As Double
Dim sRAGGIO_RULLO_CORR          As String
Dim TIPO_MOLA                   As String  'CERAMICA SG o CBN PROFILABILE
Dim VelSGR                      As Double
Dim VelPRE                      As Double
Dim VelFIN                      As Double
Dim X1                          As Double
Dim X2                          As Double
Dim Y1                          As Double
Dim Y2                          As Double
Dim RIPE_RET(3)                 As Integer
Dim PASS_PRF(3)                 As Integer
Dim INCR_PRF(3)                 As Double
Dim FEED_PRF(3)                 As Double
Dim USURA_RULLO                 As Double
Dim VelPRF_MOLA                 As Double
Dim ROTAZIONE_RULLO             As Double
Dim VELOCITA_RULLO              As Double
Dim PASS_RET(3)                 As Integer
Dim INCR_RET(3)                 As Double
Dim VelRET_MOLA                 As Double
Dim SVR_SGROSSATURA             As Double
Dim SVR_TOTALE                  As Double
Dim SVR_CAMMA                   As Double
Dim ERR_PASSO                   As Double
Dim ERR_ECCENTRICITA            As Double
Dim TIPO_SVR                    As Double
Dim FRAZIONE                    As Double
Dim LUNGHEZZA_MASSIMA_RETTIFICA As Double
Dim PASSATE_PRIMA_PROFILATURA   As Integer
Dim stmp                        As String
Dim TIPO_RCR                    As String
Dim TIPO_CAMMA                  As String
Dim FATTORE_RIDUZIONE           As Double
'
On Error GoTo errCALCOLO_SEQUENZA_CICLI
  '
  Write_Dialog "CALCOLO SEQUENZA CICLI"
  '
  'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  '
  If TIPO_LAVORAZIONE = "CREATORI_STANDARD" Then IDTIPO = "10"
  If TIPO_LAVORAZIONE = "CREATORI_FORMA" Then IDTIPO = "15"
  '
  Set DBB = OpenDatabase(PathDB, True, False)
  '
  Set RSS = DBB.OpenRecordset("OEM1_MACCHINA" & IDTIPO)
  '
  RSS.Index = "INDICE"
  '
  RSS.Seek "=", 6
  RAGGIO_RULLO_CORR = val(RSS.Fields("ACTUAL_1").Value)
  sRAGGIO_RULLO_CORR = RSS.Fields("NOME_" & Lingua).Value
  '
  RSS.Close
  '
  Set RSS = DBB.OpenRecordset("OEM1_PROFILO" & IDTIPO)
  '
  RSS.Index = "INDICE"
  '
  RSS.Seek "=", 1
  PRINCIPI = val(RSS.Fields("ACTUAL_1").Value)       'NUMERO PRINCIPI
  sPRINCIPI = RSS.Fields("NOME_" & Lingua).Value
  '
  RSS.Seek "=", 2
  PASSO = val(RSS.Fields("ACTUAL_1").Value)          'PASSO
  sPASSO = RSS.Fields("NOME_" & Lingua).Value        'PASSO
  '
  RSS.Seek "=", 4
  RAGGIO_ESTERNO = val(RSS.Fields("ACTUAL_1").Value) 'DIAMETRO ESTERNO
  RAGGIO_ESTERNO = RAGGIO_ESTERNO / 2
  '
  RSS.Seek "=", 5
  TAGLIENTI = val(RSS.Fields("ACTUAL_1").Value)      'NUMERO SCANALATURE
  '
  RSS.Seek "=", 6
  Camma = val(RSS.Fields("ACTUAL_1").Value)          'CAMMA
  '
  RSS.Seek "=", 17
  Mr = val(RSS.Fields("ACTUAL_1").Value)             'MODULO NORMALE
  sMR = RSS.Fields("NOME_" & Lingua).Value           'MODULO NORMALE
  '
  RSS.Seek "=", 21
  TIPO_MOLA = RSS.Fields("ACTUAL_1").Value
  '
  RSS.Seek "=", 22
  TIPO_SVR = val(RSS.Fields("ACTUAL_1").Value)
  '
  RSS.Seek "=", 23
  FRAZIONE = val(RSS.Fields("ACTUAL_1").Value)
  '
  RSS.Seek "=", 24
  TIPO_CAMMA = Trim$(UCase$(RSS.Fields("ACTUAL_1").Value))
  '
  'PRENDO L'ANGOLO PIU' RIPIDO: CASO PEGGIORE
  RSS.Seek "=", 27
  If (val(RSS.Fields("ACTUAL_1").Value) < val(RSS.Fields("ACTUAL_2").Value)) Then
    ANGOLO_PRESSIONE = val(RSS.Fields("ACTUAL_1").Value)   ' F1
  Else
    ANGOLO_PRESSIONE = val(RSS.Fields("ACTUAL_2").Value)   ' F2
  End If
  ANGOLO_PRESSIONE = ANGOLO_PRESSIONE / 180 * PG
  '
  'PRENDO IL RAGGIO PIU' PICCOLO: CASO PEGGIORE
  RSS.Seek "=", 29
  If (val(RSS.Fields("ACTUAL_1").Value) < val(RSS.Fields("ACTUAL_2").Value)) Then
    RAGGIO_TESTA = val(RSS.Fields("ACTUAL_1").Value)   'RAGGIO TESTA F1
  Else
    RAGGIO_TESTA = val(RSS.Fields("ACTUAL_2").Value)   'RAGGIO TESTA F2
  End If
  '
  RSS.Close
  '
  Set RSS = DBB.OpenRecordset("OEM1_LAVORO" & IDTIPO)
  '
  RSS.Index = "INDICE"
  '
  RSS.Seek "=", 19
  ZONA(1) = val(RSS.Fields("ACTUAL_1").Value) 'TRATTO 1
  ZONA(2) = val(RSS.Fields("ACTUAL_2").Value) 'TRATTO 2
  sTRATTO_TOTALE = RSS.Fields("NOME_" & Lingua).Value
  '
  RSS.Close
  '
  DBB.Close
  '
  stmp = GetInfo("TEMPI_" & TIPO_LAVORAZIONE, "TIPO_RCR", Path_LAVORAZIONE_INI)
  If (Len(stmp) > 0) Then TIPO_RCR = Trim$(UCase$(stmp)) Else TIPO_RCR = "RCR2000"
  '
  stmp = GetInfo("TEMPI_" & TIPO_LAVORAZIONE, "USURA_RULLO", Path_LAVORAZIONE_INI)
  If (Len(stmp) > 0) Then USURA_RULLO = val(stmp) Else USURA_RULLO = 0
  '
  stmp = GetInfo("TEMPI_" & TIPO_LAVORAZIONE, "VelPRF_MOLA", Path_LAVORAZIONE_INI)
  If (Len(stmp) > 0) Then VelPRF_MOLA = val(stmp) Else VelPRF_MOLA = 20
  '
  stmp = GetInfo("TEMPI_" & TIPO_LAVORAZIONE, "VELOCITA_RULLO", Path_LAVORAZIONE_INI)
  If (Len(stmp) > 0) Then VELOCITA_RULLO = val(stmp) Else VELOCITA_RULLO = 14
  '
  stmp = GetInfo("TEMPI_" & TIPO_LAVORAZIONE, "VelRET_MOLA", Path_LAVORAZIONE_INI)
  If (Len(stmp) > 0) Then VelRET_MOLA = val(stmp) Else VelRET_MOLA = 24
  '
  stmp = GetInfo("TEMPI_" & TIPO_LAVORAZIONE, "FATTORE_RIDUZIONE", Path_LAVORAZIONE_INI)
  If (Len(stmp) > 0) Then FATTORE_RIDUZIONE = val(stmp) Else FATTORE_RIDUZIONE = 0.5
  '
  SVR_CAMMA = 0.1
  ERR_PASSO = 0.12
  ERR_ECCENTRICITA = 0.15
  '
  'FILETTATO SUL TORNIO: TOLTO DOPO AVER PARLATO CON CARUSI
  '
  Select Case TIPO_SVR
    '
    Case 1    'SPOGLIATO
      SVR_TOTALE = SVR_CAMMA + ERR_ECCENTRICITA + ERR_PASSO * (ZONA(1) + ZONA(2)) / 2 / 100 / Tan(ANGOLO_PRESSIONE)
      '
    Case 2    'FILETTATO SU RCLR
      SVR_TOTALE = SVR_CAMMA * FRAZIONE
      '
    Case Else 'ASPORTAZIONE RADIALE
      SVR_TOTALE = TIPO_SVR
      '
  End Select
  '
  Dim NUMERO_GIRI As Double
  Dim CONTATTO    As Double
  Dim LUNGHEZZA   As Double
  Dim ANGOLO      As Double
  '
  'NUMERO DI GIRI PER FARE UNA PASSATA SUL PRINCIPIO
  NUMERO_GIRI = (ZONA(1) + ZONA(2)) / PASSO
  '
  'ANGOLO DI CONTATTO IN RADIANTI
  ANGOLO = FRAZIONE * 2 * PG / TAGLIENTI
  '
  'LUNGHEZZA DEL CONTATTO MOLA DENTE
  CONTATTO = Sqr(ANGOLO ^ 2 * (RAGGIO_ESTERNO ^ 2 + (PASSO / PG / 2) ^ 2) + Camma ^ 2)
  '
  'LUNGHEZZA TOTALE DEL CONTATTO MOLA DENTE PER PASSATA
  LUNGHEZZA = NUMERO_GIRI * TAGLIENTI * CONTATTO
  '
  Select Case TIPO_MOLA
    '
    Case "SG"
      '
      'VALORE COSTANTE
      LUNGHEZZA_MASSIMA_RETTIFICA = 6000
      '
      'SGROSSATURA MOLA SG
          If (Mr >= 0) And (Mr < 1) Then
        VelSGR = 180
      ElseIf (Mr >= 1) And (Mr < 3) Then
        X1 = 1: Y1 = 180
        X2 = 3: Y2 = 130
        VelSGR = (Y2 - Y1) / (X2 - X1) * (Mr - X1) + Y1
      ElseIf (Mr >= 3) And (Mr < 8) Then
        X1 = 3: Y1 = 130
        X2 = 8: Y2 = 70
        VelSGR = (Y2 - Y1) / (X2 - X1) * (Mr - X1) + Y1
      ElseIf (Mr >= 8) Then
        VelSGR = 70
      End If
      VelSGR = VelSGR / TAGLIENTI
      '
      'PRE-FINITURA MOLA SG
          If (Mr >= 0) And (Mr < 1) Then
        VelPRE = 170
      ElseIf (Mr >= 1) And (Mr < 3) Then
        X1 = 1: Y1 = 170
        X2 = 3: Y2 = 120
        VelPRE = (Y2 - Y1) / (X2 - X1) * (Mr - X1) + Y1
      ElseIf (Mr >= 3) And (Mr < 8) Then
        X1 = 3: Y1 = 120
        X2 = 8: Y2 = 60
        VelPRE = (Y2 - Y1) / (X2 - X1) * (Mr - X1) + Y1
      ElseIf (Mr >= 8) Then
        VelPRE = 60
      End If
      VelPRE = VelPRE / TAGLIENTI
      '
      'FINITURA MOLA SG
          If (Mr >= 0) And (Mr < 1) Then
        VelFIN = 140
      ElseIf (Mr >= 1) And (Mr < 3) Then
        X1 = 1: Y1 = 140
        X2 = 3: Y2 = 100
        VelFIN = (Y2 - Y1) / (X2 - X1) * (Mr - X1) + Y1
      ElseIf (Mr >= 3) And (Mr < 8) Then
        X1 = 3: Y1 = 100
        X2 = 8: Y2 = 50
        VelFIN = (Y2 - Y1) / (X2 - X1) * (Mr - X1) + Y1
      ElseIf (Mr >= 8) Then
        VelFIN = 50
      End If
      VelFIN = VelFIN / TAGLIENTI
      '
    Case "CBN"
      '
      'VALORE COSTANTE
      LUNGHEZZA_MASSIMA_RETTIFICA = 15000
      '
      'OPERAZIONE DI FASATURA
      'SGROSSATURA MOLA CBN
          If (Mr >= 0) And (Mr < 1) Then
        VelSGR = 180
      ElseIf (Mr >= 1) And (Mr < 3) Then
        X1 = 1: Y1 = 180
        X2 = 3: Y2 = 130
        VelSGR = (Y2 - Y1) / (X2 - X1) * (Mr - X1) + Y1
      ElseIf (Mr >= 3) And (Mr < 8) Then
        X1 = 3: Y1 = 130
        X2 = 8: Y2 = 70
        VelSGR = (Y2 - Y1) / (X2 - X1) * (Mr - X1) + Y1
      ElseIf (Mr >= 8) Then
        VelSGR = 70
      End If
      VelSGR = VelSGR / TAGLIENTI
      '
      'PRE-FINITURA MOLA CBN
          If (Mr >= 0) And (Mr < 1) Then
        VelPRE = 100
      ElseIf (Mr >= 1) And (Mr < 3) Then
        X1 = 1: Y1 = 100
        X2 = 3: Y2 = 70
        VelPRE = (Y2 - Y1) / (X2 - X1) * (Mr - X1) + Y1
      ElseIf (Mr >= 3) And (Mr < 8) Then
        X1 = 3: Y1 = 70
        X2 = 8: Y2 = 40
        VelPRE = (Y2 - Y1) / (X2 - X1) * (Mr - X1) + Y1
      ElseIf (Mr >= 8) Then
        VelPRE = 40
      End If
      VelPRE = VelPRE / TAGLIENTI
      '
      'FINITURA MOLA CBN
          If (Mr >= 0) And (Mr < 1) Then
        VelFIN = 140
      ElseIf (Mr >= 1) And (Mr < 3) Then
        X1 = 1: Y1 = 140
        X2 = 3: Y2 = 100
        VelFIN = (Y2 - Y1) / (X2 - X1) * (Mr - X1) + Y1
      ElseIf (Mr >= 3) And (Mr < 8) Then
        X1 = 3: Y1 = 100
        X2 = 8: Y2 = 50
        VelFIN = (Y2 - Y1) / (X2 - X1) * (Mr - X1) + Y1
      ElseIf (Mr >= 8) Then
        VelFIN = 50
      End If
      VelFIN = VelFIN / TAGLIENTI
      '
  End Select
  '
  'DIMEZZO LE VELOCITA' A CAUSA DELLA CAMMA ELETTRONICA
  If (TIPO_CAMMA = "E") Then
    VelSGR = VelSGR * FATTORE_RIDUZIONE
    VelPRE = VelPRE * FATTORE_RIDUZIONE
    VelFIN = VelFIN * FATTORE_RIDUZIONE
  End If
  '
  'CALCOLO DELLE PASSATE POSSIBILI PRIMA DELLA PROFILATURA
  PASSATE_PRIMA_PROFILATURA = INTERO(LUNGHEZZA_MASSIMA_RETTIFICA / LUNGHEZZA / PRINCIPI, "MIN")
  If (PASSATE_PRIMA_PROFILATURA = 0) Then PASSATE_PRIMA_PROFILATURA = 1
  '
  'MEDIANTE IL RAGGIO DI TESTA DELLA MOLA SI CONSIDERA IMPLICITAMENTE IL TIPO DEL MATERIALE.
  Select Case TIPO_MOLA
    '
    Case "SG"
      '
      'ROTAZIONE RULLO DISCORDE
      ROTAZIONE_RULLO = -1
      '
      If (RAGGIO_TESTA < 0.3) Then
        '
        RAGGIO_RULLO = 0.1
        '
        'PRE-FINITURA
        PASS_PRF(2) = 1
        INCR_PRF(2) = -0.02
        FEED_PRF(2) = 80
        '
        RIPE_RET(2) = 1
        If (TIPO_RCR = "RCR2000") Then PASS_RET(2) = 1 Else PASS_RET(2) = 2
        INCR_RET(2) = 0.015
        '
        'FINITURA
        PASS_PRF(3) = 1
        INCR_PRF(3) = -0.02
        FEED_PRF(3) = 30
        '
        RIPE_RET(3) = 1
        If (TIPO_RCR = "RCR2000") Then PASS_RET(3) = 1 Else PASS_RET(3) = 3
        INCR_RET(3) = 0.003
        '
        'MATERIALE DA ASPORTARE NELLE PASSATE DI SGROSSATURA
        SVR_SGROSSATURA = SVR_TOTALE - PASS_RET(2) * INCR_RET(2) - PASS_RET(3) * INCR_RET(3)
        '
        'SGROSSATURA
        PASS_PRF(1) = 1
        INCR_PRF(1) = -0.02
        FEED_PRF(1) = 150
        '
        INCR_RET(1) = 0.03
        PASS_RET(1) = PASSATE_PRIMA_PROFILATURA
        'CALCOLO DELLE RIPETIZIONI DEL CICLO DI SGROSSATURA
        RIPE_RET(1) = INTERO(SVR_SGROSSATURA / INCR_RET(1) / PASS_RET(1), "MAX")
        '
      Else
        '
        RAGGIO_RULLO = 0.25
        '
        'PRE-FINITURA
        PASS_PRF(2) = 1
        INCR_PRF(2) = -0.05
        FEED_PRF(2) = 80
        '
        RIPE_RET(2) = 1
        If (TIPO_RCR = "RCR2000") Then PASS_RET(2) = 1 Else PASS_RET(2) = 2
        INCR_RET(2) = 0.015
        '
        'FINITURA
        PASS_PRF(3) = 1
        INCR_PRF(3) = -0.05
        FEED_PRF(3) = 30
        '
        RIPE_RET(3) = 1
        If (TIPO_RCR = "RCR2000") Then PASS_RET(3) = 1 Else PASS_RET(3) = 3
        INCR_RET(3) = 0.003
        '
        'MATERIALE DA ASPORTARE NELLE PASSATE DI SGROSSATURA
        SVR_SGROSSATURA = SVR_TOTALE - PASS_RET(2) * INCR_RET(2) - PASS_RET(3) * INCR_RET(3)
        '
        'SGROSSATURA
        PASS_PRF(1) = 1
        INCR_PRF(1) = -0.05
        FEED_PRF(1) = 150
        '
        INCR_RET(1) = 0.03
        PASS_RET(1) = PASSATE_PRIMA_PROFILATURA
        'CALCOLO DELLE RIPETIZIONI DEL CICLO DI SGROSSATURA
        RIPE_RET(1) = INTERO(SVR_SGROSSATURA / INCR_RET(1) / PASS_RET(1), "MAX")
        '
      End If
      '
    Case "CBN"
      '
      'ROTAZIONE CONCORDE
      ROTAZIONE_RULLO = 1
      '
      RAGGIO_RULLO = 0.25
      '
      'FASATURA
      PASS_PRF(1) = 2
      INCR_PRF(1) = -0.01
      FEED_PRF(1) = 150
      '
      RIPE_RET(1) = 1
      PASS_RET(1) = 1
      INCR_RET(1) = 0.06
      '
      'FINITURA
      PASS_PRF(3) = 2
      INCR_PRF(3) = -0.01
      FEED_PRF(3) = 40
      '
      RIPE_RET(3) = 1
      PASS_RET(3) = 1
      INCR_RET(3) = 0.01
      '
      'MATERIALE DA ASPORTARE NELLE PASSATE DI SGROSSATURA
      SVR_SGROSSATURA = SVR_TOTALE - PASS_RET(1) * INCR_RET(1) - PASS_RET(3) * INCR_RET(3)
      '
      'SGROSSATURA
      PASS_PRF(2) = 2
      INCR_PRF(2) = -0.01
      FEED_PRF(2) = 150
      '
      INCR_RET(2) = 0.06
      PASS_RET(2) = PASSATE_PRIMA_PROFILATURA
      'CALCOLO DELLE RIPETIZIONI DEL CICLO DI SGROSSATURA
      RIPE_RET(2) = INTERO(SVR_SGROSSATURA / INCR_RET(2) / PASS_RET(2), "MAX")
      '
  End Select
  '
  'LETTURA DELLA MODALITA DI MODIFICA DATI
  MODALITA = GetInfo("Configurazione", "Modalita", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))
  '
  Select Case MODALITA
    '
    Case "OFF-LINE"
      '
      PercorsoINI = g_chOemPATH & "\" & TIPO_LAVORAZIONE & ".INI"
      '
      SEZIONE = "OEM0_CICLI"
      '
      'CICLO 1: SGROSSATURA
      ret = WritePrivateProfileString(SEZIONE, "STR[11]", "S", PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,1]", Format$(RIPE_RET(1), "###0"), PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,2]", Format$(PASS_PRF(1), "###0"), PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,3]", frmt(INCR_PRF(1), 3), PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,4]", Format$(FEED_PRF(1), "##0"), PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,5]", frmt(USURA_RULLO, 4), PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,6]", frmt(VelPRF_MOLA, 1), PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,7]", Format$(ROTAZIONE_RULLO, "#0"), PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,8]", frmt(VELOCITA_RULLO, 1), PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,9]", Format$(PASS_RET(1), "###0"), PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,10]", frmt(INCR_RET(1), 3), PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,11]", frmt(VelSGR, 1), PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,12]", frmt(VelRET_MOLA, 1), PercorsoINI)
      '
      'CICLO 2: PRE-FINITURA
      ret = WritePrivateProfileString(SEZIONE, "STR[12]", "P", PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,21]", Format$(RIPE_RET(2), "###0"), PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,22]", Format$(PASS_PRF(2), "###0"), PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,23]", frmt(INCR_PRF(2), 3), PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,24]", Format$(FEED_PRF(2), "##0"), PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,25]", frmt(USURA_RULLO, 4), PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,26]", frmt(VelPRF_MOLA, 1), PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,27]", Format$(ROTAZIONE_RULLO, "#0"), PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,28]", frmt(VELOCITA_RULLO, 1), PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,29]", Format$(PASS_RET(2), "###0"), PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,30]", frmt(INCR_RET(2), 3), PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,31]", frmt(VelPRE, 1), PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,32]", frmt(VelRET_MOLA, 1), PercorsoINI)
      '
      'CICLO 3: FINITURA
      ret = WritePrivateProfileString(SEZIONE, "STR[13]", "F", PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,41]", Format$(RIPE_RET(3), "###0"), PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,42]", Format$(PASS_PRF(3), "###0"), PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,43]", frmt(INCR_PRF(3), 3), PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,44]", Format$(FEED_PRF(3), "##0"), PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,45]", frmt(USURA_RULLO, 4), PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,46]", frmt(VelPRF_MOLA, 1), PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,47]", Format$(ROTAZIONE_RULLO, "#0"), PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,48]", frmt(VELOCITA_RULLO, 1), PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,49]", Format$(PASS_RET(3), "###0"), PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,50]", frmt(INCR_RET(3), 3), PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,51]", frmt(VelFIN, 1), PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,52]", frmt(VelRET_MOLA, 1), PercorsoINI)
      '
      'CICLO 4
      ret = WritePrivateProfileString(SEZIONE, "STR[14]", "N", PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,61]", "0", PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,62]", "0", PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,63]", "0", PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,64]", "0", PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,65]", "0", PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,66]", "0", PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,67]", "0", PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,68]", "0", PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,69]", "0", PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,70]", "0", PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,71]", "0", PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,72]", "0", PercorsoINI)
      '
      'CICLO 5
      ret = WritePrivateProfileString(SEZIONE, "STR[15]", "N", PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,81]", "0", PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,82]", "0", PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,83]", "0", PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,84]", "0", PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,85]", "0", PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,86]", "0", PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,87]", "0", PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,88]", "0", PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,89]", "0", PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,90]", "0", PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,91]", "0", PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,92]", "0", PercorsoINI)
      '
      'CICLO 6
      ret = WritePrivateProfileString(SEZIONE, "STR[16]", "N", PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,101]", "0", PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,102]", "0", PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,103]", "0", PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,104]", "0", PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,105]", "0", PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,106]", "0", PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,107]", "0", PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,108]", "0", PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,109]", "0", PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,110]", "0", PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,111]", "0", PercorsoINI)
      ret = WritePrivateProfileString(SEZIONE, "CYC[0,112]", "0", PercorsoINI)
      '
      'SEZIONE = "OEM1_MACCHINA"
      'RAGGIO RULLO
      'ret = WritePrivateProfileString(SEZIONE, "$TC_DP6[1,1]", frmt(RAGGIO_RULLO, 4), PercorsoINI)
      '
    Case "SPF"
      '
      DIRETTORIO_WKS = LEGGI_DIRETTORIO_WKS(TIPO_LAVORAZIONE)
      '
      Set Session = GetObject("@SinHMIMCDomain.MCDomain")
      '
      SOTTOPROGRAMMA = "OEM0_CICLI" & IDTIPO
      Open g_chOemPATH & "\" & SOTTOPROGRAMMA & ".SPF" For Output As #1
        '
        'INTESTAZIONE DEL PROGRAMMA
        Print #1, ";PROGRAMMA : " & SOTTOPROGRAMMA
        Print #1, ";VERSIONE  : " & Date & " " & Time
        '
        'CICLO 1: SGROSSATURA
        Print #1, "STR[11]=" & Chr(34) & "S" & Chr(34)
        Print #1, "CYC[0,1]=" & Format$(RIPE_RET(1), "###0")
        Print #1, "CYC[0,2]=" & Format$(PASS_PRF(1), "###0")
        Print #1, "CYC[0,3]=" & frmt(INCR_PRF(1), 3)
        Print #1, "CYC[0,4]=" & Format$(FEED_PRF(1), "##0")
        Print #1, "CYC[0,5]=" & frmt(USURA_RULLO, 4)
        Print #1, "CYC[0,6]=" & frmt(VelPRF_MOLA, 1)
        Print #1, "CYC[0,7]=" & Format$(ROTAZIONE_RULLO, "#0")
        Print #1, "CYC[0,8]=" & frmt(VELOCITA_RULLO, 1)
        Print #1, "CYC[0,9]=" & Format$(PASS_RET(1), "###0")
        Print #1, "CYC[0,10]=" & frmt(INCR_RET(1), 3)
        Print #1, "CYC[0,11]=" & frmt(VelSGR, 1)
        Print #1, "CYC[0,12]=" & frmt(VelRET_MOLA, 1)
        '
        'CICLO 2: PRE-FINITURA
        Print #1, "STR[12]=" & Chr(34) & "P" & Chr(34)
        Print #1, "CYC[0,21]=" & Format$(RIPE_RET(2), "###0")
        Print #1, "CYC[0,22]=" & Format$(PASS_PRF(2), "###0")
        Print #1, "CYC[0,23]=" & frmt(INCR_PRF(2), 3)
        Print #1, "CYC[0,24]=" & Format$(FEED_PRF(2), "##0")
        Print #1, "CYC[0,25]=" & frmt(USURA_RULLO, 4)
        Print #1, "CYC[0,26]=" & frmt(VelPRF_MOLA, 1)
        Print #1, "CYC[0,27]=" & Format$(ROTAZIONE_RULLO, "#0")
        Print #1, "CYC[0,28]=" & frmt(VELOCITA_RULLO, 1)
        Print #1, "CYC[0,29]=" & Format$(PASS_RET(2), "###0")
        Print #1, "CYC[0,30]=" & frmt(INCR_RET(2), 3)
        Print #1, "CYC[0,31]=" & frmt(VelPRE, 1)
        Print #1, "CYC[0,32]=" & frmt(VelRET_MOLA, 1)
        '
        'CICLO 3: FINITURA
        Print #1, "STR[13]=" & Chr(34) & "F" & Chr(34)
        Print #1, "CYC[0,41]=" & Format$(RIPE_RET(3), "###0")
        Print #1, "CYC[0,42]=" & Format$(PASS_PRF(3), "###0")
        Print #1, "CYC[0,43]=" & frmt(INCR_PRF(3), 3)
        Print #1, "CYC[0,44]=" & Format$(FEED_PRF(3), "##0")
        Print #1, "CYC[0,45]=" & frmt(USURA_RULLO, 4)
        Print #1, "CYC[0,46]=" & frmt(VelPRF_MOLA, 1)
        Print #1, "CYC[0,47]=" & Format$(ROTAZIONE_RULLO, "#0")
        Print #1, "CYC[0,48]=" & frmt(VELOCITA_RULLO, 1)
        Print #1, "CYC[0,49]=" & Format$(PASS_RET(3), "###0")
        Print #1, "CYC[0,50]=" & frmt(INCR_RET(3), 3)
        Print #1, "CYC[0,51]=" & frmt(VelFIN, 1)
        Print #1, "CYC[0,52]=" & frmt(VelRET_MOLA, 1)
        '
        'CICLO 4
        Print #1, "STR[14]=" & Chr(34) & "N" & Chr(34)
        Print #1, "CYC[0,61]=0"
        Print #1, "CYC[0,62]=0"
        Print #1, "CYC[0,63]=0"
        Print #1, "CYC[0,64]=0"
        Print #1, "CYC[0,65]=0"
        Print #1, "CYC[0,66]=0"
        Print #1, "CYC[0,67]=0"
        Print #1, "CYC[0,68]=0"
        Print #1, "CYC[0,69]=0"
        Print #1, "CYC[0,70]=0"
        Print #1, "CYC[0,71]=0"
        Print #1, "CYC[0,72]=0"
        '
        'CICLO 5
        Print #1, "STR[15]=" & Chr(34) & "N" & Chr(34)
        Print #1, "CYC[0,81]=0"
        Print #1, "CYC[0,82]=0"
        Print #1, "CYC[0,83]=0"
        Print #1, "CYC[0,84]=0"
        Print #1, "CYC[0,85]=0"
        Print #1, "CYC[0,86]=0"
        Print #1, "CYC[0,87]=0"
        Print #1, "CYC[0,88]=0"
        Print #1, "CYC[0,89]=0"
        Print #1, "CYC[0,90]=0"
        Print #1, "CYC[0,91]=0"
        Print #1, "CYC[0,92]=0"
        '
        'CICLO 6
        Print #1, "STR[16]=" & Chr(34) & "N" & Chr(34)
        Print #1, "CYC[0,101]=0"
        Print #1, "CYC[0,102]=0"
        Print #1, "CYC[0,103]=0"
        Print #1, "CYC[0,104]=0"
        Print #1, "CYC[0,105]=0"
        Print #1, "CYC[0,106]=0"
        Print #1, "CYC[0,107]=0"
        Print #1, "CYC[0,108]=0"
        Print #1, "CYC[0,109]=0"
        Print #1, "CYC[0,110]=0"
        Print #1, "CYC[0,111]=0"
        Print #1, "CYC[0,112]=0"
        '
        Print #1, "M17 ;FINE PROGRAMMA " & SOTTOPROGRAMMA  '--- FlagSTD003 05.04.06
        '
      Close #1
      '
      Write_Dialog SOTTOPROGRAMMA & ".SPF --> CNC"
      Session.CopyNC g_chOemPATH & "\" & SOTTOPROGRAMMA & ".SPF", "/NC/WKS.DIR/" & DIRETTORIO_WKS & ".WPD/" & SOTTOPROGRAMMA & ".SPF", MCDOMAIN_COPY_NC
      Write_Dialog SOTTOPROGRAMMA & ".SPF --> CNC OK!!"
      '
      'SOTTOPROGRAMMA = "OEM1_MACCHINA10"
      'Call CREAZIONE_OEM1_FILESPF(SOTTOPROGRAMMA)
      'AGGIORNAMENTO DEL VALORE SUL FILE INI
      'ret = WritePrivateProfileString(SOTTOPROGRAMMA, "$TC_DP6[1,1]", frmt(RAGGIO_RULLO, 4), PathFILESPF)
      'CREAZIONE DEL SOTTOPROGRAMMA OEM1_LAVORO10 DAL FILE INI
      'Call CREA_SOTTOPROGRAMMA(SOTTOPROGRAMMA)
      'Write_Dialog SOTTOPROGRAMMA & ".SPF --> CNC"
      'Session.CopyNC g_chOemPATH & "\" & SOTTOPROGRAMMA & ".SPF", "/NC/WKS.DIR/" & DIRETTORIO_WKS & ".WPD/" & SOTTOPROGRAMMA & ".SPF", MCDOMAIN_COPY_NC
      'Write_Dialog SOTTOPROGRAMMA & ".SPF --> CNC OK!!"
      '
      Set Session = Nothing
      '
  End Select
  '
  Write_Dialog ""
  '
Exit Sub

errCALCOLO_SEQUENZA_CICLI:
  Write_Dialog "SUB CALCOLO_SEQUENZA_CICLI ERROR " & Err
  Resume Next

End Sub

Public Sub CALCOLO_DATI_MOLA_CREATORI_STANDARD()
'
Dim MODALITA As String
'
On Error GoTo errCALCOLO_DATI_MOLA_CREATORI_STANDARD
  '
  'SHANTI NON GESTISCE L'ELICA DI RETTIFICA DALLA PAGINA DEL PROFILO
  'NON BISOGNA CREARE ED INVIARE OEM1_LAVORO10
  '
  Call AZZERAMENTO_VALORI_STRUTTURE
  '
  'DOPO OSSERVAZIONI DI ROCCO DI SIPIO.
  'DALLE DUE RCR DI SHANGAI OEM1_PROFILO10 E' STATO MODIFICATO
  '1) SONO STATI TOLTI 4 PARAMETRI DEL CONTROLLO (ST$, ETC)
  '2) SPOSTATI 4 PARAMETRI DALLA LISTA2 ALLA LISTA 1 DI OEM1 (MN, ETC)
  '
  'LETTURA DELLA MODALITA DI MODIFICA DATI
  MODALITA = GetInfo("Configurazione", "Modalita", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))
  '
  Select Case MODALITA
    '
    Case "OFF-LINE"
      '
      Call ASSEGNAZIONE_FINESTRA_STRUTTURA
      Call ASSEGNAZIONE_STRUTTURA_VARIABILI(ParametriCR)
      Call CALCOLO_PROFILO_MOLA_CREATORE
      Call ASSEGNAZIONE_STRUTTURA_SEZIONE
      '
    Case "SPF"
      '
      Call ASSEGNAZIONE_FINESTRA_STRUTTURA
      Call ASSEGNAZIONE_STRUTTURA_VARIABILI(ParametriCR)
      Call CALCOLO_PROFILO_MOLA_CREATORE
      Call ASSEGNAZIONE_STRUTTURA_SOTTOPROGRAMMI
      '
  End Select
  '
Exit Sub

errCALCOLO_DATI_MOLA_CREATORI_STANDARD:
  Write_Dialog "SUB CALCOLO_DATI_MOLA_CREATORI_STANDARD ERROR " & Err
  Resume Next

End Sub

Public Sub ASSEGNAZIONE_STRUTTURA_SEZIONE()
'
Const TIPO_LAVORAZIONE = "CREATORI_STANDARD"
'
Dim SEZIONE     As String
Dim ret         As Long
Dim PercorsoINI As String
Dim Ndec        As Integer
'
On Error GoTo errASSEGNAZIONE_STRUTTURA_SEZIONE
  '
  PercorsoINI = g_chOemPATH & "\" & TIPO_LAVORAZIONE & ".INI"
  '
  Ndec = 3
  SEZIONE = "OEM1_MOLA"
  'MODIFICHE RELATIVE AL CALCOLO DELLA MOLA
  ret = WritePrivateProfileString(SEZIONE, "R37", frmt(ParametriMola.E37, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R38", frmt(ParametriMola.E38, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R39", frmt(ParametriMola.E39, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R60", frmt(ParametriMola.E60, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R41", frmt(ParametriMola.E41, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R51", frmt(ParametriMola.E51, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R42", frmt(ParametriMola.E42, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R52", frmt(ParametriMola.E52, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R43", frmt(ParametriMola.E43, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R53", frmt(ParametriMola.E53, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R44", frmt(ParametriMola.E44, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R54", frmt(ParametriMola.E54, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R45", frmt(ParametriMola.E45, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R55", frmt(ParametriMola.E55, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R46", frmt(ParametriMola.E46, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R56", frmt(ParametriMola.E56, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R47", frmt(ParametriMola.E47, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R57", frmt(ParametriMola.E57, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R48", frmt(ParametriMola.E48, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R58", frmt(ParametriMola.E58, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R49", frmt(ParametriMola.E49, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R59", frmt(ParametriMola.E59, Ndec), PercorsoINI)
  '
  ret = WritePrivateProfileString(SEZIONE, "R173", frmt(RaggioST1, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R174", frmt(RaggioST2, Ndec), PercorsoINI)
  '
  ret = WritePrivateProfileString(SEZIONE, "R31", frmt(ParametriMola.E31, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R32", frmt(ParametriMola.E32, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R40", frmt(ParametriMola.E40, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R50", frmt(ParametriMola.E50, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R61", frmt(ParametriMola.E61, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R71", frmt(ParametriMola.E71, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R62", frmt(ParametriMola.E62, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R72", frmt(ParametriMola.E72, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R63", frmt(ParametriMola.E63, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R73", frmt(ParametriMola.E73, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R64", frmt(ParametriMola.E64, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R74", frmt(ParametriMola.E74, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R65", frmt(ParametriMola.E65, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R75", frmt(ParametriMola.E75, Ndec), PercorsoINI)
  '
  ret = WritePrivateProfileString(SEZIONE, "R80", frmt(ParametriMola.HMRastt1, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R84", frmt(ParametriMola.HMRastt2, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R81", frmt(ParametriMola.ApMRastt1, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R85", frmt(ParametriMola.ApMRastt2, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R237", frmt(ParametriMola.BombMRastt1, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R247", frmt(ParametriMola.BombMRastt2, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R82", frmt(ParametriMola.HMRastp1, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R86", frmt(ParametriMola.HMRastp2, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R83", frmt(ParametriMola.ApMRastp1, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R87", frmt(ParametriMola.ApMRastp2, Ndec), PercorsoINI)
  '
  ret = WritePrivateProfileString(SEZIONE, "R137", frmt(ParametriMola.BombMRastp1, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R227", frmt(ParametriMola.BombMRastp2, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R171", "0", PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R172", "0", PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R106", frmt(ParametriMola.E106, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R107", frmt(ParametriMola.E107, Ndec), PercorsoINI)
  '
  Ndec = 4
  SEZIONE = "OEM1_LAVORO"
  'MOFIDICO IL FILE INI CON I VALORI DEI PARAMETRI IMPOSTATI DALLA PAGINA OEM1_PROFILO10
  ret = WritePrivateProfileString(SEZIONE, "R5", frmt(ParametriCR.Nf, Ndec), PercorsoINI)
  If (ParametriCR.InMola >= 0) Then
    ret = WritePrivateProfileString(SEZIONE, "R206", frmt(Abs(ParametriCR.Paxial), Ndec), PercorsoINI)
  Else
    ret = WritePrivateProfileString(SEZIONE, "R206", frmt(-Abs(ParametriCR.Paxial), Ndec), PercorsoINI)
  End If
  If (ParametriCR.Phel <> 0) Then
    ret = WritePrivateProfileString(SEZIONE, "R207", "1/" & frmt(ParametriCR.Phel, Ndec), PercorsoINI)
  Else
    ret = WritePrivateProfileString(SEZIONE, "R207", "0", PercorsoINI)
  End If
  ret = WritePrivateProfileString(SEZIONE, "R191", frmt(ParametriCR.Z, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R192", frmt(ParametriCR.DestCr, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R193", frmt(ParametriCR.Came, Ndec), PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "R126", frmt(ParametriCR.InMola, Ndec), PercorsoINI)
  '
  SEZIONE = "OEM0_CORRETTORI"
  'AZZERAMENTO CORRETTORI
  ret = WritePrivateProfileString(SEZIONE, "COR[0,0]", "0", PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "COR[0,1]", "0", PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "COR[0,2]", "0", PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "COR[0,3]", "0", PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "COR[0,4]", "0", PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "COR[0,5]", "0", PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "COR[0,6]", "0", PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "COR[0,7]", "0", PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "COR[0,8]", "0", PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "COR[0,9]", "0", PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "COR[0,10]", "0", PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "COR[0,11]", "0", PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "COR[0,12]", "0", PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "COR[0,13]", "0", PercorsoINI)
  ret = WritePrivateProfileString(SEZIONE, "COR[0,14]", "0", PercorsoINI)
  '
Exit Sub

errASSEGNAZIONE_STRUTTURA_SEZIONE:
  Write_Dialog "SUB ASSEGNAZIONE_STRUTTURA_SEZIONE ERROR " & Err
  Resume Next

End Sub

Public Sub CREAZIONE_OEM1_MOLA10()
'
Const Ndec = 4
'
On Error GoTo errCREAZIONE_OEM1_MOLA10
  '
  Open g_chOemPATH & "\" & "OEM1_MOLA10.SPF" For Output As #1
    '
    Print #1, ";PROGRAMMA: " & "OEM1_MOLA10.SPF"
    Print #1, ";VERSIONE : " & Date
    '
    Print #1, "R37=" & frmt(ParametriMola.E37, Ndec)
    Print #1, "R38=" & frmt(ParametriMola.E38, Ndec)
    Print #1, "R39=" & frmt(ParametriMola.E39, Ndec)
    Print #1, "R60=" & frmt(ParametriMola.E60, Ndec)
    Print #1, "R41=" & frmt(ParametriMola.E41, Ndec)
    Print #1, "R51=" & frmt(ParametriMola.E51, Ndec)
    Print #1, "R42=" & frmt(ParametriMola.E42, Ndec)
    Print #1, "R52=" & frmt(ParametriMola.E52, Ndec)
    Print #1, "R43=" & frmt(ParametriMola.E43, Ndec)
    Print #1, "R53=" & frmt(ParametriMola.E53, Ndec)
    Print #1, "R44=" & frmt(ParametriMola.E44, Ndec)
    Print #1, "R54=" & frmt(ParametriMola.E54, Ndec)
    Print #1, "R45=" & frmt(ParametriMola.E45, Ndec)
    Print #1, "R55=" & frmt(ParametriMola.E55, Ndec)
    Print #1, "R46=" & frmt(ParametriMola.E46, Ndec)
    Print #1, "R56=" & frmt(ParametriMola.E56, Ndec)
    Print #1, "R47=" & frmt(ParametriMola.E47, Ndec)
    Print #1, "R57=" & frmt(ParametriMola.E57, Ndec)
    Print #1, "R48=" & frmt(ParametriMola.E48, Ndec)
    Print #1, "R58=" & frmt(ParametriMola.E58, Ndec)
    Print #1, "R49=" & frmt(ParametriMola.E49, Ndec)
    Print #1, "R59=" & frmt(ParametriMola.E59, Ndec)
    Print #1, "R173=" & frmt(RaggioST1, Ndec)
    Print #1, "R174=" & frmt(RaggioST2, Ndec)
    Print #1, "R31=" & frmt(ParametriMola.E31, Ndec)
    Print #1, "R32=" & frmt(ParametriMola.E32, Ndec)
    Print #1, "R40=" & frmt(ParametriMola.E40, Ndec)
    Print #1, "R50=" & frmt(ParametriMola.E50, Ndec)
    Print #1, "R61=" & frmt(ParametriMola.E61, Ndec)
    Print #1, "R71=" & frmt(ParametriMola.E71, Ndec)
    Print #1, "R62=" & frmt(ParametriMola.E62, Ndec)
    Print #1, "R72=" & frmt(ParametriMola.E72, Ndec)
    Print #1, "R63=" & frmt(ParametriMola.E63, Ndec)
    Print #1, "R73=" & frmt(ParametriMola.E73, Ndec)
    Print #1, "R64=" & frmt(ParametriMola.E64, Ndec)
    Print #1, "R74=" & frmt(ParametriMola.E74, Ndec)
    Print #1, "R65=" & frmt(ParametriMola.E65, Ndec)
    Print #1, "R75=" & frmt(ParametriMola.E75, Ndec)
    Print #1, "R80=" & frmt(ParametriMola.HMRastt1, Ndec)
    Print #1, "R84=" & frmt(ParametriMola.HMRastt2, Ndec)
    Print #1, "R81=" & frmt(ParametriMola.ApMRastt1, Ndec)
    Print #1, "R85=" & frmt(ParametriMola.ApMRastt2, Ndec)
    Print #1, "R237=" & frmt(ParametriMola.BombMRastt1, Ndec)
    Print #1, "R247=" & frmt(ParametriMola.BombMRastt2, Ndec)
    Print #1, "R82=" & frmt(ParametriMola.HMRastp1, Ndec)
    Print #1, "R86=" & frmt(ParametriMola.HMRastp2, Ndec)
    Print #1, "R83=" & frmt(ParametriMola.ApMRastp1, Ndec)
    Print #1, "R87=" & frmt(ParametriMola.ApMRastp2, Ndec)
    Print #1, "R137=" & frmt(ParametriMola.BombMRastp1, Ndec)
    Print #1, "R227=" & frmt(ParametriMola.BombMRastp2, Ndec)
    Print #1, "R171=0"
    Print #1, "R172=0"
    Print #1, "R106=" & frmt(ParametriMola.E106, Ndec)
    Print #1, "R107=" & frmt(ParametriMola.E107, Ndec)
    '
    Print #1, "M17 ;Fine"
    '
  Close #1
  '
Exit Sub

errCREAZIONE_OEM1_MOLA10:
  Write_Dialog "SUB CREAZIONE_OEM1_MOLA10 ERROR " & Err
  Resume Next

End Sub

Public Sub CREAZIONE_OEM1_FILESPF(ByVal NomeTb As String)
'
Dim DB      As Database
Dim TB2     As Recordset
'
Dim j       As Integer
Dim rsp     As Long
Dim stmp1   As String
Dim stmp2   As String
'
Const Ndec = 4
'
On Error GoTo errCREAZIONE_OEM1_FILESPF
  '
  'ELIMINO IL FILE INI DI APPOGGIO PER IL TRASFERIMENTO SOTTOPROGRAMMI
  If (Dir$(PathFileSPF) <> "") Then Kill PathFileSPF
  '
  'APRO IL DATABASE
  Set DB = OpenDatabase(PathDB, True, False)
  '
  'APRO LA TABELLA DEL GRUPPO DI PARAMETRI
  Set TB2 = DB.OpenRecordset(NomeTb, dbOpenTable)
    '
    Do Until TB2.EOF
      '
      If (TB2.Fields("ABILITATO").Value = "Y") Then
        '
        Select Case TB2.Fields("GRUPPO").Value
          '
          Case 1
            j = 1
              stmp1 = TB2.Fields("VARIABILE_" & Format$(j, "0")).Value
              stmp2 = TB2.Fields("ACTUAL_" & Format$(j, "0")).Value
              rsp = WritePrivateProfileString(NomeTb, stmp1, stmp2, PathFileSPF)
            '
          Case 2
            j = 2
              stmp1 = TB2.Fields("VARIABILE_" & Format$(j, "0")).Value
              stmp2 = TB2.Fields("ACTUAL_" & Format$(j, "0")).Value
              rsp = WritePrivateProfileString(NomeTb, stmp1, stmp2, PathFileSPF)
            '
          Case 3
            For j = 1 To 2
              stmp1 = TB2.Fields("VARIABILE_" & Format$(j, "0")).Value
              stmp2 = TB2.Fields("ACTUAL_" & Format$(j, "0")).Value
              rsp = WritePrivateProfileString(NomeTb, stmp1, stmp2, PathFileSPF)
            Next j
            '
        End Select
        '
      End If
      '
      'SPOSTO IN AVANTI IL PUNTATORE AL RECORD
      TB2.MoveNext
      '
    Loop
    '
  'CHIUDO LA TABELLA DEI PARAMETRI
  TB2.Close
  '
  'CHIUDO IL DATABASE
  DB.Close
  '
Exit Sub

errCREAZIONE_OEM1_FILESPF:
  Write_Dialog "SUB CREAZIONE_OEM1_FILESPF ERROR " & Err
  Resume Next

End Sub

Public Sub CREAZIONE_OEM0_CORRETTORI10()
'
On Error GoTo errCREAZIONE_OEM0_CORRETTORI10
  '
  Open g_chOemPATH & "\" & "OEM0_CORRETTORI10.SPF" For Output As #1
    '
    Print #1, ";PROGRAMMA: " & "OEM0_CORRETTORI10.SPF"
    Print #1, ";VERSIONE : " & Date
    '
    Print #1, "COR[0,0]=0"
    Print #1, "COR[0,1]=0"
    Print #1, "COR[0,2]=0"
    Print #1, "COR[0,3]=0"
    Print #1, "COR[0,4]=0"
    Print #1, "COR[0,5]=0"
    Print #1, "COR[0,6]=0"
    Print #1, "COR[0,7]=0"
    Print #1, "COR[0,8]=0"
    Print #1, "COR[0,9]=0"
    Print #1, "COR[0,10]=0"
    Print #1, "COR[0,11]=0"
    Print #1, "COR[0,12]=0"
    Print #1, "COR[0,13]=0"
    Print #1, "COR[0,14]=0"
    '
    Print #1, "M17 ;Fine"
    '
  Close #1
  '
Exit Sub

errCREAZIONE_OEM0_CORRETTORI10:
  Write_Dialog "SUB CREAZIONE_OEM0_CORRETTORI10 ERROR " & Err
  Resume Next

End Sub

Public Sub CREAZIONE_OEM1_LAVORO10()
'
Dim rsp     As Long
'
Const NomeTb = "OEM1_LAVORO10"
Const Ndec = 4
'
On Error GoTo errCREAZIONE_OEM1_LAVORO10
  '
  'CREAZIONE DEL FILE INI PER SALVARE I DATI ATTUALI
  Call CREAZIONE_OEM1_FILESPF(NomeTb)
  '
  'MOFIDICO IL FILE INI CON I VALORI DEI PARAMETRI IMPOSTATI DALLA PAGINA OEM1_PROFILO10
  rsp = WritePrivateProfileString(NomeTb, "R5", frmt(ParametriCR.Nf, Ndec), PathFileSPF)
  If (ParametriCR.InMola >= 0) Then
    rsp = WritePrivateProfileString(NomeTb, "R206", frmt(Abs(ParametriCR.Paxial), Ndec), PathFileSPF)
  Else
    rsp = WritePrivateProfileString(NomeTb, "R206", frmt(-Abs(ParametriCR.Paxial), Ndec), PathFileSPF)
  End If
  If (ParametriCR.Phel <> 0) Then
    rsp = WritePrivateProfileString(NomeTb, "R207", "1/" & frmt(ParametriCR.Phel, Ndec), PathFileSPF)
  Else
    rsp = WritePrivateProfileString(NomeTb, "R207", "0", PathFileSPF)
  End If
  rsp = WritePrivateProfileString(NomeTb, "R191", frmt(ParametriCR.Z, Ndec), PathFileSPF)
  rsp = WritePrivateProfileString(NomeTb, "R192", frmt(ParametriCR.DestCr, Ndec), PathFileSPF)
  rsp = WritePrivateProfileString(NomeTb, "R193", frmt(ParametriCR.Came, Ndec), PathFileSPF)
  rsp = WritePrivateProfileString(NomeTb, "R126", frmt(ParametriCR.InMola, Ndec), PathFileSPF)
  '
  'CREAZIONE DEL SOTTOPROGRAMMA OEM1_LAVORO10 DAL FILE INI
  Call CREA_SOTTOPROGRAMMA(NomeTb)
  '
Exit Sub

errCREAZIONE_OEM1_LAVORO10:
  Write_Dialog "SUB CREAZIONE_OEM1_LAVORO10 ERROR " & Err
  Resume Next

End Sub

Public Sub ASSEGNAZIONE_FINESTRA_STRUTTURA()
'
'On Error GoTo errASSEGNAZIONE_FINESTRA_STRUTTURA
'  '
'  'Lista1: MODIFICATO PRIMA LEGGEVA LISTA 2
'  ParametriCR.Mr = val(OEM1.List1(1).List(11))          'Modulo
'  ParametriCR.Add = val(OEM1.List1(1).List(12))         'Addendum
'  ParametriCR.Add2 = val(OEM1.List1(1).List(12))        'Addendum
'  ParametriCR.Ep0 = val(OEM1.List1(1).List(13))         'Spessore Prim.
'  ParametriCR.Htotal = val(OEM1.List1(1).List(14))      'Altezza tot
'  '
'  'Lista3 - fianco1
'  ParametriCR.Hst = val(OEM1.List3(1).List(0))          'Stacco semitopping
'  ParametriCR.Apr = val(OEM1.List3(1).List(1))          'Ang. pressione
'  ParametriCR.ApSt = val(OEM1.List3(1).List(2))         'Ang. pres. semitop.
'  ParametriCR.Rtesta = val(OEM1.List3(1).List(3))       'Rt
'  ParametriCR.Rfondo = val(OEM1.List3(1).List(4))       'Rf
'  '
'  RaggioST1 = val(OEM1.List3(1).List(5))                'Rst
'  '
'  ParametriCR.E47 = val(OEM1.List3(1).List(6))          'Stacco protuberanza
'  ParametriCR.EpProtu = val(OEM1.List3(1).List(7))      'Entita prot.
'  ParametriCR.Piattino = val(OEM1.List3(1).List(8))     'Altezza piattino
'  '
'  ParametriCR.HRastp = val(OEM1.List3(1).List(9))       'Stacco rastr. piede
'  ParametriCR.ApRastp = val(OEM1.List3(1).List(10))     'Angolo rastr. piede
'  '
'  ParametriCR.HRastt = val(OEM1.List3(1).List(11))      'Stacco rastr. testa
'  ParametriCR.ApRastt = val(OEM1.List3(1).List(12))     'Angolo rastr. testa
'  '
'  ParametriCR.E46 = 0                                   'Stacco Raggi testa
'  ParametriCR.E61 = 0                                   'Raggio testa 1
'  ParametriCR.E62 = 0                                   'Raggio testa 2
'  ParametriCR.E64 = 0                                   'Lunghezza testa
'  '
'  'Lista3 - fianco2
'  ParametriCR.Hst2 = val(OEM1.List3(2).List(0))         'Stacco semitopping
'  ParametriCR.Apr2 = val(OEM1.List3(2).List(1))         'Ang. pressione
'  ParametriCR.Apst2 = val(OEM1.List3(2).List(2))        'Ang. pres. semitop.
'  '
'  ParametriCR.E54 = val(OEM1.List3(2).List(0))          'Stacco semitopping
'  ParametriCR.E51 = val(OEM1.List3(2).List(1))          'Ang. pressione
'  ParametriCR.E52 = val(OEM1.List3(2).List(2))          'Ang. pres. semitop.
'  ParametriCR.E58 = val(OEM1.List3(2).List(3))          'Rt
'  ParametriCR.E59 = val(OEM1.List3(2).List(4))          'Rf
'  '
'  RaggioST2 = val(OEM1.List3(2).List(5))                'Rst
'  '
'  ParametriCR.E57 = val(OEM1.List3(2).List(6))          'Stacco protuberanza
'  ParametriCR.E55 = val(OEM1.List3(2).List(7))          'Entita prot.
'  ParametriCR.E73 = val(OEM1.List3(2).List(8))          'Altezza piattino
'  '
'  ParametriCR.HRastp2 = val(OEM1.List3(2).List(9))      'Stacco rastr. piede
'  ParametriCR.ApRastp2 = val(OEM1.List3(2).List(10))    'Angolo rastr. piede
'  '
'  ParametriCR.HRastt2 = val(OEM1.List3(2).List(11))     'Stacco rastr. testa
'  ParametriCR.ApRastt2 = val(OEM1.List3(2).List(12))    'Angolo rastr. testa
'  '
'  ParametriCR.E56 = 0                                   'Stacco Raggi testa
'  ParametriCR.E71 = 0                                   'Raggio testa 1
'  ParametriCR.E72 = 0                                   'Raggio testa 2
'  ParametriCR.E74 = 0                                   'Lunghezza testa
'  '
'  'Lista1
'  ParametriCR.Nf = val(OEM1.List1(1).List(0))           'Numeri filetti
'  ParametriCR.Paxial = val(OEM1.List1(1).List(1))       'Passo assiale
'  ParametriCR.Phel = val(OEM1.List1(1).List(2))         'Passo elica
'  ParametriCR.DestCr = val(OEM1.List1(1).List(3))       'Diametro ext CR
'  ParametriCR.Z = val(OEM1.List1(1).List(4))            'Numero scanalature
'  ParametriCR.Came = val(OEM1.List1(1).List(5))         'Camma
'  ParametriCR.DeMola = 2 * val(OEM1.List1(1).List(9))   'Diametro minimo mola
'  '
'  '                    'Senso +1=dx -1=sx               'Inclinazione mola rettifica
'  ParametriCR.InMola = val(OEM1.List1(1).List(7)) * Abs(val(OEM1.List1(1).List(10))) '--- FlagSTD002 05.04.06
'  '
'  'Altri dati
'  '
'  Dim DBB As Database
'  Dim RSS As Recordset
'  '
'  Set DBB = OpenDatabase(PathDB, True, False)
'  '
'  'E37 da tabella mola
'  Set RSS = DBB.OpenRecordset("OEM1_MOLA10")
'  RSS.Index = "INDICE"
'  RSS.Seek "=", 1
'  E37 = val(RSS.Fields("ACTUAL_1").Value)
'  RSS.Close
'  '
'  DBB.Close
'  '
'  ParametriCR.CalcAx = "N"
'  ParametriCR.INCAX1 = 0
'  '
'Exit Sub
'
'errASSEGNAZIONE_FINESTRA_STRUTTURA:
'  Write_Dialog "SUB ASSEGNAZIONE_FINESTRA_STRUTTURA ERROR " & Err
'  Resume Next
'
End Sub

Public Sub CALCOLO_PROFILO_MOLA_CREATORE()
'
Dim DXSX        As Integer
Dim HMRastt1    As Double
Dim ApMRastt1   As Double
Dim HMRastp1    As Double
Dim ApMRastp1   As Double
Dim BombMRastt1 As Double
Dim HMRastt2    As Double
Dim ApMRastt2   As Double
Dim BombMRastt2 As Double
Dim BombMRastp1 As Double
Dim HMRastp2    As Double
Dim ApMRastp2   As Double
Dim BombMRastp2 As Double
Dim DECALAGE    As Double
Dim DP          As Double
Dim E105        As Double
'
On Error GoTo errCALCOLO_PROFILO_MOLA_CREATORE
  '
  DXSX = Abs(InMola) / InMola    '  DX = +1 , SX = -1
  '
  Paxial = Abs(Paxial)
  InMola = Abs(InMola)
  '
  ' SE DXSX=1  ALLORA FIANCO DESTRO=F1
  '
  ' FIN DES DONNEES
  '
  If (Abs(Mr * PG * Nf / Paxial) > 1) Or (Mr = 0) Then
    '
    StopRegieEvents
    MsgBox ("WARNING: NOT POSSIBLE !! -- Check following data: PITCH e MODUL !!")
    ResumeRegieEvents
    '
    Exit Sub
    '
  End If
  '
  InFilet = Acos(Mr * PG * Nf / Paxial)
  Ent = DestCr / 2 + DeMola / 2 ' ENTRAXE MEULE-FRAIS
  DP = Mr * Nf / Sin(InFilet)
  Rp = DP / 2
  SAILLIE = (DestCr - DP) / 2
  ApaFl = Atn(Tan(Apr) / Sin(InFilet))
  Dbfl = DP * Cos(ApaFl)
  InbFl = Atn(PG * Dbfl / Paxial)
  '
  EpFl = PG * Mr - Ep0 + (SAILLIE - Add) * Tan(Apr) * 2
  EbDbFl = EpFl / Sin(InFilet) / DP + FnInv(ApaFl)
  '
  ' DATI DEL CREATORE CALCOLATI
  '
  Dim stmp As String
  '
  stmp = "Passo assiale  :" & Paxial & vbCrLf
  stmp = stmp + "Diam.primitivo :" & DP & vbCrLf
  stmp = stmp + "Incl.Creatore  :" & Fnrd(InFilet) & vbCrLf
  stmp = stmp + vbCrLf & "PIGNONE EQUIVALENTE :" & vbCrLf
  stmp = stmp + "Dia.di base :" & Dbfl & vbCrLf
  stmp = stmp + "Hel.di base :" & Fnrd(InbFl) & vbCrLf
  'MsgBox(stmp)
  '
  If (HRastt > 0) Then
    '
    ApArastt = Atn(Tan(ApRastt) / Sin(InFilet))
    DbRastt = DP * Cos(ApArastt)
    InbRastt = Atn(PG * DbRastt / Paxial)
    Ip = Ep0 + (Add - HRastt) * (Tan(Apr) - Tan(ApRastt)) * 2
    EpRastt = PG * Mr - Ip + (SAILLIE - Add) * Tan(ApRastt) * 2
    EbDbRastt = EpRastt / Sin(InFilet) / DP + FnInv(ApArastt)
    '
  Else
    '
    ApArastt = 0
    DbRastt = 0
    InbRastt = 0
    Ip = 0
    EpRastt = 0
    EbDbRastt = 0
    '
  End If
  '
  If (HRastp > 0) Then
    '
    ApaRastp = Atn(Tan(ApRastp) / Sin(InFilet))
    DbRastp = DP * Cos(ApaRastp)
    InbRastp = Atn(PG * DbRastp / Paxial)
    Ip = Ep0 + (HRastp - Add) * (Tan(ApRastp) - Tan(Apr)) * 2
    EpRastp = PG * Mr - Ip + (SAILLIE - Add) * Tan(ApRastp) * 2
    EbDbRastp = EpRastp / Sin(InFilet) / DP + FnInv(ApaRastp)
    '
  Else
    '
    ApaRastp = 0
    DbRastp = 0
    InbRastp = 0
    Ip = 0
    EpRastp = 0
    EbDbRastp = 0
    '
  End If
  '
  SP = Rtesta * (1 - Sin(Apr))
  DmaxiApr = DestCr - SP * 2 ' Premiere approximation
  a = Atn(Tan(Acos(Dbfl / DmaxiApr)) / (PG * DmaxiApr / Paxial))
  SP = Rtesta * (1 - Sin(a))
  DmaxiApr = DestCr - SP * 2
  '
  If (Hprotu > 0) Then DmaxiApr = DestCr - Hprotu * 2
  If (HRastt > 0) Then DmaxiApr = DestCr - HRastt * 2
  '
  If (ApSt = 0) Then
    DminiApr = DestCr - (Hst - Rfondo * (1 - Sin(Apr))) * 2
  Else
    DminiApr = DestCr - Hst * 2
  End If
  If (DminiApr < Dbfl) Then DminiApr = Dbfl + 0.01
  a = Atn(Tan(Acos(Dbfl / DminiApr)) / (PG * DminiApr / Paxial)) ' A.P. axial
  If (ApSt = 0) Then
    DminiApr = DestCr - (Hst - Rfondo * (1 - Sin(a))) * 2
  Else
    DminiApr = DestCr - Hst * 2
  End If
  If (DminiApr < Dbfl) Then DminiApr = Dbfl + 0.001
  '
  If (HRastp > 0) Then DminiApr = DestCr - HRastp * 2
  '
  ' CALCOLO PIANO ASSIALE
  '
  For i = 1 To 5
    dx(i) = DmaxiApr - (DmaxiApr - DminiApr) * (i - 1) / (5 - 1)
    DB = Dbfl: EbDb = EbDbFl
    Call CalculAxialVis
    If (i = 5) And (ApSt > 0) Then
      Phi(5) = Atn(Tan(ApSt) / Cos(InFilet))
    End If
    R = dx(i) / 2
    E = -VX(i)
    Call CalculRaxialCr
    RG(i) = Rax: RAPH(i) = RAPH0
    E = VX(i)
    Call CalculRaxialCr
    Rd(i) = Rax
  Next i
  '
  '  Testa : Punti 10 , 11 , 12 , 13 + Rastremazione
  '
  ' Rastremazione di testa
  If (HRastt > 0) Then
    '
    DB = DbRastt: EbDb = EbDbRastt
    Dmini = DmaxiApr
    If (Hprotu = 0) Then
      Dmaxi = DestCr - Rtesta * (1 - Sin(Apr)) * 2
      a = Atn(Tan(Acos(DB / Dmaxi)) / (PG * Dmaxi / Paxial))
      Dmaxi = DestCr - Rtesta * (1 - Sin(a)) * 2
    Else
      Dmaxi = DestCr - Hprotu * 2
    End If
    i = 7: dx(i) = Dmaxi: CalculAxialVis
    i = 6: dx(i) = (Dmini + Dmaxi) / 2: CalculAxialVis
    '
  Else
    '
    Phi(6) = Phi(1): VX(6) = VX(1): RX(6) = RX(1): dx(6) = RX(6) * 2
    Phi(7) = Phi(1): VX(7) = VX(1): RX(7) = RX(1): dx(7) = RX(7) * 2
    '
  End If
  '
  Phi(10) = PG / 2: VX(10) = Paxial / 2 / Nf: dx(10) = DestCr: RX(10) = DestCr / 2
  If (Hprotu = 0) Then
    '
    Phi(13) = Phi(7): VX(13) = VX(7): RX(13) = RX(7)
    '
  Else
    '
    RX(13) = DestCr / 2 - Rtesta * (1 - Sin(Phi(7))) - Piattino
    VX(13) = VX(7) + (RX(13) - RX(7)) * Tan(Phi(7)) - EpProtu / Cos(Apr)
    Phi(13) = Phi(7) - Atn(EpProtu / (RX(13) - RX(7)))
    '
  End If
  dx(13) = RX(13) * 2
  Phi(12) = Phi(7): RX(12) = RX(13) + Piattino: dx(12) = RX(12) * 2
  VX(12) = VX(13) + Piattino * Tan(Phi(7))
  dx(11) = DestCr: RX(11) = DestCr / 2: Phi(11) = PG / 2
  VX(11) = VX(12) + Rtesta * Cos(Phi(7))
  '
  ' Piede : Punti 9 , 14 , 15 , 16 + Rastremazione
  '
  ' Rastremazione di piede
  '
  If (HRastp > 0) Then
    '
    DB = DbRastp: EbDb = EbDbRastp
    Dmaxi = DminiApr
    If (ApSt = 0) Then
      Dmini = DestCr - (Hst - Rfondo * (1 - Sin(ApRastp))) * 2
      a = Atn(Tan(Acos(DB / Dmini)) / (PG * Dmini / Paxial))
      Dmini = DestCr - (Hst - Rfondo * (1 - Sin(a))) * 2
    Else
      Dmini = DestCr - Hst * 2
    End If
    i = 8: dx(i) = (Dmini + Dmaxi) / 2: CalculAxialVis
    i = 9: dx(i) = Dmini: CalculAxialVis
    '
  Else
    '
    RX(9) = RX(5): VX(9) = VX(5): Phi(9) = Phi(5): dx(9) = RX(9) * 2
    Phi(8) = Phi(5): VX(8) = VX(5): RX(8) = RX(5): dx(8) = RX(8) * 2
    '
  End If
  '
  If (ApSt <> 0) Then
    '
    Phi(14) = Atn(Tan(ApSt) / Cos(InFilet))
    RX(14) = RX(9) - (Htotal - Hst) + Rfondo * (1 - Sin(Phi(14)))
    VX(14) = VX(9) - (RX(9) - RX(14)) * Tan(Phi(14)) / Cos(InFilet)
    '
  Else
    '
    Phi(14) = Phi(9): RX(14) = RX(9): VX(14) = VX(9)
    '
  End If
  '
  dx(14) = RX(14) * 2
  RX(15) = RX(14) - Rfondo * (1 - Sin(Phi(14))): dx(15) = RX(15) * 2
  VX(15) = VX(14) - Rfondo * Cos(Phi(14))
  Phi(15) = PG / 2
  RX(16) = RX(15): VX(16) = 0: Phi(16) = PG / 2: dx(16) = RX(16) * 2
  '
  ' Punti 18 e 19 ( non vengono utilizzati  )
  '
  Phi(18) = (PG / 2 + Apr) / 2
  VX(18) = VX(11) - Rtesta * Sin((PG / 2 - Apr) / 2) / Cos(InFilet)
  RX(18) = RX(11) - Rtesta * (1 - Cos((PG / 2 - Apr) / 2)) / Cos(InFilet)
  dx(18) = RX(18) * 2
  Phi(19) = (PG / 2 + Apr) / 2
  VX(19) = VX(15) + Rfondo * Sin((PG / 2 - Apr) / 2)
  RX(19) = RX(15) + Rfondo * (1 - Cos((PG / 2 - Apr) / 2))
  If ApSt > 0 Then
    Phi(19) = (PG / 2 + ApSt) / 2
    VX(19) = VX(15) + Rfondo * Sin((PG / 2 - ApSt) / 2)
    RX(19) = RX(15) + Rfondo * (1 - Cos((PG / 2 - ApSt) / 2))
  End If
  dx(19) = RX(19) * 2
  '
  ' Punto 17 = Primitivo di cotation
  '
  i = 17
  dx(i) = DestCr - 2 * Add
  DB = Dbfl: EbDb = EbDbFl
  Call CalculAxialVis
  '
  For i = 6 To 17
    '
    R = RX(i)
    E = -VX(i)  ' Fianco sinistro
    Call CalculRaxialCr
    RG(i) = Rax
    '
    RAPH(i) = RAPH0
    E = VX(i)   ' Fianco destro
    Call CalculRaxialCr
    Rd(i) = Rax
    '
  Next i
  '
  ' RICERCA DminiApr   ( Senza Semi-Topping ne Rastremazione piede)****
  '
  If (ApSt = 0) And (ApRastp = 0) Then
    '
    ' 1) Calcolo RMmaxi
    ' 2) Calcolo DminiApr
    '
    'CALCOLO MOLA PUNTO 16 G
    j = 16
    '
    'CALCOLO MOLA PUNTO J
    RxP(1) = RG(j)
    Phi0 = Atn(Tan(Phi(j)) / RAPH(j))
    Oxp(1) = VX(j) + DECALAGE
    Sens = 1
    D = dx(j)
    Call FunMEULE   'GoSub 1780
    RMG(j) = RMola: XMG(j) = XMola
    a = Atn(Tan(Acos(Dbfl / DminiApr)) / (PG * DminiApr / Paxial))
    RMmaxi = RMG(16) - Rfondo * (1 - Sin(a))
    '
    'LOOP CALCOLO DminiApr (Punto 5)
    i = 0: dx(i) = dx(5 - 1)
    DRMOL = 0
    While (Abs(DRMOL) > 0.01) Or (DRMOL = 0)
      dx(i) = dx(i) - DRMOL
      DB = Dbfl: EbDb = EbDbFl
      Call CalculAxialVis
      R = dx(i) / 2: E = -VX(i)
      Call CalculRaxialCr
      RG(i) = Rax: RAPH(i) = RAPH0
      E = VX(i)
      Call CalculRaxialCr
      Rd(i) = Rax
      RxP(1) = RG(i)
      Phi0 = Atn(Tan(Phi(i)) / RAPH(i))
      Oxp(1) = VX(i) + DECALAGE
      Sens = 1
      D = dx(i)
      Call FunMEULE   'GoSub 1780
      RMG(i) = RMola: XMG(i) = XMola
      'Debug.WriteLine(Format(DX(I), "###.###") & "  " & Format(RMola, "###.###") & "  " & Format(DRMOL, "###.###")) '  ###.###   ###.###"; DX(I); RMola; DRMOL
      DRMOL = (RMmaxi - RMola) * 1.5
    Wend
    DminiApr = dx(i)
  End If
  '
  For i = 1 To 5
    '
    dx(i) = DmaxiApr - (DmaxiApr - DminiApr) * (i - 1) / (5 - 1)
    DB = Dbfl: EbDb = EbDbFl
    Call CalculAxialVis
    If (i = 5) And (ApSt > 0) Then
      Phi(5) = Atn(Tan(ApSt) / Cos(InFilet))
    End If
    R = dx(i) / 2: E = -VX(i)
    Call CalculRaxialCr
    RG(i) = Rax: RAPH(i) = RAPH0
    E = VX(i)
    Call CalculRaxialCr
    Rd(i) = Rax
    '
  Next i
  '
  'vis equivalente
  Apx = Atn((Ep(5) - Ep(1)) / (RX(1) - RX(5)))
  BOMB = (Ep(3) - (Ep(5) + Ep(1)) / 2) * Cos(Apx)
  'LOCATE(9, 45) : Print("PIANO ASSIALE - VITE EQUIVALENTE")
  'LOCATE 10, 45: Print USING; "tra i diametri ###.### e ###.###"; DmaxiApr; DminiApr
  'LOCATE 11, 45: Print "Angolo di pressione :"; Fnrd(Apx)
  'LOCATE 12, 45: Print "Bombatura:  "; Int(BOMB * 1000); " microns"
  '
  '                                             F.M. : PALPAGE  A.P. AXIAL
  APG = Atn((Ep(5) - Ep(1)) / (RG(1) - RG(5)))
  BombG = (Ep(3) - Ep(1) - (RG(1) - RG(3)) * Tan(APG)) * Cos(APG)
  '
  APD = Atn((Ep(5) - Ep(1)) / (Rd(1) - Rd(5)))
  BombD = (Ep(3) - Ep(1) - (Rd(1) - Rd(3)) * Tan(APD)) * Cos(APD)
  '
  ' PROFIL MEULE
  '
  For j = 1 To 17
    '
    ' Flanc gauche pour filet a droite
    RxP(1) = RG(j)
    Phi0 = Atn(Tan(Phi(j)) / RAPH(j))
    Oxp(1) = VX(j) + DECALAGE
    Sens = 1
    D = dx(j)
    Call FunMEULE  'GoSub 1780
    RMG(j) = RMola: XMG(j) = XMola
    '
    ' Flanc droit pour filet a droite
    RxP(1) = Rd(j)
    Sens = -1
    Oxp(1) = VX(j) - DECALAGE
    D = dx(j)
    Call FunMEULE  'GoSub 1780
    RMD(j) = RMola: XMD(j) = XMola
    '
  Next j
  '
  APMG = Atn((XMG(1) - XMG(5)) / (RMG(5) - RMG(1)))
  BombMG = Int((XMG(3) - ((RMG(5) - RMG(3)) * Tan(APMG) + XMG(5))) * 1000)
  APMD = Atn((XMD(5) - XMD(1)) / (RMD(5) - RMD(1)))
  BombMD = Int((-XMD(3) - ((RMD(5) - RMD(3)) * Tan(APMD) - XMD(5))) * 1000)
  '
  If (HRastt > 0) Then
    '
    HMrasttG = RMG(1) - RMG(10)
    HMrasttD = RMD(1) - RMD(10)
    APMRasttG = Atn((XMG(7) - XMG(1)) / (RMG(1) - RMG(7)))
    BombMrasttG = Int((XMG(6) - ((RMG(1) - RMG(6)) * Tan(APMRasttG) + XMG(1))) * 1000)
    APMRasttD = Atn((XMD(1) - XMD(7)) / (RMD(1) - RMD(7)))
    BombMrasttD = Int((-XMD(6) - ((RMD(1) - RMD(6)) * Tan(APMRasttD) - XMD(1))) * 1000)
    '
  Else
    '
    HMrasttG = 0
    HMrasttD = 0
    APMRasttG = 0
    BombMrasttG = 0
    APMRasttD = 0
    BombMrasttD = 0
    '
  End If
  '
  If (HRastp > 0) Then
    '
    HMrastpG = RMG(5) - RMG(10)
    HMrastpD = RMD(5) - RMD(10)
    APMRastpG = Atn((XMG(5) - XMG(9)) / (RMG(9) - RMG(5)))
    BombMrastpG = Int((XMG(8) - ((RMG(9) - RMG(8)) * Tan(APMRastpG) + XMG(9))) * 1000)
    APMRastpD = Atn((XMD(9) - XMD(5)) / (RMD(9) - RMD(5)))
    BombMrastpD = Int((-XMD(8) - ((RMD(9) - RMD(8)) * Tan(APMRastpD) - XMD(9))) * 1000)
    '
  Else
    '
    HMrastpG = 0
    HMrastpD = 0
    APMRastpG = 0
    BombMrastpG = 0
    APMRastpD = 0
    BombMrastpD = 0
    '
  End If
  '
  ' Position du Point maxi du Bomb� sur la vis equivalente
  DX0 = Dbfl: PasDx = 1
  '
VarDx:
  '
  DX0 = DX0 + PasDx
  ApApp = Acos(Dbfl / DX0)
  Apdx = Atn((Tan(ApApp) / DX0) * Paxial / PG)
  Apdx = Atn(Tan(ApApp) / (PG * DX0 / Paxial)) ' A.P. axial sur DX
  If Apdx < Apx Then GoTo VarDx
  DX0 = DX0 - PasDx: PasDx = PasDx / 2
  If PasDx > 0.005 Then GoTo VarDx
  '
  'PRINT : PRINT "Position du bomb� par rapport au diametre actif de tete :";
  posBomD = Int((DmaxiApr - DX0) / (DmaxiApr - DminiApr) * 100 + 0.5) / 100
  If DX0 > DmaxiApr Then posBomD = 0.2
  If DX0 < DminiApr Then posBomD = 0.8
  '
  posBomG = posBomD
  '
  If (posBomG > 0.49) And (posBomG < 0.51) Then posBomG = 0.51
  If (posBomD > 0.49) And (posBomD < 0.51) Then posBomD = 0.51
  '
  ' CALCOLO PARAMETRI MOLA
  '
  RMprim = (RMD(17) + RMG(17)) / 2
  INCAX2 = 0
  '
  ' FIANCO SINISTRO
  '
  P1G = Fnrd(APMG) + INCAX2 * 180 / PG       ' Angolo pressione
  P2G = 0
  If (ApSt > 0) Then P2G = Abs(Atn((XMG(14) - XMG(9)) / (RMG(9) - RMG(14))) + INCAX2) * 180 / PG
  If Abs(XMG(10)) > Abs(XMG(11)) Then
    P3G = RMprim - RMG(11)
    P4G = RMG(9) - RMG(11)
    If (ApSt = 0) Then P4G = RMG(16) - RMG(11)
  Else
    P3G = RMprim - RMG(10)
    P4G = RMG(9) - RMG(10)
    If ApSt = 0 Then P4G = RMG(16) - RMG(10)
  End If
  E39 = 0
  If (ApSt > 0) Then
    E39 = (RMG(16) - RMG(10) + RMD(16) - RMD(10)) / 2
    E106D = (RMD(16) - RMD(10)) - E39
    E106G = (RMG(16) - RMG(10)) - E39
  End If
  P7G = 0: If EpProtu > 0 Then P7G = RMG(1) - RMG(10)
  P8G = 0: If Piattino > 0 Then P8G = RMG(13) - RMG(12)
  '
  ' FIANCO DESTRO
  '
  P1D = Fnrd(APMD) + INCAX2 * 180 / PG ' Angolo pressione
  P2D = 0: If ApSt > 0 Then P2D = Abs(Atn((XMD(14) - XMD(9)) / (RMD(9) - RMD(14))) + INCAX2) * 180 / PG
  If (Abs(XMD(10)) > Abs(XMD(11))) Then
    P3D = RMprim - RMD(11)
    P4D = RMD(9) - RMD(11)
    If ApSt = 0 Then P4D = RMD(16) - RMD(11)
  Else
    P3D = RMprim - RMD(10)
    P4D = RMD(9) - RMD(10)
    If (ApSt = 0) Then P4D = RMD(16) - RMD(10)
  End If
  P5D = 0: If ApSt > 0 Then P5D = RMD(16) - RMD(10)
  P7D = 0: If EpProtu > 0 Then P7D = RMD(1) - RMD(10)
  P8D = 0: If Piattino > 0 Then P8D = RMD(13) - RMD(12)
  '
  E206 = Paxial * DXSX
  E207 = Phel
  E5 = Nf
  E191 = Z
  E192 = DestCr
  E193 = Came
  E38 = Abs(XMG(17) - XMD(17))
  If Abs(XMG(10)) > Abs(XMG(11)) Then
    DT1 = Abs(XMG(11))
  Else
    DT1 = Abs(XMG(10))
  End If
  If Abs(XMD(10)) > Abs(XMD(11)) Then
    DT2 = Abs(XMD(11))
  Else
    DT2 = Abs(XMD(10))
  End If
  ATG = 0: ATD = 0
  If XMG(10) <> XMG(11) Then ATG = -Atn((RMG(10) - RMG(11)) / (XMG(10) - XMG(11))) * 180 / PG
  If XMD(10) <> XMD(11) Then ATD = -Atn((RMD(10) - RMD(11)) / (XMD(11) - XMD(10))) * 180 / PG
  '
  If (DXSX = 1) Then
    E60 = Atn((RMG(15) - RMD(15)) / Abs(XMG(15) - XMD(15))) * 180 / PG
  Else
    E60 = Atn((RMD(15) - RMG(15)) / Abs(XMG(15) - XMD(15))) * 180 / PG
  End If
  '
  If (DXSX = 1) Then     'Fianco DX=F1
    '
    E41 = P1D
    E42 = P2D
    E43 = P3D
    E44 = P4D
    E47 = P7D
    ' E48 = RTD
    E31 = BombMD / 1000
    E40 = ATD
    E63 = P8D
    E64 = DT2
    E65 = posBomD
    '
    E51 = P1G
    E52 = P2G
    E53 = P3G
    E54 = P4G
    E57 = P7G
    'E58 = RTG
    E32 = BombMG / 1000
    E50 = ATG
    E73 = P8G
    E74 = DT1
    E75 = posBomG
    E106 = E106D
    E107 = E106G
    '
    HMRastt1 = HMrasttD
    ApMRastt1 = APMRasttD
    BombMRastt1 = BombMrasttD
    HMRastt2 = HMrasttG
    ApMRastt2 = APMRasttG
    BombMRastt2 = BombMrasttG
    HMRastp1 = HMrastpD
    ApMRastp1 = APMRastpD
    BombMRastp1 = BombMrastpD
    HMRastp2 = HMrastpG
    ApMRastp2 = APMRastpG
    BombMRastp2 = BombMrastpG
    '
  Else  'Fianco SX=F1
    '
    E41 = P1G
    E42 = P2G
    E43 = P3G
    E44 = P4G
    E47 = P7G
    'E48 = RTG
    E31 = BombMG / 1000
    E40 = ATG
    E63 = P8G
    E64 = DT1
    E65 = posBomG
    E51 = P1D
    E52 = P2D
    E53 = P3D
    E54 = P4D
    E57 = P7D
    'E58 = RTD
    E32 = BombMD / 1000
    E50 = ATD
    E73 = P8D
    E74 = DT2
    E75 = posBomD
    E106 = E106G
    E107 = E106D
    '
    HMRastt1 = HMrasttG
    ApMRastt1 = APMRasttG
    BombMRastt1 = BombMrasttG
    HMRastt2 = HMrasttD
    ApMRastt2 = APMRasttD
    BombMRastt2 = BombMrasttD
    HMRastp1 = HMrastpG
    ApMRastp1 = APMRastpG
    BombMRastp1 = BombMrastpG
    HMRastp2 = HMrastpD
    ApMRastp2 = APMRastpD
    BombMRastp2 = BombMrastpD
    '
  End If
  '
  'Apro Database
  '
  Dim DBB As Database
  Dim RSS As Recordset
  '
  Set DBB = OpenDatabase(PathDB, True, False)
  '
  'Leggo E105 da tabella mola (par. Inversione asse pezzo)
  '
  Set RSS = DBB.OpenRecordset("OEM1_LAVORO10")
  RSS.Index = "INDICE"
  RSS.Seek "=", 10
  E105 = val(RSS.Fields("ACTUAL_1").Value)
  RSS.Close
  '
  DBB.Close
  '
  ' MODIFICA 10/10/97
  If (VX(15) < 0.1) Then E60 = (E40 - E50) / 2
  '
  'ASSEGNAZIONE DA VARIABILI A STRUTTURA DEL PROFILO MOLA
  ParametriMola.E206 = E206
  ParametriMola.E207 = E207
  ParametriMola.E5 = E5
  ParametriMola.E191 = E191
  ParametriMola.E192 = E192
  ParametriMola.E193 = E193
  '
  '--- FlagSTD001 05.04.06 ---
  If E37 < 16 Then
    E37 = E64 + E74 + 2
    If E37 < 16 Then E37 = 16
    If E37 > 16 And E37 < 19 Then E37 = 20
    If E37 > 19 And E37 < 21 Then E37 = 22
    If E37 > 21 Then E37 = 27
  End If
  '--- ---- ---- ---- ---- ---
  ParametriMola.E37 = E37
  ParametriMola.E38 = E38
  ParametriMola.E39 = E39
  '
  If (E105 = 1) Then
    '
    'Assegnazione convenzionale dei fianchi (E105=1)
    'Fianco 1
    ParametriMola.E41 = E41
    ParametriMola.E42 = E42
    ParametriMola.E43 = E43
    ParametriMola.E44 = E44
    ParametriMola.E45 = E45
    ParametriMola.E46 = E46
    ParametriMola.E47 = E47
    ParametriMola.E48 = E48
    ParametriMola.E49 = E49
    'Fianco 2
    ParametriMola.E51 = E51
    ParametriMola.E52 = E52
    ParametriMola.E53 = E53
    ParametriMola.E54 = E54
    ParametriMola.E55 = E55
    ParametriMola.E56 = E56
    ParametriMola.E57 = E57
    ParametriMola.E58 = E58
    ParametriMola.E59 = E59
    'Bomb. F1/F2
    ParametriMola.E31 = E31
    ParametriMola.E32 = E32
    'Ang. Testa F1/F2
    ParametriMola.E40 = E40
    ParametriMola.E50 = E50
    'Ang. Fondo
    ParametriMola.E60 = E60
    'Fianco 1
    ParametriMola.E61 = E61
    ParametriMola.E62 = E62
    ParametriMola.E63 = E63
    ParametriMola.E64 = E64
    ParametriMola.E65 = E65
    'Fianco 2
    ParametriMola.E71 = E71
    ParametriMola.E72 = E72
    ParametriMola.E73 = E73
    ParametriMola.E74 = E74
    ParametriMola.E75 = E75
    'Corr. alt. totale F1/F2
    ParametriMola.E106 = E106
    ParametriMola.E107 = E107
    'Fianco 1
    ParametriMola.HMRastt1 = HMRastt1
    ParametriMola.ApMRastt1 = Fnrd(ApMRastt1)
    ParametriMola.BombMRastt1 = BombMRastt1 / 1000
    ParametriMola.HMRastp1 = HMRastp1
    ParametriMola.ApMRastp1 = Fnrd(ApMRastp1)
    ParametriMola.BombMRastp1 = BombMRastp1 / 1000
    'Fianco 2
    ParametriMola.HMRastt2 = HMRastt2
    ParametriMola.ApMRastt2 = Fnrd(ApMRastt2)
    ParametriMola.BombMRastt2 = BombMRastt2 / 1000
    ParametriMola.HMRastp2 = HMRastp2
    ParametriMola.ApMRastp2 = Fnrd(ApMRastp2)
    ParametriMola.BombMRastp2 = BombMRastp2 / 1000
    '
  Else
    '
    'Inversione fianchi in funzione del parametro E105
    'Fianco 1
    ParametriMola.E41 = E51
    ParametriMola.E42 = E52
    ParametriMola.E43 = E53
    ParametriMola.E44 = E54
    ParametriMola.E45 = E55
    ParametriMola.E46 = E56
    ParametriMola.E47 = E57
    ParametriMola.E48 = E58
    ParametriMola.E49 = E59
    'Fianco 2
    ParametriMola.E51 = E41
    ParametriMola.E52 = E42
    ParametriMola.E53 = E43
    ParametriMola.E54 = E44
    ParametriMola.E55 = E45
    ParametriMola.E56 = E46
    ParametriMola.E57 = E47
    ParametriMola.E58 = E48
    ParametriMola.E59 = E49
    'Bomb. F1/F2
    ParametriMola.E31 = E32
    ParametriMola.E32 = E31
    'Ang. Testa F1/F2
    ParametriMola.E40 = E50
    ParametriMola.E50 = E40
    'Ang. Fondo
    ParametriMola.E60 = E60 * -1
    'Fianco1
    ParametriMola.E61 = E71
    ParametriMola.E62 = E72
    ParametriMola.E63 = E73
    ParametriMola.E64 = E74
    ParametriMola.E65 = E75
    'Fianco 2
    ParametriMola.E71 = E61
    ParametriMola.E72 = E62
    ParametriMola.E73 = E63
    ParametriMola.E74 = E64
    ParametriMola.E75 = E65
    'Corr. alt. totale F1/F2
    ParametriMola.E106 = E107
    ParametriMola.E107 = E106
    'Fianco 1
    ParametriMola.HMRastt1 = HMRastt2
    ParametriMola.ApMRastt1 = Fnrd(ApMRastt2)
    ParametriMola.BombMRastt1 = BombMRastt2 / 1000
    ParametriMola.HMRastp1 = HMRastp2
    ParametriMola.ApMRastp1 = Fnrd(ApMRastp2)
    ParametriMola.BombMRastp1 = BombMRastp2 / 1000
    'Fianco 2
    ParametriMola.HMRastt2 = HMRastt1
    ParametriMola.ApMRastt2 = Fnrd(ApMRastt1)
    ParametriMola.BombMRastt2 = BombMRastt1 / 1000
    ParametriMola.HMRastp2 = HMRastp1
    ParametriMola.ApMRastp2 = Fnrd(ApMRastp1)
    ParametriMola.BombMRastp2 = BombMRastp1 / 1000
    '
  End If
  '
  Exit Sub
  '
errCALCOLO_PROFILO_MOLA_CREATORE:
  Write_Dialog "Sub CALCOLO_PROFILO_MOLA_CREATORE: ERROR -> " & Err
  Exit Sub

End Sub
'
'1780   MEULE
'
Private Sub FunMEULE()
'
On Error GoTo errFunMEULE
  '
  INCAX = INCAX1 * Sens
  CSINCAX = Cos(INCAX): SNINCAX = Sin(INCAX): TGINCAX = SNINCAX / CSINCAX
  '
  If (INCAX1 = 0) Then
    '
    If (Sens = 1) Then Flanc$ = "G" Else Flanc$ = "D"
    '
    R = D / 2: SxP = Oxp(1): ApAxial = Phi0
    Call Mola_Stev(R, SxP, ApAxial, XMola, RMola, Apmola, Flanc$)
    '
    If (Flanc$ = "G") Then XMola = -XMola
    '
    If (CalcAx$ = "S") Then
      XMA = XMola / Cos(InMola)
      RMA = Sqr(RMola ^ 2 - (XMola * Tan(InMola)) ^ 2)
      RMola = RMA: XMola = XMA  'ASSIALE
    End If
    '
  Else
    '
    Phi0 = Fnrd(Phi0): If Abs(Phi0) > 89 Then Phi0 = 89
    PHIA = Fndr(Phi0 * Sens): Oxp(1) = Oxp(1) * Sens
    RxP(0) = RxP(1) + 0.1 * Cos(PHIA): Oxp(0) = Oxp(1) + 0.1 * Sin(PHIA)
    RxP(2) = RxP(1) - 0.1 * Cos(PHIA): Oxp(2) = Oxp(1) - 0.1 * Sin(PHIA)
    PHIA = (PG / 4) * Sens
    For Passage = 1 To 2
      '
      For i = 0 To 2 Step 2
        Rp = RxP(i)
        OP = Oxp(i)
        Call FunCotesMeule   'GoSub 1950
        RM(i) = RMola: XM(i) = XMola
      Next i
      APM = Atn((XM(0) - XM(2)) / (RM(2) - RM(0))) '         A.P. meule
      If Sens = 1 And APM < 0 Then APM = PG + APM
      If Sens = -1 And APM > 0 Then APM = -PG + APM
      PHIA = APM
      '
    Next Passage
    PHIA = APM
    Rp = RxP(1)
    OP = Oxp(1)
    Call FunCotesMeule    'GoSub 1950
    '
  End If
  '
  XM1 = XMola * CSINCAX + RMola * SNINCAX ' pour ramener plan 0 + INCAX
  RMola = RMola * CSINCAX - XMola * SNINCAX: XMola = XM1  ' ----------
  '
Exit Sub

errFunMEULE:
  Write_Dialog "SUB FunMEULE: ERROR " & Err
Resume Next

End Sub
'
'1950   cotes meule  pour un rayon considere
'
Private Sub FunCotesMeule()
'
Dim IM        As Double
Dim IM1       As Double
Dim AAA       As Double
Dim PTETA     As Double
Dim RM1       As Double
Dim XMORIG    As Double
Dim TETAORIG  As Double
Dim APB       As Double
'
On Error GoTo errFunCotesMeule
        '
        IM = InMola: IM1 = Atn((Paxial / PG) / (Rp * 2)): AAA = IM - IM1
        If (Abs(AAA) < 0.0004) Then IM = IM1 + 0.0004 * Sgn(AAA)
        '
        CSIM = Cos(IM): SNIM = Sin(IM): TGIM = SNIM / CSIM
        '
        ' recherche de tetap2 pour obtenir rmmini ( apm=90 )
        PTETA = 0.01: TETAP2 = 0
        '
        Call FunCalXRMola 'GoSub 2150
        '
        RM1 = RMola
        '
2020:   TETAP2 = TETAP2 + PTETA
        '
        Call FunCalXRMola 'GoSub 2150
        '
        If RMola < RM1 Then RM1 = RMola: GoTo 2020
        RM1 = RMola: If Abs(PTETA) > 0.001 Then PTETA = PTETA / -2: GoTo 2020
        XMORIG = XMola: TETAORIG = TETAP2
        TETAP2 = TETAORIG + 0.0001
        '
        Call FunCalXRMola 'GoSub 2150
        '
        TETAP2 = TETAORIG: If XMola * Sens < XMORIG * Sens Then PTETA = 0.01 Else PTETA = -0.01
        '
2080:   Call FunCalXRMola 'GoSub 2150
        '
        RM(1) = RMola: XM(1) = XMola
2090:   TETAP2 = TETAP2 + PTETA
        '
        Call FunCalXRMola 'GoSub 2150
        '
        If RMola = RM(1) Then RM(1) = RMola - 0.00001
        APB = Atn((XM(1) - XMola) / (RMola - RM(1))) '         derivee de la courbe
        '
        If APB * Sens > PHIA * Sens Then RM(1) = RMola: XM(1) = XMola: GoTo 2090
        TETAP2 = TETAP2 - PTETA: If TETAP2 = TETAORIG Then GoTo 2130 Else TETAP2 = TETAP2 - PTETA
2130:   PTETA = PTETA / 2: If Abs(PTETA) > 0.002 Then GoTo 2080
        '
Exit Sub

errFunCotesMeule:
  Write_Dialog "SUB FunCotesMeule: ERROR " & Err
Resume Next

End Sub
'
'calcul Xmola et Rmola en fonction de TETAP2
'
Private Sub FunCalXRMola()
'
Dim C     As Double
Dim EP2   As Double
Dim AP2   As Double
Dim AB    As Double
Dim CP2   As Double
Dim LP2   As Double
Dim DP2   As Double
Dim tDP2  As Double
'
On Error GoTo errFunCalXRMola
  '
  C = Came: If Phel <> 0 Then C = Came * (1 + Paxial / Phel)
  '
  EP2 = Ent - Z * C * TETAP2 / (2 * PG)
  AP2 = Rp * Cos(TETAP2)
  AB = Rp * Sin(TETAP2)
  CP2 = EP2 - AP2
  LP2 = (OP + Paxial * TETAP2 / (2 * PG)) * CSINCAX - CP2 * SNINCAX
  XMola = (LP2 - AB * TGIM) * CSIM
  DP2 = LP2 * SNIM + AB * CSIM
  RMola = Sqr(tDP2 ^ 2 + (CP2 / CSINCAX + LP2 * TGINCAX) ^ 2)
  '
  'XMA = XMola / COS(InMola)
  'RMA = SQR(RMola ^ 2 - (XMola * TAN(InMola)) ^ 2)
  'RMola = RMA: XMola = XMA  'ASSIALE
  '
Exit Sub

errFunCalXRMola:
  Write_Dialog "SUB FunCalXRMola: ERROR " & Err
Resume Next

End Sub

Private Sub CalculAxialVis()
'
On Error GoTo errCalculAxialVis
  '
  ' Rx(I)   RAYON , EP(I)   epaiss  , VX(I) demi interv , PHI(I)  A.P.
  '
  RX(i) = dx(i) / 2
  ApApp = Acos(DB / dx(i))
  Ep(i) = ((EbDb - FnInv(ApApp)) * Paxial / PG) / 2 '   ep axial
  VX(i) = Paxial / Nf / 2 - Ep(i)         ' demi intervalle axial
  Helx = Atn(Paxial / PG / dx(i))
  '
  ' PHI(I) = ATN(TAN(Apapp) * SIN(Helx)): ' A.P. axial sur dx
  Phi(i) = Atn(Tan(Acos(DB / dx(i))) / (PG * dx(i) / Paxial))
  '
Exit Sub

errCalculAxialVis:
  Write_Dialog "SUB CalculAxialVis: ERROR " & Err
Resume Next

End Sub

Private Sub CalculRaxialCr() ' s / p       calcul raxial CR.
'
On Error GoTo errCalculRaxialCr
  '
  R = R + 0.1: Call CalculRaxialCr1
  RAX1 = Rax
  R = R - 0.1: Call CalculRaxialCr1
  '
  RAPH0 = (RAX1 - Rax) / 0.1
  '
Exit Sub

errCalculRaxialCr:
  Write_Dialog "SUB CalculRaxialCr: ERROR " & Err
Resume Next

End Sub

Private Sub CalculRaxialCr1()
'
Dim C     As Double
Dim ALFA  As Double
'
On Error GoTo errCalculRaxialCr1
  '
  ALFA = (2 * PG * E) / (Paxial + Phel)
  If (Phel = 0) Then ALFA = 0
  '
  C = Came: If (Phel <> 0) Then C = Came * (1 + Paxial / Phel)
  '
  Rax = R - Z * C * ALFA / (2 * PG)
  '
Exit Sub

errCalculRaxialCr1:
  Write_Dialog "SUB CalculRaxialCr1: ERROR " & Err
Resume Next

End Sub

Private Sub Mola_Stev(ByRef R As Double, ByRef SxP As Double, ByRef ApAxial As Double, ByRef XMola As Double, ByRef RMola As Double, ByRef Apmola As Double, ByRef Flanc$)
'
Dim KDEL    As Double
Dim INHEL   As Double
Dim InFil   As Double
Dim T       As Double
Dim TAXT    As Double
Dim PSI0    As Double
Dim DIF     As Double
Dim DIFOLD  As Double
Dim PSI     As Double
Dim xx      As Double
Dim YY      As Double
Dim Y       As Double
Dim TAM     As Double
Dim TAXP    As Double
Dim TGEPSI  As Double
Dim TALTET  As Double
Dim GPP     As Double
Dim TDELT   As Double
Dim MU      As Double
'
Dim D       As Double
Dim DP      As Double
'
On Error GoTo errMola_Stev
  '
  InFil = Atn(Paxial / PG / 2 / R)
  '
  If (Phel = 0) Then
    '
    If (Flanc$ = "G") Then KDEL = Came * Z / 2 / PG
    If (Flanc$ = "D") Then KDEL = -Came * Z / 2 / PG
    INHEL = 0
    '
  Else
    '
    If (Flanc$ = "G") Then KDEL = Came * Z * (1 + Paxial / Phel) / 2 / PG
    If (Flanc$ = "D") Then KDEL = -Came * Z * (1 + Paxial / Phel) / 2 / PG
    INHEL = Atn(2 * R * PG / Phel)
    '
  End If
  '
  T = SxP * Cos(InFil) * Sin(INHEL) / Cos(INHEL - InFil): PSI0 = T / R
  TAXT = Tan(ApAxial) / (1 + Tan(INHEL) * Tan(InFil))
  DIF = 10: DIFOLD = 0
  Do While (Abs(DIF) >= 0.001)
    '
    '********************* DIFER ********************************
    TALTET = TAXT / Cos(INHEL)
    GPP = Atn(Paxial / PG / 2 / (R + Sin(PSI) * KDEL))
    TDELT = KDEL * Cos(PSI) / (R + KDEL * Sin(PSI)) * Cos(GPP)
    TAXP = Sin(INHEL) / Cos(GPP) * TDELT + 1 / TALTET
    TAXP = Cos(INHEL - GPP) / Cos(GPP) / TAXP
    TGEPSI = Paxial / PG / 2 - KDEL * Cos(PSI) * TAXP
    TGEPSI = TGEPSI / (R + KDEL * Sin(PSI))
    DIF = Tan(InMola) - TGEPSI * Cos(PSI) + TAXP * Sin(PSI)
    D = Ent + (PSI - PSI0 - MU) * KDEL
    DIF = DIF * (R * Cos(PSI) - D)
    DIF = DIF / (TAXP * Cos(PSI) + TGEPSI * Sin(PSI))
    DIF = DIF - (Paxial / PG / 2 * PSI - SxP) * Tan(InMola) - R * Sin(PSI)
    '************************************************************
    'IF ABS(dif) < .001 THEN EXIT DO
    DP = DIF: PSI = PSI + 0.00001
    '********************* DIFER ********************************
    TALTET = TAXT / Cos(INHEL)
    GPP = Atn(Paxial / PG / 2 / (R + Sin(PSI) * KDEL))
    TDELT = KDEL * Cos(PSI) / (R + KDEL * Sin(PSI)) * Cos(GPP)
    TAXP = Sin(INHEL) / Cos(GPP) * TDELT + 1 / TALTET
    TAXP = Cos(INHEL - GPP) / Cos(GPP) / TAXP
    TGEPSI = Paxial / PG / 2 - KDEL * Cos(PSI) * TAXP
    TGEPSI = TGEPSI / (R + KDEL * Sin(PSI))
    DIF = Tan(InMola) - TGEPSI * Cos(PSI) + TAXP * Sin(PSI)
    D = Ent + (PSI - PSI0 - MU) * KDEL
    DIF = DIF * (R * Cos(PSI) - D)
    DIF = DIF / (TAXP * Cos(PSI) + TGEPSI * Sin(PSI))
    DIF = DIF - (Paxial / PG / 2 * PSI - SxP) * Tan(InMola) - R * Sin(PSI)
    '************************************************************
    PSI = PSI - DIF / (DIF - DP) * 0.00001
    If Abs(Abs(DIFOLD) - Abs(DIF)) < 0.0001 Then Exit Do
    DIFOLD = DIF
    '
  Loop
  '
  XMola = -SxP + PSI * Paxial / PG / 2: Y = -R * Sin(PSI)
  If (Abs(Y) > Abs(XMola)) Then
    xx = Abs(Y): YY = Abs(XMola)
  Else
    xx = Abs(XMola): YY = Abs(Y)
  End If
  YY = Atn(YY / xx)
  If (Abs(Y) > Abs(XMola)) Then YY = PG / 2 - YY
  If (XMola >= 0) And (Y < 0) Then YY = -YY
  If (XMola < 0) And (Y >= 0) Then YY = PG - YY
  If (XMola < 0) And (Y < 0) Then YY = YY - PG
  XMola = Sqr(XMola ^ 2 + Y ^ 2): Y = YY
  xx = Y - InMola
  Y = XMola * Sin(xx): XMola = XMola * Cos(xx)
  RMola = Sqr((D - R * Cos(PSI)) ^ 2 + Y ^ 2)
  '
  TAM = (R * Cos(PSI) - D) / (TAXP * Cos(PSI) + TGEPSI * Sin(PSI))
  TAM = TAM - PSI * Paxial / PG / 2 + SxP
  TAM = -RMola / (TAM / Cos(InMola) + XMola)
  Apmola = Atn(TAM)
  '
Exit Sub

errMola_Stev:
  Write_Dialog "SUB Mola_Stev: ERROR " & Err
  Resume Next

End Sub

Private Function Fndr(ByVal val As Double)
'
On Error Resume Next
  '
  Fndr = val * PG / 180
  '
End Function

Private Function Fnrd(ByVal val As Double)
'
On Error Resume Next
  '
  Fnrd = Int((val * 180 / PG) * 1000 + 0.5) / 1000
  '
End Function

Private Function FnInv(ByVal val As Double)
'
On Error Resume Next
  '
  FnInv = Tan(val) - val
  '
End Function

Private Sub ASSEGNAZIONE_STRUTTURA_VARIABILI(ByRef ParametriCR As StructParametriCR)
'
On Error Resume Next
  '
  Paxial = ParametriCR.Paxial             '  CR  PASSO ASSIALE
  Phel = ParametriCR.Phel                 '  CR  PASSO  ELICA
  Nf = ParametriCR.Nf                     '  CR  NUMERO PRINCIPI
  Z = ParametriCR.Z                       '  CR  NUMERO TAGLIENTI
  DestCr = ParametriCR.DestCr             '  CR  DIAMETRO ESTERNO
  Came = ParametriCR.Came                 '  CR  CAMMA
  DeMola = ParametriCR.DeMola             '  MOLA  DIAMETRO ESTERNO
  '
  Ep0 = ParametriCR.Ep0                   '  SPESSORE MOLA  ( VANO CR PRIM )
  E39 = ParametriCR.Htotal                '  - ALTEZZA TOTALE - - - - - -
  Htotal = E39                            '
  '
  Apr = Fndr(ParametriCR.Apr)             '  - ANGLE DE PRESSION REEL - -
  E42 = Fndr(ParametriCR.ApSt)            '  - ANGOLO SEMITOPPING - - - -
  ApSt = E42                              '
  Add = ParametriCR.Add                   '  - addendum  de cotation
  E44 = ParametriCR.Hst                   '  - STACCO SEMITOPPING- - - -
  Hst = E44                               '
  E45 = ParametriCR.EpProtu               '  - ENTITA PROTUBERANZA - - -
  EpProtu = E45                           '
  E46 = ParametriCR.E46                   '  - STACCO PIATTINO - - - - -
  E47 = ParametriCR.E47                   '  - STACCO PROTUBERANZA - - -
  Hprotu = E47                            '
  E48 = ParametriCR.Rtesta                '  - RAGGIO DI TESTA -  - - - -
  Rtesta = E48                            '
  E49 = ParametriCR.Rfondo                '  - RAGGIO DI FONDO - - - - -
  Rfondo = E49                            '
  'E51 = Fndr(VAL(L$))                    '  - ANGLE DE PRESSION REEL - -
  'E52 = Fndr(VAL(L$))                    '  - ANGOLO SEMITOPPING - - - -
  'E53 = VAL(L$)                          '  - SAILLIE = MODULO norm - -
  'E54 = VAL(L$)                          '  - STACCO SEMITOPPING- - - -
  E55 = ParametriCR.E55                   '  - ENTITA PROTUBERANZA - - -
  E56 = ParametriCR.E56                   '  - STACCO PIATTINO - - - - -
  'E57 = VAL(L$)                          '  - STACCO PROTUBERANZA - - -
  E58 = ParametriCR.E58                   '  - RAGGIO DI TESTA -  - - - -
  E59 = ParametriCR.E59                   '  - RAGGIO DI FONDO - - - - -
  E61 = ParametriCR.E61                   '  -                        - -
  E62 = ParametriCR.E62                   '  -                    - - - -
  E63 = ParametriCR.Piattino              '  - PIATTINO - -
  Piattino = E63                          '
  E64 = ParametriCR.E64                   '  - DISTANZA TESTA  - - -
  E71 = ParametriCR.E71                   '  -                        - -
  E72 = ParametriCR.E72                   '  - ANGOLO SEMITOPPING - - - -
  E73 = ParametriCR.E73                   '  - PIATTINO - -
  E74 = ParametriCR.E74                   '  - DISTANZA TESTA  - - -
  InMola = Fndr(ParametriCR.InMola)       '  - INCL.DELLA MOLA
  INCAX1 = Fndr(ParametriCR.INCAX1)       '  - INCLINAISON ASSE MOLA
  Mr = ParametriCR.Mr                     '  - MODULO REALE - - - - - - -
  '                                       '
  '****** N.B. Da abilitare               '
  CalcAx$ = ParametriCR.CalcAx            '  - S=assiale, N=normale
  HRastt = ParametriCR.HRastt             '  - Altezza rastr. testa F1
  ApRastt = Fndr(ParametriCR.ApRastt)     '  - Angolo  rastr. testa F1
  HRastp = ParametriCR.HRastp             '  - Altezza rastr. piede F1
  ApRastp = Fndr(ParametriCR.ApRastp)     '  - Angolo  rastr. piede F1
  'Hrastt2 = ParametriCR.HRastt2          '  - Altezza rastr. testa F2
  'ApRastt2 = Fndr(ParametriCR.ApRastt2)  '  - Angolo  rastr. testa F2
  'Hrastp2 = ParametriCR.HRastp2          '  - Altezza rastr. piede F2
  'ApRastp2 = Fndr(ParametriCR.ApRastp2)  '  - Angolo  rastr. piede F2
  '
End Sub

Sub ASSEGNAZIONE_STRUTTURA_SOTTOPROGRAMMI()
'
Dim Session        As IMCDomain
Dim DIRETTORIO_WKS As String
Dim SOTTOPROGRAMMA As String
'
On Error Resume Next
  '
  DIRETTORIO_WKS = LEGGI_DIRETTORIO_WKS("CREATORI_STANDARD")
  '
  Set Session = GetObject("@SinHMIMCDomain.MCDomain")
  '
  'CREAZIONE ED INVIO SOTTOPROGRAMMA (1)
  SOTTOPROGRAMMA = "OEM1_LAVORO10"
  Call CREAZIONE_OEM1_LAVORO10
  Session.CopyNC g_chOemPATH & "\" & SOTTOPROGRAMMA & ".SPF", "/NC/WKS.DIR/" & DIRETTORIO_WKS & ".WPD/" & SOTTOPROGRAMMA & ".SPF", MCDOMAIN_COPY_NC
  '
  'CREAZIONE ED INVIO SOTTOPROGRAMMA (2)
  SOTTOPROGRAMMA = "OEM1_MOLA10"
  Call CREAZIONE_OEM1_MOLA10
  Session.CopyNC g_chOemPATH & "\" & SOTTOPROGRAMMA & ".SPF", "/NC/WKS.DIR/" & DIRETTORIO_WKS & ".WPD/" & SOTTOPROGRAMMA & ".SPF", MCDOMAIN_COPY_NC
  '
  'CREAZIONE ED INVIO SOTTOPROGRAMMA (3)
  SOTTOPROGRAMMA = "OEM0_CORRETTORI10"
  Call CREAZIONE_OEM0_CORRETTORI10
  Session.CopyNC g_chOemPATH & "\" & SOTTOPROGRAMMA & ".SPF", "/NC/WKS.DIR/" & DIRETTORIO_WKS & ".WPD/" & SOTTOPROGRAMMA & ".SPF", MCDOMAIN_COPY_NC
  '
  Set Session = Nothing
  '
End Sub

Function Acos(ByVal Aux As Double) As Double
'
Dim Aux1 As Double
'
On Error Resume Next
  '
  Aux1 = (1 - Aux * Aux)
  '
  If (Aux1 < 0) Then
    '
    StopRegieEvents
    MsgBox "Function aCos with argument > 1", vbCritical, "WRONG DATA INPUT!!!"
    ResumeRegieEvents
    '
  Else
    '
    If (Aux = 0) Then
      '
      Acos = PG / 2
      '
    Else
      '
      If (Aux > 0) Then
        '
        Acos = Atn(Sqr(1 - Aux * Aux) / Aux)
        '
      Else
        '
        Aux = Abs(Aux)
        Acos = Atn(Aux / Sqr(1 - Aux * Aux)) + PG / 2
        '
      End If
      '
    End If
    '
  End If
  '
End Function
