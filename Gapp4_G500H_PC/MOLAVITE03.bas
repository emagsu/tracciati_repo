Attribute VB_Name = "MOLAVITE03"
'*********************************************************************
'
' 2014.10.16
'
' Modulo per il caricamento di HGkinetic
'
'*********************************************************************
Option Explicit

Private Type RECT
  Left As Long
  Top As Long
  Right As Long
  Bottom As Long
End Type
'--------------------------
Private Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As Long
Private Declare Function SetParent Lib "user32" (ByVal hWndChild As Long, ByVal hWndNewParent As Long) As Long
Private Declare Function SetWindowPos Lib "user32" (ByVal hwnd As Long, ByVal hWndInsertAfter As Long, ByVal X As Long, ByVal Y As Long, ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long) As Long
Private Declare Function GetClientRect Lib "user32" (ByVal hwnd As Long, lpRect As RECT) As Long
Private Declare Sub Sleep Lib "kernel32.dll" (ByVal dwMilliseconds As Long)
'------------------------------------------------------------------------
Public hndHGkinetic As Long
'------------------------------------------------------------------------
'---------------------------RUN and HGkinetic HGkinetic_XMLpopulate
Sub HGkinetic_cmdOpenWindow() 'HGkinetic_Click0
  Dim lngParent  As Long, R As RECT
  Dim lngChild  As Long, R1 As RECT
  Dim ApPath: ApPath = App.Path
  Dim Ape: Ape = ApPath & "\HGkinetic\HGkinetic.exe"
  Dim ih As Integer: ih = 0
  Dim stmp As String
  Call Shell(Ape, vbNormalFocus)
  
 lngParent = 0
 lngChild = 0
'------------------------------------------------------------------------
Do While (lngChild = 0) ' Or lngParent = 0)
 OEMX.OEM1PicCALCOLO.Cls
 stmp = "HGkinetic: PREPROCESING "
 Select Case (ih)
 Case (0): stmp = stmp & "-"
 Case (1): stmp = stmp & "/"
 Case (2): stmp = stmp & "|"
 Case (3): stmp = stmp & "\"
 Case (4): stmp = stmp & "-"
 Case (5): stmp = stmp & "/": ih = ih - 6
 End Select
 OEMX.OEM1PicCALCOLO.CurrentX = 1
 OEMX.OEM1PicCALCOLO.CurrentY = OEMX.OEM1PicCALCOLO.TextHeight(stmp)
 OEMX.OEM1PicCALCOLO.Print stmp
 lngChild = FindWindow(vbNullString, "Form1") 'Form1
 ih = ih + 1
 DoEvents
 Sleep 15
Loop
  Call GetClientRect(OEMX.OEM1frameCalcolo.hwnd, R1) 'HGkineticprep
  R1.Top = 10: R1.Left = 10: R1.Bottom = 32 'R1.Bottom / 10
  R1.Right = 215 'R1.Right * (1 / 3 + 1 / 8)
  Call SetParent(lngChild, OEMX.OEM1frameCalcolo.hwnd)
  Call SetWindowPos(lngChild, 0&, R1.Left, R1.Top, R1.Right, R1.Bottom, 0)
'------------------------------------------------------------------------
Do While lngParent = 0
 OEMX.OEM1PicCALCOLO.Cls
 stmp = "HGkinetic: LOADING "
 Select Case (ih)
 Case (0): stmp = stmp & "-"
 Case (1): stmp = stmp & "/"
 Case (2): stmp = stmp & "|"
 Case (3): stmp = stmp & "\"
 Case (4): stmp = stmp & "-"
 Case (5): stmp = stmp & "/": ih = ih - 6
 End Select
 OEMX.OEM1PicCALCOLO.CurrentX = 1
 OEMX.OEM1PicCALCOLO.CurrentY = OEMX.OEM1PicCALCOLO.TextHeight(stmp) * 2
 OEMX.OEM1PicCALCOLO.Print stmp
 lngParent = FindWindow(vbNullString, "HGkinetic") 'HGkinetic
 ih = ih + 1
 DoEvents
 Sleep 15
Loop
'-----------------------------------------------------------------
  Call GetClientRect(OEMX.OEM1frameCalcolo.hwnd, R) 'Picture1.hwnd OEMX.OEM1frameCalcolo OEMX.OEM1PicCALCOLO.hwnd
  R.Top = 45 'OEMX.OEM1Combo1.Height / OEMX.ScaleHeight * 2 '40
  R.Bottom = R.Bottom - 45
  Call SetParent(lngParent, OEMX.OEM1frameCalcolo.hwnd)  'Picture1.hwnd OEMX.OEM1PicCALCOLO OEMX.OEM1frameCalcolo
  Call SetWindowPos(lngParent, 0&, R.Left, R.Top, R.Right, R.Bottom, 0)
'------------------------------------------------------------------------
  hndHGkinetic = lngParent
'------------------------------------------------------------------------
End Sub
'------------------------------------------------------------------------
 Private Sub TerminateProcess(app_exe As String)
     Shell "taskkill /F /IM " & app_exe  'appnamehere.exe"
 End Sub
'------------------------------------------------------------------------
' Destroy the window.
Sub HGkinetic_cmdCloseWindow()
 Dim ssss As String: ssss = "HGkinetic.exe"
 Dim uExitCode As Long
 Dim uCode  As Long
 
 If hndHGkinetic <> 0 Then
  TerminateProcess (ssss)
  hndHGkinetic = 0
 End If
'ssss
 Dim sFileText As String: sFileText = " " & "CLOSE"
 Dim Path As String: Path = App.Path  '.Directory.GetCurrentDirectory()
 Dim idFileNo As Integer: idFileNo = FreeFile
 Dim Filepath As String: Filepath = Path & "\HGkinetic\Dump\APP.txt"
 Open Filepath For Output As #idFileNo
  Print #idFileNo, sFileText
 Close #idFileNo
'--------------------------------------------
  idFileNo = FreeFile
  Dim Filepaths(1) As String: Filepaths(0) = Path & "\HGkinetic\COR.txt": Filepaths(1) = Path & "\HGkinetic\HQ.txt"
 For i = 0 To 1
  sFileText = OEMX.HGkineticF(i).Value '"1"
   Open Filepaths(i) For Output As #idFileNo
     Print #idFileNo, sFileText
  Close #idFileNo
  OEMX.HGkineticF(i).Visible = False ': OEMX.HGkineticF(0).Caption = "Dtip"
 Next i
End Sub
'------------------------------------------------------------------------
Function CalcolaCBeta(Alfa_G, Beta_G) 'CorrettorePassoMola
 Dim PG As Double: PG = 3.141592654
 Dim APP01 As Double: APP01 = Tan(Alfa_G) / Cos(Beta_G)
 Dim ALFAt As Double: ALFAt = ATAN2(APP01, 1)
 Dim FW As Double: FW = Abs((Lp("iFas1Fsx") + Lp("iFas1Fdx")) / 2)
 Dim CORRFHB_G As Double: CORRFHB_G = Abs(Lp("CORRPASSO"))
 Dim FASCIAFHB_G As Double: FASCIAFHB_G = Abs(Lp("FASCIAFHB_G"))
 If (FASCIAFHB_G = 0) Then FASCIAFHB_G = FW
 Dim CorrBeta_G As Double: CorrBeta_G = CORRFHB_G * Cos(Beta_G) * Cos(Beta_G) / (FASCIAFHB_G * Cos(ALFAt)) '* (180 / PG)
 CalcolaCBeta = CorrBeta_G
End Function
'------------------------------------------------------------------------
 Sub HGkinetic_XMLpopulate()
  Const PI = 3.14159265358979
  Dim ssss As String: ssss = ""
  Dim stmp As String: stmp = ""
' Exportin information
  stmp = "HGkinetic: XML export "
  OEMX.OEM1PicCALCOLO.CurrentX = 1
  OEMX.OEM1PicCALCOLO.CurrentY = OEMX.OEM1PicCALCOLO.TextHeight(stmp)
  OEMX.OEM1PicCALCOLO.Print stmp
' Acquire parameters
' GEAR
 Dim APn As Double: APn = Abs(Lp("ALFA_G"))
 Dim Beta As Double: Beta = Abs(Lp("BETA_G")): If (Beta = 0) Then Beta = 0.001
 Dim Betab As Double: Betab = Round((180 / PI) * Atn(Tan(Abs(Beta * PI / 180)) * (Cos(Atn(Tan(Abs(APn * PI / 180)) / Cos(Abs(Beta * PI / 180)))))), 4)
 Dim WNQ As Double: WNQ = Abs(Lp("QW"))
 Dim ZWQ As Double: ZWQ = Abs(Lp("ZW"))
 Dim FW As Double: FW = Abs((Lp("iFas1Fsx") + Lp("iFas1Fdx")) / 2)
 Dim Mn As Double: Mn = Abs(Lp("MN_G"))
 Dim DSAP As Double: DSAP = Abs(Lp("DSAP_G"))
 Dim DEAP As Double: DEAP = Abs(Lp("DEAP_G"))
 Dim Z As Double: Z = Abs(Lp("NUMDENTI_G"))
' GEAR

'FasciaAMV = Lp("CORSAIN") + Lp("iFas1F1") + Lp("CORSAOUT")
ssss = ssss & "<?xml version= " & Chr(34) & "1.0" & Chr(34) & "?>"
ssss = ssss & "<!-- *********** http://www.xmlvalidation.com/ *********** -->"
ssss = ssss & "<!DOCTYPE INPUTS SYSTEM " & Chr(34) & "COS.dtd" & Chr(34) & ">"
ssss = ssss & "<INPUTS>"
ssss = ssss & "<INPUT INPUTID=" & Chr(34) & "p1" & Chr(34) & "><NAME>DEFAULT</NAME>"
ssss = ssss & "<GEAR><NAME>DEFAULT</NAME>"
ssss = ssss & "<APN>" & Replace(CStr(APn), ",", ".") & "</APN>"
ssss = ssss & "<BETAB>" & Replace(CStr(Betab), ",", ".") & "</BETAB>"
ssss = ssss & "<BETA>" & Replace(CStr(Beta), ",", ".") & "</BETA>"
ssss = ssss & "<WNQ>" & Replace(CStr(WNQ), ",", ".") & "</WNQ>"
ssss = ssss & "<ZWQ>" & Replace(CStr(ZWQ), ",", ".") & "</ZWQ>"
ssss = ssss & "<FW>" & Replace(CStr(FW), ",", ".") & "</FW>"
ssss = ssss & "<MN>" & Replace(CStr(Mn), ",", ".") & "</MN>"
ssss = ssss & "<DSAP>" & Replace(CStr(DSAP), ",", ".") & "</DSAP>"
ssss = ssss & "<DEAP>" & Replace(CStr(DEAP), ",", ".") & "</DEAP>"
ssss = ssss & "<Z>" & Replace(CStr(Z), ",", ".") & "</Z>"
ssss = ssss & "<GAMMA>0</GAMMA>"
'---------------------------------------------------------------MICROG
 Dim iBom1F1 As Double: iBom1F1 = Lp("iBom1Fsx"): Dim iBom1F2 As Double: iBom1F2 = Lp("iBom1Fdx"): Dim BoE As Double: BoE = (iBom1F1 + iBom1F2) / 2
 Dim iCon1F1 As Double: iCon1F1 = Lp("iCon1Fsx"): Dim iCon1F2 As Double: iCon1F2 = Lp("iCon1Fdx"): Dim CoE As Double: CoE = (iCon1F1 + iCon1F2) / 2

ssss = ssss & "<MICROG><NAME>DEFAULT</NAME>"
ssss = ssss & "<BoE>" & Replace(CStr(BoE), ",", ".") & "</BoE>"
ssss = ssss & "<CoE>" & Replace(CStr(CoE), ",", ".") & "</CoE>"
ssss = ssss & "</MICROG>"
ssss = ssss & "</GEAR>"
'-----------------------------------
 Dim APNr As Double: APNr = Abs(Lp("AP_RULLO"))
 Dim DRX As Double: DRX = Abs(LEP("DRULLOPROF_G"))
 Dim DRAD As Double: DRAD = Abs(Lp("BOMBATURA_RULLO"))   ' LEP("BOMBATURA_RULLO")
 Dim DRT As Double: DRT = Abs(Lp("SP_RIF_RULLO"))    ' Dresser reference thickness
 Dim DRR As Double: DRR = Abs(Lp("RG_RIF_RULLO"))    ' Dresser reference radius
 Dim DRH As Double: DRH = Abs(Lp("H_RIF_RULLO"))     ' Dresser reference heigth
 '
 'Modifica per rullo Contour Dressing
 '
 Dim TipoRullo As Integer: TipoRullo = Lp("TIPORULLO_G")
 
 If TipoRullo = 3 Then
  'Dati della zona standard del rullo per Contour Dressing
  APNr = Abs(Lp("ANGOLOPRESSIONEZ1_CD"))
  DRX = Abs(LEP("DRULLOPROF_G"))
  DRAD = Abs(Lp("RAGGIOBOMBZ1_CD"))
  'Ruotando il rullo di 180� lo spessore di riferimento coincide con il doppio
  'dello spessore di riferimento della zona standard del rullo per Contour Dressing
  DRT = Abs(Lp("SPESRIFZ1_G")) * 2
  DRR = Abs(Lp("RAGGRIFZ1_G"))
  DRH = Abs(Lp("ALTEZZARULLOZ1_CD"))
  
 End If
'-----------------------------------
 ssss = ssss & "<DRESSER><NAME>DEFAULT</NAME>"
 ssss = ssss & "<APN>" & Replace(CStr(APNr), ",", ".") & "</APN>"
 ssss = ssss & "<DRX>" & Replace(CStr(DRX), ",", ".") & "</DRX>"
 ssss = ssss & "<VR>1</VR>"
 If (DRAD >= 10000) Then: ssss = ssss & "<DKIN>2</DKIN>": Else ssss = ssss & "<DKIN>1</DKIN>"
 ssss = ssss & "<DRAD>" & Replace(CStr(DRAD), ",", ".") & "</DRAD>"
'
'Modulate in order to be affine with 2D implementation: 2D: profile center crown is based on dresser thickness; 3D: profile center crown based on dresser heigth;  when dresser height is larger than work heingth crown center is moved far from 2D case
'
 Dim DHC As Double: DHC = (DRX - DHC) / 2#
 Dim DPER As Double: If (DHC <> 0) Then DPER = DRT / DRH
 If (DPER < 0.1 Or DPER > 0.9) Then DPER = 0.5
 DPER = 0.5
 ssss = ssss & "<DPER>" & Replace(CStr(DPER), ",", ".") & "</DPER>"
 ssss = ssss & "<DRT>" & Replace(CStr(DRT), ",", ".") & "</DRT>" '4.08
 ssss = ssss & "<DRR>" & Replace(CStr(DRR), ",", ".") & "</DRR>" '55.106
 ssss = ssss & "<DRH>" & Replace(CStr(DRH), ",", ".") & "</DRH>" '9.788
 ssss = ssss & "<DSIG>1</DSIG></DRESSER>"
'----------------------------------- WORM
 Dim Np As Double: Np = Abs(LEP("NUMPRINC_G"))
 Dim DXm As Double: Dim DPm As Double
'-----------------------------------
 DXm = 2 * (val(OPC_LEGGI_DATO("/ACC/NCK/GUD7/REXTMOLA_G"))): If (DXm = 0) Then DXm = Lp("DIAMMOLA_G") 'Diametro Mola Nuova
 DPm = Abs(DXm - LEP("HDENTEMOLA_G"))
' <FW>20</FW><WH>7.2</WH><DX>231</DX>
 Dim WH As Double: WH = Abs(Lp("HDENTEMOLA_G"))    ' Worm heigth (ALTEZZACIL_G?)
 Dim WDX As Double: WDX = DXm 'Abs(Lp("HDENTEMOLA_G"))    '
 ssss = ssss & "<WORM><NAME>DEFAULT</NAME>"
 ssss = ssss & "<SF>-1</SF>"
 ssss = ssss & "<NP>" & Replace(CStr(Np), ",", ".") & "</NP>"
 ssss = ssss & "<FW>20</FW>"
'ssss = ssss & "<DP>" & Replace(CStr(DPm), ",", ".") & "</DP>"
 ssss = ssss & "<WH>" & Replace(CStr(WH), ",", ".") & "</WH>"
 ssss = ssss & "<DX>" & Replace(CStr(WDX), ",", ".") & "</DX>"
 ssss = ssss & "</WORM>"
'-----------------------------------
 ssss = ssss & "<HG250><NAME>DEFAULT</NAME>"
 ssss = ssss & "<ZPgPgC>0</ZPgPgC>"
 ssss = ssss & "<PCM><NAME>PROFILATOREp1</NAME>"
 ssss = ssss & "<RP>0</RP>"
 ssss = ssss & "<ZP>0</ZP>"
 ssss = ssss & "</PCM>"
 ssss = ssss & "<PC3><NAME>ASSEC3p1</NAME>"
 ssss = ssss & "<XPC3>0</XPC3>"
 ssss = ssss & "<YPC3>0</YPC3>"
 ssss = ssss & "<ZPC3>0</ZPC3>"
 ssss = ssss & "</PC3>"
 ssss = ssss & "<pZX><NAME>ASSIZXp1</NAME>"
 ssss = ssss & "<X0zx>0</X0zx>"
 ssss = ssss & "<Y0zx>0</Y0zx>"
 ssss = ssss & "<Z0zx>0</Z0zx>"
 ssss = ssss & "</pZX>"
 ssss = ssss & "<pA><NAME>ASSEAp1</NAME>"
 ssss = ssss & "<X0a>0</X0a>"
 ssss = ssss & "<Y0a>0</Y0a>"
 ssss = ssss & "<Z0a>0</Z0a>"
 ssss = ssss & "</pA>"
 ssss = ssss & "<pY><NAME>ASSEYp1</NAME>"
 ssss = ssss & "<X0y>0</X0y>"
 ssss = ssss & "<Y0y>0</Y0y>"
 ssss = ssss & "<Z0y>0</Z0y>"
 ssss = ssss & "</pY>"
 ssss = ssss & "</HG250>"
'-----------------------------------------------------
Dim i As Integer: i = 1 '        For i = 1 To MAXNumeroCicli
Dim iLep As Integer: iLep = (i - 1) * 20
        '  TipoCiclo(i) = LEP("CYC[0," & iLep + 1 & "]")   'Tipo ciclo
        '  Rip(i) = LEP("CYC[0," & iLep + 2 & "]")         'Numero ripetizioni
        '  IncAnd(i) = LEP("CYC[0," & iLep + 3 & "]")      'Incremento x ripetizione
Dim vAv As Double: vAv = Abs(LEP("CYC[0," & iLep + 4 & "]")) ': If (vAv < 1) Then vAv = 1 'Avanzamento passata (mm/g)
' Velocizzo il calcolo
Dim nGp As Integer: nGp = 10
Dim vAvt As Double: vAvt = (Abs(Lp("CORSAIN")) + Abs(Lp("CORSAOUT")) + Abs((Lp("iFas1Fsx") + Lp("iFas1Fdx")) / 2)) / nGp
Dim nGr As Integer: nGr = (Abs(Lp("CORSAIN")) + Abs(Lp("CORSAOUT")) + Abs((Lp("iFas1Fsx") + Lp("iFas1Fdx")) / 2)) / vAv
If (nGp < nGr) Then vAv = vAvt

If (OEMX.HGkineticF(1).Value = 1) Then vAv = 1

Dim Yshift As Double: Yshift = Abs(LEP("CYC[0," & iLep + 5 & "]"))     'Shift diagonal x 1 mm
Dim Vt As Double: Vt = Abs(LEP("CYC[0," & iLep + 6 & "]")): If (Vt < 1) Then Vt = 50       'Velocit� Taglio M/s
        '  ShiftMolaFin(i) = LEP("CYC[0," & iLep + 8 & "]") 'Spostamento mola prima di finitura
        '  RitShift(i) = LEP("CYC[0," & iLep + 9 & "]")     'Ritorno shift
          
ssss = ssss & "<PROCESS><NAME>DEFAULT</NAME>"
ssss = ssss & "<Vt>" & Replace(CStr(Vt), ",", ".") & "</Vt>"
ssss = ssss & "<vAv>" & Replace(CStr(vAv), ",", ".") & "</vAv>"
ssss = ssss & "<Yshift>" & Replace(CStr(Yshift), ",", ".") & "</Yshift>"
ssss = ssss & "</PROCESS>"
'---------------------------------------------------------------FASARETTIFICA
ssss = ssss & "<OPERATORE><NAME>DEFAULT</NAME>"
ssss = ssss & "<FASARETTIFICA><NAME>DEFAULT</NAME>"
ssss = ssss & "<fB1>0</fB1>"
ssss = ssss & "<fC>0</fC>"
ssss = ssss & "</FASARETTIFICA>"
'---------------------------------------------------------------CORREZIONERETTIFICA
Dim CaX As Double: CaX = (Lp("CORINTERASSE")): If (OEMX.HGkineticF(0).Value = 1) Then CaX = 0 '(Lp("CORINTLAV_G[0,1]") + Lp("CORINTLAV_G[1,1]")) / 2
Dim CBeta As Double: CBeta = CalcolaCBeta(APn * PI / 180, Beta * PI / 180): If (OEMX.HGkineticF(0).Value = 1) Then CBeta = 0: 'Lp("CORRFHB_G") '(Lp("CORRFHB1") + Lp("CORRFHB2")) / 2 Agisce sul passo mola

ssss = ssss & "<CORREZIONERETTIFICA><NAME>DEFAULT</NAME>"
ssss = ssss & "<CBeta>" & Replace(CStr(CBeta), ",", ".") & "</CBeta>"
ssss = ssss & "<CaX>" & Replace(CStr(CaX), ",", ".") & "</CaX>"

'CORSPESSOREMV_G
 Dim Cwt As Double: Cwt = Abs(Lp("CORSPESSOREMV_G")): If (OEMX.HGkineticF(0).Value = 1) Then Cwt = 0
ssss = ssss & "</CORREZIONERETTIFICA>"
ssss = ssss & "<CORREZIONEPROFILATURA><NAME>DEFAULT</NAME>"
ssss = ssss & "<Cwt>" & Replace(CStr(Cwt), ",", ".") & "</Cwt>"
ssss = ssss & "</CORREZIONEPROFILATURA>"
'---------------------------------------------------------------EXTRACORSE
Dim ExYin As Double: ExYin = Abs(Lp("SHIFTIN_G")) '
Dim ExYou As Double: ExYou = Abs(Lp("SHIFTOUT_G"))
Dim ExZin As Double: ExZin = Abs(Lp("CORSAIN")) '+ Lp("iFas1F1") + Lp("CORSAOUT")
Dim ExZou As Double: ExZou = Abs(Lp("CORSAOUT")) '
ssss = ssss & "<EXTRACORSE><NAME>DEFAULT</NAME>"
ssss = ssss & "<ExYin>" & Replace(CStr(ExYin), ",", ".") & "</ExYin>"
ssss = ssss & "<ExYou>" & Replace(CStr(ExYou), ",", ".") & "</ExYou>"
ssss = ssss & "<ExZin>" & Replace(CStr(ExZin), ",", ".") & "</ExZin>"
ssss = ssss & "<ExZou>" & Replace(CStr(ExZou), ",", ".") & "</ExZou>"
ssss = ssss & "</EXTRACORSE>"
'---------------------------------------------------------------CORRETTORI
Dim CfHa1 As Double: CfHa1 = -(Lp("CORFHA1")): If (OEMX.HGkineticF(0).Value = 1) Then CfHa1 = 0
Dim CfHa2 As Double: CfHa2 = -(Lp("CORFHA2")): If (OEMX.HGkineticF(0).Value = 1) Then CfHa2 = 0
ssss = ssss & "<CORRETTORI><NAME>DEFAULT</NAME>"
ssss = ssss & "<CfHa1>" & Replace(CStr(CfHa1), ",", ".") & "</CfHa1>"
ssss = ssss & "<CfHa2>" & Replace(CStr(CfHa2), ",", ".") & "</CfHa2>"
ssss = ssss & "</CORRETTORI>"
ssss = ssss & "<INSPECTION><NAME>DEFAULT</NAME><pfHa1>05</pfHa1><pfHa2>50</pfHa2><pfHa3>95</pfHa3><pfHb1>20</pfHb1><pfHb2>50</pfHb2><pfHb3>80</pfHb3></INSPECTION>"
ssss = ssss & "</OPERATORE>"
ssss = ssss & "</INPUT>"
ssss = ssss & "</INPUTS>"
'---------------------------------------------------------------
  Dim ApPath: ApPath = App.Path
  Dim Ape: Ape = ApPath & "\HGkinetic\COS.xml"
  Dim intFileNum As Integer: intFileNum = FreeFile
                ' change Output to Append if you want to add to an existing file
                ' rather than creating a new file each time
  Open Ape For Output As intFileNum
  Print #intFileNum, ssss
  Close intFileNum
End Sub

Private Sub CYC_PRFVITNP_AT_Y_PARSE(ByVal N_ZONE_FIN As Integer, ByVal ZSGRLM As Integer, ByVal cY_Zi As Double, ByVal Y_acc As Double, ByVal EXTENSION As Integer, ByRef DIME_Y As Integer, ByRef ATDL As Double, ByRef qY0 As Double, ByRef qY1 As Double, ByRef qY2 As Double, ByRef qY3 As Double, ByRef qY4 As Double, ByRef qY5 As Double, ByRef qY6 As Double, ByRef qY7 As Double, ByRef qY8 As Double)
'*****************************************************************
'* Quote fascia mola                                             *
'* GBERTACCHI@SAMP@2016.12.23                                    *
'* VERSIONE:                                                     *
'* ZSGRLM     ' ZONA_SGR_LATO_MOTORE 1: VERO, 0: FALSE           *
'* EXTENSION  ' (0;1)=(falso,vero) PROFILATURA EXTRACORSE        *
'*****************************************************************
Dim L_X_FIN_C As Double
Dim L_X_FIN As Double
Dim FW_SG As Double
Dim FW_L(0 To 2) As Double  ' LUNGHEZZE MOLA CARATTERISTICHE
Dim DELTA_Y(0 To 9) As Double
Dim qY_CP(0 To 9) As Double ' PUNTI DI CONTROLLO ASSE Y
Dim STP As Integer
Dim SPR As Double
Dim EXTC As Double      ' ENTITA CORSA LINEARE > cY_Zi
Dim LARGHMOLA_G#, ShiftOut_G#, ShiftIn_G#
LARGHMOLA_G = Lp("LARGHMOLA_G")
ShiftOut_G = Lp("ShiftOut_G")
ShiftIn_G = Lp("ShiftIn_G")
 EXTC = 3
 SPR = Lp("DIST_CR") * 2
 '*****************************************************************
 If (Lp("LNG_TW[0,0]") > Lp("LNG_TW[0,1]")) Then
  ATDL = Lp("LNG_TW[0,0]") ' +(ShiftIn_G+ShiftOut_G)*EXENSION
 Else
  ATDL = Lp("LNG_TW[0,1]") ' +(ShiftIn_G+ShiftOut_G)*EXENSION
 End If
 '*****************************************************************
 DIME_Y = 6 + 2 * (N_ZONE_FIN - 1)
 L_X_FIN_C = (2 / 3) / N_ZONE_FIN
 L_X_FIN = LARGHMOLA_G - ShiftIn_G - ShiftOut_G
 If (ATDL + (ShiftIn_G + ShiftOut_G)) > (L_X_FIN * L_X_FIN_C) Then
  ATDL = L_X_FIN * L_X_FIN_C + (ShiftIn_G + ShiftOut_G) * (EXTENSION - 1)
 Else
  ATDL = ATDL + (ShiftIn_G + ShiftOut_G) * EXTENSION
 End If
'*****************************************************************
 qY_CP(0) = 0: DELTA_Y(0) = 0: qY0 = 0
 qY_CP(1) = 0: DELTA_Y(1) = 0: qY1 = 0
 qY_CP(2) = 0: DELTA_Y(2) = 0: qY2 = 0
 qY_CP(3) = 0: DELTA_Y(3) = 0: qY3 = 0
 qY_CP(4) = 0: DELTA_Y(4) = 0: qY4 = 0
 qY_CP(5) = 0: DELTA_Y(5) = 0: qY5 = 0
 qY_CP(6) = 0: DELTA_Y(6) = 0: qY6 = 0
 qY_CP(7) = 0: DELTA_Y(7) = 0: qY7 = 0
 qY_CP(8) = 0: DELTA_Y(8) = 0: qY8 = 0

' FW_SG = LARGHMOLA_G-(ShiftOut_G+(ATDL+ShiftIn_G)*N_ZONE_FIN+cY_Zi+ShiftOut_G)-ShiftIn_G
FW_SG = LARGHMOLA_G - (ShiftOut_G * (1 - EXTENSION) + (ATDL + ShiftIn_G * (1 - EXTENSION) + EXTC * EXTENSION) * N_ZONE_FIN + cY_Zi - EXTC * EXTENSION + ShiftOut_G) - ShiftIn_G
FW_L(0) = FW_SG: FW_L(1) = ATDL

DELTA_Y(0) = ShiftIn_G + SPR / 2 - Y_acc - ShiftIn_G * EXTENSION * (1 - ZSGRLM) '=ShiftIn_G+SPR/2-Y_acc
DELTA_Y(1) = FW_L(1 - ZSGRLM) + 2 * Y_acc
DELTA_Y(2) = ShiftOut_G - 2 * Y_acc - ShiftOut_G * EXTENSION * (1 - ZSGRLM) + EXTC * EXTENSION * (1 - ZSGRLM) ' ShiftOut_G-2*Y_acc
If (N_ZONE_FIN = 1) Then
  DELTA_Y(3) = cY_Zi - EXTC * EXTENSION + 2 * Y_acc  ' cY_Zi+2*Y_acc
  DELTA_Y(4) = EXTC * EXTENSION * ZSGRLM - 2 * Y_acc + ShiftIn_G * (1 - EXTENSION * ZSGRLM)  ' +ShiftIn_G-2*Y_acc
  DELTA_Y(5) = FW_L(ZSGRLM) + 2 * Y_acc
  DELTA_Y(6) = SPR / 2 - Y_acc + ShiftOut_G * (1 - EXTENSION * ZSGRLM)  ' ShiftOut_G+SPR/2-Y_acc
Else
 If (EXTENSION = 1) Then
ERR02:
 MsgBox ("Y_PARSE: N_ZONE_FIN=2 AND EXTENSION=1 : TOBE DONE!!")
   ' GoTo ERR02
End If
' TOBE UPDATE WHEN EXTENSION=1
 DELTA_Y(3) = cY_Zi * ZSGRLM + FW_L(1) * (1 - ZSGRLM) + 2 * Y_acc
 DELTA_Y(4) = ShiftIn_G - 2 * Y_acc
 DELTA_Y(5) = FW_L(1) * ZSGRLM + cY_Zi * (1 - ZSGRLM) + 2 * Y_acc
 DELTA_Y(6) = ShiftIn_G - 2 * Y_acc
 DELTA_Y(7) = FW_L(ZSGRLM) + 2 * Y_acc
 DELTA_Y(8) = ShiftOut_G + SPR / 2 - Y_acc
End If
'----------------------------INTERVALLI MOLA
 qY_CP(0) = DELTA_Y(0)
 For STP = 1 To DIME_Y
  qY_CP(STP) = qY_CP(STP - 1) + DELTA_Y(STP) ' DELTA_Y[DIRPASSY*STP-DIME_Y*(DIRPASSY-1)/2]
 Next
 qY0 = qY_CP(0)
 qY1 = qY_CP(1)
 qY2 = qY_CP(2)
 qY3 = qY_CP(3)
 qY4 = qY_CP(4)
 qY5 = qY_CP(5)
 qY6 = qY_CP(6)
 qY7 = qY_CP(7)
 qY8 = qY_CP(8)
End Sub



Sub MV_POS_INIZ_SH(ByVal DS As Integer, ByVal P_M As Integer, ByVal WM As Double, ByRef CY_IN As Double, ByVal CY_OU As Double, ByVal FW As Double, ByVal NFZ As Integer, ByVal PZS As Integer, ByVal CZT As Double, ByVal YAC As Double, ByVal EXS As Integer, ByRef PS_IN1 As Double, ByRef PS_OU1 As Double, ByRef PS_IN2 As Double, ByRef PS_OU2 As Double)

'*****************************************************************
'* IDENTIFICA POSIZIONI DI INIZIO E FINE SHIFT                   *
'* GBERTACCHI@SAMP@2016.12.23                                    *
'* VERSIONE: 1.1                                                 *
'* Calcola le posizioni di inio e fine corsa dell'asse Y utilizz *
'* durante la rettifica con mola a vite.                         *
'*****************************************************************
'* INGRESSI                                                      *
'* DS: direz.asseY in rettif.{1,-1}={in fuori,in dentro}'DirShift*
'* PM: parzioalizzazione mola {1,0}={vero,false}'     AT_DRESSING*
'* WM: quota asseY inizio mola'                           WMola_G*
'* CY_IN: corsa asseY ingresso'                         ShiftIn_G*
'* CY_OU: corsa asseY uscita  '                        ShiftOut_G*
'* FW: larghezza mola'                                Larghmola_G*
'* NFZ: numero zone di finitura mola'                  N_ZONE_FIN*
'* PZS: posiz. zona sgros. mola (+1,-1)=(motore,controp.)  ZSGRLM*
'* CZT: corsa asseY zona transizione intermedia             cY_Zi*
'* YAC: quotaY sottratta alle corseY x control. accelleraz' Y_acc*
'* EXS: profil. extracorse di fin. (1,0)=(vero,false)'  EXTENSION*
'* USCITE                                                        *
'*PS_IN1: quota asse Y inizio shift sgrossatura     PosInizioSh_G*
'*PS_OU1: quota asse Y fine shift sgrossatura         PosFineSh_G*
'*PS_IN2: quota asse Y inizio shift finitura       PosInizioSh2_L*
'*PS_OU2: quota asse Y fine shift finitura           PosFineSh2_L*
'*****************************************************************
 Dim DIME_Y As Integer
 Dim ATDL_OU As Double
 Dim qY_CP(0 To 9) As Double ' PUNTI DI CONTROLLO ASSE Y
 Dim ATDL_L As Double
 Dim LE(0 To 2) As Double

 If (DS <> 0) Then
  If (P_M <> 0) Then
   Call CYC_PRFVITNP_AT_Y_PARSE(NFZ, PZS, CZT, YAC, EXS, DIME_Y, ATDL_OU, qY_CP(0), qY_CP(1), qY_CP(2), qY_CP(3), qY_CP(4), qY_CP(5), qY_CP(6), qY_CP(7), qY_CP(8))
   ATDL_L = ATDL_OU - EXS * (CY_IN + CY_OU) ' AT_DL=ATDL_L
   PS_IN1 = -WM + (-CY_IN - (qY_CP(DIME_Y - 2) - qY_CP(0)) * (1 - PZS)) * (1 - DS) / 2 + (-FW + CY_OU + (qY_CP(DIME_Y - 1) - qY_CP(1) + (1 - EXS) * DS * CY_IN) * PZS) * (1 + DS) / 2
   PS_OU1 = -WM + (-CY_IN + (qY_CP(DIME_Y - 1) - qY_CP(DIME_Y - 2)) * (1 - PZS)) * (1 + DS) / 2 + (-FW + CY_OU + (qY_CP(DIME_Y - 1) - qY_CP(1) - (1 - EXS) * DS * CY_IN) * PZS) * (1 - DS) / 2
   PS_IN2 = -WM + (-CY_IN - (qY_CP(DIME_Y - 2 + (DS + 1) / 2) - qY_CP(0) - EXS * DS * CY_IN) * PZS)
   PS_OU2 = PS_IN2 + DS * ATDL_L
  Else
   LE(0) = -WM - CY_IN: LE(1) = -WM - FW + CY_OU
   PS_IN1 = LE(0) * (1 - DS) / 2 + LE(1) * (1 + DS) / 2
   PS_OU1 = LE(1) * (1 - DS) / 2 + LE(0) * (1 + DS) / 2
   PS_IN2 = 0
   PS_OU2 = 0
  End If
 Else
ERRSHF:
   MsgBox ("Please set the shift direction")
 ' GoTo ERRSHF
 End If

 End Sub