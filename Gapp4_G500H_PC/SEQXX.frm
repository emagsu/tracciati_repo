VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form SEQXX 
   Appearance      =   0  'Flat
   BackColor       =   &H00EBE1D7&
   BorderStyle     =   0  'None
   ClientHeight    =   7836
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   11052
   ForeColor       =   &H80000008&
   Icon            =   "SEQXX.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MDIChild        =   -1  'True
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7836
   ScaleWidth      =   11052
   ShowInTaskbar   =   0   'False
   Tag             =   "SEQXX"
   Visible         =   0   'False
   WhatsThisButton =   -1  'True
   WhatsThisHelp   =   -1  'True
   Begin TabDlg.SSTab INTRODUZIONE_ESTERNIO 
      Height          =   7212
      Left            =   50
      TabIndex        =   0
      Top             =   240
      Width           =   10100
      _ExtentX        =   17822
      _ExtentY        =   12721
      _Version        =   393216
      Tabs            =   8
      TabsPerRow      =   8
      TabHeight       =   420
      TabCaption(0)   =   "Tab 0"
      TabPicture(0)   =   "SEQXX.frx":030A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraTAB(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).ControlCount=   1
      TabCaption(1)   =   "Tab 1"
      TabPicture(1)   =   "SEQXX.frx":0326
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraTAB(1)"
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Tab 2"
      TabPicture(2)   =   "SEQXX.frx":0342
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "fraTAB(2)"
      Tab(2).ControlCount=   1
      TabCaption(3)   =   "Tab 3"
      TabPicture(3)   =   "SEQXX.frx":035E
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "fraTAB(3)"
      Tab(3).ControlCount=   1
      TabCaption(4)   =   "Tab 4"
      TabPicture(4)   =   "SEQXX.frx":037A
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "fraTAB(4)"
      Tab(4).ControlCount=   1
      TabCaption(5)   =   "Tab 5"
      TabPicture(5)   =   "SEQXX.frx":0396
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "fraTAB(5)"
      Tab(5).ControlCount=   1
      TabCaption(6)   =   "Tab 6"
      TabPicture(6)   =   "SEQXX.frx":03B2
      Tab(6).ControlEnabled=   0   'False
      Tab(6).Control(0)=   "fraTAB(6)"
      Tab(6).ControlCount=   1
      TabCaption(7)   =   "Tab 7"
      TabPicture(7)   =   "SEQXX.frx":03CE
      Tab(7).ControlEnabled=   0   'False
      Tab(7).Control(0)=   "fraTAB(7)"
      Tab(7).ControlCount=   1
      Begin VB.Frame fraTAB 
         Caption         =   "fraTAB"
         Height          =   6612
         Index           =   1
         Left            =   -74950
         TabIndex        =   8
         Top             =   300
         Width           =   10000
      End
      Begin VB.Frame fraTAB 
         Caption         =   "fraTAB"
         Height          =   6612
         Index           =   0
         Left            =   50
         TabIndex        =   7
         Top             =   300
         Width           =   10000
      End
      Begin VB.Frame fraTAB 
         Caption         =   "fraTAB"
         Height          =   6612
         Index           =   7
         Left            =   -74950
         TabIndex        =   6
         Top             =   300
         Width           =   10000
      End
      Begin VB.Frame fraTAB 
         Caption         =   "fraTAB"
         Height          =   6612
         Index           =   6
         Left            =   -74950
         TabIndex        =   5
         Top             =   300
         Width           =   10000
      End
      Begin VB.Frame fraTAB 
         Caption         =   "fraTAB"
         Height          =   6612
         Index           =   5
         Left            =   -74950
         TabIndex        =   4
         Top             =   300
         Width           =   10000
      End
      Begin VB.Frame fraTAB 
         Caption         =   "fraTAB"
         Height          =   6612
         Index           =   4
         Left            =   -74950
         TabIndex        =   3
         Top             =   300
         Width           =   10000
      End
      Begin VB.Frame fraTAB 
         Caption         =   "fraTAB"
         Height          =   6612
         Index           =   3
         Left            =   -74950
         TabIndex        =   2
         Top             =   300
         Width           =   10000
      End
      Begin VB.Frame fraTAB 
         Caption         =   "fraTAB"
         Height          =   6612
         Index           =   2
         Left            =   -74950
         TabIndex        =   1
         Top             =   300
         Width           =   10000
      End
   End
End
Attribute VB_Name = "SEQXX"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Activate()
  '
  Call ChildActivate(Me)
  Me.Line (0, 0)-(Me.Width * 0.999, Me.Height * 0.998), , B
  '
End Sub

Private Sub Form_Load()
'
Dim i As Integer
'
On Error GoTo errForm_Load
  '
  'NASCONDO I TASTI DELLA SECONDA RIGA
  MDIForm1.SSPanel1.Height = MDIForm1.Picture1(0).Height
  For i = 0 To 7
    MDIForm1.Picture3(i).Visible = False
  Next i
  Me.Width = g_nBeArtWidth * 0.99
  Me.Height = g_nBeArtHeight
  Call LEGGI_LINGUA
  Call IMPOSTA_FONT(Me)
  '
  INTRODUZIONE_ESTERNIO.Width = SEQXX.Width
  INTRODUZIONE_ESTERNIO.Left = 0
  INTRODUZIONE_ESTERNIO.Top = 0
  INTRODUZIONE_ESTERNIO.Height = SEQXX.Height
  '
  For i = 0 To INTRODUZIONE_ESTERNIO.Tabs - 1
    INTRODUZIONE_ESTERNIO.TabCaption(i) = "Par - " & Format$(i, "#0") & " - "
    SEQXX.fraTAB(i).Caption = ""
    SEQXX.fraTAB(i).Top = 300
    SEQXX.fraTAB(i).Left = 50
    SEQXX.fraTAB(i).Height = INTRODUZIONE_ESTERNIO.Height - SEQXX.fraTAB(i).Top - 50
    SEQXX.fraTAB(i).Width = INTRODUZIONE_ESTERNIO.Width - 100
  Next i
  '
  INTRODUZIONE_ESTERNIO.Tab = 0
  '
Exit Sub
  '
errForm_Load:
  WRITE_DIALOG Error(Err)
  Resume Next
  '
End Sub

Private Sub Form_Deactivate()
  '
  Call ChildDeActivate(Me)
  '
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  '
  Call GENERA_EVENTO_CLICK(KeyCode, Shift)
  '
End Sub

Private Sub INTRODUZIONE_ESTERNIO_Click(PreviousTab As Integer)
  '
  WRITE_DIALOG "CURRENT: " & INTRODUZIONE_ESTERNIO.Tab & " PreviousTab: " & PreviousTab
  fraTAB(PreviousTab).Visible = False
  fraTAB(INTRODUZIONE_ESTERNIO.Tab).Visible = True
  '
End Sub

