Attribute VB_Name = "VITI_CONICHE"
Option Explicit
'
'DIMENSIONI
Dim R37 As Double
Dim R38 As Double
'FONDO MOLA
Dim R45 As Double
Dim R55 As Double
'FIANCO 1
Dim R41 As Double
Dim R43 As Double
Dim R44 As Double
Dim R46 As Double
Dim R47 As Double
Dim R48 As Double
Dim R49 As Double
'FIANCO 2
Dim R51 As Double
Dim R53 As Double
Dim R54 As Double
Dim R56 As Double
Dim R57 As Double
Dim R58 As Double
Dim R59 As Double
'
Dim XX(0, 99) As Double
Dim YY(0, 99) As Double
Dim RR(0, 11) As Double
'
Dim R17 As Integer
Dim R12 As Integer
Dim R21 As Integer
Dim R19 As Integer
Dim R39 As Double
'
Dim R258 As Double
Dim R259 As Double
'
Dim R260 As Double
Dim R261 As Double
Dim R350 As Double
Dim R267 As Double
Dim R268 As Double
Dim R269 As Double
Dim R256 As Double
Dim R257 As Double
'
Dim R272 As Double
Dim R273 As Double
Dim R274 As Double

Sub CALCOLO_SPOSTAMENTI_BS5S()
'
Dim sERRORE   As String
Dim PM        As Double
Dim IMO       As Double
Dim DiaESTM   As Double

Dim DSW1   As Double
Dim DSW2   As Double
'
Dim DLF1   As Double
Dim DLF2   As Double
'
Dim LCott    As Double
Dim LCott_A  As Double
Dim LCott_B  As Double
Dim LCrif1   As Double
Dim LCrif2   As Double
Dim R328     As Double
Dim R528     As Double
'
Dim ret  As Long
Dim stmp As String

Dim sLARGHEZZA_MOLA As String
Dim LARGHEZZA_MOLA  As Double
'
Dim PA_BS5S  As Double
Dim PB_BS5S  As Double
Dim ODA_BS5S As Double
Dim ODB_BS5S As Double
Dim MODALITA As String
'
Dim sRESET   As String
Dim iRESET   As Integer
Dim Valore   As Double
Dim sVALORE  As String
Dim INVIO    As Integer
'
On Error GoTo errCALCOLO_SPOSTAMENTI_BS5S
  '
  'LETTURA PARAMETRI DI LAVORO
  PM = Lp("R[132]")       'PASSO VITE
  DiaESTM = Lp("R[189]")  'DIAMETRO ESTERNO MAX
  LCrif1 = Lp("R[66]")    'LARGHEZZA MINIMA CANALE
  LCrif2 = Lp("R[65]")    'LARGHEZZA MINIMA CANALE
  IMO = Lp("R[124]")      'INCLINAZIONE MOLA
  '
  PA_BS5S = Lp("R[520]"): ODA_BS5S = Lp("R[523]")
  PB_BS5S = Lp("R[320]"): ODB_BS5S = Lp("R[323]")
  '
  LARGHEZZA_MOLA = Lp("R[38]"): sLARGHEZZA_MOLA = LNP("R[38]")
  '
  'CALCOLO DELLE VARIAZIONI DI SPESSORE BS5B
  Call CALCOLO_CANALE(PM, IMO, DiaESTM, DiaESTM - PB_BS5S * 2 + ODB_BS5S * 2, DSW1, DSW2, 0, 0)
  LCott_B = DSW1 + DSW2: LCott_B = Int(LCott_B * 100) / 100
  R328 = LCrif1 - LCott_B: R328 = Int(R328 * 100) / 100
  '
  'CALCOLO DELLE VARIAZIONI DI SPESSORE BS5A
  Call CALCOLO_CANALE(PM, IMO, DiaESTM, DiaESTM - PA_BS5S * 2 + ODA_BS5S * 2, DSW1, DSW2, 0, 0)
  LCott_A = DSW1 + DSW2: LCott_A = Int(LCott_A * 100) / 100
  R528 = LCrif2 - LCott_A: R528 = Int(R528 * 100) / 100
  '
  'LEGGO LA MODALITA
  MODALITA = GetInfo("Configurazione", "Modalita", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))
  stmp = "MELT CHANNEL BREITE=" & frmt(LCott_B, 3) & " diff.=" & frmt(R328, 3) & Chr(13) & Chr(13)
  stmp = stmp & "SOLIDS CHANNEL BREITE=" & frmt(LCott_A, 3) & " diff.=" & frmt(R528, 3) & Chr(13) & Chr(13)
  If (MODALITA = "OFF-LINE") Then
    stmp = stmp & "----> HD"
  Else
    stmp = stmp & "----> CNC"
  End If
  StopRegieEvents
  If (R328 >= 0) And (R528 >= 0) Then
    ret = MsgBox(stmp, vbCritical + vbYesNo + vbDefaultButton2, "RESULTS")
  Else
    MsgBox sLARGHEZZA_MOLA & ": TOO BIG!!!!", vbCritical, "WARNING"
    ret = vbNo
  End If
  ResumeRegieEvents
  If (ret = vbYes) Then
    'EVENTUALE AGGIORNAMENTO DEL SOTTOPROGRAMMA
    If (MODALITA = "OFF-LINE") Then
      iRESET = 1
    Else
      'CONTROLLO SE SONO NELLO DI RESET PER ACCETTARE LA MODIFICA
      'LETTURA DELL'ITEM DDE PER CONTROLLO STATO DI RESET
      sRESET = GetInfo("CONFIGURAZIONE", "sRESET", PathFILEINI)
      If (Len(sRESET) > 0) Then
        iRESET = val(OPC_LEGGI_DATO(sRESET))
      Else
        iRESET = 1
      End If
    End If
    INVIO = 0
    If (iRESET = 1) Then
      'CALCOLO DIFFERENZA MOLA-VANO PER FASE A
      If (R528 > 0) Then
        Call SP("R[528]", frmt(R528, 3))
        'ABILITO L'INVIO DEL SOTTOPROGRAMMA MODIFICATO
        INVIO = 1
      End If
      'CALCOLO DIFFERENZA MOLA-VANO PER FASE B
      If (R328 > 0) Then
        Call SP("R[328]", frmt(R328, 3))
        'ABILITO L'INVIO DEL SOTTOPROGRAMMA MODIFICATO
        INVIO = 1
      End If
      'INVIO IL SOTTOPROGRAMMA MODIFICATO
      If (INVIO = 1) And (MODALITA = "SPF") Then Call OEMX.SuOEM1.CreaSPF
      Call OEMX.SuOEM1.DisegnaOEM
    Else
      'SEGNALAZIONE UTENTE
      WRITE_DIALOG "Data cannot be modified: CNC is not on RESET state"
    End If
  Else
      'SEGNALAZIONE UTENTE
      WRITE_DIALOG "Operation Aborted by user!!!"
  End If
'
Exit Sub
'
errCALCOLO_SPOSTAMENTI_BS5S:
  WRITE_DIALOG "Sub CALCOLO_SPOSTAMENTI_BS5S: Error -> " & Err
  sERRORE = " LOADED WITH ERROR " & str(Err)
  Resume Next
  
End Sub

Sub CALCOLO_SEQUENZA_CICLI_CONICHE(ByVal PASSO As Double, ByVal PROFONDITA As Double, ByVal Diametro As Double, ByVal TipoMola As String, ByRef RIPE_RET() As Integer, ByRef PASS_PRF() As Integer, ByRef INCR_PRF() As Double, ByRef FEED_PRF() As Double, ByRef ROTA_PRF() As String, ByRef PASS_RET() As Integer, ByRef INCR_RET() As Double, ByRef VELO_ASS() As Double, ByRef VELO_MOL() As Double, ByRef SVRM_CYC() As Double)
'
Dim SCARTO(3) As Double
Dim stmp      As String
'
On Error Resume Next
  '
  'MR. GIGEL 26/09/06
  'RIPARTIZIONE PROFONDITA'
  SVRM_CYC(1) = PROFONDITA
  'INCREMENTO FINITURA
  stmp = GetInfo("CICLI_VITI_CONICHE", "SVRM_CYC(3)", Path_LAVORAZIONE_INI)
  If (Len(stmp) > 0) Then SVRM_CYC(3) = val(stmp) Else SVRM_CYC(3) = 0.05
  'INCREMENTO PREFINITURA
  stmp = GetInfo("CICLI_VITI_CONICHE", "SVRM_CYC(2)", Path_LAVORAZIONE_INI)
  If (Len(stmp) > 0) Then SVRM_CYC(2) = val(stmp) Else SVRM_CYC(2) = 0.14
  'PROFONDITA' DI SGROSSATURA
  SVRM_CYC(1) = SVRM_CYC(1) - SVRM_CYC(2) - SVRM_CYC(3)
  '
  stmp = GetInfo("CICLI_VITI_CONICHE", "RIPE_RET(3)", Path_LAVORAZIONE_INI)
  If (Len(stmp) > 0) Then RIPE_RET(3) = val(stmp) Else RIPE_RET(3) = 1
  stmp = GetInfo("CICLI_VITI_CONICHE", "PASS_RET(3)", Path_LAVORAZIONE_INI)
  If (Len(stmp) > 0) Then PASS_RET(3) = val(stmp) Else PASS_RET(3) = 1
  '
  stmp = GetInfo("CICLI_VITI_CONICHE", "RIPE_RET(2)", Path_LAVORAZIONE_INI)
  If (Len(stmp) > 0) Then RIPE_RET(2) = val(stmp) Else RIPE_RET(2) = 1
  stmp = GetInfo("CICLI_VITI_CONICHE", "PASS_RET(2)", Path_LAVORAZIONE_INI)
  If (Len(stmp) > 0) Then PASS_RET(2) = val(stmp) Else PASS_RET(2) = 2
  '
  For i = 1 To 3
    stmp = GetInfo("CICLI_VITI_CONICHE", "VELO_MOL" & TipoMola & "(" & Format$(i, "#") & ")", Path_LAVORAZIONE_INI)
    If (Len(stmp) > 0) Then VELO_MOL(i) = val(stmp) Else VELO_MOL(i) = 45
    stmp = GetInfo("CICLI_VITI_CONICHE", "PASS_PRF" & TipoMola & "(" & Format$(i, "#") & ")", Path_LAVORAZIONE_INI)
    If (Len(stmp) > 0) Then PASS_PRF(i) = val(stmp) Else PASS_PRF(i) = 2
    stmp = GetInfo("CICLI_VITI_CONICHE", "INCR_PRF" & TipoMola & "(" & Format$(i, "#") & ")", Path_LAVORAZIONE_INI)
    If (Len(stmp) > 0) Then INCR_PRF(i) = val(stmp) Else INCR_PRF(i) = -0.06
    stmp = GetInfo("CICLI_VITI_CONICHE", "FEED_PRF" & TipoMola & "(" & Format$(i, "#") & ")", Path_LAVORAZIONE_INI)
    If (Len(stmp) > 0) Then FEED_PRF(i) = val(stmp) Else FEED_PRF(i) = 140
    stmp = GetInfo("CICLI_VITI_CONICHE", "VELO_RUL" & TipoMola & "(" & Format$(i, "#") & ")", Path_LAVORAZIONE_INI)
    If (Len(stmp) > 0) Then ROTA_PRF(i) = val(stmp) Else ROTA_PRF(i) = VELO_MOL(i) * 0.8
  Next i
        '
        INCR_RET(3) = SVRM_CYC(3) / PASS_RET(3)
        INCR_RET(2) = SVRM_CYC(2) / PASS_RET(2)
        '
      If (Diametro <= 25) Then
        '
        PASS_RET(1) = 6
        INCR_RET(1) = 0.25
        '
        VELO_ASS(1) = 40: VELO_ASS(2) = 40: VELO_ASS(3) = 40
        '
  ElseIf (Diametro > 25) And (Diametro <= 30) Then
        '
        PASS_RET(1) = 5
        INCR_RET(1) = 0.16
        '
        VELO_ASS(1) = 36: VELO_ASS(2) = 40: VELO_ASS(3) = 40
        '
  ElseIf (Diametro > 30) And (Diametro <= 35) Then
        '
        PASS_RET(1) = 5
        INCR_RET(1) = 0.16
        '
        VELO_ASS(1) = 36: VELO_ASS(2) = 40: VELO_ASS(3) = 40
        '
  ElseIf (Diametro > 35) And (Diametro <= 40) Then
        '
        PASS_RET(1) = 5
        INCR_RET(1) = 0.16
        '
        VELO_ASS(1) = 32: VELO_ASS(2) = 40: VELO_ASS(3) = 40
        '
  ElseIf (Diametro > 40) And (Diametro <= 45) Then
        '
        PASS_RET(1) = 4
        INCR_RET(1) = 0.12
        '
        VELO_ASS(1) = 32: VELO_ASS(2) = 40: VELO_ASS(3) = 40
        '
  ElseIf (Diametro > 45) And (Diametro <= 50) Then
        '
        PASS_RET(1) = 4
        INCR_RET(1) = 0.12
        '
        VELO_ASS(1) = 29: VELO_ASS(2) = 40: VELO_ASS(3) = 40
        '
  ElseIf (Diametro > 50) And (Diametro <= 55) Then
        '
        PASS_RET(1) = 4
        INCR_RET(1) = 0.12
        '
        VELO_ASS(1) = 29: VELO_ASS(2) = 40: VELO_ASS(3) = 40
        '
  ElseIf (Diametro > 55) And (Diametro <= 60) Then
        '
        PASS_RET(1) = 4
        INCR_RET(1) = 0.12
        '
        VELO_ASS(1) = 24: VELO_ASS(2) = 35: VELO_ASS(3) = 35
        '
  ElseIf (Diametro > 60) And (Diametro <= 65) Then
        '
        PASS_RET(1) = 4
        INCR_RET(1) = 0.12
        '
        VELO_ASS(1) = 24: VELO_ASS(2) = 35: VELO_ASS(3) = 35
        '
  ElseIf (Diametro > 65) And (Diametro <= 70) Then
        '
        PASS_RET(1) = 4
        INCR_RET(1) = 0.12
        '
        VELO_ASS(1) = 20: VELO_ASS(2) = 30: VELO_ASS(3) = 30
        '
  ElseIf (Diametro > 70) And (Diametro <= 75) Then
        '
        PASS_RET(1) = 4
        INCR_RET(1) = 0.12
        '
        VELO_ASS(1) = 20: VELO_ASS(2) = 30: VELO_ASS(3) = 30
  Else
        '
        PASS_RET(1) = 4
        INCR_RET(1) = 0.1
        '
        VELO_ASS(1) = 20: VELO_ASS(2) = 30: VELO_ASS(3) = 30
  End If
  '
End Sub

Sub CALCOLO_RAGGIO2(ByRef XX() As Double, ByRef YY() As Double, ByRef Np As Integer, CoordX As Double, CoordY As Double, Raggio As Double)
'
Dim i As Long
'
Dim A As Double
Dim B As Double
Dim C As Double
Dim D As Double
Dim E As Double
'
Dim Sx   As Double
Dim Sy   As Double
Dim Sxy  As Double
Dim Sxy2 As Double
Dim Sx2y As Double
Dim Sx2  As Double
Dim Sy2  As Double
Dim Sx3  As Double
Dim Sy3  As Double
'
On Error GoTo errCALCOLO_RAGGIO2
  '
  'SOMMA X
  Sx = 0:   For i = 1 To Np: Sx = Sx + XX(i):                     Next i
  'SOMMA Y
  Sy = 0:   For i = 1 To Np: Sy = Sy + YY(i):                     Next i
  'SOMMA X Y
  Sxy = 0:  For i = 1 To Np: Sxy = Sxy + XX(i) * YY(i):           Next i
  'SOMMA X X
  Sx2 = 0:  For i = 1 To Np: Sx2 = Sx2 + XX(i) * XX(i):           Next i
  'SOMMA X X X
  Sx3 = 0:  For i = 1 To Np: Sx3 = Sx3 + XX(i) * XX(i) * XX(i):   Next i
  'SOMMA X X Y
  Sx2y = 0: For i = 1 To Np: Sx2y = Sx2y + XX(i) * XX(i) * YY(i): Next i
  'SOMMA X Y Y
  Sxy2 = 0: For i = 1 To Np: Sxy2 = Sxy2 + XX(i) * YY(i) * YY(i): Next i
  'SOMMA Y Y
  Sy2 = 0:  For i = 1 To Np: Sy2 = Sy2 + YY(i) * YY(i):           Next i
  'SOMMA Y Y Y
  Sy3 = 0:  For i = 1 To Np: Sy3 = Sy3 + YY(i) * YY(i) * YY(i):   Next i
  'CALCOLO A
  A = Np * Sx2 - Sx ^ 2
  'CALCOLO B
  B = Np * Sxy - Sx * Sy
  'CALCOLO C
  C = Np * Sy2 - Sy ^ 2
  'CALCOLO D
  D = 0.5 * (Np * Sxy2 - Sx * Sy2 + Np * Sx3 - Sx * Sx2)
  'CALCOLO E
  E = 0.5 * (Np * Sx2y - Sy * Sx2 + Np * Sy3 - Sy * Sy2)
  'CALCOLO CoordX
  CoordX = (D * C - B * E) / (A * C - B * B)
  'CALCOLO CoordY
  CoordY = (A * E - B * D) / (A * C - B * B)
  'CALCOLO Raggio
  Raggio = 0
  For i = 1 To Np
    Raggio = Raggio + Sqr((XX(i) - CoordX) ^ 2 + (YY(i) - CoordY) ^ 2)
  Next i
  Raggio = Raggio / Np
  '
  Dim R164 As Double: Dim R165 As Double
  Dim R166 As Double: Dim R167 As Double
  Dim R168 As Double: Dim R169 As Double
  Dim R160 As Double
  '
  R164 = XX(1): R165 = YY(1)
  R166 = XX(Np / 2): R167 = YY(Np / 2)
  R168 = XX(Np): R169 = YY(Np)
  'prodotto misto per senso di rotazione
  R160 = (R166 - R164) * (R169 - R165) - (R168 - R164) * (R167 - R165)
  If (R160 < 0) Then Raggio = -Raggio
  '
Exit Sub

errCALCOLO_RAGGIO2:
  WRITE_DIALOG "Sub CALCOLO_RAGGIO2: ERROR -> " & Err
  Resume Next

End Sub

Sub CALCOLO_CANALE(ByVal PAS As Double, ByVal IMO As Double, ByVal DEV As Double, ByVal DIV As Double, DSW1 As Double, DSW2 As Double, ByVal CORREZIONE_INTERASSE As Double, ByVal CORREZIONE_LARGHEZZA_MOLA As Double, Optional INDICE_MOLA As Integer = 0)
'
Dim ALLONTANAMENTO As Double
  '
  ALLONTANAMENTO = ALLONTANAMENTO + CORREZIONE_INTERASSE
  '
'VARIE
Dim ftmp       As Double
Dim stmp       As String
Dim Atmp       As String * 255
Dim i          As Integer
Dim j          As Integer
Dim COLORE     As Integer
'GRAFICA
Dim ZeroPezzoX As Single
Dim ZeroPezzoY As Single
Dim pX1        As Single
Dim pX2        As Single
Dim pY1        As Single
Dim pY2        As Single
Dim PicH       As Single
Dim PicW       As Single
''DIMENSIONI
'Dim R37        As Double
'Dim R38        As Double
''FONDO MOLA
'Dim R45        As Double
'Dim R55        As Double
''FIANCO 1
'Dim R41        As Double
'Dim R43        As Double
'Dim R44        As Double
'Dim R46        As Double
'Dim R47        As Double
'Dim R48        As Double
'Dim R49        As Double
''FIANCO 2
'Dim R51        As Double
'Dim R53        As Double
'Dim R54        As Double
'Dim R56        As Double
'Dim R57        As Double
'Dim R58        As Double
'Dim R59        As Double
'VARIE
Dim Scala      As Single
Dim ScalaX     As Single
Dim ScalaY     As Single
Dim DF         As Single
Dim AF         As Single
Dim XXp1       As Double
Dim YYp1       As Double
Dim AAP1       As Double
Dim XXp2       As Double
Dim YYp2       As Double
Dim AAP2       As Double
'
Dim XX(0, 99)  As Double
Dim YY(0, 99)  As Double
Dim RR(0, 11)  As Double
'
Dim APF        As Double
Dim AT1        As Double
Dim AT11       As Double
Dim GAMMA      As Double
Dim Delta      As Double
Dim angF       As Double
Dim angC1      As Double
Dim angC11     As Double
Dim DIAG       As Double
Dim ac         As Double
Dim AB         As Double
Dim BC         As Double
Dim RAD        As Double
Dim HF         As Double
Dim dALFA      As Double
Dim ALFA       As Double
Dim Beta       As Double
Dim GAMMA1     As Double
Dim GAMMA11    As Double
'
Dim R184       As Single
Dim R209       As Integer
'
Dim PROFONDITA       As Single
Dim XX_PROFONDITA    As Single
Dim SPOSTAMENTO      As Integer
'
Dim NumeroRette As Integer
Dim da          As Single
Dim DISTANZA    As Double
Dim ANGOLO      As Double
Dim Raggio      As Single
Dim X0          As Single
Dim Y0          As Single
'
Dim Ang         As Single
Dim DeltaAng    As Single
'
Dim XP1         As Single
Dim YP1         As Single
Dim AP1         As Single
'
Dim XP2         As Single
Dim YP2         As Single
Dim AP2         As Single
'
Dim ENTRX       As Double
Dim INF         As Double
'
Dim RXV         As Double
Dim EXV         As Double
'
Dim EXF         As Double
Dim RXF         As Double
'
Dim RM          As Single
'
Dim XmF(2, 9999) As Double
Dim YmF(2, 9999) As Double
Dim TMF(2, 9999) As Double
'
Dim XvF(2, 9999) As Double
Dim YvF(2, 9999) As Double
Dim TvF(2, 9999) As Double
'
Dim Xv(2)  As Double
Dim DhF(2) As Double
Dim IIv(2) As Integer
Dim JJv(2) As Integer
'
Dim k1  As Integer
Dim k2  As Integer
Dim k3  As Integer
Dim k4  As Integer
Dim K5  As Integer
Dim XXc As Double
Dim YYc As Double
Dim RRc As Double
'
Dim chFilettoReale  As Single
'
Dim PZ1 As Single
Dim PZ3 As Single
Dim PZ5 As Single
Dim PZ7 As Single
'
'Valori calcolati considerando i precedenti definiti nella mola
Dim R45ch As Double
Dim R55ch As Double
Dim R46ch As Double
Dim R56ch As Double
'
Dim RIFERIMENTI As Integer
Dim Fianco      As Integer
'
On Error GoTo errCALCOLO_CANALE
  '
  'PARAMETRI MACCHINA
  R184 = 0 'SEMPRE 0 PER IL RULLO
  'LETTURA PARAMETRI MOLA
      If (INDICE_MOLA = 1) Then
    R37 = Lp("MO1R[0,37]"): R38 = Lp("MO1R[0,38]"): R45 = Lp("MO1R[0,45]"): R55 = Lp("MO1R[0,55]")
    R41 = Lp("MO1R[0,41]"): R43 = Lp("MO1R[0,43]"): R44 = Lp("MO1R[0,44]"): R46 = Lp("MO1R[0,46]"): R47 = Lp("MO1R[0,47]"): R48 = Lp("MO1R[0,48]"): R49 = Lp("MO1R[0,49]")
    R51 = Lp("MO1R[0,51]"): R53 = Lp("MO1R[0,53]"): R54 = Lp("MO1R[0,54]"): R56 = Lp("MO1R[0,56]"): R57 = Lp("MO1R[0,57]"): R58 = Lp("MO1R[0,58]"): R59 = Lp("MO1R[0,59]")
  ElseIf (INDICE_MOLA = 2) Then
    R37 = Lp("MO2R[0,37]"): R38 = Lp("MO2R[0,38]"): R45 = Lp("MO2R[0,45]"): R55 = Lp("MO2R[0,55]")
    R41 = Lp("MO2R[0,41]"): R43 = Lp("MO2R[0,43]"): R44 = Lp("MO2R[0,44]"): R46 = Lp("MO2R[0,46]"): R47 = Lp("MO2R[0,47]"): R48 = Lp("MO2R[0,48]"): R49 = Lp("MO2R[0,49]")
    R51 = Lp("MO2R[0,51]"): R53 = Lp("MO2R[0,53]"): R54 = Lp("MO2R[0,54]"): R56 = Lp("MO2R[0,56]"): R57 = Lp("MO2R[0,57]"): R58 = Lp("MO2R[0,58]"): R59 = Lp("MO2R[0,59]")
  ElseIf (INDICE_MOLA = 3) Then
    R37 = Lp("MO3R[0,37]"): R38 = Lp("MO3R[0,38]"): R45 = Lp("MO3R[0,45]"): R55 = Lp("MO3R[0,55]")
    R41 = Lp("MO3R[0,41]"): R43 = Lp("MO3R[0,43]"): R44 = Lp("MO3R[0,44]"): R46 = Lp("MO3R[0,46]"): R47 = Lp("MO3R[0,47]"): R48 = Lp("MO3R[0,48]"): R49 = Lp("MO3R[0,49]")
    R51 = Lp("MO3R[0,51]"): R53 = Lp("MO3R[0,53]"): R54 = Lp("MO3R[0,54]"): R56 = Lp("MO3R[0,56]"): R57 = Lp("MO3R[0,57]"): R58 = Lp("MO3R[0,58]"): R59 = Lp("MO3R[0,59]")
  Else
    R37 = Lp("R[37]"): R38 = Lp("R[38]"): R45 = Lp("R[45]"): R55 = Lp("R[55]")
    R41 = Lp("R[41]"): R43 = Lp("R[43]"): R44 = Lp("R[44]"): R46 = Lp("R[46]"): R47 = Lp("R[47]"): R48 = Lp("R[48]"): R49 = Lp("R[49]")
    R51 = Lp("R[51]"): R53 = Lp("R[53]"): R54 = Lp("R[54]"): R56 = Lp("R[56]"): R57 = Lp("R[57]"): R58 = Lp("R[58]"): R59 = Lp("R[59]")
  End If
  '******************************
  R38 = R38 + CORREZIONE_LARGHEZZA_MOLA
  'CONVERSIONE GRADI --> RADIANTI
  R41 = R41 / 180 * PG
  R47 = R47 / 180 * PG
  'CONVERSIONE GRADI --> RADIANTI
  R51 = R51 / 180 * PG
  R57 = R57 / 180 * PG
  'LARGHEZZA MOLA
  If R37 = 0 Then R37 = R38 + R48 + R58 + 2
  'CavitÓ solo positiva !
  If R45 <= 0.0001 Then R45 = 0.0001
  'GESTIONE CONICITA' FONDO
  R44 = R44 + R55 / 2
  R54 = R54 - R55 / 2
  'Bombatura solo positiva !
  If R46 <= 0.0001 Then R46 = 0.0001
  If R56 <= 0.0001 Then R56 = 0.0001
  '-------------------------------------------------------------
  stmp = GetInfo("MOLA_VITI_CONICHE", "NumeroRette", Path_LAVORAZIONE_INI)
  NumeroRette = val(stmp): If (NumeroRette < 1) Then NumeroRette = 1
  'Lettura della risoluzione per le freccie
  stmp = GetInfo("MOLA_VITI_CONICHE", "DF", Path_LAVORAZIONE_INI):     DF = val(stmp)
  stmp = GetInfo("MOLA_VITI_CONICHE", "AF", Path_LAVORAZIONE_INI):     AF = val(stmp)
  '-------------------------------------------------------------
  stmp = GetInfo("MOLA_VITI_CONICHE", "Scala", Path_LAVORAZIONE_INI)
  Scala = val(stmp): If Scala <= 0 Then Scala = 1
  ScalaX = Scala: ScalaY = Scala
  '-------------------------------------------------------------
  stmp = GetInfo("MOLA_VITI_CONICHE", "RAGGIO", Path_LAVORAZIONE_INI): RM = val(stmp)
  '-------------------------------------------------------------
  stmp = GetInfo("MOLA_VITI_CONICHE", "DhF(1)", Path_LAVORAZIONE_INI): DhF(1) = val(stmp)
  '-------------------------------------------------------------
  stmp = GetInfo("MOLA_VITI_CONICHE", "DhF(2)", Path_LAVORAZIONE_INI): DhF(2) = val(stmp)
  '-------------------------------------------------------------
  stmp = GetInfo("MOLA_VITI_CONICHE", "PicW", Path_LAVORAZIONE_INI):   PicW = val(stmp)
  stmp = GetInfo("MOLA_VITI_CONICHE", "PicH", Path_LAVORAZIONE_INI):   PicH = val(stmp)
  '-------------------------------------------------------------
  stmp = GetInfo("MOLA_VITI_CONICHE", "ZeroPezzoX", Path_LAVORAZIONE_INI):  ZeroPezzoX = val(stmp)
  stmp = GetInfo("MOLA_VITI_CONICHE", "ZeroPezzoY", Path_LAVORAZIONE_INI):  ZeroPezzoY = val(stmp)
  '-------------------------------------------------------------
  '
  'CALCOLO ENTI DEL PROFILO MOLA...............................................................
  'Punto C1
  DIAG = R48 / Cos(PG / 4 - R47 / 2 - R41 / 2)
  YY(0, 31) = -R43 + DIAG * Sin(PG / 4 - R47 / 2 + R41 / 2)
  XX(0, 31) = R38 / 2 + DIAG * Cos(PG / 4 - R47 / 2 + R41 / 2) + R43 * Tan(R41)
  'Punto C11
  DIAG = R58 / Cos(PG / 4 - R57 / 2 - R51 / 2)
  YY(0, 41) = -R53 + DIAG * Sin(PG / 4 - R57 / 2 + R51 / 2)
  XX(0, 41) = -R38 / 2 - DIAG * Cos(PG / 4 - R57 / 2 + R51 / 2) - R53 * Tan(R51)
  'Punto P2
  XX(0, 2) = XX(0, 31) - R48 * Sin(R47)
  YY(0, 2) = YY(0, 31) - R48 * Cos(R47)
  'Punto P12
  XX(0, 12) = XX(0, 41) + R58 * Sin(R57)
  YY(0, 12) = YY(0, 41) - R58 * Cos(R57)
  'Punto P1
  XX(0, 1) = R37 / 2
  YY(0, 1) = YY(0, 2) - (XX(0, 1) - XX(0, 2)) * Tan(R47)
  'Punto P11
  XX(0, 11) = -R37 / 2
  YY(0, 11) = YY(0, 12) - (XX(0, 12) - XX(0, 11)) * Tan(R57)
  'Punto C3
  YY(0, 33) = R44 - R43 - R49
  XX(0, 33) = R38 / 2 - R49 * Tan((PG / 2 - R41) / 2) - (R44 - R43) * Tan(R41)
  'Punto C13
  YY(0, 43) = R54 - R53 - R59
  XX(0, 43) = -R38 / 2 + R59 * Tan((PG / 2 - R51) / 2) + (R54 - R53) * Tan(R51)
  'Calcolo R-bombatura
  'F1
  HF = (YY(0, 33) - YY(0, 31) + (R48 + R49) * Sin(R41)) / Cos(R41) / 2
  RR(0, 1) = (HF ^ 2 + R46 ^ 2) / 2 / R46
  ALFA = Atn((YY(0, 33) - YY(0, 31)) / (XX(0, 31) - XX(0, 33)))
  ac = RR(0, 1) - R49
  BC = RR(0, 1) + R48
  AB = (XX(0, 31) - XX(0, 33)) / Cos(ALFA)
  RAD = ((ac ^ 2 + AB ^ 2 - BC ^ 2) / (2 * AB * ac))
  If (Abs(RAD) > 1) Then
    StopRegieEvents
    MsgBox "CALCULATION NOT POSSIBLE: FLANK CROWN F1 TOO BIG", 32, "SUB CALCOLO_CANALE"
    ResumeRegieEvents
    Exit Sub
  End If
  Beta = FnACS(RAD)
  If (Beta < 0) Then Beta = PG + Beta
  GAMMA1 = PG - (ALFA + Beta)
  'F2
  HF = (YY(0, 43) - YY(0, 41) + (R58 + R59) * Sin(R51)) / Cos(R51) / 2
  RR(0, 11) = (HF ^ 2 + R56 ^ 2) / 2 / R56
  ALFA = Atn((YY(0, 43) - YY(0, 41)) / (XX(0, 43) - XX(0, 41)))
  ac = RR(0, 11) - R59
  BC = RR(0, 11) + R58
  AB = (XX(0, 43) - XX(0, 41)) / Cos(ALFA)
  RAD = ((ac ^ 2 + AB ^ 2 - BC ^ 2) / (2 * AB * ac))
  If (Abs(RAD) > 1) Then
    StopRegieEvents
    MsgBox "CALCULATION NOT POSSIBLE:" & Chr(13) & "FLANK CROWN F2 TOO BIG", 32, "SUB CALCOLO_CANALE"
    ResumeRegieEvents
    Exit Sub
  End If
  Beta = FnACS(RAD)
  If (Beta < 0) Then Beta = PG + Beta
  GAMMA11 = PG - (ALFA + Beta)
  'Punto C2
  XX(0, 32) = XX(0, 33) - (RR(0, 1) - R49) * Cos(GAMMA1)
  YY(0, 32) = YY(0, 33) - (RR(0, 1) - R49) * Sin(GAMMA1)
  AT1 = Atn((YY(0, 31) - YY(0, 32)) / (XX(0, 31) - XX(0, 32)))
  If (AT1 < 0) Then
    StopRegieEvents
    MsgBox "CALCULATION NOT POSSIBLE:" & Chr(13) & "NEGATIVE FLANK 1 " & FnRG(AT1), 32, "SUB CALCOLO_CANALE"
    ResumeRegieEvents
    Exit Sub
  End If
  'Punto C12
  XX(0, 42) = XX(0, 43) + (RR(0, 11) - R59) * Cos(GAMMA11)
  YY(0, 42) = YY(0, 43) - (RR(0, 11) - R59) * Sin(GAMMA11)
  AT11 = Atn((YY(0, 41) - YY(0, 42)) / (XX(0, 42) - XX(0, 41)))
  If (AT11 < 0) Then
    StopRegieEvents
    MsgBox "CALCULATION NOT POSSIBLE:" & Chr(13) & "NEGATIVE FLANK 2 " & FnRG(AT11), 32, "SUB CALCOLO_CANALE"
    ResumeRegieEvents
    Exit Sub
  End If
  'Punto P4
  XX(0, 4) = XX(0, 31) - R48 * Cos(AT1)
  YY(0, 4) = YY(0, 31) - R48 * Sin(AT1)
  'Punto P5
  XX(0, 5) = XX(0, 33) + R49 * Cos(GAMMA1)
  YY(0, 5) = YY(0, 33) + R49 * Sin(GAMMA1)
  'Punto P14
  XX(0, 14) = XX(0, 41) + R58 * Cos(AT11)
  YY(0, 14) = YY(0, 41) - R58 * Sin(AT11)
  'Punto P15
  XX(0, 15) = XX(0, 43) - R59 * Cos(GAMMA11)
  YY(0, 15) = YY(0, 43) + R59 * Sin(GAMMA11)
  '
  'SEGMENTAZIONE DEL PROFILO MOLA..............................................................
  'Spalla
  'F1
  dALFA = Sqr((XX(0, 2) - XX(0, 1)) ^ 2 + (YY(0, 2) - YY(0, 1)) ^ 2) / NumeroRette
  For i = 0 To NumeroRette - 1
    j = (i + 1) + 0 * NumeroRette
    APF = R47
    XmF(1, j) = XX(0, 1) - i * dALFA * Cos(APF)
    YmF(1, j) = YY(0, 1) + i * dALFA * Sin(APF)
    TMF(1, j) = PG / 2 - APF
  Next i
  'F2
  dALFA = Sqr((XX(0, 12) - XX(0, 11)) ^ 2 + (YY(0, 12) - YY(0, 11)) ^ 2) / NumeroRette
  For i = 0 To NumeroRette - 1
    j = (i + 1) + 0 * NumeroRette
    APF = R57
    XmF(2, j) = XX(0, 11) + i * dALFA * Cos(APF)
    YmF(2, j) = YY(0, 11) + i * dALFA * Sin(APF) - R184
    TMF(2, j) = PG / 2 - APF
  Next i
  'Calcolo R-TESTA
  'F1
  dALFA = (PG / 2 - AT1 - R47) / NumeroRette
  For i = 0 To NumeroRette - 1
    j = (i + 1) + 1 * NumeroRette
    APF = R47 + i * dALFA
    XmF(1, j) = XX(0, 31) - R48 * Sin(APF)
    YmF(1, j) = YY(0, 31) - R48 * Cos(APF)
    TMF(1, j) = PG / 2 - APF
  Next i
  'F2
  dALFA = (PG / 2 - AT11 - R57) / NumeroRette
  For i = 0 To NumeroRette - 1
    j = (i + 1) + 1 * NumeroRette
    APF = R57 + i * dALFA
    XmF(2, j) = XX(0, 41) + R58 * Sin(APF)
    YmF(2, j) = YY(0, 41) - R58 * Cos(APF) - R184
    TMF(2, j) = -(PG / 2 - APF)
  Next i
  'Calcolo Fianco
  'F1
  dALFA = (GAMMA1 - AT1) / NumeroRette
  For i = 0 To NumeroRette - 1
    j = (i + 1) + 2 * NumeroRette
    APF = AT1 + i * dALFA
    XmF(1, j) = XX(0, 32) + RR(0, 1) * Cos(APF)
    YmF(1, j) = YY(0, 32) + RR(0, 1) * Sin(APF)
    TMF(1, j) = APF
  Next i
  'F2
  dALFA = (GAMMA11 - AT11) / NumeroRette
  For i = 0 To NumeroRette - 1
    j = (i + 1) + 2 * NumeroRette
    APF = AT11 + i * dALFA
    XmF(2, j) = XX(0, 42) - RR(0, 11) * Cos(APF)
    YmF(2, j) = YY(0, 42) + RR(0, 11) * Sin(APF) - R184
    TMF(2, j) = -APF
  Next i
  'Calcolo R-fondo
  RR(0, 0) = (((XX(0, 33) - XX(0, 43)) / 2) ^ 2 + R45 ^ 2) / 2 / R45
  AB = RR(0, 0) + R59
  ac = RR(0, 0) + R49
  BC = Sqr((XX(0, 33) - XX(0, 43)) ^ 2 + (YY(0, 43) - YY(0, 33)) ^ 2)
  GAMMA = Atn((YY(0, 33) - YY(0, 43)) / (XX(0, 33) - XX(0, 43)))
  Delta = FnACS((AB ^ 2 + BC ^ 2 - ac ^ 2) / (2 * AB * BC))
  angC11 = PG / 2 - Delta - GAMMA
  XX(0, 34) = XX(0, 43) + (RR(0, 0) + R59) * Sin(angC11)
  YY(0, 34) = YY(0, 43) + (RR(0, 0) + R59) * Cos(angC11)
  angC1 = Atn((XX(0, 33) - XX(0, 34)) / (YY(0, 34) - YY(0, 33)))
  angC11 = Atn((XX(0, 34) - XX(0, 43)) / (YY(0, 34) - YY(0, 43)))
  'Punto P6
  XX(0, 6) = XX(0, 34) + RR(0, 0) * Sin(angC1)
  YY(0, 6) = YY(0, 34) - RR(0, 0) * Cos(angC1)
  'Punto P16
  XX(0, 16) = XX(0, 34) - RR(0, 0) * Sin(angC11)
  YY(0, 16) = YY(0, 34) - RR(0, 0) * Cos(angC11)
  'Punto P7
  XX(0, 7) = 0
  YY(0, 7) = YY(0, 34) - RR(0, 0)
  'Punto P17
  XX(0, 17) = XX(0, 6) + 1
  YY(0, 17) = YY(0, 34) - Sqr(RR(0, 0) ^ 2 - (XX(0, 17) - XX(0, 34)) ^ 2)
  'F1
  dALFA = (PG / 2 - GAMMA1 + angC1) / NumeroRette
  For i = 0 To NumeroRette - 1
    j = (i + 1) + 3 * NumeroRette
    APF = GAMMA1 + i * dALFA
    XmF(1, j) = XX(0, 33) + R49 * Cos(APF)
    YmF(1, j) = YY(0, 33) + R49 * Sin(APF)
    TMF(1, j) = APF
  Next i
  'F2
  dALFA = (PG / 2 - GAMMA11 + angC11) / NumeroRette
  For i = 0 To NumeroRette - 1
    j = (i + 1) + 3 * NumeroRette
    APF = GAMMA11 + i * dALFA
    XmF(2, j) = XX(0, 43) - R59 * Cos(APF)
    YmF(2, j) = YY(0, 43) + R59 * Sin(APF) - R184
    TMF(2, j) = -APF
  Next i
  k1 = NumeroRette / 2
  k2 = NumeroRette / 2
  While k1 + k2 < NumeroRette
    k2 = k2 + 1
  Wend
  'Calcolo Fondo FIANCO 1
  dALFA = (angC1 + angC11) / NumeroRette
  For i = 0 To k1 - 1
    j = (i + 1) + 0 * NumeroRette
    angF = angC1 - i * dALFA
    XmF(0, j) = XX(0, 34) + RR(0, 0) * Sin(angF)
    YmF(0, j) = YY(0, 34) - RR(0, 0) * Cos(angF) - R184
    TMF(0, j) = PG / 2 - angF
  Next i
  'Calcolo Fondo FIANCO 2
  For i = 0 To k2 - 1
    j = k1 + (i + 1) + 0 * NumeroRette
    angF = angC1 - k1 * dALFA - i * dALFA
    XmF(0, j) = XX(0, 34) + RR(0, 0) * Sin(angF)
    YmF(0, j) = YY(0, 34) - RR(0, 0) * Cos(angF) - R184
    TMF(0, j) = PG / 2 - angF
  Next i
  i = k2
    j = k1 + (i + 1) + 0 * NumeroRette
    angF = angC1 - k1 * dALFA - i * dALFA
    XmF(0, j) = XX(0, 34) + RR(0, 0) * Sin(angF)
    YmF(0, j) = YY(0, 34) - RR(0, 0) * Cos(angF) - R184
    TMF(0, j) = PG / 2 - angF
  'PUNTO SUL DIAMETRO INTERNO VITE
  PROFONDITA = YY(0, 7): XX_PROFONDITA = XX(0, 7)
  'CALCOLO CANALE..............................................................................
  'GRADI --> RADIANTI
  INF = FnGR(90 - IMO)
  'CALCOLO INTERASSE DI LAVORO
  ENTRX = RM + DIV / 2
  Do
    APF = PG / 2 - 0.001
    RXF = RM
    EXF = XX_PROFONDITA
    GoSub CALRA
    If Abs(DIV / 2 - RXV) > 0.001 Then
      ENTRX = ENTRX + (DIV / 2 - RXV) / 2
    Else
      Exit Do
    End If
  Loop
  'CORREGGO DELLA CAVITA DI TESTA DELLA MOLA: RM -> PUNTO SPORGENTE MOLA
  PROFONDITA = PROFONDITA + R45
  'ALLONTANAMENTO DELLA MOLA
  ENTRX = ENTRX + ALLONTANAMENTO
  ENTRX = ENTRX - R45
  'CALCOLO CANALE
  For Fianco = 1 To 2
    'SPALLA --> CANALE
    For i = 1 To NumeroRette
      j = i + 0 * NumeroRette
      APF = TMF(Fianco, j)
      RXF = RM - PROFONDITA + YmF(Fianco, j)
      EXF = XmF(Fianco, j)
      GoSub CALRA
      TvF(Fianco, j) = 0
      XvF(Fianco, j) = EXV
      YvF(Fianco, j) = RXV
    Next i
    'RAGGIO DI FONDO MOLA --> CANALE
    For i = 1 To NumeroRette
      j = i + 1 * NumeroRette
      APF = TMF(Fianco, j)
      RXF = RM - PROFONDITA + YmF(Fianco, j)
      EXF = XmF(Fianco, j)
      GoSub CALRA
      TvF(Fianco, j) = 0
      XvF(Fianco, j) = EXV
      YvF(Fianco, j) = RXV
    Next i
    'FIANCO MOLA --> CANALE
    For i = 1 To NumeroRette
      j = i + 2 * NumeroRette
      APF = TMF(Fianco, j)
      RXF = RM - PROFONDITA + YmF(Fianco, j)
      EXF = XmF(Fianco, j)
      GoSub CALRA
      TvF(Fianco, j) = 0
      XvF(Fianco, j) = EXV
      YvF(Fianco, j) = RXV
    Next i
    'RAGGIO DI TESTA MOLA --> CANALE
    For i = 1 To NumeroRette
      j = i + 3 * NumeroRette
      APF = TMF(Fianco, j)
      RXF = RM - PROFONDITA + YmF(Fianco, j)
      EXF = XmF(Fianco, j)
      GoSub CALRA
      TvF(Fianco, j) = 0
      XvF(Fianco, j) = EXV
      YvF(Fianco, j) = RXV
    Next i
  Next Fianco
  'CALCOLO LARGHEZZA CANALE EFETTTIVA
  For Fianco = 1 To 2
    For i = 1 To 4 * NumeroRette
      'INIZIO FIANCO DEL CANALE DAL DIAMETRO ESTERNO
      If (YvF(Fianco, i) >= DEV / 2) And (YvF(Fianco, i + 1) <= DEV / 2) Then
        Xv(Fianco) = XvF(Fianco, i + 1) + (XvF(Fianco, i) - XvF(Fianco, i + 1)) * (DEV / 2 - YvF(Fianco, i + 1)) / (YvF(Fianco, i) - YvF(Fianco, i + 1))
        IIv(Fianco) = i + 1
      End If
      'FINE FIANCO DEL CANALE DAL DIAMETRO INTERNO
      If (YvF(Fianco, i) > DIV / 2 + ALLONTANAMENTO + DhF(Fianco)) And (YvF(Fianco, i + 1) < (DIV / 2 + ALLONTANAMENTO + DhF(Fianco))) Then
        JJv(Fianco) = i
      End If
    Next i
  Next Fianco
  '
  'ASSEGNO LE VARIABILI DA RITORNARE
  DSW1 = Abs(Xv(1)): DSW2 = Abs(Xv(2))
  '
  'TESTA DELLA MOLA --> FONDO DEL CANALE
  For i = 1 To NumeroRette + 1
    j = i
    APF = TMF(0, j)
    RXF = RM - PROFONDITA + YmF(0, j)
    EXF = XmF(0, j)
    GoSub CALRA
    TvF(0, j) = 0
    XvF(0, j) = EXV
    YvF(0, j) = RXV
  Next i
  'PROFONDITA'
  stmp = frmt(DEV / 2 - DIV / 2, 4)
  'LARGHEZZA CANALE
  stmp = frmt(Xv(1) - Xv(2), 3)
  'SPESSORE FILETTO
  chFilettoReale = PAS - (Xv(1) - Xv(2))
  stmp = frmt(chFilettoReale, 3)
  'CALCOLO ANGOLO SUI FIANCHI
  Dim sXh   As Double
  Dim sYh   As Double
  Dim sXhXh As Double
  Dim sXhYh As Double
  Dim B     As Double
  Dim A     As Double
  Dim CoeffAngolare(2) As Double
  Dim NumeroPunti As Integer
  Dim iitmp           As Integer
  Dim XXtmp()         As Double
  Dim YYtmp()         As Double
  '
  For Fianco = 1 To 2
    '--------------------------------------------------------------------
    If (IIv(Fianco) > 3 * NumeroRette) Then
      'TUTTI I PUNTI DEL FIANCO DEL CANALE DESCRITTO DAL FIANCO MOLA
      sXh = 0:   For i = 1 + 2 * NumeroRette To 3 * NumeroRette: sXh = sXh + YvF(Fianco, i):                      Next i
      sYh = 0:   For i = 1 + 2 * NumeroRette To 3 * NumeroRette: sYh = sYh + XvF(Fianco, i):                      Next i
      sXhXh = 0: For i = 1 + 2 * NumeroRette To 3 * NumeroRette: sXhXh = sXhXh + YvF(Fianco, i) * YvF(Fianco, i): Next i
      sXhYh = 0: For i = 1 + 2 * NumeroRette To 3 * NumeroRette: sXhYh = sXhYh + YvF(Fianco, i) * XvF(Fianco, i): Next i
      NumeroPunti = NumeroRette
    Else
      If (DhF(Fianco) > 0) Then
       'DAL DIAMETRO ESTERNO AD UNA DISTANZA FISSA DAL DIAMETRO INTERNO
       sXh = 0:   For i = IIv(Fianco) To JJv(Fianco): sXh = sXh + YvF(Fianco, i):                      Next i
       sYh = 0:   For i = IIv(Fianco) To JJv(Fianco): sYh = sYh + XvF(Fianco, i):                      Next i
       sXhXh = 0: For i = IIv(Fianco) To JJv(Fianco): sXhXh = sXhXh + YvF(Fianco, i) * YvF(Fianco, i): Next i
       sXhYh = 0: For i = IIv(Fianco) To JJv(Fianco): sXhYh = sXhYh + YvF(Fianco, i) * XvF(Fianco, i): Next i
       NumeroPunti = JJv(Fianco) - IIv(Fianco) + 1
      Else
       'DAL DIAMETRO ESTERNO ALLA FINE DEL FIANCO MOLA
       sXh = 0:   For i = IIv(Fianco) To 3 * NumeroRette: sXh = sXh + YvF(Fianco, i):                      Next i
       sYh = 0:   For i = IIv(Fianco) To 3 * NumeroRette: sYh = sYh + XvF(Fianco, i):                      Next i
       sXhXh = 0: For i = IIv(Fianco) To 3 * NumeroRette: sXhXh = sXhXh + YvF(Fianco, i) * YvF(Fianco, i): Next i
       sXhYh = 0: For i = IIv(Fianco) To 3 * NumeroRette: sXhYh = sXhYh + YvF(Fianco, i) * XvF(Fianco, i): Next i
       NumeroPunti = 3 * NumeroRette - IIv(Fianco) + 1
      End If
    End If
    'Coefficiente angolare della retta di regressione
    B = (NumeroPunti * sXhXh - sXh * sXh)
    If (B = 0) Then B = 0.001
    B = (NumeroPunti * sXhYh - sXh * sYh) / B
    'VISUALIZZAZIONE E CONVERSIONE IN GRADI
    B = Atn(B) * 180 / PG * (-Fianco + 1.5) * 2
    CoeffAngolare(Fianco) = B
    '--------------------------------------------------------------------
    stmp = "R" & Format$(Fianco + 3, "0") & "1ch= " & frmt(B, 3)
  Next Fianco
  'VISUALIZZO VALORE DEL PASSO
  stmp = "Pitch= " & frmt(PAS, 3)
  'VISUALIZZO VALORE DELL'ELICA
  stmp = "Wheel tilt= " & frmt(IMO, 3)
  '
  'CALCOLO CAVITA' DI FONDO: R45ch
  DISTANZA = Sqr((XvF(0, 1) - XvF(0, NumeroRette + 1)) ^ 2 + (YvF(0, 1) - YvF(0, NumeroRette + 1)) ^ 2)
  Fianco = 0
  k1 = 1
  k2 = (NumeroRette + 1) / 2
  k3 = (NumeroRette + 1)
  XXc = 0: YYc = 0: RRc = 10000000
  'VECCHIO METODO
  'Call CALCOLO_RAGGIO1(XvF(Fianco, k1), YvF(Fianco, k1), XvF(Fianco, K2), YvF(Fianco, K2), XvF(Fianco, k3), YvF(Fianco, k3), XXc, YYc, RRc)
  'NUOVO METODO
  iitmp = 0
  For i = 1 To NumeroRette + 1
    iitmp = iitmp + 1
    ReDim Preserve XXtmp(iitmp):    ReDim Preserve YYtmp(iitmp)
    XXtmp(iitmp) = XvF(Fianco, i):  YYtmp(iitmp) = YvF(Fianco, i)
  Next i
  Call CALCOLO_RAGGIO2(XXtmp, YYtmp, iitmp, XXc, YYc, RRc)
  R45ch = RRc * (1 - Sqr(1 - (DISTANZA / 2 / RRc) ^ 2))
  stmp = " R45ch= " & frmt(R45ch, 3) & " "
  '
  'CALCOLO DELLA CONICITA DI FONDO: R55ch
  '--------------------------------------------------------------------
  sXh = 0:   For i = 1 To NumeroRette + 1: sXh = sXh + XvF(0, i):                 Next i
  sYh = 0:   For i = 1 To NumeroRette + 1: sYh = sYh + YvF(0, i):                 Next i
  sXhXh = 0: For i = 1 To NumeroRette + 1: sXhXh = sXhXh + XvF(0, i) * XvF(0, i): Next i
  sXhYh = 0: For i = 1 To NumeroRette + 1: sXhYh = sXhYh + XvF(0, i) * YvF(0, i): Next i
  'Coefficiente angolare della retta di regressione
  B = ((NumeroRette + 1) * sXhYh - sXh * sYh) / ((NumeroRette + 1) * sXhXh - sXh * sXh)
  '--------------------------------------------------------------------
  'stmp = "R55= " & frmt(Atn(b) * 180 / PG, 3)
  'CONVERSIONE IN mm
  R55ch = B * (XvF(0, NumeroRette + 1) - XvF(0, 1))
  '--------------------------------------------------------------------
  stmp = " R55ch= " & frmt(R55ch, 3) & " "
  If (ALLONTANAMENTO <> 0) Then
    'ALLONTANAMENTO
    stmp = frmt(ALLONTANAMENTO, 4)
    If (CORREZIONE_INTERASSE <> 0) Then
      'PROFONFITA
      stmp = frmt(PROFONDITA - ALLONTANAMENTO, 4)
    End If
  End If
  '
  Dim RAGGIO_MEDIO(2) As Double
  '
  For Fianco = 1 To 2
    RAGGIO_MEDIO(Fianco) = 0
    If (DhF(Fianco) > 0) Then
      'COMPLETO
      'k1 = JJv(Fianco)
      'K2 = (JJv(Fianco) + 4 * NumeroRette) / 2
      'k3 = 4 * NumeroRette
      'XXc = 0: YYc = 0: RRc = 0
      'VECCHIO METODO
      'Call CALCOLO_RAGGIO1(XvF(Fianco, k1), YvF(Fianco, k1), XvF(Fianco, K2), YvF(Fianco, K2), XvF(Fianco, k3), YvF(Fianco, k3), XXc, YYc, RRc)
      'RAGGIO_MEDIO(Fianco) = RAGGIO_MEDIO(Fianco) + Abs(RRc)
      'NUOVO METODO
      iitmp = 0
      XXc = 0: YYc = 0: RRc = 0
      For i = JJv(Fianco) To 4 * NumeroRette
        iitmp = iitmp + 1
        ReDim Preserve XXtmp(iitmp):    ReDim Preserve YYtmp(iitmp)
        XXtmp(iitmp) = XvF(Fianco, i):  YYtmp(iitmp) = YvF(Fianco, i)
      Next i
      Call CALCOLO_RAGGIO2(XXtmp, YYtmp, iitmp, XXc, YYc, RRc)
      'MEDIO
      RAGGIO_MEDIO(Fianco) = Abs(RRc)
    Else
      'VECCHIO METODO
      'SUPERIORE
      'k1 = 3 * NumeroRette + 0 * NumeroRette / 4
      'K2 = 3 * NumeroRette + 1 * NumeroRette / 4
      'k3 = 3 * NumeroRette + 2 * NumeroRette / 4
      'XXc = 0: YYc = 0: RRc = 0
      'Call CALCOLO_RAGGIO1(XvF(Fianco, k1), YvF(Fianco, k1), XvF(Fianco, K2), YvF(Fianco, K2), XvF(Fianco, k3), YvF(Fianco, k3), XXc, YYc, RRc)
      'RAGGIO_MEDIO(Fianco) = RAGGIO_MEDIO(Fianco) + Abs(RRc)
      'INFERIORE
      'k1 = 3 * NumeroRette + 2 * NumeroRette / 4
      'K2 = 3 * NumeroRette + 3 * NumeroRette / 4
      'k3 = 3 * NumeroRette + 4 * NumeroRette / 4
      'XXc = 0: YYc = 0: RRc = 0
      'Call CALCOLO_RAGGIO1(XvF(Fianco, k1), YvF(Fianco, k1), XvF(Fianco, K2), YvF(Fianco, K2), XvF(Fianco, k3), YvF(Fianco, k3), XXc, YYc, RRc)
      'RAGGIO_MEDIO(Fianco) = RAGGIO_MEDIO(Fianco) + Abs(RRc)
      'MEDIO
      'RAGGIO_MEDIO(Fianco) = RAGGIO_MEDIO(Fianco) / 2
      'NUOVO METODO
      iitmp = 0
      XXc = 0: YYc = 0: RRc = 0
      For i = 3 * NumeroRette To 4 * NumeroRette
        iitmp = iitmp + 1
        ReDim Preserve XXtmp(iitmp):    ReDim Preserve YYtmp(iitmp)
        XXtmp(iitmp) = XvF(Fianco, i):  YYtmp(iitmp) = YvF(Fianco, i)
      Next i
      Call CALCOLO_RAGGIO2(XXtmp, YYtmp, iitmp, XXc, YYc, RRc)
      'MEDIO
      RAGGIO_MEDIO(Fianco) = Abs(RRc)
    End If
    'RAGGI DI FONDO DEL CANALE
    stmp = "RF" & Format$(Fianco, "0") & "ch= " & frmt(RAGGIO_MEDIO(Fianco), 2)
  Next Fianco
  '
  'CALCOLO BOMBATURA SUI FIANCHI
  For Fianco = 1 To 2
    'LUNGHEZZA DEL FIANCO
    DISTANZA = Sqr((Xv(Fianco) - XvF(Fianco, 3 * NumeroRette)) ^ 2 + (DEV / 2 - YvF(Fianco, 3 * NumeroRette)) ^ 2)
    'RAGGIO SUL FIANCO
    If (DhF(Fianco) <= 0) Then
      K5 = (IIv(Fianco) - 2 * NumeroRette)
      K5 = Int(K5 / 2) * 2
      k4 = Int(NumeroRette / 2) * 2 + 1
      k1 = 2 * NumeroRette + K5
      k2 = 2 * NumeroRette + (K5 + k4) / 2
      k3 = 2 * NumeroRette + k4
    Else
      k1 = IIv(Fianco)
      k2 = Int((IIv(Fianco) + JJv(Fianco)) / 2)
      k3 = JJv(Fianco)
    End If
    XXc = 0: YYc = 0: RRc = 10000000
    Call CALCOLO_RAGGIO1(Xv(Fianco), DEV / 2, XvF(Fianco, k2), YvF(Fianco, k2), XvF(Fianco, k3), YvF(Fianco, k3), XXc, YYc, RRc)
    'CAVITA' SUL FIANCO DEL CANALE
    If (Fianco = 1) Then
      R46ch = -RRc * (1 - Sqr(1 - (DISTANZA / 2 / RRc) ^ 2))
      stmp = " CF" & Format$(Fianco, "0") & "ch= " & frmt(R46ch, 3) & " "
    End If
    If (Fianco = 2) Then
      R56ch = RRc * (1 - Sqr(1 - (DISTANZA / 2 / RRc) ^ 2))
      stmp = " CF" & Format$(Fianco, "0") & "ch= " & frmt(R56ch, 3) & " "
    End If
  Next Fianco
  '
  'VISUALIZZO IL DIAMETRO ESTERNO
  stmp = " De= " & frmt(DEV, 4)
  'VISUALIZZO IL DIAMETRO INTERNO
  stmp = " Di= " & frmt(DIV + CORREZIONE_INTERASSE * 2, 4)
  'DISTANZE DAL CENTRO MOLA
  For Fianco = 1 To 2
    stmp = "L" & Format$(Fianco, "0") & "ch= " & frmt(Abs(Xv(Fianco)), 3)
  Next Fianco
  'DISTANZE DAI FIANCHI MOLA
  stmp = " D1ch= " & frmt(Xv(1) - XvF(0, 1), 3)
  stmp = " D2ch= " & frmt(-Xv(2) + XvF(0, NumeroRette + 1), 3)
  'SPESSORE FILETTO NORMALE REALE
  stmp = " FLIGHT (n)= " & frmt(chFilettoReale * Cos(IMO * PG / 180), 3)
  'CALCOLO AREA RISPETTO AL DIAMETRO ESTERNO
  Dim AREA(2) As Double
  For Fianco = 1 To 2
    AREA(Fianco) = 0
    pX1 = Xv(Fianco)
    pY1 = (DEV / 2) - (DEV / 2)
    pX2 = XvF(Fianco, IIv(Fianco))
    pY2 = (DEV / 2) - YvF(Fianco, IIv(Fianco))
    AREA(Fianco) = AREA(Fianco) + Abs(pX1 - pX2) * (pY1 + pY2) / 2
    For i = IIv(Fianco) To 3 * NumeroRette - 1
      pX1 = XvF(Fianco, i + 0)
      pY1 = (DEV / 2) - YvF(Fianco, i + 0)
      pX2 = XvF(Fianco, i + 1)
      pY2 = (DEV / 2) - YvF(Fianco, i + 1)
      AREA(Fianco) = AREA(Fianco) + Abs(pX1 - pX2) * (pY1 + pY2) / 2
    Next i
    For i = 3 * NumeroRette To 4 * NumeroRette - 1
      pX1 = XvF(Fianco, i + 0)
      pY1 = (DEV / 2) - YvF(Fianco, i + 0)
      pX2 = XvF(Fianco, i + 1)
      pY2 = (DEV / 2) - YvF(Fianco, i + 1)
      AREA(Fianco) = AREA(Fianco) + Abs(pX1 - pX2) * (pY1 + pY2) / 2
    Next i
  Next Fianco
  Fianco = 0
    AREA(Fianco) = 0
    For i = 1 To NumeroRette
      pX1 = XvF(Fianco, i + 0)
      pY1 = (DEV / 2) - YvF(Fianco, i + 0)
      pX2 = XvF(Fianco, i + 1)
      pY2 = (DEV / 2) - YvF(Fianco, i + 1)
      AREA(Fianco) = AREA(Fianco) + Abs(pX1 - pX2) * (pY1 + pY2) / 2
    Next i
  stmp = " AREA= " & frmt(AREA(1) + AREA(0) + AREA(2), 3)
  'ESTENSIONE DEL FONDO
  stmp = "FLAT= " & frmt(XvF(0, 1) - XvF(0, NumeroRette + 1), 3) & " "
  'DISTANZA CENTRI DEI CERCHI DEI RAGGI DI FONDO MOLA
  stmp = GetInfo("MOLA_VITI_CONICHE", "DXfm", Path_LAVORAZIONE_INI)
  stmp = " DXfm= " & stmp & " "
  '
Exit Sub

CALRA:
Dim v7  As Double
Dim v8  As Double
Dim V9  As Double
Dim V12 As Double
Dim V13 As Double
Dim V14 As Double
Dim V15 As Double
Dim V16 As Double
Dim VPB As Double
  '************** S/P ************ CALCUL PIECE D'APRES FRAISE
  ' Entrees  constantes : ENTRX , PAS , INF
  ' RXF=###.###  EXF=###.###  APF=###.###"; RXF; EXF; FNRD(APF)
  ' Sorties             : RXV , EXV
  RXV = DEV / 2 + 0.1
  v7 = PAS / 2 / PG
  V12 = -Tan(APF) * (ENTRX * Sin(INF) + v7 * Cos(INF))
  V13 = v7 * Sin(INF) - ENTRX * Cos(INF)
  V14 = Cos(INF) * ((-Tan(APF) * EXF) + RXF)
  V15 = (V14 ^ 2) + (V12 ^ 2) - (V13 ^ 2)
  If (V15 > 0) Then
    V15 = Sqr(V15)
    V16 = -(V15 + V12) / (V13 - V14)
    V15 = (V15 - V12) / (V13 - V14)
    VPB = V16
    If (Abs(V15) < Abs(V16)) Then VPB = V15
    V12 = 2 * Atn(VPB)
    V13 = (RXF * Sin(INF) * Sin(V12)) + EXF * Cos(INF)
    V13 = Atn(V13 / ((RXF * Cos(V12)) - ENTRX))
    RXV = (ENTRX - RXF * Cos(V12)) / Cos(V13)
    EXV = EXF * Sin(INF) - v7 * V13 - RXF * Sin(V12) * Cos(INF)
    Ang = 2 * PG * EXV / PAS  'BETA
  Else
    If (RXV <= (DEV / 2)) Then
      StopRegieEvents
      MsgBox "CALCULATION NOT POSSIBLE:" & Chr(13) & "RXV <= (DEV / 2)", 32, "SUB CALCOLO_CANALE"
      ResumeRegieEvents
      Exit Sub
    End If
  End If
Return

errCALCOLO_CANALE:
  WRITE_DIALOG "Sub CALCOLO_CANALE: ERROR -> " & Err
Resume Next
  
End Sub

Sub CALCOLO_SPOSTAMENTI_VITI_PASSO_VARIABILE()
'
Dim stmp             As String
Dim i                As Integer
Dim ret              As Integer
Dim TIPO_LAVORAZIONE As String
Dim sERRORE          As String
Dim R(999)           As Double
Dim LS1_1_C          As Double
Dim LS1_2_C          As Double
Dim LS1_1_L          As Double
Dim LS1_2_L          As Double
Dim LS3_1_C          As Double
Dim LS3_2_C          As Double
Dim LS3_1_L          As Double
Dim LS3_2_L          As Double
Dim NomeTipo         As String
'
On Error GoTo errCALCOLO_SPOSTAMENTI_VITI_PASSO_VARIABILE
  '
  'LEGGO IL TIPO DI LAVORAZIONE
  TIPO_LAVORAZIONE = GetInfo("CONFIGURAZIONE", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  NomeTipo = RICERCA_NOME_TIPO(TIPO_LAVORAZIONE)
  WRITE_DIALOG NomeTipo & ": EVALUATING SHITF."
  '
  'CALCOLO I PARAMETRI SHIFT DELLA MOLA LARGA
  'LETTURA PARAMETRI DI LAVORO
  R(214) = Abs(Lp("R[214]")) 'PASSO 1
  R(215) = Abs(Lp("R[215]")) 'PASSO 2
  R(124) = Abs(Lp("R[124]")) 'INCLINAZIONE MOLA 1
  R(125) = Abs(Lp("R[125]")) 'INCLINAZIONE MOLA 2
  R(189) = Lp("R[189]")      'DIAMETRO ESTERNO
  R(113) = Lp("R[113]")      'PROFONDITA 1
  R(114) = Lp("R[114]")      'PROFONDITA 3
  '
  'CALCOLO SPOSTAMENTI PER FIANCO E PROFONDITA
  Call CALCOLO_CANALE(R(214), R(124), R(189), (R(189) - 2 * R(113)), LS1_1_C, LS1_2_C, 0, 0)
  Call CALCOLO_CANALE(R(215), R(125), R(189), (R(189) - 2 * R(113)), LS1_1_L, LS1_2_L, 0, 0)
  Call CALCOLO_CANALE(R(214), R(124), R(189), (R(189) - 2 * R(114)), LS3_1_C, LS3_2_C, 0, 0)
  Call CALCOLO_CANALE(R(215), R(125), R(189), (R(189) - 2 * R(114)), LS3_1_L, LS3_2_L, 0, 0)
  '
  'CALCOLO DELLE COMPENSAZIONI
  If (R(215) > R(214)) Then
    R(111) = (LS3_1_C - LS1_1_C) / 2 + (LS3_1_L - LS1_1_L) / 2
    R(112) = (LS3_2_C - LS1_2_C) / 2 + (LS3_2_L - LS1_2_L) / 2
    R(121) = (LS3_1_L - LS1_1_C) - R(111)
    R(122) = (LS3_2_L - LS1_2_C) - R(112)
  End If
  If (R(215) < R(214)) Then
    R(111) = (LS1_1_C - LS3_1_C) / 2 + (LS1_1_L - LS3_1_L) / 2
    R(112) = (LS1_2_C - LS3_2_C) / 2 + (LS1_2_L - LS3_2_L) / 2
    R(121) = (LS1_1_C - LS3_1_L) - R(111)
    R(122) = (LS1_2_C - LS3_2_L) - R(112)
  End If
  '
  '*********************************************************************
  'COPIO I VALORI NEI RELATIVI CAMPI ACTUAL DELLA TABELLA OEM1_LAVORO45
  'PERCHE' CI SONO TUTTI GLI ALTRI VALORI CHE NON DEVO MODIFICARE
  '*********************************************************************
  '
  'EVENTUALE AGGIORNAMENTO DEL SOTTOPROGRAMMA
  Dim MODALITA As String
  Dim sRESET   As String
  Dim iRESET   As Integer
  'LEGGO LA MODALITA
  MODALITA = GetInfo("Configurazione", "Modalita", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))
  If (MODALITA = "OFF-LINE") Then
    iRESET = 1
  Else
    'CONTROLLO SE SONO NELLO DI RESET PER ACCETTARE LA MODIFICA
    'LETTURA DELL'ITEM DDE PER CONTROLLO STATO DI RESET
    sRESET = GetInfo("CONFIGURAZIONE", "sRESET", PathFILEINI)
    If (Len(sRESET) > 0) Then
      iRESET = val(OPC_LEGGI_DATO(sRESET))
    Else
      iRESET = 1
    End If
  End If
  If (iRESET = 1) Then
    Call SP("R[111]", frmt(R(111), 2), False)
    Call SP("R[112]", frmt(R(112), 2), False)
    Call SP("R[121]", frmt(R(121), 2), False)
    Call SP("R[122]", frmt(R(122), 2), False)
    Call AggiornaDB
    'INVIO IL SOTTOPROGRAMMA MODIFICATO
    If (MODALITA = "SPF") Then Call OEMX.SuOEM1.CreaSPF
    Call OEMX.SuOEM1.DisegnaOEM
  Else
    'SEGNALAZIONE UTENTE
    WRITE_DIALOG "Data cannot be modified: CNC is not on RESET state"
  End If
'
Exit Sub

errCALCOLO_SPOSTAMENTI_VITI_PASSO_VARIABILE:
  WRITE_DIALOG "Sub CALCOLO_SPOSTAMENTI_VITI_PASSO_VARIABILE: Error -> " & Err
  sERRORE = " LOADED WITH ERROR " & str(Err)
Resume Next

End Sub

Sub CALCOLO_SPOSTAMENTI_VITI_PASSO_VARIABILE_BARRIERA6()
'
Dim stmp             As String
Dim i                As Integer
Dim ret              As Integer
Dim TIPO_LAVORAZIONE As String
Dim sERRORE          As String
Dim R(999)           As Double
Dim LS1_1_C          As Double
Dim LS1_2_C          As Double
Dim LS1_1_L          As Double
Dim LS1_2_L          As Double
Dim LS3_1_C          As Double
Dim LS3_2_C          As Double
Dim LS3_1_L          As Double
Dim LS3_2_L          As Double
Dim NomeTipo         As String
'
On Error GoTo errCALCOLO_SPOSTAMENTI_VITI_PASSO_VARIABILE_BARRIERA6
  '
  'LEGGO IL TIPO DI LAVORAZIONE
  TIPO_LAVORAZIONE = GetInfo("CONFIGURAZIONE", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  NomeTipo = RICERCA_NOME_TIPO(TIPO_LAVORAZIONE)
  WRITE_DIALOG NomeTipo & ": EVALUATING SHITF."
  '
  'CALCOLO I PARAMETRI SHIFT DELLA MOLA LARGA
  'LETTURA PARAMETRI DI LAVORO
  R(214) = Abs(Lp("MO1R[0,214]")) 'PASSO 1
  R(215) = Abs(Lp("MO1R[0,215]")) 'PASSO 2
  R(124) = Abs(Lp("MO1R[0,124]")) 'INCLINAZIONE MOLA 1
  R(125) = Abs(Lp("MO1R[0,125]")) 'INCLINAZIONE MOLA 2
  R(189) = Lp("MO1R[0,189]")      'DIAMETRO ESTERNO
  R(113) = Lp("MO1R[0,113]")      'PROFONDITA 1
  R(114) = Lp("MO1R[0,114]")      'PROFONDITA 3
  '
  'CALCOLO SPOSTAMENTI PER FIANCO E PROFONDITA
  Call CALCOLO_CANALE(R(214), R(124), R(189), (R(189) - 2 * R(113)), LS1_1_C, LS1_2_C, 0, 0, 1)
  Call CALCOLO_CANALE(R(215), R(125), R(189), (R(189) - 2 * R(113)), LS1_1_L, LS1_2_L, 0, 0, 1)
  Call CALCOLO_CANALE(R(214), R(124), R(189), (R(189) - 2 * R(114)), LS3_1_C, LS3_2_C, 0, 0, 1)
  Call CALCOLO_CANALE(R(215), R(125), R(189), (R(189) - 2 * R(114)), LS3_1_L, LS3_2_L, 0, 0, 1)
  '
  'CALCOLO DELLE COMPENSAZIONI
  If (R(215) > R(214)) Then
    R(111) = (LS3_1_C - LS1_1_C) / 2 + (LS3_1_L - LS1_1_L) / 2
    R(112) = (LS3_2_C - LS1_2_C) / 2 + (LS3_2_L - LS1_2_L) / 2
    R(121) = (LS3_1_L - LS1_1_C) - R(111)
    R(122) = (LS3_2_L - LS1_2_C) - R(112)
  End If
  If (R(215) < R(214)) Then
    R(111) = (LS1_1_C - LS3_1_C) / 2 + (LS1_1_L - LS3_1_L) / 2
    R(112) = (LS1_2_C - LS3_2_C) / 2 + (LS1_2_L - LS3_2_L) / 2
    R(121) = (LS1_1_C - LS3_1_L) - R(111)
    R(122) = (LS1_2_C - LS3_2_L) - R(112)
  End If
  '
  '*********************************************************************
  'COPIO I VALORI NEI RELATIVI CAMPI ACTUAL DELLA TABELLA OEM1_LAVORO50M1
  'PERCHE' CI SONO TUTTI GLI ALTRI VALORI CHE NON DEVO MODIFICARE
  '*********************************************************************
  '
  'EVENTUALE AGGIORNAMENTO DEL SOTTOPROGRAMMA
  Dim MODALITA As String
  Dim sRESET   As String
  Dim iRESET   As Integer
  'LEGGO LA MODALITA
  MODALITA = GetInfo("Configurazione", "Modalita", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))
  If (MODALITA = "OFF-LINE") Then
    iRESET = 1
  Else
    'CONTROLLO SE SONO NELLO DI RESET PER ACCETTARE LA MODIFICA
    'LETTURA DELL'ITEM DDE PER CONTROLLO STATO DI RESET
    sRESET = GetInfo("CONFIGURAZIONE", "sRESET", PathFILEINI)
    If (Len(sRESET) > 0) Then
      iRESET = val(OPC_LEGGI_DATO(sRESET))
    Else
      iRESET = 1
    End If
  End If
  If (iRESET = 1) Then
    Call SP("MO1R[0,111]", frmt(R(111), 2), False)
    Call SP("MO1R[0,112]", frmt(R(112), 2), False)
    Call SP("MO1R[0,121]", frmt(R(121), 2), False)
    Call SP("MO1R[0,122]", frmt(R(122), 2), False)
    Call AggiornaDB
    'INVIO IL SOTTOPROGRAMMA MODIFICATO
    If (MODALITA = "SPF") Then Call OEMX.SuOEM1.CreaSPF
    Call OEMX.SuOEM1.DisegnaOEM
  Else
    'SEGNALAZIONE UTENTE
    WRITE_DIALOG "Data cannot be modified: CNC is not on RESET state"
  End If
'
Exit Sub

errCALCOLO_SPOSTAMENTI_VITI_PASSO_VARIABILE_BARRIERA6:
  WRITE_DIALOG "Sub CALCOLO_SPOSTAMENTI_VITI_PASSO_VARIABILE_BARRIERA6: Error -> " & Err
  sERRORE = " LOADED WITH ERROR " & str(Err)
Resume Next

End Sub

Sub CALCOLO_VITI_PASSO_VARIABILE()
'
Const MMp = 1 ' 1 mm 25.4 pollici
'
Dim Atmp     As String * 255
Dim stmp     As String
Dim sERRORE  As String
Dim NomeVite As String
'
Dim DE     As Double  'R189: Diametro esterno
Dim Passo1 As Double  'Passo iniziale
Dim IMO1   As Double  'Elica iniziale
Dim Passo2 As Double  'Passo finale
Dim PASSO  As Double
Dim IMO2   As Double  'Elica finale
Dim Z1     As Double  'Lunghezza tratto 1
Dim Z2     As Double  'Lunghezza tratto 2
Dim Z3     As Double  'R93: Lunghezza tratto 3
Dim PZ1    As Double  'ProfonditÓ tratto 1
Dim PZ3    As Double  'ProfonditÓ tratto 3
Dim SFN    As Double  'Spessore normale filetto
Dim LC1    As Double  'Channel width
Dim LC2    As Double  'Channel width
Dim RGT    As Double  'Radius tool offset
Dim R41ch  As Double  'R41, R51: Angolo  fianco
Dim R51ch  As Double  'R41, R51: Angolo  fianco
Dim R49ch  As Double  'R49, R59: Raggio di fondo
Dim R59ch  As Double  'R49, R59: Raggio di fondo
'
Dim LatoPartenza      As Integer
Dim ret               As Integer
Dim sVALORE           As String
'
Dim VELO_PRF          As String
Dim VELO_RUL          As String
Dim CALCOLO_SEQUENZA  As String
Dim RIPE_RET(3)       As Integer
Dim PASS_PRF(3)       As Integer
Dim INCR_PRF(3)       As Double
Dim FEED_PRF(3)       As Double
Dim ROTA_PRF(3)       As String
Dim PASS_RET(3)       As Integer
Dim INCR_RET(3)       As Double
Dim VELO_ASS(3)       As Double
Dim VELO_MOL(3)       As Double
Dim SVRM_CYC(3)       As Double
Dim SCARTO(3)         As Double
Dim TipoMola          As String
'
On Error GoTo errCALCOLO_VITI_PASSO_VARIABILE
  '
  'LETTURA DEL NOME DELLA VITE CONICA
  NomeVite = GetInfo("VITI_PASSO_VARIABILE", "NomePezzoInserimento", Path_LAVORAZIONE_INI)
  NomeVite = UCase$(Trim$(NomeVite))
  WRITE_DIALOG NomeVite & " EVALUATING "
  'LETTURA VALORI DALLA FINESTRA D'INSERIMENTO
  DE = Lp("DE")
  Passo1 = Lp("PASSO1")
  IMO1 = Lp("IMO1")
  Passo2 = Lp("PASSO2")
  IMO2 = Lp("IMO2")
  PASSO = Passo1
  If (Passo2 > PASSO) Then PASSO = Passo2
  Z1 = Lp("Z1")
  Z2 = Lp("Z2")
  Z3 = Lp("Z3")
  PZ1 = Lp("PZ1")
  PZ3 = Lp("PZ3")
  LatoPartenza = 1 'Lp("LP")
   If (1 = Lp("TipoMola")) Then
    TipoMola = "_CBN"
  Else
    TipoMola = ""
  End If
  SFN = Lp("SFN")
  RGT = Lp("RGT")
  R41ch = Lp("R41ch")
  R51ch = Lp("R51ch")
  R49ch = Lp("R49ch")
  R59ch = Lp("R59ch")
  '
  'CALCOLO ELICHE
  Dim EGP1   As Double
  Dim EGP2   As Double
  Dim PZMAX  As Double
  Dim LW     As Integer
  'CALCOLO ELICA 1 SE NON E' IMPOSTATA
  If (IMO1 = 0) Then
    EGP1 = -Atn(Passo1 / PG / DE)
    Call SP("IMO1", frmt(EGP1 * 180 / PG, 4))
  Else
    EGP1 = IMO1 * PG / 180
  End If
  'CALCOLO ELICA 2 SE NON E' IMPOSTATA
  If (IMO2 = 0) Then
    EGP2 = -Atn(Passo2 / PG / DE)
    Call SP("IMO2", frmt(EGP2 * 180 / PG, 4))
  Else
    EGP2 = IMO2 * PG / 180
  End If
  'RICERCA PROFONDITA' MASSIMA
  PZMAX = PZ1: If PZ3 > PZMAX Then PZMAX = PZ3
  'LARGHEZZA CANALE ASSIALE ZONA 1
  LC1 = Passo1 - SFN / Cos(EGP1)
  'LARGHEZZA CANALE ASSIALE ZONA 1
  LC2 = Passo2 - SFN / Cos(EGP2)
  '
  'LAVORO
  'AGGIORNAMENTO CON CALCOLO NEL FILE INI
  Call SP("R[189]", frmt(DE, 4), False)
  'Call SP("R[17]", frmt(LatoPartenza, 4), False)
  Call SP("R[213]", "30", False)
  If (LatoPartenza = 1) Then
    'PARTENZA DA SINISTRA
    Call SP("R[214]", frmt(Passo1, 4), False)
    Call SP("R[124]", frmt(EGP1 * 180 / PG, 2), False)
    Call SP("R[215]", frmt(Passo2, 4), False)
    Call SP("R[125]", frmt(EGP2 * 180 / PG, 2), False)
    Call SP("R[210]", frmt(Z1, 4), False)
    Call SP("R[211]", frmt(Z2, 4), False)
    Call SP("R[212]", frmt(Z3, 4), False)
    Call SP("R[113]", frmt(PZ1, 4), False)
    Call SP("R[114]", frmt(PZ3, 4), False)
  Else
    'PARTENZA DA DESTRA
    Call SP("R[214]", frmt(Passo2, 4), False)
    Call SP("R[124]", frmt(EGP2 * 180 / PG, 2), False)
    Call SP("R[215]", frmt(Passo1, 4), False)
    Call SP("R[125]", frmt(EGP1 * 180 / PG, 2), False)
    Call SP("R[210]", frmt(Z3, 4), False)
    Call SP("R[211]", frmt(Z2, 4), False)
    Call SP("R[212]", frmt(Z1, 4), False)
    Call SP("R[113]", frmt(PZ3, 4), False)
    Call SP("R[114]", frmt(PZ1, 4), False)
  End If
  '
  VELO_PRF = GetInfo("CICLI_VITI_CONICHE", "VELO_PRF" & TipoMola, Path_LAVORAZIONE_INI)
  If (Len(VELO_PRF) <= 0) Then VELO_PRF = "30"
  VELO_RUL = GetInfo("CICLI_VITI_CONICHE", "VELO_RUL" & TipoMola, Path_LAVORAZIONE_INI)
  If (Len(VELO_RUL) <= 0) Then VELO_RUL = "24"
  '
  LW = (Int(LC1 / 5) + 1) * 5
  Call SP("R[37]", Format$(LW, "###0"), False)
  Call SP("R[38]", frmt(LC1 * Cos(EGP1), 3), False)
  Call SP("R[204]", VELO_PRF, False)
  'Call SP("MAC[0,39]", VELO_RUL, False)
  Call SP("R[248]", "250", False)
  Call SP("R[200]", "360", False)
  Call SP("R[49]", frmt(R49ch, 4), False)
  Call SP("R[59]", frmt(R59ch, 4), False)
  Call SP("R[48]", frmt((RGT + 0.2) / MMp, 3), False)
  Call SP("R[58]", frmt((RGT + 0.2) / MMp, 3), False)
  Call SP("R[43]", frmt((RGT + 0.5) / MMp, 3), False)
  Call SP("R[53]", frmt((RGT + 0.5) / MMp, 3), False)
  Call SP("R[44]", frmt((RGT + 0.5) / MMp + PZMAX, 3), False)
  Call SP("R[54]", frmt((RGT + 0.5) / MMp + PZMAX, 3), False)
  Call SP("R[47]", "10", False)
  Call SP("R[57]", "10", False)
  Call SP("R[41]", frmt(R41ch, 3), False)
  Call SP("R[51]", frmt(R51ch, 3), False)
  'COSTRUZIONE DELLA SEQUENZA DEI CICLI DI RETTIFICA
  CALCOLO_SEQUENZA = GetInfo("VITI_PASSO_VARIABILE", "CALCOLO_SEQUENZA", Path_LAVORAZIONE_INI)
  CALCOLO_SEQUENZA = UCase$(Trim$(CALCOLO_SEQUENZA))
  If Len(CALCOLO_SEQUENZA) <= 0 Then CALCOLO_SEQUENZA = "N"
  If (CALCOLO_SEQUENZA = "Y") Then
    Call CALCOLO_SEQUENZA_CICLI_CONICHE(PASSO, PZMAX, DE, TipoMola, RIPE_RET, PASS_PRF, INCR_PRF, FEED_PRF, ROTA_PRF, PASS_RET, INCR_RET, VELO_ASS, VELO_MOL, SVRM_CYC)
    'CALCOLO I PARAMETRI PER IL PRIMO CICLO
    RIPE_RET(1) = INTERO(SVRM_CYC(1) / (INCR_RET(1) + INCR_RET(1)) / PASS_RET(1), "MAX")
    INCR_RET(1) = SVRM_CYC(1) / RIPE_RET(1) / PASS_RET(1) / 2
    INCR_RET(1) = Int(INCR_RET(1) * 10000) / 10000
    SCARTO(1) = SVRM_CYC(1) - RIPE_RET(1) * PASS_RET(1) * (INCR_RET(1) + INCR_RET(1))
    'SCARICO LO SCARTO NEL SECONDO CICLO
    SVRM_CYC(2) = SVRM_CYC(2) + SCARTO(1)
    INCR_RET(2) = SVRM_CYC(2) / PASS_RET(2)
    'CONVERSIONE GP->VT
    'GP=FEED/SQRT(360^2+PASSO^2)
    'VT=GP*SQRT((Pi*Dext)^2+PASSO^2)
    'GP=VT/SQRT((Pi*Dext)^2+PASSO^2)
    'CALCOLO VELOCITA'
    VELO_ASS(1) = INTERO(VELO_ASS(1) * Sqr((PG * DE) ^ 2 + PASSO ^ 2), "MIN")
    VELO_ASS(2) = INTERO(VELO_ASS(2) * Sqr((PG * DE) ^ 2 + PASSO ^ 2), "MIN")
    VELO_ASS(3) = INTERO(VELO_ASS(3) * Sqr((PG * DE) ^ 2 + PASSO ^ 2), "MIN")
    'SGROSSATURA
    Call SP("STR[11]", "S", False)
    Call SP("CYC[0,1]", Format$(RIPE_RET(1), "#####"), False)
    Call SP("CYC[0,2]", Format$(PASS_PRF(1), "#####"), False)
    Call SP("CYC[0,3]", frmt(INCR_PRF(1), 4), False)
    Call SP("CYC[0,4]", Format$(FEED_PRF(1), "#####"), False)
    Call SP("CYC[0,5]", ROTA_PRF(1), False)
    Call SP("CYC[0,6]", Format$(PASS_RET(1), "#####"), False)
    Call SP("CYC[0,7]", frmt(INCR_RET(1), 4), False)
    Call SP("CYC[0,8]", Format$(VELO_ASS(1), "##########"), False)
    Call SP("CYC[0,9]", frmt(INCR_RET(1), 4), False)
    Call SP("CYC[0,10]", Format$(VELO_ASS(1), "##########"), False)
    Call SP("CYC[0,11]", "0", False)
    Call SP("CYC[0,12]", Format$(VELO_MOL(1), "##########"), False)
    Call SP("CYC[0,13]", Format$(VELO_PRF, "##########"), False)
    'PREFINITURA
    Call SP("STR[12]", "S", False)
    Call SP("CYC[0,21]", Format$(RIPE_RET(2), "#####"), False)
    Call SP("CYC[0,22]", Format$(PASS_PRF(2), "#####"), False)
    Call SP("CYC[0,23]", frmt(INCR_PRF(2), 4), False)
    Call SP("CYC[0,24]", Format$(FEED_PRF(2), "#####"), False)
    Call SP("CYC[0,25]", ROTA_PRF(2), False)
    Call SP("CYC[0,26]", Format$(PASS_RET(2), "#####"), False)
    Call SP("CYC[0,27]", frmt(INCR_RET(2), 4), False)
    Call SP("CYC[0,28]", Format$(VELO_ASS(2), "##########"), False)
    Call SP("CYC[0,29]", "0", False)
    Call SP("CYC[0,30]", Format$(VELO_ASS(2), "##########"), False)
    Call SP("CYC[0,31]", "0", False)
    Call SP("CYC[0,32]", Format$(VELO_MOL(2), "##########"), False)
    Call SP("CYC[0,33]", Format$(VELO_PRF, "##########"), False)
    'FINITURA
    Call SP("STR[13]", "S", False)
    Call SP("CYC[0,41]", Format$(RIPE_RET(3), "#####"), False)
    Call SP("CYC[0,42]", Format$(PASS_PRF(3), "#####"), False)
    Call SP("CYC[0,43]", frmt(INCR_PRF(3), 4), False)
    Call SP("CYC[0,44]", Format$(FEED_PRF(3), "#####"), False)
    Call SP("CYC[0,45]", ROTA_PRF(3), False)
    Call SP("CYC[0,46]", Format$(PASS_RET(3), "#####"), False)
    Call SP("CYC[0,47]", frmt(INCR_RET(3), 4), False)
    Call SP("CYC[0,48]", Format$(VELO_ASS(3), "##########"), False)
    Call SP("CYC[0,49]", "0", False)
    Call SP("CYC[0,50]", Format$(VELO_ASS(3), "##########"), False)
    Call SP("CYC[0,51]", "0", False)
    Call SP("CYC[0,52]", Format$(VELO_MOL(3), "##########"), False)
    Call SP("CYC[0,53]", Format$(VELO_PRF, "##########"), False)
    'NULLO
    Call SP("STR[14]", "N", False)
    Call SP("CYC[0,61]", "0", False)
    Call SP("CYC[0,62]", "0", False)
    Call SP("CYC[0,63]", "0", False)
    Call SP("CYC[0,64]", "0", False)
    Call SP("CYC[0,65]", "0", False)
    Call SP("CYC[0,66]", "0", False)
    Call SP("CYC[0,67]", "0", False)
    Call SP("CYC[0,68]", "0", False)
    Call SP("CYC[0,69]", "0", False)
    Call SP("CYC[0,70]", "0", False)
    Call SP("CYC[0,71]", "0", False)
    Call SP("CYC[0,72]", "0", False)
    Call SP("CYC[0,73]", "0", False)
  Else
    Call SP("STR[11]", "S", False)
    Call SP("CYC[0,1]", "1", False)
    Call SP("CYC[0,6]", "1", False)
    Call SP("CYC[0,7]", frmt(PZMAX, 4), False)
  End If
  '
  Call SP("DIAMEST[0,0]", frmt(DE, 4), False)
  Call SCRITTURA_CICLI_RETTIFICA_DIAMETRI
  '
  'SVG120117
  'RICHIESTA PER MACCHINA TIPO ENGEL ED INUTILE PER SUMITOMO
  'stmp = ""
  'ret = LoadString(g_hLanguageLibHandle, 1003, Atmp, 255)
  'If (ret > 0) Then stmp = Left$(Atmp, ret)
  'stmp = stmp & " " & Tabella1.MESSAGGI(2).NOME
  'stmp = stmp & Chr(13) & Chr(13) & " [CNC " & LETTERA_LAVORAZIONE & "] " & LEGGI_NomePezzo("VITI_PASSO_VARIABILE")
  'stmp = stmp & Chr(13) & Chr(13) & " ---> "
  'stmp = stmp & Chr(13) & Chr(13) & " [OFF-LINE] " & NomeVite & "?"
  'StopRegieEvents
  'ret = MsgBox(stmp, vbInformation + vbYesNo + vbDefaultButton2, "WARNING")
  'ResumeRegieEvents
  'If (ret = vbYes) Then Call CARICA_DATI_MACCHINA_CORRENTI("VITI_PASSO_VARIABILE", "OEM1_MACCHINA45", "94")
  Call AggiornaDB
  ret = WritePrivateProfileString("Configurazione", "CARICA_CALCOLO", "N", PathFILEINI)
  WRITE_DIALOG NomeVite & " EVALUATED "
  If (Len(sERRORE) > 0) Then WRITE_DIALOG NomeVite & sERRORE
  '
Exit Sub

errCALCOLO_VITI_PASSO_VARIABILE:
  WRITE_DIALOG "Sub CALCOLO_VITI_PASSO_VARIABILE: Error -> " & Err
  sERRORE = ": STANDARD EVALUATE WITH ERROR " & str(Err)
  Resume Next

End Sub

Sub CALCOLO_RAGGIO1(ByVal R164 As Double, ByVal R165 As Double, ByVal R166 As Double, ByVal R167 As Double, ByVal R168 As Double, ByVal R169 As Double, CoordX As Double, CoordY As Double, Raggio As Double)
'
Dim R160 As Double
Dim R162 As Double
Dim R171 As Double
Dim R172 As Double
Dim R173 As Double
'
On Error GoTo errCALCOLO_RAGGIO1
  '
  'COORD. X        COORD. Y
  'R164 = xx(i + 0): R165 = YY(i + 0)
  'R166 = xx(i + 1): R167 = YY(i + 1)
  'R168 = xx(i + 2): R169 = YY(i + 2)
  'DISTANZA PER TRE PUNTI
  R162 = Abs((R165 - R169) * (R164 - R166) + (R167 - R165) * (R164 - R168)) / Sqr((R165 - R169) * (R165 - R169) + (R164 - R168) * (R164 - R168))
  If (R162 < 0.0005) Then
    'Write_Dialog "TRATTO RETTILINEO"
  Else
    'Write_Dialog "TRATTO CURVILINEO"
    If R169 = R167 Then R169 = R167 + 0.0001
    If R165 = R167 Then R165 = R167 + 0.0001
    R160 = (R164 - R166) / (R167 - R165)
    R171 = (R166 - R168) / (R169 - R167)
    R172 = (R165 + R167) / 2 - R160 * (R164 + R166) / 2
    R173 = (R169 + R167) / 2 - R171 * (R168 + R166) / 2
    If (R160 = R171) Then R160 = R171 + 0.0001
    'coordinate del centro
    CoordX = (R173 - R172) / (R160 - R171)
    CoordY = R160 * CoordX + R172
    'raggio del cerchio
    Raggio = Sqr((CoordX - R166) * (CoordX - R166) + (CoordY - R167) * (CoordY - R167))
    'prodotto misto per senso di rotazione
    R160 = (R166 - R164) * (R169 - R165) - (R168 - R164) * (R167 - R165)
    If (R160 < 0) Then Raggio = -Raggio
    'Calcolo angoli compresi tra 0 e 2 * Pigreco
    'AngStart = CALCOLO_ANGOLO(R164 - CoordX, R165 - CoordY)
    'AngEnd = CALCOLO_ANGOLO(R168 - CoordX, R169 - CoordY)
  End If
  '
Exit Sub

errCALCOLO_RAGGIO1:
  WRITE_DIALOG "Sub CALCOLO_RAGGIO1: ERROR -> " & Err
  Resume Next

End Sub

Sub VISUALIZZA_CANALE(SiStampa As String)
'
Dim COLORE                As Integer
Dim RIFERIMENTI           As Integer
Dim SPOSTAMENTO           As Integer
Dim ALLONTANAMENTO        As Single
Dim CORREZIONE_INTERASSE  As Single
'
Dim ftmp   As Double
Dim stmp   As String
Dim s1     As String
Dim Atmp   As String * 255
Dim Fianco As Integer
Dim i      As Integer
Dim j      As Integer
Dim ret    As Integer
'
Dim pX1 As Single
Dim pX2 As Single
Dim pY1 As Single
Dim pY2 As Single
'
Dim PicH As Single
Dim PicW As Single
'
Dim Scala      As Single
Dim ScalaX     As Single
Dim ScalaY     As Single
'
Dim ZeroPezzoX As Single
Dim ZeroPezzoY As Single
'
Dim NumeroRette As Integer
Dim da          As Single
Dim DISTANZA    As Double
Dim ANGOLO      As Double
Dim Raggio      As Single
Dim X0          As Single
Dim Y0          As Single
'
Dim Ang      As Single
Dim DeltaAng As Single
'
Dim XP1 As Single
Dim YP1 As Single
Dim AP1 As Single
'
Dim XP2 As Single
Dim YP2 As Single
Dim AP2 As Single
'
Dim DF As Single
Dim AF As Single
'
Dim ENTRX As Double
Dim PRI   As Integer
Dim PAS   As Double
Dim INF   As Double
Dim IMO   As Double
Dim DIV   As Double
Dim DEV   As Double
'
Dim RXV   As Double
Dim EXV   As Double
'
Dim APF   As Double
Dim EXF   As Double
Dim RXF   As Double
'
Dim XX_PROFONDITA As Single
Dim PROFONDITA    As Single
Dim RM            As Single
'
Dim XmF(2, 9999) As Double
Dim YmF(2, 9999) As Double
Dim TMF(2, 9999) As Double
'
Dim XvF(2, 9999) As Double
Dim YvF(2, 9999) As Double
Dim TvF(2, 9999) As Double
'
Dim Xv(2)  As Double
Dim DhF(2) As Double
Dim IIv(2) As Integer
Dim JJv(2) As Integer
'
Dim k1  As Integer
Dim k2  As Integer
Dim k3  As Integer
Dim k4  As Integer
Dim K5  As Integer
Dim XXc As Double
Dim YYc As Double
Dim RRc As Double
'
Dim chFilettoReale  As Single
Dim chReale  As Single
'
Dim PZ0 As Single
Dim PZ1 As Single
Dim PZ3 As Single
Dim PZ5 As Single
Dim PZ7 As Single
'
'Valori letti
Dim R45    As Double
Dim R55    As Double
Dim R46    As Double
Dim R56    As Double
'
'Valori calcolati considerando i precedenti definiti nella mola
Dim R45ch As Double
Dim R55ch As Double
Dim R46ch As Double
Dim R56ch As Double
'
Dim TIPO_LAVORAZIONE As String
'
Dim ERRORE As Integer
Dim FINESTRA As Object
'
Const SiMOLA = "N"
'
On Error GoTo errVISUALIZZA_CANALE
  '
  If (SiStampa = "N") Then
    Set FINESTRA = OEMX.OEM1PicCALCOLO
  Else
    Set FINESTRA = Printer
  End If
  'SEGNALAZIONE UTENTE
  WRITE_DIALOG "CHANNEL"
  'If OEM1.Check2.Value = 1 Then
    RIFERIMENTI = 0
  'Else
  '  RIFERIMENTI = 1
  'End If
  'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  stmp = GetInfo("MOLA_VITI_CONICHE", "NumeroRette", Path_LAVORAZIONE_INI)
  NumeroRette = val(stmp): If NumeroRette < 1 Then NumeroRette = 1
  'Lettura della risoluzione per le freccie
  stmp = GetInfo("MOLA_VITI_CONICHE", "DF", Path_LAVORAZIONE_INI):    DF = val(stmp)
  stmp = GetInfo("MOLA_VITI_CONICHE", "AF", Path_LAVORAZIONE_INI):    AF = val(stmp)
  stmp = GetInfo("MOLA_VITI_CONICHE", "Scala", Path_LAVORAZIONE_INI): Scala = val(stmp)
  If (Scala <= 0) Then Scala = 1
  ScalaX = Scala: ScalaY = Scala
  'VISUALIZZO LA SCALA
  i = LoadString(g_hLanguageLibHandle, 1000, Atmp, 255)
  If (i > 0) Then OEMX.lblINP(0).Caption = Left$(Atmp, i)
  OEMX.txtINP(0).Text = frmt(Scala, 4)
  'VISUALIZZO IL RAGGIO DELLA MOLA
  stmp = GetInfo("MOLA_VITI_CONICHE", "RAGGIO", Path_LAVORAZIONE_INI): RM = val(stmp)
  OEMX.lblINP(1).Caption = "RM"
  OEMX.txtINP(1).Text = frmt(RM, 4)
  stmp = GetInfo("MOLA_VITI_CONICHE", "CORREZIONE_INTERASSE", Path_LAVORAZIONE_INI)
  CORREZIONE_INTERASSE = val(stmp)
  'VISUALIZZO LA CORREZIONE D'INTERASSE
  OEMX.lblINP(2).Caption = "DI"
  OEMX.txtINP(2).Text = frmt(CORREZIONE_INTERASSE, 4)
  'VISUALIZZO DHF1
  stmp = GetInfo("MOLA_VITI_CONICHE", "DhF(1)", Path_LAVORAZIONE_INI): DhF(1) = val(stmp)
  OEMX.lblINP(3).Caption = "DhF1"
  OEMX.txtINP(3).Text = stmp
  'VISUALIZZO DHF2
  stmp = GetInfo("MOLA_VITI_CONICHE", "DhF(2)", Path_LAVORAZIONE_INI): DhF(2) = val(stmp)
  OEMX.lblINP(4).Caption = "DhF2"
  OEMX.txtINP(4).Text = stmp
  stmp = GetInfo("MOLA_VITI_CONICHE", "PicW", Path_LAVORAZIONE_INI): PicW = val(stmp)
  stmp = GetInfo("MOLA_VITI_CONICHE", "PicH", Path_LAVORAZIONE_INI): PicH = val(stmp)
  stmp = GetInfo("MOLA_VITI_CONICHE", "ZeroPezzoX", Path_LAVORAZIONE_INI): ZeroPezzoX = val(stmp)
  stmp = GetInfo("MOLA_VITI_CONICHE", "ZeroPezzoY", Path_LAVORAZIONE_INI): ZeroPezzoY = val(stmp)
  'AQUISIZIONE PUNTI DEL PROFILO MOLA
  stmp = GetInfo("MOLA_VITI_CONICHE", "PROFONDITA", Path_LAVORAZIONE_INI)
  PROFONDITA = val(stmp)
  stmp = GetInfo("MOLA_VITI_CONICHE", "XX_PROFONDITA", Path_LAVORAZIONE_INI)
  XX_PROFONDITA = val(stmp)
  Call SCRIVI_SCALA_ORIZZONTALE(Scala, FINESTRA, SiStampa)
  If (RIFERIMENTI = 1) Then Call SCRIVI_ASSI(FINESTRA, SiStampa)
  '
  Select Case TIPO_LAVORAZIONE
  Case "VITI_CONICHE"
      NomeTb = "OEM1_MOLA41"
      PRI = Abs(Lp("R[5]"))    'PRINCIPI
      IMO = Abs(Lp("R[124]"))  'INCLINAZIONE MOLA
      PAS = Abs(Lp("R[128]"))  'PASSO
      DEV = Abs(Lp("R[189]"))  'DIAMETRO ESTERNO
      PZ1 = Abs(Lp("R[220]"))  'PROFONDITA' DELLA PRIMA   ZONA
      PZ3 = Abs(Lp("R[221]"))  'PROFONDITA' DELLA TERZA   ZONA
      PZ5 = Abs(Lp("R[222]"))  'PROFONDITA' DELLA QUINTA  ZONA
      PZ7 = Abs(Lp("R[223]"))  'PROFONDITA' DELLA SETTIMA ZONA
      '-------------------------------------------------------------
      DIV = DEV - 2 * PZ1   'DIAMETRO INTERNO DELLA PRIMA ZONA
      '-------------------------------------------------------------
      stmp = GetInfo("MOLA_VITI_CONICHE", "INDICE_COMBO", Path_LAVORAZIONE_INI)
      Select Case val(stmp)
        Case 0:     ALLONTANAMENTO = 0
        Case 1:     ALLONTANAMENTO = PZ1 - PZ3
        Case 2:     ALLONTANAMENTO = PZ1 - PZ5
        Case 3:     ALLONTANAMENTO = PZ1 - PZ7
        Case Else:  ALLONTANAMENTO = 0
      End Select
      '-------------------------------------------------------------
      R45 = Lp("R[45]"): R55 = Lp("R[55]")
      R46 = Lp("R[46]"): R56 = Lp("R[56]")
'  Case "VITI_DBG"
'      Set RSS = DBB.OpenRecordset("OEM1_LAVORO42")
'      RSS.Index = "INDICE"
'      RSS.Seek "=", 6
'      IMO = Abs(val(RSS.Fields("ACTUAL_1").Value)) 'INCLINAZIONE MOLA
'      stmp = GetInfo("MOLA_VITI_CONICHE", "INDICE_COMBO", Path_LAVORAZIONE_INI)
'      If (val(stmp) = 0) Then
'        RSS.Seek "=", 3      'PASSO VITE
'        PAS = Abs(val(RSS.Fields("ACTUAL_1").Value)) 'PASSO
'      Else
'        RSS.Seek "=", 22     'PASSO BARRIERA
'        PAS = Abs(val(RSS.Fields("ACTUAL_2").Value)) 'PASSO
'      End If
'      RSS.Seek "=", 4
'      DEV = val(RSS.Fields("ACTUAL_1").Value)      'DIAMETRO ESTERNO
'      RSS.Seek "=", 24
'      DIV = val(RSS.Fields("ACTUAL_2").Value)      'DIAMETRO INTERNO
'      RSS.Close
'      '-------------------------------------------------------------
'      ALLONTANAMENTO = 0
'  Case "VITI_BARRIERA3L"
'      Set RSS = DBB.OpenRecordset("OEM1_LAVORO43")
'      RSS.Index = "INDICE"
'      RSS.Seek "=", 1
'      IMO = Abs(val(RSS.Fields("ACTUAL_1").Value)) 'INCLINAZIONE MOLA
'      RSS.Seek "=", 2
'      PAS = Abs(val(RSS.Fields("ACTUAL_1").Value)) 'PASSO
'      RSS.Seek "=", 3
'      DEV = val(RSS.Fields("ACTUAL_1").Value)      'DIAMETRO ESTERNO
'      '-------------------------------------------------------------
'      RSS.Seek "=", 15 + 0
'      PZ1 = val(RSS.Fields("ACTUAL_1").Value)      'PROFONDITA' DELLA PRIMA   ZONA
'      '-------------------------------------------------------------
'      RSS.Seek "=", 15 + 1
'      PZ3 = val(RSS.Fields("ACTUAL_1").Value)      'PROFONDITA' DELLA TERZA   ZONA
'      '-------------------------------------------------------------
'      DIV = DEV - 2 * PZ1   'DIAMETRO INTERNO DELLA PRIMA ZONA
'      RSS.Close
'      '-------------------------------------------------------------
'      stmp = GetInfo("MOLA_VITI_CONICHE", "INDICE_COMBO", Path_LAVORAZIONE_INI)
'      Select Case val(stmp)
'        Case 0:    ALLONTANAMENTO = 0
'        Case 1:    ALLONTANAMENTO = PZ1 - PZ3
'        Case Else: ALLONTANAMENTO = 0
'      End Select
'  Case "VITI_BARRIERA3S"
'      Set RSS = DBB.OpenRecordset("OEM1_LAVORO44")
'      RSS.Index = "INDICE"
'      RSS.Seek "=", 4
'      IMO = Abs(val(RSS.Fields("ACTUAL_1").Value)) 'INCLINAZIONE MOLA
'      stmp = GetInfo("MOLA_VITI_CONICHE", "INDICE_COMBO", Path_LAVORAZIONE_INI)
'      Select Case val(stmp)
'        Case 0
'          RSS.Seek "=", 5                              'PASSO LUNGO
'          PAS = Abs(val(RSS.Fields("ACTUAL_1").Value)) 'PASSO
'        Case 1
'          RSS.Seek "=", 1                              'PASSO CORTO
'          PAS = Abs(val(RSS.Fields("ACTUAL_1").Value)) 'PASSO
'        Case Else
'          RSS.Seek "=", 5                              'PASSO LUNGO
'          PAS = Abs(val(RSS.Fields("ACTUAL_1").Value)) 'PASSO
'      End Select
'      RSS.Seek "=", 2
'      DEV = val(RSS.Fields("ACTUAL_1").Value)      'DIAMETRO ESTERNO
'      RSS.Seek "=", 10
'      DIV = val(RSS.Fields("ACTUAL_2").Value)      'DIAMETRO INTERNO
'      RSS.Close
'      '-------------------------------------------------------------
'      ALLONTANAMENTO = 0
'  Case "VITI_BARRIERA4L"
'      Set RSS = DBB.OpenRecordset("OEM1_LAVORO46")
'      RSS.Index = "INDICE"
'      RSS.Seek "=", 1
'      IMO = Abs(val(RSS.Fields("ACTUAL_1").Value)) 'INCLINAZIONE MOLA
'      RSS.Seek "=", 2
'      PAS = Abs(val(RSS.Fields("ACTUAL_1").Value)) 'PASSO
'      RSS.Seek "=", 3
'      DEV = val(RSS.Fields("ACTUAL_1").Value)      'DIAMETRO ESTERNO
'      '-------------------------------------------------------------
'      RSS.Seek "=", 24
'      PZ1 = val(RSS.Fields("ACTUAL_1").Value)      'PROFONDITA' BS4LA
'      PZ3 = val(RSS.Fields("ACTUAL_2").Value)      'PROFONDITA' BS4LB
'      '-------------------------------------------------------------
'      RSS.Seek "=", 18
'      PZ5 = val(RSS.Fields("ACTUAL_1").Value)      'PROFONDITA'BARRIERA
'      '-------------------------------------------------------------
'      DIV = DEV - 2 * PZ1   'DIAMETRO INTERNO DELLA PRIMA ZONA
'      RSS.Close
'      '-------------------------------------------------------------
'      stmp = GetInfo("MOLA_VITI_CONICHE", "INDICE_COMBO", Path_LAVORAZIONE_INI)
'      Select Case val(stmp)
'        Case 0:    ALLONTANAMENTO = 0
'        Case 1:    ALLONTANAMENTO = PZ1 - PZ3
'        Case 2:    ALLONTANAMENTO = PZ1 - PZ5
'        Case Else: ALLONTANAMENTO = 0
'      End Select
'  Case "VITI_BARRIERA4S"
'      Set RSS = DBB.OpenRecordset("OEM1_LAVORO47")
'      RSS.Index = "INDICE"
'      RSS.Seek "=", 4
'      IMO = Abs(val(RSS.Fields("ACTUAL_1").Value)) 'INCLINAZIONE MOLA
'      RSS.Seek "=", 5
'      PAS = Abs(val(RSS.Fields("ACTUAL_1").Value)) 'PASSO
'      RSS.Seek "=", 2
'      DEV = val(RSS.Fields("ACTUAL_1").Value)      'DIAMETRO ESTERNO
'      '-------------------------------------------------------------
'      RSS.Seek "=", 23
'      PZ1 = (DEV - val(RSS.Fields("ACTUAL_1").Value)) / 2  'PROFONDITA'
'      PZ5 = (DEV - val(RSS.Fields("ACTUAL_2").Value)) / 2  'PROFONDITA'
'      '-------------------------------------------------------------
'      RSS.Seek "=", 24
'      PZ3 = (DEV - val(RSS.Fields("ACTUAL_1").Value)) / 2  'PROFONDITA'
'      PZ7 = (DEV - val(RSS.Fields("ACTUAL_2").Value)) / 2  'PROFONDITA'
'      '-------------------------------------------------------------
'      'DIAMETRO INTERNO DELLA PRIMA ZONA
'      If (PZ1 > PZ5) Then PZ0 = PZ1 Else PZ0 = PZ5
'      DIV = DEV - 2 * PZ0
'      RSS.Close
'      '-------------------------------------------------------------
'      stmp = GetInfo("MOLA_VITI_CONICHE", "INDICE_COMBO", Path_LAVORAZIONE_INI)
'      Select Case val(stmp)
'        Case 0:    ALLONTANAMENTO = 0
'        Case 1:    ALLONTANAMENTO = PZ0 - PZ1
'        Case 2:    ALLONTANAMENTO = PZ0 - PZ3
'        Case 3:    ALLONTANAMENTO = PZ0 - PZ5
'        Case 4:    ALLONTANAMENTO = PZ0 - PZ7
'        Case Else: ALLONTANAMENTO = 0
'      End Select
  Case "VITI_PASSO_VARIABILE"
      NomeTb = "OEM1_MOLA45"
      DEV = Abs(Lp("DE")) 'DIAMETRO ESTERNO
      PZ1 = Lp("PZ1")     'PROFONDITA' DELLA PRIMA   ZONA
      PZ3 = Lp("PZ3")     'PROFONDITA' DELLA TERZA   ZONA
      '-------------------------------------------------------------
      stmp = GetInfo("MOLA_VITI_CONICHE", "INDICE_COMBO", Path_LAVORAZIONE_INI)
      Select Case val(stmp)
        Case 0
          ALLONTANAMENTO = 0
          PAS = Abs(Lp("PASSO1")) 'PASSO INIZIALE
          IMO = Abs(Lp("IMO1"))   'INCLINAZIONE MOLA INIZIALE
        Case 1
          ALLONTANAMENTO = PZ1 - PZ3
          PAS = Abs(Lp("PASSO2")) 'PASSO FINALE
          IMO = Abs(Lp("IMO2"))   'INCLINAZIONE MOLA FINALE
        Case Else
          ALLONTANAMENTO = 0
          PAS = Abs(Lp("PASSO1")) 'PASSO INIZIALE
          IMO = Abs(Lp("IMO1"))   'INCLINAZIONE MOLA INIZIALE
      End Select
      '-------------------------------------------------------------
      DIV = DEV - 2 * PZ1   'DIAMETRO INTERNO DELLA PRIMA ZONA
      '-------------------------------------------------------------
      R45 = Lp("R[45]"): R55 = Lp("R[55]")
      R46 = Lp("R[46]"): R56 = Lp("R[56]")
  Case "BS5L"
      NomeTb = "OEM1_MOLA48"
      IMO = Abs(Lp("R[124]"))  'INCLINAZIONE MOLA
      PAS = Abs(Lp("R[128]"))  'PASSO
      DEV = Abs(Lp("R[189]"))  'DIAMETRO ESTERNO
      PZ1 = Abs(Lp("R[420]"))  'PROFONDITA' DELLA PRIMA   ZONA
      '-------------------------------------------------------------
      DIV = DEV - 2 * PZ1   'DIAMETRO INTERNO DELLA PRIMA ZONA
      '-------------------------------------------------------------
      ALLONTANAMENTO = 0
      '-------------------------------------------------------------
      R45 = Lp("R[45]"): R55 = Lp("R[55]")
      R46 = Lp("R[46]"): R56 = Lp("R[56]")
  Case "BS5S"
      NomeTb = "OEM1_MOLA49"
      IMO = Abs(Lp("R[124]"))  'INCLINAZIONE MOLA
      PAS = Abs(Lp("R[132]"))  'PASSO
      DEV = Abs(Lp("R[189]"))  'DIAMETRO ESTERNO
      PZ0 = Abs(Lp("R[521]"))  'PROFONDITA' DELLA PRIMA   ZONA
      PZ1 = Abs(Lp("R[520]"))  '
      PZ3 = Abs(Lp("R[320]"))  '
      PZ5 = Abs(Lp("R[523]"))  '
      PZ7 = Abs(Lp("R[323]"))  '
      '-------------------------------------------------------------
      DIV = DEV - 2 * PZ0   'DIAMETRO INTERNO DELLA PRIMA ZONA
      '-------------------------------------------------------------
      stmp = GetInfo("MOLA_VITI_CONICHE", "INDICE_COMBO", Path_LAVORAZIONE_INI)
      Select Case val(stmp)
        Case 0:     ALLONTANAMENTO = 0
        Case 1:     ALLONTANAMENTO = PZ0 - PZ1 + PZ5
        Case 2:     ALLONTANAMENTO = PZ0 - PZ3 + PZ7
        Case Else:  ALLONTANAMENTO = 0
      End Select
      '-------------------------------------------------------------
      R45 = Lp("R[45]"): R55 = Lp("R[55]")
      R46 = Lp("R[46]"): R56 = Lp("R[56]")
  Case "VITI_BARRIERA3"
      NomeTb = "OEM1_MOLA47"
      PRI = 1
      '-------------------------------------------------------------
        DEV = Abs(Lp("MO1R[0,189]"))  'DIAMETRO ESTERNO
          If (UCase(ActualGroup) = "OEM1_MOLA47A") Then
        IMO = Abs(Lp("MO1R[0,124]"))  'INCLINAZIONE MOLA
        PAS = Abs(Lp("MO1R[0,128]"))  'PASSO
        PZ1 = Abs(Lp("MO1R[0,220]"))  'PROFONDITA' DELLA PRIMA   ZONA
        DIV = DEV - 2 * PZ1         'DIAMETRO INTERNO DELLA PRIMA ZONA
        R45 = Lp("MO1R[0,45]"): R55 = Lp("MO1R[0,55]")
        R46 = Lp("MO1R[0,46]"): R56 = Lp("MO1R[0,56]")
      ElseIf (UCase(ActualGroup) = "OEM1_MOLA47B") Then
        IMO = Abs(Lp("MO2R[0,124]"))  'INCLINAZIONE MOLA
        PAS = Abs(Lp("MO2R[0,128]"))  'PASSO
        PZ1 = Abs(Lp("MO2R[0,220]"))  'PROFONDITA' DELLA PRIMA   ZONA
        DIV = DEV - 2 * PZ1         'DIAMETRO INTERNO DELLA PRIMA ZONA
        R45 = Lp("MO2R[0,45]"): R55 = Lp("MO2R[0,55]")
        R46 = Lp("MO2R[0,46]"): R56 = Lp("MO2R[0,56]")
      ElseIf (UCase(ActualGroup) = "OEM1_MOLA47C") Then
        IMO = Abs(Lp("MO3R[0,124]"))  'INCLINAZIONE MOLA
        PAS = Abs(Lp("MO3R[0,128]"))  'PASSO
        PZ1 = Abs(Lp("MO3R[0,220]"))  'PROFONDITA' DELLA PRIMA   ZONA
        DIV = DEV - 2 * PZ1         'DIAMETRO INTERNO DELLA PRIMA ZONA
        R45 = Lp("MO3R[0,45]"): R55 = Lp("MO3R[0,55]")
        R46 = Lp("MO3R[0,46]"): R56 = Lp("MO3R[0,56]")
      End If
  Case "VITI_BARRIERA6"
      NomeTb = "OEM1_MOLA50"
      PRI = 1
      '-------------------------------------------------------------
        DEV = Abs(Lp("MO1R[0,189]"))  'DIAMETRO ESTERNO
          If (UCase(ActualGroup) = "OEM1_MOLA50A") Then
        stmp = GetInfo("MOLA_VITI_CONICHE", "INDICE_COMBO", Path_LAVORAZIONE_INI)
        Select Case val(stmp)
        Case 0
        IMO = Abs(Lp("MO1R[0,124]")) 'INCLINAZIONE MOLA
        PAS = Abs(Lp("MO1R[0,214]")) 'PASSO CORTO
        Case 1
        IMO = Abs(Lp("MO1R[0,125]")) 'INCLINAZIONE MOLA
        PAS = Abs(Lp("MO1R[0,215]")) 'PASSO LUNGO
        End Select
        PZ1 = Abs(Lp("MO1R[0,113]")) 'PROFONDITA'
        DIV = DEV - 2 * PZ1          'DIAMETRO INTERNO
        R45 = Lp("MO1R[0,45]"): R55 = Lp("MO1R[0,55]")
        R46 = Lp("MO1R[0,46]"): R56 = Lp("MO1R[0,56]")
      ElseIf (UCase(ActualGroup) = "OEM1_MOLA50B") Then
        IMO = Abs(Lp("MO2R[0,124]")) 'INCLINAZIONE MOLA
        PAS = Abs(Lp("MO2R[0,128]")) 'PASSO
        PZ1 = Abs(Lp("MO2R[0,220]")) 'PROFONDITA'
        DIV = DEV - 2 * PZ1          'DIAMETRO INTERNO
        R45 = Lp("MO2R[0,45]"): R55 = Lp("MO2R[0,55]")
        R46 = Lp("MO2R[0,46]"): R56 = Lp("MO2R[0,56]")
      ElseIf (UCase(ActualGroup) = "OEM1_MOLA50C") Then
        IMO = Abs(Lp("MO3R[0,124]")) 'INCLINAZIONE MOLA
        PAS = Abs(Lp("MO3R[0,128]")) 'PASSO
        PZ1 = Abs(Lp("MO3R[0,321]")) 'PROFONDITA'
        DIV = DEV - 2 * PZ1          'DIAMETRO INTERNO
        R45 = Lp("MO3R[0,45]"): R55 = Lp("MO3R[0,55]")
        R46 = Lp("MO3R[0,46]"): R56 = Lp("MO3R[0,56]")
      End If
  End Select
  If (PAS = 0) Then PAS = 0.0001
  If (PRI <= 0) Then PRI = 1
  '-------------------------------------------------------------
  ALLONTANAMENTO = ALLONTANAMENTO + CORREZIONE_INTERASSE
  i = WritePrivateProfileString("MOLA_VITI_CONICHE", "ALLONTANAMENTO", frmt(ALLONTANAMENTO, 4), Path_LAVORAZIONE_INI)
  '-------------------------------------------------------------
  'GRADI --> RADIANTI
  INF = FnGR(90 - IMO)
  'CALCOLO INTERASSE DI LAVORO
  ENTRX = RM + DIV / 2
  Do
    APF = PG / 2 - 0.001
    RXF = RM
    EXF = XX_PROFONDITA
    GoSub CALRA
    If Abs(DIV / 2 - RXV) > 0.001 Then
      ENTRX = ENTRX + (DIV / 2 - RXV) / 2
    Else
      Exit Do
    End If
  Loop
  'ALLONTANAMENTO DELLA MOLA
  ENTRX = ENTRX + ALLONTANAMENTO
  PROFONDITA = PROFONDITA + R45
  ENTRX = ENTRX - R45
  'AQUISIZIONE PUNTI DEL PROFILO MOLA
  'FIANCO 1 MOLA
  Open g_chOemPATH & "\R47.PNT" For Input As #1
    For i = 1 To NumeroRette
      j = i + 0 * NumeroRette: Input #1, XmF(1, j), YmF(1, j), TMF(1, j)
    Next i
  Close #1
  Open g_chOemPATH & "\R48.PNT" For Input As #1
    For i = 1 To NumeroRette
      j = i + 1 * NumeroRette: Input #1, XmF(1, j), YmF(1, j), TMF(1, j)
    Next i
  Close #1
  Open g_chOemPATH & "\R46.PNT" For Input As #1
    For i = 1 To NumeroRette
      j = i + 2 * NumeroRette: Input #1, XmF(1, j), YmF(1, j), TMF(1, j)
    Next i
  Close #1
  Open g_chOemPATH & "\R49.PNT" For Input As #1
    For i = 1 To NumeroRette
      j = i + 3 * NumeroRette: Input #1, XmF(1, j), YmF(1, j), TMF(1, j)
    Next i
  Close #1
  'FIANCO 2 MOLA
  Open g_chOemPATH & "\R57.PNT" For Input As #1
    For i = 1 To NumeroRette
      j = i + 0 * NumeroRette: Input #1, XmF(2, j), YmF(2, j), TMF(2, j)
    Next i
  Close #1
  Open g_chOemPATH & "\R58.PNT" For Input As #1
    For i = 1 To NumeroRette
      j = i + 1 * NumeroRette: Input #1, XmF(2, j), YmF(2, j), TMF(2, j)
    Next i
  Close #1
  Open g_chOemPATH & "\R56.PNT" For Input As #1
    For i = 1 To NumeroRette
      j = i + 2 * NumeroRette: Input #1, XmF(2, j), YmF(2, j), TMF(2, j)
    Next i
  Close #1
  Open g_chOemPATH & "\R59.PNT" For Input As #1
    For i = 1 To NumeroRette
      j = i + 3 * NumeroRette: Input #1, XmF(2, j), YmF(2, j), TMF(2, j)
    Next i
  Close #1
  'TESTA MOLA
  Open g_chOemPATH & "\R45.PNT" For Input As #1
    For i = 1 To NumeroRette + 1
      j = i + 0 * NumeroRette: Input #1, XmF(0, j), YmF(0, j), TMF(0, j)
    Next i
  Close #1
  'CALCOLO CANALE
  For Fianco = 1 To 2
    COLORE = 0
    'SPALLA --> CANALE
    For i = 1 To NumeroRette
      j = i + 0 * NumeroRette
      APF = TMF(Fianco, j)
      RXF = RM - PROFONDITA + YmF(Fianco, j)
      EXF = XmF(Fianco, j)
      GoSub CALRA
      If (ERRORE = 0) Then
        TvF(Fianco, j) = 0
        XvF(Fianco, j) = EXV
        YvF(Fianco, j) = RXV
      Else
        TvF(Fianco, j) = TvF(Fianco, j - 1)
        XvF(Fianco, j) = XvF(Fianco, j - 1)
        YvF(Fianco, j) = YvF(Fianco, j - 1)
      End If
    Next i
    For i = 1 To NumeroRette - 1
      'CANALE
      j = i + 0 * NumeroRette
      pX1 = (-XvF(Fianco, j) - ZeroPezzoX) * ScalaX + PicW / 2
      pY1 = -(YvF(Fianco, j) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2
      j = j + 1
      pX2 = (-XvF(Fianco, j) - ZeroPezzoX) * ScalaX + PicW / 2
      pY2 = -(YvF(Fianco, j) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2
      FINESTRA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
    Next i
    If SiMOLA = "Y" Then
      For i = 1 To NumeroRette - 1
        'MOLA
        j = i + 0 * NumeroRette
        pX1 = (-XmF(Fianco, j) - ZeroPezzoX) * ScalaX + PicW / 2
        pY1 = (YmF(Fianco, j) - ZeroPezzoY) * ScalaY + PicH / 2
        j = j + 1
        pX2 = (-XmF(Fianco, j) - ZeroPezzoX) * ScalaX + PicW / 2
        pY2 = (YmF(Fianco, j) - ZeroPezzoY) * ScalaY + PicH / 2
        FINESTRA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
      Next i
    End If
    COLORE = 12
    'RAGGIO DI FONDO MOLA --> CANALE
    For i = 1 To NumeroRette
      j = i + 1 * NumeroRette
      APF = TMF(Fianco, j)
      RXF = RM - PROFONDITA + YmF(Fianco, j)
      EXF = XmF(Fianco, j)
      GoSub CALRA
      If (ERRORE = 0) Then
        TvF(Fianco, j) = 0
        XvF(Fianco, j) = EXV
        YvF(Fianco, j) = RXV
      Else
        TvF(Fianco, j) = TvF(Fianco, j - 1)
        XvF(Fianco, j) = XvF(Fianco, j - 1)
        YvF(Fianco, j) = YvF(Fianco, j - 1)
      End If
    Next i
    For i = 1 To NumeroRette - 1
      'CANALE
      j = i + 1 * NumeroRette
      pX1 = (-XvF(Fianco, j) - ZeroPezzoX) * ScalaX + PicW / 2
      pY1 = -(YvF(Fianco, j) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2
      j = j + 1
      pX2 = (-XvF(Fianco, j) - ZeroPezzoX) * ScalaX + PicW / 2
      pY2 = -(YvF(Fianco, j) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2
      FINESTRA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
    Next i
    If SiMOLA = "Y" Then
      For i = 1 To NumeroRette - 1
        'MOLA
        j = i + 1 * NumeroRette
        pX1 = (-XmF(Fianco, j) - ZeroPezzoX) * ScalaX + PicW / 2
        pY1 = (YmF(Fianco, j) - ZeroPezzoY) * ScalaY + PicH / 2
        j = j + 1
        pX2 = (-XmF(Fianco, j) - ZeroPezzoX) * ScalaX + PicW / 2
        pY2 = (YmF(Fianco, j) - ZeroPezzoY) * ScalaY + PicH / 2
        FINESTRA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
      Next i
    End If
    COLORE = 0
    'RACCORDO: (SPALLA MOLA --> RAGGIO DI FONDO MOLA) --> CANALE
    j = 1 * NumeroRette
      pX1 = (-XvF(Fianco, j) - ZeroPezzoX) * ScalaX + PicW / 2
      pY1 = -(YvF(Fianco, j) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2
    j = j + 1
      pX2 = (-XvF(Fianco, j) - ZeroPezzoX) * ScalaX + PicW / 2
      pY2 = -(YvF(Fianco, j) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2
      FINESTRA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
      'SEPARAZIONE
      pX1 = (-XvF(Fianco, j) - ZeroPezzoX) * ScalaX + PicW / 2 - DF
      pY1 = -(YvF(Fianco, j) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2
      pX2 = (-XvF(Fianco, j) - ZeroPezzoX) * ScalaX + PicW / 2 + DF
      pY2 = -(YvF(Fianco, j) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2
      FINESTRA.Line (pX1, pY1)-(pX2, pY2)
      pX1 = (-XvF(Fianco, j) - ZeroPezzoX) * ScalaX + PicW / 2
      pY1 = -(YvF(Fianco, j) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2 - DF
      pX2 = (-XvF(Fianco, j) - ZeroPezzoX) * ScalaX + PicW / 2
      pY2 = -(YvF(Fianco, j) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2 + DF
      FINESTRA.Line (pX1, pY1)-(pX2, pY2)
    COLORE = 9
    'FIANCO MOLA --> CANALE
    For i = 1 To NumeroRette
      j = i + 2 * NumeroRette
      APF = TMF(Fianco, j)
      RXF = RM - PROFONDITA + YmF(Fianco, j)
      EXF = XmF(Fianco, j)
      GoSub CALRA
      If (ERRORE = 0) Then
        TvF(Fianco, j) = 0
        XvF(Fianco, j) = EXV
        YvF(Fianco, j) = RXV
      Else
        TvF(Fianco, j) = TvF(Fianco, j - 1)
        XvF(Fianco, j) = XvF(Fianco, j - 1)
        YvF(Fianco, j) = YvF(Fianco, j - 1)
      End If
    Next i
    For i = 1 To NumeroRette - 1
      'CANALE
      j = i + 2 * NumeroRette
      pX1 = (-XvF(Fianco, j) - ZeroPezzoX) * ScalaX + PicW / 2
      pY1 = -(YvF(Fianco, j) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2
      j = j + 1
      pX2 = (-XvF(Fianco, j) - ZeroPezzoX) * ScalaX + PicW / 2
      pY2 = -(YvF(Fianco, j) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2
      FINESTRA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
    Next i
    If SiMOLA = "Y" Then
      For i = 1 To NumeroRette - 1
        'MOLA
        j = i + 2 * NumeroRette
        pX1 = (-XmF(Fianco, j) - ZeroPezzoX) * ScalaX + PicW / 2
        pY1 = (YmF(Fianco, j) - ZeroPezzoY) * ScalaY + PicH / 2
        j = j + 1
        pX2 = (-XmF(Fianco, j) - ZeroPezzoX) * ScalaX + PicW / 2
        pY2 = (YmF(Fianco, j) - ZeroPezzoY) * ScalaY + PicH / 2
        FINESTRA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
      Next i
    End If
    'RACCORDO: (RAGGIO DI FONDO MOLA --> FIANCO MOLA) --> CANALE
    j = 2 * NumeroRette
      pX1 = (-XvF(Fianco, j) - ZeroPezzoX) * ScalaX + PicW / 2
      pY1 = -(YvF(Fianco, j) - DEV / 2 - -ZeroPezzoY) * ScalaY + PicH / 2
    j = j + 1
      pX2 = (-XvF(Fianco, j) - ZeroPezzoX) * ScalaX + PicW / 2
      pY2 = -(YvF(Fianco, j) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2
      FINESTRA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
      'SEPARAZIONE
      pX1 = (-XvF(Fianco, j) - ZeroPezzoX) * ScalaX + PicW / 2 - DF
      pY1 = -(YvF(Fianco, j) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2
      pX2 = (-XvF(Fianco, j) - ZeroPezzoX) * ScalaX + PicW / 2 + DF
      pY2 = -(YvF(Fianco, j) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2
      FINESTRA.Line (pX1, pY1)-(pX2, pY2)
      pX1 = (-XvF(Fianco, j) - ZeroPezzoX) * ScalaX + PicW / 2
      pY1 = -(YvF(Fianco, j) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2 - DF
      pX2 = (-XvF(Fianco, j) - ZeroPezzoX) * ScalaX + PicW / 2
      pY2 = -(YvF(Fianco, j) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2 + DF
      FINESTRA.Line (pX1, pY1)-(pX2, pY2)
    COLORE = 12
    'RAGGIO DI TESTA MOLA --> CANALE
    For i = 1 To NumeroRette
      j = i + 3 * NumeroRette
      APF = TMF(Fianco, j)
      RXF = RM - PROFONDITA + YmF(Fianco, j)
      EXF = XmF(Fianco, j)
      GoSub CALRA
      If (ERRORE = 0) Then
        TvF(Fianco, j) = 0
        XvF(Fianco, j) = EXV
        YvF(Fianco, j) = RXV
      Else
        TvF(Fianco, j) = TvF(Fianco, j - 1)
        XvF(Fianco, j) = XvF(Fianco, j - 1)
        YvF(Fianco, j) = YvF(Fianco, j - 1)
      End If
    Next i
    For i = 1 To NumeroRette - 1
      'CANALE
      j = i + 3 * NumeroRette
      pX1 = (-XvF(Fianco, j) - ZeroPezzoX) * ScalaX + PicW / 2
      pY1 = -(YvF(Fianco, j) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2
      j = j + 1
      pX2 = (-XvF(Fianco, j) - ZeroPezzoX) * ScalaX + PicW / 2
      pY2 = -(YvF(Fianco, j) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2
      FINESTRA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
    Next i
    If SiMOLA = "Y" Then
      For i = 1 To NumeroRette - 1
        'MOLA
        j = i + 3 * NumeroRette
        pX1 = (-XmF(Fianco, j) - ZeroPezzoX) * ScalaX + PicW / 2
        pY1 = (YmF(Fianco, j) - ZeroPezzoY) * ScalaY + PicH / 2
        j = j + 1
        pX2 = (-XmF(Fianco, j) - ZeroPezzoX) * ScalaX + PicW / 2
        pY2 = (YmF(Fianco, j) - ZeroPezzoY) * ScalaY + PicH / 2
        FINESTRA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
      Next i
    End If
    COLORE = 9
    'RACCORDO: (FIANCO MOLA --> RAGGIO DI TESTA MOLA) --> CANALE
    j = 3 * NumeroRette
      pX1 = (-XvF(Fianco, j) - ZeroPezzoX) * ScalaX + PicW / 2
      pY1 = -(YvF(Fianco, j) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2
    j = j + 1
      pX2 = (-XvF(Fianco, j) - ZeroPezzoX) * ScalaX + PicW / 2
      pY2 = -(YvF(Fianco, j) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2
      FINESTRA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
      'SEPARAZIONE
      pX1 = (-XvF(Fianco, j) - ZeroPezzoX) * ScalaX + PicW / 2 - DF
      pY1 = -(YvF(Fianco, j) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2
      pX2 = (-XvF(Fianco, j) - ZeroPezzoX) * ScalaX + PicW / 2 + DF
      pY2 = -(YvF(Fianco, j) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2
      FINESTRA.Line (pX1, pY1)-(pX2, pY2)
      pX1 = (-XvF(Fianco, j) - ZeroPezzoX) * ScalaX + PicW / 2
      pY1 = -(YvF(Fianco, j) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2 - DF
      pX2 = (-XvF(Fianco, j) - ZeroPezzoX) * ScalaX + PicW / 2
      pY2 = -(YvF(Fianco, j) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2 + DF
      FINESTRA.Line (pX1, pY1)-(pX2, pY2)
  Next Fianco
  'CALCOLO LARGHEZZA CANALE EFFETTTIVA
  For Fianco = 1 To 2
    For i = 1 To 4 * NumeroRette
      'INIZIO FIANCO DEL CANALE DAL DIAMETRO ESTERNO
      If (YvF(Fianco, i) >= DEV / 2) And (YvF(Fianco, i + 1) <= DEV / 2) Then
        Xv(Fianco) = XvF(Fianco, i + 1) + (XvF(Fianco, i) - XvF(Fianco, i + 1)) * (DEV / 2 - YvF(Fianco, i + 1)) / (YvF(Fianco, i) - YvF(Fianco, i + 1))
        IIv(Fianco) = i + 1
      End If
      'FINE FIANCO DEL CANALE DAL DIAMETRO INTERNO
      If (YvF(Fianco, i) > DIV / 2 + ALLONTANAMENTO + DhF(Fianco)) And (YvF(Fianco, i + 1) < (DIV / 2 + ALLONTANAMENTO + DhF(Fianco))) Then
        JJv(Fianco) = i
      End If
    Next i
  Next Fianco
  'VISUALIZZAZIONE AGGIUNTIVA CANALE: COMPLETAMENTO DEL FILETTO
  For Fianco = 1 To 2
    SPOSTAMENTO = (PAS / PRI) * 2 * (Fianco - 1.5)
    COLORE = 9
    'RACCORDO TESTA FILETTO --> FIANCO MOLA
    pX1 = (-Xv(Fianco) - SPOSTAMENTO - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(0 - ZeroPezzoY) * ScalaY + PicH / 2
    j = IIv(Fianco)
    pX2 = (-XvF(Fianco, j) - SPOSTAMENTO - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(YvF(Fianco, j) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2
    FINESTRA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
    'FIANCO MOLA --> CANALE
    For i = 1 To NumeroRette
      j = i + 2 * NumeroRette - 1
      'ELIMINO I PUNTI SUL FIANCO CHE NON LAVORANO
      If (j >= IIv(Fianco)) Then
        pX1 = (-XvF(Fianco, j) - SPOSTAMENTO - ZeroPezzoX) * ScalaX + PicW / 2
        pY1 = -(YvF(Fianco, j) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2
        j = j + 1
        pX2 = (-XvF(Fianco, j) - SPOSTAMENTO - ZeroPezzoX) * ScalaX + PicW / 2
        pY2 = -(YvF(Fianco, j) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2
        FINESTRA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
      End If
    Next i
    COLORE = 12
    'RAGGIO DI TESTA MOLA --> CANALE
    For i = 1 To NumeroRette
      'COSTRUISCO L'INDICE DEI PUNTI SUL FIANCO
      j = i + 3 * NumeroRette - 1
      'ELIMINO I PUNTI SUL RAGGIO CHE CHE NON LAVORANO
      If (j >= IIv(Fianco)) Then
        pX1 = (-XvF(Fianco, j) - SPOSTAMENTO - ZeroPezzoX) * ScalaX + PicW / 2
        pY1 = -(YvF(Fianco, j) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2
        j = j + 1
        pX2 = (-XvF(Fianco, j) - SPOSTAMENTO - ZeroPezzoX) * ScalaX + PicW / 2
        pY2 = -(YvF(Fianco, j) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2
        FINESTRA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
      End If
    Next i
  Next Fianco
  'TESTA DEL FILETTO
  FINESTRA.DrawWidth = 2
  pX1 = (-Xv(1) - ZeroPezzoX) * ScalaX + PicW / 2
  pY1 = -(DEV / 2 - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2
  pX2 = (-(Xv(2) + (PAS / PRI)) - ZeroPezzoX) * ScalaX + PicW / 2
  pY2 = -(DEV / 2 - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2
  FINESTRA.Line (pX1, pY1)-(pX2, pY2)
  pX1 = (-Xv(2) - ZeroPezzoX) * ScalaX + PicW / 2
  pY1 = -(DEV / 2 - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2
  pX2 = (-(Xv(1) - (PAS / PRI)) - ZeroPezzoX) * ScalaX + PicW / 2
  pY2 = -(DEV / 2 - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2
  FINESTRA.Line (pX1, pY1)-(pX2, pY2)
  FINESTRA.DrawWidth = 1
  'TESTA DELLA MOLA --> FONDO DEL CANALE
  COLORE = 9
  For i = 1 To NumeroRette + 1
    j = i
    APF = TMF(0, j)
    RXF = RM - PROFONDITA + YmF(0, j)
    EXF = XmF(0, j)
    GoSub CALRA
    If (ERRORE = 0) Then
      TvF(0, j) = 0
      XvF(0, j) = EXV
      YvF(0, j) = RXV
    Else
      TvF(0, j) = TvF(0, j - 1)
      XvF(0, j) = XvF(0, j - 1)
      YvF(0, j) = YvF(0, j - 1)
    End If
  Next i
  For i = 1 To NumeroRette
    'CANALE
    j = i
    pX1 = (-XvF(0, j) - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(YvF(0, j) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2
    j = j + 1
    pX2 = (-XvF(0, j) - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(YvF(0, j) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2
    FINESTRA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
  Next i
  If SiMOLA = "Y" Then
    For i = 1 To NumeroRette - 1
      'MOLA
      j = i
      pX1 = (-XmF(0, j) - ZeroPezzoX) * ScalaX + PicW / 2
      pY1 = (YmF(0, j) - ZeroPezzoY) * ScalaY + PicH / 2
      j = j + 1
      pX2 = (-XmF(0, j) - ZeroPezzoX) * ScalaX + PicW / 2
      pY2 = (YmF(0, j) - ZeroPezzoY) * ScalaY + PicH / 2
      FINESTRA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
    Next i
  End If
  'RACCORDO: TESTA --> RAGGIO DI TESTA (R49)
  j = 4 * NumeroRette
    pX1 = (-XvF(1, j) - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(YvF(1, j) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = (-XvF(0, 1) - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(YvF(0, 1) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2
    FINESTRA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
    'SEPARAZIONE
    pX1 = (-XvF(0, 1) - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(YvF(0, 1) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2 - DF
    pX2 = (-XvF(0, 1) - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(YvF(0, 1) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2 + DF
    FINESTRA.Line (pX1, pY1)-(pX2, pY2)
    pX1 = (-XvF(0, 1) - ZeroPezzoX) * ScalaX + PicW / 2 - DF
    pY1 = -(YvF(0, 1) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = (-XvF(0, 1) - ZeroPezzoX) * ScalaX + PicW / 2 + DF
    pY2 = -(YvF(0, 1) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2
    FINESTRA.Line (pX1, pY1)-(pX2, pY2)
  'RACCORDO: TESTA --> RAGGIO DI TESTA (R59)
  j = 4 * NumeroRette
    pX1 = (-XvF(2, j) - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(YvF(2, j) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = (-XvF(0, NumeroRette + 1) - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(YvF(0, NumeroRette + 1) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2
    FINESTRA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
    'SEPARAZIONE
    pX1 = (-XvF(0, NumeroRette + 1) - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(YvF(0, NumeroRette + 1) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2 - DF
    pX2 = (-XvF(0, NumeroRette + 1) - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(YvF(0, NumeroRette + 1) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2 + DF
    FINESTRA.Line (pX1, pY1)-(pX2, pY2)
    pX1 = (-XvF(0, NumeroRette + 1) - ZeroPezzoX) * ScalaX + PicW / 2 - DF
    pY1 = -(YvF(0, NumeroRette + 1) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = (-XvF(0, NumeroRette + 1) - ZeroPezzoX) * ScalaX + PicW / 2 + DF
    pY2 = -(YvF(0, NumeroRette + 1) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2
    FINESTRA.Line (pX1, pY1)-(pX2, pY2)
  'DIAMETRO ESTERNO VITE
  pX1 = (0 - ZeroPezzoX) * ScalaX + PicW / 2 - (DF + 1)
  pY1 = -(0 - ZeroPezzoY) * ScalaY + PicH / 2
  pX2 = (0 - ZeroPezzoX) * ScalaX + PicW / 2 + (DF + 1)
  pY2 = -(0 - ZeroPezzoY) * ScalaY + PicH / 2
  FINESTRA.Line (pX1, pY1)-(pX2, pY2)
  pX1 = (0 - ZeroPezzoX) * ScalaX + PicW / 2
  pY1 = -(0 - ZeroPezzoY) * ScalaY + PicH / 2 - (DF + 1)
  pX2 = (0 - ZeroPezzoX) * ScalaX + PicW / 2
  pY2 = -(-PROFONDITA - ZeroPezzoY) * ScalaY + PicH / 2 + (DF + 1)
  FINESTRA.Line (pX1, pY1)-(pX2, pY2)
  XP1 = 0
  YP1 = -PROFONDITA
  XP2 = 0
  YP2 = 0
  GoSub FRECCIE_VERTICALI
  'PROFONDITA'
  stmp = frmt(DEV / 2 - DIV / 2, 4)
  FINESTRA.CurrentX = (0 - ZeroPezzoX) * ScalaX + PicW / 2 - FINESTRA.TextWidth(stmp) / 2
  FINESTRA.CurrentY = -(0 - ZeroPezzoY) * ScalaY + PicH / 2 - (DF + 1) - FINESTRA.TextHeight(stmp)
  FINESTRA.Print stmp
  'RIFERIMENTI
  pX1 = (-Xv(1) - ZeroPezzoX) * ScalaX + PicW / 2
  pY1 = -(-PROFONDITA - ZeroPezzoY) * ScalaY + PicH / 2
  pX2 = (-Xv(2) - ZeroPezzoX) * ScalaX + PicW / 2
  pY2 = -(-PROFONDITA - ZeroPezzoY) * ScalaY + PicH / 2
  FINESTRA.Line (pX1, pY1)-(pX2, pY2)
  FINESTRA.DrawStyle = 4
  pX1 = (-Xv(1) - ZeroPezzoX) * ScalaX + PicW / 2
  pY1 = -(0 - ZeroPezzoY) * ScalaY + PicH / 2
  pX2 = (-Xv(1) - ZeroPezzoX) * ScalaX + PicW / 2
  pY2 = -(-PROFONDITA - ZeroPezzoY) * ScalaY + PicH / 2 + (DF + 1)
  FINESTRA.Line (pX1, pY1)-(pX2, pY2)
  pX1 = (-Xv(2) - ZeroPezzoX) * ScalaX + PicW / 2
  pY1 = -(0 - ZeroPezzoY) * ScalaY + PicH / 2
  pX2 = (-Xv(2) - ZeroPezzoX) * ScalaX + PicW / 2
  pY2 = -(-PROFONDITA - ZeroPezzoY) * ScalaY + PicH / 2 + (DF + 1)
  FINESTRA.Line (pX1, pY1)-(pX2, pY2)
  FINESTRA.DrawStyle = 0
  'LARGHEZZA CANALE
  stmp = frmt(Xv(1) - Xv(2), 3)
  FINESTRA.CurrentX = (0 - ZeroPezzoX) * ScalaX + PicW / 2 - FINESTRA.TextWidth(stmp) / 2
  FINESTRA.CurrentY = -(-PROFONDITA - ZeroPezzoY) * ScalaY + PicH / 2 + (DF + 1)
  FINESTRA.Print stmp
  XP1 = -Xv(2)
  YP1 = -PROFONDITA
  XP2 = -Xv(1)
  YP2 = -PROFONDITA
  GoSub FRECCIE_ORIZZONTALI
  'SPESSORE FILETTO
  COLORE = 12
  chFilettoReale = (PAS / PRI) - (Xv(1) - Xv(2))
  chReale = (Xv(1) - Xv(2))
  stmp = frmt(chFilettoReale, 3)
  FINESTRA.CurrentX = (-Xv(2) - (PAS / PRI) - ZeroPezzoX) * ScalaX + PicW / 2 - FINESTRA.TextWidth(stmp) / 2
  FINESTRA.CurrentY = -(-PROFONDITA - ZeroPezzoY) * ScalaY + PicH / 2 + (DF + 1)
  FINESTRA.Print stmp
  pX1 = (-Xv(1) - ZeroPezzoX) * ScalaX + PicW / 2
  pY1 = -(-PROFONDITA - ZeroPezzoY) * ScalaY + PicH / 2
  pX2 = (-(Xv(2) + (PAS / PRI)) - ZeroPezzoX) * ScalaX + PicW / 2
  pY2 = -(-PROFONDITA - ZeroPezzoY) * ScalaY + PicH / 2
  FINESTRA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
  FINESTRA.DrawStyle = 4
  pX1 = (-(Xv(2) + (PAS / PRI)) - ZeroPezzoX) * ScalaX + PicW / 2
  pY1 = -(0 - ZeroPezzoY) * ScalaY + PicH / 2
  pX2 = (-(Xv(2) + (PAS / PRI)) - ZeroPezzoX) * ScalaX + PicW / 2
  pY2 = -(-PROFONDITA - ZeroPezzoY) * ScalaY + PicH / 2 + (DF + 1)
  FINESTRA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
  FINESTRA.DrawStyle = 0
  XP1 = -Xv(1)
  YP1 = -PROFONDITA
  XP2 = -(Xv(2) + (PAS / PRI))
  YP2 = -PROFONDITA
  GoSub FRECCIE_ORIZZONTALI
  '
  'CALCOLO ANGOLO SUI FIANCHI
  Dim sXh               As Double
  Dim sYh               As Double
  Dim sXhXh             As Double
  Dim sXhYh             As Double
  Dim B                 As Double
  Dim A                 As Double
  Dim CoeffAngolare(2)  As Double
  Dim NumeroPunti       As Integer
  Dim iitmp             As Integer
  Dim XXtmp()           As Double
  Dim YYtmp()           As Double
  '
  For Fianco = 1 To 2
    '--------------------------------------------------------------------
    If (IIv(Fianco) > 3 * NumeroRette) Then
      'TUTTI I PUNTI DEL FIANCO DEL CANALE DESCRITTO DAL FIANCO MOLA
      sXh = 0:   For i = 1 + 2 * NumeroRette To 3 * NumeroRette: sXh = sXh + YvF(Fianco, i):                      Next i
      sYh = 0:   For i = 1 + 2 * NumeroRette To 3 * NumeroRette: sYh = sYh + XvF(Fianco, i):                      Next i
      sXhXh = 0: For i = 1 + 2 * NumeroRette To 3 * NumeroRette: sXhXh = sXhXh + YvF(Fianco, i) * YvF(Fianco, i): Next i
      sXhYh = 0: For i = 1 + 2 * NumeroRette To 3 * NumeroRette: sXhYh = sXhYh + YvF(Fianco, i) * XvF(Fianco, i): Next i
      NumeroPunti = NumeroRette
    Else
      If (DhF(Fianco) > 0) Then
       'DAL DIAMETRO ESTERNO AD UNA DISTANZA FISSA DAL DIAMETRO INTERNO
       sXh = 0:   For i = IIv(Fianco) To JJv(Fianco): sXh = sXh + YvF(Fianco, i):                       Next i
       sYh = 0:   For i = IIv(Fianco) To JJv(Fianco): sYh = sYh + XvF(Fianco, i):                      Next i
       sXhXh = 0: For i = IIv(Fianco) To JJv(Fianco): sXhXh = sXhXh + YvF(Fianco, i) * YvF(Fianco, i): Next i
       sXhYh = 0: For i = IIv(Fianco) To JJv(Fianco): sXhYh = sXhYh + YvF(Fianco, i) * XvF(Fianco, i): Next i
       NumeroPunti = JJv(Fianco) - IIv(Fianco) + 1
      Else
       'DAL DIAMETRO ESTERNO ALLA FINE DEL FIANCO MOLA
       sXh = 0:   For i = IIv(Fianco) To 3 * NumeroRette: sXh = sXh + YvF(Fianco, i):                      Next i
       sYh = 0:   For i = IIv(Fianco) To 3 * NumeroRette: sYh = sYh + XvF(Fianco, i):                      Next i
       sXhXh = 0: For i = IIv(Fianco) To 3 * NumeroRette: sXhXh = sXhXh + YvF(Fianco, i) * YvF(Fianco, i): Next i
       sXhYh = 0: For i = IIv(Fianco) To 3 * NumeroRette: sXhYh = sXhYh + YvF(Fianco, i) * XvF(Fianco, i): Next i
       NumeroPunti = 3 * NumeroRette - IIv(Fianco) + 1
      End If
    End If
    'Coefficiente angolare della retta di regressione
    B = (NumeroPunti * sXhXh - sXh * sXh)
    If (B = 0) Then B = 0.001
    B = (NumeroPunti * sXhYh - sXh * sYh) / B
    'VISUALIZZAZIONE E CONVERSIONE IN GRADI
    B = Atn(B) * 180 / PG * (-Fianco + 1.5) * 2
    CoeffAngolare(Fianco) = B
    '--------------------------------------------------------------------
    stmp = " R" & Format$(Fianco + 3, "0") & "1ch= " & frmt(B, 3)
    FINESTRA.CurrentX = 0
    FINESTRA.CurrentY = FINESTRA.TextHeight(stmp) * Fianco
    FINESTRA.Print stmp
  Next Fianco
  'VISUALIZZO VALORE DEL PASSO
  stmp = " Pitch= " & frmt(PAS, 3)
  FINESTRA.CurrentX = 0
  FINESTRA.CurrentY = 0
  FINESTRA.Print stmp
  'VISUALIZZO VALORE DELL'ELICA
  stmp = " Wheel tilt= " & frmt(IMO, 3)
  FINESTRA.CurrentX = PicW / 6
  FINESTRA.CurrentY = 0
  FINESTRA.Print stmp
  'VISUALIZZAZIONE DELLA DISTANZA DAL DIAMETRO INTERNO REALIZZATO
  COLORE = 12
  Fianco = 1
    If (DhF(Fianco) > 0) Then
      XP1 = -XvF(0, 1)
      YP1 = (YvF(0, 1) - DEV / 2)
      XP2 = XP1
      YP2 = (YvF(Fianco, JJv(Fianco)) - DEV / 2)
      pX1 = (-Xv(Fianco) - ZeroPezzoX) * ScalaX + PicW / 2 - (DF + 1)
      pY1 = -(YP2 - ZeroPezzoY) * ScalaY + PicH / 2
      pX2 = (XP2 - ZeroPezzoX) * ScalaX + PicW / 2 + (DF + 1)
      pY2 = -(YP2 - ZeroPezzoY) * ScalaY + PicH / 2
      FINESTRA.Line (pX1, pY1)-(pX2, pY2)
      pX1 = (XP2 - ZeroPezzoX) * ScalaX + PicW / 2
      pY1 = -(YP2 - ZeroPezzoY) * ScalaY + PicH / 2 - (DF + 1)
      pX2 = (XP2 - ZeroPezzoX) * ScalaX + PicW / 2
      pY2 = -(YP1 - ZeroPezzoY) * ScalaY + PicH / 2 + (DF + 1)
      FINESTRA.Line (pX1, pY1)-(pX2, pY2)
      GoSub FRECCIE_VERTICALI
      stmp = frmt((YvF(Fianco, JJv(Fianco)) - ALLONTANAMENTO - DIV / 2), 3)
      FINESTRA.CurrentX = (XP2 - ZeroPezzoX) * ScalaX + PicW / 2
      FINESTRA.CurrentY = -(YP2 - ZeroPezzoY) * ScalaY + PicH / 2 - (DF + 1) - FINESTRA.TextHeight(stmp)
      FINESTRA.Print stmp
    End If
  Fianco = 2
    If (DhF(Fianco) > 0) Then
      XP1 = -XvF(0, NumeroRette + 1)
      YP1 = (YvF(0, NumeroRette + 1) - DEV / 2)
      XP2 = XP1
      YP2 = (YvF(Fianco, JJv(Fianco)) - DEV / 2)
      pX1 = (-Xv(Fianco) - ZeroPezzoX) * ScalaX + PicW / 2 + (DF + 1)
      pY1 = -(YP2 - ZeroPezzoY) * ScalaY + PicH / 2
      pX2 = (XP2 - ZeroPezzoX) * ScalaX + PicW / 2 - (DF + 1)
      pY2 = -(YP2 - ZeroPezzoY) * ScalaY + PicH / 2
      FINESTRA.Line (pX1, pY1)-(pX2, pY2)
      pX1 = (XP2 - ZeroPezzoX) * ScalaX + PicW / 2
      pY1 = -(YP2 - ZeroPezzoY) * ScalaY + PicH / 2 - (DF + 1)
      pX2 = (XP2 - ZeroPezzoX) * ScalaX + PicW / 2
      pY2 = -(YP1 - ZeroPezzoY) * ScalaY + PicH / 2 + (DF + 1)
      FINESTRA.Line (pX1, pY1)-(pX2, pY2)
      GoSub FRECCIE_VERTICALI
      stmp = frmt((YvF(Fianco, JJv(Fianco)) - ALLONTANAMENTO - DIV / 2), 3)
      FINESTRA.CurrentX = (XP2 - ZeroPezzoX) * ScalaX + PicW / 2 - FINESTRA.TextWidth(stmp)
      FINESTRA.CurrentY = -(YP2 - ZeroPezzoY) * ScalaY + PicH / 2 - (DF + 1) - FINESTRA.TextHeight(stmp)
      FINESTRA.Print stmp
    End If
  '
  'CALCOLO CAVITA' DI FONDO: R45ch
  DISTANZA = Sqr((XvF(0, 1) - XvF(0, NumeroRette + 1)) ^ 2 + (YvF(0, 1) - YvF(0, NumeroRette + 1)) ^ 2)
  Fianco = 0
  k1 = 1
  k2 = (NumeroRette + 1) / 2
  k3 = (NumeroRette + 1)
  XXc = 0: YYc = 0: RRc = 10000000
  'VECCHIO METODO
  'Call CALCOLO_RAGGIO1(XvF(Fianco, k1), YvF(Fianco, k1), XvF(Fianco, K2), YvF(Fianco, K2), XvF(Fianco, k3), YvF(Fianco, k3), XXc, YYc, RRc)
  'NUOVO METODO
  iitmp = 0
  For i = 1 To NumeroRette + 1
    iitmp = iitmp + 1
    ReDim Preserve XXtmp(iitmp):    ReDim Preserve YYtmp(iitmp)
    XXtmp(iitmp) = XvF(Fianco, i):  YYtmp(iitmp) = YvF(Fianco, i)
  Next i
  Call CALCOLO_RAGGIO2(XXtmp, YYtmp, iitmp, XXc, YYc, RRc)
  R45ch = RRc * (1 - Sqr(1 - (DISTANZA / 2 / RRc) ^ 2))
  stmp = " R45ch= " & frmt(R45ch, 3) & " "
  FINESTRA.CurrentX = PicW / 2 - FINESTRA.TextWidth(stmp)
  FINESTRA.CurrentY = PicH - FINESTRA.TextHeight(stmp)
  FINESTRA.Print stmp
  '
  'CALCOLO DELLA CONICITA DI FONDO: R55ch
  '--------------------------------------------------------------------
  sXh = 0:   For i = 1 To NumeroRette + 1: sXh = sXh + XvF(0, i):                 Next i
  sYh = 0:   For i = 1 To NumeroRette + 1: sYh = sYh + YvF(0, i):                 Next i
  sXhXh = 0: For i = 1 To NumeroRette + 1: sXhXh = sXhXh + XvF(0, i) * XvF(0, i): Next i
  sXhYh = 0: For i = 1 To NumeroRette + 1: sXhYh = sXhYh + XvF(0, i) * YvF(0, i): Next i
  'Coefficiente angolare della retta di regressione
  B = ((NumeroRette + 1) * sXhYh - sXh * sYh) / ((NumeroRette + 1) * sXhXh - sXh * sXh)
  '--------------------------------------------------------------------
  'stmp = "R55= " & frmt(Atn(b) * 180 / PG, 3)
  'CONVERSIONE IN mm
  R55ch = B * (XvF(0, NumeroRette + 1) - XvF(0, 1))
  '--------------------------------------------------------------------
  stmp = " R55ch= " & frmt(R55ch, 3) & " "
  FINESTRA.CurrentX = PicW / 2
  FINESTRA.CurrentY = PicH - FINESTRA.TextHeight(stmp)
  FINESTRA.Print stmp
  '
  'Fianco 1
  stmp = "F1"
  FINESTRA.CurrentX = (-XvF(1, 1) - ZeroPezzoX) * ScalaX + PicW / 2 - FINESTRA.TextWidth(stmp) / 2
  FINESTRA.CurrentY = -(YvF(1, 1) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2 - (DF + 1) - FINESTRA.TextHeight(stmp)
  FINESTRA.Print stmp
  'Fianco 2
  stmp = "F2"
  FINESTRA.CurrentX = (-XvF(2, 1) - ZeroPezzoX) * ScalaX + PicW / 2 - FINESTRA.TextWidth(stmp) / 2
  FINESTRA.CurrentY = -(YvF(2, 1) - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2 - (DF + 1) - FINESTRA.TextHeight(stmp)
  FINESTRA.Print stmp
  If (ALLONTANAMENTO <> 0) Then
    COLORE = 12
    XP1 = -Xv(1) + (PAS / PRI) - chFilettoReale / 2
    YP1 = -PROFONDITA
    XP2 = -Xv(1) + (PAS / PRI) - chFilettoReale / 2
    YP2 = 0 - (PROFONDITA - ALLONTANAMENTO)
    pX1 = (XP1 - ZeroPezzoX) * ScalaX + PicW / 2 - (DF + 1)
    pY1 = -(YP1 - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = (XP1 - ZeroPezzoX) * ScalaX + PicW / 2 + (DF + 1)
    pY2 = -(YP1 - ZeroPezzoY) * ScalaY + PicH / 2
    FINESTRA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
    pX1 = (XP2 - ZeroPezzoX) * ScalaX + PicW / 2 - (DF + 1)
    pY1 = -(YP2 - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = (XP2 - ZeroPezzoX) * ScalaX + PicW / 2 + (DF + 1)
    pY2 = -(YP2 - ZeroPezzoY) * ScalaY + PicH / 2
    FINESTRA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
    pX1 = (XP1 - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(YP1 - ZeroPezzoY) * ScalaY + PicH / 2 + (DF + 1)
    pX2 = (XP1 - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(YP2 - ZeroPezzoY) * ScalaY + PicH / 2 - (DF + 1)
    FINESTRA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
    GoSub FRECCIE_VERTICALI
    'ALLONTANAMENTO
    stmp = frmt(ALLONTANAMENTO, 4)
    FINESTRA.CurrentX = (XP2 - ZeroPezzoX) * ScalaX + PicW / 2 - FINESTRA.TextWidth(stmp) / 2
    FINESTRA.CurrentY = -(YP2 - ZeroPezzoY) * ScalaY + PicH / 2 - (DF + 1) - FINESTRA.TextHeight(stmp)
    FINESTRA.Print stmp
  End If
  '
  Dim RAGGIO_MEDIO(2) As Double
  Dim XXc_MEDIO(2)    As Double
  Dim YYc_MEDIO(2)    As Double
  '
  For Fianco = 1 To 2
    RAGGIO_MEDIO(Fianco) = 0
    If (DhF(Fianco) > 0) Then
      'COMPLETO
      'k1 = JJv(Fianco)
      'K2 = (JJv(Fianco) + 4 * NumeroRette) / 2
      'k3 = 4 * NumeroRette
      'XXc = 0: YYc = 0: RRc = 0
      'VECCHIO METODO
      'Call CALCOLO_RAGGIO1(XvF(Fianco, k1), YvF(Fianco, k1), XvF(Fianco, K2), YvF(Fianco, K2), XvF(Fianco, k3), YvF(Fianco, k3), XXc, YYc, RRc)
      'RAGGIO_MEDIO(Fianco) = RAGGIO_MEDIO(Fianco) + Abs(RRc)
      'NUOVO METODO
      iitmp = 0
      XXc = 0: YYc = 0: RRc = 0
      For i = JJv(Fianco) To 4 * NumeroRette
        iitmp = iitmp + 1
        ReDim Preserve XXtmp(iitmp):    ReDim Preserve YYtmp(iitmp)
        XXtmp(iitmp) = XvF(Fianco, i):  YYtmp(iitmp) = YvF(Fianco, i)
      Next i
      Call CALCOLO_RAGGIO2(XXtmp, YYtmp, iitmp, XXc, YYc, RRc)
      'MEDIO
      RAGGIO_MEDIO(Fianco) = Abs(RRc)
      XXc_MEDIO(Fianco) = XXc
      YYc_MEDIO(Fianco) = YYc
      'DISEGNO IL RAGGIO DI FONDO DEL CANALE
      pX1 = XXc_MEDIO(Fianco) + RAGGIO_MEDIO(Fianco)
      pY1 = YYc_MEDIO(Fianco)
      DeltaAng = 2 * PG / NumeroRette
      For j = 1 To NumeroRette
        Ang = j * DeltaAng
        pX2 = XXc_MEDIO(Fianco) + RAGGIO_MEDIO(Fianco) * Cos(Ang)
        pY2 = YYc_MEDIO(Fianco) + RAGGIO_MEDIO(Fianco) * Sin(Ang)
        'Adattamento nella finestra grafica
        pX1 = (-pX1 - ZeroPezzoX) * ScalaX + PicW / 2
        pY1 = -(pY1 - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2
        pX2 = (-pX2 - ZeroPezzoX) * ScalaX + PicW / 2
        pY2 = -(pY2 - DEV / 2 - ZeroPezzoY) * ScalaY + PicH / 2
        'Traccio la linea
        If (pY1 <= FINESTRA.ScaleHeight) And (pX1 <= FINESTRA.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          FINESTRA.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
        End If
        'Aggiorno Px1 e Py1
        pX1 = XXc_MEDIO(Fianco) + RAGGIO_MEDIO(Fianco) * Cos(Ang)
        pY1 = YYc_MEDIO(Fianco) + RAGGIO_MEDIO(Fianco) * Sin(Ang)
      Next j
    Else
      'VECCHIO METODO
      'SUPERIORE
      'k1 = 3 * NumeroRette + 0 * NumeroRette / 4
      'K2 = 3 * NumeroRette + 1 * NumeroRette / 4
      'k3 = 3 * NumeroRette + 2 * NumeroRette / 4
      'XXc = 0: YYc = 0: RRc = 0
      'Call CALCOLO_RAGGIO1(XvF(Fianco, k1), YvF(Fianco, k1), XvF(Fianco, K2), YvF(Fianco, K2), XvF(Fianco, k3), YvF(Fianco, k3), XXc, YYc, RRc)
      'RAGGIO_MEDIO(Fianco) = RAGGIO_MEDIO(Fianco) + Abs(RRc)
      'INFERIORE
      'k1 = 3 * NumeroRette + 2 * NumeroRette / 4
      'K2 = 3 * NumeroRette + 3 * NumeroRette / 4
      'k3 = 3 * NumeroRette + 4 * NumeroRette / 4
      'XXc = 0: YYc = 0: RRc = 0
      'Call CALCOLO_RAGGIO1(XvF(Fianco, k1), YvF(Fianco, k1), XvF(Fianco, K2), YvF(Fianco, K2), XvF(Fianco, k3), YvF(Fianco, k3), XXc, YYc, RRc)
      'RAGGIO_MEDIO(Fianco) = RAGGIO_MEDIO(Fianco) + Abs(RRc)
      'MEDIO
      'RAGGIO_MEDIO(Fianco) = RAGGIO_MEDIO(Fianco) / 2
      'NUOVO METODO
      iitmp = 0
      XXc = 0: YYc = 0: RRc = 0
      For i = 3 * NumeroRette To 4 * NumeroRette
        iitmp = iitmp + 1
        ReDim Preserve XXtmp(iitmp):    ReDim Preserve YYtmp(iitmp)
        XXtmp(iitmp) = XvF(Fianco, i):  YYtmp(iitmp) = YvF(Fianco, i)
      Next i
      Call CALCOLO_RAGGIO2(XXtmp, YYtmp, iitmp, XXc, YYc, RRc)
      'MEDIO
      RAGGIO_MEDIO(Fianco) = Abs(RRc)
      XXc_MEDIO(Fianco) = XXc
      YYc_MEDIO(Fianco) = YYc
    End If
    'RAGGI DI FONDO DEL CANALE
    stmp = " RF" & Format$(Fianco, "0") & "ch= " & frmt(RAGGIO_MEDIO(Fianco), 2)
    FINESTRA.CurrentX = 0
    FINESTRA.CurrentY = FINESTRA.TextHeight(stmp) * (4 + Fianco)
    FINESTRA.Print stmp
  Next Fianco
  '
  'CALCOLO BOMBATURA SUI FIANCHI
  For Fianco = 1 To 2
    'LUNGHEZZA DEL FIANCO
    DISTANZA = Sqr((Xv(Fianco) - XvF(Fianco, 3 * NumeroRette)) ^ 2 + (DEV / 2 - YvF(Fianco, 3 * NumeroRette)) ^ 2)
    'RAGGIO SUL FIANCO
    If (DhF(Fianco) <= 0) Then
      K5 = (IIv(Fianco) - 2 * NumeroRette)
      K5 = Int(K5 / 2) * 2
      k4 = Int(NumeroRette / 2) * 2 + 1
      k1 = 2 * NumeroRette + K5
      k2 = 2 * NumeroRette + (K5 + k4) / 2
      k3 = 2 * NumeroRette + k4
    Else
      k1 = IIv(Fianco)
      k2 = Int((IIv(Fianco) + JJv(Fianco)) / 2)
      k3 = JJv(Fianco)
    End If
    XXc = 0: YYc = 0: RRc = 10000000
    Call CALCOLO_RAGGIO1(Xv(Fianco), DEV / 2, XvF(Fianco, k2), YvF(Fianco, k2), XvF(Fianco, k3), YvF(Fianco, k3), XXc, YYc, RRc)
    'CAVITA' SUL FIANCO DEL CANALE
    If (Fianco = 1) Then
      R46ch = -RRc * (1 - Sqr(1 - (DISTANZA / 2 / RRc) ^ 2))
      R46ch = Int(R46ch * 1000) / 1000
      stmp = " CF" & Format$(Fianco, "0") & "ch= " & frmt(R46ch, 3) & " "
      FINESTRA.CurrentX = PicW / 2 - FINESTRA.TextWidth(stmp)
      FINESTRA.CurrentY = 0
      FINESTRA.Print stmp
    End If
    If (Fianco = 2) Then
      R56ch = RRc * (1 - Sqr(1 - (DISTANZA / 2 / RRc) ^ 2))
      R56ch = Int(R56ch * 1000) / 1000
      stmp = " CF" & Format$(Fianco, "0") & "ch= " & frmt(R56ch, 3) & " "
      FINESTRA.CurrentX = PicW / 2
      FINESTRA.CurrentY = 0
      FINESTRA.Print stmp
    End If
  Next Fianco
  '
  'VISUALIZZO IL DIAMETRO ESTERNO
  stmp = " De= " & frmt(DEV, 4)
  FINESTRA.CurrentX = 0
  FINESTRA.CurrentY = FINESTRA.TextHeight(stmp) * 7
  FINESTRA.Print stmp
  'VISUALIZZO IL DIAMETRO INTERNO
  stmp = " Di= " & frmt(DIV + CORREZIONE_INTERASSE * 2, 4)
  FINESTRA.CurrentX = 0
  FINESTRA.CurrentY = FINESTRA.TextHeight(stmp) * 8
  FINESTRA.Print stmp
  If CORREZIONE_INTERASSE <> 0 Then
    'LINEA ORIZZONTALE DI ZERO MOLA
    pX1 = (-(PAS / PRI) / 2 - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(CORREZIONE_INTERASSE - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = ((PAS / PRI) / 2 - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(CORREZIONE_INTERASSE - ZeroPezzoY) * ScalaY + PicH / 2
    FINESTRA.Line (pX1, pY1)-(pX2, pY2)
  End If
  'DISTANZE DAL CENTRO MOLA
  For Fianco = 1 To 2
    stmp = " L" & Format$(Fianco, "0") & "ch= " & frmt(Abs(Xv(Fianco)), 3)
    FINESTRA.CurrentX = 0
    FINESTRA.CurrentY = FINESTRA.TextHeight(stmp) * (8 + Fianco)
    FINESTRA.Print stmp
  Next Fianco
  'DISTANZE DAI FIANCHI MOLA
  stmp = " D1ch= " & frmt(Xv(1) - XvF(0, 1), 3)
  FINESTRA.CurrentX = 0
  FINESTRA.CurrentY = FINESTRA.TextHeight(stmp) * (11)
  FINESTRA.Print stmp
  stmp = " D2ch= " & frmt(-Xv(2) + XvF(0, NumeroRette + 1), 3)
  FINESTRA.CurrentX = 0
  FINESTRA.CurrentY = FINESTRA.TextHeight(stmp) * (12)
  FINESTRA.Print stmp
  'SPESSORE FILETTO NORMALE REALE
  stmp = " FLIGHT (n)= " & frmt(chFilettoReale * Cos(IMO * PG / 180), 3)
  FINESTRA.CurrentX = 0
  FINESTRA.CurrentY = FINESTRA.TextHeight(stmp) * 3
  FINESTRA.Print stmp
  'LARGHEZZA CANALE NORMALE REALE
  stmp = " CHANNEL (n)= " & frmt(chReale * Cos(IMO * PG / 180), 3)
  FINESTRA.CurrentX = 0
  FINESTRA.CurrentY = FINESTRA.TextHeight(stmp) * 4
  FINESTRA.Print stmp
  'CALCOLO AREA RISPETTO AL DIAMETRO ESTERNO
  Dim AREA(2) As Double
  For Fianco = 1 To 2
    AREA(Fianco) = 0
    pX1 = Xv(Fianco)
    pY1 = (DEV / 2) - (DEV / 2)
    pX2 = XvF(Fianco, IIv(Fianco))
    pY2 = (DEV / 2) - YvF(Fianco, IIv(Fianco))
    AREA(Fianco) = AREA(Fianco) + Abs(pX1 - pX2) * (pY1 + pY2) / 2
    For i = IIv(Fianco) To 3 * NumeroRette - 1
      pX1 = XvF(Fianco, i + 0)
      pY1 = (DEV / 2) - YvF(Fianco, i + 0)
      pX2 = XvF(Fianco, i + 1)
      pY2 = (DEV / 2) - YvF(Fianco, i + 1)
      AREA(Fianco) = AREA(Fianco) + Abs(pX1 - pX2) * (pY1 + pY2) / 2
    Next i
    For i = 3 * NumeroRette To 4 * NumeroRette - 1
      pX1 = XvF(Fianco, i + 0)
      pY1 = (DEV / 2) - YvF(Fianco, i + 0)
      pX2 = XvF(Fianco, i + 1)
      pY2 = (DEV / 2) - YvF(Fianco, i + 1)
      AREA(Fianco) = AREA(Fianco) + Abs(pX1 - pX2) * (pY1 + pY2) / 2
    Next i
  Next Fianco
  Fianco = 0
    AREA(Fianco) = 0
    For i = 1 To NumeroRette
      pX1 = XvF(Fianco, i + 0)
      pY1 = (DEV / 2) - YvF(Fianco, i + 0)
      pX2 = XvF(Fianco, i + 1)
      pY2 = (DEV / 2) - YvF(Fianco, i + 1)
      AREA(Fianco) = AREA(Fianco) + Abs(pX1 - pX2) * (pY1 + pY2) / 2
    Next i
  stmp = " AREA= " & frmt(AREA(1) + AREA(0) + AREA(2), 3)
  FINESTRA.CurrentX = 0
  FINESTRA.CurrentY = PicH - FINESTRA.TextHeight(stmp)
  FINESTRA.Print stmp
  'ESTENSIONE DEL FONDO
  stmp = " FLAT= " & frmt(XvF(0, 1) - XvF(0, NumeroRette + 1), 3) & " "
  FINESTRA.CurrentX = PicW - FINESTRA.TextWidth(stmp)
  FINESTRA.CurrentY = PicH - FINESTRA.TextHeight(stmp)
  FINESTRA.Print stmp
  'EVENTUALE AGGIORNAMENTO DEL SOTTOPROGRAMMA
  ret = InStr(ACTUAL_PARAMETER_GROUP, "OEM1_MOLA")
  If (ret > 0) And (SiStampa = "N") Then
    Dim MODALITA As String
    Dim sRESET   As String
    Dim iRESET   As Integer
    Dim Valore   As Double
    Dim sVALORE  As String
    Dim INVIO    As Integer
    MODALITA = GetInfo("Configurazione", "Modalita", PathFILEINI)
    MODALITA = UCase$(Trim$(MODALITA))
    If (MODALITA = "OFF-LINE") Then
      iRESET = 1
    Else
    sRESET = GetInfo("CONFIGURAZIONE", "sRESET", PathFILEINI)
    If (Len(sRESET) > 0) Then
      iRESET = val(OPC_LEGGI_DATO(sRESET))
    Else
      iRESET = 1
    End If
    End If
    INVIO = 0
    If (iRESET = 1) Then
      If (Abs(R45ch) > 0.001) Then
        'OBIETTIVO: R45ch ~ 0.001
        Valore = R45 - R45ch
        If (Valore > 0) Then
          sVALORE = frmt(Valore, 3)
              If (Right(ACTUAL_PARAMETER_GROUP, 1) = "A") Then
            Call SP("MO1R[0,45]", sVALORE)
          ElseIf (Right(ACTUAL_PARAMETER_GROUP, 1) = "B") Then
            Call SP("MO2R[0,45]", sVALORE)
          ElseIf (Right(ACTUAL_PARAMETER_GROUP, 1) = "C") Then
            Call SP("MO3R[0,45]", sVALORE)
          Else
            Call SP("R[45]", sVALORE)
          End If
          INVIO = 1
        End If
      End If
      If (Abs(R55ch) > 0.001) Then
        'OBIETTIVO: R55ch ~ 0.001
        Valore = R55 - R55ch
          sVALORE = frmt(Valore, 3)
              If (Right(ACTUAL_PARAMETER_GROUP, 1) = "A") Then
            Call SP("MO1R[0,55]", sVALORE)
          ElseIf (Right(ACTUAL_PARAMETER_GROUP, 1) = "B") Then
            Call SP("MO2R[0,55]", sVALORE)
          ElseIf (Right(ACTUAL_PARAMETER_GROUP, 1) = "C") Then
            'ELIMINAZIONE DELLA CORREZIONE PER GESTIONE MANUALE SVG130318
            'Call SP("MO3R[0,55]", sVALORE)
            stmp = " R55ch= " & frmt(R55ch, 3) & " ohne Korrektur!!!"
            FINESTRA.CurrentX = PicW / 2
            FINESTRA.CurrentY = PicH - FINESTRA.TextHeight(stmp)
            FINESTRA.Print stmp
          Else
            Call SP("R[55]", sVALORE)
          End If
          INVIO = 1
      End If
      If (Abs(R46ch) > 0.001) Then
        'OBIETTIVO: R46ch ~ 0.001
        Valore = R46 - R46ch
        If (Valore > 0) Then
          sVALORE = frmt(Valore, 3)
              If (Right(ACTUAL_PARAMETER_GROUP, 1) = "A") Then
            stmp = LNP("MO1R[0,46]")
          ElseIf (Right(ACTUAL_PARAMETER_GROUP, 1) = "B") Then
            stmp = LNP("MO2R[0,46]")
          ElseIf (Right(ACTUAL_PARAMETER_GROUP, 1) = "C") Then
            stmp = LNP("MO3R[0,46]")
          Else
            stmp = LNP("R[46]")
          End If
          StopRegieEvents
          ret = MsgBox(stmp & " F1" & Chr(13) & frmt(R46, 3) & " ---> " & sVALORE, vbQuestion + vbDefaultButton2 + vbYesNo, "WARNING")
          ResumeRegieEvents
          If (ret = vbYes) Then
              If (Right(ACTUAL_PARAMETER_GROUP, 1) = "A") Then
            Call SP("MO1R[0,46]", sVALORE)
          ElseIf (Right(ACTUAL_PARAMETER_GROUP, 1) = "B") Then
            Call SP("MO2R[0,46]", sVALORE)
          ElseIf (Right(ACTUAL_PARAMETER_GROUP, 1) = "C") Then
            Call SP("MO3R[0,46]", sVALORE)
          Else
            Call SP("R[46]", sVALORE)
          End If
          INVIO = 1
          End If
        End If
      End If
      If (Abs(R56ch) > 0.001) Then
        'OBIETTIVO: R56ch ~ 0.001
        Valore = R56 - R56ch
        If (Valore > 0) Then
          sVALORE = frmt(Valore, 3)
              If (Right(ACTUAL_PARAMETER_GROUP, 1) = "A") Then
            stmp = LNP("MO1R[0,56]")
          ElseIf (Right(ACTUAL_PARAMETER_GROUP, 1) = "B") Then
            stmp = LNP("MO2R[0,56]")
          ElseIf (Right(ACTUAL_PARAMETER_GROUP, 1) = "C") Then
            stmp = LNP("MO3R[0,56]")
          Else
            stmp = LNP("R[56]")
          End If
          StopRegieEvents
          ret = MsgBox(stmp & " F2" & Chr(13) & frmt(R56, 3) & " ---> " & sVALORE, vbQuestion + vbDefaultButton2 + vbYesNo, "WARNING")
          ResumeRegieEvents
          If (ret = vbYes) Then
              If (Right(ACTUAL_PARAMETER_GROUP, 1) = "A") Then
            Call SP("MO1R[0,56]", sVALORE)
          ElseIf (Right(ACTUAL_PARAMETER_GROUP, 1) = "B") Then
            Call SP("MO2R[0,56]", sVALORE)
          ElseIf (Right(ACTUAL_PARAMETER_GROUP, 1) = "C") Then
            Call SP("MO3R[0,56]", sVALORE)
          Else
            Call SP("R[56]", sVALORE)
          End If
          INVIO = 1
          End If
        End If
      End If
      If (INVIO = 1) And (MODALITA = "SPF") Then Call OEMX.SuOEM1.CreaSPF
    Else
      WRITE_DIALOG "Data cannot be modified: CNC is not on RESET state"
    End If
  End If
  '
Exit Sub

CALRA:
Dim v7  As Double
Dim v8  As Double
Dim V9  As Double
Dim V12 As Double
Dim V13 As Double
Dim V14 As Double
Dim V15 As Double
Dim V16 As Double
Dim VPB As Double
  '************** S/P ************ CALCUL PIECE D'APRES FRAISE
  ' Entrees  constantes : ENTRX , PAS , INF
  ' RXF=###.###  EXF=###.###  APF=###.###"; RXF; EXF; FNRD(APF)
  ' Sorties             : RXV , EXV
  RXV = 0: ERRORE = 0
  v7 = PAS / 2 / PG
  V12 = -Tan(APF) * (ENTRX * Sin(INF) + v7 * Cos(INF))
  V13 = v7 * Sin(INF) - ENTRX * Cos(INF)
  V14 = Cos(INF) * ((-Tan(APF) * EXF) + RXF)
  V15 = (V14 ^ 2) + (V12 ^ 2) - (V13 ^ 2)
  If (V15 > 0) Then
    V15 = Sqr(V15)
    V16 = -(V15 + V12) / (V13 - V14)
    V15 = (V15 - V12) / (V13 - V14)
    VPB = V16
    If Abs(V15) < Abs(V16) Then VPB = V15
    V12 = 2 * Atn(VPB)
    V13 = (RXF * Sin(INF) * Sin(V12)) + EXF * Cos(INF)
    V13 = Atn(V13 / ((RXF * Cos(V12)) - ENTRX))
    RXV = (ENTRX - RXF * Cos(V12)) / Cos(V13)
    EXV = EXF * Sin(INF) - v7 * V13 - RXF * Sin(V12) * Cos(INF)
    Ang = 2 * PG * EXV / PAS  'BETA
  Else
    'StopRegieEvents
    'MsgBox "CALCULATION NOT POSSIBLE:" & Chr(13) & "CHANNEL POINT FROM WHEEL ERROR (LABEL CALRA)", 32, "SUB VISUALIZZA_CANALE"
    'ResumeRegieEvents
    'Exit Sub
    'Write_Dialog "CHANNEL POINT FROM WHEEL ERROR (SUB VISUALIZZA_CANALE CALRA)"
    ERRORE = 1
  End If
  '
Return

FRECCIE_VERTICALI:
  Ang = AF / 180 * PG
  DeltaAng = Ang / NumeroRette
  'FRECCIA DI SOTTO
  pX2 = (XP1 - ZeroPezzoX) * ScalaX + PicW / 2
  pY2 = -(YP1 - ZeroPezzoY) * ScalaY + PicH / 2
  For i = -NumeroRette To NumeroRette
    pX1 = (XP1 - ZeroPezzoX) * ScalaX - DF * Sin(i * DeltaAng) + PicW / 2
    pY1 = -(YP1 - ZeroPezzoY) * ScalaY + DF * Cos(Ang) + PicH / 2
    FINESTRA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
  Next i
  'FRECCIA DI SOPRA
  pX2 = (XP2 - ZeroPezzoX) * ScalaX + PicW / 2
  pY2 = -(YP2 - ZeroPezzoY) * ScalaY + PicH / 2
  For i = -NumeroRette To NumeroRette
    pX1 = (XP2 - ZeroPezzoX) * ScalaX - DF * Sin(i * DeltaAng) + PicW / 2
    pY1 = -(YP2 - ZeroPezzoY) * ScalaY - DF * Cos(Ang) + PicH / 2
    FINESTRA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
  Next i
Return

FRECCIE_ORIZZONTALI:
  Ang = AF / 180 * PG
  DeltaAng = Ang / NumeroRette
  'FRECCIA DI SOTTO
  pX2 = (XP1 - ZeroPezzoX) * ScalaX + PicW / 2
  pY2 = -(YP1 - ZeroPezzoY) * ScalaY + PicH / 2
  For i = -NumeroRette To NumeroRette
    pX1 = (XP1 - ZeroPezzoX) * ScalaX - DF * Cos(Ang) + PicW / 2
    pY1 = -(YP1 - ZeroPezzoY) * ScalaY - DF * Sin(i * DeltaAng) + PicH / 2
    FINESTRA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
  Next i
  'FRECCIA DI SOPRA
  pX2 = (XP2 - ZeroPezzoX) * ScalaX + PicW / 2
  pY2 = -(YP2 - ZeroPezzoY) * ScalaY + PicH / 2
  For i = -NumeroRette To NumeroRette
    pX1 = (XP2 - ZeroPezzoX) * ScalaX + DF * Cos(Ang) + PicW / 2
    pY1 = -(YP2 - ZeroPezzoY) * ScalaY + DF * Sin(i * DeltaAng) + PicH / 2
    FINESTRA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
  Next i
Return

errVISUALIZZA_CANALE:
  WRITE_DIALOG "Sub VISUALIZZA_CANALE: ERROR -> " & Err
Resume Next

End Sub

Sub VISUALIZZA_MOLA()
'
'RISOLUZIONE PER LA BOMBATURA
Const CIFRE = 15
Const COLONNA = 20
'
'VARIE
Dim ftmp As Double
Dim stmp As String
Dim Atmp As String * 255
Dim i    As Integer
'
'GRAFICA
Dim ZeroPezzoX As Single
Dim ZeroPezzoY As Single
Dim pX1        As Single
Dim pX2        As Single
Dim pY1        As Single
Dim pY2        As Single
Dim PicH       As Single
Dim PicW       As Single
Dim COLORE     As Integer
'
'VARIE
Dim Scala          As Single
Dim ScalaX         As Single
Dim ScalaY         As Single
Dim NumeroRette    As Integer
Dim DF             As Single
Dim AF             As Single
Dim XXp1           As Double
Dim YYp1           As Double
Dim AAP1           As Double
Dim XXp2           As Double
Dim YYp2           As Double
Dim AAP2           As Double
'
Dim PROFONDITA     As Single
Dim XX_PROFONDITA  As Single
Dim ALLONTANAMENTO As Single
'
Dim APF       As Double
Dim AT1       As Double
Dim AT11      As Double
Dim GAMMA     As Double
Dim Delta     As Double
Dim angF      As Double
Dim angC1     As Double
Dim angC11    As Double
Dim DIAG      As Double
Dim ac        As Double
Dim AB        As Double
Dim BC        As Double
Dim RAD       As Double
Dim HF        As Double
Dim dALFA     As Double
Dim ALFA      As Double
Dim Beta      As Double
Dim GAMMA1    As Double
Dim GAMMA11   As Double
Dim NomeMOLA  As String
Dim R184      As Single
Dim R209      As Integer
'
On Error GoTo errVISUALIZZA_MOLA
  '
  WRITE_DIALOG "WHEEL"
  '-------------------------------------------------------------
  R184 = 0 'SEMPRE 0 PER IL RULLO
  '-------------------------------------------------------------
  stmp = GetInfo("MOLA_VITI_CONICHE", "NumeroRette", Path_LAVORAZIONE_INI)
  NumeroRette = val(stmp): If NumeroRette < 1 Then NumeroRette = 1
  'Lettura della risoluzione per le freccie
  stmp = GetInfo("MOLA_VITI_CONICHE", "DF", Path_LAVORAZIONE_INI): DF = val(stmp)
  stmp = GetInfo("MOLA_VITI_CONICHE", "AF", Path_LAVORAZIONE_INI): AF = val(stmp)
  '-------------------------------------------------------------
  stmp = GetInfo("MOLA_VITI_CONICHE", "Scala", Path_LAVORAZIONE_INI): Scala = val(stmp)
  If (Scala <= 0) Then Scala = 1
  ScalaX = Scala:  ScalaY = Scala
  'VISUALIZZO LA SCALA
  i = LoadString(g_hLanguageLibHandle, 1000, Atmp, 255)
  If (i > 0) Then OEMX.lblINP(0).Caption = Left$(Atmp, i)
  OEMX.txtINP(0).Text = frmt(Scala, 4)
  '-------------------------------------------------------------
  stmp = GetInfo("MOLA_VITI_CONICHE", "PicW", Path_LAVORAZIONE_INI): PicW = val(stmp)
  stmp = GetInfo("MOLA_VITI_CONICHE", "PicH", Path_LAVORAZIONE_INI): PicH = val(stmp)
  '-------------------------------------------------------------
  stmp = GetInfo("MOLA_VITI_CONICHE", "ZeroPezzoX", Path_LAVORAZIONE_INI): ZeroPezzoX = val(stmp)
  stmp = GetInfo("MOLA_VITI_CONICHE", "ZeroPezzoY", Path_LAVORAZIONE_INI): ZeroPezzoY = val(stmp)
  '-------------------------------------------------------------
  stmp = GetInfo("MOLA_VITI_CONICHE", "ALLONTANAMENTO", Path_LAVORAZIONE_INI): ALLONTANAMENTO = val(stmp)
  '-------------------------------------------------------------
  NomeMOLA = LEGGI_PARAMETRI_MOLA_CONICA
  'CONVERSIONE GRADI --> RADIANTI
  R41 = R41 / 180 * PG
  R47 = R47 / 180 * PG
  'CONVERSIONE GRADI --> RADIANTI
  R51 = R51 / 180 * PG
  R57 = R57 / 180 * PG
  'LARGHEZZA MOLA
  If (R37 = 0) Then R37 = R38 + R48 + R58 + 2
  'CavitÓ solo positiva !
  If (R45 <= 0.0001) Then R45 = 0.0001
  'GESTIONE CONICITA' FONDO
  R44 = R44 + R55 / 2
  R54 = R54 - R55 / 2
  'Bombatura solo positiva !
  If (R46 <= 0.0001) Then R46 = 0.0001
  If (R56 <= 0.0001) Then R56 = 0.0001
  OEMX.OEM1PicCALCOLO.CurrentX = 0
  OEMX.OEM1PicCALCOLO.CurrentY = OEMX.OEM1PicCALCOLO.ScaleHeight - 2 * OEMX.OEM1PicCALCOLO.TextHeight("X")
  OEMX.OEM1PicCALCOLO.Print " " & NomeMOLA & " "
  'Punto C1
  DIAG = R48 / Cos(PG / 4 - R47 / 2 - R41 / 2)
  YY(0, 31) = -R43 + DIAG * Sin(PG / 4 - R47 / 2 + R41 / 2)
  XX(0, 31) = R38 / 2 + DIAG * Cos(PG / 4 - R47 / 2 + R41 / 2) + R43 * Tan(R41)
  'Punto C11
  DIAG = R58 / Cos(PG / 4 - R57 / 2 - R51 / 2)
  YY(0, 41) = -R53 + DIAG * Sin(PG / 4 - R57 / 2 + R51 / 2)
  XX(0, 41) = -R38 / 2 - DIAG * Cos(PG / 4 - R57 / 2 + R51 / 2) - R53 * Tan(R51)
  'Punto P2
  XX(0, 2) = XX(0, 31) - R48 * Sin(R47)
  YY(0, 2) = YY(0, 31) - R48 * Cos(R47)
  'Punto P12
  XX(0, 12) = XX(0, 41) + R58 * Sin(R57)
  YY(0, 12) = YY(0, 41) - R58 * Cos(R57)
  'Punto P1
  XX(0, 1) = R37 / 2
  YY(0, 1) = YY(0, 2) - (XX(0, 1) - XX(0, 2)) * Tan(R47)
  'Punto P11
  XX(0, 11) = -R37 / 2
  YY(0, 11) = YY(0, 12) - (XX(0, 12) - XX(0, 11)) * Tan(R57)
  'Punto C3
  YY(0, 33) = R44 - R43 - R49
  XX(0, 33) = R38 / 2 - R49 * Tan((PG / 2 - R41) / 2) - (R44 - R43) * Tan(R41)
  'Punto C13
  YY(0, 43) = R54 - R53 - R59
  XX(0, 43) = -R38 / 2 + R59 * Tan((PG / 2 - R51) / 2) + (R54 - R53) * Tan(R51)
  'Calcolo R-bombatura
  'F1
  HF = (YY(0, 33) - YY(0, 31) + (R48 + R49) * Sin(R41)) / Cos(R41) / 2
  RR(0, 1) = (HF ^ 2 + R46 ^ 2) / 2 / R46
  ALFA = Atn((YY(0, 33) - YY(0, 31)) / (XX(0, 31) - XX(0, 33)))
  ac = RR(0, 1) - R49
  BC = RR(0, 1) + R48
  AB = (XX(0, 31) - XX(0, 33)) / Cos(ALFA)
  RAD = ((ac ^ 2 + AB ^ 2 - BC ^ 2) / (2 * AB * ac))
  If (Abs(RAD) > 1) Then
    StopRegieEvents
    MsgBox "CALCULATION NOT POSSIBLE:" & Chr(13) & "FLANK CROWN F1 TOO BIG", 32, "SUB: VISUALIZZA_MOLA"
    ResumeRegieEvents
    Exit Sub
  End If
  Beta = FnACS(RAD)
  If Beta < 0 Then Beta = PG + Beta
  GAMMA1 = PG - (ALFA + Beta)
  'F2
  HF = (YY(0, 43) - YY(0, 41) + (R58 + R59) * Sin(R51)) / Cos(R51) / 2
  RR(0, 11) = (HF ^ 2 + R56 ^ 2) / 2 / R56
  ALFA = Atn((YY(0, 43) - YY(0, 41)) / (XX(0, 43) - XX(0, 41)))
  ac = RR(0, 11) - R59
  BC = RR(0, 11) + R58
  AB = (XX(0, 43) - XX(0, 41)) / Cos(ALFA)
  RAD = ((ac ^ 2 + AB ^ 2 - BC ^ 2) / (2 * AB * ac))
  If (Abs(RAD) > 1) Then
    StopRegieEvents
    MsgBox "CALCULATION NOT POSSIBLE:" & Chr(13) & "FLANK CROWN F2 TOO BIG", 32, "SUB VISUALIZZA_MOLA"
    ResumeRegieEvents
    Exit Sub
  End If
  Beta = FnACS(RAD)
  If Beta < 0 Then Beta = PG + Beta
  GAMMA11 = PG - (ALFA + Beta)
  'punto C2
  XX(0, 32) = XX(0, 33) - (RR(0, 1) - R49) * Cos(GAMMA1)
  YY(0, 32) = YY(0, 33) - (RR(0, 1) - R49) * Sin(GAMMA1)
  AT1 = Atn((YY(0, 31) - YY(0, 32)) / (XX(0, 31) - XX(0, 32)))
  If (AT1 < 0) Then
    StopRegieEvents
    MsgBox "CALCULATION NOT POSSIBLE:" & Chr(13) & "NEGATIVE FLANK 1: " & FnRG(AT1), 32, "SUB VISUALIZZA_MOLA"
    ResumeRegieEvents
    Exit Sub
  End If
  'punto C12
  XX(0, 42) = XX(0, 43) + (RR(0, 11) - R59) * Cos(GAMMA11)
  YY(0, 42) = YY(0, 43) - (RR(0, 11) - R59) * Sin(GAMMA11)
  AT11 = Atn((YY(0, 41) - YY(0, 42)) / (XX(0, 42) - XX(0, 41)))
  If (AT11 < 0) Then
    StopRegieEvents
    MsgBox "CALCULATION NOT POSSIBLE:" & Chr(13) & "NEGATIVE FLANK 2: " & FnRG(AT11), 32, "SUB VISUALIZZA_MOLA"
    ResumeRegieEvents
    Exit Sub
  End If
  'punto P4
  XX(0, 4) = XX(0, 31) - R48 * Cos(AT1)
  YY(0, 4) = YY(0, 31) - R48 * Sin(AT1)
  'punto P5
  XX(0, 5) = XX(0, 33) + R49 * Cos(GAMMA1)
  YY(0, 5) = YY(0, 33) + R49 * Sin(GAMMA1)
  'punto P14
  XX(0, 14) = XX(0, 41) + R58 * Cos(AT11)
  YY(0, 14) = YY(0, 41) - R58 * Sin(AT11)
  'punto P15
  XX(0, 15) = XX(0, 43) - R59 * Cos(GAMMA11)
  YY(0, 15) = YY(0, 43) + R59 * Sin(GAMMA11)
  'SIMULAZIONE SPLIT
'  Open g_chOemPATH & "\CALCOLO_MOLA_VITI_CONICHE.TXT" For Output As #1
'  Print #1, "FIANCO 1"
'  Print #1, Left$("YY[0,5]=" & frmt(YY(0, 5), 4) & String(20, " "), 20) & Left$("HF1=" & frmt(R44 - R43 - R45, 4) & String(20, " "), 20) & Left$("=" & String(20, " "), 20)
'  Print #1, Left$("R44=" & frmt(R44, 4) & String(20, " "), 20) & Left$("R43=" & frmt(R43, 4) & String(20, " "), 20) & Left$("R45=" & frmt(R45, 4) & String(20, " "), 20)
'  Print #1, Left$("XX[0,32]=" & frmt(XX(0, 32), 4) & String(20, " "), 20) & Left$("YY[0,32]=" & frmt(YY(0, 32), 4) & String(20, " "), 20) & Left$("RR[0,1]=" & frmt(RR(0, 1), 4) & String(20, " "), 20)
'  Print #1, Left$("XX[0,33]=" & frmt(XX(0, 33), 4) & String(20, " "), 20) & Left$("YY[0,33]=" & frmt(YY(0, 33), 4) & String(20, " "), 20) & Left$("R49=" & frmt(R49, 4) & String(20, " "), 20)
'  Print #1, ""
'  Print #1, "FIANCO 2"
'  Print #1, Left$("YY[0,15]=" & frmt(YY(0, 15), 4) & String(20, " "), 20) & Left$("HF2=" & frmt(R54 - R43 - R45, 4) & String(20, " "), 20) & Left$("=" & String(20, " "), 20)
'  Print #1, Left$("R54=" & frmt(R54, 4) & String(20, " "), 20) & Left$("R53=" & frmt(R53, 4) & String(20, " "), 20) & Left$("R45=" & frmt(R45, 4) & String(20, " "), 20)
'  Print #1, Left$("XX[0,42]=" & frmt(XX(0, 42), 4) & String(20, " "), 20) & Left$("YY[0,42]=" & frmt(YY(0, 42), 4) & String(20, " "), 20) & Left$("RR[0,11]=" & frmt(RR(0, 11), 4) & String(20, " "), 20)
'  Print #1, Left$("XX[0,43]=" & frmt(XX(0, 43), 4) & String(20, " "), 20) & Left$("YY[0,43]=" & frmt(YY(0, 43), 4) & String(20, " "), 20) & Left$("R59=" & frmt(R59, 4) & String(20, " "), 20)
'  Close #1
'  R17 = 1
'  R12 = 1
'  R21 = 1
  'R39 = 1 / Cos(Lp("R[124]") * PG / 180)
  'Call SPLITTA_F1(5.75, 7.75, 74.2, 7.75, 74.2, 44)
  'Call SPLITTA_F2(5.75, 7.75, 360 * 12, 5, 400, 44)
  'Call CALCOLO_DSW(0.7, 2, 1, R258)
  'Call CALCOLO_DSW(0.7, 2, 2, R259)
  'Spalla
  'F1
  COLORE = 0
  dALFA = Sqr((XX(0, 2) - XX(0, 1)) ^ 2 + (YY(0, 2) - YY(0, 1)) ^ 2) / NumeroRette
  Open g_chOemPATH & "\R47.PNT" For Output As #1
    For i = 0 To NumeroRette - 1
      APF = R47
      XXp1 = XX(0, 1) - i * dALFA * Cos(APF)
      YYp1 = YY(0, 1) + i * dALFA * Sin(APF)
      AAP1 = PG / 2 - APF
      APF = R47
      XXp2 = XX(0, 1) - (i + 1) * dALFA * Cos(APF)
      YYp2 = YY(0, 1) + (i + 1) * dALFA * Sin(APF)
      AAP2 = PG / 2 - APF
      GoSub SCRIVI_DISEGNA
    Next i
  Close #1
  'F2
  COLORE = 0
  dALFA = Sqr((XX(0, 12) - XX(0, 11)) ^ 2 + (YY(0, 12) - YY(0, 11)) ^ 2) / NumeroRette
  Open g_chOemPATH & "\R57.PNT" For Output As #1
    For i = 0 To NumeroRette - 1
      APF = R57
      XXp1 = XX(0, 11) + i * dALFA * Cos(APF)
      YYp1 = YY(0, 11) + i * dALFA * Sin(APF) - R184
      AAP1 = PG / 2 - APF
      APF = R57
      XXp2 = XX(0, 11) + (i + 1) * dALFA * Cos(APF)
      YYp2 = YY(0, 11) + (i + 1) * dALFA * Sin(APF) - R184
      AAP2 = PG / 2 - APF
      GoSub SCRIVI_DISEGNA
    Next i
  Close #1
  'Calcolo R-TESTA
  'F1
  COLORE = 12
  dALFA = (PG / 2 - AT1 - R47) / NumeroRette
  Open g_chOemPATH & "\R48.PNT" For Output As #1
    For i = 0 To NumeroRette - 1
      APF = R47 + i * dALFA
      XXp1 = XX(0, 31) - R48 * Sin(APF)
      YYp1 = YY(0, 31) - R48 * Cos(APF)
      AAP1 = PG / 2 - APF
      APF = R47 + (i + 1) * dALFA
      XXp2 = XX(0, 31) - R48 * Sin(APF)
      YYp2 = YY(0, 31) - R48 * Cos(APF)
      AAP2 = PG / 2 - APF
      GoSub SCRIVI_DISEGNA
    Next i
  Close #1
  'F2
  COLORE = 12
  dALFA = (PG / 2 - AT11 - R57) / NumeroRette
  Open g_chOemPATH & "\R58.PNT" For Output As #1
    For i = 0 To NumeroRette - 1
      APF = R57 + i * dALFA
      YYp1 = YY(0, 41) - R58 * Cos(APF) - R184
      XXp1 = XX(0, 41) + R58 * Sin(APF)
      AAP1 = -(PG / 2 - APF)
      APF = R57 + (i + 1) * dALFA
      YYp2 = YY(0, 41) - R58 * Cos(APF) - R184
      XXp2 = XX(0, 41) + R58 * Sin(APF)
      AAP2 = -(PG / 2 - APF)
      GoSub SCRIVI_DISEGNA
    Next i
  Close #1
  'Calcolo Fianco
  'F1
  COLORE = 9
  dALFA = (GAMMA1 - AT1) / NumeroRette
  Open g_chOemPATH & "\R46.PNT" For Output As #1
    For i = 0 To NumeroRette - 1
      APF = AT1 + i * dALFA
      XXp1 = XX(0, 32) + RR(0, 1) * Cos(APF)
      YYp1 = YY(0, 32) + RR(0, 1) * Sin(APF)
      AAP1 = APF
      APF = AT1 + (i + 1) * dALFA
      XXp2 = XX(0, 32) + RR(0, 1) * Cos(APF)
      YYp2 = YY(0, 32) + RR(0, 1) * Sin(APF)
      AAP2 = APF
      GoSub SCRIVI_DISEGNA
    Next i
  Close #1
  'F2
  COLORE = 9
  dALFA = (GAMMA11 - AT11) / NumeroRette
  Open g_chOemPATH & "\R56.PNT" For Output As #1
    For i = 0 To NumeroRette - 1
      APF = AT11 + i * dALFA
      XXp1 = XX(0, 42) - RR(0, 11) * Cos(APF)
      YYp1 = YY(0, 42) + RR(0, 11) * Sin(APF) - R184
      AAP1 = -APF
      APF = AT11 + (i + 1) * dALFA
      XXp2 = XX(0, 42) - RR(0, 11) * Cos(APF)
      YYp2 = YY(0, 42) + RR(0, 11) * Sin(APF) - R184
      AAP2 = -APF
      GoSub SCRIVI_DISEGNA
    Next i
  Close #1
  'Calcolo R-fondo
  If R45 = 0 Then R45 = 0.0001
  RR(0, 0) = (((XX(0, 33) - XX(0, 43)) / 2) ^ 2 + R45 ^ 2) / 2 / R45
  AB = RR(0, 0) + R59
  ac = RR(0, 0) + R49
  BC = Sqr((XX(0, 33) - XX(0, 43)) ^ 2 + (YY(0, 43) - YY(0, 33)) ^ 2)
  GAMMA = Atn((YY(0, 33) - YY(0, 43)) / (XX(0, 33) - XX(0, 43)))
  Delta = FnACS((AB ^ 2 + BC ^ 2 - ac ^ 2) / (2 * AB * BC))
  angC11 = PG / 2 - Delta - GAMMA
  XX(0, 34) = XX(0, 43) + (RR(0, 0) + R59) * Sin(angC11)
  YY(0, 34) = YY(0, 43) + (RR(0, 0) + R59) * Cos(angC11)
  angC1 = Atn((XX(0, 33) - XX(0, 34)) / (YY(0, 34) - YY(0, 33)))
  angC11 = Atn((XX(0, 34) - XX(0, 43)) / (YY(0, 34) - YY(0, 43)))
  'P6-P16
  XX(0, 6) = XX(0, 34) + RR(0, 0) * Sin(angC1)
  YY(0, 6) = YY(0, 34) - RR(0, 0) * Cos(angC1)
  XX(0, 16) = XX(0, 34) - RR(0, 0) * Sin(angC11)
  YY(0, 16) = YY(0, 34) - RR(0, 0) * Cos(angC11)
  'P7-P17
  XX(0, 7) = 0
  YY(0, 7) = YY(0, 34) - RR(0, 0)
  XX(0, 17) = XX(0, 6) + 1
  YY(0, 17) = YY(0, 34) - Sqr(RR(0, 0) ^ 2 - (XX(0, 17) - XX(0, 34)) ^ 2)
  'F1
  COLORE = 12
  dALFA = (PG / 2 - GAMMA1 + angC1) / NumeroRette
  Open g_chOemPATH & "\R49.PNT" For Output As #1
    For i = 0 To NumeroRette - 1
      APF = GAMMA1 + i * dALFA
      XXp1 = XX(0, 33) + R49 * Cos(APF)
      YYp1 = YY(0, 33) + R49 * Sin(APF)
      AAP1 = APF
      APF = GAMMA1 + (i + 1) * dALFA
      XXp2 = XX(0, 33) + R49 * Cos(APF)
      YYp2 = YY(0, 33) + R49 * Sin(APF)
      AAP2 = APF
      GoSub SCRIVI_DISEGNA
    Next i
  Close #1
  'F2
  COLORE = 12
  dALFA = (PG / 2 - GAMMA11 + angC11) / NumeroRette
  Open g_chOemPATH & "\R59.PNT" For Output As #1
    For i = 0 To NumeroRette - 1
      APF = GAMMA11 + i * dALFA
      XXp1 = XX(0, 43) - R59 * Cos(APF)
      YYp1 = YY(0, 43) + R59 * Sin(APF) - R184
      AAP1 = -APF
      APF = GAMMA11 + (i + 1) * dALFA
      XXp2 = XX(0, 43) - R59 * Cos(APF)
      YYp2 = YY(0, 43) + R59 * Sin(APF) - R184
      AAP2 = -APF
      GoSub SCRIVI_DISEGNA
    Next i
  Close #1
  Dim k1 As Integer
  Dim k2 As Integer
  k1 = NumeroRette / 2
  k2 = NumeroRette / 2
  While k1 + k2 < NumeroRette
    k2 = k2 + 1
  Wend
  'Calcolo Fondo
  COLORE = 9
  'FIANCO 1
  dALFA = (angC1 + angC11) / NumeroRette
  Open g_chOemPATH & "\R45.PNT" For Output As #1
    For i = 0 To k1 - 1
      angF = angC1 - i * dALFA
      XXp1 = XX(0, 34) + RR(0, 0) * Sin(angF)
      YYp1 = YY(0, 34) - RR(0, 0) * Cos(angF) - R184
      AAP1 = PG / 2 - angF
      angF = angC1 - (i + 1) * dALFA
      XXp2 = XX(0, 34) + RR(0, 0) * Sin(angF)
      YYp2 = YY(0, 34) - RR(0, 0) * Cos(angF) - R184
      AAP2 = PG / 2 - angF
      GoSub SCRIVI_DISEGNA
    Next i
  Close #1
  'FIANCO 2
  Open g_chOemPATH & "\R45.PNT" For Append As #1
    For i = 0 To k2 - 1
      angF = angC1 - k1 * dALFA - i * dALFA
      XXp1 = XX(0, 34) + RR(0, 0) * Sin(angF)
      YYp1 = YY(0, 34) - RR(0, 0) * Cos(angF) - R184
      AAP1 = PG / 2 - angF
      angF = angC1 - k1 * dALFA - (i + 1) * dALFA
      XXp2 = XX(0, 34) + RR(0, 0) * Sin(angF)
      YYp2 = YY(0, 34) - RR(0, 0) * Cos(angF) - R184
      AAP2 = PG / 2 - angF
      GoSub SCRIVI_DISEGNA
    Next i
    stmp = ""
    stmp = stmp & Left$(frmt(XXp2, CIFRE) & String(COLONNA, " "), COLONNA)
    stmp = stmp & Left$(frmt(YYp2, CIFRE) & String(COLONNA, " "), COLONNA)
    stmp = stmp & Left$(frmt(AAP2, CIFRE) & String(COLONNA, " "), COLONNA)
    Print #1, stmp
  Close #1
  '
  Dim AngStart  As Double
  Dim AngEnd    As Double
  Open g_chOemPATH & "\CONICHE_MOLA.DXF" For Output As #3
  'INIZIO FILE
  Print #3, "0"
  Print #3, "SECTION"
  'RIMOSSO PER SIEMENS
  'Print #3, "2"
  'Print #3, "HEADER"
  'Print #3, "9"
  'Print #3, "$ACADVER"
  'Print #3, "1"
  'Print #3, "AC1009"
  Print #3, "2"
  Print #3, "ENTITIES"
  'ASSE VERTICALE MOLA
  Print #3, "0"
  Print #3, "LINE"
  Print #3, "8"
  Print #3, "1"
  Print #3, "62"
  Print #3, "1"
  'PRIMO PUNTO
  Print #3, "10"
  Print #3, frmt(0, CIFRE)
  Print #3, "20"
  Print #3, frmt(YY(0, 1) * Scala, CIFRE)
  Print #3, "30"
  Print #3, "0.0"
  'SECONDO PUNTO
  Print #3, "11"
  Print #3, frmt(0, CIFRE)
  Print #3, "21"
  Print #3, frmt(YY(0, 16) * Scala, CIFRE)
  Print #3, "31"
  Print #3, "0.0"
  'ASSE ORIZZONTALE MOLA
  Print #3, "0"
  Print #3, "LINE"
  Print #3, "8"
  Print #3, "1"
  Print #3, "62"
  Print #3, "1"
  'PRIMO PUNTO
  Print #3, "10"
  Print #3, frmt(XX(0, 1) * Scala, CIFRE)
  Print #3, "20"
  Print #3, frmt(0, CIFRE)
  Print #3, "30"
  Print #3, "0.0"
  'SECONDO PUNTO
  Print #3, "11"
  Print #3, frmt(XX(0, 11) * Scala, CIFRE)
  Print #3, "21"
  Print #3, frmt(0, CIFRE)
  Print #3, "31"
  Print #3, "0.0"
  'SPALLA FIANCO 1
  Print #3, "0"
  Print #3, "LINE"
  Print #3, "8"
  Print #3, "1"
  Print #3, "62"
  Print #3, "1"
  'PRIMO PUNTO
  Print #3, "10"
  Print #3, frmt(XX(0, 1) * Scala, CIFRE)
  Print #3, "20"
  Print #3, frmt(YY(0, 1) * Scala, CIFRE)
  Print #3, "30"
  Print #3, "0.0"
  'SECONDO PUNTO
  Print #3, "11"
  Print #3, frmt(XX(0, 2) * Scala, CIFRE)
  Print #3, "21"
  Print #3, frmt(YY(0, 2) * Scala, CIFRE)
  Print #3, "31"
  Print #3, "0.0"
  'RAGGIO DI TESTA FIANCO 1
  'Calcolo angoli compresi tra 0 e 2 * Pigreco
  AngStart = CALCOLO_ANGOLO(XX(0, 2) - XX(0, 31), YY(0, 2) - YY(0, 31))
  AngEnd = CALCOLO_ANGOLO(XX(0, 4) - XX(0, 31), YY(0, 4) - YY(0, 31))
  Print #3, "0"
  Print #3, "ARC"
  Print #3, "8"
  Print #3, "1"
  Print #3, "62"
  Print #3, "1"
  Print #3, "10"
  Print #3, frmt(XX(0, 31) * Scala, CIFRE) & " (x centro)"
  Print #3, "20"
  Print #3, frmt(YY(0, 31) * Scala, CIFRE) & " (y centro)"
  Print #3, "30"
  Print #3, "0.0"
  Print #3, "40"
  Print #3, frmt(R48 * Scala, CIFRE) & " (raggio)"
  Print #3, "50"
  Print #3, frmt(AngEnd * 180 / PG, CIFRE) & " (angolo iniziale)"
  Print #3, "51"
  Print #3, frmt(AngStart * 180 / PG, CIFRE) & " (angolo finale)"
  'RAGGIO FIANCO 1
  'Calcolo angoli compresi tra 0 e 2 * Pigreco
  AngStart = CALCOLO_ANGOLO(XX(0, 4) - XX(0, 32), YY(0, 4) - YY(0, 32))
  AngEnd = CALCOLO_ANGOLO(XX(0, 5) - XX(0, 32), YY(0, 5) - YY(0, 32))
  Print #3, "0"
  Print #3, "ARC"
  Print #3, "8"
  Print #3, "1"
  Print #3, "62"
  Print #3, "1"
  Print #3, "10"
  Print #3, frmt(XX(0, 32) * Scala, CIFRE) & " (x centro)"
  Print #3, "20"
  Print #3, frmt(YY(0, 32) * Scala, CIFRE) & " (y centro)"
  Print #3, "30"
  Print #3, "0.0"
  Print #3, "40"
  Print #3, frmt(RR(0, 1) * Scala, CIFRE) & " (raggio)"
  Print #3, "50"
  Print #3, frmt(AngStart * 180 / PG, CIFRE) & " (angolo iniziale)"
  Print #3, "51"
  Print #3, frmt(AngEnd * 180 / PG, CIFRE) & " (angolo finale)"
  'RAGGIO FONDO FIANCO 1
  'Calcolo angoli compresi tra 0 e 2 * Pigreco
  AngStart = CALCOLO_ANGOLO(XX(0, 5) - XX(0, 33), YY(0, 5) - YY(0, 33))
  AngEnd = CALCOLO_ANGOLO(XX(0, 6) - XX(0, 33), YY(0, 6) - YY(0, 33))
  Print #3, "0"
  Print #3, "ARC"
  Print #3, "8"
  Print #3, "1"
  Print #3, "62"
  Print #3, "1"
  Print #3, "10"
  Print #3, frmt(XX(0, 33) * Scala, CIFRE) & " (x centro)"
  Print #3, "20"
  Print #3, frmt(YY(0, 33) * Scala, CIFRE) & " (y centro)"
  Print #3, "30"
  Print #3, "0.0"
  Print #3, "40"
  Print #3, frmt(R49 * Scala, CIFRE) & " (raggio)"
  Print #3, "50"
  Print #3, frmt(AngStart * 180 / PG, CIFRE) & " (angolo iniziale)"
  Print #3, "51"
  Print #3, frmt(AngEnd * 180 / PG, CIFRE) & " (angolo finale)"
  'RAGGIO FONDO MOLA
  'Calcolo angoli compresi tra 0 e 2 * Pigreco
  AngStart = CALCOLO_ANGOLO(XX(0, 6) - XX(0, 34), YY(0, 6) - YY(0, 34))
  AngEnd = CALCOLO_ANGOLO(XX(0, 16) - XX(0, 34), YY(0, 16) - YY(0, 34))
  Print #3, "0"
  Print #3, "ARC"
  Print #3, "8"
  Print #3, "1"
  Print #3, "62"
  Print #3, "1"
  Print #3, "10"
  Print #3, frmt(XX(0, 34) * Scala, CIFRE) & " (x centro)"
  Print #3, "20"
  Print #3, frmt(YY(0, 34) * Scala, CIFRE) & " (y centro)"
  Print #3, "30"
  Print #3, "0.0"
  Print #3, "40"
  Print #3, frmt(RR(0, 0) * Scala, CIFRE) & " (raggio)"
  Print #3, "50"
  Print #3, frmt(AngEnd * 180 / PG, CIFRE) & " (angolo iniziale)"
  Print #3, "51"
  Print #3, frmt(AngStart * 180 / PG, CIFRE) & " (angolo finale)"
  'RAGGIO FONDO FIANCO 2
  'Calcolo angoli compresi tra 0 e 2 * Pigreco
  AngStart = CALCOLO_ANGOLO(XX(0, 16) - XX(0, 43), YY(0, 16) - YY(0, 43))
  AngEnd = CALCOLO_ANGOLO(XX(0, 15) - XX(0, 43), YY(0, 15) - YY(0, 43))
  Print #3, "0"
  Print #3, "ARC"
  Print #3, "8"
  Print #3, "1"
  Print #3, "62"
  Print #3, "1"
  Print #3, "10"
  Print #3, frmt(XX(0, 43) * Scala, CIFRE) & " (x centro)"
  Print #3, "20"
  Print #3, frmt(YY(0, 43) * Scala, CIFRE) & " (y centro)"
  Print #3, "30"
  Print #3, "0.0"
  Print #3, "40"
  Print #3, frmt(R59 * Scala, CIFRE) & " (raggio)"
  Print #3, "50"
  Print #3, frmt(AngStart * 180 / PG, CIFRE) & " (angolo iniziale)"
  Print #3, "51"
  Print #3, frmt(AngEnd * 180 / PG, CIFRE) & " (angolo finale)"
  'RAGGIO FIANCO 2
  'Calcolo angoli compresi tra 0 e 2 * Pigreco
  AngStart = CALCOLO_ANGOLO(XX(0, 14) - XX(0, 42), YY(0, 14) - YY(0, 42))
  AngEnd = CALCOLO_ANGOLO(XX(0, 15) - XX(0, 42), YY(0, 15) - YY(0, 42))
  Print #3, "0"
  Print #3, "ARC"
  Print #3, "8"
  Print #3, "1"
  Print #3, "62"
  Print #3, "1"
  Print #3, "10"
  Print #3, frmt(XX(0, 42) * Scala, CIFRE) & " (x centro)"
  Print #3, "20"
  Print #3, frmt(YY(0, 42) * Scala, CIFRE) & " (y centro)"
  Print #3, "30"
  Print #3, "0.0"
  Print #3, "40"
  Print #3, frmt(RR(0, 11) * Scala, CIFRE) & " (raggio)"
  Print #3, "50"
  Print #3, frmt(AngEnd * 180 / PG, CIFRE) & " (angolo iniziale)"
  Print #3, "51"
  Print #3, frmt(AngStart * 180 / PG, CIFRE) & " (angolo finale)"
  'RAGGIO DI TESTA FIANCO 2
  'Calcolo angoli compresi tra 0 e 2 * Pigreco
  AngStart = CALCOLO_ANGOLO(XX(0, 12) - XX(0, 41), YY(0, 12) - YY(0, 41))
  AngEnd = CALCOLO_ANGOLO(XX(0, 14) - XX(0, 41), YY(0, 14) - YY(0, 41))
  Print #3, "0"
  Print #3, "ARC"
  Print #3, "8"
  Print #3, "1"
  Print #3, "62"
  Print #3, "1"
  Print #3, "10"
  Print #3, frmt(XX(0, 41) * Scala, CIFRE) & " (x centro)"
  Print #3, "20"
  Print #3, frmt(YY(0, 41) * Scala, CIFRE) & " (y centro)"
  Print #3, "30"
  Print #3, "0.0"
  Print #3, "40"
  Print #3, frmt(R58 * Scala, CIFRE) & " (raggio)"
  Print #3, "50"
  Print #3, frmt(AngStart * 180 / PG, CIFRE) & " (angolo iniziale)"
  Print #3, "51"
  Print #3, frmt(AngEnd * 180 / PG, CIFRE) & " (angolo finale)"
  'SPALLA FIANCO 2
  Print #3, "0"
  Print #3, "LINE"
  Print #3, "8"
  Print #3, "1"
  Print #3, "62"
  Print #3, "1"
  'PRIMO PUNTO
  Print #3, "10"
  Print #3, frmt(XX(0, 11) * Scala, CIFRE)
  Print #3, "20"
  Print #3, frmt(YY(0, 11) * Scala, CIFRE)
  Print #3, "30"
  Print #3, "0.0"
  'SECONDO PUNTO
  Print #3, "11"
  Print #3, frmt(XX(0, 12) * Scala, CIFRE)
  Print #3, "21"
  Print #3, frmt(YY(0, 12) * Scala, CIFRE)
  Print #3, "31"
  Print #3, "0.0"
  'FINE FILE
  Print #3, "0"
  Print #3, "ENDSEC"
  Print #3, "0"
  Print #3, "EOF"
  Close #3
  '
  'PUNTO SUL DIAMETRO INTERNO VITE...........................................................
  PROFONDITA = YY(0, 7): XX_PROFONDITA = XX(0, 7)
  i = WritePrivateProfileString("MOLA_VITI_CONICHE", "DXfm", frmt(XX(0, 33) - XX(0, 43), 4), Path_LAVORAZIONE_INI)
  i = WritePrivateProfileString("MOLA_VITI_CONICHE", "PROFONDITA", frmt(PROFONDITA, CIFRE), Path_LAVORAZIONE_INI)
  i = WritePrivateProfileString("MOLA_VITI_CONICHE", "XX_PROFONDITA", frmt(XX_PROFONDITA, CIFRE), Path_LAVORAZIONE_INI)
  '
  WRITE_DIALOG ""
  '
Exit Sub

SCRIVI_DISEGNA:
  stmp = ""
  stmp = stmp & Left$(frmt(XXp1, CIFRE) & String(COLONNA, " "), COLONNA)
  stmp = stmp & Left$(frmt(YYp1, CIFRE) & String(COLONNA, " "), COLONNA)
  stmp = stmp & Left$(frmt(AAP1, CIFRE) & String(COLONNA, " "), COLONNA)
  Print #1, stmp
  pX1 = (-XXp1 - ZeroPezzoX) * ScalaX + PicW / 2
  pY1 = -(-YYp1 + ALLONTANAMENTO - ZeroPezzoY) * ScalaY + PicH / 2
  pX2 = (-XXp2 - ZeroPezzoX) * ScalaX + PicW / 2
  pY2 = -(-YYp2 + ALLONTANAMENTO - ZeroPezzoY) * ScalaY + PicH / 2
  OEMX.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
Return

errVISUALIZZA_MOLA:
  WRITE_DIALOG "Sub VISUALIZZA_MOLA: ERROR -> " & Err
Resume Next
  
End Sub

Sub VISUALIZZA_MOLA_VITI()
'
Dim i                As Integer
Dim stmp             As String
Dim Atmp             As String * 255
Dim TipoCarattere    As String
Dim TIPO_LAVORAZIONE As String
'
On Error GoTo errVISUALIZZA_MOLA_VITI
  '
  OEMX.OEM1Command2.Visible = False
  OEMX.OEM1PicCALCOLO.Cls
  stmp = GetInfo("MOLA_VITI_CONICHE", "PicW", Path_LAVORAZIONE_INI)
  If (val(stmp) > 0) Then
    OEMX.OEM1PicCALCOLO.ScaleWidth = val(stmp)
  Else
    OEMX.OEM1PicCALCOLO.ScaleWidth = 190
  End If
  stmp = GetInfo("MOLA_VITI_CONICHE", "PicH", Path_LAVORAZIONE_INI)
  If (val(stmp) > 0) Then
    OEMX.OEM1PicCALCOLO.ScaleHeight = val(stmp)
  Else
    OEMX.OEM1PicCALCOLO.ScaleHeight = 95
  End If
  i = LoadString(g_hLanguageLibHandle, 1100, Atmp, 255)
  If (i > 0) Then OEMX.frameCalcolo.Caption = Left$(Atmp, i)
  OEMX.lblINP(0).Visible = True: OEMX.txtINP(0).Visible = True
  OEMX.lblINP(1).Visible = True: OEMX.txtINP(1).Visible = True
  OEMX.lblINP(2).Visible = True: OEMX.txtINP(2).Visible = True
  OEMX.lblINP(3).Visible = True: OEMX.txtINP(3).Visible = True
  OEMX.lblINP(4).Visible = True: OEMX.txtINP(4).Visible = True
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  Select Case TIPO_LAVORAZIONE
      '
    Case "VITI_CONICHE"
      OEMX.OEM1ListaCombo1.Clear
      OEMX.OEM1ListaCombo1.Visible = False
      Call OEMX.OEM1ListaCombo1.AddItem(" " & "SECTION 1")    '--> 0
      Call OEMX.OEM1ListaCombo1.AddItem(" " & "SECTION 3")    '--> 1
      Call OEMX.OEM1ListaCombo1.AddItem(" " & "SECTION 5")    '--> 2
      Call OEMX.OEM1ListaCombo1.AddItem(" " & "SECTION 7")    '--> 3
      stmp = GetInfo("MOLA_VITI_CONICHE", "INDICE_COMBO", Path_LAVORAZIONE_INI)
      i = val(stmp): If (i > 3) Then i = 0
      OEMX.OEM1Combo1.Visible = True
      OEMX.OEM1Combo1.Caption = OEMX.OEM1ListaCombo1.List(i)
      i = WritePrivateProfileString("MOLA_VITI_CONICHE", "INDICE_COMBO", Format$(i, "##0"), Path_LAVORAZIONE_INI)
      '
    Case "VITI_DBG"
      OEMX.OEM1ListaCombo1.Clear
      OEMX.OEM1ListaCombo1.Visible = False
      Call OEMX.OEM1ListaCombo1.AddItem(" " & "MAIN LEAD")    '--> 0
      Call OEMX.OEM1ListaCombo1.AddItem(" " & "BARRIER LEAD")  '--> 1
      stmp = GetInfo("MOLA_VITI_CONICHE", "INDICE_COMBO", Path_LAVORAZIONE_INI)
      i = val(stmp): If (i > 1) Then i = 0
      OEMX.OEM1Combo1.Visible = True
      OEMX.OEM1Combo1.Caption = OEMX.OEM1ListaCombo1.List(i)
      i = WritePrivateProfileString("MOLA_VITI_CONICHE", "INDICE_COMBO", Format$(i, "##0"), Path_LAVORAZIONE_INI)
      '
    Case "VITI_BARRIERA1L", "VITI_BARRIERA1S", "VITI_BARRIERA3S"
      OEMX.OEM1ListaCombo1.Clear
      OEMX.OEM1ListaCombo1.Visible = False
      Call OEMX.OEM1ListaCombo1.AddItem(" " & "LONG LEAD")    '--> 0
      Call OEMX.OEM1ListaCombo1.AddItem(" " & "SHORT LEAD")   '--> 1
      stmp = GetInfo("MOLA_VITI_CONICHE", "INDICE_COMBO", Path_LAVORAZIONE_INI)
      i = val(stmp): If (i > 1) Then i = 0
      OEMX.OEM1Combo1.Visible = True
      OEMX.OEM1Combo1.Caption = OEMX.OEM1ListaCombo1.List(i)
      i = WritePrivateProfileString("MOLA_VITI_CONICHE", "INDICE_COMBO", Format$(i, "##0"), Path_LAVORAZIONE_INI)
      '
    Case "VITI_PASSO_VARIABILE", "VITI_BARRIERA3L"
      OEMX.OEM1ListaCombo1.Clear
      OEMX.OEM1ListaCombo1.Visible = False
      Call OEMX.OEM1ListaCombo1.AddItem(" " & "SECTION 1")    '--> 0
      Call OEMX.OEM1ListaCombo1.AddItem(" " & "SECTION 3")    '--> 1
      stmp = GetInfo("MOLA_VITI_CONICHE", "INDICE_COMBO", Path_LAVORAZIONE_INI)
      i = val(stmp): If (i > 1) Then i = 0
      OEMX.OEM1Combo1.Visible = True
      OEMX.OEM1Combo1.Caption = OEMX.OEM1ListaCombo1.List(i)
      i = WritePrivateProfileString("MOLA_VITI_CONICHE", "INDICE_COMBO", Format$(i, "##0"), Path_LAVORAZIONE_INI)
      '
    Case "VITI_BARRIERA4L"
      OEMX.OEM1ListaCombo1.Clear
      OEMX.OEM1ListaCombo1.Visible = False
      Call OEMX.OEM1ListaCombo1.AddItem(" " & "SECTION BS4LA")   '--> 0
      Call OEMX.OEM1ListaCombo1.AddItem(" " & "SECTION BS4LB")   '--> 1
      Call OEMX.OEM1ListaCombo1.AddItem(" " & "SECTION BARRIER")  '--> 2
      stmp = GetInfo("MOLA_VITI_CONICHE", "INDICE_COMBO", Path_LAVORAZIONE_INI)
      i = val(stmp): If (i > 1) Then i = 0
      OEMX.OEM1Combo1.Visible = True
      OEMX.OEM1Combo1.Caption = OEMX.OEM1ListaCombo1.List(i)
      i = WritePrivateProfileString("MOLA_VITI_CONICHE", "INDICE_COMBO", Format$(i, "##0"), Path_LAVORAZIONE_INI)
      '
    Case "VITI_BARRIERA4S"
      OEMX.OEM1ListaCombo1.Clear
      OEMX.OEM1ListaCombo1.Visible = False
      Call OEMX.OEM1ListaCombo1.AddItem(" " & "CHANNEL OF REFERENCE")  '--> 0
      Call OEMX.OEM1ListaCombo1.AddItem(" " & "SECTION BS4SA-1")    '--> 1
      Call OEMX.OEM1ListaCombo1.AddItem(" " & "SECTION BS4SA-2")    '--> 2
      Call OEMX.OEM1ListaCombo1.AddItem(" " & "SECTION BS4SB-1")    '--> 3
      Call OEMX.OEM1ListaCombo1.AddItem(" " & "SECTION BS4SB-2")    '--> 4
      stmp = GetInfo("MOLA_VITI_CONICHE", "INDICE_COMBO", Path_LAVORAZIONE_INI)
      i = val(stmp): If (i > 1) Then i = 0
      OEMX.OEM1Combo1.Visible = True
      OEMX.OEM1Combo1.Caption = OEMX.OEM1ListaCombo1.List(i)
      i = WritePrivateProfileString("MOLA_VITI_CONICHE", "INDICE_COMBO", Format$(i, "##0"), Path_LAVORAZIONE_INI)
      '
    Case "VITI_BARRIERA2L", "VITI_BARRIERA2S"
      OEMX.OEM1ListaCombo1.Clear
      OEMX.OEM1ListaCombo1.Visible = False
      OEMX.OEM1Combo1.Visible = False
      '
    Case "BS5L"
      OEMX.OEM1ListaCombo1.Clear
      OEMX.OEM1ListaCombo1.Visible = False
      OEMX.OEM1Combo1.Visible = False
      '
    Case "BS5S"
      OEMX.OEM1ListaCombo1.Clear
      OEMX.OEM1ListaCombo1.Visible = False
      Call OEMX.OEM1ListaCombo1.AddItem(" " & "REFERENCE")     '--> 0
      Call OEMX.OEM1ListaCombo1.AddItem(" " & "SECTION BS5SA")  '--> 1
      Call OEMX.OEM1ListaCombo1.AddItem(" " & "SECTION BS5SB")  '--> 2
      stmp = GetInfo("MOLA_VITI_CONICHE", "INDICE_COMBO", Path_LAVORAZIONE_INI)
      i = val(stmp): If (i > 1) Then i = 0
      OEMX.OEM1Combo1.Visible = True
      OEMX.OEM1Combo1.Caption = OEMX.OEM1ListaCombo1.List(i)
      i = WritePrivateProfileString("MOLA_VITI_CONICHE", "INDICE_COMBO", Format$(i, "##0"), Path_LAVORAZIONE_INI)
      '
    Case "VITI_BARRIERA3"
      OEMX.OEM1ListaCombo1.Clear
      OEMX.OEM1ListaCombo1.Visible = False
      OEMX.OEM1Combo1.Visible = False
      '
    Case "VITI_BARRIERA6"
      ACTUAL_PARAMETER_GROUP = ActualParGroup
      OEMX.OEM1ListaCombo1.Clear
      OEMX.OEM1ListaCombo1.Visible = False
      If (ACTUAL_PARAMETER_GROUP = "OEM1_MOLA50A") Then
        Call OEMX.OEM1ListaCombo1.AddItem(" " & "KLEINE STEIGUNG")   '--> 0
        Call OEMX.OEM1ListaCombo1.AddItem(" " & "GROESSE STEIGUNG")  '--> 1
        stmp = GetInfo("MOLA_VITI_CONICHE", "INDICE_COMBO", Path_LAVORAZIONE_INI)
        i = val(stmp): If (i > 1) Then i = 0
        OEMX.OEM1Combo1.Visible = True
        OEMX.OEM1Combo1.Caption = OEMX.OEM1ListaCombo1.List(i)
        i = WritePrivateProfileString("MOLA_VITI_CONICHE", "INDICE_COMBO", Format$(i, "##0"), Path_LAVORAZIONE_INI)
      Else
        OEMX.OEM1Combo1.Visible = False
      End If
      '
  End Select
  TipoCarattere = GetInfo("MOLA_VITI_CONICHE", "TipoCarattere", Path_LAVORAZIONE_INI)
  If Len(TipoCarattere) < 0 Then TipoCarattere = "Courier New"
  Call VISUALIZZA_MOLA
  Call VISUALIZZA_CANALE("N")
  If (OEMX.OEM1Combo1.Visible = True) Then
    OEMX.OEM1Combo1.SetFocus
  Else
    OEMX.txtINP(0).SetFocus
  End If
  '
Exit Sub

errVISUALIZZA_MOLA_VITI:
  WRITE_DIALOG "Sub VISUALIZZA_MOLA_VITI: ERROR -> " & Err
  Resume Next
  
End Sub

Sub CALCOLO_VITI_CONICHE()
'
Const MMp = 1 '1mm --> 25.4pollici
'
Dim Atmp     As String * 255
Dim stmp     As String
Dim sERRORE  As String
Dim NomeVite As String
'
Dim Np       As Integer 'R5  : Numero principi
Dim PASSO    As Double  'R128: Passo assiale (sx=+)
Dim DE       As Double  'R189: Diametro esterno
Dim Z1       As Double  'R91 : Lunghezza tratto 1
Dim Z2       As Double  'R92 : Lunghezza tratto 2
Dim Z3       As Double  'R93 : Lunghezza tratto 3
Dim Z4       As Double  'R94 : Lunghezza tratto 4
Dim Z5       As Double  'R95 : Lunghezza tratto 5
Dim Z6       As Double  'R96 : Lunghezza tratto 6
Dim Z7       As Double  'R97 : Lunghezza tratto 7
Dim PZ1      As Double  'R220: ProfonditÓ tratto 1
Dim PZ3      As Double  'R221: ProfonditÓ tratto 3
Dim PZ5      As Double  'R222: ProfonditÓ tratto 5
Dim PZ7      As Double  'R223: ProfonditÓ tratto 7
Dim Se       As Integer 'Senso dell'elica
Dim SFN      As Double  'Spessore normale filetto
Dim LC       As Double  'Channel width
Dim RGT      As Double  'Radius tool offset
Dim R41ch    As Double  'R41, R51: Angolo  fianco
Dim R51ch    As Double  'R41, R51: Angolo  fianco
Dim R49ch    As Double  'R49, R59: Raggio di fondo
Dim R59ch    As Double  'R49, R59: Raggio di fondo
'
Dim ret               As Integer
Dim sVALORE           As String
Dim EGP               As Double
Dim PZMAX             As Double
Dim LW                As Integer
Dim VELO_PRF          As String
Dim VELO_RUL          As String
Dim CALCOLO_SEQUENZA  As String
Dim RIPE_RET(3)       As Integer
Dim PASS_PRF(3)       As Integer
Dim INCR_PRF(3)       As Double
Dim FEED_PRF(3)       As Double
Dim ROTA_PRF(3)       As String
Dim PASS_RET(3)       As Integer
Dim INCR_RET(3)       As Double
Dim VELO_ASS(3)       As Double
Dim VELO_MOL(3)       As Double
Dim SVRM_CYC(3)       As Double
Dim SCARTO(3)         As Double
Dim TipoMola          As String
'
On Error GoTo errCALCOLO_VITI_CONICHE
  '
  'SEGNALAZIONE UTENTE
  WRITE_DIALOG NomeVite & " EVALUATING "
  'LETTURA DEL NOME DELLA VITE CONICA
  NomeVite = GetInfo("VITI_CONICHE", "NomePezzoInserimento", Path_LAVORAZIONE_INI)
  NomeVite = UCase$(Trim$(NomeVite))
  'NOME FILE INI TEMPORANEO
  'LETTURA VALORI DALLA FINESTRA D'INSERIMENTO
  Np = Lp("NP")
  PASSO = Lp("PASSO")
  DE = Lp("DE")
   If (1 = Lp("TipoMola")) Then
    TipoMola = "_CBN"
  Else
    TipoMola = ""
  End If
  If (PASSO > 0) Then
    Z1 = Lp("Z1")
    Z2 = Lp("Z2")
    Z3 = Lp("Z3")
    Z4 = Lp("Z4")
    Z5 = Lp("Z5")
    Z6 = Lp("Z6")
    Z7 = Lp("Z7")
    PZ1 = Lp("PZ1")
    PZ3 = Lp("PZ3")
    PZ5 = Lp("PZ5")
    PZ7 = Lp("PZ7")
  Else
      If (Lp("Z2") = 0) Then
    'SOLO UNA ZONA CILINDRICA
    Z1 = Lp("Z1")
    PZ1 = Lp("PZ1")
  ElseIf (Lp("Z4") = 0) Then
    'SOLO UNA ZONA CONICA
    Z3 = Lp("Z1")
    Z2 = Lp("Z2")
    Z1 = Lp("Z3")
    PZ3 = Lp("PZ1")
    PZ1 = Lp("PZ3")
  ElseIf (Lp("Z6") = 0) Then
    'SOLO DUE ZONA CONICHE
    Z5 = Lp("Z1")
    Z4 = Lp("Z2")
    Z3 = Lp("Z3")
    Z2 = Lp("Z4")
    Z1 = Lp("Z5")
    PZ5 = Lp("PZ1")
    PZ3 = Lp("PZ3")
    PZ1 = Lp("PZ5")
  Else
    'SOLO TRE ZONA CONICHE
    Z7 = Lp("Z1")
    Z6 = Lp("Z2")
    Z5 = Lp("Z3")
    Z4 = Lp("Z4")
    Z3 = Lp("Z5")
    Z2 = Lp("Z6")
    Z1 = Lp("Z7")
    PZ7 = Lp("PZ1")
    PZ5 = Lp("PZ3")
    PZ3 = Lp("PZ5")
    PZ1 = Lp("PZ7")
  End If
  End If
  Se = Lp("SE")
  SFN = Lp("SFN")
  RGT = Lp("RGT")
  R41ch = Lp("R41ch")
  R51ch = Lp("R51ch")
  R49ch = Lp("R49ch")
  R59ch = Lp("R59ch")
  'CALCOLO ELICHE
  EGP = Abs(Atn(PASSO / PG / DE))
  'RICERCA PROFONDITA' MAGGIORE
  PZMAX = PZ1
  If (PZ3 > PZMAX) Then PZMAX = PZ3
  If (PZ5 > PZMAX) Then PZMAX = PZ5
  If (PZ7 > PZMAX) Then PZMAX = PZ7
  'LARGHEZZA CANALE ASSIALE
  LC = PASSO / Np - SFN / Cos(EGP)
  'LAVORO
  Call SP("R[5]", Format$(Np, "###0"), False)
  Call SP("R[124]", frmt(Se * EGP * 180 / PG, 2), False)
  Call SP("R[128]", frmt(PASSO, 4), False)
  Call SP("R[189]", frmt(DE, 4), False)
  Call SP("R[213]", "30", False)
  Call SP("R[280]", frmt(250 / MMp, 4), False)
  Call SP("R[281]", frmt(500 / MMp, 4), False)
  Call SP("R[91]", frmt(Z1, 4), False)
  Call SP("R[92]", frmt(Z2, 4), False)
  Call SP("R[93]", frmt(Z3, 4), False)
  Call SP("R[94]", frmt(Z4, 4), False)
  Call SP("R[95]", frmt(Z5, 4), False)
  Call SP("R[96]", frmt(Z6, 4), False)
  Call SP("R[97]", frmt(Z7, 4), False)
  Call SP("R[220]", frmt(PZ1, 4), False)
  Call SP("R[221]", frmt(PZ3, 4), False)
  Call SP("R[222]", frmt(PZ5, 4), False)
  Call SP("R[223]", frmt(PZ7, 4), False)
  If (PZ1 > 0) Then Call SP("R[320]", frmt(DE - 2 * PZ1, 3), False)
  If (PZ3 > 0) Then Call SP("R[321]", frmt(DE - 2 * PZ3, 3), False)
  If (PZ5 > 0) Then Call SP("R[322]", frmt(DE - 2 * PZ5, 3), False)
  If (PZ7 > 0) Then Call SP("R[323]", frmt(DE - 2 * PZ7, 3), False)
  If (Np > 1) Then
    For i = 1 To Np - 1
      Call SP("MISF2[0," & Format$(i, "##0") & "]", frmt((360 / Np) * i, 4), False)
    Next i
    For i = Np To 10
      Call SP("MISF2[0," & Format$(i, "##0") & "]", "0", False)
    Next i
  End If
  '
  VELO_PRF = GetInfo("CICLI_VITI_CONICHE", "VELO_PRF" & TipoMola, Path_LAVORAZIONE_INI)
  If (Len(VELO_PRF) <= 0) Then VELO_PRF = "30"
  VELO_RUL = GetInfo("CICLI_VITI_CONICHE", "VELO_RUL" & TipoMola, Path_LAVORAZIONE_INI)
  If (Len(VELO_RUL) <= 0) Then VELO_RUL = "24"
  '
  LW = (Int(LC / 5) + 1) * 5
  Call SP("R[37]", Format$(LW, "###0"), False)
  Call SP("R[38]", frmt(LC * Cos(EGP), 3), False)
  Call SP("R[204]", VELO_PRF, False)
  'Call SP("MAC[0,39]", VELO_RUL, False)
  Call SP("R[248]", "250", False)
  Call SP("R[200]", "360", False)
  Call SP("R[49]", frmt(R49ch, 3), False)
  Call SP("R[59]", frmt(R59ch, 3), False)
  Call SP("R[48]", frmt((RGT + 0.2) / MMp, 3), False)
  Call SP("R[58]", frmt((RGT + 0.2) / MMp, 3), False)
  Call SP("R[43]", frmt((RGT + 0.5) / MMp, 3), False)
  Call SP("R[53]", frmt((RGT + 0.5) / MMp, 3), False)
  Call SP("R[44]", frmt((RGT + 0.5) / MMp + PZMAX, 3), False)
  Call SP("R[54]", frmt((RGT + 0.5) / MMp + PZMAX, 3), False)
  Call SP("R[47]", "10", False)
  Call SP("R[57]", "10", False)
  Call SP("R[41]", frmt(R41ch, 3), False)
  Call SP("R[51]", frmt(R51ch, 3), False)
  '
  'INIZIALIZZAZIONE DELLA BOMBATURA SUI FIANCHI
  'Dim Bombatura As Double
  'Bombatura = Lp("R[46]")
  'If (Bombatura = 0) Then
  '  Bombatura = PZMAX / Tan(R41ch * PG / 180) / 360 'SVG120117
  '  Call SP("R[46]", frmt(Bombatura, 4), False)
  'End If
  'Bombatura = Lp("R[56]")
  'If (Bombatura = 0) Then
  '  Bombatura = PZMAX / Tan(R51ch * PG / 180) / 360 'SVG120117
  '  Call SP("R[56]", frmt(Bombatura, 4), False)
  'End If
  '
  'COSTRUZIONE DELLA SEQUENZA DEI CICLI DI RETTIFICA
  CALCOLO_SEQUENZA = GetInfo("VITI_CONICHE", "CALCOLO_SEQUENZA", Path_LAVORAZIONE_INI)
  CALCOLO_SEQUENZA = UCase$(Trim$(CALCOLO_SEQUENZA))
  If Len(CALCOLO_SEQUENZA) <= 0 Then CALCOLO_SEQUENZA = "N"
  If (CALCOLO_SEQUENZA = "Y") Then
    Call CALCOLO_SEQUENZA_CICLI_CONICHE(PASSO, PZMAX, DE, TipoMola, RIPE_RET, PASS_PRF, INCR_PRF, FEED_PRF, ROTA_PRF, PASS_RET, INCR_RET, VELO_ASS, VELO_MOL, SVRM_CYC)
    'CALCOLO I PARAMETRI PER IL PRIMO CICLO
    RIPE_RET(1) = INTERO(SVRM_CYC(1) / (INCR_RET(1) + INCR_RET(1)) / PASS_RET(1), "MAX")
    INCR_RET(1) = SVRM_CYC(1) / RIPE_RET(1) / PASS_RET(1) / 2
    INCR_RET(1) = Int(INCR_RET(1) * 10000) / 10000
    SCARTO(1) = SVRM_CYC(1) - RIPE_RET(1) * PASS_RET(1) * (INCR_RET(1) + INCR_RET(1))
    'SCARICO LO SCARTO NEL SECONDO CICLO
    SVRM_CYC(2) = SVRM_CYC(2) + SCARTO(1)
    INCR_RET(2) = SVRM_CYC(2) / PASS_RET(2)
    'CONVERSIONE GP->VT
    'GP=FEED/SQRT(360^2+PASSO^2)
    'VT=GP*SQRT((Pi*Dext)^2+PASSO^2)
    'GP=VT/SQRT((Pi*Dext)^2+PASSO^2)
    'CALCOLO VELOCITA'
    VELO_ASS(1) = INTERO(VELO_ASS(1) * Sqr((PG * DE) ^ 2 + PASSO ^ 2), "MIN")
    VELO_ASS(2) = INTERO(VELO_ASS(2) * Sqr((PG * DE) ^ 2 + PASSO ^ 2), "MIN")
    VELO_ASS(3) = INTERO(VELO_ASS(3) * Sqr((PG * DE) ^ 2 + PASSO ^ 2), "MIN")
    'SGROSSATURA
    Call SP("STR[11]", "S", False)
    Call SP("CYC[0,1]", Format$(RIPE_RET(1), "#####"), False)
    Call SP("CYC[0,2]", Format$(PASS_PRF(1), "#####"), False)
    Call SP("CYC[0,3]", frmt(INCR_PRF(1), 4), False)
    Call SP("CYC[0,4]", Format$(FEED_PRF(1), "#####"), False)
    Call SP("CYC[0,5]", ROTA_PRF(1), False)
    Call SP("CYC[0,6]", Format$(PASS_RET(1), "#####"), False)
    Call SP("CYC[0,7]", frmt(INCR_RET(1), 4), False)
    Call SP("CYC[0,8]", Format$(VELO_ASS(1), "##########"), False)
    Call SP("CYC[0,9]", frmt(INCR_RET(1), 4), False)
    Call SP("CYC[0,10]", Format$(VELO_ASS(1), "##########"), False)
    Call SP("CYC[0,12]", Format$(VELO_MOL(1), "##########"), False)
    Call SP("CYC[0,13]", Format$(VELO_PRF, "##########"), False)
    'PREFINITURA
    Call SP("STR[12]", "S", False)
    Call SP("CYC[0,21]", Format$(RIPE_RET(2), "#####"), False)
    Call SP("CYC[0,22]", Format$(PASS_PRF(2), "#####"), False)
    Call SP("CYC[0,23]", frmt(INCR_PRF(2), 4), False)
    Call SP("CYC[0,24]", Format$(FEED_PRF(2), "#####"), False)
    Call SP("CYC[0,25]", ROTA_PRF(2), False)
    Call SP("CYC[0,26]", Format$(PASS_RET(2), "#####"), False)
    Call SP("CYC[0,27]", frmt(INCR_RET(2), 4), False)
    Call SP("CYC[0,28]", Format$(VELO_ASS(2), "##########"), False)
    Call SP("CYC[0,29]", "0", False)
    Call SP("CYC[0,30]", Format$(VELO_ASS(2), "##########"), False)
    Call SP("CYC[0,32]", Format$(VELO_MOL(2), "##########"), False)
    Call SP("CYC[0,33]", Format$(VELO_PRF, "##########"), False)
    'FINITURA
    Call SP("STR[13]", "S", False)
    Call SP("CYC[0,41]", Format$(RIPE_RET(3), "#####"), False)
    Call SP("CYC[0,42]", Format$(PASS_PRF(3), "#####"), False)
    Call SP("CYC[0,43]", frmt(INCR_PRF(3), 4), False)
    Call SP("CYC[0,44]", Format$(FEED_PRF(3), "#####"), False)
    Call SP("CYC[0,45]", ROTA_PRF(3), False)
    Call SP("CYC[0,46]", Format$(PASS_RET(3), "#####"), False)
    Call SP("CYC[0,47]", frmt(INCR_RET(3), 4), False)
    Call SP("CYC[0,48]", Format$(VELO_ASS(3), "##########"), False)
    Call SP("CYC[0,49]", "0", False)
    Call SP("CYC[0,50]", Format$(VELO_ASS(3), "##########"), False)
    Call SP("CYC[0,52]", Format$(VELO_MOL(3), "##########"), False)
    Call SP("CYC[0,53]", Format$(VELO_PRF, "##########"), False)
    'NULLO
    Call SP("STR[14]", "N", False)
    Call SP("CYC[0,61]", "0", False)
    Call SP("CYC[0,62]", "0", False)
    Call SP("CYC[0,63]", "0", False)
    Call SP("CYC[0,64]", "0", False)
    Call SP("CYC[0,65]", "0", False)
    Call SP("CYC[0,66]", "0", False)
    Call SP("CYC[0,67]", "0", False)
    Call SP("CYC[0,68]", "0", False)
    Call SP("CYC[0,69]", "0", False)
    Call SP("CYC[0,70]", "0", False)
    Call SP("CYC[0,72]", "0", False)
    Call SP("CYC[0,73]", "0", False)
  Else
    Call SP("STR[11]", "S", False)
    Call SP("CYC[0,1]", "1", False)
    Call SP("CYC[0,6]", "1", False)
    Call SP("CYC[0,7]", frmt(PZMAX, 4), False)
  End If
  '
  Call SP("DIAMEST[0,0]", frmt(DE, 4), False)
  Call SCRITTURA_CICLI_RETTIFICA_DIAMETRI
  '
  'SVG120117
  'RICHIESTA PER MACCHINA TIPO ENGEL ED INUTILE PER SUMITOMO
  'stmp = ""
  'ret = LoadString(g_hLanguageLibHandle, 1003, Atmp, 255)
  'If (ret > 0) Then stmp = Left$(Atmp, ret)
  'stmp = stmp & " " & Tabella1.MESSAGGI(2).NOME
  'stmp = stmp & Chr(13) & Chr(13) & " [CNC " & LETTERA_LAVORAZIONE & "] " & LEGGI_NomePezzo("VITI_CONICHE")
  'stmp = stmp & Chr(13) & Chr(13) & " ---> "
  'stmp = stmp & Chr(13) & Chr(13) & " [OFF-LINE] " & NomeVite & "?"
  'StopRegieEvents
  'ret = MsgBox(stmp, vbInformation + vbYesNo + vbDefaultButton2, "WARNING")
  'ResumeRegieEvents
  'If (ret = vbYes) Then Call CARICA_DATI_MACCHINA_CORRENTI("VITI_CONICHE", "OEM1_MACCHINA41", "94")
  Call AggiornaDB
  ret = WritePrivateProfileString("Configurazione", "CARICA_CALCOLO", "N", PathFILEINI)
  WRITE_DIALOG NomeVite & " EVALUATED "
  If (Len(sERRORE) > 0) Then WRITE_DIALOG NomeVite & sERRORE
  '
Exit Sub

errCALCOLO_VITI_CONICHE:
  WRITE_DIALOG "Sub CALCOLO_VITI_CONICHE: Error -> " & Err
  sERRORE = ": STANDARD EVALUATE WITH ERROR " & str(Err)
  Resume Next

End Sub

Sub CALCOLO_VITI_BARRIERA_BS5()
'
Dim Atmp     As String * 255
Dim stmp     As String
Dim sERRORE  As String
Dim NomeVite As String
'
Dim pX1 As Double  'R128: Passo assiale
Dim pX2 As Double  'R132: Passo assiale
Dim PX3 As Double  'R133: Passo assiale

Dim R190      As Double
Dim DE        As Double  'R189: Diametro esterno

Dim L1        As Double  'R91: Lunghezza tratto 1
Dim L2        As Double  'R92: Lunghezza tratto 2
Dim L3        As Double  'R93: Lunghezza tratto 3
Dim L4        As Double  'R94: Lunghezza tratto 4
Dim L5        As Double  'R95: Lunghezza tratto 5
Dim L6        As Double  'R96: Lunghezza tratto 6
Dim L7        As Double  'R97: Lunghezza tratto 7
Dim L8        As Double  'R98: Lunghezza tratto 8

Dim SD1       As Double
Dim SD2       As Double
Dim SD3       As Double
Dim SD4       As Double
Dim SD5       As Double

Dim MD2       As Double
Dim MD3       As Double
Dim MD4       As Double
Dim MD5       As Double

Dim MD7       As Double
Dim MD8       As Double

Dim OD4       As Double
Dim OD5       As Double

Dim MF1       As Double
Dim MF2       As Double
Dim BF        As Double

Dim SCMW      As Double
Dim MCMW      As Double

Dim LC        As Double  'Channel width

Dim RGT       As Double  'Radius tool offset

Dim R41ch     As Double  'R41, R51: Angolo  fianco
Dim R51ch     As Double  'R41, R51: Angolo  fianco
Dim R49ch     As Double  'R49, R59: Raggio di fondo
Dim R59ch     As Double  'R49, R59: Raggio di fondo
'
Dim ret               As Integer
Dim sVALORE           As String
Dim EGP1              As Double
Dim EGP2              As Double

Dim LW                As Integer
Dim VELO_PRF          As String
Dim VELO_RUL          As String
Dim CALCOLO_SEQUENZA  As String
Dim RIPE_RET(3)       As Integer
Dim PASS_PRF(3)       As Integer
Dim INCR_PRF(3)       As Double
Dim FEED_PRF(3)       As Double
Dim ROTA_PRF(3)       As String
Dim PASS_RET(3)       As Integer
Dim INCR_RET(3)       As Double
Dim VELO_ASS(3)       As Double
Dim VELO_MOL(3)       As Double
Dim SVRM_CYC(3)       As Double
Dim SCARTO(3)         As Double
Dim TipoMola          As String
'
On Error GoTo errCALCOLO_VITI_BARRIERA_BS5
  '
  'SEGNALAZIONE UTENTE
  WRITE_DIALOG NomeVite & " EVALUATING "
  'LETTURA DEL NOME DELLA VITE
  NomeVite = GetInfo("BS5L", "NomePezzoInserimento", Path_LAVORAZIONE_INI)
  NomeVite = UCase$(Trim$(NomeVite))
  'LETTURA VALORI DALLA FINESTRA D'INSERIMENTO
  pX1 = Lp("PX1")
  pX2 = Lp("PX2")
  PX3 = Lp("PX3")
  DE = Lp("MOD")
  R190 = Lp("WS")
   If (1 = Lp("TipoMola")) Then
    TipoMola = "_CBN"
  Else
    TipoMola = ""
  End If
  L1 = Lp("L1")
  L2 = Lp("L2")
  'L3 = Lp("L3")
  L4 = Lp("L4")
  'L5 = Lp("L5")
  L6 = Lp("L6")
  L7 = Lp("L7")
  L8 = Lp("L8")
  
  SD1 = Lp("SD1")
  MD2 = Lp("MD2")
  OD4 = Lp("OD4")
  
  SD5 = Lp("SD5")
  MD7 = Lp("MD7")
  MD8 = Lp("MD8")
  
  SD4 = Lp("SD4")
  SD2 = Lp("SD2")
  
  MD3 = Lp("MD3")
  MD4 = Lp("MD4")
  
  MD7 = Lp("MD7")
  MD8 = Lp("MD8")
  
  MF1 = Lp("MF1")
  MF2 = Lp("MF2")
  BF = Lp("BF")
  
  SCMW = Lp("SCMW")
  MCMW = Lp("MCMW")
  
  RGT = Lp("RGT")
  
  R41ch = Lp("R41ch")
  R51ch = Lp("R51ch")
  R49ch = Lp("R49ch")
  R59ch = Lp("R59ch")
  
  'CALCOLO L3 L4 e L5
  L3 = pX2: L5 = pX2
  L4 = L4 - L3 - L5
  
  'CALCOLO SD4
  If (SD4 <= 0) Then
    SD4 = (MD4 - SD5) / (L3 + L4) * L3 + SD5
    SD4 = Int(SD4 * 100) / 100
  End If

  'CALCOLO MD3
  If (MD3 <= 0) Then
    MD3 = (SD2 - MD2) / (L3 + L4 + L5) * L5 + MD2
    MD3 = Int(MD3 * 100) / 100
  End If
  
  'CALCOLO ELICHE
  EGP1 = Abs(Atn((pX1 / 2 + pX2 / 2) / PG / DE))
  EGP2 = Abs(Atn((PX3 / 2 + pX2 / 2) / PG / DE))
    
  'LARGHEZZA CANALE INGRESSO
  LC = pX1 - MF1
  'LARGHEZZA MOLA
  LW = (Int(LC / 5) + 1) * 5
  '
  'LAVORO
  Call SP("R[124]", frmt(EGP1 * 180 / PG, 2), False)
  
  Call SP("R[60]", frmt(MF1, 3), False)
  Call SP("R[61]", frmt(MF2, 3), False)
  Call SP("R[62]", frmt(BF, 3), False)
  
  Call SP("R[128]", frmt(pX1, 3), False)
  Call SP("R[132]", frmt(pX2, 3), False)
  Call SP("R[133]", frmt(PX3, 3), False)
  
  Call SP("R[65]", frmt(SCMW, 3), False)
  Call SP("R[66]", frmt(MCMW, 3), False)
  
  Call SP("R[190]", frmt(R190, 3), False)
  Call SP("R[189]", frmt(DE, 3), False)
  Call SP("R[213]", "360", False)
  Call SP("R[280]", frmt(250, 3), False)
  Call SP("R[281]", frmt(500, 3), False)
    
  Call SP("R[91]", frmt(L1, 4), False)
  Call SP("R[92]", frmt(L2, 4), False)
  Call SP("R[93]", frmt(L3, 4), False)
  Call SP("R[94]", frmt(L4, 4), False)
  Call SP("R[95]", frmt(L5, 4), False)
  Call SP("R[96]", frmt(L6, 4), False)
  Call SP("R[97]", frmt(L7, 4), False)
  Call SP("R[98]", frmt(L8, 4), False)
  
  Call SP("R[75]", "1", False)
  Call SP("R[420]", frmt(SD1, 4), False)
  Call SP("R[421]", frmt(MD2, 4), False)
  Call SP("R[422]", frmt(OD4, 4), False)
  Call SP("R[423]", "0", False)
  
  Call SP("R[76]", "1", False)
  Call SP("R[220]", frmt(OD4, 4), False)
  Call SP("R[221]", frmt(SD5, 4), False)
  Call SP("R[222]", frmt(MD7, 4), False)
  Call SP("R[223]", frmt(MD8, 4), False)
  '
  VELO_PRF = GetInfo("CICLI_VITI_CONICHE", "VELO_PRF" & TipoMola, Path_LAVORAZIONE_INI)
  If (Len(VELO_PRF) <= 0) Then VELO_PRF = "30"
  VELO_RUL = GetInfo("CICLI_VITI_CONICHE", "VELO_RUL" & TipoMola, Path_LAVORAZIONE_INI)
  If (Len(VELO_RUL) <= 0) Then VELO_RUL = "24"
  '
  'MOLA LARGA
  Call SP("R[37]", Format$(LW, "###0"), False)
  Call SP("R[38]", frmt(LC * Cos(EGP1), 4), False)
  Call SP("R[204]", VELO_PRF, False)
  'Call SP("MAC[0,39]", VELO_RUL, False)
  Call SP("R[248]", "250", False)
  Call SP("R[200]", "360", False)
  Call SP("R[49]", frmt(R49ch, 4), False)
  Call SP("R[59]", frmt(R59ch, 4), False)
  Call SP("R[48]", frmt((RGT + 0.2), 4), False)
  Call SP("R[58]", frmt((RGT + 0.2), 4), False)
  Call SP("R[43]", frmt((RGT + 0.5), 4), False)
  Call SP("R[53]", frmt((RGT + 0.5), 4), False)
  Call SP("R[44]", frmt((RGT + 0.5) + SD1, 4), False)
  Call SP("R[54]", frmt((RGT + 0.5) + SD1, 4), False)
  Call SP("R[47]", "10", False)
  Call SP("R[57]", "10", False)
  Call SP("R[41]", frmt(R41ch, 4), False)
  Call SP("R[51]", frmt(R51ch, 4), False)
  '
  'INIZIALIZZAZIONE DELLA BOMBATURA SUI FIANCHI
  'Dim Bombatura As Double
  'Bombatura = Lp("R[46]")
  'If (Bombatura = 0) Then
  '  Bombatura = SD1 / Tan(R41ch * PG / 180) / 450
  '  Call SP("R[46]", frmt(Bombatura, 4), False)
  'End If
  'Bombatura = Lp("R[56]")
  'If (Bombatura = 0) Then
  '  Bombatura = SD1 / Tan(R51ch * PG / 180) / 450
  '  Call SP("R[56]", frmt(Bombatura, 4), False)
  'End If
  '
  'COSTRUZIONE DELLA SEQUENZA DEI CICLI DI RETTIFICA
  CALCOLO_SEQUENZA = GetInfo("BS5L", "CALCOLO_SEQUENZA", Path_LAVORAZIONE_INI)
  CALCOLO_SEQUENZA = UCase$(Trim$(CALCOLO_SEQUENZA))
  If (Len(CALCOLO_SEQUENZA) <= 0) Then CALCOLO_SEQUENZA = "N"
  If (CALCOLO_SEQUENZA = "Y") Then
    'CICLI FASE A
    Call CALCOLO_SEQUENZA_CICLI_CONICHE(pX2, SD1, DE, TipoMola, RIPE_RET, PASS_PRF, INCR_PRF, FEED_PRF, ROTA_PRF, PASS_RET, INCR_RET, VELO_ASS, VELO_MOL, SVRM_CYC)
    'CALCOLO I PARAMETRI PER IL PRIMO CICLO
    RIPE_RET(1) = INTERO(SVRM_CYC(1) / (INCR_RET(1) + INCR_RET(1)) / PASS_RET(1), "MAX")
    INCR_RET(1) = SVRM_CYC(1) / RIPE_RET(1) / PASS_RET(1) / 2
    INCR_RET(1) = Int(INCR_RET(1) * 10000) / 10000
    SCARTO(1) = SVRM_CYC(1) - RIPE_RET(1) * PASS_RET(1) * (INCR_RET(1) + INCR_RET(1))
    'SCARICO LO SCARTO NEL SECONDO CICLO
    SVRM_CYC(2) = SVRM_CYC(2) + SCARTO(1)
    INCR_RET(2) = SVRM_CYC(2) / PASS_RET(2)
    'CONVERSIONE GP->VT
    'GP=FEED/SQRT(360^2+PASSO^2)
    'VT=GP*SQRT((Pi*Dext)^2+PASSO^2)
    'GP=VT/SQRT((Pi*Dext)^2+PASSO^2)
    'CALCOLO VELOCITA'
    VELO_ASS(1) = INTERO(VELO_ASS(1) * Sqr((PG * DE) ^ 2 + pX2 ^ 2), "MIN")
    VELO_ASS(2) = INTERO(VELO_ASS(2) * Sqr((PG * DE) ^ 2 + pX2 ^ 2), "MIN")
    VELO_ASS(3) = INTERO(VELO_ASS(3) * Sqr((PG * DE) ^ 2 + pX2 ^ 2), "MIN")
    'SGROSSATURA
    Call SP("STR[11]", "S", False)
    Call SP("CYC[0,1]", Format$(RIPE_RET(1), "#####"), False)
    Call SP("CYC[0,2]", Format$(PASS_PRF(1), "#####"), False)
    Call SP("CYC[0,3]", frmt(INCR_PRF(1), 4), False)
    Call SP("CYC[0,4]", Format$(FEED_PRF(1), "#####"), False)
    Call SP("CYC[0,5]", ROTA_PRF(1), False)
    Call SP("CYC[0,6]", Format$(PASS_RET(1), "#####"), False)
    Call SP("CYC[0,7]", frmt(INCR_RET(1), 4), False)
    Call SP("CYC[0,8]", Format$(VELO_ASS(1), "##########"), False)
    Call SP("CYC[0,9]", frmt(INCR_RET(1), 4), False)
    Call SP("CYC[0,10]", Format$(VELO_ASS(1), "##########"), False)
    Call SP("CYC[0,12]", Format$(VELO_MOL(1), "##########"), False)
    'PREFINITURA
    Call SP("STR[12]", "S", False)
    Call SP("CYC[0,21]", Format$(RIPE_RET(2), "#####"), False)
    Call SP("CYC[0,22]", Format$(PASS_PRF(2), "#####"), False)
    Call SP("CYC[0,23]", frmt(INCR_PRF(2), 4), False)
    Call SP("CYC[0,24]", Format$(FEED_PRF(2), "#####"), False)
    Call SP("CYC[0,25]", ROTA_PRF(2), False)
    Call SP("CYC[0,26]", Format$(PASS_RET(2), "#####"), False)
    Call SP("CYC[0,27]", frmt(INCR_RET(2), 4), False)
    Call SP("CYC[0,28]", Format$(VELO_ASS(2), "##########"), False)
    Call SP("CYC[0,29]", "0", False)
    Call SP("CYC[0,30]", Format$(VELO_ASS(2), "##########"), False)
    Call SP("CYC[0,32]", Format$(VELO_MOL(2), "##########"), False)
    'FINITURA
    Call SP("STR[13]", "S", False)
    Call SP("CYC[0,41]", Format$(RIPE_RET(3), "#####"), False)
    Call SP("CYC[0,42]", Format$(PASS_PRF(3), "#####"), False)
    Call SP("CYC[0,43]", frmt(INCR_PRF(3), 4), False)
    Call SP("CYC[0,44]", Format$(FEED_PRF(3), "#####"), False)
    Call SP("CYC[0,45]", ROTA_PRF(3), False)
    Call SP("CYC[0,46]", Format$(PASS_RET(3), "#####"), False)
    Call SP("CYC[0,47]", frmt(INCR_RET(3), 4), False)
    Call SP("CYC[0,48]", Format$(VELO_ASS(3), "##########"), False)
    Call SP("CYC[0,49]", "0", False)
    Call SP("CYC[0,50]", Format$(VELO_ASS(3), "##########"), False)
    Call SP("CYC[0,52]", Format$(VELO_MOL(3), "##########"), False)
    'NULLO
    Call SP("STR[14]", "N", False)
    Call SP("CYC[0,61]", "0", False)
    Call SP("CYC[0,62]", "0", False)
    Call SP("CYC[0,63]", "0", False)
    Call SP("CYC[0,64]", "0", False)
    Call SP("CYC[0,65]", "0", False)
    Call SP("CYC[0,66]", "0", False)
    Call SP("CYC[0,67]", "0", False)
    Call SP("CYC[0,68]", "0", False)
    Call SP("CYC[0,69]", "0", False)
    Call SP("CYC[0,70]", "0", False)
    Call SP("CYC[0,72]", "0", False)
    'CICLI FASE B
    Call CALCOLO_SEQUENZA_CICLI_CONICHE(pX2, MD7 - OD4, DE, TipoMola, RIPE_RET, PASS_PRF, INCR_PRF, FEED_PRF, ROTA_PRF, PASS_RET, INCR_RET, VELO_ASS, VELO_MOL, SVRM_CYC)
    'CALCOLO I PARAMETRI PER IL PRIMO CICLO
    RIPE_RET(1) = INTERO(SVRM_CYC(1) / (INCR_RET(1) + INCR_RET(1)) / PASS_RET(1), "MAX")
    INCR_RET(1) = SVRM_CYC(1) / RIPE_RET(1) / PASS_RET(1) / 2
    INCR_RET(1) = Int(INCR_RET(1) * 10000) / 10000
    SCARTO(1) = SVRM_CYC(1) - RIPE_RET(1) * PASS_RET(1) * (INCR_RET(1) + INCR_RET(1))
    'SCARICO LO SCARTO NEL SECONDO CICLO
    SVRM_CYC(2) = SVRM_CYC(2) + SCARTO(1)
    INCR_RET(2) = SVRM_CYC(2) / PASS_RET(2)
    'CONVERSIONE GP->VT
    'GP=FEED/SQRT(360^2+PASSO^2)
    'VT=GP*SQRT((Pi*Dext)^2+PASSO^2)
    'GP=VT/SQRT((Pi*Dext)^2+PASSO^2)
    'CALCOLO VELOCITA'
    VELO_ASS(1) = INTERO(VELO_ASS(1) * Sqr((PG * DE) ^ 2 + pX2 ^ 2), "MIN")
    VELO_ASS(2) = INTERO(VELO_ASS(2) * Sqr((PG * DE) ^ 2 + pX2 ^ 2), "MIN")
    VELO_ASS(3) = INTERO(VELO_ASS(3) * Sqr((PG * DE) ^ 2 + pX2 ^ 2), "MIN")
    'DIAMETRO ESTERNO BARRIERA
    Call SP("STR[21]", "S", False)
    Call SP("CYC[0,101]", "1", False)
    Call SP("CYC[0,102]", Format$(PASS_PRF(1), "#####"), False)
    Call SP("CYC[0,103]", frmt(INCR_PRF(1), 4), False)
    Call SP("CYC[0,104]", Format$(FEED_PRF(1), "#####"), False)
    Call SP("CYC[0,105]", ROTA_PRF(1), False)
    Call SP("CYC[0,106]", "1", False)
    Call SP("CYC[0,107]", frmt(OD4, 4), False)
    Call SP("CYC[0,108]", Format$(VELO_ASS(1), "##########"), False)
    Call SP("CYC[0,109]", "0", False)
    Call SP("CYC[0,110]", Format$(VELO_ASS(1), "##########"), False)
    Call SP("CYC[0,112]", Format$(VELO_MOL(1), "##########"), False)
    'SGROSSATURA
    Call SP("STR[22]", "S", False)
    Call SP("CYC[0,121]", Format$(RIPE_RET(1), "#####"), False)
    Call SP("CYC[0,122]", Format$(PASS_PRF(1), "#####"), False)
    Call SP("CYC[0,123]", frmt(INCR_PRF(1), 4), False)
    Call SP("CYC[0,124]", Format$(FEED_PRF(1), "#####"), False)
    Call SP("CYC[0,125]", ROTA_PRF(1), False)
    Call SP("CYC[0,126]", Format$(PASS_RET(1), "#####"), False)
    Call SP("CYC[0,127]", frmt(INCR_RET(1), 4), False)
    Call SP("CYC[0,128]", Format$(VELO_ASS(1), "##########"), False)
    Call SP("CYC[0,129]", frmt(INCR_RET(1), 4), False)
    Call SP("CYC[0,130]", Format$(VELO_ASS(1), "##########"), False)
    Call SP("CYC[0,132]", Format$(VELO_MOL(1), "##########"), False)
    'PREFINITURA
    Call SP("STR[23]", "S", False)
    Call SP("CYC[0,141]", Format$(RIPE_RET(2), "#####"), False)
    Call SP("CYC[0,142]", Format$(PASS_PRF(2), "#####"), False)
    Call SP("CYC[0,143]", frmt(INCR_PRF(2), 4), False)
    Call SP("CYC[0,144]", Format$(FEED_PRF(2), "#####"), False)
    Call SP("CYC[0,145]", ROTA_PRF(2), False)
    Call SP("CYC[0,146]", Format$(PASS_RET(2), "#####"), False)
    Call SP("CYC[0,147]", frmt(INCR_RET(2), 4), False)
    Call SP("CYC[0,148]", Format$(VELO_ASS(2), "##########"), False)
    Call SP("CYC[0,149]", "0", False)
    Call SP("CYC[0,150]", Format$(VELO_ASS(2), "##########"), False)
    Call SP("CYC[0,152]", Format$(VELO_MOL(2), "##########"), False)
    'FINITURA
    Call SP("STR[24]", "S", False)
    Call SP("CYC[0,161]", Format$(RIPE_RET(3), "#####"), False)
    Call SP("CYC[0,162]", Format$(PASS_PRF(3), "#####"), False)
    Call SP("CYC[0,163]", frmt(INCR_PRF(3), 4), False)
    Call SP("CYC[0,164]", Format$(FEED_PRF(3), "#####"), False)
    Call SP("CYC[0,165]", ROTA_PRF(3), False)
    Call SP("CYC[0,166]", Format$(PASS_RET(3), "#####"), False)
    Call SP("CYC[0,167]", frmt(INCR_RET(3), 4), False)
    Call SP("CYC[0,168]", Format$(VELO_ASS(3), "##########"), False)
    Call SP("CYC[0,169]", "0", False)
    Call SP("CYC[0,170]", Format$(VELO_ASS(3), "##########"), False)
    Call SP("CYC[0,172]", Format$(VELO_MOL(3), "##########"), False)
  Else
    'CICLI FASE A
    Call SP("STR[11]", "S", False)
    Call SP("CYC[0,1]", "1", False)
    Call SP("CYC[0,6]", "1", False)
    Call SP("CYC[0,7]", frmt(SD1, 4), False)
    'CICLI FASE B
    Call SP("STR[21]", "S", False)
    Call SP("CYC[0,101]", "1", False)
    Call SP("CYC[0,106]", "1", False)
    Call SP("CYC[0,107]", frmt(MD7, 4), False)
  End If
  '
  'SVG120117
  'RICHIESTA PER MACCHINA TIPO ENGEL ED INUTILE PER SUMITOMO
  'stmp = ""
  'ret = LoadString(g_hLanguageLibHandle, 1003, Atmp, 255)
  'If (ret > 0) Then stmp = Left$(Atmp, ret)
  'stmp = stmp & " " & Tabella1.MESSAGGI(2).NOME
  'stmp = stmp & Chr(13) & Chr(13) & " [CNC " & LETTERA_LAVORAZIONE & "] " & LEGGI_NomePezzo("BS5L")
  'stmp = stmp & Chr(13) & Chr(13) & " ---> "
  'stmp = stmp & Chr(13) & Chr(13) & " [OFF-LINE] " & NomeVite & "?"
  'StopRegieEvents
  'ret = MsgBox(stmp, vbInformation + vbYesNo + vbDefaultButton2, "WARNING")
  'ResumeRegieEvents
  'If (ret = vbYes) Then Call CARICA_DATI_MACCHINA_CORRENTI("BS5L", "OEM1_MACCHINA48", "94")
  '
  Call AggiornaDB
  '
  Dim Appoggio As String
  Appoggio = g_chOemPATH & "\APPOGGIO.INI"
  '
  'LETTURA DATI MACCHINA BS5L E COPIA IN APPOGGIO PER BS5S
  stmp = GetInfo("OFF-LINE", "R[202]", g_chOemPATH & "\BS5L.INI")
  ret = WritePrivateProfileString("OFF-LINE", "R[202]", stmp, Appoggio)
  stmp = GetInfo("OFF-LINE", "MAC[0,20]", g_chOemPATH & "\BS5L.INI")
  ret = WritePrivateProfileString("OFF-LINE", "MAC[0,20]", stmp, Appoggio)
  stmp = GetInfo("OFF-LINE", "R[36]", g_chOemPATH & "\BS5L.INI")
  ret = WritePrivateProfileString("OFF-LINE", "R[36]", stmp, Appoggio)
  stmp = GetInfo("OFF-LINE", "R[197]", g_chOemPATH & "\BS5L.INI")
  ret = WritePrivateProfileString("OFF-LINE", "R[197]", stmp, Appoggio)
  stmp = GetInfo("OFF-LINE", "R[187]", g_chOemPATH & "\BS5L.INI")
  ret = WritePrivateProfileString("OFF-LINE", "R[187]", stmp, Appoggio)
  stmp = GetInfo("OFF-LINE", "COR[0,170]", g_chOemPATH & "\BS5L.INI")
  ret = WritePrivateProfileString("OFF-LINE", "COR[0,170]", stmp, Appoggio)
  stmp = GetInfo("OFF-LINE", "POS_Z_DRESSER", g_chOemPATH & "\BS5L.INI")
  ret = WritePrivateProfileString("OFF-LINE", "POS_Z_DRESSER", stmp, Appoggio)
  stmp = GetInfo("OFF-LINE", "POS_X_DRESSER", g_chOemPATH & "\BS5L.INI")
  ret = WritePrivateProfileString("OFF-LINE", "POS_X_DRESSER", stmp, Appoggio)
  'stmp = GetInfo("OFF-LINE", "MAC[0,39]", g_chOemPATH & "\BS5L.INI")
  'ret = WritePrivateProfileString("OFF-LINE", "MAC[0,39]", stmp, Appoggio)
  stmp = GetInfo("OFF-LINE", "MAC[0,80]", g_chOemPATH & "\BS5L.INI")
  ret = WritePrivateProfileString("OFF-LINE", "MAC[0,80]", stmp, Appoggio)
  '
  'LARGHEZZA CANALE
  LC = MCMW + (SD2 - MD3) * (Tan(R41ch * PG / 180) + Tan(R51ch * PG / 180))
  LW = (Int(LC / 5) + 1) * 5
  '
  'MOLA STRETTA
  ret = WritePrivateProfileString("OFF-LINE", "R[37]", Format$(LW, "###0"), Appoggio)
  ret = WritePrivateProfileString("OFF-LINE", "R[38]", frmt(LC * Cos(EGP2), 3), Appoggio)
  ret = WritePrivateProfileString("OFF-LINE", "R[204]", VELO_PRF, Appoggio)
  'ret = WritePrivateProfileString("OFF-LINE", "MAC[0,39]", VELO_RUL, Appoggio)
  ret = WritePrivateProfileString("OFF-LINE", "R[248]", "250", Appoggio)
  ret = WritePrivateProfileString("OFF-LINE", "R[200]", "360", Appoggio)
  ret = WritePrivateProfileString("OFF-LINE", "R[49]", frmt(R49ch, 3), Appoggio)
  ret = WritePrivateProfileString("OFF-LINE", "R[59]", frmt(R59ch, 3), Appoggio)
  ret = WritePrivateProfileString("OFF-LINE", "R[48]", frmt((RGT + 0.2), 3), Appoggio)
  ret = WritePrivateProfileString("OFF-LINE", "R[58]", frmt((RGT + 0.2), 3), Appoggio)
  ret = WritePrivateProfileString("OFF-LINE", "R[43]", frmt((RGT + 0.5), 3), Appoggio)
  ret = WritePrivateProfileString("OFF-LINE", "R[53]", frmt((RGT + 0.5), 3), Appoggio)
  ret = WritePrivateProfileString("OFF-LINE", "R[44]", frmt((RGT + 0.5) + SD2, 3), Appoggio)
  ret = WritePrivateProfileString("OFF-LINE", "R[54]", frmt((RGT + 0.5) + SD2, 3), Appoggio)
  ret = WritePrivateProfileString("OFF-LINE", "R[47]", "10", Appoggio)
  ret = WritePrivateProfileString("OFF-LINE", "R[57]", "10", Appoggio)
  ret = WritePrivateProfileString("OFF-LINE", "R[41]", frmt(R41ch, 3), Appoggio)
  ret = WritePrivateProfileString("OFF-LINE", "R[51]", frmt(R51ch, 3), Appoggio)
  '
  'INIZIALIZZAZIONE DELLA BOMBATURA SUI FIANCHI
  'Bombatura = GetInfo("OFF-LINE", "R[46]", Appoggio)
  'If (Bombatura = 0) Then
  '  Bombatura = SD2 / Tan(R41ch * PG / 180) / 450
  '  ret = WritePrivateProfileString("OFF-LINE", "R[46]", frmt(Bombatura, 4), Appoggio)
  'End If
  'Bombatura = GetInfo("OFF-LINE", "R[56]", Appoggio)
  'If (Bombatura = 0) Then
  '  Bombatura = SD2 / Tan(R51ch * PG / 180) / 450
  '  ret = WritePrivateProfileString("OFF-LINE", "R[56]", frmt(Bombatura, 4), Appoggio)
  'End If
  '
  'LAVORO
  ret = WritePrivateProfileString("OFF-LINE", "R[124]", frmt(EGP2 * 180 / PG, 2), Appoggio)
  
  ret = WritePrivateProfileString("OFF-LINE", "R[60]", frmt(MF1, 4), Appoggio)
  ret = WritePrivateProfileString("OFF-LINE", "R[61]", frmt(MF2, 4), Appoggio)
  ret = WritePrivateProfileString("OFF-LINE", "R[62]", frmt(BF, 4), Appoggio)
  
  ret = WritePrivateProfileString("OFF-LINE", "R[128]", frmt(pX1, 4), Appoggio)
  ret = WritePrivateProfileString("OFF-LINE", "R[132]", frmt(pX2, 4), Appoggio)
  ret = WritePrivateProfileString("OFF-LINE", "R[133]", frmt(PX3, 4), Appoggio)
  
  ret = WritePrivateProfileString("OFF-LINE", "R[65]", frmt(SCMW, 4), Appoggio)
  ret = WritePrivateProfileString("OFF-LINE", "R[66]", frmt(MCMW, 4), Appoggio)
  
  ret = WritePrivateProfileString("OFF-LINE", "R[190]", frmt(R190, 4), Appoggio)
  ret = WritePrivateProfileString("OFF-LINE", "R[189]", frmt(DE, 4), Appoggio)
  ret = WritePrivateProfileString("OFF-LINE", "R[280]", frmt(250, 4), Appoggio)
  ret = WritePrivateProfileString("OFF-LINE", "R[281]", frmt(500, 4), Appoggio)
  ret = WritePrivateProfileString("OFF-LINE", "R[109]", frmt(EGP1 * 180 / PG, 2), Appoggio)
  ret = WritePrivateProfileString("OFF-LINE", "R[110]", "270", Appoggio)
  
  ret = WritePrivateProfileString("OFF-LINE", "R[91]", frmt(L1, 4), Appoggio)
  ret = WritePrivateProfileString("OFF-LINE", "R[92]", frmt(L2, 4), Appoggio)
  ret = WritePrivateProfileString("OFF-LINE", "R[93]", frmt(L3, 4), Appoggio)
  ret = WritePrivateProfileString("OFF-LINE", "R[94]", frmt(L4, 4), Appoggio)
  ret = WritePrivateProfileString("OFF-LINE", "R[95]", frmt(L5, 4), Appoggio)
  ret = WritePrivateProfileString("OFF-LINE", "R[96]", frmt(L6, 4), Appoggio)
  ret = WritePrivateProfileString("OFF-LINE", "R[97]", frmt(L7, 4), Appoggio)
  ret = WritePrivateProfileString("OFF-LINE", "R[98]", frmt(L8, 4), Appoggio)
  
  ret = WritePrivateProfileString("OFF-LINE", "R[75]", "1", Appoggio)
  ret = WritePrivateProfileString("OFF-LINE", "R[520]", frmt(SD4, 4), Appoggio)
  ret = WritePrivateProfileString("OFF-LINE", "R[521]", frmt(SD2, 4), Appoggio)
  ret = WritePrivateProfileString("OFF-LINE", "R[522]", frmt(SD1, 4), Appoggio)
  ret = WritePrivateProfileString("OFF-LINE", "R[523]", frmt(OD4, 4), Appoggio)
  ret = WritePrivateProfileString("OFF-LINE", "R[524]", frmt(SD5, 4), Appoggio)
  
  ret = WritePrivateProfileString("OFF-LINE", "R[76]", "1", Appoggio)
  ret = WritePrivateProfileString("OFF-LINE", "R[320]", frmt(MD3, 4), Appoggio)
  ret = WritePrivateProfileString("OFF-LINE", "R[321]", frmt(MD4, 4), Appoggio)
  ret = WritePrivateProfileString("OFF-LINE", "R[322]", frmt(MD7, 4), Appoggio)
  ret = WritePrivateProfileString("OFF-LINE", "R[323]", frmt(OD4, 4), Appoggio)
  ret = WritePrivateProfileString("OFF-LINE", "R[324]", frmt(MD2, 4), Appoggio)
  '
  'COSTRUZIONE DELLA SEQUENZA DEI CICLI DI RETTIFICA
  If (CALCOLO_SEQUENZA = "Y") Then
    'CICLI FASE A
    Call CALCOLO_SEQUENZA_CICLI_CONICHE(PX3, SD2 - OD4, DE, TipoMola, RIPE_RET, PASS_PRF, INCR_PRF, FEED_PRF, ROTA_PRF, PASS_RET, INCR_RET, VELO_ASS, VELO_MOL, SVRM_CYC)
    'CALCOLO I PARAMETRI PER IL PRIMO CICLO
    RIPE_RET(1) = INTERO(SVRM_CYC(1) / (INCR_RET(1) + INCR_RET(1)) / PASS_RET(1), "MAX")
    INCR_RET(1) = SVRM_CYC(1) / RIPE_RET(1) / PASS_RET(1) / 2
    INCR_RET(1) = Int(INCR_RET(1) * 10000) / 10000
    SCARTO(1) = SVRM_CYC(1) - RIPE_RET(1) * PASS_RET(1) * (INCR_RET(1) + INCR_RET(1))
    'SCARICO LO SCARTO NEL SECONDO CICLO
    SVRM_CYC(2) = SVRM_CYC(2) + SCARTO(1)
    INCR_RET(2) = SVRM_CYC(2) / PASS_RET(2)
    'CONVERSIONE GP->VT
    'GP=FEED/SQRT(360^2+PASSO^2)
    'VT=GP*SQRT((Pi*Dext)^2+PASSO^2)
    'GP=VT/SQRT((Pi*Dext)^2+PASSO^2)
    'CALCOLO VELOCITA'
    VELO_ASS(1) = INTERO(VELO_ASS(1) * Sqr((PG * DE) ^ 2 + PX3 ^ 2), "MIN")
    VELO_ASS(2) = INTERO(VELO_ASS(2) * Sqr((PG * DE) ^ 2 + PX3 ^ 2), "MIN")
    VELO_ASS(3) = INTERO(VELO_ASS(3) * Sqr((PG * DE) ^ 2 + PX3 ^ 2), "MIN")
    'PROFONDITA' OD4
    ret = WritePrivateProfileString("OFF-LINE", "STR[11]", "S", Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,1]", "1", Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,2]", Format$(PASS_PRF(1), "#####"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,3]", frmt(INCR_PRF(1), 4), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,4]", Format$(FEED_PRF(1), "#####"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,5]", ROTA_PRF(1), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,6]", "1", Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,7]", frmt(OD4, 4), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,8]", Format$(VELO_ASS(1), "##########"), Appoggio)
    'ret = WritePrivateProfileString("OFF-LINE", "CYC[0,9]", frmt(INCR_RET(1), 4), Appoggio)
    'ret = WritePrivateProfileString("OFF-LINE", "CYC[0,10]", Format$(VELO_ASS(1), "##########"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,12]", Format$(VELO_MOL(1), "##########"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,13]", "0", Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,14]", "1", Appoggio)
    'SGROSSATURA
    ret = WritePrivateProfileString("OFF-LINE", "STR[12]", "S", Appoggio)
    'RADDOPPIO LE RIPETIZIONI PERCHE' NON USO L'INCREMENTO AL RITORNO
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,21]", Format$(2 * RIPE_RET(1), "#####"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,22]", Format$(PASS_PRF(1), "#####"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,23]", frmt(INCR_PRF(1), 4), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,24]", Format$(FEED_PRF(1), "#####"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,25]", ROTA_PRF(1), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,26]", Format$(PASS_RET(1), "#####"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,27]", frmt(INCR_RET(1), 4), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,28]", Format$(VELO_ASS(1), "##########"), Appoggio)
    'ret = WritePrivateProfileString("OFF-LINE", "CYC[0,29]", frmt(INCR_RET(1), 4), Appoggio)
    'ret = WritePrivateProfileString("OFF-LINE", "CYC[0,30]", Format$(VELO_ASS(1), "##########"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,32]", Format$(VELO_MOL(1), "##########"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,33]", "0", Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,34]", "1", Appoggio)
    'PREFINITURA
    ret = WritePrivateProfileString("OFF-LINE", "STR[13]", "S", Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,41]", Format$(RIPE_RET(2), "#####"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,42]", Format$(PASS_PRF(2), "#####"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,43]", frmt(INCR_PRF(2), 4), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,44]", Format$(FEED_PRF(2), "#####"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,45]", ROTA_PRF(2), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,46]", Format$(PASS_RET(2), "#####"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,47]", frmt(INCR_RET(2), 4), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,48]", Format$(VELO_ASS(2), "##########"), Appoggio)
    'ret = WritePrivateProfileString("OFF-LINE", "CYC[0,49]", "0", Appoggio)
    'ret = WritePrivateProfileString("OFF-LINE", "CYC[0,50]", Format$(VELO_ASS(2), "##########"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,52]", Format$(VELO_MOL(2), "##########"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,53]", "1", Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,54]", "1", Appoggio)
    'FINITURA
    ret = WritePrivateProfileString("OFF-LINE", "STR[14]", "S", Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,61]", Format$(RIPE_RET(3), "#####"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,62]", Format$(PASS_PRF(3), "#####"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,63]", frmt(INCR_PRF(3), 4), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,64]", Format$(FEED_PRF(3), "#####"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,65]", ROTA_PRF(3), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,66]", Format$(PASS_RET(3), "#####"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,67]", frmt(INCR_RET(3), 4), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,68]", Format$(VELO_ASS(3), "##########"), Appoggio)
    'ret = WritePrivateProfileString("OFF-LINE", "CYC[0,69]", "0", Appoggio)
    'ret = WritePrivateProfileString("OFF-LINE", "CYC[0,70]", Format$(VELO_ASS(3), "##########"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,72]", Format$(VELO_MOL(3), "##########"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,73]", "1", Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,74]", "1", Appoggio)
    'CICLI FASE B
    Call CALCOLO_SEQUENZA_CICLI_CONICHE(PX3, MD4 - OD4, DE, TipoMola, RIPE_RET, PASS_PRF, INCR_PRF, FEED_PRF, ROTA_PRF, PASS_RET, INCR_RET, VELO_ASS, VELO_MOL, SVRM_CYC)
    'CALCOLO I PARAMETRI PER IL PRIMO CICLO
    RIPE_RET(1) = INTERO(SVRM_CYC(1) / (INCR_RET(1) + INCR_RET(1)) / PASS_RET(1), "MAX")
    INCR_RET(1) = SVRM_CYC(1) / RIPE_RET(1) / PASS_RET(1) / 2
    INCR_RET(1) = Int(INCR_RET(1) * 10000) / 10000
    SCARTO(1) = SVRM_CYC(1) - RIPE_RET(1) * PASS_RET(1) * (INCR_RET(1) + INCR_RET(1))
    'SCARICO LO SCARTO NEL SECONDO CICLO
    SVRM_CYC(2) = SVRM_CYC(2) + SCARTO(1)
    INCR_RET(2) = SVRM_CYC(2) / PASS_RET(2)
    'CONVERSIONE GP->VT
    'GP=FEED/SQRT(360^2+PASSO^2)
    'VT=GP*SQRT((Pi*Dext)^2+PASSO^2)
    'GP=VT/SQRT((Pi*Dext)^2+PASSO^2)
    'CALCOLO VELOCITA'
    VELO_ASS(1) = INTERO(VELO_ASS(1) * Sqr((PG * DE) ^ 2 + PX3 ^ 2), "MIN")
    VELO_ASS(2) = INTERO(VELO_ASS(2) * Sqr((PG * DE) ^ 2 + PX3 ^ 2), "MIN")
    VELO_ASS(3) = INTERO(VELO_ASS(3) * Sqr((PG * DE) ^ 2 + PX3 ^ 2), "MIN")
    'PROFONDITA' OD4
    ret = WritePrivateProfileString("OFF-LINE", "STR[21]", "S", Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,101]", "1", Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,102]", Format$(PASS_PRF(1), "#####"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,103]", frmt(INCR_PRF(1), 4), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,104]", Format$(FEED_PRF(1), "#####"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,106]", "1", Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,105]", ROTA_PRF(1), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,107]", frmt(OD4, 4), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,108]", Format$(VELO_ASS(1), "##########"), Appoggio)
    'ret = WritePrivateProfileString("OFF-LINE", "CYC[0,109]", frmt(INCR_RET(1), 4), Appoggio)
    'ret = WritePrivateProfileString("OFF-LINE", "CYC[0,110]", Format$(VELO_ASS(1), "##########"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,112]", Format$(VELO_MOL(1), "##########"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,113]", "0", Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,114]", "1", Appoggio)
    'SGROSSATURA
    ret = WritePrivateProfileString("OFF-LINE", "STR[22]", "S", Appoggio)
    'RADDOPPIO LE RIPETIZIONI PERCHE' NON USO L'INCREMENTO AL RITORNO
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,121]", Format$(2 * RIPE_RET(1), "#####"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,122]", Format$(PASS_PRF(1), "#####"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,123]", frmt(INCR_PRF(1), 4), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,124]", Format$(FEED_PRF(1), "#####"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,126]", Format$(PASS_RET(1), "#####"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,125]", ROTA_PRF(1), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,127]", frmt(INCR_RET(1), 4), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,128]", Format$(VELO_ASS(1), "##########"), Appoggio)
    'ret = WritePrivateProfileString("OFF-LINE", "CYC[0,129]", frmt(INCR_RET(1), 4), Appoggio)
    'ret = WritePrivateProfileString("OFF-LINE", "CYC[0,130]", Format$(VELO_ASS(1), "##########"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,132]", Format$(VELO_MOL(1), "##########"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,133]", "0", Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,134]", "1", Appoggio)
    'PREFINITURA
    ret = WritePrivateProfileString("OFF-LINE", "STR[23]", "S", Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,141]", Format$(RIPE_RET(2), "#####"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,142]", Format$(PASS_PRF(2), "#####"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,143]", frmt(INCR_PRF(2), 4), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,144]", Format$(FEED_PRF(2), "#####"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,145]", ROTA_PRF(2), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,146]", Format$(PASS_RET(2), "#####"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,147]", frmt(INCR_RET(2), 4), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,148]", Format$(VELO_ASS(2), "##########"), Appoggio)
    'ret = WritePrivateProfileString("OFF-LINE", "CYC[0,149]", "0", Appoggio)
    'ret = WritePrivateProfileString("OFF-LINE", "CYC[0,150]", Format$(VELO_ASS(2), "##########"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,152]", Format$(VELO_MOL(2), "##########"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,153]", "1", Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,154]", "1", Appoggio)
    'FINITURA
    ret = WritePrivateProfileString("OFF-LINE", "STR[24]", "S", Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,161]", Format$(RIPE_RET(3), "#####"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,162]", Format$(PASS_PRF(3), "#####"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,163]", frmt(INCR_PRF(3), 4), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,164]", Format$(FEED_PRF(3), "#####"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,165]", ROTA_PRF(3), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,166]", Format$(PASS_RET(3), "#####"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,167]", frmt(INCR_RET(3), 4), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,168]", Format$(VELO_ASS(3), "##########"), Appoggio)
    'ret = WritePrivateProfileString("OFF-LINE", "CYC[0,169]", "0", Appoggio)
    'ret = WritePrivateProfileString("OFF-LINE", "CYC[0,170]", Format$(VELO_ASS(3), "##########"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,172]", Format$(VELO_MOL(3), "##########"), Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,173]", "1", Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,174]", "1", Appoggio)
  Else
    'CICLI FASE A
    ret = WritePrivateProfileString("OFF-LINE", "STR[11]", "S", Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,1]", "1", Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,6]", "1", Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,7]", frmt(SD2, 4), Appoggio)
    'CICLI FASE B
    ret = WritePrivateProfileString("OFF-LINE", "STR[21]", "S", Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,101]", "1", Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,106]", "1", Appoggio)
    ret = WritePrivateProfileString("OFF-LINE", "CYC[0,107]", frmt(MD4, 4), Appoggio)
  End If
  ret = WritePrivateProfileString("Configurazione", "CARICA_CALCOLO", "Y", PathFILEINI)
  WRITE_DIALOG NomeVite & " EVALUATED "
  If (Len(sERRORE) > 0) Then WRITE_DIALOG NomeVite & sERRORE
  '
Exit Sub

errCALCOLO_VITI_BARRIERA_BS5:
  WRITE_DIALOG "Sub CALCOLO_VITI_BARRIERA_BS5: Error -> " & Err
  sERRORE = ": STANDARD EVALUATE WITH ERROR " & str(Err)
  Resume Next

End Sub

Sub CALCOLO_VITI_BARRIERA3()
'
Dim Atmp     As String * 255
Dim stmp     As String
Dim sERRORE  As String
Dim NomeVite As String
'
Dim PL As Double  'R132: Passo corto
Dim BL As Double  'R131: Passo lungo

Dim Ws  As Double

Dim L1          As Double
Dim L2          As Double
Dim L345        As Double
Dim L5          As Double
Dim L6          As Double
Dim L78         As Double
Dim L9          As Double
Dim L10         As Double

Dim DPT1  As Double 'profonditÓ zona 1
Dim DPT23 As Double 'profonditÓ zona 2-3
Dim DPT2  As Double 'profonditÓ fine zona 5
Dim DPT3  As Double 'profonditÓ fine zona 7
Dim DPT4  As Double 'profonditÓ zona 9
Dim DPT5  As Double 'profonditÓ inizio zona 5 barriera
Dim DPT6  As Double 'profonditÓ inizio zona 6 barriera
Dim DPT7  As Double 'profonditÓ inizio zona 7 barriera

Dim MFW As Double
Dim BFW As Double

Dim SCW As Double
Dim MCW As Double

Dim DE  As Double  'R189: Diametro esterno
Dim D2  As Double  'Diametro esterno barriera
Dim D10 As Double

Dim LC  As Double  'Channel width
Dim RGT As Double  'Radius tool offset

Dim R41ch     As Double
Dim R51ch     As Double
Dim R49ch     As Double
Dim R59ch     As Double
'
Dim ret       As Integer
Dim sVALORE   As String

Dim IMO      As Double
Dim EGP      As Double
Dim EBS      As Double
Dim LW       As Integer

Dim VELO_PRF          As String
Dim VELO_RUL          As String
Dim CALCOLO_SEQUENZA  As String
Dim RIPE_RET(3)       As Integer
Dim PASS_PRF(3)       As Integer
Dim INCR_PRF(3)       As Double
Dim FEED_PRF(3)       As Double
Dim ROTA_PRF(3)       As String
Dim PASS_RET(3)       As Integer
Dim INCR_RET(3)       As Double
Dim VELO_ASS(3)       As Double
Dim VELO_MOL(3)       As Double
Dim SVRM_CYC(3)       As Double
Dim SCARTO(3)         As Double
Dim TipoMola          As String
'
On Error GoTo errCALCOLO_VITI_BARRIERA3
  '
  WRITE_DIALOG NomeVite & " EVALUATING "
  
  NomeVite = GetInfo("VITI_BARRIERA3", "NomePezzoInserimento", Path_LAVORAZIONE_INI)
  NomeVite = UCase$(Trim$(NomeVite))
  
                            DPT1 = LEP("DPT1")
  L1 = LEP("L1"):           DPT23 = LEP("DPT23")
  L2 = LEP("L2"):           DPT2 = LEP("DPT2")
  L345 = LEP("L345"):       DPT3 = LEP("DPT3")
  L5 = LEP("L5"):           DPT4 = LEP("DPT4")
  L6 = LEP("L6"):           DPT5 = LEP("DPT5")
  L78 = LEP("L78"):         DPT6 = LEP("DPT6")
  L9 = LEP("L9"):           DPT7 = LEP("DPT7")
  L10 = LEP("L10"):
  
  PL = LEP("PL"):           MFW = LEP("MFW")
  BL = LEP("BL"):           BFW = LEP("BFW")
  DE = LEP("DE"):           R49ch = LEP("R49ch")
  D2 = LEP("D2"):           R59ch = LEP("R59ch")
  D10 = LEP("D10"):
  If (D10 <= 0) Then
    D10 = (DE - DPT4 * 2)
    Call SP("D10", frmt(D10, 3), False)
  End If
  SCW = LEP("SCW"):
  MCW = LEP("MCW"):
    
  Ws = LEP("WS")    '400
  TipoMola = ""     'CERAMICA
  RGT = LEP("RGT")     '1
  
  R41ch = LEP("R41ch") '5
  R51ch = LEP("R51ch") '30
  R49ch = LEP("R49ch") '3.5
  R59ch = LEP("R59ch") '8

  VELO_PRF = GetInfo("CICLI_VITI_CONICHE", "VELO_PRF" & TipoMola, Path_LAVORAZIONE_INI)
  If (Len(VELO_PRF) <= 0) Then VELO_PRF = "30"
  VELO_RUL = GetInfo("CICLI_VITI_CONICHE", "VELO_RUL" & TipoMola, Path_LAVORAZIONE_INI)
  If (Len(VELO_RUL) <= 0) Then VELO_RUL = "24"
  
  Call SP("R[320]", frmt(DE - 2 * DPT1, 3), False)
  Call SP("R[321]", frmt(D10, 3), False)
  Call SP("R[322]", frmt(D10, 3), False)
  Call SP("R[323]", frmt(D2, 3), False)
  
  Call SP("R[131]", frmt(BL, 3), False)
  Call SP("R[132]", frmt(PL, 3), False)
  
  Call SP("R[248]", "250", False)
  Call SP("R[200]", "360", False)

  EGP = Abs(Atn(PL / PG / DE))
  EBS = Abs(Atn(BL / PG / DE))
  IMO = EGP + (EBS - EGP) * 0.7
  Call SP("R[130]", frmt(-IMO * 180 / PG, 2), False)
          
  Call SP("R[80]", frmt(DPT1, 3), False)
  Call SP("R[282]", frmt(DPT23, 3), False)
  Call SP("R[81]", frmt(DPT2, 3), False)
  Call SP("R[82]", frmt(DPT3, 3), False)
  Call SP("R[87]", frmt(DPT4, 3), False)
  Call SP("R[83]", frmt(DPT5, 3), False)
  Call SP("R[84]", frmt(DPT6, 3), False)
  Call SP("R[85]", frmt((DE - D2) / 2, 3), False)
  Call SP("R[88]", frmt(DPT7, 3), False)
  Call SP("R[86]", frmt(DPT3, 3), False)

  Call SP("R[35]", frmt(MFW, 3), False)
  Call SP("R[33]", frmt(BFW, 3), False)
  Call SP("R[31]", frmt(SCW / Cos(EBS), 3), False)
  Call SP("R[32]", frmt(MCW / Cos(EBS), 3), False)
  Call SP("R[189]", frmt(DE, 3), False)

  Call SP("R[288]", frmt((DE - D10) / 2, 3), False)

  Call SP("R[91]", frmt(L1 / PL, 3), False)
  Call SP("R[92]", frmt(L2 / PL, 3), False)
  Call SP("R[94]", frmt(0.1, 3), False)
  Call SP("R[95]", frmt(L5 / BL, 3), False)
  Call SP("R[93]", frmt(L345 / BL - 0.1 - L5 / BL, 3), False)
  Call SP("R[96]", frmt(L6 / BL, 3), False)
  
  Dim R97 As Double
  R97 = (L78 / BL) * (DPT3 - (DE - D2) / 2) / (DPT4 - (DE - D2) / 2)
  Call SP("R[97]", frmt(R97, 3), False)
  
  Call SP("R[98]", frmt(L78 / BL - R97, 3), False)
  Call SP("R[99]", frmt(L9 / BL, 3), False)
  
  If (L10 > 0) Then
    Call SP("R[90]", frmt(L10 / BL - 0.1, 3), False)
  Else
    Call SP("R[90]", frmt(L10 / BL, 3), False)
  End If

  Call SP("R[280]", "200", False)
  Call SP("R[281]", "400", False)
  Call SP("R[213]", "40", False)

  'MOLA 1 LARGA
  Call SP("MO1R[0,189]", frmt(DE, 3), False)
  LC = SCW
  LW = (Int(LC / 5) + 1) * 5
  Call SP("MO1R[0,37]", Format$(LW, "###0"), False)
  Call SP("MO1R[0,38]", frmt(LC, 3), False)
  Call SP("MO1R[0,49]", frmt(R49ch, 3), False)
  Call SP("MO1R[0,59]", frmt(R59ch, 3), False)
  Call SP("MO1R[0,48]", frmt((RGT + 0.2), 3), False)
  Call SP("MO1R[0,58]", frmt((RGT + 0.2), 3), False)
  Call SP("MO1R[0,43]", frmt((RGT + 0.5), 3), False)
  Call SP("MO1R[0,53]", frmt((RGT + 0.5), 3), False)
  Call SP("MO1R[0,44]", frmt((RGT + 0.5) + DPT1, 3), False)
  Call SP("MO1R[0,54]", frmt((RGT + 0.5) + DPT1, 3), False)
  Call SP("MO1R[0,47]", "10", False)
  Call SP("MO1R[0,57]", "10", False)
  Call SP("MO1R[0,41]", frmt(R41ch, 3), False)
  Call SP("MO1R[0,51]", frmt(R51ch, 3), False)
  Call SP("CODICE_M1", "NONE", False)
  '
  Call SP("MO1R[0,204]", VELO_PRF, False)
  Call SP("MO1R[0,248]", "250", False)
  Call SP("MO1R[0,200]", "360", False)
  '
  Call SP("MO1R[0,189]", frmt(DE, 3), False)
  Call SP("MO1R[0,124]", frmt(-IMO * 180 / PG, 2), False)
  Call SP("MO1R[0,128]", frmt(PL, 3), False)
  Call SP("MO1R[0,190]", frmt(Ws, 3), False)
  Call SP("MO1R[0,220]", frmt(DPT1, 3), False)
  Call SP("MO1R[0,213]", "30", False)

  Dim TESTMOL As Double
  Dim LNC     As Double
  
  'CALCOLO TESTMOL COME DESCRITTO IN INS_BS3.PRG
  If (DPT5 < R49ch * (1 - Sin(R41ch * PG / 180))) Then
    TESTMOL = Sqr(R49ch * R49ch - (R49ch - DPT5) * (R49ch - DPT5))
  Else
    TESTMOL = R49ch * Cos(R41ch * PG / 180) + (DPT5 - R49ch * (1 - Sin(R41ch * PG / 180))) * Tan(R41ch * PG / 180)
  End If
  If ((DE - D2) / 2 < R59ch * (1 - Sin(R51ch * PG / 180))) Then
    TESTMOL = TESTMOL + Sqr(R59ch * R59ch - (R59ch - (DE - D2) / 2) * (R59ch - (DE - D2) / 2))
  Else
    TESTMOL = TESTMOL + R59ch * Cos(R51ch * PG / 180) + ((DE - D2) / 2 - R59ch * (1 - Sin(R51ch * PG / 180))) * Tan(R51ch * PG / 180)
  End If
  TESTMOL = TESTMOL - R49ch * Tan((90 - R41ch) * PG / 360) - R59ch * Tan((90 - R51ch) * PG / 360)
  TESTMOL = TESTMOL - DPT7 * Tan(R41ch * PG / 180) - DPT7 * Tan(R51ch * PG / 180)
  
  'CALCOLO Larghezza Normale Canale
  LNC = BL * Cos(IMO) - SCW - MFW - BFW - 0.2
  
  'MOLA 2 STRETTA
  LC = MCW - TESTMOL - 0.3
  If (LC > LNC) Then
    LC = LNC
  End If
  LW = (Int(LC / 5) + 1) * 5
  Call SP("MO2R[0,37]", Format$(LW, "###0"), False)
  Call SP("MO2R[0,38]", frmt(LC, 3), False)
  Call SP("MO2R[0,49]", frmt(R49ch, 3), False)
  Call SP("MO2R[0,59]", frmt(R59ch, 3), False)
  Call SP("MO2R[0,48]", frmt((RGT + 0.2), 3), False)
  Call SP("MO2R[0,58]", frmt((RGT + 0.2), 3), False)
  Call SP("MO2R[0,43]", frmt((RGT + 0.5), 3), False)
  Call SP("MO2R[0,53]", frmt((RGT + 0.5), 3), False)
  Call SP("MO2R[0,44]", frmt((RGT + 0.5) + DPT7, 3), False)
  Call SP("MO2R[0,54]", frmt((RGT + 0.5) + DPT7, 3), False)
  Call SP("MO2R[0,47]", "10", False)
  Call SP("MO2R[0,57]", "10", False)
  Call SP("MO2R[0,41]", frmt(R41ch, 3), False)
  Call SP("MO2R[0,51]", frmt(R51ch, 3), False)
  Call SP("CODICE_M2", "NONE", False)
  '
  Call SP("MO2R[0,204]", VELO_PRF, False)
  Call SP("MO2R[0,248]", "250", False)
  Call SP("MO2R[0,200]", "360", False)
  '
  Call SP("MO2R[0,189]", frmt(DE, 3), False)
  Call SP("MO2R[0,124]", frmt(-IMO * 180 / PG, 2), False)
  Call SP("MO2R[0,128]", frmt(BL, 3), False)
  Call SP("MO2R[0,190]", frmt(Ws, 3), False)
  Call SP("MO2R[0,220]", frmt(DPT7, 3), False)
  Call SP("MO2R[0,213]", "40", False)
  
  'MOLA 3
'  EGP2 = Abs(Atn(BL / PG / DE))
'  LC = MCMW / Cos(EGP2)
'  LW = (Int(LC / 5) + 1) * 5
'  Call SP("MO3R[0,37]", Format$(LW, "###0"), False)
'  Call SP("MO3R[0,38]", frmt(MCMW, 3), False)
'  Call SP("MO3R[0,49]", frmt(R49ch, 3), False)
'  Call SP("MO3R[0,59]", frmt(R59ch, 3), False)
'  Call SP("MO3R[0,48]", frmt((RGT + 0.2), 3), False)
'  Call SP("MO3R[0,58]", frmt((RGT + 0.2), 3), False)
'  Call SP("MO3R[0,43]", frmt((RGT + 0.5), 3), False)
'  Call SP("MO3R[0,53]", frmt((RGT + 0.5), 3), False)
'  Call SP("MO3R[0,44]", frmt((RGT + 0.5) + MD5, 3), False)
'  Call SP("MO3R[0,54]", frmt((RGT + 0.5) + MD5, 3), False)
'  Call SP("MO3R[0,47]", "10", False)
'  Call SP("MO3R[0,57]", "10", False)
'  Call SP("MO3R[0,41]", frmt(R41ch, 3), False)
'  Call SP("MO3R[0,51]", frmt(R51ch, 3), False)
'  Call SP("CODICE_M3", "NONE", False)
'  '
'  Call SP("MO3R[0,204]", VELO_PRF, False)
'  Call SP("MO3R[0,248]", "250", False)
'  Call SP("MO3R[0,200]", "360", False)
'  '
'  Call SP("MO3R[0,124]", frmt(-EGP2 * 180 / PG, 2), False)
'  Call SP("MO3R[0,128]", frmt(pX2, 3), False)
'  Call SP("MO3R[0,190]", frmt(Ws, 3), False)
'  Call SP("MO3R[0,220]", frmt(MD5, 3), False)
'  Call SP("MO3R[0,213]", "40", False)
  '
  '**********************************************************************
  'AGGIORNAMENTO DEL DATABASE PER CONSENTIRE IL CALCOLO DEGLI SPOSTAMENTI
  Call AggiornaDB
  '**********************************************************************
  '
  'CALCOLO SPOSTAMENTI
  Dim DSW1 As Double:   Dim DSW2 As Double
  '***************************************************************************************************
  'MOLA LARGA
  '***************************************************************************************************
  Dim LS2_1 As Double:  Dim LS2_2 As Double
  Dim LE2_1 As Double:  Dim LE2_2 As Double
  Dim LS3_1 As Double:  Dim LS3_2 As Double
  Dim LE3_1 As Double:  Dim LE3_2 As Double
  Dim LS6_1 As Double:  Dim LS6_2 As Double
  Dim LE6_1 As Double:  Dim LE6_2 As Double
  Dim LE8_1 As Double:  Dim LE8_2 As Double
  Dim LE7_1 As Double:  Dim LE7_2 As Double:  Dim LE72_1 As Double
  Dim L10_1 As Double:  Dim L10_2 As Double
  Dim IMOg As Double
  IMOg = (IMO * 180 / PG)
  'ZONA 2/F1 INIZIO
  Call CALCOLO_CANALE(PL, IMOg, DE, DE - DPT1 * 2, DSW1, DSW2, 0, 0, 1):  LS2_1 = DSW1:  LS2_2 = DSW2
  'ZONA 2/F2 FINE
  Call CALCOLO_CANALE(PL, IMOg, DE, DE - DPT23 * 2, DSW1, DSW2, 0, 0, 1): LE2_1 = DSW1:  LE2_2 = DSW2
  'ZONA 3/F1 INIZIO
  Call CALCOLO_CANALE(BL, IMOg, DE, DE - DPT23 * 2, DSW1, DSW2, 0, 0, 1): LS3_1 = DSW1:  LS3_2 = DSW2
  'ZONA 3/F1 FINE
  Call CALCOLO_CANALE(BL, IMOg, DE, D2, DSW1, DSW2, 0, 0, 1):             LE3_1 = DSW1:
  'ZONA 6+7/F1 INIZIO
  Call CALCOLO_CANALE(BL, IMOg, D2, DE - DPT2 * 2, DSW1, DSW2, 0, 0, 1):  LS6_1 = DSW1:
  'ZONA 6+7/F1 FINE
  Call CALCOLO_CANALE(BL, IMOg, D2, DE - DPT3 * 2, DSW1, DSW2, 0, 0, 1):  LE6_1 = DSW1:
  'ZONA 6+7/F2 INIZIO
  Call CALCOLO_CANALE(BL, IMOg, DE, DE - DPT2 * 2, DSW1, DSW2, 0, 0, 1):                 LS6_2 = DSW2
  'ZONA 6+7/F2 FINE
  Call CALCOLO_CANALE(BL, IMOg, DE, DE - DPT3 * 2, DSW1, DSW2, 0, 0, 1):                 LE6_2 = DSW2
  'ZONA 8/F1+F2 FINE
  Call CALCOLO_CANALE(BL, IMOg, DE, DE - DPT4 * 2, DSW1, DSW2, 0, 0, 1):  LE8_1 = DSW1:  LE8_2 = DSW2
  'ZONA 7/F1 FINE
  Call CALCOLO_CANALE(BL, IMOg, D2, DE - DPT3 * 2, DSW1, DSW2, 0, 0, 1):  LE7_1 = DSW1:
  'ZONA 7/F1+F2 FINE
  Call CALCOLO_CANALE(BL, IMOg, DE, DE - DPT3 * 2, DSW1, DSW2, 0, 0, 1):  LE72_1 = DSW1: LE7_2 = DSW2
  'ZONA 10/F1+F2 FINE
  Call CALCOLO_CANALE(BL, IMOg, DE, D10, DSW1, DSW2, 0, 0, 1):            L10_1 = DSW1:  L10_2 = DSW2
  '***************************************************************************************************
  'MOLA STRETTA
  '***************************************************************************************************
  Dim LS5S_1 As Double:  Dim LS5S_2 As Double
  Dim LE5S_1 As Double:  Dim LE5S_2 As Double
  Dim LS6S_1 As Double:  Dim LS6S_2 As Double
  Dim LE6S_1 As Double:  Dim LE6S_2 As Double
  Dim LE8S_1 As Double:  Dim LE8S_2 As Double
  'ZONA 5/F1 INIZIO
  Call CALCOLO_CANALE(BL, IMOg, DE, DE - DPT5 * 2, DSW1, DSW2, 0, 0, 2):   LS5S_1 = DSW1:
  'ZONA 5/F2 INIZIO
  Call CALCOLO_CANALE(BL, IMOg, D2, DE - DPT5 * 2, DSW1, DSW2, 0, 0, 2):                  LS5S_2 = DSW2
  'ZONA 5/F1 FINE
  Call CALCOLO_CANALE(BL, IMOg, DE, DE - DPT6 * 2, DSW1, DSW2, 0, 0, 2):   LE5S_1 = DSW1:
  'ZONA 6S/F2 INIZIO
  Call CALCOLO_CANALE(BL, IMOg, D2, DE - DPT6 * 2, DSW1, DSW2, 0, 0, 2):                  LS6S_2 = DSW2
  'ZONA 6S/F1 FINE
  Call CALCOLO_CANALE(BL, IMOg, DE, DE - DPT7 * 2, DSW1, DSW2, 0, 0, 2):   LE6S_1 = DSW1:
  'ZONA 6S/F2 FINE
  Call CALCOLO_CANALE(BL, IMOg, D2, DE - DPT7 * 2, DSW1, DSW2, 0, 0, 2):                  LE6S_2 = DSW2
  'ZONA 8S/F1 FINE
  Call CALCOLO_CANALE(BL, IMOg, DE, DE - DPT4 * 2, DSW1, DSW2, 0, 0, 2):   LE8S_1 = DSW1: LE8S_2 = DSW2
  '***************************************************************************************************
  'MOLA LARGA
  '***************************************************************************************************
  Call SP("R[184]", frmt(LE2_1 - LS2_1, 3), False) 'DZ 2/F1 (-)
  Call SP("R[185]", frmt(LE2_2 - LS2_2, 3), False) 'DZ 2/F2 (-)
  Call SP("R[148]", frmt(LS6_1 - LS3_1, 3), False) 'DZ 3,4,5/F1 (+)
  Call SP("R[149]", frmt(LS6_2 - LS3_2, 3), False) 'DZ 3,4,5/F2 (+)
  Call SP("R[165]", frmt(LE6_1 - LS6_1, 3), False) 'DZ 6,7/F1 (-)
  Call SP("R[167]", frmt(LE6_2 - LS6_2, 3), False) 'DZ 6,7/F2 (-)
  Call SP("R[286]", frmt(LE8_1 - LE7_1, 3), False) 'DZ 8/F1 (+)
  Call SP("R[163]", frmt(LE8_2 - LE7_2, 3), False) 'DZ 8/F2 (+)
  Call SP("R[284]", frmt(LE2_1 + LE2_2 - LS3_1 - LS3_2, 3), False) 'COMP.DIFF.PASSI
  Call SP("R[287]", frmt(LE3_1 - LS3_1, 3), False) 'DZ 3/F1 FASE 2
  Call SP("R[137]", frmt(LS2_1 - LE8_1, 3), False) 'DZ /F1 RITORNO COMPLETO
  Call SP("R[138]", frmt(L10_1 + L10_2, 3), False) 'DZ /F2 RITORNO COMPLETO
  Call SP("R[205]", frmt(LE3_1 - LE72_1, 3), False) 'DZ 7/F1 FASE 2
  '*************************************************************************************************
  'MOLA STRETTA
  '*************************************************************************************************
  Call SP("R[206]", frmt(LE5S_1 - LS5S_1, 3), False) 'DZ 5/F1
  Call SP("R[176]", frmt(LE6S_2 - LS6S_2, 3), False) 'DZ 6/F2
  Call SP("R[186]", frmt(LE6S_1 - LE5S_1, 3), False) 'DZ 6/F1
  Call SP("R[178]", frmt(LE6S_1 - LE8S_1, 3), False) 'DZ 7,8/F1
  Call SP("R[188]", frmt(LS5S_1 + LS5S_2, 3), False) 'VANO ASSIALE 4/5
  Call SP("R[208]", frmt(LE5S_1 + LS6S_2, 3), False) 'VANO ASSIALE 5/6
  Call SP("R[207]", frmt(LE6S_1 + LE6S_2, 3), False) 'VANO ASSIALE 6/7
  '*******************************************************************
  'COSTRUZIONE DELLA SEQUENZA DEI CICLI DI RETTIFICA CON MOLA 1
  CALCOLO_SEQUENZA = GetInfo("VITI_BARRIERA3", "CALCOLO_SEQUENZA", Path_LAVORAZIONE_INI)
  CALCOLO_SEQUENZA = UCase$(Trim$(CALCOLO_SEQUENZA))
  If (Len(CALCOLO_SEQUENZA) <= 0) Then CALCOLO_SEQUENZA = "N"
  If (CALCOLO_SEQUENZA = "Y") Then
    'CICLI FASE A1
    Call CALCOLO_SEQUENZA_CICLI_CONICHE(PL, DPT1, DE, TipoMola, RIPE_RET, PASS_PRF, INCR_PRF, FEED_PRF, ROTA_PRF, PASS_RET, INCR_RET, VELO_ASS, VELO_MOL, SVRM_CYC)
    'CALCOLO I PARAMETRI PER IL PRIMO CICLO
    RIPE_RET(1) = INTERO(SVRM_CYC(1) / (INCR_RET(1) + INCR_RET(1)) / PASS_RET(1), "MAX")
    INCR_RET(1) = SVRM_CYC(1) / RIPE_RET(1) / PASS_RET(1) / 2
    INCR_RET(1) = Int(INCR_RET(1) * 10000) / 10000
    SCARTO(1) = SVRM_CYC(1) - RIPE_RET(1) * PASS_RET(1) * (INCR_RET(1) + INCR_RET(1))
    'SCARICO LO SCARTO NEL SECONDO CICLO
    SVRM_CYC(2) = SVRM_CYC(2) + SCARTO(1)
    INCR_RET(2) = SVRM_CYC(2) / PASS_RET(2)
    'CONVERSIONE GP->VT
    'GP=FEED/SQRT(360^2+PASSO^2)
    'VT=GP*SQRT((Pi*Dext)^2+PASSO^2)
    'GP=VT/SQRT((Pi*Dext)^2+PASSO^2)
    'CALCOLO VELOCITA'
    VELO_ASS(1) = INTERO(VELO_ASS(1) * Sqr((PG * DE) ^ 2 + PL ^ 2), "MIN")
    VELO_ASS(2) = INTERO(VELO_ASS(2) * Sqr((PG * DE) ^ 2 + PL ^ 2), "MIN")
    VELO_ASS(3) = INTERO(VELO_ASS(3) * Sqr((PG * DE) ^ 2 + PL ^ 2), "MIN")
    'SGROSSATURA
    Call SP("STR[11]", "S", False)
    Call SP("CICA1[0,1]", Format$(RIPE_RET(1), "#####"), False)
    Call SP("CICA1[0,2]", Format$(PASS_PRF(1), "#####"), False)
    Call SP("CICA1[0,3]", frmt(INCR_PRF(1), 4), False)
    Call SP("CICA1[0,4]", Format$(FEED_PRF(1), "#####"), False)
    Call SP("CICA1[0,5]", ROTA_PRF(1), False)
    Call SP("CICA1[0,6]", Format$(PASS_RET(1), "#####"), False)
    Call SP("CICA1[0,7]", frmt(INCR_RET(1), 4), False)
    Call SP("CICA1[0,8]", Format$(VELO_ASS(1), "##########"), False)
    Call SP("CICA1[0,9]", frmt(INCR_RET(1), 4), False)
    Call SP("CICA1[0,10]", Format$(VELO_ASS(1), "##########"), False)
    Call SP("CICA1[0,12]", Format$(VELO_MOL(1), "##########"), False)
    Call SP("CICA1[0,13]", Format$(VELO_PRF, "##########"), False)
    'PREFINITURA
    Call SP("STR[12]", "S", False)
    Call SP("CICA1[0,21]", Format$(RIPE_RET(2), "#####"), False)
    Call SP("CICA1[0,22]", Format$(PASS_PRF(2), "#####"), False)
    Call SP("CICA1[0,23]", frmt(INCR_PRF(2), 4), False)
    Call SP("CICA1[0,24]", Format$(FEED_PRF(2), "#####"), False)
    Call SP("CICA1[0,25]", ROTA_PRF(2), False)
    Call SP("CICA1[0,26]", Format$(PASS_RET(2), "#####"), False)
    Call SP("CICA1[0,27]", frmt(INCR_RET(2), 4), False)
    Call SP("CICA1[0,28]", Format$(VELO_ASS(2), "##########"), False)
    Call SP("CICA1[0,29]", "0", False)
    Call SP("CICA1[0,30]", Format$(VELO_ASS(2), "##########"), False)
    Call SP("CICA1[0,32]", Format$(VELO_MOL(2), "##########"), False)
    Call SP("CICA1[0,33]", Format$(VELO_PRF, "##########"), False)
    'FINITURA
    Call SP("STR[13]", "S", False)
    Call SP("CICA1[0,41]", Format$(RIPE_RET(3), "#####"), False)
    Call SP("CICA1[0,42]", Format$(PASS_PRF(3), "#####"), False)
    Call SP("CICA1[0,43]", frmt(INCR_PRF(3), 4), False)
    Call SP("CICA1[0,44]", Format$(FEED_PRF(3), "#####"), False)
    Call SP("CICA1[0,45]", ROTA_PRF(3), False)
    Call SP("CICA1[0,46]", Format$(PASS_RET(3), "#####"), False)
    Call SP("CICA1[0,47]", frmt(INCR_RET(3), 4), False)
    Call SP("CICA1[0,48]", Format$(VELO_ASS(3), "##########"), False)
    Call SP("CICA1[0,49]", "0", False)
    Call SP("CICA1[0,50]", Format$(VELO_ASS(3), "##########"), False)
    Call SP("CICA1[0,52]", Format$(VELO_MOL(3), "##########"), False)
    Call SP("CICA1[0,53]", Format$(VELO_PRF, "##########"), False)
    'NULLO
    Call SP("STR[14]", "N", False)
    Call SP("CICA1[0,61]", "0", False)
    Call SP("CICA1[0,62]", "0", False)
    Call SP("CICA1[0,63]", "0", False)
    Call SP("CICA1[0,64]", "0", False)
    Call SP("CICA1[0,65]", "0", False)
    Call SP("CICA1[0,66]", "0", False)
    Call SP("CICA1[0,67]", "0", False)
    Call SP("CICA1[0,68]", "0", False)
    Call SP("CICA1[0,69]", "0", False)
    Call SP("CICA1[0,70]", "0", False)
    Call SP("CICA1[0,72]", "0", False)
    Call SP("CICA1[0,73]", "0", False)
    'CICLI FASE A2
    Call CALCOLO_SEQUENZA_CICLI_CONICHE(PL, DPT23, DE, TipoMola, RIPE_RET, PASS_PRF, INCR_PRF, FEED_PRF, ROTA_PRF, PASS_RET, INCR_RET, VELO_ASS, VELO_MOL, SVRM_CYC)
    'CALCOLO I PARAMETRI PER IL PRIMO CICLO
    RIPE_RET(1) = INTERO(SVRM_CYC(1) / (INCR_RET(1) + INCR_RET(1)) / PASS_RET(1), "MAX")
    INCR_RET(1) = SVRM_CYC(1) / RIPE_RET(1) / PASS_RET(1) / 2
    INCR_RET(1) = Int(INCR_RET(1) * 10000) / 10000
    SCARTO(1) = SVRM_CYC(1) - RIPE_RET(1) * PASS_RET(1) * (INCR_RET(1) + INCR_RET(1))
    'SCARICO LO SCARTO NEL SECONDO CICLO
    SVRM_CYC(2) = SVRM_CYC(2) + SCARTO(1)
    INCR_RET(2) = SVRM_CYC(2) / PASS_RET(2)
    'CONVERSIONE GP->VT
    'GP=FEED/SQRT(360^2+PASSO^2)
    'VT=GP*SQRT((Pi*Dext)^2+PASSO^2)
    'GP=VT/SQRT((Pi*Dext)^2+PASSO^2)
    'CALCOLO VELOCITA'
    VELO_ASS(1) = INTERO(VELO_ASS(1) * Sqr((PG * DE) ^ 2 + PL ^ 2), "MIN")
    VELO_ASS(2) = INTERO(VELO_ASS(2) * Sqr((PG * DE) ^ 2 + PL ^ 2), "MIN")
    VELO_ASS(3) = INTERO(VELO_ASS(3) * Sqr((PG * DE) ^ 2 + PL ^ 2), "MIN")
    'SGROSSATURA
    Call SP("STR[21]", "S", False)
    Call SP("CICA2[0,1]", Format$(RIPE_RET(1), "#####"), False)
    Call SP("CICA2[0,2]", Format$(PASS_PRF(1), "#####"), False)
    Call SP("CICA2[0,3]", frmt(INCR_PRF(1), 4), False)
    Call SP("CICA2[0,4]", Format$(FEED_PRF(1), "#####"), False)
    Call SP("CICA2[0,5]", ROTA_PRF(1), False)
    Call SP("CICA2[0,6]", Format$(PASS_RET(1), "#####"), False)
    Call SP("CICA2[0,7]", frmt(INCR_RET(1), 4), False)
    Call SP("CICA2[0,8]", Format$(VELO_ASS(1), "##########"), False)
    Call SP("CICA2[0,9]", frmt(INCR_RET(1), 4), False)
    Call SP("CICA2[0,10]", Format$(VELO_ASS(1), "##########"), False)
    Call SP("CICA2[0,12]", Format$(VELO_MOL(1), "##########"), False)
    Call SP("CICA2[0,13]", Format$(VELO_PRF, "##########"), False)
    'PREFINITURA
    Call SP("STR[22]", "S", False)
    Call SP("CICA2[0,21]", Format$(RIPE_RET(2), "#####"), False)
    Call SP("CICA2[0,22]", Format$(PASS_PRF(2), "#####"), False)
    Call SP("CICA2[0,23]", frmt(INCR_PRF(2), 4), False)
    Call SP("CICA2[0,24]", Format$(FEED_PRF(2), "#####"), False)
    Call SP("CICA2[0,25]", ROTA_PRF(2), False)
    Call SP("CICA2[0,26]", Format$(PASS_RET(2), "#####"), False)
    Call SP("CICA2[0,27]", frmt(INCR_RET(2), 4), False)
    Call SP("CICA2[0,28]", Format$(VELO_ASS(2), "##########"), False)
    Call SP("CICA2[0,29]", "0", False)
    Call SP("CICA2[0,30]", Format$(VELO_ASS(2), "##########"), False)
    Call SP("CICA2[0,32]", Format$(VELO_MOL(2), "##########"), False)
    Call SP("CICA2[0,33]", Format$(VELO_PRF, "##########"), False)
    'FINITURA
    Call SP("STR[23]", "S", False)
    Call SP("CICA2[0,41]", Format$(RIPE_RET(3), "#####"), False)
    Call SP("CICA2[0,42]", Format$(PASS_PRF(3), "#####"), False)
    Call SP("CICA2[0,43]", frmt(INCR_PRF(3), 4), False)
    Call SP("CICA2[0,44]", Format$(FEED_PRF(3), "#####"), False)
    Call SP("CICA2[0,45]", ROTA_PRF(3), False)
    Call SP("CICA2[0,46]", Format$(PASS_RET(3), "#####"), False)
    Call SP("CICA2[0,47]", frmt(INCR_RET(3), 4), False)
    Call SP("CICA2[0,48]", Format$(VELO_ASS(3), "##########"), False)
    Call SP("CICA2[0,49]", "0", False)
    Call SP("CICA2[0,50]", Format$(VELO_ASS(3), "##########"), False)
    Call SP("CICA2[0,52]", Format$(VELO_MOL(3), "##########"), False)
    Call SP("CICA2[0,53]", Format$(VELO_PRF, "##########"), False)
    'NULLO
    Call SP("STR[24]", "N", False)
    Call SP("CICA2[0,61]", "0", False)
    Call SP("CICA2[0,62]", "0", False)
    Call SP("CICA2[0,63]", "0", False)
    Call SP("CICA2[0,64]", "0", False)
    Call SP("CICA2[0,65]", "0", False)
    Call SP("CICA2[0,66]", "0", False)
    Call SP("CICA2[0,67]", "0", False)
    Call SP("CICA2[0,68]", "0", False)
    Call SP("CICA2[0,69]", "0", False)
    Call SP("CICA2[0,70]", "0", False)
    Call SP("CICA2[0,72]", "0", False)
    Call SP("CICA2[0,73]", "0", False)
  Else
    'CICLI FASE A
    Call SP("STR[11]", "S", False)
    Call SP("CICA1[0,1]", "1", False)
    Call SP("CICA1[0,6]", "1", False)
    Call SP("CICA1[0,7]", frmt(DPT1, 4), False)
    'CICLI FASE B
    Call SP("STR[21]", "S", False)
    Call SP("CICA2[0,1]", "1", False)
    Call SP("CICA2[0,6]", "1", False)
    Call SP("CICA2[0,7]", frmt(DPT23, 4), False)
  End If
  If (CALCOLO_SEQUENZA = "Y") Then
    'CICLI FASE B1
    Call CALCOLO_SEQUENZA_CICLI_CONICHE(BL, DPT7, DE, TipoMola, RIPE_RET, PASS_PRF, INCR_PRF, FEED_PRF, ROTA_PRF, PASS_RET, INCR_RET, VELO_ASS, VELO_MOL, SVRM_CYC)
    'CALCOLO I PARAMETRI PER IL PRIMO CICLO
    RIPE_RET(1) = INTERO(SVRM_CYC(1) / INCR_RET(1) / PASS_RET(1), "MAX")
    INCR_RET(1) = SVRM_CYC(1) / RIPE_RET(1) / PASS_RET(1) / 2
    INCR_RET(1) = Int(INCR_RET(1) * 10000) / 10000
    SCARTO(1) = SVRM_CYC(1) - RIPE_RET(1) * PASS_RET(1) * INCR_RET(1)
    'SCARICO LO SCARTO NEL SECONDO CICLO
    SVRM_CYC(2) = SVRM_CYC(2) + SCARTO(1)
    INCR_RET(2) = SVRM_CYC(2) / PASS_RET(2)
    'CONVERSIONE GP->VT
    'GP=FEED/SQRT(360^2+PASSO^2)
    'VT=GP*SQRT((Pi*Dext)^2+PASSO^2)
    'GP=VT/SQRT((Pi*Dext)^2+PASSO^2)
    'CALCOLO VELOCITA'
    VELO_ASS(1) = INTERO(VELO_ASS(1) * Sqr((PG * DE) ^ 2 + BL ^ 2), "MIN")
    VELO_ASS(2) = INTERO(VELO_ASS(2) * Sqr((PG * DE) ^ 2 + BL ^ 2), "MIN")
    VELO_ASS(3) = INTERO(VELO_ASS(3) * Sqr((PG * DE) ^ 2 + BL ^ 2), "MIN")
    'SGROSSATURA
    Call SP("STR[31]", "S", False)
    Call SP("CICB1[0,1]", Format$(RIPE_RET(1), "#####"), False)
    Call SP("CICB1[0,2]", Format$(PASS_PRF(1), "#####"), False)
    Call SP("CICB1[0,3]", frmt(INCR_PRF(1), 4), False)
    Call SP("CICB1[0,4]", Format$(FEED_PRF(1), "#####"), False)
    Call SP("CICB1[0,5]", ROTA_PRF(1), False)
    Call SP("CICB1[0,6]", Format$(PASS_RET(1), "#####"), False)
    Call SP("CICB1[0,7]", frmt(INCR_RET(1), 4), False)
    Call SP("CICB1[0,8]", Format$(VELO_ASS(1), "##########"), False)
    Call SP("CICB1[0,9]", "0")
    Call SP("CICB1[0,10]", Format$(VELO_ASS(1), "##########"), False)
    Call SP("CICB1[0,12]", Format$(VELO_MOL(1), "##########"), False)
    Call SP("CICB1[0,13]", Format$(VELO_PRF, "##########"), False)
    'PREFINITURA
    Call SP("STR[32]", "S", False)
    Call SP("CICB1[0,21]", Format$(RIPE_RET(2), "#####"), False)
    Call SP("CICB1[0,22]", Format$(PASS_PRF(2), "#####"), False)
    Call SP("CICB1[0,23]", frmt(INCR_PRF(2), 4), False)
    Call SP("CICB1[0,24]", Format$(FEED_PRF(2), "#####"), False)
    Call SP("CICB1[0,25]", ROTA_PRF(2), False)
    Call SP("CICB1[0,26]", Format$(PASS_RET(2), "#####"), False)
    Call SP("CICB1[0,27]", frmt(INCR_RET(2), 4), False)
    Call SP("CICB1[0,28]", Format$(VELO_ASS(2), "##########"), False)
    Call SP("CICB1[0,29]", "1", False)
    Call SP("CICB1[0,30]", Format$(VELO_ASS(2), "##########"), False)
    Call SP("CICB1[0,32]", Format$(VELO_MOL(2), "##########"), False)
    Call SP("CICB1[0,33]", Format$(VELO_PRF, "##########"), False)
    'FINITURA
    Call SP("STR[33]", "S", False)
    Call SP("CICB1[0,41]", Format$(RIPE_RET(3), "#####"), False)
    Call SP("CICB1[0,42]", Format$(PASS_PRF(3), "#####"), False)
    Call SP("CICB1[0,43]", frmt(INCR_PRF(3), 4), False)
    Call SP("CICB1[0,44]", Format$(FEED_PRF(3), "#####"), False)
    Call SP("CICB1[0,45]", ROTA_PRF(3), False)
    Call SP("CICB1[0,46]", Format$(PASS_RET(3), "#####"), False)
    Call SP("CICB1[0,47]", frmt(INCR_RET(3), 4), False)
    Call SP("CICB1[0,48]", Format$(VELO_ASS(3), "##########"), False)
    Call SP("CICB1[0,49]", "1", False)
    Call SP("CICB1[0,50]", Format$(VELO_ASS(3), "##########"), False)
    Call SP("CICB1[0,52]", Format$(VELO_MOL(3), "##########"), False)
    Call SP("CICB1[0,53]", Format$(VELO_PRF, "##########"), False)
    'NULLO
    Call SP("STR[34]", "N", False)
    Call SP("CICB1[0,61]", "0", False)
    Call SP("CICB1[0,62]", "0", False)
    Call SP("CICB1[0,63]", "0", False)
    Call SP("CICB1[0,64]", "0", False)
    Call SP("CICB1[0,65]", "0", False)
    Call SP("CICB1[0,66]", "0", False)
    Call SP("CICB1[0,67]", "0", False)
    Call SP("CICB1[0,68]", "0", False)
    Call SP("CICB1[0,69]", "0", False)
    Call SP("CICB1[0,70]", "0", False)
    Call SP("CICB1[0,72]", "0", False)
    Call SP("CICB1[0,73]", "0", False)
    'CICLI FASE B2
    Call CALCOLO_SEQUENZA_CICLI_CONICHE(BL, DPT4, DE, TipoMola, RIPE_RET, PASS_PRF, INCR_PRF, FEED_PRF, ROTA_PRF, PASS_RET, INCR_RET, VELO_ASS, VELO_MOL, SVRM_CYC)
    'CALCOLO I PARAMETRI PER IL PRIMO CICLO
    RIPE_RET(1) = INTERO(SVRM_CYC(1) / (INCR_RET(1) + INCR_RET(1)) / PASS_RET(1), "MAX")
    INCR_RET(1) = SVRM_CYC(1) / RIPE_RET(1) / PASS_RET(1)
    INCR_RET(1) = Int(INCR_RET(1) * 10000) / 10000
    SCARTO(1) = SVRM_CYC(1) - RIPE_RET(1) * PASS_RET(1) * INCR_RET(1)
    'SCARICO LO SCARTO NEL SECONDO CICLO
    SVRM_CYC(2) = SVRM_CYC(2) + SCARTO(1)
    INCR_RET(2) = SVRM_CYC(2) / PASS_RET(2)
    'CONVERSIONE GP->VT
    'GP=FEED/SQRT(360^2+PASSO^2)
    'VT=GP*SQRT((Pi*Dext)^2+PASSO^2)
    'GP=VT/SQRT((Pi*Dext)^2+PASSO^2)
    'CALCOLO VELOCITA'
    VELO_ASS(1) = INTERO(VELO_ASS(1) * Sqr((PG * DE) ^ 2 + BL ^ 2), "MIN")
    VELO_ASS(2) = INTERO(VELO_ASS(2) * Sqr((PG * DE) ^ 2 + BL ^ 2), "MIN")
    VELO_ASS(3) = INTERO(VELO_ASS(3) * Sqr((PG * DE) ^ 2 + BL ^ 2), "MIN")
    'SGROSSATURA
    Call SP("STR[41]", "S", False)
    Call SP("CICB2[0,1]", "1")
    Call SP("CICB2[0,2]", Format$(PASS_PRF(1), "#####"), False)
    Call SP("CICB2[0,3]", frmt(INCR_PRF(1), 4), False)
    Call SP("CICB2[0,4]", Format$(FEED_PRF(1), "#####"), False)
    Call SP("CICB2[0,5]", ROTA_PRF(1), False)
    Call SP("CICB2[0,6]", Format$(PASS_RET(1) * RIPE_RET(1), "#####"), False)
    Call SP("CICB2[0,7]", frmt(INCR_RET(1), 4), False)
    Call SP("CICB2[0,8]", Format$(VELO_ASS(1), "##########"), False)
    'Call SP("CICB2[0,9]", frmt(INCR_RET(1), 4), False)
    'Call SP("CICB2[0,10]", Format$(VELO_ASS(1), "##########"), False)
    Call SP("CICB2[0,12]", Format$(VELO_MOL(1), "##########"), False)
    Call SP("CICB2[0,13]", Format$(VELO_PRF, "##########"), False)
    'PREFINITURA
    Call SP("STR[42]", "S", False)
    Call SP("CICB2[0,21]", Format$(RIPE_RET(2), "#####"), False)
    Call SP("CICB2[0,22]", Format$(PASS_PRF(2), "#####"), False)
    Call SP("CICB2[0,23]", frmt(INCR_PRF(2), 4), False)
    Call SP("CICB2[0,24]", Format$(FEED_PRF(2), "#####"), False)
    Call SP("CICB2[0,25]", ROTA_PRF(2), False)
    Call SP("CICB2[0,26]", Format$(PASS_RET(2), "#####"), False)
    Call SP("CICB2[0,27]", frmt(INCR_RET(2), 4), False)
    Call SP("CICB2[0,28]", Format$(VELO_ASS(2), "##########"), False)
    'Call SP("CICB2[0,29]", "0", False)
    'Call SP("CICB2[0,30]", Format$(VELO_ASS(2), "##########"), False)
    Call SP("CICB2[0,32]", Format$(VELO_MOL(3), "##########"), False)
    Call SP("CICB2[0,33]", Format$(VELO_PRF, "##########"), False)
    'FINITURA
    Call SP("STR[43]", "S", False)
    Call SP("CICB2[0,41]", Format$(RIPE_RET(3), "#####"), False)
    Call SP("CICB2[0,42]", Format$(PASS_PRF(3), "#####"), False)
    Call SP("CICB2[0,43]", frmt(INCR_PRF(3), 4), False)
    Call SP("CICB2[0,44]", Format$(FEED_PRF(3), "#####"), False)
    Call SP("CICB2[0,45]", ROTA_PRF(3), False)
    Call SP("CICB2[0,46]", Format$(PASS_RET(3), "#####"), False)
    Call SP("CICB2[0,47]", frmt(INCR_RET(3), 4), False)
    Call SP("CICB2[0,48]", Format$(VELO_ASS(3), "##########"), False)
    'Call SP("CICB2[0,49]", "0", False)
    'Call SP("CICB2[0,50]", Format$(VELO_ASS(3), "##########"), False)
    Call SP("CICB2[0,52]", Format$(VELO_MOL(3), "##########"), False)
    Call SP("CICB2[0,53]", Format$(VELO_PRF, "##########"), False)
    'NULLO
    Call SP("STR[44]", "N", False)
    Call SP("CICB2[0,61]", "0", False)
    Call SP("CICB2[0,62]", "0", False)
    Call SP("CICB2[0,63]", "0", False)
    Call SP("CICB2[0,64]", "0", False)
    Call SP("CICB2[0,65]", "0", False)
    Call SP("CICB2[0,66]", "0", False)
    Call SP("CICB2[0,67]", "0", False)
    Call SP("CICB2[0,68]", "0", False)
    Call SP("CICB2[0,69]", "0", False)
    Call SP("CICB2[0,70]", "0", False)
    Call SP("CICB2[0,72]", "0", False)
    Call SP("CICB2[0,73]", "0", False)
  Else
    'CICLI FASE B1
    Call SP("STR[31]", "S", False)
    Call SP("CICB1[0,1]", "1", False)
    Call SP("CICB1[0,6]", "1", False)
    Call SP("CICB1[0,7]", frmt(DPT7, 4), False)
    'CICLI FASE B2
    Call SP("STR[41]", "S", False)
    Call SP("CICB2[0,1]", "1", False)
    Call SP("CICB2[0,6]", "1", False)
    Call SP("CICB2[0,7]", frmt(DPT4, 4), False)
  End If
  '
  Call SP("DIAMEST[0,0]", frmt(DE, 4), False)
  Call SCRITTURA_CICLI_RETTIFICA_DIAMETRI
  '
  Call AggiornaDB
  ret = WritePrivateProfileString("Configurazione", "CARICA_CALCOLO", "N", PathFILEINI)
  WRITE_DIALOG NomeVite & " EVALUATED "
  If (Len(sERRORE) > 0) Then WRITE_DIALOG NomeVite & sERRORE
  '
Exit Sub

errCALCOLO_VITI_BARRIERA3:
  WRITE_DIALOG "Sub CALCOLO_VITI_BARRIERA3: Error -> " & Err
  sERRORE = ": STANDARD EVALUATE WITH ERROR " & str(Err)
  Resume Next

End Sub

Sub CALCOLO_VITI_BARRIERA6()
'
Dim Atmp     As String * 255
Dim stmp     As String
Dim sERRORE  As String
Dim NomeVite As String
'
Dim PC1 As Double '45
Dim PC2 As Double '55
Dim PB  As Double '62

Dim SEZIONE_RPV As Double
Dim SEZIONE_RVC As Double

Dim TRANSIZIONE  As Double
Dim M3L1  As Double
Dim M3L2  As Double
Dim M3L3  As Double
Dim M3L4  As Double
Dim BARRIERA  As Double
Dim M3L6  As Double
Dim M3L7  As Double
Dim M3L8  As Double
Dim M3L9  As Double

Dim MFW As Double '5.5
Dim BFW As Double '3.8

Dim DE_VIT  As Double
Dim DF_VIT  As Double
Dim DE_BAR  As Double
Dim DF_BAR  As Double

Dim RGT As Double  'Radius tool offset

Dim R41ch   As Double
Dim R51ch   As Double
Dim R49ch   As Double
Dim R59ch   As Double
'
Dim ret     As Integer
Dim sVALORE As String

Dim VELO_PRF         As String
Dim VELO_RUL         As String
Dim CALCOLO_SEQUENZA As String
Dim RIPE_RET(3)      As Integer
Dim PASS_PRF(3)      As Integer
Dim INCR_PRF(3)      As Double
Dim FEED_PRF(3)      As Double
Dim ROTA_PRF(3)      As String
Dim PASS_RET(3)      As Integer
Dim INCR_RET(3)      As Double
Dim VELO_ASS(3)      As Double
Dim VELO_MOL(3)      As Double
Dim SVRM_CYC(3)      As Double
Dim SCARTO(3)        As Double
Dim TipoMola         As String
'
Dim ZZ          As Double
Dim PROZ        As Double
Dim RAMPA1      As Double
Dim ESTENSIONE  As Double
'
On Error GoTo errCALCOLO_VITI_BARRIERA6
  '
  WRITE_DIALOG NomeVite & " EVALUATING "
  '
  NomeVite = GetInfo("VITI_BARRIERA6", "NomePezzoInserimento", Path_LAVORAZIONE_INI)
  NomeVite = UCase$(Trim$(NomeVite))
  '
  PC1 = LEP("PC1"): PC2 = LEP("PC2"): PB = LEP("PB")
  MFW = LEP("MFW"): BFW = LEP("BFW")
  '
  SEZIONE_RPV = LEP("SEZIONE_RPV")
  TRANSIZIONE = LEP("TRANSIZIONE")
  SEZIONE_RVC = LEP("SEZIONE_RVC")
  BARRIERA = LEP("BARRIERA")  'LUNGHEZZA DELLA BARRIERA
  RAMPA1 = LEP("M3L2")        'RAMPA DI ENTRATA/USCITA
  If (RAMPA1 <= 0.1) Then RAMPA1 = 0.1
  ESTENSIONE = LEP("M3L1")    'ESTENSIONE DALLA BARRIERA
  '
  M3L1 = ESTENSIONE
  M3L2 = Int(RAMPA1 * PC2)
  M3L1 = M3L1 - M3L2
  '
  M3L3 = LEP("M3L3"):  M3L4 = LEP("M3L4")
  M3L6 = LEP("M3L4"):  M3L7 = LEP("M3L3")
  '
  M3L9 = LEP("M3L9") 'SEZIONE PER CALCOLO SPESSORE MOLA M3
  'M3L8 = Int(RAMPA1 * PC2)
  '
  DE_VIT = LEP("DE_VIT"): DF_VIT = LEP("DF_VIT")
  DE_BAR = LEP("DE_BAR"): DF_BAR = LEP("DF_BAR")
  '
  Call SP("R[320]", frmt(DF_VIT, 3), False)
  Call SP("R[321]", frmt(DE_BAR, 3), False)
  '
  TipoMola = ""        'CERAMICA
  RGT = LEP("RGT")     '1
  '
  R41ch = LEP("R41ch") '5
  R51ch = LEP("R51ch") '30
  R49ch = LEP("R49ch") '3
  R59ch = LEP("R59ch") '5
  '
  VELO_PRF = GetInfo("CICLI_VITI_CONICHE", "VELO_PRF" & TipoMola, Path_LAVORAZIONE_INI)
  If (Len(VELO_PRF) <= 0) Then VELO_PRF = "30"
  VELO_RUL = GetInfo("CICLI_VITI_CONICHE", "VELO_RUL" & TipoMola, Path_LAVORAZIONE_INI)
  If (Len(VELO_RUL) <= 0) Then VELO_RUL = "24"
  '
  'MOLA M1 ****************************************
  Dim Z1   As Double
  Dim Z2   As Double
  Dim Z3   As Double
  Dim PZ1  As Double
  Dim PZ3  As Double
  Dim IMO1 As Double
  Dim IMO2 As Double
  Dim EGP1 As Double
  Dim EGP2 As Double
  Dim LC   As Double
  Dim LW   As Integer
  '************************************************
  Z1 = (PC1 + 5)           'TRATTO INIZIALE
  Z3 = TRANSIZIONE         'TRATTO FINALE
  Z2 = SEZIONE_RPV - Z1 - Z3 'TRATTO CENTRALE
  PZ1 = (DE_VIT - DF_VIT) / 2
  PZ3 = PZ1
  '************************************************
  Dim nGIRI1  As Double
  Dim nGIRI2  As Double
  '*************************************************
  'CALCOLO ELICA 1 SE NON E' IMPOSTATA
  IMO1 = LEP("MO1R[0,124]")
  If (IMO1 = 0) Then
    EGP1 = -Atn(PC1 / PG / DE_VIT)
    Call SP("MO1R[0,124]", frmt(EGP1 * 180 / PG, 2))
  Else
    EGP1 = IMO1 * PG / 180
  End If
  Call SP("MO1R[0,214]", frmt(PC1, 2), False)
  'CALCOLO ELICA 2 SE NON E' IMPOSTATA
  IMO2 = LEP("MO1R[0,125]")
  If (IMO2 = 0) Then
    EGP2 = -Atn(PC2 / PG / DE_VIT)
    Call SP("MO1R[0,125]", frmt(EGP2 * 180 / PG, 2))
  Else
    EGP2 = IMO2 * PG / 180
  End If
  Call SP("MO1R[0,215]", frmt(PC2, 2), False)
  Call SP("MO1R[0,189]", frmt(DE_VIT, 3), False)
  Call SP("MO1R[0,213]", "30", False)
  Call SP("MO1R[0,210]", frmt(Z1, 4), False)
  Call SP("MO1R[0,211]", frmt(Z2, 4), False)
  Call SP("MO1R[0,212]", frmt(Z3, 4), False)
  Call SP("MO1R[0,113]", frmt(PZ1, 4), False)
  Call SP("MO1R[0,114]", frmt(PZ3, 4), False)
  '************************************************
  LC = PC1 - MFW / Cos(EGP1)
  LW = (Int(LC / 5) + 1) * 5
  Call SP("MO1R[0,37]", Format$(LW, "###0"), False)
  Call SP("MO1R[0,38]", frmt(LC * Cos(EGP1), 3), False)
  Call SP("MO1R[0,49]", frmt(R49ch, 3), False)
  Call SP("MO1R[0,59]", frmt(R59ch, 3), False)
  Call SP("MO1R[0,48]", frmt((RGT + 0.2), 3), False)
  Call SP("MO1R[0,58]", frmt((RGT + 0.2), 3), False)
  Call SP("MO1R[0,43]", frmt((RGT + 0.5), 3), False)
  Call SP("MO1R[0,53]", frmt((RGT + 0.5), 3), False)
  Call SP("MO1R[0,44]", frmt((RGT + 0.5) + PZ1, 3), False)
  Call SP("MO1R[0,54]", frmt((RGT + 0.5) + PZ1, 3), False)
  Call SP("MO1R[0,47]", "10", False)
  Call SP("MO1R[0,57]", "10", False)
  Call SP("MO1R[0,41]", frmt(R41ch, 3), False)
  Call SP("MO1R[0,51]", frmt(R51ch, 3), False)
  Call SP("CODICE_M1", "NONE", False)
  '*******************************************************************
  Call SP("MO1R[0,204]", VELO_PRF, False)
  Call SP("R[248]", "250", False)
  Call SP("R[200]", "360", False)
  '*******************************************************************
  CALCOLO_SEQUENZA = GetInfo("VITI_BARRIERA6", "CALCOLO_SEQUENZA", Path_LAVORAZIONE_INI)
  CALCOLO_SEQUENZA = UCase$(Trim$(CALCOLO_SEQUENZA))
  If (Len(CALCOLO_SEQUENZA) <= 0) Then CALCOLO_SEQUENZA = "N"
  'COSTRUZIONE DELLA SEQUENZA DEI CICLI DI RETTIFICA CON MOLA 1
  If (CALCOLO_SEQUENZA = "Y") Then
    'CICLI FASE A1
    Call CALCOLO_SEQUENZA_CICLI_CONICHE(PC1, PZ1, DE_VIT, TipoMola, RIPE_RET, PASS_PRF, INCR_PRF, FEED_PRF, ROTA_PRF, PASS_RET, INCR_RET, VELO_ASS, VELO_MOL, SVRM_CYC)
    'CALCOLO I PARAMETRI PER IL PRIMO CICLO
    RIPE_RET(1) = INTERO(SVRM_CYC(1) / (INCR_RET(1) + INCR_RET(1)) / PASS_RET(1), "MAX")
    INCR_RET(1) = SVRM_CYC(1) / RIPE_RET(1) / PASS_RET(1) / 2
    INCR_RET(1) = Int(INCR_RET(1) * 10000) / 10000
    SCARTO(1) = SVRM_CYC(1) - RIPE_RET(1) * PASS_RET(1) * (INCR_RET(1) + INCR_RET(1))
    'SCARICO LO SCARTO NEL SECONDO CICLO
    SVRM_CYC(2) = SVRM_CYC(2) + SCARTO(1)
    INCR_RET(2) = SVRM_CYC(2) / PASS_RET(2)
    'CALCOLO VELOCITA'
    VELO_ASS(1) = INTERO(VELO_ASS(1) * Sqr((PG * DE_VIT) ^ 2 + PC1 ^ 2), "MIN")
    VELO_ASS(2) = INTERO(VELO_ASS(2) * Sqr((PG * DE_VIT) ^ 2 + PC1 ^ 2), "MIN")
    VELO_ASS(3) = INTERO(VELO_ASS(3) * Sqr((PG * DE_VIT) ^ 2 + PC1 ^ 2), "MIN")
    'SGROSSATURA
    Call SP("STR[11]", "S", False)
    Call SP("CICA1[0,1]", Format$(RIPE_RET(1), "#####"), False)
    Call SP("CICA1[0,2]", Format$(PASS_PRF(1), "#####"), False)
    Call SP("CICA1[0,3]", frmt(INCR_PRF(1), 4), False)
    Call SP("CICA1[0,4]", Format$(FEED_PRF(1), "#####"), False)
    Call SP("CICA1[0,5]", ROTA_PRF(1), False)
    Call SP("CICA1[0,6]", Format$(PASS_RET(1), "#####"), False)
    Call SP("CICA1[0,7]", frmt(INCR_RET(1), 4), False)
    Call SP("CICA1[0,8]", Format$(VELO_ASS(1), "##########"), False)
    Call SP("CICA1[0,9]", frmt(INCR_RET(1), 4), False)
    Call SP("CICA1[0,10]", Format$(VELO_ASS(1), "##########"), False)
    Call SP("CICA1[0,12]", Format$(VELO_MOL(1), "##########"), False)
    Call SP("CICA1[0,13]", Format$(VELO_PRF, "##########"), False)
    'PREFINITURA
    Call SP("STR[12]", "S", False)
    Call SP("CICA1[0,21]", Format$(RIPE_RET(2), "#####"), False)
    Call SP("CICA1[0,22]", Format$(PASS_PRF(2), "#####"), False)
    Call SP("CICA1[0,23]", frmt(INCR_PRF(2), 4), False)
    Call SP("CICA1[0,24]", Format$(FEED_PRF(2), "#####"), False)
    Call SP("CICA1[0,25]", ROTA_PRF(2), False)
    Call SP("CICA1[0,26]", Format$(PASS_RET(2), "#####"), False)
    Call SP("CICA1[0,27]", frmt(INCR_RET(2), 4), False)
    Call SP("CICA1[0,28]", Format$(VELO_ASS(2), "##########"), False)
    Call SP("CICA1[0,29]", "0", False)
    Call SP("CICA1[0,30]", Format$(VELO_ASS(2), "##########"), False)
    Call SP("CICA1[0,32]", Format$(VELO_MOL(2), "##########"), False)
    Call SP("CICA1[0,33]", Format$(VELO_PRF, "##########"), False)
    'FINITURA
    Call SP("STR[13]", "S", False)
    Call SP("CICA1[0,41]", Format$(RIPE_RET(3), "#####"), False)
    Call SP("CICA1[0,42]", Format$(PASS_PRF(3), "#####"), False)
    Call SP("CICA1[0,43]", frmt(INCR_PRF(3), 4), False)
    Call SP("CICA1[0,44]", Format$(FEED_PRF(3), "#####"), False)
    Call SP("CICA1[0,45]", ROTA_PRF(3), False)
    Call SP("CICA1[0,46]", Format$(PASS_RET(3), "#####"), False)
    Call SP("CICA1[0,47]", frmt(INCR_RET(3), 4), False)
    Call SP("CICA1[0,48]", Format$(VELO_ASS(3), "##########"), False)
    Call SP("CICA1[0,49]", "0", False)
    Call SP("CICA1[0,50]", Format$(VELO_ASS(3), "##########"), False)
    Call SP("CICA1[0,52]", Format$(VELO_MOL(3), "##########"), False)
    Call SP("CICA1[0,53]", Format$(VELO_PRF, "##########"), False)
    'NULLO
    Call SP("STR[14]", "N", False)
    Call SP("CICA1[0,61]", "0", False)
    Call SP("CICA1[0,62]", "0", False)
    Call SP("CICA1[0,63]", "0", False)
    Call SP("CICA1[0,64]", "0", False)
    Call SP("CICA1[0,65]", "0", False)
    Call SP("CICA1[0,66]", "0", False)
    Call SP("CICA1[0,67]", "0", False)
    Call SP("CICA1[0,68]", "0", False)
    Call SP("CICA1[0,69]", "0", False)
    Call SP("CICA1[0,70]", "0", False)
    Call SP("CICA1[0,72]", "0", False)
    Call SP("CICA1[0,73]", "0", False)
    'CICLI FASE A2
    Call CALCOLO_SEQUENZA_CICLI_CONICHE(PC1, PZ1, DE_VIT, TipoMola, RIPE_RET, PASS_PRF, INCR_PRF, FEED_PRF, ROTA_PRF, PASS_RET, INCR_RET, VELO_ASS, VELO_MOL, SVRM_CYC)
    'CALCOLO I PARAMETRI PER IL PRIMO CICLO
    RIPE_RET(1) = INTERO(SVRM_CYC(1) / (INCR_RET(1) + INCR_RET(1)) / PASS_RET(1), "MAX")
    INCR_RET(1) = SVRM_CYC(1) / RIPE_RET(1) / PASS_RET(1) / 2
    INCR_RET(1) = Int(INCR_RET(1) * 10000) / 10000
    SCARTO(1) = SVRM_CYC(1) - RIPE_RET(1) * PASS_RET(1) * (INCR_RET(1) + INCR_RET(1))
    'SCARICO LO SCARTO NEL SECONDO CICLO
    SVRM_CYC(2) = SVRM_CYC(2) + SCARTO(1)
    INCR_RET(2) = SVRM_CYC(2) / PASS_RET(2)
    'CALCOLO VELOCITA'
    VELO_ASS(1) = INTERO(VELO_ASS(1) * Sqr((PG * DE_VIT) ^ 2 + PC1 ^ 2), "MIN")
    VELO_ASS(2) = INTERO(VELO_ASS(2) * Sqr((PG * DE_VIT) ^ 2 + PC1 ^ 2), "MIN")
    VELO_ASS(3) = INTERO(VELO_ASS(3) * Sqr((PG * DE_VIT) ^ 2 + PC1 ^ 2), "MIN")
    'SGROSSATURA
    Call SP("STR[21]", "S", False)
    Call SP("CICA2[0,1]", Format$(RIPE_RET(1), "#####"), False)
    Call SP("CICA2[0,2]", Format$(PASS_PRF(1), "#####"), False)
    Call SP("CICA2[0,3]", frmt(INCR_PRF(1), 4), False)
    Call SP("CICA2[0,4]", Format$(FEED_PRF(1), "#####"), False)
    Call SP("CICA2[0,5]", ROTA_PRF(1), False)
    Call SP("CICA2[0,6]", Format$(PASS_RET(1), "#####"), False)
    Call SP("CICA2[0,7]", frmt(INCR_RET(1), 4), False)
    Call SP("CICA2[0,8]", Format$(VELO_ASS(1), "##########"), False)
    Call SP("CICA2[0,9]", frmt(INCR_RET(1), 4), False)
    Call SP("CICA2[0,10]", Format$(VELO_ASS(1), "##########"), False)
    Call SP("CICA2[0,12]", Format$(VELO_MOL(1), "##########"), False)
    Call SP("CICA2[0,13]", Format$(VELO_PRF, "##########"), False)
    'PREFINITURA
    Call SP("STR[22]", "S", False)
    Call SP("CICA2[0,21]", Format$(RIPE_RET(2), "#####"), False)
    Call SP("CICA2[0,22]", Format$(PASS_PRF(2), "#####"), False)
    Call SP("CICA2[0,23]", frmt(INCR_PRF(2), 4), False)
    Call SP("CICA2[0,24]", Format$(FEED_PRF(2), "#####"), False)
    Call SP("CICA2[0,25]", ROTA_PRF(2), False)
    Call SP("CICA2[0,26]", Format$(PASS_RET(2), "#####"), False)
    Call SP("CICA2[0,27]", frmt(INCR_RET(2), 4), False)
    Call SP("CICA2[0,28]", Format$(VELO_ASS(2), "##########"), False)
    Call SP("CICA2[0,29]", "0", False)
    Call SP("CICA2[0,30]", Format$(VELO_ASS(2), "##########"), False)
    Call SP("CICA2[0,32]", Format$(VELO_MOL(2), "##########"), False)
    Call SP("CICA2[0,33]", Format$(VELO_PRF, "##########"), False)
    'FINITURA
    Call SP("STR[23]", "S", False)
    Call SP("CICA2[0,41]", Format$(RIPE_RET(3), "#####"), False)
    Call SP("CICA2[0,42]", Format$(PASS_PRF(3), "#####"), False)
    Call SP("CICA2[0,43]", frmt(INCR_PRF(3), 4), False)
    Call SP("CICA2[0,44]", Format$(FEED_PRF(3), "#####"), False)
    Call SP("CICA2[0,45]", ROTA_PRF(3), False)
    Call SP("CICA2[0,46]", Format$(PASS_RET(3), "#####"), False)
    Call SP("CICA2[0,47]", frmt(INCR_RET(3), 4), False)
    Call SP("CICA2[0,48]", Format$(VELO_ASS(3), "##########"), False)
    Call SP("CICA2[0,49]", "0", False)
    Call SP("CICA2[0,50]", Format$(VELO_ASS(3), "##########"), False)
    Call SP("CICA2[0,52]", Format$(VELO_MOL(3), "##########"), False)
    Call SP("CICA2[0,53]", Format$(VELO_PRF, "##########"), False)
    'NULLO
    Call SP("STR[24]", "N", False)
    Call SP("CICA2[0,61]", "0", False)
    Call SP("CICA2[0,62]", "0", False)
    Call SP("CICA2[0,63]", "0", False)
    Call SP("CICA2[0,64]", "0", False)
    Call SP("CICA2[0,65]", "0", False)
    Call SP("CICA2[0,66]", "0", False)
    Call SP("CICA2[0,67]", "0", False)
    Call SP("CICA2[0,68]", "0", False)
    Call SP("CICA2[0,69]", "0", False)
    Call SP("CICA2[0,70]", "0", False)
    Call SP("CICA2[0,72]", "0", False)
    Call SP("CICA2[0,73]", "0", False)
  Else
    'CICLI FASE A
    Call SP("STR[11]", "S", False)
    Call SP("CICA1[0,1]", "1", False)
    Call SP("CICA1[0,6]", "1", False)
    Call SP("CICA1[0,7]", frmt(PZ1, 4), False)
    'CICLI FASE B
    Call SP("STR[21]", "S", False)
    Call SP("CICA2[0,1]", "1", False)
    Call SP("CICA2[0,6]", "1", False)
    Call SP("CICA2[0,7]", frmt(PZ1, 4), False)
  End If
  'MOLA M2 ****************************************
  Dim Z1_A  As Double: Dim Z1_B  As Double
  Dim Z2_A  As Double: Dim Z2_B  As Double
  Dim Z3_A  As Double: Dim Z3_B  As Double
  Dim Z4_A  As Double: Dim Z4_B  As Double
  Dim Z5_A  As Double: Dim Z5_B  As Double
  Dim Z6_A  As Double: Dim Z6_B  As Double
  Dim Z7_A  As Double: Dim Z7_B  As Double
  Dim PZ1_A As Double: Dim PZ1_B As Double
  Dim PZ3_A As Double: Dim PZ3_B As Double
  Dim PZ5_A As Double: Dim PZ5_B As Double
  Dim PZ7_A As Double: Dim PZ7_B As Double
  'CALCOLO ELICA 2 SE NON E' IMPOSTATA
  IMO2 = LEP("MO2R[0,124]")
  If (IMO2 = 0) Then
    EGP2 = -Atn(PC2 / PG / DE_VIT)
    Call SP("MO2R[0,124]", frmt(EGP2 * 180 / PG, 2))
  Else
    EGP2 = IMO2 * PG / 180
  End If
  'TRATTI M2-B
  Z1_B = Int(RAMPA1 * PC2 * 10 + 0.5) / 10 'TRATTO CILINDRICO PER L'ENTRATA
  Z2_B = M3L9 - Z1_B - PC2           'TRATTO CONICO
  Z3_B = PC2 + PC2                         'TRATTO CILINDRICO DI COMPLETAMENTO
  Z4_B = 0: Z5_B = 0: Z6_B = 0: Z7_B = 0   'TRATTI NON USATI
  'TRATTI M2-A
  Z1_A = TRANSIZIONE   'TRATTO CICLIDRICO
  Z2_A = M3L1          'TRATTO CONICO
  Z3_A = 0.1           'TRATTO CILINDRICO PER RAMPA
  Z4_A = M3L2 - Z3_A   'RAMPA DI SALITA
  Z5_A = BARRIERA - PC2 + MFW / Cos(EGP2)
  Z5_A = Z5_A + Z1_B + Z2_B + Z3_B
  Z5_A = Int(Z5_A)     'DIAMETRO ESTERNO BARRIERA
  Z6_A = 0: Z7_A = 0   'TRATTI NON USATI
  'PROFONDITA' M2-A
  PZ1_A = (DE_VIT - DF_VIT) / 2
  ZZ = Z2_A
  GoSub CALCOLO_PROZ
  PZ3_A = PROZ: PZ3_A = Int(PZ3_A * 100) / 100
  PZ5_A = (DE_VIT - DE_BAR) / 2
  PZ7_A = 0            'TRATTO NON USATI
  'PROFONDITA' M2-B
  ZZ = M3L1 + M3L2 + BARRIERA - (M3L7 / PB) * PC2 - (M3L6 / PB) * PC2 + Z1_B
  GoSub CALCOLO_PROZ
  PZ1_B = PROZ: PZ1_B = Int(PZ1_B * 100) / 100
  ZZ = M3L1 + M3L2 + BARRIERA - (M3L7 / PB) * PC2 - (M3L6 / PB) * PC2 + Z1_B + Z2_B + Z3_B
  GoSub CALCOLO_PROZ
  PZ3_B = PROZ: PZ3_B = Int(PZ3_B * 100) / 100
  PZ5_B = 0: PZ7_B = 0 'TRATTI NON USATI
  '*******************************************************************
  Call SP("MO2R[0,128]", frmt(PC2, 2), False)
  Call SP("MO2R[0,61]", frmt(MFW / Cos(EGP2), 2), False)
  Call SP("MO2R[0,60]", frmt(BARRIERA, 4), False)
  Call SP("MO2R[0,280]", "250", False)
  Call SP("MO2R[0,281]", "500", False)
  '
  'RAMPA DI ENTRATA M2-B
  ZZ = Int(RAMPA1 * 360 + 0.5)
  Call SP("MO2R[0,313]", Format$(ZZ, "##0"), False)
  'RAMPA DI ENTRATA M1-B
  If (ZZ < 40) Then
    Call SP("MO2R[0,213]", Format$(ZZ, "##0"), False)
  Else
    Call SP("MO2R[0,213]", "40", False)
  End If
  '*******************************************************************
  Call SP("MO2R[0,91]", frmt(Z1_A, 2), False):   Call SP("MO2R[0,191]", frmt(Z1_B, 2), False)
  Call SP("MO2R[0,92]", frmt(Z2_A, 2), False):   Call SP("MO2R[0,192]", frmt(Z2_B, 2), False)
  Call SP("MO2R[0,93]", frmt(Z3_A, 2), False):   Call SP("MO2R[0,193]", frmt(Z3_B, 2), False)
  Call SP("MO2R[0,94]", frmt(Z4_A, 2), False):   Call SP("MO2R[0,194]", frmt(Z4_B, 2), False)
  Call SP("MO2R[0,95]", frmt(Z5_A, 2), False):   Call SP("MO2R[0,195]", frmt(Z5_B, 2), False)
  Call SP("MO2R[0,96]", frmt(Z6_A, 2), False):   Call SP("MO2R[0,196]", frmt(Z6_B, 2), False)
  Call SP("MO2R[0,97]", frmt(Z7_A, 2), False):   Call SP("MO2R[0,197]", frmt(Z7_B, 2), False)
  Call SP("MO2R[0,220]", frmt(PZ1_A, 2), False): Call SP("MO2R[0,320]", frmt(PZ1_B, 2), False)
  Call SP("MO2R[0,221]", frmt(PZ3_A, 2), False): Call SP("MO2R[0,321]", frmt(PZ3_B, 2), False)
  Call SP("MO2R[0,222]", frmt(PZ5_A, 2), False): Call SP("MO2R[0,322]", frmt(PZ5_B, 2), False)
  Call SP("MO2R[0,223]", frmt(PZ7_A, 2), False): Call SP("MO2R[0,323]", frmt(PZ7_B, 2), False)
  '*******************************************************************
  LC = PC2 - MFW / Cos(EGP2)
  LW = (Int(LC / 5) + 1) * 5
  Call SP("MO2R[0,37]", Format$(LW, "###0"), False)
  Call SP("MO2R[0,38]", frmt(LC * Cos(EGP2), 3), False)
  Call SP("MO2R[0,204]", VELO_PRF, False)
  Call SP("MO2R[0,49]", frmt(R49ch, 3), False)
  Call SP("MO2R[0,59]", frmt(R59ch, 3), False)
  Call SP("MO2R[0,48]", frmt((RGT + 0.2), 3), False)
  Call SP("MO2R[0,58]", frmt((RGT + 0.2), 3), False)
  Call SP("MO2R[0,43]", frmt((RGT + 0.5), 3), False)
  Call SP("MO2R[0,53]", frmt((RGT + 0.5), 3), False)
  Call SP("MO2R[0,44]", frmt((RGT + 0.5) + PZ1_A, 3), False)
  Call SP("MO2R[0,54]", frmt((RGT + 0.5) + PZ1_A, 3), False)
  Call SP("MO2R[0,47]", "10", False)
  Call SP("MO2R[0,57]", "10", False)
  Call SP("MO2R[0,41]", frmt(R41ch, 3), False)
  Call SP("MO2R[0,51]", frmt(R51ch, 3), False)
  'COSTRUZIONE DELLA SEQUENZA DEI CICLI DI RETTIFICA CON MOLA 2
  If (CALCOLO_SEQUENZA = "Y") Then
    Call CALCOLO_SEQUENZA_CICLI_CONICHE(PC2, PZ1_A, DE_VIT, TipoMola, RIPE_RET, PASS_PRF, INCR_PRF, FEED_PRF, ROTA_PRF, PASS_RET, INCR_RET, VELO_ASS, VELO_MOL, SVRM_CYC)
    'CALCOLO I PARAMETRI PER IL PRIMO CICLO
    RIPE_RET(1) = INTERO(SVRM_CYC(1) / (INCR_RET(1) + INCR_RET(1)) / PASS_RET(1), "MAX")
    INCR_RET(1) = SVRM_CYC(1) / RIPE_RET(1) / PASS_RET(1) / 2
    INCR_RET(1) = Int(INCR_RET(1) * 10000) / 10000
    SCARTO(1) = SVRM_CYC(1) - RIPE_RET(1) * PASS_RET(1) * (INCR_RET(1) + INCR_RET(1))
    'SCARICO LO SCARTO NEL SECONDO CICLO
    SVRM_CYC(2) = SVRM_CYC(2) + SCARTO(1)
    INCR_RET(2) = SVRM_CYC(2) / PASS_RET(2)
    'CALCOLO VELOCITA'
    VELO_ASS(1) = INTERO(VELO_ASS(1) * Sqr((PG * DE_VIT) ^ 2 + PC2 ^ 2), "MIN")
    VELO_ASS(2) = INTERO(VELO_ASS(2) * Sqr((PG * DE_VIT) ^ 2 + PC2 ^ 2), "MIN")
    VELO_ASS(3) = INTERO(VELO_ASS(3) * Sqr((PG * DE_VIT) ^ 2 + PC2 ^ 2), "MIN")
    'SGROSSATURA
    Call SP("STR[31]", "S", False)
    Call SP("CICB1[0,1]", Format$(RIPE_RET(1), "#####"), False)
    Call SP("CICB1[0,2]", Format$(PASS_PRF(1), "#####"), False)
    Call SP("CICB1[0,3]", frmt(INCR_PRF(1), 4), False)
    Call SP("CICB1[0,4]", Format$(FEED_PRF(1), "#####"), False)
    Call SP("CICB1[0,5]", ROTA_PRF(1), False)
    Call SP("CICB1[0,6]", Format$(PASS_RET(1), "#####"), False)
    Call SP("CICB1[0,7]", frmt(INCR_RET(1), 4), False)
    Call SP("CICB1[0,8]", Format$(VELO_ASS(1), "##########"), False)
    Call SP("CICB1[0,9]", frmt(INCR_RET(1), 4), False)
    Call SP("CICB1[0,10]", Format$(VELO_ASS(1), "##########"), False)
    Call SP("CICB1[0,12]", Format$(VELO_MOL(1), "##########"), False)
    Call SP("CICB1[0,13]", Format$(VELO_PRF, "##########"), False)
    'PREFINITURA
    Call SP("STR[32]", "S", False)
    Call SP("CICB1[0,21]", Format$(RIPE_RET(2), "#####"), False)
    Call SP("CICB1[0,22]", Format$(PASS_PRF(2), "#####"), False)
    Call SP("CICB1[0,23]", frmt(INCR_PRF(2), 4), False)
    Call SP("CICB1[0,24]", Format$(FEED_PRF(2), "#####"), False)
    Call SP("CICB1[0,25]", ROTA_PRF(2), False)
    Call SP("CICB1[0,26]", Format$(PASS_RET(2), "#####"), False)
    Call SP("CICB1[0,27]", frmt(INCR_RET(2), 4), False)
    Call SP("CICB1[0,28]", Format$(VELO_ASS(2), "##########"), False)
    Call SP("CICB1[0,29]", "0", False)
    Call SP("CICB1[0,30]", Format$(VELO_ASS(2), "##########"), False)
    Call SP("CICB1[0,32]", Format$(VELO_MOL(2), "##########"), False)
    Call SP("CICB1[0,33]", Format$(VELO_PRF, "##########"), False)
    'FINITURA
    Call SP("STR[33]", "S", False)
    Call SP("CICB1[0,41]", Format$(RIPE_RET(3), "#####"), False)
    Call SP("CICB1[0,42]", Format$(PASS_PRF(3), "#####"), False)
    Call SP("CICB1[0,43]", frmt(INCR_PRF(3), 4), False)
    Call SP("CICB1[0,44]", Format$(FEED_PRF(3), "#####"), False)
    Call SP("CICB1[0,45]", ROTA_PRF(3), False)
    Call SP("CICB1[0,46]", Format$(PASS_RET(3), "#####"), False)
    Call SP("CICB1[0,47]", frmt(INCR_RET(3), 4), False)
    Call SP("CICB1[0,48]", Format$(VELO_ASS(3), "##########"), False)
    Call SP("CICB1[0,49]", "0", False)
    Call SP("CICB1[0,50]", Format$(VELO_ASS(3), "##########"), False)
    Call SP("CICB1[0,52]", Format$(VELO_MOL(3), "##########"), False)
    Call SP("CICB1[0,53]", Format$(VELO_PRF, "##########"), False)
    'NULLO
    Call SP("STR[34]", "N", False)
    Call SP("CICB1[0,61]", "0", False)
    Call SP("CICB1[0,62]", "0", False)
    Call SP("CICB1[0,63]", "0", False)
    Call SP("CICB1[0,64]", "0", False)
    Call SP("CICB1[0,65]", "0", False)
    Call SP("CICB1[0,66]", "0", False)
    Call SP("CICB1[0,67]", "0", False)
    Call SP("CICB1[0,68]", "0", False)
    Call SP("CICB1[0,69]", "0", False)
    Call SP("CICB1[0,70]", "0", False)
    Call SP("CICB1[0,72]", "0", False)
    Call SP("CICB1[0,73]", "0", False)
    '
    Call CALCOLO_SEQUENZA_CICLI_CONICHE(PC2, PZ1_B, DE_VIT, TipoMola, RIPE_RET, PASS_PRF, INCR_PRF, FEED_PRF, ROTA_PRF, PASS_RET, INCR_RET, VELO_ASS, VELO_MOL, SVRM_CYC)
    'CALCOLO I PARAMETRI PER IL PRIMO CICLO
    RIPE_RET(1) = INTERO(SVRM_CYC(1) / (INCR_RET(1) + INCR_RET(1)) / PASS_RET(1), "MAX")
    INCR_RET(1) = SVRM_CYC(1) / RIPE_RET(1) / PASS_RET(1) / 2
    INCR_RET(1) = Int(INCR_RET(1) * 10000) / 10000
    SCARTO(1) = SVRM_CYC(1) - RIPE_RET(1) * PASS_RET(1) * (INCR_RET(1) + INCR_RET(1))
    'SCARICO LO SCARTO NEL SECONDO CICLO
    SVRM_CYC(2) = SVRM_CYC(2) + SCARTO(1)
    INCR_RET(2) = SVRM_CYC(2) / PASS_RET(2)
    'CALCOLO VELOCITA'
    VELO_ASS(1) = INTERO(VELO_ASS(1) * Sqr((PG * DE_VIT) ^ 2 + PC2 ^ 2), "MIN")
    VELO_ASS(2) = INTERO(VELO_ASS(2) * Sqr((PG * DE_VIT) ^ 2 + PC2 ^ 2), "MIN")
    VELO_ASS(3) = INTERO(VELO_ASS(3) * Sqr((PG * DE_VIT) ^ 2 + PC2 ^ 2), "MIN")
    'SGROSSATURA
    Call SP("STR[41]", "S", False)
    Call SP("CICB2[0,1]", Format$(RIPE_RET(1), "#####"), False)
    Call SP("CICB2[0,2]", Format$(PASS_PRF(1), "#####"), False)
    Call SP("CICB2[0,3]", frmt(INCR_PRF(1), 4), False)
    Call SP("CICB2[0,4]", Format$(FEED_PRF(1), "#####"), False)
    Call SP("CICB2[0,5]", ROTA_PRF(1), False)
    Call SP("CICB2[0,6]", Format$(PASS_RET(1), "#####"), False)
    Call SP("CICB2[0,7]", frmt(INCR_RET(1), 4), False)
    Call SP("CICB2[0,8]", Format$(VELO_ASS(1), "##########"), False)
    Call SP("CICB2[0,9]", frmt(INCR_RET(1), 4), False)
    Call SP("CICB2[0,10]", Format$(VELO_ASS(1), "##########"), False)
    Call SP("CICB2[0,12]", Format$(VELO_MOL(1), "##########"), False)
    Call SP("CICB2[0,13]", Format$(VELO_PRF, "##########"), False)
    'PREFINITURA
    Call SP("STR[42]", "S", False)
    Call SP("CICB2[0,21]", Format$(RIPE_RET(2), "#####"), False)
    Call SP("CICB2[0,22]", Format$(PASS_PRF(2), "#####"), False)
    Call SP("CICB2[0,23]", frmt(INCR_PRF(2), 4), False)
    Call SP("CICB2[0,24]", Format$(FEED_PRF(2), "#####"), False)
    Call SP("CICB2[0,25]", ROTA_PRF(2), False)
    Call SP("CICB2[0,26]", Format$(PASS_RET(2), "#####"), False)
    Call SP("CICB2[0,27]", frmt(INCR_RET(2), 4), False)
    Call SP("CICB2[0,28]", Format$(VELO_ASS(2), "##########"), False)
    Call SP("CICB2[0,29]", "0", False)
    Call SP("CICB2[0,30]", Format$(VELO_ASS(2), "##########"), False)
    Call SP("CICB2[0,32]", Format$(VELO_MOL(2), "##########"), False)
    Call SP("CICB2[0,33]", Format$(VELO_PRF, "##########"), False)
    'FINITURA
    Call SP("STR[43]", "S", False)
    Call SP("CICB2[0,41]", Format$(RIPE_RET(3), "#####"), False)
    Call SP("CICB2[0,42]", Format$(PASS_PRF(3), "#####"), False)
    Call SP("CICB2[0,43]", frmt(INCR_PRF(3), 4), False)
    Call SP("CICB2[0,44]", Format$(FEED_PRF(3), "#####"), False)
    Call SP("CICB2[0,45]", ROTA_PRF(3), False)
    Call SP("CICB2[0,46]", Format$(PASS_RET(3), "#####"), False)
    Call SP("CICB2[0,47]", frmt(INCR_RET(3), 4), False)
    Call SP("CICB2[0,48]", Format$(VELO_ASS(3), "##########"), False)
    Call SP("CICB2[0,49]", "0", False)
    Call SP("CICB2[0,50]", Format$(VELO_ASS(3), "##########"), False)
    Call SP("CICB2[0,52]", Format$(VELO_MOL(3), "##########"), False)
    Call SP("CICB2[0,53]", Format$(VELO_PRF, "##########"), False)
    'NULLO
    Call SP("STR[44]", "N", False)
    Call SP("CICB2[0,61]", "0", False)
    Call SP("CICB2[0,62]", "0", False)
    Call SP("CICB2[0,63]", "0", False)
    Call SP("CICB2[0,64]", "0", False)
    Call SP("CICB2[0,65]", "0", False)
    Call SP("CICB2[0,66]", "0", False)
    Call SP("CICB2[0,67]", "0", False)
    Call SP("CICB2[0,68]", "0", False)
    Call SP("CICB2[0,69]", "0", False)
    Call SP("CICB2[0,70]", "0", False)
    Call SP("CICB2[0,72]", "0", False)
    Call SP("CICB2[0,73]", "0", False)
  Else
    Call SP("STR[31]", "S", False)
    Call SP("CICB1[0,1]", "1", False)
    Call SP("CICB1[0,6]", "1", False)
    Call SP("CICB1[0,7]", frmt(PZ1_A, 4), False)
    Call SP("STR[41]", "S", False)
    Call SP("CICB2[0,1]", "1", False)
    Call SP("CICB2[0,6]", "1", False)
    Call SP("CICB2[0,7]", frmt(PZ1_B, 4), False)
  End If
  'MOLA M3 ****************************************
  Dim nG1   As Double 'X PC2
  Dim nG2   As Double 'X PB
  Dim IMO3  As Double
  Dim EGP3  As Double
  'CALCOLO ELICA 2 SE NON E' IMPOSTATA
  IMO3 = LEP("MO3R[0,124]")
  If (IMO3 = 0) Then
    EGP3 = -Atn(PB / PG / DE_VIT)
    Call SP("MO3R[0,124]", frmt(EGP3 * 180 / PG, 2))
  Else
    EGP3 = IMO3 * PG / 180
  End If
  Call SP("MO3R[0,128]", frmt(PB, 2), False)
  Call SP("MO3R[0,60]", frmt(BFW / Cos(EGP3), 2), False)
  '*************************************************
  nG1 = M3L9 / PC2
  nG2 = M3L3 / PB + M3L4 / PB
  '*************************************************
  Z1_A = (M3L3 / PB) * PC2: Z1_B = (M3L7 / PB) * PC2
  Z2_A = (M3L4 / PB) * PC2: Z2_B = (M3L6 / PB) * PC2
  Z3_A = BARRIERA - PC2:    Z3_B = BARRIERA - PC2
  '*************************************************
  Call SP("MO3R[0,91]", frmt(Z1_A, 2), False): Call SP("MO3R[0,191]", frmt(Z1_B, 2), False)
  Call SP("MO3R[0,92]", frmt(Z2_A, 2), False): Call SP("MO3R[0,192]", frmt(Z2_B, 2), False)
  Call SP("MO3R[0,93]", frmt(Z3_A, 2), False): Call SP("MO3R[0,193]", frmt(Z3_B, 2), False)
  '*************************************************
  ZZ = M3L1 + M3L2 + Z1_A + Z2_A
  GoSub CALCOLO_PROZ
  PZ1_A = Int(PROZ * 100 + 0.5) / 100
  '
  ZZ = M3L1 + M3L2 + BARRIERA
  GoSub CALCOLO_PROZ
  PZ3_A = Int(PROZ * 100 + 0.5) / 100
  '
  ZZ = M3L1 + M3L2 + BARRIERA - Z1_B - Z2_B
  GoSub CALCOLO_PROZ
  PZ1_B = Int(PROZ * 100 + 0.5) / 100
  '
  ZZ = M3L1 + M3L2 + BARRIERA - Z1_B - Z2_B - Z3_B
  GoSub CALCOLO_PROZ
  PZ3_B = Int(PROZ * 100 + 0.5) / 100
  '*************************************************
  Call SP("MO3R[0,220]", frmt(PZ1_A, 2), False): Call SP("MO3R[0,320]", frmt(PZ1_B, 2), False)
  Call SP("MO3R[0,221]", frmt(PZ3_A, 2), False): Call SP("MO3R[0,321]", frmt(PZ3_B, 2), False)
  '*************************************************
  R51ch = R41ch
  R49ch = R49ch * 0.8
  R59ch = R59ch * 0.8
  '*************************************************
  LC = 0
  LC = LC + (DE_VIT - DE_BAR) * (Tan(R41ch * PG / 180) + Tan(R51ch * PG / 180)) / Cos(EGP3) / 2
  LC = LC + (PZ3_B - PZ1_A) * Tan(R51ch * PG / 180) / Cos(EGP3)
  LC = LC + (nG1 + nG2) * (PB - PC2) - BFW / Cos(EGP3)
  LW = (Int(LC / 5) + 1) * 5
  Call SP("MO3R[0,37]", Format$(LW, "###0"), False)
  Call SP("MO3R[0,38]", frmt(LC * Cos(EGP3), 3), False)
  Call SP("MO3R[0,204]", VELO_PRF, False)
  Call SP("MO3R[0,49]", frmt(R49ch, 3), False)
  Call SP("MO3R[0,59]", frmt(R49ch, 3), False)
  Call SP("MO3R[0,48]", frmt((RGT + 0.2), 3), False)
  Call SP("MO3R[0,58]", frmt((RGT + 0.2), 3), False)
  Call SP("MO3R[0,43]", frmt((RGT + 0.5), 3), False)
  Call SP("MO3R[0,53]", frmt((RGT + 0.5), 3), False)
  Call SP("MO3R[0,44]", frmt((RGT + 0.5) + PZ3_B, 3), False)
  Call SP("MO3R[0,54]", frmt((RGT + 0.5) + PZ3_B, 3), False)
  Call SP("MO3R[0,47]", "10", False)
  Call SP("MO3R[0,57]", "10", False)
  Call SP("MO3R[0,41]", frmt(R41ch, 3), False)
  Call SP("MO3R[0,51]", frmt(R51ch, 3), False)
  '*************************************************
  Call SP("MO3R[0,213]", "30", False)
  Call SP("MO3R[0,280]", "250", False)
  Call SP("MO3R[0,281]", "500", False)
  'COSTRUZIONE DELLA SEQUENZA DEI CICLI DI RETTIFICA CON MOLA 3
  If (CALCOLO_SEQUENZA = "Y") Then
    Call CALCOLO_SEQUENZA_CICLI_CONICHE(PC2, PZ1_A, DE_VIT, TipoMola, RIPE_RET, PASS_PRF, INCR_PRF, FEED_PRF, ROTA_PRF, PASS_RET, INCR_RET, VELO_ASS, VELO_MOL, SVRM_CYC)
    'CALCOLO I PARAMETRI PER IL PRIMO CICLO
    RIPE_RET(1) = INTERO(SVRM_CYC(1) / INCR_RET(1) / PASS_RET(1), "MAX")
    INCR_RET(1) = SVRM_CYC(1) / RIPE_RET(1) / PASS_RET(1)
    INCR_RET(1) = Int(INCR_RET(1) * 10000) / 10000
    SCARTO(1) = SVRM_CYC(1) - RIPE_RET(1) * PASS_RET(1) * INCR_RET(1)
    'SCARICO LO SCARTO NEL SECONDO CICLO
    SVRM_CYC(2) = SVRM_CYC(2) + SCARTO(1)
    INCR_RET(2) = SVRM_CYC(2) / PASS_RET(2)
    'CALCOLO VELOCITA'
    VELO_ASS(1) = INTERO(VELO_ASS(1) * Sqr((PG * DE_VIT) ^ 2 + PC2 ^ 2), "MIN")
    VELO_ASS(2) = INTERO(VELO_ASS(2) * Sqr((PG * DE_VIT) ^ 2 + PC2 ^ 2), "MIN")
    VELO_ASS(3) = INTERO(VELO_ASS(3) * Sqr((PG * DE_VIT) ^ 2 + PC2 ^ 2), "MIN")
    'SGROSSATURA
    Call SP("STR[51]", "R", False)
    Call SP("CICC1[0,1]", Format$(RIPE_RET(1), "#####"), False)
    Call SP("CICC1[0,2]", Format$(PASS_PRF(1), "#####"), False)
    Call SP("CICC1[0,3]", frmt(INCR_PRF(1), 4), False)
    Call SP("CICC1[0,4]", Format$(FEED_PRF(1), "#####"), False)
    Call SP("CICC1[0,5]", ROTA_PRF(1), False)
    Call SP("CICC1[0,6]", Format$(PASS_RET(1), "#####"), False)
    Call SP("CICC1[0,7]", frmt(INCR_RET(1), 4), False)
    Call SP("CICC1[0,8]", Format$(VELO_ASS(1), "##########"), False)
    'Call SP("CICC1[0,9]", frmt(INCR_RET(1), 4), False)
    'Call SP("CICC1[0,10]", Format$(VELO_ASS(1), "##########"), False)
    Call SP("CICC1[0,11]", "0", False)
    Call SP("CICC1[0,12]", Format$(VELO_MOL(1), "##########"), False)
    Call SP("CICC1[0,13]", Format$(VELO_PRF, "##########"), False)
    'PREFINITURA
    Call SP("STR[52]", "R", False)
    Call SP("CICC1[0,21]", Format$(RIPE_RET(2), "#####"), False)
    Call SP("CICC1[0,22]", Format$(PASS_PRF(2), "#####"), False)
    Call SP("CICC1[0,23]", frmt(INCR_PRF(2), 4), False)
    Call SP("CICC1[0,24]", Format$(FEED_PRF(2), "#####"), False)
    Call SP("CICC1[0,25]", ROTA_PRF(2), False)
    Call SP("CICC1[0,26]", Format$(PASS_RET(2), "#####"), False)
    Call SP("CICC1[0,27]", frmt(INCR_RET(2), 4), False)
    Call SP("CICC1[0,28]", Format$(VELO_ASS(2), "##########"), False)
    'Call SP("CICC1[0,29]", "0", False)
    'Call SP("CICC1[0,30]", Format$(VELO_ASS(2), "##########"), False)
    Call SP("CICC1[0,31]", "0", False)
    Call SP("CICC1[0,32]", Format$(VELO_MOL(2), "##########"), False)
    Call SP("CICC1[0,33]", Format$(VELO_PRF, "##########"), False)
    'FINITURA
    Call SP("STR[53]", "R", False)
    Call SP("CICC1[0,41]", Format$(RIPE_RET(3), "#####"), False)
    Call SP("CICC1[0,42]", Format$(PASS_PRF(3), "#####"), False)
    Call SP("CICC1[0,43]", frmt(INCR_PRF(3), 4), False)
    Call SP("CICC1[0,44]", Format$(FEED_PRF(3), "#####"), False)
    Call SP("CICC1[0,45]", ROTA_PRF(3), False)
    Call SP("CICC1[0,46]", Format$(PASS_RET(3), "#####"), False)
    Call SP("CICC1[0,47]", frmt(INCR_RET(3), 4), False)
    Call SP("CICC1[0,48]", Format$(VELO_ASS(3), "##########"), False)
    'Call SP("CICC1[0,49]", "0", False)
    'Call SP("CICC1[0,50]", Format$(VELO_ASS(3), "##########"), False)
    Call SP("CICC1[0,51]", "0", False)
    Call SP("CICC1[0,52]", Format$(VELO_MOL(3), "##########"), False)
    Call SP("CICC1[0,53]", Format$(VELO_PRF, "##########"), False)
    'NULLO
    Call SP("STR[54]", "N", False)
    Call SP("CICC1[0,61]", "0", False)
    Call SP("CICC1[0,62]", "0", False)
    Call SP("CICC1[0,63]", "0", False)
    Call SP("CICC1[0,64]", "0", False)
    Call SP("CICC1[0,65]", "0", False)
    Call SP("CICC1[0,66]", "0", False)
    Call SP("CICC1[0,67]", "0", False)
    Call SP("CICC1[0,68]", "0", False)
    'Call SP("CICC1[0,69]", "0", False)
    'Call SP("CICC1[0,70]", "0", False)
    Call SP("CICC1[0,71]", "0", False)
    Call SP("CICC1[0,72]", "0", False)
    Call SP("CICC1[0,73]", "0", False)
    
    Call CALCOLO_SEQUENZA_CICLI_CONICHE(PC2, PZ3_B, DE_VIT, TipoMola, RIPE_RET, PASS_PRF, INCR_PRF, FEED_PRF, ROTA_PRF, PASS_RET, INCR_RET, VELO_ASS, VELO_MOL, SVRM_CYC)
    'CALCOLO I PARAMETRI PER IL PRIMO CICLO
    RIPE_RET(1) = INTERO(SVRM_CYC(1) / INCR_RET(1) / PASS_RET(1), "MAX")
    INCR_RET(1) = SVRM_CYC(1) / RIPE_RET(1) / PASS_RET(1)
    INCR_RET(1) = Int(INCR_RET(1) * 10000) / 10000
    SCARTO(1) = SVRM_CYC(1) - RIPE_RET(1) * PASS_RET(1) * INCR_RET(1)
    'SCARICO LO SCARTO NEL SECONDO CICLO
    SVRM_CYC(2) = SVRM_CYC(2) + SCARTO(1)
    INCR_RET(2) = SVRM_CYC(2) / PASS_RET(2)
    'CALCOLO VELOCITA'
    VELO_ASS(1) = INTERO(VELO_ASS(1) * Sqr((PG * DE_VIT) ^ 2 + PC2 ^ 2), "MIN")
    VELO_ASS(2) = INTERO(VELO_ASS(2) * Sqr((PG * DE_VIT) ^ 2 + PC2 ^ 2), "MIN")
    VELO_ASS(3) = INTERO(VELO_ASS(3) * Sqr((PG * DE_VIT) ^ 2 + PC2 ^ 2), "MIN")
    'SGROSSATURA
    Call SP("STR[61]", "R", False)
    Call SP("CICC2[0,1]", Format$(RIPE_RET(1), "#####"), False)
    Call SP("CICC2[0,2]", Format$(PASS_PRF(1), "#####"), False)
    Call SP("CICC2[0,3]", frmt(INCR_PRF(1), 4), False)
    Call SP("CICC2[0,4]", Format$(FEED_PRF(1), "#####"), False)
    Call SP("CICC2[0,5]", ROTA_PRF(1), False)
    Call SP("CICC2[0,6]", Format$(PASS_RET(1), "#####"), False)
    Call SP("CICC2[0,7]", frmt(INCR_RET(1), 4), False)
    Call SP("CICC2[0,8]", Format$(VELO_ASS(1), "##########"), False)
    'Call SP("CICC2[0,9]", frmt(INCR_RET(1), 4), False)
    'Call SP("CICC2[0,10]", Format$(VELO_ASS(1), "##########"), False)
    Call SP("CICC2[0,11]", "0", False)
    Call SP("CICC2[0,12]", Format$(VELO_MOL(1), "##########"), False)
    Call SP("CICC2[0,13]", Format$(VELO_PRF, "##########"), False)
    'PREFINITURA
    Call SP("STR[62]", "R", False)
    Call SP("CICC2[0,21]", Format$(RIPE_RET(2), "#####"), False)
    Call SP("CICC2[0,22]", Format$(PASS_PRF(2), "#####"), False)
    Call SP("CICC2[0,23]", frmt(INCR_PRF(2), 4), False)
    Call SP("CICC2[0,24]", Format$(FEED_PRF(2), "#####"), False)
    Call SP("CICC2[0,25]", ROTA_PRF(2), False)
    Call SP("CICC2[0,26]", Format$(PASS_RET(2), "#####"), False)
    Call SP("CICC2[0,27]", frmt(INCR_RET(2), 4), False)
    Call SP("CICC2[0,28]", Format$(VELO_ASS(2), "##########"), False)
    'Call SP("CICC2[0,29]", "0", False)
    'Call SP("CICC2[0,30]", Format$(VELO_ASS(2), "##########"), False)
    Call SP("CICC2[0,31]", "0", False)
    Call SP("CICC2[0,32]", Format$(VELO_MOL(2), "##########"), False)
    Call SP("CICC2[0,33]", Format$(VELO_PRF, "##########"), False)
    'FINITURA
    Call SP("STR[63]", "R", False)
    Call SP("CICC2[0,41]", Format$(RIPE_RET(3), "#####"), False)
    Call SP("CICC2[0,42]", Format$(PASS_PRF(3), "#####"), False)
    Call SP("CICC2[0,43]", frmt(INCR_PRF(3), 4), False)
    Call SP("CICC2[0,44]", Format$(FEED_PRF(3), "#####"), False)
    Call SP("CICC2[0,45]", ROTA_PRF(3), False)
    Call SP("CICC2[0,46]", Format$(PASS_RET(3), "#####"), False)
    Call SP("CICC2[0,47]", frmt(INCR_RET(3), 4), False)
    Call SP("CICC2[0,48]", Format$(VELO_ASS(3), "##########"), False)
    'Call SP("CICC2[0,49]", "0", False)
    'Call SP("CICC2[0,50]", Format$(VELO_ASS(3), "##########"), False)
    Call SP("CICC2[0,51]", "0", False)
    Call SP("CICC2[0,52]", Format$(VELO_MOL(3), "##########"), False)
    Call SP("CICC2[0,53]", Format$(VELO_PRF, "##########"), False)
    'NULLO
    Call SP("STR[64]", "N", False)
    Call SP("CICC2[0,61]", "0", False)
    Call SP("CICC2[0,62]", "0", False)
    Call SP("CICC2[0,63]", "0", False)
    Call SP("CICC2[0,64]", "0", False)
    Call SP("CICC2[0,65]", "0", False)
    Call SP("CICC2[0,66]", "0", False)
    Call SP("CICC2[0,67]", "0", False)
    Call SP("CICC2[0,68]", "0", False)
    'Call SP("CICC2[0,69]", "0", False)
    'Call SP("CICC2[0,70]", "0", False)
    Call SP("CICC2[0,71]", "0", False)
    Call SP("CICC2[0,72]", "0", False)
    Call SP("CICC2[0,73]", "0", False)
  Else
    'CICLI FASE C1
    Call SP("STR[51]", "R", False)
    Call SP("CICC1[0,1]", "1", False)
    Call SP("CICC1[0,6]", "1", False)
    Call SP("CICC1[0,7]", frmt(PZ1_A, 4), False)
    'CICLI FASE C2
    Call SP("STR[61]", "R", False)
    Call SP("CICC2[0,1]", "1", False)
    Call SP("CICC2[0,6]", "1", False)
    Call SP("CICC2[0,7]", frmt(PZ3_B, 4), False)
  End If

  Call SP("DIAMEST[0,0]", frmt(DE_VIT, 4), False)
  Call SCRITTURA_CICLI_RETTIFICA_DIAMETRI

  Call AggiornaDB

  ret = WritePrivateProfileString("Configurazione", "CARICA_CALCOLO", "N", PathFILEINI)
  WRITE_DIALOG NomeVite & " EVALUATED "
  If (Len(sERRORE) > 0) Then WRITE_DIALOG NomeVite & sERRORE

'Dim ng    As Double
'Dim nr    As Integer
'Dim DP    As Double
'Dim PASSO As Double
'Dim fondo As Double
'Dim iRiga As Integer
'fondo = 6
'ng = BARRIERA / PC2
'nr = ng * (PB - PC2) / fondo
'nr = 2 * Int(nr / 2) + 2
'DP = (PB - PC2) / (nr - 1)
'Open g_chOemPATH & "\prova.TXT" For Output As #1
'PASSO = PC2: iRiga = 0
'While (PASSO < PB)
'  stmp = ""
'  stmp = stmp & Left$(Format$(iRiga + 1, "###0") & String(4, " "), 4) & ") "
'  stmp = stmp & "dC=" & frmt(-BARRIERA / PASSO * 360, 2) & " Z=" & frmt(BARRIERA, 2) & " andata passo=" & frmt(PASSO, 3)
'  Print #1, stmp
'  stmp = ""
'  stmp = stmp & String(4, " ") & "  "
'  stmp = stmp & "dC=" & frmt(BARRIERA * (1 / PASSO - 1 / (PASSO + DP)) * 360, 2) & " Z=" & frmt(0, 2) & " recupero"
'  Print #1, stmp
'  PASSO = PASSO + DP
'  stmp = ""
'  stmp = stmp & Left$(Format$(iRiga + 2, "###0") & String(4, " "), 4) & ") "
'  stmp = stmp & "dC=" & frmt(BARRIERA / PASSO * 360, 2) & " Z=" & frmt(-BARRIERA, 2) & " ritorno passo=" & frmt(PASSO, 3)
'  Print #1, stmp
'  PASSO = PASSO + DP
'  If (PASSO >= PB) Then
'    PASSO = PB
'  End If
'  iRiga = iRiga + 2
'Wend
'Close #1

Exit Sub

errCALCOLO_VITI_BARRIERA6:
  WRITE_DIALOG "Sub CALCOLO_VITI_BARRIERA6: Error -> " & Err
  sERRORE = ": STANDARD EVALUATE WITH ERROR " & str(Err)
Resume Next

CALCOLO_PROZ:
Dim PRO1 As Double
Dim PRO2 As Double

  If (ZZ <= 0) Then ZZ = 0
  If (ZZ >= SEZIONE_RVC) Then ZZ = SEZIONE_RVC
  
  PRO1 = (DE_VIT - DF_VIT) / 2
  PRO2 = (DE_VIT - DF_BAR) / 2
  PROZ = PRO1 - (ZZ / SEZIONE_RVC) * (PRO1 - PRO2)

Return

End Sub

Sub CICLI_VISUALIZZAZIONE_CONICHE(SiStampa As String, TIPO_LISTA As String)
'
Dim ftmp   As Double
Dim stmp   As String
Dim Atmp   As String * 255
Dim i      As Integer
Dim j      As Integer
Dim k      As Integer
Dim ret    As Integer
'
Dim RPC    As Integer
Dim PAS    As Integer
Dim INC    As Single
Dim ASP    As Single
'
Dim DB     As Database
Dim TB     As Recordset
'
Dim sFEED1 As String
Dim sFEED2 As String
Dim FEED1  As Single
Dim FEED2  As Single
Dim Pas_A  As Single
Dim Pas_R  As Single
Dim D_EXT  As Single
'
Dim CONSUMO          As Single
Dim sAsportazione    As String
Dim TabXl            As Single
Dim TabXt            As Single
Dim TipoCarattere    As String
Dim TIPO_LAVORAZIONE As String
Dim FINESTRA         As Object
Dim TITOLO           As String
Dim QwA              As Single
Dim QwR              As Single
Dim VRULLO           As Single
Dim VMOLA            As Single
Dim TextFont         As String
'
On Error GoTo errCICLI_VISUALIZZAZIONE_CONICHE
  '
    If (LINGUA = "CH") Then
    TextFont = "MS Song"
  ElseIf (LINGUA = "RU") Then
    TextFont = "Arial Cyr"
  Else
    TextFont = "Arial"
  End If
  'SELEZIONE DEL TIPO DI PERIFERICA
  If (SiStampa = "N") Then
    OEMX.PicCALCOLO.Cls
    OEMX.PicCALCOLO.FontName = TextFont
    OEMX.PicCALCOLO.FontSize = OEMX.PicCALCOLO.Height / 500
    Set FINESTRA = OEMX.PicCALCOLO
  Else
    Printer.FontName = TextFont
    Printer.FontSize = 12
    Set FINESTRA = Printer
  End If
  'SEGNALAZIONE UTENTE
  i = InStr(Tabella1.INTESTAZIONE(0), ";")
  TITOLO = Left$(Tabella1.INTESTAZIONE(0), i - 1)
  WRITE_DIALOG TITOLO
  'LEGGO IL TIPO DI LAVORAZIONE
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  'COLONNA DA VISUALIZZARE
  Dim INDICI()   As String
  Dim VV(4, 20)  As String
  Dim LISTA      As String
  Dim ii         As Integer
  Dim Nparametri As Integer
  If (TIPO_LISTA = "") Or (TIPO_LISTA = "A") Then
    LISTA = GetInfo(TIPO_LAVORAZIONE, "LISTA", Path_LAVORAZIONE_INI)
  Else
    LISTA = GetInfo(TIPO_LAVORAZIONE, "LISTA" & TIPO_LISTA, Path_LAVORAZIONE_INI)
  End If
  LISTA = Trim$(LISTA)
  If (Len(LISTA) > 0) Then
    INDICI = Split(LISTA, ";")
    Nparametri = UBound(INDICI)
    Set DB = OpenDatabase(PathDB, True, False)
    Set TB = DB.OpenRecordset(TIPO_LAVORAZIONE, dbOpenTable)
    For ii = 0 To Nparametri
      TB.Index = "INDICE"
      TB.Seek "=", CInt(INDICI(ii))
      VV(0, ii) = TB.Fields("NOME_" & LINGUA)
      VV(1, ii) = Split(TB.Fields("VARIABILE"), ";")(0): VV(1, ii) = LEP_STR(VV(1, ii))
      VV(2, ii) = Split(TB.Fields("VARIABILE"), ";")(1): VV(2, ii) = LEP_STR(VV(2, ii))
      VV(3, ii) = Split(TB.Fields("VARIABILE"), ";")(2): VV(3, ii) = LEP_STR(VV(3, ii))
      VV(4, ii) = Split(TB.Fields("VARIABILE"), ";")(3): VV(4, ii) = LEP_STR(VV(4, ii))
    Next ii
    TB.Close
    DB.Close
  Else
    WRITE_DIALOG "ERROR: CYCLE LIST HAVE NOT BEEN DEFINED!!!"
    Exit Sub
  End If
  stmp = GetInfo("MOLA_VITI_CONICHE", "PicW", Path_LAVORAZIONE_INI)
  If (val(stmp) > 0) Then
    FINESTRA.ScaleWidth = val(stmp)
  Else
    FINESTRA.ScaleWidth = 190
  End If
  stmp = GetInfo("MOLA_VITI_CONICHE", "PicH", Path_LAVORAZIONE_INI)
  If (val(stmp) > 0) Then
    FINESTRA.ScaleHeight = val(stmp)
  Else
    FINESTRA.ScaleHeight = 95
  End If
  Select Case TIPO_LAVORAZIONE
    Case "VITI_CONICHE"
      Pas_A = Abs(Lp("R[128]")) 'PASSO
      Pas_R = Abs(Lp("R[128]")) 'PASSO
      D_EXT = Abs(Lp("R[189]")) 'DIAMETRO ESTERNO
      sFEED1 = VV(0, 8)
      sFEED2 = VV(0, 10)
    Case "VITI_PASSO_VARIABILE"
      Pas_A = Abs(Lp("R[214]")) 'PASSO
      Pas_R = Abs(Lp("R[215]")) 'PASSO
      D_EXT = Abs(Lp("R[189]")) 'DIAMETRO ESTERNO
      sFEED1 = VV(0, 8)
      sFEED2 = VV(0, 10)
'    Case "VITI_BARRIERA1L", "VITI_PASSO_VARIABILE", "VITI_BARRIERA2L", "VITI_BARRIERA2S", "VITI_BARRIERA3L"
'    Case "VITI_BARRIERA4L"
'      Set TB = DB.OpenRecordset("OEM1_LAVORO46")
'      TB.Index = "INDICE"
'      TB.Seek "=", 2
'      PASSO = val(Trim$(TB.Fields("ACTUAL_1").Value)) 'PASSO
'      TB.Seek "=", 3
'      D_EXT = val(Trim$(TB.Fields("ACTUAL_1").Value)) 'DIAMETRO ESTERNO
'      TB.Close
'      sFEED1 = OEMX.List1(i).List(7)
'      sFEED2 = OEMX.List1(i).List(9)
'    Case "VITI_DBG", "VITI_BARRIERA1S", "VITI_BARRIERA3S"
'    Case "VITI_BARRIERA4S"
'      Set TB = DB.OpenRecordset("OEM1_LAVORO47")
'      TB.Index = "INDICE"
'      TB.Seek "=", 1
'      PASSO = val(Trim$(TB.Fields("ACTUAL_1").Value)) 'PASSO
'      TB.Seek "=", 2
'      D_EXT = val(Trim$(TB.Fields("ACTUAL_1").Value)) 'DIAMETRO ESTERNO
'      TB.Close
'      sFEED1 = OEMX.List1(i).List(7)
'      sFEED2 = OEMX.List1(i).List(8)
    Case "BS5L"
      VMOLA = Lp("R[204]")
      Pas_A = Abs(Lp("R[128]")) 'PASSO
      Pas_R = Abs(Lp("R[132]")) 'PASSO
      D_EXT = Abs(Lp("R[189]")) 'DIAMETRO ESTERNO
      sFEED1 = VV(0, 8)
      sFEED2 = VV(0, 10)
    Case "BS5S"
      VMOLA = Lp("R[204]")
      Pas_A = Abs(Lp("R[132]")) 'PASSO
      Pas_R = 0
      D_EXT = Abs(Lp("R[189]")) 'DIAMETRO ESTERNO
      sFEED1 = VV(0, 8)
      sFEED2 = VV(0, 10)
    Case "VITI_BARRIERA3"
      Select Case TIPO_LISTA
      Case "E", "F"
          Pas_A = Abs(Lp("MO3R[0,128]"))
          Pas_R = Abs(Lp("MO3R[0,128]"))
          D_EXT = Abs(Lp("MO3R[0,189]"))
      Case "C", "D"
          Pas_A = Abs(Lp("MO2R[0,128]"))
          Pas_R = Abs(Lp("MO2R[0,128]"))
          D_EXT = Abs(Lp("MO2R[0,189]"))
      Case Else
          Pas_A = Abs(Lp("MO1R[0,128]"))
          Pas_R = Abs(Lp("MO1R[0,128]"))
          D_EXT = Abs(Lp("MO1R[0,189]"))
      End Select
      sFEED1 = VV(0, 8)
      sFEED2 = VV(0, 10)
    Case "VITI_BARRIERA6"
      D_EXT = Abs(Lp("MO1R[0,189]"))
      Select Case TIPO_LISTA
      Case "E", "F"
          Pas_A = Abs(Lp("MO3R[0,128]"))
          Pas_R = Abs(Lp("MO3R[0,128]"))
      Case "C", "D"
          Pas_A = Abs(Lp("MO2R[0,128]"))
          Pas_R = Abs(Lp("MO2R[0,128]"))
      Case Else
          Pas_A = Abs(Lp("MO1R[0,214]"))
          Pas_R = Abs(Lp("MO1R[0,215]"))
      End Select
      sFEED1 = VV(0, 8)
      sFEED2 = VV(0, 10)
  End Select
  TabXt = 0
  CONSUMO = 0
  '
  i = 0
    '
    TabXl = 0
      'INTESTAZIONE DEI CICLI
      stmp = " " & TITOLO & " "
      FINESTRA.CurrentX = TabXt
      FINESTRA.CurrentY = 0 '0.5 * FINESTRA.TextHeight(stmp)
      FINESTRA.Print stmp
      If (TabXl < FINESTRA.TextWidth(stmp)) Then TabXl = FINESTRA.TextWidth(stmp)
      'CAMPI DEI CICLI
      For j = 1 To Nparametri
        stmp = " " & VV(0, j) & " "
        FINESTRA.CurrentX = TabXt
        FINESTRA.CurrentY = 0 + (j + 0) * FINESTRA.TextHeight(stmp)
        FINESTRA.Print stmp
        If (TabXl < FINESTRA.TextWidth(stmp)) Then TabXl = FINESTRA.TextWidth(stmp)
      Next j
      'VISUALIZZO L'ASPORTAZIONE SULLA FINESTRA
      ret = LoadString(g_hLanguageLibHandle, 1011, Atmp, 255)
      If (ret > 0) Then sAsportazione = Left$(Atmp, ret)
      stmp = " " & sAsportazione
      j = Nparametri
      FINESTRA.CurrentX = TabXt
      FINESTRA.CurrentY = (j + 1) * FINESTRA.TextHeight(stmp)
      FINESTRA.Print stmp
      'VISUALIZZO GIRI PEZZO ANDATA
      If (Pas_A <> 0) And (D_EXT <> 0) Then
        stmp = " " & sFEED1 & " (RPM) "
        FINESTRA.CurrentX = TabXt
        FINESTRA.CurrentY = (j + 2) * FINESTRA.TextHeight(stmp)
        FINESTRA.Print stmp
        If (TabXl < FINESTRA.TextWidth(stmp)) Then TabXl = FINESTRA.TextWidth(stmp)
      End If
      'VISUALIZZO GIRI PEZZO RITORNO
      If (Pas_R <> 0) And (D_EXT <> 0) Then
        stmp = " " & sFEED2 & " (RPM) "
        FINESTRA.CurrentX = TabXt
        FINESTRA.CurrentY = (j + 3) * FINESTRA.TextHeight(stmp)
        FINESTRA.Print stmp
        If (TabXl < FINESTRA.TextWidth(stmp)) Then TabXl = FINESTRA.TextWidth(stmp)
      End If
     'VISUALIZZO ASPORTAZIONI SPECIFICHE
      stmp = " Qwa "
      FINESTRA.CurrentX = TabXt
      FINESTRA.CurrentY = (j + 4) * FINESTRA.TextHeight(stmp)
      FINESTRA.Print stmp
      If (TabXl < FINESTRA.TextWidth(stmp)) Then TabXl = FINESTRA.TextWidth(stmp)
      stmp = " Qwr "
      FINESTRA.CurrentX = TabXt
      FINESTRA.CurrentY = (j + 5) * FINESTRA.TextHeight(stmp)
      FINESTRA.Print stmp
      If (TabXl < FINESTRA.TextWidth(stmp)) Then TabXl = FINESTRA.TextWidth(stmp)
      'VISUALIZZO AVANZAMENTO ANDATA
      If (Pas_A <> 0) And (D_EXT <> 0) Then
        stmp = " " & sFEED1 & " (mm/min) "
        FINESTRA.CurrentX = TabXt
        FINESTRA.CurrentY = (j + 6) * FINESTRA.TextHeight(stmp)
        FINESTRA.Print stmp
        If (TabXl < FINESTRA.TextWidth(stmp)) Then TabXl = FINESTRA.TextWidth(stmp)
      End If
      'VISUALIZZO AVANZAMENTO RITORNO
      If (Pas_R <> 0) And (D_EXT <> 0) Then
        stmp = " " & sFEED2 & " (mm/min) "
        FINESTRA.CurrentX = TabXt
        FINESTRA.CurrentY = (j + 7) * FINESTRA.TextHeight(stmp)
        FINESTRA.Print stmp
        If (TabXl < FINESTRA.TextWidth(stmp)) Then TabXl = FINESTRA.TextWidth(stmp)
      End If
      'VISUALIZZO RAPPORTO MOLA/RULLO
        stmp = " qd=Vr/Vm "
        FINESTRA.CurrentX = TabXt
        FINESTRA.CurrentY = (j + 8) * FINESTRA.TextHeight(stmp)
        FINESTRA.Print stmp
        If (TabXl < FINESTRA.TextWidth(stmp)) Then TabXl = FINESTRA.TextWidth(stmp)
    TabXt = TabXt + TabXl
    'LINEE
    FINESTRA.Line (TabXt, 0)-(TabXt, (j + 9) * FINESTRA.TextHeight(stmp))
  '
  For i = 1 To 4
    TabXl = 0
    'INTESTAZIONE DEI CICLI
    stmp = "  " & "C" & Format$(i, "#0") & " "
    FINESTRA.CurrentX = TabXt
    FINESTRA.CurrentY = 0 '0.5 * FINESTRA.TextHeight(stmp)
    FINESTRA.Print stmp
    If (TabXl < FINESTRA.TextWidth(stmp)) Then TabXl = FINESTRA.TextWidth(stmp)
    'CAMPI DEI CICLI
    For j = 1 To Nparametri
      If (VV(i, 0) <> "N") Then
        stmp = "  " & VV(i, j) & "  "
        FINESTRA.CurrentX = TabXt
        FINESTRA.CurrentY = 0 + (j + 0) * FINESTRA.TextHeight(stmp)
        FINESTRA.Print stmp
        If (TabXl < FINESTRA.TextWidth(stmp)) Then TabXl = FINESTRA.TextWidth(stmp)
      Else
        FINESTRA.Print ""
      End If
    Next j
    'INCREMENTI
    If (VV(i, 0) <> "N") Then
      RPC = val(VV(i, 1))
      PAS = val(VV(i, 6))
      Select Case TIPO_LAVORAZIONE
        Case "VITI_CONICHE", "VITI_BARRIERA1L", "VITI_PASSO_VARIABILE", "VITI_BARRIERA2L", "VITI_BARRIERA2S", "VITI_BARRIERA3L", "VITI_BARRIERA4L", "BS5L"
          INC = val(VV(i, 7)) + val(VV(i, 9))
          FEED1 = val(VV(i, 8)):  FEED2 = val(VV(i, 10))
          QwA = val(VV(i, 7)) * FEED1 / 60: QwR = val(VV(i, 9)) * FEED2 / 60
        Case "VITI_DBG", "VITI_BARRIERA1S", "VITI_BARRIERA3S", "VITI_BARRIERA4S"
          INC = val(VV(i, 7))
          FEED1 = val(VV(i, 8)):  FEED2 = val(VV(i, 9))
          QwA = val(VV(i, 7)) * FEED1 / 60: QwR = 0 * FEED2 / 60
        Case "BS5S"
          INC = val(VV(i, 7))
          FEED1 = val(VV(i, 8))
          QwA = val(VV(i, 7)) * FEED1 / 60: QwR = QwA
        Case "VITI_BARRIERA3"
          Select Case TIPO_LISTA
          Case "E", "F"
          INC = val(VV(i, 7)) + val(VV(i, 9))
          FEED1 = val(VV(i, 8)):  FEED2 = val(VV(i, 10))
          QwA = val(VV(i, 7)) * FEED1 / 60: QwR = val(VV(i, 9)) * FEED2 / 60
          Case "C", "D"
          INC = val(VV(i, 7))
          FEED1 = val(VV(i, 8)):  FEED2 = val(VV(i, 10))
          QwA = val(VV(i, 7)) * FEED1 / 60: QwR = 0 * FEED2 / 60
          Case Else
          INC = val(VV(i, 7)) + val(VV(i, 9))
          FEED1 = val(VV(i, 8)):  FEED2 = val(VV(i, 10))
          QwA = val(VV(i, 7)) * FEED1 / 60: QwR = val(VV(i, 9)) * FEED2 / 60
          End Select
        Case "VITI_BARRIERA6"
          Select Case TIPO_LISTA
          Case "E", "F"
          INC = val(VV(i, 7))
          FEED1 = val(VV(i, 8)):  FEED2 = val(VV(i, 10))
          QwA = val(VV(i, 7)) * FEED1 / 60: QwR = val(VV(i, 9)) * FEED2 / 60
          Case "C", "D"
          INC = val(VV(i, 7)) + val(VV(i, 9))
          FEED1 = val(VV(i, 8)):  FEED2 = val(VV(i, 10))
          QwA = val(VV(i, 7)) * FEED1 / 60: QwR = 0 * FEED2 / 60
          Case Else
          INC = val(VV(i, 7)) + val(VV(i, 9))
          FEED1 = val(VV(i, 8)):  FEED2 = val(VV(i, 10))
          QwA = val(VV(i, 7)) * FEED1 / 60: QwR = val(VV(i, 9)) * FEED2 / 60
          End Select
      End Select
      'CALCOLO DELL'ASPORTAZIONE TOTALE DEI CICLI
      ASP = ASP + RPC * PAS * INC
      'CONSUMO MOLA
      CONSUMO = CONSUMO + RPC * val(VV(i, 2)) * val(VV(i, 3))
      stmp = " " & frmt(RPC * PAS * INC, 4) & " "
    Else
      'stmp = " 0 "
      stmp = "  "
    End If
    'VISUALIZZO L'ASPORTAZIONE SULLA FINESTRA
    j = Nparametri
    FINESTRA.CurrentX = TabXt
    FINESTRA.CurrentY = (j + 1) * FINESTRA.TextHeight(stmp)
    FINESTRA.Print stmp
    'CONVERSIONE VT->FEED
    'GP=FEED/SQRT(360^2+PASSO^2)
    'VT=GP*SQRT((Pi*Dext)^2+PASSO^2)
    'GP=VT/SQRT((Pi*Dext)^2+PASSO^2)
    'VISUALIZZO GIRI PEZZO ANDATA
    If (VV(i, 0) <> "N") Then
      If (Pas_A <> 0) And (D_EXT <> 0) Then
        stmp = " " & Format$(FEED1 / Sqr((PG * D_EXT) ^ 2 + Pas_A ^ 2), "########0")
      Else
        stmp = " 0 "
      End If
    Else
      'stmp = " 0 "
      stmp = "  "
    End If
    FINESTRA.CurrentX = TabXt
    FINESTRA.CurrentY = (j + 2) * FINESTRA.TextHeight(stmp)
    FINESTRA.Print stmp
    'VISUALIZZO GIRI PEZZO RITORNO
    If (VV(i, 0) <> "N") Then
      If (Pas_R <> 0) And (D_EXT <> 0) Then
        stmp = " " & Format$(FEED2 / Sqr((PG * D_EXT) ^ 2 + Pas_R ^ 2), "########0")
      Else
        stmp = " 0 "
      End If
    Else
      'stmp = " 0 "
      stmp = "   "
    End If
    FINESTRA.CurrentX = TabXt
    FINESTRA.CurrentY = (j + 3) * FINESTRA.TextHeight(stmp)
    FINESTRA.Print stmp
    'VISUALIZZO ASPORTAZIONI SPECIFICHE
    If (VV(i, 0) <> "N") Then
      If (QwA <> 0) Then
        stmp = " " & frmt(QwA, 1)
      Else
        stmp = " 0 "
      End If
    Else
      'stmp = " 0 "
      stmp = "   "
    End If
    FINESTRA.CurrentX = TabXt
    FINESTRA.CurrentY = (j + 4) * FINESTRA.TextHeight(stmp)
    FINESTRA.Print stmp
    If (VV(i, 0) <> "N") Then
      If (QwR <> 0) Then
        stmp = " " & frmt(QwR, 1)
      Else
        stmp = " 0 "
      End If
    Else
      'stmp = " 0 "
      stmp = "   "
    End If
    FINESTRA.CurrentX = TabXt
    FINESTRA.CurrentY = (j + 5) * FINESTRA.TextHeight(stmp)
    FINESTRA.Print stmp
    'VISUALIZZO AVANZAMENTO ANDATA
    If (VV(i, 0) <> "N") Then
      If (Pas_A <> 0) And (D_EXT <> 0) Then
        stmp = " " & Format$(FEED1 * Sqr((360 ^ 2 + Pas_A ^ 2) / ((PG * D_EXT) ^ 2 + Pas_A ^ 2)), "########0")
      Else
        stmp = " 0 "
      End If
    Else
      'stmp = " 0 "
      stmp = "   "
    End If
    FINESTRA.CurrentX = TabXt
    FINESTRA.CurrentY = (j + 6) * FINESTRA.TextHeight(stmp)
    FINESTRA.Print stmp
    'VISUALIZZO AVANZAMENTO RITORNO
    If (VV(i, 0) <> "N") Then
      If (Pas_R <> 0) And (D_EXT <> 0) Then
        stmp = " " & Format$(FEED2 * Sqr((360 ^ 2 + Pas_R ^ 2) / ((PG * D_EXT) ^ 2 + Pas_R ^ 2)), "########0")
      Else
        stmp = " 0 "
      End If
    Else
      'stmp = " 0 "
      stmp = "   "
    End If
    FINESTRA.CurrentX = TabXt
    FINESTRA.CurrentY = (j + 7) * FINESTRA.TextHeight(stmp)
    FINESTRA.Print stmp
    'RAPPORTO MOLA E RULLO
    VRULLO = val(VV(i, 5)): VMOLA = val(VV(i, 12))
    If (VMOLA <> 0) Then
      stmp = " " & frmt(VRULLO / VMOLA, 2)
      FINESTRA.CurrentX = TabXt
      FINESTRA.CurrentY = (j + 8) * FINESTRA.TextHeight(stmp)
      FINESTRA.Print stmp
    End If
    'INCREMENTO TABULAZIONE LATERALE
    TabXt = TabXt + TabXl
    'LINEE
    FINESTRA.Line (TabXt, 0)-(TabXt, (j + 9) * FINESTRA.TextHeight(stmp))
    FINESTRA.Line (0, (j + 9) * FINESTRA.TextHeight(stmp))-(TabXt, (j + 9) * FINESTRA.TextHeight(stmp))
  Next i
  'TOTALE DEI CICLI
  j = Nparametri
  stmp = " --> " & frmt(ASP, 4) & "mm "
  FINESTRA.CurrentX = TabXt
  FINESTRA.CurrentY = (j + 1) * FINESTRA.TextHeight(stmp)
  FINESTRA.Print stmp
  'LINEE ORIZZONTALI
  FINESTRA.Line (0, 1 * FINESTRA.TextHeight(stmp))-(TabXt, 1 * FINESTRA.TextHeight(stmp))
  FINESTRA.Line (0, (j + 1) * FINESTRA.TextHeight(stmp))-(TabXt, (j + 1) * FINESTRA.TextHeight(stmp))
  'VELOCITA' MOLA PROFILATURA
  'j = 0
  'stmp = " -->  Vm: " & frmt(VMOLA, 2) & "m/s"
  'FINESTRA.CurrentX = TabXt
  'FINESTRA.CurrentY = (j + 0) * FINESTRA.TextHeight(stmp)
  'FINESTRA.Print stmp
  'CONSUMO MOLA
  j = 3
  stmp = " --> " & frmt(CONSUMO, 4) & "mm "
  FINESTRA.CurrentX = TabXt
  FINESTRA.CurrentY = (j + 0) * FINESTRA.TextHeight(stmp)
  FINESTRA.Print stmp
  stmp = " " & TIPO_LISTA & " "
  FINESTRA.CurrentX = FINESTRA.ScaleWidth - FINESTRA.TextWidth(stmp)
  FINESTRA.CurrentY = FINESTRA.ScaleHeight - FINESTRA.TextHeight(stmp)
  FINESTRA.Print stmp
  'VISUALIZZO L'ASPORTAZIONE IN BASSO A SINSTRA DELLO SCHERMO
  WRITE_DIALOG sAsportazione & " --> " & frmt(ASP, 4) & " " & VV(0, 3) & " --> " & frmt(CONSUMO, 4)
  '
Exit Sub

errCICLI_VISUALIZZAZIONE_CONICHE:
  WRITE_DIALOG "Sub CICLI_VISUALIZZAZIONE_CONICHE: ERROR -> " & Err
Resume Next

End Sub

Sub SPLITTA_F2(DDL As Double, DDH As Double, RR252 As Double, RR253 As Double, RR254 As Double, PASSO As Double)
'
Dim ii        As Integer
Dim QUOTA_DDL As Double
Dim QUOTA_DDH As Double
'
'Input: DDL
'       DDH
'       RR252=Pezzo   int: DX - / SX +
'       RR253=Radiale int:
'       RR254=Assiale int: DX + / SX +
Dim RR255 As Integer
'Kost.  RR255=5 (zone)
'
If (Abs(RR252) < 180) Then
   RR255 = 3
Else
   If (Abs(RR252) > 360) Then
     RR255 = 5
   Else
     RR255 = 4
   End If
End If
R19 = R17 * R21
'VARIAZIONE DI SPESSORE
R256 = RR254 - Abs(RR252 / 360 * PASSO)
Open g_chOemPATH & "\CALCOLO_MOLA_VITI_CONICHE.TXT" For Append As #1
   Print #1, ""
   Print #1, "F2: AX[Pezzo]=" & frmt(RR252, 4) & " AX[Radiale]=" & frmt(RR253, 4) & " AX[Assiale]=" & frmt(RR254, 4) & " PASSO=" & frmt(PASSO, 4)
If (Abs(RR252) < 40) Or (Abs(R256) < 0.02) Then
   'Print #1, "F2: AX[Pezzo]=" & frmt(RR252, 4) & " AX[Radiale]=" & frmt(RR253, 4) & " AX[Assiale]=" & frmt(RR254, 4)
Else
   R261 = R54 - R53 - R45 - DDL
   If (R261 < YY(0, 15)) Then
     R350 = XX(0, 42) - Sqr(POT(RR(0, 11)) - POT(R261 - YY(0, 42)))
   Else
     R350 = XX(0, 43) - Sqr(R59 * R59 - POT(R261 - YY(0, 43)))
   End If
   QUOTA_DDL = Abs(R350)
   R261 = R54 - R53 - R45 - DDH
   If (R261 < YY(0, 15)) Then
     R350 = XX(0, 42) - Sqr(POT(RR(0, 11)) - POT(R261 - YY(0, 42)))
   Else
     R350 = XX(0, 43) - Sqr(R59 * R59 - POT(R261 - YY(0, 43)))
   End If
   QUOTA_DDH = Abs(R350)
   R257 = R256 - R19 * (QUOTA_DDH - QUOTA_DDL) * R39
   '
   R272 = RR252 / RR255
   R273 = RR253 / RR255
     R269 = QUOTA_DDL
     R268 = DDH
     R267 = DDL
   If (R19 <> -1) Then
     R269 = QUOTA_DDH
     R268 = DDL
     R267 = DDH
   End If
   ii = 1
   Do While (ii <= RR255)
     R261 = R54 - R53 - R45 - (R267 + ii / RR255 * (R268 - R267))
     If (R261 < YY(0, 15)) Then
       R350 = XX(0, 42) - Sqr(POT(RR(0, 11)) - POT(R261 - YY(0, 42)))
     Else
       R350 = XX(0, 43) - Sqr(R59 * R59 - POT(R261 - YY(0, 43)))
     End If
     R350 = Abs(R350)
     R274 = R272 * PASSO / 360 * R12 + R19 * Abs(R350 - R269) * R39 + R257 / RR255
     Print #1, "F2: AX[Pezzo]="; frmt(R272, 4) & " AX[Radiale]=" & frmt(R273, 4) & " AX[Assiale]=" & frmt(R274, 4)
     R269 = R350
     ii = ii + 1
   Loop
End If
Close #1
'
End Sub

Sub SPLITTA_F1(DDL As Double, DDH As Double, RR252 As Double, RR253 As Double, RR254 As Double, PASSO As Double)
'
Dim ii        As Integer
Dim QUOTA_DDL As Double
Dim QUOTA_DDH As Double
'
'Input: DDL
'       DDH
'       RR252=Pezzo   int: DX + / SX -
'       RR253=Radiale int:
'       RR254=Assiale int: DX - / SX -
Dim RR255 As Integer
'Kost.  RR255=5 (zone)
'
If (Abs(RR252) < 180) Then
   RR255 = 3
Else
   If (Abs(RR252) > 360) Then
     RR255 = 5
   Else
     RR255 = 4
   End If
End If
R19 = R17 * R21
'VARIAZIONE DI SPESSORE
R256 = RR254 + Abs(RR252 / 360 * PASSO)
Open g_chOemPATH & "\CALCOLO_MOLA_VITI_CONICHE.TXT" For Append As #1
   Print #1, ""
   Print #1, "F1: AX[Pezzo]=" & frmt(RR252, 4) & " AX[Radiale]=" & frmt(RR253, 4) & " AX[Assiale]=" & frmt(RR254, 4) & " PASSO=" & frmt(PASSO, 4)
If (Abs(RR252) < 40) Or (Abs(R256) < 0.02) Then
   'Print #1, "F1: AX[Pezzo]=" & frmt(RR252, 4) & " AX[Radiale]=" & frmt(RR253, 4) & " AX[Assiale]=" & frmt(RR254, 4)
Else
   R261 = R44 - R43 - R45 - DDL
   If (R261 < YY(0, 5)) Then
     R350 = XX(0, 32) + Sqr(POT(RR(0, 1)) - POT(R261 - YY(0, 32)))
   Else
     R350 = XX(0, 33) + Sqr(R49 * R49 - POT(R261 - YY(0, 33)))
   End If
   QUOTA_DDL = Abs(R350)
   R261 = R44 - R43 - R45 - DDH
   If (R261 < YY(0, 5)) Then
     R350 = XX(0, 32) + Sqr(POT(RR(0, 1)) - POT(R261 - YY(0, 32)))
   Else
     R350 = XX(0, 33) + Sqr(R49 * R49 - POT(R261 - YY(0, 33)))
   End If
   QUOTA_DDH = Abs(R350)
   R257 = R256 - R19 * (QUOTA_DDH - QUOTA_DDL) * R39
   '
   R272 = RR252 / RR255
   R273 = RR253 / RR255
     R269 = QUOTA_DDL
     R268 = DDH
     R267 = DDL
   If (R19 <> 1) Then
     R269 = QUOTA_DDH
     R268 = DDL
     R267 = DDH
   End If
   ii = 1
   Do While (ii <= RR255)
     R261 = R44 - R43 - R45 - (R267 + ii / RR255 * (R268 - R267))
     If (R261 < YY(0, 5)) Then
       R350 = XX(0, 32) + Sqr(POT(RR(0, 1)) - POT(R261 - YY(0, 32)))
     Else
       R350 = XX(0, 33) + Sqr(R49 * R49 - POT(R261 - YY(0, 33)))
     End If
     R350 = Abs(R350)
     R274 = R272 * PASSO / 360 * R12 + R19 * Abs(R350 - R269) * R39 + R257 / RR255
     Print #1, "F1: AX[Pezzo]=" & frmt(R272, 4) & " AX[Radiale]=" & frmt(R273, 4) & " AX[Assiale]=" & frmt(R274, 4)
     R269 = R350
     ii = ii + 1
   Loop
End If
Close #1
'
End Sub

Sub CALCOLO_DSW(DDL As Double, DDH As Double, Fianco As Integer, DSW As Double)
'
Dim QUOTA_DDL As Double
Dim QUOTA_DDH As Double
'
If (Fianco = 2) Then
   R260 = DDL: R261 = 0.15 * R59
   If (R260 <= R261) Then
     R260 = DDL + 0.1
   End If
   R261 = R54 - R53 - R45 - R260
   If (R261 < YY(0, 15)) Then
     QUOTA_DDL = XX(0, 42) - Sqr(POT(RR(0, 11)) - POT(R261 - YY(0, 42)))
   Else
     QUOTA_DDL = XX(0, 43) - Sqr(R59 * R59 - POT(R261 - YY(0, 43)))
   End If
   R260 = R54 - R53 - R45 - DDH
   If (R260 < YY(0, 15)) Then
     QUOTA_DDH = XX(0, 42) - Sqr(POT(RR(0, 11)) - POT(R260 - YY(0, 42)))
   Else
     QUOTA_DDH = XX(0, 43) - Sqr(R59 * R59 - POT(R260 - YY(0, 43)))
   End If
Else
If (Fianco = 1) Then
   R260 = DDL: R261 = 0.15 * R49
   If (R260 <= R261) Then
     R260 = DDL + 0.1
   End If
   R261 = R44 - R43 - R45 - R260
   If (R261 < YY(0, 5)) Then
     QUOTA_DDL = XX(0, 32) + Sqr(POT(RR(0, 1)) - POT(R261 - YY(0, 32)))
   Else
     QUOTA_DDL = XX(0, 33) + Sqr(R49 * R49 - POT(R261 - YY(0, 33)))
   End If
   R260 = R44 - R43 - R45 - DDH
   If (R260 < YY(0, 5)) Then
     QUOTA_DDH = XX(0, 32) + Sqr(POT(RR(0, 1)) - POT(R260 - YY(0, 32)))
   Else
     QUOTA_DDH = XX(0, 33) + Sqr(R49 * R49 - POT(R260 - YY(0, 33)))
   End If
Else
StopRegieEvents
MsgBox "VALUE " & Fianco & " NOT POSSIBLE!!", vbInformation, "WARNING: WRONG FLANK INDEX"
ResumeRegieEvents
End If
End If
'DIFFERENZA DI LARGHEZZA CON SEGNO
DSW = Abs(QUOTA_DDH) - Abs(QUOTA_DDL)
'PROIEZIONE ASSIALE
DSW = DSW * R39
'
End Sub

Sub CARICA_DATI_MACCHINA_CORRENTI(ByVal TIPO_LAVORAZIONE As String, ByVal NomeTb As String, ByVal IMPORTAZIONE_LST As String)
'
Dim k        As Integer
Dim ii       As Integer
Dim ret      As Long
Dim stmp     As String
Dim ITEMDDE  As String
Dim Pagina   As Integer
Dim DB       As Database
Dim RS       As Recordset
Dim TB       As Recordset
Dim INDICI() As String
Dim VV()     As String
Dim VA()     As String
Dim VN()     As String
'
On Error GoTo errCARICA_DATI_MACCHINA_CORRENTI
  '
  If (Len(NomeTb) > 0) Then
    Call SCARICA_GRUPPO(NomeTb)
    Call WritePrivateProfileString(NomeTb, vbNullString, vbNullString, g_chOemPATH & "\SWAPPO.INI")
    Call SCRIVI_SEZIONE_INI(g_chOemPATH & "\" & NomeTb & ".SPF", g_chOemPATH & "\SWAPPO.INI", NomeTb)
    Set DB = OpenDatabase(PathDB, True, False)
    Set RS = DB.OpenRecordset(TIPO_LAVORAZIONE & "_CONFIG", dbOpenSnapshot)
    RS.MoveFirst
    Do
      If (NomeTb = RS.Fields("NOMEGRUPPO")) Then
        Pagina = val(RS.Fields("PAGINA"))
        Exit Do
      Else
        RS.MoveNext
      End If
    Loop While (Not RS.EOF)
    RS.Close
    If (Len(IMPORTAZIONE_LST) > 0) Then
      INDICI = Split(IMPORTAZIONE_LST, ";")
      ReDim VV(UBound(INDICI))
      ReDim VA(UBound(INDICI))
      ReDim VN(UBound(INDICI))
      Set TB = DB.OpenRecordset(TIPO_LAVORAZIONE, dbOpenTable)
      For ii = 0 To UBound(INDICI)
        TB.Index = "INDICE"
        TB.Seek "=", CInt(INDICI(ii))
        VV(ii) = "" & TB.Fields("ITEMDDE")
        VN(ii) = "" & TB.Fields("VARIABILE")
      Next ii
      TB.Close
      For ii = 0 To UBound(INDICI)
        VA(ii) = OPC_LEGGI_DATO(VV(ii))
        If IsNumeric(VA(ii)) Then VA(ii) = frmt(val(VA(ii)), 8)
      Next ii
      For ii = 0 To UBound(INDICI)
        If (Len(VV(ii)) > 0) Then
          ret = WritePrivateProfileString(NomeTb, VN(ii), VA(ii), g_chOemPATH & "\SWAPPO.INI")
        End If
      Next ii
    End If
    DB.Close
    Call OEMX.SuOEM1.LeggiSWAP2(Pagina)
  End If
  '
Exit Sub

errCARICA_DATI_MACCHINA_CORRENTI:
  WRITE_DIALOG "SUB CARICA_DATI_MACCHINA_CORRENTI: ERROR " & Format$(Err)
Exit Sub

End Sub

Sub VISUALIZZA_MOLA_PARAMETRI(RR37 As Double, ByVal RR38 As Double, ByVal RR45 As Double, ByVal RR55 As Double, ByVal CODICE_MOLA As String, ByVal SOPRA As Integer, ByVal Spessore As Integer, ByVal TIPO As String, ByVal ZeroPezzoX As Single, ByVal ZeroPezzoY As Single, ByVal SiStampa As String, _
                              ByVal RR41 As Double, ByVal RR43 As Double, ByVal RR44 As Double, ByVal RR46 As Double, ByVal RR47 As Double, ByVal RR48 As Double, ByVal RR49 As Double, _
                              ByVal RR51 As Double, ByVal RR53 As Double, ByVal RR54 As Double, ByVal RR56 As Double, ByVal RR57 As Double, ByVal RR58 As Double, ByVal RR59 As Double)
'
Dim ftmp        As Double
Dim stmp        As String
Dim Atmp        As String * 255
Dim i           As Integer
'
Dim pX1         As Single
Dim pX2         As Single
Dim pY1         As Single
Dim pY2         As Single
Dim PicH        As Single
Dim PicW        As Single
Dim COLORE      As Integer
'
Dim Scala       As Single
Dim ScalaX      As Single
Dim ScalaY      As Single
Dim NumeroRette As Integer
Dim DF          As Single
Dim AF          As Single
Dim XXp1        As Double
Dim YYp1        As Double
Dim AAP1        As Double
Dim XXp2        As Double
Dim YYp2        As Double
Dim AAP2        As Double
'
Dim APF         As Double
Dim AT1         As Double
Dim AT11        As Double
Dim GAMMA       As Double
Dim Delta       As Double
Dim angF        As Double
Dim angC1       As Double
Dim angC11      As Double
Dim DIAG        As Double
Dim ac          As Double
Dim AB          As Double
Dim BC          As Double
Dim RAD         As Double
Dim HF          As Double
Dim dALFA       As Double
Dim ALFA        As Double
Dim Beta        As Double
Dim GAMMA1      As Double
Dim GAMMA11     As Double
Dim USCITA      As Object
Dim sSCALA      As String
'
On Error GoTo errVISUALIZZA_MOLA_PARAMETRI
  '
  stmp = GetInfo("MOLA_VITI_CONICHE", "NumeroRette", Path_LAVORAZIONE_INI)
  NumeroRette = val(stmp): If NumeroRette < 1 Then NumeroRette = 1
  stmp = GetInfo("MOLA_VITI_CONICHE", "DF", Path_LAVORAZIONE_INI):    DF = val(stmp)
  stmp = GetInfo("MOLA_VITI_CONICHE", "AF", Path_LAVORAZIONE_INI):    AF = val(stmp)
  stmp = GetInfo("MOLA_VITI_CONICHE", "Scala", Path_LAVORAZIONE_INI): Scala = val(stmp)
  If (Scala <= 0) Then Scala = 1
  ScalaX = Scala:  ScalaY = Scala
  '
  i = LoadString(g_hLanguageLibHandle, 1000, Atmp, 255)
  If (i > 0) Then sSCALA = Left$(Atmp, i)
  '
  stmp = GetInfo("MOLA_VITI_CONICHE", "PicW", Path_LAVORAZIONE_INI):  PicW = val(stmp)
  stmp = GetInfo("MOLA_VITI_CONICHE", "PicH", Path_LAVORAZIONE_INI):  PicH = val(stmp)
  If (SiStampa <> "Y") Then
        If (TIPO = "OEMX") Then
      Set USCITA = OEMX.OEM1PicCALCOLO
    ElseIf (TIPO = "OEM1_HD") Then
      Set USCITA = OEM1.PicELABORA_HD
      USCITA.ScaleMode = 0
      USCITA.ScaleWidth = PicW
      USCITA.ScaleHeight = PicH
    ElseIf (TIPO = "OEM1") Then
      Set USCITA = OEM1.PicELABORA
      USCITA.ScaleMode = 0
      USCITA.ScaleWidth = PicW
      USCITA.ScaleHeight = PicH
    Else
      WRITE_DIALOG TIPO & " has not been defined!!!!"
      Exit Sub
    End If
  Else
    Set USCITA = Printer
    USCITA.ScaleMode = 6
    stmp = sSCALA & " " & frmt(Scala, 4) & ":1 "
    USCITA.CurrentX = PicW - USCITA.TextWidth(stmp)
    USCITA.CurrentY = PicH - USCITA.TextHeight("X")
    USCITA.Print stmp
  End If
  '
  If (Spessore <= 1) Then Spessore = 1
  USCITA.DrawWidth = Spessore
  '
  i = LoadString(g_hLanguageLibHandle, 1000, Atmp, 255)
      If (TIPO = "OEMX") Then
    If (i > 0) Then OEMX.lblINP(0).Caption = Left$(Atmp, i)
    OEMX.txtINP(0).Text = frmt(Scala, 4)
  ElseIf (TIPO = "OEM1_HD") Then
    If (i > 0) Then OEM1.lblINP_HD(0).Caption = Left$(Atmp, i)
    OEM1.txtINP_HD(0).Text = frmt(Scala, 4)
  ElseIf (TIPO = "OEM1") Then
    If (i > 0) Then OEM1.lblINP(0).Caption = Left$(Atmp, i)
    OEM1.txtINP(0).Text = frmt(Scala, 4)
  End If
  '
  'CONVERSIONE GRADI --> RADIANTI
  RR41 = RR41 / 180 * PG
  RR47 = RR47 / 180 * PG
  RR51 = RR51 / 180 * PG
  RR57 = RR57 / 180 * PG
  'LARGHEZZA MOLA
  If (RR37 = 0) Then RR37 = RR38 + RR48 + RR58 + 2
  'CavitÓ solo positiva !
  If (RR45 <= 0.0001) Then RR45 = 0.0001
  'GESTIONE CONICITA' FONDO
  RR44 = RR44 + RR55 / 2
  RR54 = RR54 - RR55 / 2
  'Bombatura solo positiva !
  If (RR46 <= 0.0001) Then RR46 = 0.0001
  If (RR56 <= 0.0001) Then RR56 = 0.0001
  'Punto C1
  DIAG = RR48 / Cos(PG / 4 - RR47 / 2 - RR41 / 2)
  YY(0, 31) = -RR43 + DIAG * Sin(PG / 4 - RR47 / 2 + RR41 / 2)
  XX(0, 31) = RR38 / 2 + DIAG * Cos(PG / 4 - RR47 / 2 + RR41 / 2) + RR43 * Tan(RR41)
  'Punto C11
  DIAG = RR58 / Cos(PG / 4 - RR57 / 2 - RR51 / 2)
  YY(0, 41) = -RR53 + DIAG * Sin(PG / 4 - RR57 / 2 + RR51 / 2)
  XX(0, 41) = -RR38 / 2 - DIAG * Cos(PG / 4 - RR57 / 2 + RR51 / 2) - RR53 * Tan(RR51)
  'Punto P2
  XX(0, 2) = XX(0, 31) - RR48 * Sin(RR47)
  YY(0, 2) = YY(0, 31) - RR48 * Cos(RR47)
  'Punto P12
  XX(0, 12) = XX(0, 41) + RR58 * Sin(RR57)
  YY(0, 12) = YY(0, 41) - RR58 * Cos(RR57)
  'Punto P1
  XX(0, 1) = RR37 / 2
  YY(0, 1) = YY(0, 2) - (XX(0, 1) - XX(0, 2)) * Tan(RR47)
  'Punto P11
  XX(0, 11) = -RR37 / 2
  YY(0, 11) = YY(0, 12) - (XX(0, 12) - XX(0, 11)) * Tan(RR57)
  'Punto C3
  YY(0, 33) = RR44 - RR43 - RR49
  XX(0, 33) = RR38 / 2 - RR49 * Tan((PG / 2 - RR41) / 2) - (RR44 - RR43) * Tan(RR41)
  'Punto C13
  YY(0, 43) = RR54 - RR53 - RR59
  XX(0, 43) = -RR38 / 2 + RR59 * Tan((PG / 2 - RR51) / 2) + (RR54 - RR53) * Tan(RR51)
  'Calcolo R-bombatura
  'F1
  HF = (YY(0, 33) - YY(0, 31) + (RR48 + RR49) * Sin(RR41)) / Cos(RR41) / 2
  RR(0, 1) = (HF ^ 2 + RR46 ^ 2) / 2 / RR46
  ALFA = Atn((YY(0, 33) - YY(0, 31)) / (XX(0, 31) - XX(0, 33)))
  ac = RR(0, 1) - RR49
  BC = RR(0, 1) + RR48
  AB = (XX(0, 31) - XX(0, 33)) / Cos(ALFA)
  RAD = ((ac ^ 2 + AB ^ 2 - BC ^ 2) / (2 * AB * ac))
  If (Abs(RAD) > 1) Then
    StopRegieEvents
    MsgBox "CALCULATION NOT POSSIBLE:" & Chr(13) & "FLANK CROWN F1 TOO BIG", 32, "SUB: VISUALIZZA_MOLA_PARAMETRI"
    ResumeRegieEvents
    Exit Sub
  End If
  Beta = FnACS(RAD)
  If (Beta < 0) Then Beta = PG + Beta
  GAMMA1 = PG - (ALFA + Beta)
  'F2
  HF = (YY(0, 43) - YY(0, 41) + (RR58 + RR59) * Sin(RR51)) / Cos(RR51) / 2
  RR(0, 11) = (HF ^ 2 + RR56 ^ 2) / 2 / RR56
  ALFA = Atn((YY(0, 43) - YY(0, 41)) / (XX(0, 43) - XX(0, 41)))
  ac = RR(0, 11) - RR59
  BC = RR(0, 11) + RR58
  AB = (XX(0, 43) - XX(0, 41)) / Cos(ALFA)
  RAD = ((ac ^ 2 + AB ^ 2 - BC ^ 2) / (2 * AB * ac))
  If (Abs(RAD) > 1) Then
    StopRegieEvents
    MsgBox "CALCULATION NOT POSSIBLE:" & Chr(13) & "FLANK CROWN F2 TOO BIG", 32, "SUB VISUALIZZA_MOLA_PARAMETRI"
    ResumeRegieEvents
    Exit Sub
  End If
  Beta = FnACS(RAD)
  If (Beta < 0) Then Beta = PG + Beta
  GAMMA11 = PG - (ALFA + Beta)
  'punto C2
  XX(0, 32) = XX(0, 33) - (RR(0, 1) - RR49) * Cos(GAMMA1)
  YY(0, 32) = YY(0, 33) - (RR(0, 1) - RR49) * Sin(GAMMA1)
  AT1 = Atn((YY(0, 31) - YY(0, 32)) / (XX(0, 31) - XX(0, 32)))
  If (AT1 < 0) Then
    StopRegieEvents
    MsgBox "CALCULATION NOT POSSIBLE:" & Chr(13) & "NEGATIVE FLANK 1: " & FnRG(AT1), 32, "SUB VISUALIZZA_MOLA_PARAMETRI"
    ResumeRegieEvents
    Exit Sub
  End If
  'punto C12
  XX(0, 42) = XX(0, 43) + (RR(0, 11) - RR59) * Cos(GAMMA11)
  YY(0, 42) = YY(0, 43) - (RR(0, 11) - RR59) * Sin(GAMMA11)
  AT11 = Atn((YY(0, 41) - YY(0, 42)) / (XX(0, 42) - XX(0, 41)))
  If (AT11 < 0) Then
    StopRegieEvents
    MsgBox "CALCULATION NOT POSSIBLE:" & Chr(13) & "NEGATIVE FLANK 2: " & FnRG(AT11), 32, "SUB VISUALIZZA_MOLA_PARAMETRI"
    ResumeRegieEvents
    Exit Sub
  End If
  'punto P4
  XX(0, 4) = XX(0, 31) - RR48 * Cos(AT1)
  YY(0, 4) = YY(0, 31) - RR48 * Sin(AT1)
  'punto P5
  XX(0, 5) = XX(0, 33) + RR49 * Cos(GAMMA1)
  YY(0, 5) = YY(0, 33) + RR49 * Sin(GAMMA1)
  'punto P14
  XX(0, 14) = XX(0, 41) + RR58 * Cos(AT11)
  YY(0, 14) = YY(0, 41) - RR58 * Sin(AT11)
  'punto P15
  XX(0, 15) = XX(0, 43) - RR59 * Cos(GAMMA11)
  YY(0, 15) = YY(0, 43) + RR59 * Sin(GAMMA11)
  '
  USCITA.CurrentX = (-XX(0, 1) - ZeroPezzoX) * ScalaX + PicW / 2 - USCITA.TextWidth(CODICE_MOLA & " ")
  USCITA.CurrentY = -(-YY(0, 1) - ZeroPezzoY) * ScalaY + PicH / 2 - USCITA.TextHeight("X") * SOPRA
  USCITA.Print CODICE_MOLA
  '
  'Spalla
  'F1
  COLORE = 0
  dALFA = Sqr((XX(0, 2) - XX(0, 1)) ^ 2 + (YY(0, 2) - YY(0, 1)) ^ 2) / NumeroRette
  For i = 0 To NumeroRette - 1
    APF = RR47
    XXp1 = XX(0, 1) - i * dALFA * Cos(APF)
    YYp1 = YY(0, 1) + i * dALFA * Sin(APF)
    AAP1 = PG / 2 - APF
    APF = RR47
    XXp2 = XX(0, 1) - (i + 1) * dALFA * Cos(APF)
    YYp2 = YY(0, 1) + (i + 1) * dALFA * Sin(APF)
    AAP2 = PG / 2 - APF
    GoSub Disegna
  Next i
  XXp1 = (-XX(0, 1) - ZeroPezzoX) * ScalaX + PicW / 2
  YYp1 = -(-YY(0, 1) - ZeroPezzoY) * ScalaY + PicH / 2
  XXp2 = (-XX(0, 1) - ZeroPezzoX) * ScalaX + PicW / 2
  YYp2 = -PicH / 2
  USCITA.Line (XXp1, YYp1)-(XXp2, YYp2), QBColor(COLORE)
  'F2
  COLORE = 0
  dALFA = Sqr((XX(0, 12) - XX(0, 11)) ^ 2 + (YY(0, 12) - YY(0, 11)) ^ 2) / NumeroRette
  For i = 0 To NumeroRette - 1
    APF = RR57
    XXp1 = XX(0, 11) + i * dALFA * Cos(APF)
    YYp1 = YY(0, 11) + i * dALFA * Sin(APF)
    AAP1 = PG / 2 - APF
    APF = RR57
    XXp2 = XX(0, 11) + (i + 1) * dALFA * Cos(APF)
    YYp2 = YY(0, 11) + (i + 1) * dALFA * Sin(APF)
    AAP2 = PG / 2 - APF
    GoSub Disegna
  Next i
  XXp1 = (-XX(0, 11) - ZeroPezzoX) * ScalaX + PicW / 2
  YYp1 = -(-YY(0, 11) - ZeroPezzoY) * ScalaY + PicH / 2
  XXp2 = (-XX(0, 11) - ZeroPezzoX) * ScalaX + PicW / 2
  YYp2 = -PicH / 2
  USCITA.Line (XXp1, YYp1)-(XXp2, YYp2), QBColor(COLORE)
  'Calcolo R-TESTA
  'F1
  COLORE = 12
  dALFA = (PG / 2 - AT1 - RR47) / NumeroRette
  For i = 0 To NumeroRette - 1
    APF = RR47 + i * dALFA
    XXp1 = XX(0, 31) - RR48 * Sin(APF)
    YYp1 = YY(0, 31) - RR48 * Cos(APF)
    AAP1 = PG / 2 - APF
    APF = RR47 + (i + 1) * dALFA
    XXp2 = XX(0, 31) - RR48 * Sin(APF)
    YYp2 = YY(0, 31) - RR48 * Cos(APF)
    AAP2 = PG / 2 - APF
    GoSub Disegna
  Next i
  'F2
  COLORE = 12
  dALFA = (PG / 2 - AT11 - RR57) / NumeroRette
  For i = 0 To NumeroRette - 1
    APF = RR57 + i * dALFA
    YYp1 = YY(0, 41) - RR58 * Cos(APF)
    XXp1 = XX(0, 41) + RR58 * Sin(APF)
    AAP1 = -(PG / 2 - APF)
    APF = RR57 + (i + 1) * dALFA
    YYp2 = YY(0, 41) - RR58 * Cos(APF)
    XXp2 = XX(0, 41) + RR58 * Sin(APF)
    AAP2 = -(PG / 2 - APF)
    GoSub Disegna
  Next i
  'Calcolo Fianco
  'F1
  COLORE = 9
  dALFA = (GAMMA1 - AT1) / NumeroRette
  For i = 0 To NumeroRette - 1
    APF = AT1 + i * dALFA
    XXp1 = XX(0, 32) + RR(0, 1) * Cos(APF)
    YYp1 = YY(0, 32) + RR(0, 1) * Sin(APF)
    AAP1 = APF
    APF = AT1 + (i + 1) * dALFA
    XXp2 = XX(0, 32) + RR(0, 1) * Cos(APF)
    YYp2 = YY(0, 32) + RR(0, 1) * Sin(APF)
    AAP2 = APF
    GoSub Disegna
  Next i
  'F2
  COLORE = 9
  dALFA = (GAMMA11 - AT11) / NumeroRette
  For i = 0 To NumeroRette - 1
    APF = AT11 + i * dALFA
    XXp1 = XX(0, 42) - RR(0, 11) * Cos(APF)
    YYp1 = YY(0, 42) + RR(0, 11) * Sin(APF)
    AAP1 = -APF
    APF = AT11 + (i + 1) * dALFA
    XXp2 = XX(0, 42) - RR(0, 11) * Cos(APF)
    YYp2 = YY(0, 42) + RR(0, 11) * Sin(APF)
    AAP2 = -APF
    GoSub Disegna
  Next i
  'Calcolo R-fondo
  If RR45 = 0 Then RR45 = 0.0001
  RR(0, 0) = (((XX(0, 33) - XX(0, 43)) / 2) ^ 2 + RR45 ^ 2) / 2 / RR45
  AB = RR(0, 0) + RR59
  ac = RR(0, 0) + RR49
  BC = Sqr((XX(0, 33) - XX(0, 43)) ^ 2 + (YY(0, 43) - YY(0, 33)) ^ 2)
  GAMMA = Atn((YY(0, 33) - YY(0, 43)) / (XX(0, 33) - XX(0, 43)))
  Delta = FnACS((AB ^ 2 + BC ^ 2 - ac ^ 2) / (2 * AB * BC))
  angC11 = PG / 2 - Delta - GAMMA
  XX(0, 34) = XX(0, 43) + (RR(0, 0) + RR59) * Sin(angC11)
  YY(0, 34) = YY(0, 43) + (RR(0, 0) + RR59) * Cos(angC11)
  angC1 = Atn((XX(0, 33) - XX(0, 34)) / (YY(0, 34) - YY(0, 33)))
  angC11 = Atn((XX(0, 34) - XX(0, 43)) / (YY(0, 34) - YY(0, 43)))
  'P6-P16
  XX(0, 6) = XX(0, 34) + RR(0, 0) * Sin(angC1)
  YY(0, 6) = YY(0, 34) - RR(0, 0) * Cos(angC1)
  XX(0, 16) = XX(0, 34) - RR(0, 0) * Sin(angC11)
  YY(0, 16) = YY(0, 34) - RR(0, 0) * Cos(angC11)
  'P7-P17
  XX(0, 7) = 0
  YY(0, 7) = YY(0, 34) - RR(0, 0)
  XX(0, 17) = XX(0, 6) + 1
  YY(0, 17) = YY(0, 34) - Sqr(RR(0, 0) ^ 2 - (XX(0, 17) - XX(0, 34)) ^ 2)
  'F1
  COLORE = 12
  dALFA = (PG / 2 - GAMMA1 + angC1) / NumeroRette
  For i = 0 To NumeroRette - 1
    APF = GAMMA1 + i * dALFA
    XXp1 = XX(0, 33) + RR49 * Cos(APF)
    YYp1 = YY(0, 33) + RR49 * Sin(APF)
    AAP1 = APF
    APF = GAMMA1 + (i + 1) * dALFA
    XXp2 = XX(0, 33) + RR49 * Cos(APF)
    YYp2 = YY(0, 33) + RR49 * Sin(APF)
    AAP2 = APF
    GoSub Disegna
  Next i
  'F2
  COLORE = 12
  dALFA = (PG / 2 - GAMMA11 + angC11) / NumeroRette
  For i = 0 To NumeroRette - 1
    APF = GAMMA11 + i * dALFA
    XXp1 = XX(0, 43) - RR59 * Cos(APF)
    YYp1 = YY(0, 43) + RR59 * Sin(APF)
    AAP1 = -APF
    APF = GAMMA11 + (i + 1) * dALFA
    XXp2 = XX(0, 43) - RR59 * Cos(APF)
    YYp2 = YY(0, 43) + RR59 * Sin(APF)
    AAP2 = -APF
    GoSub Disegna
  Next i
  Dim k1 As Integer
  Dim k2 As Integer
  k1 = NumeroRette / 2
  k2 = NumeroRette / 2
  While k1 + k2 < NumeroRette
    k2 = k2 + 1
  Wend
  'Calcolo Fondo
  COLORE = 9
  'FIANCO 1
  dALFA = (angC1 + angC11) / NumeroRette
  For i = 0 To k1 - 1
    angF = angC1 - i * dALFA
    XXp1 = XX(0, 34) + RR(0, 0) * Sin(angF)
    YYp1 = YY(0, 34) - RR(0, 0) * Cos(angF)
    AAP1 = PG / 2 - angF
    angF = angC1 - (i + 1) * dALFA
    XXp2 = XX(0, 34) + RR(0, 0) * Sin(angF)
    YYp2 = YY(0, 34) - RR(0, 0) * Cos(angF)
    AAP2 = PG / 2 - angF
    GoSub Disegna
  Next i
  'FIANCO 2
  For i = 0 To k2 - 1
    angF = angC1 - k1 * dALFA - i * dALFA
    XXp1 = XX(0, 34) + RR(0, 0) * Sin(angF)
    YYp1 = YY(0, 34) - RR(0, 0) * Cos(angF)
    AAP1 = PG / 2 - angF
    angF = angC1 - k1 * dALFA - (i + 1) * dALFA
    XXp2 = XX(0, 34) + RR(0, 0) * Sin(angF)
    YYp2 = YY(0, 34) - RR(0, 0) * Cos(angF)
    AAP2 = PG / 2 - angF
    GoSub Disegna
  Next i
  '
  If (ZeroPezzoX = 0) And (ZeroPezzoY = 0) Then
    'LARGHEZZA MOLA
    COLORE = 12
    XXp1 = -XX(0, 1)
    YYp1 = -YY(0, 1) + 15
    XXp2 = -XX(0, 11)
    YYp2 = -YY(0, 1) + 15
    pX1 = (XXp1 - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(YYp1 - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = (XXp2 - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(YYp2 - ZeroPezzoY) * ScalaY + PicH / 2
    USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
    GoSub FRECCIE_ORIZZONTALI
    stmp = " " & frmt(RR37, 4) & " "
    USCITA.CurrentX = (0 - ZeroPezzoX) * ScalaX + PicW / 2 - USCITA.TextWidth(stmp) / 2
    USCITA.CurrentY = -(YYp1 - ZeroPezzoY) * ScalaY + PicH / 2 - USCITA.TextHeight("X") * SOPRA
    USCITA.Print stmp
    'SPESSORE MOLA
    COLORE = 9
    XXp1 = -XX(0, 4)
    YYp1 = -YY(0, 4)
    XXp2 = -XX(0, 14)
    YYp2 = -YY(0, 4) '-YY(0, 14)
    pX1 = (XXp1 - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(YYp1 - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = (XXp2 - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(YYp2 - ZeroPezzoY) * ScalaY + PicH / 2
    USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
    GoSub FRECCIE_ORIZZONTALI
    stmp = " " & frmt(RR38, 4) & " "
    USCITA.CurrentX = (0 - ZeroPezzoX) * ScalaX + PicW / 2 - USCITA.TextWidth(stmp) / 2
    USCITA.CurrentY = -(YYp1 - ZeroPezzoY) * ScalaY + PicH / 2 - USCITA.TextHeight("X") * SOPRA
    USCITA.Print stmp
    'FONDO MOLA
    COLORE = 12
    XXp1 = -XX(0, 6)
    YYp1 = -YY(0, 6) - 1
    XXp2 = -XX(0, 16)
    YYp2 = -YY(0, 6) - 1 '-YY(0, 16) - 1
    pX1 = (XXp1 - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(YYp1 - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = (XXp2 - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(YYp2 - ZeroPezzoY) * ScalaY + PicH / 2
    USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
    pX1 = (XXp1 - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(YYp1 + 1 - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = (XXp1 - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(YYp1 - 1 - ZeroPezzoY) * ScalaY + PicH / 2
    USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
    pX1 = (XXp2 - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(YYp2 + 1 - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = (XXp2 - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(YYp2 - 1 - ZeroPezzoY) * ScalaY + PicH / 2
    USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
    GoSub FRECCIE_ORIZZONTALI
    stmp = " " & frmt(XX(0, 6) - XX(0, 16), 2) & " "
    USCITA.CurrentX = (0 - ZeroPezzoX) * ScalaX + PicW / 2 - USCITA.TextWidth(stmp) / 2
    USCITA.CurrentY = -(YYp1 - ZeroPezzoY) * ScalaY + PicH / 2
    USCITA.Print stmp
  End If
  USCITA.DrawStyle = 0
  USCITA.DrawWidth = 1
  WRITE_DIALOG ""
  '
Exit Sub

Disegna:
  pX1 = (-XXp1 - ZeroPezzoX) * ScalaX + PicW / 2
  pY1 = -(-YYp1 - ZeroPezzoY) * ScalaY + PicH / 2
  pX2 = (-XXp2 - ZeroPezzoX) * ScalaX + PicW / 2
  pY2 = -(-YYp2 - ZeroPezzoY) * ScalaY + PicH / 2
  USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
Return

FRECCIE_ORIZZONTALI:
Dim Ang      As Double
Dim DeltaAng As Double

  Ang = AF / 180 * PG
  DeltaAng = Ang / NumeroRette
  'FRECCIA SX
  pX2 = (XXp1 - ZeroPezzoX) * ScalaX + PicW / 2
  pY2 = -(YYp1 - ZeroPezzoY) * ScalaY + PicH / 2
  For i = -NumeroRette To NumeroRette
    pX1 = (XXp1 - ZeroPezzoX) * ScalaX + DF * Cos(Ang) + PicW / 2
    pY1 = -(YYp1 - ZeroPezzoY) * ScalaY + DF * Sin(i * DeltaAng) + PicH / 2
    USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
  Next i
  'FRECCIA DX
  pX2 = (XXp2 - ZeroPezzoX) * ScalaX + PicW / 2
  pY2 = -(YYp2 - ZeroPezzoY) * ScalaY + PicH / 2
  For i = -NumeroRette To NumeroRette
    pX1 = (XXp2 - ZeroPezzoX) * ScalaX - DF * Cos(Ang) + PicW / 2
    pY1 = -(YYp2 - ZeroPezzoY) * ScalaY - DF * Sin(i * DeltaAng) + PicH / 2
    USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
  Next i
Return

errVISUALIZZA_MOLA_PARAMETRI:
  WRITE_DIALOG "Sub VISUALIZZA_MOLA_PARAMETRI: ERROR -> " & Err
Resume Next
  
End Sub

Sub CONFRONTA_MOLE_VITI_CONICHE(ByVal SiStampa As String)
'
Dim Gruppo      As String
Dim CODICE_MOLA As String
Dim RR37(2)     As Double
Dim RR38(2)     As Double
Dim RR45(2)     As Double
Dim RR55(2)     As Double
Dim RR41(2)     As Double
Dim RR43(2)     As Double
Dim RR44(2)     As Double
Dim RR46(2)     As Double
Dim RR47(2)     As Double
Dim RR48(2)     As Double
Dim RR49(2)     As Double
Dim RR51(2)     As Double
Dim RR53(2)     As Double
Dim RR54(2)     As Double
Dim RR56(2)     As Double
Dim RR57(2)     As Double
Dim RR58(2)     As Double
Dim RR59(2)     As Double
Dim ZeroPezzoX  As Single
Dim ZeroPezzoY  As Single
Dim NomeINI     As String
Dim stmp        As String
Dim stmp1       As String
Dim i           As Integer
'
On Error Resume Next
  '
  CODICE_MOLA = LEGGI_PARAMETRI_MOLA_CONICA
  RR37(1) = R37: RR38(1) = R38: RR45(1) = R45: RR55(1) = R55
  RR41(1) = R41: RR43(1) = R43: RR44(1) = R44: RR46(1) = R46: RR47(1) = R47: RR48(1) = R48: RR49(1) = R49
  RR51(1) = R51: RR53(1) = R53: RR54(1) = R54: RR56(1) = R56: RR57(1) = R57: RR58(1) = R58: RR59(1) = R59
  'NOME TABELLA ADDIN PER LE MOLE
  Gruppo = GetInfo("OEM1", "TB_CONFRONTA_MOLE", Path_LAVORAZIONE_INI)
  Gruppo = UCase$(Trim$(Gruppo)) 'NOME DEL CAMPO
  Gruppo = GetInfo("OEM1", Gruppo, Path_LAVORAZIONE_INI)
  Gruppo = UCase$(Trim$(Gruppo)) 'NOME DELLA TABELLA
  If (Len(Gruppo) > 0) Then
    If (SiStampa <> "Y") Then OEMX.OEM1PicCALCOLO.Cls
    Call VISUALIZZA_MOLA_PARAMETRI(RR37(1), RR38(1), RR45(1), RR55(1), "RVC: " & CODICE_MOLA, 0, 2, "OEMX", 0, 0, SiStampa, _
                                   RR41(1), RR43(1), RR44(1), RR46(1), RR47(1), RR48(1), RR49(1), _
                                   RR51(1), RR53(1), RR54(1), RR56(1), RR57(1), RR58(1), RR59(1))
    If (Len(CODICE_MOLA) > 0) Then
    'CREAZIONE DEL FILE INI DI APPOGGIO PER LA LETTURA DEI PARAMETRI
    If (INIZIALIZZA_OFFLINE(Gruppo, CODICE_MOLA) = "TROVATO") Then
    NomeINI = GetInfo("Configurazione", "NomeFileTemporaneoSPF", PathFILEINI)
    NomeINI = g_chOemPATH & "\" & NomeINI & ".INI"
    ZeroPezzoX = val(GetInfo("MOLA_VITI_CONICHE", "ZeroPezzoX", Path_LAVORAZIONE_INI))
    ZeroPezzoY = val(GetInfo("MOLA_VITI_CONICHE", "ZeroPezzoY", Path_LAVORAZIONE_INI))
    RR37(2) = val(GetInfo(Gruppo, "R[37]", NomeINI))
    RR38(2) = val(GetInfo(Gruppo, "R[38]", NomeINI))
    RR45(2) = val(GetInfo(Gruppo, "R[45]", NomeINI))
    RR55(2) = val(GetInfo(Gruppo, "R[55]", NomeINI))
    RR41(2) = val(GetInfo(Gruppo, "R[41]", NomeINI))
    RR43(2) = val(GetInfo(Gruppo, "R[43]", NomeINI))
    RR44(2) = val(GetInfo(Gruppo, "R[44]", NomeINI))
    RR46(2) = val(GetInfo(Gruppo, "R[46]", NomeINI))
    RR47(2) = val(GetInfo(Gruppo, "R[47]", NomeINI))
    RR48(2) = val(GetInfo(Gruppo, "R[48]", NomeINI))
    RR49(2) = val(GetInfo(Gruppo, "R[49]", NomeINI))
    RR51(2) = val(GetInfo(Gruppo, "R[51]", NomeINI))
    RR53(2) = val(GetInfo(Gruppo, "R[53]", NomeINI))
    RR54(2) = val(GetInfo(Gruppo, "R[54]", NomeINI))
    RR56(2) = val(GetInfo(Gruppo, "R[56]", NomeINI))
    RR57(2) = val(GetInfo(Gruppo, "R[57]", NomeINI))
    RR58(2) = val(GetInfo(Gruppo, "R[58]", NomeINI))
    RR59(2) = val(GetInfo(Gruppo, "R[59]", NomeINI))
    Call VISUALIZZA_MOLA_PARAMETRI(RR37(2), RR38(2), RR45(2), RR55(2), "ADDIN: " & CODICE_MOLA, 1, 1, "OEMX", ZeroPezzoX, ZeroPezzoY, SiStampa, _
                                   RR41(2), RR43(2), RR44(2), RR46(2), RR47(2), RR48(2), RR49(2), _
                                   RR51(2), RR53(2), RR54(2), RR56(2), RR57(2), RR58(2), RR59(2))
    
    i = 1: stmp1 = UCase(ActualGroup)
    If Abs(RR37(2) - RR37(1)) > 5 Then
      i = i + 1
          If (InStr(stmp1, "OEM1_MOLA") > 0) And (Right(stmp1, 1) = "A") Then
        stmp = LNP("MO1R[0,37]")
      ElseIf (InStr(stmp1, "OEM1_MOLA") > 0) And (Right(stmp1, 1) = "B") Then
        stmp = LNP("MO2R[0,37]")
      ElseIf (InStr(stmp1, "OEM1_MOLA") > 0) And (Right(stmp1, 1) = "C") Then
        stmp = LNP("MO3R[0,37]")
      Else
        stmp = LNP("R[37]")
      End If
      OEMX.OEM1PicCALCOLO.CurrentX = OEMX.OEM1PicCALCOLO.TextWidth("X")
      OEMX.OEM1PicCALCOLO.CurrentY = OEMX.OEM1PicCALCOLO.ScaleHeight - OEMX.OEM1PicCALCOLO.TextHeight("X") * i
      OEMX.OEM1PicCALCOLO.Print stmp
    End If
    If Abs(RR44(2) - RR44(1)) > 0.5 Then
      i = i + 1
          If (InStr(stmp1, "OEM1_MOLA") > 0) And (Right(stmp1, 1) = "A") Then
        stmp = LNP("MO1R[0,44]")
      ElseIf (InStr(stmp1, "OEM1_MOLA") > 0) And (Right(stmp1, 1) = "B") Then
        stmp = LNP("MO2R[0,44]")
      ElseIf (InStr(stmp1, "OEM1_MOLA") > 0) And (Right(stmp1, 1) = "C") Then
        stmp = LNP("MO3R[0,44]")
      Else
        stmp = LNP("R[44]")
      End If
      OEMX.OEM1PicCALCOLO.CurrentX = OEMX.OEM1PicCALCOLO.TextWidth("X")
      OEMX.OEM1PicCALCOLO.CurrentY = OEMX.OEM1PicCALCOLO.ScaleHeight - OEMX.OEM1PicCALCOLO.TextHeight("X") * i
      OEMX.OEM1PicCALCOLO.Print stmp
    End If
    If Abs(RR49(2) - RR49(1)) > 0.5 Then
      i = i + 1
          If (InStr(stmp1, "OEM1_MOLA") > 0) And (Right(stmp1, 1) = "A") Then
        stmp = LNP("MO1R[0,49]")
      ElseIf (InStr(stmp1, "OEM1_MOLA") > 0) And (Right(stmp1, 1) = "B") Then
        stmp = LNP("MO2R[0,49]")
      ElseIf (InStr(stmp1, "OEM1_MOLA") > 0) And (Right(stmp1, 1) = "C") Then
        stmp = LNP("MO3R[0,49]")
      Else
        stmp = LNP("R[49]")
      End If
      stmp = stmp & " 1"
      OEMX.OEM1PicCALCOLO.CurrentX = OEMX.OEM1PicCALCOLO.TextWidth("X")
      OEMX.OEM1PicCALCOLO.CurrentY = OEMX.OEM1PicCALCOLO.ScaleHeight - OEMX.OEM1PicCALCOLO.TextHeight("X") * i
      OEMX.OEM1PicCALCOLO.Print stmp
    End If
    If Abs(RR59(2) - RR59(1)) > 0.5 Then
      i = i + 1
          If (InStr(stmp1, "OEM1_MOLA") > 0) And (Right(stmp1, 1) = "A") Then
        stmp = LNP("MO1R[0,59]")
      ElseIf (InStr(stmp1, "OEM1_MOLA") > 0) And (Right(stmp1, 1) = "B") Then
        stmp = LNP("MO2R[0,59]")
      ElseIf (InStr(stmp1, "OEM1_MOLA") > 0) And (Right(stmp1, 1) = "C") Then
        stmp = LNP("MO3R[0,59]")
      Else
        stmp = LNP("R[59]")
      End If
      stmp = stmp & " 2"
      OEMX.OEM1PicCALCOLO.CurrentX = OEMX.OEM1PicCALCOLO.TextWidth("X")
      OEMX.OEM1PicCALCOLO.CurrentY = OEMX.OEM1PicCALCOLO.ScaleHeight - OEMX.OEM1PicCALCOLO.TextHeight("X") * i
      OEMX.OEM1PicCALCOLO.Print stmp
    End If
    If (i > 1) Then
      i = i + 1
      stmp = "ADDIN <> RVC "
      OEMX.OEM1PicCALCOLO.CurrentX = OEMX.OEM1PicCALCOLO.TextWidth("X")
      OEMX.OEM1PicCALCOLO.CurrentY = OEMX.OEM1PicCALCOLO.ScaleHeight - OEMX.OEM1PicCALCOLO.TextHeight("X") * i
      OEMX.OEM1PicCALCOLO.Print stmp
    End If
    Else
      WRITE_DIALOG CODICE_MOLA & " " & Error(53)
    End If
    End If
  Else
    WRITE_DIALOG Gruppo & " WHEEL CODE HAVE NOT BEEN DEFINED!!!"
  End If
  '
End Sub

Sub VISUALIZZA_MOLA_RDE(Altezza As Double, ByVal Larghezza As Double, ByVal Raggio As Double, ByVal CONICITA As Double, ByVal CODICE_MOLA As String, ByVal Spessore As Integer, ByVal TIPO As String, ByVal ZeroPezzoX As Single, ByVal ZeroPezzoY As Single, ByVal SiStampa As String)
'
Dim ftmp        As Double
Dim stmp        As String
Dim Atmp        As String * 255
Dim i           As Integer
'
Dim pX1         As Single
Dim pX2         As Single
Dim pY1         As Single
Dim pY2         As Single
Dim PicH        As Single
Dim PicW        As Single
Dim COLORE      As Integer
'
Dim Scala       As Single
Dim ScalaX      As Single
Dim ScalaY      As Single
Dim NumeroRette As Integer
Dim DF          As Single
Dim AF          As Single
Dim XXp1        As Double
Dim YYp1        As Double
Dim AAP1        As Double
Dim XXp2        As Double
Dim YYp2        As Double
Dim AAP2        As Double
'
Dim Delta       As Double
Dim dALFA       As Double
Dim ALFA        As Double
Dim USCITA      As Object
Dim sSCALA      As String
'
On Error GoTo errVISUALIZZA_MOLA_RDE
  '
  stmp = GetInfo("MOLA_VITI_CONICHE", "NumeroRette", Path_LAVORAZIONE_INI)
  NumeroRette = val(stmp): If NumeroRette < 1 Then NumeroRette = 1
  stmp = GetInfo("MOLA_VITI_CONICHE", "DF", Path_LAVORAZIONE_INI):    DF = val(stmp)
  stmp = GetInfo("MOLA_VITI_CONICHE", "AF", Path_LAVORAZIONE_INI):    AF = val(stmp)
  stmp = GetInfo("MOLA_VITI_CONICHE", "Scala", Path_LAVORAZIONE_INI): Scala = val(stmp)
  If (Scala <= 0) Then Scala = 1
  ScalaX = Scala:  ScalaY = Scala
  '
  i = LoadString(g_hLanguageLibHandle, 1000, Atmp, 255)
  If (i > 0) Then sSCALA = Left$(Atmp, i)
  '
  stmp = GetInfo("MOLA_VITI_CONICHE", "PicW", Path_LAVORAZIONE_INI):  PicW = val(stmp)
  stmp = GetInfo("MOLA_VITI_CONICHE", "PicH", Path_LAVORAZIONE_INI):  PicH = val(stmp)
  If (SiStampa <> "Y") Then
        If (TIPO = "OEMX") Then
      Set USCITA = OEMX.OEM1PicCALCOLO
    ElseIf (TIPO = "OEM1_HD") Then
      Set USCITA = OEM1.PicELABORA_HD
      USCITA.ScaleMode = 0
      USCITA.ScaleWidth = PicW
      USCITA.ScaleHeight = PicH
    ElseIf (TIPO = "OEM1") Then
      Set USCITA = OEM1.PicELABORA
      USCITA.ScaleMode = 0
      USCITA.ScaleWidth = PicW
      USCITA.ScaleHeight = PicH
    Else
      WRITE_DIALOG TIPO & " has not been defined!!!!"
      Exit Sub
    End If
  Else
    Set USCITA = Printer
    USCITA.ScaleMode = 6
    stmp = sSCALA & " " & frmt(Scala, 4) & ":1 "
    USCITA.CurrentX = PicW - USCITA.TextWidth(stmp)
    USCITA.CurrentY = PicH - USCITA.TextHeight("X")
    USCITA.Print stmp
  End If
  '
  If (Spessore <= 1) Then Spessore = 1
  USCITA.DrawWidth = Spessore
  '
  i = LoadString(g_hLanguageLibHandle, 1000, Atmp, 255)
      If (TIPO = "OEMX") Then
    If (i > 0) Then OEMX.lblINP(0).Caption = Left$(Atmp, i)
    OEMX.txtINP(0).Text = frmt(Scala, 4)
  ElseIf (TIPO = "OEM1_HD") Then
    If (i > 0) Then OEM1.lblINP_HD(0).Caption = Left$(Atmp, i)
    OEM1.txtINP_HD(0).Text = frmt(Scala, 4)
  ElseIf (TIPO = "OEM1") Then
    If (i > 0) Then OEM1.lblINP(0).Caption = Left$(Atmp, i)
    OEM1.txtINP(0).Text = frmt(Scala, 4)
  End If
  '
  If (Larghezza <= 2 * Raggio) Then Larghezza = 2 * Raggio + 0.001
  If (Altezza <= Raggio) Then Altezza = Raggio + 0.001
  CONICITA = -Atn(CONICITA / (Larghezza - 2 * Raggio))
  '
  Dim ag As Double
  Dim DG As Double
  Dim jj As Integer
  Dim NN As Integer
  Dim Xc As Double
  Dim YC As Double
  '
  Dim DDX1 As Double
  Dim DDX2 As Double
  Dim DDY1 As Double
  Dim DDY2 As Double
  '
  'USCITA.Line (0, PicH / 2)-(PicW, PicH / 2), QBColor(COLORE)
  'USCITA.Line (PicW / 2, 0)-(PicW / 2, PicH), QBColor(COLORE)
  '
  DDX1 = Larghezza / 2 - Raggio * (1 + Sin(CONICITA)): DDY1 = DDX1 * Tan(CONICITA)
  DDX2 = Larghezza / 2 - Raggio * (1 - Sin(CONICITA)): DDY2 = DDX2 * Tan(CONICITA)
  '
  XXp1 = (-Larghezza / 2 - 5 - ZeroPezzoX) * ScalaX + PicW / 2
  YYp1 = -(-Altezza - ZeroPezzoY) * ScalaY + PicH / 2
  XXp2 = (-Larghezza / 2 - ZeroPezzoX) * ScalaX + PicW / 2
  YYp2 = -(-Altezza - ZeroPezzoY) * ScalaY + PicH / 2
  USCITA.Line (XXp1, YYp1)-(XXp2, YYp2), QBColor(COLORE)
  '
  XXp1 = (-Larghezza / 2 - ZeroPezzoX) * ScalaX + PicW / 2
  YYp1 = -(-Altezza - ZeroPezzoY) * ScalaY + PicH / 2
  XXp2 = (-Larghezza / 2 - ZeroPezzoX) * ScalaX + PicW / 2
  YYp2 = -(DDY1 - Raggio * Cos(CONICITA) - ZeroPezzoY) * ScalaY + PicH / 2
  USCITA.Line (XXp1, YYp1)-(XXp2, YYp2), QBColor(COLORE)
  '
  NN = 20
  DG = (PG / 2 + CONICITA) / NN
  Xc = -Larghezza / 2 + Raggio: YC = DDY1 - Raggio * Cos(CONICITA)
  For j = 0 To NN - 1
    XXp1 = (Xc - Raggio * Cos(DG * j) - ZeroPezzoX) * ScalaX + PicW / 2
    YYp1 = -(YC + Raggio * Sin(DG * j) - ZeroPezzoY) * ScalaY + PicH / 2
    XXp2 = (Xc - Raggio * Cos(DG * (j + 1)) - ZeroPezzoX) * ScalaX + PicW / 2
    YYp2 = -(YC + Raggio * Sin(DG * (j + 1)) - ZeroPezzoY) * ScalaY + PicH / 2
    USCITA.Line (XXp1, YYp1)-(XXp2, YYp2), QBColor(COLORE)
  Next j
  stmp = " r=" & frmt(Raggio, 4) & " "
  USCITA.CurrentX = (Xc - Raggio - ZeroPezzoX) * ScalaX + PicW / 2 - USCITA.TextWidth(stmp)
  USCITA.CurrentY = -(YC - ZeroPezzoY) * ScalaY + PicH / 2 - USCITA.TextHeight("X")
  USCITA.Print stmp
  '
  XXp1 = (-DDX1 - ZeroPezzoX) * ScalaX + PicW / 2
  YYp1 = -(DDY1 - ZeroPezzoY) * ScalaY + PicH / 2
  XXp2 = (DDX2 - ZeroPezzoX) * ScalaX + PicW / 2
  YYp2 = -(-DDY2 - ZeroPezzoY) * ScalaY + PicH / 2
  USCITA.Line (XXp1, YYp1)-(XXp2, YYp2), QBColor(COLORE)
  '
  NN = 20
  DG = (PG / 2 - CONICITA) / NN
  Xc = Larghezza / 2 - Raggio: YC = -DDY2 - Raggio * Cos(CONICITA)
  For j = 0 To NN - 1
    XXp1 = (Xc + Raggio * Cos(DG * j) - ZeroPezzoX) * ScalaX + PicW / 2
    YYp1 = -(YC + Raggio * Sin(DG * j) - ZeroPezzoY) * ScalaY + PicH / 2
    XXp2 = (Xc + Raggio * Cos(DG * (j + 1)) - ZeroPezzoX) * ScalaX + PicW / 2
    YYp2 = -(YC + Raggio * Sin(DG * (j + 1)) - ZeroPezzoY) * ScalaY + PicH / 2
    USCITA.Line (XXp1, YYp1)-(XXp2, YYp2), QBColor(COLORE)
  Next j
  stmp = " r=" & frmt(Raggio, 4) & " "
  USCITA.CurrentX = (Xc + Raggio - ZeroPezzoX) * ScalaX + PicW / 2
  USCITA.CurrentY = -(YC - ZeroPezzoY) * ScalaY + PicH / 2 - USCITA.TextHeight("X")
  USCITA.Print stmp
  '
  XXp1 = (Larghezza / 2 - ZeroPezzoX) * ScalaX + PicW / 2
  YYp1 = -(-DDY2 - Raggio * Cos(CONICITA) - ZeroPezzoY) * ScalaY + PicH / 2
  XXp2 = (Larghezza / 2 - ZeroPezzoX) * ScalaX + PicW / 2
  YYp2 = -(-Altezza - ZeroPezzoY) * ScalaY + PicH / 2
  USCITA.Line (XXp1, YYp1)-(XXp2, YYp2), QBColor(COLORE)
  '
  XXp1 = (Larghezza / 2 - ZeroPezzoX) * ScalaX + PicW / 2
  YYp1 = -(-Altezza - ZeroPezzoY) * ScalaY + PicH / 2
  XXp2 = (Larghezza / 2 + 5 - ZeroPezzoX) * ScalaX + PicW / 2
  YYp2 = -(-Altezza - ZeroPezzoY) * ScalaY + PicH / 2
  USCITA.Line (XXp1, YYp1)-(XXp2, YYp2), QBColor(COLORE)
  '
  'USCITA.CurrentX = (-XX(0, 1) - ZeroPezzoX) * ScalaX + PicW / 2 - USCITA.TextWidth(CODICE_MOLA & " ")
  'USCITA.CurrentY = -(-YY(0, 1) - ZeroPezzoY) * ScalaY + PicH / 2 - USCITA.TextHeight("X")
  USCITA.CurrentX = PicW - USCITA.TextWidth(CODICE_MOLA & " ")
  USCITA.CurrentY = PicH - USCITA.TextHeight("X")
  USCITA.Print CODICE_MOLA
  '
  'LARGHEZZA MOLA
  COLORE = 12
  XXp1 = -Larghezza / 2
  YYp1 = -Altezza
  XXp2 = Larghezza / 2
  YYp2 = -Altezza
  pX1 = (XXp1 - ZeroPezzoX) * ScalaX + PicW / 2
  pY1 = -(YYp1 - ZeroPezzoY) * ScalaY + PicH / 2
  pX2 = (XXp2 - ZeroPezzoX) * ScalaX + PicW / 2
  pY2 = -(YYp2 - ZeroPezzoY) * ScalaY + PicH / 2
  USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
  GoSub FRECCIE_ORIZZONTALI
  stmp = " " & frmt(Larghezza, 4) & " "
  USCITA.CurrentX = (0 - ZeroPezzoX) * ScalaX + PicW / 2 - USCITA.TextWidth(stmp) / 2
  USCITA.CurrentY = -(YYp1 - ZeroPezzoY) * ScalaY + PicH / 2 - USCITA.TextHeight("X")
  USCITA.Print stmp
  '
  WRITE_DIALOG ""
  '
Exit Sub

Disegna:
  pX1 = (-XXp1 - ZeroPezzoX) * ScalaX + PicW / 2
  pY1 = -(-YYp1 - ZeroPezzoY) * ScalaY + PicH / 2
  pX2 = (-XXp2 - ZeroPezzoX) * ScalaX + PicW / 2
  pY2 = -(-YYp2 - ZeroPezzoY) * ScalaY + PicH / 2
  USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
Return

FRECCIE_ORIZZONTALI:
Dim Ang      As Double
Dim DeltaAng As Double

  Ang = AF / 180 * PG
  DeltaAng = Ang / NumeroRette
  'FRECCIA SX
  pX2 = (XXp1 - ZeroPezzoX) * ScalaX + PicW / 2
  pY2 = -(YYp1 - ZeroPezzoY) * ScalaY + PicH / 2
  For i = -NumeroRette To NumeroRette
    pX1 = (XXp1 - ZeroPezzoX) * ScalaX + DF * Cos(Ang) + PicW / 2
    pY1 = -(YYp1 - ZeroPezzoY) * ScalaY + DF * Sin(i * DeltaAng) + PicH / 2
    USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
  Next i
  'FRECCIA DX
  pX2 = (XXp2 - ZeroPezzoX) * ScalaX + PicW / 2
  pY2 = -(YYp2 - ZeroPezzoY) * ScalaY + PicH / 2
  For i = -NumeroRette To NumeroRette
    pX1 = (XXp2 - ZeroPezzoX) * ScalaX - DF * Cos(Ang) + PicW / 2
    pY1 = -(YYp2 - ZeroPezzoY) * ScalaY - DF * Sin(i * DeltaAng) + PicH / 2
    USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
  Next i
Return

errVISUALIZZA_MOLA_RDE:
  WRITE_DIALOG "Sub VISUALIZZA_MOLA_RDE: ERROR -> " & Err
Resume Next
  
End Sub

Sub VISUALIZZAZIONE_RDE_CONICHE(ByVal TIPO_LAVORAZIONE As String, ByVal SiStampa As String)
'
Dim ftmp As Double
Dim stmp As String
Dim Atmp As String * 255
Dim i    As Integer
Dim j    As Integer
Dim k    As Integer
Dim L    As Integer
Dim ret  As Integer
'
Dim TipoCarattere As String
Dim USCITA        As Object
'
Dim pX1         As Double
Dim pX2         As Double
Dim pY1         As Double
Dim pY2         As Double
Dim PicH        As Double
Dim PicW        As Double
Dim COLORE      As Integer
'
Dim ZeroPezzoX  As Double
Dim ZeroPezzoY  As Double
Dim Scala       As Double
Dim ScalaX      As Double
Dim ScalaY      As Double
'
Dim NumeroRette As Integer
Dim DF          As Double
Dim AF          As Double
Dim XXp1        As Double
Dim YYp1        As Double
Dim AAP1        As Double
Dim XXp2        As Double
Dim YYp2        As Double
Dim AAP2        As Double
Dim Ang         As Double
Dim DeltaAng    As Double
Dim MTop        As Single
Dim MLeft       As Single
Dim TextFont    As String
'
On Error GoTo errVISUALIZZAZIONE_RDE_CONICHE
  '
    If (LINGUA = "CH") Then
    TextFont = "MS Song"
  ElseIf (LINGUA = "RU") Then
    TextFont = "Arial Cyr"
  Else
    TextFont = "Arial"
  End If
  'SELEZIONE DEL TIPO DI PERIFERICA
  PicW = 190: PicH = 95
  If (SiStampa = "N") Then
    OEMX.PicCALCOLO.Cls
    OEMX.PicCALCOLO.FontName = TextFont
    OEMX.PicCALCOLO.FontSize = OEMX.PicCALCOLO.Height / 500
    Set USCITA = OEMX.PicCALCOLO
    USCITA.ScaleWidth = PicW
    USCITA.ScaleHeight = PicH
    MTop = 0
    MLeft = 0
  Else
    Printer.FontName = TextFont
    Printer.FontSize = 12
    Set USCITA = Printer
    MTop = 5
    MLeft = 5
  End If
  Exit Sub
  Dim ZIV      As Double
  Dim LLV      As Double
  Dim DDV      As Double
  Dim PRI      As Integer
  Dim PAV      As Double
  Dim PP0      As Double
  Dim RR0      As Double
  Dim LL0      As Double
  Dim LL1      As Double
  Dim tC(1, 9) As Double
  Dim Sc(1, 9) As Integer
  Dim ZI(1, 9) As Double
  Dim DD(1, 9) As Double
  'Dim NS(1, 9) As Integer
  Dim LS(1, 9) As Double
  Dim ZL(1, 9) As Double
  Dim ZLMIN    As Double
  Dim ZLMAX    As Double
  Dim NGV      As Integer
  Dim Zmin     As Double
  Dim ZMax     As Double
  Dim DDX      As Double
  Dim DDY      As Double
  Dim ZI2      As Double
  Dim PP2      As Double
  Dim DD2      As Double
  Dim LL2      As Double
  Dim RR3      As Integer
  Dim ZI3      As Double
  Dim DISTFORO  As Double
  '
  Select Case TIPO_LAVORAZIONE
  Case "VITI_CONICHE"
    ZI3 = OPC_LEGGI_DATO("/ACC/NCK/UGUD/POSZFORO_G[1]")
    ZIV = ZI3 - Lp("DISTFIXRIF") 'Lp("R[190]")
    DDV = Lp("R[189]")
    PRI = Lp("R[5]")
    PAV = Lp("R[128]")
    LLV = Lp("R[91]") + Lp("R[92]") + Lp("R[93]") + Lp("R[94]") + Lp("R[95]") + Lp("R[96]") + Lp("R[97]")
  Case "VITI_PASSO_VARIABILE"
    ZI3 = OPC_LEGGI_DATO("/ACC/NCK/UGUD/POSZFORO_G[2]")
    ZIV = ZI3 - Lp("DISTFIXRIF") 'Lp("R[190]")
    DDV = Lp("R[189]")
    PRI = 1
    PAV = (Lp("R[214]") + Lp("R[215]")) / 2 'DA MODIFICARE
    LLV = Lp("R[210]") + Lp("R[211]") + Lp("R[212]")
  Case "VITI_BARRIERA3"
    ZI3 = OPC_LEGGI_DATO("/ACC/NCK/UGUD/POSZFORO_G[3]")
    ZIV = ZI3 - Lp("DISTFIXRIF") 'Lp("R[190]")
    DDV = Lp("R[189]")
    PRI = 1
    PAV = (Lp("PL") + Lp("BL")) / 2
    LLV = (Lp("R[91]") + Lp("R[92]")) * Lp("PL") + (Lp("R[93]") + Lp("R[94]") + Lp("R[95]") + Lp("R[96]") + Lp("R[97]") + Lp("R[98]") + Lp("R[99]") + Lp("R[90]")) * Lp("BL")
  Case "VITI_BARRIERA6"
    ZI3 = OPC_LEGGI_DATO("/ACC/NCK/UGUD/POSZFORO_G[4]")
    ZIV = ZI3 - Lp("DISTFIXRIF") 'Lp("R[190]")
    DDV = Lp("R[189]")
    PRI = 1
    PAV = (Lp("PL") + Lp("BL")) / 2
    LLV = (Lp("R[91]") + Lp("R[92]")) * Lp("PL") + (Lp("R[93]") + Lp("R[94]") + Lp("R[95]") + Lp("R[96]") + Lp("R[97]") + Lp("R[98]") + Lp("R[99]") + Lp("R[90]")) * Lp("BL")
  End Select
  If (PRI <= 0) Then PRI = 1
  If (PAV > 0) Then
    PAV = PAV / PRI
    NGV = LLV / PAV
  Else
    NGV = 1
  End If
  '
  'LARGHEZZA EFFICACE MOLA: AGGIORNATO DA SUB INIZIALIZZA_OEM1_CNC
  PP0 = val(GetInfo("DATI_MOLA_RDE", "NMOLA_G", g_chOemPATH & "\ADDIN.INI"))
  RR0 = val(GetInfo("DATI_MOLA_RDE", "RFONDO_G", g_chOemPATH & "\ADDIN.INI"))
  If (RR0 <= 0) Then RR0 = 1
  LL0 = val(GetInfo("DATI_MOLA_RDE", "LARGMOLAPR_G", g_chOemPATH & "\ADDIN.INI"))
  If (LL0 <= 0) Then LL0 = 60
  LL0 = LL0 - 2 * RR0
  '
  'MOLA TERMINALE
  ZI2 = Lp("RTE_POSINI")
  PP2 = Lp("RTE_ALTEZZA")
  DD2 = Lp("RTE_DIAMETRO")
  LL2 = 20 'val(GetInfo("DATI_TERMINALE", "R[38]", g_chOemPATH & "\ADDIN.INI"))
  RR3 = val(GetInfo("DATI_TERMINALE", "R[3]", g_chOemPATH & "\ADDIN.INI"))
  '
  ZLMIN = Lp("QZLUN[0,0]")
  ZLMAX = Lp("QZLUN[0,1]")
  LL1 = Lp("MAC[0,200]")
  '
  Zmin = 100000: ZMax = -100000
  For j = 0 To 1
    For i = 0 To 9
      tC(j, i) = Lp("RDS[0," & Format$(100 * j + 10 * i + 0, "#0") & "]")
      Sc(j, i) = Lp("RDS[0," & Format$(100 * j + 10 * i + 9, "#0") & "]")
      If (tC(j, i) > 0) And (Sc(j, i) > 0) Then
      ZI(j, i) = Lp("RDS[0," & Format$(100 * j + 10 * i + 1, "#0") & "]")
      DD(j, i) = Lp("RDS[0," & Format$(100 * j + 10 * i + 2, "#0") & "]") ' + Lp("RDS[0," & Format$(100 * j + 10 * i + 3, "#0") & "]")
      'NS(j, i) = Lp("RDS[0," & Format$(100 * j + 10 * i + 5, "#0") & "]")
      LS(j, i) = Lp("RDS[0," & Format$(100 * j + 10 * i + 6, "#0") & "]")
      ZL(j, i) = Lp("RDS[0," & Format$(100 * j + 10 * i + 7, "#0") & "]")
      'QUOTA ASSIALE INIZIALE MINIMA
      ftmp = ZI(j, i) - LL0 / 2
      If (Zmin >= ftmp) Then Zmin = ftmp
      'QUOTA ASSIALE FINALE MASSIMA
      'ftmp = ZI(j, i) + LL0 / 2 + NS(j, i) * LS(j, i)
      ftmp = ZI(j, i) + LL0 / 2 + LS(j, i)
      If (ZMax <= ftmp) Then ZMax = ftmp
      End If
    Next i
  Next j
  'CALCOLO SCALA DA ESCURSIONE ASSIALE MASSIMA
  ftmp = ZLMAX + LL1 / 2
  If (ftmp > ZMax) Then ZMax = ftmp
  ftmp = ZIV + LLV + LL1
  If (ftmp > ZMax) Then ZMax = ftmp
  Scala = Int((PicW / ZMax) * 1000) / 1000
  If (Scala <= 0) Then Scala = 0.1
  ScalaX = Scala
  ScalaY = Scala
  '
  USCITA.DrawWidth = 2
  USCITA.DrawStyle = 0
  USCITA.Line (MLeft + 0, MTop + 0 * PicH / 4)-(MLeft + PicW, MTop + 2 * PicH / 4), QBColor(14), BF
  USCITA.Line (MLeft + 0, MTop + 0 * PicH / 4)-(MLeft + PicW, MTop + 2 * PicH / 4), QBColor(0), B
  '
  USCITA.Line (MLeft + 0, MTop + 2 * PicH / 4)-(MLeft + PicW, MTop + 4 * PicH / 4), QBColor(15), BF
  USCITA.Line (MLeft + 0, MTop + 2 * PicH / 4)-(MLeft + PicW, MTop + 4 * PicH / 4), QBColor(0), B
  '
  USCITA.DrawWidth = 1
  USCITA.DrawStyle = 3
  USCITA.FontItalic = True
  USCITA.FontBold = False
  USCITA.FontUnderline = True
  j = 0
    'NOME DELLA FASE
    stmp = Tabella1.MESSAGGI(22).NOME
    stmp = " " & Left$(stmp, InStr(stmp, ";") - 1) & " s1 & s2"
    USCITA.CurrentX = MLeft + USCITA.TextWidth("X")
    USCITA.CurrentY = MTop + (0 + 2 * j) * PicH / 4 + USCITA.TextHeight("X") / 2
    USCITA.Print stmp
    USCITA.Line (MLeft + 0, MTop + (1 + 2 * j) * PicH / 4)-(MLeft + PicW, MTop + (1 + 2 * j) * PicH / 4), QBColor(0)
  j = 1
    'NOME DELLA FASE
    stmp = Tabella1.MESSAGGI(24).NOME
    stmp = " " & Left$(stmp, InStr(stmp, ";") - 1) & " s1 & s2"
    USCITA.CurrentX = MLeft + USCITA.TextWidth("X")
    USCITA.CurrentY = MTop + (0 + 2 * j) * PicH / 4 + USCITA.TextHeight("X") / 2
    USCITA.Print stmp
    USCITA.Line (MLeft + 0, MTop + (1 + 2 * j) * PicH / 4)-(MLeft + PicW, MTop + (1 + 2 * j) * PicH / 4), QBColor(0)
  j = 0
    'LARGHEZZA E POSIZIONE MOLA
    stmp = " CUT: " & frmt(PP0, 4) & " W: " & frmt(LL0, 4) & " "
    USCITA.CurrentX = MLeft + PicW - USCITA.TextWidth(stmp) - USCITA.TextWidth("X")
    USCITA.CurrentY = MTop + (0 + 2 * j) * PicH / 4 + USCITA.TextHeight("X") / 2
    USCITA.Print stmp
    USCITA.Line (MLeft + 0, MTop + (1 + 2 * j) * PicH / 4)-(MLeft + PicW, MTop + (1 + 2 * j) * PicH / 4), QBColor(0)
  USCITA.FontUnderline = False
  USCITA.FontItalic = False
  USCITA.FontBold = False
  '
  'VISUALIZZAZIONE VITE: SFONDO
  COLORE = 0
  USCITA.DrawWidth = 1
  USCITA.DrawStyle = 2
  For j = 0 To 1
    XXp1 = (ZIV - ZeroPezzoX) * ScalaX
    YYp1 = -(DDV / 2 - ZeroPezzoY) * ScalaY + (1 + 2 * j) * PicH / 4
    XXp2 = (ZIV + LLV - ZeroPezzoX) * ScalaX
    YYp2 = -(-DDV / 2 - ZeroPezzoY) * ScalaY + (1 + 2 * j) * PicH / 4
    USCITA.Line (MLeft + XXp1, MTop + YYp1)-(MLeft + XXp2, MTop + YYp2), QBColor(15), BF
  Next j
  '
  'CORSA LUNETTA
  If (ZLMIN <> ZLMAX) Then
    COLORE = 0
    XXp1 = (ZLMIN - ZeroPezzoX) * ScalaX: YYp1 = 0
    XXp2 = (ZLMIN - ZeroPezzoX) * ScalaX: YYp2 = PicH
    stmp = " ZLmin: " & frmt(ZLMIN, 4) & " "
    USCITA.Line (MLeft + XXp1, MTop + YYp1)-(MLeft + XXp2, MTop + YYp2), QBColor(COLORE)
    USCITA.CurrentX = MLeft + XXp1 - USCITA.TextWidth(stmp)
    USCITA.CurrentY = MTop + PicH - USCITA.TextHeight("X")
    USCITA.Print stmp
    XXp1 = (ZLMAX - ZeroPezzoX) * ScalaX: YYp1 = 0
    XXp2 = (ZLMAX - ZeroPezzoX) * ScalaX: YYp2 = PicH
    USCITA.Line (MLeft + XXp1, MTop + YYp1)-(MLeft + XXp2, MTop + YYp2), QBColor(COLORE)
    stmp = " ZLmax: " & frmt(ZLMAX, 4) & " "
    USCITA.Line (MLeft + XXp1, MTop + YYp1)-(MLeft + XXp2, MTop + YYp2), QBColor(COLORE)
    USCITA.CurrentX = MLeft + XXp1
    USCITA.CurrentY = MTop + PicH - USCITA.TextHeight("X")
    USCITA.Print stmp
    USCITA.DrawWidth = 1
    USCITA.DrawStyle = 0
    COLORE = 12
    XXp1 = (ZLMIN - ZeroPezzoX) * ScalaX
    YYp1 = PicH - 5
    XXp2 = (ZLMIN + 10 - ZeroPezzoX) * ScalaX
    YYp2 = PicH - 5 + 1
    USCITA.Line (MLeft + XXp1, MTop + YYp1)-(MLeft + XXp2, MTop + YYp2), QBColor(COLORE)
    XXp2 = (ZLMIN + 10 - ZeroPezzoX) * ScalaX
    YYp2 = PicH - 5 - 1
    USCITA.Line (MLeft + XXp1, MTop + YYp1)-(MLeft + XXp2, MTop + YYp2), QBColor(COLORE)
    XXp1 = (ZLMIN - ZeroPezzoX) * ScalaX
    YYp1 = PicH - 5
    XXp2 = (ZLMAX - ZeroPezzoX) * ScalaX
    YYp2 = PicH - 5
    USCITA.Line (MLeft + XXp1, MTop + YYp1)-(MLeft + XXp2, MTop + YYp2), QBColor(COLORE)
    XXp1 = (ZLMAX - ZeroPezzoX) * ScalaX
    YYp1 = PicH - 5
    XXp2 = (ZLMAX - 10 - ZeroPezzoX) * ScalaX
    YYp2 = PicH - 5 + 1
    USCITA.Line (MLeft + XXp1, MTop + YYp1)-(MLeft + XXp2, MTop + YYp2), QBColor(COLORE)
    XXp2 = (ZLMAX - 10 - ZeroPezzoX) * ScalaX
    YYp2 = PicH - 5 - 1
    USCITA.Line (MLeft + XXp1, MTop + YYp1)-(MLeft + XXp2, MTop + YYp2), QBColor(COLORE)
    COLORE = 9
    XXp1 = (ZLMIN - LL1 / 2 - ZeroPezzoX) * ScalaX
    YYp1 = PicH / 2 - 0.5 * USCITA.TextHeight("X")
    XXp2 = (ZLMIN + LL1 / 2 - ZeroPezzoX) * ScalaX
    YYp2 = PicH / 2 + 0.5 * USCITA.TextHeight("X")
    USCITA.Line (MLeft + XXp1, MTop + YYp1)-(MLeft + XXp2, MTop + YYp2), QBColor(COLORE), BF
    COLORE = 9
    XXp1 = (ZLMAX - LL1 / 2 - ZeroPezzoX) * ScalaX
    YYp1 = PicH / 2 - 0.5 * USCITA.TextHeight("X")
    XXp2 = (ZLMAX + LL1 / 2 - ZeroPezzoX) * ScalaX
    YYp2 = PicH / 2 + 0.5 * USCITA.TextHeight("X")
    USCITA.Line (MLeft + XXp1, MTop + YYp1)-(MLeft + XXp2, MTop + YYp2), QBColor(COLORE), BF
    stmp = ""
    USCITA.CurrentX = XXp1
    USCITA.CurrentY = YYp1
    USCITA.Print stmp
  End If
  '
  COLORE = 12
  USCITA.DrawWidth = 2
  USCITA.DrawStyle = 0
  For j = 0 To 1
    For i = 0 To 9
      'CALCOLO RIGHE IN FUNZIONE DELLA POSIZIONE INIZIALE
      k = 0
      For L = 1 To i
        If (Abs(ZI(j, L) - ZI(j, i)) <= LL0) And (tC(j, i) > 0) Then k = k + 1
      Next L
      If (tC(j, i) > 0) And (Sc(j, i) > 0) Then
        'LUNGHEZZA TOTALE RETTIFICATA
        XXp1 = (ZI(j, i) - LL0 / 2 - ZeroPezzoX) * ScalaX
        YYp1 = -(DD(j, i) / 2 - ZeroPezzoY) * ScalaY + (1 + 2 * j) * PicH / 4
        'XXp2 = (ZI(j, i) + LL0 / 2 + NS(j, i) * LS(j, i) - ZeroPezzoX) * ScalaX
        XXp2 = (ZI(j, i) + LL0 / 2 + LS(j, i) - ZeroPezzoX) * ScalaX
        YYp2 = -(-DD(j, i) / 2 - ZeroPezzoY) * ScalaY + (1 + 2 * j) * PicH / 4
        USCITA.Line (MLeft + XXp1, MTop + YYp1)-(MLeft + XXp2, MTop + YYp2), QBColor(15), BF
        USCITA.Line (MLeft + XXp1, MTop + YYp1)-(MLeft + XXp2, MTop + YYp2), QBColor(COLORE), B
        'RAPPRESENTAZIONE MOLA
        XXp1 = (ZI(j, i) - LL0 / 2 - ZeroPezzoX) * ScalaX
        YYp1 = USCITA.TextHeight("X") * 2 + j * PicH / 2
        XXp2 = (ZI(j, i) + LL0 / 2 - ZeroPezzoX) * ScalaX
        YYp2 = -(DD(j, i) / 2 - ZeroPezzoY) * ScalaY + (1 + 2 * j) * PicH / 4
        USCITA.Line (MLeft + XXp1, MTop + YYp1)-(MLeft + XXp2, MTop + YYp2), QBColor(11), BF
        USCITA.Line (MLeft + XXp1, MTop + YYp1)-(MLeft + XXp2, MTop + YYp2), QBColor(0), B
        'INDICE DELLA SEQUENZA
        stmp = " s" & Format$(Sc(j, i), "#0") & " "
        USCITA.CurrentX = MLeft + (ZI(j, i) - (LL0 / 2) * (3 - 2 * Sc(j, i)) - ZeroPezzoX) * ScalaX - USCITA.TextWidth(stmp) / 2
        USCITA.CurrentY = MTop + USCITA.TextHeight("X") * 1 + j * PicH / 2
        USCITA.Print stmp
        'POSIZIONE LUNETTA
        If (ZL(j, i) > 0) Then
          'LUNETTA FISSA
          XXp1 = (ZL(j, i) - ZeroPezzoX) * ScalaX
          YYp1 = -(-DD(j, i) / 2 - ZeroPezzoY) * ScalaY + (1 + 2 * j) * PicH / 4
          XXp2 = (ZL(j, i) - ZeroPezzoX) * ScalaX
          YYp2 = -(-DD(j, i) / 2 - 30 - ZeroPezzoY) * ScalaY + (1 + 2 * j) * PicH / 4
          USCITA.Line (MLeft + XXp1, MTop + YYp1)-(MLeft + XXp2, MTop + YYp2), QBColor(0)
          'RAPPRESENTAZIONE FRECCIA VERTICALE
          XXp1 = (ZL(j, i) - ZeroPezzoX) * ScalaX
          YYp1 = -(-DD(j, i) / 2 - ZeroPezzoY) * ScalaY + (1 + 2 * j) * PicH / 4
          XXp2 = (ZL(j, i) - 5 - ZeroPezzoX) * ScalaX
          YYp2 = -(-DD(j, i) / 2 - 5 - ZeroPezzoY) * ScalaY + (1 + 2 * j) * PicH / 4
          USCITA.Line (MLeft + XXp1, MTop + YYp1)-(MLeft + XXp2, MTop + YYp2), QBColor(0)
          XXp1 = (ZL(j, i) - ZeroPezzoX) * ScalaX
          YYp1 = -(-DD(j, i) / 2 - ZeroPezzoY) * ScalaY + (1 + 2 * j) * PicH / 4
          XXp2 = (ZL(j, i) + 5 - ZeroPezzoX) * ScalaX
          YYp2 = -(-DD(j, i) / 2 - 5 - ZeroPezzoY) * ScalaY + (1 + 2 * j) * PicH / 4
          USCITA.Line (MLeft + XXp1, MTop + YYp1)-(MLeft + XXp2, MTop + YYp2), QBColor(0)
          'INDICE DEL DIAMETRO
          stmp = " D" & Format$(i + 1, "#0") & " "
          USCITA.CurrentX = MLeft + (ZL(j, i) - ZeroPezzoX) * ScalaX - USCITA.TextWidth(stmp) / 2
          USCITA.CurrentY = MTop - (-DD(j, i) / 2 - 30 * (k + 1) - ZeroPezzoY) * ScalaY + (1 + 2 * j) * PicH / 4
          USCITA.Print stmp
        End If
        'NOME DEL DIAMETRO
        stmp = " D" & Format$(i + 1, "#0") & " "
        USCITA.CurrentX = MLeft + (ZI(j, i) - ZeroPezzoX) * ScalaX - USCITA.TextWidth(stmp) / 2
        USCITA.CurrentY = MTop - (DD(j, i) / 2 - ZeroPezzoY) * ScalaY + (1 + 2 * j) * PicH / 4 - 1 * USCITA.TextHeight("X") * 1.1
        USCITA.Print stmp
        'TIPO CICLO
        stmp = Chr$(64 + tC(j, i))
        USCITA.CurrentX = MLeft + (ZI(j, i) - ZeroPezzoX) * ScalaX - USCITA.TextWidth(stmp) / 2
        USCITA.CurrentY = MTop - (DD(j, i) / 2 - ZeroPezzoY) * ScalaY + (1 + 2 * j) * PicH / 4 - 2 * USCITA.TextHeight("X") * 1.1
        USCITA.Print stmp
      End If
    Next i
  Next j
  '
  'VISUALIZZAZIONE VITE: CONTORNO
  COLORE = 0
  USCITA.DrawWidth = 1
  USCITA.DrawStyle = 2
  For j = 0 To 1
    XXp1 = (ZIV - ZeroPezzoX) * ScalaX
    YYp1 = -(DDV / 2 - ZeroPezzoY) * ScalaY + (1 + 2 * j) * PicH / 4
    XXp2 = (ZIV + LLV - ZeroPezzoX) * ScalaX
    YYp2 = -(-DDV / 2 - ZeroPezzoY) * ScalaY + (1 + 2 * j) * PicH / 4
    USCITA.Line (MLeft + XXp1, MTop + YYp1)-(MLeft + XXp2, MTop + YYp2), QBColor(COLORE), B
  Next j
  '
  'VISUALIZZAZIONE VITE: FILETTO
  COLORE = 0
  USCITA.DrawWidth = 1
  USCITA.DrawStyle = 0
  For j = 0 To 1
    For L = 0 To NGV - 1
      XXp1 = (ZIV + PAV * L - ZeroPezzoX) * ScalaX
      YYp1 = -(DDV / 2 - ZeroPezzoY) * ScalaY + (1 + 2 * j) * PicH / 4
      XXp2 = (ZIV + PAV * L + PAV / 2 - ZeroPezzoX) * ScalaX
      YYp2 = -(-DDV / 2 - ZeroPezzoY) * ScalaY + (1 + 2 * j) * PicH / 4
      USCITA.Line (MLeft + XXp1, MTop + YYp1)-(MLeft + XXp2, MTop + YYp2), QBColor(COLORE)
    Next L
  Next j
  '
  'VISUALIZZAZIONE MOLA TERMINALE
  If (RR3 > 0) And (ZI2 > 0) And (DD2 > 0) And (PP2 > 0) Then
  For j = 0 To 1
    COLORE = 12
    USCITA.DrawWidth = 1
    USCITA.DrawStyle = 0
    XXp1 = (ZI2 - ZeroPezzoX) * ScalaX
    YYp1 = -(DD2 / 2 + 50 - ZeroPezzoY) * ScalaY + (1 + 2 * j) * PicH / 4
    XXp2 = (ZI2 + LL2 - ZeroPezzoX) * ScalaX
    YYp2 = -(DD2 / 2 - PP2 - ZeroPezzoY) * ScalaY + (1 + 2 * j) * PicH / 4
    USCITA.Line (MLeft + XXp1, MTop + YYp1)-(MLeft + XXp2, MTop + YYp2), QBColor(COLORE), BF
    USCITA.DrawWidth = 2
    USCITA.Line (MLeft + XXp1, MTop + YYp1)-(MLeft + XXp2, MTop + YYp2), QBColor(0), B
    'RASAMENTO MOLA
    stmp = frmt(ZI2, 3)
    USCITA.CurrentX = MLeft + (ZI2 - ZeroPezzoX) * ScalaX
    USCITA.CurrentY = MTop - (DD2 / 2 + 50 - ZeroPezzoY) * ScalaY + (1 + 2 * j) * PicH / 4 - 1 * USCITA.TextHeight("X") * 1.1
    USCITA.Print stmp
    'DISTANZA MOLA-TERMINALE VITE
    ftmp = ZI2 - ZIV - LLV
    stmp = " " & frmt(ftmp, 3) & " "
    USCITA.CurrentX = MLeft + (ZI2 - ZeroPezzoX) * ScalaX - USCITA.TextWidth(stmp) / 2
    USCITA.CurrentY = MTop - (DDV / 2 - ZeroPezzoY) * ScalaY + (1 + 2 * j) * PicH / 4 + 2 * USCITA.TextHeight("X") * 1.1
    USCITA.Print stmp
    'TERMINALE VITE
    COLORE = 0
    USCITA.DrawWidth = 1
    XXp1 = (ZIV + LLV - ZeroPezzoX) * ScalaX
    YYp1 = -(0 - ZeroPezzoY) * ScalaY + (1 + 2 * j) * PicH / 4
    XXp2 = (ZIV + LLV - ZeroPezzoX) * ScalaX
    YYp2 = -(-DDV * 1.25 - ZeroPezzoY) * ScalaY + (1 + 2 * j) * PicH / 4
    USCITA.Line (MLeft + XXp1, MTop + YYp1)-(MLeft + XXp2, MTop + YYp2), QBColor(COLORE)
    'RASAMENTO MOLA
    XXp1 = (ZI2 - ZeroPezzoX) * ScalaX
    YYp1 = -(DD2 / 2 - ZeroPezzoY) * ScalaY + (1 + 2 * j) * PicH / 4
    XXp2 = (ZI2 - ZeroPezzoX) * ScalaX
    YYp2 = -(-DDV * 1.25 - ZeroPezzoY) * ScalaY + (1 + 2 * j) * PicH / 4
    USCITA.Line (MLeft + XXp1, MTop + YYp1)-(MLeft + XXp2, MTop + YYp2), QBColor(COLORE)
    'DISTANZA TERMINALE E RASAMENTO MOLA
    XXp1 = (ZIV + LLV - 10 - ZeroPezzoX) * ScalaX
    YYp1 = -(-DDV - ZeroPezzoY) * ScalaY + (1 + 2 * j) * PicH / 4
    XXp2 = (ZI2 + 10 - ZeroPezzoX) * ScalaX
    YYp2 = -(-DDV - ZeroPezzoY) * ScalaY + (1 + 2 * j) * PicH / 4
    USCITA.Line (MLeft + XXp1, MTop + YYp1)-(MLeft + XXp2, MTop + YYp2), QBColor(COLORE)
    'FRECCIA SX
    XXp1 = (ZIV + LLV - 5 - ZeroPezzoX) * ScalaX
    YYp1 = -(-DDV + 5 - ZeroPezzoY) * ScalaY + (1 + 2 * j) * PicH / 4
    XXp2 = (ZIV + LLV - ZeroPezzoX) * ScalaX
    YYp2 = -(-DDV - ZeroPezzoY) * ScalaY + (1 + 2 * j) * PicH / 4
    USCITA.Line (MLeft + XXp1, MTop + YYp1)-(MLeft + XXp2, MTop + YYp2), QBColor(COLORE)
    XXp1 = (ZIV + LLV - 5 - ZeroPezzoX) * ScalaX
    YYp1 = -(-DDV - 5 - ZeroPezzoY) * ScalaY + (1 + 2 * j) * PicH / 4
    XXp2 = (ZIV + LLV - ZeroPezzoX) * ScalaX
    YYp2 = -(-DDV - ZeroPezzoY) * ScalaY + (1 + 2 * j) * PicH / 4
    USCITA.Line (MLeft + XXp1, MTop + YYp1)-(MLeft + XXp2, MTop + YYp2), QBColor(COLORE)
    'FRECCIA DX
    XXp1 = (ZI2 + 5 - ZeroPezzoX) * ScalaX
    YYp1 = -(-DDV + 5 - ZeroPezzoY) * ScalaY + (1 + 2 * j) * PicH / 4
    XXp2 = (ZI2 - ZeroPezzoX) * ScalaX
    YYp2 = -(-DDV - ZeroPezzoY) * ScalaY + (1 + 2 * j) * PicH / 4
    USCITA.Line (MLeft + XXp1, MTop + YYp1)-(MLeft + XXp2, MTop + YYp2), QBColor(COLORE)
    XXp1 = (ZI2 + 5 - ZeroPezzoX) * ScalaX
    YYp1 = -(-DDV - 5 - ZeroPezzoY) * ScalaY + (1 + 2 * j) * PicH / 4
    XXp2 = (ZI2 - ZeroPezzoX) * ScalaX
    YYp2 = -(-DDV - ZeroPezzoY) * ScalaY + (1 + 2 * j) * PicH / 4
    USCITA.Line (MLeft + XXp1, MTop + YYp1)-(MLeft + XXp2, MTop + YYp2), QBColor(COLORE)
  Next j
  End If
  '
  'VISUALIZZAZIONE FORO
  If (ZI3 > 0) Then
    COLORE = 9
    USCITA.DrawWidth = 2
    USCITA.DrawStyle = 0
    For j = 0 To 1
      'CROCE
      XXp1 = (ZI3 - ZeroPezzoX) * ScalaX
      YYp1 = -(DDV / 2 - ZeroPezzoY) * ScalaY + (1 + 2 * j) * PicH / 4
      XXp2 = (ZI3 - ZeroPezzoX) * ScalaX
      YYp2 = -(-DDV / 2 - ZeroPezzoY) * ScalaY + (1 + 2 * j) * PicH / 4
      USCITA.Line (MLeft + XXp1, MTop + YYp1)-(MLeft + XXp2, MTop + YYp2), QBColor(COLORE)
      XXp1 = (ZI3 - DDV / 2 - ZeroPezzoX) * ScalaX
      YYp1 = -(0 - ZeroPezzoY) * ScalaY + (1 + 2 * j) * PicH / 4
      XXp2 = (ZI3 + DDV / 2 - ZeroPezzoX) * ScalaX
      YYp2 = -(0 - ZeroPezzoY) * ScalaY + (1 + 2 * j) * PicH / 4
      USCITA.Line (MLeft + XXp1, MTop + YYp1)-(MLeft + XXp2, MTop + YYp2), QBColor(COLORE)
      'VALORE DI RIFERIMENTO
      stmp = " " & frmt(ZI3, 3) & " "
      USCITA.CurrentX = MLeft + (ZI3 - ZeroPezzoX) * ScalaX - USCITA.TextWidth(stmp) / 2
      USCITA.CurrentY = MTop - (DDV / 2 - ZeroPezzoY) * ScalaY + (1 + 2 * j) * PicH / 4 - 1 * USCITA.TextHeight("X") * 1.1
      USCITA.Print stmp
    Next j
    COLORE = 12
    USCITA.DrawWidth = 2
    USCITA.DrawStyle = 0
    For j = 0 To 1
      'CERCHIO
      XXp1 = (ZI3 - ZeroPezzoX) * ScalaX
      YYp1 = -(0 - ZeroPezzoY) * ScalaY + (1 + 2 * j) * PicH / 4
      USCITA.Circle (MLeft + XXp1, MTop + YYp1), (DDV / 2) * ScalaX, QBColor(COLORE)
    Next j
  End If
  USCITA.DrawWidth = 1
  USCITA.DrawStyle = 0
  '
Exit Sub

errVISUALIZZAZIONE_RDE_CONICHE:
  WRITE_DIALOG "Sub VISUALIZZAZIONE_RDE_CONICHE: ERROR -> " & Err
Resume Next

End Sub

Private Function LEGGI_PARAMETRI_MOLA_CONICA() As String
'
Dim NomeParametriMola As String
Dim LETTERA_MOLA      As String * 1
'
On Error Resume Next
    '
    LEGGI_PARAMETRI_MOLA_CONICA = ""
    NomeParametriMola = UCase$(ActualGroup)
    LETTERA_MOLA = Right(NomeParametriMola, 1)
      If (LETTERA_MOLA = "A") Then
    LEGGI_PARAMETRI_MOLA_CONICA = Lp_Str("CODICE_M1")   '<-------------
    R37 = Lp("MO1R[0,37]"): R38 = Lp("MO1R[0,38]"): R45 = Lp("MO1R[0,45]"): R55 = Lp("MO1R[0,55]")
    R41 = Lp("MO1R[0,41]"): R43 = Lp("MO1R[0,43]"): R44 = Lp("MO1R[0,44]"): R46 = Lp("MO1R[0,46]"): R47 = Lp("MO1R[0,47]"): R48 = Lp("MO1R[0,48]"): R49 = Lp("MO1R[0,49]")
    R51 = Lp("MO1R[0,51]"): R53 = Lp("MO1R[0,53]"): R54 = Lp("MO1R[0,54]"): R56 = Lp("MO1R[0,56]"): R57 = Lp("MO1R[0,57]"): R58 = Lp("MO1R[0,58]"): R59 = Lp("MO1R[0,59]")
  ElseIf (LETTERA_MOLA = "B") Then
    LEGGI_PARAMETRI_MOLA_CONICA = Lp_Str("CODICE_M2")   '<-------------
    R37 = Lp("MO2R[0,37]"): R38 = Lp("MO2R[0,38]"): R45 = Lp("MO2R[0,45]"): R55 = Lp("MO2R[0,55]")
    R41 = Lp("MO2R[0,41]"): R43 = Lp("MO2R[0,43]"): R44 = Lp("MO2R[0,44]"): R46 = Lp("MO2R[0,46]"): R47 = Lp("MO2R[0,47]"): R48 = Lp("MO2R[0,48]"): R49 = Lp("MO2R[0,49]")
    R51 = Lp("MO2R[0,51]"): R53 = Lp("MO2R[0,53]"): R54 = Lp("MO2R[0,54]"): R56 = Lp("MO2R[0,56]"): R57 = Lp("MO2R[0,57]"): R58 = Lp("MO2R[0,58]"): R59 = Lp("MO2R[0,59]")
  ElseIf (LETTERA_MOLA = "C") Then
    LEGGI_PARAMETRI_MOLA_CONICA = Lp_Str("CODICE_M3")   '<-------------
    R37 = Lp("MO3R[0,37]"): R38 = Lp("MO3R[0,38]"): R45 = Lp("MO3R[0,45]"): R55 = Lp("MO3R[0,55]")
    R41 = Lp("MO3R[0,41]"): R43 = Lp("MO3R[0,43]"): R44 = Lp("MO3R[0,44]"): R46 = Lp("MO3R[0,46]"): R47 = Lp("MO3R[0,47]"): R48 = Lp("MO3R[0,48]"): R49 = Lp("MO3R[0,49]")
    R51 = Lp("MO3R[0,51]"): R53 = Lp("MO3R[0,53]"): R54 = Lp("MO3R[0,54]"): R56 = Lp("MO3R[0,56]"): R57 = Lp("MO3R[0,57]"): R58 = Lp("MO3R[0,58]"): R59 = Lp("MO3R[0,59]")
  Else
    LEGGI_PARAMETRI_MOLA_CONICA = Lp_Str("CODICE_MOLA") '<-------------
    R37 = Lp("R[37]"): R38 = Lp("R[38]"): R45 = Lp("R[45]"): R55 = Lp("R[55]")
    R41 = Lp("R[41]"): R43 = Lp("R[43]"): R44 = Lp("R[44]"): R46 = Lp("R[46]"): R47 = Lp("R[47]"): R48 = Lp("R[48]"): R49 = Lp("R[49]")
    R51 = Lp("R[51]"): R53 = Lp("R[53]"): R54 = Lp("R[54]"): R56 = Lp("R[56]"): R57 = Lp("R[57]"): R58 = Lp("R[58]"): R59 = Lp("R[59]")
  End If
  LEGGI_PARAMETRI_MOLA_CONICA = UCase$(Trim$(LEGGI_PARAMETRI_MOLA_CONICA))
  If (LEGGI_PARAMETRI_MOLA_CONICA = "NONE") Then LEGGI_PARAMETRI_MOLA_CONICA = ""
  '
End Function

Sub SCRITTURA_CICLI_RETTIFICA_DIAMETRI()
'
On Error Resume Next
  '
  'CICLI FISSI PER RETTIFICA DIAMETRI ESTERNI
  Call SP("CYC[0,300]", "400", False)
  Call SP("CYC[0,301]", "400", False)
  Call SP("CYC[0,210]", "28", False)
  Call SP("CYC[0,401]", "0.15", False):  Call SP("CYC[0,406]", "0.03", False):  Call SP("CYC[0,411]", "0", False):  Call SP("CYC[0,416]", "0", False)
  Call SP("CYC[0,402]", "0.015", False): Call SP("CYC[0,407]", "0.005", False): Call SP("CYC[0,412]", "0", False):  Call SP("CYC[0,417]", "0", False)
  Call SP("CYC[0,403]", "3", False):     Call SP("CYC[0,408]", "3", False):     Call SP("CYC[0,413]", "2", False):  Call SP("CYC[0,418]", "0", False)
  Call SP("CYC[0,404]", "20", False):    Call SP("CYC[0,409]", "15", False):    Call SP("CYC[0,414]", "15", False): Call SP("CYC[0,419]", "0", False)
  Call SP("CYC[0,421]", "0.25", False):  Call SP("CYC[0,426]", "0.05", False):  Call SP("CYC[0,431]", "0", False):  Call SP("CYC[0,436]", "0", False)
  Call SP("CYC[0,422]", "0.015", False): Call SP("CYC[0,427]", "0.005", False): Call SP("CYC[0,432]", "0", False):  Call SP("CYC[0,437]", "0", False)
  Call SP("CYC[0,423]", "3", False):     Call SP("CYC[0,428]", "3", False):     Call SP("CYC[0,433]", "3", False):  Call SP("CYC[0,438]", "0", False)
  Call SP("CYC[0,424]", "20", False):    Call SP("CYC[0,429]", "15", False):    Call SP("CYC[0,434]", "15", False): Call SP("CYC[0,439]", "0", False)
  Call SP("CYC[0,441]", "0.35", False):  Call SP("CYC[0,446]", "0.05", False):  Call SP("CYC[0,451]", "0", False):  Call SP("CYC[0,456]", "0", False)
  Call SP("CYC[0,442]", "0.01", False):  Call SP("CYC[0,447]", "0.005", False): Call SP("CYC[0,452]", "0", False):  Call SP("CYC[0,457]", "0", False)
  Call SP("CYC[0,443]", "2", False):     Call SP("CYC[0,448]", "2", False):     Call SP("CYC[0,453]", "3", False):  Call SP("CYC[0,458]", "0", False)
  Call SP("CYC[0,444]", "20", False):    Call SP("CYC[0,449]", "15", False):    Call SP("CYC[0,454]", "15", False): Call SP("CYC[0,459]", "0", False)
  Call SP("CYC[0,461]", "0.13", False):  Call SP("CYC[0,466]", "0.02", False):  Call SP("CYC[0,471]", "0", False):  Call SP("CYC[0,476]", "0", False)
  Call SP("CYC[0,462]", "0.005", False): Call SP("CYC[0,467]", "0.002", False): Call SP("CYC[0,472]", "0", False):  Call SP("CYC[0,477]", "0", False)
  Call SP("CYC[0,463]", "2", False):     Call SP("CYC[0,468]", "2", False):     Call SP("CYC[0,473]", "2", False):  Call SP("CYC[0,478]", "0", False)
  Call SP("CYC[0,464]", "20", False):    Call SP("CYC[0,469]", "15", False):    Call SP("CYC[0,474]", "15", False): Call SP("CYC[0,479]", "0", False)
  '
End Sub
