Attribute VB_Name = "CREATORI"
Option Explicit
'
'DefDbl A-Z
'
Dim Session As IMCDomain
'
Global Const vd = 100              'dimensione vettori
'
Global Aux    As Double
Global Aux1   As Double
'
Global Et$                         ' Codice progetto
Global Tp$                         ' "O" "N" Topping
'
Global Mn       As Double          ' Cremagliera: Modulo normale
Global AlfaDeg  As Double          ' -----------: Angolo di pressione (in gradi)
Global ApFl     As Double          ' -----------: ------------------- (in radianti)
Global Sw       As Double          ' -----------: Spessore sulla linea primitiva
Global Hkw      As Double          ' -----------: Addendum
Global HW       As Double          ' -----------: Altezza fianco
Global Esfm     As Double          ' -----------: Spessore di testa (per verifica)
Global Rtesta   As Double          ' -----------: Raggio di testa
Global Xrtesta  As Double          ' -----------: Centro Rtesta / Asse Pieno
Global Yrtesta  As Double          ' -----------: ------------- / linea primitiva
Global Rfondo   As Double          ' -----------: Raggio di fondo
Global XrFondo  As Double          ' -----------: Centro Rfondo / asse Pieno
Global Yrfondo  As Double          ' -----------: Centro Rfondo / linea Primit
Global Hr       As Double          ' -----------: Alt.totale fianco ( Dato )
Global Yfond    As Double          ' -----------: ------------------------- ( Calcolato )
'
Global Pr$                         ' -----------: "O" /  "N"   Protuberanza
Global OC       As Double          ' -----------: Entita protuberanza
Global AngProt  As Double          ' -----------: Angolo tra AlfaDeg e ApProtuD (in gradi)
Global ApProtuD As Double          ' -----------: Ang.Press. protuberanza (in gradi)
Global ApProtu  As Double          ' -----------: ----------------------- (in radianti)
Global Hprot    As Double          ' -----------: Altezza protuberanza / testa
Global XiProt   As Double          ' -----------: Inizio Protu / asse pieno
Global YiProt   As Double          ' -----------: ------------ / linea primitiva
Global Hpiatt   As Double          ' -----------: Stacco Del Piattino
Global E63      As Double          ' piattino come RCR
Global Rpiatt   As Double          ' -----------: Raggio Protu/Piatt ( = Rtesta )
Global XrPiatt  As Double          ' -----------: Rpiatt : Centro / Asse Pieno
Global Yrpiatt  As Double          ' -----------: Rpiatt : Centro / linea primitiva
Global HrPiatt  As Double          ' -----------: Rpiatt : Centro / Testa
Global XiPiatt  As Double          ' -----------: Inizio Piattino / asse pieno
Global Yipiatt  As Double          ' -----------: --------------- / linea primitiva
'
' testa con 3 raggi                ( sempre con protuberanza )
Global TreRaggi$                   ' -----------: "O" "N"  Testa CR con tre raggi
Global TreRag2  As Double          ' -----------: Raggio di testa medio per 3 raggi
Global XtreRag2 As Double          ' -----------: ---------------------  / Asse pieno
Global YtreRag2 As Double          ' -----------: ---------------------  / linea primitiva
Global TreRag3  As Double          ' -----------: Raggio di testa / protu per 3 Raggi
Global XtreRag3 As Double          ' -----------: ----------------------- / Asse Pieno
Global YtreRag3 As Double          ' -----------: ----------------------- / linea primitiva
'
' Semi topping
Global St$                         ' -----------: "O" "N" Semitopping
Global GammaDeg As Double          ' -----------: Ang.Pressione semitopping (in gradi)
Global ApSt     As Double          ' -----------: ------------------------- (in radianti)
Global Rsemit   As Double          ' -----------: Raggio tra fianco e semitopping
Global Xrsemit  As Double          ' -----------: Centro Rsemit / asse pieno
Global Yrsemit  As Double          ' -----------: Centro Rsemit / linea prim.
Global Xist     As Double          ' -----------: Inizio Semi-topping / asse pieno
Global Yist     As Double          ' -----------: Inizio Semi-topping / linea primitiva
'
' Rastremazione di testa
Global Rastt$                      ' -----------: "O" "N" Rastremazione Testa
Global Hrastrt   As Double         ' -----------: Altezza Rastremazione Testa / testa
Global AngRastrT As Double         ' -----------: Ang.Press.Rastremazione Testa (in gradi)
Global ApRastt   As Double         ' -----------: ----------------------------- (in radianti)
Global XiRastt   As Double         ' -----------: Inizio Rastremazione / asse pieno
Global YiRastt   As Double         ' -----------: -------------------- / linea primitiva
'
' Rastremazione di piede
Global Rastp$                      ' -----------: "O" "N" : Rastremazione Piede
Global Rastrp   As Double          ' -----------: Entita rastremazione di Piede su "fine fianco"
Global Hrastrp  As Double          ' -----------: Altezza Rastrem.Piede rispetto a "fine fianco"
Global E82      As Double          ' -----------: Stacco rastremazione di piede rispetto a testa
Global E83      As Double          ' -----------: Ang.Pressione Rastrem.piede (in gradi)
Global ApRastp  As Double          ' -----------: Ang.Pressione Rastrem.piede (in radianti)
Global XiRastp  As Double          ' -----------: Inizio rastrem.piede / asse pieno
Global YiRastp  As Double          ' -----------: Inizio rastrem.piede / linea primitiva
'
Global NFil         As Long        ' CREATORE : Numero Filetti
Global InFilD       As Double      ' -------- : Inclinazione Filetti in gradi
Global InFil        As Double      ' -------- : Inclinazione Filetti in radianti
Global AngProjD     As Double      ' -------- : Inclinazione sur projecteur in gradi
Global AngProj      As Double      ' -------- : Inclinazione sur projecteur in radianti
Global PasHel       As Double      ' -------- : Passo Spirale
Global DestCr       As Double      ' -------- : Diametro esterno
Global PasAxial     As Double      ' -------- : Passo Assiale
Global DprimCr      As Double      ' -------- : Diametro primitivo
Global RprimCr      As Double
Global SottIntaglio As Double      ' ------ : Valore del sottintaglio in mm
Global Nscan        As Long        ' ------ :
Global Camma        As Double      ' ------
Global Senso        As Double      ' senso elica filetti ( 1=dx ; -1=sx)
Global Dsf          As Double      ' diametro sonda
Global CamCor       As Double
Global YSCP         As Double      ' quota Y per portare il centro sonda sull'asse pezzo
Global DisTagliente As Double      ' Distanza dal tagliente della sezione di controllo
'
Global Xfm(2, vd)   As Double      ' PROFIL CREMAILLERE PAR POINTS : Xx / Asse pieno
Global RFM(2, vd)   As Double      ' ----------------------------- : Raggio / asse CR
Global ApFm(2, vd)  As Double      ' ----------------------------- : Angle de Pression
'
Global Xfma(2, vd)  As Double      ' PROFIL AXIAL CR PAR POINTS : Xx / Asse pieno
Global Rfma(2, vd)  As Double      ' -------------------------- : Raggio / asse CR
Global ApFma(2, vd) As Double      ' -------------------------- : Angle de Pression
'
Global XX As Double
Global YY As Double
'
Global NbpTotal  As Integer   ' Nombre de points 1/2 profil
'
Global NInRt     As Integer   ' indice punto inizio raggio di testa
Global NFinRt    As Integer   '              fine raggio di testa
Global NInTesta  As Integer   '              inizio zona testa
Global NFinTesta As Integer   '              fine zona testa
Global NInRastT  As Integer   '              inizio rastremazione testa
Global NFinRastT As Integer   '              fine rastremazione testa
Global NInFia    As Integer   '              inizio fianco
Global NFinFia   As Integer   '              fine fianco
Global NInRastP  As Integer   '              inizio rastremazione piede
Global NFinRastP As Integer   '              fine rastremazione piede
Global NInSt     As Integer   '              inizio semitopping
Global NFinSt    As Integer   '              fine semitopping
Global NFinRf    As Integer   '              fine raggio di fondo
Global Ntot      As Integer   '              su diametro interno
'
Global NpTESTA   As Integer   'numero punti per calcolo testa
Global NpRtesta  As Integer   '              raggio di testa
Global NpPia     As Integer   '              piattino
Global NpRPia    As Integer   '              raccordo piattino-protub.
Global NpProtu   As Integer   '              protuberanza
Global NpRastT   As Integer   '              rastremazione testa
Global NpFIANCO  As Integer   '              fianco
Global NpRastP   As Integer   '              rastremazione piede
Global NpRSemit  As Integer   '              raggio semitopping
Global NpSemit   As Integer   '              semitopping
Global NpRfondo  As Integer   '              raggio di fondo
Global Npfondo   As Integer   '              fondo
'
Global flagErrore As Boolean
'
Global FlagFia   As Long      ' 1 = fianco dx ; -1 = fianco sx
Global Fianco    As Long      ' 1 = fianco dx ;  2 = fianco sx


Sub VISUALIZZAZIONE_PROFILO_MOLA_CREATORI(ByVal Scala As Single, ByVal SiStampa As String)
''
'Dim DBB As Database
'Dim RSS As Recordset
''
'Dim stmp As String
'Dim Atmp As String * 255
'Dim i    As Integer
''
'Dim X1 As Single
'Dim X2 As Single
'Dim Y1 As Single
'Dim Y2 As Single
''
'Dim PicH As Single
'Dim PicW As Single
''
'Dim dx As Single
'Dim a$
''
'Dim xx(40) As Double
'Dim YY(40) As Double
'Dim RR(10) As Double
''
'Dim R235 As Double
'Dim R238 As Double
''
'Dim R242 As Double
'Dim R228 As Double
''
'Dim R66 As Double
'Dim R67 As Double
''
''LOCALI
'Dim R300 As Double
'Dim R301 As Double
'Dim R302 As Double
'Dim R303 As Double
''
'Dim R304 As Double
'Dim R305 As Double
'Dim R323 As Double
''
'Dim R306 As Double
''
'Dim R309 As Double
'Dim R308 As Double
'Dim R307 As Double
''
'Dim R319 As Double
'Dim R318 As Double
''
'Dim R138 As Double
'Dim R136 As Double
''
'Dim A1     As Double
'Dim A2     As Double
'Dim ANGOLO As Double
'Dim DIST   As Double
'Dim PMC    As Double
'Dim Xc     As Double
'Dim YC     As Double
''
'Dim R320 As Double
''
'Dim R321 As Double
'Dim R322 As Double
''
'Dim R314 As Double
'Dim R315 As Double
'Dim R316 As Double
'Dim R317 As Double
''
'Dim R310 As Double
'Dim R311 As Double
'Dim R312 As Double
'Dim R313 As Double
''
'Dim R20 As Double
'Dim R96 As Double
'Dim R98 As Double
''
'Dim R193 As Double
'Dim R5 As Double
'Dim R206 As Double
'Dim R207 As Double
''
'Dim R36 As Double
'Dim R37 As Double
''
'Dim R38 As Double
'Dim R39 As Double
''
'Dim R60 As Double
''
''ZONA 1
'Dim R195 As Double
'Dim R178 As Double
'Dim R196 As Double
'Dim R186 As Double
''
''ZONA 2
'Dim R205 As Double
'Dim R179 As Double
'Dim R197 As Double
'Dim R187 As Double
''
''FIANCO 1
'Dim R41 As Double
'Dim R42 As Double
'Dim R43 As Double
'Dim R44 As Double
'Dim R45 As Double
'Dim R46 As Double
'Dim R47 As Double
'Dim R48 As Double
'Dim R49 As Double
'Dim R173 As Double
'Dim R31 As Double
'Dim R40 As Double
''
'Dim R61 As Double
'Dim R62 As Double
'Dim R63 As Double
'Dim R64 As Double
'Dim R65 As Double
''
'Dim R80 As Double
'Dim R81 As Double
''
'Dim R237 As Double
''
'Dim R82 As Double
'Dim R83 As Double
''
'Dim R137 As Double
'Dim R171 As Double
'Dim R106 As Double
''
''FIANCO 2
'Dim R51 As Double
'Dim R52 As Double
'Dim R53 As Double
'Dim R54 As Double
'Dim R55 As Double
'Dim R56 As Double
'Dim R57 As Double
'Dim R58 As Double
'Dim R59 As Double
'Dim R174 As Double
'Dim R32 As Double
'Dim R50 As Double
''
'Dim R71 As Double
'Dim R72 As Double
'Dim R73 As Double
'Dim R74 As Double
'Dim R75 As Double
''
'Dim R84 As Double
'Dim R85 As Double
''
'Dim R247 As Double
''
'Dim R86 As Double
'Dim R87 As Double
''
'Dim R227 As Double
'Dim R172 As Double
'Dim R107 As Double
''
'Dim TipoCarattere As String
'Dim ZeroPezzoX As Single
'Dim ZeroPezzoY As Single
''
'  '
'  OEM1.OEM1chkINPUT(0).Visible = False
'  OEM1.OEM1chkINPUT(1).Visible = False
'  '
'  OEM1.OEM1lblOX.Visible = False
'  OEM1.OEM1lblOY.Visible = False
'  OEM1.OEM1txtOX.Visible = False
'  OEM1.OEM1txtOY.Visible = False
'  '-------------------------------------------------------------
'  stmp = GetInfo("DIS0", "PicW", Path_LAVORAZIONE_INI)
'  If val(stmp) > 0 Then
'    OEM1.OEM1PicCALCOLO.ScaleWidth = val(stmp)
'  Else
'    OEM1.OEM1PicCALCOLO.ScaleWidth = 190
'  End If
'  stmp = GetInfo("DIS0", "PicH", Path_LAVORAZIONE_INI)
'  If val(stmp) > 0 Then
'    OEM1.OEM1PicCALCOLO.ScaleHeight = val(stmp)
'  Else
'    OEM1.OEM1PicCALCOLO.ScaleHeight = 95
'  End If
'  '-------------------------------------------------------------
'  PicH = OEM1.OEM1PicCALCOLO.ScaleHeight
'  PicW = OEM1.OEM1PicCALCOLO.ScaleWidth
'  '
'  If OEM1.lblINP(0).Visible = False Then OEM1.lblINP(0).Visible = True
'  If OEM1.txtINP(0).Visible = False Then OEM1.txtINP(0).Visible = True
'  '
'  'VISUALIZZO LE SCALA
'  i = LoadString(g_hLanguageLibHandle, 1000, Atmp, 255)
'  If i > 0 Then OEM1.lblINP(0).Caption = Left$(Atmp, i)
'  i = LoadString(g_hLanguageLibHandle, 1100, Atmp, 255)
'  If i > 0 Then OEM1.OEM1frameCalcolo.Caption = Left$(Atmp, i)
'  '
'  stmp = GetInfo("DIS0", "ZeroPezzoX", Path_LAVORAZIONE_INI)
'  ZeroPezzoX = val(stmp)
'  stmp = GetInfo("DIS0", "ZeroPezzoY", Path_LAVORAZIONE_INI)
'  ZeroPezzoY = val(stmp)
'  '
'  If (SiStampa = "Y") Then
'    '
'    OEM1.PicLogo.Picture = LoadPicture(g_chOemPATH & "\" & "Logo2.bmp")
'    Printer.ScaleMode = 6
'    For i = 1 To 2
'      Printer.FontName = TipoCarattere
'      Printer.FontSize = 10
'    Next i
'    Write_Dialog OEM1.OEM1frameCalcolo.Caption & " Printing on " & Printer.DeviceName
'    '
'  Else
'    '
'    OEM1.OEM1PicCALCOLO.Cls
'    Write_Dialog ""
'    '
'  End If
'  '
'  If (OEM1.OEM1Check2.Value = 1) Then
'    Call SCRIVI_ASSI(OEM1.OEM1PicCALCOLO, SiStampa)
'    Call SCRIVI_SCALA_ORIZZONTALE(Scala, OEM1.OEM1PicCALCOLO, SiStampa)
'  End If
'  '
'  'Lettura parametri
'  GoSub LETTURA_PARAMETRI
'  '
'  'controllo parametri
'  GoSub CONTROLLO_PARAMETRI
'  '
'  'Convertire gli angoli in radianti (solo PC)
'  GoSub CONVERSIONE_RADIANTI
'  '
'  'Calcolo Fianco 1
'  GoSub CALCOLO_F1
'  '
'  'Percorso F1: registra su file CNC.TXT
'  GoSub VISUALIZZA_PERCORSO_F1
'  '
'  'Profilo F1: registra su file MOLA1.DXF
'  GoSub SCRIVI_FILE_DXF_FIANCO1
'  '
'  'Calcolo Fianco 2
'  GoSub CALCOLO_F2
'  '
'  'Percorso F2: registra su file CNC.TXT
'  GoSub VISUALIZZA_PERCORSO_F2
'  '
'  'Registra coordinate punti su file PUNTI.TXT
'  GoSub WRTPUNTI
'  '
'  'Profilo mola: registra su file MOLA.DXF
'  GoSub SCRIVI_FILE_DXF_FIANCO2
'  '
'  'Calcolo valori R98 e R96
'  GoSub CALCOLOALTEZZE
'  '
'  If (SiStampa = "Y") Then
'    '
'    'Carattere per titolo
'    For i = 1 To 2
'      Printer.FontName = TipoCarattere
'      Printer.FontSize = 12
'      Printer.FontBold = True
'    Next i
'    'Logo
'    Printer.PaintPicture OEM1.PicLogo, 0, PicH, 20, 20
'    'Nome del documento
'    stmp = OEM1.OEM1frameCalcolo.Caption & "  " & Date & " " & Time
'    Printer.CurrentX = 0 + 25
'    Printer.CurrentY = PicH
'    Printer.Print stmp
'    'Scala
'    stmp = OEM1.lblINP(0).Caption & ": x" & OEM1.txtINP(0).Text
'    Printer.CurrentX = 0 + 25
'    Printer.CurrentY = PicH + 10
'    Printer.Print stmp
'    'Carattere per parametri
'    For i = 1 To 2
'      Printer.FontName = TipoCarattere
'      Printer.FontSize = 10
'      Printer.FontBold = True
'    Next i
'    Printer.EndDoc
'    '
'    Write_Dialog OEM1.OEM1frameCalcolo.Caption & " Printed on " & Printer.DeviceName
'    '
'  End If
'  '
'Exit Sub
'
'CALCOLO_F1:
'  'Punto 6
'  xx(6) = R38 / 2
'  YY(6) = 0
'  ';*************     Punto 5     **************
'  '; Calcolo angolo raccordo fianco-testa R323
'  '; Rastremazione di testa ?
'  If (R81 > 0) Then
'   R323 = R81
'  Else
'   R323 = R41
'  End If
'  If (R45 = 0) Then
'   If (R61 > 0) Or (R62 > 0) Then
'    YY(5) = R46 - R63 - R43             'piu raggi testa, senza prot.
'   Else
'    YY(5) = R48 * (1 - Sin(R323)) - R43 'un solo RT senza protub.
'   End If
'  Else
'   YY(5) = R47 - R43                    'caso con protub.
'  End If
'  'Rastremazione testa ?
'  If (R81 > 0) Then
'   R300 = -YY(5) - (R43 - R80)
'   R301 = R300 * (Tan(R41) - Tan(R81))
'  Else
'   R300 = 0
'   R301 = 0
'  End If
'  xx(5) = xx(6) - Tan(R41) * YY(5) - R301
'  ';*************   Y_Punto 3    ****************
'  '; aggiorno raccordo fianco-testa R323
'  '; Piattino ?
'  If (R63 > 0) Then
'   R323 = R323 + R171
'  End If
'  If (R61 > 0) Or (R62 > 0) Then
'   YY(3) = R46 - R63 - R43      'piu raggi testa
'  Else
'   If ((R45 > 0) And (R63 > 0)) Or (R45 = 0) Then 'piattino
'    YY(3) = R48 * (1 - Sin(R323)) - R43
'   End If
'   If (R45 > 0) And (R63 = 0) Then
'    R316 = xx(5) + R47 * Tan(R323) + R48 * Tan((PG / 2 - R323) / 2) - R45 / Cos(R323)
'    R317 = YY(5) - R47 + R48
'    'TEOREMA DI CARNOT
'    R308 = Sqr((xx(5) - R316) ^ 2 + (YY(5) - R317) ^ 2)
'    If (R308 < R48) Then
'      StopRegieEvents
'      MsgBox "ERROR : R47 too small !"
'      ResumeRegieEvents
'      Exit Sub
'    End If
'    R307 = Sqr(R308 ^ 2 - R48 ^ 2)
'    R323 = Atn((R316 - xx(5)) / (YY(5) - R317))
'    R323 = R323 - FnACS((R308 ^ 2 + R307 ^ 2 - R48 ^ 2) / 2 / R308 / R307)
'    YY(3) = R317 - R48 * Sin(R323)
'    xx(3) = R316 - R48 * Cos(R323)
'   End If
'  End If
'  If (R323 < 0) Then
'    StopRegieEvents
'    MsgBox "ERROR : angolo raccordo testa negative !!"
'    ResumeRegieEvents
'    Exit Sub
'  End If
'  ';*************     Punto 4    ****************
'  YY(4) = YY(3) + R63
'  If R45 > 0 And R63 > 0 Then
'    xx(4) = xx(5) - R45 / Cos(R323) + (YY(5) - YY(4)) * Tan(R323)
'  Else
'    xx(4) = xx(5) + (YY(5) - YY(4)) * Tan(R323)
'  End If
'  ';*************     Punto 23    ****************
'  If (R81 = 0) Then
'   xx(23) = xx(5)
'   YY(23) = YY(5)
'  Else
'   YY(23) = YY(5) + R300
'   xx(23) = xx(6) - YY(23) * Tan(R41)
'   ';bomb. rastr. Testa
'   If (R237 <> 0) Then
'    R235 = Sqr((YY(23) - YY(5)) * (YY(23) - YY(5)) + (xx(23) - xx(5)) * (xx(23) - xx(5))) / 2
'    R238 = (R235 * R235 + R237 * R237) / (2 * R237)   'Raggio bombatura rastr.T
'   Else
'    R238 = 0
'   End If
'  End If
'  ';*************    X_Punto 3    ****************
'  xx(3) = xx(4) + R63 * Tan(R41 + R171)
'  ';*************    Punto 10 (caso 1 RT)    ****************
'  xx(10) = xx(3)
'  YY(10) = YY(3)
'  ';*************     Punto 2    ****************
'  If (R61 > 0) Or (R62 > 0) Then
'   xx(2) = R64
'  Else
'   xx(2) = xx(3) + R48 * Cos(R323)
'  End If
'  YY(2) = -R43
'  '; spostare secondo angolo testa R40
'  If (R61 = 0) Then
'   R304 = R48
'  Else
'   R304 = R61
'  End If
'  R314 = xx(2)
'  R315 = YY(2) + R304
'  xx(2) = R314 - R304 * Sin(R40)
'  YY(2) = R315 - R304 * Cos(R40)
'  ';*************     Punto 31   ****************
'  If (R42 > 0) Then
'   YY(31) = R39 - R43 + R106
'  Else
'   YY(31) = R44 - R43
'  End If
'  xx(31) = 0
'  ';************   Angolo Fianco, punto 24, punto 7   ****************
'  If (R42 > 0) Then
'   R320 = R42
'  Else
'   If (R83 = 0) Then
'    R320 = R41
'   Else
'    R320 = R83
'   End If
'  End If
'  If (R39 = 0) Then
'   YY(7) = R44 - R43 - R49 * (1 - Sin(R320))
'  Else
'   YY(7) = R44 - R43
'  End If
'  If (R83 > 0) Then
'   R302 = YY(7) - (R82 - R43)
'   R303 = R302 * (Tan(R83) - Tan(R41))
'  Else
'   R302 = 0
'   R303 = 0
'  End If
'  YY(24) = YY(7) - R302
'  xx(24) = xx(6) - YY(24) * Tan(R41)
'  xx(7) = xx(24) - R302 * Tan(R83)
'  '; p8=intersezione fianco_fondo
'  '; M1=coeff. ang.fianco
'  '; M2=coeff. ang.fondo
'  R312 = -Tan(R320)
'  If (R60 = 0) Then
'   R313 = -9999999
'  Else
'   R313 = -Tan(PG / 2 - R60)
'  End If
'  R310 = xx(7) - R312 * YY(7)
'  R311 = xx(31) - R313 * YY(31)
'  YY(8) = ((R311 - R310) / (R312 - R313))
'  xx(8) = R312 * YY(8) + R310
'  ';********** Punto 33 , 35 *************
'  R321 = (PG / 2 - R320 - R60) / 2
'  R322 = (PG / 2 - R320 + R60) / 2
'  R309 = R49 / Cos(R321)
'  R318 = xx(8) - R309 * Sin(R322)
'  R319 = YY(8) - R309 * Cos(R322)
'  xx(33) = R318 + R49 * Sin(R60)
'  YY(33) = R319 + R49 * Cos(R60)
'  YY(35) = R319 + R49 * Sin(R320)
'  xx(35) = R318 + R49 * Cos(R320)
'  '; ricalcolo P7 se R39=0
'  If (R39 = 0) Then
'   xx(7) = xx(35)
'   YY(7) = YY(35)
'  End If
'  '; ricalcolo P24 se R82=0
'  If (R82 = 0) Then
'   xx(24) = xx(7)
'   YY(24) = YY(7)
'  End If
'  '; R138 = Raggio bomb. rastr. fondo
'  If (R137 = 0) Then
'   R138 = 0
'  Else
'   R136 = Sqr((xx(7) - xx(24)) * (xx(7) - xx(24)) + (YY(7) - YY(24)) * (YY(7) - YY(24))) / 2
'   R138 = (R136 * R136 + R137 * R137) / (2 * R137)
'  End If
'  '----------------------------------------------------------------------------
'  'SVG: non presente nel PP da qui
'  ';*************     Punto 9 (uscita F1)  ****************
'  If (xx(8) > 0) Then
'   xx(9) = -xx(8)
'  Else
'   xx(9) = xx(8) - 1
'  End If
'  YY(9) = YY(31) - xx(9) * Tan(R60)
'  'SVG: non presente nel PP a qui
'  '----------------------------------------------------------------------------
'  '; ********** Punto C3, P10 ***********
'  If (R61 > 0) Or (R62 > 0) Then
'   If (R62 = 0) Then
'    R305 = R48
'   Else
'    R305 = R62
'   End If
'   R316 = xx(3) + R305 * Cos(R323)
'   R317 = YY(3) + R305 * Sin(R323)
'   ';Calcolo angolo C1-C3
'   R306 = ((R317 + R43) - R304) / (R305 - R304)
'   If (R306 > 1) Then
'    StopRegieEvents
'    MsgBox "ERROR: R46 too large !!"
'    ResumeRegieEvents
'    Exit Sub
'   End If
'   R306 = FnACS(R306)
'   ';C1
'   R314 = R316 + (R304 - R305) * Sin(R306)
'   R315 = R317 + (R304 - R305) * Cos(R306)
'   '; *********** Punto 10   ***************
'   xx(10) = R314 - R304 * Sin(R306)
'   YY(10) = R315 - R304 * Cos(R306)
'   '; *********** Punto 2    ***************
'   xx(2) = R314 - R304 * Sin(R40)
'   YY(2) = R315 - R304 * Cos(R40)
'  End If
'  ';*************     Punto 1 (inizio F1)  ****************
'  xx(1) = R37 / 2
'  YY(1) = YY(2) - (R37 / 2 - xx(2)) * Tan(R40)
'  ';Raggi di bombatura fianco
'  If (R31 <> 0) Then
'   R66 = R65 * (YY(24) - YY(23)) / Cos(R41)
'   YY(21) = YY(23) + R65 * (YY(24) - YY(23))
'   xx(21) = xx(23) - R65 * (YY(24) - YY(23)) * Tan(R41) + R31
'   RR(1) = -(R66 * R66 + R31 * R31) / 2 / R31
'   R67 = (1 - R65) * (YY(24) - YY(23)) / Cos(R41)
'   RR(2) = -(R67 * R67 + R31 * R31) / 2 / R31
'  Else
'   xx(21) = xx(6)
'   YY(21) = YY(6)
'  End If
'  '
'  '; Visualizza coordinate F1
'  'OEM1.OEM1PicCALCOLO.Cls
'  'OEM1.OEM1PicCALCOLO.Print "FIANCO 1"
'  'OEM1.OEM1PicCALCOLO.Print " punto     X         Y"
'  'OEM1.OEM1PicCALCOLO.Print "   1  " & frmt(xx(1), 4) & " " & frmt(YY(1), 4)
'  'OEM1.OEM1PicCALCOLO.Print "   2  " & frmt(xx(2), 4) & " " & frmt(YY(2), 4)
'  'OEM1.OEM1PicCALCOLO.Print "  10  " & frmt(xx(10), 4) & " " & frmt(YY(10), 4)
'  'OEM1.OEM1PicCALCOLO.Print "   3  " & frmt(xx(3), 4) & " " & frmt(YY(3), 4)
'  'OEM1.OEM1PicCALCOLO.Print "   4  " & frmt(xx(4), 4) & " " & frmt(YY(4), 4)
'  'OEM1.OEM1PicCALCOLO.Print "   5  " & frmt(xx(5), 4) & " " & frmt(YY(5), 4)
'  'OEM1.OEM1PicCALCOLO.Print "  23  " & frmt(xx(23), 4) & " " & frmt(YY(23), 4)
'  'OEM1.OEM1PicCALCOLO.Print "   6  " & frmt(xx(6), 4) & " " & frmt(YY(6), 4)
'  'OEM1.OEM1PicCALCOLO.Print "  21  " & frmt(xx(21), 4) & " " & frmt(YY(21), 4)
'  'OEM1.OEM1PicCALCOLO.Print "  24  " & frmt(xx(24), 4) & " " & frmt(YY(24), 4)
'  'OEM1.OEM1PicCALCOLO.Print "   7  " & frmt(xx(7), 4) & " " & frmt(YY(7), 4)
'  'OEM1.OEM1PicCALCOLO.Print "   8  " & frmt(xx(8), 4) & " " & frmt(YY(8), 4)
'  'OEM1.OEM1PicCALCOLO.Print "   9  " & frmt(xx(9), 4) & " " & frmt(YY(9), 4)
'  'OEM1.OEM1PicCALCOLO.Print "  33  " & frmt(xx(33), 4) & " " & frmt(YY(33), 4)
'  'OEM1.OEM1PicCALCOLO.Print "  35  " & frmt(xx(35), 4) & " " & frmt(YY(35), 4)
'  'OEM1.OEM1PicCALCOLO.Print "  31  " & frmt(xx(31), 4) & " " & frmt(YY(31), 4)
'  'OEM1.OEM1PicCALCOLO.Print "  C1  " & frmt(R314, 4) & " " & frmt(R315, 4)
'  'OEM1.OEM1PicCALCOLO.Print "  C3  " & frmt(R316, 4) & " " & frmt(R317, 4)
'  'OEM1.OEM1PicCALCOLO.Print "  C5  " & frmt(R318, 4) & " " & frmt(R319, 4)
'  'OEM1.OEM1PicCALCOLO.Print " R323 =" & frmt(R323, 4)
'  'OEM1.OEM1PicCALCOLO.Print "AC1C3=" & frmt(FnRG(R306), 4)
'  'OEM1.OEM1PicCALCOLO.Print "RBF11=" & frmt(RR(1), 4)
'  'OEM1.OEM1PicCALCOLO.Print "RBF12=" & frmt(RR(2), 4)
'  '
'Return
'
'CALCOLO_F2:
'  'Punto 16
'  YY(16) = 0
'  xx(16) = -R38 / 2
'  ';*************     Punto 5     **************
'  '; Calcolo angolo raccordo fianco-testa R323
'  '; Rastremazione di testa ?
'  If (R85 > 0) Then
'   R323 = R85
'  Else
'   R323 = R51
'  End If
'  If (R57 = 0) Then
'   If (R71 > 0) Or (R72 > 0) Then
'    YY(15) = R56 - R73 - R53             'piu raggi testa, senza prot.
'   Else
'    YY(15) = R58 * (1 - Sin(R323)) - R53 'un solo RT senza protub.
'   End If
'  Else
'   YY(15) = R57 - R53                    'caso con protub.
'  End If
'  'Rastremazione testa ?
'  If (R85 > 0) Then
'   R300 = -YY(15) - (R53 - R84)
'   R301 = R300 * (Tan(R51) - Tan(R85))
'  Else
'   R300 = 0
'   R301 = 0
'  End If
'  xx(15) = xx(16) + Tan(R51) * YY(15) + R301
'  ';*************   Y_Punto 13    ****************
'  '; aggiorno angolo raccordo fianco-testa R323
'  '; Piattino ?
'  If (R73 > 0) Then
'   R323 = R323 + R172
'  End If
'  If (R71 > 0) Or (R72 > 0) Then
'   YY(13) = R56 - R73 - R53      'piu raggi testa
'  Else
'   If (R55 > 0 And R73 > 0) Or (R55 = 0) Then 'piattino
'    YY(13) = R58 * (1 - Sin(R323)) - R53
'   End If
'   If (R55 > 0) And (R73 = 0) Then
'    R316 = xx(15) - R57 * Tan(R323) - R58 * Tan((PG / 2 - R323) / 2) + R55 / Cos(R323)
'    R317 = YY(15) - R57 + R58
'    R308 = Sqr((xx(15) - R316) ^ 2 + (YY(15) - R317) ^ 2)
'    If (R308 < R58) Then
'      StopRegieEvents
'      MsgBox "ERROR : R57, R55 not congruent !"
'      ResumeRegieEvents
'      Exit Sub
'    End If
'    R307 = Sqr(R308 ^ 2 - R58 ^ 2)
'    R323 = Atn((xx(15) - R316) / (YY(15) - R317)) - FnACS((R308 ^ 2 + R307 ^ 2 - R58 ^ 2) / 2 / R308 / R307)
'    YY(13) = R58 * (1 - Sin(R323)) - R53
'   End If
'  End If
'  If (R323 < 0) Then
'    StopRegieEvents
'    MsgBox "ERROR : angolo raccordo testa negative !!"
'    ResumeRegieEvents
'    Exit Sub
'  End If
'  ';*************     Punto 14   ****************
'  YY(14) = YY(13) + R73
'  If R55 > 0 And R73 > 0 Then
'    xx(14) = xx(15) + R55 / Cos(R323) - (YY(15) - YY(14)) * Tan(R323)
'  Else
'    xx(14) = xx(15) - (YY(15) - YY(14)) * Tan(R323)
'  End If
'  ';*************     Punto 25    ****************
'  If (R85 = 0) Then
'   xx(25) = xx(15)
'   YY(25) = YY(15)
'  Else
'   YY(25) = YY(15) + R300
'   xx(25) = xx(16) + YY(25) * Tan(R51)
'   ';bomb. rastr. T.
'   If (R247 <> 0) Then
'    R235 = Sqr((YY(25) - YY(15)) * (YY(25) - YY(15)) + (xx(25) - xx(15)) * (xx(25) - xx(15))) / 2
'    R242 = (R235 * R235 + R247 * R247) / (2 * R247)   'Raggio bombatura rastr.T2
'   Else
'    R242 = 0
'   End If
'  End If
'  ';*************    X_Punto 13    ****************
'  xx(13) = xx(14) - R73 * Tan(R51 + R172)
'  ';*************    Punto 20 (caso 1 RT)    ****************
'  xx(20) = xx(13)
'  YY(20) = YY(13)
'  ';*************     Punto 12    ****************
'  If (R71 > 0) Or (R72 > 0) Then
'   xx(12) = R74
'  Else
'   xx(12) = xx(13) - R58 * Cos(R323)
'  End If
'  YY(12) = -R53
'  '; spostare secondo angolo testa R50
'  If (R71 = 0) Then
'   R304 = R58
'  Else
'   R304 = R71
'  End If
'  R314 = xx(12)
'  R315 = YY(12) + R304
'  xx(12) = R314 + R304 * Sin(R50)
'  YY(12) = R315 - R304 * Cos(R50)
'  ';*************     Punto 32   ****************
'  If (R52 > 0) Then
'   YY(32) = R39 - R53 + R107
'  Else
'   YY(32) = R54 - R53
'  End If
'  xx(32) = 0
'  ';************     Punto 26,17   ****************
'  If (R52 > 0) Then
'   R320 = R52
'  Else
'   If (R83 = 0) Then
'    R320 = R51
'   Else
'    R320 = R87
'   End If
'  End If
'  If (R39 = 0) Then
'   YY(17) = R54 - R53 - R59 * (1 - Sin(R320))
'  Else
'   YY(17) = R54 - R53
'  End If
'  If (R87 > 0) Then
'   R302 = YY(17) - (R86 - R53)
'   R303 = R302 * (Tan(R87) - Tan(R51))
'  Else
'   R302 = 0
'   R303 = 0
'  End If
'  YY(26) = YY(17) - R302
'  xx(26) = xx(16) + YY(26) * Tan(R51)
'  xx(17) = xx(26) + R302 * Tan(R87)
'  '; p18=intersezione fianco_fondo
'  '; M1=coeff. ang.fianco
'  '; M2=coeff. ang.fondo
'  If (R52 > 0) Then
'   R312 = -Tan(R52)
'  Else
'   If (R87 = 0) Then
'    R312 = -Tan(R51)
'   Else
'    R312 = -Tan(R87)
'   End If
'  End If
'  If (R60 = 0) Then
'   R313 = 9999999
'  Else
'   R313 = Tan(PG / 2 - R60)
'  End If
'  R310 = -xx(17) - R312 * YY(17)
'  R311 = -xx(31) - R313 * YY(32)
'  YY(18) = ((R311 - R310) / (R312 - R313))
'  xx(18) = -(R312 * YY(18) + R310)
'  ';********** Punto 34 *************
'  R321 = (PG / 2 - R320 + R60) / 2
'  R322 = (PG / 2 - R320 - R60) / 2
'  R309 = R59 / Cos(R321)
'  R318 = xx(18) + R309 * Sin(R322)
'  R319 = YY(18) - R309 * Cos(R322)
'  xx(34) = R318 + R59 * Sin(R60)
'  YY(34) = R319 + R59 * Cos(R60)
'  YY(36) = R319 + R59 * Sin(R320)
'  xx(36) = R318 - R59 * Cos(R320)
'  '; ricalcolo P17 se R39=0
'  If (R39 = 0) Then
'   xx(17) = xx(36)
'   YY(17) = YY(36)
'  End If
'  '; ricalcolo P26 se R86=0
'  If (R86 = 0) Then
'   xx(26) = xx(17)
'   YY(26) = YY(17)
'  End If
'  '; R228 = Raggio bomb. rastr. fondo
'  If (R227 = 0) Then
'   R228 = 0
'  Else
'   R136 = Sqr((xx(17) - xx(26)) * (xx(17) - xx(26)) + (YY(17) - YY(26)) * (YY(17) - YY(26))) / 2
'   R228 = (R136 * R136 + R227 * R227) / (2 * R227)
'  End If
'  ';*************     Punto19 (uscita F2)  ****************
'  xx(19) = Abs(xx(8)) + 0.2
'  YY(19) = YY(32) - xx(19) * Tan(R60)
'  ';*************     Punto 9 (uscita F1)  ****************
'  xx(9) = -Abs(xx(18)) - 0.2
'  YY(9) = YY(31) - xx(9) * Tan(R60)
'  '; ********** Punto C3, P10 ***********
'  If (R71 > 0) Or (R72 > 0) Then
'   If (R72 = 0) Then
'    R305 = R58
'   Else
'    R305 = R72
'   End If
'   R316 = xx(13) - R305 * Cos(R323)
'   R317 = YY(13) + R305 * Sin(R323)
'   ';Calcolo angolo C11-C13
'   R306 = ((R317 + R43) - R304) / (R305 - R304)
'   If (R306 > 1) Then
'    StopRegieEvents
'    MsgBox "ERROR: R56 too large !!"
'    ResumeRegieEvents
'    Exit Sub
'   End If
'   R306 = FnACS(R306)
'   ';C11
'   R314 = R316 - (R304 - R305) * Sin(R306)
'   R315 = R317 + (R304 - R305) * Cos(R306)
'   '; *********** Punto 20   ***************
'   xx(20) = R314 + R304 * Sin(R306)
'   YY(20) = R315 - R304 * Cos(R306)
'   '; *********** Punto 12   ***************
'   xx(12) = R314 + R304 * Sin(R50)
'   YY(12) = R315 - R304 * Cos(R50)
'  End If
'  ';*************     Punto 1 (inizio F2)  ****************
'  xx(11) = -R37 / 2
'  YY(11) = YY(12) - (R37 / 2 + xx(12)) * Tan(R50)
'  ';Raggi di bombatura fianco, P22
'  If (R32 <> 0) Then
'   YY(22) = YY(25) + R75 * (YY(26) - YY(25))
'   xx(22) = xx(25) + R75 * (YY(26) - YY(25)) * Tan(R51) - R32
'   R66 = R75 * (YY(26) - YY(25)) / Cos(R51)
'   RR(3) = -(R66 * R66 + R32 * R32) / 2 / R32
'   R67 = (1 - R75) * (YY(26) - YY(25)) / Cos(R51)
'   RR(4) = -(R67 * R67 + R32 * R32) / 2 / R32
'  Else
'   xx(22) = xx(16)
'   YY(22) = YY(16)
'  End If
'  '
'  'OEM1.OEM1PicCALCOLO.Cls
'  'OEM1.OEM1PicCALCOLO.Print "FIANCO 2"
'  'OEM1.OEM1PicCALCOLO.Print " punto     X         Y"
'  'OEM1.OEM1PicCALCOLO.Print "  11  " & frmt(xx(11), 4) & " " & frmt(YY(11), 4)
'  'OEM1.OEM1PicCALCOLO.Print "  12  " & frmt(xx(12), 4) & " " & frmt(YY(12), 4)
'  'OEM1.OEM1PicCALCOLO.Print "  20  " & frmt(xx(20), 4) & " " & frmt(YY(20), 4)
'  'OEM1.OEM1PicCALCOLO.Print "  13  " & frmt(xx(13), 4) & " " & frmt(YY(13), 4)
'  'OEM1.OEM1PicCALCOLO.Print "  14  " & frmt(xx(14), 4) & " " & frmt(YY(14), 4)
'  'OEM1.OEM1PicCALCOLO.Print "  15  " & frmt(xx(15), 4) & " " & frmt(YY(15), 4)
'  'OEM1.OEM1PicCALCOLO.Print "  25  " & frmt(xx(25), 4) & " " & frmt(YY(25), 4)
'  'OEM1.OEM1PicCALCOLO.Print "  16  " & frmt(xx(16), 4) & " " & frmt(YY(16), 4)
'  'OEM1.OEM1PicCALCOLO.Print "  22  " & frmt(xx(22), 4) & " " & frmt(YY(22), 4)
'  'OEM1.OEM1PicCALCOLO.Print "  26  " & frmt(xx(26), 4) & " " & frmt(YY(26), 4)
'  'OEM1.OEM1PicCALCOLO.Print "  17  " & frmt(xx(17), 4) & " " & frmt(YY(17), 4)
'  'OEM1.OEM1PicCALCOLO.Print "  18  " & frmt(xx(18), 4) & " " & frmt(YY(18), 4)
'  'OEM1.OEM1PicCALCOLO.Print "  19  " & frmt(xx(19), 4) & " " & frmt(YY(19), 4)
'  'OEM1.OEM1PicCALCOLO.Print "  34  " & frmt(xx(33), 4) & " " & frmt(YY(33), 4)
'  'OEM1.OEM1PicCALCOLO.Print "  35  " & frmt(xx(34), 4) & " " & frmt(YY(34), 4)
'  'OEM1.OEM1PicCALCOLO.Print "  32  " & frmt(xx(32), 4) & " " & frmt(YY(32), 4)
'  'OEM1.OEM1PicCALCOLO.Print "  C11 " & frmt(R314, 4) & " " & frmt(R315, 4)
'  'OEM1.OEM1PicCALCOLO.Print "  C13 " & frmt(R316, 4) & " " & frmt(R317, 4)
'  'OEM1.OEM1PicCALCOLO.Print "  C15 " & frmt(R318, 4) & " " & frmt(R319, 4)
'  'OEM1.OEM1PicCALCOLO.Print " R323 =" & frmt(R323, 4)
'  'OEM1.OEM1PicCALCOLO.Print "R306=" & frmt(FnRG(R306), 4)
'  'OEM1.OEM1PicCALCOLO.Print "RR(3)=" & frmt(RR(3), 4)
'  'OEM1.OEM1PicCALCOLO.Print "RR(4)=" & frmt(RR(4), 4)
'  '
'Return
'
'CALCOLOALTEZZE:
'  '; Calcolo altezza profilo
'  If (R20 = 1) Then
'   If (R40 > 0) Then
'    R98 = -YY(1)
'    R96 = YY(31) - YY(1) + xx(8) * Tan(R60)
'   Else
'    R98 = -YY(2)
'    R96 = YY(8) - YY(2)
'   End If
'  End If
'  If (R20 = 2) Then
'   If (R50 > 0) Then
'    R98 = -YY(11)
'    R96 = YY(31) - YY(11) + xx(18) * Tan(R60)
'   Else
'    R98 = -YY(12)
'    R96 = YY(18) - YY(12)
'   End If
'  End If
'  If (R20 = 0) Then
'   R98 = YY(1)
'   If (R98 > YY(2)) Then
'    R98 = YY(2)
'   End If
'   If (R98 > YY(12)) Then
'    R98 = YY(12)
'   End If
'   If (R98 > YY(11)) Then
'    R98 = YY(11)
'   End If
'   R98 = -R98
'   R96 = YY(8)
'   If (R96 < YY(18)) Then
'    R96 = YY(18)
'   End If
'   R96 = R96 + R98
'  End If
'  stmp = " HWT:" & frmt(R96, 4) & "  HWpt:" & frmt(R98, 4) & " "
'  OEM1.OEM1PicCALCOLO.CurrentX = PicW / 2 - OEM1.OEM1PicCALCOLO.TextWidth(stmp) / 2
'  OEM1.OEM1PicCALCOLO.CurrentY = 0
'  OEM1.OEM1PicCALCOLO.Print stmp
'  '
'Return
'
'LETTURA_PARAMETRI:
'  '
'  Set DBB = OpenDatabase(PathDB, True, False)
'  '
'  Set RSS = DBB.OpenRecordset("OEM1_LAVORO10")
'  RSS.Index = "INDICE"
'  RSS.Seek "=", 12
'  R20 = val(RSS.Fields("ACTUAL_1").Value)
'  RSS.Close
'  '
'  'FILTRO IL VALORE DI R20 PER IL CALCOLO DELL'ALTEZZA
'  If (R20 <> 1) And (R20 <> 2) And (R20 <> 0) Then R20 = 0
'  '
'  DBB.Close
'  '
'  R37 = val(OEM1.List1(1).List(0))
'  R38 = val(OEM1.List1(1).List(1))
'  R39 = val(OEM1.List1(1).List(2))
'  R60 = val(OEM1.List1(1).List(3))
'  '
'  R41 = val(OEM1.List3(1).List(0))
'  R42 = val(OEM1.List3(1).List(1))
'  R43 = val(OEM1.List3(1).List(2))
'  R44 = val(OEM1.List3(1).List(3))
'  R45 = val(OEM1.List3(1).List(4))
'  R46 = val(OEM1.List3(1).List(5))
'  R47 = val(OEM1.List3(1).List(6))
'  R48 = val(OEM1.List3(1).List(7))
'  R49 = val(OEM1.List3(1).List(8))
'  R173 = val(OEM1.List3(1).List(9))
'  '
'  R31 = val(OEM1.List3(1).List(10))
'  R40 = val(OEM1.List3(1).List(11))
'  R61 = val(OEM1.List3(1).List(12))
'  R62 = val(OEM1.List3(1).List(13))
'  R63 = val(OEM1.List3(1).List(14))
'  R64 = val(OEM1.List3(1).List(15))
'  R65 = val(OEM1.List3(1).List(16))
'  R80 = val(OEM1.List3(1).List(17))
'  R81 = val(OEM1.List3(1).List(18))
'  R237 = val(OEM1.List3(1).List(19))
'  R82 = val(OEM1.List3(1).List(20))
'  R83 = val(OEM1.List3(1).List(21))
'  R137 = val(OEM1.List3(1).List(22))
'  R171 = val(OEM1.List3(1).List(23))  'FlagSTD007
'  R106 = val(OEM1.List3(1).List(24))  'FlagSTD008
'  '
'  R51 = val(OEM1.List3(2).List(0))
'  R52 = val(OEM1.List3(2).List(1))
'  R53 = val(OEM1.List3(2).List(2))
'  R54 = val(OEM1.List3(2).List(3))
'  R55 = val(OEM1.List3(2).List(4))
'  R56 = val(OEM1.List3(2).List(5))
'  R57 = val(OEM1.List3(2).List(6))
'  R58 = val(OEM1.List3(2).List(7))
'  R59 = val(OEM1.List3(2).List(8))
'  R174 = val(OEM1.List3(2).List(9))
'  '
'  R32 = val(OEM1.List3(2).List(10))
'  R50 = val(OEM1.List3(2).List(11))
'  R71 = val(OEM1.List3(2).List(12))
'  R72 = val(OEM1.List3(2).List(13))
'  R73 = val(OEM1.List3(2).List(14))
'  R74 = val(OEM1.List3(2).List(15))
'  R75 = val(OEM1.List3(2).List(16))
'  R84 = val(OEM1.List3(2).List(17))
'  R85 = val(OEM1.List3(2).List(18))
'  R247 = val(OEM1.List3(2).List(19))
'  R86 = val(OEM1.List3(2).List(20))
'  R87 = val(OEM1.List3(2).List(21))
'  R227 = val(OEM1.List3(2).List(22))
'  R172 = val(OEM1.List3(2).List(23))  'FlagSTD009
'  R107 = val(OEM1.List3(2).List(24))  'FlagSTD010
'  '
'Return
'
'CONVERSIONE_RADIANTI: 'Convertire gli angoli in radianti (solo PC)
'  R41 = FnGR(R41)
'  R51 = FnGR(R51)
'  R42 = FnGR(R42)
'  R52 = FnGR(R52)
'  R81 = FnGR(R81)
'  R83 = FnGR(R83)
'  R85 = FnGR(R85)
'  R87 = FnGR(R87)
'  R40 = FnGR(R40)
'  R50 = FnGR(R50)
'  R60 = FnGR(R60)
'Return
'
'CONTROLLO_PARAMETRI:
'  a$ = "R45"
'  If R45 < 0 Then GoTo ERRORE
'  a$ = "R55"
'  If R55 < 0 Then GoTo ERRORE
'  a$ = "R42,R39"
'  If (R42 = 0) And (R39 > 0) Then GoTo ERRORE
'  a$ = "R52,R39"
'  If (R52 = 0) And (R39 > 0) Then GoTo ERRORE
'  a$ = "R39,R42,R44"
'  If (R42 > 0) And (R39 < R44) Then GoTo ERRORE
'  a$ = "R39,R52,R54"
'  If (R52 > 0) And (R39 < R54) Then GoTo ERRORE
'  a$ = "R61,R62,R46"
'  If ((R61 > 0) Or (R62 > 0)) And (R46 = 0) Then GoTo ERRORE
'  a$ = "R71,R72,R56"
'  If ((R71 > 0) Or (R72 > 0)) And (R56 = 0) Then GoTo ERRORE
'  a$ = "R61,R62"
'  If (R61 > 0) And (R62 > 0) Then GoTo ERRORE
'  a$ = "R71,R72"
'  If (R71 > 0) And (R72 > 0) Then GoTo ERRORE
'  a$ = "R65"
'  If (R65 < 0.05) Or (R65 > 0.95) Then GoTo ERRORE
'  a$ = "R75"
'  If (R75 < 0.05) Or (R75 > 0.95) Then GoTo ERRORE
'  GoTo PAROK
'  '
'ERRORE:
'  '
'  StopRegieEvents
'  MsgBox "ERROR : wrong parameters " & a$
'  ResumeRegieEvents
'  Exit Sub
'  '
'PAROK:
'
'Return
'
'VISUALIZZA_PERCORSO_F1:
'  '
'  Open g_chOemPATH & "\CNC.TXT" For Output As #1
'    '
'    stmp = "F1"
'    If (SiStampa = "Y") Then
'      X1 = (-xx(1) - ZeroPezzoX) * Scala + PicW / 2 - Printer.TextWidth(stmp) / 2
'      Y1 = -(0 - ZeroPezzoY) * Scala + PicH / 2 - Printer.TextHeight(stmp)
'      If (Y1 <= Printer.ScaleHeight) And (X1 <= Printer.ScaleWidth) And (X1 >= 0) And (Y1 >= 0) Then
'        Printer.CurrentX = X1
'        Printer.CurrentY = Y1
'        Printer.Print stmp
'      End If
'    Else
'      X1 = (-xx(1) - ZeroPezzoX) * Scala + PicW / 2 - OEM1.OEM1PicCALCOLO.TextWidth(stmp) / 2
'      Y1 = -(0 - ZeroPezzoY) * Scala + PicH / 2 - OEM1.OEM1PicCALCOLO.TextHeight(stmp)
'      If (Y1 <= OEM1.OEM1PicCALCOLO.ScaleHeight) And (X1 <= OEM1.OEM1PicCALCOLO.ScaleWidth) And (X1 >= 0) And (Y1 >= 0) Then
'        OEM1.OEM1PicCALCOLO.CurrentX = X1
'        OEM1.OEM1PicCALCOLO.CurrentY = Y1
'        OEM1.OEM1PicCALCOLO.Print stmp
'      End If
'    End If
'    Print #1, stmp
'    '
'    Print #1, "G1 X=" & frmt(xx(1), 4)
'    Print #1, "G1 Y=" & frmt(YY(1), 4)
'    Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -xx(1), 0, -xx(1), YY(1), 0, "", SiStampa, ZeroPezzoX, ZeroPezzoY)
'    '
'    Print #1, "G1 X=" & frmt(xx(2), 4) & " Y=" & frmt(YY(2), 4)
'    Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -xx(1), YY(1), -xx(2), YY(2), 0, "", SiStampa, ZeroPezzoX, ZeroPezzoY)
'    '
'    Print #1, "G3 X=" & frmt(xx(10), 4) & " Y=" & frmt(YY(10), 4) & " R=" & frmt(R304, 4)
'    Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -xx(2), YY(2), -xx(10), YY(10), R304, "DX", SiStampa, ZeroPezzoX, ZeroPezzoY)
'    '
'    X1 = xx(10): Y1 = YY(10)
'    '
'    If (R61 > 0) Or (R62 > 0) Then
'      Print #1, "G3 X=" & frmt(xx(3), 4) & " Y=" & frmt(YY(3), 4) & " R=" & frmt(R305, 4)
'      Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -xx(10), YY(10), -xx(3), YY(3), R305, "DX", SiStampa, ZeroPezzoX, ZeroPezzoY)
'      X1 = xx(3): Y1 = YY(3)
'    End If
'    '
'    If (R63 > 0) Then
'      Print #1, "G1 X=" & frmt(xx(4), 4) & " Y=" & frmt(YY(4), 4)
'      Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -X1, Y1, -xx(4), YY(4), 0, "", SiStampa, ZeroPezzoX, ZeroPezzoY)
'      X1 = xx(4): Y1 = YY(4)
'    End If
'    '
'    If (R45 > 0) Then
'      Print #1, "G1 X=" & frmt(xx(5), 4) & " Y=" & frmt(YY(5), 4)
'      Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -X1, Y1, -xx(5), YY(5), 0, "", SiStampa, ZeroPezzoX, ZeroPezzoY)
'      X1 = xx(5): Y1 = YY(5)
'    End If
'    '
'    If (R81 > 0) Then
'      Print #1, "G1 X=" & frmt(xx(23), 4) & " Y=" & frmt(YY(23), 4)
'      Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -X1, Y1, -xx(23), YY(23), 0, "", SiStampa, ZeroPezzoX, ZeroPezzoY)
'      X1 = xx(23): Y1 = YY(23)
'    End If
'    '
'    If (R31 <> 0) Then
'      If (RR(1) < 0) Then
'        Print #1, "G2 X=" & frmt(xx(21), 4) & " Y=" & frmt(YY(21), 4) & " R=" & frmt(Abs(RR(1)), 4)
'        Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -X1, Y1, -xx(21), YY(21), Abs(RR(1)), "SX", SiStampa, ZeroPezzoX, ZeroPezzoY)
'        Print #1, "G2 X=" & frmt(xx(24), 4) & " Y=" & frmt(YY(24), 4) & " R=" & frmt(Abs(RR(2)), 4)
'        Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -xx(21), YY(21), -xx(24), YY(24), Abs(RR(2)), "SX", SiStampa, ZeroPezzoX, ZeroPezzoY)
'      Else
'        Print #1, "G3 X=" & frmt(xx(21), 4) & " Y=" & frmt(YY(21), 4) & " R=" & frmt(RR(1), 4)
'        Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -X1, Y1, -xx(21), YY(21), Abs(RR(1)), "DX", SiStampa, ZeroPezzoX, ZeroPezzoY)
'        Print #1, "G3 X=" & frmt(xx(24), 4) & " Y=" & frmt(YY(24), 4) & " R=" & frmt(RR(2), 4)
'        Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -xx(21), YY(21), -xx(24), YY(24), Abs(RR(2)), "DX", SiStampa, ZeroPezzoX, ZeroPezzoY)
'      End If
'      X1 = xx(24): Y1 = YY(24)
'    Else
'      Print #1, "G1 X=" & frmt(xx(24), 4) & " Y=" & frmt(YY(24), 4)
'      Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -X1, Y1, -xx(24), YY(24), 0, "", SiStampa, ZeroPezzoX, ZeroPezzoY)
'      X1 = xx(24): Y1 = YY(24)
'    End If
'    '
'    If (R83 <> 0) Then
'      If (R138 = 0) Then
'        Print #1, "G1 X=" & frmt(xx(7), 4) & " Y=" & frmt(YY(7), 4)
'        Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -X1, Y1, -xx(7), YY(7), 0, "", SiStampa, ZeroPezzoX, ZeroPezzoY)
'      Else
'        If (R138 > 0) Then
'          Print #1, "G3 X=" & frmt(xx(7), 4) & " Y=" & frmt(YY(7), 4) & " R=" & frmt(R138, 4)
'          Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -xx(24), YY(24), -xx(7), YY(7), R138, "DX", SiStampa, ZeroPezzoX, ZeroPezzoY)
'        Else
'          Print #1, "G2 X=" & frmt(xx(7), 4) & " Y=" & frmt(YY(7), 4) & " R=" & frmt(R138, 4)
'          Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -xx(24), YY(24), -xx(7), YY(7), R138, "SX", SiStampa, ZeroPezzoX, ZeroPezzoY)
'        End If
'      End If
'      X1 = xx(7): Y1 = YY(7)
'    End If
'    '
'    If (R42 > 0) Then
'      If (R173 > 0) Then
'        Print #1, "R=" & frmt(R173, 4)
'      End If
'      Print #1, "G1 X=" & frmt(xx(35), 4) & " Y=" & frmt(YY(35), 4)
'      Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -X1, Y1, -xx(35), YY(35), 0, "", SiStampa, ZeroPezzoX, ZeroPezzoY)
'      X1 = xx(35): Y1 = YY(35)
'    End If
'    '
'    Print #1, "G2 X=" & frmt(xx(33), 4) & " Y=" & frmt(YY(33), 4) & " R=" & frmt(R49, 4)
'    Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -X1, Y1, -xx(33), YY(33), R49, "SX", SiStampa, ZeroPezzoX, ZeroPezzoY)
'    Print #1, "G1 X=" & frmt(xx(9), 4) & " Y=" & frmt(YY(9), 4)
'    'Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -xx(33), YY(33), -xx(9), YY(9), 0, "", SiStampa, ZeroPezzoX, ZeroPezzoY)
'    Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -xx(33), YY(33), -xx(31), YY(31), 0, "", SiStampa, ZeroPezzoX, ZeroPezzoY)
'    '
'  Close #1
'  '
'Return
'
'VISUALIZZA_PERCORSO_F2:
'  '
'  Open g_chOemPATH & "\CNC.TXT" For Append As #1
'    '
'    stmp = "F2"
'    If (SiStampa = "Y") Then
'      X1 = (-xx(11) - ZeroPezzoX) * Scala + PicW / 2 - Printer.TextWidth(stmp) / 2
'      Y1 = -(0 - ZeroPezzoY) * Scala + PicH / 2 - Printer.TextHeight(stmp)
'      If (Y1 <= Printer.ScaleHeight) And (X1 <= Printer.ScaleWidth) And (X1 >= 0) And (Y1 >= 0) Then
'        Printer.CurrentX = X1
'        Printer.CurrentY = Y1
'        Printer.Print stmp
'      End If
'    Else
'      X1 = (-xx(11) - ZeroPezzoX) * Scala + PicW / 2 - OEM1.OEM1PicCALCOLO.TextWidth(stmp) / 2
'      Y1 = -(0 - ZeroPezzoY) * Scala + PicH / 2 - OEM1.OEM1PicCALCOLO.TextHeight(stmp)
'      If (Y1 <= OEM1.OEM1PicCALCOLO.ScaleHeight) And (X1 <= OEM1.OEM1PicCALCOLO.ScaleWidth) And (X1 >= 0) And (Y1 >= 0) Then
'        OEM1.OEM1PicCALCOLO.CurrentX = X1
'        OEM1.OEM1PicCALCOLO.CurrentY = Y1
'        OEM1.OEM1PicCALCOLO.Print stmp
'      End If
'    End If
'    Print #1, stmp
'    '
'    Print #1, "G1 X=" & frmt(xx(11), 4)
'    Print #1, "G1 X=" & frmt(YY(11), 4)
'    Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -xx(11), 0, -xx(11), YY(11), 0, "", SiStampa, ZeroPezzoX, ZeroPezzoY)
'    '
'    Print #1, "G1 X=" & frmt(xx(12), 4) & " Y=" & frmt(YY(12), 4)
'    Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -xx(11), YY(11), -xx(12), YY(12), 0, "", SiStampa, ZeroPezzoX, ZeroPezzoY)
'    '
'    Print #1, "G2 X=" & frmt(xx(20), 4) & " Y=" & frmt(YY(20), 4) & " R=" & frmt(R304, 4)
'    Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -xx(12), YY(12), -xx(20), YY(20), R304, "SX", SiStampa, ZeroPezzoX, ZeroPezzoY)
'    '
'    X1 = xx(20): Y1 = YY(20)
'    '
'    If (R71 > 0) Or (R72 > 0) Then
'      Print #1, "G2 X=" & frmt(xx(13), 4) & " Y=" & frmt(YY(13), 4) & " R=" & frmt(R305, 4)
'      Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -xx(20), YY(20), -xx(13), YY(13), R305, "SX", SiStampa, ZeroPezzoX, ZeroPezzoY)
'      X1 = xx(13): Y1 = YY(13)
'    End If
'    '
'    If (R73 > 0) Then
'      Print #1, "G1 X=" & frmt(xx(14), 4) & " Y=" & frmt(YY(14), 4)
'      Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -X1, Y1, -xx(14), YY(14), 0, "", SiStampa, ZeroPezzoX, ZeroPezzoY)
'      X1 = xx(14): Y1 = YY(14)
'    End If
'    '
'    If (R55 > 0) Then
'      Print #1, "G1 X=" & frmt(xx(15), 4) & " Y=" & frmt(YY(15), 4)
'      Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -X1, Y1, -xx(15), YY(15), 0, "", SiStampa, ZeroPezzoX, ZeroPezzoY)
'      X1 = xx(15): Y1 = YY(15)
'    End If
'    '
'    If (R85 > 0) Then
'      Print #1, "G1 X=" & frmt(xx(25), 4) & " Y=" & frmt(YY(25), 4)
'      Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -X1, Y1, -xx(25), YY(25), 0, "", SiStampa, ZeroPezzoX, ZeroPezzoY)
'      X1 = xx(25): Y1 = YY(25)
'    End If
'    '
'    If (R32 <> 0) Then
'      If (RR(3) > 0) Then
'        Print #1, "G2 X=" & frmt(xx(22), 4) & " Y=" & frmt(YY(22), 4) & " R=" & frmt(RR(3), 4)
'        Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -X1, Y1, -xx(22), YY(22), RR(3), "SX", SiStampa, ZeroPezzoX, ZeroPezzoY)
'        Print #1, "G2 X=" & frmt(xx(26), 4) & " Y=" & frmt(YY(26), 4) & " R=" & frmt(RR(4), 4)
'        Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -xx(22), YY(22), -xx(26), YY(26), RR(4), "SX", SiStampa, ZeroPezzoX, ZeroPezzoY)
'      Else
'        Print #1, "G3 X=" & frmt(xx(22), 4) & " Y=" & frmt(YY(22), 4) & " R=" & frmt(Abs(RR(3)), 4)
'        Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -X1, Y1, -xx(22), YY(22), RR(3), "DX", SiStampa, ZeroPezzoX, ZeroPezzoY)
'        Print #1, "G3 X=" & frmt(xx(26), 4) & " Y=" & frmt(YY(26), 4) & " R=" & frmt(Abs(RR(4)), 4)
'        Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -xx(22), YY(22), -xx(26), YY(26), RR(4), "DX", SiStampa, ZeroPezzoX, ZeroPezzoY)
'      End If
'      X1 = xx(26): Y1 = YY(26)
'    Else
'      Print #1, "G1 X=" & frmt(xx(26), 4) & " Y=" & frmt(YY(26), 4)
'      Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -X1, Y1, -xx(26), YY(26), 0, "", SiStampa, ZeroPezzoX, ZeroPezzoY)
'      X1 = xx(26): Y1 = YY(26)
'    End If
'    '
'    If (R87 <> 0) Then
'      If (R228 = 0) Then
'        Print #1, "G1 X=" & frmt(xx(17), 4) & " Y=" & frmt(YY(17), 4)
'        Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -X1, Y1, -xx(17), YY(17), 0, "", SiStampa, ZeroPezzoX, ZeroPezzoY)
'      Else
'        If (R228 < 0) Then
'          Print #1, "G3 X=" & frmt(xx(17), 4) & " Y=" & frmt(YY(17), 4) & " R=" & frmt(R228, 4)
'          Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -xx(26), YY(26), -xx(17), YY(17), R228, "DX", SiStampa, ZeroPezzoX, ZeroPezzoY)
'        Else
'          Print #1, "G2 X=" & frmt(xx(17), 4) & " Y=" & frmt(YY(17), 4) & " R=" & frmt(R228, 4)
'          Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -xx(26), YY(26), -xx(17), YY(17), R228, "SX", SiStampa, ZeroPezzoX, ZeroPezzoY)
'        End If
'      End If
'      X1 = xx(17): Y1 = YY(17)
'    End If
'    '
'    If (R52 > 0) Then
'      If (R174 > 0) Then
'        Print #1, "R=" & frmt(-R174, 4)
'      End If
'      Print #1, "G1 X=" & frmt(xx(36), 4) & " Y=" & frmt(YY(36), 4)
'      Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -X1, Y1, -xx(36), YY(36), 0, "", SiStampa, ZeroPezzoX, ZeroPezzoY)
'      X1 = xx(36): Y1 = YY(36)
'    End If
'    '
'    Print #1, "G3 X=" & frmt(xx(34), 4) & " Y=" & frmt(YY(34), 4) & " R=" & frmt(R59, 4)
'    Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -X1, Y1, -xx(34), YY(34), R59, "DX", SiStampa, ZeroPezzoX, ZeroPezzoY)
'    '
'    Print #1, "G1 X=" & frmt(xx(19), 4) & " Y=" & frmt(YY(19), 4)
'    Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -xx(34), YY(34), -xx(32), YY(32), 0, "", SiStampa, ZeroPezzoX, ZeroPezzoY)
'    'Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, -xx(34), YY(34), -xx(19), YY(19), 0, "", SiStampa, ZeroPezzoX, ZeroPezzoY)
'    '
'  Close #1
'  '
'Return
'
'SCRIVI_FILE_DXF_FIANCO1:
'  Open g_chOemPATH & "\MOLA.DXF" For Output As #1
'    Print #1, "0"
'    Print #1, "SECTION"
'    Print #1, "2"
'    Print #1, "ENTITIES"
'    'LINE P1-P2
'    Print #1, "0"
'    Print #1, "LINE"
'    Print #1, "8"
'    Print #1, "1"
'    Print #1, "62"
'    Print #1, "1"
'    Print #1, "10"
'    Print #1, frmt(YY(1), 4)
'    Print #1, "20"
'    Print #1, frmt(xx(1), 4)
'    Print #1, "30"
'    Print #1, "0.0"
'    Print #1, "11"
'    Print #1, frmt(YY(2), 4)
'    Print #1, "21"
'    Print #1, frmt(xx(2), 4)
'    Print #1, "31"
'    Print #1, "0.0"
'    'ARC C1
'    A1 = 180 + FnRG(Atn((xx(2) - R314) / (YY(2) - R315)))
'    A2 = 180 + FnRG(Atn((xx(10) - R314) / (YY(10) - R315)))
'    Print #1, "0"
'    Print #1, "ARC"
'    Print #1, "8"
'    Print #1, "1"
'    Print #1, "62"
'    Print #1, "1"
'    Print #1, "10"
'    Print #1, frmt(R315, 4)
'    Print #1, "20"
'    Print #1, frmt(R314, 4)
'    Print #1, "30"
'    Print #1, "0.0"
'    Print #1, "40"
'    Print #1, frmt(R304, 4)
'    Print #1, "50"
'    Print #1, frmt(A1, 4)
'    Print #1, "51"
'    Print #1, frmt(A2, 4)
'    If R61 > 0 Or R62 > 0 Then
'      'ARC C3
'      A1 = 180 + FnRG(Atn((xx(10) - R316) / (YY(10) - R317)))
'      A2 = 180 + FnRG(Atn((xx(3) - R316) / (YY(3) - R317)))
'      Print #1, "0"
'      Print #1, "ARC"
'      Print #1, "8"
'      Print #1, "1"
'      Print #1, "62"
'      Print #1, "1"
'      Print #1, "10"
'      Print #1, frmt(R317, 4)
'      Print #1, "20"
'      Print #1, frmt(R316, 4)
'      Print #1, "30"
'      Print #1, "0.0"
'      Print #1, "40"
'      Print #1, frmt(R305, 4)
'      Print #1, "50"
'      Print #1, frmt(A1, 4)
'      Print #1, "51"
'      Print #1, frmt(A2, 4)
'    End If
'    'LINE P3-P4
'    If R63 > 0 Then
'      Print #1, "0"
'      Print #1, "LINE"
'      Print #1, "8"
'      Print #1, "1"
'      Print #1, "62"
'      Print #1, "1"
'      Print #1, "10"
'      Print #1, frmt(YY(3), 4)
'      Print #1, "20"
'      Print #1, frmt(xx(3), 4)
'      Print #1, "30"
'      Print #1, "0.0"
'      Print #1, "11"
'      Print #1, frmt(YY(4), 4)
'      Print #1, "21"
'      Print #1, frmt(xx(4), 4)
'      Print #1, "31"
'      Print #1, "0.0"
'    End If
'    'LINE P4-P5
'    If R45 > 0 Then
'      Print #1, "0"
'      Print #1, "LINE"
'      Print #1, "8"
'      Print #1, "1"
'      Print #1, "62"
'      Print #1, "1"
'      Print #1, "10"
'      Print #1, frmt(YY(4), 4)
'      Print #1, "20"
'      Print #1, frmt(xx(4), 4)
'      Print #1, "30"
'      Print #1, "0.0"
'      Print #1, "11"
'      Print #1, frmt(YY(5), 4)
'      Print #1, "21"
'      Print #1, frmt(xx(5), 4)
'      Print #1, "31"
'      Print #1, "0.0"
'    End If
'    If R81 = 0 Then GoTo NORST1
'    If R237 = 0 Then
'      'LINE P5-P23
'      Print #1, "0"
'      Print #1, "LINE"
'      Print #1, "8"
'      Print #1, "1"
'      Print #1, "62"
'      Print #1, "1"
'      Print #1, "10"
'      Print #1, frmt(YY(5), 4)
'      Print #1, "20"
'      Print #1, frmt(xx(5), 4)
'      Print #1, "30"
'      Print #1, "0.0"
'      Print #1, "11"
'      Print #1, frmt(YY(23), 4)
'      Print #1, "21"
'      Print #1, frmt(xx(23), 4)
'      Print #1, "31"
'      Print #1, "0.0"
'     Else
'      'ARCO P5-P23
'      'Centro
'      ANGOLO = Atn((xx(5) - xx(23)) / (YY(23) - YY(5)))
'      DIST = Sqr((xx(5) - xx(23)) ^ 2 + (YY(23) - YY(5)) ^ 2)
'      PMC = Sqr(R238 ^ 2 - DIST ^ 2 / 4)
'      Xc = (xx(5) + xx(23)) / 2 - PMC * Cos(ANGOLO) * Sgn(R238)
'      YC = (YY(5) + YY(23)) / 2 - PMC * Sin(ANGOLO) * Sgn(R238)
'      If R238 > 0 Then
'        A2 = 90 - FnRG(Atn((YY(5) - YC) / (xx(5) - Xc)))
'        A1 = 90 - FnRG(Atn((YY(23) - YC) / (xx(23) - Xc)))
'      Else
'        A1 = 270 - FnRG(Atn((YY(5) - YC) / (xx(5) - Xc)))
'        A2 = 270 - FnRG(Atn((YY(23) - YC) / (xx(23) - Xc)))
'      End If
'      Print #1, "0"
'      Print #1, "ARC"
'      Print #1, "8"
'      Print #1, "1"
'      Print #1, "62"
'      Print #1, "1"
'      Print #1, "10"
'      Print #1, frmt(YC, 4)
'      Print #1, "20"
'      Print #1, frmt(Xc, 4)
'      Print #1, "30"
'      Print #1, "0.0"
'      Print #1, "40"
'      Print #1, frmt(Abs(R238), 4)
'      Print #1, "50"
'      Print #1, frmt(A1, 4)
'      Print #1, "51"
'      Print #1, frmt(A2, 4)
'    End If
'NORST1:
'  If R31 = 0 Then
'    'LINE P23-P24
'    Print #1, "0"
'    Print #1, "LINE"
'    Print #1, "8"
'    Print #1, "1"
'    Print #1, "62"
'    Print #1, "1"
'    Print #1, "10"
'    Print #1, frmt(YY(23), 4)
'    Print #1, "20"
'    Print #1, frmt(xx(23), 4)
'    Print #1, "30"
'    Print #1, "0.0"
'    Print #1, "11"
'    Print #1, frmt(YY(24), 4)
'    Print #1, "21"
'    Print #1, frmt(xx(24), 4)
'    Print #1, "31"
'    Print #1, "0.0"
'  Else
'    'ARC C5
'    ANGOLO = Atn((xx(23) - xx(21)) / (YY(21) - YY(23)))
'    DIST = Sqr((xx(23) - xx(21)) ^ 2 + (YY(21) - YY(23)) ^ 2)
'    PMC = Sqr(RR(1) ^ 2 - DIST ^ 2 / 4)
'    Xc = (xx(21) + xx(23)) / 2 + PMC * Cos(ANGOLO) * Sgn(RR(1))
'    YC = (YY(21) + YY(23)) / 2 + PMC * Sin(ANGOLO) * Sgn(RR(1))
'    If RR(1) < 0 Then
'      A2 = 90 - FnRG(Atn((YY(23) - YC) / (xx(23) - Xc)))
'      A1 = 90 - FnRG(Atn((YY(21) - YC) / (xx(21) - Xc)))
'    Else
'      A1 = 270 - FnRG(Atn((YY(23) - YC) / (xx(23) - Xc)))
'      A2 = 270 - FnRG(Atn((YY(21) - YC) / (xx(21) - Xc)))
'    End If
'    Print #1, "0"
'    Print #1, "ARC"
'    Print #1, "8"
'    Print #1, "1"
'    Print #1, "62"
'    Print #1, "1"
'    Print #1, "10"
'    Print #1, frmt(YC, 4)
'    Print #1, "20"
'    Print #1, frmt(Xc, 4)
'    Print #1, "30"
'    Print #1, "0.0"
'    Print #1, "40"
'    Print #1, frmt(Abs(RR(1)), 4)
'    Print #1, "50"
'    Print #1, frmt(A1, 4)
'    Print #1, "51"
'    Print #1, frmt(A2, 4)
'    ';*************  ARC C6 ********************
'    ANGOLO = Atn((xx(21) - xx(24)) / (YY(24) - YY(21)))
'    DIST = Sqr((xx(21) - xx(24)) ^ 2 + (YY(24) - YY(21)) ^ 2)
'    PMC = Sqr(RR(2) ^ 2 - DIST ^ 2 / 4)
'    Xc = (xx(24) + xx(21)) / 2 + PMC * Cos(ANGOLO) * Sgn(RR(2))
'    YC = (YY(24) + YY(21)) / 2 + PMC * Sin(ANGOLO) * Sgn(RR(2))
'    If RR(2) < 0 Then
'      A2 = 90 - FnRG(Atn((YY(21) - YC) / (xx(21) - Xc)))
'      A1 = 90 - FnRG(Atn((YY(24) - YC) / (xx(24) - Xc)))
'    Else
'      A1 = 270 - FnRG(Atn((YY(21) - YC) / (xx(21) - Xc)))
'      A2 = 270 - FnRG(Atn((YY(24) - YC) / (xx(24) - Xc)))
'    End If
'    Print #1, "0"
'    Print #1, "ARC"
'    Print #1, "8"
'    Print #1, "1"
'    Print #1, "62"
'    Print #1, "1"
'    Print #1, "10"
'    Print #1, frmt(YC, 4)
'    Print #1, "20"
'    Print #1, frmt(Xc, 4)
'    Print #1, "30"
'    Print #1, "0.0"
'    Print #1, "40"
'    Print #1, frmt(Abs(RR(2)), 4)
'    Print #1, "50"
'    Print #1, frmt(A1, 4)
'    Print #1, "51"
'    Print #1, frmt(A2, 4)
'  End If
'  '****************  P24-P7
'  If R83 = 0 Then GoTo P7P35
'  If R138 = 0 Then
'    Print #1, "0"
'    Print #1, "LINE"
'    Print #1, "8"
'    Print #1, "1"
'    Print #1, "62"
'    Print #1, "1"
'    Print #1, "10"
'    Print #1, frmt(YY(24), 4)
'    Print #1, "20"
'    Print #1, frmt(xx(24), 4)
'    Print #1, "30"
'    Print #1, "0.0"
'    Print #1, "11"
'    Print #1, frmt(YY(7), 4)
'    Print #1, "21"
'    Print #1, frmt(xx(7), 4)
'    Print #1, "31"
'    Print #1, "0.0"
'  Else
'    ANGOLO = Atn((xx(24) - xx(7)) / (YY(7) - YY(24)))
'    DIST = Sqr((xx(24) - xx(7)) ^ 2 + (YY(7) - YY(24)) ^ 2)
'    PMC = Sqr(R138 ^ 2 - DIST ^ 2 / 4)
'    Xc = (xx(7) + xx(24)) / 2 - PMC * Cos(ANGOLO) * Sgn(R138)
'    YC = (YY(7) + YY(24)) / 2 - PMC * Sin(ANGOLO) * Sgn(R138)
'    If R138 > 0 Then
'      A2 = 90 - FnRG(Atn((YY(24) - YC) / (xx(24) - Xc)))
'      A1 = 90 - FnRG(Atn((YY(7) - YC) / (xx(7) - Xc)))
'    Else
'      A1 = 270 - FnRG(Atn((YY(24) - YC) / (xx(24) - Xc)))
'      A2 = 270 - FnRG(Atn((YY(7) - YC) / (xx(7) - Xc)))
'    End If
'    Print #1, "0"
'    Print #1, "ARC"
'    Print #1, "8"
'    Print #1, "1"
'    Print #1, "62"
'    Print #1, "1"
'    Print #1, "10"
'    Print #1, frmt(YC, 4)
'    Print #1, "20"
'    Print #1, frmt(Xc, 4)
'    Print #1, "30"
'    Print #1, "0.0"
'    Print #1, "40"
'    Print #1, frmt(Abs(R138), 4)
'    Print #1, "50"
'    Print #1, frmt(A1, 4)
'    Print #1, "51"
'    Print #1, frmt(A2, 4)
'  End If
'P7P35:
'  Print #1, "0"
'  Print #1, "LINE"
'  Print #1, "8"
'  Print #1, "1"
'  Print #1, "62"
'  Print #1, "1"
'  Print #1, "10"
'  Print #1, frmt(YY(7), 4)
'  Print #1, "20"
'  Print #1, frmt(xx(7), 4)
'  Print #1, "30"
'  Print #1, "0.0"
'  Print #1, "11"
'  Print #1, frmt(YY(35), 4)
'  Print #1, "21"
'  Print #1, frmt(xx(35), 4)
'  Print #1, "31"
'  Print #1, "0.0"
'  '; Raggio fondo
'  A1 = FnRG(Atn((xx(35) - R318) / (YY(35) - R319)))
'  A2 = FnRG(Atn((xx(33) - R318) / (YY(33) - R319)))
'  Print #1, "0"
'  Print #1, "ARC"
'  Print #1, "8"
'  Print #1, "1"
'  Print #1, "62"
'  Print #1, "1"
'  Print #1, "10"
'  Print #1, frmt(R319, 4)
'  Print #1, "20"
'  Print #1, frmt(R318, 4)
'  Print #1, "30"
'  Print #1, "0.0"
'  Print #1, "40"
'  Print #1, frmt(R49, 4)
'  Print #1, "50"
'  Print #1, frmt(A2, 4)
'  Print #1, "51"
'  Print #1, frmt(A1, 4)
'  '; P33-P9
'  Print #1, "0"
'  Print #1, "LINE"
'  Print #1, "8"
'  Print #1, "1"
'  Print #1, "62"
'  Print #1, "1"
'  Print #1, "10"
'  Print #1, frmt(YY(33), 4)
'  Print #1, "20"
'  Print #1, frmt(xx(33), 4)
'  Print #1, "30"
'  Print #1, "0.0"
'  Print #1, "11"
'  Print #1, frmt(YY(9), 4)
'  Print #1, "21"
'  Print #1, frmt(xx(9), 4)
'  Print #1, "31"
'  Print #1, "0.0"
'  'fine
'  'PRINT #1, "0"
'  'PRINT #1, "ENDSEC"
'  'PRINT #1, "0"
'  'PRINT #1, "EOF"
'  Close #1
'Return
'
'SCRIVI_FILE_DXF_FIANCO2:
'  Open g_chOemPATH & "\MOLA.DXF" For Append As #1
'  'PRINT #1, "0"
'  'PRINT #1, "SECTION"
'  'PRINT #1, "2"
'  'PRINT #1, "ENTITIES"
'  'LINE P11-P12
'  Print #1, "0"
'  Print #1, "LINE"
'  Print #1, "8"
'  Print #1, "1"
'  Print #1, "62"
'  Print #1, "1"
'  Print #1, "10"
'  Print #1, frmt(YY(11), 4)
'  Print #1, "20"
'  Print #1, frmt(xx(11), 4)
'  Print #1, "30"
'  Print #1, "0.0"
'  Print #1, "11"
'  Print #1, frmt(YY(12), 4)
'  Print #1, "21"
'  Print #1, frmt(xx(12), 4)
'  Print #1, "31"
'  Print #1, "0.0"
'  'ARC C1
'  A1 = 180 + FnRG(Atn((xx(12) - R314) / (YY(12) - R315)))
'  A2 = 180 + FnRG(Atn((xx(20) - R314) / (YY(20) - R315)))
'  Print #1, "0"
'  Print #1, "ARC"
'  Print #1, "8"
'  Print #1, "1"
'  Print #1, "62"
'  Print #1, "1"
'  Print #1, "10"
'  Print #1, frmt(R315, 4)
'  Print #1, "20"
'  Print #1, frmt(R314, 4)
'  Print #1, "30"
'  Print #1, "0.0"
'  Print #1, "40"
'  Print #1, frmt(R304, 4)
'  Print #1, "50"
'  Print #1, frmt(A2, 4)
'  Print #1, "51"
'  Print #1, frmt(A1, 4)
'  If R71 > 0 Or R72 > 0 Then
'  'ARC C13
'  A1 = 180 + FnRG(Atn((xx(20) - R316) / (YY(20) - R317)))
'  A2 = 180 + FnRG(Atn((xx(13) - R316) / (YY(13) - R317)))
'  Print #1, "0"
'  Print #1, "ARC"
'  Print #1, "8"
'  Print #1, "1"
'  Print #1, "62"
'  Print #1, "1"
'  Print #1, "10"
'  Print #1, frmt(R317, 4)
'  Print #1, "20"
'  Print #1, frmt(R316, 4)
'  Print #1, "30"
'  Print #1, "0.0"
'  Print #1, "40"
'  Print #1, frmt(R305, 4)
'  Print #1, "50"
'  Print #1, frmt(A2, 4)
'  Print #1, "51"
'  Print #1, frmt(A1, 4)
'  End If
'  'LINE P3-P4
'  If R63 > 0 Then
'    Print #1, "0"
'    Print #1, "LINE"
'    Print #1, "8"
'    Print #1, "1"
'    Print #1, "62"
'    Print #1, "1"
'    Print #1, "10"
'    Print #1, frmt(YY(13), 4)
'    Print #1, "20"
'    Print #1, frmt(xx(13), 4)
'    Print #1, "30"
'    Print #1, "0.0"
'    Print #1, "11"
'    Print #1, frmt(YY(14), 4)
'    Print #1, "21"
'    Print #1, frmt(xx(14), 4)
'    Print #1, "31"
'    Print #1, "0.0"
'  End If
'  'LINE P4-P5
'  If R45 > 0 Then
'    Print #1, "0"
'    Print #1, "LINE"
'    Print #1, "8"
'    Print #1, "1"
'    Print #1, "62"
'    Print #1, "1"
'    Print #1, "10"
'    Print #1, frmt(YY(14), 4)
'    Print #1, "20"
'    Print #1, frmt(xx(14), 4)
'    Print #1, "30"
'    Print #1, "0.0"
'    Print #1, "11"
'    Print #1, frmt(YY(15), 4)
'    Print #1, "21"
'    Print #1, frmt(xx(15), 4)
'    Print #1, "31"
'    Print #1, "0.0"
'  End If
'  If R85 = 0 Then GoTo NORST12
'  If R247 = 0 Then
'    'LINE P15-P25
'    Print #1, "0"
'    Print #1, "LINE"
'    Print #1, "8"
'    Print #1, "1"
'    Print #1, "62"
'    Print #1, "1"
'    Print #1, "10"
'    Print #1, frmt(YY(15), 4)
'    Print #1, "20"
'    Print #1, frmt(xx(15), 4)
'    Print #1, "30"
'    Print #1, "0.0"
'    Print #1, "11"
'    Print #1, frmt(YY(25), 4)
'    Print #1, "21"
'    Print #1, frmt(xx(25), 4)
'    Print #1, "31"
'    Print #1, "0.0"
'   Else
'    'ARCO P15-P25
'    'Centro
'    ANGOLO = Atn((xx(15) - xx(25)) / (YY(25) - YY(15)))
'    DIST = Sqr((xx(15) - xx(25)) ^ 2 + (YY(25) - YY(15)) ^ 2)
'    PMC = Sqr(R242 ^ 2 - DIST ^ 2 / 4)
'    Xc = (xx(15) + xx(25)) / 2 + PMC * Cos(ANGOLO) * Sgn(R242)
'    YC = (YY(15) + YY(25)) / 2 + PMC * Sin(ANGOLO) * Sgn(R242)
'  If R242 > 0 Then
'          A2 = 270 - FnRG(Atn((YY(25) - YC) / (xx(25) - Xc)))
'          A1 = 270 - FnRG(Atn((YY(15) - YC) / (xx(15) - Xc)))
'      Else
'          A1 = 90 - FnRG(Atn((YY(25) - YC) / (xx(25) - Xc)))
'          A2 = 90 - FnRG(Atn((YY(15) - YC) / (xx(15) - Xc)))
'  End If
'    Print #1, "0"
'    Print #1, "ARC"
'    Print #1, "8"
'    Print #1, "1"
'    Print #1, "62"
'    Print #1, "1"
'    Print #1, "10"
'    Print #1, frmt(YC, 4)
'    Print #1, "20"
'    Print #1, frmt(Xc, 4)
'    Print #1, "30"
'    Print #1, "0.0"
'    Print #1, "40"
'    Print #1, frmt(Abs(R242), 4)
'    Print #1, "50"
'    Print #1, frmt(A1, 4)
'    Print #1, "51"
'    Print #1, frmt(A2, 4)
'  End If
'NORST12:
'  If R32 = 0 Then
'    'LINE P25-P26
'    Print #1, "0"
'    Print #1, "LINE"
'    Print #1, "8"
'    Print #1, "1"
'    Print #1, "62"
'    Print #1, "1"
'    Print #1, "10"
'    Print #1, frmt(YY(25), 4)
'    Print #1, "20"
'    Print #1, frmt(xx(25), 4)
'    Print #1, "30"
'    Print #1, "0.0"
'    Print #1, "11"
'    Print #1, frmt(YY(26), 4)
'    Print #1, "21"
'    Print #1, frmt(xx(26), 4)
'    Print #1, "31"
'    Print #1, "0.0"
'  Else
'  'ARC C15
'  ANGOLO = Atn((xx(25) - xx(22)) / (YY(22) - YY(25)))
'  DIST = Sqr((xx(25) - xx(22)) ^ 2 + (YY(22) - YY(25)) ^ 2)
'  PMC = Sqr(RR(3) ^ 2 - DIST ^ 2 / 4)
'  Xc = (xx(22) + xx(25)) / 2 - PMC * Cos(ANGOLO) * Sgn(RR(3))
'  YC = (YY(22) + YY(25)) / 2 - PMC * Sin(ANGOLO) * Sgn(RR(3))
'  If RR(3) < 0 Then
'    A2 = 270 - FnRG(Atn((YY(22) - YC) / (xx(22) - Xc)))
'    A1 = 270 - FnRG(Atn((YY(25) - YC) / (xx(25) - Xc)))
'  Else
'    A1 = 90 - FnRG(Atn((YY(22) - YC) / (xx(22) - Xc)))
'    A2 = 90 - FnRG(Atn((YY(25) - YC) / (xx(25) - Xc)))
'  End If
'  Print #1, "0"
'  Print #1, "ARC"
'  Print #1, "8"
'  Print #1, "1"
'  Print #1, "62"
'  Print #1, "1"
'  Print #1, "10"
'  Print #1, frmt(YC, 4)
'  Print #1, "20"
'  Print #1, frmt(Xc, 4)
'  Print #1, "30"
'  Print #1, "0.0"
'  Print #1, "40"
'  Print #1, frmt(Abs(RR(3)), 4)
'  Print #1, "50"
'  Print #1, frmt(A1, 4)
'  Print #1, "51"
'  Print #1, frmt(A2, 4)
'  ';*************  ARC C6 ********************
'  ANGOLO = Atn((xx(22) - xx(26)) / (YY(26) - YY(22)))
'  DIST = Sqr((xx(22) - xx(26)) ^ 2 + (YY(26) - YY(22)) ^ 2)
'  PMC = Sqr(RR(4) ^ 2 - DIST ^ 2 / 4)
'  Xc = (xx(26) + xx(22)) / 2 - PMC * Cos(ANGOLO) * Sgn(RR(4))
'  YC = (YY(26) + YY(22)) / 2 - PMC * Sin(ANGOLO) * Sgn(RR(4))
'  If RR(3) < 0 Then
'    A2 = 270 - FnRG(Atn((YY(26) - YC) / (xx(26) - Xc)))
'    A1 = 270 - FnRG(Atn((YY(22) - YC) / (xx(22) - Xc)))
'  Else
'    A1 = 90 - FnRG(Atn((YY(26) - YC) / (xx(26) - Xc)))
'    A2 = 90 - FnRG(Atn((YY(22) - YC) / (xx(22) - Xc)))
'  End If
'  Print #1, "0"
'  Print #1, "ARC"
'  Print #1, "8"
'  Print #1, "1"
'  Print #1, "62"
'  Print #1, "1"
'  Print #1, "10"
'  Print #1, frmt(YC, 4)
'  Print #1, "20"
'  Print #1, frmt(Xc, 4)
'  Print #1, "30"
'  Print #1, "0.0"
'  Print #1, "40"
'  Print #1, frmt(Abs(RR(4)), 4)
'  Print #1, "50"
'  Print #1, frmt(A1, 4)
'  Print #1, "51"
'  Print #1, frmt(A2, 4)
'  End If
'  '****************  P26-P17
'  If R87 = 0 Then GoTo P17P36
'  If R228 = 0 Then
'    Print #1, "0"
'    Print #1, "LINE"
'    Print #1, "8"
'    Print #1, "1"
'    Print #1, "62"
'    Print #1, "1"
'    Print #1, "10"
'    Print #1, frmt(YY(26), 4)
'    Print #1, "20"
'    Print #1, frmt(xx(26), 4)
'    Print #1, "30"
'    Print #1, "0.0"
'    Print #1, "11"
'    Print #1, frmt(YY(17), 4)
'    Print #1, "21"
'    Print #1, frmt(xx(17), 4)
'    Print #1, "31"
'    Print #1, "0.0"
'  Else
'    ANGOLO = Atn((xx(26) - xx(17)) / (YY(17) - YY(26)))
'    DIST = Sqr((xx(26) - xx(17)) ^ 2 + (YY(17) - YY(26)) ^ 2)
'    PMC = Sqr(R228 ^ 2 - DIST ^ 2 / 4)
'    Xc = (xx(17) + xx(26)) / 2 + PMC * Cos(ANGOLO) * Sgn(R228)
'    YC = (YY(17) + YY(26)) / 2 + PMC * Sin(ANGOLO) * Sgn(R228)
'    If R228 > 0 Then
'      A2 = 270 - FnRG(Atn((YY(17) - YC) / (xx(17) - Xc)))
'      A1 = 270 - FnRG(Atn((YY(26) - YC) / (xx(26) - Xc)))
'    Else
'      A1 = 90 - FnRG(Atn((YY(17) - YC) / (xx(17) - Xc)))
'      A2 = 90 - FnRG(Atn((YY(26) - YC) / (xx(26) - Xc)))
'    End If
'    Print #1, "0"
'    Print #1, "ARC"
'    Print #1, "8"
'    Print #1, "1"
'    Print #1, "62"
'    Print #1, "1"
'    Print #1, "10"
'    Print #1, frmt(YC, 4)
'    Print #1, "20"
'    Print #1, frmt(Xc, 4)
'    Print #1, "30"
'    Print #1, "0.0"
'    Print #1, "40"
'    Print #1, frmt(Abs(R228), 4)
'    Print #1, "50"
'    Print #1, frmt(A1, 4)
'    Print #1, "51"
'    Print #1, frmt(A2, 4)
'  End If
'P17P36:
'    Print #1, "0"
'    Print #1, "LINE"
'    Print #1, "8"
'    Print #1, "1"
'    Print #1, "62"
'    Print #1, "1"
'    Print #1, "10"
'    Print #1, frmt(YY(17), 4)
'    Print #1, "20"
'    Print #1, frmt(xx(17), 4)
'    Print #1, "30"
'    Print #1, "0.0"
'    Print #1, "11"
'    Print #1, frmt(YY(36), 4)
'    Print #1, "21"
'    Print #1, frmt(xx(36), 4)
'    Print #1, "31"
'    Print #1, "0.0"
'  '; Raggio fondo
'  A1 = FnRG(Atn((xx(36) - R318) / (YY(36) - R319)))
'  A2 = FnRG(Atn((xx(34) - R318) / (YY(34) - R319)))
'  Print #1, "0"
'  Print #1, "ARC"
'  Print #1, "8"
'  Print #1, "1"
'  Print #1, "62"
'  Print #1, "1"
'  Print #1, "10"
'  Print #1, frmt(R319, 4)
'  Print #1, "20"
'  Print #1, frmt(R318, 4)
'  Print #1, "30"
'  Print #1, "0.0"
'  Print #1, "40"
'  Print #1, frmt(R59, 4)
'  Print #1, "50"
'  Print #1, frmt(A1, 4)
'  Print #1, "51"
'  Print #1, frmt(A2, 4)
'  '; P34-P19
'          Print #1, "0"
'          Print #1, "LINE"
'          Print #1, "8"
'          Print #1, "1"
'          Print #1, "62"
'          Print #1, "1"
'          Print #1, "10"
'          Print #1, frmt(YY(34), 4)
'          Print #1, "20"
'          Print #1, frmt(xx(34), 4)
'          Print #1, "30"
'          Print #1, "0.0"
'          Print #1, "11"
'          Print #1, frmt(YY(19), 4)
'          Print #1, "21"
'          Print #1, frmt(xx(19), 4)
'          Print #1, "31"
'          Print #1, "0.0"
'ASSEY:
'          Print #1, "0"
'          Print #1, "LINE"
'          Print #1, "8"
'          Print #1, "1"
'          Print #1, "62"
'          Print #1, "1"
'          Print #1, "10"
'          Print #1, frmt(0, 4)
'          Print #1, "20"
'          Print #1, frmt(R38 / 2 + 0.5, 4)
'          Print #1, "30"
'          Print #1, "0.0"
'          Print #1, "11"
'          Print #1, frmt(0, 4)
'          Print #1, "21"
'          Print #1, frmt(-R38 / 2 - 0.5, 4)
'          Print #1, "31"
'          Print #1, "0.0"
'ASSEX:
'          Print #1, "0"
'          Print #1, "LINE"
'          Print #1, "8"
'          Print #1, "1"
'          Print #1, "62"
'          Print #1, "1"
'          Print #1, "10"
'          Print #1, frmt(R44 - R43 + 0.5, 4)
'          Print #1, "20"
'          Print #1, frmt(0, 4)
'          Print #1, "30"
'          Print #1, "0.0"
'          Print #1, "11"
'          Print #1, frmt(-R43 - 0.5, 4)
'          Print #1, "21"
'          Print #1, frmt(0, 4)
'          Print #1, "31"
'          Print #1, "0.0"
'  'fine
'  Print #1, "0"
'  Print #1, "ENDSEC"
'  Print #1, "0"
'  Print #1, "EOF"
'  Close #1
'Return
'
'WRTPUNTI:
''Open g_chOemPATH & "\PUNTI.TXT" For Output As #1
''Print #1, " P R F R C R : " & Format$(Date)
''Print #1, "       R39 =" & frmt(R39, 4)
''Print #1, "       R37 =" & frmt(R37, 4)
''Print #1, "       R38 =" & frmt(R38, 4)
''Print #1, " R106=" & frmt(R106, 4) & " R107=" & frmt(R107, 4)
''Print #1, " R41 =" & frmt(FnRG(R41), 4) & "  R51=" & frmt(FnRG(R51), 4)
''Print #1, " R42 =" & frmt(FnRG(R42), 4) & "  R52=" & frmt(FnRG(R52), 4)
''Print #1, " R43 =" & frmt(R43, 4) & " R53=" & frmt(R53, 4)
''Print #1, " R44 =" & frmt(R44, 4) & " R54=" & frmt(R54, 4)
''Print #1, " R45 =" & frmt(R45, 4) & " R55=" & frmt(R55, 4)
''Print #1, " R46 =" & frmt(R46, 4) & " R56=" & frmt(R56, 4)
''Print #1, " R47 =" & frmt(R47, 4) & " R57=" & frmt(R57, 4)
''Print #1, " R48 =" & frmt(R48, 4) & " R58=" & frmt(R58, 4)
''Print #1, " R49 =" & frmt(R48, 4) & " R59=" & frmt(R59, 4)
''Print #1, " R40 =" & frmt(FnRG(R40), 4) & "  R50=" & frmt(FnRG(R50), 4)
''Print #1, " R31 =" & frmt(R31, 4) & " R32=" & frmt(R32, 4)
''Print #1, "       R60 =" & frmt(FnRG(R60), 4)
''Print #1, " R61 =" & frmt(R61, 4) & " R71=" & frmt(R71, 4)
''Print #1, " R62 =" & frmt(R62, 4) & " R72=" & frmt(R72, 4)
''Print #1, " R63 =" & frmt(R63, 4) & " R73=" & frmt(R73, 4)
''Print #1, " R64 =" & frmt(R64, 4) & " R74=" & frmt(R74, 4)
''Print #1, " R65 =" & frmt(R65, 4) & " R75=" & frmt(R75, 4)
''Print #1, "       R179 =" & frmt(R179, 4)
''Print #1, " R80=" & frmt(R80, 4) & " R84=" & frmt(R84, 4)
''Print #1, " R81 =" & frmt(FnRG(R81), 4) & "  R85=" & frmt(FnRG(R85), 4)
''Print #1, " R237=" & frmt(R237, 4) & " R137=" & frmt(R137, 4)
''Print #1, " R82=" & frmt(R82, 4) & " R86=" & frmt(R86, 4)
''Print #1, " R83 =" & frmt(FnRG(R83), 4) & "  R87=" & frmt(FnRG(R87), 4)
''Print #1, " R247=" & frmt(R247, 4) & " R227=" & frmt(R227, 4)
''Print #1, " R171=" & frmt(R171, 4) & " R172=" & frmt(R172, 4)
''Print #1, " R173=" & frmt(R173, 4) & " R174=" & frmt(R174, 4)
''Close #1
'Return
'
'Exit Sub
'
End Sub

Sub LEGGI_CNC_CONTROLLO_CREATORI(ByVal NomeFile As String)
'
Dim i    As Integer
Dim j    As Integer
Dim X    As Double
Dim Np   As Integer
Dim riga As String
'
Dim GEN(20) As Double
'
On Error GoTo errLEGGI_CNC_CONTROLLO_CREATORI
  '
  WRITE_DIALOG "Reading data from CNC."
  '
  Open NomeFile For Output As #1
  '
  'APRO IL FILE DI DEFAULT PER COPIARE I VALORI LETTI
  riga = ""
  For i = 1 To 17
    GEN(i) = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/GEN[" & Format$(i + 1, "#0") & "]"))
    riga = riga & frmt(GEN(i), 4) & " "
  Next i
  Print #1, riga
  '
  'MsgBox "[" & riga & "]"
  Np = GEN(11)
  '
  'FIANCO 1
  WRITE_DIALOG "Reading F1 data from CNC."
  For i = 1 To Np
    riga = ""
    X = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/XX[" & Format$(i + 1, "#0") & "]"))
    riga = riga & frmt(X, 4) & " "
    X = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/YY[" & Format$(i + 1, "#0") & "]"))
    riga = riga & frmt(X, 4) & " "
    X = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/TT[" & Format$(i + 1, "#0") & "]"))
    riga = riga & frmt(X, 4) & " "
    X = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF1[" & Format$(i + 1, "#0") & "]"))
    riga = riga & frmt(X, 4)
    'MsgBox "[" & riga & "]"
    Print #1, riga
  Next i
  '
  'FIANCO 2
  WRITE_DIALOG "Reading F2 data from CNC."
  For i = 1 To Np
    riga = ""
    X = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/XX[" & Format$(i + 1 + 100, "#0") & "]"))
    riga = riga & frmt(-X, 4) & " "
    X = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/YY[" & Format$(i + 1 + 100, "#0") & "]"))
    riga = riga & frmt(X, 4) & " "
    X = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/TT[" & Format$(i + 1 + 100, "#0") & "]"))
    riga = riga & frmt(X, 4) & " "
    X = val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/MISF2[" & Format$(i + 1, "#0") & "]"))
    riga = riga & frmt(X, 4)
    'MsgBox "[" & riga & "]"
    Print #1, riga
  Next i
  '
  Close #1
  '
  i = InStr(NomeFile, "DEFAULT")
  If (i <= 0) Then
    If (Dir$(PathMIS & "\DEFAULT") <> "") Then Kill PathMIS & "\DEFAULT"
    FileCopy NomeFile, PathMIS & "\DEFAULT"
  End If
  '
  WRITE_DIALOG ""
  '
Exit Sub

errLEGGI_CNC_CONTROLLO_CREATORI:
  WRITE_DIALOG "SUB: LEGGI_CNC_CONTROLLO_CREATORI " & str(Err)
  Exit Sub
  
End Sub

Sub CONTROLLO_CREATORI_VISUALIZZAZIONE(ByVal iXX As Integer, ByVal iYY As Integer)
'
Dim riga          As String
Dim stmp          As String
Dim i             As Integer
Dim j             As Integer
Dim GEN(20)       As Double
Dim NomeControllo As String
Dim XX_F1()       As Double
Dim YY_F1()       As Double
Dim TT_F1()       As Double
Dim MISF1()       As Double
Dim XX_F2()       As Double
Dim YY_F2()       As Double
Dim TT_F2()       As Double
Dim MISF2()       As Double
Dim Np            As Integer
Dim PicW          As Single
Dim PicH1         As Single
Dim PicH2         As Single
Dim MLeft         As Single
Dim MTop          As Single
Dim pX1           As Single
Dim pX2           As Single
Dim pY1           As Single
Dim pY2           As Single
Dim ScalaPROFILO  As Single
Dim ScalaERRORI   As Single
Dim OX            As Single
Dim OY            As Single
Dim DL            As Single
Dim k1            As Integer
Dim k2            As Integer
Dim TextFont      As String
Dim MISF1max      As Double
Dim MISF1min      As Double
Dim MISF2max      As Double
Dim MISF2min      As Double
Dim SCRIVI_CNC    As String
Dim bgnRAGGI      As Integer
Dim endRAGGI      As Integer
Dim bgnPROTUB     As Integer
Dim endPROTUB     As Integer
Dim bgnTESTA      As Integer
Dim endTESTA      As Integer
Dim bgnFIANCO     As Integer
Dim endFIANCO     As Integer
Dim bgnPIEDE      As Integer
Dim endPIEDE      As Integer
Dim bgnSEMITOP    As Integer
Dim endSEMITOP    As Integer
Dim COLORE        As Integer
'
On Error GoTo errCONTROLLO_CREATORI_VISUALIZZAZIONE
  '
  'NASCONDO LA CASELLA DI TESTO SE NON SERVE
  If (CHK0.Combo1.ListIndex > 0) Then
    CHK0.txtHELP.BackColor = &H0&
    CHK0.txtHELP.ForeColor = &HFFFFFF
    CHK0.txtHELP.Visible = True
  Else
    CHK0.txtHELP.Visible = False
  End If
  '
  'PULISCO LO SCHERMO
  CHK0.Picture1.Cls
  '
  'SOLO PER PROVARE: SCRIVO SUL CNC I VALORI LETTI DAL FILE
  SCRIVI_CNC = "N"
  '
  'CARATTERI DI STAMPA
  TextFont = GetInfo("StampaParametri", "TextFont", PathFILEINI)
  If (LINGUA = "CH") Then TextFont = "MS Song"
  If (LINGUA = "RU") Then TextFont = "Arial Cyr"
  CHK0.Picture1.FontName = TextFont
  CHK0.Picture1.FontBold = False
  CHK0.Picture1.FontSize = 8
  '
  'LETTURA DEL NOME DEL FILE DI MISURA
  NomeControllo = GetInfo("CHK0", "FileMisura", Path_LAVORAZIONE_INI)
  '
  'LETTURA DATI DEL CREATORE DAL FILE DI CONTROLLO
  Open PathMIS & "\" & NomeControllo For Input As #1
    '
    'LETTURA DELLA PRIMA RIGA: INDICI DEI PUNTI
    Line Input #1, riga
    riga = Trim$(riga)
    i = 1
    j = InStr(riga, " ")
    While (j > 0)
      GEN(i) = val(Left$(riga, j - 1))
      riga = Right(riga, Len(riga) - j)
      i = i + 1
      j = InStr(riga, " ")
    Wend
    GEN(i) = val(riga)
    '
    'If (SCRIVI_CNC = "Y") Then
    '  For i = 1 To 17
    '    Call OPC_SCRIVI_DATO("/ACC/NCK/MGUD/GEN[" & Format$(i + 1, "##0") & "]", frmt(GEN(i), 4))
    '  Next i
    'End If
    '
    'VERIFICA DEI VALORI LETTI DALLA PRIMA RIGA DEL FILE
    'stmp = ""
    'For i = 1 To 17
    '  stmp = stmp & "GEN[0," & Format$(i) & "]=" & frmt(GEN(i), 4) & Chr(13)
    'Next i
    'MsgBox stmp
    '
    'INIZIO E FINE RAGGI TESTA
    bgnRAGGI = GEN(1)
    endRAGGI = GEN(2)
    'INIZIO E FINE TESTA
    bgnTESTA = GEN(3)
    endTESTA = GEN(4)
    'INIZIO E FINE FIANCO
    bgnFIANCO = GEN(5)
    endFIANCO = GEN(6)
    'INIZIO E FINE PIEDE
    bgnPIEDE = GEN(7)
    endPIEDE = GEN(8)
    'INIZIO E FINE SEMITOPPING
    bgnSEMITOP = GEN(9)
    endSEMITOP = GEN(10)
    'INIZIO E FINE PROTUBERANZA
    If (bgnTESTA = 0) Then
      'SENZA RASTREMAZIONE DI TESTA
      bgnPROTUB = endRAGGI
      endPROTUB = bgnFIANCO
    Else
      'CON   RASTREMAZIONE DI TESTA
      bgnPROTUB = endRAGGI
      endPROTUB = bgnTESTA
    End If
    '
    'INIZIALIZZO IL NUMERO DEI PUNTI DEL PROFILO
    Np = GEN(11)
    '
    'DIMENSIONO I VETTORI IN BASE AL NUMERO DEI PUNTI
    ReDim XX_F1(Np): ReDim YY_F1(Np): ReDim TT_F1(Np): ReDim MISF1(Np)
    ReDim XX_F2(Np): ReDim YY_F2(Np): ReDim TT_F2(Np): ReDim MISF2(Np)
    '
    'FIANCO 1
    For i = 1 To Np
      '
      Line Input #1, riga
      '
      'XX
      j = InStr(riga, " ")
      stmp = Left$(riga, j - 1)
      XX_F1(i) = val(stmp)
      'If (SCRIVI_CNC = "Y") Then Call OPC_SCRIVI_DATO("/ACC/NCK/MGUD/XX[" & Format$(i + 1, "##0") & "]", stmp)
      riga = Right(riga, Len(riga) - j)
      riga = Trim$(riga)
      'YY
      j = InStr(riga, " ")
      stmp = Left$(riga, j - 1)
      YY_F1(i) = val(stmp)
      'If (SCRIVI_CNC = "Y") Then Call OPC_SCRIVI_DATO("/ACC/NCK/MGUD/YY[" & Format$(i + 1, "##0") & "]", stmp)
      riga = Right(riga, Len(riga) - j)
      riga = Trim$(riga)
      'TT
      j = InStr(riga, " ")
      stmp = Left$(riga, j - 1)
      TT_F1(i) = val(stmp)
      'If (SCRIVI_CNC = "Y") Then Call OPC_SCRIVI_DATO("/ACC/NCK/MGUD/TT[" & Format$(i + 1, "##0") & "]", stmp)
      riga = Right(riga, Len(riga) - j)
      riga = Trim$(riga)
      'MIS
      stmp = riga
      MISF1(i) = val(stmp)
      'If (SCRIVI_CNC = "Y") Then Call OPC_SCRIVI_DATO("/ACC/NCK/MGUD/MISF1[" & Format$(i + 1, "##0") & "]", stmp)
      '
      'VERIFICA DEI VALORI LETTI DALLA PRIMA RIGA DEL FILE
      'stmp = ""
      'stmp = stmp & "XX_F1[0," & Format$(i, "##0") & "]=" & frmt(XX_F1(i), 4) & Chr(13)
      'stmp = stmp & "YY_F1[0," & Format$(i, "##0") & "]=" & frmt(YY_F1(i), 4) & Chr(13)
      'stmp = stmp & "TT_F1[0," & Format$(i, "##0") & "]=" & frmt(TT_F1(i), 4) & Chr(13)
      'stmp = stmp & "MISF1[0," & Format$(i, "##0") & "]=" & frmt(MISF1(i), 4) & Chr(13)
      'MsgBox stmp
      '
    Next i
    '
    'FIANCO 2
    For i = 1 To Np
      '
      Line Input #1, riga
      '
      'XX
      j = InStr(riga, " ")
      stmp = Left$(riga, j - 1)
      XX_F2(i) = val(stmp)
      'If (SCRIVI_CNC = "Y") Then Call OPC_SCRIVI_DATO("/ACC/NCK/MGUD/XX[" & Format$(i + 1 + 100, "##0") & "]", Mid$(stmp, 2))
      riga = Right(riga, Len(riga) - j)
      riga = Trim$(riga)
      'YY
      j = InStr(riga, " ")
      stmp = Left$(riga, j - 1)
      YY_F2(i) = val(stmp)
      'If (SCRIVI_CNC = "Y") Then Call OPC_SCRIVI_DATO("/ACC/NCK/MGUD/YY[" & Format$(i + 1 + 100, "##0") & "]", stmp)
      riga = Right(riga, Len(riga) - j)
      riga = Trim$(riga)
      'TT
      j = InStr(riga, " ")
      stmp = Left$(riga, j - 1)
      TT_F2(i) = val(stmp)
      'If (SCRIVI_CNC = "Y") Then Call OPC_SCRIVI_DATO("/ACC/NCK/MGUD/TT[" & Format$(i + 1 + 100, "##0") & "]", stmp)
      riga = Right(riga, Len(riga) - j)
      riga = Trim$(riga)
      'MIS
      stmp = riga
      MISF2(i) = val(stmp)
      'If (SCRIVI_CNC = "Y") Then Call OPC_SCRIVI_DATO("/ACC/NCK/MGUD/MISF2[" & Format$(i + 1, "##0") & "]", stmp)
      '
      'VERIFICA DEI VALORI LETTI DALLA PRIMA RIGA DEL FILE
      'stmp = ""
      'stmp = stmp & "XX_F2[0," & Format$(i, "##0") & "]=" & frmt(XX_F2(i), 4) & Chr(13)
      'stmp = stmp & "YY_F2[0," & Format$(i, "##0") & "]=" & frmt(YY_F2(i), 4) & Chr(13)
      'stmp = stmp & "TT_F2[0," & Format$(i, "##0") & "]=" & frmt(TT_F2(i), 4) & Chr(13)
      'stmp = stmp & "MISF2[0," & Format$(i, "##0") & "]=" & frmt(MISF2(i), 4) & Chr(13)
      'MsgBox stmp
      '
    Next i
    '
  Close #1
  '
  'VISUALIZZAZIONE DEL RISULTATO OTTENUTO
  'LETTURA DEI VALORI DI SCALA
  CHK0.txtScX.Text = GetInfo("CHK0", "ScalaProfiloCreatori", Path_LAVORAZIONE_INI)
  CHK0.txtScY.Text = GetInfo("CHK0", "ScalaErroreCreatori", Path_LAVORAZIONE_INI)
  CHK0.lblScX.Visible = True
  CHK0.lblScY.Visible = True
  CHK0.txtScX.Visible = True
  CHK0.txtScY.Visible = True
  '
  'RIPOSIZIONO LA FINESTRA
  CHK0.Picture1.ScaleTop = 0
  CHK0.Picture1.ScaleLeft = 0
  'FOGLIO DIMENSIONATO PER CM
  'PicW = 19
  'PicH1 = PicW / 2
  'PicH2 = 3.5
  PicW = 28
  PicH1 = PicW / 2
  PicH2 = 3.5
  'MARGINI
  MLeft = 0.2
  MTop = 0.4
  '
  'RIQUADRI PER VISUALIZZAZIONI E STAMPE
  'CHK0.Picture1.Line (MLeft, MTop)-(PicW + MLeft, PicH1 + MTop), , B
  'CHK0.Picture1.Line (MLeft, MTop)-(PicW / 2 + MLeft, PicH1 + MTop), , B
  'CHK0.Picture1.Line (MLeft, MTop + PicH1)-(PicW + MLeft, PicH1 + MTop + PicH2), , B
  '
  Dim ANGOLO As Single
  Dim NumeroRette As Integer
  Dim ftmp1 As Double
  '
  ANGOLO = 30
  NumeroRette = 30
  'ASSE XX
  pX1 = MLeft + PicW
  pY1 = MTop + PicH1
  CHK0.Picture1.Line (pX1 - PicW, pY1)-(pX1, pY1)
  'FRECCIA XX
  ftmp1 = (ANGOLO / 180 * PG) / NumeroRette
  For i = -NumeroRette To NumeroRette
    pX2 = pX1 - 0.3 * Cos((ANGOLO / 180 * PG))
    pY2 = pY1 + 0.3 * Sin(i * ftmp1)
    CHK0.Picture1.Line (pX1, pY1)-(pX2, pY2)
  Next i
  'NOME DELL'ASSE XX
  stmp = "XX"
  CHK0.Picture1.CurrentX = pX1 - CHK0.Picture1.TextWidth(stmp)
  CHK0.Picture1.CurrentY = pY1 - 0.3 - CHK0.Picture1.TextHeight(stmp)
  CHK0.Picture1.Print stmp
  'ASSE YY
  pX1 = MLeft + PicW / 2
  pY1 = MTop
  CHK0.Picture1.Line (pX1, pY1)-(pX1, pY1 + PicH1)
  'FRECCIA YY
  ftmp1 = (ANGOLO / 180 * PG) / NumeroRette
  For i = -NumeroRette To NumeroRette
    pX2 = pX1 + 0.3 * Sin(i * ftmp1)
    pY2 = pY1 + 0.3 * Cos((ANGOLO / 180 * PG))
    CHK0.Picture1.Line (pX1, pY1)-(pX2, pY2)
  Next i
  'NOME DELL'ASSE YY
  stmp = "YY"
  CHK0.Picture1.CurrentX = pX1 + 0.3
  CHK0.Picture1.CurrentY = pY1
  CHK0.Picture1.Print stmp
  'LETTURA VALORI DELLE SCALE DALLE CASELLE DI TESTO
  ScalaPROFILO = val(CHK0.txtScX.Text)
  ScalaERRORI = val(CHK0.txtScY.Text)
  'SCALA PROFILO
  pX1 = MLeft + 1
  pY1 = MTop + 2
  stmp = frmt(ScalaPROFILO, 4) & ":1"
  CHK0.Picture1.CurrentX = pX1 - CHK0.Picture1.TextWidth(stmp) / 2
  CHK0.Picture1.CurrentY = pY1 - 0.5 - 2 * CHK0.Picture1.TextHeight(stmp)
  CHK0.Picture1.Print stmp
  CHK0.Picture1.Line (pX1 - 0.5, pY1 - 0.5)-(pX1 + 0.5, pY1 - 0.5), QBColor(9)
  'FRECCIA SUPERIORE
  ftmp1 = (ANGOLO / 180 * PG) / NumeroRette
  For i = -NumeroRette To NumeroRette
    pX2 = pX1 + 0.3 * Sin(i * ftmp1)
    pY2 = pY1 - 0.5 - 0.3 * Cos((ANGOLO / 180 * PG))
    CHK0.Picture1.Line (pX1, pY1 - 0.5)-(pX2, pY2), QBColor(9)
  Next i
  'FRECCIA INFERIORE
  ftmp1 = (ANGOLO / 180 * PG) / NumeroRette
  For i = -NumeroRette To NumeroRette
    pX2 = pX1 + 0.3 * Sin(i * ftmp1)
    pY2 = pY1 + 0.5 + 0.3 * Cos((ANGOLO / 180 * PG))
    CHK0.Picture1.Line (pX1, pY1 + 0.5)-(pX2, pY2), QBColor(9)
  Next i
  CHK0.Picture1.Line (pX1 - 0.5, pY1 + 0.5)-(pX1 + 0.5, pY1 + 0.5), QBColor(9)
  stmp = frmt(10 / ScalaPROFILO, 4) & "mm"
  CHK0.Picture1.CurrentX = pX1 - CHK0.Picture1.TextWidth(stmp) / 2
  If CHK0.Picture1.CurrentX < MLeft Then CHK0.Picture1.CurrentX = MLeft  'FlagSTD005
  CHK0.Picture1.CurrentY = pY1 - CHK0.Picture1.TextHeight(stmp) / 2
  CHK0.Picture1.Print stmp
  'ORIGINE DEL RIFERIMENTO
  Dim xFIANCO As Integer
  Dim yFIANCO As Integer
  Dim iXXtmp  As Integer
  Dim iYYtmp  As Integer
  'INDICE XX PER IL RIFERIMENTO OX
    If (iXX >= Np) Then
      iXXtmp = Np: iXX = Np: xFIANCO = 1
  ElseIf (iXX >= 1) And (iXX < Np) Then
      iXXtmp = iXX: xFIANCO = 1
  ElseIf (iXX = 0) Then
      iXXtmp = 1: iXX = 1: xFIANCO = 1
  ElseIf (iXX > -Np) And (iXX <= -1) Then
      iXXtmp = Abs(iXX): xFIANCO = 2
  Else
      iXXtmp = Np: iXX = -Np: xFIANCO = 2
  End If
  'INDICE YY PER IL RIFERIMENTO OY
    If (iYY >= Np) Then
      iYYtmp = Np: iYY = Np: yFIANCO = 1
  ElseIf (iYY >= 1) And (iYY < Np) Then
      iYYtmp = iYY: yFIANCO = 1
  ElseIf (iYY = 0) Then
      iYYtmp = Np: iYY = Np: yFIANCO = 1
  ElseIf (iYY > -Np) And (iYY <= -1) Then
      iYYtmp = Abs(iYY): yFIANCO = 2
  Else
      iYYtmp = Np: iYY = -Np: yFIANCO = 2
  End If
  If xFIANCO = 1 Then
    OX = XX_F1(iXXtmp)
  Else
    OX = XX_F2(iXXtmp)
  End If
  If yFIANCO = 1 Then
    OY = YY_F1(iYYtmp)
  Else
    OY = YY_F2(iYYtmp)
  End If
  'VISUALIZZO OX E OY
  stmp = "(" & frmt(OX, 4) & ", " & frmt(OY, 4) & ")"
  CHK0.Picture1.CurrentX = MLeft + PicW / 2 - CHK0.Picture1.TextWidth(stmp) / 2
  CHK0.Picture1.CurrentY = MTop + PicH1
  CHK0.Picture1.Print stmp
  'AGGIORNO LE CASELLE
  CHK0.Text1(0).Text = Format$(iXX, "##0")
  CHK0.Text1(1).Text = Format$(iYY, "##0")
  'RISOLUZIONE CROCI
  DL = 0.1
  'PROFILO DEL FIANCO 1
  For i = 1 To Np - 1
    pX1 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F1(i) - OX) / 10
    pY1 = MTop + PicH1 - ScalaPROFILO * (YY_F1(i) - OY) / 10
    pX2 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F1(i + 1) - OX) / 10
    pY2 = MTop + PicH1 - ScalaPROFILO * (YY_F1(i + 1) - OY) / 10
    CHK0.Picture1.Line (pX1, pY1)-(pX2, pY2)
  Next i
  'PUNTI DEL FIANCO 1
  For i = 1 To Np
    pX1 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F1(i) - DL / 2 * Sin(TT_F1(i) * PG / 180) - OX) / 10
    pY1 = MTop + PicH1 - ScalaPROFILO * (YY_F1(i) + DL / 2 * Cos(TT_F1(i) * PG / 180) - OY) / 10
    pX2 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F1(i) + DL / 2 * Sin(TT_F1(i) * PG / 180) - OX) / 10
    pY2 = MTop + PicH1 - ScalaPROFILO * (YY_F1(i) - DL / 2 * Cos(TT_F1(i) * PG / 180) - OY) / 10
    CHK0.Picture1.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
    pX1 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F1(i) - DL / 2 * Cos(TT_F1(i) * PG / 180) - OX) / 10
    pY1 = MTop + PicH1 - ScalaPROFILO * (YY_F1(i) - DL / 2 * Sin(TT_F1(i) * PG / 180) - OY) / 10
    pX2 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F1(i) + DL / 2 * Cos(TT_F1(i) * PG / 180) - OX) / 10
    pY2 = MTop + PicH1 - ScalaPROFILO * (YY_F1(i) + DL / 2 * Sin(TT_F1(i) * PG / 180) - OY) / 10
    CHK0.Picture1.Line (pX1, pY1)-(pX2, pY2), QBColor(9)
  Next i
  'ERRORI SUL FIANCO 1
  MISF1max = 0
  MISF1min = 0
  For i = 1 To Np
    If (MISF1(i) < 1000) Then
      pX1 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F1(i) + ScalaERRORI * MISF1(i) * Cos(TT_F1(i) * PG / 180) / ScalaPROFILO - OX) / 10
      pY1 = MTop + PicH1 - ScalaPROFILO * (YY_F1(i) + ScalaERRORI * MISF1(i) * Sin(TT_F1(i) * PG / 180) / ScalaPROFILO - OY) / 10
      pX2 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F1(i) - OX) / 10
      pY2 = MTop + PicH1 - ScalaPROFILO * (YY_F1(i) - OY) / 10
      CHK0.Picture1.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
      If MISF1(i) >= MISF1max Then MISF1max = MISF1(i)
      If MISF1(i) <= MISF1min Then MISF1min = MISF1(i)
    End If
  Next i
  'CURVA DEGLI ERRORI SUL FIANCO 1
  If Np >= 2 Then
    k1 = 1
    Do
      'INDICE INIZIALE TRATTO
      While (MISF1(k1) = 1000) And (k1 < Np)
        k1 = k1 + 1
      Wend
      'INDICE FINALE TRATTO
      k2 = k1 + 1
      While (MISF1(k2) = 1000) And (k2 < Np)
        k2 = k2 + 1
      Wend
      'DISEGNO IL TRATTO
      If (k2 > k1) And (MISF1(k1) <> 1000) And (MISF1(k2) <> 1000) Then
        pX1 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F1(k1) + ScalaERRORI * MISF1(k1) * Cos(TT_F1(k1) * PG / 180) / ScalaPROFILO - OX) / 10
        pY1 = MTop + PicH1 - ScalaPROFILO * (YY_F1(k1) + ScalaERRORI * MISF1(k1) * Sin(TT_F1(k1) * PG / 180) / ScalaPROFILO - OY) / 10
        pX2 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F1(k2) + ScalaERRORI * MISF1(k2) * Cos(TT_F1(k2) * PG / 180) / ScalaPROFILO - OX) / 10
        pY2 = MTop + PicH1 - ScalaPROFILO * (YY_F1(k2) + ScalaERRORI * MISF1(k2) * Sin(TT_F1(k2) * PG / 180) / ScalaPROFILO - OY) / 10
        CHK0.Picture1.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
      End If
      'AGGIORNO PER IL PROSSIMO TRATTO
      k1 = k2
    Loop While (k2 < Np)
  End If
  '
  'NOME DEL FIANCO 1
  i = Np
  stmp = " F1 "
  CHK0.Picture1.CurrentX = MLeft + PicW / 2 + ScalaPROFILO * (XX_F1(i) - OX) / 10
  CHK0.Picture1.CurrentY = MTop + PicH1 - ScalaPROFILO * (YY_F1(i) - OY) / 10
  CHK0.Picture1.Print stmp
  '
  'PROFILO DEL FIANCO 2
  For i = 1 To Np - 1
    pX1 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F2(i) - OX) / 10
    pY1 = MTop + PicH1 - ScalaPROFILO * (YY_F2(i) - OY) / 10
    pX2 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F2(i + 1) - OX) / 10
    pY2 = MTop + PicH1 - ScalaPROFILO * (YY_F2(i + 1) - OY) / 10
    CHK0.Picture1.Line (pX1, pY1)-(pX2, pY2)
  Next i
  '
  'PUNTI DEL FIANCO 2
  For i = 1 To Np
    pX1 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F2(i) + DL / 2 * Sin(TT_F2(i) * PG / 180) - OX) / 10
    pY1 = MTop + PicH1 - ScalaPROFILO * (YY_F2(i) + DL / 2 * Cos(TT_F2(i) * PG / 180) - OY) / 10
    pX2 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F2(i) - DL / 2 * Sin(TT_F2(i) * PG / 180) - OX) / 10
    pY2 = MTop + PicH1 - ScalaPROFILO * (YY_F2(i) - DL / 2 * Cos(TT_F2(i) * PG / 180) - OY) / 10
    CHK0.Picture1.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
    pX1 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F2(i) - DL / 2 * Cos(TT_F2(i) * PG / 180) - OX) / 10
    pY1 = MTop + PicH1 - ScalaPROFILO * (YY_F2(i) + DL / 2 * Sin(TT_F2(i) * PG / 180) - OY) / 10
    pX2 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F2(i) + DL / 2 * Cos(TT_F2(i) * PG / 180) - OX) / 10
    pY2 = MTop + PicH1 - ScalaPROFILO * (YY_F2(i) - DL / 2 * Sin(TT_F2(i) * PG / 180) - OY) / 10
    CHK0.Picture1.Line (pX1, pY1)-(pX2, pY2), QBColor(9)
  Next i
  '
  'ERRORI SUL FIANCO 2
  MISF2max = 0
  MISF2min = 0
  For i = 1 To Np
    If (MISF2(i) < 1000) Then
      pX1 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F2(i) - ScalaERRORI * MISF2(i) * Cos(TT_F2(i) * PG / 180) / ScalaPROFILO - OX) / 10
      pY1 = MTop + PicH1 - ScalaPROFILO * (YY_F2(i) + ScalaERRORI * MISF2(i) * Sin(TT_F2(i) * PG / 180) / ScalaPROFILO - OY) / 10
      pX2 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F2(i) - OX) / 10
      pY2 = MTop + PicH1 - ScalaPROFILO * (YY_F2(i) - OY) / 10
      CHK0.Picture1.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
      If MISF2(i) >= MISF2max Then MISF2max = MISF2(i)
      If MISF2(i) <= MISF2min Then MISF2min = MISF2(i)
    End If
  Next i
  '
  'CURVA DEGLI ERRORI SUL FIANCO 2
  If Np >= 2 Then
    k1 = 1
    Do
      'INDICE INIZIALE TRATTO
      While (MISF2(k1) = 1000) And (k1 < Np)
        k1 = k1 + 1
      Wend
      'INDICE FINALE TRATTO
      k2 = k1 + 1
      While (MISF2(k2) = 1000) And (k2 < Np)
        k2 = k2 + 1
      Wend
      'DISEGNO IL TRATTO
      If (k2 > k1) And (MISF2(k1) <> 1000) And (MISF2(k2) <> 1000) Then
        pX1 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F2(k1) - ScalaERRORI * MISF2(k1) * Cos(TT_F2(k1) * PG / 180) / ScalaPROFILO - OX) / 10
        pY1 = MTop + PicH1 - ScalaPROFILO * (YY_F2(k1) + ScalaERRORI * MISF2(k1) * Sin(TT_F2(k1) * PG / 180) / ScalaPROFILO - OY) / 10
        pX2 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F2(k2) - ScalaERRORI * MISF2(k2) * Cos(TT_F2(k2) * PG / 180) / ScalaPROFILO - OX) / 10
        pY2 = MTop + PicH1 - ScalaPROFILO * (YY_F2(k2) + ScalaERRORI * MISF2(k2) * Sin(TT_F2(k2) * PG / 180) / ScalaPROFILO - OY) / 10
        CHK0.Picture1.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
      End If
      'AGGIORNO PER IL PROSSIMO TRATTO
      k1 = k2
    Loop While (k2 < Np)
  End If
  '
  'NOME DEL FIANCO 2
  i = Np
  stmp = " F2 "
  CHK0.Picture1.CurrentX = MLeft + PicW / 2 + ScalaPROFILO * (XX_F2(i) - OX) / 10 - CHK0.Picture1.TextWidth(stmp)
  CHK0.Picture1.CurrentY = MTop + PicH1 - ScalaPROFILO * (YY_F2(i) - OY) / 10
  CHK0.Picture1.Print stmp
  '
  Dim NpTESTA   As Integer
  Dim NpFIANCO  As Integer
  Dim NpPIEDE   As Integer
  Dim NpSEMITOP As Integer
  Dim B         As Double
  Dim NpSEZIONE As Integer
  '
  Dim pntX()  As Double
  Dim pntY()  As Double
  '
  'RAGGI DI TESTA
  Dim MAXerrMISR(2) As Double
  Dim MINerrMISR(2) As Double
  Dim errFormaR(2)  As Double
  Dim pntListaR(2)  As String
  'PROTUBERANZA
  Dim MAXerrMISB(2) As Double
  Dim MINerrMISB(2) As Double
  Dim errFormaB(2)  As Double
  Dim pntListaB(2)  As String
  'TESTA
  Dim AngoloTeoT(2) As Double
  Dim AngoloMisT(2) As Double
  Dim erroreT(2)    As Double
  Dim AltezzaT(2)   As Double
  Dim MAXerrMIST(2) As Double
  Dim MINerrMIST(2) As Double
  Dim errFormaT(2)  As Double
  Dim pntListaT(2)  As String
  'FIANCO
  Dim AngoloTeoF(2) As Double
  Dim AngoloMisF(2) As Double
  Dim erroreF(2)    As Double
  Dim AltezzaF(2)   As Double
  Dim MAXerrMISF(2) As Double
  Dim MINerrMISF(2) As Double
  Dim errFormaF(2)  As Double
  Dim pntListaF(2)  As String
  'PIEDE
  Dim AngoloTeoP(2) As Double
  Dim AngoloMisP(2) As Double
  Dim erroreP(2)    As Double
  Dim AltezzaP(2)   As Double
  Dim MAXerrMISP(2) As Double
  Dim MINerrMISP(2) As Double
  Dim errFormaP(2)  As Double
  Dim pntListaP(2)  As String
  'SEMITOPPING
  Dim AngoloTeoS(2) As Double
  Dim AngoloMisS(2) As Double
  Dim erroreS(2)    As Double
  Dim AltezzaS(2)   As Double
  Dim MAXerrMISS(2) As Double
  Dim MINerrMISS(2) As Double
  Dim errFormaS(2)  As Double
  Dim pntListaS(2)  As String
  '
  'VALUTAZIONI SUI RAGGI DI TESTA
  If (endRAGGI > bgnRAGGI) Then
    'ERRORE DI FORMA SUI RAGGI DEL FIANCO 1
    MAXerrMISR(1) = 0: MINerrMISR(1) = 0
    For i = (bgnRAGGI + 1) To (endRAGGI - 1)
      If MISF1(i) <> 1000 Then
        'SALVO GLI INDICI DEI PUNTI
        pntListaR(1) = pntListaR(1) & Format$(i, "##0") & ";"
        'RICERCA DEL MASSIMO E MINIMO
        If MISF1(i) >= MAXerrMISR(1) Then MAXerrMISR(1) = MISF1(i)
        If MISF1(i) <= MINerrMISR(1) Then MINerrMISR(1) = MISF1(i)
      End If
    Next i
    pntListaR(1) = Left$(pntListaR(1), Len(pntListaR(1)) - 1)
    errFormaR(1) = MAXerrMISR(1) - MINerrMISR(1)
    'ERRORE DI FORMA SUI RAGGI DEL FIANCO 2
    MAXerrMISR(2) = 0: MINerrMISR(2) = 0
    For i = (bgnRAGGI + 1) To (endRAGGI - 1)
      If MISF2(i) <> 1000 Then
        'SALVO GLI INDICI DEI PUNTI
        pntListaR(2) = pntListaR(2) & Format$(i, "##0") & ";"
        'RICERCA DEL MASSIMO E MINIMO
        If MISF2(i) >= MAXerrMISR(2) Then MAXerrMISR(2) = MISF2(i)
        If MISF2(i) <= MINerrMISR(2) Then MINerrMISR(2) = MISF2(i)
      End If
    Next i
    pntListaR(2) = Left$(pntListaR(2), Len(pntListaR(2)) - 1)
    errFormaR(2) = MAXerrMISR(2) - MINerrMISR(2)
  End If
  '
  'VALUTAZIONI SULLA PROTUBERANZA
  If (endPROTUB > bgnPROTUB) Then
    'ERRORE DI FORMA SULLA PROTUBERANZA DEL FIANCO 1
    MAXerrMISB(1) = 0: MINerrMISB(1) = 0
    For i = (bgnPROTUB + 1) To (endPROTUB - 1)
      If MISF1(i) <> 1000 Then
        'SALVO GLI INDICI DEI PUNTI
        pntListaB(1) = pntListaB(1) & Format$(i, "##0") & ";"
        'RICERCA DEL MASSIMO E MINIMO
        If MISF1(i) >= MAXerrMISB(1) Then MAXerrMISB(1) = MISF1(i)
        If MISF1(i) <= MINerrMISB(1) Then MINerrMISB(1) = MISF1(i)
      End If
    Next i
    pntListaB(1) = Left$(pntListaB(1), Len(pntListaB(1)) - 1)
    errFormaB(1) = MAXerrMISB(1) - MINerrMISB(1)
    'ERRORE DI FORMA SULLA PROTUBERANZA DEL FIANCO 2
    MAXerrMISB(2) = 0: MINerrMISB(2) = 0
    For i = (bgnPROTUB + 1) To (endPROTUB - 1)
      If MISF2(i) <> 1000 Then
        'SALVO GLI INDICI DEI PUNTI
        pntListaB(2) = pntListaB(2) & Format$(i, "##0") & ";"
        'RICERCA DEL MASSIMO E MINIMO
        If MISF2(i) >= MAXerrMISB(2) Then MAXerrMISB(2) = MISF2(i)
        If MISF2(i) <= MINerrMISB(2) Then MINerrMISB(2) = MISF2(i)
      End If
    Next i
    pntListaB(2) = Left$(pntListaB(2), Len(pntListaB(2)) - 1)
    errFormaB(2) = MAXerrMISB(2) - MINerrMISB(2)
  End If
  '
  'VALUTAZIONI SULLA RASTREMAZIONE DI TESTA
  If (endTESTA > bgnTESTA) Then
    'CALCOLO ALTEZZA TESTA DEL FIANCO 1
    AltezzaT(1) = YY_F1(bgnTESTA) - YY_F1(endTESTA)
    'CALCOLO DEI PUNTI TESTA DEL FIANCO 1
    NpTESTA = 0
    'CONSIDERO TUTTI I PUNTI DELLA TESTA DEL FIANCO 1
    For i = (bgnTESTA + 1) To (endTESTA - 1)
      If MISF1(i) <> 1000 Then
        'SALVO GLI INDICI DEI PUNTI
        pntListaT(1) = pntListaT(1) & Format$(i, "##0") & ";"
        'INCREMENTO SOLO PER I PUNTI CONTROLLATI
        NpTESTA = NpTESTA + 1
        'ALLOCO LO SPAZIO
        ReDim Preserve pntX(NpTESTA): ReDim Preserve pntY(NpTESTA)
        'CALCOLO LE COORDINATE DEL PUNTO MISURATO
        pntX(NpTESTA) = XX_F1(i)
        pntY(NpTESTA) = YY_F1(i)
      End If
    Next i
    pntListaT(1) = Left$(pntListaT(1), Len(pntListaT(1)) - 1)
    If NpTESTA >= 2 Then
      NpSEZIONE = NpTESTA: GoSub CALCOLO_COEFFICIENTE_ANGOLARE_b
      'CALCOLO ANGOLO TEORICO IN RADIANTI
      AngoloTeoT(1) = -Atn(B)
    Else
      AngoloTeoT(1) = TT_F1(bgnTESTA + 1) / 180 * PG
    End If
    'CALCOLO DEI PUNTI DEL FIANCO 1
    NpTESTA = 0
    MAXerrMIST(1) = 0: MINerrMIST(1) = 0
    'CONSIDERO TUTTI I PUNTI DEL FIANCO 1
    For i = (bgnTESTA + 1) To (endTESTA - 1)
      If MISF1(i) <> 1000 Then
        'INCREMENTO SOLO PER I PUNTI CONTROLLATI
        NpTESTA = NpTESTA + 1
        'CALCOLO LE COORDINATE DEL PUNTO MISURATO
        pntX(NpTESTA) = XX_F1(i) + MISF1(i) * Cos(TT_F1(i) * PG / 180)
        pntY(NpTESTA) = YY_F1(i) + MISF1(i) * Sin(TT_F1(i) * PG / 180)
        'RICERCA DEL MASSIMO E MINIMO
        If MISF1(i) >= MAXerrMIST(1) Then MAXerrMIST(1) = MISF1(i)
        If MISF1(i) <= MINerrMIST(1) Then MINerrMIST(1) = MISF1(i)
      End If
    Next i
    If NpTESTA >= 2 Then
      NpSEZIONE = NpTESTA: GoSub CALCOLO_COEFFICIENTE_ANGOLARE_b
      'CALCOLO ANGOLO TEORICO IN RADIANTI
      AngoloMisT(1) = -Atn(B)
    Else
      AngoloMisT(1) = TT_F1(bgnTESTA + 1) / 180 * PG
    End If
    erroreT(1) = (AngoloMisT(1) - AngoloTeoT(1))
    'CALCOLO ERRORE DI FORMA DEL FIANCO 1
    errFormaT(1) = 0
    For i = 1 To NpTESTA
      For j = 1 To NpTESTA
        errFormaT(0) = (pntX(j) - pntX(i)) * Cos(AngoloTeoT(1)) + (pntY(j) - pntY(i)) * Sin(AngoloTeoT(1))
        errFormaT(0) = Abs(errFormaT(0))
        If errFormaT(0) > errFormaT(1) Then errFormaT(1) = errFormaT(0)
      Next j
    Next i
    'CALCOLO ALTEZZA DEL FIANCO 2
    AltezzaT(2) = YY_F2(bgnTESTA) - YY_F2(endTESTA)
    'CALCOLO DEI PUNTI DEL FIANCO 2
    NpTESTA = 0
    'CONSIDERO TUTTI I PUNTI DEL FIANCO 2
    For i = (bgnTESTA + 1) To (endTESTA - 1)
      If MISF2(i) <> 1000 Then
        'SALVO GLI INDICI DEI PUNTI
        pntListaT(2) = pntListaT(2) & Format$(i, "##0") & ";"
        'INCREMENTO SOLO PER I PUNTI CONTROLLATI
        NpTESTA = NpTESTA + 1
        'ALLOCO LO SPAZIO
        ReDim Preserve pntX(NpTESTA): ReDim Preserve pntY(NpTESTA)
        'CALCOLO LE COORDINATE DEL PUNTO MISURATO
        pntX(NpTESTA) = XX_F2(i)
        pntY(NpTESTA) = YY_F2(i)
      End If
    Next i
    pntListaT(2) = Left$(pntListaT(2), Len(pntListaT(2)) - 1)
    If NpTESTA >= 2 Then
      NpSEZIONE = NpTESTA: GoSub CALCOLO_COEFFICIENTE_ANGOLARE_b
      'CALCOLO ANGOLO TEORICO IN RADIANTI
      AngoloTeoT(2) = Atn(B)
    Else
      AngoloTeoT(2) = TT_F2(bgnTESTA + 1) / 180 * PG
    End If
    'CALCOLO DEI PUNTI DEL FIANCO 2
    NpTESTA = 0
    MAXerrMIST(2) = 0: MINerrMIST(2) = 0
    'CONSIDERO TUTTI I PUNTI DEL FIANCO 2
    For i = (bgnTESTA + 1) To (endTESTA - 1)
      If MISF2(i) <> 1000 Then
        'INCREMENTO SOLO PER I PUNTI CONTROLLATI
        NpTESTA = NpTESTA + 1
        'CALCOLO LE COORDINATE DEL PUNTO MISURATO
        pntX(NpTESTA) = XX_F2(i) - MISF2(i) * Cos(TT_F2(i) * PG / 180)
        pntY(NpTESTA) = YY_F2(i) + MISF2(i) * Sin(TT_F2(i) * PG / 180)
        'RICERCA DEL MASSIMO E MINIMO
        If MISF2(i) >= MAXerrMIST(2) Then MAXerrMIST(2) = MISF2(i)
        If MISF2(i) <= MINerrMIST(2) Then MINerrMIST(2) = MISF2(i)
      End If
    Next i
    If NpTESTA >= 2 Then
      NpSEZIONE = NpTESTA: GoSub CALCOLO_COEFFICIENTE_ANGOLARE_b
      'CALCOLO ANGOLO TEORICO IN RADIANTI
      AngoloMisT(2) = Atn(B)
    Else
      AngoloMisT(2) = TT_F2(bgnTESTA + 1) / 180 * PG
    End If
    erroreT(2) = AngoloMisT(2) - AngoloTeoT(2)
    'CALCOLO ERRORE DI FORMA DEL FIANCO 2
    errFormaT(2) = 0
    For i = 1 To NpTESTA
      For j = 1 To NpTESTA
        errFormaT(0) = -(pntX(j) - pntX(i)) * Cos(AngoloTeoT(2)) + (pntY(j) - pntY(i)) * Sin(AngoloTeoT(2))
        errFormaT(0) = Abs(errFormaT(0))
        If errFormaT(0) > errFormaT(2) Then errFormaT(2) = errFormaT(0)
      Next j
    Next i
  End If
  '
  'VALUTAZIONI SUI FIANCHI
  If (endFIANCO > bgnFIANCO) Then
    'CALCOLO ALTEZZA DEL FIANCO 1
    AltezzaF(1) = YY_F1(bgnFIANCO) - YY_F1(endFIANCO)
    'CALCOLO DEI PUNTI DEL FIANCO 1
    NpFIANCO = 0
    'CONSIDERO TUTTI I PUNTI DEL FIANCO 1
    For i = (bgnFIANCO + 1) To (endFIANCO - 1)
      If MISF1(i) <> 1000 Then
        'SALVO GLI INDICI DEI PUNTI
        pntListaF(1) = pntListaF(1) & Format$(i, "##0") & ";"
        'INCREMENTO SOLO PER I PUNTI CONTROLLATI
        NpFIANCO = NpFIANCO + 1
        'ALLOCO LO SPAZIO
        ReDim Preserve pntX(NpFIANCO): ReDim Preserve pntY(NpFIANCO)
        'CALCOLO LE COORDINATE DEL PUNTO MISURATO
        pntX(NpFIANCO) = XX_F1(i)
        pntY(NpFIANCO) = YY_F1(i)
      End If
    Next i
    pntListaF(1) = Left$(pntListaF(1), Len(pntListaF(1)) - 1)
    If NpFIANCO >= 2 Then
      NpSEZIONE = NpFIANCO: GoSub CALCOLO_COEFFICIENTE_ANGOLARE_b
      'CALCOLO ANGOLO TEORICO IN RADIANTI
      AngoloTeoF(1) = -Atn(B)
    Else
      AngoloTeoF(1) = TT_F1(bgnFIANCO + 1) / 180 * PG
    End If
    'CALCOLO DEI PUNTI DEL FIANCO 1
    NpFIANCO = 0
    MAXerrMISF(1) = 0: MINerrMISF(1) = 0
    'CONSIDERO TUTTI I PUNTI DEL FIANCO 1
    For i = (bgnFIANCO + 1) To (endFIANCO - 1)
      If MISF1(i) <> 1000 Then
        'INCREMENTO SOLO PER I PUNTI CONTROLLATI
        NpFIANCO = NpFIANCO + 1
        'CALCOLO LE COORDINATE DEL PUNTO MISURATO
        pntX(NpFIANCO) = XX_F1(i) + MISF1(i) * Cos(TT_F1(i) * PG / 180)
        pntY(NpFIANCO) = YY_F1(i) + MISF1(i) * Sin(TT_F1(i) * PG / 180)
        'RICERCA DEL MASSIMO E MINIMO
        If MISF1(i) >= MAXerrMISF(1) Then MAXerrMISF(1) = MISF1(i)
        If MISF1(i) <= MINerrMISF(1) Then MINerrMISF(1) = MISF1(i)
      End If
    Next i
    If NpFIANCO >= 2 Then
      NpSEZIONE = NpFIANCO: GoSub CALCOLO_COEFFICIENTE_ANGOLARE_b
      'CALCOLO ANGOLO TEORICO IN RADIANTI
      AngoloMisF(1) = -Atn(B)
    Else
      AngoloMisF(1) = TT_F1(bgnFIANCO + 1) / 180 * PG
    End If
    erroreF(1) = (AngoloMisF(1) - AngoloTeoF(1))
    'CALCOLO ERRORE DI FORMA DEL FIANCO 1
    errFormaF(1) = 0
    For i = 1 To NpFIANCO
      For j = 1 To NpFIANCO
        errFormaF(0) = (pntX(j) - pntX(i)) * Cos(AngoloTeoF(1)) + (pntY(j) - pntY(i)) * Sin(AngoloTeoF(1))
        errFormaF(0) = Abs(errFormaF(0))
        If errFormaF(0) > errFormaF(1) Then errFormaF(1) = errFormaF(0)
      Next j
    Next i
    'CALCOLO ALTEZZA DEL FIANCO 2
    AltezzaF(2) = YY_F2(bgnFIANCO) - YY_F2(endFIANCO)
    'CALCOLO DEI PUNTI DEL FIANCO 2
    NpFIANCO = 0
    'CONSIDERO TUTTI I PUNTI DEL FIANCO 2
    For i = (bgnFIANCO + 1) To (endFIANCO - 1)
      If MISF2(i) <> 1000 Then
        'SALVO GLI INDICI DEI PUNTI
        pntListaF(2) = pntListaF(2) & Format$(i, "##0") & ";"
        'INCREMENTO SOLO PER I PUNTI CONTROLLATI
        NpFIANCO = NpFIANCO + 1
        'ALLOCO LO SPAZIO
        ReDim Preserve pntX(NpFIANCO): ReDim Preserve pntY(NpFIANCO)
        'CALCOLO LE COORDINATE DEL PUNTO MISURATO
        pntX(NpFIANCO) = XX_F2(i)
        pntY(NpFIANCO) = YY_F2(i)
      End If
    Next i
    pntListaF(2) = Left$(pntListaF(2), Len(pntListaF(2)) - 1)
    If NpFIANCO >= 2 Then
      NpSEZIONE = NpFIANCO: GoSub CALCOLO_COEFFICIENTE_ANGOLARE_b
      'CALCOLO ANGOLO TEORICO IN RADIANTI
      AngoloTeoF(2) = Atn(B)
    Else
      AngoloTeoF(2) = TT_F2(bgnFIANCO + 1) / 180 * PG
    End If
    'CALCOLO DEI PUNTI DEL FIANCO 2
    NpFIANCO = 0
    MAXerrMISF(2) = 0: MINerrMISF(2) = 0
    'CONSIDERO TUTTI I PUNTI DEL FIANCO 2
    For i = (bgnFIANCO + 1) To (endFIANCO - 1)
      If MISF2(i) <> 1000 Then
        'INCREMENTO SOLO PER I PUNTI CONTROLLATI
        NpFIANCO = NpFIANCO + 1
        'CALCOLO LE COORDINATE DEL PUNTO MISURATO
        pntX(NpFIANCO) = XX_F2(i) - MISF2(i) * Cos(TT_F2(i) * PG / 180)
        pntY(NpFIANCO) = YY_F2(i) + MISF2(i) * Sin(TT_F2(i) * PG / 180)
        'RICERCA DEL MASSIMO E MINIMO
        If MISF2(i) >= MAXerrMISF(2) Then MAXerrMISF(2) = MISF2(i)
        If MISF2(i) <= MINerrMISF(2) Then MINerrMISF(2) = MISF2(i)
      End If
    Next i
    If NpFIANCO >= 2 Then
      NpSEZIONE = NpFIANCO: GoSub CALCOLO_COEFFICIENTE_ANGOLARE_b
      'CALCOLO ANGOLO TEORICO IN RADIANTI
      AngoloMisF(2) = Atn(B)
    Else
      AngoloMisF(2) = TT_F2(bgnFIANCO + 1) / 180 * PG
    End If
    erroreF(2) = AngoloMisF(2) - AngoloTeoF(2)
    'CALCOLO ERRORE DI FORMA DEL FIANCO 2
    errFormaF(2) = 0
    For i = 1 To NpFIANCO
      For j = 1 To NpFIANCO
        errFormaF(0) = -(pntX(j) - pntX(i)) * Cos(AngoloTeoF(2)) + (pntY(j) - pntY(i)) * Sin(AngoloTeoF(2))
        errFormaF(0) = Abs(errFormaF(0))
        If errFormaF(0) > errFormaF(2) Then errFormaF(2) = errFormaF(0)
      Next j
    Next i
  End If
  '
  'VALUTAZIONI SULLA RASTREMAZIONE DI PIEDE
  If (endPIEDE > bgnPIEDE) Then
    'CALCOLO ALTEZZA TESTA DEL FIANCO 1
    AltezzaP(1) = YY_F1(bgnPIEDE) - YY_F1(endPIEDE)
    'CALCOLO DEI PUNTI TESTA DEL FIANCO 1
    NpPIEDE = 0
    'CONSIDERO TUTTI I PUNTI DELLA TESTA DEL FIANCO 1
    For i = (bgnPIEDE + 1) To (endPIEDE - 1)
      If MISF1(i) <> 1000 Then
        'SALVO GLI INDICI DEI PUNTI
        pntListaP(1) = pntListaP(1) & Format$(i, "##0") & ";"
        'INCREMENTO SOLO PER I PUNTI CONTROLLATI
        NpPIEDE = NpPIEDE + 1
        'ALLOCO LO SPAZIO
        ReDim Preserve pntX(NpPIEDE): ReDim Preserve pntY(NpPIEDE)
        'CALCOLO LE COORDINATE DEL PUNTO MISURATO
        pntX(NpPIEDE) = XX_F1(i)
        pntY(NpPIEDE) = YY_F1(i)
      End If
    Next i
    pntListaP(1) = Left$(pntListaP(1), Len(pntListaP(1)) - 1)
    If NpPIEDE >= 2 Then
      NpSEZIONE = NpPIEDE: GoSub CALCOLO_COEFFICIENTE_ANGOLARE_b
      'CALCOLO ANGOLO TEORICO IN RADIANTI
      AngoloTeoP(1) = -Atn(B)
    Else
      AngoloTeoP(1) = TT_F1(bgnPIEDE + 1) / 180 * PG
    End If
    'CALCOLO DEI PUNTI DEL FIANCO 1
    NpPIEDE = 0
    MAXerrMISP(1) = 0: MINerrMISP(1) = 0
    'CONSIDERO TUTTI I PUNTI DEL FIANCO 1
    For i = (bgnPIEDE + 1) To (endPIEDE - 1)
      If MISF1(i) <> 1000 Then
        'INCREMENTO SOLO PER I PUNTI CONTROLLATI
        NpPIEDE = NpPIEDE + 1
        'CALCOLO LE COORDINATE DEL PUNTO MISURATO
        pntX(NpPIEDE) = XX_F1(i) + MISF1(i) * Cos(TT_F1(i) * PG / 180)
        pntY(NpPIEDE) = YY_F1(i) + MISF1(i) * Sin(TT_F1(i) * PG / 180)
        'RICERCA DEL MASSIMO E MINIMO
        If MISF1(i) >= MAXerrMISP(1) Then MAXerrMISP(1) = MISF1(i)
        If MISF1(i) <= MINerrMISP(1) Then MINerrMISP(1) = MISF1(i)
      End If
    Next i
    If NpPIEDE >= 2 Then
      NpSEZIONE = NpPIEDE: GoSub CALCOLO_COEFFICIENTE_ANGOLARE_b
      'CALCOLO ANGOLO TEORICO IN RADIANTI
      AngoloMisP(1) = -Atn(B)
    Else
      AngoloMisP(1) = TT_F1(bgnPIEDE + 1) / 180 * PG
    End If
    erroreP(1) = (AngoloMisP(1) - AngoloTeoP(1))
    'CALCOLO ERRORE DI FORMA DEL FIANCO 1
    errFormaP(1) = 0
    For i = 1 To NpPIEDE
      For j = 1 To NpPIEDE
        errFormaP(0) = (pntX(j) - pntX(i)) * Cos(AngoloTeoP(1)) + (pntY(j) - pntY(i)) * Sin(AngoloTeoP(1))
        errFormaP(0) = Abs(errFormaP(0))
        If errFormaP(0) > errFormaP(1) Then errFormaP(1) = errFormaP(0)
      Next j
    Next i
    'CALCOLO ALTEZZA DEL FIANCO 2
    AltezzaP(2) = YY_F2(bgnPIEDE) - YY_F2(endPIEDE)
    'CALCOLO DEI PUNTI DEL FIANCO 2
    NpPIEDE = 0
    'CONSIDERO TUTTI I PUNTI DEL FIANCO 2
    For i = (bgnPIEDE + 1) To (endPIEDE - 1)
      If MISF2(i) <> 1000 Then
        'SALVO GLI INDICI DEI PUNTI
        pntListaP(2) = pntListaP(2) & Format$(i, "##0") & ";"
        'INCREMENTO SOLO PER I PUNTI CONTROLLATI
        NpPIEDE = NpPIEDE + 1
        'ALLOCO LO SPAZIO
        ReDim Preserve pntX(NpPIEDE): ReDim Preserve pntY(NpPIEDE)
        'CALCOLO LE COORDINATE DEL PUNTO MISURATO
        pntX(NpPIEDE) = XX_F2(i)
        pntY(NpPIEDE) = YY_F2(i)
      End If
    Next i
    pntListaF(2) = Left$(pntListaF(2), Len(pntListaF(2)) - 1)
    If NpPIEDE >= 2 Then
      NpSEZIONE = NpPIEDE: GoSub CALCOLO_COEFFICIENTE_ANGOLARE_b
      'CALCOLO ANGOLO TEORICO IN RADIANTI
      AngoloTeoP(2) = Atn(B)
    Else
      AngoloTeoP(2) = TT_F2(bgnPIEDE + 1) / 180 * PG
    End If
    'CALCOLO DEI PUNTI DEL FIANCO 2
    NpPIEDE = 0
    MAXerrMISP(2) = 0: MINerrMISP(2) = 0
    'CONSIDERO TUTTI I PUNTI DEL FIANCO 2
    For i = (bgnPIEDE + 1) To (endPIEDE - 1)
      If MISF2(i) <> 1000 Then
        'INCREMENTO SOLO PER I PUNTI CONTROLLATI
        NpPIEDE = NpPIEDE + 1
        'CALCOLO LE COORDINATE DEL PUNTO MISURATO
        pntX(NpPIEDE) = XX_F2(i) - MISF2(i) * Cos(TT_F2(i) * PG / 180)
        pntY(NpPIEDE) = YY_F2(i) + MISF2(i) * Sin(TT_F2(i) * PG / 180)
        'RICERCA DEL MASSIMO E MINIMO
        If MISF2(i) >= MAXerrMISP(2) Then MAXerrMISP(2) = MISF2(i)
        If MISF2(i) <= MINerrMISP(2) Then MINerrMISP(2) = MISF2(i)
      End If
    Next i
    If NpPIEDE >= 2 Then
      NpSEZIONE = NpPIEDE: GoSub CALCOLO_COEFFICIENTE_ANGOLARE_b
      'CALCOLO ANGOLO TEORICO IN RADIANTI
      AngoloMisP(2) = Atn(B)
    Else
      AngoloMisP(2) = TT_F2(bgnPIEDE + 1) / 180 * PG
    End If
    erroreP(2) = AngoloMisP(2) - AngoloTeoP(2)
    'CALCOLO ERRORE DI FORMA DEL FIANCO 2
    errFormaP(2) = 0
    For i = 1 To NpPIEDE
      For j = 1 To NpPIEDE
        errFormaP(0) = -(pntX(j) - pntX(i)) * Cos(AngoloTeoP(2)) + (pntY(j) - pntY(i)) * Sin(AngoloTeoP(2))
        errFormaP(0) = Abs(errFormaP(0))
        If errFormaP(0) > errFormaP(2) Then errFormaP(2) = errFormaP(0)
      Next j
    Next i
  End If
  '
  'VALUTAZIONI SUL SEMITOPPING
  If (endSEMITOP > bgnSEMITOP) Then
    'CALCOLO ALTEZZA TESTA DEL FIANCO 1
    AltezzaS(1) = YY_F1(bgnSEMITOP) - YY_F1(endSEMITOP)
    'CALCOLO DEI PUNTI SEMITOP DEL FIANCO 1
    NpSEMITOP = 0
    'CONSIDERO TUTTI I PUNTI DELLA SEMITOP DEL FIANCO 1
    For i = (bgnSEMITOP + 1) To (endSEMITOP - 1)
      If MISF1(i) <> 1000 Then
        'SALVO GLI INDICI DEI PUNTI
        pntListaS(1) = pntListaS(1) & Format$(i, "##0") & ";"
        'INCREMENTO SOLO PER I PUNTI CONTROLLATI
        NpSEMITOP = NpSEMITOP + 1
        'ALLOCO LO SPAZIO
        ReDim Preserve pntX(NpSEMITOP): ReDim Preserve pntY(NpSEMITOP)
        'CALCOLO LE COORDINATE DEL PUNTO MISURATO
        pntX(NpSEMITOP) = XX_F1(i)
        pntY(NpSEMITOP) = YY_F1(i)
      End If
    Next i
    pntListaS(1) = Left$(pntListaS(1), Len(pntListaS(1)) - 1)
    If NpSEMITOP >= 2 Then
      NpSEZIONE = NpSEMITOP: GoSub CALCOLO_COEFFICIENTE_ANGOLARE_b
      'CALCOLO ANGOLO TEORICO IN RADIANTI
      AngoloTeoS(1) = -Atn(B)
    Else
      AngoloTeoS(1) = TT_F1(bgnSEMITOP + 1) / 180 * PG
    End If
    'CALCOLO DEI PUNTI DEL FIANCO 1
    NpSEMITOP = 0
    MAXerrMISS(1) = 0: MINerrMISS(1) = 0
    'CONSIDERO TUTTI I PUNTI DEL FIANCO 1
    For i = (bgnSEMITOP + 1) To (endSEMITOP - 1)
      If MISF1(i) <> 1000 Then
        'INCREMENTO SOLO PER I PUNTI CONTROLLATI
        NpSEMITOP = NpSEMITOP + 1
        'CALCOLO LE COORDINATE DEL PUNTO MISURATO
        pntX(NpSEMITOP) = XX_F1(i) + MISF1(i) * Cos(TT_F1(i) * PG / 180)
        pntY(NpSEMITOP) = YY_F1(i) + MISF1(i) * Sin(TT_F1(i) * PG / 180)
        'RICERCA DEL MASSIMO E MINIMO
        If MISF1(i) >= MAXerrMISS(1) Then MAXerrMISS(1) = MISF1(i)
        If MISF1(i) <= MINerrMISS(1) Then MINerrMISS(1) = MISF1(i)
      End If
    Next i
    If NpSEMITOP >= 2 Then
      NpSEZIONE = NpSEMITOP: GoSub CALCOLO_COEFFICIENTE_ANGOLARE_b
      'CALCOLO ANGOLO TEORICO IN RADIANTI
      AngoloMisS(1) = -Atn(B)
    Else
      AngoloMisS(1) = TT_F1(bgnSEMITOP + 1) / 180 * PG
    End If
    erroreS(1) = (AngoloMisS(1) - AngoloTeoS(1))
    'CALCOLO ERRORE DI FORMA DEL FIANCO 1
    errFormaS(1) = 0
    For i = 1 To NpSEMITOP
      For j = 1 To NpSEMITOP
        errFormaS(0) = (pntX(j) - pntX(i)) * Cos(AngoloTeoS(1)) + (pntY(j) - pntY(i)) * Sin(AngoloTeoS(1))
        errFormaS(0) = Abs(errFormaS(0))
        If errFormaS(0) > errFormaS(1) Then errFormaS(1) = errFormaS(0)
      Next j
    Next i
    'CALCOLO ALTEZZA DEL FIANCO 2
    AltezzaS(2) = YY_F2(bgnSEMITOP) - YY_F2(endSEMITOP)
    'CALCOLO DEI PUNTI DEL FIANCO 2
    NpSEMITOP = 0
    'CONSIDERO TUTTI I PUNTI DEL FIANCO 2
    For i = (bgnSEMITOP + 1) To (endSEMITOP - 1)
      If MISF2(i) <> 1000 Then
        'SALVO GLI INDICI DEI PUNTI
        pntListaS(2) = pntListaS(2) & Format$(i, "##0") & ";"
        'INCREMENTO SOLO PER I PUNTI CONTROLLATI
        NpSEMITOP = NpSEMITOP + 1
        'ALLOCO LO SPAZIO
        ReDim Preserve pntX(NpSEMITOP): ReDim Preserve pntY(NpSEMITOP)
        'CALCOLO LE COORDINATE DEL PUNTO MISURATO
        pntX(NpSEMITOP) = XX_F2(i)
        pntY(NpSEMITOP) = YY_F2(i)
      End If
    Next i
    pntListaS(2) = Left$(pntListaS(2), Len(pntListaS(2)) - 1)
    If NpSEMITOP >= 2 Then
      NpSEZIONE = NpSEMITOP: GoSub CALCOLO_COEFFICIENTE_ANGOLARE_b
      'CALCOLO ANGOLO TEORICO IN RADIANTI
      AngoloTeoS(2) = Atn(B)
    Else
      AngoloTeoS(2) = TT_F2(bgnSEMITOP + 1) / 180 * PG
    End If
    'CALCOLO DEI PUNTI DEL FIANCO 2
    NpSEMITOP = 0
    MAXerrMISS(2) = 0: MINerrMISS(2) = 0
    'CONSIDERO TUTTI I PUNTI DEL FIANCO 2
    For i = (bgnSEMITOP + 1) To (endSEMITOP - 1)
      If MISF2(i) <> 1000 Then
        'INCREMENTO SOLO PER I PUNTI CONTROLLATI
        NpSEMITOP = NpSEMITOP + 1
        'CALCOLO LE COORDINATE DEL PUNTO MISURATO
        pntX(NpSEMITOP) = XX_F2(i) - MISF2(i) * Cos(TT_F2(i) * PG / 180)
        pntY(NpSEMITOP) = YY_F2(i) + MISF2(i) * Sin(TT_F2(i) * PG / 180)
        'RICERCA DEL MASSIMO E MINIMO
        If MISF2(i) >= MAXerrMISS(2) Then MAXerrMISS(2) = MISF2(i)
        If MISF2(i) <= MINerrMISS(2) Then MINerrMISS(2) = MISF2(i)
      End If
    Next i
    If NpSEMITOP >= 2 Then
      NpSEZIONE = NpSEMITOP: GoSub CALCOLO_COEFFICIENTE_ANGOLARE_b
      'CALCOLO ANGOLO TEORICO IN RADIANTI
      AngoloMisS(2) = Atn(B)
    Else
      AngoloMisS(2) = TT_F2(bgnSEMITOP + 1) / 180 * PG
    End If
    erroreS(2) = AngoloMisS(2) - AngoloTeoS(2)
    'CALCOLO ERRORE DI FORMA DEL FIANCO 2
    errFormaS(2) = 0
    For i = 1 To NpSEMITOP
      For j = 1 To NpSEMITOP
        errFormaS(0) = -(pntX(j) - pntX(i)) * Cos(AngoloTeoS(2)) + (pntY(j) - pntY(i)) * Sin(AngoloTeoS(2))
        errFormaS(0) = Abs(errFormaS(0))
        If errFormaS(0) > errFormaS(2) Then errFormaS(2) = errFormaS(0)
      Next j
    Next i
  End If
  '
  'SEGNALAZIONE UTENTE
  WRITE_DIALOG CHK0.Combo1.List(CHK0.Combo1.ListIndex)
  '
  'SALVATAGGIO CORREZIONI
  '
  'CORR. DI SPESSORE DENTE
  i = WritePrivateProfileString("CREATORI_STANDARD", "Correttore(1)", frmt(-GEN(16), 3), Path_LAVORAZIONE_INI)
  'CORR. ANGOLO F1
  i = WritePrivateProfileString("CREATORI_STANDARD", "Correttore(2)", frmt(-erroreF(1) * 180 / PG, 3), Path_LAVORAZIONE_INI)
  'CORR. ANGOLO F2
  i = WritePrivateProfileString("CREATORI_STANDARD", "Correttore(3)", frmt(-erroreF(2) * 180 / PG, 3), Path_LAVORAZIONE_INI)
  'CORR. ANGOLO SEMITOPPING F1
  i = WritePrivateProfileString("CREATORI_STANDARD", "Correttore(4)", frmt(-erroreS(1) * 180 / PG, 3), Path_LAVORAZIONE_INI)
  'CORR. ANGOLO SEMITOPPING F2
  i = WritePrivateProfileString("CREATORI_STANDARD", "Correttore(5)", frmt(-erroreS(2) * 180 / PG, 3), Path_LAVORAZIONE_INI)
  'CORR. ANGOLO RASTREMAZIONE TESTA F1
  i = WritePrivateProfileString("CREATORI_STANDARD", "Correttore(6)", frmt(-erroreT(1) * 180 / PG, 3), Path_LAVORAZIONE_INI)
  'CORR. ANGOLO RASTREMAZIONE PIEDE F1
  i = WritePrivateProfileString("CREATORI_STANDARD", "Correttore(7)", frmt(-erroreP(1) * 180 / PG, 3), Path_LAVORAZIONE_INI)
  'CORR. ANGOLO RASTREMAZIONE TESTA F2
  i = WritePrivateProfileString("CREATORI_STANDARD", "Correttore(8)", frmt(-erroreT(2) * 180 / PG, 3), Path_LAVORAZIONE_INI)
  'CORR. ANGOLO RASTREMAZIONE PIEDE F2
  i = WritePrivateProfileString("CREATORI_STANDARD", "Correttore(9)", frmt(-erroreP(2) * 180 / PG, 3), Path_LAVORAZIONE_INI)
  '
  i = WritePrivateProfileString("CREATORI_STANDARD", "Correttore(10)", "0", Path_LAVORAZIONE_INI)
  i = WritePrivateProfileString("CREATORI_STANDARD", "Correttore(11)", "0", Path_LAVORAZIONE_INI)
  i = WritePrivateProfileString("CREATORI_STANDARD", "Correttore(12)", "0", Path_LAVORAZIONE_INI)
  i = WritePrivateProfileString("CREATORI_STANDARD", "Correttore(13)", "0", Path_LAVORAZIONE_INI)
  i = WritePrivateProfileString("CREATORI_STANDARD", "Correttore(14)", "0", Path_LAVORAZIONE_INI)
  i = WritePrivateProfileString("CREATORI_STANDARD", "Correttore(15)", "0", Path_LAVORAZIONE_INI)
  '
  i = WritePrivateProfileString("CREATORI_STANDARD", "CorrezioniCalcolate", "Y", Path_LAVORAZIONE_INI)
  '
  'SCRITTURA MESSAGGI PER LA STAMPA
  i = WritePrivateProfileString("CHK0", "stmp(1)", "SPE meas: " & frmt(GEN(17), 3) & "mm", Path_LAVORAZIONE_INI)
  i = WritePrivateProfileString("CHK0", "stmp(2)", "SPE  err: " & frmt(GEN(16), 3) & "mm", Path_LAVORAZIONE_INI)
  i = WritePrivateProfileString("CHK0", "stmp(3)", "DEX meas: " & frmt(GEN(15), 3) & "mm", Path_LAVORAZIONE_INI)
  i = WritePrivateProfileString("CHK0", "stmp(4)", "fha F1: " & frmt(erroreF(1) * AltezzaF(1), 3) & "mm", Path_LAVORAZIONE_INI)
  i = WritePrivateProfileString("CHK0", "stmp(5)", "fha F2: " & frmt(erroreF(2) * AltezzaF(2), 3) & "mm", Path_LAVORAZIONE_INI)
  '
  'VISUALIZZAZIONE CALCOLI
  i = 1
  stmp = GetInfo("CHK0", "stmp(" & Format$(i, "0") & ")", Path_LAVORAZIONE_INI): stmp = Trim$(stmp)
  While Len(stmp) > 0
    '
    CHK0.Picture1.CurrentX = MLeft + PicW - CHK0.Picture1.TextWidth(stmp)
    CHK0.Picture1.CurrentY = MTop + CHK0.Picture1.TextHeight(stmp) * (i - 1)
    CHK0.Picture1.Print stmp
    '
    i = i + 1
    '
    stmp = GetInfo("CHK0", "stmp(" & Format$(i, "0") & ")", Path_LAVORAZIONE_INI)
    stmp = Trim$(stmp)
  Wend
  '
  'RIEMPIO LA CASELLA DI TESTO
  If CHK0.txtHELP.Visible = True Then
    '
    Dim NomeFileTXT As String
    '
    Select Case CHK0.Combo1.ListIndex
      '
      Case 0
        '
      Case 1
        NomeFileTXT = g_chOemPATH & "\PUNTI.TXT"
        Open NomeFileTXT For Output As #1
          Print #1, String(20, "-") & " F1 " & String(20, "-")
          'FIANCO 1
          Print #1, Left$("nr." & String(4, " "), 4);
          Print #1, Left$("XX " & String(8, " "), 8);
          Print #1, Left$("YY" & String(8, " "), 8);
          Print #1, Left$("TT" & String(8, " "), 8);
          Print #1, Left$("MISF1" & String(8, " "), 8)
          For i = 1 To Np
            If MISF1(i) <> 1000 Then
              Print #1, Left$(Format$(i, "###0") & String(4, " "), 4);
              Print #1, Left$(frmt(XX_F1(i), 4) & String(8, " "), 8);
              Print #1, Left$(frmt(YY_F1(i), 4) & String(8, " "), 8);
              Print #1, Left$(frmt(TT_F1(i), 4) & String(8, " "), 8);
              Print #1, Left$(frmt(MISF1(i), 3) & String(8, " "), 8)
            End If
          Next i
          Print #1, String(20, "-") & " F2 " & String(20, "-")
          'FIANCO 2
          Print #1, Left$("nr." & String(4, " "), 4);
          Print #1, Left$("XX " & String(8, " "), 8);
          Print #1, Left$("YY" & String(8, " "), 8);
          Print #1, Left$("TT" & String(8, " "), 8);
          Print #1, Left$("MISF2" & String(8, " "), 8)
          For i = 1 To Np
            If MISF2(i) <> 1000 Then
              Print #1, Left$(Format$(i, "###0") & String(4, " "), 4);
              Print #1, Left$(frmt(XX_F2(i), 4) & String(8, " "), 8);
              Print #1, Left$(frmt(YY_F2(i), 4) & String(8, " "), 8);
              Print #1, Left$(frmt(TT_F2(i), 4) & String(8, " "), 8);
              Print #1, Left$(frmt(MISF2(i), 3) & String(8, " "), 8)
            End If
          Next i
        Close #1
        Open NomeFileTXT For Input As #1
          CHK0.txtHELP.Text = Input$(LOF(1), 1)
        Close #1
        '
      Case 2
        NomeFileTXT = g_chOemPATH & "\ERRORI.TXT"
        Open NomeFileTXT For Output As #1
          'INTESTAZIONE
          Print #1, Left$("***  " & String(4, " "), 4);
          Print #1, Left$("theo " & String(8, " "), 8);
          Print #1, Left$("meas " & String(8, " "), 8);
          Print #1, Left$("err " & String(8, " "), 8)
          Print #1, String(56, "_")
          'SPESSORE
          Print #1, Left$("SPE" & String(4, " "), 4);
          Print #1, Left$(frmt(GEN(17) - GEN(16), 3) & String(8, " "), 8);
          Print #1, Left$(frmt(GEN(17), 3) & String(8, " "), 8);
          Print #1, Left$(frmt(GEN(16), 3) & String(8, " "), 8)
          'DIAMETRO ESTERNO
          Print #1, Left$("DEX" & String(4, " "), 4);
          Print #1, Left$(String(8, " "), 8);
          Print #1, Left$(frmt(GEN(15), 3) & String(8, " "), 8)
          Print #1, String(56, "_")
          'VALUTAZIONI SULLA RASTREMAZIONE DI TESTA
          If (endRAGGI > bgnRAGGI) Then
            Print #1, Left$("TIP RADIUS" & String(36, " "), 36);
            Print #1, Left$("bgn(" & Format$(bgnRAGGI, "#0") & ")" & String(8, " "), 8);
            Print #1, Left$("end(" & Format$(endRAGGI, "#0") & ")" & String(8, " "), 8)
            Print #1, "F1 " & pntListaR(1)
            Print #1, "F2 " & pntListaR(2)
            'INTESTAZIONE
            Print #1, Left$("err  " & String(4, " "), 4);
            Print #1, Left$("max  " & String(8, " "), 8);
            Print #1, Left$("min  " & String(8, " "), 8);
            Print #1, Left$("form " & String(8, " "), 8)
            Print #1, String(56, "_")
            'FIANCO 1 e 2
            For j = 1 To 2
              Print #1, Left$("F" & Format$(j, "0") & String(4, " "), 4);
              Print #1, Left$(frmt(MAXerrMISR(j), 3) & String(8, " "), 8);
              Print #1, Left$(frmt(MINerrMISR(j), 3) & String(8, " "), 8);
              Print #1, Left$(frmt(errFormaR(j), 3) & String(8, " "), 8)
            Next j
            Print #1, String(56, "_")
          End If
          'VALUTAZIONI SULLA PROTUBERANZA
          If (endPROTUB > bgnPROTUB) Then
            Print #1, Left$("PROTUBERANCE" & String(36, " "), 36);
            Print #1, Left$("bgn(" & Format$(bgnPROTUB, "#0") & ")" & String(8, " "), 8);
            Print #1, Left$("end(" & Format$(endPROTUB, "#0") & ")" & String(8, " "), 8)
            Print #1, "F1 " & pntListaB(1)
            Print #1, "F2 " & pntListaB(2)
            'INTESTAZIONE
            Print #1, Left$("err  " & String(4, " "), 4);
            Print #1, Left$("max  " & String(8, " "), 8);
            Print #1, Left$("min  " & String(8, " "), 8);
            Print #1, Left$("form " & String(8, " "), 8)
            Print #1, String(56, "_")
            'FIANCO 1 e 2
            For j = 1 To 2
              Print #1, Left$("F" & Format$(j, "0") & String(4, " "), 4);
              Print #1, Left$(frmt(MAXerrMISB(j), 3) & String(8, " "), 8);
              Print #1, Left$(frmt(MINerrMISB(j), 3) & String(8, " "), 8);
              Print #1, Left$(frmt(errFormaB(j), 3) & String(8, " "), 8)
            Next j
            Print #1, String(56, "_")
          End If
          'VALUTAZIONI SULLA RASTREMAZIONE DI TESTA
          If (endTESTA > bgnTESTA) Then
            Print #1, Left$("TIP TAPERING" & String(36, " "), 36);
            Print #1, Left$("bgn(" & Format$(bgnTESTA, "#0") & ")" & String(8, " "), 8);
            Print #1, Left$("end(" & Format$(endTESTA, "#0") & ")" & String(8, " "), 8)
            Print #1, "F1 " & pntListaT(1)
            Print #1, "F2 " & pntListaT(2)
            'INTESTAZIONE
            Print #1, Left$("ang" & String(4, " "), 4);
            Print #1, Left$("theo(�)" & String(8, " "), 8);
            Print #1, Left$("meas(�)" & String(8, " "), 8);
            Print #1, Left$("err(�) " & String(8, " "), 8);
            Print #1, Left$("err(mm)" & String(8, " "), 8);
            Print #1, Left$("H(mm)" & String(8, " "), 8)
            Print #1, String(56, "_")
            'FIANCO 1 e 2
            For j = 1 To 2
              Print #1, Left$("F" & Format$(j, "0") & String(4, " "), 4);
              Print #1, Left$(frmt(AngoloTeoT(j) * 180 / PG, 3) & String(8, " "), 8);
              Print #1, Left$(frmt(AngoloMisT(j) * 180 / PG, 3) & String(8, " "), 8);
              Print #1, Left$(frmt(erroreT(j) * 180 / PG, 3) & String(8, " "), 8);
              Print #1, Left$(frmt(erroreT(j) * AltezzaT(j), 3) & String(8, " "), 8);
              Print #1, Left$(frmt(AltezzaT(j), 3) & String(8, " "), 8)
            Next j
            Print #1, String(56, "_")
            'INTESTAZIONE
            Print #1, Left$("err  " & String(4, " "), 4);
            Print #1, Left$("max  " & String(8, " "), 8);
            Print #1, Left$("min  " & String(8, " "), 8);
            Print #1, Left$("form " & String(8, " "), 8)
            Print #1, String(56, "_")
            'FIANCO 1 e 2
            For j = 1 To 2
              Print #1, Left$("F" & Format$(j, "0") & String(4, " "), 4);
              Print #1, Left$(frmt(MAXerrMIST(j), 3) & String(8, " "), 8);
              Print #1, Left$(frmt(MINerrMIST(j), 3) & String(8, " "), 8);
              Print #1, Left$(frmt(errFormaT(j), 3) & String(8, " "), 8)
            Next j
            Print #1, String(56, "_")
          End If
          If (endFIANCO > bgnFIANCO) Then
            Print #1, Left$("FLANKS" & String(36, " "), 36);
            Print #1, Left$("bgn(" & Format$(bgnFIANCO, "#0") & ")" & String(8, " "), 8);
            Print #1, Left$("end(" & Format$(endFIANCO, "#0") & ")" & String(8, " "), 8)
            Print #1, "F1 " & pntListaF(1)
            Print #1, "F2 " & pntListaF(2)
            'INTESTAZIONE
            Print #1, Left$("ang" & String(4, " "), 4);
            Print #1, Left$("theo(�)" & String(8, " "), 8);
            Print #1, Left$("meas(�)" & String(8, " "), 8);
            Print #1, Left$("err(�) " & String(8, " "), 8);
            Print #1, Left$("err(mm)" & String(8, " "), 8);
            Print #1, Left$("H(mm)" & String(8, " "), 8)
            Print #1, String(56, "_")
            'FIANCO 1 e 2
            For j = 1 To 2
              Print #1, Left$("F" & Format$(j, "0") & String(4, " "), 4);
              Print #1, Left$(frmt(AngoloTeoF(j) * 180 / PG, 3) & String(8, " "), 8);
              Print #1, Left$(frmt(AngoloMisF(j) * 180 / PG, 3) & String(8, " "), 8);
              Print #1, Left$(frmt(erroreF(j) * 180 / PG, 3) & String(8, " "), 8);
              Print #1, Left$(frmt(erroreF(j) * AltezzaF(j), 3) & String(8, " "), 8);
              Print #1, Left$(frmt(AltezzaF(j), 3) & String(8, " "), 8)
            Next j
            Print #1, String(56, "_")
            'INTESTAZIONE
            Print #1, Left$("err    " & String(4, " "), 4);
            Print #1, Left$("max    " & String(8, " "), 8);
            Print #1, Left$("min    " & String(8, " "), 8);
            Print #1, Left$("form   " & String(8, " "), 8)
            Print #1, String(56, "_")
            'FIANCO 1 e 2
            For j = 1 To 2
              Print #1, Left$("F" & Format$(j, "0") & String(4, " "), 4);
              Print #1, Left$(frmt(MAXerrMISF(j), 3) & String(8, " "), 8);
              Print #1, Left$(frmt(MINerrMISF(j), 3) & String(8, " "), 8);
              Print #1, Left$(frmt(errFormaF(j), 3) & String(8, " "), 8)
            Next j
            Print #1, String(56, "_")
          End If
          'VALUTAZIONI SULLA RASTREMAZIONE DI PIEDE
          If (endPIEDE > bgnPIEDE) Then
            Print #1, Left$("ROOT TAPERING" & String(36, " "), 36);
            Print #1, Left$("bgn(" & Format$(bgnPIEDE, "#0") & ")" & String(8, " "), 8);
            Print #1, Left$("end(" & Format$(endPIEDE, "#0") & ")" & String(8, " "), 8)
            Print #1, "F1 " & pntListaP(1)
            Print #1, "F2 " & pntListaP(2)
            'INTESTAZIONE
            Print #1, Left$("ang" & String(4, " "), 4);
            Print #1, Left$("theo(�)" & String(8, " "), 8);
            Print #1, Left$("meas(�)" & String(8, " "), 8);
            Print #1, Left$("err(�) " & String(8, " "), 8);
            Print #1, Left$("err(mm)" & String(8, " "), 8);
            Print #1, Left$("H(mm)" & String(8, " "), 8)
            Print #1, String(56, "_")
            'FIANCO 1 e 2
            For j = 1 To 2
              Print #1, Left$("F" & Format$(j, "0") & String(4, " "), 4);
              Print #1, Left$(frmt(AngoloTeoP(j) * 180 / PG, 3) & String(8, " "), 8);
              Print #1, Left$(frmt(AngoloMisP(j) * 180 / PG, 3) & String(8, " "), 8);
              Print #1, Left$(frmt(erroreP(j) * 180 / PG, 3) & String(8, " "), 8);
              Print #1, Left$(frmt(erroreP(j) * AltezzaP(j), 3) & String(8, " "), 8);
              Print #1, Left$(frmt(AltezzaP(j), 3) & String(8, " "), 8)
            Next j
            Print #1, String(56, "_")
            'INTESTAZIONE
            Print #1, Left$("err  " & String(4, " "), 4);
            Print #1, Left$("max  " & String(8, " "), 8);
            Print #1, Left$("min  " & String(8, " "), 8);
            Print #1, Left$("form " & String(8, " "), 8)
            Print #1, String(56, "_")
            'FIANCO 1 e 2
            For j = 1 To 2
              Print #1, Left$("F" & Format$(j, "0") & String(4, " "), 4);
              Print #1, Left$(frmt(MAXerrMISP(j), 3) & String(8, " "), 8);
              Print #1, Left$(frmt(MINerrMISP(j), 3) & String(8, " "), 8);
              Print #1, Left$(frmt(errFormaP(j), 3) & String(8, " "), 8)
            Next j
            Print #1, String(56, "_")
          End If
          'VALUTAZIONI SUL SEMITOPPING
          If (endSEMITOP > bgnSEMITOP) Then
            Print #1, Left$("SEMITOPPING" & String(36, " "), 36);
            Print #1, Left$("bgn(" & Format$(bgnSEMITOP, "#0") & ")" & String(8, " "), 8);
            Print #1, Left$("end(" & Format$(endSEMITOP, "#0") & ")" & String(8, " "), 8)
            Print #1, "F1 " & pntListaS(1)
            Print #1, "F2 " & pntListaS(2)
            'INTESTAZIONE
            Print #1, Left$("ang" & String(4, " "), 4);
            Print #1, Left$("theo(�)" & String(8, " "), 8);
            Print #1, Left$("meas(�)" & String(8, " "), 8);
            Print #1, Left$("err(�) " & String(8, " "), 8);
            Print #1, Left$("err(mm)" & String(8, " "), 8);
            Print #1, Left$("H(mm)" & String(8, " "), 8)
            Print #1, String(56, "_")
            'FIANCO 1 e 2
            For j = 1 To 2
              Print #1, Left$("F" & Format$(j, "0") & String(4, " "), 4);
              Print #1, Left$(frmt(AngoloTeoS(j) * 180 / PG, 3) & String(8, " "), 8);
              Print #1, Left$(frmt(AngoloMisS(j) * 180 / PG, 3) & String(8, " "), 8);
              Print #1, Left$(frmt(erroreS(j) * 180 / PG, 3) & String(8, " "), 8);
              Print #1, Left$(frmt(erroreS(j) * AltezzaS(j), 3) & String(8, " "), 8);
              Print #1, Left$(frmt(AltezzaS(j), 3) & String(8, " "), 8)
            Next j
            Print #1, String(56, "_")
            'INTESTAZIONE
            Print #1, Left$("err  " & String(4, " "), 4);
            Print #1, Left$("max  " & String(8, " "), 8);
            Print #1, Left$("min  " & String(8, " "), 8);
            Print #1, Left$("form " & String(8, " "), 8)
            Print #1, String(56, "_")
            'FIANCO 1 e 2
            For j = 1 To 2
              Print #1, Left$("F" & Format$(j, "0") & String(4, " "), 4);
              Print #1, Left$(frmt(MAXerrMISS(j), 3) & String(8, " "), 8);
              Print #1, Left$(frmt(MINerrMISS(j), 3) & String(8, " "), 8);
              Print #1, Left$(frmt(errFormaS(j), 3) & String(8, " "), 8)
            Next j
            Print #1, String(56, "_")
          End If
        Close #1
        '
        Open NomeFileTXT For Input As #1
          CHK0.txtHELP.Text = Input$(LOF(1), 1)
        Close #1
        '
    End Select
    '
  End If
  '
  Exit Sub
  '
errCONTROLLO_CREATORI_VISUALIZZAZIONE:
  WRITE_DIALOG "SUB: CONTROLLO_CREATORI_VISUALIZZAZIONE " & str(Err)
  Exit Sub

CALCOLO_COEFFICIENTE_ANGOLARE_b:
'
Dim sXh   As Double
Dim sYh   As Double
Dim sXhXh As Double
Dim sXhYh As Double
  '
  Select Case NpSEZIONE
    '
    Case 0
      StopRegieEvents
      MsgBox "ERROR: ANGLE CANNOT BE EVALUATED WITHOUT A POINT.", 16, "SUB: CONTROLLO_CREATORI_VISUALIZZAZIONE"
      ResumeRegieEvents
      B = 0
      '
    Case 1
      B = 0
      '
    Case 2
      B = (pntX(2) - pntX(1)) / (pntY(2) - pntY(1))
      '
    Case Else
      '--------------------------------------------------------------------------
      'INPUT : NpFIANCO, pntX(.), pntY(.)
      'OUTPUT: b
      '--------------------------------------------------------------------------
      sXh = 0:   For i = 1 To NpSEZIONE: sXh = sXh + pntY(i):               Next i
      sYh = 0:   For i = 1 To NpSEZIONE: sYh = sYh + pntX(i):               Next i
      sXhXh = 0: For i = 1 To NpSEZIONE: sXhXh = sXhXh + pntY(i) * pntY(i): Next i
      sXhYh = 0: For i = 1 To NpSEZIONE: sXhYh = sXhYh + pntY(i) * pntX(i): Next i
      'Coefficiente angolare della retta di regressione
      B = (NpSEZIONE * sXhYh - sXh * sYh) / (NpSEZIONE * sXhXh - sXh * sXh)
      '--------------------------------------------------------------------------
      '
  End Select
  '
  Return

End Sub

Sub CONTROLLO_CREATORI_STAMPA(ByVal iXX As Integer, ByVal iYY As Integer)
'
Dim riga        As String
Dim stmp        As String
Dim i           As Integer
Dim j           As Integer
Dim k           As Integer
Dim jj          As Integer
Dim jjbgnFIANCO As Integer
Dim jjendFIANCO As Integer
'
Dim GEN(20)       As Double
Dim NomeControllo As String
Dim XX_F1()       As Double
Dim YY_F1()       As Double
Dim TT_F1()       As Double
Dim MISF1()       As Double
Dim XX_F2()       As Double
Dim YY_F2()       As Double
Dim TT_F2()       As Double
Dim MISF2()       As Double
'
Dim Np As Integer
'
Dim PicW  As Single
Dim PicH1 As Single
Dim PicH2 As Single
Dim MLeft As Single
Dim MTop  As Single
'
Dim pX1 As Single
Dim pX2 As Single
Dim pY1 As Single
Dim pY2 As Single
'
Dim ScalaPROFILO As Single
Dim ScalaERRORI  As Single
'
Dim OX As Single
Dim OY As Single
'
Dim DL As Single
Dim k1 As Integer
Dim k2 As Integer
'
Dim MISF1max As Double
Dim MISF1min As Double
Dim MISF2max As Double
Dim MISF2min As Double
'
Dim TextFont As String
'
Dim bgnRAGGI   As Integer
Dim endRAGGI   As Integer
Dim bgnPROTUB  As Integer
Dim endPROTUB  As Integer
Dim bgnTESTA   As Integer
Dim endTESTA   As Integer
Dim bgnFIANCO  As Integer
Dim endFIANCO  As Integer
Dim bgnPIEDE   As Integer
Dim endPIEDE   As Integer
Dim bgnSEMITOP As Integer
Dim endSEMITOP As Integer
'
Dim iCl         As Single
Dim iRG         As Single
Dim nRgMAX      As Integer
Dim NomeFileTXT As String
'
On Error GoTo errCONTROLLO_CREATORI_STAMPA
'
'SEGNALAZIONE UTENTE
WRITE_DIALOG CHK0.Text2.Text & " --> " & Printer.DeviceName
Select Case CHK0.Combo1.ListIndex
  '
  Case 0 'STAMPA DEL CERTIFICATO DI CONTROLLO
  'CARATTERI DI STAMPA
  TextFont = GetInfo("StampaParametri", "TextFont", PathFILEINI)
  If (LINGUA = "CH") Then TextFont = "MS Song"
  If (LINGUA = "RU") Then TextFont = "Arial Cyr"
  'IMPOSTAZIONE STAMPANTE
  Printer.ScaleMode = 7
  Printer.DrawWidth = 1
  Printer.Orientation = 2
  'IMPOSTAZIONE DEI CARATTERI
  For i = 1 To 2
    Printer.FontName = TextFont
    Printer.FontBold = False
    Printer.FontSize = 12
  Next i
  'LETTURA DEL NOME DEL FILE DI MISURA
  NomeControllo = GetInfo("CHK0", "FileMisura", Path_LAVORAZIONE_INI)
  GoSub LETTURA_DATI_DAL_FILE_NomeControllo
  'VISUALIZZAZIONE DEL RISULTATO OTTENUTO
  'LETTURA DEI VALORI DI SCALA
  CHK0.txtScX.Text = GetInfo("CHK0", "ScalaProfiloCreatori", Path_LAVORAZIONE_INI)
  CHK0.txtScY.Text = GetInfo("CHK0", "ScalaErroreCreatori", Path_LAVORAZIONE_INI)
  CHK0.lblScX.Visible = True
  CHK0.lblScY.Visible = True
  CHK0.txtScX.Visible = True
  CHK0.txtScY.Visible = True
  'MARGINI
  Dim MRight As Double
  MLeft = val(GetInfo("CHK0", "MLeft", Path_LAVORAZIONE_INI))
  MTop = val(GetInfo("CHK0", "MTop", Path_LAVORAZIONE_INI))
  MRight = MLeft
  'DEFINIZIONE AREA DI STAMPA
  PicW = Printer.ScaleWidth - (MLeft + MRight)
  PicH1 = PicW / 2
  PicH2 = 3.5
'-----------------------------------------
  'RIQUADRI PER VISUALIZZAZIONI E STAMPE
  'Printer.Line (MLeft, MTop)-(PicW + MLeft, PicH1 + MTop), , B
  'Printer.Line (MLeft, MTop)-(PicW / 2 + MLeft, PicH1 + MTop), , B
  'Printer.Line (MLeft, MTop + PicH1)-(PicW + MLeft, PicH1 + MTop + PicH2), , B
  Dim ANGOLO As Single
  Dim NumeroRette As Integer
  Dim ftmp1 As Double
  ANGOLO = 30
  NumeroRette = 30
  'ASSE XX
  pX1 = MLeft + PicW
  pY1 = MTop + PicH1
  Printer.Line (pX1 - PicW, pY1)-(pX1, pY1)
  'FRECCIA XX
  ftmp1 = (ANGOLO / 180 * PG) / NumeroRette
  For i = -NumeroRette To NumeroRette
    pX2 = pX1 - 0.3 * Cos((ANGOLO / 180 * PG))
    pY2 = pY1 + 0.3 * Sin(i * ftmp1)
    Printer.Line (pX1, pY1)-(pX2, pY2)
  Next i
  'NOME DELL'ASSE XX
  stmp = "XX"
  Printer.CurrentX = pX1 - Printer.TextWidth(stmp)
  Printer.CurrentY = pY1 - 0.3 - Printer.TextHeight(stmp)
  Printer.Print stmp
  'ASSE YY
  pX1 = MLeft + PicW / 2
  pY1 = MTop
  Printer.Line (pX1, pY1)-(pX1, pY1 + PicH1)
  'FRECCIA YY
  ftmp1 = (ANGOLO / 180 * PG) / NumeroRette
  For i = -NumeroRette To NumeroRette
    pX2 = pX1 + 0.3 * Sin(i * ftmp1)
    pY2 = pY1 + 0.3 * Cos((ANGOLO / 180 * PG))
    Printer.Line (pX1, pY1)-(pX2, pY2)
  Next i
  'NOME DELL'ASSE YY
  stmp = "YY"
  Printer.CurrentX = pX1 + 0.3
  Printer.CurrentY = pY1
  Printer.Print stmp
  'LETTURA VALORI DELLE SCALE DALLE CASELLE DI TESTO
  ScalaPROFILO = val(CHK0.txtScX.Text)
  ScalaERRORI = val(CHK0.txtScY.Text)
  'SCALA PROFILO
  pX1 = MLeft + 1
  pY1 = MTop + 2
  stmp = frmt(ScalaPROFILO, 4) & ":1"
  Printer.CurrentX = pX1 - Printer.TextWidth(stmp) / 2
  Printer.CurrentY = pY1 - 0.5 - 2 * Printer.TextHeight(stmp)
  Printer.Print stmp
  Printer.Line (pX1 - 0.5, pY1 - 0.5)-(pX1 + 0.5, pY1 - 0.5), QBColor(9)
  'FRECCIA SUPERIORE
  ftmp1 = (ANGOLO / 180 * PG) / NumeroRette
  For i = -NumeroRette To NumeroRette
    pX2 = pX1 + 0.3 * Sin(i * ftmp1)
    pY2 = pY1 - 0.5 - 0.3 * Cos((ANGOLO / 180 * PG))
    Printer.Line (pX1, pY1 - 0.5)-(pX2, pY2), QBColor(9)
  Next i
  'FRECCIA INFERIORE
  ftmp1 = (ANGOLO / 180 * PG) / NumeroRette
  For i = -NumeroRette To NumeroRette
    pX2 = pX1 + 0.3 * Sin(i * ftmp1)
    pY2 = pY1 + 0.5 + 0.3 * Cos((ANGOLO / 180 * PG))
    Printer.Line (pX1, pY1 + 0.5)-(pX2, pY2), QBColor(9)
  Next i
  Printer.Line (pX1 - 0.5, pY1 + 0.5)-(pX1 + 0.5, pY1 + 0.5), QBColor(9)
  stmp = frmt(10 / ScalaPROFILO, 4) & "mm"
  Printer.CurrentX = pX1 - Printer.TextWidth(stmp) / 2
  If Printer.CurrentX < MLeft Then Printer.CurrentX = MLeft  'FlagSTD006
  Printer.CurrentY = pY1 - Printer.TextHeight(stmp) / 2
  Printer.Print stmp
  'ORIGINE DEL RIFERIMENTO
  Dim xFIANCO As Integer
  Dim yFIANCO As Integer
  Dim iXXtmp  As Integer
  Dim iYYtmp  As Integer
  'INDICE XX PER IL RIFERIMENTO OX
    If (iXX >= Np) Then
      iXXtmp = Np: iXX = Np: xFIANCO = 1
  ElseIf (iXX >= 1) And (iXX < Np) Then
      iXXtmp = iXX: xFIANCO = 1
  ElseIf (iXX = 0) Then
      iXXtmp = 1: iXX = 1: xFIANCO = 1
  ElseIf (iXX > -Np) And (iXX <= -1) Then
      iXXtmp = Abs(iXX): xFIANCO = 2
  Else
      iXXtmp = Np: iXX = -Np: xFIANCO = 2
  End If
  'INDICE YY PER IL RIFERIMENTO OY
    If (iYY >= Np) Then
      iYYtmp = Np: iYY = Np: yFIANCO = 1
  ElseIf (iYY >= 1) And (iYY < Np) Then
      iYYtmp = iYY: yFIANCO = 1
  ElseIf (iYY = 0) Then
      iYYtmp = Np: iYY = Np: yFIANCO = 1
  ElseIf (iYY > -Np) And (iYY <= -1) Then
      iYYtmp = Abs(iYY): yFIANCO = 2
  Else
      iYYtmp = Np: iYY = -Np: yFIANCO = 2
  End If
  If xFIANCO = 1 Then
    OX = XX_F1(iXXtmp)
  Else
    OX = XX_F2(iXXtmp)
  End If
  If yFIANCO = 1 Then
    OY = YY_F1(iYYtmp)
  Else
    OY = YY_F2(iYYtmp)
  End If
  'VISUALIZZO OX E OY
  stmp = "(" & frmt(OX, 4) & ", " & frmt(OY, 4) & ")"
  Printer.CurrentX = MLeft + PicW / 2 - Printer.TextWidth(stmp) / 2
  Printer.CurrentY = MTop + PicH1
  Printer.Print stmp
  'AGGIORNO LE CASELLE
  CHK0.Text1(0).Text = Format$(iXX, "##0")
  CHK0.Text1(1).Text = Format$(iYY, "##0")
  'RISOLUZIONE CROCI
  DL = 0.1
  'PROFILO DEL FIANCO 1
  For i = 1 To Np - 1
    pX1 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F1(i) - OX) / 10
    pY1 = MTop + PicH1 - ScalaPROFILO * (YY_F1(i) - OY) / 10
    pX2 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F1(i + 1) - OX) / 10
    pY2 = MTop + PicH1 - ScalaPROFILO * (YY_F1(i + 1) - OY) / 10
    Printer.Line (pX1, pY1)-(pX2, pY2)
  Next i
  'PUNTI DEL FIANCO 1
  For i = 1 To Np
    pX1 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F1(i) - DL / 2 * Sin(TT_F1(i) * PG / 180) - OX) / 10
    pY1 = MTop + PicH1 - ScalaPROFILO * (YY_F1(i) + DL / 2 * Cos(TT_F1(i) * PG / 180) - OY) / 10
    pX2 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F1(i) + DL / 2 * Sin(TT_F1(i) * PG / 180) - OX) / 10
    pY2 = MTop + PicH1 - ScalaPROFILO * (YY_F1(i) - DL / 2 * Cos(TT_F1(i) * PG / 180) - OY) / 10
    Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
    pX1 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F1(i) - DL / 2 * Cos(TT_F1(i) * PG / 180) - OX) / 10
    pY1 = MTop + PicH1 - ScalaPROFILO * (YY_F1(i) - DL / 2 * Sin(TT_F1(i) * PG / 180) - OY) / 10
    pX2 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F1(i) + DL / 2 * Cos(TT_F1(i) * PG / 180) - OX) / 10
    pY2 = MTop + PicH1 - ScalaPROFILO * (YY_F1(i) + DL / 2 * Sin(TT_F1(i) * PG / 180) - OY) / 10
    Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(9)
  Next i
  'ERRORI SUL FIANCO 1
  MISF1max = 0
  MISF1min = 0
  For i = 1 To Np
    If (MISF1(i) < 1000) Then
      pX1 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F1(i) + ScalaERRORI * MISF1(i) * Cos(TT_F1(i) * PG / 180) / ScalaPROFILO - OX) / 10
      pY1 = MTop + PicH1 - ScalaPROFILO * (YY_F1(i) + ScalaERRORI * MISF1(i) * Sin(TT_F1(i) * PG / 180) / ScalaPROFILO - OY) / 10
      pX2 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F1(i) - OX) / 10
      pY2 = MTop + PicH1 - ScalaPROFILO * (YY_F1(i) - OY) / 10
      Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
      If MISF1(i) >= MISF1max Then MISF1max = MISF1(i)
      If MISF1(i) <= MISF1min Then MISF1min = MISF1(i)
    End If
  Next i
  'CURVA DEGLI ERRORI SUL FIANCO 1
  If Np >= 2 Then
    k1 = 1
    Do
      'INDICE INIZIALE TRATTO
      While (MISF1(k1) = 1000) And (k1 < Np)
        k1 = k1 + 1
      Wend
      'INDICE FINALE TRATTO
      k2 = k1 + 1
      While (MISF1(k2) = 1000) And (k2 < Np)
        k2 = k2 + 1
      Wend
      'DISEGNO IL TRATTO
      If (k2 > k1) And (MISF1(k1) <> 1000) And (MISF1(k2) <> 1000) Then
        pX1 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F1(k1) + ScalaERRORI * MISF1(k1) * Cos(TT_F1(k1) * PG / 180) / ScalaPROFILO - OX) / 10
        pY1 = MTop + PicH1 - ScalaPROFILO * (YY_F1(k1) + ScalaERRORI * MISF1(k1) * Sin(TT_F1(k1) * PG / 180) / ScalaPROFILO - OY) / 10
        pX2 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F1(k2) + ScalaERRORI * MISF1(k2) * Cos(TT_F1(k2) * PG / 180) / ScalaPROFILO - OX) / 10
        pY2 = MTop + PicH1 - ScalaPROFILO * (YY_F1(k2) + ScalaERRORI * MISF1(k2) * Sin(TT_F1(k2) * PG / 180) / ScalaPROFILO - OY) / 10
        Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
      End If
      'AGGIORNO PER IL PROSSIMO TRATTO
      k1 = k2
    Loop While (k2 < Np)
  End If
  'NOME DEL FIANCO 1
  i = Np
  stmp = " F1 "
  Printer.CurrentX = MLeft + PicW / 2 + ScalaPROFILO * (XX_F1(i) - OX) / 10
  Printer.CurrentY = MTop + PicH1 - ScalaPROFILO * (YY_F1(i) - OY) / 10
  Printer.Print stmp
  'PROFILO DEL FIANCO 2
  For i = 1 To Np - 1
    pX1 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F2(i) - OX) / 10
    pY1 = MTop + PicH1 - ScalaPROFILO * (YY_F2(i) - OY) / 10
    pX2 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F2(i + 1) - OX) / 10
    pY2 = MTop + PicH1 - ScalaPROFILO * (YY_F2(i + 1) - OY) / 10
    Printer.Line (pX1, pY1)-(pX2, pY2)
  Next i
  'PUNTI DEL FIANCO 2
  For i = 1 To Np
    pX1 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F2(i) + DL / 2 * Sin(TT_F2(i) * PG / 180) - OX) / 10
    pY1 = MTop + PicH1 - ScalaPROFILO * (YY_F2(i) + DL / 2 * Cos(TT_F2(i) * PG / 180) - OY) / 10
    pX2 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F2(i) - DL / 2 * Sin(TT_F2(i) * PG / 180) - OX) / 10
    pY2 = MTop + PicH1 - ScalaPROFILO * (YY_F2(i) - DL / 2 * Cos(TT_F2(i) * PG / 180) - OY) / 10
    Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
    pX1 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F2(i) - DL / 2 * Cos(TT_F2(i) * PG / 180) - OX) / 10
    pY1 = MTop + PicH1 - ScalaPROFILO * (YY_F2(i) + DL / 2 * Sin(TT_F2(i) * PG / 180) - OY) / 10
    pX2 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F2(i) + DL / 2 * Cos(TT_F2(i) * PG / 180) - OX) / 10
    pY2 = MTop + PicH1 - ScalaPROFILO * (YY_F2(i) - DL / 2 * Sin(TT_F2(i) * PG / 180) - OY) / 10
    Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(9)
  Next i
  'ERRORI SUL FIANCO 2
  MISF2max = 0
  MISF2min = 0
  For i = 1 To Np
    If (MISF2(i) < 1000) Then
      pX1 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F2(i) - ScalaERRORI * MISF2(i) * Cos(TT_F2(i) * PG / 180) / ScalaPROFILO - OX) / 10
      pY1 = MTop + PicH1 - ScalaPROFILO * (YY_F2(i) + ScalaERRORI * MISF2(i) * Sin(TT_F2(i) * PG / 180) / ScalaPROFILO - OY) / 10
      pX2 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F2(i) - OX) / 10
      pY2 = MTop + PicH1 - ScalaPROFILO * (YY_F2(i) - OY) / 10
      Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
      If MISF2(i) >= MISF2max Then MISF2max = MISF2(i)
      If MISF2(i) <= MISF2min Then MISF2min = MISF2(i)
    End If
  Next i
  'CURVA DEGLI ERRORI SUL FIANCO 2
  If Np >= 2 Then
    k1 = 1
    Do
      'INDICE INIZIALE TRATTO
      While (MISF2(k1) = 1000) And (k1 < Np)
        k1 = k1 + 1
      Wend
      'INDICE FINALE TRATTO
      k2 = k1 + 1
      While (MISF2(k2) = 1000) And (k2 < Np)
        k2 = k2 + 1
      Wend
      'DISEGNO IL TRATTO
      If (k2 > k1) And (MISF2(k1) <> 1000) And (MISF2(k2) <> 1000) Then
        pX1 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F2(k1) - ScalaERRORI * MISF2(k1) * Cos(TT_F2(k1) * PG / 180) / ScalaPROFILO - OX) / 10
        pY1 = MTop + PicH1 - ScalaPROFILO * (YY_F2(k1) + ScalaERRORI * MISF2(k1) * Sin(TT_F2(k1) * PG / 180) / ScalaPROFILO - OY) / 10
        pX2 = MLeft + PicW / 2 + ScalaPROFILO * (XX_F2(k2) - ScalaERRORI * MISF2(k2) * Cos(TT_F2(k2) * PG / 180) / ScalaPROFILO - OX) / 10
        pY2 = MTop + PicH1 - ScalaPROFILO * (YY_F2(k2) + ScalaERRORI * MISF2(k2) * Sin(TT_F2(k2) * PG / 180) / ScalaPROFILO - OY) / 10
        Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
      End If
      'AGGIORNO PER IL PROSSIMO TRATTO
      k1 = k2
    Loop While (k2 < Np)
  End If
  'NOME DEL FIANCO 2
  i = Np
  stmp = " F2 "
  Printer.CurrentX = MLeft + PicW / 2 + ScalaPROFILO * (XX_F2(i) - OX) / 10 - Printer.TextWidth(stmp)
  Printer.CurrentY = MTop + PicH1 - ScalaPROFILO * (YY_F2(i) - OY) / 10
  Printer.Print stmp
  'IMMAGINE BITMAP
  Printer.PaintPicture CHK0.PicLogo.Picture, MLeft, MTop + PicH1 + PicH2, 1, 1
  'INTESTAZIONE
  stmp = CHK0.INTESTAZIONE.Caption & ": " & NomeControllo
  Printer.CurrentX = MLeft + 1
  Printer.CurrentY = MTop + PicH1 + PicH2
  Printer.Print stmp
  'DATA
  stmp = "DATE: " & FileDateTime(PathMIS & "\" & NomeControllo)
  Printer.CurrentX = MLeft + PicW / 2
  Printer.CurrentY = MTop + PicH1 + PicH2
  Printer.Print stmp
  'STAMPA CALCOLI DALLA VISUALIZZAZIONE
  i = 1
  stmp = GetInfo("CHK0", "stmp(" & Format$(i, "0") & ")", Path_LAVORAZIONE_INI)
  stmp = Trim$(stmp)
  While Len(stmp) > 0
    '
    Printer.CurrentX = MLeft + PicW - Printer.TextWidth(stmp)
    Printer.CurrentY = MTop + Printer.TextHeight(stmp) * (i - 1)
    Printer.Print stmp
    '
    i = i + 1
    '
    stmp = GetInfo("CHK0", "stmp(" & Format$(i, "0") & ")", Path_LAVORAZIONE_INI)
    stmp = Trim$(stmp)
  Wend
  '
  Printer.EndDoc

  Case 1
  'CARATTERI DI STAMPA
  TextFont = GetInfo("StampaParametri", "TextFont", PathFILEINI)
  If (LINGUA = "CH") Then TextFont = "MS Song"
  If (LINGUA = "RU") Then TextFont = "Arial Cyr"
  'IMPOSTAZIONE STAMPANTE
  Printer.ScaleMode = 7
  Printer.DrawWidth = 1
  Printer.Orientation = 1
  'IMPOSTAZIONE DEI CARATTERI
  For i = 1 To 2
    Printer.FontName = TextFont
    Printer.FontBold = False
    Printer.FontSize = 8
  Next i
  'LETTURA DEL NOME DEL FILE DI MISURA
  NomeControllo = GetInfo("CHK0", "FileMisura", Path_LAVORAZIONE_INI)
  GoSub LETTURA_DATI_DAL_FILE_NomeControllo
  'FOGLIO DIMENSIONATO PER CM
  PicW = 19
  PicH1 = 0 'PicW / 2
  PicH2 = 3.5
  'MARGINI
  MLeft = val(GetInfo("CHK0", "MLeft", Path_LAVORAZIONE_INI))
  MTop = val(GetInfo("CHK0", "MTop", Path_LAVORAZIONE_INI))
  'IMMAGINE BITMAP
  Printer.PaintPicture CHK0.PicLogo.Picture, MLeft, MTop + PicH1, 2, 2
  'INTESTAZIONE
  stmp = CHK0.INTESTAZIONE.Caption & ": " & NomeControllo
  Printer.CurrentX = MLeft + 2
  Printer.CurrentY = MTop + PicH1
  Printer.Print stmp
  'LINEA DI RIFERIMENTO
  Printer.DrawWidth = 2
  pX1 = MLeft
  pY1 = MTop + PicH1 + PicH2
  pX2 = MLeft + PicW
  pY2 = MTop + PicH1 + PicH2
  Printer.Line (pX1, pY1)-(pX2, pY2)
  Printer.DrawWidth = 1
  'LETTURA VALORI DELLE SCALE DALLE CASELLE DI TESTO
  ScalaERRORI = val(CHK0.txtScY.Text)
  'ERRORI SUL FIANCO 1
  MISF1max = 0
  MISF1min = 0
  For i = 1 To Np
    If (MISF1(i) < 1000) Then
      If MISF1(i) >= MISF1max Then MISF1max = MISF1(i)
      If MISF1(i) <= MISF1min Then MISF1min = MISF1(i)
    End If
  Next i
  'ERRORI SUL FIANCO 2
  MISF2max = 0
  MISF2min = 0
  For i = 1 To Np
    If (MISF2(i) < 1000) Then
      If MISF2(i) >= MISF2max Then MISF2max = MISF2(i)
      If MISF2(i) <= MISF2min Then MISF2min = MISF2(i)
    End If
  Next i
    'STAMPA DEI VALORI
    Dim hRIGA As Single
    Dim ERRmax As Double
    'MASSIMO ERRORE XX IN VALORE ASSOLUTO
    ERRmax = Abs(MISF1max)
    If Abs(MISF1min) > ERRmax Then ERRmax = Abs(MISF1min)
    If Abs(MISF2max) > ERRmax Then ERRmax = Abs(MISF2max)
    If Abs(MISF2min) > ERRmax Then ERRmax = Abs(MISF2min)
    If ERRmax = 0 Then ERRmax = 1
    'FIANCO 1
    'COLONNA DEGLI INDICI
    stmp = "nr."
    Printer.CurrentX = MLeft + PicW / 2
    Printer.CurrentY = MTop + PicH1 + PicH2
    Printer.Print stmp
    'COLONNA XX
    stmp = "XX"
    Printer.CurrentX = MLeft + PicW / 2 + 1
    Printer.CurrentY = MTop + PicH1 + PicH2
    Printer.Print stmp
    'COLONNA YY
    stmp = "YY"
    Printer.CurrentX = MLeft + PicW / 2 + 1 + 1.5
    Printer.CurrentY = MTop + PicH1 + PicH2
    Printer.Print stmp
    'COLONNA TT
    stmp = "TT"
    Printer.CurrentX = MLeft + PicW / 2 + 1 + 1.5 + 1.5
    Printer.CurrentY = MTop + PicH1 + PicH2
    Printer.Print stmp
    'COLONNA MISF1
    stmp = "MISF1"
    Printer.CurrentX = MLeft + PicW / 2 + 1 + 1.5 + 1.5 + 1.5
    Printer.CurrentY = MTop + PicH1 + PicH2
    Printer.Print stmp
    'COLONNA SCALA PER ERRORE DI FORMA
    stmp = frmt(ScalaERRORI, 4) & ":1"
    Printer.CurrentX = MLeft + PicW / 2 + 1 + 1.5 + 1.5 + 1.5 + 1.5 + 1 - Printer.TextWidth(stmp) / 2
    Printer.CurrentY = MTop + PicH1 + PicH2
    Printer.Print stmp
    'ALTEZZA DELLA RIGA
    hRIGA = Printer.TextHeight("X")
    jj = 1  'INDICE DELLA RIGA DA STAMPARE
    jjbgnFIANCO = 1 'INDICE DELLA RIGA DI INIZIO FIANCO
    jjendFIANCO = 1 'INDICE DELLA RIGA DI FINE   FIANCO
    For i = 1 To Np
      'STAMPA DEI SOLI PUNTI CONTROLLATI
      If MISF1(i) <> 1000 Then
        'INDICE
        stmp = Format$(i, "##0")
        Printer.CurrentX = MLeft + PicW / 2
        Printer.CurrentY = MTop + PicH1 + PicH2 + jj * hRIGA
        Printer.Print stmp
        'COLONNA XX
        stmp = frmt(XX_F1(i), 4)
        Printer.CurrentX = MLeft + PicW / 2 + 1
        Printer.CurrentY = MTop + PicH1 + PicH2 + jj * hRIGA
        Printer.Print stmp
        'COLONNA YY
        stmp = frmt(YY_F1(i), 4)
        Printer.CurrentX = MLeft + PicW / 2 + 1 + 1.5
        Printer.CurrentY = MTop + PicH1 + PicH2 + jj * hRIGA
        Printer.Print stmp
        'COLONNA TT
        stmp = frmt(TT_F1(i), 4)
        Printer.CurrentX = MLeft + PicW / 2 + 1 + 1.5 + 1.5
        Printer.CurrentY = MTop + PicH1 + PicH2 + jj * hRIGA
        Printer.Print stmp
        'COLONNA MISF1
        stmp = frmt(MISF1(i), 3)
        Printer.CurrentX = MLeft + PicW / 2 + 1 + 1.5 + 1.5 + 1.5
        Printer.CurrentY = MTop + PicH1 + PicH2 + jj * hRIGA
        Printer.Print stmp
        'ERRORE DI FORMA
        pX1 = MLeft + PicW / 2 + 1 + 1.5 + 1.5 + 1.5 + 1.5 + 1
        pY1 = MTop + PicH1 + PicH2 + (jj + 0.5) * hRIGA
        pX2 = pX1 + MISF1(i) * ScalaERRORI / 10
        Printer.Line (pX1, pY1)-(pX2, pY1)
        'INCREMENTO IL CONTATORE DELLA RIGA DA STAMPARE
        jj = jj + 1
          If (i < bgnFIANCO) Then
          jjbgnFIANCO = jj 'RIGA DI INIZIO FIANCO
        ElseIf (i < endFIANCO) Then
          jjendFIANCO = jj 'RIGA DI FINE FIANCO
        End If
      End If
    Next i
    'LINEA VERTICALE ERRORE DI FORMA
    pX1 = MLeft + PicW / 2 + 1 + 1.5 + 1.5 + 1.5 + 1.5 + 1
    pY1 = MTop + PicH1 + PicH2 + (1 + 0.5) * hRIGA
    pY2 = MTop + PicH1 + PicH2 + (jj - 0.5) * hRIGA
    Printer.Line (pX1, pY1)-(pX1, pY2)
    'RIQUADRO DA INIZIO A FINE FIANCO 1
    Printer.DrawWidth = 2
    pX1 = MLeft + PicW / 2 + 1 + 1.5 + 1.5 + 1.5 + 1.5 + 1 - 1
    pY1 = MTop + PicH1 + PicH2 + jjbgnFIANCO * hRIGA
    pX2 = MLeft + PicW / 2 + 1 + 1.5 + 1.5 + 1.5 + 1.5 + 1 + 1
    pY2 = MTop + PicH1 + PicH2 + jjendFIANCO * hRIGA
    Printer.Line (pX1, pY1)-(pX2, pY2), , B
    Printer.DrawWidth = 1
    'FIANCO 2
    'COLONNA DEGLI INDICI
    stmp = "nr."
    Printer.CurrentX = MLeft
    Printer.CurrentY = MTop + PicH1 + PicH2
    Printer.Print stmp
    'COLONNA XX
    stmp = "XX"
    Printer.CurrentX = MLeft + 1
    Printer.CurrentY = MTop + PicH1 + PicH2
    Printer.Print stmp
    'COLONNA YY
    stmp = "YY"
    Printer.CurrentX = MLeft + 1 + 1.5
    Printer.CurrentY = MTop + PicH1 + PicH2
    Printer.Print stmp
    'COLONNA TT
    stmp = "TT"
    Printer.CurrentX = MLeft + 1 + 1.5 + 1.5
    Printer.CurrentY = MTop + PicH1 + PicH2
    Printer.Print stmp
    'COLONNA MISF2
    stmp = "MISF2"
    Printer.CurrentX = MLeft + 1 + 1.5 + 1.5 + 1.5
    Printer.CurrentY = MTop + PicH1 + PicH2
    Printer.Print stmp
    'COLONNA SCALA PER ERRORE DI FORMA
    stmp = frmt(ScalaERRORI, 4) & ":1"
    Printer.CurrentX = MLeft + 1 + 1.5 + 1.5 + 1.5 + 1.5 + 1 - Printer.TextWidth(stmp) / 2
    Printer.CurrentY = MTop + PicH1 + PicH2
    Printer.Print stmp
    jj = 1  'INDICE DELLA RIGA DA STAMPARE
    For i = 1 To Np
      'STAMPA DEI SOLI PUNTI CONTROLLATI
      If MISF2(i) <> 1000 Then
        'INDICE
        stmp = Format$(i, "##0")
        Printer.CurrentX = MLeft
        Printer.CurrentY = MTop + PicH1 + PicH2 + jj * hRIGA
        Printer.Print stmp
        'COLONNA XX
        stmp = frmt(XX_F2(i), 4)
        Printer.CurrentX = MLeft + 1
        Printer.CurrentY = MTop + PicH1 + PicH2 + jj * hRIGA
        Printer.Print stmp
        'COLONNA YY
        stmp = frmt(YY_F2(i), 4)
        Printer.CurrentX = MLeft + 1 + 1.5
        Printer.CurrentY = MTop + PicH1 + PicH2 + jj * hRIGA
        Printer.Print stmp
        'COLONNA TT
        stmp = frmt(TT_F2(i), 4)
        Printer.CurrentX = MLeft + 1 + 1.5 + 1.5
        Printer.CurrentY = MTop + PicH1 + PicH2 + jj * hRIGA
        Printer.Print stmp
        'COLONNA MISF2
        stmp = frmt(MISF2(i), 3)
        Printer.CurrentX = MLeft + 1 + 1.5 + 1.5 + 1.5
        Printer.CurrentY = MTop + PicH1 + PicH2 + jj * hRIGA
        Printer.Print stmp
        'ERRORE DI FORMA
        pX1 = MLeft + 1 + 1.5 + 1.5 + 1.5 + 1.5 + 1
        pY1 = MTop + PicH1 + PicH2 + (jj + 0.5) * hRIGA
        pX2 = pX1 + MISF2(i) * ScalaERRORI / 10
        Printer.Line (pX1, pY1)-(pX2, pY1)
        'INCREMENTO IL CONTATORE DELLA RIGA DA STAMPARE
        jj = jj + 1
          If (i < bgnFIANCO) Then
          jjbgnFIANCO = jj 'RIGA DI INIZIO FIANCO
        ElseIf (i < endFIANCO) Then
          jjendFIANCO = jj 'RIGA DI FINE FIANCO
        End If
      End If
    Next i
    'LINEA VERTICALE ERRORE DI FORMA
    pX1 = MLeft + 1 + 1.5 + 1.5 + 1.5 + 1.5 + 1
    pY1 = MTop + PicH1 + PicH2 + (1 + 0.5) * hRIGA
    pY2 = MTop + PicH1 + PicH2 + (jj - 0.5) * hRIGA
    Printer.Line (pX1, pY1)-(pX1, pY2)
    'RIQUADRO DA INIZIO A FINE FIANCO 2
    Printer.DrawWidth = 2
    pX1 = MLeft + 1 + 1.5 + 1.5 + 1.5 + 1.5 + 1 - 1
    pY1 = MTop + PicH1 + PicH2 + jjbgnFIANCO * hRIGA
    pX2 = MLeft + 1 + 1.5 + 1.5 + 1.5 + 1.5 + 1 + 1
    pY2 = MTop + PicH1 + PicH2 + jjendFIANCO * hRIGA
    Printer.Line (pX1, pY1)-(pX2, pY2), , B
    Printer.DrawWidth = 1
    'FINE DOCUMENTO
    Printer.EndDoc
    
  Case 2 'STAMPA DEL TABULATO DELLE VALUTAZIONI
    WRITE_DIALOG "STAMPA DEL TABULATO DELLE VALUTAZIONI"
    'CARATTERI DI STAMPA
    TextFont = "Courier New"
    'IMPOSTAZIONE STAMPANTE
    Printer.ScaleMode = 7
    Printer.DrawWidth = 1
    'IMPOSTAZIONE DEI CARATTERI
    For k = 1 To 2
      Printer.FontName = TextFont
      Printer.FontBold = False
      Printer.FontSize = 10
    Next k
    'LARGHEZZA DI UNA COLONNA
    iCl = Printer.TextWidth("X")
    'ALTEZZA DI UNA RIGA
    iRG = Printer.TextHeight("X")
    'CALCOLO DEL NUMERO MASSIMO DI RIGHE PER FOGLIO
    nRgMAX = (Printer.Height / 1440 * 2.54) / iRG
    'APRO IL FILE DELLE VALUTAZIONI
    NomeFileTXT = g_chOemPATH & "\ERRORI.TXT"
    Open NomeFileTXT For Input As #1
      i = 1: j = 1
      While Not EOF(1)
        'LEGGO LA RIGA DA STAMPARE
        Line Input #1, riga
        'STAMPO LA RIGA
        Printer.CurrentX = 15 * iCl
        Printer.CurrentY = (i - 1) * iRG
        Printer.Print riga
        'INCREMENTO IL CONTATORE DI RIGA
        i = i + 1
        'CONTROLLO SE DEVO CAMBIARE PAGINA
        If (i >= nRgMAX - 5) Then
          j = j + 1
          'NUOVA PAGINA
          Printer.NewPage
          i = 1
          stmp = ""
        End If
      Wend
      'FINE DOCUMENTO
      Printer.EndDoc
    Close #1
  
End Select
'
'CHIUDO I FILE EVENTAULMENTE APERTI
If FreeFile > 1 Then Close
'SEGNALAZIONE UTENTE
WRITE_DIALOG CHK0.Text2.Text & " --> " & Printer.DeviceName & " OK!!"
'
Exit Sub

LETTURA_DATI_DAL_FILE_NomeControllo:
  'LETTURA DATI DEL CREATORE DAL FILE DI CONTROLLO
  Open PathMIS & "\" & NomeControllo For Input As #1
    'LETTURA DELLA PIMA RIGA: INDICI DEI PUNTI
    Line Input #1, riga
    riga = Trim$(riga)
    i = 1
    j = InStr(riga, " ")
    While (j > 0)
      GEN(i) = val(Left$(riga, j - 1))
      riga = Right(riga, Len(riga) - j)
      i = i + 1
      j = InStr(riga, " ")
    Wend
    GEN(i) = val(riga)
    'VERIFICA DEI VALORI LETTI DALLA PRIMA RIGA DEL FILE
    stmp = ""
    For i = 1 To 17
      stmp = stmp & "GEN[0," & Format$(i) & "]=" & frmt(GEN(i), 4) & Chr(13)
    Next i
    'MsgBox stmp
    'INIZIO E FINE RAGGI TESTA
    bgnRAGGI = GEN(1)
    endRAGGI = GEN(2)
    'INIZIO E FINE TESTA
    bgnTESTA = GEN(3)
    endTESTA = GEN(4)
    'INIZIO E FINE FIANCO
    bgnFIANCO = GEN(5)
    endFIANCO = GEN(6)
    'INIZIO E FINE PIEDE
    bgnPIEDE = GEN(7)
    endPIEDE = GEN(8)
    'INIZIO E FINE SEMITOPPING
    bgnSEMITOP = GEN(9)
    endSEMITOP = GEN(10)
    'INIZIO E FINE PROTUBERANZA
    If bgnTESTA = 0 Then
      'SENZA RASTREMAZIONE DI TESTA
      bgnPROTUB = endRAGGI
      endPROTUB = bgnFIANCO
    Else
      'CON   RASTREMAZIONE DI TESTA
      bgnPROTUB = endRAGGI
      endPROTUB = bgnTESTA
    End If
    'INIZIALIZZO IL NUMERO DEI PUNTI DEL PROFILO
    Np = GEN(11)
    'DIMENSIONO I VETTORI IN BASE AL NUMERO DEI PUNTI
    ReDim XX_F1(Np): ReDim YY_F1(Np): ReDim TT_F1(Np): ReDim MISF1(Np)
    ReDim XX_F2(Np): ReDim YY_F2(Np): ReDim TT_F2(Np): ReDim MISF2(Np)
    'FIANCO 1
    For i = 1 To Np
      Line Input #1, riga
      'XX
      j = InStr(riga, " ")
      stmp = Left$(riga, j - 1)
      XX_F1(i) = val(stmp)
      riga = Right(riga, Len(riga) - j)
      riga = Trim$(riga)
      'YY
      j = InStr(riga, " ")
      stmp = Left$(riga, j - 1)
      YY_F1(i) = val(stmp)
      riga = Right(riga, Len(riga) - j)
      riga = Trim$(riga)
      'TT
      j = InStr(riga, " ")
      stmp = Left$(riga, j - 1)
      TT_F1(i) = val(stmp)
      riga = Right(riga, Len(riga) - j)
      riga = Trim$(riga)
      'MIS
      stmp = riga
      MISF1(i) = val(stmp)
      'VERIFICA DEI VALORI LETTI DALLA PRIMA RIGA DEL FILE
      'stmp = ""
      'stmp = stmp & "XX_F1[0," & Format$(i, "##0") & "]=" & frmt(XX_F1(i), 4) & Chr(13)
      'stmp = stmp & "YY_F1[0," & Format$(i, "##0") & "]=" & frmt(YY_F1(i), 4) & Chr(13)
      'stmp = stmp & "TT_F1[0," & Format$(i, "##0") & "]=" & frmt(TT_F1(i), 4) & Chr(13)
      'stmp = stmp & "MISF1[0," & Format$(i, "##0") & "]=" & frmt(MISF1(i), 4) & Chr(13)
      'MsgBox stmp
    Next i
    'FIANCO 2
    For i = 1 To Np
      Line Input #1, riga
      'XX
      j = InStr(riga, " ")
      stmp = Left$(riga, j - 1)
      XX_F2(i) = val(stmp)
      riga = Right(riga, Len(riga) - j)
      riga = Trim$(riga)
      'YY
      j = InStr(riga, " ")
      stmp = Left$(riga, j - 1)
      YY_F2(i) = val(stmp)
      riga = Right(riga, Len(riga) - j)
      riga = Trim$(riga)
      'TT
      j = InStr(riga, " ")
      stmp = Left$(riga, j - 1)
      TT_F2(i) = val(stmp)
      riga = Right(riga, Len(riga) - j)
      riga = Trim$(riga)
      'MIS
      stmp = riga
      MISF2(i) = val(stmp)
      'VERIFICA DEI VALORI LETTI DALLA PRIMA RIGA DEL FILE
      'stmp = ""
      'stmp = stmp & "XX_F2[0," & Format$(i, "##0") & "]=" & frmt(XX_F2(i), 4) & Chr(13)
      'stmp = stmp & "YY_F2[0," & Format$(i, "##0") & "]=" & frmt(YY_F2(i), 4) & Chr(13)
      'stmp = stmp & "TT_F2[0," & Format$(i, "##0") & "]=" & frmt(TT_F2(i), 4) & Chr(13)
      'stmp = stmp & "MISF2[0," & Format$(i, "##0") & "]=" & frmt(MISF2(i), 4) & Chr(13)
      'MsgBox stmp
    Next i
  Close #1
Return

errCONTROLLO_CREATORI_STAMPA:
  WRITE_DIALOG "SUB: CONTROLLO_CREATORI_STAMPA " & str(Err)
  Resume Next

End Sub

Sub CONTROLLO_CREATORI_MODIFICA_CORRETTORI_MOLA()

StopRegieEvents
MsgBox ("Sub: Not S380 ready")
ResumeRegieEvents

'
'
''
'Dim MODALITA            As String
'Dim TIPO_LAVORAZIONE    As String
'Dim sRESET              As String
'Dim iRESET              As Integer
'Dim CorrezioniCalcolate As String
'Dim rsp                 As Integer
'Dim DB                  As Database
'Dim TB                  As Recordset
'Dim Correttore          As String
'Dim stmp                As String
'Dim stmp1               As String
'Dim stmp2               As String
'Dim Atmp                As String * 255
'Dim sDIAMETRO_RULLO     As String
'Dim DIAMETRO_RULLO      As Double
''
'On Error GoTo errCONTROLLO_CREATORI_MODIFICA_CORRETTORI_MOLA
'  '
'  'LEGGO LA MODALITA DI TRASFERIMENTO DATI
'  MODALITA = GetInfo("Configurazione", "MODALITA", PathFILEINI)
'  MODALITA = UCase$(Trim$(MODALITA))
'  If (MODALITA = "OFF-LINE") Then
'    iRESET = 1
'  Else
'    iRESET = CONTROLLO_STATO_RESET
'  End If
'  '
'  If (iRESET <> 1) Then
'    'AVVERTO CHE NON FACCIO NIENTE
'    Write_Dialog "Data cannot be modified: CNC is not on RESET state"
'    Exit Sub
'  End If
'  '
'  'LEGGO IL TIPO DI LAVORAZIONE
'  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
'  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
'  '
'  Select Case TIPO_LAVORAZIONE
'
'    Case "CREATORI_STANDARD"
'      '
'      'SEGNALAZIONE DI CORRETTORI CALCOLATI DAL PROGRAMMA DI CONTROLLO
'      CorrezioniCalcolate = GetInfo(TIPO_LAVORAZIONE, "CorrezioniCalcolate", Path_LAVORAZIONE_INI)
'      '
'      If (CorrezioniCalcolate = "Y") Then
'        '
'        'LE CORREZIONI SONO GIA' CALCOLATE E DISPONIBILI IN GAPP4.INI
'        '
'        'PULISCO LA LISTA DEI CORRETTORI
'        OEM0.List1(1).Clear
'        '
'        'APRO IL DATABASE
'        Set DB = OpenDatabase(PathDB, True, False)
'        Set TB = DB.OpenRecordset("OEM0_CORRETTORI10")
'        '
'        'AUTOCORREZIONE
'        '
'        i = 1
'        'RICERCO NELLA TABELLA
'        TB.Index = "INDICE": TB.Seek "=", i
'        While TB.Fields("ABILITATO").Value = "Y"
'          '
'          'LEGGO LA VARIABILE
'          stmp1 = TB.Fields("VARIABILE_1").Value
'          '
'          'LEGGO LA CORREZIONE ATTUALE
'          stmp = TB.Fields("ACTUAL_1").Value
'          stmp = Trim$(stmp)
'          '
'          'LEGGO LA MODIFICA
'          Correttore = GetInfo(TIPO_LAVORAZIONE, "Correttore(" & Format$(i, "#0") & ")", Path_LAVORAZIONE_INI)
'          '
'          'AGGIORNO IL VALORE DELLA CORREZIONE
'          stmp = frmt(val(stmp) + val(Correttore), 6)
'          '
'          'AGGIORNO LA TABELLA SOMMANDO IL VALORE DELLE CORREZIONI
'          TB.Edit
'          TB.Fields("ACTUAL_1").Value = stmp
'          TB.Update
'          '
'          'AGGIORNO LA LISTA DEI PARAMETRI
'          CALL OEM0.List1(1).AddItem( stmp)
'          '
'          'SCRIVO NEI CORRETTORI
'          rsp = WritePrivateProfileString("OEM0_CORRETTORI10", stmp1, stmp, PathFileSPF)
'          '
'          'SCRIVO NELLA SEZIONE PARAMETRI
'          rsp = WritePrivateProfileString("OEM0_CORRETTORI", stmp1, stmp, g_chOemPATH & "\" & TIPO_LAVORAZIONE & ".INI")
'          '
'          i = i + 1
'          TB.Seek "=", i
'          '
'        Wend
'        '
'        'CHIUDO IL DATABASE
'        TB.Close
'        DB.Close
'        '
'        OEM0.List1(1).Refresh
'        OEM0.List1(1).ListIndex = 0
'        '
'        If (MODALITA = "SPF") Then Call INVIO_SOTTOPROGRAMMA("OEM0_CORRETTORI10")
'        '
'        rsp = WritePrivateProfileString(TIPO_LAVORAZIONE, "CorrezioniCalcolate", "N", Path_LAVORAZIONE_INI)
'        '
'      Else
'        '
'        'COMPENSAZIONE DEL DIAMETRO RULLO CON L'ERRORE DI SPESSORE
'        stmp = GetInfo("CREATORI_STANDARD", "COMPENSAZIONE_DIAMETRO_RULLO", PathFILEINI)
'        stmp = UCase$(Trim$(stmp))
'        '
'        If (stmp = "Y") Then
'          '
'          If (val(OEM0.List1(1).List(0)) <> 0) Then
'            '
'            'RICHIESTA CONFERMA ALL'UTENTE
'            StopRegieEvents
'            rsp = val(GetInfo("SRV0_CREATORI_STANDARD", "AzioneSTR(0)", PathFILEINI))
'            rsp = LoadString(g_hLanguageLibHandle, rsp, Atmp, 255)
'            If (rsp > 0) Then sDIAMETRO_RULLO = Left$(Atmp, rsp)
'            stmp = OEM0.List1(0).List(0) & " --> " & sDIAMETRO_RULLO
'            rsp = MsgBox(stmp, 32 + 4 + 256, "WARNING")
'            ResumeRegieEvents
'            '
'            If (rsp = 6) Then
'              '
'              'AGGIORNAMENTO DEL DIAMETRO RULLO
'              stmp2 = GetInfo("SRV0_CREATORI_STANDARD", "AzioneDDE(0)", PathFILEINI)
'              DIAMETRO_RULLO = val(OPC_LEGGI_DATO(stmp2))
'              DIAMETRO_RULLO = -val(OEM0.List1(1).List(0)) + DIAMETRO_RULLO
'              stmp = frmt(DIAMETRO_RULLO, 8)
'              Call OPC_SCRIVI_DATO(stmp2, stmp)
'              '
'              'AZZERAMENTO DEL VALORE DI CORREZIONE DI SPESSORE
'              OEM0.List1(1).Clear
'              '
'              'APRO IL DATABASE
'              Set DB = OpenDatabase(PathDB, True, False)
'              Set TB = DB.OpenRecordset("OEM0_CORRETTORI10")
'              '
'              'AUTOCORREZIONE
'              '
'              i = 1
'              'RICERCO NELLA TABELLA
'              TB.Index = "INDICE": TB.Seek "=", i
'              While TB.Fields("ABILITATO").Value = "Y"
'                '
'                'LEGGO LA VARIABILE
'                stmp1 = TB.Fields("VARIABILE_1").Value
'                '
'                'LEGGO LA CORREZIONE ATTUALE
'                stmp = TB.Fields("ACTUAL_1").Value
'                stmp = Trim$(stmp)
'                '
'                'AZZERO LA CORREZIONE DI SPESSORE
'                If (i = 1) Then stmp = "0"
'                '
'                'AGGIORNO LA TABELLA
'                TB.Edit
'                TB.Fields("ACTUAL_1").Value = stmp
'                TB.Update
'                '
'                'AGGIORNO LA LISTA DEI PARAMETRI
'                CALL OEM0.List1(1).AddItem (stmp)
'                '
'                'SCRIVO NEI CORRETTORI
'                rsp = WritePrivateProfileString("OEM0_CORRETTORI10", stmp1, stmp, PathFileSPF)
'                '
'                'SCRIVO NELLA SEZIONE PARAMETRI
'                rsp = WritePrivateProfileString("OEM0_CORRETTORI", stmp1, stmp, g_chOemPATH & "\" & TIPO_LAVORAZIONE & ".INI")
'                '
'                i = i + 1
'                TB.Seek "=", i
'                '
'              Wend
'              '
'              'CHIUDO IL DATABASE
'              TB.Close
'              DB.Close
'              '
'              OEM0.List1(1).Refresh
'              OEM0.List1(1).ListIndex = 0
'              '
'              If (MODALITA = "SPF") Then Call INVIO_SOTTOPROGRAMMA("OEM0_CORRETTORI10")
'              '
'              'SEGNALAZIONE UTENTE
'              Write_Dialog sDIAMETRO_RULLO & ": " & frmt(DIAMETRO_RULLO, 8) & " (Updated)"
'              '
'            Else
'              '
'              'SEGNALAZIONE UTENTE
'              Write_Dialog "Operation aborted by user."
'              '
'            End If
'            '
'          Else
'            '
'            'AVVERTO CHE NON FACCIO NIENTE
'            Write_Dialog "SOFTKEY DISENABLED!!"
'            '
'          End If
'          '
'        End If
'        '
'      End If
'
'  End Select
'  '
'  'CHIUDO I FILE EVENTUALMENTE APERTI
'  If (FreeFile > 1) Then Close
'  '
'Exit Sub
'
'errCONTROLLO_CREATORI_MODIFICA_CORRETTORI_MOLA:
'  Write_Dialog "SUB: CONTROLLO_CREATORI_MODIFICA_CORRETTORI_MOLA " & Str(Err)
'  Resume Next

End Sub

Public Sub CONTROLLO_CREATORI_CREAZIONE_SOTTOPROGRAMMI()
'
Dim FeedCtr   As Double
Dim ErMax     As Double
Dim InerziaY  As Double
Dim InerziaAX As Double
Dim DSM       As Double
Dim Xsr       As Double
Dim NomeFile  As String
'
On Error Resume Next
  '
  NomeFile = GetInfo("CREATORI_STANDARD", "SPF(1)", Path_LAVORAZIONE_INI)
  NomeFile = NomeFile & ".SPF"
  Open g_chOemPATH & "\" & NomeFile For Output As #1
    Print #1, ";PROGRAMMA: " & NomeFile
    Print #1, ";DISEGNO  : " & Et$
    Print #1, ";DATA     : " & Date
    Print #1, "R300 ="; Mn; Tab(20); ";Mn       = Modulo normale"
    Print #1, "R301 ="; NFil; Tab(20); ";Nfil     = numero principi CR"
    Print #1, "R302 ="; AlfaDeg; Tab(20); ";Alfa     = Angolo di pressione"
    Print #1, "R303 ="; PasAxial; Tab(20); ";PasAxial = Passo assiale"
    Print #1, "R304 ="; PasHel; Tab(20); ";PasHel   = Passo spirale"
    Print #1, "R305 ="; DestCr; Tab(20); ";DestCr   = Diametro esterno teorico"
    Print #1, "R306 ="; Nscan; Tab(20); ";Nscan    = numero taglienti"
    Print #1, "R307 ="; Camma; Tab(20); ";Camma    = Camma"
    Print #1, "R308 ="; SottIntaglio; Tab(20); ";Sottintaglio = Sottintaglio in mm"
    Print #1, "R309 ="; Senso; Tab(20); ";Senso    = senso elica (1=DX, -1=SX)"
    Print #1, "R310 ="; DisTagliente; Tab(20); ";DisTagliente = Distanza da tagliente x controllo"
    Print #1, "R311 ="; Hkw; Tab(20); ";HKW      = Addendum"
    Print #1, "R312 ="; Sw; Tab(20); ";SW      = Spessore"
    Print #1, "M17 ;Fine"
  Close #1
  '
  NomeFile = GetInfo("CREATORI_STANDARD", "SPF(2)", Path_LAVORAZIONE_INI)
  NomeFile = NomeFile & ".SPF"
  Open g_chOemPATH & "\" & NomeFile For Output As #1
    Print #1, ";PROGRAMMA: " & NomeFile
    Print #1, ";DISEGNO  : " & Et$
    Print #1, ";DATA     : " & Date
    Print #1, "GEN[0,1] ="; NInTesta; Tab(20); ";NinTesta="; Tab(32); "primo punto Raggio di testa "
    ' Print #1, "GEN[0,2] ="; NFinTesta; Tab(20); ";NfinTesta="; Tab(32); "ultimo punto Raggio di testa"
    Print #1, "GEN[0,2] ="; NFinRt; Tab(20); ";NfinTesta="; Tab(32); "ultimo punto Raggio di testa"
    Print #1, "GEN[0,3] ="; NInRastT; Tab(20); ";NinRastT="; Tab(32); "primo punto Rastremazione di testa"
    Print #1, "GEN[0,4] ="; NFinRastT; Tab(20); ";NfinRastT="; Tab(32); "ultimo punto Rastremazione di testa"
    Print #1, "GEN[0,5] ="; NInFia; Tab(20); ";NInFia ="; Tab(32); "primo punto fianco"
    Print #1, "GEN[0,6] ="; NFinFia; Tab(20); ";NFinFia="; Tab(32); "ultimo punto fianco"
    Print #1, "GEN[0,7] ="; NInRastP; Tab(20); ";NInRastP="; Tab(32); "primo punto Rastremazione di piede"
    Print #1, "GEN[0,8] ="; NFinRastP; Tab(20); ";NFinRastP="; Tab(32); "ultimo punto Rastremazione di piede"
    Print #1, "GEN[0,9] ="; NInSt; Tab(20); ";NInSt ="; Tab(32); "primo punto semitopping"
    Print #1, "GEN[0,10] ="; NFinSt; Tab(20); ";NFinSt ="; Tab(32); "ultimo punto semitopping"
    Print #1, "GEN[0,11] ="; Ntot; Tab(20); ";Ntot="; Tab(32); "ultimo punto"
    Print #1, ";Profilo assiale fianco 1"
    For i = 1 To NbpTotal
      Print #1, "XX[0,"; Format(i, "##0"); "]="; frmt(Xfma(1, i), 4); Tab(20);
      Print #1, "YY[0,"; Format(i, "##0"); "]="; frmt(Rfma(1, i), 4); Tab(40);
      Print #1, "TT[0,"; Format(i, "##0"); "]="; frmt(FnGrad(ApFma(1, i)), 4)
    Next i
    Print #1, ";Profilo assiale fianco 2"
    For i = 1 To NbpTotal
      Print #1, "XX[0,"; Format(i + 100, "##0"); "]="; frmt(Xfma(2, i), 4); Tab(20);
      Print #1, "YY[0,"; Format(i + 100, "##0"); "]="; frmt(Rfma(2, i), 4); Tab(40);
      Print #1, "TT[0,"; Format(i + 100, "##0"); "]="; frmt(FnGrad(ApFma(2, i)), 4)
    Next i
    Print #1, "M17 ;Fine"
  Close #1
  '
  'Profilo cremagliera
  NomeFile = GetInfo("CREATORI_STANDARD", "SPF(3)", Path_LAVORAZIONE_INI)
  NomeFile = NomeFile & ".SPF"
  Open g_chOemPATH & "\" & NomeFile For Output As #1
    Print #1, ";PROGRAMMA: " & NomeFile
    Print #1, ";DISEGNO  : " & Et$
    Print #1, ";DATA     : " & Date
    Print #1, ";Profilo fianco 1"
    For i = 1 To NbpTotal
      Print #1, "XX[0,"; Format(i, "##0"); "]="; frmt(Xfm(1, i), 4); Tab(20);
      Print #1, "YY[0,"; Format(i, "##0"); "]="; frmt(RFM(1, i), 4); Tab(40);
      Print #1, "TT[0,"; Format(i, "##0"); "]="; frmt(FnGrad(ApFm(1, i)), 4)
    Next i
    Print #1, ";Profilo fianco 2"
    For i = 1 To NbpTotal
      Print #1, "XX[0,"; Format(i + 100, "##0"); "]="; frmt(Xfm(2, i), 4); Tab(20);
      Print #1, "YY[0,"; Format(i + 100, "##0"); "]="; frmt(RFM(2, i), 4); Tab(40);
      Print #1, "TT[0,"; Format(i + 100, "##0"); "]="; frmt(FnGrad(ApFm(2, i)), 4)
    Next i
    Print #1, "M17 ;Fine"
  Close #1
  '
End Sub

Sub CalcUnPuntoAxial(Xcrem As Double, Rref As Double, ApCrem As Double, Xax As Double, Rax As Double, ApAx As Double)
'
Dim InEngr  As Double
Dim Ycrem   As Double
Dim Apa     As Double
Dim Rb      As Double
Dim A       As Double
Dim Intf$
Dim ApApp   As Double
Dim Helx    As Double
'
On Error Resume Next
   '
   ' Calcul de 1 point dans le plan axial
   ' obtenu en considerant le profil du plan axial de la fraise-mere
   ' comme etant le profil conjugue de la cremaillere de reference
   ' roulant sur le diametre primitif correspondant a l'inclinaison
   ' Ceci pour une inclinaison < 1 degre
   '
   ' Donnees :  PG
   ' + pour chaque point du profil de la cremaillere de reference :
   '           Xcrem  : Distance point considere a l'axe du plein
   '           Rref   : Distance point considere a l'axe de la fraise-mere
   '           Apcrem : Angle de pression sur le point considere en radians
   ' + CR : InFild , InFil , DprimCr , PasAxial
  '
  'Stop
   If InFilD < 1 Then
     Xax = Xcrem / Cos(InFil)
     Rax = Rref
     ApAx = Atn(Tan(ApCrem) / Cos(InFil))
   Else
     InEngr = PG / 2 - InFil
     RprimCr = DprimCr / 2
     Ycrem = RprimCr - Rref
     If FnGrad(Abs(ApCrem)) < 89 Then
        If Abs(ApCrem) = 0 Then
          ApCrem = 0.00001
        End If
        Apa = Atn(Tan(ApCrem) / Cos(InEngr))
        Rb = RprimCr * Cos(Apa)
        A = Rb * Tan(Apa) - Ycrem / Sin(Apa)
        If A < 0 Then Intf$ = "OUI" Else Intf$ = "NON"
        Rax = Sqr(Rb ^ 2 + A ^ 2)
        Aux = (Ycrem / Tan(Apa) + Xcrem / Cos(InEngr)) / RprimCr
        Aux = Aux - Apa + Atn(A / Rb)
        ApApp = Acs(Rb / Rax)
        Helx = Atn(PasAxial / PG / (Rax * 2))
        ApAx = Atn(Tan(ApApp) * Tan(Helx))
     Else
        Intf$ = "NON"
        Rax = Rref
        Aux = Xcrem / Cos(InEngr) / RprimCr
        ApApp = PG / 2
        Helx = Atn(PasAxial / PG / (Rax * 2))
        ApAx = PG / 2
     End If
     Xax = Aux * PasAxial / PG / 2
   End If
  '
End Sub

Sub CalcPuntiRaggio(ByVal R As Double, ByVal XR As Double, ByVal YR As Double, ByVal Adep As Double, ByVal Afin As Double, ByVal Nbp As Integer)
'
Dim i As Integer
'
On Error Resume Next
  '
  For i = 2 To Nbp  ' le point N.1 est calcule pour la zone precedente
    Aux = (Afin - Adep) / (Nbp - 1) * (i - 1) + Adep
    XX = XR + R * Sin(Aux)
    YY = YR + R * Cos(Aux)
    Call MemorisPunto(XX, YY, PG / 2 - Aux)
  Next i
  '
End Sub
'
Sub CalcPuntiFianco(ByVal Xdep As Double, ByVal Ydep As Double, ByVal Xfin As Double, ByVal Yfin As Double, ByVal Ap As Double, ByVal Nbp As Integer)
'
Dim i As Integer
'
On Error Resume Next
  '
  For i = 2 To Nbp
    YY = Ydep - (Ydep - Yfin) / (Nbp - 1) * (i - 1)
    XX = Xdep - (Xdep - Xfin) / (Nbp - 1) * (i - 1)
    Call MemorisPunto(XX, YY, Ap)
  Next i
  '
End Sub

Sub MemorisPunto(ByVal XX As Double, ByVal YY As Double, ByVal Ap As Double)
'
On Error Resume Next
  '
  ' Memorise Xx , Yy , Ap   et Incremente NbpTotal
  '
  NbpTotal = NbpTotal + 1
  Xfm(Fianco, NbpTotal) = XX
  RFM(Fianco, NbpTotal) = DestCr / 2 - Hkw + YY
  ApFm(Fianco, NbpTotal) = Ap
  '
End Sub

Sub CalcPuntiCrem()
'
Dim Rcot    As Double
Dim Ap      As Double
Dim Xdep    As Double
Dim Ydep    As Double
Dim Adep    As Double
Dim Afin    As Double
Dim Xfin    As Double
Dim Yfin    As Double
Dim XXfin   As Double
Dim YYfin   As Double
Dim ApTanRt As Double
'
On Error Resume Next
  '
  NpTESTA = 1      'numero punti per calcolo testa
  NpRtesta = 9     '                       raggio di testa
  NpPia = 5        '                       piattino
  NpRPia = 3       '                       raccordo piattino-protub.
  NpProtu = 5      '                       protuberanza
  NpRastT = 5      '                       rastremazione testa
  NpFIANCO = 15    '                       fianco
  NpRastP = 5      '                       rastremazione piede
  NpRSemit = 3     '                       raggio semitopping
  NpSemit = 5      '                       semitopping
  NpRfondo = 7     '                       raggio di fondo
  Npfondo = 4      '                       fondo
  '
  NInRt = 0        'indice punto inizio raggio di testa
  NFinRt = 0       '             fine raggio di testa
  NInTesta = 0     'indice punto inizio zona testa
  NFinTesta = 0    '             fine zona testa
  NInRastT = 0     '             inizio rastremazione testa
  NFinRastT = 0    '             fine rastremazione testa
  NInFia = 0       '             inizio fianco
  NFinFia = 0      '             fine fianco
  NInRastP = 0     '             inizio rastremazione piede
  NFinRastP = 0    '             fine rastremazione piede
  NInSt = 0        '             inizio semitopping
  NFinSt = 0       '             fine semitopping
  NFinRf = 0       '             fine raggio di fondo
  '
  ' Calcul de tout le profil de la cremaillere par points
  '
  ApTanRt = ApFl
  If Rastt$ = "O" Then ApTanRt = ApRastt
  '
  Rcot = DestCr / 2 - Hkw
  '
  ' 1 ) TESTA : RAGGIO + PIATTO TESTA *******
  '                                               ' dessus
  NbpTotal = 0
  Call MemorisPunto(0, Hkw, PG / 2)
  If Xrtesta > 0.001 Then
    NpTESTA = 4
    Call CalcPuntiFianco(0, Hkw, Xrtesta, Hkw, PG / 2, NpTESTA)
  End If
  '                                             Raggio di testa
  NInRt = NbpTotal   'inizio raggio di testa
  NInTesta = NbpTotal   'inizio zona testa
  Ap = ApTanRt
  If TreRaggi$ <> "O" Then
    If Pr$ = "O" Then
      If Hpiatt > 0 Then Ap = ApTanRt Else Ap = ApProtu
    End If
    Call CalcPuntiRaggio(Rtesta, Xrtesta, Yrtesta, 0, PG / 2 - Ap, NpRtesta)
    Xdep = Xfm(Fianco, NbpTotal): Ydep = RFM(Fianco, NbpTotal) - Rcot
  End If
  '
  NFinRt = NbpTotal   ' fine raggi di testa
  '
  ' 2  ) TESTA : PROTUBERANZA  *******
  '
  If Pr$ = "O" Then
    If TreRaggi$ = "O" Then
      ' Tre Raggi
      Aux = Asn(XtreRag2 / (TreRag2 - Rtesta))
      Call CalcPuntiRaggio(Rtesta, Xrtesta, Yrtesta, 0, Aux, 5)
      Aux1 = Asn((XtreRag2 + XtreRag3) / (TreRag2 - TreRag3))
      Call CalcPuntiRaggio(TreRag2, -XtreRag2, YtreRag2, Aux, Aux1, 9)
      Aux = PG / 2 - ApProtu
      Call CalcPuntiRaggio(TreRag3, XtreRag3, YtreRag3, Aux1, Aux, 5)
      Xdep = Xfm(Fianco, NbpTotal): Ydep = RFM(Fianco, NbpTotal) - Rcot
      '
      NFinRt = NbpTotal   ' fine raggi di testa
      '
    Else
      If Hpiatt > 0 Then
        ' piattino
        XXfin = XrPiatt + Rpiatt * Cos(ApTanRt)
        YYfin = Yrpiatt + Rpiatt * Sin(ApTanRt)
        Call CalcPuntiFianco(Xdep, Ydep, XXfin, YYfin, ApTanRt, NpPia)
        ' raggio piattino / protu
        Adep = PG / 2 - ApTanRt: Afin = PG / 2 - ApProtu
        Call CalcPuntiRaggio(Rpiatt, XrPiatt, Yrpiatt, Adep, Afin, NpRPia)
        Xdep = Xfm(Fianco, NbpTotal): Ydep = RFM(Fianco, NbpTotal) - Rcot
      End If
    End If
    ' Angle protu
    Call CalcPuntiFianco(Xdep, Ydep, XiProt, YiProt, ApProtu, NpProtu)
    Xdep = Xfm(Fianco, NbpTotal): Ydep = RFM(Fianco, NbpTotal) - Rcot
  End If
  '
  NFinTesta = NbpTotal   ' fine raggi di testa e protub. (forma testa)
  '
  ' 3 ) TESTA : RASTREMAZIONE  *******
  '
  If Rastt$ = "O" Then
    NInRastT = NbpTotal   'inizio rastremazione testa
    NpRastT = Int((Ydep - YiRastt) / 0.4) * 2 + 1
    If NpRastT < 5 Then NpRastT = 5
    Call CalcPuntiFianco(Xdep, Ydep, XiRastt, YiRastt, ApRastt, NpRastT)
    Xdep = Xfm(Fianco, NbpTotal): Ydep = RFM(Fianco, NbpTotal) - Rcot
    NFinRastT = NbpTotal   'fine rastremazione testa
  End If
  '
  ' 4 ) FIANCO  *******
  '
  NInFia = NbpTotal   'inizio fianco
  If Rastp$ = "O" Then
    Xfin = XiRastp
    Yfin = -YiRastp
  Else
    If St$ = "O" Then
       Xfin = Xrsemit - Rsemit * Cos(ApFl)
       Yfin = -(Yrsemit + Rsemit * Sin(ApFl))
    End If
    If St$ = "N" Then
       Xfin = XrFondo - Rfondo * Cos(ApFl)
       Yfin = -(Yrfondo + Rfondo * Sin(ApFl))
    End If
  End If
  NpFIANCO = Int((Ydep - Yfin) / 0.25) * 2 + 1
  If NpFIANCO < 5 Then NpFIANCO = 5
  If NpFIANCO > 55 Then NpFIANCO = 55
  'If (Yfin - Ydep) < 1.2 Then NpFianco = 9
  Call CalcPuntiFianco(Xdep, Ydep, Xfin, Yfin, ApFl, NpFIANCO)
  '
  NFinFia = NbpTotal   ' fine fianco
  '
  ' 5 ) PIEDE : RASTREMAZIONE  *******
  '
  If Rastp$ = "O" Then
    If St$ = "O" Then
       Xfin = Xrsemit - Rsemit * Cos(ApRastp)
       Yfin = -(Yrsemit + Rsemit * Sin(ApRastp))
    End If
    If St$ = "N" Then
       Xfin = XrFondo - Rfondo * Cos(ApRastp)
       Yfin = -(Yrfondo + Rfondo * Sin(ApRastp))
    End If
   
    NInRastP = NbpTotal   'inizio rastremazione piede
    
    NpRastP = Int((-YiRastp - Yfin) / 0.4) * 2 + 1
    If NpRastP < 5 Then NpRastP = 5

    Call CalcPuntiFianco(XiRastp, -YiRastp, Xfin, Yfin, ApRastp, NpRastP)
    NFinRastP = NbpTotal   'fine rastremazione piede
    '
  End If
  '
  ' 6 ) PIEDE : SEMI-TOPPING  *******
  '
  If St$ = "O" Then
    ' raggio fianco / Semi-topping
    If Rastp$ = "O" Then Adep = PG / 2 - ApRastp Else Adep = PG / 2 - ApFl
    Afin = PG / 2 - ApSt
    Call CalcPuntiRaggio(-Rsemit, Xrsemit, -Yrsemit, Adep, Afin, NpRSemit)
    '
    NInSt = NbpTotal   'inizio semitopping
    '
    Xdep = Xfm(Fianco, NbpTotal): Ydep = RFM(Fianco, NbpTotal) - Rcot
    ' Semi-topping
    Xfin = XrFondo - Rfondo * Cos(ApSt)
    Yfin = -(Yrfondo + Rfondo * Sin(ApSt))
    Call CalcPuntiFianco(Xdep, Ydep, Xfin, Yfin, ApSt, NpSemit)
    '
    NFinSt = NbpTotal   'fine semitopping
    '
  End If
  '
  '
  ' 7 ) PIEDE : RAGGIO DI FONDO  *******
  If St$ = "O" Then Adep = PG / 2 - ApSt
  If St$ = "N" And Rastp$ = "O" Then Adep = PG / 2 - ApRastp
  If St$ = "N" And Rastp$ = "N" Then Adep = PG / 2 - ApFl
  Call CalcPuntiRaggio(-Rfondo, XrFondo, -Yrfondo, Adep, 0, NpRfondo)
  '
  NFinRf = NbpTotal   ' fine raggio fondo
  '
  ' 9 ) PIEDE : FONDO DENTE  *******
  '
  If Abs(XrFondo - PG * Mn / 2) > 0.002 Then
    Call CalcPuntiFianco(XrFondo, -Yfond, PG * Mn / 2, -Yfond, PG / 2, Npfondo)
  End If
  '
  Ntot = NbpTotal
  '
End Sub

Function Acs(ByVal Aux As Double) As Double
'
Dim Aux1 As Double
'
On Error Resume Next
  '
  Aux1 = (1 - Aux * Aux)
  If (Aux1 < 0) Then
    '
    flagErrore = True
    '
  Else
    '
    If (Aux = 0) Then
      '
      Acs = PG / 2
      '
    Else
      '
      If (Aux > 0) Then
        '
        Acs = Atn(Sqr(1 - Aux * Aux) / Aux)
        '
      Else
        '
        Aux = Abs(Aux)
        Acs = Atn(Aux / Sqr(1 - Aux * Aux)) + PG / 2
        '
      End If
      '
    End If
    '
  End If
  '
End Function

Function Asn(ByVal Aux As Double) As Double
'
Dim Aux1 As Double
'
On Error Resume Next
  '
  Aux1 = (1 - Aux * Aux)
  If (Aux1 < 0) Then
    '
    flagErrore = True
    '
  Else
    '
    If (Aux = 1) Then
      '
      Asn = PG / 2
      '
    Else
      '
      Asn = Atn(Aux / Sqr(Aux1))
      '
    End If
    '
  End If
  '
End Function

Function FnGrad(ByVal A As Double) As Double
'
On Error Resume Next
  '
  FnGrad = A / PG * 180
  '
End Function

Function FnRad(ByVal A As Double) As Double
'
On Error Resume Next
  '
  FnRad = A / 180 * PG
  '
End Function

'
Public Sub CalcoloCremDIB()
'
'CALCUL DES POINTS CARACTERISTIQUES DE LA CREM.
'
'1 )      TESTA
'
Dim AB      As Double
Dim ac      As Double
Dim ACB     As Double
Dim Aux     As Double
Dim DE      As Double
Dim CDE     As Double
Dim CD      As Double
Dim XfondSt As Double
Dim Ap      As Double
Dim ApTanRt As Double
'
On Error Resume Next
  '
  ApFl = FnRad(AlfaDeg)
  ApRastt = FnRad(AngRastrT)
  Yrtesta = Hkw - Rtesta
  '
  'angolo tangenza RT
  '
  ApTanRt = ApFl: If Rastt$ = "O" Then ApTanRt = ApRastt
  '
  If (Pr$ <> "O") Then
    Hprot = 0
    ApProtu = ApFl
    ApProtuD = FnGrad(ApProtu)
  Else
    If (TreRaggi$ <> "O") Then
      ApProtu = ApTanRt - Atn(OC / Cos(ApTanRt) / (Hprot - Rtesta * (1 - Sin(ApTanRt))))
      ApProtuD = FnGrad(ApProtu)
    End If
  End If
  '
  If (Rastt$ = "O") And (Pr$ = "O") Then ApTanRt = ApProtu
  ' ********************     calcolo Xrtesta
  If (TreRaggi$ = "O") Then
    '
    Xrtesta = 0
    '
  Else
    '                      Rastt$ : Rastremazione testa CR
    If (Rastt$ = "O") Then
      '
      ApRastt = FnRad(AngRastrT)
      YiRastt = Hkw - Hrastrt
      XiRastt = Sw / 2 - YiRastt * Tan(ApFl)
      Esfm = (XiRastt - (Hrastrt - Hprot) * Tan(ApRastt) - Hprot * Tan(ApProtu)) * 2
      If E63 > 0 Then Esfm = Sw - Hkw * 2 * Tan(ApRastt) + 2 * OC / Cos(ApRastt)
      '
    Else
      '
      Esfm = Sw - Hkw * 2 * Tan(ApFl) + 2 * OC / Cos(ApFl)
      '
    End If
    Xrtesta = Esfm / 2 - Rtesta * Tan((PG / 2 - ApTanRt) / 2)
    If (E63 > 0) And (Rastt$ = "O") Then Xrtesta = Esfm / 2 - Rtesta * Tan((PG / 2 - ApRastt) / 2)
    '
  End If
  '
  ' ***********************
  If (Pr$ = "O") Then
    '
    If (TreRaggi$ = "O") Then
      '
      YtreRag2 = Yrtesta - Sqr((TreRag2 - Rtesta) ^ 2 - XtreRag2 ^ 2)
      AB = Sw / 2 + XtreRag2 - YtreRag2 * Tan(ApFl) - (TreRag3 - OC) / Cos(ApFl)
      ac = TreRag2 - TreRag3
      ACB = Asn(Cos(ApFl) * AB / ac)
      Aux = ACB - ApFl
      YtreRag3 = ac * Cos(Aux) + YtreRag2
      XtreRag3 = ac * Sin(Aux) - XtreRag2
      DE = Sw / 2 - (Hkw - Hprot) * Tan(ApFl) - XtreRag3
      CDE = Atn((YtreRag3 - Hkw + Hprot) / DE)
      CD = DE / Cos(CDE)
      ApProtu = Acs(TreRag3 / CD) - CDE
      ApProtuD = FnGrad(ApProtu)
      '
    Else
      '
      'If Rastt$ = "O" Then ApProtuD = AngRastrT - AngProt Else ApProtuD = AlfaDeg - AngProt
      'ApProtu = FnRad(ApProtuD)
      If (E63 > 0) Then
        '
        ' Tra il piattino e l'angolo protuberanza, c'e sempre un raggio :
        ' Rpiatt = Rtesta - demande Claudio Noffs 9.6.99
        If Rastt$ = "O" Then ApTanRt = ApRastt Else ApTanRt = ApFl
        Hpiatt = Rtesta * (1 - Sin(ApTanRt)) + E63
        Rpiatt = Rtesta
        XiPiatt = Esfm / 2 + Hpiatt * Tan(ApTanRt)
        Yipiatt = Hkw - Hpiatt
        Aux = (ApTanRt - ApProtu) / 2 + ApProtu
        XrPiatt = XiPiatt - Rpiatt / Cos((ApTanRt - ApProtu) / 2) * Cos(Aux)
        HrPiatt = Hpiatt + Rpiatt / Cos((ApTanRt - ApProtu) / 2) * Sin(Aux)
        Yrpiatt = Hkw - HrPiatt
        'Aux = ApTanRT - ApProtu
        'Hprot = Hpiatt + OC / Sin(Aux) * Cos(ApProtu)
        '
      Else
        '
        'Aux = Rtesta * (Tan(PG / 4 - ApProtu / 2) - Tan(PG / 4 - ApFl / 2))
        'Hprot = (Aux + OC / Cos(ApFl)) / (Tan(ApFl) - Tan(ApProtu))
        Hpiatt = 0
        '
      End If
      '
    End If
    YiProt = Hkw - Hprot
    XiProt = Sw / 2 - YiProt * Tan(ApFl)
    If (Rastt$ = "O") Then XiProt = XiRastt + (YiRastt - YiProt) * Tan(ApRastt)
  End If
  '
  '      CALCUL DES POINTS DE LA CREM.         2 )      PIEDE
  '
  If (Rastp$ = "O") Then
    ApRastp = Atn((Hrastrp * Tan(ApFl) + Rastrp) / Hrastrp)
  Else
    Rastrp = 0: Hrastrp = 0
  End If
  If (St$ = "N") Then              '         2.1 ) Senza Semi-Topping
    '
    Yfond = HW - Hkw
    Yrfondo = Yfond - Rfondo
    If (Rastp$ = "N") Then
      XrFondo = Sw / 2 + Yrfondo * Tan(ApFl) + Rfondo / Cos(ApFl)
    Else
      YiRastp = Yfond - Hrastrp
      XiRastp = Sw / 2 + YiRastp * Tan(ApFl)
      XrFondo = XiRastp + (Yrfondo - YiRastp) * Tan(ApRastp) + Rfondo / Cos(ApRastp)
    End If
    '
  End If
  '
  If (St$ = "O") Then              '         2.2 ) Con Semi-Topping
    ApSt = FnRad(GammaDeg)
    Xist = Sw / 2 + Yist * Tan(ApFl) + Rastrp
    YiRastp = Yist - Hrastrp
    XiRastp = Sw / 2 + YiRastp * Tan(ApFl)
    If (Tp$ = "N") Then                   ' Senza Topping
      XrFondo = PG * Mn / 2
      Yrfondo = (XrFondo - Xist) / Tan(ApSt) + Yist - Rfondo / Sin(ApSt)
      Yfond = Yrfondo + Rfondo
    Else                                  ' con topping
      Yfond = Hr - Hkw
      Yrfondo = Yfond - Rfondo
      XfondSt = (Yfond - Yist) * Tan(ApSt) + Xist
      XrFondo = XfondSt + Rfondo * Tan(PG / 4 - ApSt / 2)
    End If
    If (Rastp$ = "O") Then Ap = ApRastp Else Ap = ApFl
    Aux = Rsemit * Tan((ApSt - Ap) / 2)
    Xrsemit = Xist - Aux * Sin(Ap) + Rsemit * Cos(Ap)
    Yrsemit = Yist - Aux * Cos(Ap) - Rsemit * Sin(Ap)
  End If
  '
End Sub

Public Sub LETTURA_PROFILO_CREATORE(ByVal FINESTRA As Form, ByVal TIPO_LETTURA As String)
'
Dim stmp        As String
Dim k           As Integer
Dim PercorsoINI As String
Dim MODALITA    As String
'
Const SEZIONE = "OEM1_PROFILO"
Const TIPO_LAVORAZIONE = "CREATORI_STANDARD"
'
On Error GoTo errLETTURA_PROFILO_CREATORE
  '
  MODALITA = GetInfo("Configurazione", "MODALITA", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))
  '
  'GRAFICO
  If (MODALITA <> "OFF-LINE") Then
    Et$ = LEGGI_NomePezzo(TIPO_LAVORAZIONE)
  Else
    Et$ = GetInfo(TIPO_LAVORAZIONE, "NomePezzoInserimento", Path_LAVORAZIONE_INI)
  End If
  '
  Select Case TIPO_LETTURA
    '
    Case "FILE_INI"
      '
      PercorsoINI = g_chOemPATH & "\" & TIPO_LAVORAZIONE & ".INI"
      '
      'PROTUBERANZA: Pr$
      'k = Val(GetInfo(SEZIONE, "PR", PercorsoINI))
      'If (k = 1) Then Pr$ = "O" Else Pr$ = "N"
      Pr$ = "N"
      Hprot = val(GetInfo(SEZIONE, "Hprot(1)", PercorsoINI))
      If (Hprot <> 0) Then
        Pr$ = "O"
      End If
      Hprot = val(GetInfo(SEZIONE, "Hprot(2)", PercorsoINI))
      If (Hprot <> 0) Then
        Pr$ = "O"
      End If
      '
      'SEMITOPPING: St$
      'k = Val(GetInfo(SEZIONE, "ST", PercorsoINI))
      'If (k = 1) Then St$ = "O" Else St$ = "N"
      Hr = val(GetInfo(SEZIONE, "HR", PercorsoINI))
      If (Hr <> 0) Then
        St$ = "O"
      Else
        St$ = "N"
      End If
      '
      Tp$ = "O"
      '
      'RASTREMAZIONE DI PIEDE: Rastp$
      'k = Val(GetInfo(SEZIONE, "Rastp", PercorsoINI))
      'If (k = 1) Then Rastp$ = "O" Else Rastp$ = "N"
      Rastp$ = "N"
      E82 = val(GetInfo(SEZIONE, "E82(1)", PercorsoINI))
      If (E82 <> 0) Then
        Rastp$ = "O"
      End If
      E82 = val(GetInfo(SEZIONE, "E82(2)", PercorsoINI))
      If (E82 <> 0) Then
        Rastp$ = "O"
      End If
      '
      'RASTREMAZIONE DI TESTA: Rastt$
      'k = Val(GetInfo(SEZIONE, "RastT", PercorsoINI))
      'If (k = 1) Then Rastt$ = "O" Else Rastt$ = "N"
      Rastt$ = "N"
      Hrastrt = val(GetInfo(SEZIONE, "Hrastrt(1)", PercorsoINI))
      If (Hrastrt <> 0) Then
        Rastt$ = "O"
      End If
      Hrastrt = val(GetInfo(SEZIONE, "Hrastrt(2)", PercorsoINI))
      If (Hrastrt <> 0) Then
        Rastt$ = "O"
      End If
      '
      'SONO SOLO NUMERI REALI
      Mn = val(GetInfo(SEZIONE, "Mn", PercorsoINI))
      Hkw = val(GetInfo(SEZIONE, "HKW", PercorsoINI))
      'SPESSORE MOLA SUL PRIMITIVO --> VANO CREATORE
      Sw = val(GetInfo(SEZIONE, "SW", PercorsoINI))
      Sw = Mn * PG - Sw 'SPESSORE EFFETTIVO CREATORE
      Hr = val(GetInfo(SEZIONE, "HR", PercorsoINI))
      '
      'fianco1
        HW = val(GetInfo(SEZIONE, "HW(1)", PercorsoINI))
        AlfaDeg = val(GetInfo(SEZIONE, "AlfaDeg(1)", PercorsoINI))
        GammaDeg = val(GetInfo(SEZIONE, "GammaDeg(1)", PercorsoINI))
        Rtesta = val(GetInfo(SEZIONE, "Rtesta(1)", PercorsoINI))
        Rfondo = val(GetInfo(SEZIONE, "Rfondo(1)", PercorsoINI))
        Rsemit = val(GetInfo(SEZIONE, "Rsemit(1)", PercorsoINI))
        Hprot = val(GetInfo(SEZIONE, "Hprot(1)", PercorsoINI))
        OC = val(GetInfo(SEZIONE, "OC(1)", PercorsoINI))
        E63 = val(GetInfo(SEZIONE, "E63(1)", PercorsoINI))
        E82 = val(GetInfo(SEZIONE, "E82(1)", PercorsoINI))
        E83 = val(GetInfo(SEZIONE, "E83(1)", PercorsoINI))
        Hrastrt = val(GetInfo(SEZIONE, "Hrastrt(1)", PercorsoINI))
        AngRastrT = val(GetInfo(SEZIONE, "AngRastrT(1)", PercorsoINI))
      '
      'fianco2
      If (FlagFia = -1) Then
        HW = val(GetInfo(SEZIONE, "HW(2)", PercorsoINI))
        AlfaDeg = val(GetInfo(SEZIONE, "AlfaDeg(2)", PercorsoINI))
        GammaDeg = val(GetInfo(SEZIONE, "GammaDeg(2)", PercorsoINI))
        Rtesta = val(GetInfo(SEZIONE, "Rtesta(2)", PercorsoINI))
        Rfondo = val(GetInfo(SEZIONE, "Rfondo(2)", PercorsoINI))
        Rsemit = val(GetInfo(SEZIONE, "Rsemit(2)", PercorsoINI))
        Hprot = val(GetInfo(SEZIONE, "Hprot(2)", PercorsoINI))
        OC = val(GetInfo(SEZIONE, "OC(2)", PercorsoINI))
        E63 = val(GetInfo(SEZIONE, "E63(2)", PercorsoINI))
        E82 = val(GetInfo(SEZIONE, "E82(2)", PercorsoINI))
        E83 = val(GetInfo(SEZIONE, "E83(2)", PercorsoINI))
        Hrastrt = val(GetInfo(SEZIONE, "Hrastrt(2)", PercorsoINI))
        AngRastrT = val(GetInfo(SEZIONE, "AngRastrT(2)", PercorsoINI))
      End If
      '
      Yist = HW - Hkw
      Hrastrp = HW - E82
      ApRastp = FnRad(E83)
      Rastrp = Hrastrp * (Tan(ApRastp) - Tan(FnRad(AlfaDeg)))
      '
      NFil = val(GetInfo(SEZIONE, "Nfil", PercorsoINI))
      PasAxial = Abs(val(GetInfo(SEZIONE, "PasAxial", PercorsoINI)))
      PasHel = val(GetInfo(SEZIONE, "PasHel", PercorsoINI))
      DestCr = val(GetInfo(SEZIONE, "DestCr", PercorsoINI))
      Nscan = val(GetInfo(SEZIONE, "Nscan", PercorsoINI))
      Camma = val(GetInfo(SEZIONE, "Camma", PercorsoINI))
      SottIntaglio = val(GetInfo(SEZIONE, "SottIntaglio", PercorsoINI))
      Senso = val(GetInfo(SEZIONE, "Senso", PercorsoINI))
      DisTagliente = val(GetInfo(SEZIONE, "DisTagliente", PercorsoINI))
      '
      'DprimCr = DestCr - 2 * Hkw
      InFil = Acs(Mn * PG * NFil / PasAxial)
      InFilD = FnGrad(InFil)
      AngProjD = InFilD
      DprimCr = PasAxial / PG / Tan(InFil)
      AngProj = FnRad(AngProjD)
      '
    Case "LISTE"
      '
      'PROTUBERANZA: Pr$
      'k = Val(FINESTRA.List1(1).List(9))
      'If (k = 1) Then Pr$ = "O" Else Pr$ = "N"
      Pr$ = "N"
      Hprot = val(FINESTRA.List3(1).List(6))
      If (Hprot <> 0) Then
        Pr$ = "O"
      End If
      Hprot = val(FINESTRA.List3(2).List(6))
      If (Hprot <> 0) Then
        Pr$ = "O"
      End If
      '
      'SEMITOPPING: St$
      'k = Val(FINESTRA.List1(1).List(10))
      'If (k = 1) Then St$ = "O" Else St$ = "N"
      Hr = val(FINESTRA.List1(1).List(14))
      If (Hr <> 0) Then
        St$ = "O"
      Else
        St$ = "N"
      End If
      '
      Tp$ = "O"
      '
      'RASTREMAZIONE DI PIEDE: Rastp$
      'k = Val(FINESTRA.List1(1).List(11))
      'If (k = 1) Then Rastp$ = "O" Else Rastp$ = "N"
      Rastp$ = "N"
      E82 = val(FINESTRA.List3(1).List(9))
      If (E82 <> 0) Then
        Rastp$ = "O"
      End If
      E82 = val(FINESTRA.List3(2).List(9))
      If (E82 <> 0) Then
        Rastp$ = "O"
      End If
      '
      'RASTREMAZIONE DI TESTA: Rastt$
      'k = Val(FINESTRA.List1(1).List(12))
      'If (k = 1) Then Rastt$ = "O" Else Rastt$ = "N"
      Rastt$ = "N"
      Hrastrt = val(FINESTRA.List3(1).List(11))
      If (Hrastrt <> 0) Then
        Rastt$ = "O"
      End If
      Hrastrt = val(FINESTRA.List3(2).List(11))
      If (Hrastrt <> 0) Then
        Rastt$ = "O"
      End If
      '
      'SONO SOLO NUMERI REALI
      Mn = val(FINESTRA.List1(1).List(11))
      Hkw = val(FINESTRA.List1(1).List(12))
      'SPESSORE MOLA SUL PRIMITIVO --> VANO CREATORE
      Sw = val(FINESTRA.List1(1).List(13))
      Sw = Mn * PG - Sw 'SPESSORE EFFETTIVO CREATORE
      Hr = val(FINESTRA.List1(1).List(14))
      '
      'fianco1
        HW = val(FINESTRA.List3(1).List(0))
        AlfaDeg = val(FINESTRA.List3(1).List(1))
        GammaDeg = val(FINESTRA.List3(1).List(2))
        Rtesta = val(FINESTRA.List3(1).List(3))
        Rfondo = val(FINESTRA.List3(1).List(4))
        Rsemit = val(FINESTRA.List3(1).List(5))
        Hprot = val(FINESTRA.List3(1).List(6))
        OC = val(FINESTRA.List3(1).List(7))
        E63 = val(FINESTRA.List3(1).List(8))
        E82 = val(FINESTRA.List3(1).List(9))
        E83 = val(FINESTRA.List3(1).List(10))
        Hrastrt = val(FINESTRA.List3(1).List(11))
        AngRastrT = val(FINESTRA.List3(1).List(12))
      '
      'fianco2
      If FlagFia = -1 Then
        HW = val(FINESTRA.List3(2).List(0))
        AlfaDeg = val(FINESTRA.List3(2).List(1))
        GammaDeg = val(FINESTRA.List3(2).List(2))
        Rtesta = val(FINESTRA.List3(2).List(3))
        Rfondo = val(FINESTRA.List3(2).List(4))
        Rsemit = val(FINESTRA.List3(2).List(5))
        Hprot = val(FINESTRA.List3(2).List(6))
        OC = val(FINESTRA.List3(2).List(7))
        E63 = val(FINESTRA.List3(2).List(8))
        E82 = val(FINESTRA.List3(2).List(9))
        E83 = val(FINESTRA.List3(2).List(10))
        Hrastrt = val(FINESTRA.List3(2).List(11))
        AngRastrT = val(FINESTRA.List3(2).List(12))
      End If
      '
      Yist = HW - Hkw
      Hrastrp = HW - E82
      ApRastp = FnRad(E83)
      Rastrp = Hrastrp * (Tan(ApRastp) - Tan(FnRad(AlfaDeg)))
      '
      NFil = val(FINESTRA.List1(1).List(0))
      PasAxial = Abs(val(FINESTRA.List1(1).List(1)))
      PasHel = val(FINESTRA.List1(1).List(2))
      DestCr = val(FINESTRA.List1(1).List(3))
      Nscan = val(FINESTRA.List1(1).List(4))
      Camma = val(FINESTRA.List1(1).List(5))
      SottIntaglio = val(FINESTRA.List1(1).List(6))
      Senso = val(FINESTRA.List1(1).List(7))
      DisTagliente = val(FINESTRA.List1(1).List(8))
      '
      'DprimCr = DestCr - 2 * Hkw
      InFil = Acs(Mn * PG * NFil / PasAxial)
      InFilD = FnGrad(InFil)
      AngProjD = InFilD
      DprimCr = PasAxial / PG / Tan(InFil)
      AngProj = FnRad(AngProjD)
      '
  End Select
  '
  stmp = ""
  stmp = stmp & "ET$= " & Et$ & Chr(13)
  stmp = stmp & "PR$= " & Pr$ & Chr(13)
  stmp = stmp & "ST$= " & St$ & Chr(13)
  stmp = stmp & "TP$= " & Tp$ & Chr(13)
  stmp = stmp & "RASTp$= " & Rastp$ & Chr(13)
  stmp = stmp & "TASTt$= " & Rastt$ & Chr(13)
  stmp = stmp & Chr(13)
  stmp = stmp & "Mn= " & Mn & Chr(13)
  stmp = stmp & "HKW " & Hkw & Chr(13)
  stmp = stmp & "SW= " & Sw & Chr(13)
  stmp = stmp & "HR= " & Hr & Chr(13)
  stmp = stmp & Chr(13)
  stmp = stmp & "HW= " & HW & Chr(13)
  stmp = stmp & "AlfaDeg= " & AlfaDeg & Chr(13)
  stmp = stmp & "GammaDeg= " & GammaDeg & Chr(13)
  stmp = stmp & "Rtesta= " & Rtesta & Chr(13)
  stmp = stmp & "Rfondo= " & Rfondo & Chr(13)
  stmp = stmp & "Rsemit= " & Rsemit & Chr(13)
  stmp = stmp & "Hprot= " & Hprot & Chr(13)
  stmp = stmp & "OC= " & OC & Chr(13)
  stmp = stmp & "E63= " & E63 & Chr(13)
  stmp = stmp & "Hrastrt= " & Hrastrt & Chr(13)
  stmp = stmp & "AngRastrT= " & AngRastrT & Chr(13)
  stmp = stmp & "E82= " & E82 & Chr(13)
  stmp = stmp & "E83= " & E83 & Chr(13)
  stmp = stmp & Chr(13)
  stmp = stmp & "Nfil= " & NFil & Chr(13)
  stmp = stmp & "PasAxial= " & PasAxial & Chr(13)
  stmp = stmp & "PasHel= " & PasHel & Chr(13)
  stmp = stmp & "DestCr= " & DestCr & Chr(13)
  stmp = stmp & "Nscan= " & Nscan & Chr(13)
  stmp = stmp & "Camma= " & Camma & Chr(13)
  stmp = stmp & "SottIntaglio =" & SottIntaglio & Chr(13)
  stmp = stmp & "Senso= " & Senso & Chr(13)
  stmp = stmp & "DisTagliente= " & DisTagliente & Chr(13)
  'MsgBox stmp
  '
Exit Sub

errLETTURA_PROFILO_CREATORE:
  WRITE_DIALOG "SUB LETTURA_PROFILO_CREATORE ERROR " & Err
  Resume Next
  
End Sub

Sub CONTROLLO_CREATORI_CALCOLO_DATI(ByVal FINESTRA As Form, ByVal TIPO As String)
'
Dim i    As Integer
Dim Atmp As String * 255
Dim stmp As String
Dim riga As String
Dim ret  As Integer
Dim rsp  As Integer
Dim scmd As String
'
Dim DIRETTORIO_WKStmp  As String
Dim MODALITA           As String
'
On Error GoTo errCONTROLLO_CREATORI_CALCOLO_DATI
  '
  'ABILITAZIONE DELLA SELEZIONE DELLA PAGINA DEL CONTROLLO IN MACCHINA
  '
  'LEGGO IL PERCORSO PER IL DIRETTORIO DEI CONTROLLI
  riga = GetInfo("CREATORI_STANDARD", "PathMIS", Path_LAVORAZIONE_INI): riga = UCase$(Trim$(riga))
  If (Len(riga) <= 0) Then Exit Sub
  '
  flagErrore = False
  '
  For Fianco = 1 To 2
    '
    FlagFia = 3 - Fianco * 2 ' 1=F1 , 2=F2
    '
    If (flagErrore = False) Then Call LETTURA_PROFILO_CREATORE(FINESTRA, TIPO)
    If (flagErrore = False) Then Call CalcoloCremDIB
    '
    If (flagErrore = False) Then
      '
      Call CalcPuntiCrem
      For i = 1 To NbpTotal
        Call CalcUnPuntoAxial(Xfm(Fianco, i), RFM(Fianco, i), ApFm(Fianco, i), Xfma(Fianco, i), Rfma(Fianco, i), ApFma(Fianco, i))
      Next i
      '
    End If
    '
  Next Fianco
  '
  If (flagErrore = False) Then Call CONTROLLO_CREATORI_CREAZIONE_SOTTOPROGRAMMI
  '
  If (flagErrore = False) Then
    '
    'VISUALIZZAZIONE DEL PROFILO SOLO SULLA FINESTRA DEI PARAMETRI
    If (UCase$(FINESTRA.Name) = "OEM1") Then Call VISUALIZZAZIONE_PROFILO_CREATORE(0, "N")
    '
    MODALITA = GetInfo("Configurazione", "MODALITA", PathFILEINI)
    MODALITA = UCase$(Trim$(MODALITA))
    '
    If (MODALITA <> "OFF-LINE") Then
      '
      DIRETTORIO_WKStmp = LEGGI_DIRETTORIO_WKS("CREATORI_STANDARD")
      '
      ReDim DIRETTORIO_WKS(0)
      ReDim SOTTOPROGRAMMI(0)
      '
      ReDim Preserve DIRETTORIO_WKS(1): DIRETTORIO_WKS(1) = DIRETTORIO_WKStmp
      ReDim Preserve SOTTOPROGRAMMI(1): SOTTOPROGRAMMI(1) = GetInfo("CREATORI_STANDARD", "SPF(1)", Path_LAVORAZIONE_INI)
      ReDim Preserve DIRETTORIO_WKS(2): DIRETTORIO_WKS(2) = DIRETTORIO_WKStmp
      ReDim Preserve SOTTOPROGRAMMI(2): SOTTOPROGRAMMI(2) = GetInfo("CREATORI_STANDARD", "SPF(2)", Path_LAVORAZIONE_INI)
      ReDim Preserve DIRETTORIO_WKS(3): DIRETTORIO_WKS(3) = DIRETTORIO_WKStmp
      ReDim Preserve SOTTOPROGRAMMI(3): SOTTOPROGRAMMI(3) = GetInfo("CREATORI_STANDARD", "SPF(3)", Path_LAVORAZIONE_INI)
      '
      Set Session = GetObject("@SinHMIMCDomain.MCDomain")
      For i = 1 To 3
        WRITE_DIALOG SOTTOPROGRAMMI(i) & ".SPF --> CNC"
        Session.CopyNC g_chOemPATH & "\" & SOTTOPROGRAMMI(i) & ".SPF", "/NC/WKS.DIR/" & DIRETTORIO_WKS(i) & ".WPD/" & SOTTOPROGRAMMI(i) & ".SPF", MCDOMAIN_COPY_NC
      Next i
      Set Session = Nothing
      '
    End If
    '
  End If
  '
Exit Sub

errCONTROLLO_CREATORI_CALCOLO_DATI:
  WRITE_DIALOG "SUB CONTROLLO_CREATORI_CALCOLO_DATI ERROR " & Err
  Resume Next
  
End Sub

Sub VISUALIZZAZIONE_PROFILO_CREATORE(Scala As Double, ByVal SiStampa As String)
'
Dim ZeroPezzoX    As Single
Dim ZeroPezzoY    As Single
Dim pXX()         As Single
Dim pYY()         As Single
Dim i             As Integer
Dim j             As Integer
Dim pX1           As Single
Dim pX2           As Single
Dim pY1           As Single
Dim pY2           As Single
Dim PicW          As Single
Dim PicH          As Single
Dim ScalaY        As Single
Dim ScalaX        As Single
Dim stmp          As String
Dim Atmp          As String * 255
Dim TipoCarattere As String
Dim iFianco       As Integer
Dim Np            As Integer
'
On Error GoTo errVISUALIZZAZIONE_PROFILO_CREATORE
  '
  OEMX.OEM1frameCalcolo.Caption = ""
  '
  TipoCarattere = GetInfo("DIS0", "TextFont", Path_LAVORAZIONE_INI)
  If (LINGUA = "CH") Then TipoCarattere = "MS Song"
  If (LINGUA = "RU") Then TipoCarattere = "Arial Cyr"
  If (Len(TipoCarattere) <= 0) Then TipoCarattere = "Arial"
  '
  OEMX.lblINP(0).Visible = True
  OEMX.txtINP(0).Visible = True
  '
  OEMX.OEM1lblOX.Visible = True
  OEMX.OEM1txtOX.Visible = True
  '
  OEMX.OEM1lblOY.Visible = True
  OEMX.OEM1txtOY.Visible = True
  '
  stmp = GetInfo("DIS0", "PicW", Path_LAVORAZIONE_INI)
  If val(stmp) > 0 Then
    OEMX.OEM1PicCALCOLO.ScaleWidth = val(stmp)
  Else
    OEMX.OEM1PicCALCOLO.ScaleWidth = 190
  End If
  stmp = GetInfo("DIS0", "PicH", Path_LAVORAZIONE_INI)
  If val(stmp) > 0 Then
    OEMX.OEM1PicCALCOLO.ScaleHeight = val(stmp)
  Else
    OEMX.OEM1PicCALCOLO.ScaleHeight = 95
  End If
  '
  PicH = OEMX.OEM1PicCALCOLO.ScaleHeight
  PicW = OEMX.OEM1PicCALCOLO.ScaleWidth
  '
  stmp = GetInfo("DIS0", "ZeroPezzoX", Path_LAVORAZIONE_INI)
  ZeroPezzoX = val(stmp)
  stmp = GetInfo("DIS0", "ZeroPezzoY", Path_LAVORAZIONE_INI)
  ZeroPezzoY = val(stmp)
  '
  'VISUALIZZO LA SCALA
  i = LoadString(g_hLanguageLibHandle, 1000, Atmp, 255)
  If (i > 0) Then OEMX.lblINP(0).Caption = Left$(Atmp, i)
  i = LoadString(g_hLanguageLibHandle, 1099, Atmp, 255)
  If (i > 0) Then OEMX.OEM1frameCalcolo.Caption = Left$(Atmp, i)
  '
  If (SiStampa = "Y") Then
    '
    'OEMX.PicLogo.Picture = LoadPicture(g_chOemPATH & "\" & "Logo2.bmp")
    Printer.ScaleMode = 6
    For i = 1 To 2
      Printer.FontName = TipoCarattere
      Printer.FontSize = 10
    Next i
    WRITE_DIALOG OEMX.OEM1frameCalcolo.Caption & " Printing on " & Printer.DeviceName
    '
  Else
    '
    OEMX.OEM1PicCALCOLO.Cls
    WRITE_DIALOG ""
    '
  End If
  '
  'CALCOLO SCALA
  If (Scala <= 0) Then
    ScalaY = PicH / (Rfma(1, 1) - Rfma(1, NbpTotal) + 0.2)
    ScalaX = PicW / (2 * Xfma(1, NbpTotal) + 0.2)
    Scala = ScalaX
    If (ScalaX > ScalaY) Then Scala = ScalaY
  End If
  '
  If (Scala >= 1) Then Scala = Int(Scala)
  '
  OEMX.txtINP(0).Text = frmt(Scala, 4)
  '
  ScalaY = Scala
  ScalaX = Scala
  '
  'ASSE ORIZZONTALE
  pX1 = 0
  pY1 = PicH
  pX2 = PicW
  pY2 = PicH
  'Traccio la linea
  If (SiStampa = "Y") Then
    If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
      Printer.Line (pX1, pY1)-(pX2, pY2)
    End If
  Else
    If (pY1 <= OEMX.OEM1PicCALCOLO.ScaleHeight) And (pX1 <= OEMX.OEM1PicCALCOLO.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
      OEMX.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2)
    End If
  End If
  '
  'ASSE VERTICALE
  pX1 = PicW / 2
  pY1 = 0
  pX2 = PicW / 2
  pY2 = PicH
  'Traccio la linea
  If (SiStampa = "Y") Then
    If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
      Printer.Line (pX1, pY1)-(pX2, pY2)
    End If
  Else
    If (pY1 <= OEMX.OEM1PicCALCOLO.ScaleHeight) And (pX1 <= OEMX.OEM1PicCALCOLO.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
      OEMX.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2)
    End If
  End If
  '
  stmp = " " & OEMX.lblINP(0).Caption & " --> " & frmt$(Scala, 4) & " : 1"
  If (SiStampa = "Y") Then
    Printer.CurrentX = 0
    Printer.CurrentY = 0
    Printer.Print stmp
  Else
    OEMX.OEM1PicCALCOLO.CurrentX = 0
    OEMX.OEM1PicCALCOLO.CurrentY = 0
    OEMX.OEM1PicCALCOLO.Print stmp
  End If
  '
  stmp = " " & Et$ & " "
  If (SiStampa = "Y") Then
    Printer.CurrentX = PicW - Printer.TextWidth(stmp)
    Printer.CurrentY = 0
    Printer.Print stmp
  Else
    OEMX.OEM1PicCALCOLO.CurrentX = PicW - OEMX.OEM1PicCALCOLO.TextWidth(stmp)
    OEMX.OEM1PicCALCOLO.CurrentY = 0
    OEMX.OEM1PicCALCOLO.Print stmp
  End If
  '
  'CAMBIO NOMI
  Np = NbpTotal
  ReDim pXX(Np): ReDim pYY(Np)
  For iFianco = 1 To 2
    For i = 1 To Np
      pXX(i) = -(iFianco - 1.5) / 0.5 * Xfma(iFianco, i): pYY(i) = Rfma(iFianco, i) - Rfma(iFianco, Np)
    Next i
    For i = 1 To Np - 1
      pX1 = pXX(i)
      pY1 = pYY(i) + 0.1
      pX2 = pXX(i + 1)
      pY2 = pYY(i + 1) + 0.1
      pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
      pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH
      pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
      pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH
      'Traccio la linea
      If (SiStampa = "Y") Then
        If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          Printer.Line (pX1, pY1)-(pX2, pY2)
        End If
      Else
        If (pY1 <= OEMX.OEM1PicCALCOLO.ScaleHeight) And (pX1 <= OEMX.OEM1PicCALCOLO.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
          OEMX.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2)
        End If
      End If
    Next i
    stmp = " F" & Format$(iFianco, "0") & " "
    If (SiStampa = "Y") Then
      If (pY2 <= Printer.ScaleHeight) And (pX2 <= Printer.ScaleWidth) And (pX2 >= 0) And (pY2 >= 0) Then
        Printer.CurrentX = pX2 - Printer.TextWidth(stmp) / 2
        Printer.CurrentY = pY2 - Printer.TextHeight(stmp)
        Printer.Print stmp
      End If
    Else
      If (pY2 <= OEMX.OEM1PicCALCOLO.ScaleHeight) And (pX2 <= OEMX.OEM1PicCALCOLO.ScaleWidth) And (pX2 >= 0) And (pY2 >= 0) Then
        OEMX.OEM1PicCALCOLO.CurrentX = pX2 - OEMX.OEM1PicCALCOLO.TextWidth(stmp) / 2
        OEMX.OEM1PicCALCOLO.CurrentY = pY2 - OEMX.OEM1PicCALCOLO.TextHeight(stmp)
        OEMX.OEM1PicCALCOLO.Print stmp
      End If
    End If
    '
    If (SiStampa = "Y") Then
      '
      'evidenzia punti separazione enti
      Printer.FillStyle = 0
      Printer.FillColor = QBColor(12)
      j = NInTesta: GoSub cerchioPRN
      j = NFinTesta: GoSub cerchioPRN
      j = NInFia: GoSub cerchioPRN
      j = NFinFia: GoSub cerchioPRN
      j = NFinRf: GoSub cerchioPRN
      j = NbpTotal: GoSub cerchioPRN
      If NInSt > 0 Then
        j = NInSt: GoSub cerchioPRN
        j = NFinSt: GoSub cerchioPRN
      End If
      If NInRastT > 0 Then
        j = NInRastT: GoSub cerchioPRN
        j = NFinRastT: GoSub cerchioPRN
      End If
      If NInRastP > 0 Then
        j = NInRastP: GoSub cerchioPRN
        j = NFinRastP: GoSub cerchioPRN
      End If
      Printer.FillStyle = 1
      Printer.FillColor = QBColor(0)
      '
    Else
      '
      'evidenzia punti separazione enti
      OEMX.OEM1PicCALCOLO.FillStyle = 0
      OEMX.OEM1PicCALCOLO.FillColor = QBColor(12)
      j = NInTesta: GoSub Cerchio
      j = NFinTesta: GoSub Cerchio
      j = NInFia: GoSub Cerchio
      j = NFinFia: GoSub Cerchio
      j = NFinRf: GoSub Cerchio
      j = NbpTotal: GoSub Cerchio
      If NInSt > 0 Then
        j = NInSt: GoSub Cerchio
        j = NFinSt: GoSub Cerchio
      End If
      If NInRastT > 0 Then
        j = NInRastT: GoSub Cerchio
        j = NFinRastT: GoSub Cerchio
      End If
      If NInRastP > 0 Then
        j = NInRastP: GoSub Cerchio
        j = NFinRastP: GoSub Cerchio
      End If
      OEMX.OEM1PicCALCOLO.FillStyle = 1
      OEMX.OEM1PicCALCOLO.FillColor = QBColor(0)
      '
    End If
    '
  Next iFianco
  '
  If (SiStampa = "Y") Then Printer.EndDoc
  '
Exit Sub

Cerchio:
  pX1 = pXX(j)
  pY1 = pYY(j) + 0.1
  pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
  pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH
  OEMX.OEM1PicCALCOLO.Circle (pX1, pY1), 0.06 * Scala, QBColor(12)
Return

cerchioPRN:
  pX1 = pXX(j)
  pY1 = pYY(j) + 0.1
  pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
  pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH
  Printer.Circle (pX1, pY1), 0.06 * Scala, QBColor(12)
Return

errVISUALIZZAZIONE_PROFILO_CREATORE:
  WRITE_DIALOG "SUB VISUALIZZAZIONE_PROFILO_CREATORE ERROR " & Err
  Resume Next

End Sub
