Attribute VB_Name = "Disegni"
Option Explicit

Private Declare Function Polygon Lib "gdi32" (ByVal hdc As Long, lpPoint As Pointapi, ByVal nCount As Long) As Long

Private Declare Function ExtFloodFill Lib "gdi32" (ByVal hdc As Long, _
    ByVal X As Long, ByVal Y As Long, ByVal colorCode As Long, _
    ByVal fillType As Long) As Long

Private Declare Sub GetSystemTime Lib "Kernel32" (lpSystemTime As SYSTEMTIME)

Const FLOODFILLBORDER = 0
Const FLOODFILLSURFACE = 1

Global Connesso As Boolean
Global ERig     As Integer
Global ECol     As Integer

Public CurrVis As Integer
'
Private Type SYSTEMTIME
 wYear As Integer
 wMonth As Integer
 wDayOfWeek As Integer
 wDay As Integer
 wHour As Integer
 wMinute As Integer
 wSecond As Integer
 wMilliseconds As Integer
End Type
Private BIAS_MOLAVITE_TIMER As Long
'
Private Type Pointapi
  X As Long
  Y As Long
End Type

Private Type ParPic
  Xc            As Double
  YC            As Double
  Scala         As Double
  ScalaMargine  As Double
  MaxX          As Double
  MinX          As Double
  MaxY          As Double
  MinY          As Double
  SpostX        As Double
  SpostY        As Double
  '
  MargineSxPic  As Double
  MargineDxPic  As Double
  '
  RegioneX1     As Double
  RegioneX2     As Double
  RegioneY1     As Double
  RegioneY2     As Double
  AllBlack      As Boolean
  Printing      As Boolean
  Pic           As Object 'PictureBox
  Su            As SuOEM
  NInfo         As Integer
  NInfoAll(4)   As Integer
  Allineamento  As Integer ' 0=Centro 1=Alto 2=Basso
End Type

Private Type DatiAppoggio
  LdevEap         As Variant
  LdevIni         As Variant
  NpuntiMola      As Variant
  DB1             As Variant
  Raggio()        As Variant
  Eb1             As Variant
  Z1              As Variant
  ENTRX           As Variant
  IncMola         As Variant
  PasHelImpostato As Variant
  Xmol()          As Variant
  Rmol()          As Variant
  Lmol()          As Variant
  XLmol()         As Variant
  Tmol()          As Variant
  LcontattoMola   As Variant
  LContMax        As Variant
  LContMin        As Variant
End Type

Private Type DatiGlobali
  DB        As Double
  DP        As Double
  LungCont  As Double
  Z         As Integer
  Mn        As Double
  ALFA      As Double
  Beta      As Double
  ALFAt     As Double
  DI        As Double
  DE        As Double
  SON       As Double
  SONt      As Double
  RT        As Double   'Raggio di testa
  RF        As Double   'Raggio di fondo
  Eb        As Double
  DIni      As Double   'Diametro inizio rettifica
  DatiOK    As Boolean
  ValTIFDIA As Integer
  ZonaF1()  As Double   'ZonaFx(0) = SAP .... ZonaFx(Ubound(ZonaFx))= EAP
  ZonaF2()  As Double   'ZonaFx(0) = SAP .... ZonaFx(Ubound(ZonaFx))= EAP
  DistPtEv  As Double
  NumPtEvF1 As Integer
  NumPtEvF2 As Integer
  WbQ       As Double
  WbN       As Integer
  RulQ      As Double
  RulD      As Double
  PB        As Double
  DefSpe    As Integer
  FattoreX  As Double
  BetaF1    As Double
  BetaF2    As Double
  SAP       As Double
  EAP       As Double
End Type

Private Type DatiMola
  X() As Double
  Y() As Double
End Type

Private PI         As Double
Private PG         As Double
Private PAR        As ParPic
Private DGlob      As DatiGlobali
Public ParametriOK As Boolean

Public Sub CalcolaBombConXC(BombX As Double, BombC As Double, ConX As Double, ConC As Double, BombaturaF1 As Double, BombaturaF2 As Double, Conf1 As Double, ConF2 As Double, MyBeta As Double)

On Error Resume Next
 
 'Bombatura con asse Radiale
  BombX = (BombaturaF1 + BombaturaF2) / 2
 'Bombatura con asse Pezzo
  BombC = (BombaturaF1 - BombaturaF2) / 2
  
 'Agg.26.01.13
 'Conicit� con asse Radiale
  ConX = (Conf1 + ConF2) / 2
 'Conicit� con asse Pezzo
  ConC = (Conf1 - ConF2) / 2
  

End Sub

Private Sub InizializzaVariabili()
'
On Error Resume Next
  '
  PG = 4 * Atn(1)
  PI = 4 * Atn(1)
  Set PAR.Pic = PictureDisegni
  Set PAR.Su = OEMX.SuOEM1
  Select Case LINGUA
  Case "CH"
    PAR.Pic.Font.Name = "Ms Song"
  Case "RU"
    PAR.Pic.Font.Name = "Arial Cyr"
    PAR.Pic.Font.Charset = 204
  Case Else
    PAR.Pic.Font.Name = "Arial"
  End Select
  'PAR.Pic.Font.Italic = True
  PAR.Pic.FontSize = 8
  PAR.ScalaMargine = 1
  '
End Sub

Private Function inv(Aux)
'
On Error Resume Next
  '
  inv = Tan(Aux) - Aux
  '
End Function

Private Function Acs(Aux)
'
Dim Aux1, A$
Dim PI As Double
'
On Error Resume Next
  '
  PI = 4 * Atn(1)
   Aux1 = (1 - Aux * Aux)
   If Aux1 < 0 Then
      A$ = "Coseno > 1 : Il programma si ferma : Controllare i dati"
      A$ = A$ & vbCrLf & " ( non dovrebbe succedere )"
      'Debug.Print a$
'      MsgBox A$, vbCritical
'      If Not bDDL And Not bPRG Then
'         Open "C:\SuFwAperto.txt" For Output As #5: Print #5, "C": Close #5
'      End If
      'End
   Else
      If Aux = 0 Then
         Acs = PI / 2
      Else
         If Aux > 0 Then
            Acs = Atn(Sqr(1 - Aux * Aux) / Aux)
         Else
            Aux = Abs(Aux)
            Acs = Atn(Aux / Sqr(1 - Aux * Aux)) + 1.570796
         End If
      End If
   End If
   
End Function

Private Function Asn(Aux)

Dim PI As Double
Dim Aux1
'
On Error Resume Next
  '
  PI = 4 * Atn(1)
  Aux1 = (1 - Aux * Aux)
  If Aux1 < 0 Then
'     MsgBox Parola("2", TabUtente$), vbCritical ' "Seno > 1 : Il programma si ferma : Controllare i dati"
'     If Not bDDL And Not bPRG Then
'        Open "C:\SuFwAperto.txt" For Output As #5: Print #5, "C": Close #5
'     End If
'     End
  Else
     If Aux = 1 Then
        Asn = PI / 2
     Else
        Asn = Atn(Aux / Sqr(Aux1))
     End If
  End If

End Function

Sub CalcoloParametri_INGRANAGGI_INTERNI()
'
Dim tmp     As Double
Dim i       As Integer
Dim ERRORE  As Boolean
'
On Error GoTo errCalcoloParametri_INGRANAGGI_INTERNI
  '
  Call InizializzaVariabili
  '
  ERRORE = False
  ParametriOK = True
  '
    If Lp("iAngoloElica") < 0 Then
      Call SP("iSensoElica", -1)
      Call SP("iAngoloElica", Abs(Lp("iAngoloElica")))
    End If
    Call SP("R[41]", Lp("iAngoloElica") * Lp("iSensoElica"))
    DGlob.Z = Lp("R[5]")
    DGlob.Mn = Lp("R[42]")
    DGlob.ALFA = Lp("R[40]") * PG / 180
    DGlob.DI = Lp("R[38]")   'Diametro Interno
    DGlob.DE = Lp("R[45]")   'Diametro Esterno
    DGlob.SON = Lp("R[46]")
    DGlob.DefSpe = Lp("iDefSp")
    DGlob.RulQ = Lp("R[49]")
    DGlob.RulD = Lp("R[50]")
    DGlob.Beta = Lp("R[41]") * PG / 180
    DGlob.RT = Lp("R[51]")
    DGlob.RF = Lp("R[52]")
    DGlob.DistPtEv = Lp("R[194]")
    DGlob.ALFAt = Atn(Tan(DGlob.ALFA) / Cos(DGlob.Beta))
    DGlob.DP = DGlob.Mn * DGlob.Z / Cos(DGlob.Beta) 'Diametro Primitivo
    DGlob.DB = DGlob.DP * Cos(DGlob.ALFAt)          'Diametro di Base
    Call SP("Dp", Format(DGlob.DP, "0.####"))
    Call SP("Db", Format(DGlob.DB, "0.####"))
    Dim Betab As Double
    If (DGlob.SON = 0) And (DGlob.DefSpe = 1) Then
      DGlob.SON = Round((DGlob.Mn * PG) / 2, 3)
      Call SP("R[46]", DGlob.SON)
    End If
    Select Case DGlob.DefSpe
    Case 1
        Call SP("R[49]", 0)
        DGlob.RulQ = 0
    Case 2
        Call SP("R[46]", 0)
        If (DGlob.RulQ <> 0) And (DGlob.RulD <> 0) And (DGlob.RulD <> DGlob.RulQ) Then
          Betab = Atn(Tan(DGlob.Beta) * Cos(DGlob.ALFAt)) 'Elica di base
          DGlob.SON = QUOTA_RULLI_SON(DGlob.Z, DGlob.DB / 2, DGlob.ALFAt, Betab, DGlob.DP / 2, DGlob.RulQ, DGlob.RulD)
          DGlob.SON = (DGlob.DP * PG / DGlob.Z - DGlob.SON) * Cos(DGlob.Beta)
        End If
    End Select
    'Calcolo altri parametri
    DGlob.SONt = DGlob.SON / Cos(DGlob.Beta)
    DGlob.Eb = DGlob.DB * ((DGlob.SONt / DGlob.DP) + inv(DGlob.ALFAt))
    'Se l'inclinazione della mola in rettifica � 0
    'viene impostata uguale all'angolo elica.
    If Lp("R[258]") = 0 Then
      Call SP("R[258]", Abs(Lp("iAngoloElica")))
    End If
    '
    ReDim DGlob.ZonaF1(0)
    ReDim DGlob.ZonaF2(0)
    '
    DGlob.ZonaF1(0) = DIA2TIF(Lp("R[44]"), DGlob.DB)    'SAP F1
    DGlob.ZonaF2(0) = DIA2TIF(Lp("SAPF2_G"), DGlob.DB)  'SAP F2
    'DGlob.ZonaF1(0) = DIA2TIF(lp("R[43]"), DGlob.DB)    'EAP F1
    'DGlob.ZonaF2(0) = DIA2TIF(lp("EAPF2_G"), DGlob.DB)  'EAP F2
    DGlob.ValTIFDIA = Lp("R[901]")
    '
'    'Se SAP � sotto il DB metto il diametro interno RaggioUtensile (Se Beta=0) + DistanzaPunti
'    '(solo come valore di default)
'
'    If (DGlob.ZonaF1(0) = 0) Then
'      If DGlob.Beta = 0 Then
'        tmp = LEP("$TC_DP6[1,1]")
'        tmp = tmp + DGlob.DistPtEv
'      Else
'         tmp = DGlob.DistPtEv
'      End If
'      DGlob.ZonaF1(0) = tmp
'      call sp( "R[44]", TIF2DIA(DGlob.ZonaF1(0), DGlob.DB)
'    End If
'
'    If (DGlob.ZonaF2(0) = 0) Then
'      If DGlob.Beta = 0 Then
'        tmp = LEP("$TC_DP6[1,2]")
'        tmp = tmp + DGlob.DistPtEv
'      Else
'         tmp = DGlob.DistPtEv
'      End If
'
'      DGlob.ZonaF2(0) = tmp
'      call sp( "SAPF2_G", TIF2DIA(DGlob.ZonaF2(0), DGlob.DB)
'    End If
    '
    For i = 0 To 2
      tmp = Lp("R[" & 270 + i * 3 & "]") 'Fine Zona i+1  ( R270 R273 R276 )
      If tmp <> 0 Then
        'Eventuale conversione Dia -> Tif
        If DGlob.ValTIFDIA = 2 Then
          tmp = DIA2TIF(tmp, DGlob.DB)
        End If
        ReDim Preserve DGlob.ZonaF1(UBound(DGlob.ZonaF1) + 1)
        If tmp > DIA2TIF(DGlob.DE, DGlob.DB) Then
          DGlob.ZonaF1(UBound(DGlob.ZonaF1)) = DIA2TIF(DGlob.DE, DGlob.DB)
          Exit For
        Else
          DGlob.ZonaF1(UBound(DGlob.ZonaF1)) = tmp
        End If
      Else
        If UBound(DGlob.ZonaF1) = 0 Then
          ReDim Preserve DGlob.ZonaF1(UBound(DGlob.ZonaF1) + 1)
          DGlob.ZonaF1(UBound(DGlob.ZonaF1)) = DIA2TIF(DGlob.DE, DGlob.DB)
        End If
        Exit For
      End If
    Next i
    For i = 0 To 2
      tmp = Lp("R[" & 285 + i * 3 & "]") 'Fine Zona i+1  ( R285 R288 R291 )
      If tmp <> 0 Then
        'Eventuale conversione Dia -> Tif
        If DGlob.ValTIFDIA = 2 Then
          tmp = DIA2TIF(tmp, DGlob.DB)
        End If
        ReDim Preserve DGlob.ZonaF2(UBound(DGlob.ZonaF2) + 1)
        If tmp > DIA2TIF(DGlob.DE, DGlob.DB) Then
          DGlob.ZonaF2(UBound(DGlob.ZonaF2)) = DIA2TIF(DGlob.DE, DGlob.DB)
          Exit For
        Else
          DGlob.ZonaF2(UBound(DGlob.ZonaF2)) = tmp
        End If
      Else
        If UBound(DGlob.ZonaF2) = 0 Then
          ReDim Preserve DGlob.ZonaF2(UBound(DGlob.ZonaF2) + 1)
          DGlob.ZonaF2(UBound(DGlob.ZonaF2)) = DIA2TIF(DGlob.DE, DGlob.DB)
        End If
        Exit For
      End If
    Next i
    If DGlob.DistPtEv > 0 Then
      DGlob.NumPtEvF1 = Int((DGlob.ZonaF1(UBound(DGlob.ZonaF1)) - DGlob.ZonaF1(0)) / DGlob.DistPtEv) + 2
      DGlob.NumPtEvF2 = Int((DGlob.ZonaF2(UBound(DGlob.ZonaF2)) - DGlob.ZonaF2(0)) / DGlob.DistPtEv) + 2
    End If
    Dim Code
    
'    Rp = AM * Z / 2 / Cos(Beta)         'Raggio primitivo
'    Betab = Atn(Tan(Beta) * Cos(AlfaT)) 'Elica di base
'    Rb = Rp * Cos(AlfaT)                'Raggio di base
'    PB = (Rb * 2 * PG) / Z              'Passo di base
'    Db = 2 * Rb                         'Diametro di base
'    DP = 2 * Rp                         'Diametro primitivo

'    DApp.DB1 = DGlob.DB
'    DApp.Eb1 = DGlob.Eb
'    DApp.LcontattoMola = 0
'    DApp.LdevIni = DGlob.ZonaF1(0)
'    DApp.LdevEap = DGlob.ZonaF1(UBound(.ZonaF1))
'    DApp.NpuntiMola = DGlob.NumPtEvF1
'    DApp.Z1 = DGlob.Z
'    DApp.IncMola = lp("R[258]") * PG / 180
'    If DApp.IncMola = 0 Then DApp.IncMola = 0.00001
'
'    DApp.ENTRX = (DGlob.Di + lp("iDiaMola")) / 2
'    DApp.PasHelImpostato = DGlob.DP * PG / Tan(Abs(DGlob.Beta + 0.00001))
'
'
'    Call CalcoloPuntiMola(Code, DApp)
'
'    tmp = Abs(DApp.LContMax)
'    If Abs(DApp.LContMin) > tmp Then
'      tmp = Abs(DApp.LContMin)
'    End If
    '
On Error GoTo errCalcoloParametri_INGRANAGGI_INTERNI
    '
    If (Lp("iMetDiv") = 1) Then Call SP("R[130]", 0)
    
    Select Case Lp("iNZone")
      Case 1
        Call SP("Fascia2F1", 0)
        Call SP("Fascia2F2", 0)
        Call SP("Fascia3F1", 0)
        Call SP("Fascia3F2", 0)
      
        Call SP("Conic2F1", 0)
        Call SP("Conic2F2", 0)
        Call SP("Bomb2F1", 0)
        Call SP("Bomb2F2", 0)
        
        Call SP("Conic3F1", 0)
        Call SP("Conic3F2", 0)
        Call SP("Bomb3F1", 0)
        Call SP("Bomb3F2", 0)
  
      Case 2
        Call SP("Fascia3F1", 0)
        Call SP("Fascia3F2", 0)
        
        Call SP("Conic3F1", 0)
        Call SP("Conic3F2", 0)
        Call SP("Bomb3F1", 0)
        Call SP("Bomb3F2", 0)
      
      Case 3
        '
    End Select
    Call SP("FASCIATOTALE_G", Lp("Fascia1F1") + Lp("Fascia2F1") + Lp("Fascia3F1"))
  '
  Call CALCOLO_INGRANAGGIO_INTERNO(0, True)
  '
  tmp = 0
  '
On Error Resume Next
  '
  'Se non � possibile completare il calcolo perch� mancano dei parametri
  'Inizializzo l'extracorsa a 0 (tmp=0) e ignoro gli errori.
  tmp = Abs(CalcoloEsterni.LCont(1, 1))
  If tmp < Abs(CalcoloEsterni.LCont(1, CalcoloEsterni.Np)) Then
    tmp = Abs(CalcoloEsterni.LCont(1, CalcoloEsterni.Np))
  End If
  '
On Error GoTo errCalcoloParametri_INGRANAGGI_INTERNI
  '
  Call SP("iCORSAIN", Round(tmp + 0.05, 1))
  Call SP("iCORSAOUT", Round(tmp + 0.05, 1))
  '
  Call SP("CORSAIN", Lp("iCORSAIN") + Lp("iCORSAIN_CORR"))
  Call SP("CORSAOUT", Lp("iCORSAOUT") + Lp("iCORSAOUT_CORR"))
  '
On Error GoTo 0
Exit Sub

errCalcoloParametri_INGRANAGGI_INTERNI:
  If (Err.Number = 6) Or (Err.Number = 11) Then
    PAR.Pic.Cls
  Else
    MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Sub: CalcoloParametri_INGRANAGGI_INTERNI"
  End If
  
End Sub

Sub CalcoloParametri_ROTORI_ESTERNI()
'
Dim Div_T   As Double
Dim Div_Rm  As Double
Dim Div_hz  As Double
Dim Div_Rt  As Double
Dim tSpe    As Double
Dim ret     As Long
Dim stmp    As String
'
On Error Resume Next
  '
  If (ActualGroup = "INP1_ROTORI_ESTERNI") Then Exit Sub
  '
  Call InizializzaVariabili
  '
Dim PERCORSO    As String
Dim COORDINATE  As String
Dim RADICE      As String
Dim i           As Integer
Dim j           As Integer
  '
  'LETTURA DEL PERCORSO DI RIFERIMENTO E COORDINATE CLIENTE
  PERCORSO = LEP_STR("PERCORSO")
  PERCORSO = g_chOemPATH & "\" & PERCORSO
  'RICAVO IL NOME DEL ROTORE DAL PERCORSO
  COORDINATE = PERCORSO
  If (Len(COORDINATE) > 0) Then
    i = InStr(COORDINATE, "\")
    While (i <> 0)
      j = i
      i = InStr(i + 1, COORDINATE, "\")
    Wend
    'RICAVO IL NOME DEL ROTORE
    COORDINATE = Right$(COORDINATE, Len(COORDINATE) - j)
  End If
  'RICAVO IL PERCORSO DI LAVORO
  RADICE = PERCORSO
  i = InStr(RADICE, COORDINATE)
  RADICE = Left$(RADICE, i - 2)
  'CONTROLLO SE E' STATA DEFINITA LA TABELLA OEM0_CORRETTORI71 PER IL PROFILO RAB
  stmp = ""
  For j = 0 To UBound(Tabella1.Pagine)
    If (Tabella1.Pagine(j).Nomegruppo = "OEM0_CORRETTORI71") Then
      stmp = "TROVATO": Exit For
    End If
  Next j
  If (stmp = "") And (Dir$(RADICE & "\OEM0_CORRETTORI71.SPF") = "") Then
    stmp = ""
    stmp = stmp & Error(53) & Chr(13) & Chr(13)
    stmp = stmp & "FILE PROFILE POINTS HAVE NOT BEEN CREATED!!" & Chr(13) & Chr(13)
    MsgBox stmp, vbCritical, "ERROR: OEM0_CORRETTORI71.SPF"
    Exit Sub
  End If
  '
  Call SP("iCORSAIN", Round(MaxLcontRotori + 0.05, 1))
  Call SP("iCORSAOUT", Round(MaxLcontRotori + 0.05, 1))
  Call SP("CORSAIN", Lp("iCORSAIN") + Lp("iCORSAIN_CORR"))
  Call SP("CORSAOUT", Lp("iCORSAOUT") + Lp("iCORSAOUT_CORR"))
  '
  If (Lp("iMetDiv") = 1) Then
    Call SP("R[130]", 0)
  Else
    'Calcolo tratto di divisione se � a 0
    If (Lp("R[130]") = 0) Then
      Div_Rm = Lp("iDiaMola") / 2
      Div_hz = (Lp("R[44]") - Lp("R[45]")) / 2
      Div_Rt = 0
      Div_T = Sqr(Div_Rm ^ 2 - (Div_Rm - Div_hz - Div_Rt) ^ 2)
      Call SP("R[130]", Round(Div_T - MaxLcontRotori + 0.05, 1))
    End If
  End If
  '
  'AGGIORNO LA LARGHEZZA QUANDO E' NULLO
  tSpe = CalcolaSpessoreMola(ParMolaROT.XX(UBound(ParMolaROT.XX)) - ParMolaROT.XX(1))
  If (Lp("R[37]") <= 0) Then Call SP("R[37]", tSpe)
  '
  Select Case Lp("iNZone")
    '
    Case 1
      Call SP("Fascia2F1", 0)
      Call SP("Fascia3F1", 0)
      '
      Call SP("Conic2F1", 0)
      Call SP("Conic3F1", 0)
      '
      Call SP("CorPasso2", 0)
      Call SP("CorPasso3", 0)
      '
    Case 2
      Call SP("Fascia3F1", 0)
      Call SP("Conic3F1", 0)
      Call SP("CorPasso3", 0)
      '
    Case 3
      '
  End Select
  '
  Call SP("FASCIATOTALE_G", Lp("Fascia1F1") + Lp("Fascia2F1") + Lp("Fascia3F1"))
  '
End Sub

Sub CalcoloParametri_PROFILO_RAB_ESTERNI()
'
Dim Div_T   As Double
Dim Div_Rm  As Double
Dim Div_hz  As Double
Dim Div_Rt  As Double
Dim tSpe    As Double
Dim ret     As Long
Dim stmp    As String
'
On Error Resume Next
  '
  ParametriOK = True
  Call InizializzaVariabili
  '
  If (ActualGroup = "INP1_PROFILO_RAB_ESTERNI") Then Exit Sub
  '
  Call SP("iCORSAIN", Round(MaxLcontRotori + 0.05, 1))
  Call SP("iCORSAOUT", Round(MaxLcontRotori + 0.05, 1))
  Call SP("CORSAIN", Lp("iCORSAIN") + Lp("iCORSAIN_CORR"))
  Call SP("CORSAOUT", Lp("iCORSAOUT") + Lp("iCORSAOUT_CORR"))
  '
  If (Lp("iMetDiv") = 1) Then
    Call SP("R[130]", 0)
  Else
    'Calcolo tratto di divisione se � a 0
    If (Lp("R[130]") = 0) Then
      Div_Rm = Lp("iDiaMola") / 2
      Div_hz = Int(val(Lp("RA[0,1]")) - val(Lp("DINT")) / 2) + 2
      Div_Rt = 0
      Div_T = Sqr(Div_Rm ^ 2 - (Div_Rm - Div_hz - Div_Rt) ^ 2)
      Call SP("R[130]", Round(Div_T - MaxLcontRotori + 0.05, 1))
    End If
  End If
  '
  Select Case Lp("iNZone")
    '
    Case 1
      Call SP("Fascia2F1", 0)
      Call SP("Fascia3F1", 0)
      '
      Call SP("Conic2F1", 0)
      Call SP("Conic3F1", 0)
      '
      Call SP("CorPasso2", 0)
      Call SP("CorPasso3", 0)
      '
    Case 2
      Call SP("Fascia3F1", 0)
      Call SP("Conic3F1", 0)
      Call SP("CorPasso3", 0)
      '
    Case 3
      '
  End Select
  '
  Call SP("FASCIATOTALE_G", Lp("Fascia1F1") + Lp("Fascia2F1") + Lp("Fascia3F1"))
  '
End Sub
'*****
Sub CalcoloParametri_Skiving()

Dim tmp       As Double
Dim i         As Integer
Dim FattoreX  As Double
Dim stmp      As String

'Var. conversione spessore
Dim iB    As Double
Dim Betab As Double
Dim PB    As Double
Dim Sb    As Double
Dim E1    As Double
Dim H2    As Double
Dim H3    As Double
Dim F1    As Double
Dim F2    As Double
Dim Wx      As Double
Dim We      As Double
Dim X       As Double
Dim X1      As Double
Dim Ymini   As Double
Dim Ymaxi   As Double
Dim Ymoyen  As Double

'Var. calcolo lung. evolvente
Dim ValSAP  As Double
Dim ValEAP  As Double
Dim TifSAP  As Double
Dim TifEAP  As Double
Dim LungEvo As Double
'
Dim Dtip    As Double
'
Dim SovraMetalloRAD As Double
Dim SovrFianco      As Double
'
Dim L2        As Double
Dim L3        As Double
Dim G2        As Double
Dim LSV       As Double
Dim Dpri      As Double
Dim Hkw       As Double
Dim MODULO_CR As Double
Dim Alfa_CR    As Double
Dim TipocicloMV As Integer
Dim IndiceUT    As Integer
Dim Npassate    As Integer
Dim ShiftMM     As Double
Dim ShiftCR     As Double
Dim rsp         As Integer
Dim VTHob       As Double
Dim MaxVT       As Double
Dim SENSO_FILETTO_CR   As Double
Dim PRINCIPI_CR        As Double
Dim DIAM_EXT_CR        As Double
Dim DIAM_PRIM_CR       As Double
Dim ARGOMENTO          As Double
Dim INCLINAZIONE_CR_RAD As Double
Dim INCLINAZIONE_CR_GRD As Double
Dim InclTesta          As Double
Dim PROFONDITA  As Double
Dim Corsa_X1    As Double
Dim AlfaMV      As Double


On Error GoTo errCalcoloParametri_Skiving

  ParametriOK = True

'Lettura Parametri Ingranaggio
'  DGlob.Z = Lp("NUMDENTI_G")
'  DGlob.Mn = Lp("MN_G")
'  DGlob.ALFA = Lp("ALFA_G") * PG / 180
'  DGlob.DI = Lp("DINT_G")
'  DGlob.DE = Lp("DEXT_G")
'  DGlob.SON = Lp("R[938]")
'  DGlob.DefSpe = Lp("iDefSp")
'  DGlob.WbQ = Lp("QW")
'  DGlob.WbN = Lp("ZW")
'  DGlob.RulQ = Lp("QR")
'  DGlob.RulD = Lp("DR")
'  DGlob.Beta = Lp("BETA_G") * PG / 180
'  DGlob.RT = 1 'lp("R[51]")
'  DGlob.RF = 1 'lp("R[52]")
  
'Lettura Parametri Creatore
  MODULO_CR = Lp("MNUT_H")
  Alfa_CR = Lp("ALFAUT_H")
  SENSO_FILETTO_CR = Lp("SENSOFIL_H")
  PRINCIPI_CR = Lp("NUMPRINC_H")
  DIAM_EXT_CR = Lp("DIAMOLAMAX_H") 'Diametro Ext CR
  DIAM_PRIM_CR = Lp("DIAMMOLA_H")   'Diametro Prim CR
'
  Dim CorrSpessore As Double
  Dim CorrInterasse As Double
  Dim NewRulQ As Double
  Dim NewSon As Double
  Dim CorrSon As Double
  Dim Indtav As Integer
  Dim ValoreVar As String

  
  For Indtav = 0 To 1
  
    ValoreVar = Lp_Str("iCorrSpessore_H[" + LTrim(str(Indtav)) + "]")
    If ValoreVar = "NOTFOUND" Then GoTo FineCorrIntH
    
    CorrSpessore = Lp("iCorrSpessore_H[" + LTrim(str(Indtav)) + "]")
  
  'Calcolo Correzione Interasse da Correzione su SON , Wn , Qr
  Select Case DGlob.DefSpe
    Case 1 'SON
      CorrInterasse = Int(CorrSpessore / 2 / Tan(DGlob.ALFA) * 10000) / 10000
    
    Case 2 'Quota Wb
      CorrInterasse = Int(CorrSpessore / 2 / Tan(DGlob.ALFA) * 10000) / 10000
          
    Case 3 'Quota Rulli
      'Calcolo nuova quota rulli da quota rulli impostata
      If (DGlob.RulQ <> 0) And (DGlob.RulD <> 0) And (DGlob.RulD <> DGlob.RulQ) Then
        NewRulQ = DGlob.RulQ - CorrSpessore
        
        E1 = PG / 2 / DGlob.Z
        If (Int(DGlob.Z / 2) = DGlob.Z / 2) Then E1 = 0
        H2 = DGlob.Mn * DGlob.Z * Cos(DGlob.ALFAt) * Cos(E1) / ((NewRulQ - DGlob.RulD) * Cos(Abs(DGlob.Beta)))
        H3 = Tan(Acs(H2))
        F2 = (inv(DGlob.ALFAt) + DGlob.RulD / (DGlob.Mn * DGlob.Z * Cos(DGlob.ALFA)) - H3 + Atn(H3)) * DGlob.Mn * DGlob.Z
        NewSon = PG * DGlob.Mn - F2
      End If
      
      CorrSon = DGlob.SON - NewSon
      CorrInterasse = Int(CorrSon / 2 / Tan(DGlob.ALFA) * 10000) / 10000
  End Select
  
  'Aggiorno valore correttore interasse
  Call SP("CORINTLAV_G[" + LTrim(str(Indtav)) + ",2]", CorrInterasse)
  
  Next Indtav
FineCorrIntH:

'Calcolo sovrametallo radiale
  SovrFianco = Lp("STOCK_H")
  SovraMetalloRAD = SovrFianco / Tan(DGlob.ALFA)
  'Scrivi parametro sovrametallo radiale
  Call SP("SOVRMETRAD_H", frmt(SovraMetalloRAD, 3))

'Calcolo Shift creatore
  LSV = MODULO_CR * (PG / 2 + Tan(Alfa_CR * PG / 180) / Cos(DGlob.Beta))
  Dpri = DGlob.Mn * DGlob.Z / Cos(DGlob.Beta)
  Hkw = (Dpri - DGlob.DI) / 2
  
  If (Alfa_CR <> 0) Then
     G2 = Hkw / Tan(Alfa_CR * PG / 180)
  Else
     G2 = Hkw / Tan(DGlob.ALFA)
  End If
  L2 = G2 + LSV * 1.4
  '
  Call SP("iSHIFTIN_H", Round(L2, 1))
  Call SP("iSHIFTOUT_H", Round(L2, 1))
  Call SP("SHIFTIN_H", Lp("iSHIFTIN_H") + Lp("iSHIFTIN_H_CORR"))
  Call SP("SHIFTOUT_H", Lp("iSHIFTOUT_H") + Lp("iSHIFTOUT_H_CORR"))

  TipocicloMV = Lp("CYC[0,1]")
  IndiceUT = Lp("CYC[0,10]")
  Npassate = Lp("CYC[0,2]")
  ShiftMM = Lp("CYC[0,5]")
  ShiftCR = Lp("CYC[0,8]")
  VTHob = Lp("CYC[0,6]")
  MaxVT = GetInfo("CALCOLO", "VTHOBMAX", Path_LAVORAZIONE_INI)
  
  If IndiceUT > 1 Then
     If TipocicloMV > 1 Then
        StopRegieEvents
        MsgBox ("Type Cycle <2> is not allowed for hob tool")
        ResumeRegieEvents
        Call SP("CYC[0,1]", 1)
     End If
     If Npassate > 1 Then
        StopRegieEvents
        MsgBox ("Only one (1) passe is allowed for hob cycle")
        ResumeRegieEvents
        Call SP("CYC[0,2]", 1)
     End If
     If ShiftMM <> 0 Then
        StopRegieEvents
        MsgBox ("With hob tool is not allowed diagonal shift")
        ResumeRegieEvents
        Call SP("CYC[0,5]", 0)
        Call SP("SHF1", 0)
     End If
     If ShiftCR = 0 Then
        StopRegieEvents
        rsp = MsgBox("The hob shift is null! Do you accept standard value ?", 256 + 4 + 32, "WARNING")
        ResumeRegieEvents
        If rsp = 6 Then
           ShiftCR = Int((MODULO_CR * 3.1415) * 10) / 10
           Call SP("CYC[0,8]", ShiftCR)
        End If
     End If
     If VTHob > MaxVT Then
        stmp = str(VTHob)
        StopRegieEvents
        rsp = MsgBox("The hob cut speed : " & stmp & " is over limit! The max value is update!")
        ResumeRegieEvents
        Call SP("CYC[0,6]", MaxVT)
     End If
  
  End If

'CALCOLO EXTRACORSA PER CREATORE
  
  stmp = GetInfo("CALCOLO", "PROFONDITA", Path_LAVORAZIONE_INI)
  PROFONDITA = val(stmp) ' Letto da file INI
  
  AlfaMV = Lp("ALFAUT_H")
  
  'CALCOLO INCLINAZIONE FILETTO CREATORE
   
   ARGOMENTO = MODULO_CR * PRINCIPI_CR / DIAM_PRIM_CR
   INCLINAZIONE_CR_RAD = Atn(ARGOMENTO / Sqr(1 - ARGOMENTO * ARGOMENTO))  'RADIANTI
   INCLINAZIONE_CR_GRD = INCLINAZIONE_CR_RAD / PG * 180                   'GRADI
   
  'CALCOLO INCLINAZIONE ASSE A IN RADIANTI
  If (SENSO_FILETTO_CR = -1) Then
    'filetto creatore destro
    InclTesta = DGlob.Beta - INCLINAZIONE_CR_RAD
  Else
   If (SENSO_FILETTO_CR = 1) Then
     'filetto creatore sinistro
     InclTesta = DGlob.Beta + INCLINAZIONE_CR_RAD
   End If
  End If
  '
  Corsa_X1 = Tan(Abs(InclTesta)) * Sqr(PROFONDITA * (DIAM_EXT_CR / Sin(InclTesta) / Sin(InclTesta) + Lp("DEXT_G") - PROFONDITA))
  Corsa_X1 = (Int(Corsa_X1 * 10)) / 10
  If (Corsa_X1 <= 2) Then Corsa_X1 = 2
  '
  Call SP("iCORSAIN_H", Round(Corsa_X1 + 0.05, 1))
  Call SP("iCORSAOUT_H", Round(Corsa_X1 + 0.05, 1))
  Call SP("CORSAIN_H", Lp("iCORSAIN_H") + Lp("iCORSAIN_H_CORR"))
  Call SP("CORSAOUT_H", Lp("iCORSAOUT_H") + Lp("iCORSAOUT_H_CORR"))


''  Calcolo parametri correzione elica con utensili creatore

Dim FasciaAMV As Double
Dim FasciaBMV As Double
Dim FasciaCMV As Double
Dim FasciaA   As Double
Dim FasciaB   As Double
Dim FasciaC   As Double

Dim AppBombXA As Double
Dim AppBombCA As Double
Dim AppConXA  As Double
Dim AppConCA  As Double
Dim AppBombXB As Double
Dim AppBombCB As Double
Dim AppConXB  As Double
Dim AppConCB  As Double
Dim AppBombXC As Double
Dim AppBombCC As Double
Dim AppConXC  As Double
Dim AppConCC  As Double

Dim BetaMV    As Double
Dim BombAMV   As Double
Dim BombAF2MV As Double
Dim ConAMV    As Double
Dim ConAF2MV  As Double

Dim BombBMV   As Double
Dim BombBF2MV As Double
Dim ConBMV    As Double
Dim ConBF2MV  As Double

Dim BombCMV   As Double
Dim BombCF2MV As Double
Dim ConCMV    As Double
Dim ConCF2MV  As Double
  
Dim BombFondoA!
Dim BombFondoB!
Dim BombFondoC!

Dim RaggioBOMB As Double

Dim ANGOLO      As Double
Dim CorsaRadA   As Double
Dim CorsaRadB   As Double
Dim CorsaRadC   As Double

' *** Agg 26.01.13
Dim RagBombCT1   As Double
Dim AppBombCT1  As Double
Dim AppConCT1   As Double
Dim RagBombCT2   As Double
Dim AppBombCT2  As Double
Dim AppConCT2   As Double
Dim RagBombCT3   As Double
Dim AppBombCT3  As Double
Dim AppConCT3   As Double

'
Dim LatoPart As Integer
Dim nZone    As Integer
'
LatoPart = Lp("LatoPA_H")
nZone = Lp("iNZoneH")
  
  Select Case nZone
      '
    Case 1
      Call SP("iFas2F1H", 0): Call SP("iFas2F2H", 0)
      Call SP("iFas3F1H", 0): Call SP("iFas3F2H", 0)
      Call SP("iCon2F1H", 0): Call SP("iCon2F2H", 0)
      Call SP("iBom2F1H", 0): Call SP("iBom2F2H", 0)
      Call SP("iCon3F1H", 0): Call SP("iCon3F2H", 0)
      Call SP("iBom3F1H", 0): Call SP("iBom3F2H", 0)
      FasciaAMV = Lp("CORSAIN_H") + Lp("iFas1F1H") + Lp("CORSAOUT_H")
      FasciaBMV = 0
      FasciaCMV = 0
      FasciaA = Lp("iFas1F1H")
      FasciaB = 0
      FasciaC = 0
      '
    Case 2
      Call SP("iFas3F1H", 0): Call SP("iFas3F2H", 0)
      Call SP("iCon3F1H", 0): Call SP("iCon3F2H", 0)
      Call SP("iBom3F1H", 0): Call SP("iBom3F2H", 0)
      FasciaAMV = Lp("CORSAIN_H") + Lp("iFas1F1H")
      FasciaBMV = Lp("iFas2F1H") + Lp("CORSAOUT_H")
      FasciaCMV = 0
      FasciaA = Lp("iFas1F1H")
      FasciaB = Lp("iFas2F1H")
      FasciaC = 0
      '
    Case 3
      FasciaAMV = Lp("CORSAIN_H") + Lp("iFas1F1H")
      FasciaBMV = Lp("iFas2F1H")
      FasciaCMV = Lp("iFas3F1H") + Lp("CORSAOUT_H")
      FasciaA = Lp("iFas1F1H")
      FasciaB = Lp("iFas2F1H")
      FasciaC = Lp("iFas3F1H")
      '
  End Select
  
  'Call SP("FASCIATOTALE_G", FasciaA + FasciaB + FasciaC)
  
  'ELICA PEZZO
    BetaMV = Lp("BETA_G")
    
    'TRATTO A
    BombAMV = Lp("iBom1F1H"): BombAF2MV = Lp("iBom1F2H")
    ConAMV = Lp("iCon1F1H"):  ConAF2MV = Lp("iCon1F2H")
    'TRATTO B
    BombBMV = Lp("iBom2F1H"): BombBF2MV = Lp("iBom2F2H")
    ConBMV = Lp("iCon2F1H"):  ConBF2MV = Lp("iCon2F2H")
    'TRATTO C
    BombCMV = Lp("iBom3F1H"): BombCF2MV = Lp("iBom3F2H")
    ConCMV = Lp("iCon3F1H"):  ConCF2MV = Lp("iCon3F2H")
    '
    Call CalcolaBombConXC(AppBombXA, AppBombCA, AppConXA, AppConCA, BombAMV, BombAF2MV, ConAMV, ConAF2MV, BetaMV)
    Call CalcolaBombConXC(AppBombXB, AppBombCB, AppConXB, AppConCB, BombBMV, BombBF2MV, ConBMV, ConBF2MV, BetaMV)
    Call CalcolaBombConXC(AppBombXC, AppBombCC, AppConXC, AppConCC, BombCMV, BombCF2MV, ConCMV, ConCF2MV, BetaMV)
    '

  If (FasciaA <> 0) Then
     If AppBombCA <> 0 Then
        RagBombCT1 = (FasciaA * FasciaA / (8 * Abs(AppBombCA))) + Abs(AppBombCA) / 2
        AppBombCT1 = Int(((2 * RagBombCT1 - Sqr(4 * RagBombCT1 * RagBombCT1 - FasciaAMV * FasciaAMV)) / 2) * 10000 + 0.5) / 10000
        If AppBombCA < 0 Then
           AppBombCT1 = -AppBombCT1
        End If
     Else
        AppBombCT1 = 0
     End If
     If AppConCA <> 0 Then
        AppConCT1 = AppConCA / FasciaA * FasciaAMV
        AppConCT1 = Int(AppConCT1 * 10000 + 0.5) / 10000
     Else
        AppConCT1 = 0
     End If
  Else
     AppBombCT1 = 0
     AppConCT1 = 0
  End If
  
  If (FasciaB <> 0) Then
     If AppBombCB <> 0 Then
        RagBombCT2 = (FasciaB * FasciaB / (8 * Abs(AppBombCB))) + Abs(AppBombCB) / 2
        AppBombCT2 = Int(((2 * RagBombCT2 - Sqr(4 * RagBombCT2 * RagBombCT2 - FasciaBMV * FasciaBMV)) / 2) * 10000 + 0.5) / 10000
        If AppBombCB < 0 Then
           AppBombCT2 = -AppBombCT2
        End If
     Else
        AppBombCT2 = 0
     End If
     If AppConCB <> 0 Then
        AppConCT2 = AppConCB / FasciaB * FasciaBMV
        AppConCT2 = Int(AppConCT2 * 10000 + 0.5) / 10000
     Else
        AppConCT2 = 0
     End If
  Else
     AppBombCT2 = 0
     AppConCT2 = 0
  End If
  
  If (FasciaC <> 0) Then
     If AppBombCC <> 0 Then
        RagBombCT3 = (FasciaC * FasciaC / (8 * Abs(AppBombCC))) + Abs(AppBombCC) / 2
        AppBombCT3 = Int(((2 * RagBombCT3 - Sqr(4 * RagBombCT3 * RagBombCT3 - FasciaCMV * FasciaCMV)) / 2) * 10000 + 0.5) / 10000
        If AppBombCC < 0 Then
           AppBombCT3 = -AppBombCT3
        End If
     Else
        AppBombCT3 = 0
     End If
     If AppConCC <> 0 Then
        AppConCT3 = AppConCC / FasciaC * FasciaCMV
        AppConCT3 = Int(AppConCT3 * 10000 + 0.5) / 10000
     Else
        AppConCT3 = 0
     End If
  Else
     AppBombCT3 = 0
     AppConCT3 = 0
  End If
  
  'Inserire in DB Molavite_SG
  Call SP("AppBombCT1H", frmt(AppBombCT1, 8))
  Call SP("AppConCT1H", frmt(AppConCT1, 8))
  Call SP("AppBombCT2H", frmt(AppBombCT2, 8))
  Call SP("AppConCT2H", frmt(AppConCT2, 8))
  Call SP("AppBombCT3H", frmt(AppBombCT3, 8))
  Call SP("AppConCT3H", frmt(AppConCT3, 8))
  
  'Angolo pressione pezzo in radianti
  ANGOLO = Lp("ALFA_G") * PG / 180
  '
  BombFondoA! = AppBombXA / Tan(ANGOLO): AppConXA = Int(AppConXA * 1000 + 0.5) / 1000
  BombFondoB! = AppBombXB / Tan(ANGOLO): AppConXB = Int(AppConXB * 1000 + 0.5) / 1000
  BombFondoC! = AppBombXC / Tan(ANGOLO): AppConXC = Int(AppConXC * 1000 + 0.5) / 1000
  '
  If (LatoPart = -1) Then 'Lato Testa portapezzo
      'TRATTO A
      If (BombFondoA! <> 0) Then
        RaggioBOMB = (FasciaA * FasciaA / (8 * Abs(BombFondoA!))) + Abs(BombFondoA!) / 2
        If (BombFondoA! > 0) Then
          RaggioBOMB = RaggioBOMB + DIAM_EXT_CR / 2
        Else
          RaggioBOMB = RaggioBOMB - DIAM_EXT_CR / 2
        End If
        RaggioBOMB = Int(RaggioBOMB * 1000 + 0.5) / 1000
        If (BombFondoA! > 0) Then RaggioBOMB = -RaggioBOMB
      Else
        RaggioBOMB = 0
      End If
      If (FasciaA <> 0) Then
      CorsaRadA = (AppConXA / Tan(ANGOLO)) / FasciaA * FasciaAMV
      CorsaRadA = Int(CorsaRadA * 10000 + 0.5) / 10000
      Else
      CorsaRadA = 0
      End If
      Call SP("CORSAASS1_H", FasciaAMV):     Call SP("CORSAASS1F2_H", FasciaAMV)
      Call SP("RAGGIOBOMB1_H", RaggioBOMB):  Call SP("RAGGIOBOMB1F2_H", RaggioBOMB)
      Call SP("CORSARAD1_H", CorsaRadA):     Call SP("CORSARAD1F2_H", CorsaRadA)
      'TRATTO B
      If (BombFondoB! <> 0) Then
        RaggioBOMB = (FasciaB * FasciaB / (8 * Abs(BombFondoB!))) + Abs(BombFondoB!) / 2
        If (BombFondoB! > 0) Then
          RaggioBOMB = RaggioBOMB + DIAM_EXT_CR / 2
        Else
          RaggioBOMB = RaggioBOMB - DIAM_EXT_CR / 2
        End If
        RaggioBOMB = Int(RaggioBOMB * 1000 + 0.5) / 1000
        If (BombFondoB! > 0) Then RaggioBOMB = -RaggioBOMB
      Else
        RaggioBOMB = 0
      End If
      If (FasciaB <> 0) Then
      CorsaRadB = (AppConXB / Tan(ANGOLO)) / FasciaB * FasciaBMV
      CorsaRadB = Int(CorsaRadB * 10000 + 0.5) / 10000
      Else
      CorsaRadB = 0
      End If
      Call SP("CORSAASS2_H", FasciaBMV):     Call SP("CORSAASS2F2_H", FasciaBMV)
      Call SP("RAGGIOBOMB2_H", RaggioBOMB):  Call SP("RAGGIOBOMB2F2_H", RaggioBOMB)
      Call SP("CORSARAD2_H", CorsaRadB):     Call SP("CORSARAD2F2_H", CorsaRadB)
      'TRATTO C
      If (BombFondoC! <> 0) Then
        RaggioBOMB = (FasciaC * FasciaC / (8 * Abs(BombFondoC!))) + Abs(BombFondoC!) / 2
        If (BombFondoC! > 0) Then
          RaggioBOMB = RaggioBOMB + DIAM_EXT_CR / 2
        Else
          RaggioBOMB = RaggioBOMB - DIAM_EXT_CR / 2
        End If
        RaggioBOMB = Int(RaggioBOMB * 1000 + 0.5) / 1000
        If (BombFondoC! > 0) Then RaggioBOMB = -RaggioBOMB
      Else
        RaggioBOMB = 0
      End If
      If (FasciaC <> 0) Then
      CorsaRadC = (AppConXC / Tan(ANGOLO)) / FasciaC * FasciaCMV
      CorsaRadC = Int(CorsaRadC * 10000 + 0.5) / 10000
      Else
      CorsaRadC = 0
      End If
      Call SP("CORSAASS3_H", FasciaCMV):     Call SP("CORSAASS3F2_H", FasciaCMV)
      Call SP("RAGGIOBOMB3_H", RaggioBOMB):  Call SP("RAGGIOBOMB3F2_H", RaggioBOMB)
      Call SP("CORSARAD3_H", CorsaRadC):     Call SP("CORSARAD3F2_H", CorsaRadC)
      '
  Else   'Lato Partenza Contropunta
    '
    Select Case nZone
    Case 3
      'TRATTO A -> Corsa 3
      If (BombFondoA! <> 0) Then
        RaggioBOMB = (FasciaA * FasciaA / (8 * Abs(BombFondoA!))) + Abs(BombFondoA!) / 2
        If (BombFondoA! > 0) Then
          RaggioBOMB = RaggioBOMB + DIAM_EXT_CR / 2
        Else
          RaggioBOMB = RaggioBOMB - DIAM_EXT_CR / 2
        End If
        RaggioBOMB = Int(RaggioBOMB * 1000 + 0.5) / 1000
        If (BombFondoA! > 0) Then RaggioBOMB = -RaggioBOMB
      Else
        RaggioBOMB = 0
      End If
      If (FasciaA <> 0) Then
      CorsaRadA = (AppConXA / Tan(ANGOLO)) / FasciaA * FasciaAMV
      CorsaRadA = Int(CorsaRadA * 10000 + 0.5) / 10000
      Else
      CorsaRadA = 0
      End If
      Call SP("CORSAASS3_H", FasciaAMV):     Call SP("CORSAASS3F2_H", FasciaAMV)
      Call SP("RAGGIOBOMB3_H", -RaggioBOMB): Call SP("RAGGIOBOMB3F2_H", -RaggioBOMB)
      Call SP("CORSARAD3_H", -CorsaRadA):    Call SP("CORSARAD3F2_H", -CorsaRadA)
      'TRATTO B -> Corsa 2
      If (BombFondoB! <> 0) Then
        RaggioBOMB = (FasciaB * FasciaB / (8 * Abs(BombFondoB!))) + Abs(BombFondoB!) / 2
        If (BombFondoB! > 0) Then
          RaggioBOMB = RaggioBOMB + DIAM_EXT_CR / 2
        Else
          RaggioBOMB = RaggioBOMB - DIAM_EXT_CR / 2
        End If
        RaggioBOMB = Int(RaggioBOMB * 1000 + 0.5) / 1000
        If (BombFondoB! > 0) Then RaggioBOMB = -RaggioBOMB
      Else
        RaggioBOMB = 0
      End If
      If (FasciaB <> 0) Then
      CorsaRadB = (AppConXB / Tan(ANGOLO)) / FasciaB * FasciaBMV
      CorsaRadB = Int(CorsaRadB * 10000 + 0.5) / 10000
      Else
      CorsaRadB = 0
      End If
      Call SP("CORSAASS2_H", FasciaBMV):     Call SP("CORSAASS2F2_H", FasciaBMV)
      Call SP("RAGGIOBOMB2_H", -RaggioBOMB): Call SP("RAGGIOBOMB2F2_H", -RaggioBOMB)
      Call SP("CORSARAD2_H", -CorsaRadB):    Call SP("CORSARAD2F2_H", -CorsaRadB)
      'TRATTO C -> Corsa 1
      If (BombFondoC! <> 0) Then
        RaggioBOMB = (FasciaC * FasciaC / (8 * Abs(BombFondoC!))) + Abs(BombFondoC!) / 2
        If (BombFondoC! > 0) Then
          RaggioBOMB = RaggioBOMB + DIAM_EXT_CR / 2
        Else
          RaggioBOMB = RaggioBOMB - DIAM_EXT_CR / 2
        End If
        RaggioBOMB = Int(RaggioBOMB * 1000 + 0.5) / 1000
        If (BombFondoC! > 0) Then RaggioBOMB = -RaggioBOMB
      Else
        RaggioBOMB = 0
      End If
      If (FasciaC <> 0) Then
      CorsaRadC = (AppConXC / Tan(ANGOLO)) / FasciaC * FasciaCMV
      CorsaRadC = Int(CorsaRadC * 10000 + 0.5) / 10000
      Else
      CorsaRadC = 0
      End If
      Call SP("CORSAASS1_H", FasciaCMV):     Call SP("CORSAASS1F2_H", FasciaCMV)
      Call SP("RAGGIOBOMB1_H", -RaggioBOMB): Call SP("RAGGIOBOMB1F2_H", -RaggioBOMB)
      Call SP("CORSARAD1_H", -CorsaRadC):    Call SP("CORSARAD1F2_H", -CorsaRadC)
    Case 2
      'TRATTO A -> Corsa 2
      If (BombFondoA! <> 0) Then
        RaggioBOMB = (FasciaA * FasciaA / (8 * Abs(BombFondoA!))) + Abs(BombFondoA!) / 2
        If (BombFondoA! > 0) Then
          RaggioBOMB = RaggioBOMB + DIAM_EXT_CR / 2
        Else
          RaggioBOMB = RaggioBOMB - DIAM_EXT_CR / 2
        End If
        RaggioBOMB = Int(RaggioBOMB * 1000 + 0.5) / 1000
        If (BombFondoA! > 0) Then RaggioBOMB = -RaggioBOMB
      Else
        RaggioBOMB = 0
      End If
      If (FasciaA <> 0) Then
      CorsaRadA = (AppConXA / Tan(ANGOLO)) / FasciaA * FasciaAMV
      CorsaRadA = Int(CorsaRadA * 10000 + 0.5) / 10000
      Else
      CorsaRadA = 0
      End If
      Call SP("CORSAASS2_H", FasciaAMV):     Call SP("CORSAASS2F2_H", FasciaAMV)
      Call SP("RAGGIOBOMB2_H", -RaggioBOMB): Call SP("RAGGIOBOMB2F2_H", -RaggioBOMB)
      Call SP("CORSARAD2_H", -CorsaRadA):    Call SP("CORSARAD2F2_H", -CorsaRadA)
      'TRATTO B -> Corsa 1
      If (BombFondoB! <> 0) Then
        RaggioBOMB = (FasciaB * FasciaB / (8 * Abs(BombFondoB!))) + Abs(BombFondoB!) / 2
        If (BombFondoB! > 0) Then
          RaggioBOMB = RaggioBOMB + DIAM_EXT_CR / 2
        Else
          RaggioBOMB = RaggioBOMB - DIAM_EXT_CR / 2
        End If
        RaggioBOMB = Int(RaggioBOMB * 1000 + 0.5) / 1000
        If (BombFondoB! > 0) Then RaggioBOMB = -RaggioBOMB
      Else
        RaggioBOMB = 0
      End If
      If (FasciaB <> 0) Then
      CorsaRadB = (AppConXB / Tan(ANGOLO)) / FasciaB * FasciaBMV
      CorsaRadB = Int(CorsaRadB * 10000 + 0.5) / 10000
      Else
      CorsaRadB = 0
      End If
      Call SP("CORSAASS1_H", FasciaBMV):     Call SP("CORSAASS1F2_H", FasciaBMV)
      Call SP("RAGGIOBOMB1_H", -RaggioBOMB): Call SP("RAGGIOBOMB1F2_H", -RaggioBOMB)
      Call SP("CORSARAD1_H", -CorsaRadB):    Call SP("CORSARAD1F2_H", -CorsaRadB)
      'TRATTO C
      Call SP("CORSAASS3_H", 0):             Call SP("CORSAASS3F2_H", 0)
      Call SP("RAGGIOBOMB3_H", 0):           Call SP("RAGGIOBOMB3F2_H", 0)
      Call SP("CORSARAD3_H", 0):             Call SP("CORSARAD3F2_H", 0)
      '
    Case 1
      'TRATTO A -> Corsa 1
      If (BombFondoA! <> 0) Then
        RaggioBOMB = (FasciaA * FasciaA / (8 * Abs(BombFondoA!))) + Abs(BombFondoA!) / 2
        If (BombFondoA! > 0) Then
          RaggioBOMB = RaggioBOMB + DIAM_EXT_CR / 2
        Else
          RaggioBOMB = RaggioBOMB - DIAM_EXT_CR / 2
        End If
        RaggioBOMB = Int(RaggioBOMB * 1000 + 0.5) / 1000
        If (BombFondoA! > 0) Then RaggioBOMB = -RaggioBOMB
      Else
        RaggioBOMB = 0
      End If
      If (FasciaA <> 0) Then
      CorsaRadA = (AppConXA / Tan(ANGOLO)) / FasciaA * FasciaAMV
      CorsaRadA = Int(CorsaRadA * 10000 + 0.5) / 10000
      Else
      CorsaRadA = 0
      End If
      Call SP("CORSAASS1_H", FasciaAMV):     Call SP("CORSAASS1F2_H", FasciaAMV)
      Call SP("RAGGIOBOMB1_H", -RaggioBOMB): Call SP("RAGGIOBOMB1F2_H", -RaggioBOMB)
      Call SP("CORSARAD1_H", -CorsaRadA):    Call SP("CORSARAD1F2_H", -CorsaRadA)
      'TRATTO B
      Call SP("CORSAASS2_H", 0):             Call SP("CORSAASS2F2_H", 0)
      Call SP("RAGGIOBOMB2_H", 0):           Call SP("RAGGIOBOMB2F2_H", 0)
      Call SP("CORSARAD2_H", 0):             Call SP("CORSARAD2F2_H", 0)
      'TRATTO C
      Call SP("CORSAASS3_H", 0):             Call SP("CORSAASS3F2_H", 0)
      Call SP("RAGGIOBOMB3_H", 0):           Call SP("RAGGIOBOMB3F2_H", 0)
      Call SP("CORSARAD3_H", 0):             Call SP("CORSARAD3F2_H", 0)
      '
    End Select
  End If



Exit Sub

errCalcoloParametri_Skiving:
  If (Err.Number = 6) Or (Err.Number = 11) Then
    'ParametriOK = False
    'PAR.Pic.Cls
  Else
    MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Sub: CalcoloParametri_Skiving"
  End If
  Exit Sub

End Sub
'*****
Sub CalcoloParametri_MOLAVITEG160()

'
Dim tmp       As Double
Dim i         As Integer
'Dim FattoreX  As Double
Dim stmp      As String
'Var. conversione spessore
Dim iB    As Double
Dim Betab As Double
Dim PB    As Double
Dim Sb    As Double
Dim E1    As Double
Dim H2    As Double
Dim H3    As Double
Dim F1    As Double
Dim F2    As Double
Dim Wx      As Double
Dim We      As Double
Dim X       As Double
Dim X1      As Double
Dim Ymini   As Double
Dim Ymaxi   As Double
Dim Ymoyen  As Double
'Var. calcolo lung. evolvente
Dim ValSAP  As Double
Dim ValEAP  As Double
Dim TifSAP  As Double
Dim TifEAP  As Double
Dim LungEvo As Double
'
Dim Dtip    As Double
'
Dim SovraMetalloRAD As Double
Dim SovrFianco      As Double
Dim ActualCorrF1    As Double
Dim ActualCorrF2    As Double
Dim CorrfHaF1       As Double
Dim CorrfHaF2       As Double

Dim PosW_Mola       As Double
Dim L_Mola          As Double
Dim SHIFTIN_Mola    As Double
Dim SHIFTOUT_Mola   As Double
Dim PosIn_Mola      As Double
Dim PosFin_Mola     As Double
Dim PosIn2_Mola#, PosFin2_Mola#
Dim DirShift_Mola   As Double
Dim DirShift_Pre    As Double
Dim PosShf          As Double
'
Dim DbetaF1         As Double
Dim DbetaF2         As Double
Dim CorConTA        As Double
Dim CorConTB        As Double
Dim CorConTC        As Double
Dim CorsaRadTA      As Double
Dim CorsaRadTB      As Double
Dim CorsaRadTC      As Double
'
Dim ErrPasso        As Double
Dim ErrPassoSx      As Double
Dim ErrPassoDx      As Double
Dim ConSx           As Double
Dim ConDx           As Double
Dim fHbSX           As Double
Dim fHbDx           As Double
Dim iConAfHb        As Double
Dim iConBfHb        As Double
Dim iConCfHb        As Double
Dim CorsaRadZA      As Double
Dim CorsaRadZB      As Double
Dim CorsaRadZC      As Double

Dim FasciaFhb       As Double
Dim PercentualeFascia As Double

Dim FiancoSx        As Double
Dim FiancoSxPrec    As Double

Dim Con_F1          As Double
Dim fHb_F1          As Double
Dim Con_F2          As Double
Dim fHb_F2          As Double

Dim ErrPassoF1      As Double
Dim ErrPassoF2      As Double

Dim CorrFHBSX       As Double
Dim ActualErrSXFHB  As Double
Dim CorrFHBDX       As Double
Dim ActualErrDXFHB  As Double
Dim MODALITA        As String
Dim TW_TUNING#, iTW_TUNING, iTW_TUNING_corr#
Dim DPOSMOLA        As Double
Dim DPOSB1          As Double

'
On Error GoTo errCalcoloParametri_MOLAVITE
'
  ParametriOK = True
  Call InizializzaVariabili
  '
  'LEGGO LA MODALITA DI TRASFERIMENTO DATI
  MODALITA = GetInfo("Configurazione", "MODALITA", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))
  
  'Azzeramento variabili calcolo
  CalcoloEsterni.DB = 0
  ReDim CalcoloEsterni.DCont(0)
  CalcoloEsterni.DE = 0
  CalcoloEsterni.DI = 0
  CalcoloEsterni.DIOtt = 0
  CalcoloEsterni.DP = 0
  ReDim CalcoloEsterni.IngrX(0)
  ReDim CalcoloEsterni.IngrY(0)
  ReDim CalcoloEsterni.LCont(0)
  ReDim CalcoloEsterni.MolaX(0)
  ReDim CalcoloEsterni.MolaY(0)
  CalcoloEsterni.Np = 0

'Gestione Lato serraggio
FiancoSx = Lp("iFiancoSX")
FiancoSxPrec = Lp("iFiancoSXPRE")

If FiancoSxPrec <> FiancoSx Then
    FiancoSxPrec = FiancoSx
    Call SP("iFiancoSXPRE", FiancoSxPrec)
   
   'Cancella tutte le correzioni di fHa
   'Fianco Sx
    Call SP("iERRFHAFSX", 0)
    Call SP("CORFHA1", 0)
    Call SP("COREVFHASX_G", 0)
    Call SP("iCORPREFHAFSX", 0)
   'Fianco Dx
    Call SP("iERRFHAFDX", 0)
    Call SP("CORFHA2", 0)
    Call SP("COREVFHADX_G", 0)
    Call SP("iCORPREFHAFDX", 0)
    
    'Cancella tutte le correzioni di fHb
    
    Call SP("iERRPREDXFHB", 0)
    Call SP("iERRDXFHB", 0)
    
    Call SP("iERRPRESXFHB", 0)
    Call SP("iERRSXFHB", 0)
    
    Call SP("ERRTOTSXFHB_G", 0)
    Call SP("ERRTOTDXFHB_G", 0)
    Call SP("CORRPASSO", 0)
    Call SP("CORX1FHB", 0)
    Call SP("CORX2FHB", 0)
    Call SP("CORX3FHB", 0)
      
    If MODALITA = "SPF" Then
       Call OPC_SCRIVI_DATO("/ACC/NCK/UGUD/CORRPASSO", "0")
       Call OPC_SCRIVI_DATO("/ACC/NCK/GUD7/CORX1FHB", "0")
       Call OPC_SCRIVI_DATO("/ACC/NCK/GUD7/CORX2FHB", "0")
       Call OPC_SCRIVI_DATO("/ACC/NCK/GUD7/CORX3FHB", "0")
       Call OPC_SCRIVI_DATO("/ACC/NCK/UGUD/CORFHA1", "0")
       Call OPC_SCRIVI_DATO("/ACC/NCK/UGUD/CORFHA2", "0")
    End If
End If
    
  '
  'Lettura Parametri Profilo
  DGlob.Z = Lp("NUMDENTI_G")
  DGlob.Mn = Lp("MN_G")
  DGlob.ALFA = Lp("ALFA_G") * PG / 180
  DGlob.DI = Lp("DINT_G")
  DGlob.DE = Lp("DEXT_G")
  DGlob.SON = Lp("R[938]")
  DGlob.DefSpe = Lp("iDefSp")
  DGlob.WbQ = Lp("QW")
  DGlob.WbN = Lp("ZW")
  DGlob.RulQ = Lp("QR")
  DGlob.RulD = Lp("DR")
  
    
  DGlob.BetaF1 = Lp("BETAFsx_G") * PG / 180 * Lp("iSensoElica")
  DGlob.BetaF2 = Lp("BETAFdx_G") * PG / 180 * Lp("iSensoElica")
      
  'Angolo elica medio comprensivo di segno che indica il senso elica
  DGlob.Beta = ((DGlob.BetaF1 + DGlob.BetaF2) / 2)
  Call SP("BETA_G", DGlob.Beta * 180 / PG)
  
  DGlob.RT = 1
  DGlob.RF = 1
  DGlob.DistPtEv = 0.5
  '
  If (DGlob.Z = 0) Or (DGlob.Mn = 0) Then Exit Sub
  '
  'Calcolo correzione per angolo elica differenti tra i fianchi
  DbetaF1 = (DGlob.BetaF1 - DGlob.Beta)
  DbetaF2 = (DGlob.Beta - DGlob.BetaF2)
  '
  'Calcolo altri parametri
  DGlob.ALFAt = Atn(Tan(DGlob.ALFA) / Cos(DGlob.Beta))
  DGlob.DP = DGlob.Mn * DGlob.Z / Cos(DGlob.Beta)     'Diametro Primitivo
  DGlob.DB = DGlob.DP * Cos(DGlob.ALFAt)              'Diametro di Base
  '
  'Calcolo sovrametallo radiale
  SovrFianco = Lp("STOCK")
  SovraMetalloRAD = SovrFianco / Tan(DGlob.ALFA)
  'Scrivi parametro sovrametallo radiale
  Call SP("SOVRMETRAD_G", frmt(SovraMetalloRAD, 2))
  '
  'Leggi SAP e EAP
  ValSAP = Lp("DSAP_G")
  ValEAP = Lp("DEAP_G")
  DGlob.SAP = ValSAP
  DGlob.EAP = ValEAP
  '
  'Leggi Diametro rastremazione
  Dtip = Lp("TIPRELIEF_DIAMETER")
  '
  If ((DGlob.DE <= 0) And (DGlob.Mn > 0)) Or ((DGlob.DE <= DGlob.DB) And (DGlob.Mn > 0)) Then
      DGlob.DE = DGlob.DB + (DGlob.Mn / Cos(DGlob.Beta) * 2.2 * 2)
      Call SP("DEXT_G", frmt(DGlob.DE, 4))
      Call SP("DINT_G", frmt(DGlob.DE - (DGlob.Mn / Cos(DGlob.Beta) * 2.2 * 2), 4))
  End If
  '
  If (ValEAP > DGlob.DE) And (DGlob.DE > 0) Then
      ValEAP = DGlob.DE
      Call SP("DEAP_G", frmt(ValEAP, 4))
  End If
  '
  If (ValSAP <= 0) Or (ValSAP <= DGlob.DB) Then
      ValSAP = DGlob.DB + 0.01
      Call SP("DSAP_G", frmt(ValSAP, 4))
      Call SP("SAP_F1_CD", frmt(ValSAP, 4))
      Call SP("SAP_F2_CD", frmt(ValSAP, 4))
  Else
      Call SP("SAP_F1_CD", frmt(ValSAP, 4))
      Call SP("SAP_F2_CD", frmt(ValSAP, 4))
  End If
  
  If (ValEAP <= 0) Or (ValEAP <= DGlob.DB) Or (ValEAP <= ValSAP) Then
      ValEAP = DGlob.DB + (DGlob.Mn * 2.2 * 2)
      Call SP("DEAP_G", frmt(ValEAP, 4))
  End If
  '
  If (Dtip > ValEAP) Then
      Dtip = ValEAP
      Call SP("TIPRELIEF_DIAMETER", frmt(Dtip, 4))
  End If
  '
  'Calcolo Lung. evolvente
  If (ValSAP > 0) And (DGlob.DB > 0) And (ValSAP > DGlob.DB) Then
     TifSAP = Sqr((ValSAP / 2) ^ 2 - (DGlob.DB / 2) ^ 2)
  End If
  If (ValEAP > 0) And (DGlob.DB > 0) And (ValEAP > ValSAP) Then
     TifEAP = Sqr((ValEAP / 2) ^ 2 - (DGlob.DB / 2) ^ 2)
  End If
  If (TifEAP > TifSAP) And (TifSAP > 0) Then
      LungEvo = Int((TifEAP - TifSAP) * 1000) / 1000
  Else
      LungEvo = 0
  End If
  '
  'Scrivi parametro Lunghezza evolvente
  Call SP("LAEVOLV_G", frmt(LungEvo, 4))
  '
  '----------------------------------
  'Se lo spessore � nullo lo imposto a mezzo passo
  If (DGlob.SON = 0) And (DGlob.DefSpe = 1) Then
    DGlob.SON = Round((DGlob.Mn * PG) / 2, 3)
    Call SP("R[938]", frmt(DGlob.SON, 8))
    'CALCOLO DEL FATTORE DI SPOSTAMENTO
    DGlob.FattoreX = (DGlob.SON - ((PG / 2) * DGlob.Mn)) / (2 * DGlob.Mn * Tan(DGlob.ALFA))
    DGlob.FattoreX = Int(DGlob.FattoreX * 100000 + 0.5) / 100000
    Call SP("FattoreX", frmt(DGlob.FattoreX, 8))

    'FattoreX = (DGlob.SON - ((PG / 2) * DGlob.Mn)) / (2 * DGlob.Mn * Tan(DGlob.ALFA))
    'FattoreX = Int(FattoreX * 100000 + 0.5) / 100000
    'Call SP("FattoreX", FattoreX)
  End If
  '
  Betab = Atn(Tan(Abs(DGlob.Beta)) * (Cos(DGlob.ALFAt)))
  PB = DGlob.DB * PG / DGlob.Z
  '
  Select Case DGlob.DefSpe
    '
    Case 1 'SON
      If (DGlob.SON > 0) Then
        'CALCOLO QUOTA WILDHABER
        If (DGlob.WbN > 0) Then
          DGlob.SONt = DGlob.SON / Cos(DGlob.Beta)
          Sb = (DGlob.SONt + DGlob.DP * inv(DGlob.ALFAt)) * DGlob.DB / DGlob.DP
          iB = PB - Sb
          DGlob.WbQ = DGlob.WbN * PB * Cos(Betab) - iB * Cos(Betab)
          'CALCOLO QUOTA RULLI
          If (DGlob.RulD > 0) Then
            Wx = Cos(PG / (2 * DGlob.Z)) * DGlob.DB
            If Int(DGlob.Z / 2) = DGlob.Z / 2 Then Wx = DGlob.DB
            We = Sb - PB
            X = (We + DGlob.RulD / Cos(Betab)) / DGlob.DB
            Ymini = 0: Ymaxi = PG / 2
            For i = 1 To 25
              Ymoyen = (Ymini + Ymaxi) / 2: X1 = Tan(Ymoyen) - Ymoyen
              If X1 < X Then Ymini = Ymoyen Else Ymaxi = Ymoyen
            Next i
            DGlob.RulQ = (Wx / Cos(Ymoyen)) + DGlob.RulD
          Else
            DGlob.RulQ = 0
          End If
        Else
          DGlob.WbQ = 0
        End If
        'CALCOLO DEL FATTORE DI SPOSTAMENTO
        DGlob.FattoreX = (DGlob.SON - ((PG / 2) * DGlob.Mn)) / (2 * DGlob.Mn * Tan(DGlob.ALFA))
        'AGGIORNAMENTO VALORI
        Call SP("QW", frmt(DGlob.WbQ, 8))
        Call SP("QR", frmt(DGlob.RulQ, 8))
        Call SP("FattoreX", frmt(DGlob.FattoreX, 8))
      End If
      '
    Case 2 'Quota Wb
      If (DGlob.WbN > 0) And (DGlob.WbQ > 0) Then
        iB = DGlob.WbN * PB - DGlob.WbQ / Cos(Betab)
        Sb = PB - iB
        DGlob.SONt = DGlob.DP * (Sb / DGlob.DB - (inv(DGlob.ALFAt)))
        DGlob.SON = DGlob.SONt * Cos(DGlob.Beta)
        'CALCOLO QUOTA RULLI
        If (DGlob.RulD > 0) Then
          Wx = Cos(PG / (2 * DGlob.Z)) * DGlob.DB
          If Int(DGlob.Z / 2) = DGlob.Z / 2 Then Wx = DGlob.DB
          We = Sb - PB
          X = (We + DGlob.RulD / Cos(Betab)) / DGlob.DB
          Ymini = 0: Ymaxi = PG / 2
          For i = 1 To 25
            Ymoyen = (Ymini + Ymaxi) / 2: X1 = Tan(Ymoyen) - Ymoyen
            If X1 < X Then Ymini = Ymoyen Else Ymaxi = Ymoyen
          Next i
          DGlob.RulQ = (Wx / Cos(Ymoyen)) + DGlob.RulD
        Else
          DGlob.RulQ = 0
        End If
        'CALCOLO DEL FATTORE DI SPOSTAMENTO
        DGlob.FattoreX = (DGlob.SON - ((PG / 2) * DGlob.Mn)) / (2 * DGlob.Mn * Tan(DGlob.ALFA))
        'AGGIORNAMENTO VALORI
        Call SP("R[938]", frmt(DGlob.SON, 8))
        Call SP("QR", frmt(DGlob.RulQ, 8))
        Call SP("FattoreX", frmt(DGlob.FattoreX, 8))
      End If
      '
    Case 3 'Quota Rulli
      If (DGlob.RulQ <> 0) And (DGlob.RulD <> 0) And (DGlob.RulD <> DGlob.RulQ) Then
        E1 = PG / 2 / DGlob.Z
        If (Int(DGlob.Z / 2) = DGlob.Z / 2) Then E1 = 0
        H2 = DGlob.Mn * DGlob.Z * Cos(DGlob.ALFAt) * Cos(E1) / ((DGlob.RulQ - DGlob.RulD) * Cos(Abs(DGlob.Beta)))
        H3 = Tan(Acs(H2))
        F2 = (inv(DGlob.ALFAt) + DGlob.RulD / (DGlob.Mn * DGlob.Z * Cos(DGlob.ALFA)) - H3 + Atn(H3)) * DGlob.Mn * DGlob.Z
        DGlob.SON = PG * DGlob.Mn - F2
        'CALCOLO DEL FATTORE DI SPOSTAMENTO
        DGlob.FattoreX = (DGlob.SON - ((PG / 2) * DGlob.Mn)) / (2 * DGlob.Mn * Tan(DGlob.ALFA))
      End If
      'CALCOLO QUOTA WILDHABER
      If (DGlob.WbN > 0) Then
        DGlob.SONt = DGlob.SON / Cos(DGlob.Beta)
        Sb = (DGlob.SONt + DGlob.DP * inv(DGlob.ALFAt)) * DGlob.DB / DGlob.DP
        iB = PB - Sb
        DGlob.WbQ = DGlob.WbN * PB * Cos(Betab) - iB * Cos(Betab)
      Else
        DGlob.WbQ = 0
      End If
      'AGGIORNAMENTO VALORI
      Call SP("R[938]", frmt(DGlob.SON, 8))
      Call SP("QW", frmt(DGlob.WbQ, 8))
      Call SP("FattoreX", frmt(DGlob.FattoreX, 8))
      '
    Case 4 'Fattore X
      DGlob.FattoreX = Lp("FattoreX")
      'CALCOLO DELLO SPESSORE NORMALE
      DGlob.SON = 2 * DGlob.Mn * DGlob.FattoreX * Tan(DGlob.ALFA) + ((PG / 2) * DGlob.Mn)
      'CALCOLO QUOTA WILDHABER
      If (DGlob.WbN > 0) Then
        DGlob.SONt = DGlob.SON / Cos(DGlob.Beta)
        Sb = (DGlob.SONt + DGlob.DP * inv(DGlob.ALFAt)) * DGlob.DB / DGlob.DP
        iB = PB - Sb
        DGlob.WbQ = DGlob.WbN * PB * Cos(Betab) - iB * Cos(Betab)
        'CALCOLO QUOTA RULLI
        If (DGlob.RulD > 0) Then
          Wx = Cos(PG / (2 * DGlob.Z)) * DGlob.DB
          If Int(DGlob.Z / 2) = DGlob.Z / 2 Then Wx = DGlob.DB
          We = Sb - PB
          X = (We + DGlob.RulD / Cos(Betab)) / DGlob.DB
          Ymini = 0: Ymaxi = PG / 2
          For i = 1 To 25
            Ymoyen = (Ymini + Ymaxi) / 2: X1 = Tan(Ymoyen) - Ymoyen
            If X1 < X Then Ymini = Ymoyen Else Ymaxi = Ymoyen
          Next i
          DGlob.RulQ = (Wx / Cos(Ymoyen)) + DGlob.RulD
        Else
          DGlob.RulQ = 0
        End If
      Else
        DGlob.WbQ = 0
      End If
      'AGGIORNAMENTO VALORI
      Call SP("R[938]", frmt(DGlob.SON, 8))
      Call SP("QW", frmt(DGlob.WbQ, 8))
      Call SP("QR", frmt(DGlob.RulQ, 8))
      '
  End Select
  '
  'Spessore Riferimento per Controllo
  Dim DefSpeChk As Double
  Dim SONCHK As Double
  Dim WbQCHK As Double
  Dim WbNCHK As Double
  Dim RulQCHK As Double
  Dim RulDCHK As Double
  Dim SONtCHK As Double
  Dim SbCHK As Double
  Dim ibCHK As Double
  Dim WxCHK As Double
  Dim WeCHK As Double
  Dim XCHK As Double
  Dim E1CHK As Double
  Dim H2CHK As Double
  Dim H3CHK As Double
  Dim F2CHK As Double
  '
  If DGlob.DefSpe = 4 Then
     'Se TipoSpessore = "Fattore X" carico "SON"
     DefSpeChk = 1
  Else
     DefSpeChk = DGlob.DefSpe
  End If
  
  Call SP("iDefSpCHK", frmt(DefSpeChk, 2))
  
  SONCHK = Lp("SONCHK")
  WbQCHK = Lp("QWCHK")
  WbNCHK = Lp("ZWCHK")
  RulQCHK = Lp("QRCHK")
  RulDCHK = Lp("DRCHK")
  
  Select Case DefSpeChk
    '
    Case 1 'SON
      If (SONCHK > 0) Then
        'CALCOLO QUOTA WILDHABER
        If (WbNCHK > 0) Then
          SONtCHK = SONCHK / Cos(DGlob.Beta)
          SbCHK = (SONtCHK + DGlob.DP * inv(DGlob.ALFAt)) * DGlob.DB / DGlob.DP
          ibCHK = PB - SbCHK
          WbQCHK = WbNCHK * PB * Cos(Betab) - ibCHK * Cos(Betab)
          'CALCOLO QUOTA RULLI
          If (RulDCHK > 0) Then
            WxCHK = Cos(PG / (2 * DGlob.Z)) * DGlob.DB
            If Int(DGlob.Z / 2) = DGlob.Z / 2 Then WxCHK = DGlob.DB
            WeCHK = SbCHK - PB
            XCHK = (WeCHK + RulDCHK / Cos(Betab)) / DGlob.DB
            
            Ymini = 0: Ymaxi = PG / 2
            For i = 1 To 25
              Ymoyen = (Ymini + Ymaxi) / 2: X1 = Tan(Ymoyen) - Ymoyen
              If X1 < XCHK Then Ymini = Ymoyen Else Ymaxi = Ymoyen
            Next i
            RulQCHK = (WxCHK / Cos(Ymoyen)) + RulDCHK
          Else
            RulQCHK = 0
          End If
        Else
          WbQCHK = 0
        End If
        'AGGIORNAMENTO VALORI
        Call SP("QWCHK", frmt(WbQCHK, 4))
        Call SP("QRCHK", frmt(RulQCHK, 4))
      End If
      '
    Case 2 'Quota Wb
      If (WbNCHK > 0) And (WbQCHK > 0) Then
        ibCHK = WbNCHK * PB - WbQCHK / Cos(Betab)
        SbCHK = PB - ibCHK
        SONtCHK = DGlob.DP * (SbCHK / DGlob.DB - (inv(DGlob.ALFAt)))
        SONCHK = SONtCHK * Cos(DGlob.Beta)
        'CALCOLO QUOTA RULLI
        If (RulDCHK > 0) Then
          WxCHK = Cos(PG / (2 * DGlob.Z)) * DGlob.DB
          If Int(DGlob.Z / 2) = DGlob.Z / 2 Then WxCHK = DGlob.DB
          WeCHK = SbCHK - PB
          XCHK = (WeCHK + RulDCHK / Cos(Betab)) / DGlob.DB
          Ymini = 0: Ymaxi = PG / 2
          For i = 1 To 25
            Ymoyen = (Ymini + Ymaxi) / 2: X1 = Tan(Ymoyen) - Ymoyen
            If X1 < XCHK Then Ymini = Ymoyen Else Ymaxi = Ymoyen
          Next i
          RulQCHK = (WxCHK / Cos(Ymoyen)) + RulDCHK
        Else
          RulQCHK = 0
        End If
        'AGGIORNAMENTO VALORI
        Call SP("SONCHK", frmt(SONCHK, 4))
        Call SP("QRCHK", frmt(RulQCHK, 4))
      End If
      '
    Case 3 'Quota Rulli
      If (RulQCHK <> 0) And (RulDCHK <> 0) And (RulDCHK <> RulQCHK) Then
        E1CHK = PG / 2 / DGlob.Z
        If (Int(DGlob.Z / 2) = DGlob.Z / 2) Then E1CHK = 0
        H2CHK = DGlob.Mn * DGlob.Z * Cos(DGlob.ALFAt) * Cos(E1CHK) / ((RulQCHK - RulDCHK) * Cos(Abs(DGlob.Beta)))
        H3CHK = Tan(Acs(H2CHK))
        F2CHK = (inv(DGlob.ALFAt) + RulDCHK / (DGlob.Mn * DGlob.Z * Cos(DGlob.ALFA)) - H3CHK + Atn(H3CHK)) * DGlob.Mn * DGlob.Z
        SONCHK = PG * DGlob.Mn - F2CHK
      End If
      'CALCOLO QUOTA WILDHABER
      If (WbNCHK > 0) Then
        SONtCHK = SONCHK / Cos(DGlob.Beta)
        SbCHK = (SONtCHK + DGlob.DP * inv(DGlob.ALFAt)) * DGlob.DB / DGlob.DP
        ibCHK = PB - SbCHK
        WbQCHK = WbNCHK * PB * Cos(Betab) - ibCHK * Cos(Betab)
      Else
        WbQCHK = 0
      End If
      'AGGIORNAMENTO VALORI
      Call SP("SONCHK", frmt(SONCHK, 4))
      Call SP("QWCHK", frmt(WbQCHK, 4))
      '
      '
  End Select
  '
    
  ' *** Agg.23.11.2013 ***
  Dim CorrSpessore As Double
  Dim CorrInterasse As Double
  Dim NewRulQ As Double
  Dim NewSon As Double
  Dim CorrSon As Double
  Dim Indtav As Integer
  Dim ValoreVar As String

  
  For Indtav = 0 To 1
  
    ValoreVar = Lp_Str("iCorrSpessore[" + LTrim(str(Indtav)) + "]")
    If ValoreVar = "NOTFOUND" Then GoTo FineCorrInt
    
    CorrSpessore = Lp("iCorrSpessore[" + LTrim(str(Indtav)) + "]")
  
  'Calcolo Correzione Interasse da Correzione su SON , Wn , Qr
  Select Case DGlob.DefSpe
    Case 1 'SON
      CorrInterasse = Int(CorrSpessore / 2 / Tan(DGlob.ALFA) * 10000) / 10000
    
    Case 2 'Quota Wb
      CorrInterasse = Int(CorrSpessore / 2 / Tan(DGlob.ALFA) * 10000) / 10000
          
    Case 3 'Quota Rulli
      'Calcolo nuova quota rulli da quota rulli impostata
      If (DGlob.RulQ <> 0) And (DGlob.RulD <> 0) And (DGlob.RulD <> DGlob.RulQ) Then
        NewRulQ = DGlob.RulQ - CorrSpessore
        
        E1 = PG / 2 / DGlob.Z
        If (Int(DGlob.Z / 2) = DGlob.Z / 2) Then E1 = 0
        H2 = DGlob.Mn * DGlob.Z * Cos(DGlob.ALFAt) * Cos(E1) / ((NewRulQ - DGlob.RulD) * Cos(Abs(DGlob.Beta)))
        H3 = Tan(Acs(H2))
        F2 = (inv(DGlob.ALFAt) + DGlob.RulD / (DGlob.Mn * DGlob.Z * Cos(DGlob.ALFA)) - H3 + Atn(H3)) * DGlob.Mn * DGlob.Z
        NewSon = PG * DGlob.Mn - F2
      End If
      
      CorrSon = DGlob.SON - NewSon
      CorrInterasse = Int(CorrSon / 2 / Tan(DGlob.ALFA) * 10000) / 10000
  End Select
  
  'Aggiorno valore correttore interasse
  Call SP("CORINTLAV_G[" + LTrim(str(Indtav)) + ",1]", CorrInterasse)
  
  Next Indtav
FineCorrInt:
  '
  Dim LatoPart As Integer
  Dim nZone    As Integer
  '
  LatoPart = Lp("LatoPA")
  nZone = Lp("iNZone")
  '
  Dim INCLINAZIONE_MOLA_GRD  As Double
  Dim INCLINAZIONE_MOLA_RAD  As Double
  Dim MODULO_MOLA            As Double
  Dim PRINCIPI_MOLA          As Double
  Dim DIAMETRO_MOLA          As Double
  Dim DIAMETRO_MOLA_EXT_EFF  As Double ' DIametro mola esterno durante la rettifica
  Dim DIAMETRO_MOLA_INT_EFF  As Double ' Diametro mola di fondo usato durante la rettifica
  Dim DIAMETRO_MOLA_MED_EFF  As Double ' Diametro mola medio usato durante la rettifica
  Dim ALTEZZA_DENTEMOLA_EFF  As Double ' Altezza dente mola effettivamente usata in profilatura
  
  Dim ARGOMENTO              As Double
  Dim SENSO_FILETTO          As Integer
  Dim InclTesta              As Double
  Dim H_filetto              As Double
  
  SENSO_FILETTO = Lp("SENSOFIL_G") '
  MODULO_MOLA = Lp("MNUT_G")       'MnUtensileMV
  PRINCIPI_MOLA = Lp("NUMPRINC_G") 'NPrincipiMV
  
  DIAMETRO_MOLA = Lp("DIAMMOLA_G") 'Diametro Mola Nuova
  If DIAMETRO_MOLA <= 0 Then
     stmp = GetInfo("CALCOLO", "DIAMETRO_MOLA_NEW", Path_LAVORAZIONE_INI)
     DIAMETRO_MOLA = val(stmp) ' Letto da file INI
     Call SP("DIAMMOLA_G", DIAMETRO_MOLA)
  End If

  DIAMETRO_MOLA_EXT_EFF = 2 * val(OPC_LEGGI_DATO("/ACC/NCK/GUD7/REXTMOLA_G"))
  If (DIAMETRO_MOLA_EXT_EFF <= 0) And (DIAMETRO_MOLA > 0) Then
    DIAMETRO_MOLA_EXT_EFF = DIAMETRO_MOLA
  End If
  
    ALTEZZA_DENTEMOLA_EFF = Lp("HDENTEMOLA_G")
  If ALTEZZA_DENTEMOLA_EFF <= 0 Then
     'ALTEZZA_DENTEMOLA_EFF = 2.2 * MODULO_MOLA
     ALTEZZA_DENTEMOLA_EFF = 2.2 * DGlob.Mn
     H_filetto = ALTEZZA_DENTEMOLA_EFF
     Call SP("HDENTEMOLA_G", frmt(H_filetto, 2))
  End If
  
  DIAMETRO_MOLA_INT_EFF = 2 * val(OPC_LEGGI_DATO("/ACC/NCK/GUD7/RFONDOMOLA_G"))
  If (DIAMETRO_MOLA_INT_EFF <= 0) Then
      DIAMETRO_MOLA_INT_EFF = DIAMETRO_MOLA_EXT_EFF - (ALTEZZA_DENTEMOLA_EFF * 2)
  End If
    
  'Diametro mola effettivo preso sull'Addendum
  'DIAMETRO_MOLA_MED_EFF = DIAMETRO_MOLA_EXT_EFF - 2 * MODULO_MOLA
  DIAMETRO_MOLA_MED_EFF = DIAMETRO_MOLA_INT_EFF + 2 * ALTEZZA_DENTEMOLA_EFF - 2 * MODULO_MOLA
    
  'CALCOLO INCLINAZIONE FILETTO sul diametro mola medio
  ARGOMENTO = MODULO_MOLA * PRINCIPI_MOLA / DIAMETRO_MOLA_MED_EFF
  INCLINAZIONE_MOLA_RAD = Atn(ARGOMENTO / Sqr(1 - ARGOMENTO * ARGOMENTO))  'RADIANTI
  INCLINAZIONE_MOLA_GRD = INCLINAZIONE_MOLA_RAD / PG * 180                 'GRADI
  
  Dim HDenteMolaMV           As Double
  Dim PROFONDITA             As Double
  Dim AlfaMV                 As Double
  Dim Corsa_X1               As Double
  Dim Corsa_X2               As Double

  HDenteMolaMV = ALTEZZA_DENTEMOLA_EFF
  'PROFONDITA = (DGlob.DE - DGlob.Di) / 2
  stmp = GetInfo("CALCOLO", "PROFONDITA", Path_LAVORAZIONE_INI)
  PROFONDITA = val(stmp) ' Letto da file INI

  AlfaMV = Lp("ALFAUT_G")

'----------------------------------
' calcola angolo mola a vite
'----------------------------------

 Dim ApRast_IN As Double
 ApRast_IN = 0
 Dim param(1 To 20) As Double
 'Leggi par. rullo con Lp
 param(1) = Lp("AP_RULLO")
 param(2) = Lp("SP_RIF_RULLO")
 param(3) = Lp("BOMBATURA_RULLO")
 param(4) = Lp("DRULLOPROF_G")
 param(5) = Lp("RG_RIF_RULLO")
 param(6) = Lp("TIPRELIEF_DIS_RULLO")
 param(7) = Lp("TIPRELIEF_ANG_RULLO")
 param(8) = Lp("RG_RAC_TESTA_RULLO")
 'assegna par. profilo letti con Lp
 param(9) = ValSAP
 param(10) = Lp("BETA_G")
 param(11) = Lp("ALFA_G")
 param(12) = DGlob.Mn
 param(13) = DGlob.Z
 param(14) = DGlob.WbQ
 param(15) = DGlob.WbN
 param(16) = DGlob.DE
 param(17) = DGlob.DI
 
 param(18) = ValEAP
 param(19) = MODULO_MOLA
  
 Dim Checkinp As Boolean
 Checkinp = param(1) > 0 And param(2) > 0 And param(3) > 0 And param(4) > 0 And param(5) > 0 And param(8) > 0 And param(9) > 0 And param(11) > 0 And param(12) > 0 And param(13) > 0 And param(14) > 0 And param(15) > 0 And param(9) > 0 And param(17) > 0 And param(16) > 0 And param(18) > 0

If (Checkinp) Then
  
 If (FillParamMV( _
 param(1), _
 param(2), _
 param(3), _
 param(4), _
 param(5), _
 param(6), _
 param(7), _
 param(8), _
 ApRast_IN, _
 param(9), _
 param(10), _
 param(11), _
 param(12), _
 param(13), _
 param(14), _
 param(15), _
 param(16), _
 param(17), _
 param(18), _
 0)) Then

'--------------------------------------------------
' DATI SENSIBILI INGRANAMENTO MOLA RUOTA
' Specs(01) gioco fondo mola testa ingranaggio
' Specs(02) spostamento rullo
' Specs(03) bombatura ottenuta
' Specs(04) mola_modulo normale
' Specs(05) mola angolo pressione normale
' Specs(06) mola_spessore
' Specs(07) mola dedendum
' Specs(08) mola addendum per rispettare sap
' Specs(09) mola altezza dente minima
' Specs(10) Raggio Minimo Inviluppato da mola
' Specs(11) Ascissa spessore mola
' Specs(12) Ordinata spessore mola
' Specs(13) Diametro Rotolamento Mola
' Specs(14) Ordinata spessore mola
' Specs(15) Diametro Rotolamento Mola
'--------------------------------------------------
    Dim Specs(1 To 18): Call Get_WormWheel_Specs(Specs)
    Dim ApMVtmp As Double
    ApMVtmp = Int(Specs(5) * 10000) / 10000
    AlfaMV = Lp("ALFAUT_G")
    '    If (AlfaMV <> ApMVtmp) Then
    '    AlfaMV = ApMVtmp: Call SP("ALFAUT_G", frmt(ApMVtmp, 4), True)
    '    End If

    ' Calcolo parametri x antitwist
    If (Lp("AT_DRESSING") <> 0) And (Lp("BETA_G") <> 0) Then Call Calcolo_Parametri_AT

    '--------------------------------------------
 Else
    'Checkinp = False
    'GoTo errCalcoloParametri_MOLAVITE
    'Exit Sub
 End If
End If


  Dim PAX                   As Double
  Dim PTE                   As Double
  Dim PBT                   As Double
  Dim AE                    As Double
  Dim MODULO_MOLA_CORRETTO  As Double
    
  'Passo base
  PBT = DGlob.Mn * PG * Cos(DGlob.ALFA)
  'Angolo pressione filetto mola in radianti
  AE = AlfaMV * PG / 180
  
  'Aggiorna Modulo Utensile se cambia l'angolo pressione del filetto
  MODULO_MOLA_CORRETTO = Int(PBT / PG / Cos(AE) * 100000000) / 100000000
  If (MODULO_MOLA <> MODULO_MOLA_CORRETTO) Then
     MODULO_MOLA = MODULO_MOLA_CORRETTO
     Call SP("MNUT_G", frmt(MODULO_MOLA, 4))
     'CALCOLO INCLINAZIONE FILETTO con Diametro mola medio effettivo
     DIAMETRO_MOLA_MED_EFF = DIAMETRO_MOLA_INT_EFF + 2 * ALTEZZA_DENTEMOLA_EFF - 2 * MODULO_MOLA
     ARGOMENTO = MODULO_MOLA * PRINCIPI_MOLA / DIAMETRO_MOLA_MED_EFF
     INCLINAZIONE_MOLA_RAD = Atn(ARGOMENTO / Sqr(1 - ARGOMENTO * ARGOMENTO))  'RADIANTI
     INCLINAZIONE_MOLA_GRD = INCLINAZIONE_MOLA_RAD / PG * 180                 'GRADI
  End If
  
  'Calcolo Extra Corse
  'CALCOLO INCLINAZIONE Asse Porta mola in RADIANTI
  'CALCOLO INCLINAZIONE FILETTO con Diametro Mola massimo
  Dim InclinazioneFiletto As Double
  Dim ParTmp              As Double
  ParTmp = MODULO_MOLA * PRINCIPI_MOLA / DIAMETRO_MOLA
  InclinazioneFiletto = Atn(ParTmp / Sqr(1 - ParTmp * ParTmp))  'RADIANTI
  
  If (SENSO_FILETTO = 0) Then
    InclTesta = DGlob.Beta - InclinazioneFiletto
  Else
    InclTesta = DGlob.Beta + InclinazioneFiletto
  End If
  '
  Corsa_X1 = Tan(Abs(InclTesta)) * Sqr(PROFONDITA * (DIAMETRO_MOLA / Sin(InclTesta) / Sin(InclTesta) + Lp("DEXT_G") - PROFONDITA))
  Corsa_X1 = (Int(Corsa_X1 * 10)) / 10
  If (Corsa_X1 <= 2) Then Corsa_X1 = 2
  '
  Corsa_X2 = MODULO_MOLA * Sin(Abs(InclTesta)) / Tan(AlfaMV / 180 * PG)
  Corsa_X2 = (Int(Corsa_X2 * 10)) / 10
  If (Corsa_X2 <= 2) Then Corsa_X2 = 2
  '
  Call SP("iCORSAIN", Round(Corsa_X1 + 0.05, 1))
  Call SP("iCORSAOUT", Round(Corsa_X1 + 0.05, 1))
  Call SP("CORSAIN", Lp("iCORSAIN") + Lp("iCORSAIN_CORR"))
  Call SP("CORSAOUT", Lp("iCORSAOUT") + Lp("iCORSAOUT_CORR"))
  '******
  '
  Dim L1    As Double
  Dim L2    As Double
  Dim L3    As Double
  Dim G2    As Double
  Dim LSV   As Double
  Dim ac    As Double
  Dim Dpri  As Double
  Dim Hkw   As Double
  '
  Dim PASSO_ASSIALE_MOLA As Double
  '
  'CALCOLO PASSO ASSIALE
  PASSO_ASSIALE_MOLA = MODULO_MOLA * PRINCIPI_MOLA * PG / Cos(INCLINAZIONE_MOLA_RAD)
  '
  ac = Abs(INCLINAZIONE_MOLA_RAD - DGlob.Beta)
  L1 = (((DGlob.DE - ((DGlob.DE - DGlob.DI) / 2)) * (DGlob.DE - DGlob.DI) / 2) ^ 0.5) / Abs(Cos(ac))
  LSV = MODULO_MOLA * (PG / 2 + Tan(AlfaMV * PG / 180) / Cos(DGlob.Beta))
  Dpri = DGlob.Mn * DGlob.Z / Cos(DGlob.Beta)
  Hkw = (Dpri - DGlob.DI) / 2
  
  'Calcolo shift a inizio e fine mola
  If (AlfaMV <> 0) Then
  
  G2 = Hkw / Tan(AlfaMV * PG / 180)
  Else
  G2 = Hkw / Tan(DGlob.ALFA)
  End If
  L2 = G2 + LSV * 1.4
  '
  'Call SP("iSHIFTIN_G", Round(L1, 1))
  Call SP("iSHIFTIN_G", Round(L2, 1))
  Call SP("iSHIFTOUT_G", Round(L2, 1))
  Call SP("SHIFTIN_G", Lp("iSHIFTIN_G") + Lp("iSHIFTIN_G_CORR"))
  Call SP("SHIFTOUT_G", Lp("iSHIFTOUT_G") + Lp("iSHIFTOUT_G_CORR"))
  
'
'BIAS
  If (Lp("CORR_TW_KIND") = 0) Then
   Call DB_GUD_AT_Azzera
   iTW_TUNING = 0: Call SP("iTW_TUNING", 0)
   iTW_TUNING_corr = 0: Call SP("iTW_TUNING_corr", 0)
   Call SP("AT_DRESSING", 0)
  Else
   Call SP("AT_DRESSING", 1)
   Call DB_GUD_AT_Aggiorna
   iTW_TUNING = Lp("iTW_TUNING")
   iTW_TUNING_corr = Lp("iTW_TUNING_corr")
  End If
  TW_TUNING = iTW_TUNING + iTW_TUNING_corr: Call SP("TW_TUNING", TW_TUNING)
  '
  'Calcolo Posizione Inizio e Fine Shift Mola in funzione della direzione di Shift
   DirShift_Pre = Lp("iDirShiftPre")
  DirShift_Mola = Lp("DirShift")
         DPOSB1 = val(OPC_LEGGI_DATO("/ACC/NCK/GUD7/DPOSB1"))
       DPOSMOLA = val(OPC_LEGGI_DATO("/ACC/NCK/GUD7/DPOSMOLA"))
      PosW_Mola = DPOSB1 + DPOSMOLA 'Lp("WMOLA_G")

         L_Mola = Lp("LARGHMOLA_G")
   SHIFTIN_Mola = Lp("SHIFTIN_G")
  SHIFTOUT_Mola = Lp("SHIFTOUT_G")
  If (Lp("AT_DRESSING") = 1) Then
     Dim N_ZONE_FIN%, ZSGRLM%, cY_Zi#, Y_acc#, EXTENSION%
     N_ZONE_FIN = 1
         ZSGRLM = 1
          Y_acc = 0
      EXTENSION = 1
          cY_Zi = SHIFTIN_Mola
      If (cY_Zi > 9) Then
         cY_Zi = 9
      End If
      If (cY_Zi < 3) Then
         cY_Zi = 3
      End If
      Call MV_POS_INIZ_SH(DirShift_Mola, Lp("AT_DRESSING"), PosW_Mola, SHIFTIN_Mola, SHIFTOUT_Mola, L_Mola, N_ZONE_FIN, ZSGRLM, cY_Zi, Y_acc, EXTENSION, PosIn_Mola, PosFin_Mola, PosIn2_Mola, PosFin2_Mola)
  Else
      PosIn2_Mola = 0
      PosFin2_Mola = 0
      If DirShift_Mola < 0 Then
         PosIn_Mola = PosW_Mola - SHIFTIN_Mola
         PosFin_Mola = PosW_Mola - L_Mola + SHIFTOUT_Mola
      Else
        If DirShift_Mola > 0 Then
           PosFin_Mola = PosW_Mola - SHIFTIN_Mola
           PosIn_Mola = PosW_Mola - L_Mola + SHIFTOUT_Mola
        End If
      End If
  End If
  
  If DirShift_Mola <> DirShift_Pre Then
     DirShift_Pre = DirShift_Mola
     Call SP("iDirShiftPre", DirShift_Pre)
     'Aggiorno la Posizione Shift se cambia DirShift
     Call SP("POSSHIFT_G[2]", PosIn_Mola)
     PosShf = PosIn_Mola
     If MODALITA = "SPF" Then
        stmp = str(PosIn_Mola)
        Call OPC_SCRIVI_DATO("/ACC/NCK/UGUD/POSSHIFT_G[2]", stmp)
     End If
  End If
  
  Call SP("POSINIZIOSH_G", PosIn_Mola)
  Call SP("POSFINESH_G", PosFin_Mola)
  If MODALITA = "SPF" Then
     stmp = str(PosIn_Mola)
     Call OPC_SCRIVI_DATO("/ACC/NCK/UGUD/POSINIZIOSH_G", stmp)
     stmp = str(PosFin_Mola)
     Call OPC_SCRIVI_DATO("/ACC/NCK/UGUD/POSFINESH_G", stmp)
  End If
  
  If MODALITA = "OFF-LINE" Then
     PosShf = Lp("POSSHIFT_G[2]")
  End If
  If MODALITA = "SPF" Then
     PosShf = val(OPC_LEGGI_DATO("/ACC/NCK/UGUD/POSSHIFT_G[2]"))
  End If
  
  'Controllo quota PosShift[2]
  If DirShift_Mola < 0 Then
     If (PosShf > PosIn_Mola) Or (PosFin_Mola > PosShf) Then
         PosShf = PosIn_Mola
         Call SP("POSSHIFT_G[2]", PosIn_Mola)
         If MODALITA = "SPF" Then
            stmp = str(PosIn_Mola)
            Call OPC_SCRIVI_DATO("/ACC/NCK/UGUD/POSSHIFT_G[2]", stmp)
         End If
     End If
  Else
     If (PosShf < PosIn_Mola) Or (PosFin_Mola < PosShf) Then
         PosShf = PosIn_Mola
         Call SP("POSSHIFT_G[2]", PosIn_Mola)
         If MODALITA = "SPF" Then
            stmp = str(PosIn_Mola)
            Call OPC_SCRIVI_DATO("/ACC/NCK/UGUD/POSSHIFT_G[2]", stmp)
         End If
     End If
  End If
  
  'Aggiorno PosYPezzo:Posizione Y inizio Pezzo corrente
  stmp = str(PosShf)
  Call OPC_SCRIVI_DATO("/ACC/NCK/GUD7/PosYPezzo", stmp)
  
  
  Dim FasciaAMV As Double
  Dim FasciaBMV As Double
  Dim FasciaCMV As Double
  Dim FasciaA   As Double
  Dim FasciaB   As Double
  Dim FasciaC   As Double
  '
  'Calcolo delle Zone con extracorsa
  '
  Select Case nZone
      '
    Case 1
      Call SP("iFas2Fsx", 0): Call SP("iFas2Fdx", 0)
      Call SP("iFas3Fsx", 0): Call SP("iFas3Fdx", 0)
      Call SP("iCon2Fsx", 0): Call SP("iCon2Fdx", 0)
      Call SP("iCon3Fsx", 0): Call SP("iCon3Fdx", 0)
      Call SP("iBom2Fsx", 0): Call SP("iBom2Fdx", 0)
      Call SP("iBom3Fsx", 0): Call SP("iBom3Fdx", 0)
      FasciaAMV = Lp("CORSAIN") + Lp("iFas1Fsx") + Lp("CORSAOUT")
      FasciaBMV = 0
      FasciaCMV = 0
      FasciaA = Lp("iFas1Fsx")
      FasciaB = 0
      FasciaC = 0
      '
    Case 2
      Call SP("iFas3Fsx", 0): Call SP("iFas3Fdx", 0)
      Call SP("iCon3Fsx", 0): Call SP("iCon3Fdx", 0)
      Call SP("iBom3Fsx", 0): Call SP("iBom3Fdx", 0)
      FasciaAMV = Lp("CORSAIN") + Lp("iFas1Fsx")
      FasciaBMV = Lp("iFas2Fsx") + Lp("CORSAOUT")
      FasciaCMV = 0
      FasciaA = Lp("iFas1Fsx")
      FasciaB = Lp("iFas2Fsx")
      FasciaC = 0
      '
    Case 3
      FasciaAMV = Lp("CORSAIN") + Lp("iFas1Fsx")
      FasciaBMV = Lp("iFas2Fsx")
      FasciaCMV = Lp("iFas3Fsx") + Lp("CORSAOUT")
      FasciaA = Lp("iFas1Fsx")
      FasciaB = Lp("iFas2Fsx")
      FasciaC = Lp("iFas3Fsx")
      '
  End Select
  
Call SP("FASCIATOTALE_G", FasciaA + FasciaB + FasciaC)
  
'Leggo Fascia ed Extracorse
Dim CorsaTot   As Single
Dim ExcorsaIn  As Single
Dim ExcorsaOut As Single
Dim FASCIA     As Single

ExcorsaIn = Lp("CORSAIN")
ExcorsaOut = Lp("CORSAOUT")
FASCIA = Lp("FASCIATOTALE_G")
CorsaTot = ExcorsaIn + ExcorsaOut + FASCIA
    
'Calcolo Fascia controllata x correzione passo
PercentualeFascia = Lp("iFASCIALB_G")
FasciaFhb = PercentualeFascia * FASCIA / 100
Call SP("FASCIAFHB_G", FasciaFhb)
  
  
Dim AppBombXA As Double
Dim AppBombCA As Double
Dim AppConXA  As Double
Dim AppConCA  As Double
Dim AppBombXB As Double
Dim AppBombCB As Double
Dim AppConXB  As Double
Dim AppConCB  As Double
Dim AppBombXC As Double
Dim AppBombCC As Double
Dim AppConXC  As Double
Dim AppConCC  As Double

Dim BetaMV    As Double
Dim BombAMV   As Double
Dim BombAF2MV As Double
Dim ConAMV    As Double
Dim ConAF2MV  As Double

Dim BombBMV   As Double
Dim BombBF2MV As Double
Dim ConBMV    As Double
Dim ConBF2MV  As Double

Dim BombCMV   As Double
Dim BombCF2MV As Double
Dim ConCMV    As Double
Dim ConCF2MV  As Double
    '
BetaMV = Lp("BETA_G")
  
If FiancoSx = 1 Then
   'FiancoSx=F1
   'Controllo pezzo con lato serraggio in basso
    
    'TRATTO A
    BombAMV = Lp("iBom1Fsx") + Lp("iCorBom1Fsx") / 1000: BombAF2MV = Lp("iBom1Fdx") + Lp("iCorBom1Fdx") / 1000
     ConAMV = Lp("iCon1Fsx") + Lp("iCorCon1Fsx") / 1000: ConAF2MV = Lp("iCon1Fdx") + Lp("iCorCon1Fdx") / 1000
    
    'TRATTO B
    BombBMV = Lp("iBom2Fsx") + Lp("iCorBom2Fsx") / 1000: BombBF2MV = Lp("iBom2Fdx") + Lp("iCorBom2Fdx") / 1000
     ConBMV = Lp("iCon2Fsx") + Lp("iCorCon2Fsx") / 1000: ConBF2MV = Lp("iCon2Fdx") + Lp("iCorCon2Fdx") / 1000
    
    'TRATTO C
    BombCMV = Lp("iBom3Fsx") + Lp("iCorBom3Fsx") / 1000: BombCF2MV = Lp("iBom3Fdx") + Lp("iCorBom3Fdx") / 1000
     ConCMV = Lp("iCon3Fsx") + Lp("iCorCon3Fsx") / 1000: ConCF2MV = Lp("iCon3Fdx") + Lp("iCorCon3Fdx") / 1000
Else
   'FiancoSx=F2
    'Inverto solo le correzioni
   'Controllo pezzo con lato serraggio in alto
    
    'TRATTO A
    BombAF2MV = Lp("iBom1Fdx") + Lp("iCorBom1Fsx") / 1000: BombAMV = Lp("iBom1Fsx") + Lp("iCorBom1Fdx") / 1000
     ConAF2MV = Lp("iCon1Fdx") + Lp("iCorCon1Fsx") / 1000: ConAMV = Lp("iCon1Fsx") + Lp("iCorCon1Fdx") / 1000
        
    'TRATTO B
    BombBF2MV = Lp("iBom2Fdx") + Lp("iCorBom2Fsx") / 1000: BombBMV = Lp("iBom2Fsx") + Lp("iCorBom2Fdx") / 1000
     ConBF2MV = Lp("iCon2Fdx") + Lp("iCorCon2Fsx") / 1000: ConBMV = Lp("iCon2Fsx") + Lp("iCorCon2Fdx") / 1000
    
    'TRATTO C
    BombCF2MV = Lp("iBom3Fdx") + Lp("iCorBom3Fsx") / 1000: BombCMV = Lp("iBom3Fsx") + Lp("iCorBom3Fdx") / 1000
     ConCF2MV = Lp("iCon3Fdx") + Lp("iCorCon3Fsx") / 1000: ConCMV = Lp("iCon3Fsx") + Lp("iCorCon3Fdx") / 1000

End If

'
If (Lp("AT_DRESSING")) Then
  TW_TUNING = Lp("TW_TUNING")
    'TRATTO A
    BombAF2MV = Lp("iBom1Fdx") * (1 - TW_TUNING) + Lp("iCorBom1Fsx") / 1000: BombAMV = Lp("iBom1Fsx") * (1 - TW_TUNING) + Lp("iCorBom1Fdx") / 1000
    'BombAMV = BombAMV * (1 - TW_TUNING): BombAF2MV = BombAF2MV * (1 - TW_TUNING)
    'TRATTO B
    BombBF2MV = Lp("iBom2Fdx") * (1 - TW_TUNING) + Lp("iCorBom2Fsx") / 1000: BombBMV = Lp("iBom2Fsx") * (1 - TW_TUNING) + Lp("iCorBom2Fdx") / 1000
    'BombBMV = BombBMV * (1 - TW_TUNING): BombBF2MV = BombBF2MV * (1 - TW_TUNING)
    'TRATTO C
    BombCF2MV = Lp("iBom3Fdx") * (1 - TW_TUNING) + Lp("iCorBom3Fsx") / 1000: BombCMV = Lp("iBom3Fsx") * (1 - TW_TUNING) + Lp("iCorBom3Fdx") / 1000
    'BombCMV = BombCMV * (1 - TW_TUNING): BombCF2MV = BombCF2MV * (1 - TW_TUNING)
Else
 TW_TUNING = 0#
End If
'
'Calcolo per separare correzioni tra asse X e asse C
'
Call CalcolaBombConXC(AppBombXA, AppBombCA, AppConXA, AppConCA, BombAMV, BombAF2MV, ConAMV, ConAF2MV, BetaMV)
Call CalcolaBombConXC(AppBombXB, AppBombCB, AppConXB, AppConCB, BombBMV, BombBF2MV, ConBMV, ConBF2MV, BetaMV)
Call CalcolaBombConXC(AppBombXC, AppBombCC, AppConXC, AppConCC, BombCMV, BombCF2MV, ConCMV, ConCF2MV, BetaMV)
'
Dim BombFondoA!
Dim BombFondoB!
Dim BombFondoC!

Dim RaggioBOMB As Double

Dim ANGOLO      As Double
Dim CorsaRadA   As Double
Dim CorsaRadB   As Double
Dim CorsaRadC   As Double

' *** Agg 26.01.13
Dim RagBombCT1   As Double
Dim AppBombCT1  As Double
Dim AppConCT1   As Double
Dim RagBombCT2   As Double
Dim AppBombCT2  As Double
Dim AppConCT2   As Double
Dim RagBombCT3   As Double
Dim AppBombCT3  As Double
Dim AppConCT3   As Double

  If (FasciaA <> 0) Then
     If AppBombCA <> 0 Then
        RagBombCT1 = (FasciaA * FasciaA / (8 * Abs(AppBombCA))) + Abs(AppBombCA) / 2
        AppBombCT1 = Int(((2 * RagBombCT1 - Sqr(4 * RagBombCT1 * RagBombCT1 - FasciaAMV * FasciaAMV)) / 2) * 10000 + 0.5) / 10000
        If AppBombCA < 0 Then
           AppBombCT1 = -AppBombCT1
        End If
     Else
        AppBombCT1 = 0
     End If
     If AppConCA <> 0 Then
        AppConCT1 = AppConCA / FasciaA * FasciaAMV
        AppConCT1 = Int(AppConCT1 * 10000 + 0.5) / 10000
     Else
        AppConCT1 = 0
     End If
  Else
     AppBombCT1 = 0
     AppConCT1 = 0
  End If
  
  If (FasciaB <> 0) Then
     If AppBombCB <> 0 Then
        RagBombCT2 = (FasciaB * FasciaB / (8 * Abs(AppBombCB))) + Abs(AppBombCB) / 2
        AppBombCT2 = Int(((2 * RagBombCT2 - Sqr(4 * RagBombCT2 * RagBombCT2 - FasciaBMV * FasciaBMV)) / 2) * 10000 + 0.5) / 10000
        If AppBombCB < 0 Then
           AppBombCT2 = -AppBombCT2
        End If
     Else
        AppBombCT2 = 0
     End If
     If AppConCB <> 0 Then
        AppConCT2 = AppConCB / FasciaB * FasciaBMV
        AppConCT2 = Int(AppConCT2 * 10000 + 0.5) / 10000
     Else
        AppConCT2 = 0
     End If
  Else
     AppBombCT2 = 0
     AppConCT2 = 0
  End If
  
  If (FasciaC <> 0) Then
     If AppBombCC <> 0 Then
        RagBombCT3 = (FasciaC * FasciaC / (8 * Abs(AppBombCC))) + Abs(AppBombCC) / 2
        AppBombCT3 = Int(((2 * RagBombCT3 - Sqr(4 * RagBombCT3 * RagBombCT3 - FasciaCMV * FasciaCMV)) / 2) * 10000 + 0.5) / 10000
        If AppBombCC < 0 Then
           AppBombCT3 = -AppBombCT3
        End If
     Else
        AppBombCT3 = 0
     End If
     If AppConCC <> 0 Then
        AppConCT3 = AppConCC / FasciaC * FasciaCMV
        AppConCT3 = Int(AppConCT3 * 10000 + 0.5) / 10000
     Else
        AppConCT3 = 0
     End If
  Else
     AppBombCT3 = 0
     AppConCT3 = 0
  End If
  
  'Variabili appoggio usate per gestire le corr. elica diverse tra F1 e F2
  Call SP("RAPP[0,311]", frmt(AppBombCT1, 8))
  Call SP("RAPP[0,312]", frmt(AppConCT1, 8))
  Call SP("RAPP[0,314]", frmt(AppBombCT2, 8))
  Call SP("RAPP[0,315]", frmt(AppConCT2, 8))
  Call SP("RAPP[0,317]", frmt(AppBombCT3, 8))
  Call SP("RAPP[0,318]", frmt(AppConCT3, 8))
  
  'Angolo pressione pezzo in radianti
  ANGOLO = DGlob.ALFA
    
  'Modificato con ANGOLO (angolo pressione pezzo) al posto di AlfaMV  (angolo pressione mola)
  BombFondoA! = AppBombXA / Tan(ANGOLO): AppConXA = Int(AppConXA * 1000 + 0.5) / 1000
  BombFondoB! = AppBombXB / Tan(ANGOLO): AppConXB = Int(AppConXB * 1000 + 0.5) / 1000
  BombFondoC! = AppBombXC / Tan(ANGOLO): AppConXC = Int(AppConXC * 1000 + 0.5) / 1000
  
  '
  If (LatoPart = -1) Then 'Lato Testa portapezzo
      'TRATTO A
      If (BombFondoA! <> 0) Then
        RaggioBOMB = (FasciaA * FasciaA / (8 * Abs(BombFondoA!))) + Abs(BombFondoA!) / 2
        If (BombFondoA! > 0) Then
          RaggioBOMB = RaggioBOMB + DIAMETRO_MOLA / 2
        Else
          RaggioBOMB = RaggioBOMB - DIAMETRO_MOLA / 2
        End If
        RaggioBOMB = Int(RaggioBOMB * 1000 + 0.5) / 1000
        If (BombFondoA! > 0) Then RaggioBOMB = -RaggioBOMB
      Else
        RaggioBOMB = 0
      End If
      If (FasciaA <> 0) Then
      CorsaRadA = (AppConXA / Tan(ANGOLO)) / FasciaA * FasciaAMV
      CorsaRadA = Int(CorsaRadA * 10000 + 0.5) / 10000
      Else
      CorsaRadA = 0
      End If
      Call SP("CORSAASS1_G", FasciaAMV):     Call SP("CORSAASS1F2_G", FasciaAMV)
      Call SP("RAGGIOBOMB1_G", RaggioBOMB):  Call SP("RAGGIOBOMB1F2_G", RaggioBOMB)
      Call SP("CORSARAD1_G", CorsaRadA):     Call SP("CORSARAD1F2_G", CorsaRadA)
      'TRATTO B
      If (BombFondoB! <> 0) Then
        RaggioBOMB = (FasciaB * FasciaB / (8 * Abs(BombFondoB!))) + Abs(BombFondoB!) / 2
        If (BombFondoB! > 0) Then
          RaggioBOMB = RaggioBOMB + DIAMETRO_MOLA / 2
        Else
          RaggioBOMB = RaggioBOMB - DIAMETRO_MOLA / 2
        End If
        RaggioBOMB = Int(RaggioBOMB * 1000 + 0.5) / 1000
        If (BombFondoB! > 0) Then RaggioBOMB = -RaggioBOMB
      Else
        RaggioBOMB = 0
      End If
      If (FasciaB <> 0) Then
      CorsaRadB = (AppConXB / Tan(ANGOLO)) / FasciaB * FasciaBMV
      CorsaRadB = Int(CorsaRadB * 10000 + 0.5) / 10000
      Else
      CorsaRadB = 0
      End If
      Call SP("CORSAASS2_G", FasciaBMV):     Call SP("CORSAASS2F2_G", FasciaBMV)
      Call SP("RAGGIOBOMB2_G", RaggioBOMB):  Call SP("RAGGIOBOMB2F2_G", RaggioBOMB)
      Call SP("CORSARAD2_G", CorsaRadB):     Call SP("CORSARAD2F2_G", CorsaRadB)
      'TRATTO C
      If (BombFondoC! <> 0) Then
        RaggioBOMB = (FasciaC * FasciaC / (8 * Abs(BombFondoC!))) + Abs(BombFondoC!) / 2
        If (BombFondoC! > 0) Then
          RaggioBOMB = RaggioBOMB + DIAMETRO_MOLA / 2
        Else
          RaggioBOMB = RaggioBOMB - DIAMETRO_MOLA / 2
        End If
        RaggioBOMB = Int(RaggioBOMB * 1000 + 0.5) / 1000
        If (BombFondoC! > 0) Then RaggioBOMB = -RaggioBOMB
      Else
        RaggioBOMB = 0
      End If
      If (FasciaC <> 0) Then
      CorsaRadC = (AppConXC / Tan(ANGOLO)) / FasciaC * FasciaCMV
      CorsaRadC = Int(CorsaRadC * 10000 + 0.5) / 10000
      Else
      CorsaRadC = 0
      End If
      Call SP("CORSAASS3_G", FasciaCMV):     Call SP("CORSAASS3F2_G", FasciaCMV)
      Call SP("RAGGIOBOMB3_G", RaggioBOMB):  Call SP("RAGGIOBOMB3F2_G", RaggioBOMB)
      Call SP("CORSARAD3_G", CorsaRadC):     Call SP("CORSARAD3F2_G", CorsaRadC)
      '
  Else   'Lato Partenza Contropunta
    '
    Select Case nZone
    Case 3
      'TRATTO A -> Corsa 3
      If (BombFondoA! <> 0) Then
        RaggioBOMB = (FasciaA * FasciaA / (8 * Abs(BombFondoA!))) + Abs(BombFondoA!) / 2
        If (BombFondoA! > 0) Then
          RaggioBOMB = RaggioBOMB + DIAMETRO_MOLA / 2
        Else
          RaggioBOMB = RaggioBOMB - DIAMETRO_MOLA / 2
        End If
        RaggioBOMB = Int(RaggioBOMB * 1000 + 0.5) / 1000
        If (BombFondoA! > 0) Then RaggioBOMB = -RaggioBOMB
      Else
        RaggioBOMB = 0
      End If
      If (FasciaA <> 0) Then
      CorsaRadA = (AppConXA / Tan(ANGOLO)) / FasciaA * FasciaAMV
      CorsaRadA = Int(CorsaRadA * 10000 + 0.5) / 10000
      Else
      CorsaRadA = 0
      End If
      Call SP("CORSAASS3_G", FasciaAMV):     Call SP("CORSAASS3F2_G", FasciaAMV)
      Call SP("RAGGIOBOMB3_G", -RaggioBOMB): Call SP("RAGGIOBOMB3F2_G", -RaggioBOMB)
      Call SP("CORSARAD3_G", -CorsaRadA):    Call SP("CORSARAD3F2_G", -CorsaRadA)
      'TRATTO B -> Corsa 2
      If (BombFondoB! <> 0) Then
        RaggioBOMB = (FasciaB * FasciaB / (8 * Abs(BombFondoB!))) + Abs(BombFondoB!) / 2
        If (BombFondoB! > 0) Then
          RaggioBOMB = RaggioBOMB + DIAMETRO_MOLA / 2
        Else
          RaggioBOMB = RaggioBOMB - DIAMETRO_MOLA / 2
        End If
        RaggioBOMB = Int(RaggioBOMB * 1000 + 0.5) / 1000
        If (BombFondoB! > 0) Then RaggioBOMB = -RaggioBOMB
      Else
        RaggioBOMB = 0
      End If
      If (FasciaB <> 0) Then
      CorsaRadB = (AppConXB / Tan(ANGOLO)) / FasciaB * FasciaBMV
      CorsaRadB = Int(CorsaRadB * 10000 + 0.5) / 10000
      Else
      CorsaRadB = 0
      End If
      Call SP("CORSAASS2_G", FasciaBMV):     Call SP("CORSAASS2F2_G", FasciaBMV)
      Call SP("RAGGIOBOMB2_G", -RaggioBOMB): Call SP("RAGGIOBOMB2F2_G", -RaggioBOMB)
      Call SP("CORSARAD2_G", -CorsaRadB):    Call SP("CORSARAD2F2_G", -CorsaRadB)
      'TRATTO C -> Corsa 1
      If (BombFondoC! <> 0) Then
        RaggioBOMB = (FasciaC * FasciaC / (8 * Abs(BombFondoC!))) + Abs(BombFondoC!) / 2
        If (BombFondoC! > 0) Then
          RaggioBOMB = RaggioBOMB + DIAMETRO_MOLA / 2
        Else
          RaggioBOMB = RaggioBOMB - DIAMETRO_MOLA / 2
        End If
        RaggioBOMB = Int(RaggioBOMB * 1000 + 0.5) / 1000
        If (BombFondoC! > 0) Then RaggioBOMB = -RaggioBOMB
      Else
        RaggioBOMB = 0
      End If
      If (FasciaC <> 0) Then
      CorsaRadC = (AppConXC / Tan(ANGOLO)) / FasciaC * FasciaCMV
      CorsaRadC = Int(CorsaRadC * 10000 + 0.5) / 10000
      Else
      CorsaRadC = 0
      End If
      Call SP("CORSAASS1_G", FasciaCMV):     Call SP("CORSAASS1F2_G", FasciaCMV)
      Call SP("RAGGIOBOMB1_G", -RaggioBOMB): Call SP("RAGGIOBOMB1F2_G", -RaggioBOMB)
      Call SP("CORSARAD1_G", -CorsaRadC):    Call SP("CORSARAD1F2_G", -CorsaRadC)
    Case 2
      'TRATTO A -> Corsa 2
      If (BombFondoA! <> 0) Then
        RaggioBOMB = (FasciaA * FasciaA / (8 * Abs(BombFondoA!))) + Abs(BombFondoA!) / 2
        If (BombFondoA! > 0) Then
          RaggioBOMB = RaggioBOMB + DIAMETRO_MOLA / 2
        Else
          RaggioBOMB = RaggioBOMB - DIAMETRO_MOLA / 2
        End If
        RaggioBOMB = Int(RaggioBOMB * 1000 + 0.5) / 1000
        If (BombFondoA! > 0) Then RaggioBOMB = -RaggioBOMB
      Else
        RaggioBOMB = 0
      End If
      If (FasciaA <> 0) Then
      CorsaRadA = (AppConXA / Tan(ANGOLO)) / FasciaA * FasciaAMV
      CorsaRadA = Int(CorsaRadA * 10000 + 0.5) / 10000
      Else
      CorsaRadA = 0
      End If
      Call SP("CORSAASS2_G", FasciaAMV):     Call SP("CORSAASS2F2_G", FasciaAMV)
      Call SP("RAGGIOBOMB2_G", -RaggioBOMB): Call SP("RAGGIOBOMB2F2_G", -RaggioBOMB)
      Call SP("CORSARAD2_G", -CorsaRadA):    Call SP("CORSARAD2F2_G", -CorsaRadA)
      'TRATTO B -> Corsa 1
      If (BombFondoB! <> 0) Then
        RaggioBOMB = (FasciaB * FasciaB / (8 * Abs(BombFondoB!))) + Abs(BombFondoB!) / 2
        If (BombFondoB! > 0) Then
          RaggioBOMB = RaggioBOMB + DIAMETRO_MOLA / 2
        Else
          RaggioBOMB = RaggioBOMB - DIAMETRO_MOLA / 2
        End If
        RaggioBOMB = Int(RaggioBOMB * 1000 + 0.5) / 1000
        If (BombFondoB! > 0) Then RaggioBOMB = -RaggioBOMB
      Else
        RaggioBOMB = 0
      End If
      If (FasciaB <> 0) Then
      CorsaRadB = (AppConXB / Tan(ANGOLO)) / FasciaB * FasciaBMV
      CorsaRadB = Int(CorsaRadB * 10000 + 0.5) / 10000
      Else
      CorsaRadB = 0
      End If
      Call SP("CORSAASS1_G", FasciaBMV):     Call SP("CORSAASS1F2_G", FasciaBMV)
      Call SP("RAGGIOBOMB1_G", -RaggioBOMB): Call SP("RAGGIOBOMB1F2_G", -RaggioBOMB)
      Call SP("CORSARAD1_G", -CorsaRadB):    Call SP("CORSARAD1F2_G", -CorsaRadB)
      'TRATTO C
      Call SP("CORSAASS3_G", 0):             Call SP("CORSAASS3F2_G", 0)
      Call SP("RAGGIOBOMB3_G", 0):           Call SP("RAGGIOBOMB3F2_G", 0)
      Call SP("CORSARAD3_G", 0):             Call SP("CORSARAD3F2_G", 0)
      '
    Case 1
      'TRATTO A -> Corsa 1
      If (BombFondoA! <> 0) Then
        RaggioBOMB = (FasciaA * FasciaA / (8 * Abs(BombFondoA!))) + Abs(BombFondoA!) / 2
        If (BombFondoA! > 0) Then
          RaggioBOMB = RaggioBOMB + DIAMETRO_MOLA / 2
        Else
          RaggioBOMB = RaggioBOMB - DIAMETRO_MOLA / 2
        End If
        RaggioBOMB = Int(RaggioBOMB * 1000 + 0.5) / 1000
        If (BombFondoA! > 0) Then RaggioBOMB = -RaggioBOMB
      Else
        RaggioBOMB = 0
      End If
      If (FasciaA <> 0) Then
      CorsaRadA = (AppConXA / Tan(ANGOLO)) / FasciaA * FasciaAMV
      CorsaRadA = Int(CorsaRadA * 10000 + 0.5) / 10000
      Else
      CorsaRadA = 0
      End If
      Call SP("CORSAASS1_G", FasciaAMV):     Call SP("CORSAASS1F2_G", FasciaAMV)
      Call SP("RAGGIOBOMB1_G", -RaggioBOMB): Call SP("RAGGIOBOMB1F2_G", -RaggioBOMB)
      Call SP("CORSARAD1_G", -CorsaRadA):    Call SP("CORSARAD1F2_G", -CorsaRadA)
      'TRATTO B
      Call SP("CORSAASS2_G", 0):             Call SP("CORSAASS2F2_G", 0)
      Call SP("RAGGIOBOMB2_G", 0):           Call SP("RAGGIOBOMB2F2_G", 0)
      Call SP("CORSARAD2_G", 0):             Call SP("CORSARAD2F2_G", 0)
      'TRATTO C
      Call SP("CORSAASS3_G", 0):             Call SP("CORSAASS3F2_G", 0)
      Call SP("RAGGIOBOMB3_G", 0):           Call SP("RAGGIOBOMB3F2_G", 0)
      Call SP("CORSARAD3_G", 0):             Call SP("CORSARAD3F2_G", 0)
      '
    End Select
  End If
  
  'Gestione correzione elica nel caso di eliche differenti tra F1 e F2
  If DbetaF1 <> 0 Then
  If (LatoPart = -1) Then
   'Lato Testa portapezzo
   'TRATTO A
      If (FasciaA <> 0) Then
          CorConTA = FasciaA * Tan(DbetaF1)  'Correzione conicit� zona A fianco 1
          'Nota la correzione x fianco 2 � identica al fianco 1 dovendo rettificare due fianchi insieme
          CorsaRadTA = -LatoPart * (CorConTA / Tan(ANGOLO)) / FasciaA * FasciaAMV
          CorsaRadTA = Int(CorsaRadTA * 10000 + 0.5) / 10000
      Else
          CorsaRadTA = 0
      End If
      Call SP("CORSARADT1_G", CorsaRadTA)
   'TRATTO B
      If (FasciaB <> 0) Then
          CorConTB = FasciaB * Tan(DbetaF1)  'Correzione conicit� zona B fianco 1
          'Nota la correzione x fianco 2 � identica al fianco 1 dovendo rettificare due fianchi insieme
          CorsaRadTB = -LatoPart * (CorConTB / Tan(ANGOLO)) / FasciaB * FasciaBMV
          CorsaRadTB = Int(CorsaRadTB * 10000 + 0.5) / 10000
      Else
          CorsaRadTB = 0
      End If
      Call SP("CORSARADT2_G", CorsaRadTB)
   'TRATTO C
      If (FasciaC <> 0) Then
          CorConTC = FasciaC * Tan(DbetaF1)  'Correzione conicit� zona C fianco 1
          'Nota la correzione x fianco 2 � identica al fianco 1 dovendo rettificare due fianchi insieme
          CorsaRadTC = -LatoPart * (CorConTC / Tan(ANGOLO)) / FasciaC * FasciaCMV
          CorsaRadTC = Int(CorsaRadTC * 10000 + 0.5) / 10000
      Else
          CorsaRadTC = 0
      End If
      Call SP("CORSARADT3_G", CorsaRadTC)
  Else
   'Lato Contropunta
   Select Case nZone
   Case 3
   'TRATTO A -->Corsa 3
      If (FasciaA <> 0) Then
          CorConTA = FasciaA * Tan(DbetaF1)  'Correzione conicit� zona A fianco 1
          'Nota la correzione x fianco 2 � identica al fianco 1 dovendo rettificare due fianchi insieme
          CorsaRadTA = -LatoPart * (CorConTA / Tan(ANGOLO)) / FasciaA * FasciaAMV
          CorsaRadTA = Int(CorsaRadTA * 10000 + 0.5) / 10000
      Else
          CorsaRadTA = 0
      End If
      Call SP("CORSARADT3_G", CorsaRadTA)
   
   'TRATTO B -->Corsa 2
      If (FasciaB <> 0) Then
          CorConTB = FasciaB * Tan(DbetaF1)  'Correzione conicit� zona B fianco 1
          'Nota la correzione x fianco 2 � identica al fianco 1 dovendo rettificare due fianchi insieme
          CorsaRadTB = -LatoPart * (CorConTB / Tan(ANGOLO)) / FasciaB * FasciaBMV
          CorsaRadTB = Int(CorsaRadTB * 10000 + 0.5) / 10000
      Else
          CorsaRadTB = 0
      End If
      Call SP("CORSARADT2_G", CorsaRadTB)
   
   'TRATTO C -->Corsa 1
      If (FasciaC <> 0) Then
          CorConTC = FasciaC * Tan(DbetaF1)  'Correzione conicit� zona C fianco 1
          'Nota la correzione x fianco 2 � identica al fianco 1 dovendo rettificare due fianchi insieme
          CorsaRadTC = -LatoPart * (CorConTC / Tan(ANGOLO)) / FasciaC * FasciaCMV
          CorsaRadTC = Int(CorsaRadTC * 10000 + 0.5) / 10000
      Else
          CorsaRadTC = 0
      End If
      Call SP("CORSARADT1_G", CorsaRadTC)
      
   Case 2
   
   'TRATTO A -->Corsa 2
      If (FasciaA <> 0) Then
          CorConTA = FasciaA * Tan(DbetaF1)  'Correzione conicit� zona A fianco 1
          'Nota la correzione x fianco 2 � identica al fianco 1 dovendo rettificare due fianchi insieme
          CorsaRadTA = -LatoPart * (CorConTA / Tan(ANGOLO)) / FasciaA * FasciaAMV
          CorsaRadTA = Int(CorsaRadTA * 10000 + 0.5) / 10000
      Else
          CorsaRadTA = 0
      End If
      Call SP("CORSARADT2_G", CorsaRadTA)
   
   'TRATTO B -->Corsa 1
      If (FasciaB <> 0) Then
          CorConTB = FasciaB * Tan(DbetaF1)  'Correzione conicit� zona B fianco 1
          'Nota la correzione x fianco 2 � identica al fianco 1 dovendo rettificare due fianchi insieme
          CorsaRadTB = -LatoPart * (CorConTB / Tan(ANGOLO)) / FasciaB * FasciaBMV
          CorsaRadTB = Int(CorsaRadTB * 10000 + 0.5) / 10000
      Else
          CorsaRadTB = 0
      End If
      Call SP("CORSARADT1_G", CorsaRadTB)
   
   'TRATTO C
      Call SP("CORSARADT3_G", 0)
      
   Case 1
   'TRATTO A -->Corsa 1
      If (FasciaA <> 0) Then
          CorConTA = FasciaA * Tan(DbetaF1)  'Correzione conicit� zona A fianco 1
          'Nota la correzione x fianco 2 � identica al fianco 1 dovendo rettificare due fianchi insieme
          CorsaRadTA = -LatoPart * (CorConTA / Tan(ANGOLO)) / FasciaA * FasciaAMV
          CorsaRadTA = Int(CorsaRadTA * 10000 + 0.5) / 10000
      Else
          CorsaRadTA = 0
      End If
      Call SP("CORSARADT1_G", CorsaRadTA)
   'TRATTO B
      Call SP("CORSARADT2_G", 0)
   'TRATTO C
      Call SP("CORSARADT3_G", 0)
   
   End Select
  End If
  Else
      Call SP("CORSARADT1_G", 0)
      Call SP("CORSARADT2_G", 0)
      Call SP("CORSARADT3_G", 0)
  End If
  '
  'Gestione correzione elica automatica da correzione di passo
  '
  'FiancoSx = F1
   ActualErrSXFHB = Lp("iERRSXFHB")                        'Errore del grafico
   If ActualErrSXFHB <> 0 Then
      CorrFHBSX = Lp("ERRTOTSXFHB_G")                      'leggo Errore cumulato
      Call SP("iERRPRESXFHB", CorrFHBSX)                  'Errore cumulato precedente
      Call SP("ERRTOTSXFHB_G", CorrFHBSX + ActualErrSXFHB) 'Scrivo nuovo errore cumulato
      ActualErrSXFHB = 0
      Call SP("iERRSXFHB", ActualErrSXFHB)
   End If

  'FiancoDx = F2
   ActualErrDXFHB = Lp("iERRDXFHB")        'Errore del grafico
   If ActualErrDXFHB <> 0 Then
      CorrFHBDX = Lp("ERRTOTDXFHB_G")      'leggo Errore cumulato
      Call SP("iERRPREDXFHB", CorrFHBDX)  'Errore cumulato precedente
      Call SP("ERRTOTDXFHB_G", CorrFHBDX + ActualErrDXFHB)
      ActualErrDXFHB = 0
      Call SP("iERRDXFHB", ActualErrDXFHB)
   End If
  
  'Calcolo errore passo medio
  ErrPassoSx = Lp("ERRTOTSXFHB_G") / 1000
  ErrPassoDx = Lp("ERRTOTDXFHB_G") / 1000
  ErrPasso = (ErrPassoSx + ErrPassoDx) / 2
  Call SP("CORRPASSO", ErrPasso)
  If MODALITA = "SPF" Then
     stmp = frmt(ErrPasso, 4)
     Call OPC_SCRIVI_DATO("/ACC/NCK/UGUD/CORRPASSO", stmp)
  End If
  
  If FiancoSx = 1 Then
     ErrPassoF1 = ErrPassoSx
     ErrPassoF2 = ErrPassoDx
  Else
     ErrPassoF1 = ErrPassoDx
     ErrPassoF2 = ErrPassoSx
  End If
  
  'Calcolo correzioni conicit� per entrambi i fianchi
  If DGlob.Beta >= 0 Then
     'fHb residuo
     fHb_F1 = ErrPassoF1 - ErrPasso
     fHb_F2 = ErrPassoF2 - ErrPasso
     'Valore di correzione conicit� riferita alla fascia controllata (FasciaFhb)
     Con_F1 = -fHb_F1
     Con_F2 = fHb_F2
  End If
  
  If DGlob.Beta < 0 Then
     fHb_F1 = ErrPassoF1 - ErrPasso
     fHb_F2 = ErrPassoF2 - ErrPasso
     Con_F1 = fHb_F1
     Con_F2 = -fHb_F2
  End If
  
  Dim ConTot_F1    As Double
  Dim ConTot_F2    As Double
  Dim ConA_F1      As Double
  Dim ConA_F2      As Double
  Dim ConB_F1      As Double
  Dim ConB_F2      As Double
  Dim ConC_F1      As Double
  Dim ConC_F2      As Double
  
  'Estrapolare le corr. conicit� Con_F1 e Con_F2 al 100% della fascia
  ConTot_F1 = Con_F1 / FasciaFhb * FASCIA
  ConTot_F2 = Con_F2 / FasciaFhb * FASCIA

  Select Case nZone
     Case 1
        'Correzione su Zona 1
        ConA_F1 = ConTot_F1 / FASCIA * FasciaA
        ConA_F2 = ConTot_F2 / FASCIA * FasciaA
        'Correzione compreso extracorse
        Call SP("iCONASXFHB", ConA_F1 / FasciaA * FasciaAMV)
        Call SP("iCONADXFHB", ConA_F2 / FasciaA * FasciaAMV)
        Call SP("iCONBSXFHB", 0)
        Call SP("iCONBDXFHB", 0)
        Call SP("iCONCSXFHB", 0)
        Call SP("iCONCDXFHB", 0)
     
     Case 2
        'Correzione su Zona 1
        ConA_F1 = ConTot_F1 / FASCIA * FasciaA
        ConA_F2 = ConTot_F2 / FASCIA * FasciaA
        'Correzione su Zona 2
        ConB_F1 = ConTot_F1 / FASCIA * FasciaB
        ConB_F2 = ConTot_F2 / FASCIA * FasciaB
        '
        Call SP("iCONASXFHB", ConA_F1 / FasciaA * FasciaAMV)
        Call SP("iCONADXFHB", ConA_F2 / FasciaA * FasciaAMV)
        Call SP("iCONBSXFHB", ConB_F1 / FasciaB * FasciaBMV)
        Call SP("iCONBDXFHB", ConB_F2 / FasciaB * FasciaBMV)
        Call SP("iCONCSXFHB", 0)
        Call SP("iCONCDXFHB", 0)
     Case 3
        'Correzione su Zona 1
        ConA_F1 = ConTot_F1 / FASCIA * FasciaA
        ConA_F2 = ConTot_F2 / FASCIA * FasciaA
        'Correzione su Zona 2
        ConB_F1 = ConTot_F1 / FASCIA * FasciaB
        ConB_F2 = ConTot_F2 / FASCIA * FasciaB
        'Correzione su Zona 3
        ConC_F1 = ConTot_F1 / FASCIA * FasciaC
        ConC_F2 = ConTot_F2 / FASCIA * FasciaC
        '
        Call SP("iCONASXFHB", ConA_F1 / FasciaA * FasciaAMV)
        Call SP("iCONADXFHB", ConA_F2 / FasciaA * FasciaAMV)
        Call SP("iCONBSXFHB", ConB_F1 / FasciaB * FasciaBMV)
        Call SP("iCONBDXFHB", ConB_F2 / FasciaB * FasciaBMV)
        Call SP("iCONCSXFHB", ConC_F1 / FasciaC * FasciaCMV)
        Call SP("iCONCDXFHB", ConC_F2 / FasciaC * FasciaCMV)
  End Select
  
  'Considero  solo la correzione del fianco Sx che � complementare al fDx dovendo rettificare due fianchi insieme
  iConAfHb = Lp("iCONASXFHB")
  iConBfHb = Lp("iCONBSXFHB")
  iConCfHb = Lp("iCONCSXFHB")
  
  If (LatoPart = -1) Then
   'Lato Testa portapezzo
   'TRATTO A
      If (FasciaA <> 0) Then
          'Nota la correzione x fianco 2 � identica al fianco 1 dovendo rettificare due fianchi insieme
          CorsaRadZA = -LatoPart * (iConAfHb / Tan(ANGOLO))  '/ FasciaA * FasciaAMV
          CorsaRadZA = Int(CorsaRadZA * 10000 + 0.5) / 10000
      Else
          CorsaRadZA = 0
      End If
      Call SP("CORX1FHB", CorsaRadZA)
      If MODALITA = "SPF" Then
         stmp = Trim$(frmt(CorsaRadZA, 4))
         Call OPC_SCRIVI_DATO("/ACC/NCK/GUD7/CORX1FHB", stmp)
      End If
   'TRATTO B
      If (FasciaB <> 0) Then
          'Nota la correzione x fianco 2 � identica al fianco 1 dovendo rettificare due fianchi insieme
          CorsaRadZB = -LatoPart * (iConBfHb / Tan(ANGOLO))  '/ FasciaB * FasciaBMV
          CorsaRadZB = Int(CorsaRadZB * 10000 + 0.5) / 10000
      Else
          CorsaRadZB = 0
      End If
      Call SP("CORX2FHB", CorsaRadZB)
      If MODALITA = "SPF" Then
         stmp = Trim$(frmt(CorsaRadZB, 4))
        Call OPC_SCRIVI_DATO("/ACC/NCK/GUD7/CORX2FHB", stmp)
      End If
   'TRATTO C
      If (FasciaC <> 0) Then
          'Nota la correzione x fianco 2 � identica al fianco 1 dovendo rettificare due fianchi insieme
          CorsaRadZC = -LatoPart * (iConCfHb / Tan(ANGOLO))  '/ FasciaC * FasciaCMV
          CorsaRadZC = Int(CorsaRadZC * 10000 + 0.5) / 10000
      Else
          CorsaRadZC = 0
      End If
      Call SP("CORX3FHB", CorsaRadZC)
      If MODALITA = "SPF" Then
         stmp = Trim$(frmt(CorsaRadZC, 4))
         Call OPC_SCRIVI_DATO("/ACC/NCK/GUD7/CORX3FHB", stmp)
      End If
  Else
   'Lato Contropunta
   Select Case nZone
   Case 3
   'TRATTO A -->Corsa 3
      If (FasciaA <> 0) Then
          'Nota la correzione x fianco 2 � identica al fianco 1 dovendo rettificare due fianchi insieme
          CorsaRadZA = -LatoPart * (iConAfHb / Tan(ANGOLO)) '/ FasciaA * FasciaAMV
          CorsaRadZA = Int(CorsaRadZA * 10000 + 0.5) / 10000
      Else
          CorsaRadZA = 0
      End If
      Call SP("CORX3FHB", CorsaRadZA)
      If MODALITA = "SPF" Then
         stmp = Trim$(frmt(CorsaRadZA, 4))
         Call OPC_SCRIVI_DATO("/ACC/NCK/GUD7/CORX3FHB", stmp)
      End If
   'TRATTO B -->Corsa 2
      If (FasciaB <> 0) Then
          'Nota la correzione x fianco 2 � identica al fianco 1 dovendo rettificare due fianchi insieme
          CorsaRadZB = -LatoPart * (iConBfHb / Tan(ANGOLO)) '/ FasciaB * FasciaBMV
          CorsaRadZB = Int(CorsaRadZB * 10000 + 0.5) / 10000
      Else
          CorsaRadZB = 0
      End If
      Call SP("CORX2FHB", CorsaRadZB)
      If MODALITA = "SPF" Then
         stmp = Trim$(frmt(CorsaRadZB, 4))
         Call OPC_SCRIVI_DATO("/ACC/NCK/GUD7/CORX2FHB", stmp)
      End If
   'TRATTO C -->Corsa 1
      If (FasciaC <> 0) Then
          'Nota la correzione x fianco 2 � identica al fianco 1 dovendo rettificare due fianchi insieme
          CorsaRadZC = -LatoPart * (iConCfHb / Tan(ANGOLO)) '/ FasciaC * FasciaCMV
          CorsaRadZC = Int(CorsaRadZC * 10000 + 0.5) / 10000
      Else
          CorsaRadZC = 0
      End If
      Call SP("CORX1FHB", CorsaRadZC)
      If MODALITA = "SPF" Then
         stmp = Trim$(frmt(CorsaRadZC, 4))
         Call OPC_SCRIVI_DATO("/ACC/NCK/GUD7/CORX1FHB", stmp)
      End If
   Case 2
   
   'TRATTO A -->Corsa 2
      If (FasciaA <> 0) Then
          'Nota la correzione x fianco 2 � identica al fianco 1 dovendo rettificare due fianchi insieme
          CorsaRadZA = -LatoPart * (iConAfHb / Tan(ANGOLO)) '/ FasciaA * FasciaAMV
          CorsaRadZA = Int(CorsaRadZA * 10000 + 0.5) / 10000
      Else
          CorsaRadZA = 0
      End If
      Call SP("CORX2FHB", CorsaRadZA)
      If MODALITA = "SPF" Then
         stmp = Trim$(frmt(CorsaRadZA, 4))
         Call OPC_SCRIVI_DATO("/ACC/NCK/GUD7/CORX2FHB", stmp)
      End If
   'TRATTO B -->Corsa 1
      If (FasciaB <> 0) Then
          'Nota la correzione x fianco 2 � identica al fianco 1 dovendo rettificare due fianchi insieme
          CorsaRadZB = -LatoPart * (iConBfHb / Tan(ANGOLO)) '/ FasciaB * FasciaBMV
          CorsaRadZB = Int(CorsaRadZB * 10000 + 0.5) / 10000
      Else
          CorsaRadZB = 0
      End If
      Call SP("CORX1FHB", CorsaRadZB)
      If MODALITA = "SPF" Then
         stmp = Trim$(frmt(CorsaRadZB, 4))
         Call OPC_SCRIVI_DATO("/ACC/NCK/GUD7/CORX1FHB", stmp)
      End If
   'TRATTO C
      Call SP("CORX3FHB", 0)
      If MODALITA = "SPF" Then
         stmp = "0"
         Call OPC_SCRIVI_DATO("/ACC/NCK/GUD7/CORX3FHB", stmp)
      End If
   Case 1
   'TRATTO A -->Corsa 1
      If (FasciaA <> 0) Then
          'Nota la correzione x fianco 2 � identica al fianco 1 dovendo rettificare due fianchi insieme
          CorsaRadZA = -LatoPart * (iConAfHb / Tan(ANGOLO)) '/ FasciaA * FasciaAMV
          CorsaRadZA = Int(CorsaRadZA * 10000 + 0.5) / 10000
      Else
          CorsaRadZA = 0
      End If
      Call SP("CORX1FHB", CorsaRadZA)
      If MODALITA = "SPF" Then
         stmp = Trim$(frmt(CorsaRadZA, 4))
         Call OPC_SCRIVI_DATO("/ACC/NCK/GUD7/CORX1FHB", stmp)
      End If
   'TRATTO B
      Call SP("CORX2FHB", 0)
      If MODALITA = "SPF" Then
         stmp = "0"
         Call OPC_SCRIVI_DATO("/ACC/NCK/GUD7/CORX2FHB", stmp)
      End If
   'TRATTO C
      Call SP("CORX3FHB", 0)
      If MODALITA = "SPF" Then
         stmp = "0"
         Call OPC_SCRIVI_DATO("/ACC/NCK/GUD7/CORX3FHB", stmp)
      End If
   End Select
  End If
  
  '
  RaggioBOMB = Lp("CORSARAD1_G") + Lp("CORSARAD2_G") + Lp("CORSARAD3_G")
  Call SP("MEZZACORSARAD_G", Int(RaggioBOMB * 1000 / 2 + 0.5) / 1000)
  'Calcola e disegna evolvente
  Call CALCOLO_INGRANAGGIO_ESTERNO_MOLAVITE
'
'
Dim ShiftC1    As Single
Dim ShiftC1Pas As Single
Dim ShiftC2    As Single
Dim ShiftC2Pas As Single
Dim ShiftC3    As Single
Dim ShiftC3Pas As Single
Dim ShiftC4    As Single
Dim ShiftC4Pas As Single

'Aggiorno valore dello shift x passata da shift x mm

ShiftC1 = Lp("CYC[0,5]")
ShiftC1Pas = ShiftC1 * CorsaTot
ShiftC1Pas = Int(ShiftC1Pas * 100) / 100
Call SP("SHF1", ShiftC1Pas)

ShiftC2 = Lp("CYC[0,25]")
ShiftC2Pas = ShiftC2 * CorsaTot
ShiftC2Pas = Int(ShiftC2Pas * 100) / 100
Call SP("SHF2", ShiftC2Pas)

ShiftC3 = Lp("CYC[0,45]")
ShiftC3Pas = ShiftC3 * CorsaTot
ShiftC3Pas = Int(ShiftC3Pas * 100) / 100
Call SP("SHF3", ShiftC3Pas)

ShiftC4 = Lp("CYC[0,65]")
ShiftC4Pas = ShiftC4 * CorsaTot
ShiftC4Pas = Int(ShiftC4Pas * 100) / 100
Call SP("SHF4", ShiftC4Pas)

'Gestione correzioni fHa Nuova
Dim CorFHA1mm As Double
Dim CorFHA2mm As Double

If FiancoSx = 1 Then
   'Controllo con lato serraggio in Basso
   'FiancoSx = F1
   ActualCorrF1 = Lp("iERRFHAFSX")
   
   If ActualCorrF1 <> 0 Then
      CorrfHaF1 = Lp("COREVFHASX_G")
      Call SP("iCORPREFHAFSX", CorrfHaF1)
      tmp = CorrfHaF1 + ActualCorrF1
      CorFHA1mm = tmp / 1000
      Call SP("CORFHA1", CorFHA1mm)
      If MODALITA = "SPF" Then
         stmp = frmt(CorFHA1mm, 4)
         Call OPC_SCRIVI_DATO("/ACC/NCK/UGUD/CORFHA1", stmp)
      End If
      Call SP("COREVFHASX_G", tmp)
      ActualCorrF1 = 0
      Call SP("iERRFHAFSX", ActualCorrF1)
   Else
      CorrfHaF1 = Lp("COREVFHASX_G")
      CorFHA1mm = CorrfHaF1 / 1000
      Call SP("CORFHA1", CorFHA1mm)
      If MODALITA = "SPF" Then
         stmp = frmt(CorFHA1mm, 4)
         Call OPC_SCRIVI_DATO("/ACC/NCK/UGUD/CORFHA1", stmp)
      End If
   End If

   'FiancoDx = F2
   ActualCorrF2 = Lp("iERRFHAFDX")
   If ActualCorrF2 <> 0 Then
      CorrfHaF2 = Lp("COREVFHADX_G")
      Call SP("iCORPREFHAFDX", CorrfHaF2)
      tmp = CorrfHaF2 + ActualCorrF2
      CorFHA2mm = tmp / 1000
      Call SP("CORFHA2", CorFHA2mm)
      If MODALITA = "SPF" Then
         stmp = frmt(CorFHA2mm, 4)
         Call OPC_SCRIVI_DATO("/ACC/NCK/UGUD/CORFHA2", stmp)
      End If
      Call SP("COREVFHADX_G", tmp)
      ActualCorrF2 = 0
      Call SP("iERRFHAFDX", ActualCorrF2)
   Else
      CorrfHaF2 = Lp("COREVFHADX_G")
      CorFHA2mm = CorrfHaF2 / 1000
      Call SP("CORFHA2", CorFHA2mm)
      If MODALITA = "SPF" Then
         stmp = frmt(CorFHA2mm, 4)
         Call OPC_SCRIVI_DATO("/ACC/NCK/UGUD/CORFHA2", stmp)
      End If
   End If

Else
   'Controllo con lato serraggio in Alto
   'FiancoSx = F2
   ActualCorrF2 = Lp("iERRFHAFSX")
   If ActualCorrF2 <> 0 Then
      CorrfHaF2 = Lp("COREVFHASX_G")
      Call SP("iCORPREFHAFSX", CorrfHaF2)
      tmp = CorrfHaF2 + ActualCorrF2
      CorFHA2mm = tmp / 1000
      Call SP("CORFHA2", CorFHA2mm)
      If MODALITA = "SPF" Then
         stmp = frmt(CorFHA2mm, 4)
         Call OPC_SCRIVI_DATO("/ACC/NCK/UGUD/CORFHA2", stmp)
      End If
      Call SP("COREVFHASX_G", tmp)
      ActualCorrF2 = 0
      Call SP("iERRFHAFSX", ActualCorrF2)
   Else
      CorrfHaF2 = Lp("COREVFHASX_G")
      CorFHA2mm = CorrfHaF2 / 1000
      Call SP("CORFHA2", CorFHA2mm)
      If MODALITA = "SPF" Then
         stmp = frmt(CorFHA2mm, 4)
         Call OPC_SCRIVI_DATO("/ACC/NCK/UGUD/CORFHA2", stmp)
      End If
   End If
   
   'FiancoDx = F1
   ActualCorrF1 = Lp("iERRFHAFDX")
   If ActualCorrF1 <> 0 Then
      CorrfHaF1 = Lp("COREVFHADX_G")
      Call SP("iCORPREFHAFDX", CorrfHaF1)
      tmp = CorrfHaF1 + ActualCorrF1
      CorFHA1mm = tmp / 1000
      Call SP("CORFHA1", CorFHA1mm)
      If MODALITA = "SPF" Then
         stmp = frmt(CorFHA1mm, 4)
         Call OPC_SCRIVI_DATO("/ACC/NCK/UGUD/CORFHA1", stmp)
      End If
      Call SP("COREVFHADX_G", tmp)
      ActualCorrF1 = 0
      Call SP("iERRFHAFDX", ActualCorrF1)
   Else
      CorrfHaF1 = Lp("COREVFHADX_G")
      CorFHA1mm = CorrfHaF1 / 1000
      Call SP("CORFHA1", CorFHA1mm)
      If MODALITA = "SPF" Then
         stmp = frmt(CorFHA1mm, 4)
         Call OPC_SCRIVI_DATO("/ACC/NCK/UGUD/CORFHA1", stmp)
      End If
   End If
End If


Exit Sub

errCalcoloParametri_MOLAVITE:
  If (Err.Number = 6) Or (Err.Number = 11) Then
    ParametriOK = False
    PAR.Pic.Cls
  Else
    MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Sub: CalcoloParametri_MOLAVITE"
  End If
  Exit Sub


End Sub


Sub CalcoloParametri_MOLAVITE()
'
Dim tmp       As Double
Dim i         As Integer
'Dim FattoreX  As Double
Dim stmp      As String
'Var. conversione spessore
Dim iB    As Double
Dim Betab As Double
Dim PB    As Double
Dim Sb    As Double
Dim E1    As Double
Dim H2    As Double
Dim H3    As Double
Dim F1    As Double
Dim F2    As Double
Dim Wx      As Double
Dim We      As Double
Dim X       As Double
Dim X1      As Double
Dim Ymini   As Double
Dim Ymaxi   As Double
Dim Ymoyen  As Double
'Var. calcolo lung. evolvente
Dim ValSAP  As Double
Dim ValEAP  As Double
Dim TifSAP  As Double
Dim TifEAP  As Double
Dim LungEvo As Double
'
Dim Dtip    As Double
'
Dim SovraMetalloRAD As Double
Dim SovrFianco      As Double
Dim ActualCorrF1    As Double
Dim ActualCorrF2    As Double
Dim CorrfHaF1       As Double
Dim CorrfHaF2       As Double

Dim PosW_Mola       As Double
Dim L_Mola          As Double
Dim SHIFTIN_Mola    As Double
Dim SHIFTOUT_Mola   As Double
Dim PosIn_Mola      As Double
Dim PosFin_Mola     As Double
Dim PosIn2_Mola#, PosFin2_Mola#
Dim DirShift_Mola   As Double
Dim DirShift_Pre    As Double
Dim PosShf          As Double
'
Dim DbetaF1         As Double
Dim DbetaF2         As Double
Dim CorConTA        As Double
Dim CorConTB        As Double
Dim CorConTC        As Double
Dim CorsaRadTA      As Double
Dim CorsaRadTB      As Double
Dim CorsaRadTC      As Double
'
Dim ErrPasso        As Double
Dim ErrPassoSx      As Double
Dim ErrPassoDx      As Double
Dim ConSx           As Double
Dim ConDx           As Double
Dim fHbSX           As Double
Dim fHbDx           As Double
Dim iConAfHb        As Double
Dim iConBfHb        As Double
Dim iConCfHb        As Double
Dim CorsaRadZA      As Double
Dim CorsaRadZB      As Double
Dim CorsaRadZC      As Double

Dim FasciaFhb       As Double
Dim PercentualeFascia As Double

Dim FiancoSx        As Double
Dim FiancoSxPrec    As Double

Dim Con_F1          As Double
Dim fHb_F1          As Double
Dim Con_F2          As Double
Dim fHb_F2          As Double

Dim ErrPassoF1      As Double
Dim ErrPassoF2      As Double

Dim CorrFHBSX       As Double
Dim ActualErrSXFHB  As Double
Dim CorrFHBDX       As Double
Dim ActualErrDXFHB  As Double
Dim MODALITA        As String
Dim TW_TUNING#, iTW_TUNING, iTW_TUNING_corr#

'
On Error GoTo errCalcoloParametri_MOLAVITE
'
  ParametriOK = True
  Call InizializzaVariabili
  '
  'LEGGO LA MODALITA DI TRASFERIMENTO DATI
  MODALITA = GetInfo("Configurazione", "MODALITA", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))
  
  'Azzeramento variabili calcolo
  CalcoloEsterni.DB = 0
  ReDim CalcoloEsterni.DCont(0)
  CalcoloEsterni.DE = 0
  CalcoloEsterni.DI = 0
  CalcoloEsterni.DIOtt = 0
  CalcoloEsterni.DP = 0
  ReDim CalcoloEsterni.IngrX(0)
  ReDim CalcoloEsterni.IngrY(0)
  ReDim CalcoloEsterni.LCont(0)
  ReDim CalcoloEsterni.MolaX(0)
  ReDim CalcoloEsterni.MolaY(0)
  CalcoloEsterni.Np = 0

'Gestione Lato serraggio
FiancoSx = Lp("iFiancoSX")
FiancoSxPrec = Lp("iFiancoSXPRE")

If FiancoSxPrec <> FiancoSx Then
    FiancoSxPrec = FiancoSx
    Call SP("iFiancoSXPRE", FiancoSxPrec)
   
   'Cancella tutte le correzioni di fHa
   'Fianco Sx
    Call SP("iERRFHAFSX", 0)
    Call SP("CORFHA1", 0)
    Call SP("COREVFHASX_G", 0)
    Call SP("iCORPREFHAFSX", 0)
   'Fianco Dx
    Call SP("iERRFHAFDX", 0)
    Call SP("CORFHA2", 0)
    Call SP("COREVFHADX_G", 0)
    Call SP("iCORPREFHAFDX", 0)
    
    'Cancella tutte le correzioni di fHb
    
    Call SP("iERRPREDXFHB", 0)
    Call SP("iERRDXFHB", 0)
    
    Call SP("iERRPRESXFHB", 0)
    Call SP("iERRSXFHB", 0)
    
    Call SP("ERRTOTSXFHB_G", 0)
    Call SP("ERRTOTDXFHB_G", 0)
    Call SP("CORRPASSO", 0)
    Call SP("CORX1FHB", 0)
    Call SP("CORX2FHB", 0)
    Call SP("CORX3FHB", 0)
      
    If MODALITA = "SPF" Then
       Call OPC_SCRIVI_DATO("/ACC/NCK/UGUD/CORRPASSO", "0")
       Call OPC_SCRIVI_DATO("/ACC/NCK/GUD7/CORX1FHB", "0")
       Call OPC_SCRIVI_DATO("/ACC/NCK/GUD7/CORX2FHB", "0")
       Call OPC_SCRIVI_DATO("/ACC/NCK/GUD7/CORX3FHB", "0")
       Call OPC_SCRIVI_DATO("/ACC/NCK/UGUD/CORFHA1", "0")
       Call OPC_SCRIVI_DATO("/ACC/NCK/UGUD/CORFHA2", "0")
    End If
End If
    
  '
  'Lettura Parametri Profilo
  DGlob.Z = Lp("NUMDENTI_G")
  DGlob.Mn = Lp("MN_G")
  DGlob.ALFA = Lp("ALFA_G") * PG / 180
  DGlob.DI = Lp("DINT_G")
  DGlob.DE = Lp("DEXT_G")
  DGlob.SON = Lp("R[938]")
  DGlob.DefSpe = Lp("iDefSp")
  DGlob.WbQ = Lp("QW")
  DGlob.WbN = Lp("ZW")
  DGlob.RulQ = Lp("QR")
  DGlob.RulD = Lp("DR")
  
    
  DGlob.BetaF1 = Lp("BETAFsx_G") * PG / 180 * Lp("iSensoElica")
  DGlob.BetaF2 = Lp("BETAFdx_G") * PG / 180 * Lp("iSensoElica")
      
  'Angolo elica medio comprensivo di segno che indica il senso elica
  DGlob.Beta = ((DGlob.BetaF1 + DGlob.BetaF2) / 2)
  Call SP("BETA_G", DGlob.Beta * 180 / PG)
  
  DGlob.RT = 1
  DGlob.RF = 1
  DGlob.DistPtEv = 0.5
  '
  If (DGlob.Z = 0) Or (DGlob.Mn = 0) Then Exit Sub
  '
  'Calcolo correzione per angolo elica differenti tra i fianchi
  DbetaF1 = (DGlob.BetaF1 - DGlob.Beta)
  DbetaF2 = (DGlob.Beta - DGlob.BetaF2)
  '
  'Calcolo altri parametri
  DGlob.ALFAt = Atn(Tan(DGlob.ALFA) / Cos(DGlob.Beta))
  DGlob.DP = DGlob.Mn * DGlob.Z / Cos(DGlob.Beta)     'Diametro Primitivo
  DGlob.DB = DGlob.DP * Cos(DGlob.ALFAt)              'Diametro di Base
  '
  'Calcolo sovrametallo radiale
  SovrFianco = Lp("STOCK")
  SovraMetalloRAD = SovrFianco / Tan(DGlob.ALFA)
  'Scrivi parametro sovrametallo radiale
  Call SP("SOVRMETRAD_G", frmt(SovraMetalloRAD, 2))
  '
  'Leggi SAP e EAP
  ValSAP = Lp("DSAP_G")
  ValEAP = Lp("DEAP_G")
  DGlob.SAP = ValSAP
  DGlob.EAP = ValEAP
  '
  'Leggi Diametro rastremazione
  Dtip = Lp("TIPRELIEF_DIAMETER")
  '
  If ((DGlob.DE <= 0) And (DGlob.Mn > 0)) Or ((DGlob.DE <= DGlob.DB) And (DGlob.Mn > 0)) Then
      DGlob.DE = DGlob.DB + (DGlob.Mn / Cos(DGlob.Beta) * 2.2 * 2)
      Call SP("DEXT_G", frmt(DGlob.DE, 4))
      Call SP("DINT_G", frmt(DGlob.DE - (DGlob.Mn / Cos(DGlob.Beta) * 2.2 * 2), 4))
  End If
  '
  If (ValEAP > DGlob.DE) And (DGlob.DE > 0) Then
      ValEAP = DGlob.DE
      Call SP("DEAP_G", frmt(ValEAP, 4))
  End If
  '
  If (ValSAP <= 0) Or (ValSAP <= DGlob.DB) Then
      ValSAP = DGlob.DB + 0.01
      Call SP("DSAP_G", frmt(ValSAP, 4))
      Call SP("SAP_F1_CD", frmt(ValSAP, 4))
      Call SP("SAP_F2_CD", frmt(ValSAP, 4))
  Else
      Call SP("SAP_F1_CD", frmt(ValSAP, 4))
      Call SP("SAP_F2_CD", frmt(ValSAP, 4))
  End If
  
  If (ValEAP <= 0) Or (ValEAP <= DGlob.DB) Or (ValEAP <= ValSAP) Then
      ValEAP = DGlob.DB + (DGlob.Mn * 2.2 * 2)
      Call SP("DEAP_G", frmt(ValEAP, 4))
  End If
  '
  If (Dtip > ValEAP) Then
      Dtip = ValEAP
      Call SP("TIPRELIEF_DIAMETER", frmt(Dtip, 4))
  End If
  '
  'Calcolo Lung. evolvente
  If (ValSAP > 0) And (DGlob.DB > 0) And (ValSAP > DGlob.DB) Then
     TifSAP = Sqr((ValSAP / 2) ^ 2 - (DGlob.DB / 2) ^ 2)
  End If
  If (ValEAP > 0) And (DGlob.DB > 0) And (ValEAP > ValSAP) Then
     TifEAP = Sqr((ValEAP / 2) ^ 2 - (DGlob.DB / 2) ^ 2)
  End If
  If (TifEAP > TifSAP) And (TifSAP > 0) Then
      LungEvo = Int((TifEAP - TifSAP) * 1000) / 1000
  Else
      LungEvo = 0
  End If
  '
  'Scrivi parametro Lunghezza evolvente
  Call SP("LAEVOLV_G", frmt(LungEvo, 4))
  '
  '----------------------------------
  'Se lo spessore � nullo lo imposto a mezzo passo
  If (DGlob.SON = 0) And (DGlob.DefSpe = 1) Then
    DGlob.SON = Round((DGlob.Mn * PG) / 2, 3)
    Call SP("R[938]", frmt(DGlob.SON, 8))
    'CALCOLO DEL FATTORE DI SPOSTAMENTO
    DGlob.FattoreX = (DGlob.SON - ((PG / 2) * DGlob.Mn)) / (2 * DGlob.Mn * Tan(DGlob.ALFA))
    DGlob.FattoreX = Int(DGlob.FattoreX * 100000 + 0.5) / 100000
    Call SP("FattoreX", frmt(DGlob.FattoreX, 8))

    'FattoreX = (DGlob.SON - ((PG / 2) * DGlob.Mn)) / (2 * DGlob.Mn * Tan(DGlob.ALFA))
    'FattoreX = Int(FattoreX * 100000 + 0.5) / 100000
    'Call SP("FattoreX", FattoreX)
  End If
  '
  Betab = Atn(Tan(Abs(DGlob.Beta)) * (Cos(DGlob.ALFAt)))
  PB = DGlob.DB * PG / DGlob.Z
  '
  Select Case DGlob.DefSpe
    '
    Case 1 'SON
      If (DGlob.SON > 0) Then
        'CALCOLO QUOTA WILDHABER
        If (DGlob.WbN > 0) Then
          DGlob.SONt = DGlob.SON / Cos(DGlob.Beta)
          Sb = (DGlob.SONt + DGlob.DP * inv(DGlob.ALFAt)) * DGlob.DB / DGlob.DP
          iB = PB - Sb
          DGlob.WbQ = DGlob.WbN * PB * Cos(Betab) - iB * Cos(Betab)
          'CALCOLO QUOTA RULLI
          If (DGlob.RulD > 0) Then
            Wx = Cos(PG / (2 * DGlob.Z)) * DGlob.DB
            If Int(DGlob.Z / 2) = DGlob.Z / 2 Then Wx = DGlob.DB
            We = Sb - PB
            X = (We + DGlob.RulD / Cos(Betab)) / DGlob.DB
            Ymini = 0: Ymaxi = PG / 2
            For i = 1 To 25
              Ymoyen = (Ymini + Ymaxi) / 2: X1 = Tan(Ymoyen) - Ymoyen
              If X1 < X Then Ymini = Ymoyen Else Ymaxi = Ymoyen
            Next i
            DGlob.RulQ = (Wx / Cos(Ymoyen)) + DGlob.RulD
          Else
            DGlob.RulQ = 0
          End If
        Else
          DGlob.WbQ = 0
        End If
        'CALCOLO DEL FATTORE DI SPOSTAMENTO
        DGlob.FattoreX = (DGlob.SON - ((PG / 2) * DGlob.Mn)) / (2 * DGlob.Mn * Tan(DGlob.ALFA))
        'AGGIORNAMENTO VALORI
        Call SP("QW", frmt(DGlob.WbQ, 8))
        Call SP("QR", frmt(DGlob.RulQ, 8))
        Call SP("FattoreX", frmt(DGlob.FattoreX, 8))
      End If
      '
    Case 2 'Quota Wb
      If (DGlob.WbN > 0) And (DGlob.WbQ > 0) Then
        iB = DGlob.WbN * PB - DGlob.WbQ / Cos(Betab)
        Sb = PB - iB
        DGlob.SONt = DGlob.DP * (Sb / DGlob.DB - (inv(DGlob.ALFAt)))
        DGlob.SON = DGlob.SONt * Cos(DGlob.Beta)
        'CALCOLO QUOTA RULLI
        If (DGlob.RulD > 0) Then
          Wx = Cos(PG / (2 * DGlob.Z)) * DGlob.DB
          If Int(DGlob.Z / 2) = DGlob.Z / 2 Then Wx = DGlob.DB
          We = Sb - PB
          X = (We + DGlob.RulD / Cos(Betab)) / DGlob.DB
          Ymini = 0: Ymaxi = PG / 2
          For i = 1 To 25
            Ymoyen = (Ymini + Ymaxi) / 2: X1 = Tan(Ymoyen) - Ymoyen
            If X1 < X Then Ymini = Ymoyen Else Ymaxi = Ymoyen
          Next i
          DGlob.RulQ = (Wx / Cos(Ymoyen)) + DGlob.RulD
        Else
          DGlob.RulQ = 0
        End If
        'CALCOLO DEL FATTORE DI SPOSTAMENTO
        DGlob.FattoreX = (DGlob.SON - ((PG / 2) * DGlob.Mn)) / (2 * DGlob.Mn * Tan(DGlob.ALFA))
        'AGGIORNAMENTO VALORI
        Call SP("R[938]", frmt(DGlob.SON, 8))
        Call SP("QR", frmt(DGlob.RulQ, 8))
        Call SP("FattoreX", frmt(DGlob.FattoreX, 8))
      End If
      '
    Case 3 'Quota Rulli
      If (DGlob.RulQ <> 0) And (DGlob.RulD <> 0) And (DGlob.RulD <> DGlob.RulQ) Then
        E1 = PG / 2 / DGlob.Z
        If (Int(DGlob.Z / 2) = DGlob.Z / 2) Then E1 = 0
        H2 = DGlob.Mn * DGlob.Z * Cos(DGlob.ALFAt) * Cos(E1) / ((DGlob.RulQ - DGlob.RulD) * Cos(Abs(DGlob.Beta)))
        H3 = Tan(Acs(H2))
        F2 = (inv(DGlob.ALFAt) + DGlob.RulD / (DGlob.Mn * DGlob.Z * Cos(DGlob.ALFA)) - H3 + Atn(H3)) * DGlob.Mn * DGlob.Z
        DGlob.SON = PG * DGlob.Mn - F2
        'CALCOLO DEL FATTORE DI SPOSTAMENTO
        DGlob.FattoreX = (DGlob.SON - ((PG / 2) * DGlob.Mn)) / (2 * DGlob.Mn * Tan(DGlob.ALFA))
      End If
      'CALCOLO QUOTA WILDHABER
      If (DGlob.WbN > 0) Then
        DGlob.SONt = DGlob.SON / Cos(DGlob.Beta)
        Sb = (DGlob.SONt + DGlob.DP * inv(DGlob.ALFAt)) * DGlob.DB / DGlob.DP
        iB = PB - Sb
        DGlob.WbQ = DGlob.WbN * PB * Cos(Betab) - iB * Cos(Betab)
      Else
        DGlob.WbQ = 0
      End If
      'AGGIORNAMENTO VALORI
      Call SP("R[938]", frmt(DGlob.SON, 8))
      Call SP("QW", frmt(DGlob.WbQ, 8))
      Call SP("FattoreX", frmt(DGlob.FattoreX, 8))
      '
    Case 4 'Fattore X
      DGlob.FattoreX = Lp("FattoreX")
      'CALCOLO DELLO SPESSORE NORMALE
      DGlob.SON = 2 * DGlob.Mn * DGlob.FattoreX * Tan(DGlob.ALFA) + ((PG / 2) * DGlob.Mn)
      'CALCOLO QUOTA WILDHABER
      If (DGlob.WbN > 0) Then
        DGlob.SONt = DGlob.SON / Cos(DGlob.Beta)
        Sb = (DGlob.SONt + DGlob.DP * inv(DGlob.ALFAt)) * DGlob.DB / DGlob.DP
        iB = PB - Sb
        DGlob.WbQ = DGlob.WbN * PB * Cos(Betab) - iB * Cos(Betab)
        'CALCOLO QUOTA RULLI
        If (DGlob.RulD > 0) Then
          Wx = Cos(PG / (2 * DGlob.Z)) * DGlob.DB
          If Int(DGlob.Z / 2) = DGlob.Z / 2 Then Wx = DGlob.DB
          We = Sb - PB
          X = (We + DGlob.RulD / Cos(Betab)) / DGlob.DB
          Ymini = 0: Ymaxi = PG / 2
          For i = 1 To 25
            Ymoyen = (Ymini + Ymaxi) / 2: X1 = Tan(Ymoyen) - Ymoyen
            If X1 < X Then Ymini = Ymoyen Else Ymaxi = Ymoyen
          Next i
          DGlob.RulQ = (Wx / Cos(Ymoyen)) + DGlob.RulD
        Else
          DGlob.RulQ = 0
        End If
      Else
        DGlob.WbQ = 0
      End If
      'AGGIORNAMENTO VALORI
      Call SP("R[938]", frmt(DGlob.SON, 8))
      Call SP("QW", frmt(DGlob.WbQ, 8))
      Call SP("QR", frmt(DGlob.RulQ, 8))
      '
  End Select
  '
  'Spessore Riferimento per Controllo
  Dim DefSpeChk As Double
  Dim SONCHK As Double
  Dim WbQCHK As Double
  Dim WbNCHK As Double
  Dim RulQCHK As Double
  Dim RulDCHK As Double
  Dim SONtCHK As Double
  Dim SbCHK As Double
  Dim ibCHK As Double
  Dim WxCHK As Double
  Dim WeCHK As Double
  Dim XCHK As Double
  Dim E1CHK As Double
  Dim H2CHK As Double
  Dim H3CHK As Double
  Dim F2CHK As Double
  '
  If DGlob.DefSpe = 4 Then
     'Se TipoSpessore = "Fattore X" carico "SON"
     DefSpeChk = 1
  Else
     DefSpeChk = DGlob.DefSpe
  End If
  
  Call SP("iDefSpCHK", frmt(DefSpeChk, 2))
  
  SONCHK = Lp("SONCHK")
  WbQCHK = Lp("QWCHK")
  WbNCHK = Lp("ZWCHK")
  RulQCHK = Lp("QRCHK")
  RulDCHK = Lp("DRCHK")
  
  Select Case DefSpeChk
    '
    Case 1 'SON
      If (SONCHK > 0) Then
        'CALCOLO QUOTA WILDHABER
        If (WbNCHK > 0) Then
          SONtCHK = SONCHK / Cos(DGlob.Beta)
          SbCHK = (SONtCHK + DGlob.DP * inv(DGlob.ALFAt)) * DGlob.DB / DGlob.DP
          ibCHK = PB - SbCHK
          WbQCHK = WbNCHK * PB * Cos(Betab) - ibCHK * Cos(Betab)
          'CALCOLO QUOTA RULLI
          If (RulDCHK > 0) Then
            WxCHK = Cos(PG / (2 * DGlob.Z)) * DGlob.DB
            If Int(DGlob.Z / 2) = DGlob.Z / 2 Then WxCHK = DGlob.DB
            WeCHK = SbCHK - PB
            XCHK = (WeCHK + RulDCHK / Cos(Betab)) / DGlob.DB
            
            Ymini = 0: Ymaxi = PG / 2
            For i = 1 To 25
              Ymoyen = (Ymini + Ymaxi) / 2: X1 = Tan(Ymoyen) - Ymoyen
              If X1 < XCHK Then Ymini = Ymoyen Else Ymaxi = Ymoyen
            Next i
            RulQCHK = (WxCHK / Cos(Ymoyen)) + RulDCHK
          Else
            RulQCHK = 0
          End If
        Else
          WbQCHK = 0
        End If
        'AGGIORNAMENTO VALORI
        Call SP("QWCHK", frmt(WbQCHK, 4))
        Call SP("QRCHK", frmt(RulQCHK, 4))
      End If
      '
    Case 2 'Quota Wb
      If (WbNCHK > 0) And (WbQCHK > 0) Then
        ibCHK = WbNCHK * PB - WbQCHK / Cos(Betab)
        SbCHK = PB - ibCHK
        SONtCHK = DGlob.DP * (SbCHK / DGlob.DB - (inv(DGlob.ALFAt)))
        SONCHK = SONtCHK * Cos(DGlob.Beta)
        'CALCOLO QUOTA RULLI
        If (RulDCHK > 0) Then
          WxCHK = Cos(PG / (2 * DGlob.Z)) * DGlob.DB
          If Int(DGlob.Z / 2) = DGlob.Z / 2 Then WxCHK = DGlob.DB
          WeCHK = SbCHK - PB
          XCHK = (WeCHK + RulDCHK / Cos(Betab)) / DGlob.DB
          Ymini = 0: Ymaxi = PG / 2
          For i = 1 To 25
            Ymoyen = (Ymini + Ymaxi) / 2: X1 = Tan(Ymoyen) - Ymoyen
            If X1 < XCHK Then Ymini = Ymoyen Else Ymaxi = Ymoyen
          Next i
          RulQCHK = (WxCHK / Cos(Ymoyen)) + RulDCHK
        Else
          RulQCHK = 0
        End If
        'AGGIORNAMENTO VALORI
        Call SP("SONCHK", frmt(SONCHK, 4))
        Call SP("QRCHK", frmt(RulQCHK, 4))
      End If
      '
    Case 3 'Quota Rulli
      If (RulQCHK <> 0) And (RulDCHK <> 0) And (RulDCHK <> RulQCHK) Then
        E1CHK = PG / 2 / DGlob.Z
        If (Int(DGlob.Z / 2) = DGlob.Z / 2) Then E1CHK = 0
        H2CHK = DGlob.Mn * DGlob.Z * Cos(DGlob.ALFAt) * Cos(E1CHK) / ((RulQCHK - RulDCHK) * Cos(Abs(DGlob.Beta)))
        H3CHK = Tan(Acs(H2CHK))
        F2CHK = (inv(DGlob.ALFAt) + RulDCHK / (DGlob.Mn * DGlob.Z * Cos(DGlob.ALFA)) - H3CHK + Atn(H3CHK)) * DGlob.Mn * DGlob.Z
        SONCHK = PG * DGlob.Mn - F2CHK
      End If
      'CALCOLO QUOTA WILDHABER
      If (WbNCHK > 0) Then
        SONtCHK = SONCHK / Cos(DGlob.Beta)
        SbCHK = (SONtCHK + DGlob.DP * inv(DGlob.ALFAt)) * DGlob.DB / DGlob.DP
        ibCHK = PB - SbCHK
        WbQCHK = WbNCHK * PB * Cos(Betab) - ibCHK * Cos(Betab)
      Else
        WbQCHK = 0
      End If
      'AGGIORNAMENTO VALORI
      Call SP("SONCHK", frmt(SONCHK, 4))
      Call SP("QWCHK", frmt(WbQCHK, 4))
      '
      '
  End Select
  '
    
  ' *** Agg.23.11.2013 ***
  Dim CorrSpessore As Double
  Dim CorrInterasse As Double
  Dim NewRulQ As Double
  Dim NewSon As Double
  Dim CorrSon As Double
  Dim Indtav As Integer
  Dim ValoreVar As String

  
  For Indtav = 0 To 1
  
    ValoreVar = Lp_Str("iCorrSpessore[" + LTrim(str(Indtav)) + "]")
    If ValoreVar = "NOTFOUND" Then GoTo FineCorrInt
    
    CorrSpessore = Lp("iCorrSpessore[" + LTrim(str(Indtav)) + "]")
  
  'Calcolo Correzione Interasse da Correzione su SON , Wn , Qr
  Select Case DGlob.DefSpe
    Case 1 'SON
      CorrInterasse = Int(CorrSpessore / 2 / Tan(DGlob.ALFA) * 10000) / 10000
    
    Case 2 'Quota Wb
      CorrInterasse = Int(CorrSpessore / 2 / Tan(DGlob.ALFA) * 10000) / 10000
          
    Case 3 'Quota Rulli
      'Calcolo nuova quota rulli da quota rulli impostata
      If (DGlob.RulQ <> 0) And (DGlob.RulD <> 0) And (DGlob.RulD <> DGlob.RulQ) Then
        NewRulQ = DGlob.RulQ - CorrSpessore
        
        E1 = PG / 2 / DGlob.Z
        If (Int(DGlob.Z / 2) = DGlob.Z / 2) Then E1 = 0
        H2 = DGlob.Mn * DGlob.Z * Cos(DGlob.ALFAt) * Cos(E1) / ((NewRulQ - DGlob.RulD) * Cos(Abs(DGlob.Beta)))
        H3 = Tan(Acs(H2))
        F2 = (inv(DGlob.ALFAt) + DGlob.RulD / (DGlob.Mn * DGlob.Z * Cos(DGlob.ALFA)) - H3 + Atn(H3)) * DGlob.Mn * DGlob.Z
        NewSon = PG * DGlob.Mn - F2
      End If
      
      CorrSon = DGlob.SON - NewSon
      CorrInterasse = Int(CorrSon / 2 / Tan(DGlob.ALFA) * 10000) / 10000
  End Select
  
  'Aggiorno valore correttore interasse
  Call SP("CORINTLAV_G[" + LTrim(str(Indtav)) + ",1]", CorrInterasse)
  
  Next Indtav
FineCorrInt:
  '
  Dim LatoPart As Integer
  Dim nZone    As Integer
  '
  LatoPart = Lp("LatoPA")
  nZone = Lp("iNZone")
  '
  Dim INCLINAZIONE_MOLA_GRD  As Double
  Dim INCLINAZIONE_MOLA_RAD  As Double
  Dim MODULO_MOLA            As Double
  Dim PRINCIPI_MOLA          As Double
  Dim DIAMETRO_MOLA          As Double
  Dim DIAMETRO_MOLA_EXT_EFF  As Double ' DIametro mola esterno durante la rettifica
  Dim DIAMETRO_MOLA_INT_EFF  As Double ' Diametro mola di fondo usato durante la rettifica
  Dim DIAMETRO_MOLA_MED_EFF  As Double ' Diametro mola medio usato durante la rettifica
  Dim ALTEZZA_DENTEMOLA_EFF  As Double ' Altezza dente mola effettivamente usata in profilatura
  
  Dim ARGOMENTO              As Double
  Dim SENSO_FILETTO          As Integer
  Dim InclTesta              As Double
  Dim H_filetto              As Double
  
  SENSO_FILETTO = Lp("SENSOFIL_G") '
  MODULO_MOLA = Lp("MNUT_G")       'MnUtensileMV
  PRINCIPI_MOLA = Lp("NUMPRINC_G") 'NPrincipiMV
  
  DIAMETRO_MOLA = Lp("DIAMMOLA_G") 'Diametro Mola Nuova
  If DIAMETRO_MOLA <= 0 Then
     stmp = GetInfo("CALCOLO", "DIAMETRO_MOLA_NEW", Path_LAVORAZIONE_INI)
     DIAMETRO_MOLA = val(stmp) ' Letto da file INI
     Call SP("DIAMMOLA_G", DIAMETRO_MOLA)
  End If

  DIAMETRO_MOLA_EXT_EFF = 2 * val(OPC_LEGGI_DATO("/ACC/NCK/GUD7/REXTMOLA_G"))
  If (DIAMETRO_MOLA_EXT_EFF <= 0) And (DIAMETRO_MOLA > 0) Then
    DIAMETRO_MOLA_EXT_EFF = DIAMETRO_MOLA
  End If
  
    ALTEZZA_DENTEMOLA_EFF = Lp("HDENTEMOLA_G")
  If ALTEZZA_DENTEMOLA_EFF <= 0 Then
     'ALTEZZA_DENTEMOLA_EFF = 2.2 * MODULO_MOLA
     ALTEZZA_DENTEMOLA_EFF = 2.2 * DGlob.Mn
     H_filetto = ALTEZZA_DENTEMOLA_EFF
     Call SP("HDENTEMOLA_G", frmt(H_filetto, 2))
  End If
  
  DIAMETRO_MOLA_INT_EFF = 2 * val(OPC_LEGGI_DATO("/ACC/NCK/GUD7/RFONDOMOLA_G"))
  If (DIAMETRO_MOLA_INT_EFF <= 0) Then
      DIAMETRO_MOLA_INT_EFF = DIAMETRO_MOLA_EXT_EFF - (ALTEZZA_DENTEMOLA_EFF * 2)
  End If
    
  'Diametro mola effettivo preso sull'Addendum
  'DIAMETRO_MOLA_MED_EFF = DIAMETRO_MOLA_EXT_EFF - 2 * MODULO_MOLA
  DIAMETRO_MOLA_MED_EFF = DIAMETRO_MOLA_INT_EFF + 2 * ALTEZZA_DENTEMOLA_EFF - 2 * MODULO_MOLA
    
  'CALCOLO INCLINAZIONE FILETTO sul diametro mola medio
  ARGOMENTO = MODULO_MOLA * PRINCIPI_MOLA / DIAMETRO_MOLA_MED_EFF
  INCLINAZIONE_MOLA_RAD = Atn(ARGOMENTO / Sqr(1 - ARGOMENTO * ARGOMENTO))  'RADIANTI
  INCLINAZIONE_MOLA_GRD = INCLINAZIONE_MOLA_RAD / PG * 180                 'GRADI
  
  Dim HDenteMolaMV           As Double
  Dim PROFONDITA             As Double
  Dim AlfaMV                 As Double
  Dim Corsa_X1               As Double
  Dim Corsa_X2               As Double

  HDenteMolaMV = ALTEZZA_DENTEMOLA_EFF
  'PROFONDITA = (DGlob.DE - DGlob.Di) / 2
  stmp = GetInfo("CALCOLO", "PROFONDITA", Path_LAVORAZIONE_INI)
  PROFONDITA = val(stmp) ' Letto da file INI

  AlfaMV = Lp("ALFAUT_G")

'----------------------------------
' calcola angolo mola a vite
'----------------------------------

 Dim ApRast_IN As Double
 ApRast_IN = 0
 Dim param(1 To 20) As Double
 'Leggi par. rullo con Lp
 param(1) = Lp("AP_RULLO")
 param(2) = Lp("SP_RIF_RULLO")
 param(3) = Lp("BOMBATURA_RULLO")
 param(4) = Lp("DRULLOPROF_G")
 param(5) = Lp("RG_RIF_RULLO")
 param(6) = Lp("TIPRELIEF_DIS_RULLO")
 param(7) = Lp("TIPRELIEF_ANG_RULLO")
 param(8) = Lp("RG_RAC_TESTA_RULLO")
 'assegna par. profilo letti con Lp
 param(9) = ValSAP
 param(10) = Lp("BETA_G")
 param(11) = Lp("ALFA_G")
 param(12) = DGlob.Mn
 param(13) = DGlob.Z
 param(14) = DGlob.WbQ
 param(15) = DGlob.WbN
 param(16) = DGlob.DE
 param(17) = DGlob.DI
 
 param(18) = ValEAP
 param(19) = MODULO_MOLA
  
 Dim Checkinp As Boolean
 Checkinp = param(1) > 0 And param(2) > 0 And param(3) > 0 And param(4) > 0 And param(5) > 0 And param(8) > 0 And param(9) > 0 And param(11) > 0 And param(12) > 0 And param(13) > 0 And param(14) > 0 And param(15) > 0 And param(9) > 0 And param(17) > 0 And param(16) > 0 And param(18) > 0

If (Checkinp) Then
  
 If (FillParamMV( _
 param(1), _
 param(2), _
 param(3), _
 param(4), _
 param(5), _
 param(6), _
 param(7), _
 param(8), _
 ApRast_IN, _
 param(9), _
 param(10), _
 param(11), _
 param(12), _
 param(13), _
 param(14), _
 param(15), _
 param(16), _
 param(17), _
 param(18), _
 0)) Then

'--------------------------------------------------
' DATI SENSIBILI INGRANAMENTO MOLA RUOTA
' Specs(01) gioco fondo mola testa ingranaggio
' Specs(02) spostamento rullo
' Specs(03) bombatura ottenuta
' Specs(04) mola_modulo normale
' Specs(05) mola angolo pressione normale
' Specs(06) mola_spessore
' Specs(07) mola dedendum
' Specs(08) mola addendum per rispettare sap
' Specs(09) mola altezza dente minima
' Specs(10) Raggio Minimo Inviluppato da mola
' Specs(11) Ascissa spessore mola
' Specs(12) Ordinata spessore mola
' Specs(13) Diametro Rotolamento Mola
' Specs(14) Ordinata spessore mola
' Specs(15) Diametro Rotolamento Mola
'--------------------------------------------------
    Dim Specs(1 To 18): Call Get_WormWheel_Specs(Specs)
    Dim ApMVtmp As Double
    ApMVtmp = Int(Specs(5) * 10000) / 10000
    AlfaMV = Lp("ALFAUT_G")
    '    If (AlfaMV <> ApMVtmp) Then
    '    AlfaMV = ApMVtmp: Call SP("ALFAUT_G", frmt(ApMVtmp, 4), True)
    '    End If

    ' Calcolo parametri x antitwist
    If (Lp("AT_DRESSING") <> 0) And (Lp("BETA_G") <> 0) Then Call Calcolo_Parametri_AT

    '--------------------------------------------
 Else
    'Checkinp = False
    'GoTo errCalcoloParametri_MOLAVITE
    'Exit Sub
 End If
End If


  Dim PAX                   As Double
  Dim PTE                   As Double
  Dim PBT                   As Double
  Dim AE                    As Double
  Dim MODULO_MOLA_CORRETTO  As Double
    
  'Passo base
  PBT = DGlob.Mn * PG * Cos(DGlob.ALFA)
  'Angolo pressione filetto mola in radianti
  AE = AlfaMV * PG / 180
  
  'Aggiorna Modulo Utensile se cambia l'angolo pressione del filetto
  MODULO_MOLA_CORRETTO = Int(PBT / PG / Cos(AE) * 100000000) / 100000000
  If (MODULO_MOLA <> MODULO_MOLA_CORRETTO) Then
     MODULO_MOLA = MODULO_MOLA_CORRETTO
     Call SP("MNUT_G", frmt(MODULO_MOLA, 4))
     'CALCOLO INCLINAZIONE FILETTO con Diametro mola medio effettivo
     DIAMETRO_MOLA_MED_EFF = DIAMETRO_MOLA_INT_EFF + 2 * ALTEZZA_DENTEMOLA_EFF - 2 * MODULO_MOLA
     ARGOMENTO = MODULO_MOLA * PRINCIPI_MOLA / DIAMETRO_MOLA_MED_EFF
     INCLINAZIONE_MOLA_RAD = Atn(ARGOMENTO / Sqr(1 - ARGOMENTO * ARGOMENTO))  'RADIANTI
     INCLINAZIONE_MOLA_GRD = INCLINAZIONE_MOLA_RAD / PG * 180                 'GRADI
  End If
  
  'Calcolo Extra Corse
  'CALCOLO INCLINAZIONE Asse Porta mola in RADIANTI
  'CALCOLO INCLINAZIONE FILETTO con Diametro Mola massimo
  Dim InclinazioneFiletto As Double
  Dim ParTmp              As Double
  ParTmp = MODULO_MOLA * PRINCIPI_MOLA / DIAMETRO_MOLA
  InclinazioneFiletto = Atn(ParTmp / Sqr(1 - ParTmp * ParTmp))  'RADIANTI
  
  If (SENSO_FILETTO = 0) Then
    InclTesta = DGlob.Beta - InclinazioneFiletto
  Else
    InclTesta = DGlob.Beta + InclinazioneFiletto
  End If
  '
  Corsa_X1 = Tan(Abs(InclTesta)) * Sqr(PROFONDITA * (DIAMETRO_MOLA / Sin(InclTesta) / Sin(InclTesta) + Lp("DEXT_G") - PROFONDITA))
  Corsa_X1 = (Int(Corsa_X1 * 10)) / 10
  If (Corsa_X1 <= 2) Then Corsa_X1 = 2
  '
  Corsa_X2 = MODULO_MOLA * Sin(Abs(InclTesta)) / Tan(AlfaMV / 180 * PG)
  Corsa_X2 = (Int(Corsa_X2 * 10)) / 10
  If (Corsa_X2 <= 2) Then Corsa_X2 = 2
  '
  Call SP("iCORSAIN", Round(Corsa_X1 + 0.05, 1))
  Call SP("iCORSAOUT", Round(Corsa_X1 + 0.05, 1))
  Call SP("CORSAIN", Lp("iCORSAIN") + Lp("iCORSAIN_CORR"))
  Call SP("CORSAOUT", Lp("iCORSAOUT") + Lp("iCORSAOUT_CORR"))
  '******
  '
  Dim L1    As Double
  Dim L2    As Double
  Dim L3    As Double
  Dim G2    As Double
  Dim LSV   As Double
  Dim ac    As Double
  Dim Dpri  As Double
  Dim Hkw   As Double
  '
  Dim PASSO_ASSIALE_MOLA As Double
  '
  'CALCOLO PASSO ASSIALE
  PASSO_ASSIALE_MOLA = MODULO_MOLA * PRINCIPI_MOLA * PG / Cos(INCLINAZIONE_MOLA_RAD)
  '
  ac = Abs(INCLINAZIONE_MOLA_RAD - DGlob.Beta)
  L1 = (((DGlob.DE - ((DGlob.DE - DGlob.DI) / 2)) * (DGlob.DE - DGlob.DI) / 2) ^ 0.5) / Abs(Cos(ac))
  LSV = MODULO_MOLA * (PG / 2 + Tan(AlfaMV * PG / 180) / Cos(DGlob.Beta))
  Dpri = DGlob.Mn * DGlob.Z / Cos(DGlob.Beta)
  Hkw = (Dpri - DGlob.DI) / 2
  
  'Calcolo shift a inizio e fine mola
  If (AlfaMV <> 0) Then
  
  G2 = Hkw / Tan(AlfaMV * PG / 180)
  Else
  G2 = Hkw / Tan(DGlob.ALFA)
  End If
  L2 = G2 + LSV * 1.4
  '
  'Call SP("iSHIFTIN_G", Round(L1, 1))
  Call SP("iSHIFTIN_G", Round(L2, 1))
  Call SP("iSHIFTOUT_G", Round(L2, 1))
  Call SP("SHIFTIN_G", Lp("iSHIFTIN_G") + Lp("iSHIFTIN_G_CORR"))
  Call SP("SHIFTOUT_G", Lp("iSHIFTOUT_G") + Lp("iSHIFTOUT_G_CORR"))
  
'
'BIAS
  If (Lp("CORR_TW_KIND") = 0) Then
   Call DB_GUD_AT_Azzera
   iTW_TUNING = 0: Call SP("iTW_TUNING", 0)
   iTW_TUNING_corr = 0: Call SP("iTW_TUNING_corr", 0)
   Call SP("AT_DRESSING", 0)
  Else
   Call SP("AT_DRESSING", 1)
   Call DB_GUD_AT_Aggiorna
   iTW_TUNING = Lp("iTW_TUNING")
   iTW_TUNING_corr = Lp("iTW_TUNING_corr")
  End If
  TW_TUNING = iTW_TUNING + iTW_TUNING_corr: Call SP("TW_TUNING", TW_TUNING)
  '
  'Calcolo Posizione Inizio e Fine Shift Mola in funzione della direzione di Shift
   DirShift_Pre = Lp("iDirShiftPre")
  DirShift_Mola = Lp("DirShift")
      PosW_Mola = Lp("WMOLA_G")
         L_Mola = Lp("LARGHMOLA_G")
   SHIFTIN_Mola = Lp("SHIFTIN_G")
  SHIFTOUT_Mola = Lp("SHIFTOUT_G")
  If (Lp("AT_DRESSING") = 1) Then
     Dim N_ZONE_FIN%, ZSGRLM%, cY_Zi#, Y_acc#, EXTENSION%
     N_ZONE_FIN = 1
         ZSGRLM = 1
          Y_acc = 0
      EXTENSION = 1
          cY_Zi = SHIFTIN_Mola
      If (cY_Zi > 9) Then
         cY_Zi = 9
      End If
      If (cY_Zi < 3) Then
         cY_Zi = 3
      End If
      Call MV_POS_INIZ_SH(DirShift_Mola, Lp("AT_DRESSING"), PosW_Mola, SHIFTIN_Mola, SHIFTOUT_Mola, L_Mola, N_ZONE_FIN, ZSGRLM, cY_Zi, Y_acc, EXTENSION, PosIn_Mola, PosFin_Mola, PosIn2_Mola, PosFin2_Mola)
  Else
      PosIn2_Mola = 0
      PosFin2_Mola = 0
      If DirShift_Mola < 0 Then
         PosIn_Mola = -PosW_Mola - SHIFTIN_Mola
         PosFin_Mola = -PosW_Mola - L_Mola + SHIFTOUT_Mola
      Else
        If DirShift_Mola > 0 Then
           PosFin_Mola = -PosW_Mola - SHIFTIN_Mola
           PosIn_Mola = -PosW_Mola - L_Mola + SHIFTOUT_Mola
        End If
      End If
  End If
  
  If DirShift_Mola <> DirShift_Pre Then
     DirShift_Pre = DirShift_Mola
     Call SP("iDirShiftPre", DirShift_Pre)
     'Aggiorno la Posizione Shift se cambia DirShift
     Call SP("POSSHIFT_G[2]", PosIn_Mola)
     PosShf = PosIn_Mola
     If MODALITA = "SPF" Then
        stmp = str(PosIn_Mola)
        Call OPC_SCRIVI_DATO("/ACC/NCK/UGUD/POSSHIFT_G[2]", stmp)
     End If
  End If
  
  Call SP("POSINIZIOSH_G", PosIn_Mola)
  Call SP("POSFINESH_G", PosFin_Mola)
  If MODALITA = "SPF" Then
     stmp = str(PosIn_Mola)
     Call OPC_SCRIVI_DATO("/ACC/NCK/UGUD/POSINIZIOSH_G", stmp)
     stmp = str(PosFin_Mola)
     Call OPC_SCRIVI_DATO("/ACC/NCK/UGUD/POSFINESH_G", stmp)
  End If
  
  If MODALITA = "OFF-LINE" Then
     PosShf = Lp("POSSHIFT_G[2]")
  End If
  If MODALITA = "SPF" Then
     PosShf = val(OPC_LEGGI_DATO("/ACC/NCK/UGUD/POSSHIFT_G[2]"))
  End If
  
  'Controllo quota PosShift[2]
  If DirShift_Mola < 0 Then
     If (PosShf > PosIn_Mola) Or (PosFin_Mola > PosShf) Then
         PosShf = PosIn_Mola
         Call SP("POSSHIFT_G[2]", PosIn_Mola)
         If MODALITA = "SPF" Then
            stmp = str(PosIn_Mola)
            Call OPC_SCRIVI_DATO("/ACC/NCK/UGUD/POSSHIFT_G[2]", stmp)
         End If
     End If
  Else
     If (PosShf < PosIn_Mola) Or (PosFin_Mola < PosShf) Then
         PosShf = PosIn_Mola
         Call SP("POSSHIFT_G[2]", PosIn_Mola)
         If MODALITA = "SPF" Then
            stmp = str(PosIn_Mola)
            Call OPC_SCRIVI_DATO("/ACC/NCK/UGUD/POSSHIFT_G[2]", stmp)
         End If
     End If
  End If
  
  'Aggiorno PosYPezzo:Posizione Y inizio Pezzo corrente
  stmp = str(PosShf)
  Call OPC_SCRIVI_DATO("/ACC/NCK/GUD7/PosYPezzo", stmp)
  
  
  Dim FasciaAMV As Double
  Dim FasciaBMV As Double
  Dim FasciaCMV As Double
  Dim FasciaA   As Double
  Dim FasciaB   As Double
  Dim FasciaC   As Double
  '
  'Calcolo delle Zone con extracorsa
  '
  Select Case nZone
      '
    Case 1
      Call SP("iFas2Fsx", 0): Call SP("iFas2Fdx", 0)
      Call SP("iFas3Fsx", 0): Call SP("iFas3Fdx", 0)
      Call SP("iCon2Fsx", 0): Call SP("iCon2Fdx", 0)
      Call SP("iCon3Fsx", 0): Call SP("iCon3Fdx", 0)
      Call SP("iBom2Fsx", 0): Call SP("iBom2Fdx", 0)
      Call SP("iBom3Fsx", 0): Call SP("iBom3Fdx", 0)
      FasciaAMV = Lp("CORSAIN") + Lp("iFas1Fsx") + Lp("CORSAOUT")
      FasciaBMV = 0
      FasciaCMV = 0
      FasciaA = Lp("iFas1Fsx")
      FasciaB = 0
      FasciaC = 0
      '
    Case 2
      Call SP("iFas3Fsx", 0): Call SP("iFas3Fdx", 0)
      Call SP("iCon3Fsx", 0): Call SP("iCon3Fdx", 0)
      Call SP("iBom3Fsx", 0): Call SP("iBom3Fdx", 0)
      FasciaAMV = Lp("CORSAIN") + Lp("iFas1Fsx")
      FasciaBMV = Lp("iFas2Fsx") + Lp("CORSAOUT")
      FasciaCMV = 0
      FasciaA = Lp("iFas1Fsx")
      FasciaB = Lp("iFas2Fsx")
      FasciaC = 0
      '
    Case 3
      FasciaAMV = Lp("CORSAIN") + Lp("iFas1Fsx")
      FasciaBMV = Lp("iFas2Fsx")
      FasciaCMV = Lp("iFas3Fsx") + Lp("CORSAOUT")
      FasciaA = Lp("iFas1Fsx")
      FasciaB = Lp("iFas2Fsx")
      FasciaC = Lp("iFas3Fsx")
      '
  End Select
  
Call SP("FASCIATOTALE_G", FasciaA + FasciaB + FasciaC)
  
'Leggo Fascia ed Extracorse
Dim CorsaTot   As Single
Dim ExcorsaIn  As Single
Dim ExcorsaOut As Single
Dim FASCIA     As Single

ExcorsaIn = Lp("CORSAIN")
ExcorsaOut = Lp("CORSAOUT")
FASCIA = Lp("FASCIATOTALE_G")
CorsaTot = ExcorsaIn + ExcorsaOut + FASCIA
    
'Calcolo Fascia controllata x correzione passo
PercentualeFascia = Lp("iFASCIALB_G")
FasciaFhb = PercentualeFascia * FASCIA / 100
Call SP("FASCIAFHB_G", FasciaFhb)
  
  
Dim AppBombXA As Double
Dim AppBombCA As Double
Dim AppConXA  As Double
Dim AppConCA  As Double
Dim AppBombXB As Double
Dim AppBombCB As Double
Dim AppConXB  As Double
Dim AppConCB  As Double
Dim AppBombXC As Double
Dim AppBombCC As Double
Dim AppConXC  As Double
Dim AppConCC  As Double

Dim BetaMV    As Double
Dim BombAMV   As Double
Dim BombAF2MV As Double
Dim ConAMV    As Double
Dim ConAF2MV  As Double

Dim BombBMV   As Double
Dim BombBF2MV As Double
Dim ConBMV    As Double
Dim ConBF2MV  As Double

Dim BombCMV   As Double
Dim BombCF2MV As Double
Dim ConCMV    As Double
Dim ConCF2MV  As Double
    '
BetaMV = Lp("BETA_G")
  
If FiancoSx = 1 Then
   'FiancoSx=F1
   'Controllo pezzo con lato serraggio in basso
    
    'TRATTO A
    BombAMV = Lp("iBom1Fsx") + Lp("iCorBom1Fsx") / 1000: BombAF2MV = Lp("iBom1Fdx") + Lp("iCorBom1Fdx") / 1000
     ConAMV = Lp("iCon1Fsx") + Lp("iCorCon1Fsx") / 1000: ConAF2MV = Lp("iCon1Fdx") + Lp("iCorCon1Fdx") / 1000
    
    'TRATTO B
    BombBMV = Lp("iBom2Fsx") + Lp("iCorBom2Fsx") / 1000: BombBF2MV = Lp("iBom2Fdx") + Lp("iCorBom2Fdx") / 1000
     ConBMV = Lp("iCon2Fsx") + Lp("iCorCon2Fsx") / 1000: ConBF2MV = Lp("iCon2Fdx") + Lp("iCorCon2Fdx") / 1000
    
    'TRATTO C
    BombCMV = Lp("iBom3Fsx") + Lp("iCorBom3Fsx") / 1000: BombCF2MV = Lp("iBom3Fdx") + Lp("iCorBom3Fdx") / 1000
     ConCMV = Lp("iCon3Fsx") + Lp("iCorCon3Fsx") / 1000: ConCF2MV = Lp("iCon3Fdx") + Lp("iCorCon3Fdx") / 1000
Else
   'FiancoSx=F2
    'Inverto solo le correzioni
   'Controllo pezzo con lato serraggio in alto
    
    'TRATTO A
    BombAF2MV = Lp("iBom1Fdx") + Lp("iCorBom1Fsx") / 1000: BombAMV = Lp("iBom1Fsx") + Lp("iCorBom1Fdx") / 1000
     ConAF2MV = Lp("iCon1Fdx") + Lp("iCorCon1Fsx") / 1000: ConAMV = Lp("iCon1Fsx") + Lp("iCorCon1Fdx") / 1000
        
    'TRATTO B
    BombBF2MV = Lp("iBom2Fdx") + Lp("iCorBom2Fsx") / 1000: BombBMV = Lp("iBom2Fsx") + Lp("iCorBom2Fdx") / 1000
     ConBF2MV = Lp("iCon2Fdx") + Lp("iCorCon2Fsx") / 1000: ConBMV = Lp("iCon2Fsx") + Lp("iCorCon2Fdx") / 1000
    
    'TRATTO C
    BombCF2MV = Lp("iBom3Fdx") + Lp("iCorBom3Fsx") / 1000: BombCMV = Lp("iBom3Fsx") + Lp("iCorBom3Fdx") / 1000
     ConCF2MV = Lp("iCon3Fdx") + Lp("iCorCon3Fsx") / 1000: ConCMV = Lp("iCon3Fsx") + Lp("iCorCon3Fdx") / 1000

End If

'
If (Lp("AT_DRESSING")) Then
  TW_TUNING = Lp("TW_TUNING")
    'TRATTO A
    BombAF2MV = Lp("iBom1Fdx") * (1 - TW_TUNING) + Lp("iCorBom1Fsx") / 1000: BombAMV = Lp("iBom1Fsx") * (1 - TW_TUNING) + Lp("iCorBom1Fdx") / 1000
    'BombAMV = BombAMV * (1 - TW_TUNING): BombAF2MV = BombAF2MV * (1 - TW_TUNING)
    'TRATTO B
    BombBF2MV = Lp("iBom2Fdx") * (1 - TW_TUNING) + Lp("iCorBom2Fsx") / 1000: BombBMV = Lp("iBom2Fsx") * (1 - TW_TUNING) + Lp("iCorBom2Fdx") / 1000
    'BombBMV = BombBMV * (1 - TW_TUNING): BombBF2MV = BombBF2MV * (1 - TW_TUNING)
    'TRATTO C
    BombCF2MV = Lp("iBom3Fdx") * (1 - TW_TUNING) + Lp("iCorBom3Fsx") / 1000: BombCMV = Lp("iBom3Fsx") * (1 - TW_TUNING) + Lp("iCorBom3Fdx") / 1000
    'BombCMV = BombCMV * (1 - TW_TUNING): BombCF2MV = BombCF2MV * (1 - TW_TUNING)
Else
 TW_TUNING = 0#
End If
'
'Calcolo per separare correzioni tra asse X e asse C
'
Call CalcolaBombConXC(AppBombXA, AppBombCA, AppConXA, AppConCA, BombAMV, BombAF2MV, ConAMV, ConAF2MV, BetaMV)
Call CalcolaBombConXC(AppBombXB, AppBombCB, AppConXB, AppConCB, BombBMV, BombBF2MV, ConBMV, ConBF2MV, BetaMV)
Call CalcolaBombConXC(AppBombXC, AppBombCC, AppConXC, AppConCC, BombCMV, BombCF2MV, ConCMV, ConCF2MV, BetaMV)
'
Dim BombFondoA!
Dim BombFondoB!
Dim BombFondoC!

Dim RaggioBOMB As Double

Dim ANGOLO      As Double
Dim CorsaRadA   As Double
Dim CorsaRadB   As Double
Dim CorsaRadC   As Double

' *** Agg 26.01.13
Dim RagBombCT1   As Double
Dim AppBombCT1  As Double
Dim AppConCT1   As Double
Dim RagBombCT2   As Double
Dim AppBombCT2  As Double
Dim AppConCT2   As Double
Dim RagBombCT3   As Double
Dim AppBombCT3  As Double
Dim AppConCT3   As Double

  If (FasciaA <> 0) Then
     If AppBombCA <> 0 Then
        RagBombCT1 = (FasciaA * FasciaA / (8 * Abs(AppBombCA))) + Abs(AppBombCA) / 2
        AppBombCT1 = Int(((2 * RagBombCT1 - Sqr(4 * RagBombCT1 * RagBombCT1 - FasciaAMV * FasciaAMV)) / 2) * 10000 + 0.5) / 10000
        If AppBombCA < 0 Then
           AppBombCT1 = -AppBombCT1
        End If
     Else
        AppBombCT1 = 0
     End If
     If AppConCA <> 0 Then
        AppConCT1 = AppConCA / FasciaA * FasciaAMV
        AppConCT1 = Int(AppConCT1 * 10000 + 0.5) / 10000
     Else
        AppConCT1 = 0
     End If
  Else
     AppBombCT1 = 0
     AppConCT1 = 0
  End If
  
  If (FasciaB <> 0) Then
     If AppBombCB <> 0 Then
        RagBombCT2 = (FasciaB * FasciaB / (8 * Abs(AppBombCB))) + Abs(AppBombCB) / 2
        AppBombCT2 = Int(((2 * RagBombCT2 - Sqr(4 * RagBombCT2 * RagBombCT2 - FasciaBMV * FasciaBMV)) / 2) * 10000 + 0.5) / 10000
        If AppBombCB < 0 Then
           AppBombCT2 = -AppBombCT2
        End If
     Else
        AppBombCT2 = 0
     End If
     If AppConCB <> 0 Then
        AppConCT2 = AppConCB / FasciaB * FasciaBMV
        AppConCT2 = Int(AppConCT2 * 10000 + 0.5) / 10000
     Else
        AppConCT2 = 0
     End If
  Else
     AppBombCT2 = 0
     AppConCT2 = 0
  End If
  
  If (FasciaC <> 0) Then
     If AppBombCC <> 0 Then
        RagBombCT3 = (FasciaC * FasciaC / (8 * Abs(AppBombCC))) + Abs(AppBombCC) / 2
        AppBombCT3 = Int(((2 * RagBombCT3 - Sqr(4 * RagBombCT3 * RagBombCT3 - FasciaCMV * FasciaCMV)) / 2) * 10000 + 0.5) / 10000
        If AppBombCC < 0 Then
           AppBombCT3 = -AppBombCT3
        End If
     Else
        AppBombCT3 = 0
     End If
     If AppConCC <> 0 Then
        AppConCT3 = AppConCC / FasciaC * FasciaCMV
        AppConCT3 = Int(AppConCT3 * 10000 + 0.5) / 10000
     Else
        AppConCT3 = 0
     End If
  Else
     AppBombCT3 = 0
     AppConCT3 = 0
  End If
  
  'Variabili appoggio usate per gestire le corr. elica diverse tra F1 e F2
  Call SP("RAPP[0,311]", frmt(AppBombCT1, 8))
  Call SP("RAPP[0,312]", frmt(AppConCT1, 8))
  Call SP("RAPP[0,314]", frmt(AppBombCT2, 8))
  Call SP("RAPP[0,315]", frmt(AppConCT2, 8))
  Call SP("RAPP[0,317]", frmt(AppBombCT3, 8))
  Call SP("RAPP[0,318]", frmt(AppConCT3, 8))
  
  'Angolo pressione pezzo in radianti
  ANGOLO = DGlob.ALFA
    
  'Modificato con ANGOLO (angolo pressione pezzo) al posto di AlfaMV  (angolo pressione mola)
  BombFondoA! = AppBombXA / Tan(ANGOLO): AppConXA = Int(AppConXA * 1000 + 0.5) / 1000
  BombFondoB! = AppBombXB / Tan(ANGOLO): AppConXB = Int(AppConXB * 1000 + 0.5) / 1000
  BombFondoC! = AppBombXC / Tan(ANGOLO): AppConXC = Int(AppConXC * 1000 + 0.5) / 1000
  
  '
  If (LatoPart = -1) Then 'Lato Testa portapezzo
      'TRATTO A
      If (BombFondoA! <> 0) Then
        RaggioBOMB = (FasciaA * FasciaA / (8 * Abs(BombFondoA!))) + Abs(BombFondoA!) / 2
        If (BombFondoA! > 0) Then
          RaggioBOMB = RaggioBOMB + DIAMETRO_MOLA / 2
        Else
          RaggioBOMB = RaggioBOMB - DIAMETRO_MOLA / 2
        End If
        RaggioBOMB = Int(RaggioBOMB * 1000 + 0.5) / 1000
        If (BombFondoA! > 0) Then RaggioBOMB = -RaggioBOMB
      Else
        RaggioBOMB = 0
      End If
      If (FasciaA <> 0) Then
      CorsaRadA = (AppConXA / Tan(ANGOLO)) / FasciaA * FasciaAMV
      CorsaRadA = Int(CorsaRadA * 10000 + 0.5) / 10000
      Else
      CorsaRadA = 0
      End If
      Call SP("CORSAASS1_G", FasciaAMV):     Call SP("CORSAASS1F2_G", FasciaAMV)
      Call SP("RAGGIOBOMB1_G", RaggioBOMB):  Call SP("RAGGIOBOMB1F2_G", RaggioBOMB)
      Call SP("CORSARAD1_G", CorsaRadA):     Call SP("CORSARAD1F2_G", CorsaRadA)
      'TRATTO B
      If (BombFondoB! <> 0) Then
        RaggioBOMB = (FasciaB * FasciaB / (8 * Abs(BombFondoB!))) + Abs(BombFondoB!) / 2
        If (BombFondoB! > 0) Then
          RaggioBOMB = RaggioBOMB + DIAMETRO_MOLA / 2
        Else
          RaggioBOMB = RaggioBOMB - DIAMETRO_MOLA / 2
        End If
        RaggioBOMB = Int(RaggioBOMB * 1000 + 0.5) / 1000
        If (BombFondoB! > 0) Then RaggioBOMB = -RaggioBOMB
      Else
        RaggioBOMB = 0
      End If
      If (FasciaB <> 0) Then
      CorsaRadB = (AppConXB / Tan(ANGOLO)) / FasciaB * FasciaBMV
      CorsaRadB = Int(CorsaRadB * 10000 + 0.5) / 10000
      Else
      CorsaRadB = 0
      End If
      Call SP("CORSAASS2_G", FasciaBMV):     Call SP("CORSAASS2F2_G", FasciaBMV)
      Call SP("RAGGIOBOMB2_G", RaggioBOMB):  Call SP("RAGGIOBOMB2F2_G", RaggioBOMB)
      Call SP("CORSARAD2_G", CorsaRadB):     Call SP("CORSARAD2F2_G", CorsaRadB)
      'TRATTO C
      If (BombFondoC! <> 0) Then
        RaggioBOMB = (FasciaC * FasciaC / (8 * Abs(BombFondoC!))) + Abs(BombFondoC!) / 2
        If (BombFondoC! > 0) Then
          RaggioBOMB = RaggioBOMB + DIAMETRO_MOLA / 2
        Else
          RaggioBOMB = RaggioBOMB - DIAMETRO_MOLA / 2
        End If
        RaggioBOMB = Int(RaggioBOMB * 1000 + 0.5) / 1000
        If (BombFondoC! > 0) Then RaggioBOMB = -RaggioBOMB
      Else
        RaggioBOMB = 0
      End If
      If (FasciaC <> 0) Then
      CorsaRadC = (AppConXC / Tan(ANGOLO)) / FasciaC * FasciaCMV
      CorsaRadC = Int(CorsaRadC * 10000 + 0.5) / 10000
      Else
      CorsaRadC = 0
      End If
      Call SP("CORSAASS3_G", FasciaCMV):     Call SP("CORSAASS3F2_G", FasciaCMV)
      Call SP("RAGGIOBOMB3_G", RaggioBOMB):  Call SP("RAGGIOBOMB3F2_G", RaggioBOMB)
      Call SP("CORSARAD3_G", CorsaRadC):     Call SP("CORSARAD3F2_G", CorsaRadC)
      '
  Else   'Lato Partenza Contropunta
    '
    Select Case nZone
    Case 3
      'TRATTO A -> Corsa 3
      If (BombFondoA! <> 0) Then
        RaggioBOMB = (FasciaA * FasciaA / (8 * Abs(BombFondoA!))) + Abs(BombFondoA!) / 2
        If (BombFondoA! > 0) Then
          RaggioBOMB = RaggioBOMB + DIAMETRO_MOLA / 2
        Else
          RaggioBOMB = RaggioBOMB - DIAMETRO_MOLA / 2
        End If
        RaggioBOMB = Int(RaggioBOMB * 1000 + 0.5) / 1000
        If (BombFondoA! > 0) Then RaggioBOMB = -RaggioBOMB
      Else
        RaggioBOMB = 0
      End If
      If (FasciaA <> 0) Then
      CorsaRadA = (AppConXA / Tan(ANGOLO)) / FasciaA * FasciaAMV
      CorsaRadA = Int(CorsaRadA * 10000 + 0.5) / 10000
      Else
      CorsaRadA = 0
      End If
      Call SP("CORSAASS3_G", FasciaAMV):     Call SP("CORSAASS3F2_G", FasciaAMV)
      Call SP("RAGGIOBOMB3_G", -RaggioBOMB): Call SP("RAGGIOBOMB3F2_G", -RaggioBOMB)
      Call SP("CORSARAD3_G", -CorsaRadA):    Call SP("CORSARAD3F2_G", -CorsaRadA)
      'TRATTO B -> Corsa 2
      If (BombFondoB! <> 0) Then
        RaggioBOMB = (FasciaB * FasciaB / (8 * Abs(BombFondoB!))) + Abs(BombFondoB!) / 2
        If (BombFondoB! > 0) Then
          RaggioBOMB = RaggioBOMB + DIAMETRO_MOLA / 2
        Else
          RaggioBOMB = RaggioBOMB - DIAMETRO_MOLA / 2
        End If
        RaggioBOMB = Int(RaggioBOMB * 1000 + 0.5) / 1000
        If (BombFondoB! > 0) Then RaggioBOMB = -RaggioBOMB
      Else
        RaggioBOMB = 0
      End If
      If (FasciaB <> 0) Then
      CorsaRadB = (AppConXB / Tan(ANGOLO)) / FasciaB * FasciaBMV
      CorsaRadB = Int(CorsaRadB * 10000 + 0.5) / 10000
      Else
      CorsaRadB = 0
      End If
      Call SP("CORSAASS2_G", FasciaBMV):     Call SP("CORSAASS2F2_G", FasciaBMV)
      Call SP("RAGGIOBOMB2_G", -RaggioBOMB): Call SP("RAGGIOBOMB2F2_G", -RaggioBOMB)
      Call SP("CORSARAD2_G", -CorsaRadB):    Call SP("CORSARAD2F2_G", -CorsaRadB)
      'TRATTO C -> Corsa 1
      If (BombFondoC! <> 0) Then
        RaggioBOMB = (FasciaC * FasciaC / (8 * Abs(BombFondoC!))) + Abs(BombFondoC!) / 2
        If (BombFondoC! > 0) Then
          RaggioBOMB = RaggioBOMB + DIAMETRO_MOLA / 2
        Else
          RaggioBOMB = RaggioBOMB - DIAMETRO_MOLA / 2
        End If
        RaggioBOMB = Int(RaggioBOMB * 1000 + 0.5) / 1000
        If (BombFondoC! > 0) Then RaggioBOMB = -RaggioBOMB
      Else
        RaggioBOMB = 0
      End If
      If (FasciaC <> 0) Then
      CorsaRadC = (AppConXC / Tan(ANGOLO)) / FasciaC * FasciaCMV
      CorsaRadC = Int(CorsaRadC * 10000 + 0.5) / 10000
      Else
      CorsaRadC = 0
      End If
      Call SP("CORSAASS1_G", FasciaCMV):     Call SP("CORSAASS1F2_G", FasciaCMV)
      Call SP("RAGGIOBOMB1_G", -RaggioBOMB): Call SP("RAGGIOBOMB1F2_G", -RaggioBOMB)
      Call SP("CORSARAD1_G", -CorsaRadC):    Call SP("CORSARAD1F2_G", -CorsaRadC)
    Case 2
      'TRATTO A -> Corsa 2
      If (BombFondoA! <> 0) Then
        RaggioBOMB = (FasciaA * FasciaA / (8 * Abs(BombFondoA!))) + Abs(BombFondoA!) / 2
        If (BombFondoA! > 0) Then
          RaggioBOMB = RaggioBOMB + DIAMETRO_MOLA / 2
        Else
          RaggioBOMB = RaggioBOMB - DIAMETRO_MOLA / 2
        End If
        RaggioBOMB = Int(RaggioBOMB * 1000 + 0.5) / 1000
        If (BombFondoA! > 0) Then RaggioBOMB = -RaggioBOMB
      Else
        RaggioBOMB = 0
      End If
      If (FasciaA <> 0) Then
      CorsaRadA = (AppConXA / Tan(ANGOLO)) / FasciaA * FasciaAMV
      CorsaRadA = Int(CorsaRadA * 10000 + 0.5) / 10000
      Else
      CorsaRadA = 0
      End If
      Call SP("CORSAASS2_G", FasciaAMV):     Call SP("CORSAASS2F2_G", FasciaAMV)
      Call SP("RAGGIOBOMB2_G", -RaggioBOMB): Call SP("RAGGIOBOMB2F2_G", -RaggioBOMB)
      Call SP("CORSARAD2_G", -CorsaRadA):    Call SP("CORSARAD2F2_G", -CorsaRadA)
      'TRATTO B -> Corsa 1
      If (BombFondoB! <> 0) Then
        RaggioBOMB = (FasciaB * FasciaB / (8 * Abs(BombFondoB!))) + Abs(BombFondoB!) / 2
        If (BombFondoB! > 0) Then
          RaggioBOMB = RaggioBOMB + DIAMETRO_MOLA / 2
        Else
          RaggioBOMB = RaggioBOMB - DIAMETRO_MOLA / 2
        End If
        RaggioBOMB = Int(RaggioBOMB * 1000 + 0.5) / 1000
        If (BombFondoB! > 0) Then RaggioBOMB = -RaggioBOMB
      Else
        RaggioBOMB = 0
      End If
      If (FasciaB <> 0) Then
      CorsaRadB = (AppConXB / Tan(ANGOLO)) / FasciaB * FasciaBMV
      CorsaRadB = Int(CorsaRadB * 10000 + 0.5) / 10000
      Else
      CorsaRadB = 0
      End If
      Call SP("CORSAASS1_G", FasciaBMV):     Call SP("CORSAASS1F2_G", FasciaBMV)
      Call SP("RAGGIOBOMB1_G", -RaggioBOMB): Call SP("RAGGIOBOMB1F2_G", -RaggioBOMB)
      Call SP("CORSARAD1_G", -CorsaRadB):    Call SP("CORSARAD1F2_G", -CorsaRadB)
      'TRATTO C
      Call SP("CORSAASS3_G", 0):             Call SP("CORSAASS3F2_G", 0)
      Call SP("RAGGIOBOMB3_G", 0):           Call SP("RAGGIOBOMB3F2_G", 0)
      Call SP("CORSARAD3_G", 0):             Call SP("CORSARAD3F2_G", 0)
      '
    Case 1
      'TRATTO A -> Corsa 1
      If (BombFondoA! <> 0) Then
        RaggioBOMB = (FasciaA * FasciaA / (8 * Abs(BombFondoA!))) + Abs(BombFondoA!) / 2
        If (BombFondoA! > 0) Then
          RaggioBOMB = RaggioBOMB + DIAMETRO_MOLA / 2
        Else
          RaggioBOMB = RaggioBOMB - DIAMETRO_MOLA / 2
        End If
        RaggioBOMB = Int(RaggioBOMB * 1000 + 0.5) / 1000
        If (BombFondoA! > 0) Then RaggioBOMB = -RaggioBOMB
      Else
        RaggioBOMB = 0
      End If
      If (FasciaA <> 0) Then
      CorsaRadA = (AppConXA / Tan(ANGOLO)) / FasciaA * FasciaAMV
      CorsaRadA = Int(CorsaRadA * 10000 + 0.5) / 10000
      Else
      CorsaRadA = 0
      End If
      Call SP("CORSAASS1_G", FasciaAMV):     Call SP("CORSAASS1F2_G", FasciaAMV)
      Call SP("RAGGIOBOMB1_G", -RaggioBOMB): Call SP("RAGGIOBOMB1F2_G", -RaggioBOMB)
      Call SP("CORSARAD1_G", -CorsaRadA):    Call SP("CORSARAD1F2_G", -CorsaRadA)
      'TRATTO B
      Call SP("CORSAASS2_G", 0):             Call SP("CORSAASS2F2_G", 0)
      Call SP("RAGGIOBOMB2_G", 0):           Call SP("RAGGIOBOMB2F2_G", 0)
      Call SP("CORSARAD2_G", 0):             Call SP("CORSARAD2F2_G", 0)
      'TRATTO C
      Call SP("CORSAASS3_G", 0):             Call SP("CORSAASS3F2_G", 0)
      Call SP("RAGGIOBOMB3_G", 0):           Call SP("RAGGIOBOMB3F2_G", 0)
      Call SP("CORSARAD3_G", 0):             Call SP("CORSARAD3F2_G", 0)
      '
    End Select
  End If
  
  'Gestione correzione elica nel caso di eliche differenti tra F1 e F2
  If DbetaF1 <> 0 Then
  If (LatoPart = -1) Then
   'Lato Testa portapezzo
   'TRATTO A
      If (FasciaA <> 0) Then
          CorConTA = FasciaA * Tan(DbetaF1)  'Correzione conicit� zona A fianco 1
          'Nota la correzione x fianco 2 � identica al fianco 1 dovendo rettificare due fianchi insieme
          CorsaRadTA = -LatoPart * (CorConTA / Tan(ANGOLO)) / FasciaA * FasciaAMV
          CorsaRadTA = Int(CorsaRadTA * 10000 + 0.5) / 10000
      Else
          CorsaRadTA = 0
      End If
      Call SP("CORSARADT1_G", CorsaRadTA)
   'TRATTO B
      If (FasciaB <> 0) Then
          CorConTB = FasciaB * Tan(DbetaF1)  'Correzione conicit� zona B fianco 1
          'Nota la correzione x fianco 2 � identica al fianco 1 dovendo rettificare due fianchi insieme
          CorsaRadTB = -LatoPart * (CorConTB / Tan(ANGOLO)) / FasciaB * FasciaBMV
          CorsaRadTB = Int(CorsaRadTB * 10000 + 0.5) / 10000
      Else
          CorsaRadTB = 0
      End If
      Call SP("CORSARADT2_G", CorsaRadTB)
   'TRATTO C
      If (FasciaC <> 0) Then
          CorConTC = FasciaC * Tan(DbetaF1)  'Correzione conicit� zona C fianco 1
          'Nota la correzione x fianco 2 � identica al fianco 1 dovendo rettificare due fianchi insieme
          CorsaRadTC = -LatoPart * (CorConTC / Tan(ANGOLO)) / FasciaC * FasciaCMV
          CorsaRadTC = Int(CorsaRadTC * 10000 + 0.5) / 10000
      Else
          CorsaRadTC = 0
      End If
      Call SP("CORSARADT3_G", CorsaRadTC)
  Else
   'Lato Contropunta
   Select Case nZone
   Case 3
   'TRATTO A -->Corsa 3
      If (FasciaA <> 0) Then
          CorConTA = FasciaA * Tan(DbetaF1)  'Correzione conicit� zona A fianco 1
          'Nota la correzione x fianco 2 � identica al fianco 1 dovendo rettificare due fianchi insieme
          CorsaRadTA = -LatoPart * (CorConTA / Tan(ANGOLO)) / FasciaA * FasciaAMV
          CorsaRadTA = Int(CorsaRadTA * 10000 + 0.5) / 10000
      Else
          CorsaRadTA = 0
      End If
      Call SP("CORSARADT3_G", CorsaRadTA)
   
   'TRATTO B -->Corsa 2
      If (FasciaB <> 0) Then
          CorConTB = FasciaB * Tan(DbetaF1)  'Correzione conicit� zona B fianco 1
          'Nota la correzione x fianco 2 � identica al fianco 1 dovendo rettificare due fianchi insieme
          CorsaRadTB = -LatoPart * (CorConTB / Tan(ANGOLO)) / FasciaB * FasciaBMV
          CorsaRadTB = Int(CorsaRadTB * 10000 + 0.5) / 10000
      Else
          CorsaRadTB = 0
      End If
      Call SP("CORSARADT2_G", CorsaRadTB)
   
   'TRATTO C -->Corsa 1
      If (FasciaC <> 0) Then
          CorConTC = FasciaC * Tan(DbetaF1)  'Correzione conicit� zona C fianco 1
          'Nota la correzione x fianco 2 � identica al fianco 1 dovendo rettificare due fianchi insieme
          CorsaRadTC = -LatoPart * (CorConTC / Tan(ANGOLO)) / FasciaC * FasciaCMV
          CorsaRadTC = Int(CorsaRadTC * 10000 + 0.5) / 10000
      Else
          CorsaRadTC = 0
      End If
      Call SP("CORSARADT1_G", CorsaRadTC)
      
   Case 2
   
   'TRATTO A -->Corsa 2
      If (FasciaA <> 0) Then
          CorConTA = FasciaA * Tan(DbetaF1)  'Correzione conicit� zona A fianco 1
          'Nota la correzione x fianco 2 � identica al fianco 1 dovendo rettificare due fianchi insieme
          CorsaRadTA = -LatoPart * (CorConTA / Tan(ANGOLO)) / FasciaA * FasciaAMV
          CorsaRadTA = Int(CorsaRadTA * 10000 + 0.5) / 10000
      Else
          CorsaRadTA = 0
      End If
      Call SP("CORSARADT2_G", CorsaRadTA)
   
   'TRATTO B -->Corsa 1
      If (FasciaB <> 0) Then
          CorConTB = FasciaB * Tan(DbetaF1)  'Correzione conicit� zona B fianco 1
          'Nota la correzione x fianco 2 � identica al fianco 1 dovendo rettificare due fianchi insieme
          CorsaRadTB = -LatoPart * (CorConTB / Tan(ANGOLO)) / FasciaB * FasciaBMV
          CorsaRadTB = Int(CorsaRadTB * 10000 + 0.5) / 10000
      Else
          CorsaRadTB = 0
      End If
      Call SP("CORSARADT1_G", CorsaRadTB)
   
   'TRATTO C
      Call SP("CORSARADT3_G", 0)
      
   Case 1
   'TRATTO A -->Corsa 1
      If (FasciaA <> 0) Then
          CorConTA = FasciaA * Tan(DbetaF1)  'Correzione conicit� zona A fianco 1
          'Nota la correzione x fianco 2 � identica al fianco 1 dovendo rettificare due fianchi insieme
          CorsaRadTA = -LatoPart * (CorConTA / Tan(ANGOLO)) / FasciaA * FasciaAMV
          CorsaRadTA = Int(CorsaRadTA * 10000 + 0.5) / 10000
      Else
          CorsaRadTA = 0
      End If
      Call SP("CORSARADT1_G", CorsaRadTA)
   'TRATTO B
      Call SP("CORSARADT2_G", 0)
   'TRATTO C
      Call SP("CORSARADT3_G", 0)
   
   End Select
  End If
  Else
      Call SP("CORSARADT1_G", 0)
      Call SP("CORSARADT2_G", 0)
      Call SP("CORSARADT3_G", 0)
  End If
  '
  'Gestione correzione elica automatica da correzione di passo
  '
  'FiancoSx = F1
   ActualErrSXFHB = Lp("iERRSXFHB")                        'Errore del grafico
   If ActualErrSXFHB <> 0 Then
      CorrFHBSX = Lp("ERRTOTSXFHB_G")                      'leggo Errore cumulato
      Call SP("iERRPRESXFHB", CorrFHBSX)                  'Errore cumulato precedente
      Call SP("ERRTOTSXFHB_G", CorrFHBSX + ActualErrSXFHB) 'Scrivo nuovo errore cumulato
      ActualErrSXFHB = 0
      Call SP("iERRSXFHB", ActualErrSXFHB)
   End If

  'FiancoDx = F2
   ActualErrDXFHB = Lp("iERRDXFHB")        'Errore del grafico
   If ActualErrDXFHB <> 0 Then
      CorrFHBDX = Lp("ERRTOTDXFHB_G")      'leggo Errore cumulato
      Call SP("iERRPREDXFHB", CorrFHBDX)  'Errore cumulato precedente
      Call SP("ERRTOTDXFHB_G", CorrFHBDX + ActualErrDXFHB)
      ActualErrDXFHB = 0
      Call SP("iERRDXFHB", ActualErrDXFHB)
   End If
  
  'Calcolo errore passo medio
  ErrPassoSx = Lp("ERRTOTSXFHB_G") / 1000
  ErrPassoDx = Lp("ERRTOTDXFHB_G") / 1000
  ErrPasso = (ErrPassoSx + ErrPassoDx) / 2
  Call SP("CORRPASSO", ErrPasso)
  If MODALITA = "SPF" Then
     stmp = frmt(ErrPasso, 4)
     Call OPC_SCRIVI_DATO("/ACC/NCK/UGUD/CORRPASSO", stmp)
  End If
  
  If FiancoSx = 1 Then
     ErrPassoF1 = ErrPassoSx
     ErrPassoF2 = ErrPassoDx
  Else
     ErrPassoF1 = ErrPassoDx
     ErrPassoF2 = ErrPassoSx
  End If
  
  'Calcolo correzioni conicit� per entrambi i fianchi
  If DGlob.Beta >= 0 Then
     'fHb residuo
     fHb_F1 = ErrPassoF1 - ErrPasso
     fHb_F2 = ErrPassoF2 - ErrPasso
     'Valore di correzione conicit� riferita alla fascia controllata (FasciaFhb)
     Con_F1 = -fHb_F1
     Con_F2 = fHb_F2
  End If
  
  If DGlob.Beta < 0 Then
     fHb_F1 = ErrPassoF1 - ErrPasso
     fHb_F2 = ErrPassoF2 - ErrPasso
     Con_F1 = fHb_F1
     Con_F2 = -fHb_F2
  End If
  
  Dim ConTot_F1    As Double
  Dim ConTot_F2    As Double
  Dim ConA_F1      As Double
  Dim ConA_F2      As Double
  Dim ConB_F1      As Double
  Dim ConB_F2      As Double
  Dim ConC_F1      As Double
  Dim ConC_F2      As Double
  
  'Estrapolare le corr. conicit� Con_F1 e Con_F2 al 100% della fascia
  ConTot_F1 = Con_F1 / FasciaFhb * FASCIA
  ConTot_F2 = Con_F2 / FasciaFhb * FASCIA

  Select Case nZone
     Case 1
        'Correzione su Zona 1
        ConA_F1 = ConTot_F1 / FASCIA * FasciaA
        ConA_F2 = ConTot_F2 / FASCIA * FasciaA
        'Correzione compreso extracorse
        Call SP("iCONASXFHB", ConA_F1 / FasciaA * FasciaAMV)
        Call SP("iCONADXFHB", ConA_F2 / FasciaA * FasciaAMV)
        Call SP("iCONBSXFHB", 0)
        Call SP("iCONBDXFHB", 0)
        Call SP("iCONCSXFHB", 0)
        Call SP("iCONCDXFHB", 0)
     
     Case 2
        'Correzione su Zona 1
        ConA_F1 = ConTot_F1 / FASCIA * FasciaA
        ConA_F2 = ConTot_F2 / FASCIA * FasciaA
        'Correzione su Zona 2
        ConB_F1 = ConTot_F1 / FASCIA * FasciaB
        ConB_F2 = ConTot_F2 / FASCIA * FasciaB
        '
        Call SP("iCONASXFHB", ConA_F1 / FasciaA * FasciaAMV)
        Call SP("iCONADXFHB", ConA_F2 / FasciaA * FasciaAMV)
        Call SP("iCONBSXFHB", ConB_F1 / FasciaB * FasciaBMV)
        Call SP("iCONBDXFHB", ConB_F2 / FasciaB * FasciaBMV)
        Call SP("iCONCSXFHB", 0)
        Call SP("iCONCDXFHB", 0)
     Case 3
        'Correzione su Zona 1
        ConA_F1 = ConTot_F1 / FASCIA * FasciaA
        ConA_F2 = ConTot_F2 / FASCIA * FasciaA
        'Correzione su Zona 2
        ConB_F1 = ConTot_F1 / FASCIA * FasciaB
        ConB_F2 = ConTot_F2 / FASCIA * FasciaB
        'Correzione su Zona 3
        ConC_F1 = ConTot_F1 / FASCIA * FasciaC
        ConC_F2 = ConTot_F2 / FASCIA * FasciaC
        '
        Call SP("iCONASXFHB", ConA_F1 / FasciaA * FasciaAMV)
        Call SP("iCONADXFHB", ConA_F2 / FasciaA * FasciaAMV)
        Call SP("iCONBSXFHB", ConB_F1 / FasciaB * FasciaBMV)
        Call SP("iCONBDXFHB", ConB_F2 / FasciaB * FasciaBMV)
        Call SP("iCONCSXFHB", ConC_F1 / FasciaC * FasciaCMV)
        Call SP("iCONCDXFHB", ConC_F2 / FasciaC * FasciaCMV)
  End Select
  
  'Considero  solo la correzione del fianco Sx che � complementare al fDx dovendo rettificare due fianchi insieme
  iConAfHb = Lp("iCONASXFHB")
  iConBfHb = Lp("iCONBSXFHB")
  iConCfHb = Lp("iCONCSXFHB")
  
  If (LatoPart = -1) Then
   'Lato Testa portapezzo
   'TRATTO A
      If (FasciaA <> 0) Then
          'Nota la correzione x fianco 2 � identica al fianco 1 dovendo rettificare due fianchi insieme
          CorsaRadZA = -LatoPart * (iConAfHb / Tan(ANGOLO))  '/ FasciaA * FasciaAMV
          CorsaRadZA = Int(CorsaRadZA * 10000 + 0.5) / 10000
      Else
          CorsaRadZA = 0
      End If
      Call SP("CORX1FHB", CorsaRadZA)
      If MODALITA = "SPF" Then
         stmp = Trim$(frmt(CorsaRadZA, 4))
         Call OPC_SCRIVI_DATO("/ACC/NCK/GUD7/CORX1FHB", stmp)
      End If
   'TRATTO B
      If (FasciaB <> 0) Then
          'Nota la correzione x fianco 2 � identica al fianco 1 dovendo rettificare due fianchi insieme
          CorsaRadZB = -LatoPart * (iConBfHb / Tan(ANGOLO))  '/ FasciaB * FasciaBMV
          CorsaRadZB = Int(CorsaRadZB * 10000 + 0.5) / 10000
      Else
          CorsaRadZB = 0
      End If
      Call SP("CORX2FHB", CorsaRadZB)
      If MODALITA = "SPF" Then
         stmp = Trim$(frmt(CorsaRadZB, 4))
        Call OPC_SCRIVI_DATO("/ACC/NCK/GUD7/CORX2FHB", stmp)
      End If
   'TRATTO C
      If (FasciaC <> 0) Then
          'Nota la correzione x fianco 2 � identica al fianco 1 dovendo rettificare due fianchi insieme
          CorsaRadZC = -LatoPart * (iConCfHb / Tan(ANGOLO))  '/ FasciaC * FasciaCMV
          CorsaRadZC = Int(CorsaRadZC * 10000 + 0.5) / 10000
      Else
          CorsaRadZC = 0
      End If
      Call SP("CORX3FHB", CorsaRadZC)
      If MODALITA = "SPF" Then
         stmp = Trim$(frmt(CorsaRadZC, 4))
         Call OPC_SCRIVI_DATO("/ACC/NCK/GUD7/CORX3FHB", stmp)
      End If
  Else
   'Lato Contropunta
   Select Case nZone
   Case 3
   'TRATTO A -->Corsa 3
      If (FasciaA <> 0) Then
          'Nota la correzione x fianco 2 � identica al fianco 1 dovendo rettificare due fianchi insieme
          CorsaRadZA = -LatoPart * (iConAfHb / Tan(ANGOLO)) '/ FasciaA * FasciaAMV
          CorsaRadZA = Int(CorsaRadZA * 10000 + 0.5) / 10000
      Else
          CorsaRadZA = 0
      End If
      Call SP("CORX3FHB", CorsaRadZA)
      If MODALITA = "SPF" Then
         stmp = Trim$(frmt(CorsaRadZA, 4))
         Call OPC_SCRIVI_DATO("/ACC/NCK/GUD7/CORX3FHB", stmp)
      End If
   'TRATTO B -->Corsa 2
      If (FasciaB <> 0) Then
          'Nota la correzione x fianco 2 � identica al fianco 1 dovendo rettificare due fianchi insieme
          CorsaRadZB = -LatoPart * (iConBfHb / Tan(ANGOLO)) '/ FasciaB * FasciaBMV
          CorsaRadZB = Int(CorsaRadZB * 10000 + 0.5) / 10000
      Else
          CorsaRadZB = 0
      End If
      Call SP("CORX2FHB", CorsaRadZB)
      If MODALITA = "SPF" Then
         stmp = Trim$(frmt(CorsaRadZB, 4))
         Call OPC_SCRIVI_DATO("/ACC/NCK/GUD7/CORX2FHB", stmp)
      End If
   'TRATTO C -->Corsa 1
      If (FasciaC <> 0) Then
          'Nota la correzione x fianco 2 � identica al fianco 1 dovendo rettificare due fianchi insieme
          CorsaRadZC = -LatoPart * (iConCfHb / Tan(ANGOLO)) '/ FasciaC * FasciaCMV
          CorsaRadZC = Int(CorsaRadZC * 10000 + 0.5) / 10000
      Else
          CorsaRadZC = 0
      End If
      Call SP("CORX1FHB", CorsaRadZC)
      If MODALITA = "SPF" Then
         stmp = Trim$(frmt(CorsaRadZC, 4))
         Call OPC_SCRIVI_DATO("/ACC/NCK/GUD7/CORX1FHB", stmp)
      End If
   Case 2
   
   'TRATTO A -->Corsa 2
      If (FasciaA <> 0) Then
          'Nota la correzione x fianco 2 � identica al fianco 1 dovendo rettificare due fianchi insieme
          CorsaRadZA = -LatoPart * (iConAfHb / Tan(ANGOLO)) '/ FasciaA * FasciaAMV
          CorsaRadZA = Int(CorsaRadZA * 10000 + 0.5) / 10000
      Else
          CorsaRadZA = 0
      End If
      Call SP("CORX2FHB", CorsaRadZA)
      If MODALITA = "SPF" Then
         stmp = Trim$(frmt(CorsaRadZA, 4))
         Call OPC_SCRIVI_DATO("/ACC/NCK/GUD7/CORX2FHB", stmp)
      End If
   'TRATTO B -->Corsa 1
      If (FasciaB <> 0) Then
          'Nota la correzione x fianco 2 � identica al fianco 1 dovendo rettificare due fianchi insieme
          CorsaRadZB = -LatoPart * (iConBfHb / Tan(ANGOLO)) '/ FasciaB * FasciaBMV
          CorsaRadZB = Int(CorsaRadZB * 10000 + 0.5) / 10000
      Else
          CorsaRadZB = 0
      End If
      Call SP("CORX1FHB", CorsaRadZB)
      If MODALITA = "SPF" Then
         stmp = Trim$(frmt(CorsaRadZB, 4))
         Call OPC_SCRIVI_DATO("/ACC/NCK/GUD7/CORX1FHB", stmp)
      End If
   'TRATTO C
      Call SP("CORX3FHB", 0)
      If MODALITA = "SPF" Then
         stmp = "0"
         Call OPC_SCRIVI_DATO("/ACC/NCK/GUD7/CORX3FHB", stmp)
      End If
   Case 1
   'TRATTO A -->Corsa 1
      If (FasciaA <> 0) Then
          'Nota la correzione x fianco 2 � identica al fianco 1 dovendo rettificare due fianchi insieme
          CorsaRadZA = -LatoPart * (iConAfHb / Tan(ANGOLO)) '/ FasciaA * FasciaAMV
          CorsaRadZA = Int(CorsaRadZA * 10000 + 0.5) / 10000
      Else
          CorsaRadZA = 0
      End If
      Call SP("CORX1FHB", CorsaRadZA)
      If MODALITA = "SPF" Then
         stmp = Trim$(frmt(CorsaRadZA, 4))
         Call OPC_SCRIVI_DATO("/ACC/NCK/GUD7/CORX1FHB", stmp)
      End If
   'TRATTO B
      Call SP("CORX2FHB", 0)
      If MODALITA = "SPF" Then
         stmp = "0"
         Call OPC_SCRIVI_DATO("/ACC/NCK/GUD7/CORX2FHB", stmp)
      End If
   'TRATTO C
      Call SP("CORX3FHB", 0)
      If MODALITA = "SPF" Then
         stmp = "0"
         Call OPC_SCRIVI_DATO("/ACC/NCK/GUD7/CORX3FHB", stmp)
      End If
   End Select
  End If
  
  '
  RaggioBOMB = Lp("CORSARAD1_G") + Lp("CORSARAD2_G") + Lp("CORSARAD3_G")
  Call SP("MEZZACORSARAD_G", Int(RaggioBOMB * 1000 / 2 + 0.5) / 1000)
  'Calcola e disegna evolvente
  Call CALCOLO_INGRANAGGIO_ESTERNO_MOLAVITE
'
'
Dim ShiftC1    As Single
Dim ShiftC1Pas As Single
Dim ShiftC2    As Single
Dim ShiftC2Pas As Single
Dim ShiftC3    As Single
Dim ShiftC3Pas As Single
Dim ShiftC4    As Single
Dim ShiftC4Pas As Single

'Aggiorno valore dello shift x passata da shift x mm

ShiftC1 = Lp("CYC[0,5]")
ShiftC1Pas = ShiftC1 * CorsaTot
ShiftC1Pas = Int(ShiftC1Pas * 100) / 100
Call SP("SHF1", ShiftC1Pas)

ShiftC2 = Lp("CYC[0,25]")
ShiftC2Pas = ShiftC2 * CorsaTot
ShiftC2Pas = Int(ShiftC2Pas * 100) / 100
Call SP("SHF2", ShiftC2Pas)

ShiftC3 = Lp("CYC[0,45]")
ShiftC3Pas = ShiftC3 * CorsaTot
ShiftC3Pas = Int(ShiftC3Pas * 100) / 100
Call SP("SHF3", ShiftC3Pas)

ShiftC4 = Lp("CYC[0,65]")
ShiftC4Pas = ShiftC4 * CorsaTot
ShiftC4Pas = Int(ShiftC4Pas * 100) / 100
Call SP("SHF4", ShiftC4Pas)

'Gestione correzioni fHa Nuova
Dim CorFHA1mm As Double
Dim CorFHA2mm As Double

If FiancoSx = 1 Then
   'Controllo con lato serraggio in Basso
   'FiancoSx = F1
   ActualCorrF1 = Lp("iERRFHAFSX")
   
   If ActualCorrF1 <> 0 Then
      CorrfHaF1 = Lp("COREVFHASX_G")
      Call SP("iCORPREFHAFSX", CorrfHaF1)
      tmp = CorrfHaF1 + ActualCorrF1
      CorFHA1mm = tmp / 1000
      Call SP("CORFHA1", CorFHA1mm)
      If MODALITA = "SPF" Then
         stmp = frmt(CorFHA1mm, 4)
         Call OPC_SCRIVI_DATO("/ACC/NCK/UGUD/CORFHA1", stmp)
      End If
      Call SP("COREVFHASX_G", tmp)
      ActualCorrF1 = 0
      Call SP("iERRFHAFSX", ActualCorrF1)
   Else
      CorrfHaF1 = Lp("COREVFHASX_G")
      CorFHA1mm = CorrfHaF1 / 1000
      Call SP("CORFHA1", CorFHA1mm)
      If MODALITA = "SPF" Then
         stmp = frmt(CorFHA1mm, 4)
         Call OPC_SCRIVI_DATO("/ACC/NCK/UGUD/CORFHA1", stmp)
      End If
   End If

   'FiancoDx = F2
   ActualCorrF2 = Lp("iERRFHAFDX")
   If ActualCorrF2 <> 0 Then
      CorrfHaF2 = Lp("COREVFHADX_G")
      Call SP("iCORPREFHAFDX", CorrfHaF2)
      tmp = CorrfHaF2 + ActualCorrF2
      CorFHA2mm = tmp / 1000
      Call SP("CORFHA2", CorFHA2mm)
      If MODALITA = "SPF" Then
         stmp = frmt(CorFHA2mm, 4)
         Call OPC_SCRIVI_DATO("/ACC/NCK/UGUD/CORFHA2", stmp)
      End If
      Call SP("COREVFHADX_G", tmp)
      ActualCorrF2 = 0
      Call SP("iERRFHAFDX", ActualCorrF2)
   Else
      CorrfHaF2 = Lp("COREVFHADX_G")
      CorFHA2mm = CorrfHaF2 / 1000
      Call SP("CORFHA2", CorFHA2mm)
      If MODALITA = "SPF" Then
         stmp = frmt(CorFHA2mm, 4)
         Call OPC_SCRIVI_DATO("/ACC/NCK/UGUD/CORFHA2", stmp)
      End If
   End If

Else
   'Controllo con lato serraggio in Alto
   'FiancoSx = F2
   ActualCorrF2 = Lp("iERRFHAFSX")
   If ActualCorrF2 <> 0 Then
      CorrfHaF2 = Lp("COREVFHASX_G")
      Call SP("iCORPREFHAFSX", CorrfHaF2)
      tmp = CorrfHaF2 + ActualCorrF2
      CorFHA2mm = tmp / 1000
      Call SP("CORFHA2", CorFHA2mm)
      If MODALITA = "SPF" Then
         stmp = frmt(CorFHA2mm, 4)
         Call OPC_SCRIVI_DATO("/ACC/NCK/UGUD/CORFHA2", stmp)
      End If
      Call SP("COREVFHASX_G", tmp)
      ActualCorrF2 = 0
      Call SP("iERRFHAFSX", ActualCorrF2)
   Else
      CorrfHaF2 = Lp("COREVFHASX_G")
      CorFHA2mm = CorrfHaF2 / 1000
      Call SP("CORFHA2", CorFHA2mm)
      If MODALITA = "SPF" Then
         stmp = frmt(CorFHA2mm, 4)
         Call OPC_SCRIVI_DATO("/ACC/NCK/UGUD/CORFHA2", stmp)
      End If
   End If
   
   'FiancoDx = F1
   ActualCorrF1 = Lp("iERRFHAFDX")
   If ActualCorrF1 <> 0 Then
      CorrfHaF1 = Lp("COREVFHADX_G")
      Call SP("iCORPREFHAFDX", CorrfHaF1)
      tmp = CorrfHaF1 + ActualCorrF1
      CorFHA1mm = tmp / 1000
      Call SP("CORFHA1", CorFHA1mm)
      If MODALITA = "SPF" Then
         stmp = frmt(CorFHA1mm, 4)
         Call OPC_SCRIVI_DATO("/ACC/NCK/UGUD/CORFHA1", stmp)
      End If
      Call SP("COREVFHADX_G", tmp)
      ActualCorrF1 = 0
      Call SP("iERRFHAFDX", ActualCorrF1)
   Else
      CorrfHaF1 = Lp("COREVFHADX_G")
      CorFHA1mm = CorrfHaF1 / 1000
      Call SP("CORFHA1", CorFHA1mm)
      If MODALITA = "SPF" Then
         stmp = frmt(CorFHA1mm, 4)
         Call OPC_SCRIVI_DATO("/ACC/NCK/UGUD/CORFHA1", stmp)
      End If
   End If
End If


Exit Sub

errCalcoloParametri_MOLAVITE:
  If (Err.Number = 6) Or (Err.Number = 11) Then
    ParametriOK = False
    PAR.Pic.Cls
  'Else
  '  MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Sub: CalcoloParametri_MOLAVITE"
  End If
  MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Sub: CalcoloParametri_MOLAVITE"
  
  Exit Sub
  
End Sub

Sub CalcoloPasso_Molavite(ByVal NumZ As Integer, ByVal Modn As Double, ByVal Alfn As Double, ByVal Beta As Double, ByVal SAP As Double, ByVal EAP As Double, _
                          ByVal NumFil As Integer, ByVal ModnUt As Double, ByVal AlfnUt As Double, ByVal DiametroMola As Double, _
                          ByVal CorrFha1 As Double, ByVal CorrFha2 As Double, InclFiletto_Gradi As Double, PASSO As Double)

'Calcola InclFiletto e PASSO (inclinazione filetto e Passo della mola)

Dim Alft        As Double
Dim DP          As Double
Dim DB          As Double
Dim TifSAP      As Double
Dim TifEAP      As Double
Dim LungEvo     As Double
Dim ModnUtCor   As Double
Dim ERRFHA      As Double
Dim ERRFHA1     As Double
Dim ERRFHA2     As Double

Dim EntitaCorrezioneFC  As Double
Dim PassoBase   As Double
Dim AlfnUtCorr  As Double
Dim APP01       As Double
Dim InclFiletto As Double
Dim ARGOMENTO   As Double
Dim PassoTeo    As Double
Dim PG          As Double

Dim InclFilettoTeo         As Double
Dim InclFilettoTeo_Gradi   As Double

On Error GoTo errCalcoloPasso_MOLAVITE

PG = 4 * Atn(1)

  'Calcolo Diametro di Base
  Alft = Atn(Tan(Alfn) / Cos(Beta))
  DP = Modn * NumZ / Cos(Beta)     'Diametro Primitivo
  DB = DP * Cos(Alft)              'Diametro di Base


  'Calcolo Lung. evolvente
  If (SAP > 0) And (DB > 0) And (SAP > DB) Then
     TifSAP = Sqr((SAP / 2) ^ 2 - (DB / 2) ^ 2)
  End If
  If (EAP > 0) And (DB > 0) And (EAP > SAP) Then
     TifEAP = Sqr((EAP / 2) ^ 2 - (DB / 2) ^ 2)
  End If
  If (TifEAP > TifSAP) And (TifSAP > 0) Then
      LungEvo = Int((TifEAP - TifSAP) * 1000) / 1000
  Else
      LungEvo = 0
  End If

  'Calcolo modulo utensile corretto
  If (CorrFha1 = 0) And (CorrFha2 = 0) Then
      ModnUtCor = ModnUt
  Else
      ' Cambio segno ai correttori (Errori)
      ERRFHA1 = -CorrFha1
      ERRFHA2 = -CorrFha2

      If ERRFHA1 <> ERRFHA2 Then
          'Correzioni x fuori centro e passo
          EntitaCorrezioneFC = (ERRFHA1 - ERRFHA2) / 2
          
          'FCF1_G = -EntitaCorrezioneFC
          'FCF2_G = EntitaCorrezioneFC
          
          'Errore fHa da correggere con modifica del passo
          ERRFHA = (ERRFHA1 - EntitaCorrezioneFC)
      Else
          'Errore fHa da correggere con modifica del passo
          ERRFHA = ERRFHA1
      End If

      'Passo di base teorico ingranaggio
      'PBt = MyMn * Pg * Cos(MyAlfa * Pg / 180)
       PassoBase = Modn * PG * Cos(Alfn)

      'Angolo  ottenuto
      'AE = (AlfaUtensileMV * Pg / 180) + Atn((fHa) * 2 / La)
       APP01 = (-ERRFHA) * 2 / LungEvo
       AlfnUtCorr = AlfnUt + Atn(APP01)
 
       'Modulo normale corretto
       'MnUtensileCorretto = ((PBt) / (Pg * Cos(AE)))
       ModnUtCor = PassoBase / (PG * Cos(AlfnUtCorr))
  End If

  'Calcolo Inclinazione filetto e Passo
  If ModnUt <> ModnUtCor Then
     'Valori Teorici
     ARGOMENTO = ModnUt * NumFil / DiametroMola
     InclFilettoTeo = Atn(ARGOMENTO / Sqr(1 - ARGOMENTO * ARGOMENTO))  'RADIANTI
     InclFilettoTeo_Gradi = InclFilettoTeo / PG * 180                  'GRADI
     PassoTeo = ModnUt * NumFil * PG / Cos(InclFilettoTeo)
     
     'Valori Corretti
     ARGOMENTO = ModnUtCor * NumFil / DiametroMola
     InclFiletto = Atn(ARGOMENTO / Sqr(1 - ARGOMENTO * ARGOMENTO))  'RADIANTI
     InclFiletto_Gradi = InclFiletto / PG * 180                     'GRADI
     PASSO = ModnUtCor * NumFil * PG / Cos(InclFiletto)
  Else
     ARGOMENTO = ModnUt * NumFil / DiametroMola
     InclFiletto = Atn(ARGOMENTO / Sqr(1 - ARGOMENTO * ARGOMENTO))  'RADIANTI
     InclFiletto_Gradi = InclFiletto / PG * 180                     'GRADI
  
     PASSO = ModnUt * NumFil * PG / Cos(InclFiletto)
  End If
  

Exit Sub
errCalcoloPasso_MOLAVITE:
    MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Sub: CalcoloPasso_MOLAVITE"

End Sub

Sub CalcoloSINCRONISMO()
'
Dim MnUT_G        As Double
Dim NumPrinc_G    As Integer
Dim DiamMola_G    As Double
Dim INCLMOLA_G    As Double
Dim PASSOMOLA_G   As Double
Dim SENSOFIL_G    As Integer
Dim ApSensoFil_G  As Integer
Dim BetaUt        As Double
Dim Beta_G        As Double
Dim Alfa_G        As Double
Dim AlfaUt_G      As Double
Dim Helr          As Double
Dim InclTesta     As Double
Dim NumDenti_G    As Integer
Dim CorrBeta_G    As Double
Dim Mn_G          As Double
Dim nUDS          As Double
Dim dUDS          As Double
Dim nUDZ          As Double
Dim dUDZ          As Double
Dim nUDY          As Double
Dim dUDY          As Double
  '
  INCLMOLA_G = ASIN(MnUT_G * NumPrinc_G / DiamMola_G)
  PASSOMOLA_G = MnUT_G * NumPrinc_G * 3.141592654 / Cos(INCLMOLA_G)
  '
  'Senso filettatura (1=Sx 0=Dx)
  If (SENSOFIL_G = 0) Then
     ApSensoFil_G = -1 'Senso filetto dx
     BetaUt = -INCLMOLA_G
  Else
     ApSensoFil_G = 1 'Senso filetto sx
     BetaUt = INCLMOLA_G
  End If
  '
  Betab = ASIN(Sin(Beta_G) * Cos(Alfa_G)) 'elica di base
  Helr = ASIN(Sin(Betab) / Cos(AlfaUt_G)) 'elica di rotolamento
  InclTesta = -(Helr - BetaUt)            'calcolo inclinaz. testa
  '
  'calc.differenziale mandrino
  nUDS = -ApSensoFil_G * NumPrinc_G
  dUDS = NumDenti_G
  '
  'calc.differenziale assiale
  nUDZ = -(TRUNC(Sin(Beta_G + CorrBeta_G) * 360 * 10000)) / 10000
  dUDZ = (TRUNC(Mn_G * NumDenti_G * 3.141592654 * 10000)) / 10000
  '
  'calc.differenziale shifting
  nUDY = (TRUNC(Abs(Cos(BetaUt)) * 360 * 10000)) / 10000
  dUDY = (TRUNC(MnUT_G * NumDenti_G * 3.141592654 * 10000)) / 10000
  '
End Sub

Function TRUNC(ByVal Valore As Double) As Double
  
  TRUNC = Fix(Valore)

End Function

Sub CalcoloParametri_DENTATURA()
'
Dim tmp       As Double
Dim i         As Integer
'Dim FattoreX  As Double
'
On Error GoTo errCalcoloParametri_DENTATURA
  '
  ParametriOK = True
  Call InizializzaVariabili
  '
  'Azzeramento variabili calcolo
  CalcoloEsterni.DB = 0
  ReDim CalcoloEsterni.DCont(0)
  CalcoloEsterni.DE = 0
  CalcoloEsterni.DI = 0
  CalcoloEsterni.DIOtt = 0
  CalcoloEsterni.DP = 0
  ReDim CalcoloEsterni.IngrX(0)
  ReDim CalcoloEsterni.IngrY(0)
  ReDim CalcoloEsterni.LCont(0)
  ReDim CalcoloEsterni.MolaX(0)
  ReDim CalcoloEsterni.MolaY(0)
  CalcoloEsterni.Np = 0
  '
  'Se viene inserito un angolo elica negativo, viene diviso il modulo dal segno
  If (Lp("iAngoloElica") < 0) Then
    Call SP("iSensoElica", -1)
    Call SP("iAngoloElica", Abs(Lp("iAngoloElica")))
  End If
  Call SP("BETA_G", frmt(Lp("iAngoloElica") * Lp("iSensoElica"), 4))
  '
  'Lettura Parametri
  DGlob.Z = Lp("NUMDENTI_G")
  DGlob.Mn = Lp("MN_G")
  DGlob.ALFA = Lp("ALFA_G") * PG / 180
  DGlob.DI = Lp("DINT_G")
  DGlob.DE = Lp("DEXT_G")
  DGlob.SON = Lp("R[938]")
  DGlob.DefSpe = Lp("iDefSp")
  DGlob.WbQ = Lp("QW")
  DGlob.WbN = Lp("ZW")
  DGlob.RulQ = Lp("QR")
  DGlob.RulD = Lp("DR")
  DGlob.Beta = Lp("BETA_G") * PG / 180
  DGlob.RT = 1
  DGlob.RF = 1
  DGlob.DistPtEv = 0.5
  'Calcolo altri parametri
  DGlob.ALFAt = Atn(Tan(DGlob.ALFA) / Cos(DGlob.Beta))
  DGlob.DP = DGlob.Mn * DGlob.Z / Cos(DGlob.Beta)
  DGlob.DB = DGlob.DP * Cos(DGlob.ALFAt)
  'Eventuale calcolo del SONn
  Dim iB      As Double
  Dim Betab   As Double
  Dim PB      As Double
  Dim Sb      As Double
  Dim E1      As Double
  Dim H2      As Double
  Dim H3      As Double
  Dim F1      As Double
  Dim F2      As Double
  '
  Dim Wx      As Double
  Dim We      As Double
  Dim X       As Double
  Dim X1      As Double
  Dim Ymini   As Double
  Dim Ymaxi   As Double
  Dim Ymoyen  As Double
  '
  'Se lo spessore � nullo lo imposto a mezzo passo
  If (DGlob.SON = 0) And (DGlob.DefSpe = 1) Then
    DGlob.SON = Round((DGlob.Mn * PG) / 2, 3)
    Call SP("R[938]", DGlob.SON)
    'CALCOLO DEL FATTORE DI SPOSTAMENTO
    DGlob.FattoreX = (DGlob.SON - ((PG / 2) * DGlob.Mn)) / (2 * DGlob.Mn * Tan(DGlob.ALFA))
    DGlob.FattoreX = Int(DGlob.FattoreX * 100000 + 0.5) / 100000
    Call SP("FattoreX", DGlob.FattoreX)
  End If
  '
  Betab = Atn(Tan(Abs(DGlob.Beta)) * (Cos(DGlob.ALFAt)))
  PB = DGlob.DB * PG / DGlob.Z
  '
  Select Case DGlob.DefSpe
    '
    Case 1 'SON
      If (DGlob.SON > 0) Then
        'CALCOLO QUOTA WILDHABER
        If (DGlob.WbN > 0) Then
          DGlob.SONt = DGlob.SON / Cos(DGlob.Beta)
          Sb = (DGlob.SONt + DGlob.DP * inv(DGlob.ALFAt)) * DGlob.DB / DGlob.DP
          iB = PB - Sb
          DGlob.WbQ = DGlob.WbN * PB * Cos(Betab) - iB * Cos(Betab)
          'CALCOLO QUOTA RULLI
          If (DGlob.RulD > 0) Then
            Wx = Cos(PG / (2 * DGlob.Z)) * DGlob.DB
            If Int(DGlob.Z / 2) = DGlob.Z / 2 Then Wx = DGlob.DB
            We = Sb - PB
            X = (We + DGlob.RulD / Cos(Betab)) / DGlob.DB
            Ymini = 0: Ymaxi = PG / 2
            For i = 1 To 25
              Ymoyen = (Ymini + Ymaxi) / 2: X1 = Tan(Ymoyen) - Ymoyen
              If X1 < X Then Ymini = Ymoyen Else Ymaxi = Ymoyen
            Next i
            DGlob.RulQ = (Wx / Cos(Ymoyen)) + DGlob.RulD
          Else
            DGlob.RulQ = 0
          End If
        Else
          DGlob.WbQ = 0
        End If
        'CALCOLO DEL FATTORE DI SPOSTAMENTO
        DGlob.FattoreX = (DGlob.SON - ((PG / 2) * DGlob.Mn)) / (2 * DGlob.Mn * Tan(DGlob.ALFA))
        'AGGIORNAMENTO VALORI
        Call SP("QW", frmt(DGlob.WbQ, 8))
        Call SP("QR", frmt(DGlob.RulQ, 8))
        Call SP("FattoreX", frmt(DGlob.FattoreX, 8))
      End If
      '
    Case 2 'Quota Wb
      If (DGlob.WbN > 0) And (DGlob.WbQ > 0) Then
        iB = DGlob.WbN * PB - DGlob.WbQ / Cos(Betab)
        Sb = PB - iB
        DGlob.SONt = DGlob.DP * (Sb / DGlob.DB - (inv(DGlob.ALFAt)))
        DGlob.SON = DGlob.SONt * Cos(DGlob.Beta)
        'CALCOLO QUOTA RULLI
        If (DGlob.RulD > 0) Then
          Wx = Cos(PG / (2 * DGlob.Z)) * DGlob.DB
          If Int(DGlob.Z / 2) = DGlob.Z / 2 Then Wx = DGlob.DB
          We = Sb - PB
          X = (We + DGlob.RulD / Cos(Betab)) / DGlob.DB
          Ymini = 0: Ymaxi = PG / 2
          For i = 1 To 25
            Ymoyen = (Ymini + Ymaxi) / 2: X1 = Tan(Ymoyen) - Ymoyen
            If X1 < X Then Ymini = Ymoyen Else Ymaxi = Ymoyen
          Next i
          DGlob.RulQ = (Wx / Cos(Ymoyen)) + DGlob.RulD
        Else
          DGlob.RulQ = 0
        End If
        'CALCOLO DEL FATTORE DI SPOSTAMENTO
        DGlob.FattoreX = (DGlob.SON - ((PG / 2) * DGlob.Mn)) / (2 * DGlob.Mn * Tan(DGlob.ALFA))
        'AGGIORNAMENTO VALORI
        Call SP("R[938]", frmt(DGlob.SON, 8))
        Call SP("QR", frmt(DGlob.RulQ, 8))
        Call SP("FattoreX", frmt(DGlob.FattoreX, 8))
      End If
      '
    Case 3 'Quota Rulli
      If (DGlob.RulQ <> 0) And (DGlob.RulD <> 0) And (DGlob.RulD <> DGlob.RulQ) Then
        E1 = PG / 2 / DGlob.Z
        If (Int(DGlob.Z / 2) = DGlob.Z / 2) Then E1 = 0
        H2 = DGlob.Mn * DGlob.Z * Cos(DGlob.ALFAt) * Cos(E1) / ((DGlob.RulQ - DGlob.RulD) * Cos(Abs(DGlob.Beta)))
        H3 = Tan(Acs(H2))
        F2 = (inv(DGlob.ALFAt) + DGlob.RulD / (DGlob.Mn * DGlob.Z * Cos(DGlob.ALFA)) - H3 + Atn(H3)) * DGlob.Mn * DGlob.Z
        DGlob.SON = PG * DGlob.Mn - F2
        'CALCOLO DEL FATTORE DI SPOSTAMENTO
        DGlob.FattoreX = (DGlob.SON - ((PG / 2) * DGlob.Mn)) / (2 * DGlob.Mn * Tan(DGlob.ALFA))
      End If
      'CALCOLO QUOTA WILDHABER
      If (DGlob.WbN > 0) Then
        DGlob.SONt = DGlob.SON / Cos(DGlob.Beta)
        Sb = (DGlob.SONt + DGlob.DP * inv(DGlob.ALFAt)) * DGlob.DB / DGlob.DP
        iB = PB - Sb
        DGlob.WbQ = DGlob.WbN * PB * Cos(Betab) - iB * Cos(Betab)
      Else
        DGlob.WbQ = 0
      End If
      'AGGIORNAMENTO VALORI
      Call SP("R[938]", frmt(DGlob.SON, 8))
      Call SP("QW", frmt(DGlob.WbQ, 8))
      Call SP("FattoreX", frmt(DGlob.FattoreX, 8))
      '
    Case 4 'Fattore X
      DGlob.FattoreX = Lp("FattoreX")
      'CALCOLO DELLO SPESSORE NORMALE
      DGlob.SON = 2 * DGlob.Mn * DGlob.FattoreX * Tan(DGlob.ALFA) + ((PG / 2) * DGlob.Mn)
      'CALCOLO QUOTA WILDHABER
      If (DGlob.WbN > 0) Then
        DGlob.SONt = DGlob.SON / Cos(DGlob.Beta)
        Sb = (DGlob.SONt + DGlob.DP * inv(DGlob.ALFAt)) * DGlob.DB / DGlob.DP
        iB = PB - Sb
        DGlob.WbQ = DGlob.WbN * PB * Cos(Betab) - iB * Cos(Betab)
        'CALCOLO QUOTA RULLI
        If (DGlob.RulD > 0) Then
          Wx = Cos(PG / (2 * DGlob.Z)) * DGlob.DB
          If Int(DGlob.Z / 2) = DGlob.Z / 2 Then Wx = DGlob.DB
          We = Sb - PB
          X = (We + DGlob.RulD / Cos(Betab)) / DGlob.DB
          Ymini = 0: Ymaxi = PG / 2
          For i = 1 To 25
            Ymoyen = (Ymini + Ymaxi) / 2: X1 = Tan(Ymoyen) - Ymoyen
            If X1 < X Then Ymini = Ymoyen Else Ymaxi = Ymoyen
          Next i
          DGlob.RulQ = (Wx / Cos(Ymoyen)) + DGlob.RulD
        Else
          DGlob.RulQ = 0
        End If
      Else
        DGlob.WbQ = 0
      End If
      'AGGIORNAMENTO VALORI
      Call SP("R[938]", frmt(DGlob.SON, 8))
      Call SP("QW", frmt(DGlob.WbQ, 8))
      Call SP("QR", frmt(DGlob.RulQ, 8))
      '
  End Select
  '
  Dim LatoPart As Integer
  Dim nZone    As Integer
  '
  LatoPart = Lp("R[12]")
  nZone = Lp("iNZone")
  '
  'CALCOLO CORSE DI ENTRATA ED USCITA
  Dim ARGOMENTO              As Double
  Dim INCLINAZIONE_MOLA_GRD  As Double
  Dim INCLINAZIONE_MOLA_RAD  As Double
  Dim MODULO_MOLA            As Double
  Dim PASSO_MOLA             As Double
  Dim PRINCIPI_MOLA          As Double
  Dim DIAMETRO_MOLA          As Double
  Dim SENSO_FILETTO          As Integer
  Dim TIPO_INCLINAZIONE      As Integer
  '
  MODULO_MOLA = Lp("MNUT_G")
  PASSO_MOLA = Lp("PassoMola_G")
  PRINCIPI_MOLA = Lp("NUMPRINC_G")
  DIAMETRO_MOLA = Lp("DIAMMOLA_G")
  INCLINAZIONE_MOLA_GRD = Lp("INCLMOLA_G")
  SENSO_FILETTO = Lp("SENSOFIL_G")
  TIPO_INCLINAZIONE = Lp("iDefIN")
  '
  Select Case TIPO_INCLINAZIONE
    Case 1
      'CALCOLO DIAMETRO PRIMITIVO E PASSO
      If (INCLINAZIONE_MOLA_GRD <> 0) Then
        INCLINAZIONE_MOLA_RAD = INCLINAZIONE_MOLA_GRD / 180 * PG                 'RADIANTI
        DIAMETRO_MOLA = MODULO_MOLA * PRINCIPI_MOLA / Sin(INCLINAZIONE_MOLA_RAD)
        PASSO_MOLA = MODULO_MOLA * PRINCIPI_MOLA * PG / Cos(INCLINAZIONE_MOLA_RAD)
        Call SP("DIAMMOLA_G", frmt(DIAMETRO_MOLA, 4))
        Call SP("PassoMola_G", frmt(PASSO_MOLA, 4))
      End If
    Case 2
      'CALCOLO INCLINAZIONE FILETTO E PASSO
      If (DIAMETRO_MOLA <> 0) Then
        ARGOMENTO = MODULO_MOLA * PRINCIPI_MOLA / DIAMETRO_MOLA
        INCLINAZIONE_MOLA_RAD = Atn(ARGOMENTO / Sqr(1 - ARGOMENTO * ARGOMENTO))  'RADIANTI
        INCLINAZIONE_MOLA_GRD = INCLINAZIONE_MOLA_RAD / PG * 180                 'GRADI
        PASSO_MOLA = MODULO_MOLA * PRINCIPI_MOLA * PG / Cos(INCLINAZIONE_MOLA_RAD)
        Call SP("INCLMOLA_G", frmt(INCLINAZIONE_MOLA_GRD, 4))
        Call SP("PassoMola_G", frmt(PASSO_MOLA, 4))
      End If
    Case 3
      'CALCOLO INCLINAZIONE FILETTO E DIAMETRO
      If (PASSO_MOLA <> 0) Then
        ARGOMENTO = MODULO_MOLA * PRINCIPI_MOLA * PG / PASSO_MOLA
        INCLINAZIONE_MOLA_RAD = Atn(Sqr(1 - ARGOMENTO * ARGOMENTO) / ARGOMENTO)  'RADIANTI
        INCLINAZIONE_MOLA_GRD = INCLINAZIONE_MOLA_RAD / PG * 180                 'GRADI
        Call SP("INCLMOLA_G", frmt(INCLINAZIONE_MOLA_GRD, 4))
        Call SP("DIAMMOLA_G", frmt(DIAMETRO_MOLA, 4))
      End If
    Case Else
      WRITE_DIALOG "TIPO INCLINAZIONE NOT DEFINED!!!!"
      Exit Sub
  End Select
  '
  Dim InclTesta              As Double
  Dim PROFONDITA             As Double
  Dim AlfaMV                 As Double
  Dim Corsa_X1               As Double
  Dim Corsa_X2               As Double
  Dim DIAMOLAMAX             As Double
  Dim Hkw                    As Double
  '
  'CALCOLO INCLINAZIONE TESTA I RADIANTI
  If (SENSO_FILETTO = 0) Then
    InclTesta = DGlob.Beta - INCLINAZIONE_MOLA_RAD
  Else
    InclTesta = DGlob.Beta + INCLINAZIONE_MOLA_RAD
  End If
  '
  DIAMOLAMAX = Lp("DIAMOLAMAX_G")
  Hkw = (DIAMOLAMAX - DIAMETRO_MOLA) / 2
  PROFONDITA = (Lp("DEXT_G") - Lp("DINT_G")) / 2
  AlfaMV = Lp("ALFAUT_G")
  '
  Corsa_X1 = Tan(Abs(InclTesta)) * Sqr(PROFONDITA * (DIAMOLAMAX / Sin(InclTesta) / Sin(InclTesta) + Lp("DEXT_G") - PROFONDITA))
  Corsa_X1 = (Int(Corsa_X1 * 10)) / 10
  '
  Corsa_X2 = Hkw * Sin(Abs(InclTesta)) / Tan(AlfaMV / 180 * PG)
  Corsa_X2 = (Int(Corsa_X2 * 10)) / 10
  If (Corsa_X2 <= 0) Then Corsa_X2 = 2
  '
  Call SP("iCORSAIN", Round(Corsa_X1 + 0.05, 1))
  Call SP("iCORSAOUT", Round(Corsa_X2 + 0.05, 1))
  Call SP("CORSAIN", Lp("iCORSAIN") + Lp("iCORSAIN_CORR"))
  Call SP("CORSAOUT", Lp("iCORSAOUT") + Lp("iCORSAOUT_CORR"))
  '
  'CALCOLO SHIFTTING UTENSILE
  Dim L1    As Double
  Dim L2    As Double
  Dim L3    As Double
  Dim G2    As Double
  Dim LSV   As Double
  Dim ac    As Double
  Dim Dpri  As Double
  ac = Abs(InclTesta)
  L1 = (((DGlob.DE - ((DGlob.DE - DGlob.DI) / 2)) * (DGlob.DE - DGlob.DI) / 2) ^ 0.5) / Abs(Cos(ac))
  LSV = MODULO_MOLA * (PG / 2 + Tan(AlfaMV * PG / 180) / Cos(DGlob.Beta))
  Dpri = DGlob.Mn * DGlob.Z / Cos(DGlob.Beta)
  Hkw = (Dpri - DGlob.DI) / 2
  G2 = Hkw / Tan(AlfaMV * PG / 180)
  L2 = G2 + LSV * 1.4
  Call SP("iSHIFTIN_G", Round(L1, 1))
  Call SP("iSHIFTOUT_G", Round(L2, 1))
  Call SP("SHIFTIN_G", Lp("iSHIFTIN_G") + Lp("iSHIFTIN_G_CORR"))
  Call SP("SHIFTOUT_G", Lp("iSHIFTOUT_G") + Lp("iSHIFTOUT_G_CORR"))
  '
  Dim FasciaAMV As Double
  Dim FasciaBMV As Double
  Dim FasciaCMV As Double
  '
  Select Case nZone
    Case 1
      Call SP("iFas2F1", 0): Call SP("iFas2F2", 0)
      Call SP("iFas3F1", 0): Call SP("iFas3F2", 0)
      Call SP("iCon2F1", 0): Call SP("iCon2F2", 0)
      Call SP("iBom2F1", 0): Call SP("iBom2F2", 0)
      Call SP("iCon3F1", 0): Call SP("iCon3F2", 0)
      Call SP("iBom3F1", 0): Call SP("iBom3F2", 0)
      FasciaAMV = Lp("CORSAOUT") + Lp("iFas1F1") + Lp("CORSAOUT")
      FasciaBMV = 0
      FasciaCMV = 0
    Case 2
      Call SP("iFas3F1", 0): Call SP("iFas3F2", 0)
      Call SP("iCon3F1", 0): Call SP("iCon3F2", 0)
      Call SP("iBom3F1", 0): Call SP("iBom3F2", 0)
      FasciaAMV = Lp("CORSAOUT") + Lp("iFas1F1")
      FasciaBMV = Lp("iFas2F1") + Lp("CORSAOUT")
      FasciaCMV = 0
    Case 3
      FasciaAMV = Lp("CORSAOUT") + Lp("iFas1F1")
      FasciaBMV = Lp("iFas2F1")
      FasciaCMV = Lp("iFas3F1") + Lp("CORSAOUT")
  End Select
  Call SP("FASCIATOTALE_G", Lp("iFas1F1") + Lp("iFas2F1") + Lp("iFas3F1"))
  '
  If (Lp("iMetDiv") = 1) Then
    Call SP("PenetrRAD_G", 1)
  Else
    Call SP("PenetrRAD_G", 0)
  End If
  '
Dim AppBombXA As Double
Dim AppBombCA As Double
Dim AppConXA  As Double
Dim AppConCA  As Double
Dim AppBombXB As Double
Dim AppBombCB As Double
Dim AppConXB  As Double
Dim AppConCB  As Double
Dim AppBombXC As Double
Dim AppBombCC As Double
Dim AppConXC  As Double
Dim AppConCC  As Double
'
Dim BetaMV    As Double
Dim BombAMV   As Double
Dim BombAF2MV As Double
Dim ConAMV    As Double
Dim ConAF2MV  As Double
'
Dim BombBMV   As Double
Dim BombBF2MV As Double
Dim ConBMV    As Double
Dim ConBF2MV  As Double
'
Dim BombCMV   As Double
Dim BombCF2MV As Double
Dim ConCMV    As Double
Dim ConCF2MV  As Double
    '
    BetaMV = Lp("BETA_G")
    'TRATTO A
    BombAMV = Lp("iBom1F1"): BombAF2MV = Lp("iBom1F2")
    ConAMV = Lp("iCon1F1"):  ConAF2MV = Lp("iCon1F2")
    'TRATTO B
    BombBMV = Lp("iBom2F1"): BombBF2MV = Lp("iBom2F2")
    ConBMV = Lp("iCon2F1"):  ConBF2MV = Lp("iCon2F2")
    'TRATTO C
    BombCMV = Lp("iBom3F1"): BombCF2MV = Lp("iBom3F2")
    ConCMV = Lp("iCon3F1"):  ConCF2MV = Lp("iCon3F2")
    '
    Call CalcolaBombConXC(AppBombXA, AppBombCA, AppConXA, AppConCA, BombAMV, BombAF2MV, ConAMV, ConAF2MV, BetaMV)
    Call CalcolaBombConXC(AppBombXB, AppBombCB, AppConXB, AppConCB, BombBMV, BombBF2MV, ConBMV, ConBF2MV, BetaMV)
    Call CalcolaBombConXC(AppBombXC, AppBombCC, AppConXC, AppConCC, BombCMV, BombCF2MV, ConCMV, ConCF2MV, BetaMV)
    '
Dim BombFondoA!
Dim BombFondoB!
Dim BombFondoC!
Dim RaggioBOMB As Double
  '
  BombFondoA! = AppBombXA / Tan(AlfaMV * PG / 180): AppConXA = Int(AppConXA * 1000 + 0.5) / 1000
  BombFondoB! = AppBombXB / Tan(AlfaMV * PG / 180): AppConXB = Int(AppConXB * 1000 + 0.5) / 1000
  BombFondoC! = AppBombXC / Tan(AlfaMV * PG / 180): AppConXC = Int(AppConXC * 1000 + 0.5) / 1000
  '
  If (LatoPart = -1) Then
      'TRATTO A
      If (BombFondoA! <> 0) Then
        If (BombFondoA! > 0) Then
          RaggioBOMB = (FasciaAMV * FasciaAMV / (8 * Abs(BombFondoA!))) + Abs(BombFondoA!) / 2 + DIAMETRO_MOLA / 2
        Else
          RaggioBOMB = (FasciaAMV * FasciaAMV / (8 * Abs(BombFondoA!))) + Abs(BombFondoA!) / 2 - DIAMETRO_MOLA / 2
        End If
        RaggioBOMB = Int(RaggioBOMB * 1000 + 0.5) / 1000
        If (BombFondoA! < 0) Then RaggioBOMB = -RaggioBOMB
      Else
        RaggioBOMB = 0
      End If
      Call SP("CORSAASS1_G", FasciaAMV):     Call SP("CORSAASS1F2_G", FasciaAMV)
      Call SP("RAGGIOBOMB1_G", RaggioBOMB):  Call SP("RAGGIOBOMB1F2_G", RaggioBOMB)
      Call SP("CORSARAD1_G", AppConXA):      Call SP("CORSARAD1F2_G", AppConXA)
      'TRATTO B
      If (BombFondoB! <> 0) Then
        If (BombFondoB! > 0) Then
          RaggioBOMB = (FasciaBMV * FasciaBMV / (8 * Abs(BombFondoB!))) + Abs(BombFondoB!) / 2 + DIAMETRO_MOLA / 2
        Else
          RaggioBOMB = (FasciaBMV * FasciaBMV / (8 * Abs(BombFondoB!))) + Abs(BombFondoB!) / 2 - DIAMETRO_MOLA / 2
        End If
        RaggioBOMB = Int(RaggioBOMB * 1000 + 0.5) / 1000
        If (BombFondoB! < 0) Then RaggioBOMB = -RaggioBOMB
      Else
        RaggioBOMB = 0
      End If
      Call SP("CORSAASS2_G", FasciaBMV):     Call SP("CORSAASS2F2_G", FasciaBMV)
      Call SP("RAGGIOBOMB2_G", RaggioBOMB):  Call SP("RAGGIOBOMB2F2_G", RaggioBOMB)
      Call SP("CORSARAD2_G", AppConXB):      Call SP("CORSARAD2F2_G", AppConXB)
      'TRATTO C
      If (BombFondoC! <> 0) Then
        If (BombFondoC! > 0) Then
          RaggioBOMB = (FasciaCMV * FasciaCMV / (8 * Abs(BombFondoC!))) + Abs(BombFondoC!) / 2 + DIAMETRO_MOLA / 2
        Else
          RaggioBOMB = (FasciaCMV * FasciaCMV / (8 * Abs(BombFondoC!))) + Abs(BombFondoC!) / 2 - DIAMETRO_MOLA / 2
        End If
        RaggioBOMB = Int(RaggioBOMB * 1000 + 0.5) / 1000
        If (BombFondoC! < 0) Then RaggioBOMB = -RaggioBOMB
      Else
        RaggioBOMB = 0
      End If
      Call SP("CORSAASS3_G", FasciaCMV):     Call SP("CORSAASS3F2_G", FasciaCMV)
      Call SP("RAGGIOBOMB3_G", RaggioBOMB):  Call SP("RAGGIOBOMB3F2_G", RaggioBOMB)
      Call SP("CORSARAD3_G", AppConXC):      Call SP("CORSARAD3F2_G", AppConXC)
  Else
    Select Case nZone
    Case 3
      'TRATTO A -> C
      If (BombFondoA! <> 0) Then
        If (BombFondoA! > 0) Then
          RaggioBOMB = (FasciaAMV * FasciaAMV / (8 * Abs(BombFondoA!))) + Abs(BombFondoA!) / 2 + DIAMETRO_MOLA / 2
        Else
          RaggioBOMB = (FasciaAMV * FasciaAMV / (8 * Abs(BombFondoA!))) + Abs(BombFondoA!) / 2 - DIAMETRO_MOLA / 2
        End If
        RaggioBOMB = Int(RaggioBOMB * 1000 + 0.5) / 1000
        If (BombFondoA! < 0) Then RaggioBOMB = -RaggioBOMB
      Else
        RaggioBOMB = 0
      End If
      Call SP("CORSAASS3_G", FasciaAMV):     Call SP("CORSAASS3F2_G", FasciaAMV)
      Call SP("RAGGIOBOMB3_G", -RaggioBOMB): Call SP("RAGGIOBOMB3F2_G", -RaggioBOMB)
      Call SP("CORSARAD3_G", -AppConXA):     Call SP("CORSARAD3F2_G", -AppConXA)
      'TRATTO B
      If (BombFondoB! <> 0) Then
        If (BombFondoB! > 0) Then
          RaggioBOMB = (FasciaBMV * FasciaBMV / (8 * Abs(BombFondoB!))) + Abs(BombFondoB!) / 2 + DIAMETRO_MOLA / 2
        Else
          RaggioBOMB = (FasciaBMV * FasciaBMV / (8 * Abs(BombFondoB!))) + Abs(BombFondoB!) / 2 - DIAMETRO_MOLA / 2
        End If
        RaggioBOMB = Int(RaggioBOMB * 1000 + 0.5) / 1000
        If (BombFondoB! < 0) Then RaggioBOMB = -RaggioBOMB
      Else
        RaggioBOMB = 0
      End If
      Call SP("CORSAASS2_G", FasciaBMV):     Call SP("CORSAASS2F2_G", FasciaBMV)
      Call SP("RAGGIOBOMB2_G", -RaggioBOMB): Call SP("RAGGIOBOMB2F2_G", -RaggioBOMB)
      Call SP("CORSARAD2_G", -AppConXB):     Call SP("CORSARAD2F2_G", -AppConXB)
      'TRATTO C -> A
      If (BombFondoC! <> 0) Then
        If (BombFondoC! > 0) Then
          RaggioBOMB = (FasciaCMV * FasciaCMV / (8 * Abs(BombFondoC!))) + Abs(BombFondoC!) / 2 + DIAMETRO_MOLA / 2
        Else
          RaggioBOMB = (FasciaCMV * FasciaCMV / (8 * Abs(BombFondoC!))) + Abs(BombFondoC!) / 2 - DIAMETRO_MOLA / 2
        End If
        RaggioBOMB = Int(RaggioBOMB * 1000 + 0.5) / 1000
        If (BombFondoC! < 0) Then RaggioBOMB = -RaggioBOMB
      Else
        RaggioBOMB = 0
      End If
      Call SP("CORSAASS1_G", FasciaCMV):     Call SP("CORSAASS1F2_G", FasciaCMV)
      Call SP("RAGGIOBOMB1_G", -RaggioBOMB): Call SP("RAGGIOBOMB1F2_G", -RaggioBOMB)
      Call SP("CORSARAD1_G", -AppConXC):     Call SP("CORSARAD1F2_G", -AppConXC)
    Case 2
      'TRATTO A -> B
      If (BombFondoA! <> 0) Then
        If (BombFondoA! > 0) Then
          RaggioBOMB = (FasciaAMV * FasciaAMV / (8 * Abs(BombFondoA!))) + Abs(BombFondoA!) / 2 + DIAMETRO_MOLA / 2
        Else
          RaggioBOMB = (FasciaAMV * FasciaAMV / (8 * Abs(BombFondoA!))) + Abs(BombFondoA!) / 2 - DIAMETRO_MOLA / 2
        End If
        RaggioBOMB = Int(RaggioBOMB * 1000 + 0.5) / 1000
        If (BombFondoA! < 0) Then RaggioBOMB = -RaggioBOMB
      Else
        RaggioBOMB = 0
      End If
      Call SP("CORSAASS2_G", FasciaAMV):     Call SP("CORSAASS2F2_G", FasciaAMV)
      Call SP("RAGGIOBOMB2_G", -RaggioBOMB): Call SP("RAGGIOBOMB2F2_G", -RaggioBOMB)
      Call SP("CORSARAD2_G", -AppConXA):     Call SP("CORSARAD2F2_G", -AppConXA)
      'TRATTO B -> A
      If (BombFondoB! <> 0) Then
        If (BombFondoB! > 0) Then
          RaggioBOMB = (FasciaBMV * FasciaBMV / (8 * Abs(BombFondoB!))) + Abs(BombFondoB!) / 2 + DIAMETRO_MOLA / 2
        Else
          RaggioBOMB = (FasciaBMV * FasciaBMV / (8 * Abs(BombFondoB!))) + Abs(BombFondoB!) / 2 - DIAMETRO_MOLA / 2
        End If
        RaggioBOMB = Int(RaggioBOMB * 1000 + 0.5) / 1000
        If (BombFondoB! < 0) Then RaggioBOMB = -RaggioBOMB
      Else
        RaggioBOMB = 0
      End If
      Call SP("CORSAASS1_G", FasciaBMV):     Call SP("CORSAASS1F2_G", FasciaBMV)
      Call SP("RAGGIOBOMB1_G", -RaggioBOMB): Call SP("RAGGIOBOMB1F2_G", -RaggioBOMB)
      Call SP("CORSARAD1_G", -AppConXB):     Call SP("CORSARAD1F2_G", -AppConXB)
      'TRATTO C
      Call SP("CORSAASS3_G", 0):             Call SP("CORSAASS3F2_G", 0)
      Call SP("RAGGIOBOMB3_G", 0):           Call SP("RAGGIOBOMB3F2_G", 0)
      Call SP("CORSARAD3_G", 0):             Call SP("CORSARAD3F2_G", 0)
    Case 1
      'TRATTO A
      If (BombFondoA! <> 0) Then
        If (BombFondoA! > 0) Then
          RaggioBOMB = (FasciaAMV * FasciaAMV / (8 * Abs(BombFondoA!))) + Abs(BombFondoA!) / 2 + DIAMETRO_MOLA / 2
        Else
          RaggioBOMB = (FasciaAMV * FasciaAMV / (8 * Abs(BombFondoA!))) + Abs(BombFondoA!) / 2 - DIAMETRO_MOLA / 2
        End If
        RaggioBOMB = Int(RaggioBOMB * 1000 + 0.5) / 1000
        If (BombFondoA! < 0) Then RaggioBOMB = -RaggioBOMB
      Else
        RaggioBOMB = 0
      End If
      Call SP("CORSAASS1_G", FasciaAMV):     Call SP("CORSAASS1F2_G", FasciaAMV)
      Call SP("RAGGIOBOMB1_G", -RaggioBOMB): Call SP("RAGGIOBOMB1F2_G", -RaggioBOMB)
      Call SP("CORSARAD1_G", -AppConXA):     Call SP("CORSARAD1F2_G", -AppConXA)
      'TRATTO B
      Call SP("CORSAASS2_G", 0):             Call SP("CORSAASS2F2_G", 0)
      Call SP("RAGGIOBOMB2_G", 0):           Call SP("RAGGIOBOMB2F2_G", 0)
      Call SP("CORSARAD2_G", 0):             Call SP("CORSARAD2F2_G", 0)
      'TRATTO C
      Call SP("CORSAASS3_G", 0):             Call SP("CORSAASS3F2_G", 0)
      Call SP("RAGGIOBOMB3_G", 0):           Call SP("RAGGIOBOMB3F2_G", 0)
      Call SP("CORSARAD3_G", 0):             Call SP("CORSARAD3F2_G", 0)
    End Select
  End If
  '
  'IL PROGRAMMA TABVZ DEVE ESSERE RIFATTO
  Call SP("RAPP[0,311]", frmt(AppBombCA, 8))
  Call SP("RAPP[0,312]", frmt(AppConCA, 8))
  Call SP("RAPP[0,314]", frmt(AppBombCB, 8))
  Call SP("RAPP[0,315]", frmt(AppConCB, 8))
  Call SP("RAPP[0,317]", frmt(AppBombCC, 8))
  Call SP("RAPP[0,318]", frmt(AppConCC, 8))
  '
  RaggioBOMB = Lp("CORSARAD1_G") + Lp("CORSARAD2_G") + Lp("CORSARAD3_G")
  Call SP("MEZZACORSARAD_G", Int(RaggioBOMB * 1000 / 2 + 0.5) / 1000)
  '
  Call CALCOLO_INGRANAGGIO_ESTERNO_MOLAVITE
  '
On Error GoTo 0
Exit Sub

errCalcoloParametri_DENTATURA:
  If (Err.Number = 6) Or (Err.Number = 11) Then
    ParametriOK = False
    PAR.Pic.Cls
  Else
    MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Sub: CalcoloParametri_DENTATURA"
  End If
  Exit Sub
  
End Sub

Sub CalcoloParametri_INGRANAGGI_ESTERNI()
'
Dim tmp       As Double
Dim i         As Integer
'Dim FattoreX  As Double
'
On Error GoTo errCalcoloParametri_INGRANAGGI_ESTERNI
  '
  ParametriOK = True
  Call InizializzaVariabili
  '
  'Azzeramento variabili calcolo
  CalcoloEsterni.DB = 0
  ReDim CalcoloEsterni.DCont(0)
  CalcoloEsterni.DE = 0
  CalcoloEsterni.DI = 0
  CalcoloEsterni.DIOtt = 0
  CalcoloEsterni.DP = 0
  ReDim CalcoloEsterni.IngrX(0)
  ReDim CalcoloEsterni.IngrY(0)
  ReDim CalcoloEsterni.LCont(0)
  ReDim CalcoloEsterni.MolaX(0)
  ReDim CalcoloEsterni.MolaY(0)
  CalcoloEsterni.Np = 0
  '
  'Se viene inserito un angolo elica negativo, viene diviso il modulo dal segno
  If (Lp("iAngoloElica") < 0) Then
    Call SP("iSensoElica", -1)
    Call SP("iAngoloElica", Abs(Lp("iAngoloElica")))
  End If
  Call SP("R[41]", Lp("iAngoloElica") * Lp("iSensoElica"))
  '
  'Lettura Parametri
  DGlob.Z = Lp("R[5]")
  DGlob.Mn = Lp("R[42]")
  DGlob.ALFA = Lp("R[40]") * PG / 180
  DGlob.DI = Lp("R[45]")
  DGlob.DE = Lp("DEXT_G")
  DGlob.SON = Lp("R[46]")
  DGlob.DefSpe = Lp("iDefSp")
  DGlob.WbQ = Lp("R[47]")
  DGlob.WbN = Lp("R[48]")
  DGlob.RulQ = Lp("R[49]")
  DGlob.RulD = Lp("R[50]")
  DGlob.Beta = Lp("R[41]") * PG / 180
  DGlob.RT = Lp("R[51]")
  DGlob.RF = Lp("R[52]")
  DGlob.DistPtEv = Lp("R[194]")
  
  'Calcolo altri parametri
  DGlob.ALFAt = Atn(Tan(DGlob.ALFA) / Cos(DGlob.Beta))
  DGlob.DP = DGlob.Mn * DGlob.Z / Cos(DGlob.Beta)
  DGlob.DB = DGlob.DP * Cos(DGlob.ALFAt)
  '
  Dim iB    As Double
  Dim Betab As Double
  Dim PB    As Double
  Dim Sb    As Double
  Dim E1    As Double
  Dim H2    As Double
  Dim H3    As Double
  Dim F1    As Double
  Dim F2    As Double
  '
  Dim Wx      As Double
  Dim We      As Double
  Dim X       As Double
  Dim X1      As Double
  Dim Ymini   As Double
  Dim Ymaxi   As Double
  Dim Ymoyen  As Double
  '
  'Se lo spessore � nullo lo imposto a mezzo passo
  If (DGlob.SON = 0) And (DGlob.DefSpe = 1) Then
    DGlob.SON = Round((DGlob.Mn * PG) / 2, 3)
    Call SP("R[46]", DGlob.SON)
    'CALCOLO DEL FATTORE DI SPOSTAMENTO
    DGlob.FattoreX = (DGlob.SON - ((PG / 2) * DGlob.Mn)) / (2 * DGlob.Mn * Tan(DGlob.ALFA))
    DGlob.FattoreX = Int(DGlob.FattoreX * 100000 + 0.5) / 100000
    'Call SP("FattoreX", DGlob.FattoreX)
  End If
  '
  Betab = Atn(Tan(Abs(DGlob.Beta)) * (Cos(DGlob.ALFAt)))
  PB = DGlob.DB * PG / DGlob.Z
  '
  Select Case DGlob.DefSpe
    '
    Case 1 'SON
      If (DGlob.SON > 0) Then
        'CALCOLO QUOTA WILDHABER
        If (DGlob.WbN > 0) Then
          DGlob.SONt = DGlob.SON / Cos(DGlob.Beta)
          Sb = (DGlob.SONt + DGlob.DP * inv(DGlob.ALFAt)) * DGlob.DB / DGlob.DP
          iB = PB - Sb
          DGlob.WbQ = DGlob.WbN * PB * Cos(Betab) - iB * Cos(Betab)
          'CALCOLO QUOTA RULLI
          If (DGlob.RulD > 0) Then
            Wx = Cos(PG / (2 * DGlob.Z)) * DGlob.DB
            If Int(DGlob.Z / 2) = DGlob.Z / 2 Then Wx = DGlob.DB
            We = Sb - PB
            X = (We + DGlob.RulD / Cos(Betab)) / DGlob.DB
            Ymini = 0: Ymaxi = PG / 2
            For i = 1 To 25
              Ymoyen = (Ymini + Ymaxi) / 2: X1 = Tan(Ymoyen) - Ymoyen
              If X1 < X Then Ymini = Ymoyen Else Ymaxi = Ymoyen
            Next i
            DGlob.RulQ = (Wx / Cos(Ymoyen)) + DGlob.RulD
          Else
            DGlob.RulQ = 0
          End If
        Else
          DGlob.WbQ = 0
        End If
        'CALCOLO DEL FATTORE DI SPOSTAMENTO
        DGlob.FattoreX = (DGlob.SON - ((PG / 2) * DGlob.Mn)) / (2 * DGlob.Mn * Tan(DGlob.ALFA))
        'AGGIORNAMENTO VALORI
        Call SP("R[47]", frmt(DGlob.WbQ, 8))
        Call SP("R[49]", frmt(DGlob.RulQ, 8))
        'Call SP("FattoreX", frmt(DGlob.FattoreX, 8))
      End If
      '
    Case 2 'Quota Wb
      If (DGlob.WbN > 0) And (DGlob.WbQ > 0) Then
        iB = DGlob.WbN * PB - DGlob.WbQ / Cos(Betab)
        Sb = PB - iB
        DGlob.SONt = DGlob.DP * (Sb / DGlob.DB - (inv(DGlob.ALFAt)))
        DGlob.SON = DGlob.SONt * Cos(DGlob.Beta)
        'CALCOLO QUOTA RULLI
        If (DGlob.RulD > 0) Then
          Wx = Cos(PG / (2 * DGlob.Z)) * DGlob.DB
          If Int(DGlob.Z / 2) = DGlob.Z / 2 Then Wx = DGlob.DB
          We = Sb - PB
          X = (We + DGlob.RulD / Cos(Betab)) / DGlob.DB
          Ymini = 0: Ymaxi = PG / 2
          For i = 1 To 25
            Ymoyen = (Ymini + Ymaxi) / 2: X1 = Tan(Ymoyen) - Ymoyen
            If X1 < X Then Ymini = Ymoyen Else Ymaxi = Ymoyen
          Next i
          DGlob.RulQ = (Wx / Cos(Ymoyen)) + DGlob.RulD
        Else
          DGlob.RulQ = 0
        End If
        'CALCOLO DEL FATTORE DI SPOSTAMENTO
        DGlob.FattoreX = (DGlob.SON - ((PG / 2) * DGlob.Mn)) / (2 * DGlob.Mn * Tan(DGlob.ALFA))
        'AGGIORNAMENTO VALORI
        Call SP("R[46]", frmt(DGlob.SON, 8))
        Call SP("R[49]", frmt(DGlob.RulQ, 8))
        'Call SP("FattoreX", frmt(DGlob.FattoreX, 8))
      End If
      '
    Case 3 'Quota Rulli
      If (DGlob.RulQ <> 0) And (DGlob.RulD <> 0) And (DGlob.RulD <> DGlob.RulQ) Then
        E1 = PG / 2 / DGlob.Z
        If (Int(DGlob.Z / 2) = DGlob.Z / 2) Then E1 = 0
        H2 = DGlob.Mn * DGlob.Z * Cos(DGlob.ALFAt) * Cos(E1) / ((DGlob.RulQ - DGlob.RulD) * Cos(Abs(DGlob.Beta)))
        H3 = Tan(Acs(H2))
        F2 = (inv(DGlob.ALFAt) + DGlob.RulD / (DGlob.Mn * DGlob.Z * Cos(DGlob.ALFA)) - H3 + Atn(H3)) * DGlob.Mn * DGlob.Z
        DGlob.SON = PG * DGlob.Mn - F2
        'CALCOLO DEL FATTORE DI SPOSTAMENTO
        DGlob.FattoreX = (DGlob.SON - ((PG / 2) * DGlob.Mn)) / (2 * DGlob.Mn * Tan(DGlob.ALFA))
      End If
      'CALCOLO QUOTA WILDHABER
      If (DGlob.WbN > 0) Then
        DGlob.SONt = DGlob.SON / Cos(DGlob.Beta)
        Sb = (DGlob.SONt + DGlob.DP * inv(DGlob.ALFAt)) * DGlob.DB / DGlob.DP
        iB = PB - Sb
        DGlob.WbQ = DGlob.WbN * PB * Cos(Betab) - iB * Cos(Betab)
      Else
        DGlob.WbQ = 0
      End If
      'AGGIORNAMENTO VALORI
      Call SP("R[46]", frmt(DGlob.SON, 8))
      Call SP("R[47]", frmt(DGlob.WbQ, 8))
      'Call SP("FattoreX", frmt(DGlob.FattoreX, 8))
      '
    Case 4 'Fattore X
      DGlob.FattoreX = Lp("FattoreX")
      'CALCOLO DELLO SPESSORE NORMALE
      DGlob.SON = 2 * DGlob.Mn * DGlob.FattoreX * Tan(DGlob.ALFA) + ((PG / 2) * DGlob.Mn)
      'CALCOLO QUOTA WILDHABER
      If (DGlob.WbN > 0) Then
        DGlob.SONt = DGlob.SON / Cos(DGlob.Beta)
        Sb = (DGlob.SONt + DGlob.DP * inv(DGlob.ALFAt)) * DGlob.DB / DGlob.DP
        iB = PB - Sb
        DGlob.WbQ = DGlob.WbN * PB * Cos(Betab) - iB * Cos(Betab)
        'CALCOLO QUOTA RULLI
        If (DGlob.RulD > 0) Then
          Wx = Cos(PG / (2 * DGlob.Z)) * DGlob.DB
          If Int(DGlob.Z / 2) = DGlob.Z / 2 Then Wx = DGlob.DB
          We = Sb - PB
          X = (We + DGlob.RulD / Cos(Betab)) / DGlob.DB
          Ymini = 0: Ymaxi = PG / 2
          For i = 1 To 25
            Ymoyen = (Ymini + Ymaxi) / 2: X1 = Tan(Ymoyen) - Ymoyen
            If X1 < X Then Ymini = Ymoyen Else Ymaxi = Ymoyen
          Next i
          DGlob.RulQ = (Wx / Cos(Ymoyen)) + DGlob.RulD
        Else
          DGlob.RulQ = 0
        End If
      Else
        DGlob.WbQ = 0
      End If
      'AGGIORNAMENTO VALORI
      Call SP("R[46]", frmt(DGlob.SON, 8))
      Call SP("R[47]", frmt(DGlob.WbQ, 8))
      Call SP("R[49]", frmt(DGlob.RulQ, 8))
      '
  End Select
  '
  'Calcolo altri parametri
  DGlob.SONt = DGlob.SON / Cos(DGlob.Beta)
  DGlob.Eb = DGlob.DB * ((DGlob.SONt / DGlob.DP) + inv(DGlob.ALFAt))
  '
  'Se l'inclinazione della mola in rettifica � 0
  'viene impostata uguale all'angolo elica.
  If (Lp("R[258]") = 0) Then Call SP("R[258]", Abs(Lp("iAngoloElica")))
  '
  'Se il numero pezzi in rettifica � 1
  'azzero la distanza tra i pezzi
  If (Lp("R[9]") <= 1) Then Call SP("R[133]", 0)
  '
  'Lettura Zone
  ReDim DGlob.ZonaF1(0)
  ReDim DGlob.ZonaF2(0)
  '
  DGlob.ZonaF1(0) = DIA2TIF(Lp("R[43]"), DGlob.DB)    'SAP F1
  DGlob.ZonaF2(0) = DIA2TIF(Lp("SAPF2_G"), DGlob.DB)  'SAP F2
  '
  DGlob.ValTIFDIA = Lp("R[901]")
  '
  'Se SAP � sotto il DB metto il diametro interno RaggioUtensile (Se Beta=0) + DistanzaPunti
  '(solo come valore di default)
  If (DGlob.ZonaF1(0) = 0) Then
    If (DGlob.Beta = 0) Then
      tmp = LEP("$TC_DP6[1,1]")
      tmp = tmp + DGlob.DistPtEv
    Else
      tmp = DGlob.DistPtEv
    End If
    DGlob.ZonaF1(0) = tmp
    Call SP("R[43]", frmt0(TIF2DIA(DGlob.ZonaF1(0), DGlob.DB) + 0.001, 3))
  End If
  If (DGlob.ZonaF2(0) = 0) Then
    If (DGlob.Beta = 0) Then
      tmp = LEP("$TC_DP6[1,2]")
      tmp = tmp + DGlob.DistPtEv
    Else
      tmp = DGlob.DistPtEv
    End If
    DGlob.ZonaF2(0) = tmp
    Call SP("SAPF2_G", frmt0(TIF2DIA(DGlob.ZonaF2(0), DGlob.DB) + 0.001, 3))
  End If
  '
  For i = 0 To 2
    tmp = Lp("R[" & 270 + i * 3 & "]") 'Fine Zona i+1  ( R270 R273 R276 )
    If (tmp <> 0) Then
      'Eventuale conversione Dia -> Tif
      If (DGlob.ValTIFDIA = 2) Then
        tmp = DIA2TIF(tmp, DGlob.DB)
      End If
      ReDim Preserve DGlob.ZonaF1(UBound(DGlob.ZonaF1) + 1)
      If (tmp > DIA2TIF(DGlob.DE, DGlob.DB)) Then
        DGlob.ZonaF1(UBound(DGlob.ZonaF1)) = DIA2TIF(DGlob.DE, DGlob.DB)
        Exit For
      Else
        DGlob.ZonaF1(UBound(DGlob.ZonaF1)) = tmp
      End If
    Else
      If (UBound(DGlob.ZonaF1) = 0) Then
        ReDim Preserve DGlob.ZonaF1(UBound(DGlob.ZonaF1) + 1)
        DGlob.ZonaF1(UBound(DGlob.ZonaF1)) = DIA2TIF(DGlob.DE, DGlob.DB)
      End If
      Exit For
    End If
  Next i
  '
  For i = 0 To 2
    tmp = Lp("R[" & 285 + i * 3 & "]") 'Fine Zona i+1  ( R285 R288 R291 )
    If (tmp <> 0) Then
      'Eventuale conversione Dia -> Tif
      If (DGlob.ValTIFDIA = 2) Then
        tmp = DIA2TIF(tmp, DGlob.DB)
      End If
      ReDim Preserve DGlob.ZonaF2(UBound(DGlob.ZonaF2) + 1)
      If (tmp > DIA2TIF(DGlob.DE, DGlob.DB)) Then
        DGlob.ZonaF2(UBound(DGlob.ZonaF2)) = DIA2TIF(DGlob.DE, DGlob.DB)
        Exit For
      Else
        DGlob.ZonaF2(UBound(DGlob.ZonaF2)) = tmp
      End If
    Else
      If (UBound(DGlob.ZonaF2) = 0) Then
        ReDim Preserve DGlob.ZonaF2(UBound(DGlob.ZonaF2) + 1)
        DGlob.ZonaF2(UBound(DGlob.ZonaF2)) = DIA2TIF(DGlob.DE, DGlob.DB)
      End If
      Exit For
    End If
  Next i
  '
  If (DGlob.DistPtEv > 0) Then
    DGlob.NumPtEvF1 = Int((DGlob.ZonaF1(UBound(DGlob.ZonaF1)) - DGlob.ZonaF1(0)) / DGlob.DistPtEv) + 2
    DGlob.NumPtEvF2 = Int((DGlob.ZonaF2(UBound(DGlob.ZonaF2)) - DGlob.ZonaF2(0)) / DGlob.DistPtEv) + 2
  End If
  '
  Call CALCOLO_INGRANAGGIO_ESTERNO_380(0, True)
  '
  tmp = 0
  '
On Error Resume Next
  '
  'Se non � possibile completare il calcolo perch� mancano dei parametri
  'Inizializzo l'extracorsa a 0 (tmp=0) e ignoro gli errori.
  tmp = Abs(CalcoloEsterni.LCont(1, 1))
  If (tmp < Abs(CalcoloEsterni.LCont(1, CalcoloEsterni.Np))) Then
    tmp = Abs(CalcoloEsterni.LCont(1, CalcoloEsterni.Np))
  End If
  '
On Error GoTo errCalcoloParametri_INGRANAGGI_ESTERNI
  '
  Call SP("iCORSAIN", Round(tmp + 0.05, 1))
  Call SP("iCORSAOUT", Round(tmp + 0.05, 1))
  '
  Call SP("CORSAIN", Lp("iCORSAIN") + Lp("iCORSAIN_CORR"))
  Call SP("CORSAOUT", Lp("iCORSAOUT") + Lp("iCORSAOUT_CORR"))
  '
  Dim Div_T   As Double
  Dim Div_Rm  As Double
  Dim Div_hz  As Double
  Dim Div_Rt  As Double
  '
  If (Lp("iMetDiv") = 1) Then
    Call SP("R[130]", 0)
  Else
    If (Lp("R[130]") = 0) Then
      Div_Rm = Lp("iDiaMola") / 2
      Div_hz = (Lp("DEXT_G") - Lp("R[45]")) / 2
      Div_Rt = Lp("R[51]")
      Div_T = Sqr(Div_Rm ^ 2 - (Div_Rm - Div_hz - Div_Rt) ^ 2)
      Call SP("R[130]", Round(Div_T - tmp + 0.05, 1))
    End If
  End If
  '
  Select Case Lp("iNZone")
      '
    Case 1
      Call SP("Fascia2F1", 0)
      Call SP("Fascia2F2", 0)
      Call SP("Fascia3F1", 0)
      Call SP("Fascia3F2", 0)
      '
      Call SP("Conic2F1", 0)
      Call SP("Conic2F2", 0)
      Call SP("Bomb2F1", 0)
      Call SP("Bomb2F2", 0)
      '
      Call SP("Conic3F1", 0)
      Call SP("Conic3F2", 0)
      Call SP("Bomb3F1", 0)
      Call SP("Bomb3F2", 0)
      '
    Case 2
      Call SP("Fascia3F1", 0)
      Call SP("Fascia3F2", 0)
      '
      Call SP("Conic3F1", 0)
      Call SP("Conic3F2", 0)
      Call SP("Bomb3F1", 0)
      Call SP("Bomb3F2", 0)
      '
    Case 3
      '
  End Select
  Call SP("FASCIATOTALE_G", Lp("Fascia1F1") + Lp("Fascia2F1") + Lp("Fascia3F1"))
  '
Exit Sub

errCalcoloParametri_INGRANAGGI_ESTERNI:
  If (Err.Number = 6) Or (Err.Number = 11) Then
    ParametriOK = False
    PAR.Pic.Cls
  Else
    MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Sub: CalcoloParametri_INGRANAGGI_ESTERNI"
  End If

End Sub

Sub CalcoloParametri_INGRANAGGI_ESTERNIV()
'
Dim tmp       As Double
Dim i         As Integer
'
On Error GoTo errCalcoloParametri_INGRANAGGI_ESTERNIV
  '
  ParametriOK = True
  Call InizializzaVariabili
  '
  'Azzeramento variabili calcolo
  CalcoloEsterni.DB = 0
  ReDim CalcoloEsterni.DCont(0)
  CalcoloEsterni.DE = 0
  CalcoloEsterni.DI = 0
  CalcoloEsterni.DIOtt = 0
  CalcoloEsterni.DP = 0
  ReDim CalcoloEsterni.IngrX(0)
  ReDim CalcoloEsterni.IngrY(0)
  ReDim CalcoloEsterni.LCont(0)
  ReDim CalcoloEsterni.MolaX(0)
  ReDim CalcoloEsterni.MolaY(0)
  CalcoloEsterni.Np = 0
  '
  'Se viene inserito un angolo elica negativo, viene diviso il modulo dal segno
  If (Lp("iAngoloElica") < 0) Then
    Call SP("iSensoElica", -1)
    Call SP("iAngoloElica", Abs(Lp("iAngoloElica")))
  End If
  Call SP("R[41]", Lp("iAngoloElica") * Lp("iSensoElica"))
  
  'Lettura Parametri
  DGlob.Z = Lp("R[5]")
  DGlob.Mn = Lp("R[42]")
  DGlob.ALFA = Lp("R[40]") * PG / 180
  DGlob.DI = Lp("R[45]")
  DGlob.DE = Lp("DEXT_G")
  DGlob.SON = Lp("R[46]")
  DGlob.DefSpe = Lp("iDefSp")
  DGlob.WbQ = Lp("R[47]")
  DGlob.WbN = Lp("R[48]")
  DGlob.RulQ = Lp("R[49]")
  DGlob.RulD = Lp("R[50]")
  DGlob.Beta = Lp("R[41]") * PG / 180
  DGlob.RT = Lp("R[51]")
  DGlob.RF = Lp("R[52]")
  DGlob.DistPtEv = Lp("R[194]")
  
  'Calcolo altri parametri
  DGlob.ALFAt = Atn(Tan(DGlob.ALFA) / Cos(DGlob.Beta))
  DGlob.DP = DGlob.Mn * DGlob.Z / Cos(DGlob.Beta)
  DGlob.DB = DGlob.DP * Cos(DGlob.ALFAt)
  '
  Dim iB    As Double
  Dim Betab As Double
  Dim PB    As Double
  Dim Sb    As Double
  Dim E1    As Double
  Dim H2    As Double
  Dim H3    As Double
  Dim F1    As Double
  Dim F2    As Double
  '
  Dim Wx      As Double
  Dim We      As Double
  Dim X       As Double
  Dim X1      As Double
  Dim Ymini   As Double
  Dim Ymaxi   As Double
  Dim Ymoyen  As Double
  '
  'Se lo spessore � nullo lo imposto a mezzo passo
  If (DGlob.SON = 0) And (DGlob.DefSpe = 1) Then
    DGlob.SON = Round((DGlob.Mn * PG) / 2, 3)
    Call SP("R[46]", DGlob.SON)
    'CALCOLO DEL FATTORE DI SPOSTAMENTO
    DGlob.FattoreX = (DGlob.SON - ((PG / 2) * DGlob.Mn)) / (2 * DGlob.Mn * Tan(DGlob.ALFA))
    DGlob.FattoreX = Int(DGlob.FattoreX * 100000 + 0.5) / 100000
    Call SP("FattoreX", DGlob.FattoreX)
  End If
  '
  Betab = Atn(Tan(Abs(DGlob.Beta)) * (Cos(DGlob.ALFAt)))
  PB = DGlob.DB * PG / DGlob.Z
  '
  Select Case DGlob.DefSpe
    '
    Case 1 'SON
      If (DGlob.SON > 0) Then
        'CALCOLO QUOTA WILDHABER
        If (DGlob.WbN > 0) Then
          DGlob.SONt = DGlob.SON / Cos(DGlob.Beta)
          Sb = (DGlob.SONt + DGlob.DP * inv(DGlob.ALFAt)) * DGlob.DB / DGlob.DP
          iB = PB - Sb
          DGlob.WbQ = DGlob.WbN * PB * Cos(Betab) - iB * Cos(Betab)
          'CALCOLO QUOTA RULLI
          If (DGlob.RulD > 0) Then
            Wx = Cos(PG / (2 * DGlob.Z)) * DGlob.DB
            If Int(DGlob.Z / 2) = DGlob.Z / 2 Then Wx = DGlob.DB
            We = Sb - PB
            X = (We + DGlob.RulD / Cos(Betab)) / DGlob.DB
            Ymini = 0: Ymaxi = PG / 2
            For i = 1 To 25
              Ymoyen = (Ymini + Ymaxi) / 2: X1 = Tan(Ymoyen) - Ymoyen
              If X1 < X Then Ymini = Ymoyen Else Ymaxi = Ymoyen
            Next i
            DGlob.RulQ = (Wx / Cos(Ymoyen)) + DGlob.RulD
          Else
            DGlob.RulQ = 0
          End If
        Else
          DGlob.WbQ = 0
        End If
        'CALCOLO DEL FATTORE DI SPOSTAMENTO
        DGlob.FattoreX = (DGlob.SON - ((PG / 2) * DGlob.Mn)) / (2 * DGlob.Mn * Tan(DGlob.ALFA))
        'AGGIORNAMENTO VALORI
        Call SP("R[47]", frmt(DGlob.WbQ, 8))
        Call SP("R[49]", frmt(DGlob.RulQ, 8))
        Call SP("FattoreX", frmt(DGlob.FattoreX, 8))
      End If
      '
    Case 2 'Quota Wb
      If (DGlob.WbN > 0) And (DGlob.WbQ > 0) Then
        iB = DGlob.WbN * PB - DGlob.WbQ / Cos(Betab)
        Sb = PB - iB
        DGlob.SONt = DGlob.DP * (Sb / DGlob.DB - (inv(DGlob.ALFAt)))
        DGlob.SON = DGlob.SONt * Cos(DGlob.Beta)
        'CALCOLO QUOTA RULLI
        If (DGlob.RulD > 0) Then
          Wx = Cos(PG / (2 * DGlob.Z)) * DGlob.DB
          If Int(DGlob.Z / 2) = DGlob.Z / 2 Then Wx = DGlob.DB
          We = Sb - PB
          X = (We + DGlob.RulD / Cos(Betab)) / DGlob.DB
          Ymini = 0: Ymaxi = PG / 2
          For i = 1 To 25
            Ymoyen = (Ymini + Ymaxi) / 2: X1 = Tan(Ymoyen) - Ymoyen
            If X1 < X Then Ymini = Ymoyen Else Ymaxi = Ymoyen
          Next i
          DGlob.RulQ = (Wx / Cos(Ymoyen)) + DGlob.RulD
        Else
          DGlob.RulQ = 0
        End If
        'CALCOLO DEL FATTORE DI SPOSTAMENTO
        DGlob.FattoreX = (DGlob.SON - ((PG / 2) * DGlob.Mn)) / (2 * DGlob.Mn * Tan(DGlob.ALFA))
        'AGGIORNAMENTO VALORI
        Call SP("R[46]", frmt(DGlob.SON, 8))
        Call SP("R[49]", frmt(DGlob.RulQ, 8))
        Call SP("FattoreX", frmt(DGlob.FattoreX, 8))
      End If
      '
    Case 3 'Quota Rulli
      If (DGlob.RulQ <> 0) And (DGlob.RulD <> 0) And (DGlob.RulD <> DGlob.RulQ) Then
        E1 = PG / 2 / DGlob.Z
        If (Int(DGlob.Z / 2) = DGlob.Z / 2) Then E1 = 0
        H2 = DGlob.Mn * DGlob.Z * Cos(DGlob.ALFAt) * Cos(E1) / ((DGlob.RulQ - DGlob.RulD) * Cos(Abs(DGlob.Beta)))
        H3 = Tan(Acs(H2))
        F2 = (inv(DGlob.ALFAt) + DGlob.RulD / (DGlob.Mn * DGlob.Z * Cos(DGlob.ALFA)) - H3 + Atn(H3)) * DGlob.Mn * DGlob.Z
        DGlob.SON = PG * DGlob.Mn - F2
        'CALCOLO DEL FATTORE DI SPOSTAMENTO
        DGlob.FattoreX = (DGlob.SON - ((PG / 2) * DGlob.Mn)) / (2 * DGlob.Mn * Tan(DGlob.ALFA))
      End If
      'CALCOLO QUOTA WILDHABER
      If (DGlob.WbN > 0) Then
        DGlob.SONt = DGlob.SON / Cos(DGlob.Beta)
        Sb = (DGlob.SONt + DGlob.DP * inv(DGlob.ALFAt)) * DGlob.DB / DGlob.DP
        iB = PB - Sb
        DGlob.WbQ = DGlob.WbN * PB * Cos(Betab) - iB * Cos(Betab)
      Else
        DGlob.WbQ = 0
      End If
      'AGGIORNAMENTO VALORI
      Call SP("R[46]", frmt(DGlob.SON, 8))
      Call SP("R[47]", frmt(DGlob.WbQ, 8))
      Call SP("FattoreX", frmt(DGlob.FattoreX, 8))
      '
    Case 4 'Fattore X
      DGlob.FattoreX = Lp("FattoreX")
      'CALCOLO DELLO SPESSORE NORMALE
      DGlob.SON = 2 * DGlob.Mn * DGlob.FattoreX * Tan(DGlob.ALFA) + ((PG / 2) * DGlob.Mn)
      'CALCOLO QUOTA WILDHABER
      If (DGlob.WbN > 0) Then
        DGlob.SONt = DGlob.SON / Cos(DGlob.Beta)
        Sb = (DGlob.SONt + DGlob.DP * inv(DGlob.ALFAt)) * DGlob.DB / DGlob.DP
        iB = PB - Sb
        DGlob.WbQ = DGlob.WbN * PB * Cos(Betab) - iB * Cos(Betab)
        'CALCOLO QUOTA RULLI
        If (DGlob.RulD > 0) Then
          Wx = Cos(PG / (2 * DGlob.Z)) * DGlob.DB
          If Int(DGlob.Z / 2) = DGlob.Z / 2 Then Wx = DGlob.DB
          We = Sb - PB
          X = (We + DGlob.RulD / Cos(Betab)) / DGlob.DB
          Ymini = 0: Ymaxi = PG / 2
          For i = 1 To 25
            Ymoyen = (Ymini + Ymaxi) / 2: X1 = Tan(Ymoyen) - Ymoyen
            If X1 < X Then Ymini = Ymoyen Else Ymaxi = Ymoyen
          Next i
          DGlob.RulQ = (Wx / Cos(Ymoyen)) + DGlob.RulD
        Else
          DGlob.RulQ = 0
        End If
      Else
        DGlob.WbQ = 0
      End If
      'AGGIORNAMENTO VALORI
      Call SP("R[46]", frmt(DGlob.SON, 8))
      Call SP("R[47]", frmt(DGlob.WbQ, 8))
      Call SP("R[49]", frmt(DGlob.RulQ, 8))
      '
  End Select
  '
  'Spessore Riferimento per Controllo
  Dim DefSpeChk As Double
  Dim SONCHK    As Double
  Dim WbQCHK    As Double
  Dim WbNCHK    As Double
  Dim RulQCHK   As Double
  Dim RulDCHK   As Double
  Dim SONtCHK   As Double
  Dim SbCHK     As Double
  Dim ibCHK     As Double
  Dim WxCHK     As Double
  Dim WeCHK     As Double
  Dim XCHK      As Double
  Dim E1CHK     As Double
  Dim H2CHK     As Double
  Dim H3CHK     As Double
  Dim F2CHK     As Double
  '
  If DGlob.DefSpe = 4 Then
     'Se TipoSpessore = "Fattore X" carico "SON"
     DefSpeChk = 1
  Else
     DefSpeChk = DGlob.DefSpe
  End If
  '
  Call SP("iDefSpCHK", frmt(DefSpeChk, 2))
  '
  SONCHK = Lp("SONCHK")
  WbQCHK = Lp("QWCHK")
  WbNCHK = Lp("ZWCHK")
  RulQCHK = Lp("QRCHK")
  RulDCHK = Lp("DRCHK")
  '
  Select Case DefSpeChk
    '
    Case 1 'SON
      If (SONCHK > 0) Then
        'CALCOLO QUOTA WILDHABER
        If (WbNCHK > 0) Then
          SONtCHK = SONCHK / Cos(DGlob.Beta)
          SbCHK = (SONtCHK + DGlob.DP * inv(DGlob.ALFAt)) * DGlob.DB / DGlob.DP
          ibCHK = PB - SbCHK
          WbQCHK = WbNCHK * PB * Cos(Betab) - ibCHK * Cos(Betab)
          'CALCOLO QUOTA RULLI
          If (RulDCHK > 0) Then
            WxCHK = Cos(PG / (2 * DGlob.Z)) * DGlob.DB
            If Int(DGlob.Z / 2) = DGlob.Z / 2 Then WxCHK = DGlob.DB
            WeCHK = SbCHK - PB
            XCHK = (WeCHK + RulDCHK / Cos(Betab)) / DGlob.DB
            '
            Ymini = 0: Ymaxi = PG / 2
            For i = 1 To 25
              Ymoyen = (Ymini + Ymaxi) / 2: X1 = Tan(Ymoyen) - Ymoyen
              If X1 < XCHK Then Ymini = Ymoyen Else Ymaxi = Ymoyen
            Next i
            RulQCHK = (WxCHK / Cos(Ymoyen)) + RulDCHK
          Else
            RulQCHK = 0
          End If
        Else
          WbQCHK = 0
        End If
        'AGGIORNAMENTO VALORI
        Call SP("QWCHK", frmt(WbQCHK, 4))
        Call SP("QRCHK", frmt(RulQCHK, 4))
      End If
      '
    Case 2 'Quota Wb
      If (WbNCHK > 0) And (WbQCHK > 0) Then
        ibCHK = WbNCHK * PB - WbQCHK / Cos(Betab)
        SbCHK = PB - ibCHK
        SONtCHK = DGlob.DP * (SbCHK / DGlob.DB - (inv(DGlob.ALFAt)))
        SONCHK = SONtCHK * Cos(DGlob.Beta)
        'CALCOLO QUOTA RULLI
        If (RulDCHK > 0) Then
          WxCHK = Cos(PG / (2 * DGlob.Z)) * DGlob.DB
          If Int(DGlob.Z / 2) = DGlob.Z / 2 Then WxCHK = DGlob.DB
          WeCHK = SbCHK - PB
          XCHK = (WeCHK + RulDCHK / Cos(Betab)) / DGlob.DB
          Ymini = 0: Ymaxi = PG / 2
          For i = 1 To 25
            Ymoyen = (Ymini + Ymaxi) / 2: X1 = Tan(Ymoyen) - Ymoyen
            If X1 < XCHK Then Ymini = Ymoyen Else Ymaxi = Ymoyen
          Next i
          RulQCHK = (WxCHK / Cos(Ymoyen)) + RulDCHK
        Else
          RulQCHK = 0
        End If
        'AGGIORNAMENTO VALORI
        Call SP("SONCHK", frmt(SONCHK, 4))
        Call SP("QRCHK", frmt(RulQCHK, 4))
      End If
      '
    Case 3 'Quota Rulli
      If (RulQCHK <> 0) And (RulDCHK <> 0) And (RulDCHK <> RulQCHK) Then
        E1CHK = PG / 2 / DGlob.Z
        If (Int(DGlob.Z / 2) = DGlob.Z / 2) Then E1CHK = 0
        H2CHK = DGlob.Mn * DGlob.Z * Cos(DGlob.ALFAt) * Cos(E1CHK) / ((RulQCHK - RulDCHK) * Cos(Abs(DGlob.Beta)))
        H3CHK = Tan(Acs(H2CHK))
        F2CHK = (inv(DGlob.ALFAt) + RulDCHK / (DGlob.Mn * DGlob.Z * Cos(DGlob.ALFA)) - H3CHK + Atn(H3CHK)) * DGlob.Mn * DGlob.Z
        SONCHK = PG * DGlob.Mn - F2CHK
      End If
      'CALCOLO QUOTA WILDHABER
      If (WbNCHK > 0) Then
        SONtCHK = SONCHK / Cos(DGlob.Beta)
        SbCHK = (SONtCHK + DGlob.DP * inv(DGlob.ALFAt)) * DGlob.DB / DGlob.DP
        ibCHK = PB - SbCHK
        WbQCHK = WbNCHK * PB * Cos(Betab) - ibCHK * Cos(Betab)
      Else
        WbQCHK = 0
      End If
      'AGGIORNAMENTO VALORI
      Call SP("SONCHK", frmt(SONCHK, 4))
      Call SP("QWCHK", frmt(WbQCHK, 4))
      '
  End Select
  '
  'Calcolo altri parametri
  DGlob.SONt = DGlob.SON / Cos(DGlob.Beta)
  DGlob.Eb = DGlob.DB * ((DGlob.SONt / DGlob.DP) + inv(DGlob.ALFAt))
  '
  'Se l'inclinazione della mola in rettifica � 0
  'viene impostata uguale all'angolo elica.
  If (Lp("R[258]") = 0) Then Call SP("R[258]", Abs(Lp("iAngoloElica")))
  '
  'Se il numero pezzi in rettifica � 1
  'azzero la distanza tra i pezzi
  If (Lp("R[9]") <= 1) Then Call SP("R[133]", 0)
  '
  'Lettura Zone
  ReDim DGlob.ZonaF1(0)
  ReDim DGlob.ZonaF2(0)
  '
  DGlob.ZonaF1(0) = DIA2TIF(Lp("R[43]"), DGlob.DB)    'SAP F1
  DGlob.ZonaF2(0) = DIA2TIF(Lp("SAPF2_G"), DGlob.DB)  'SAP F2
  '
  DGlob.ValTIFDIA = Lp("R[901]")
  '
  'Se SAP � sotto il DB metto il diametro interno RaggioUtensile (Se Beta=0) + DistanzaPunti
  '(solo come valore di default)
  If (DGlob.ZonaF1(0) = 0) Then
    If (DGlob.Beta = 0) Then
      tmp = LEP("$TC_DP6[1,1]")
      tmp = tmp + DGlob.DistPtEv
    Else
      tmp = DGlob.DistPtEv
    End If
    DGlob.ZonaF1(0) = tmp
    Call SP("R[43]", frmt0(TIF2DIA(DGlob.ZonaF1(0), DGlob.DB) + 0.001, 3))
  End If
  If (DGlob.ZonaF2(0) = 0) Then
    If (DGlob.Beta = 0) Then
      tmp = LEP("$TC_DP6[1,2]")
      tmp = tmp + DGlob.DistPtEv
    Else
      tmp = DGlob.DistPtEv
    End If
    DGlob.ZonaF2(0) = tmp
    Call SP("SAPF2_G", frmt0(TIF2DIA(DGlob.ZonaF2(0), DGlob.DB) + 0.001, 3))
  End If
  '
  For i = 0 To 2
    tmp = Lp("R[" & 270 + i * 3 & "]") 'Fine Zona i+1  ( R270 R273 R276 )
    If (tmp <> 0) Then
      'Eventuale conversione Dia -> Tif
      If (DGlob.ValTIFDIA = 2) Then
        tmp = DIA2TIF(tmp, DGlob.DB)
      End If
      ReDim Preserve DGlob.ZonaF1(UBound(DGlob.ZonaF1) + 1)
      If (tmp > DIA2TIF(DGlob.DE, DGlob.DB)) Then
        DGlob.ZonaF1(UBound(DGlob.ZonaF1)) = DIA2TIF(DGlob.DE, DGlob.DB)
        Exit For
      Else
        DGlob.ZonaF1(UBound(DGlob.ZonaF1)) = tmp
      End If
    Else
      If (UBound(DGlob.ZonaF1) = 0) Then
        ReDim Preserve DGlob.ZonaF1(UBound(DGlob.ZonaF1) + 1)
        DGlob.ZonaF1(UBound(DGlob.ZonaF1)) = DIA2TIF(DGlob.DE, DGlob.DB)
      End If
      Exit For
    End If
  Next i
  '
  For i = 0 To 2
    tmp = Lp("R[" & 285 + i * 3 & "]") 'Fine Zona i+1  ( R285 R288 R291 )
    If (tmp <> 0) Then
      'Eventuale conversione Dia -> Tif
      If (DGlob.ValTIFDIA = 2) Then
        tmp = DIA2TIF(tmp, DGlob.DB)
      End If
      ReDim Preserve DGlob.ZonaF2(UBound(DGlob.ZonaF2) + 1)
      If (tmp > DIA2TIF(DGlob.DE, DGlob.DB)) Then
        DGlob.ZonaF2(UBound(DGlob.ZonaF2)) = DIA2TIF(DGlob.DE, DGlob.DB)
        Exit For
      Else
        DGlob.ZonaF2(UBound(DGlob.ZonaF2)) = tmp
      End If
    Else
      If (UBound(DGlob.ZonaF2) = 0) Then
        ReDim Preserve DGlob.ZonaF2(UBound(DGlob.ZonaF2) + 1)
        DGlob.ZonaF2(UBound(DGlob.ZonaF2)) = DIA2TIF(DGlob.DE, DGlob.DB)
      End If
      Exit For
    End If
  Next i
  '
  If (DGlob.DistPtEv > 0) Then
    DGlob.NumPtEvF1 = Int((DGlob.ZonaF1(UBound(DGlob.ZonaF1)) - DGlob.ZonaF1(0)) / DGlob.DistPtEv) + 2
    DGlob.NumPtEvF2 = Int((DGlob.ZonaF2(UBound(DGlob.ZonaF2)) - DGlob.ZonaF2(0)) / DGlob.DistPtEv) + 2
  End If
  '
  Call CALCOLO_INGRANAGGIO_ESTERNO_380(0, True)
  '
  tmp = 0
  '
On Error Resume Next
  '
  'Se non � possibile completare il calcolo perch� mancano dei parametri
  'Inizializzo l'extracorsa a 0 (tmp=0) e ignoro gli errori.
  tmp = Abs(CalcoloEsterni.LCont(1, 1))
  If (tmp < Abs(CalcoloEsterni.LCont(1, CalcoloEsterni.Np))) Then
    tmp = Abs(CalcoloEsterni.LCont(1, CalcoloEsterni.Np))
  End If
  '
On Error GoTo errCalcoloParametri_INGRANAGGI_ESTERNIV
  '
  Call SP("iCORSAIN", Round(tmp + 0.05, 1))
  Call SP("iCORSAOUT", Round(tmp + 0.05, 1))
  '
  Call SP("CORSAIN", Lp("iCORSAIN") + Lp("iCORSAIN_CORR"))
  Call SP("CORSAOUT", Lp("iCORSAOUT") + Lp("iCORSAOUT_CORR"))
  '
  Dim Div_T   As Double
  Dim Div_Rm  As Double
  Dim Div_hz  As Double
  Dim Div_Rt  As Double
  '
  If (Lp("iMetDiv") = 1) Then
    Call SP("R[130]", 0)
  Else
    If (Lp("R[130]") = 0) Then
      Div_Rm = Lp("iDiaMola") / 2
      Div_hz = (Lp("DEXT_G") - Lp("R[45]")) / 2
      Div_Rt = Lp("R[51]")
      Div_T = Sqr(Div_Rm ^ 2 - (Div_Rm - Div_hz - Div_Rt) ^ 2)
      Call SP("R[130]", Round(Div_T - tmp + 0.05, 1))
    End If
  End If
  '
  Select Case Lp("iNZone")
      '
    Case 1
      Call SP("Fascia2F1", 0)
      Call SP("Fascia2F2", 0)
      Call SP("Fascia3F1", 0)
      Call SP("Fascia3F2", 0)
      '
      Call SP("Conic2F1", 0)
      Call SP("Conic2F2", 0)
      Call SP("Bomb2F1", 0)
      Call SP("Bomb2F2", 0)
      '
      Call SP("Conic3F1", 0)
      Call SP("Conic3F2", 0)
      Call SP("Bomb3F1", 0)
      Call SP("Bomb3F2", 0)
      '
    Case 2
      Call SP("Fascia3F1", 0)
      Call SP("Fascia3F2", 0)
      '
      Call SP("Conic3F1", 0)
      Call SP("Conic3F2", 0)
      Call SP("Bomb3F1", 0)
      Call SP("Bomb3F2", 0)
      '
    Case 3
      '
  End Select
  Call SP("FASCIATOTALE_G", Lp("Fascia1F1") + Lp("Fascia2F1") + Lp("Fascia3F1"))
  '
Exit Sub

errCalcoloParametri_INGRANAGGI_ESTERNIV:
  If (Err.Number = 6) Or (Err.Number = 11) Then
    ParametriOK = False
    PAR.Pic.Cls
  Else
    MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Sub: CalcoloParametri_INGRANAGGI_ESTERNIV"
  End If

End Sub

Sub CalcoloParametri_INGRANAGGI_ESTERNIO()
'
Dim tmp       As Double
Dim i         As Integer
'
On Error GoTo errCalcoloParametri_INGRANAGGI_ESTERNIO
  '
  ParametriOK = True
  Call InizializzaVariabili
  '
  'Azzeramento variabili calcolo
  CalcoloEsterni.DB = 0
  ReDim CalcoloEsterni.DCont(0)
  CalcoloEsterni.DE = 0
  CalcoloEsterni.DI = 0
  CalcoloEsterni.DIOtt = 0
  CalcoloEsterni.DP = 0
  ReDim CalcoloEsterni.IngrX(0)
  ReDim CalcoloEsterni.IngrY(0)
  ReDim CalcoloEsterni.LCont(0)
  ReDim CalcoloEsterni.MolaX(0)
  ReDim CalcoloEsterni.MolaY(0)
  CalcoloEsterni.Np = 0
  '
  'Se viene inserito un angolo elica negativo, viene diviso il modulo dal segno
  If (Lp("iAngoloElica") < 0) Then
    Call SP("iSensoElica", -1)
    Call SP("iAngoloElica", Abs(Lp("iAngoloElica")))
  End If
  Call SP("R[41]", Lp("iAngoloElica") * Lp("iSensoElica"))
  '
  'Lettura Parametri
  DGlob.Z = Lp("R[5]")
  DGlob.Mn = Lp("R[42]")
  DGlob.ALFA = Lp("R[40]") * PG / 180
  DGlob.DI = Lp("R[45]")
  DGlob.DE = Lp("DEXT_G")
  DGlob.SON = Lp("R[46]")
  DGlob.DefSpe = Lp("iDefSp")
  DGlob.WbQ = Lp("R[47]")
  DGlob.WbN = Lp("R[48]")
  DGlob.RulQ = Lp("R[49]")
  DGlob.RulD = Lp("R[50]")
  DGlob.Beta = Lp("R[41]") * PG / 180
  DGlob.RT = Lp("R[51]")
  DGlob.RF = Lp("R[52]")
  DGlob.DistPtEv = Lp("R[194]")
  
  'Calcolo altri parametri
  DGlob.ALFAt = Atn(Tan(DGlob.ALFA) / Cos(DGlob.Beta))
  DGlob.DP = DGlob.Mn * DGlob.Z / Cos(DGlob.Beta)
  DGlob.DB = DGlob.DP * Cos(DGlob.ALFAt)
  '
  Dim iB    As Double
  Dim Betab As Double
  Dim PB    As Double
  Dim Sb    As Double
  Dim E1    As Double
  Dim H2    As Double
  Dim H3    As Double
  Dim F1    As Double
  Dim F2    As Double
  '
  Dim Wx      As Double
  Dim We      As Double
  Dim X       As Double
  Dim X1      As Double
  Dim Ymini   As Double
  Dim Ymaxi   As Double
  Dim Ymoyen  As Double
  '
  'Se lo spessore � nullo lo imposto a mezzo passo
  If (DGlob.SON = 0) And (DGlob.DefSpe = 1) Then
    DGlob.SON = Round((DGlob.Mn * PG) / 2, 3)
    Call SP("R[46]", DGlob.SON)
    'CALCOLO DEL FATTORE DI SPOSTAMENTO
    DGlob.FattoreX = (DGlob.SON - ((PG / 2) * DGlob.Mn)) / (2 * DGlob.Mn * Tan(DGlob.ALFA))
    DGlob.FattoreX = Int(DGlob.FattoreX * 100000 + 0.5) / 100000
    Call SP("FattoreX", DGlob.FattoreX)
  End If
  '
  Betab = Atn(Tan(Abs(DGlob.Beta)) * (Cos(DGlob.ALFAt)))
  PB = DGlob.DB * PG / DGlob.Z
  '
  Select Case DGlob.DefSpe
    '
    Case 1 'SON
      If (DGlob.SON > 0) Then
        'CALCOLO QUOTA WILDHABER
        If (DGlob.WbN > 0) Then
          DGlob.SONt = DGlob.SON / Cos(DGlob.Beta)
          Sb = (DGlob.SONt + DGlob.DP * inv(DGlob.ALFAt)) * DGlob.DB / DGlob.DP
          iB = PB - Sb
          DGlob.WbQ = DGlob.WbN * PB * Cos(Betab) - iB * Cos(Betab)
          'CALCOLO QUOTA RULLI
          If (DGlob.RulD > 0) Then
            Wx = Cos(PG / (2 * DGlob.Z)) * DGlob.DB
            If Int(DGlob.Z / 2) = DGlob.Z / 2 Then Wx = DGlob.DB
            We = Sb - PB
            X = (We + DGlob.RulD / Cos(Betab)) / DGlob.DB
            Ymini = 0: Ymaxi = PG / 2
            For i = 1 To 25
              Ymoyen = (Ymini + Ymaxi) / 2: X1 = Tan(Ymoyen) - Ymoyen
              If X1 < X Then Ymini = Ymoyen Else Ymaxi = Ymoyen
            Next i
            DGlob.RulQ = (Wx / Cos(Ymoyen)) + DGlob.RulD
          Else
            DGlob.RulQ = 0
          End If
        Else
          DGlob.WbQ = 0
        End If
        'CALCOLO DEL FATTORE DI SPOSTAMENTO
        DGlob.FattoreX = (DGlob.SON - ((PG / 2) * DGlob.Mn)) / (2 * DGlob.Mn * Tan(DGlob.ALFA))
        'AGGIORNAMENTO VALORI
        Call SP("R[47]", frmt(DGlob.WbQ, 8))
        Call SP("R[49]", frmt(DGlob.RulQ, 8))
        Call SP("FattoreX", frmt(DGlob.FattoreX, 8))
      End If
      '
    Case 2 'Quota Wb
      If (DGlob.WbN > 0) And (DGlob.WbQ > 0) Then
        iB = DGlob.WbN * PB - DGlob.WbQ / Cos(Betab)
        Sb = PB - iB
        DGlob.SONt = DGlob.DP * (Sb / DGlob.DB - (inv(DGlob.ALFAt)))
        DGlob.SON = DGlob.SONt * Cos(DGlob.Beta)
        'CALCOLO QUOTA RULLI
        If (DGlob.RulD > 0) Then
          Wx = Cos(PG / (2 * DGlob.Z)) * DGlob.DB
          If Int(DGlob.Z / 2) = DGlob.Z / 2 Then Wx = DGlob.DB
          We = Sb - PB
          X = (We + DGlob.RulD / Cos(Betab)) / DGlob.DB
          Ymini = 0: Ymaxi = PG / 2
          For i = 1 To 25
            Ymoyen = (Ymini + Ymaxi) / 2: X1 = Tan(Ymoyen) - Ymoyen
            If X1 < X Then Ymini = Ymoyen Else Ymaxi = Ymoyen
          Next i
          DGlob.RulQ = (Wx / Cos(Ymoyen)) + DGlob.RulD
        Else
          DGlob.RulQ = 0
        End If
        'CALCOLO DEL FATTORE DI SPOSTAMENTO
        DGlob.FattoreX = (DGlob.SON - ((PG / 2) * DGlob.Mn)) / (2 * DGlob.Mn * Tan(DGlob.ALFA))
        'AGGIORNAMENTO VALORI
        Call SP("R[46]", frmt(DGlob.SON, 8))
        Call SP("R[49]", frmt(DGlob.RulQ, 8))
        Call SP("FattoreX", frmt(DGlob.FattoreX, 8))
      End If
      '
    Case 3 'Quota Rulli
      If (DGlob.RulQ <> 0) And (DGlob.RulD <> 0) And (DGlob.RulD <> DGlob.RulQ) Then
        E1 = PG / 2 / DGlob.Z
        If (Int(DGlob.Z / 2) = DGlob.Z / 2) Then E1 = 0
        H2 = DGlob.Mn * DGlob.Z * Cos(DGlob.ALFAt) * Cos(E1) / ((DGlob.RulQ - DGlob.RulD) * Cos(Abs(DGlob.Beta)))
        H3 = Tan(Acs(H2))
        F2 = (inv(DGlob.ALFAt) + DGlob.RulD / (DGlob.Mn * DGlob.Z * Cos(DGlob.ALFA)) - H3 + Atn(H3)) * DGlob.Mn * DGlob.Z
        DGlob.SON = PG * DGlob.Mn - F2
        'CALCOLO DEL FATTORE DI SPOSTAMENTO
        DGlob.FattoreX = (DGlob.SON - ((PG / 2) * DGlob.Mn)) / (2 * DGlob.Mn * Tan(DGlob.ALFA))
      End If
      'CALCOLO QUOTA WILDHABER
      If (DGlob.WbN > 0) Then
        DGlob.SONt = DGlob.SON / Cos(DGlob.Beta)
        Sb = (DGlob.SONt + DGlob.DP * inv(DGlob.ALFAt)) * DGlob.DB / DGlob.DP
        iB = PB - Sb
        DGlob.WbQ = DGlob.WbN * PB * Cos(Betab) - iB * Cos(Betab)
      Else
        DGlob.WbQ = 0
      End If
      'AGGIORNAMENTO VALORI
      Call SP("R[46]", frmt(DGlob.SON, 8))
      Call SP("R[47]", frmt(DGlob.WbQ, 8))
      Call SP("FattoreX", frmt(DGlob.FattoreX, 8))
      '
    Case 4 'Fattore X
      DGlob.FattoreX = Lp("FattoreX")
      'CALCOLO DELLO SPESSORE NORMALE
      DGlob.SON = 2 * DGlob.Mn * DGlob.FattoreX * Tan(DGlob.ALFA) + ((PG / 2) * DGlob.Mn)
      'CALCOLO QUOTA WILDHABER
      If (DGlob.WbN > 0) Then
        DGlob.SONt = DGlob.SON / Cos(DGlob.Beta)
        Sb = (DGlob.SONt + DGlob.DP * inv(DGlob.ALFAt)) * DGlob.DB / DGlob.DP
        iB = PB - Sb
        DGlob.WbQ = DGlob.WbN * PB * Cos(Betab) - iB * Cos(Betab)
        'CALCOLO QUOTA RULLI
        If (DGlob.RulD > 0) Then
          Wx = Cos(PG / (2 * DGlob.Z)) * DGlob.DB
          If Int(DGlob.Z / 2) = DGlob.Z / 2 Then Wx = DGlob.DB
          We = Sb - PB
          X = (We + DGlob.RulD / Cos(Betab)) / DGlob.DB
          Ymini = 0: Ymaxi = PG / 2
          For i = 1 To 25
            Ymoyen = (Ymini + Ymaxi) / 2: X1 = Tan(Ymoyen) - Ymoyen
            If X1 < X Then Ymini = Ymoyen Else Ymaxi = Ymoyen
          Next i
          DGlob.RulQ = (Wx / Cos(Ymoyen)) + DGlob.RulD
        Else
          DGlob.RulQ = 0
        End If
      Else
        DGlob.WbQ = 0
      End If
      'AGGIORNAMENTO VALORI
      Call SP("R[46]", frmt(DGlob.SON, 8))
      Call SP("R[47]", frmt(DGlob.WbQ, 8))
      Call SP("R[49]", frmt(DGlob.RulQ, 8))
      '
  End Select
  '
  'Calcolo altri parametri
  DGlob.SONt = DGlob.SON / Cos(DGlob.Beta)
  DGlob.Eb = DGlob.DB * ((DGlob.SONt / DGlob.DP) + inv(DGlob.ALFAt))
  '
  'Se l'inclinazione della mola in rettifica � 0
  'viene impostata uguale all'angolo elica.
  If (Lp("R[258]") = 0) Then Call SP("R[258]", Abs(Lp("iAngoloElica")))
  '
  'Se il numero pezzi in rettifica � 1
  'azzero la distanza tra i pezzi
  If (Lp("R[9]") <= 1) Then Call SP("R[133]", 0)
  '
  'Lettura Zone
  ReDim DGlob.ZonaF1(0)
  ReDim DGlob.ZonaF2(0)
  Dim SAP_tmp As Double
  '
  DGlob.ZonaF1(0) = DIA2TIF(Lp("R[43]"), DGlob.DB)    'SAP F1
  DGlob.ZonaF2(0) = DIA2TIF(Lp("SAPF2_G"), DGlob.DB)  'SAP F2
  SAP_tmp = Lp("R[43]")
  DGlob.ValTIFDIA = Lp("R[901]")
  '
  'Se SAP � sotto il DB metto il diametro interno RaggioUtensile (Se Beta=0) + DistanzaPunti
  '(solo come valore di default)
  If (DGlob.ZonaF1(0) = 0) Then
    If (DGlob.Beta = 0) Then
      tmp = LEP("$TC_DP6[1,1]")
      tmp = tmp + DGlob.DistPtEv
    Else
      tmp = DGlob.DistPtEv
    End If
    DGlob.ZonaF1(0) = tmp
    Call SP("R[43]", frmt0(TIF2DIA(DGlob.ZonaF1(0), DGlob.DB) + 0.001, 3))
  End If
  If (DGlob.ZonaF2(0) = 0) Then
    If (DGlob.Beta = 0) Then
      tmp = LEP("$TC_DP6[1,2]")
      tmp = tmp + DGlob.DistPtEv
    Else
      tmp = DGlob.DistPtEv
    End If
    DGlob.ZonaF2(0) = tmp
    Call SP("SAPF2_G", frmt0(TIF2DIA(DGlob.ZonaF2(0), DGlob.DB) + 0.001, 3))
  End If
  '
  For i = 0 To 2
    tmp = Lp("R[" & 270 + i * 3 & "]") 'Fine Zona i+1  ( R270 R273 R276 )
    If (tmp <> 0) Then
      'Eventuale conversione Dia -> Tif
      If (DGlob.ValTIFDIA = 2) Then
        tmp = DIA2TIF(tmp, DGlob.DB)
      End If
      ReDim Preserve DGlob.ZonaF1(UBound(DGlob.ZonaF1) + 1)
      If (tmp > DIA2TIF(DGlob.DE, DGlob.DB)) Then
        DGlob.ZonaF1(UBound(DGlob.ZonaF1)) = DIA2TIF(DGlob.DE, DGlob.DB)
        Exit For
      Else
        DGlob.ZonaF1(UBound(DGlob.ZonaF1)) = tmp
      End If
    Else
      If (UBound(DGlob.ZonaF1) = 0) Then
        ReDim Preserve DGlob.ZonaF1(UBound(DGlob.ZonaF1) + 1)
        DGlob.ZonaF1(UBound(DGlob.ZonaF1)) = DIA2TIF(DGlob.DE, DGlob.DB)
      End If
      Exit For
    End If
  Next i
  '
  For i = 0 To 2
    tmp = Lp("R[" & 285 + i * 3 & "]") 'Fine Zona i+1  ( R285 R288 R291 )
    If (tmp <> 0) Then
      'Eventuale conversione Dia -> Tif
      If (DGlob.ValTIFDIA = 2) Then
        tmp = DIA2TIF(tmp, DGlob.DB)
      End If
      ReDim Preserve DGlob.ZonaF2(UBound(DGlob.ZonaF2) + 1)
      If (tmp > DIA2TIF(DGlob.DE, DGlob.DB)) Then
        DGlob.ZonaF2(UBound(DGlob.ZonaF2)) = DIA2TIF(DGlob.DE, DGlob.DB)
        Exit For
      Else
        DGlob.ZonaF2(UBound(DGlob.ZonaF2)) = tmp
      End If
    Else
      If (UBound(DGlob.ZonaF2) = 0) Then
        ReDim Preserve DGlob.ZonaF2(UBound(DGlob.ZonaF2) + 1)
        DGlob.ZonaF2(UBound(DGlob.ZonaF2)) = DIA2TIF(DGlob.DE, DGlob.DB)
      End If
      Exit For
    End If
  Next i
  '
  If (DGlob.DistPtEv > 0) Then
    DGlob.NumPtEvF1 = Int((DGlob.ZonaF1(UBound(DGlob.ZonaF1)) - DGlob.ZonaF1(0)) / DGlob.DistPtEv) + 2
    DGlob.NumPtEvF2 = Int((DGlob.ZonaF2(UBound(DGlob.ZonaF2)) - DGlob.ZonaF2(0)) / DGlob.DistPtEv) + 2
  End If
  '
  Call CALCOLO_INGRANAGGIO_ESTERNO_380(0, True)
  '
  tmp = 0
  '
On Error Resume Next
  '
  'Se non � possibile completare il calcolo perch� mancano dei parametri
  'Inizializzo l'extracorsa a 0 (tmp=0) e ignoro gli errori.
  tmp = Abs(CalcoloEsterni.LCont(1, 1))
  If (tmp < Abs(CalcoloEsterni.LCont(1, CalcoloEsterni.Np))) Then
    tmp = Abs(CalcoloEsterni.LCont(1, CalcoloEsterni.Np))
  End If
  '
On Error GoTo errCalcoloParametri_INGRANAGGI_ESTERNIO
  '
  Call SP("iCORSAIN", Round(tmp + 0.05, 1))
  Call SP("iCORSAOUT", Round(tmp + 0.05, 1))
  '
  Call SP("CORSAIN", Lp("iCORSAIN") + Lp("iCORSAIN_CORR"))
  Call SP("CORSAOUT", Lp("iCORSAOUT") + Lp("iCORSAOUT_CORR"))
  '
  Dim Div_T   As Double
  Dim Div_Rm  As Double
  Dim Div_hz  As Double
  Dim Div_Rt  As Double
  '
  If (Lp("iMetDiv") = 1) Then
    Call SP("R[130]", 0)
  Else
    If (Lp("R[130]") = 0) Then
      Div_Rm = Lp("iDiaMola") / 2
      Div_hz = (Lp("DEXT_G") - Lp("R[45]")) / 2
      Div_Rt = Lp("R[51]")
      Div_T = Sqr(Div_Rm ^ 2 - (Div_Rm - Div_hz - Div_Rt) ^ 2)
      Call SP("R[130]", Round(Div_T - tmp + 0.05, 1))
    End If
  End If
  '
  Select Case Lp("iNZone")
      '
    Case 1
      Call SP("Fascia2F1", 0)
      Call SP("Fascia2F2", 0)
      Call SP("Fascia3F1", 0)
      Call SP("Fascia3F2", 0)
      '
      Call SP("Conic2F1", 0)
      Call SP("Conic2F2", 0)
      Call SP("Bomb2F1", 0)
      Call SP("Bomb2F2", 0)
      '
      Call SP("Conic3F1", 0)
      Call SP("Conic3F2", 0)
      Call SP("Bomb3F1", 0)
      Call SP("Bomb3F2", 0)
      '
    Case 2
      Call SP("Fascia3F1", 0)
      Call SP("Fascia3F2", 0)
      '
      Call SP("Conic3F1", 0)
      Call SP("Conic3F2", 0)
      Call SP("Bomb3F1", 0)
      Call SP("Bomb3F2", 0)
      '
    Case 3
      '
  End Select
  tmp = Lp("Fascia1F1") + Lp("Fascia2F1") + Lp("Fascia3F1")
  Call SP("FASCIATOTALE_G", tmp)
  If (Lp("COR[0,74]") <= 0) Then Call SP("COR[0,74]", Int(tmp * 0.8 * 10) / 10)
  If (Lp("COR[0,79]") <= 0) Then Call SP("COR[0,79]", Int(DGlob.DE * 10) / 10 - 1)
  If (Lp("COR[0,82]") <= 0) Then Call SP("COR[0,82]", SAP_tmp)
  '
  'CONTROLLO PARAMETRI PER RETTIFICA DIAMETRI
  Dim k    As Integer
  Dim k1   As Integer 'CONTROLLO DIAMETRALE
  Dim k2   As Integer 'CHIUSURA LUNETTA
  Dim k3   As Integer 'APERTURA LUNETTA
  Dim Jd   As Integer 'CONTATORE DIAMETRALE
  Dim jL   As Integer 'CONTATORE LUNETTA
  Dim QC1  As Double  'QUOTA CHIUSURA LUNETTA 1
  Dim QA1  As Double  'QUOTA APERTURA LUNETTA 1
  Dim LM   As Double  'LARGHEZZA MOLA
  '
  LM = LeggiLarghezzaMolaRDE
  If (LM <= 0) Then LM = 50
  '
  k1 = 0: k2 = 0
  For i = 1 To 10
    'INDICE DEL DIAMETRO
    k = (i - 1) * 10
    'POSSO ATTIVARE UN SOLO CONTROLLO DIAMETRALE
    Jd = val(Lp("RDS[0," & Format$(k + 8, "#0") & "]"))
    If (Jd = 1) Then
    If (k1 = 0) Then
      k1 = k1 + 1
    Else
      Call SP("RDS[0," & Format$(k + 8, "#0") & "]", 0)
    End If
    End If
    'POSSO CHIUDERE LA LUNETTA UNA SOLA VOLTA
    jL = val(Lp("RDS[0," & Format$(k + 7, "#0") & "]"))
    Select Case jL
    'CHIUSURA LUNETTA 1
    Case 1
    If (k2 = 0) Then
      k2 = k2 + 1
      QC1 = val(Lp("RDS[0," & Format$(k + 1, "#0") & "]"))
    Else
      Call SP("RDS[0," & Format$(k + 7, "#0") & "]", 0)
    End If
    'APERTURA LUNETTA 1
    Case -1
      QA1 = val(Lp("RDS[0," & Format$(k + 1, "#0") & "]"))
    If (k3 = 0) And (k2 > 0) And (Abs(QC1 - QA1) <= LM) Then
      k3 = k3 + 1
    Else
      Call SP("RDS[0," & Format$(k + 7, "#0") & "]", 0)
    End If
    End Select
    'NON POSSO ATTIVARE IL DIAMETRALE SULLA LUNETTA
    If (Jd = 1) And ((jL = 1) Or (jL = -1)) Then
      Call SP("RDS[0," & Format$(k + 8, "#0") & "]", 0)
    End If
  Next i
  '
Exit Sub

errCalcoloParametri_INGRANAGGI_ESTERNIO:
  If (Err.Number = 6) Or (Err.Number = 11) Then
    ParametriOK = False
    PAR.Pic.Cls
  Else
    MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Sub: CalcoloParametri_INGRANAGGI_ESTERNIO"
  End If

End Sub

Sub CalcoloParametri_SCANALATI_ESTERNI()
'
Dim tmp       As Double
Dim i         As Integer
Dim ERRORE    As Boolean
'
Dim Div_T   As Double
Dim Div_Rm  As Double
Dim Div_hz  As Double
Dim Div_Rt  As Double
'
On Error GoTo errCalcoloParametri_SCANALATI_ESTERNI
  '
  ParametriOK = True
  Call InizializzaVariabili
  '
  tmp = 0
  Call SP("iCORSAIN", Round(tmp + 0.05, 1))
  Call SP("iCORSAOUT", Round(tmp + 0.05, 1))
  Call SP("CORSAIN", Lp("iCORSAIN") + Lp("iCORSAIN_CORR"))
  Call SP("CORSAOUT", Lp("iCORSAOUT") + Lp("iCORSAOUT_CORR"))
  '
  'Agg. 03.04.09
  If (Lp("iMetDiv") = 1) Then
    Call SP("R[130]", 0)
  Else
    'Calcolo tratto laterale (R130)
    If Lp("R[130]") = 0 Then
      Div_Rm = Lp("iDiaMola") / 2
      Div_hz = (Lp("R[44]") - Lp("R[103]")) / 2
      Div_Rt = Lp("R[51]")
      Div_T = Sqr(Div_Rm ^ 2 - (Div_Rm - Div_hz - Div_Rt) ^ 2)
      Call SP("R[130]", Round(Div_T - tmp + 0.05, 1))
    End If
  End If
  '
  Select Case Lp("iNZone")
      '
    Case 1
      Call SP("Fascia2F1", 0)
      Call SP("Fascia2F2", 0)
      Call SP("Fascia3F1", 0)
      Call SP("Fascia3F2", 0)
      '
      Call SP("Conic2F1", 0)
      Call SP("Conic2F2", 0)
      Call SP("Bomb2F1", 0)
      Call SP("Bomb2F2", 0)
      '
      Call SP("Conic3F1", 0)
      Call SP("Conic3F2", 0)
      Call SP("Bomb3F1", 0)
      Call SP("Bomb3F2", 0)
      '
    Case 2
      Call SP("Fascia3F1", 0)
      Call SP("Fascia3F2", 0)
      '
      Call SP("Conic3F1", 0)
      Call SP("Conic3F2", 0)
      Call SP("Bomb3F1", 0)
      Call SP("Bomb3F2", 0)
      '
    Case 3
      '
  End Select
  '
  Call SP("FASCIATOTALE_G", Lp("Fascia1F1") + Lp("Fascia2F1") + Lp("Fascia3F1"))
  '
Exit Sub

errCalcoloParametri_SCANALATI_ESTERNI:
  If (Err.Number = 6) Or (Err.Number = 11) Then
    ParametriOK = False
    PAR.Pic.Cls
  Else
    MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Sub: CalcoloParametri_SCANALATI_ESTERNI"
  End If
  Resume Next
  
End Sub

Sub CalcoloParametri_SCANALATI_CBN()
'
Dim CICL(6) As Integer
Dim MOLE(6) As Integer
Dim ii      As Integer
Dim jj      As Integer
Dim R137    As Integer
Dim R121    As Integer
Dim stmp    As String
'
On Error GoTo errCalcoloParametri_SCANALATI_CBN
  '
  ParametriOK = True
  Call InizializzaVariabili
  '
  CICL(1) = Lp("CYC[0,0]")
  CICL(2) = Lp("CYC[0,20]")
  CICL(3) = Lp("CYC[0,40]")
  CICL(4) = Lp("CYC[0,60]")
  CICL(5) = Lp("CYC[0,80]")
  CICL(6) = Lp("CYC[0,100]")
  '
  MOLE(1) = Lp("CYC[0,7]")
  MOLE(2) = Lp("CYC[0,27]")
  MOLE(3) = Lp("CYC[0,47]")
  MOLE(4) = Lp("CYC[0,67]")
  MOLE(5) = Lp("CYC[0,87]")
  MOLE(6) = Lp("CYC[0,107]")
  '
  'CONTROLLO SEQUENZA MOLE
  For ii = 1 To 6
    R137 = CICL(ii)
    If (R137 <> 0) Then
      R121 = MOLE(ii)
      If (R121 = 2) And (ii < 6) Then
        For jj = ii + 1 To 6
          R137 = CICL(jj)
          If (R137 <> 0) Then
            R121 = MOLE(jj)
            If (R121 = 1) Then
              ParametriOK = False
              PAR.Pic.Cls
              StopRegieEvents
              stmp = "WRONG WHEEL SEQUENCE!!!" & Chr(13) & Chr(13)
              stmp = stmp & "C" & Format$(ii, "0") & " --> " & "C" & Format$(jj, "0")
              Call MsgBox(stmp, vbCritical, "WARNING")
              ResumeRegieEvents
            End If
          End If
        Next jj
      End If
    End If
  Next ii
  '
Exit Sub

errCalcoloParametri_SCANALATI_CBN:
  If (Err.Number = 6) Or (Err.Number = 11) Then
    ParametriOK = False
    PAR.Pic.Cls
  Else
    MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Sub: CalcoloParametri_SCANALATI_CBN"
  End If
  Resume Next
  
End Sub

Sub CalcoloParametri_SCANALATI_INTERNI_CBN()
'
Dim CICL(6) As Integer
Dim MOLE(6) As Integer
Dim ii      As Integer
Dim jj      As Integer
Dim R137    As Integer
Dim R121    As Integer
Dim stmp    As String
'
On Error GoTo errCalcoloParametri_SCANALATI_INTERNI_CBN
  '
  ParametriOK = True
  Call InizializzaVariabili
  '
  CICL(1) = Lp("CYC[0,0]")
  CICL(2) = Lp("CYC[0,20]")
  CICL(3) = Lp("CYC[0,40]")
  CICL(4) = Lp("CYC[0,60]")
  CICL(5) = Lp("CYC[0,80]")
  CICL(6) = Lp("CYC[0,100]")
  '
  MOLE(1) = Lp("CYC[0,7]")
  MOLE(2) = Lp("CYC[0,27]")
  MOLE(3) = Lp("CYC[0,47]")
  MOLE(4) = Lp("CYC[0,67]")
  MOLE(5) = Lp("CYC[0,87]")
  MOLE(6) = Lp("CYC[0,107]")
  '
  'CONTROLLO SEQUENZA MOLE
  For ii = 1 To 6
    R137 = CICL(ii)
    If (R137 <> 0) Then
      R121 = MOLE(ii)
      If (R121 = 2) And (ii < 6) Then
        For jj = ii + 1 To 6
          R137 = CICL(jj)
          If (R137 <> 0) Then
            R121 = MOLE(jj)
            If (R121 = 1) Then
              ParametriOK = False
              PAR.Pic.Cls
              StopRegieEvents
              stmp = "WRONG WHEEL SEQUENCE!!!" & Chr(13) & Chr(13)
              stmp = stmp & "C" & Format$(ii, "0") & " --> " & "C" & Format$(jj, "0")
              Call MsgBox(stmp, vbCritical, "WARNING")
              ResumeRegieEvents
            End If
          End If
        Next jj
      End If
    End If
  Next ii
  '
Exit Sub

errCalcoloParametri_SCANALATI_INTERNI_CBN:
  If (Err.Number = 6) Or (Err.Number = 11) Then
    ParametriOK = False
    PAR.Pic.Cls
  Else
    MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Sub: CalcoloParametri_SCANALATI_INTERNI_CBN"
  End If
  Resume Next
  
End Sub
Sub CalcoloParametri_VITI_ESTERNE()

Dim tmp         As Double
Dim Div_Rm      As Double
Dim Div_Pas     As Double
Dim PASSO       As Double
'
Dim LarghezzaMOLA As Double
Dim PrincipiPEZZO As Integer
Dim PrincipiMOLA  As Integer
Dim LatoPart      As Integer
'
On Error GoTo errCalcoloParametri_VITI_ESTERNE
  '
  PASSO = Lp("PassoELICA_G")
  PrincipiPEZZO = Lp("R[5]")
  LatoPart = Lp("R[12]")
  LarghezzaMOLA = Lp("R[37]")
  PrincipiMOLA = Lp("NUMDENTIM_G")
  If (PrincipiMOLA = 0) Then PrincipiMOLA = 1
  '
  If (PrincipiPEZZO <> 0) Then
    tmp = PASSO / PrincipiPEZZO
  Else
    tmp = 0
  End If
  '
  If (PrincipiMOLA = 1) Then
    Call SP("iCORSAIN", Round(tmp + 0.05, 1))
    Call SP("iCORSAOUT", Round(tmp + 0.05, 1))
    Call SP("CORSAIN", Lp("iCORSAIN") + Lp("iCORSAIN_CORR"))
    Call SP("CORSAOUT", Lp("iCORSAOUT") + Lp("iCORSAOUT_CORR"))
  Else
    If (LatoPart = 1) Then
      Call SP("iCORSAOUT", Round(tmp + 0.05, 1))
      tmp = tmp + (PrincipiMOLA - 1) * LarghezzaMOLA
      Call SP("iCORSAIN", Round(tmp + 0.05, 1))
      Call SP("CORSAIN", Lp("iCORSAIN") + Lp("iCORSAIN_CORR"))
      Call SP("CORSAOUT", Lp("iCORSAOUT") + Lp("iCORSAOUT_CORR"))
    Else
      Call SP("iCORSAIN", Round(tmp + 0.05, 1))
      tmp = tmp + (PrincipiMOLA - 1) * LarghezzaMOLA
      Call SP("iCORSAOUT", Round(tmp + 0.05, 1))
      Call SP("CORSAIN", Lp("iCORSAIN") + Lp("iCORSAIN_CORR"))
      Call SP("CORSAOUT", Lp("iCORSAOUT") + Lp("iCORSAOUT_CORR"))
    End If
  End If
  '
  tmp = 0
  If (Lp("iMetDiv") = 1) Then
      Call SP("R[130]", 0)
  Else
    If (Lp("R[130]") = 0) Then
      Div_Rm = Lp("iDiaMola") / 2
      Div_Pas = PASSO / PrincipiPEZZO
      Call SP("R[130]", Round(Div_Pas + 0.05, 1))
    End If
  End If
  
  Select Case Lp("iNZone")
    Case 1
      Call SP("Fascia2F1", 0): Call SP("Fascia2F2", 0)
      Call SP("Conic2F1", 0):  Call SP("Conic2F2", 0)
      Call SP("Bomb2F1", 0):   Call SP("Bomb2F2", 0)
      Call SP("Fascia3F1", 0): Call SP("Fascia3F2", 0)
      Call SP("Conic3F1", 0):  Call SP("Conic3F2", 0)
      Call SP("Bomb3F1", 0):   Call SP("Bomb3F2", 0)
    Case 2
      Call SP("Fascia3F1", 0): Call SP("Fascia3F2", 0)
      Call SP("Conic3F1", 0):  Call SP("Conic3F2", 0)
      Call SP("Bomb3F1", 0):   Call SP("Bomb3F2", 0)
    Case 3
      'NON FACCIO NIENTE
  End Select
  Call SP("FASCIATOTALE_G", Lp("Fascia1F1") + Lp("Fascia2F1") + Lp("Fascia3F1"))
  '
Exit Sub

errCalcoloParametri_VITI_ESTERNE:
  If (Err.Number = 6) Or (Err.Number = 11) Then
    PAR.Pic.Cls
  Else
    MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Sub: CalcoloParametri_VITI_ESTERNE"
  End If
  
End Sub

Sub CalcoloParametri_VITIV_ESTERNE()

Dim tmp         As Double
Dim Div_Rm      As Double
Dim Div_Pas     As Double
Dim PASSO       As Double
'
Dim LarghezzaMOLA As Double
Dim PrincipiPEZZO As Integer
Dim PrincipiMOLA  As Integer
Dim LatoPart      As Integer
'
On Error GoTo errCalcoloParametri_VITIV_ESTERNE
  '
  PASSO = Lp("PassoELICA_G")
  PrincipiPEZZO = Lp("R[5]")
  LatoPart = Lp("R[12]")
  LarghezzaMOLA = Lp("R[37]")
  PrincipiMOLA = Lp("NUMDENTIM_G")
  If (PrincipiMOLA = 0) Then PrincipiMOLA = 1
  '
  If (PrincipiPEZZO <> 0) Then
    tmp = PASSO / PrincipiPEZZO
  Else
    tmp = 0
  End If
  '
  If (PrincipiMOLA = 1) Then
    Call SP("iCORSAIN", Round(tmp + 0.05, 1))
    Call SP("iCORSAOUT", Round(tmp + 0.05, 1))
    Call SP("CORSAIN", Lp("iCORSAIN") + Lp("iCORSAIN_CORR"))
    Call SP("CORSAOUT", Lp("iCORSAOUT") + Lp("iCORSAOUT_CORR"))
  Else
    If (LatoPart = 1) Then
      Call SP("iCORSAOUT", Round(tmp + 0.05, 1))
      tmp = tmp + (PrincipiMOLA - 1) * LarghezzaMOLA
      Call SP("iCORSAIN", Round(tmp + 0.05, 1))
      Call SP("CORSAIN", Lp("iCORSAIN") + Lp("iCORSAIN_CORR"))
      Call SP("CORSAOUT", Lp("iCORSAOUT") + Lp("iCORSAOUT_CORR"))
    Else
      Call SP("iCORSAIN", Round(tmp + 0.05, 1))
      tmp = tmp + (PrincipiMOLA - 1) * LarghezzaMOLA
      Call SP("iCORSAOUT", Round(tmp + 0.05, 1))
      Call SP("CORSAIN", Lp("iCORSAIN") + Lp("iCORSAIN_CORR"))
      Call SP("CORSAOUT", Lp("iCORSAOUT") + Lp("iCORSAOUT_CORR"))
    End If
  End If
  '
  tmp = 0
  If (Lp("iMetDiv") = 1) Then
      Call SP("R[130]", 0)
  Else
    If (Lp("R[130]") = 0) Then
      Div_Rm = Lp("iDiaMola") / 2
      Div_Pas = PASSO / PrincipiPEZZO
      Call SP("R[130]", Round(Div_Pas + 0.05, 1))
    End If
  End If
  
  Select Case Lp("iNZone")
    Case 1
      Call SP("Fascia2F1", 0): Call SP("Fascia2F2", 0)
      Call SP("Conic2F1", 0):  Call SP("Conic2F2", 0)
      Call SP("Bomb2F1", 0):   Call SP("Bomb2F2", 0)
      Call SP("Fascia3F1", 0): Call SP("Fascia3F2", 0)
      Call SP("Conic3F1", 0):  Call SP("Conic3F2", 0)
      Call SP("Bomb3F1", 0):   Call SP("Bomb3F2", 0)
    Case 2
      Call SP("Fascia3F1", 0): Call SP("Fascia3F2", 0)
      Call SP("Conic3F1", 0):  Call SP("Conic3F2", 0)
      Call SP("Bomb3F1", 0):   Call SP("Bomb3F2", 0)
    Case 3
      'NON FACCIO NIENTE
  End Select
  Call SP("FASCIATOTALE_G", Lp("Fascia1F1") + Lp("Fascia2F1") + Lp("Fascia3F1"))
  '
Exit Sub

errCalcoloParametri_VITIV_ESTERNE:
  If (Err.Number = 6) Or (Err.Number = 11) Then
    PAR.Pic.Cls
  Else
    MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Sub: CalcoloParametri_VITIV_ESTERNE"
  End If
  
End Sub

Sub CalcoloParametri_PROFILO_PER_PUNTI_ESTERNI()
'
Dim tmp     As Double
Dim i       As Integer
Dim ERRORE  As Boolean
'
On Error GoTo errCalcoloParametri_PROFILO_PER_PUNTI_ESTERNI
  '
  ParametriOK = True
  Call InizializzaVariabili
  'Se il numero pezzi in rettifica � 1
  'azzero la distanza tra i pezzi
    If (Lp("R[9]") <= 1) Then Call SP("R[133]", 0)
    Call SP("iCORSAIN", 0)  'Round(tmp + 0.05, 1)
    Call SP("iCORSAOUT", 0)  ' Round(tmp + 0.05, 1)
    Call SP("CORSAIN", Lp("iCORSAIN") + Lp("iCORSAIN_CORR"))
    Call SP("CORSAOUT", Lp("iCORSAOUT") + Lp("iCORSAOUT_CORR"))
    If (Lp("iMetDiv") = 1) Then Call SP("R[130]", 0)
    '
    Select Case Lp("iNZone")
      Case 1
        Call SP("Fascia2F1", 0)
        Call SP("Fascia2F2", 0)
        Call SP("Fascia3F1", 0)
        Call SP("Fascia3F2", 0)
      
        Call SP("Conic2F1", 0)
        Call SP("Conic2F2", 0)
        Call SP("Bomb2F1", 0)
        Call SP("Bomb2F2", 0)
        
        Call SP("Conic3F1", 0)
        Call SP("Conic3F2", 0)
        Call SP("Bomb3F1", 0)
        Call SP("Bomb3F2", 0)
  
      Case 2
        Call SP("Fascia3F1", 0)
        Call SP("Fascia3F2", 0)

        Call SP("Conic3F1", 0)
        Call SP("Conic3F2", 0)
        Call SP("Bomb3F1", 0)
        Call SP("Bomb3F2", 0)
      
      Case 3
        '
    End Select
    Call SP("FASCIATOTALE_G", Lp("Fascia1F1") + Lp("Fascia2F1") + Lp("Fascia3F1"))
    '
On Error GoTo 0
Exit Sub

errCalcoloParametri_PROFILO_PER_PUNTI_ESTERNI:
  If (Err.Number = 6) Or (Err.Number = 11) Then
'    Errore = True
    PAR.Pic.Cls
'    Resume Next
  Else
    MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Sub: CalcoloParametri_PROFILO_PER_PUNTI_ESTERNI"
  End If
  
End Sub

Sub CalcoloParametri_PROFILO_PER_PUNTI_INTERNI()
'
Dim tmp     As Double
Dim i       As Integer
Dim ERRORE  As Boolean
'
On Error GoTo errCalcoloParametri_PROFILO_PER_PUNTI_INTERNI
  '
  ParametriOK = True
  Call InizializzaVariabili
  'Se il numero pezzi in rettifica � 1
  'azzero la distanza tra i pezzi
    'If (Lp("R[9]") <= 1) Then Call SP("R[133]", 0)
    Call SP("iCORSAIN", 0)  'Round(tmp + 0.05, 1)
    Call SP("iCORSAOUT", 0)  ' Round(tmp + 0.05, 1)
    Call SP("CORSAIN", Lp("iCORSAIN") + Lp("iCORSAIN_CORR"))
    Call SP("CORSAOUT", Lp("iCORSAOUT") + Lp("iCORSAOUT_CORR"))
    If (Lp("iMetDiv") = 1) Then Call SP("R[130]", 0)
    '
    Select Case Lp("iNZone")
      Case 1
        Call SP("Fascia2F1", 0)
        Call SP("Fascia2F2", 0)
        Call SP("Fascia3F1", 0)
        Call SP("Fascia3F2", 0)
      
        Call SP("Conic2F1", 0)
        Call SP("Conic2F2", 0)
        Call SP("Bomb2F1", 0)
        Call SP("Bomb2F2", 0)
        
        Call SP("Conic3F1", 0)
        Call SP("Conic3F2", 0)
        Call SP("Bomb3F1", 0)
        Call SP("Bomb3F2", 0)
  
      Case 2
        Call SP("Fascia3F1", 0)
        Call SP("Fascia3F2", 0)

        Call SP("Conic3F1", 0)
        Call SP("Conic3F2", 0)
        Call SP("Bomb3F1", 0)
        Call SP("Bomb3F2", 0)
      
      Case 3
        '
    End Select
    Call SP("FASCIATOTALE_G", Lp("Fascia1F1") + Lp("Fascia2F1") + Lp("Fascia3F1"))
    '
On Error GoTo 0
Exit Sub

errCalcoloParametri_PROFILO_PER_PUNTI_INTERNI:
  If (Err.Number = 6) Or (Err.Number = 11) Then
'    Errore = True
    PAR.Pic.Cls
'    Resume Next
  Else
    MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Sub: CalcoloParametri_PROFILO_PER_PUNTI_INTERNI"
  End If
  
End Sub

Sub CalcoloPuntiMola(Code, DATI As DatiAppoggio)

Dim PAS, i, Ldev, X, Y, Beta, ALFA, gama2, Ltang, Piano
Dim LtangMax, LtangMin
Dim npt

On Error GoTo CalcoloPuntiMola_Error
  
  npt = DATI.NpuntiMola + 1
  ReDim DATI.Raggio(npt)
  ReDim DATI.Xmol(npt)
  ReDim DATI.Rmol(npt)
  ReDim DATI.Lmol(npt)
  ReDim DATI.XLmol(npt)
  ReDim DATI.Tmol(npt)
  PAS = DGlob.DistPtEv
  DATI.LdevIni = DATI.LdevIni - PAS
  Ldev = DATI.LdevIni - PAS
  LtangMax = -1000: LtangMin = 1000
  For i = 1 To npt
    Ldev = Ldev + PAS
    DATI.Raggio(i) = Sqr((DATI.DB1 / 2) ^ 2 + Ldev ^ 2)
    Call PntDevelop(DATI.Eb1, DATI.DB1, DATI.Z1, DATI.Raggio(i) * 2, X, Y, Beta, "V")
    ALFA = Asn(X / DATI.Raggio(i))
    Call CalcoloPuntoMola(DATI.ENTRX, DATI.IncMola, DATI.PasHelImpostato, DATI.Raggio(i), ALFA, Beta, DATI.Xmol(i), DATI.Rmol(i), DATI.Tmol(i), Ltang)
    DATI.Lmol(i) = Ltang
    DATI.XLmol(i) = Ldev
    If Ltang > LtangMax Then LtangMax = Ltang
    If Ltang < LtangMin Then LtangMin = Ltang
    If DATI.Rmol(i) = 0 Then
      'MsgBox "Radicando negativi per R=" & Format(.Raggio(i), "###.###"), vbInformation
      Code = 1: LtangMax = 0: LtangMin = 0
      Exit For
    Else
      Code = 0
    End If
  Next i
  DATI.LcontattoMola = LtangMax - LtangMin
  DATI.LContMax = LtangMax
  DATI.LContMin = LtangMin

  On Error GoTo 0
  Exit Sub

CalcoloPuntiMola_Error:
  'MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Sub: CalcoloPuntiMola"
  
End Sub

Sub CalcoloPuntoMola(Ent, INC, PAS, Rayon, ALFA, Beta, X, Y, gama2, Ltang)
'
Dim Rmin, Tp, DRM, RMPD, SensALF, Alfa1, beta1, Rayon1
Dim Xml, Yml
Dim T0, FI0, CTF0, RQC, PSUDP2, AA, BB, cc
Dim RAD, T1, T2, Ti, TSI
Dim A, B, F, j, FP, TSIa, XS, Ys, Zs
Dim Ap, Bp, Cp, ArgSeno, Gama, RST
'
If (PAS < 9999) Then
   If (PAS = 0) Then
      X = Rayon * Sin(ALFA)
      Y = Ent - Rayon * Cos(ALFA)
      gama2 = Beta + ALFA
      Ltang = 0
      Exit Sub
   End If
   If (INC = 0) Then Exit Sub
   Rmin = PAS / ((2 * PI) / Tan(INC))
   Tp = PAS / (2 * PI)
   DRM = Ent * Rmin
   RMPD = Rmin + Ent
   SensALF = Sgn(ALFA): If SensALF = 0 Then SensALF = 1
   ALFA = ALFA * SensALF: Beta = Beta * SensALF
   If (Abs(Beta) > 0.8) Then
      Alfa1 = ALFA + 0.002
      beta1 = Beta - 0.002
      Rayon1 = Rayon * Sin(Beta) / Sin(beta1)
   Else
      Rayon1 = Rayon - 0.1
      beta1 = Asn(Rayon * Sin(Beta) / Rayon1)
      Alfa1 = ALFA - beta1 + Beta
   End If
   GoSub CalcoloPunto
   Xml = X: Yml = Y
   If (Yml <> 0) Then
      Alfa1 = ALFA: beta1 = Beta: Rayon1 = Rayon
      GoSub CalcoloPunto
      gama2 = Gama * Sgn((X - Xml) / (Yml - Y)) * SensALF
      X = X * SensALF
      ALFA = ALFA * SensALF: Beta = Beta * SensALF
   End If
   If (Y = 0) Then
      'MsgBox "Radicando negativi per R=" & Format(Rayon1, "###.###"), vbInformation
   End If
Else
      X = Rayon * Sin(ALFA): Y = Ent - Rayon * Cos(ALFA): gama2 = Beta + ALFA
      Ltang = 0
End If
Exit Sub

CalcoloPunto:
   T0 = PI / 2 - Alfa1: FI0 = PI / 2 - Alfa1 - beta1
   If FI0 > PI Then FI0 = FI0 - 2 * PI
   If FI0 < -PI Then FI0 = FI0 + 2 * PI
   CTF0 = Cos(T0 - FI0)
   RQC = Rayon1 * Rayon1 * CTF0: PSUDP2 = Tp * Tp
   AA = RQC * Sin(T0) + (2 * PSUDP2 + DRM) * Sin(FI0)
   BB = (DRM + PSUDP2) * Cos(FI0) + RQC * Cos(T0)
   cc = 2 * (-DRM * Sin(FI0) - RQC * Sin(T0) + Rayon1 * RMPD * CTF0)
   RAD = (BB * BB / AA - cc) / AA
   If RAD < 0 Then
      X = 0: Y = 0: Ltang = 0: gama2 = 0
      Return
   End If
   RAD = Sqr(RAD)
   T1 = BB / AA + RAD: T2 = BB / AA - RAD
   Ti = T2
   If Abs(T2) > Abs(T1) Then Ti = T1
   ' CALCOLO ZERO CON METODO APPROSSIMATO
   '   TETA *--> TS
   TSI = Ti
   ' TSI initial peut etre calcule avec la routine CalcoloTSIorigine.
   ' ( imperatif si Alfa + Beta > 180 ou < -180 )
   ' GOSUB CalcoloTSIorigine
   j = 1
1350
   A = FI0 + TSI: B = T0 + TSI
   F = RQC * Sin(B) + DRM * Sin(A) + PSUDP2 * TSI * Cos(A) - Rayon1 * RMPD * CTF0
   FP = RQC * Cos(B) + DRM * Cos(A) + PSUDP2 * Cos(A) - PSUDP2 * TSI * Sin(A)
1370
   TSIa = TSI - F / FP
   j = j + 1
   If Abs(TSIa - TSI) < 0.0002 Then GoTo 1430
   If j = 80 Then GoTo 1430
   TSI = TSIa
   GoTo 1350
1430
   Ti = TSIa: GoSub RESTO
   XS = Rayon1 * Cos(T0 + Ti): Ys = Rayon1 * Sin(T0 + Ti): Zs = Tp * Ti
   Ltang = Zs
   ' CALCOLO TANGENTE AL PROFILO MOLA IN TETAS
   Ap = Tan(Ti + FI0) / XS: Bp = -1 / XS: Cp = (1 + Ys * Tan(Ti + FI0) / XS) / Tp
   ArgSeno = (Ap / Tan(INC) + Cp) / (Sqr(Ap ^ 2 + Bp ^ 2 + Cp ^ 2) * (1 / Sin(INC)))
   Gama = PI / 2 - Asn(ArgSeno): If Gama > PI / 2 Then Gama = PI - Gama
   X = Zs * Sin(INC) + XS * Cos(INC)
   Y = Sqr((XS * Sin(INC) - Zs * Cos(INC)) ^ 2 + (Ys - Ent) ^ 2)
Return

RESTO:
   RST = RQC * Sin(T0 + Ti) + DRM * Sin(FI0 + Ti) + PSUDP2 * Ti * Cos(FI0 + Ti) - Rayon1 * RMPD * CTF0
Return

CalcoloTSIorigine:
   A = Atn(2 * PI * Rayon / PAS)
   TSI = Rayon * Sin(ALFA) * Cos(A) * Cos(A) / (PAS / 2 / PI)
Return

End Sub

'  CALCUL UN POINT DE LA DEVELOPPANTE
'  Donnees : Eb , Db , Z , Dx
'  Prof$ : "P" pour Pieno ou "V" pour Vano
Sub PntDevelop(Eb, DB, Z, dx, X, Y, Ap, Prof$)
'
Dim Aux
  '
  If dx < DB Then
    Aux = Eb / DB
    Ap = 0
  Else
    Ap = Acs(DB / dx)
    Aux = Eb / DB - inv(Ap)
  End If
  If Prof$ = "P" Then
    X = (dx * Sin(Aux)) / 2
    Y = (dx * Cos(Aux)) / 2
  Else
    X = (dx * Sin(PI / Z - Aux)) / 2
    Y = (dx * Cos(PI / Z - Aux)) / 2
  End If
  '
End Sub

Private Sub CALCOLO_RAGGIO_TERNA(PInX As Double, PInY As Double, _
                                  PMeX As Double, PMeY As Double, _
                                  PFiX As Double, PFiY As Double, _
                                  Xc As Double, YC As Double, R0 As Double)
Dim X1 As Double
Dim X2 As Double
Dim X3 As Double
Dim X4 As Double
Dim X5 As Double
Dim X8 As Double
Dim X9 As Double
Dim Y1 As Double
Dim Y2 As Double
Dim Y3 As Double
Dim Y4 As Double
Dim Y5 As Double
Dim Y8 As Double
Dim Y9 As Double
Dim M1 As Double
Dim M2 As Double
Dim Q1 As Double
Dim Q2 As Double

Dim A1 As Double
Dim A2 As Double

  'CALCOLO RAGGIO
  X1 = PInX: X3 = PMeX: X5 = PFiX
  Y1 = PInY: Y3 = PMeY: Y5 = PFiY
  If Y1 = Y3 Then Y1 = Y3 + 0.00001
  If Y5 = Y3 Then Y5 = Y3 + 0.00001
  M1 = -((X3 - X1) / (Y3 - Y1))
  M2 = -((X5 - X3) / (Y5 - Y3))
  X8 = (X1 + X3) / 2: Y8 = (Y1 + Y3) / 2
  X9 = (X5 + X3) / 2: Y9 = (Y5 + Y3) / 2
  Q1 = Y8 - M1 * X8: Q2 = Y9 - M2 * X9
  If M1 = M2 Then M1 = M1 + 0.00001
  Xc = (Q2 - Q1) / (M1 - M2): YC = M1 * Xc + Q1
  R0 = Sqr((Xc - X3) ^ 2 + (YC - Y3) ^ 2)
'CALCOLO ANGOLO AL CENTRO
  'Q1=quadrante I-2     Q2=quadrante I
  If PInX >= Xc And PInY >= YC Then Q1 = 1
  If PInX >= Xc And PInY < YC Then Q1 = 4
  If PInX < Xc And PInY > YC Then Q1 = 2
  If PInX < Xc And PInY < YC Then Q1 = 3
  If PFiX >= Xc And PFiY >= YC Then Q2 = 1
  If PFiX >= Xc And PFiY < YC Then Q2 = 4
  If PFiX < Xc And PFiY > YC Then Q2 = 2
  If PFiX < Xc And PFiY < YC Then Q2 = 3
  If PInX = Xc Then
    A1 = PG / 2
  Else
    A1 = Atn((PInY - YC) / (PInX - Xc))
  End If
  If Q1 = 3 Then A1 = A1 + PG
  If Q1 = 2 Then A1 = A1 + PG
  If Q1 = 4 Then A1 = A1 + 2 * PG
  If PFiX = Xc Then
    A2 = PG / 2
  Else
    A2 = Atn((PFiY - YC) / (PFiX - Xc))
  End If
  If Q2 = 3 Then A2 = A2 + PG
  If Q2 = 2 Then A2 = A2 + PG
  If Q2 = 4 Then A2 = A2 + 2 * PG
  If A1 < 0 Then A1 = A1 + 2 * PG
  If A2 < 0 Then A2 = A2 + 2 * PG
  A1 = A1 * 180 / PG: A2 = A2 * 180 / PG
  '
  If A1 > A2 Then R0 = -R0
  '

End Sub

'Private Sub ANGOLI_CENTRO(PInX As Double, PInY As Double, PFiX As Double, PFiY As Double, _
'                                  Xc As Double, YC As Double, R0 As Double, _
'                                  A1 As Double, A2 As Double)
'
'Dim Q1 As Double
'Dim Q2 As Double
'  If PInX >= Xc And PInY >= YC Then Q1 = 1
'  If PInX >= Xc And PInY < YC Then Q1 = 4
'  If PInX < Xc And PInY > YC Then Q1 = 2
'  If PInX < Xc And PInY < YC Then Q1 = 3
'  If PFiX >= Xc And PFiY >= YC Then Q2 = 1
'  If PFiX >= Xc And PFiY < YC Then Q2 = 4
'  If PFiX < Xc And PFiY > YC Then Q2 = 2
'  If PFiX < Xc And PFiY < YC Then Q2 = 3
'  If PInX = Xc Then
'    A1 = PG / 2
'  Else
'    A1 = Atn((PInY - YC) / (PInX - Xc))
'  End If
'  If Q1 = 3 Then A1 = A1 + PG
'  If Q1 = 2 Then A1 = A1 + PG
'  If Q1 = 4 Then A1 = A1 + 2 * PG
'  If PFiX = Xc Then
'    A2 = PG / 2
'  Else
'    A2 = Atn((PFiY - YC) / (PFiX - Xc))
'  End If
'  If Q2 = 3 Then A2 = A2 + PG
'  If Q2 = 2 Then A2 = A2 + PG
'  If Q2 = 4 Then A2 = A2 + 2 * PG
'  If A1 < 0 Then A1 = A1 + 2 * PG
'  If A2 < 0 Then A2 = A2 + 2 * PG
'
'End Sub

Private Function DIA2TIF(dx As Double, DB As Double) As Double

Dim tTX As Double
  
  tTX = ((dx / 2) ^ 2 - (DB / 2) ^ 2)
  If tTX > 0 Then
    DIA2TIF = Sqr(tTX)
  Else
    DIA2TIF = 0
  End If

End Function

Private Function TIF2DIA(TifX As Double, DB As Double) As Double

Dim tTX As Double
  
  tTX = Sqr((TifX) ^ 2 + (DB / 2) ^ 2) * 2
  TIF2DIA = tTX

End Function

Private Function R2G(Radianti As Double) As Double
  
  R2G = Radianti * 180 / PG

End Function

Private Function G2R(Gradi As Double) As Double
  
  G2R = Gradi * PG / 180

End Function

'---------------------------------------------------------------------------------------
' Procedura : AngSeg
' Data e ora: 01/09/2007 10:18
' Autore    : Fil
' Note      : NON AGGIUNGERE MSGBOX E RESUME NEXT
'             ALTRIMENTI SI PIANTA GAPP4.EXE
'---------------------------------------------------------------------------------------
Private Function AngSeg(X1 As Double, Y1 As Double, X2 As Double, Y2 As Double) As Double
'
Dim tang  As Double
Dim i     As Integer
Dim R     As Double
Dim tX    As Double
Dim tY    As Double
Dim DIST  As Double
  '
  If (X2 <> X1) Then
    tang = Atn((Y2 - Y1) / (X2 - X1))
  Else
    tang = PG / 2
  End If
  R = Sqr((X2 - X1) ^ 2 + (Y2 - Y1) ^ 2)
  tX = X1 + R * Cos(tang)
  tY = Y1 + R * Sin(tang)
  DIST = Sqr((X2 - tX) ^ 2 + (Y2 - tY) ^ 2)
  i = 1
  While (DIST > R / 10) And (i < 5)
    tang = tang + PG / 2
    tX = X1 + R * Cos(tang)
    tY = Y1 + R * Sin(tang)
    DIST = Sqr((X2 - tX) ^ 2 + (Y2 - tY) ^ 2)
    i = i + 1
  Wend
  AngSeg = tang
  '
End Function

Private Function RotPnt(ByVal X1 As Double, ByVal Y1 As Double, ByVal Xc As Double, ByVal YC As Double, ByVal ANGOLO As Double, XR As Double, YR As Double)
'
Dim A1 As Double
Dim A2 As Double
Dim R As Double
'
On Error Resume Next
  '
  A1 = AngSeg(X1, Y1, Xc, YC)
  A2 = A1 + ANGOLO
  R = Sqr((X1 - Xc) ^ 2 + (Y1 - YC) ^ 2)
  XR = Xc - R * Cos(A2)
  YR = YC - R * Sin(A2)

End Function

'*********************************************************************************************
'*********************************************************************************************
'************************************* ROUTINES GRAFICHE *************************************
'*********************************************************************************************
'*********************************************************************************************
Private Function CalcoloColore(StringheF1 As String, Optional StringheF2 As String = "", Optional Fianco As Integer = 1, Optional DeselColor As Long = 0) As Long
'
Dim tstr      As String
Dim Stringhe  As String
Dim tstr2()   As String
Dim i         As Integer
'
On Error Resume Next
  '
  tstr = VarCorr
  CalcoloColore = DeselColor
  '
  'Se sto stampando non evidenzio la parte di disegno
  If PAR.Printing Then Exit Function
  '
  Stringhe = IIf(Fianco = 1, StringheF1, StringheF2)
  If Stringhe <> "" Then
    tstr2 = Split(Stringhe, ";")
    For i = 0 To UBound(tstr2)
      If tstr2(i) = tstr Then
        CalcoloColore = vbRed
        Exit Function
      End If
    Next i
  End If
  '
End Function

Sub DefRegione(Pict As Object)
'
On Error Resume Next
  '
  PAR.RegioneX1 = Pict.ScaleLeft
  PAR.RegioneX2 = Pict.ScaleLeft + Pict.ScaleWidth
  PAR.RegioneY1 = Pict.ScaleTop
  PAR.RegioneY2 = Pict.ScaleTop + Pict.ScaleHeight

End Sub

Function InRegione(X As Double, Y As Double) As Boolean
'
On Error Resume Next
  '
  InRegione = True
  If X < PAR.RegioneX1 Then InRegione = False
  If X > PAR.RegioneX2 Then InRegione = False
  If Y < PAR.RegioneY1 Then InRegione = False
  If Y > PAR.RegioneY2 Then InRegione = False
  
End Function

Private Sub CalcolaParPic(ByRef PAR As ParPic, Optional Clear As Boolean = True, Optional Stampante As Boolean = False)
'
Dim tmp As Double
'
On Error Resume Next
  '
  If Stampante Then Set PAR.Pic = Printer
  '
  If (PAR.RegioneX1 = PAR.RegioneX2) Or (PAR.RegioneY1 = PAR.RegioneY2) Then Call DefRegione(PAR.Pic)
  PAR.Xc = (PAR.RegioneX2 + PAR.RegioneX1) / 2
  Select Case PAR.Allineamento
    Case 0 'Al centro
      PAR.YC = PAR.RegioneY1 + (PAR.RegioneY2 - PAR.RegioneY1) / 2
    Case 1 'In alto
      PAR.YC = PAR.RegioneY1 + (PAR.RegioneY2 - PAR.RegioneY1) / 4
    Case 2 'In basso
      PAR.YC = PAR.RegioneY1 + (PAR.RegioneY2 - PAR.RegioneY1) * 3 / 4
    Case 3 'In centro - basso
      PAR.YC = PAR.RegioneY1 + (PAR.RegioneY2 - PAR.RegioneY1) * 5 / 8
  End Select
  PAR.Scala = 0
  PAR.SpostX = 0
  PAR.SpostY = 0
  If (PAR.MaxX > PAR.MinX) Then
    PAR.Scala = (PAR.RegioneX2 - PAR.MargineDxPic - PAR.RegioneX1 - PAR.MargineSxPic) / (PAR.MaxX - PAR.MinX)
    If (PAR.ScalaMargine > 0) Then PAR.Scala = PAR.Scala * PAR.ScalaMargine
    PAR.SpostX = (PAR.MaxX + PAR.MinX) / 2
  End If
  If (PAR.MaxY > PAR.MinY) Then
    tmp = (PAR.RegioneY2 - PAR.RegioneY1) / (PAR.MaxY - PAR.MinY)
    If (tmp > 0) Then tmp = tmp * PAR.ScalaMargine
    PAR.SpostY = (PAR.MaxY + PAR.MinY) / 2
    If (tmp < PAR.Scala) Then PAR.Scala = tmp
  End If
  If (PAR.Scala = 0) Then PAR.Scala = 1
  '
  If Clear Then
    PAR.Pic.Cls
    PAR.Pic.BackColor = vbWhite
    PAR.NInfo = 0
    PAR.NInfoAll(1) = 0
    PAR.NInfoAll(2) = 0
    PAR.NInfoAll(3) = 0
    PAR.NInfoAll(4) = 0
    If Stampante Then
      PAR.Pic.Line (PAR.RegioneX1, PAR.RegioneY1)-(PAR.RegioneX1, PAR.RegioneY2)
      PAR.Pic.Line (PAR.RegioneX1, PAR.RegioneY2)-(PAR.RegioneX2, PAR.RegioneY2)
      PAR.Pic.Line (PAR.RegioneX2, PAR.RegioneY2)-(PAR.RegioneX2, PAR.RegioneY1)
      PAR.Pic.Line (PAR.RegioneX2, PAR.RegioneY1)-(PAR.RegioneX1, PAR.RegioneY1)
    Else
      PAR.Pic.BackColor = vbWhite
    End If
  End If
  '
End Sub

''---------------------------------------------------------------------------------------
'' Procedura : AddArcoInSerie
'' Data e ora: 24/08/2007 11:13
'' Autore    : Fil
'' Note      :
''---------------------------------------------------------------------------------------
'
'Private Sub AddArcoInSerie(ByRef Sx() As Double, ByRef Sy() As Double, Xc As Double, YC As Double, R As Double, X1 As Double, Y1 As Double, X2 As Double, Y2 As Double, Optional Antiorario As Boolean = False)
''
'Dim A1 As Double
'Dim A2 As Double
'Dim Tp As Double
'Dim Xp As Double
'Dim Yp As Double
'Dim i As Integer
'Dim npt As Integer
'Dim Delta As Double
'Dim Err As Double
''
'On Error GoTo AddArcoInSerie_Error
'  '
'  npt = 10
'  If X1 = Xc Then
'    A1 = PG / 2
'  Else
'    A1 = Atn((Y1 - YC) / (X1 - Xc))
'  End If
'  Xp = Xc + R * Cos(A1)
'  Yp = YC + R * Sin(A1)
'  Err = Sqr((X1 - Xp) ^ 2 + (Y1 - Yp) ^ 2)
'
'  Delta = 0
'  For i = 1 To 3
'    Xp = Xc + R * Cos(A1 + i * (PG / 2))
'    Yp = YC + R * Sin(A1 + i * (PG / 2))
'    If Err > Sqr((X1 - Xp) ^ 2 + (Y1 - Yp) ^ 2) Then
'      Delta = i * (PG / 2)
'      Err = Sqr((X1 - Xp) ^ 2 + (Y1 - Yp) ^ 2)
'    End If
'  Next i
'  A1 = A1 + Delta
'
'  If X2 = Xc Then
'    A2 = PG / 2
'  Else
'    A2 = Atn((Y2 - YC) / (X2 - Xc))
'  End If
'
'  Xp = Xc + R * Cos(A2)
'  Yp = YC + R * Sin(A2)
'  Err = Sqr((X2 - Xp) ^ 2 + (Y2 - Yp) ^ 2)
'
'  Delta = 0
'  For i = 1 To 3
'    Xp = Xc + R * Cos(A2 + i * (PG / 2))
'    Yp = YC + R * Sin(A2 + i * (PG / 2))
'    If Err > Sqr((X2 - Xp) ^ 2 + (Y2 - Yp) ^ 2) Then
'      Delta = i * (PG / 2)
'      Err = Sqr((X2 - Xp) ^ 2 + (Y2 - Yp) ^ 2)
'    End If
'  Next i
'  A2 = A2 + Delta
'
'  Tp = -Abs(A2 - A1) / (npt - 1)
'  If Antiorario Then
'    Tp = -Tp
'  End If
'
'  For i = 1 To npt
'    Xp = Xc + R * Cos(A1 + Tp * (i - 1))
'    Yp = YC + R * Sin(A1 + Tp * (i - 1))
'    If SafeUBound(Sx) = -1 Then
'      ReDim Sx(0)
'      ReDim Sy(0)
'    Else
'      ReDim Preserve Sx(UBound(Sx) + 1)
'      ReDim Preserve Sy(UBound(Sy) + 1)
'    End If
'    Sx(UBound(Sx)) = Xp
'    Sy(UBound(Sy)) = Yp
'
'  Next i
'
'  On Error GoTo 0
'  Exit Sub
'
'AddArcoInSerie_Error:
'  MsgBox "Error " '& err.Number & " (" & err.Description & ") in Sub: AddArcoInSerie"
'  Resume Next
'
'End Sub

Private Sub DISEGNA_PUNTO(X As Double, Y As Double, Optional COLORE As Long = vbBlack)
  
On Error Resume Next

  PAR.Pic.Circle (cx(X), cy(Y)), 0.04 * PAR.Scala, COLORE

End Sub
Private Sub Disegna_Arco_V(Xc As Double, YC As Double, R As Double, _
                        X1 As Double, Y1 As Double, X2 As Double, Y2 As Double, _
                        Optional Antiorario As Boolean = False, Optional NumPunti As Integer = 10, _
                        Optional ScalaX As Double = 1, Optional COLORE As Long = 0, _
                        Optional Spessore As Integer = 1, Optional ScalaY As Double = 1)
'
Dim A1    As Double
Dim A2    As Double
Dim Tp    As Double
Dim Xp    As Double
Dim Yp    As Double
Dim Xq    As Double
Dim Yq    As Double
Dim li_m  As Double
Dim li_q  As Double
Dim li_x  As Double
Dim i     As Integer
Dim npt   As Integer
Dim Delta As Double
Dim Err   As Double
'
On Error GoTo errDisegna_Arco_V
  '
  If PAR.AllBlack Then COLORE = vbBlack
  '
  npt = NumPunti
  '
  If (X1 = X2) And (X2 = Y1) And (Y1 = Y2) Then
     A1 = 0: A2 = 2 * PG
  Else
    If (X1 = Xc) Then
      A1 = PG / 2
    Else
      A1 = Atn((Y1 - YC) / (X1 - Xc))
    End If
    Xp = Xc + R * Cos(A1)
    Yp = YC + R * Sin(A1)
    Err = Sqr((X1 - Xp) ^ 2 + (Y1 - Yp) ^ 2)
    
    Delta = 0
    For i = 1 To 3
      Xp = Xc + R * Cos(A1 + i * (PG / 2))
      Yp = YC + R * Sin(A1 + i * (PG / 2))
      If (Err > Sqr((X1 - Xp) ^ 2 + (Y1 - Yp) ^ 2)) Then
        Delta = i * (PG / 2)
        Err = Sqr((X1 - Xp) ^ 2 + (Y1 - Yp) ^ 2)
      End If
    Next i
    A1 = A1 + Delta
    
    If (X2 = Xc) Then A2 = PG / 2 Else A2 = Atn((Y2 - YC) / (X2 - Xc))
    
    Xp = Xc + R * Cos(A2)
    Yp = YC + R * Sin(A2)
    Err = Sqr((X2 - Xp) ^ 2 + (Y2 - Yp) ^ 2)
    
    Delta = 0
    For i = 1 To 3
      Xp = Xc + R * Cos(A2 + i * (PG / 2))
      Yp = YC + R * Sin(A2 + i * (PG / 2))
      If (Err > Sqr((X2 - Xp) ^ 2 + (Y2 - Yp) ^ 2)) Then
        Delta = i * (PG / 2)
        Err = Sqr((X2 - Xp) ^ 2 + (Y2 - Yp) ^ 2)
      End If
    Next i
    A2 = A2 + Delta
  End If
  
  If (A1 < 0) Then A1 = 2 * PG + A1
  If (A2 < 0) Then A2 = 2 * PG + A2
  
  Tp = -Abs(A2 - A1) / (npt - 1)
  If Antiorario Then Tp = -Tp
  '
  'SVG240111
  'ALTRIMENTI NON DISEGNA BOMBATURE PIU' PICCOLE
  'If Abs(R) > 30000 Then Exit Sub
  '
  Xp = Xc + R * Cos(A1)
  Yp = YC + R * Sin(A1)
  For i = 2 To npt
'    Xq = XC + R * Cos(A1 + Tp * (i - 1))
'    Yq = YC + R * Sin(A1 + Tp * (i - 1))
    Xq = Xc + R * Cos(A1 + Tp * (i - 1))
    Yq = YC + R * Sin(A1 + Tp * (i - 1))
    
    'Call lineaV(Xp * ScalaX, Yp * ScalaY, Xq * ScalaX, Yq * ScalaY, COLORE, Spessore)
    Call lineaV(Yp * ScalaY, Xp * ScalaX, Yq * ScalaY, Xq * ScalaX, COLORE, Spessore)
    Xp = Xq
    Yp = Yq
  Next i

Exit Sub

errDisegna_Arco_V:
 ' MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Sub: Disegna_Arco_V"
  Exit Sub
  'Resume Next

End Sub



Private Sub Disegna_Arco(Xc As Double, YC As Double, R As Double, _
                        X1 As Double, Y1 As Double, X2 As Double, Y2 As Double, _
                        Optional Antiorario As Boolean = False, Optional NumPunti As Integer = 10, _
                        Optional ScalaX As Double = 1, Optional COLORE As Long = 0, _
                        Optional Spessore As Integer = 1, Optional ScalaY As Double = 1)
'
Dim A1    As Double
Dim A2    As Double
Dim Tp    As Double
Dim Xp    As Double
Dim Yp    As Double
Dim Xq    As Double
Dim Yq    As Double
Dim li_m  As Double
Dim li_q  As Double
Dim li_x  As Double
Dim i     As Integer
Dim npt   As Integer
Dim Delta As Double
Dim Err   As Double
'
On Error GoTo errDisegna_Arco
  '
  If PAR.AllBlack Then COLORE = vbBlack
  '
  npt = NumPunti
  '
  If (X1 = X2) And (X2 = Y1) And (Y1 = Y2) Then
     A1 = 0: A2 = 2 * PG
  Else
    If (X1 = Xc) Then
      A1 = PG / 2
    Else
      A1 = Atn((Y1 - YC) / (X1 - Xc))
    End If
    Xp = Xc + R * Cos(A1)
    Yp = YC + R * Sin(A1)
    Err = Sqr((X1 - Xp) ^ 2 + (Y1 - Yp) ^ 2)
    
    Delta = 0
    For i = 1 To 3
      Xp = Xc + R * Cos(A1 + i * (PG / 2))
      Yp = YC + R * Sin(A1 + i * (PG / 2))
      If (Err > Sqr((X1 - Xp) ^ 2 + (Y1 - Yp) ^ 2)) Then
        Delta = i * (PG / 2)
        Err = Sqr((X1 - Xp) ^ 2 + (Y1 - Yp) ^ 2)
      End If
    Next i
    A1 = A1 + Delta
    
    If (X2 = Xc) Then A2 = PG / 2 Else A2 = Atn((Y2 - YC) / (X2 - Xc))
    
    Xp = Xc + R * Cos(A2)
    Yp = YC + R * Sin(A2)
    Err = Sqr((X2 - Xp) ^ 2 + (Y2 - Yp) ^ 2)
    
    Delta = 0
    For i = 1 To 3
      Xp = Xc + R * Cos(A2 + i * (PG / 2))
      Yp = YC + R * Sin(A2 + i * (PG / 2))
      If (Err > Sqr((X2 - Xp) ^ 2 + (Y2 - Yp) ^ 2)) Then
        Delta = i * (PG / 2)
        Err = Sqr((X2 - Xp) ^ 2 + (Y2 - Yp) ^ 2)
      End If
    Next i
    A2 = A2 + Delta
  End If
  
  If (A1 < 0) Then A1 = 2 * PG + A1
  If (A2 < 0) Then A2 = 2 * PG + A2
  
  Tp = -Abs(A2 - A1) / (npt - 1)
  If Antiorario Then Tp = -Tp
  '
  'SVG240111
  'ALTRIMENTI NON DISEGNA BOMBATURE PIU' PICCOLE
  'If Abs(R) > 30000 Then Exit Sub
  '
  Xp = Xc + R * Cos(A1)
  Yp = YC + R * Sin(A1)
  For i = 2 To npt
    Xq = Xc + R * Cos(A1 + Tp * (i - 1))
    Yq = YC + R * Sin(A1 + Tp * (i - 1))
    Call linea(Xp * ScalaX, Yp * ScalaY, Xq * ScalaX, Yq * ScalaY, COLORE, Spessore)
    'Call lineaV(Xp * ScalaX, Yp * ScalaY, Xq * ScalaX, Yq * ScalaY, COLORE, Spessore)
    Xp = Xq
    Yp = Yq
  Next i

Exit Sub

errDisegna_Arco:
 ' MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Sub: Disegna_Arco"
  Exit Sub
  'Resume Next
  
End Sub

'Private Sub Disegna_Serie(X() As Double, Y() As Double, C() As Long)
'
'Dim Xmin  As Double
'Dim Xmax  As Double
'Dim Ymin  As Double
'Dim Ymax  As Double
'Dim Scala As Double
'Dim Xc    As Double
'Dim YC    As Double
'Dim i     As Integer
'Dim npt   As Integer
'Dim Hpic  As Double
'Dim Lpic  As Double
''
'On Error Resume Next
'  '
'  If (UBound(X) < UBound(Y)) Then Exit Sub
'  npt = UBound(X)
'  '
'  Hpic = PAR.Pic.ScaleHeight
'  Lpic = PAR.Pic.ScaleWidth
'  YC = Hpic / 2
'  Xc = Lpic / 2
'  '
'  Xmin = X(0): Ymin = Y(0)
'  Xmax = X(0): Ymax = Y(0)
'  '
'  For i = 0 To npt
'    If X(i) < Xmin Then Xmin = X(i)
'    If Y(i) < Ymin Then Ymin = Y(i)
'    If X(i) > Xmax Then Xmax = X(i)
'    If Y(i) > Ymax Then Ymax = Y(i)
'  Next i
'  '
'  Scala = Lpic / (Xmax - Xmin) * 0.9
'  YC = YC + (Ymax + Ymin) * Scala / 2
'  PAR.Pic.DrawWidth = 1.5
'  PAR.Pic.CurrentX = Xc + X(0) * Scala
'  PAR.Pic.CurrentY = YC - Y(0) * Scala
'  For i = 1 To npt
'    PAR.Pic.Line -(Xc + X(i) * Scala, YC - Y(i) * Scala), C(i)
'  Next
'
'End Sub

Private Sub Disegna_Bombatura(ByVal X1 As Double, ByVal Y1 As Double, ByVal X2 As Double, ByVal Y2 As Double, _
                              B As Double, ScalaX As Double, _
                              Optional COLORE As Long = 0, Optional Spessore As Integer = 1)
Dim L     As Double
Dim R     As Double
Dim Ang   As Double
Dim ALFA  As Double
Dim Beta  As Double
Dim Xb    As Double
Dim Yb    As Double
Dim Xm    As Double
Dim Ym    As Double
Dim Xc    As Double
Dim YC    As Double
'
On Error Resume Next
  '
  If (B = 0) Then Call linea(X1, Y1, X2, Y2, COLORE, Spessore): Exit Sub
  X1 = X1 / ScalaX
  X2 = X2 / ScalaX
  'Mezza fascia
  L = (Sqr((X2 - X1) ^ 2 + (Y2 - Y1) ^ 2)) / 2
  'Raggio
  R = (B ^ 2 + L ^ 2) / (2 * B)
  ALFA = AngSeg(X1, Y1, X2, Y2)
  Beta = PG / 2 - ALFA
  Xm = (X1 + X2) / 2
  Ym = (Y1 + Y2) / 2
  Xb = Xm - B * Cos(Beta)
  Yb = Ym + B * Sin(Beta)
  Call CALCOLO_RAGGIO_TERNA(X1, Y1, Xb, Yb, X2, Y2, Xc, YC, R)
  If (Abs(R) < Abs(B)) Then Exit Sub
  If (B < 0) Then
    Call Disegna_Arco(Xc, YC, R, X1, Y1, X2, Y2, B < 0, 20, ScalaX, COLORE, Spessore)
  Else
    Call Disegna_Arco(Xc, YC, -R, X1, Y1, X2, Y2, B < 0, 20, ScalaX, COLORE, Spessore)
  End If
  '
End Sub
Private Sub Disegna_BombaturaElica_V(ByVal X1 As Double, ByVal Y1 As Double, ByVal X2 As Double, ByVal Y2 As Double, _
                              B As Double, ScalaY As Double, _
                              Optional COLORE As Long = 0, Optional Spessore As Integer = 1)
                              
'
Dim L     As Double
Dim R     As Double
Dim Ang   As Double
Dim Xb    As Double
Dim Yb    As Double
Dim Xm    As Double
Dim Ym    As Double
Dim Xc    As Double
Dim YC    As Double
Dim sXb   As Double
Dim sYb   As Double
Dim Sca_X1  As Double
Dim Sca_X2  As Double

'
On Error Resume Next
  '
  If (B = 0) Then Call linea(X1, Y1, X2, Y2, COLORE, Spessore): Exit Sub
  
  'Y1 = Y1 / ScalaY
  'Y2 = Y2 / ScalaY
  Sca_X1 = X1 / ScalaY
  Sca_X2 = X2 / ScalaY
  
  'Mezza fascia
  'L = (Sqr((X2 - X1) ^ 2 + (Y2 - Y1) ^ 2)) / 2
  L = (Sqr((Y2 - Y1) ^ 2 + (Sca_X2 - Sca_X1) ^ 2)) / 2
  
  'Raggio
  R = (B ^ 2 + L ^ 2) / (2 * B)
  'Ang = AngSeg(X1, Y1, X2, Y2)
  Ang = AngSeg(Y1, Sca_X1, Y2, Sca_X2)
  
  Ang = Ang + PG / 2
  Ym = (Sca_X1 + Sca_X2) / 2
  Xm = (Y1 + Y2) / 2
  Xb = Xm + B * Cos(Ang)
  Yb = Ym - B * Sin(Ang)
  Call CALCOLO_RAGGIO_TERNA(Y1, Sca_X1, Xb, Yb, Y2, Sca_X2, Xc, YC, R)
  If (Abs(R) < Abs(B)) Then Exit Sub
  
  'Call DISEGNA_PUNTO(Xb, Yb)
  'cx(Xb), cy(Yb)
  
  sXb = PAR.Xc + (Xb - PAR.SpostX) * PAR.Scala
  sYb = PAR.YC - (Yb - PAR.SpostY) * PAR.Scala
  
  PAR.Pic.Circle (sXb, sYb), 0.04 * PAR.Scala, vbBlack
  
  'Call Disegna_Arco_V(XC, YC, R, X1, Y1, X2, Y2, B < 0, 20, 1, COLORE, Spessore, ScalaY)
  Call Disegna_Arco_V(Xc, YC, R, Y1, Sca_X1, Y2, Sca_X2, B > 0, 20, 1, COLORE, Spessore, ScalaY)
                              
                              
End Sub


Private Sub Disegna_BombaturaElica(ByVal X1 As Double, ByVal Y1 As Double, ByVal X2 As Double, ByVal Y2 As Double, _
                              B As Double, ScalaY As Double, _
                              Optional COLORE As Long = 0, Optional Spessore As Integer = 1)
'
Dim L     As Double
Dim R     As Double
Dim Ang   As Double
Dim Xb    As Double
Dim Yb    As Double
Dim Xm    As Double
Dim Ym    As Double
Dim Xc    As Double
Dim YC    As Double
'
On Error Resume Next
  '
  If (B = 0) Then Call linea(X1, Y1, X2, Y2, COLORE, Spessore): Exit Sub
  Y1 = Y1 / ScalaY
  Y2 = Y2 / ScalaY
    
  'Mezza fascia
  L = (Sqr((X2 - X1) ^ 2 + (Y2 - Y1) ^ 2)) / 2
  'Raggio
  R = (B ^ 2 + L ^ 2) / (2 * B)
  Ang = AngSeg(X1, Y1, X2, Y2)
  Ang = Ang + PG / 2
  Xm = (X1 + X2) / 2
  Ym = (Y1 + Y2) / 2
  Xb = Xm + B * Cos(Ang)
  Yb = Ym + B * Sin(Ang)
  Call CALCOLO_RAGGIO_TERNA(X1, Y1, Xb, Yb, X2, Y2, Xc, YC, R)
  If (Abs(R) < Abs(B)) Then Exit Sub
  Call DISEGNA_PUNTO(Xb, Yb)
  Call Disegna_Arco(Xc, YC, R, X1, Y1, X2, Y2, B < 0, 20, 1, COLORE, Spessore, ScalaY)
  '
End Sub

Private Function SafeUBound(v As Variant)
'
On Error Resume Next
  '
  SafeUBound = -1 '' in caso di errore
  SafeUBound = UBound(v)
  '
End Function

'Private Sub TestoInfo2(TESTO As String, Optional AllDestra As Boolean = False, Optional Grassetto As Boolean = False, Optional COLORE As Long = vbBlack, Optional Negativo = False)
'
'Dim tmpcol    As OLE_COLOR
'Dim tmpbkcol  As OLE_COLOR
'Dim tBol      As Boolean
''
'On Error Resume Next
'  '
'  TESTO = Replace(TESTO, ",", ".")
'  PAR.NInfo = PAR.NInfo + 1
'  tmpcol = PAR.Pic.ForeColor
'  tBol = PAR.Pic.FontBold
'  PAR.Pic.FontBold = Grassetto
'  '
'  If Negativo Then
'    PAR.Pic.ForeColor = PAR.Pic.BackColor
'    tmpbkcol = COLORE
'  Else
'    PAR.Pic.ForeColor = COLORE
'    tmpbkcol = PAR.Pic.BackColor
'  End If
'  '
'  If AllDestra Then
'    PAR.Pic.Line (PAR.RegioneX2 - PAR.Pic.TextWidth(TESTO) - PAR.Pic.TextWidth("0"), PAR.RegioneY1 + PAR.Pic.TextHeight("0") * (PAR.NInfo - 0.7))-(PAR.RegioneX2 - PAR.Pic.TextWidth("0") * 0.25, PAR.RegioneY1 + PAR.Pic.TextHeight("0") * (PAR.NInfo + 0.15)), tmpbkcol, BF
'    PAR.Pic.CurrentX = PAR.RegioneX2 - PAR.Pic.TextWidth(TESTO) - PAR.Pic.TextWidth("0") * 0.5
'  Else
'    PAR.Pic.Line (PAR.RegioneX1 + PAR.Pic.TextWidth("0") * 0.25, PAR.RegioneY1 + PAR.Pic.TextHeight("0") * (PAR.NInfo - 0.7))-(PAR.RegioneX1 + PAR.Pic.TextWidth(TESTO) + PAR.Pic.TextWidth("0") * 0.45, PAR.RegioneY1 + PAR.Pic.TextHeight("0") * (PAR.NInfo + 0.15)), tmpbkcol, BF 'par.Pic.BackColor, BF
'
'    PAR.Pic.CurrentX = PAR.RegioneX1 + PAR.Pic.TextWidth("0") * 0.5
'  End If
'  PAR.Pic.CurrentY = PAR.RegioneY1 + PAR.Pic.TextHeight("0") * (PAR.NInfo - 0.8)
'  '
'  PAR.Pic.Print TESTO
'  PAR.Pic.ForeColor = tmpcol
'  PAR.Pic.FontBold = tBol
'
'End Sub

Private Sub TestoInfo(TESTO As String, Optional Allineamento As Integer = 1, Optional Grassetto As Boolean = False, Optional COLORE As Long = vbBlack, Optional Negativo = False)
'
Dim tmpcol    As OLE_COLOR
Dim tmpbkcol  As OLE_COLOR
Dim tBol      As Boolean
'
On Error Resume Next
  '
  TESTO = Replace(TESTO, ",", ".")
  PAR.NInfoAll(Allineamento) = PAR.NInfoAll(Allineamento) + 1
  tmpcol = PAR.Pic.ForeColor
  tBol = PAR.Pic.FontBold
  PAR.Pic.FontBold = Grassetto
  '
  If Negativo Then
    If PAR.Pic Is Printer Then
      PAR.Pic.ForeColor = vbBlack ' vbWhite
      tmpbkcol = vbWhite
    Else
      PAR.Pic.ForeColor = PAR.Pic.BackColor
      tmpbkcol = COLORE
    End If
  Else
    PAR.Pic.ForeColor = COLORE
    If PAR.Pic Is Printer Then
      tmpbkcol = vbWhite
    Else
      tmpbkcol = PAR.Pic.BackColor
    End If
  End If
  '
  Select Case Allineamento
    Case 1 'Alto sx
      PAR.Pic.Line (PAR.RegioneX1 + PAR.Pic.TextWidth("0") * 0.25, PAR.RegioneY1 + PAR.Pic.TextHeight("0") * (PAR.NInfoAll(Allineamento) - 0.7))-(PAR.RegioneX1 + PAR.Pic.TextWidth(TESTO) + PAR.Pic.TextWidth("0") * 0.45, PAR.RegioneY1 + PAR.Pic.TextHeight("0") * (PAR.NInfoAll(Allineamento) + 0.15)), tmpbkcol, BF 'par.Pic.BackColor, BF
      PAR.Pic.CurrentX = PAR.RegioneX1 + PAR.Pic.TextWidth("0") * 0.5
      PAR.Pic.CurrentY = PAR.RegioneY1 + PAR.Pic.TextHeight("0") * (PAR.NInfoAll(Allineamento) - 0.8)
    Case 2 'Alto dx
      PAR.Pic.Line (PAR.RegioneX2 - PAR.Pic.TextWidth(TESTO) - PAR.Pic.TextWidth("0"), PAR.RegioneY1 + PAR.Pic.TextHeight("0") * (PAR.NInfoAll(Allineamento) - 0.7))-(PAR.RegioneX2 - PAR.Pic.TextWidth("0") * 0.25, PAR.RegioneY1 + PAR.Pic.TextHeight("0") * (PAR.NInfoAll(Allineamento) + 0.15)), tmpbkcol, BF
      PAR.Pic.CurrentX = PAR.RegioneX2 - PAR.Pic.TextWidth(TESTO) - PAR.Pic.TextWidth("0") * 0.5
      PAR.Pic.CurrentY = PAR.RegioneY1 + PAR.Pic.TextHeight("0") * (PAR.NInfoAll(Allineamento) - 0.8)
    Case 3 'Basso dx
      PAR.Pic.Line (PAR.RegioneX2 - PAR.Pic.TextWidth(TESTO) - PAR.Pic.TextWidth("0"), PAR.RegioneY2 - PAR.Pic.TextHeight("0") * (PAR.NInfoAll(Allineamento) - 0.7))-(PAR.RegioneX2 - PAR.Pic.TextWidth("0") * 0.25, PAR.RegioneY2 - PAR.Pic.TextHeight("0") * (PAR.NInfoAll(Allineamento) + 0.15)), tmpbkcol, BF
      PAR.Pic.CurrentX = PAR.RegioneX2 - PAR.Pic.TextWidth(TESTO) - PAR.Pic.TextWidth("0") * 0.5
      PAR.Pic.CurrentY = PAR.RegioneY2 - PAR.Pic.TextHeight("0") * (PAR.NInfoAll(Allineamento) + 0.2)
    Case 4 'Basso sx
      PAR.Pic.Line (PAR.RegioneX1 + PAR.Pic.TextWidth("0") * 0.25, PAR.RegioneY2 - PAR.Pic.TextHeight("0") * (PAR.NInfoAll(Allineamento) - 0.7))-(PAR.RegioneX1 + PAR.Pic.TextWidth(TESTO) + PAR.Pic.TextWidth("0") * 0.45, PAR.RegioneY2 - PAR.Pic.TextHeight("0") * (PAR.NInfoAll(Allineamento) + 0.15)), tmpbkcol, BF 'par.Pic.BackColor, BF
      PAR.Pic.CurrentX = PAR.RegioneX1 + PAR.Pic.TextWidth("0") * 0.5
      PAR.Pic.CurrentY = PAR.RegioneY2 - PAR.Pic.TextHeight("0") * (PAR.NInfoAll(Allineamento) + 0.2)
  
  End Select
  '
  PAR.Pic.Print TESTO
  PAR.Pic.ForeColor = tmpcol
  PAR.Pic.FontBold = tBol
  
End Sub

Private Sub TestoAll_V(X1 As Double, Y1 As Double, TESTO As String, Optional COLORE As Long = vbBlack, Optional Grassetto As Boolean = False, Optional Allineamento As Integer = 1, Optional ByVal ValB As Double)

Dim tmpcol  As OLE_COLOR
Dim CuX     As Double
Dim CuY     As Double
Dim tBol    As Boolean
'
On Error Resume Next
  '
  TESTO = Replace(TESTO, ",", ".")
  If PAR.AllBlack Then COLORE = vbBlack
  '
  tmpcol = PAR.Pic.ForeColor
  tBol = PAR.Pic.FontBold
  PAR.Pic.ForeColor = COLORE
  PAR.Pic.FontBold = Grassetto
  '
  CuX = PAR.Pic.CurrentX
  CuY = PAR.Pic.CurrentY
  '
  Select Case Allineamento
    Case 1 'Dx
      PAR.Pic.CurrentX = cx(X1) + PAR.Pic.TextWidth(TESTO & " ")
      PAR.Pic.CurrentY = cy(Y1) - PAR.Pic.TextHeight(TESTO) / 2
    Case 2 'Sx
      PAR.Pic.CurrentX = cx(X1) - PAR.Pic.TextWidth(" ")
      PAR.Pic.CurrentY = cy(Y1) - PAR.Pic.TextHeight(TESTO) / 2
    Case 3 'Sopra al centro
      PAR.Pic.CurrentX = cx(X1) - PAR.Pic.TextWidth(TESTO) / 2
      PAR.Pic.CurrentY = cy(Y1) - PAR.Pic.TextHeight(TESTO) * 1.1
    Case 4 'Sotto al centro
      PAR.Pic.CurrentX = cx(X1) - PAR.Pic.TextWidth(TESTO) / 2
      PAR.Pic.CurrentY = cy(Y1) + PAR.Pic.TextHeight(TESTO) * 0.2
    Case 5 'Al centro
      PAR.Pic.CurrentX = cx(X1) - PAR.Pic.TextWidth(TESTO & " ") / 2
      PAR.Pic.CurrentY = cy(Y1) - PAR.Pic.TextHeight(TESTO) / 2
    Case 6 'Al centro Sx
      PAR.Pic.CurrentX = cx(X1) - PAR.Pic.TextWidth(" ")
      PAR.Pic.CurrentY = cy(Y1) - PAR.Pic.TextHeight(TESTO) / 2
    Case 7 'Al centro Dx
      PAR.Pic.CurrentX = cx(X1) + PAR.Pic.TextWidth(" ")
      PAR.Pic.CurrentY = cy(Y1) - PAR.Pic.TextHeight(TESTO) / 2
    Case 8 'Al centro Sx *2
      PAR.Pic.CurrentX = cx(X1) - PAR.Pic.TextWidth(" ") * 4 - ValB * 24
      PAR.Pic.CurrentY = cy(Y1) - PAR.Pic.TextHeight(TESTO) / 2
    Case 9 'Al centro Dx *2
      PAR.Pic.CurrentX = cx(X1) + PAR.Pic.TextWidth(" ") * 2 + ValB * 24
      PAR.Pic.CurrentY = cy(Y1) - PAR.Pic.TextHeight(TESTO) / 2
  
  End Select
  '
  PAR.Pic.Print TESTO
  '
  PAR.Pic.CurrentX = CuX
  PAR.Pic.CurrentY = CuY
  PAR.Pic.ForeColor = tmpcol
  PAR.Pic.FontBold = tBol

End Sub


Private Sub TestoAll(X1 As Double, Y1 As Double, TESTO As String, Optional COLORE As Long = vbBlack, Optional Grassetto As Boolean = False, Optional Allineamento As Integer = 1)
'
Dim tmpcol  As OLE_COLOR
Dim CuX     As Double
Dim CuY     As Double
Dim tBol    As Boolean
'
On Error Resume Next
  '
  TESTO = Replace(TESTO, ",", ".")
  If PAR.AllBlack Then COLORE = vbBlack
  '
  tmpcol = PAR.Pic.ForeColor
  tBol = PAR.Pic.FontBold
  PAR.Pic.ForeColor = COLORE
  PAR.Pic.FontBold = Grassetto
  '
  CuX = PAR.Pic.CurrentX
  CuY = PAR.Pic.CurrentY
  '
  Select Case Allineamento
    Case 1 'Dx
      PAR.Pic.CurrentX = cx(X1) - PAR.Pic.TextWidth(TESTO & " ")
      PAR.Pic.CurrentY = cy(Y1) - PAR.Pic.TextHeight(TESTO) / 2
    Case 2 'Sx
      PAR.Pic.CurrentX = cx(X1) + PAR.Pic.TextWidth(" ")
      PAR.Pic.CurrentY = cy(Y1) - PAR.Pic.TextHeight(TESTO) / 2
    Case 3 'Sopra al centro
      PAR.Pic.CurrentX = cx(X1) - PAR.Pic.TextWidth(TESTO) / 2
      PAR.Pic.CurrentY = cy(Y1) - PAR.Pic.TextHeight(TESTO) * 1.1
    Case 4 'Sotto al centro
      PAR.Pic.CurrentX = cx(X1) - PAR.Pic.TextWidth(TESTO) / 2
      PAR.Pic.CurrentY = cy(Y1) + PAR.Pic.TextHeight(TESTO) * 0.2
    Case 5 'Al centro
      PAR.Pic.CurrentX = cx(X1) - PAR.Pic.TextWidth(TESTO & " ") / 2
      PAR.Pic.CurrentY = cy(Y1) - PAR.Pic.TextHeight(TESTO) / 2
    Case 6 'Al centro Sx
      PAR.Pic.CurrentX = cx(X1) + PAR.Pic.TextWidth(" ")
      'PAR.Pic.CurrentX = cx(X1) - PAR.Pic.TextWidth(TESTO & " ") / 2
      PAR.Pic.CurrentY = cy(Y1) - PAR.Pic.TextHeight(TESTO) / 2
    
    Case 7 'Al centro Dx
      PAR.Pic.CurrentX = cx(X1) - PAR.Pic.TextWidth(" ")
      'PAR.Pic.CurrentX = cx(X1) - PAR.Pic.TextWidth(TESTO & " ") / 2
      PAR.Pic.CurrentY = cy(Y1) - PAR.Pic.TextHeight(TESTO) / 2
  
  End Select
  '
  PAR.Pic.Print TESTO
  '
  PAR.Pic.CurrentX = CuX
  PAR.Pic.CurrentY = CuY
  PAR.Pic.ForeColor = tmpcol
  PAR.Pic.FontBold = tBol

End Sub

Private Sub Cerchio(ByVal Xc As Double, ByVal YC As Double, ByVal R As Double, Optional COLORE As Long = vbBlack, Optional Spessore As Integer = 1, Optional stile As Integer = vbSolid, Optional Rotazione As Double = 0)
'
Dim tSp     As Integer
Dim tSt     As Integer
Dim TRATTI  As Integer
Dim da      As Double
Dim X1      As Double
Dim X2      As Double
Dim Y1      As Double
Dim Y2      As Double
'
On Error Resume Next
  '
  TRATTI = 150
  tSt = PAR.Pic.DrawStyle
  tSp = PAR.Pic.DrawWidth
  PAR.Pic.DrawStyle = stile
  PAR.Pic.DrawWidth = Spessore
  '
  If PAR.AllBlack Then COLORE = vbBlack
  '
  da = 2 * PG / TRATTI
  '
  X1 = Xc + R * Cos(0) * Cos(Rotazione)
  Y1 = YC + R * Sin(0)
  For i = 1 To TRATTI
    X2 = Xc + R * Cos(da * i) * Cos(Rotazione)
    Y2 = YC + R * Sin(da * i)
    Call linea(X1, Y1, X2, Y2, COLORE, Spessore, stile)
    X1 = X2
    Y1 = Y2
  Next i
  '
  PAR.Pic.DrawStyle = tSt
  PAR.Pic.DrawWidth = tSp
  '
End Sub
Public Sub lineaV(ByVal X1 As Double, ByVal Y1 As Double, ByVal X2 As Double, ByVal Y2 As Double, Optional COLORE As Long = vbBlack, Optional Spessore As Integer = 1, Optional stile As Integer = vbSolid, Optional trimRegione As Boolean = True)

'
Dim tX1       As Double
Dim tY1       As Double
Dim TX2       As Double
Dim tY2       As Double
Dim tdX       As Double
Dim tdY       As Double
Dim NTratti   As Integer
Dim i         As Integer
Dim j         As Integer
Dim tSp       As Integer
Dim tSt       As Integer
'
On Error Resume Next
  '
  tSt = PAR.Pic.DrawStyle:          tSp = PAR.Pic.DrawWidth
        PAR.Pic.DrawStyle = stile:        PAR.Pic.DrawWidth = Spessore
  '
  If PAR.AllBlack Then COLORE = vbBlack
  '
  'tX1 = cx(X1): tY1 = cy(Y1)
  'TX2 = cx(X2): tY2 = cy(Y2)
  
  tX1 = PAR.Xc + (X1 - PAR.SpostX) * PAR.Scala
  tY1 = PAR.YC - (Y1 - PAR.SpostY) * PAR.Scala
  
  TX2 = PAR.Xc + (X2 - PAR.SpostX) * PAR.Scala
  tY2 = PAR.YC - (Y2 - PAR.SpostY) * PAR.Scala
  
  
  '
  If (Not (InRegione(tX1, tY1) And InRegione(TX2, tY2))) And trimRegione Then
    NTratti = 20
    tdX = (TX2 - tX1) / NTratti
    tdY = (tY2 - tY1) / NTratti
    If InRegione(tX1, tY1) Then
      TX2 = tX1
      tY2 = tY1
      While InRegione(TX2 + tdX, tY2 + tdY)
        TX2 = TX2 + tdX
        tY2 = tY2 + tdY
      Wend
      PAR.Pic.Line (tX1, tY1)-(TX2, tY2), COLORE
    ElseIf InRegione(TX2, tY2) Then
      tX1 = TX2
      tY1 = tY2
      While InRegione(tX1 - tdX, tY1 - tdY)
        tX1 = tX1 - tdX
        tY1 = tY1 - tdY
      Wend
      PAR.Pic.Line (tX1, tY1)-(TX2, tY2), COLORE
    End If
  Else
    PAR.Pic.Line (tX1, tY1)-(TX2, tY2), COLORE
  End If
  '
  PAR.Pic.DrawStyle = tSt
  PAR.Pic.DrawWidth = tSp
  '

End Sub


Public Sub linea(ByVal X1 As Double, ByVal Y1 As Double, ByVal X2 As Double, ByVal Y2 As Double, Optional COLORE As Long = vbBlack, Optional Spessore As Integer = 1, Optional stile As Integer = vbSolid, Optional trimRegione As Boolean = True)
'
Dim tX1       As Double
Dim tY1       As Double
Dim TX2       As Double
Dim tY2       As Double
Dim tdX       As Double
Dim tdY       As Double
Dim NTratti   As Integer
Dim i         As Integer
Dim j         As Integer
Dim tSp       As Integer
Dim tSt       As Integer
'
On Error Resume Next
  '
  tSt = PAR.Pic.DrawStyle:          tSp = PAR.Pic.DrawWidth
        PAR.Pic.DrawStyle = stile:        PAR.Pic.DrawWidth = Spessore
  '
  If PAR.AllBlack Then COLORE = vbBlack
  '
  tX1 = cx(X1): tY1 = cy(Y1)
  TX2 = cx(X2): tY2 = cy(Y2)
  '
  If (Not (InRegione(tX1, tY1) And InRegione(TX2, tY2))) And trimRegione Then
    NTratti = 20
    tdX = (TX2 - tX1) / NTratti
    tdY = (tY2 - tY1) / NTratti
    If InRegione(tX1, tY1) Then
      TX2 = tX1
      tY2 = tY1
      While InRegione(TX2 + tdX, tY2 + tdY)
        TX2 = TX2 + tdX
        tY2 = tY2 + tdY
      Wend
      PAR.Pic.Line (tX1, tY1)-(TX2, tY2), COLORE
    ElseIf InRegione(TX2, tY2) Then
      tX1 = TX2
      tY1 = tY2
      While InRegione(tX1 - tdX, tY1 - tdY)
        tX1 = tX1 - tdX
        tY1 = tY1 - tdY
      Wend
      PAR.Pic.Line (tX1, tY1)-(TX2, tY2), COLORE
    End If
  Else
    PAR.Pic.Line (tX1, tY1)-(TX2, tY2), COLORE
  End If
  '
  PAR.Pic.DrawStyle = tSt
  PAR.Pic.DrawWidth = tSp
  '
End Sub

Private Sub QuotaH(ByVal X1 As Double, ByVal Y1 As Double, ByVal X2 As Double, ByVal Y2 As Double, Valore As String, Optional Scala As Double = 1, Optional COLORE As Long, Optional Allineamento As Integer = 4, Optional Spessore As Integer = 1, Optional TipoFreccia As Integer = 0)
'Il punto X1,Y1 deve necessariamente essere il punto pi� a sinistra

Dim Xme As Double
Dim Yme As Double
Dim Xin As Double
Dim Yin As Double
Dim Xfi As Double
Dim Yfi As Double
Dim Lin As Double
Dim HLin  As Double
Dim Xt  As Double
Dim Yt  As Double
'
On Error Resume Next
  '
  Xin = cx(X1): Yin = cy(Y2)
  Xfi = cx(X2): Yfi = cy(Y2)
  '
  Xme = (Xin + Xfi) / 2
  Yme = (Yin + Yfi) / 2
  '
  If PAR.AllBlack Then COLORE = vbBlack
  '
  Lin = PAR.Pic.TextHeight(">") / 2 '0.1 * Scala"
  HLin = Lin / 2
    
  If COLORE = vbRed Then
     PAR.Pic.DrawWidth = 2 'Spessore
  Else
     PAR.Pic.DrawWidth = 1
  End If
  PAR.Pic.Line (Xin, Yin)-(Xfi, Yfi), COLORE
  PAR.Pic.DrawWidth = 1
  '
  Select Case Allineamento
    Case 1 'Dx
      Xt = X1 - (Lin / PAR.Scala / 3)
      Yt = Y1
    Case 2 'Sx
      Xt = X2 + (Lin / PAR.Scala / 3)
      Yt = Y2
    Case 3 'Sopra al centro
      Xt = (X1 + X2) / 2
      Yt = ((Y1 + Y2) / 2) + (Lin / PAR.Scala / 3)
    Case 4 'Sotto al centro
      Xt = (X1 + X2) / 2
      Yt = (Y1 + Y2) / 2 - (Lin / PAR.Scala / 3)
    Case 5 'Al centro
      Xt = (X1 + X2) / 2
      Yt = (Y1 + Y2) / 2
      
  End Select
  '
  Call TestoAll(Xt, Yt, Valore, COLORE, , Allineamento)
  PAR.Pic.DrawWidth = 1
  
  Select Case TipoFreccia
    Case 0 'Entrambi lati
      'Linea su punto Iniziale
      PAR.Pic.Line (Xin, Yin + Lin / 2)-(Xin, Yin - Lin / 2), COLORE
      'Freccia a sx "|<--"
      PAR.Pic.Line (Xin, Yin)-(Xin + Lin, Yin + Lin / 2), COLORE
      PAR.Pic.Line (Xin, Yin)-(Xin + Lin, Yin - Lin / 2), COLORE
      'Linea su punto Finale
      PAR.Pic.Line (Xfi, Yfi + Lin / 2)-(Xfi, Yfi - Lin / 2), COLORE
      'Freccia a dx "-->|"
      PAR.Pic.Line (Xfi, Yfi)-(Xfi - Lin, Yfi + Lin / 2), COLORE
      PAR.Pic.Line (Xfi, Yfi)-(Xfi - Lin, Yfi - Lin / 2), COLORE

    Case 1  'Solo punto Iniziale
      'Linea su punto Iniziale
      PAR.Pic.Line (Xin, Yin + Lin / 2)-(Xin, Yin - Lin / 2), COLORE
      'Freccia a sx "|<--"
      PAR.Pic.Line (Xin, Yin)-(Xin + Lin, Yin + Lin / 2), COLORE
      PAR.Pic.Line (Xin, Yin)-(Xin + Lin, Yin - Lin / 2), COLORE

    Case 2  'Solo su punto Finale
      'Linea su punto Finale
      PAR.Pic.Line (Xfi, Yfi + Lin / 2)-(Xfi, Yfi - Lin / 2), COLORE
      'Freccia a dx "-->|"
      PAR.Pic.Line (Xfi, Yfi)-(Xfi - Lin, Yfi + Lin / 2), COLORE
      PAR.Pic.Line (Xfi, Yfi)-(Xfi - Lin, Yfi - Lin / 2), COLORE

    Case 3 'Solo linee senza frecce
      'Linea su punto Iniziale
      PAR.Pic.Line (Xin, Yin + Lin / 2)-(Xin, Yin - Lin / 2), COLORE
      'Linea su punto Finale
      PAR.Pic.Line (Xfi, Yfi + Lin / 2)-(Xfi, Yfi - Lin / 2), COLORE

    Case Else ' Nulla

  End Select
  
  '

End Sub
  
Private Sub QuotaVV(ByVal X1 As Double, ByVal Y1 As Double, ByVal X2 As Double, ByVal Y2 As Double, Valore As String, Optional Scala As Double = 1, Optional COLORE As Long, Optional Allineamento As Integer = 5, Optional Spessore As Integer = 1, Optional TipoFreccia As Integer = 0)

Dim Xme   As Double
Dim Yme   As Double
Dim Xin   As Double
Dim Yin   As Double
Dim Xfi   As Double
Dim Yfi   As Double
Dim Lin   As Double
Dim HLin  As Double
Dim Xt    As Double
Dim Yt    As Double
Dim lt    As Double
Dim ht    As Double
'
On Error Resume Next
  '
  'cx = PAR.XC + (X - PAR.SpostX) * PAR.Scala
  'cy = PAR.YC - (Y - PAR.SpostY) * PAR.Scala
  
  Xin = PAR.Xc + (X1 - PAR.SpostX) * PAR.Scala
  Yin = PAR.YC - (Y1 - PAR.SpostY) * PAR.Scala
  
  Xfi = PAR.Xc + (X2 - PAR.SpostX) * PAR.Scala
  Yfi = PAR.YC - (Y2 - PAR.SpostY) * PAR.Scala
  
  
  Xme = (Xin + Xfi) / 2
  Yme = (Yin + Yfi) / 2
  
  
  '
  If PAR.AllBlack Then COLORE = vbBlack
  '
  Lin = PAR.Pic.TextHeight(">") / 2
  
  If COLORE = vbRed Then
     PAR.Pic.DrawWidth = 2 'Spessore
  Else
     PAR.Pic.DrawWidth = 1
  End If
  
  HLin = Lin / 2
  '
  PAR.Pic.Line (Xin, Yin)-(Xfi, Yfi), COLORE
  PAR.Pic.DrawWidth = 1
  '
  Select Case Allineamento
    Case 1 'Dx
      Xt = X1 - (Lin / PAR.Scala / 3)
      Yt = Y1
    Case 2 'Sx
      Xt = X2 + (Lin / PAR.Scala / 3)
      Yt = Y2
    Case 3 'Sopra al centro
      Xt = (X1 + X2) / 2
      Yt = ((Y1 + Y2) / 2) + (Lin / PAR.Scala / 3)
    Case 4 'Sotto al centro
      Xt = (X1 + X2) / 2
      Yt = (Y1 + Y2) / 2 - (Lin / PAR.Scala / 3)
    Case 5 'Centro
      Xt = (X1 + X2) / 2
      Yt = (Y1 + Y2) / 2
    Case 6 'Centro a Sx
      Xt = X2 - ((Lin * 4) / PAR.Scala / 2)
      Yt = (Y1 + Y2) / 2
    Case 7 'Centro a Dx
      Xt = X2 + (Lin / PAR.Scala / 3)
      Yt = (Y1 + Y2) / 2
      
  End Select
  '
  lt = PAR.Pic.TextWidth(Valore)
  ht = PAR.Pic.TextHeight(Valore)
  '
  'PAR.Pic.Line (cx(Xt) - lt / 2 - PAR.Pic.TextWidth(" "), cy(Yt) - ht / 2)-(cx(Xt) + lt / 2, cy(Yt) + ht / 2), vbWhite, BF
  '
  Call TestoAll_V(Xt, Yt, Valore, COLORE, , Allineamento)
  PAR.Pic.DrawWidth = 1
  
  
  Select Case TipoFreccia
    Case 0  'Entrambi lati
    'Linea in alto
    PAR.Pic.Line (Xfi - HLin, Yfi)-(Xfi + HLin, Yfi), COLORE
    'frecce alte
    PAR.Pic.Line (Xin, Yfi)-(Xin + HLin, Yfi + Lin), COLORE
    PAR.Pic.Line (Xin, Yfi)-(Xin - HLin, Yfi + Lin), COLORE
  
    'linea in basso
    PAR.Pic.Line (Xin - HLin, Yin)-(Xin + HLin, Yin), COLORE
    'frecce basse
    PAR.Pic.Line (Xfi, Yin)-(Xfi + HLin, Yin - Lin), COLORE
    PAR.Pic.Line (Xfi, Yin)-(Xfi - HLin, Yin - Lin), COLORE
    
    Case 1  'Solo lato Alto
    'Linea in alto
    PAR.Pic.Line (Xfi - HLin, Yfi)-(Xfi + HLin, Yfi), COLORE
    'frecce alte
    PAR.Pic.Line (Xin, Yfi)-(Xin + HLin, Yfi + Lin), COLORE
    PAR.Pic.Line (Xin, Yfi)-(Xin - HLin, Yfi + Lin), COLORE
    
    Case 2  'Solo lato Basso
    'linea in basso
    PAR.Pic.Line (Xin - HLin, Yin)-(Xin + HLin, Yin), COLORE
    'frecce basse
    PAR.Pic.Line (Xfi, Yin)-(Xfi + HLin, Yin - Lin), COLORE
    PAR.Pic.Line (Xfi, Yin)-(Xfi - HLin, Yin - Lin), COLORE
    
    Case 3 'Nessuna freccia
  
  End Select
  
  '


End Sub
'


Private Sub QuotaV(ByVal X1 As Double, ByVal Y1 As Double, ByVal X2 As Double, ByVal Y2 As Double, Valore As String, Optional Scala As Double = 1, Optional COLORE As Long, Optional Allineamento As Integer = 5, Optional TipoFreccia As Integer = 0)
'
Dim Xme   As Double
Dim Yme   As Double
Dim Xin   As Double
Dim Yin   As Double
Dim Xfi   As Double
Dim Yfi   As Double
Dim Lin   As Double
Dim HLin  As Double
Dim Xt    As Double
Dim Yt    As Double
Dim lt    As Double
Dim ht    As Double
'
On Error Resume Next
  '
  Xin = cx(X1):  Yin = cy(Y1)
  Xfi = cx(X1):  Yfi = cy(Y2)
  Xme = (Xin + Xfi) / 2
  Yme = (Yin + Yfi) / 2
  '
  If PAR.AllBlack Then COLORE = vbBlack
  '
  Lin = PAR.Pic.TextHeight(">") / 2
  
  If COLORE = vbRed Then
     PAR.Pic.DrawWidth = 2 'Spessore
  Else
     PAR.Pic.DrawWidth = 1
  End If
  
  HLin = Lin / 2
  '
  PAR.Pic.Line (Xin, Yin)-(Xfi, Yfi), COLORE
  PAR.Pic.DrawWidth = 1
  '
  Select Case Allineamento
    Case 1 'Dx
      Xt = X1 - (Lin / PAR.Scala / 3)
      Yt = Y1
    Case 2 'Sx
      Xt = X2 + (Lin / PAR.Scala / 3)
      Yt = Y2
    Case 3 'Sopra al centro
      Xt = (X1 + X2) / 2
      Yt = ((Y1 + Y2) / 2) + (Lin / PAR.Scala / 3)
    Case 4 'Sotto al centro
      Xt = (X1 + X2) / 2
      Yt = (Y1 + Y2) / 2 - (Lin / PAR.Scala / 3)
    Case 5 'Centro
      Xt = (X1 + X2) / 2
      Yt = (Y1 + Y2) / 2
    Case 6 'Centro a Sx
      Xt = X2 + (Lin / PAR.Scala / 3)
      Yt = (Y1 + Y2) / 2
    Case 7 'Centro a Dx
      Xt = X2 - (Lin / PAR.Scala / 3)
      Yt = (Y1 + Y2) / 2
      
  End Select
  '
  lt = PAR.Pic.TextWidth(Valore)
  ht = PAR.Pic.TextHeight(Valore)
  '
  PAR.Pic.Line (cx(Xt) - lt / 2 - PAR.Pic.TextWidth(" "), cy(Yt) - ht / 2)-(cx(Xt) + lt / 2, cy(Yt) + ht / 2), vbWhite, BF
  '
  Call TestoAll(Xt, Yt, Valore, COLORE, , Allineamento)
  PAR.Pic.DrawWidth = 1
  
  'PAR.Pic.Line (Xin - HLin, Yin)-(Xin + HLin, Yin), COLORE
  'PAR.Pic.Line (Xfi - HLin, Yfi)-(Xfi + HLin, Yfi), COLORE
  
  Select Case TipoFreccia
    Case 0  'Entrambi lati
    'Linea in alto
    PAR.Pic.Line (Xfi - HLin, Yfi)-(Xfi + HLin, Yfi), COLORE
    'frecce alte
    PAR.Pic.Line (Xin, Yfi)-(Xin + HLin, Yfi + Lin), COLORE
    PAR.Pic.Line (Xin, Yfi)-(Xin - HLin, Yfi + Lin), COLORE
  
    'linea in basso
    PAR.Pic.Line (Xin - HLin, Yin)-(Xin + HLin, Yin), COLORE
    'frecce basse
    PAR.Pic.Line (Xfi, Yin)-(Xfi + HLin, Yin - Lin), COLORE
    PAR.Pic.Line (Xfi, Yin)-(Xfi - HLin, Yin - Lin), COLORE
    
    Case 1  'Solo lato Alto
    'Linea in alto
    PAR.Pic.Line (Xfi - HLin, Yfi)-(Xfi + HLin, Yfi), COLORE
    'frecce alte
    PAR.Pic.Line (Xin, Yfi)-(Xin + HLin, Yfi + Lin), COLORE
    PAR.Pic.Line (Xin, Yfi)-(Xin - HLin, Yfi + Lin), COLORE
    
    Case 2  'Solo lato Basso
    'linea in basso
    PAR.Pic.Line (Xin - HLin, Yin)-(Xin + HLin, Yin), COLORE
    'frecce basse
    PAR.Pic.Line (Xfi, Yin)-(Xfi + HLin, Yin - Lin), COLORE
    PAR.Pic.Line (Xfi, Yin)-(Xfi - HLin, Yin - Lin), COLORE
    
    Case 3 'Nessuna freccia
  
  End Select
  
  '
End Sub

Private Function CxTw2Px(X As Double) As Double 'Twip to Pixel conversion
'
On Error Resume Next
  '
  CxTw2Px = PAR.Pic.ScaleX(PAR.Xc, 1, 3) + (X - PAR.SpostX) * PAR.Pic.ScaleX(PAR.Scala, 1, 3)

End Function

Private Function CyTw2Px(Y As Double) As Double
'
On Error Resume Next
  '
  CyTw2Px = PAR.Pic.ScaleY(PAR.YC, 1, 3) - (Y - PAR.SpostY) * PAR.Pic.ScaleY(PAR.Scala, 1, 3)

End Function

Private Function cx(X As Double) As Double
'
On Error Resume Next
  '
  cx = PAR.Xc + (X - PAR.SpostX) * PAR.Scala

End Function

Private Function ICx(cx As Double) As Double
'
On Error Resume Next
  '
  If PAR.Scala = 0 Then Exit Function
  ICx = ((cx - PAR.Xc) / PAR.Scala) + PAR.SpostX

End Function

Private Function cy(Y As Double) As Double
'
On Error Resume Next
  '
  cy = PAR.YC - (Y - PAR.SpostY) * PAR.Scala

End Function

Private Function ICy(cy As Double) As Double
'
On Error Resume Next
  '
  If PAR.Scala = 0 Then Exit Function
  ICy = ((PAR.YC - cy) / PAR.Scala) + PAR.SpostY

End Function

Private Function frmt4(Valore) As String
'
On Error Resume Next
  '
  frmt4 = Format(Valore, "0.0###")

End Function

'*********************************************************************************************
'*********************************************************************************************
'************************************ ROUTINES PUBBLICHE *************************************
'*********************************************************************************************
'*********************************************************************************************
Public Sub Disegna_ProfiloMola(Optional Stampante As Boolean = False)
'
Dim i           As Integer
Dim Fianco      As Integer
Dim Xc          As Double
Dim YC          As Double
Dim diamIntOtt  As Double
Dim MaxYmola    As Double
Dim TrattoYLin  As Double
Dim XA1         As Double
Dim YA1         As Double
Dim X1Piatt     As Double
Dim X2Piatt     As Double
Dim Y1Piatt     As Double
Dim Y2Piatt     As Double
Dim LPiatt      As Double
Dim UB          As Integer
'
On Error GoTo errDisegna_ProfiloMola
  '
  UB = -1
  Call InizializzaVariabili
  '
On Error Resume Next
  '
  UB = UBound(CalcoloEsterni.MolaX, 2)
  If UB = -1 Then Exit Sub
  '
On Error GoTo errDisegna_ProfiloMola
  '
  PAR.MinX = -Max2D(CalcoloEsterni.MolaX, 1, 1)
  PAR.MaxX = Max2D(CalcoloEsterni.MolaX, 2, 1)
  PAR.MinY = Min2D(CalcoloEsterni.MolaY, 1, 1)
  PAR.MaxY = Max2D(CalcoloEsterni.MolaY, 1, 1)
  '
  PAR.ScalaMargine = 0.45
  PAR.Allineamento = 2
  '
  Call CalcolaParPic(PAR, False, Stampante)
  Call linea(0, PAR.MinY, 0, PAR.MaxY, RGB(100, 100, 100), , vbDot)
  '
  With CalcoloEsterni
    '
    For i = 2 To UBound(.MolaX, 2)
      Call linea(-.MolaX(1, i - 1), .MolaY(1, i - 1), -.MolaX(1, i), .MolaY(1, i))
      Call DISEGNA_PUNTO(-CDbl(.MolaX(1, i)), CDbl(.MolaY(1, i)), vbRed)
      Call linea(.MolaX(2, i - 1), .MolaY(2, i - 1), .MolaX(2, i), .MolaY(2, i))
      Call DISEGNA_PUNTO(CDbl(.MolaX(2, i)), CDbl(.MolaY(2, i)), vbRed)
    Next i
    '
    Call DISEGNA_PUNTO(-CDbl(.MolaX(1, 1)), CDbl(.MolaY(1, 1)), vbRed)
    Call DISEGNA_PUNTO(CDbl(.MolaX(2, 1)), CDbl(.MolaY(2, 1)), vbRed)
    '
    X1Piatt = .MolaX(1, UBound(.MolaX, 2) - 1)
    X2Piatt = .MolaX(2, UBound(.MolaX, 2) - 1)
    Y1Piatt = .MolaY(1, UBound(.MolaY, 2) - 1)
    Y2Piatt = .MolaY(2, UBound(.MolaY, 2) - 1)
    '
    LPiatt = X2Piatt + X1Piatt
    MaxYmola = Max2D(CalcoloEsterni.MolaY, 1, 1) '(YC + DGlob.RF)
    Call linea(0, PAR.MinY, 0, MaxYmola, RGB(100, 100, 100), , vbDot)
    'Quota larghezza profilo mola
    Call QuotaH(-Max2D(CalcoloEsterni.MolaX, 1, 1), Min2D(CalcoloEsterni.MolaY, 1, 1), Max2D(CalcoloEsterni.MolaX, 2, 1), Min2D(CalcoloEsterni.MolaY, 2, 1), frmt4(Max2D(CalcoloEsterni.MolaX, 2, 1) + Max2D(CalcoloEsterni.MolaX, 1, 1)) & " mm", 12)
    'Quota piattino testa mola
    Call QuotaH(X2Piatt, 0.2 + MaxYmola, -X1Piatt, 0.2 + MaxYmola, frmt4(LPiatt) & " mm", 12, IIf(LPiatt >= 0, vbBlack, vbRed), 3)
    'Quota altezza profilo mola
    Call QuotaV(0, PAR.MinY, 0, MaxYmola, frmt4((MaxYmola) - PAR.MinY) & " mm", 12, , 5)
  End With
  '
  Call TestoAll(PAR.MaxX, (PAR.MaxY + PAR.MinY) / 2, "F2", , True, 5)
  Call TestoAll(PAR.MinX, (PAR.MaxY + PAR.MinY) / 2, "F1", , True, 5)
  '
  If (DGlob.WbQ > 0) Then Call TestoInfo("W(" & DGlob.WbN & ")= " & frmt(DGlob.WbQ, 4), 1)
  If (DGlob.RulD > 0) Then Call TestoInfo("Q(" & DGlob.RulD & ")= " & frmt(DGlob.RulQ, 4), 2)
  If (DGlob.SON > 0) Then Call TestoInfo("S0Nn= " & frmt(DGlob.SON, 4), 4)
  '
Exit Sub

errDisegna_ProfiloMola:
  StopRegieEvents
  MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Sub: Disegna_ProfiloMola"
  ResumeRegieEvents
  Resume Next

End Sub
Public Sub Disegna_Mola_INGRANAGGI(Optional Stampante As Boolean = False)
'
Dim i           As Integer
Dim Fianco      As Integer
Dim Xc          As Double
Dim YC          As Double
Dim diamIntOtt  As Double
Dim MaxYmola    As Double
Dim TrattoYLin  As Double
Dim XA1         As Double
Dim YA1         As Double
Dim X1Piatt     As Double
Dim X2Piatt     As Double
Dim Y1Piatt     As Double
Dim Y2Piatt     As Double
Dim LPiatt      As Double
Dim UB          As Integer
'
On Error GoTo errDISEGNA_MOLA_INGRANAGGI
  '
  UB = -1
  Call InizializzaVariabili
  '
On Error Resume Next
  '
  UB = UBound(CalcoloEsterni.MolaX, 2)
  If UB = -1 Then Exit Sub
  '
On Error GoTo errDISEGNA_MOLA_INGRANAGGI
  '
  PAR.MinX = -Max2D(CalcoloEsterni.MolaX, 1, 1)
  PAR.MaxX = Max2D(CalcoloEsterni.MolaX, 2, 1)
  PAR.MinY = Min2D(CalcoloEsterni.MolaY, 1, 1)
  PAR.MaxY = Max2D(CalcoloEsterni.MolaY, 1, 1)
  '
  PAR.ScalaMargine = 0.45
  PAR.Allineamento = 2
  '
  Call CalcolaParPic(PAR, False, Stampante)
  Call linea(0, PAR.MinY, 0, PAR.MaxY, RGB(100, 100, 100), , vbDot)
  '
  With CalcoloEsterni
    '
    For i = 2 To UBound(.MolaX, 2)
      Call linea(-.MolaX(1, i - 1), .MolaY(1, i - 1), -.MolaX(1, i), .MolaY(1, i))
      Call DISEGNA_PUNTO(-CDbl(.MolaX(1, i)), CDbl(.MolaY(1, i)), vbRed)
      Call linea(.MolaX(2, i - 1), .MolaY(2, i - 1), .MolaX(2, i), .MolaY(2, i))
      Call DISEGNA_PUNTO(CDbl(.MolaX(2, i)), CDbl(.MolaY(2, i)), vbRed)
    Next i
    '
    Call DISEGNA_PUNTO(-CDbl(.MolaX(1, 1)), CDbl(.MolaY(1, 1)), vbRed)
    Call DISEGNA_PUNTO(CDbl(.MolaX(2, 1)), CDbl(.MolaY(2, 1)), vbRed)
    '
    X1Piatt = .MolaX(1, UBound(.MolaX, 2) - 1)
    X2Piatt = .MolaX(2, UBound(.MolaX, 2) - 1)
    Y1Piatt = .MolaY(1, UBound(.MolaY, 2) - 1)
    Y2Piatt = .MolaY(2, UBound(.MolaY, 2) - 1)
    '
    LPiatt = X2Piatt + X1Piatt
    MaxYmola = Max2D(CalcoloEsterni.MolaY, 1, 1) '(YC + DGlob.RF)
    Call linea(0, PAR.MinY, 0, MaxYmola, RGB(100, 100, 100), , vbDot)
    'Quota larghezza profilo mola
    Call QuotaH(-Max2D(CalcoloEsterni.MolaX, 1, 1), Min2D(CalcoloEsterni.MolaY, 1, 1), Max2D(CalcoloEsterni.MolaX, 2, 1), Min2D(CalcoloEsterni.MolaY, 2, 1), frmt4(Max2D(CalcoloEsterni.MolaX, 2, 1) + Max2D(CalcoloEsterni.MolaX, 1, 1)) & " mm", 12)
    'Quota piattino testa mola
    Call QuotaH(X2Piatt, 0.2 + MaxYmola, -X1Piatt, 0.2 + MaxYmola, frmt4(LPiatt) & " mm", 12, IIf(LPiatt >= 0, vbBlack, vbRed), 3)
    'Quota altezza profilo mola
    Call QuotaV(0, PAR.MinY, 0, MaxYmola, frmt4((MaxYmola) - PAR.MinY) & " mm", 12, , 5)
  End With
  '
  Call TestoAll(PAR.MaxX, (PAR.MaxY + PAR.MinY) / 2, "F2", , True, 5)
  Call TestoAll(PAR.MinX, (PAR.MaxY + PAR.MinY) / 2, "F1", , True, 5)
  '
  If (DGlob.WbQ > 0) Then Call TestoInfo("W(" & DGlob.WbN & ")= " & frmt(DGlob.WbQ, 4), 1)
  If (DGlob.RulD > 0) Then Call TestoInfo("Q(" & DGlob.RulD & ")= " & frmt(DGlob.RulQ, 4), 2)
  If (DGlob.SON > 0) Then Call TestoInfo("S0Nn= " & frmt(DGlob.SON, 4), 4)
  If (DGlob.FattoreX <> 0) Then Call TestoInfo("X= " & frmt(DGlob.FattoreX, 4), 3)
  '
Exit Sub

errDISEGNA_MOLA_INGRANAGGI:
  StopRegieEvents
  MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Sub: Disegna_Mola_INGRANAGGI"
  ResumeRegieEvents
  Resume Next

End Sub

Public Sub Disegna_Mola_INGRANAGGI_ESTERNIV(Optional ByVal X1 As Double = 0, Optional ByVal Y1 As Double = 0, Optional COLORE As Long = vbBlack, Optional Spessore As Integer = 1, Optional stile As Integer = vbSolid)

Dim i       As Integer
Dim npt     As Integer
Dim Fianco  As Integer
Dim UB      As Integer
Dim SPE     As Double
Dim Xt      As Double

UB = -1

On Error Resume Next

UB = UBound(CalcoloEsterni.MolaX, 2)
If UB = -1 Then Exit Sub
     
On Error GoTo errDisegna_Mola_INGRANAGGI_ESTERNIV

npt = UB

With CalcoloEsterni
  '
  For Fianco = 1 To 2
    For i = 2 To npt
      Call linea(Y1 - .MolaY(Fianco, i - 1), X1 + .MolaX(Fianco, i - 1), Y1 - .MolaY(Fianco, i), X1 + .MolaX(Fianco, i), COLORE, Spessore, stile)
      Call linea(Y1 - .MolaY(Fianco, i - 1), X1 - .MolaX(Fianco, i - 1), Y1 - .MolaY(Fianco, i), X1 - .MolaX(Fianco, i), COLORE, Spessore, stile)
      Call linea(Y1 + .MolaY(Fianco, i - 1), X1 + .MolaX(Fianco, i - 1), Y1 + .MolaY(Fianco, i), X1 + .MolaX(Fianco, i), COLORE, Spessore, stile)
      Call linea(Y1 + .MolaY(Fianco, i - 1), X1 - .MolaX(Fianco, i - 1), Y1 + .MolaY(Fianco, i), X1 - .MolaX(Fianco, i), COLORE, Spessore, stile)
    Next i
    '
    If Lp_Str("SPMOLA_G[0,2]") <> "NOTFOUND" Then
      SPE = Lp("SPMOLA_G[0,2]")
      SPE = SPE / 2
    ElseIf Lp_Str("SPMOLA_G[0,1]") <> "NOTFOUND" Then
      SPE = Lp("SPMOLA_G[0,1]")
      SPE = SPE / 2
    ElseIf Lp_Str("R[37]") <> "NOTFOUND" Then
      SPE = Lp("R[37]")
      SPE = SPE / 2
    End If
    '
    If SPE = 0 Then
      SPE = .MolaX(1, 1) * 1.5
      stile = vbDot
      Spessore = 1
    End If
    '
    Call linea(Y1 - .MolaY(Fianco, 1), X1 - SPE, Y1 - .MolaY(Fianco, 1), X1 - .MolaX(Fianco, 1), COLORE, Spessore, stile)
    Call linea(Y1 - .MolaY(Fianco, 1), X1 + SPE, Y1 - .MolaY(Fianco, 1), X1 + .MolaX(Fianco, 1), COLORE, Spessore, stile)
    Call linea(Y1 + .MolaY(Fianco, 1), X1 - SPE, Y1 + .MolaY(Fianco, 1), X1 - .MolaX(Fianco, 1), COLORE, Spessore, stile)
    Call linea(Y1 + .MolaY(Fianco, 1), X1 + SPE, Y1 + .MolaY(Fianco, 1), X1 + .MolaX(Fianco, 1), COLORE, Spessore, stile)
    Call linea(Y1 + .MolaY(Fianco, 1), X1 - SPE, Y1 - .MolaY(Fianco, 1), X1 - SPE, COLORE, Spessore, stile)
    Call linea(Y1 + .MolaY(Fianco, 1), X1 + SPE, Y1 - .MolaY(Fianco, 1), X1 + SPE, COLORE, Spessore, stile)
    Call linea(Y1 - .MolaY(Fianco, npt), X1 - .MolaX(Fianco, npt), Y1 - .MolaY(Fianco, npt), X1 + .MolaX(Fianco, npt), COLORE, Spessore, stile)
    Call linea(Y1 + .MolaY(Fianco, npt), X1 - .MolaX(Fianco, npt), Y1 + .MolaY(Fianco, npt), X1 + .MolaX(Fianco, npt), COLORE, Spessore, stile)
    '
  Next Fianco
  '
End With

On Error GoTo 0

Exit Sub

errDisegna_Mola_INGRANAGGI_ESTERNIV:
  
  MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Sub: Disegna_Mola_INGRANAGGI_ESTERNIV"
  Resume Next
  
End Sub

Public Sub Disegna_Mola_INGRANAGGI_ESTERNI(Optional ByVal X1 As Double = 0, Optional ByVal Y1 As Double = 0, Optional COLORE As Long = vbBlack, Optional Spessore As Integer = 1, Optional stile As Integer = vbSolid)

Dim i       As Integer
Dim npt     As Integer
Dim Fianco  As Integer
Dim UB      As Integer
Dim SPE     As Double
Dim Xt      As Double

UB = -1

On Error Resume Next

UB = UBound(CalcoloEsterni.MolaX, 2)
If UB = -1 Then Exit Sub
     
On Error GoTo errDisegna_Mola_INGRANAGGI_ESTERNI

npt = UB

With CalcoloEsterni
  '
  For Fianco = 1 To 2
    For i = 2 To npt
      Call linea(X1 + .MolaX(Fianco, i - 1), Y1 - .MolaY(Fianco, i - 1), X1 + .MolaX(Fianco, i), Y1 - .MolaY(Fianco, i), COLORE, Spessore, stile)
      Call linea(X1 - .MolaX(Fianco, i - 1), Y1 - .MolaY(Fianco, i - 1), X1 - .MolaX(Fianco, i), Y1 - .MolaY(Fianco, i), COLORE, Spessore, stile)
      Call linea(X1 + .MolaX(Fianco, i - 1), Y1 + .MolaY(Fianco, i - 1), X1 + .MolaX(Fianco, i), Y1 + .MolaY(Fianco, i), COLORE, Spessore, stile)
      Call linea(X1 - .MolaX(Fianco, i - 1), Y1 + .MolaY(Fianco, i - 1), X1 - .MolaX(Fianco, i), Y1 + .MolaY(Fianco, i), COLORE, Spessore, stile)
    Next i
    '
    If Lp_Str("SPMOLA_G[0,2]") <> "NOTFOUND" Then
      SPE = Lp("SPMOLA_G[0,2]")
      SPE = SPE / 2
    ElseIf Lp_Str("SPMOLA_G[0,1]") <> "NOTFOUND" Then
      SPE = Lp("SPMOLA_G[0,1]")
      SPE = SPE / 2
    ElseIf Lp_Str("R[37]") <> "NOTFOUND" Then
      SPE = Lp("R[37]")
      SPE = SPE / 2
    End If
    '
    If SPE = 0 Then
      SPE = .MolaX(1, 1) * 1.5
      stile = vbDot
      Spessore = 1
    End If
    '
    Call linea(X1 - SPE, Y1 - .MolaY(Fianco, 1), X1 - .MolaX(Fianco, 1), Y1 - .MolaY(Fianco, 1), COLORE, Spessore, stile)
    Call linea(X1 + SPE, Y1 - .MolaY(Fianco, 1), X1 + .MolaX(Fianco, 1), Y1 - .MolaY(Fianco, 1), COLORE, Spessore, stile)
    Call linea(X1 - SPE, Y1 + .MolaY(Fianco, 1), X1 - .MolaX(Fianco, 1), Y1 + .MolaY(Fianco, 1), COLORE, Spessore, stile)
    Call linea(X1 + SPE, Y1 + .MolaY(Fianco, 1), X1 + .MolaX(Fianco, 1), Y1 + .MolaY(Fianco, 1), COLORE, Spessore, stile)
    Call linea(X1 - SPE, Y1 + .MolaY(Fianco, 1), X1 - SPE, Y1 - .MolaY(Fianco, 1), COLORE, Spessore, stile)
    Call linea(X1 + SPE, Y1 + .MolaY(Fianco, 1), X1 + SPE, Y1 - .MolaY(Fianco, 1), COLORE, Spessore, stile)
    Call linea(X1 - .MolaX(Fianco, npt), Y1 - .MolaY(Fianco, npt), X1 + .MolaX(Fianco, npt), Y1 - .MolaY(Fianco, npt), COLORE, Spessore, stile)
    Call linea(X1 - .MolaX(Fianco, npt), Y1 + .MolaY(Fianco, npt), X1 + .MolaX(Fianco, npt), Y1 + .MolaY(Fianco, npt), COLORE, Spessore, stile)
    '
  Next Fianco
  '
End With

On Error GoTo 0

Exit Sub

errDisegna_Mola_INGRANAGGI_ESTERNI:
  MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Sub: Disegna_Mola_INGRANAGGI_ESTERNI"
  Resume Next
  
End Sub

Public Sub Disegna_Mola_INGRANAGGI_ESTERNIO(Optional ByVal X1 As Double = 0, Optional ByVal Y1 As Double = 0, Optional COLORE As Long = vbBlack, Optional Spessore As Integer = 1, Optional stile As Integer = vbSolid)

Dim i       As Integer
Dim npt     As Integer
Dim Fianco  As Integer
Dim UB      As Integer
Dim SPE     As Double
Dim Xt      As Double

UB = -1

On Error Resume Next

UB = UBound(CalcoloEsterni.MolaX, 2)
If UB = -1 Then Exit Sub
     
On Error GoTo errDisegna_Mola_INGRANAGGI_ESTERNIO

npt = UB

With CalcoloEsterni
  '
  For Fianco = 1 To 2
    For i = 2 To npt
      Call linea(X1 + .MolaX(Fianco, i - 1), Y1 - .MolaY(Fianco, i - 1), X1 + .MolaX(Fianco, i), Y1 - .MolaY(Fianco, i), COLORE, Spessore, stile)
      Call linea(X1 - .MolaX(Fianco, i - 1), Y1 - .MolaY(Fianco, i - 1), X1 - .MolaX(Fianco, i), Y1 - .MolaY(Fianco, i), COLORE, Spessore, stile)
      'Call linea(X1 + .MolaX(Fianco, i - 1), Y1 + .MolaY(Fianco, i - 1), X1 + .MolaX(Fianco, i), Y1 + .MolaY(Fianco, i), COLORE, Spessore, stile)
      'Call linea(X1 - .MolaX(Fianco, i - 1), Y1 + .MolaY(Fianco, i - 1), X1 - .MolaX(Fianco, i), Y1 + .MolaY(Fianco, i), COLORE, Spessore, stile)
    Next i
  Next Fianco
  '
End With

On Error GoTo 0

Exit Sub

errDisegna_Mola_INGRANAGGI_ESTERNIO:
  MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Sub: Disegna_Mola_INGRANAGGI_ESTERNIO"
  Resume Next
  
End Sub

Public Sub Disegna_Mola_ROTORI(Optional ByVal X1 As Double = 0, Optional ByVal Y1 As Double = 0, Optional COLORE As Long = vbBlack, Optional Spessore As Integer = 1, Optional stile As Integer = vbSolid)
'
Dim i       As Integer
Dim npt     As Integer
Dim Fianco  As Integer
Dim UB      As Integer
Dim SPE     As Double
Dim Xt      As Double
  '
  UB = -1
  '
On Error Resume Next
  '
  UB = UBound(ParMolaROT.XX)
  If (UB <= 0) Then Exit Sub
  '
On Error GoTo errDisegna_Mola_ROTORI
  '
  npt = UB
  For i = 1 To npt
    Call linea(X1 + ParMolaROT.XX(i - 1), Y1 - ParMolaROT.YY(i - 1), X1 + ParMolaROT.XX(i), Y1 - ParMolaROT.YY(i), COLORE, Spessore, stile)
    Call linea(X1 + ParMolaROT.XX(i - 1), Y1 + ParMolaROT.YY(i - 1), X1 + ParMolaROT.XX(i), Y1 + ParMolaROT.YY(i), COLORE, Spessore, stile)
  Next i
  If (Lp_Str("R[37]") <> "NOTFOUND") Then
    SPE = Lp("R[37]")
    SPE = SPE / 2
  End If
  If (SPE = 0) Then
    stile = vbDot
    Spessore = 1
    SPE = CalcolaSpessoreMola(ParMolaROT.XX(npt) - ParMolaROT.XX(1)) / 2
  End If
  Call linea(X1 - SPE, Y1 - ParMolaROT.YY(0), X1 + ParMolaROT.XX(0), Y1 - ParMolaROT.YY(0), COLORE, Spessore, stile)
  Call linea(X1 + SPE, Y1 - ParMolaROT.YY(npt), X1 + ParMolaROT.XX(npt), Y1 - ParMolaROT.YY(npt), COLORE, Spessore, stile)
  '
  Call linea(X1 - SPE, Y1 + ParMolaROT.YY(0), X1 + ParMolaROT.XX(0), Y1 + ParMolaROT.YY(0), COLORE, Spessore, stile)
  Call linea(X1 + SPE, Y1 + ParMolaROT.YY(npt), X1 + ParMolaROT.XX(npt), Y1 + ParMolaROT.YY(npt), COLORE, Spessore, stile)
  '
  Call linea(X1 - SPE, Y1 + ParMolaROT.YY(0), X1 - SPE, Y1 - ParMolaROT.YY(0), COLORE, Spessore, stile)
  Call linea(X1 + SPE, Y1 + ParMolaROT.YY(npt), X1 + SPE, Y1 - ParMolaROT.YY(npt), COLORE, Spessore, stile)
  '
  Call QuotaH(X1 - SPE, Y1 - ParMolaROT.YY(0) + 20, X1 + SPE, Y1 - ParMolaROT.YY(0) + 20, SPE * 2, , , 3)
  '
On Error GoTo 0
  '
Exit Sub

errDisegna_Mola_ROTORI:
  i = Err
  StopRegieEvents
  MsgBox Error(i), vbCritical, "Sub: Disegna_Mola_ROTORI"
  ResumeRegieEvents
  Resume Next
  
End Sub

Public Sub Disegna_Mola_PROFILO_RAB(Optional ByVal X1 As Double = 0, Optional ByVal Y1 As Double = 0, Optional COLORE As Long = vbBlack, Optional Spessore As Integer = 1, Optional stile As Integer = vbSolid)
'
Dim i       As Integer
Dim npt     As Integer
Dim Fianco  As Integer
Dim UB      As Integer
Dim SPE     As Double
Dim Xt      As Double
  '
  UB = -1
  '
On Error Resume Next
  '
  UB = UBound(ParMolaROT.XX)
  If (UB <= 0) Then Exit Sub
  '
On Error GoTo errDisegna_Mola_PROFILO_RAB
  '
  npt = UB
  For i = 1 To npt
    Call linea(X1 + ParMolaROT.XX(i - 1), Y1 - ParMolaROT.YY(i - 1), X1 + ParMolaROT.XX(i), Y1 - ParMolaROT.YY(i), COLORE, Spessore, stile)
    Call linea(X1 + ParMolaROT.XX(i - 1), Y1 + ParMolaROT.YY(i - 1), X1 + ParMolaROT.XX(i), Y1 + ParMolaROT.YY(i), COLORE, Spessore, stile)
  Next i
  If (Lp_Str("SPMOLA_G[0,1]") <> "NOTFOUND") Then
    SPE = Lp("SPMOLA_G[0,1]")
    SPE = SPE / 2
  End If
  If (SPE = 0) Then
    stile = vbDot
    Spessore = 1
    SPE = CalcolaSpessoreMola(ParMolaROT.XX(npt) - ParMolaROT.XX(1)) / 2
  End If
  Call linea(X1 - SPE, Y1 - ParMolaROT.YY(0), X1 + ParMolaROT.XX(0), Y1 - ParMolaROT.YY(0), COLORE, Spessore, stile)
  Call linea(X1 + SPE, Y1 - ParMolaROT.YY(npt), X1 + ParMolaROT.XX(npt), Y1 - ParMolaROT.YY(npt), COLORE, Spessore, stile)
  '
  Call linea(X1 - SPE, Y1 + ParMolaROT.YY(0), X1 + ParMolaROT.XX(0), Y1 + ParMolaROT.YY(0), COLORE, Spessore, stile)
  Call linea(X1 + SPE, Y1 + ParMolaROT.YY(npt), X1 + ParMolaROT.XX(npt), Y1 + ParMolaROT.YY(npt), COLORE, Spessore, stile)
  '
  Call linea(X1 - SPE, Y1 + ParMolaROT.YY(0), X1 - SPE, Y1 - ParMolaROT.YY(0), COLORE, Spessore, stile)
  Call linea(X1 + SPE, Y1 + ParMolaROT.YY(npt), X1 + SPE, Y1 - ParMolaROT.YY(npt), COLORE, Spessore, stile)
  '
  Call QuotaH(X1 - SPE, Y1 - ParMolaROT.YY(0) + 5, X1 + SPE, Y1 - ParMolaROT.YY(0) + 5, SPE * 2, , , 3)
  '
On Error GoTo 0
  '
Exit Sub

errDisegna_Mola_PROFILO_RAB:
  i = Err
  StopRegieEvents
  MsgBox Error(i), vbCritical, "Sub: Disegna_Mola_PROFILO_RAB"
  ResumeRegieEvents
  Resume Next
  
End Sub

Function CalcolaSpessoreMola(SpessoreProfilo As Double) As Double
'
On Error Resume Next
  '
  CalcolaSpessoreMola = CInt(SpessoreProfilo / 5) * 5
  If CalcolaSpessoreMola < SpessoreProfilo Then
    CalcolaSpessoreMola = CalcolaSpessoreMola + 5
  End If

End Function

Private Function Min2D(Serie As Variant, Fianco As Integer, Optional IndiceIniziale As Integer = 0) As Variant

Dim i As Integer
Dim UB As Integer
  '
  UB = -1
  '
On Error Resume Next
  '
  UB = UBound(Serie, 2)
  '
On Error GoTo 0
  '
  Min2D = Serie(Fianco, IndiceIniziale)
  For i = IndiceIniziale To UB
    If Serie(Fianco, i) < Min2D Then Min2D = Serie(Fianco, i)
  Next i

End Function

Private Function Max2D(Serie As Variant, Fianco As Integer, Optional IndiceIniziale As Integer = 0) As Variant

Dim i As Integer
Dim UB As Integer

  UB = -1

On Error Resume Next

  UB = UBound(Serie, 2)

On Error GoTo 0

  Max2D = Serie(Fianco, IndiceIniziale)
  For i = IndiceIniziale To UB
    If Serie(Fianco, i) > Max2D Then Max2D = Serie(Fianco, i)
  Next i

End Function

Private Function MIN(Serie As Variant, Optional IndiceIniziale As Integer = 0) As Variant

Dim i As Integer
'
On Error Resume Next
  '
  MIN = Serie(IndiceIniziale)
  For i = IndiceIniziale To SafeUBound(Serie)
    If Serie(i) < MIN Then MIN = Serie(i)
  Next i

End Function

Private Function Max(Serie As Variant, Optional IndiceIniziale As Integer = 0) As Variant

Dim i As Integer
'
On Error Resume Next
  '
  Max = Serie(IndiceIniziale)
  For i = IndiceIniziale To SafeUBound(Serie)
    If (Serie(i) > Max) Then Max = Serie(i)
  Next i

End Function

Public Sub Disegna_Mola_RDE(Optional Stampante As Boolean = False)
'
Dim UB        As Integer
  '
  Call InizializzaVariabili
  UB = -1
  '
On Error GoTo errDisegna_Mola_RDE
  '
Dim Larghezza As Double
Dim Raggio    As Double
Dim CONICITA  As Double
Dim Altezza   As Double
  '
  Altezza = LEP("ALTEZZAFIANCOF1_G")
  '
  Larghezza = LeggiLarghezzaMolaRDE
  If (Larghezza <= 0) Then Larghezza = 50
  '
  Raggio = LEP("RFONDO_G")
  '
  If (Larghezza <= 2 * Raggio) Then Larghezza = 2 * Raggio + 0.001
  If (Altezza <= Raggio) Then Altezza = Raggio + 0.001
  '
  CONICITA = LEP("CORRFHAF1_G") 'MM
  CONICITA = -Atn(CONICITA / (Larghezza - 2 * Raggio))
  '
  PAR.MaxX = Larghezza / 2 + 6
  PAR.MaxY = 1 + Larghezza
  PAR.MinX = -Larghezza / 2 - 6
  PAR.MinY = -1 - Larghezza
  PAR.ScalaMargine = 1
  PAR.Allineamento = 1
  '
  Call CalcolaParPic(PAR, True, Stampante)
  Call linea(0, 2, 0, -Altezza - 5, 0, 1, 1, False)
  '
  Dim ag As Double
  Dim DG As Double
  Dim jj As Integer
  Dim NN As Integer
  Dim Xc As Double
  Dim YC As Double
  '
  Dim DDX1 As Double
  Dim DDX2 As Double
  Dim DDY1 As Double
  Dim DDY2 As Double
  '
  DDX1 = Larghezza / 2 - Raggio * (1 + Sin(CONICITA)): DDY1 = DDX1 * Tan(CONICITA)
  DDX2 = Larghezza / 2 - Raggio * (1 - Sin(CONICITA)): DDY2 = DDX2 * Tan(CONICITA)
  '
  Call linea(-Larghezza / 2 - 5, -Altezza, -Larghezza / 2, -Altezza, 0, 1, 0, False)
  Call linea(-Larghezza / 2, -Altezza, -Larghezza / 2, DDY1 - Raggio * Cos(CONICITA), 0, 1, 0, False)
  Call TestoAll(-Larghezza / 2, -Altezza, "F1", , True, 4)
  '
  NN = 20
  DG = (PG / 2 + CONICITA) / NN
  Xc = -Larghezza / 2 + Raggio: YC = DDY1 - Raggio * Cos(CONICITA)
  Call TestoAll(Xc, YC, "r=" & frmt(Raggio, 4), , True, 2)
  For j = 0 To NN - 1
    Call linea(Xc - Raggio * Cos(DG * j), YC + Raggio * Sin(DG * j), Xc - Raggio * Cos(DG * (j + 1)), YC + Raggio * Sin(DG * (j + 1)), 0, 1, 0, False)
  Next j
  '
  Call linea(-Larghezza / 2, DDY1 - Raggio * Cos(CONICITA), -DDX1, DDY1, 0, 1, 0, False)
  Call linea(-DDX1, DDY1, DDX2, -DDY2, 0, 1, 0, False)
  Call linea(DDX2, -DDY2, Larghezza / 2, -DDY2 - Raggio * Cos(CONICITA), 0, 1, 0, False)
  '
  NN = 20
  DG = (PG / 2 - CONICITA) / NN
  Xc = Larghezza / 2 - Raggio: YC = -DDY2 - Raggio * Cos(CONICITA)
  Call TestoAll(Xc, YC, "r=" & frmt(Raggio, 4), , True, 1)
  For j = 0 To NN - 1
    Call linea(Xc + Raggio * Cos(DG * j), YC + Raggio * Sin(DG * j), Xc + Raggio * Cos(DG * (j + 1)), YC + Raggio * Sin(DG * (j + 1)), 0, 1, 0, False)
  Next j
  '
  Call linea(Larghezza / 2, -DDY2 - Raggio * Cos(CONICITA), Larghezza / 2, -Altezza, 0, 1, 0, False)
  Call linea(Larghezza / 2, -Altezza, Larghezza / 2 + 5, -Altezza, 0, 1, 0, False)
  '
  Call linea(-Larghezza / 2, DDY1 - Raggio * Cos(CONICITA), -Larghezza / 2 + Raggio, DDY1 - Raggio * Cos(CONICITA), QBColor(12), 1, 0, False)
  Call linea(-DDX1, DDY1, -Larghezza / 2 + Raggio, DDY1 - Raggio * Cos(CONICITA), QBColor(12), 1, 0, False)
  Call linea(DDX2, -DDY2, Larghezza / 2 - Raggio, -DDY2 - Raggio * Cos(CONICITA), QBColor(12), 1, 0, False)
  Call linea(Larghezza / 2, -DDY2 - Raggio * Cos(CONICITA), Larghezza / 2 - Raggio, -DDY2 - Raggio * Cos(CONICITA), QBColor(12), 1, 0, False)
  Call TestoAll(Larghezza / 2, -Altezza, "F2", , True, 4)
  Call TestoAll(0, Altezza, "lm=" & frmt(Larghezza, 4), , True, 3)
  '
Exit Sub

errDisegna_Mola_RDE:
  If (Err.Number = 6) Or (Err.Number = 11) Then
    PAR.Pic.Cls
  Else
    WRITE_DIALOG "Error " & Err.Number & " (" & Err.Description & ") in Sub: Disegna_Mola_RDE"
  End If
  'Resume Next
  Exit Sub

End Sub

'---------------------------------------------------------------------------------------
' Procedura : Disegna_DenteIngranaggio
' Data e ora: 21/08/2007 10:51
' Autore    : Fil
' Note      :
'---------------------------------------------------------------------------------------

Public Sub Disegna_DenteIngranaggio(Optional Stampante As Boolean = False)
'
Dim Xc        As Double
Dim YC        As Double
Dim Z         As Double
Dim Mn        As Double
Dim ALFA      As Double
Dim ALFAt     As Double
Dim Beta      As Double
Dim DP        As Double
Dim DB        As Double
Dim DE        As Double
Dim DI        As Double
Dim Eb        As Double
Dim PassoDia  As Double
Dim npt       As Integer
Dim tCol      As Long
Dim tSpe      As Integer
Dim RT        As Double
Dim RF        As Double
Dim UB        As Integer
  '
  Call InizializzaVariabili
  UB = -1
  '
On Error Resume Next
  '
  UB = UBound(CalcoloEsterni.IngrX, 2)
  If UB = -1 Then Exit Sub
  '
On Error GoTo errDisegna_DenteIngranaggio
  '
Dim xe      As Double
Dim ye      As Double
Dim tDia    As Double
Dim i       As Integer
Dim j       As Integer
Dim SON     As Double
Dim SONt    As Double
Dim Scala   As Double
Dim DminEv  As Double
Dim DmaxEv  As Double
Dim Sx()    As Double
Dim Sy()    As Double
Dim Sx2()   As Double
Dim Sy2()   As Double
Dim Sc()    As Long
Dim Bnd     As Integer
Dim X1      As Double
Dim X2      As Double
Dim Y1      As Double
Dim Y2      As Double
  '
  npt = UB
  If npt = -1 Then Exit Sub
  npt = UB - 1
  '
ReDim Sx(2 * npt)
ReDim Sy(2 * npt)
ReDim Sc(2 * npt)
  '
  For i = 1 To npt
    Sx(i) = CalcoloEsterni.IngrX(1, i + 1)
    Sy(i) = CalcoloEsterni.IngrY(1, i + 1)
    Sc(i) = RGB(100, 180, 255)
  Next i
  '
  j = npt
  '
  For i = npt To 1 Step -1
    j = j + 1
    Sx(j) = -CalcoloEsterni.IngrX(2, i + 1)
    Sy(j) = CalcoloEsterni.IngrY(2, i + 1)
    Sc(j) = RGB(100, 180, 255)
  Next i
  '
  PAR.MaxX = Max(Sx, 1)
  PAR.MaxY = Max(Sy, 1)
  PAR.MinX = MIN(Sx, 1)
  PAR.MinY = MIN(Sy, 1)
  PAR.ScalaMargine = 0.45
  PAR.Allineamento = 1
  '
Dim FillX(1 To 4) As Double
Dim FillY(1 To 4) As Double
  '
  Call CalcolaParPic(PAR, True, Stampante)
  '
Dim ANGOLO As Double
  '
  Z = LEP("R[5]")
  Sx2 = Sx
  Sy2 = Sy
  '
  If Not Stampante Then
    'disegno pieno
    ANGOLO = -2 * PG / Z
    For j = 1 To 4
      For i = 0 To UBound(Sx)
        Call RotPnt(Sx2(i), Sy2(i), 0, 0, ANGOLO, X1, Y1)
        Sx2(i) = X1
        Sy2(i) = Y1
      Next i
      i = 1
      Call RotPnt(Sx2(UBound(Sx) - i + 1), Sy2(UBound(Sx) - i + 1), 0, 0, (-2 * PG / Z), X1, Y1)
      Call linea(Sx2(i), Sy2(i), X1, Y1, RGB(225, 240, 255), , , False)
      FillX(j) = (Sx2(i) + X1) / 2
      FillY(j) = (Sy2(i) + Y1) / 2
      i = (UBound(Sx) \ 2)
      Call RotPnt(Sx2(UBound(Sx) - i + 1), Sy2(UBound(Sx) - i + 1), 0, 0, (-2 * PG / Z), X1, Y1)
      Call linea(Sx2(i), Sy2(i), X1, Y1, RGB(225, 240, 255), , , False)
      FillX(j) = (FillX(j) + ((Sx2(i) + X1) / 2)) / 2
      FillY(j) = (FillY(j) + ((Sy2(i) + Y1) / 2)) / 2
      ANGOLO = 2 * PG / Z
    Next j
  End If
  'disegno contorno dente
  ANGOLO = -2 * PG / Z
  For j = 1 To 4
    For i = 0 To UBound(Sx)
      Call RotPnt(Sx(i), Sy(i), 0, 0, ANGOLO, X1, Y1)
      Sx(i) = X1
      Sy(i) = Y1
    Next i
    For i = 1 To UBound(Sx) - 1
      Call linea(Sx(i), Sy(i), Sx(i + 1), Sy(i + 1), Sc(i), 2)
    Next i
    ANGOLO = 2 * PG / Z
  Next j
  If Not Stampante Then
    For j = 1 To 4
      Call AreaFill(PAR.Pic, cx(FillX(j)), cy(FillY(j)), RGB(225, 240, 255))
    Next j
  End If
  '
  With CalcoloEsterni
    Call TestoAll(0, .DB / 2, "Db = " & frmt4(.DB), , , IIf(.DB >= .DI, 3, 4))
    Call Cerchio(0, 0, .DB / 2, vbGreen, , vbDot)
    Call TestoAll(0, .DP / 2, "Dp = " & frmt4(.DP), , , 3)
    Call Cerchio(0, 0, .DP / 2, vbBlue, , vbDot)
    tCol = CalcoloColore("R[45]")
    tSpe = IIf(tCol = vbRed, 2, 1)
    Call TestoAll(0, .DI / 2, "Di = " & frmt4(.DI), , , IIf(.DB >= .DI, 4, 3))
    Call Cerchio(0, 0, .DI / 2, tCol, tSpe, vbDot)
    tCol = CalcoloColore("DEXT_G;R[38]")
    tSpe = IIf(tCol = vbRed, 2, 1)
    Call TestoAll(0, .DE / 2, "De = " & frmt4(.DE), , , 3)
    Call Cerchio(0, 0, .DE / 2, tCol, tSpe, vbDot)
  End With
  '
  Call Disegna_Mola_INGRANAGGI(Stampante)
  '
On Error GoTo 0
Exit Sub

errDisegna_DenteIngranaggio:
  If (Err.Number = 6) Or (Err.Number = 11) Then
    PAR.Pic.Cls
  Else
    WRITE_DIALOG "Error " & Err.Number & " (" & Err.Description & ") in Sub: Disegna_DenteIngranaggio"
  End If
  'Resume Next
  Exit Sub

End Sub

'---------------------------------------------------------------------------------------
' Procedura : Disegna_DenteIngranaggio_MOLAVITE
' Data e ora:
' Autore    :
' Note      :
'---------------------------------------------------------------------------------------
Public Sub Disegna_DenteIngranaggio_MOLAVITE(Optional Stampante As Boolean = False)
'
Dim Xc        As Double
Dim YC        As Double
Dim Z         As Double
Dim Mn        As Double
Dim ALFA      As Double
Dim ALFAt     As Double
Dim Beta      As Double
Dim DP        As Double
Dim DB        As Double
Dim DE        As Double
Dim DI        As Double
Dim Eb        As Double
Dim PassoDia  As Double
Dim npt       As Integer
Dim tCol      As Long
Dim tSpe      As Integer
Dim RT        As Double
Dim RF        As Double
Dim UB        As Integer
'
Dim xe      As Double
Dim ye      As Double
Dim tDia    As Double
Dim i       As Integer
Dim j       As Integer
Dim SON     As Double
Dim SONt    As Double
Dim Scala   As Double
Dim DminEv  As Double
Dim DmaxEv  As Double
Dim Sx()    As Double
Dim Sy()    As Double
Dim Sx2()   As Double
Dim Sy2()   As Double
Dim Sc()    As Long
Dim Bnd     As Integer
Dim X1      As Double
Dim X2      As Double
Dim Y1      As Double
Dim Y2      As Double
Dim iTESTA  As Integer
Dim nriga     As Long
Dim nstr      As Long
Dim riga      As String
  
Dim FillX(1 To 4) As Double
Dim FillY(1 To 4) As Double
Dim ANGOLO As Double
  
Dim BASE      As Double
Dim Altezza   As Double
  
Dim Xrif As Double
Dim Yrif As Double
  
'
  
On Error GoTo errDisegna_DenteIngranaggio_MOLAVITE
  
  Call InizializzaVariabili
  UB = -1
  '
  '
  UB = CalcoloEsterni.Np
  If UB = -1 Then Exit Sub
  '
  '
  
  '
  npt = UB
  If npt = -1 Then Exit Sub
  '
ReDim Sx(2 * npt)
ReDim Sy(2 * npt)
ReDim Sc(2 * npt)
  
  '
  iTESTA = Lp("INDTAV_G")
  '
  For i = 1 To npt
    Sx(i) = CalcoloEsterni.IngrX(1, i)
    Sy(i) = CalcoloEsterni.IngrY(1, i)
    Sc(i) = RGB(100, 180, 255)
  Next i
  '
  j = npt
  For i = npt To 1 Step -1
    j = j + 1
    Sx(j) = -CalcoloEsterni.IngrX(2, i)
    Sy(j) = CalcoloEsterni.IngrY(2, i)
    Sc(j) = RGB(100, 180, 255)
  Next i
  '
  PAR.MaxX = Max(Sx, 1)
  PAR.MaxY = Max(Sy, 1)
  PAR.MinX = MIN(Sx, 1)
  PAR.MinY = MIN(Sy, 1)
  PAR.ScalaMargine = 0.35
  PAR.Allineamento = 3
  '
  '
  Call CalcolaParPic(PAR, True, Stampante)
  '
  '
  Z = LEP("NUMDENTI_G")
  '
  Sx2 = Sx:  Sy2 = Sy
  If Not Stampante Then
    'disegno pieno
    ANGOLO = -2 * PG / Z
    For j = 1 To 4
      For i = 0 To UBound(Sx)
        Call RotPnt(Sx2(i), Sy2(i), 0, 0, ANGOLO, X1, Y1)
        Sx2(i) = X1
        Sy2(i) = Y1
      Next i
      i = 1
      Call RotPnt(Sx2(UBound(Sx) - i + 1), Sy2(UBound(Sx) - i + 1), 0, 0, (-2 * PG / Z), X1, Y1)
      Call linea(Sx2(i), Sy2(i), X1, Y1, RGB(225, 240, 255), , , False)
      FillX(j) = (Sx2(i) + X1) / 2
      FillY(j) = (Sy2(i) + Y1) / 2
      i = (UBound(Sx) / 2)
      Call RotPnt(Sx2(UBound(Sx) - i + 1), Sy2(UBound(Sx) - i + 1), 0, 0, (-2 * PG / Z), X1, Y1)
      Call linea(Sx2(i), Sy2(i), X1, Y1, RGB(225, 240, 255), , , False)
      FillX(j) = (FillX(j) + ((Sx2(i) + X1) / 2)) / 2
      FillY(j) = (FillY(j) + ((Sy2(i) + Y1) / 2)) / 2
      ANGOLO = 2 * PG / Z
    Next j
  End If
  '
  'disegno contorno dente
  ANGOLO = -2 * PG / Z
  For j = 1 To 4
    For i = 1 To UBound(Sx)
      Call RotPnt(Sx(i), Sy(i), 0, 0, ANGOLO, X1, Y1)
      Sx(i) = X1: Sy(i) = Y1
    Next i
    For i = 1 To UBound(Sx) - 1
      Call linea(Sx(i), Sy(i), Sx(i + 1), Sy(i + 1), Sc(i), 2)
    Next i
    ANGOLO = 2 * PG / Z
  Next j
  '
  If Not Stampante Then
    For j = 1 To 4
      Call AreaFill(PAR.Pic, cx(FillX(j)), cy(FillY(j)), RGB(225, 240, 255))
    Next j
  End If
  '
  With CalcoloEsterni
    Call TestoAll(0, .DB / 2, "Db = " & frmt4(.DB), , , IIf(.DB >= .DI, 3, 4))
    Call Cerchio(0, 0, .DB / 2, vbGreen, , vbDot)
    Call TestoAll(0, .DP / 2, "d = " & frmt4(.DP), , , 3)
    Call Cerchio(0, 0, .DP / 2, vbBlue, , vbDot)
    tCol = CalcoloColore("DINT_G")
    tSpe = IIf(tCol = vbRed, 2, 1)
    Call TestoAll(0, .DI / 2, "df = " & frmt4(.DI), , , IIf(.DB >= .DI, 4, 3))
    Call Cerchio(0, 0, .DI / 2, tCol, tSpe, vbDot)
    tCol = CalcoloColore("DEXT_G;R[38]")
    tSpe = IIf(tCol = vbRed, 2, 1)
    Call TestoAll(0, .DE / 2, "da = " & frmt4(.DE), , , 3)
    Call Cerchio(0, 0, .DE / 2, tCol, tSpe, vbDot)
  End With
  '
  'SCHEMA DELLA MOLA A VITE
  '
  'BASE = LEP("LARGHMOLA_G")
  BASE = CalcoloEsterni.IngrX(1, 1) * 2
  'Altezza = LEP("DIAMMOLA_G")
  Altezza = CalcoloEsterni.IngrY(1, 1) - CalcoloEsterni.IngrY(1, npt)
  '
  '
  Xrif = 0
  Yrif = CalcoloEsterni.DE / 2 + Altezza
  '
  j = LEP("SENSOFIL_G")
  If (j = 0) Then j = 1 Else j = -1
  i = LEP("NUMPRINC_G")
  Call TestoAll(Xrif, Yrif, "gg = " & Format$(i, "##0"), , , 3)
  Call linea(Xrif - BASE / 2, Yrif + Altezza / 2, Xrif + BASE / 2, Yrif + Altezza / 2, vbRed, 2)
  Call linea(Xrif - BASE / 2, Yrif - Altezza / 2, Xrif + BASE / 2, Yrif - Altezza / 2, vbRed, 2)
  Call linea(Xrif - BASE / 2, Yrif + Altezza / 2, Xrif - BASE / 2, Yrif - Altezza / 2, vbRed, 2)
  Call linea(Xrif + BASE / 2, Yrif + Altezza / 2, Xrif + BASE / 2, Yrif - Altezza / 2, vbRed, 2)
  '
  Select Case i
    Case 1
      Call linea(Xrif - BASE / 2 * 1, Yrif + j * Altezza / 2, Xrif + BASE / 2 * 1, Yrif - j * Altezza / 2, vbRed, 2)
    Case 2
      Call linea(Xrif - BASE / 2 * 1, Yrif + j * Altezza / 2, Xrif + BASE / 2 * 0, Yrif - j * Altezza / 2, vbRed, 2)
      Call linea(Xrif + BASE / 2 * 0, Yrif + j * Altezza / 2, Xrif + BASE / 2 * 1, Yrif - j * Altezza / 2, vbRed, 2)
    Case 3
      Call linea(Xrif - BASE / 6 * 3, Yrif + j * Altezza / 2, Xrif - BASE / 6 * 1, Yrif - j * Altezza / 2, vbRed, 2)
      Call linea(Xrif - BASE / 6 * 1, Yrif + j * Altezza / 2, Xrif + BASE / 6 * 1, Yrif - j * Altezza / 2, vbRed, 2)
      Call linea(Xrif + BASE / 6 * 1, Yrif + j * Altezza / 2, Xrif + BASE / 6 * 3, Yrif - j * Altezza / 2, vbRed, 2)
    Case 4
      Call linea(Xrif - BASE / 8 * 4, Yrif + j * Altezza / 2, Xrif - BASE / 8 * 2, Yrif - j * Altezza / 2, vbRed, 2)
      Call linea(Xrif - BASE / 8 * 2, Yrif + j * Altezza / 2, Xrif + BASE / 8 * 0, Yrif - j * Altezza / 2, vbRed, 2)
      Call linea(Xrif + BASE / 8 * 0, Yrif + j * Altezza / 2, Xrif + BASE / 8 * 2, Yrif - j * Altezza / 2, vbRed, 2)
      Call linea(Xrif + BASE / 8 * 2, Yrif + j * Altezza / 2, Xrif + BASE / 8 * 4, Yrif - j * Altezza / 2, vbRed, 2)
    Case 5
      Call linea(Xrif - BASE / 10 * 5, Yrif + j * Altezza / 2, Xrif - BASE / 10 * 3, Yrif - j * Altezza / 2, vbRed, 2)
      Call linea(Xrif - BASE / 10 * 3, Yrif + j * Altezza / 2, Xrif - BASE / 10 * 1, Yrif - j * Altezza / 2, vbRed, 2)
      Call linea(Xrif - BASE / 10 * 1, Yrif + j * Altezza / 2, Xrif + BASE / 10 * 1, Yrif - j * Altezza / 2, vbRed, 2)
      Call linea(Xrif + BASE / 10 * 1, Yrif + j * Altezza / 2, Xrif + BASE / 10 * 3, Yrif - j * Altezza / 2, vbRed, 2)
      Call linea(Xrif + BASE / 10 * 3, Yrif + j * Altezza / 2, Xrif + BASE / 10 * 5, Yrif - j * Altezza / 2, vbRed, 2)
    Case 6
      Call linea(Xrif - BASE / 12 * 6, Yrif + j * Altezza / 2, Xrif - BASE / 12 * 4, Yrif - j * Altezza / 2, vbRed, 2)
      Call linea(Xrif - BASE / 12 * 4, Yrif + j * Altezza / 2, Xrif - BASE / 12 * 2, Yrif - j * Altezza / 2, vbRed, 2)
      Call linea(Xrif - BASE / 12 * 2, Yrif + j * Altezza / 2, Xrif + BASE / 12 * 0, Yrif - j * Altezza / 2, vbRed, 2)
      Call linea(Xrif + BASE / 12 * 0, Yrif + j * Altezza / 2, Xrif + BASE / 12 * 2, Yrif - j * Altezza / 2, vbRed, 2)
      Call linea(Xrif + BASE / 12 * 2, Yrif + j * Altezza / 2, Xrif + BASE / 12 * 4, Yrif - j * Altezza / 2, vbRed, 2)
      Call linea(Xrif + BASE / 12 * 4, Yrif + j * Altezza / 2, Xrif + BASE / 12 * 6, Yrif - j * Altezza / 2, vbRed, 2)
    Case Is >= 7
      Call linea(Xrif - BASE / 14 * 7, Yrif + j * Altezza / 2, Xrif - BASE / 14 * 5, Yrif - j * Altezza / 2, vbRed, 2)
      Call linea(Xrif - BASE / 14 * 5, Yrif + j * Altezza / 2, Xrif - BASE / 14 * 3, Yrif - j * Altezza / 2, vbRed, 2)
      Call linea(Xrif - BASE / 14 * 3, Yrif + j * Altezza / 2, Xrif - BASE / 14 * 1, Yrif - j * Altezza / 2, vbRed, 2)
      Call linea(Xrif - BASE / 14 * 1, Yrif + j * Altezza / 2, Xrif + BASE / 14 * 1, Yrif - j * Altezza / 2, vbRed, 2)
      Call linea(Xrif + BASE / 14 * 1, Yrif + j * Altezza / 2, Xrif + BASE / 14 * 3, Yrif - j * Altezza / 2, vbRed, 2)
      Call linea(Xrif + BASE / 14 * 3, Yrif + j * Altezza / 2, Xrif + BASE / 14 * 5, Yrif - j * Altezza / 2, vbRed, 2)
      Call linea(Xrif + BASE / 14 * 5, Yrif + j * Altezza / 2, Xrif + BASE / 14 * 7, Yrif - j * Altezza / 2, vbRed, 2)
    Case Else
     'NON DISEGNO NIENTE
  End Select
  '
  '
  If (iTESTA = 1) Then
     nriga = 1532
     nstr = LoadString(g_hLanguageLibHandle, nriga, riga, 255)
     If nstr = 0 Then
        riga = "Grinding on C2"
     End If
    Call TestoInfo(riga, 2, True, vbRed)
  Else
     nriga = 1533
     nstr = LoadString(g_hLanguageLibHandle, nriga, riga, 255)
     If nstr = 0 Then
        riga = "Grinding on C1"
     End If
    Call TestoInfo(riga, 2, True, vbRed)
  End If
  '

Exit Sub

errDisegna_DenteIngranaggio_MOLAVITE:
  
  If (Err.Number = 6) Or (Err.Number = 11) Then
    PAR.Pic.Cls
  End If
  
  WRITE_DIALOG "Error " & Err.Number & " (" & Err.Description & ") in Sub: Disegna_DenteIngranaggio_MOLAVITE"
  
  Exit Sub

End Sub

Public Sub Disegna_PARAMETRI_DENTATURA(Optional Stampante As Boolean = False)
'
Dim UB       As Integer
  '
  Call InizializzaVariabili
  '
On Error GoTo errDisegna_PARAMETRI_DENTATURA
  '
Dim i       As Integer
Dim j       As Integer
Dim iTESTA  As Integer
  '
  PAR.MaxY = 200: PAR.MinY = 0
  PAR.MaxX = 100: PAR.MinX = 0
  '
  PAR.ScalaMargine = 1
  PAR.Allineamento = 0
  '
  Call CalcolaParPic(PAR, True, Stampante)
  '
  PAR.MargineDxPic = PAR.Pic.TextWidth("Z=888.888")
  PAR.MargineSxPic = PAR.Pic.TextWidth("Z=888.888")
  '
  iTESTA = Lp("INDTAV_G")
  '
  'SCHEMA DELLA MOLA A VITE
  Dim BASE      As Double
  Dim Altezza   As Double
  '
  BASE = 70
  Altezza = 35
  '
  Dim Xrif As Double
  Dim Yrif As Double
  '
  Xrif = 75
  Yrif = 100 + Altezza
  '
  j = LEP("SENSOFIL_G")
  If (j = 0) Then j = 1 Else j = -1
  i = LEP("NUMPRINC_G")
  Call TestoAll(Xrif, Yrif, "gg = " & Format$(i, "##0"), , , 3)
  Call linea(Xrif - BASE / 2, Yrif + Altezza / 2, Xrif + BASE / 2, Yrif + Altezza / 2, vbRed, 2)
  Call linea(Xrif - BASE / 2, Yrif - Altezza / 2, Xrif + BASE / 2, Yrif - Altezza / 2, vbRed, 2)
  Call linea(Xrif - BASE / 2, Yrif + Altezza / 2, Xrif - BASE / 2, Yrif - Altezza / 2, vbRed, 2)
  Call linea(Xrif + BASE / 2, Yrif + Altezza / 2, Xrif + BASE / 2, Yrif - Altezza / 2, vbRed, 2)
  '
  Select Case i
    Case 1
      Call linea(Xrif - BASE / 2 * 1, Yrif + j * Altezza / 2, Xrif + BASE / 2 * 1, Yrif - j * Altezza / 2, vbRed, 2)
    Case 2
      Call linea(Xrif - BASE / 2 * 1, Yrif + j * Altezza / 2, Xrif + BASE / 2 * 0, Yrif - j * Altezza / 2, vbRed, 2)
      Call linea(Xrif + BASE / 2 * 0, Yrif + j * Altezza / 2, Xrif + BASE / 2 * 1, Yrif - j * Altezza / 2, vbRed, 2)
    Case 3
      Call linea(Xrif - BASE / 6 * 3, Yrif + j * Altezza / 2, Xrif - BASE / 6 * 1, Yrif - j * Altezza / 2, vbRed, 2)
      Call linea(Xrif - BASE / 6 * 1, Yrif + j * Altezza / 2, Xrif + BASE / 6 * 1, Yrif - j * Altezza / 2, vbRed, 2)
      Call linea(Xrif + BASE / 6 * 1, Yrif + j * Altezza / 2, Xrif + BASE / 6 * 3, Yrif - j * Altezza / 2, vbRed, 2)
    Case 4
      Call linea(Xrif - BASE / 8 * 4, Yrif + j * Altezza / 2, Xrif - BASE / 8 * 2, Yrif - j * Altezza / 2, vbRed, 2)
      Call linea(Xrif - BASE / 8 * 2, Yrif + j * Altezza / 2, Xrif + BASE / 8 * 0, Yrif - j * Altezza / 2, vbRed, 2)
      Call linea(Xrif + BASE / 8 * 0, Yrif + j * Altezza / 2, Xrif + BASE / 8 * 2, Yrif - j * Altezza / 2, vbRed, 2)
      Call linea(Xrif + BASE / 8 * 2, Yrif + j * Altezza / 2, Xrif + BASE / 8 * 4, Yrif - j * Altezza / 2, vbRed, 2)
    Case 5
      Call linea(Xrif - BASE / 10 * 5, Yrif + j * Altezza / 2, Xrif - BASE / 10 * 3, Yrif - j * Altezza / 2, vbRed, 2)
      Call linea(Xrif - BASE / 10 * 3, Yrif + j * Altezza / 2, Xrif - BASE / 10 * 1, Yrif - j * Altezza / 2, vbRed, 2)
      Call linea(Xrif - BASE / 10 * 1, Yrif + j * Altezza / 2, Xrif + BASE / 10 * 1, Yrif - j * Altezza / 2, vbRed, 2)
      Call linea(Xrif + BASE / 10 * 1, Yrif + j * Altezza / 2, Xrif + BASE / 10 * 3, Yrif - j * Altezza / 2, vbRed, 2)
      Call linea(Xrif + BASE / 10 * 3, Yrif + j * Altezza / 2, Xrif + BASE / 10 * 5, Yrif - j * Altezza / 2, vbRed, 2)
    Case 6
      Call linea(Xrif - BASE / 12 * 6, Yrif + j * Altezza / 2, Xrif - BASE / 12 * 4, Yrif - j * Altezza / 2, vbRed, 2)
      Call linea(Xrif - BASE / 12 * 4, Yrif + j * Altezza / 2, Xrif - BASE / 12 * 2, Yrif - j * Altezza / 2, vbRed, 2)
      Call linea(Xrif - BASE / 12 * 2, Yrif + j * Altezza / 2, Xrif + BASE / 12 * 0, Yrif - j * Altezza / 2, vbRed, 2)
      Call linea(Xrif + BASE / 12 * 0, Yrif + j * Altezza / 2, Xrif + BASE / 12 * 2, Yrif - j * Altezza / 2, vbRed, 2)
      Call linea(Xrif + BASE / 12 * 2, Yrif + j * Altezza / 2, Xrif + BASE / 12 * 4, Yrif - j * Altezza / 2, vbRed, 2)
      Call linea(Xrif + BASE / 12 * 4, Yrif + j * Altezza / 2, Xrif + BASE / 12 * 6, Yrif - j * Altezza / 2, vbRed, 2)
    Case Is >= 7
      Call linea(Xrif - BASE / 14 * 7, Yrif + j * Altezza / 2, Xrif - BASE / 14 * 5, Yrif - j * Altezza / 2, vbRed, 2)
      Call linea(Xrif - BASE / 14 * 5, Yrif + j * Altezza / 2, Xrif - BASE / 14 * 3, Yrif - j * Altezza / 2, vbRed, 2)
      Call linea(Xrif - BASE / 14 * 3, Yrif + j * Altezza / 2, Xrif - BASE / 14 * 1, Yrif - j * Altezza / 2, vbRed, 2)
      Call linea(Xrif - BASE / 14 * 1, Yrif + j * Altezza / 2, Xrif + BASE / 14 * 1, Yrif - j * Altezza / 2, vbRed, 2)
      Call linea(Xrif + BASE / 14 * 1, Yrif + j * Altezza / 2, Xrif + BASE / 14 * 3, Yrif - j * Altezza / 2, vbRed, 2)
      Call linea(Xrif + BASE / 14 * 3, Yrif + j * Altezza / 2, Xrif + BASE / 14 * 5, Yrif - j * Altezza / 2, vbRed, 2)
      Call linea(Xrif + BASE / 14 * 5, Yrif + j * Altezza / 2, Xrif + BASE / 14 * 7, Yrif - j * Altezza / 2, vbRed, 2)
    Case Else
     'NON DISEGNO NIENTE
  End Select
  '
  Dim ARGOMENTO              As Double
  Dim INCLINAZIONE_MOLA_GRD  As Double
  Dim INCLINAZIONE_MOLA_RAD  As Double
  Dim MODULO_MOLA            As Double
  Dim PRINCIPI_MOLA          As Double
  Dim DIAMETRO_MOLA          As Double
  Dim SENSO_FILETTO          As Integer
  Dim TIPO_INCLINAZIONE      As Integer
  Dim TAGLIENTI              As Integer
  '
  MODULO_MOLA = Lp("MNUT_G")
  PRINCIPI_MOLA = Lp("NUMPRINC_G")
  DIAMETRO_MOLA = Lp("DIAMMOLA_G")
  INCLINAZIONE_MOLA_GRD = Lp("INCLMOLA_G")
  SENSO_FILETTO = Lp("SENSOFIL_G")
  TIPO_INCLINAZIONE = Lp("iDefIN")
  TAGLIENTI = Lp("TAGLIENTI")
  '
  Select Case TIPO_INCLINAZIONE
  
    Case 1
      'CALCOLO DIAMETRO PRIMITIVO
      If (INCLINAZIONE_MOLA_GRD <> 0) Then
        INCLINAZIONE_MOLA_RAD = INCLINAZIONE_MOLA_GRD / 180 * PG                 'RADIANTI
        DIAMETRO_MOLA = MODULO_MOLA * PRINCIPI_MOLA / Sin(INCLINAZIONE_MOLA_RAD)
        Call SP("DIAMMOLA_G", frmt(DIAMETRO_MOLA, 4))
      End If
    
    Case Else
      'CALCOLO INCLINAZIONE FILETTO
      If (DIAMETRO_MOLA <> 0) Then
        ARGOMENTO = MODULO_MOLA * PRINCIPI_MOLA / DIAMETRO_MOLA
        INCLINAZIONE_MOLA_RAD = Atn(ARGOMENTO / Sqr(1 - ARGOMENTO * ARGOMENTO))  'RADIANTI
        INCLINAZIONE_MOLA_GRD = INCLINAZIONE_MOLA_RAD / PG * 180                 'GRADI
        Call SP("INCLMOLA_G", frmt(INCLINAZIONE_MOLA_GRD, 4))
      End If
    
  End Select
  '
  Dim Pn As Double
  Dim Pa As Double
  '
  Pn = MODULO_MOLA * PG
  Pa = (Pn / Cos(INCLINAZIONE_MOLA_RAD)) * PRINCIPI_MOLA
  '
  Call TestoInfo("Pn=" & frmt(Pn, 4), 1, True, vbBlack)
  Call TestoInfo("Pa=" & frmt(Pa, 4), 1, True, vbBlack)
  Call TestoInfo("N=" & Format$(TAGLIENTI, "###0"), 1, True, vbBlack)
  Call TestoInfo("Pn/N=" & frmt(Pn / TAGLIENTI, 4), 1, True, vbBlack)
  '
  If (iTESTA = 1) Then
    Call TestoInfo("WORKING on C2", 2, True, vbRed)
  Else
    Call TestoInfo("WORKING on C1", 2, True, vbRed)
  End If
  '
On Error GoTo 0
Exit Sub

errDisegna_PARAMETRI_DENTATURA:
  If (Err.Number = 6) Or (Err.Number = 11) Then
    PAR.Pic.Cls
  Else
    WRITE_DIALOG "Error " & Err.Number & " (" & Err.Description & ") in Sub: Disegna_PARAMETRI_DENTATURA"
  End If
  'Resume Next
  Exit Sub

End Sub

'---------------------------------------------------------------------------------------
' Procedura : Disegna_ZONA_LAVORO_INGRANAGGI_INTERNI
' Data e ora: 21/02/2009 10:51
' Autore    : SVG
' Note      :
'---------------------------------------------------------------------------------------

Public Sub Disegna_ZONA_LAVORO_INGRANAGGI_INTERNI(Optional Stampante As Boolean = False)
'
Dim nZone As Integer
Dim Ft    As Double
Dim F1    As Double
Dim F2    As Double
Dim F3    As Double
Dim F1_2  As Double
Dim F2_2  As Double
Dim F3_2  As Double
'
Dim RS              As Double
Dim LS              As Double
Dim DiaMola         As Double
Dim AngMola         As Double
Dim ExtracorsaIn    As Double
Dim ExtracorsaOut   As Double
Dim TrattoDivisione As Double
Dim tColore         As Long
Dim tX1             As Double
Dim tX              As Double
Dim tY              As Double
Dim i               As Integer
Dim NotScale        As Boolean
Dim QuotaIL         As Double
Dim QuotaFL         As Double
Dim XF0             As Double
Dim XF1             As Double
Dim XF2             As Double
Dim XF3             As Double
Dim XF0_2           As Double
Dim XF1_2           As Double
Dim XF2_2           As Double
Dim XF3_2           As Double
Dim tScala          As Double
Dim YQuota          As Double
Dim DiaMolaMin      As Double
Dim ALTEZZA_PROFILO As Double
Dim QZI             As Double
Dim SICUREZZA       As Double
'
  Call InizializzaVariabili
  '
  NotScale = False
  'Lettura parametri ingranaggio
  nZone = Lp("iNzone")
  DiaMola = LEP("iDIAMOLA")
  ALTEZZA_PROFILO = (LEP("R[45]") - LEP("R[38]")) / 2
  DiaMolaMin = LEP("DIAMOLA_G[0,1]")
  If (DiaMolaMin <= 0) Then DiaMolaMin = LEP("DIAMOLA_G[0,2]")
  AngMola = Lp("R[258]")
  ExtracorsaIn = Lp("CORSAIN")   'lato testa
  ExtracorsaOut = Lp("CORSAOUT") 'lato contropunta
  TrattoDivisione = Lp("R[130]")
  QZI = Lp("QASSIALESTART_G")
  SICUREZZA = Lp("R[112]")
  '
  F1 = 0:   F2 = 0:   F3 = 0
  F1_2 = 0: F2_2 = 0: F3_2 = 0
  Select Case nZone
    Case 1
      F1 = Lp("Fascia1F1")
      F1_2 = Lp("Fascia1F2")
    Case 2
      F1 = Lp("Fascia1F1")
      F2 = Lp("Fascia2F1")
      F1_2 = Lp("Fascia1F2")
      F2_2 = Lp("Fascia2F2")
    Case 3
      F1 = Lp("Fascia1F1")
      F2 = Lp("Fascia2F1")
      F3 = Lp("Fascia3F1")
      F1_2 = Lp("Fascia1F2")
      F2_2 = Lp("Fascia2F2")
      F3_2 = Lp("Fascia3F2")
  End Select
  Ft = F1 + F2 + F3 'Fascia Totale
  LS = 0 'Lunghezza spina sporgente
  If Ft = 0 Then
    NotScale = True
    F1 = 50
    F1_2 = 50
    Ft = 50
  End If
  '
  PAR.MaxX = 6 * LS + Ft
  PAR.MinX = -2 * LS
  PAR.MaxY = 0
  PAR.MinY = 0
  PAR.ScalaMargine = 1
  PAR.Allineamento = 0
  
  Call CalcolaParPic(PAR, True, Stampante)
  
  PAR.MargineDxPic = PAR.Pic.TextWidth("Z=888.888")
  PAR.MargineSxPic = PAR.Pic.TextWidth("Z=888.888")
  Dim XMola As Double
  Dim Ymola As Double
  
  PAR.MaxX = (2 * LS + Ft + ExtracorsaOut + TrattoDivisione + SICUREZZA)
  PAR.MinX = -ExtracorsaIn
  PAR.MaxY = 0
  PAR.MinY = 0
  PAR.ScalaMargine = 1
  PAR.Allineamento = 0
  Call CalcolaParPic(PAR, True, Stampante)
  '
  XMola = LS + Ft + ExtracorsaOut
  Ymola = 0
  '
  tScala = PAR.Scala
  If PAR.Pic Is Printer Then tScala = tScala * 45
  '
  QuotaFL = QZI - Ft - ExtracorsaIn
  QuotaIL = QZI + ExtracorsaOut
  '
  YQuota = 350 / tScala
  If (Lp("iMetDiv") = 2) Then
    XMola = XMola + TrattoDivisione
    Call QuotaH(XMola - TrattoDivisione, YQuota, XMola, YQuota, "" & TrattoDivisione, 12, , 3)
    QuotaIL = QZI + ExtracorsaOut + TrattoDivisione
  End If
  Call TestoAll(XMola - TrattoDivisione - ExtracorsaIn - ExtracorsaOut - Ft, YQuota, "Z=" & QuotaFL, , True, 1)
  '
  If (SICUREZZA > 0) Then Call QuotaH(XMola, YQuota, XMola + SICUREZZA, YQuota, "" & SICUREZZA, 12, , 3)
  '
  Dim X1 As Double
  Dim Y1 As Double
  '
  X1 = XMola - DiaMola / 2
  Y1 = -ALTEZZA_PROFILO - DiaMola / 2
  '
  Call TestoAll(XMola, Y1, "Z=" & QuotaIL, , True, 4)
  If (SICUREZZA > 0) Then Call linea(XMola + SICUREZZA, Y1, XMola + SICUREZZA, -Y1, vbRed, 1, vbDash)
  Call linea(XMola, Y1, XMola, -Y1, vbRed, 1, vbDash)
  Call Cerchio(XMola, Y1, DiaMola / 2, vbBlue, 2, 3)
  '
  Call linea(X1, Y1 + DiaMolaMin / 2, XMola + 20, Y1 + DiaMolaMin / 2, vbBlack, 1)
  Call linea(X1, Y1 - DiaMolaMin / 2, XMola + 20, Y1 - DiaMolaMin / 2, vbBlack, 1)
  Call linea(X1, Y1 + DiaMolaMin / 2, X1, Y1 - DiaMolaMin / 2, vbBlack, 1)
  '
  tX1 = ExtracorsaIn / 2
  tX1 = XMola - tX1
  'Zone
  XF0 = LS:   XF1 = XF0 + F1:       XF2 = XF1 + F2:        XF3 = XF2 + F3
  XF0_2 = LS: XF1_2 = XF0_2 + F1_2: XF2_2 = XF1_2 + F2_2:  XF3_2 = XF2_2 + F3_2
  Call linea(LS, 0, LS + Ft, 0, vbBlue, 2)
  Call linea(LS, -ALTEZZA_PROFILO, LS + Ft, -ALTEZZA_PROFILO, vbRed, 2)
  Call linea(LS, 0, LS, -50, vbBlack, 1)
  Call linea(LS + Ft, 0, LS + Ft, -50, vbBlack, 1)
  '
  YQuota = 0 + 350 / tScala
  Call QuotaH(LS + Ft, YQuota, LS + Ft + ExtracorsaOut, YQuota, "" & ExtracorsaOut, 12, , 3)
  Call QuotaH(LS - ExtracorsaIn, YQuota, LS, YQuota, "" & ExtracorsaIn, 12, , 3)
  If Not NotScale Then
    Call QuotaH(LS, YQuota, LS + Ft, YQuota, "" & Ft, 12, , 3)
  Else
    Call QuotaH(LS, YQuota, LS + Ft, YQuota, "?", 12, vbRed, 3)
  End If
  '
  PAR.MargineDxPic = 0
  PAR.MargineSxPic = 0
  If NotScale Then Call TestoInfo("DRAWING NOT IN SCALE ! ", , True, vbRed, True)
  '
  Call TestoInfo("Units: [mm]", 3)
  '
End Sub

Public Sub Disegna_ZONA_LAVORO_PROFILO_PER_PUNTI_INTERNI(Optional Stampante As Boolean = False)
'
Dim nZone As Integer
Dim Ft    As Double
Dim F1    As Double
Dim F2    As Double
Dim F3    As Double
Dim F1_2  As Double
Dim F2_2  As Double
Dim F3_2  As Double
'
Dim RS              As Double
Dim LS              As Double
Dim DiaMola         As Double
Dim AngMola         As Double
Dim ExtracorsaIn    As Double
Dim ExtracorsaOut   As Double
Dim TrattoDivisione As Double
Dim tColore         As Long
Dim tX1             As Double
Dim tX              As Double
Dim tY              As Double
Dim i               As Integer
Dim NotScale        As Boolean
Dim QuotaIL         As Double
Dim QuotaFL         As Double
Dim XF0             As Double
Dim XF1             As Double
Dim XF2             As Double
Dim XF3             As Double
Dim XF0_2           As Double
Dim XF1_2           As Double
Dim XF2_2           As Double
Dim XF3_2           As Double
Dim tScala          As Double
Dim YQuota          As Double
Dim DiaMolaMin      As Double
Dim ALTEZZA_PROFILO As Double
Dim QZI             As Double
Dim SICUREZZA       As Double
'
  Call InizializzaVariabili
  '
  NotScale = False
  'Lettura parametri ingranaggio
  nZone = Lp("iNzone")
  DiaMola = LEP("iDIAMOLA")
  ALTEZZA_PROFILO = (LEP("R[45]") - LEP("R[38]")) / 2
  DiaMolaMin = LEP("DIAMOLA_G[0,1]")
  If (DiaMolaMin <= 0) Then DiaMolaMin = LEP("DIAMOLA_G[0,2]")
  AngMola = Lp("R[258]")
  ExtracorsaIn = Lp("CORSAIN")   'lato testa
  ExtracorsaOut = Lp("CORSAOUT") 'lato contropunta
  TrattoDivisione = Lp("R[130]")
  QZI = Lp("QASSIALESTART_G")
  SICUREZZA = Lp("R[112]")
  '
  F1 = 0:   F2 = 0:   F3 = 0
  F1_2 = 0: F2_2 = 0: F3_2 = 0
  Select Case nZone
    Case 1
      F1 = Lp("Fascia1F1")
      F1_2 = Lp("Fascia1F2")
    Case 2
      F1 = Lp("Fascia1F1")
      F2 = Lp("Fascia2F1")
      F1_2 = Lp("Fascia1F2")
      F2_2 = Lp("Fascia2F2")
    Case 3
      F1 = Lp("Fascia1F1")
      F2 = Lp("Fascia2F1")
      F3 = Lp("Fascia3F1")
      F1_2 = Lp("Fascia1F2")
      F2_2 = Lp("Fascia2F2")
      F3_2 = Lp("Fascia3F2")
  End Select
  Ft = F1 + F2 + F3 'Fascia Totale
  LS = 0 'Lunghezza spina sporgente
  If Ft = 0 Then
    NotScale = True
    F1 = 50
    F1_2 = 50
    Ft = 50
  End If
  '
  PAR.MaxX = 6 * LS + Ft
  PAR.MinX = -2 * LS
  PAR.MaxY = 0
  PAR.MinY = 0
  PAR.ScalaMargine = 1
  PAR.Allineamento = 0
  
  Call CalcolaParPic(PAR, True, Stampante)
  
  PAR.MargineDxPic = PAR.Pic.TextWidth("Z=888.888")
  PAR.MargineSxPic = PAR.Pic.TextWidth("Z=888.888")
  Dim XMola As Double
  Dim Ymola As Double
  
  PAR.MaxX = (2 * LS + Ft + ExtracorsaOut + TrattoDivisione + SICUREZZA)
  PAR.MinX = -ExtracorsaIn
  PAR.MaxY = 0
  PAR.MinY = 0
  PAR.ScalaMargine = 1
  PAR.Allineamento = 0
  Call CalcolaParPic(PAR, True, Stampante)
  '
  XMola = LS + Ft + ExtracorsaOut
  Ymola = 0
  '
  tScala = PAR.Scala
  If PAR.Pic Is Printer Then tScala = tScala * 45
  '
  QuotaFL = QZI - Ft - ExtracorsaIn
  QuotaIL = QZI + ExtracorsaOut
  '
  YQuota = 350 / tScala
  If (Lp("iMetDiv") = 2) Then
    XMola = XMola + TrattoDivisione
    Call QuotaH(XMola - TrattoDivisione, YQuota, XMola, YQuota, "" & TrattoDivisione, 12, , 3)
    QuotaIL = QZI + ExtracorsaOut + TrattoDivisione
  End If
  Call TestoAll(XMola - TrattoDivisione - ExtracorsaIn - ExtracorsaOut - Ft, YQuota, "Z=" & QuotaFL, , True, 1)
  '
  If (SICUREZZA > 0) Then Call QuotaH(XMola, YQuota, XMola + SICUREZZA, YQuota, "" & SICUREZZA, 12, , 3)
  '
  Dim X1 As Double
  Dim Y1 As Double
  '
  X1 = XMola - DiaMola / 2
  Y1 = -ALTEZZA_PROFILO - DiaMola / 2
  '
  Call TestoAll(XMola, Y1, "Z=" & QuotaIL, , True, 4)
  If (SICUREZZA > 0) Then Call linea(XMola + SICUREZZA, Y1, XMola + SICUREZZA, -Y1, vbRed, 1, vbDash)
  Call linea(XMola, Y1, XMola, -Y1, vbRed, 1, vbDash)
  Call Cerchio(XMola, Y1, DiaMola / 2, vbGreen, , 2)
  '
  Call linea(X1, Y1 + DiaMolaMin / 2, XMola + 20, Y1 + DiaMolaMin / 2, vbBlack, 1)
  Call linea(X1, Y1 - DiaMolaMin / 2, XMola + 20, Y1 - DiaMolaMin / 2, vbBlack, 1)
  Call linea(X1, Y1 + DiaMolaMin / 2, X1, Y1 - DiaMolaMin / 2, vbBlack, 1)
  '
  tX1 = ExtracorsaIn / 2
  tX1 = XMola - tX1
  'Zone
  XF0 = LS:   XF1 = XF0 + F1:       XF2 = XF1 + F2:        XF3 = XF2 + F3
  XF0_2 = LS: XF1_2 = XF0_2 + F1_2: XF2_2 = XF1_2 + F2_2:  XF3_2 = XF2_2 + F3_2
  Call linea(LS, 0, LS + Ft, 0, vbBlue, 2)
  Call linea(LS, -ALTEZZA_PROFILO, LS + Ft, -ALTEZZA_PROFILO, vbRed, 2)
  Call linea(LS, 0, LS, -50, vbBlack, 1)
  Call linea(LS + Ft, 0, LS + Ft, -50, vbBlack, 1)
  '
  YQuota = 0 + 350 / tScala
  Call QuotaH(LS + Ft, YQuota, LS + Ft + ExtracorsaOut, YQuota, "" & ExtracorsaOut, 12, , 3)
  Call QuotaH(LS - ExtracorsaIn, YQuota, LS, YQuota, "" & ExtracorsaIn, 12, , 3)
  If Not NotScale Then
    Call QuotaH(LS, YQuota, LS + Ft, YQuota, "" & Ft, 12, , 3)
  Else
    Call QuotaH(LS, YQuota, LS + Ft, YQuota, "?", 12, vbRed, 3)
  End If
  '
  PAR.MargineDxPic = 0
  PAR.MargineSxPic = 0
  If NotScale Then Call TestoInfo("DRAWING NOT IN SCALE ! ", , True, vbRed, True)
  '
  Call TestoInfo("Units: [mm]", 3)
  '
End Sub

Public Sub Disegna_ProfiloK_ESTERNI(Stampante As Boolean)
'
Dim Tif()       As Double
Dim BOMB()      As Double
Dim Rast()      As Double
Dim XRas()      As Double
Dim XcBo        As Double
Dim YcBo        As Double
Dim RBo         As Double
Dim tL          As Double
Dim tBo         As Double
'
Dim tTesto      As String
Dim tColore     As Long
'
Dim TifDia      As Integer
Dim i           As Integer
Dim j           As Integer
Dim DB          As Double
Dim DE          As Double
Dim DP          As Double
Dim TIFDE       As Double
Dim TifDp       As Double
'
Dim nZone(2)    As Integer
Dim MaxZone     As Integer
Dim tX1         As Double
Dim TX2         As Double
Dim tY1         As Double
Dim tY2         As Double
Dim Xme         As Double
Dim Yme         As Double
Dim tVal        As Double
Dim StepGriglia As Double
Dim Fianco      As Integer
Dim ScalaX      As Double
Dim FHaF(2)     As Double
'
On Error GoTo errDisegna_ProfiloK_ESTERNI
  '
  Call InizializzaVariabili
  '
  PAR.Allineamento = 0
  PAR.ScalaMargine = 0.9
  MaxZone = 4
  StepGriglia = Lp_Opt("iStepGriglia")
  ReDim Tif(2, MaxZone)
  ReDim BOMB(2, MaxZone)
  ReDim Rast(2, MaxZone)
  ReDim XRas(2, MaxZone)
  '
  ScalaX = val(Lp_Opt("iScalaX"))
  If (ScalaX = 0) Then ScalaX = Lp("iScalaX")
  '
  TifDia = Lp("R[901]")
  DB = CalcoloEsterni.DB
  DE = Lp("DEXT_G")
  TIFDE = DIA2TIF(DE, DB)
  DP = CalcoloEsterni.DP
  TifDp = DIA2TIF(DP, DB)
  'Fianco1
  Tif(1, 0) = DIA2TIF(Lp("R[43]"), DB)   'SAP
  Tif(1, 1) = Lp("R[270]")               'Fine Zona 1
  Rast(1, 1) = -Lp("R[271]")             'Rastremazione Zona 1
  BOMB(1, 1) = Lp("R[272]")              'Bombatura Zona 1
  Tif(1, 2) = Lp("R[273]")               'Fine Zona 2
  Rast(1, 2) = -Lp("R[274]")             'Rastremazione Zona 2
  BOMB(1, 2) = Lp("R[275]")              'Bombatura Zona 2
  Tif(1, 3) = Lp("R[276]")               'Fine Zona 3
  Rast(1, 3) = -Lp("R[277]")             'Rastremazione Zona 3
  BOMB(1, 3) = Lp("R[278]")              'Bombatura Zona 3
  FHaF(1) = Lp("R[175]")
  'Fianco2
  Tif(2, 0) = DIA2TIF(Lp("SAPF2_G"), DB) 'SAP
  Tif(2, 1) = Lp("R[285]")               'Fine Zona 1
  Rast(2, 1) = Lp("R[286]")              'Rastremazione Zona 1
  BOMB(2, 1) = -Lp("R[287]")             'Bombatura Zona 1
  Tif(2, 2) = Lp("R[288]")               'Fine Zona 2
  Rast(2, 2) = Lp("R[289]")              'Rastremazione Zona 2
  BOMB(2, 2) = -Lp("R[290]")             'Bombatura Zona 2
  Tif(2, 3) = Lp("R[291]")               'Fine Zona 3
  Rast(2, 3) = Lp("R[292]")              'Rastremazione Zona 3
  BOMB(2, 3) = -Lp("R[293]")             'Bombatura Zona 3
  FHaF(2) = Lp("R[178]")
  'Eventuale conversione Dia -> Tif
  If (TifDia = 2) Then
    For Fianco = 1 To 2
      For i = 1 To MaxZone
        Tif(Fianco, i) = DIA2TIF(Tif(Fianco, i), DB)
      Next i
    Next Fianco
  End If
  XRas(1, 0) = -4 'ICx(Par.Pic.ScaleWidth / 3)
  XRas(2, 0) = 4  'ICx(2 * Par.Pic.ScaleWidth / 3)
  For Fianco = 1 To 2
    'Controllo se il TIF dell'ultima zona � oltre il diametro esterno
    For i = 1 To 3
      If (Tif(Fianco, i) <> 0) Then
        nZone(Fianco) = i
        If (Tif(Fianco, i) > TIFDE) Then
          Tif(Fianco, i) = TIFDE
          Exit For
        End If
      Else
        If (i = 1) Then
          Tif(Fianco, i) = TIFDE
          Rast(Fianco, i) = 0
          BOMB(Fianco, i) = 0
          nZone(Fianco) = i
          Exit For
        End If
      End If
    Next i
  Next Fianco
  '
  Select Case nZone(1)
    Case 1
      Rast(1, 1) = Rast(1, 1) - FHaF(1)
    Case 2
      Rast(1, 1) = Rast(1, 1) - FHaF(1) * (Tif(1, 0) - Tif(1, 1)) / (Tif(1, 0) - Tif(1, 2))
      Rast(1, 2) = Rast(1, 2) - FHaF(1) * (Tif(1, 1) - Tif(1, 2)) / (Tif(1, 0) - Tif(1, 2))
    Case 3
      Rast(1, 1) = Rast(1, 1) - FHaF(1) * (Tif(1, 0) - Tif(1, 1)) / (Tif(1, 0) - Tif(1, 3))
      Rast(1, 2) = Rast(1, 2) - FHaF(1) * (Tif(1, 1) - Tif(1, 2)) / (Tif(1, 0) - Tif(1, 3))
      Rast(1, 3) = Rast(1, 3) - FHaF(1) * (Tif(1, 2) - Tif(1, 3)) / (Tif(1, 0) - Tif(1, 3))
  End Select
  Select Case nZone(2)
    Case 1
      Rast(2, 1) = Rast(2, 1) + FHaF(2)
    Case 2
      Rast(2, 1) = Rast(2, 1) + FHaF(2) * (Tif(2, 0) - Tif(2, 1)) / (Tif(2, 0) - Tif(2, 2))
      Rast(2, 2) = Rast(2, 2) + FHaF(2) * (Tif(2, 1) - Tif(2, 2)) / (Tif(2, 0) - Tif(2, 2))
    Case 3
      Rast(2, 1) = Rast(2, 1) + FHaF(2) * (Tif(2, 0) - Tif(2, 1)) / (Tif(2, 0) - Tif(2, 3))
      Rast(2, 2) = Rast(2, 2) + FHaF(2) * (Tif(2, 1) - Tif(2, 2)) / (Tif(2, 0) - Tif(2, 3))
      Rast(2, 3) = Rast(2, 3) + FHaF(2) * (Tif(2, 2) - Tif(2, 3)) / (Tif(2, 0) - Tif(2, 3))
  End Select
  '
  For Fianco = 1 To 2
    For i = 1 To nZone(Fianco)
      XRas(Fianco, i) = XRas(Fianco, i - 1) + Rast(Fianco, i) * ScalaX
    Next i
  Next Fianco
  PAR.MaxX = XRas(1, 0)
  PAR.MinX = XRas(1, 0)
  PAR.MaxY = Tif(1, 0)
  PAR.MinY = Tif(1, 0)
  For Fianco = 1 To 2
    For i = 1 To nZone(Fianco)
      If XRas(Fianco, i) > PAR.MaxX Then PAR.MaxX = XRas(Fianco, i)
      If XRas(Fianco, i) < PAR.MinX Then PAR.MinX = XRas(Fianco, i)
      If Tif(Fianco, i) > PAR.MaxY Then PAR.MaxY = Tif(Fianco, i)
      If Tif(Fianco, i) < PAR.MinY Then PAR.MinY = Tif(Fianco, i)
    Next i
  Next Fianco
  PAR.MaxX = PAR.MaxX + 2
  PAR.MinX = PAR.MinX - 2
  PAR.ScalaMargine = 0.6
  Call CalcolaParPic(PAR, True, Stampante)
  tX1 = XRas(1, 0) + 2 * StepGriglia * ScalaX
  TX2 = XRas(1, 0) + 3 * StepGriglia * ScalaX
  tY1 = PAR.MinY - 0.5
  Call QuotaH(tX1, tY1, TX2, tY1, "" & StepGriglia, 10, vbBlue)
  For Fianco = 1 To 2
    For i = -3 To 3
      If Fianco = 1 Then
        tX1 = XRas(1, 0) + i * StepGriglia * ScalaX
      Else
        tX1 = XRas(2, 0) + i * StepGriglia * ScalaX
      End If
      Call linea(tX1, PAR.MinY, tX1, PAR.MaxY, RGB(180, 180, 180), , vbDot)
    Next i
    tX1 = XRas(Fianco, 0) - 1
    tY1 = Tif(Fianco, 0)
    TX2 = XRas(Fianco, 0) + 1
    tY2 = Tif(Fianco, 0)
    tColore = vbBlack 'CalcoloColore("R[271];R[272]", "R[286];R[287]", Fianco)
    linea tX1, tY1, TX2, tY2, tColore
    tColore = CalcoloColore("R[43]", "SAPF2_G", Fianco)
    If Fianco = 1 Then
      tX1 = tX1
    Else
      tX1 = TX2
    End If
    Call TestoAll(tX1, tY1, "SAP (" & frmt4(TIF2DIA(Tif(Fianco, 0), DB)) & ")", tColore, tColore = vbRed, Fianco)
    For i = 1 To nZone(Fianco)
      tX1 = XRas(Fianco, i - 1)
      tY1 = Tif(Fianco, i - 1)
      TX2 = XRas(Fianco, i)
      tY2 = Tif(Fianco, i)
      j = 3 * i
        tColore = CalcoloColore("R[" & 269 + j & "]", "R[" & 284 + j & "]", Fianco, vbBlue)
      If Fianco = 1 Then
        Call Disegna_Bombatura(tX1, tY1, TX2, tY2, BOMB(Fianco, i), ScalaX, tColore, 2)
      Else
        Call Disegna_Bombatura(TX2, tY2, tX1, tY1, -BOMB(Fianco, i), ScalaX, tColore, 2)
      End If
      'tColore = CalcoloColore("R[" & 268 + j & "];R[" & 269 + j & "]", "R[" & 283 + j & "];R[" & 284 + j & "]", Fianco)
      tColore = CalcoloColore("R[" & 268 + j & "]", "R[" & 283 + j & "]", Fianco)
      '
      Call linea(tX1, tY1, TX2, tY2, tColore, IIf(tColore <> vbRed, 1, 2))
      Call linea(TX2 - 1, tY2, TX2 + 1, tY2, vbBlack)
      '
      tColore = CalcoloColore("R[" & 267 + 3 * i & "]", "R[" & 282 + 3 * i & "]", Fianco)
      '
      If Tif(Fianco, i) = TIFDE Then
        tTesto = "De" & " (" & frmt4(TIF2DIA(Tif(Fianco, i), DB)) & ")"
      Else
        tTesto = "DIA" & i & " (" & frmt4(TIF2DIA(Tif(Fianco, i), DB)) & ")" '"DIA" & i & " F" & Fianco & " (" & frmt4(TIF2DIA(Tif(Fianco, i), DB)) & ")"
      End If
      If Fianco = 1 Then
        TX2 = TX2 - 1
      Else
        TX2 = TX2 + 1
      End If
      Call TestoAll(TX2, tY2, tTesto, tColore, tColore = vbRed, Fianco)
    Next i
  Next Fianco
  '
  Call linea(PAR.MinX - 1, TifDp, PAR.MaxX + 1, TifDp, vbBlue, , vbDash)
  Call TestoAll((PAR.MinX + PAR.MaxX) / 2, TifDp, "Dp (" & frmt4(TIF2DIA(TifDp, DB)) & ")", vbBlue, False, 3)
  Call TestoAll(XRas(1, 0), PAR.MaxY + 0.5, "F1", vbBlue, True, 3)
  Call TestoAll(XRas(2, 0), PAR.MaxY + 0.5, "F2", vbBlue, True, 3)
  '
  Call TestoInfo("X Scale = " & ScalaX)
  Call TestoInfo("Evaluation = " & IIf(TifDia = 1, "TIF", "DIA"))
  '
  On Error GoTo 0
  Exit Sub

errDisegna_ProfiloK_ESTERNI:
  StopRegieEvents
  MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Sub: Disegna_ProfiloK_ESTERNI"
  ResumeRegieEvents
  Resume Next
  '
End Sub

Public Sub Disegna_ProfiloK_INTERNI(Stampante As Boolean)
'
Dim Tif()       As Double
Dim BOMB()      As Double
Dim Rast()      As Double
Dim XRas()      As Double
Dim XcBo        As Double
Dim YcBo        As Double
Dim RBo         As Double
Dim tL          As Double
Dim tBo         As Double
'
Dim tTesto      As String
Dim tColore     As Long
'
Dim TifDia      As Integer
Dim i           As Integer
Dim j           As Integer
Dim DB          As Double
Dim DE          As Double
Dim DP          As Double
Dim TIFDE       As Double
Dim TifDp       As Double
'
Dim nZone(2)    As Integer
Dim MaxZone     As Integer
Dim tX1         As Double
Dim TX2         As Double
Dim tY1         As Double
Dim tY2         As Double
Dim Xme         As Double
Dim Yme         As Double
Dim tVal        As Double
Dim StepGriglia As Double
Dim Fianco      As Integer
Dim ScalaX      As Double
Dim FHaF(2)     As Double
'
On Error GoTo errDisegna_ProfiloK_INTERNI
  '
  Call InizializzaVariabili
  '
  'par.Pic.Cls
  PAR.Allineamento = 0
  PAR.ScalaMargine = 0.9
  MaxZone = 4
  StepGriglia = Lp_Opt("iStepGriglia")
  ReDim Tif(2, MaxZone)
  ReDim BOMB(2, MaxZone)
  ReDim Rast(2, MaxZone)
  ReDim XRas(2, MaxZone)
  '
  ScalaX = val(Lp_Opt("iScalaX"))
  If (ScalaX = 0) Then ScalaX = Lp("iScalaX")
  TifDia = Lp("R[901]")
  DB = CalcoloEsterni.DB
  DE = Lp("R[44]")
  If (Lp("SAPF2_G") < DE) Then DE = Lp("SAPF2_G")
  TIFDE = DIA2TIF(DE, DB)
  DP = CalcoloEsterni.DP
  TifDp = DIA2TIF(DP, DB)
  'Fianco1
  Tif(1, 0) = DIA2TIF(Lp("R[43]"), DB)   'SAP
  Tif(1, 1) = Lp("R[270]")               'Fine Zona 1
  Rast(1, 1) = -Lp("R[271]")              'Rastremazione Zona 1
  BOMB(1, 1) = Lp("R[272]")             'Bombatura Zona 1
  Tif(1, 2) = Lp("R[273]")               'Fine Zona 2
  Rast(1, 2) = -Lp("R[274]")              'Rastremazione Zona 2
  BOMB(1, 2) = Lp("R[275]")             'Bombatura Zona 2
  Tif(1, 3) = Lp("R[276]")               'Fine Zona 3
  Rast(1, 3) = -Lp("R[277]")              'Rastremazione Zona 3
  BOMB(1, 3) = Lp("R[278]")             'Bombatura Zona 3
  FHaF(1) = Lp("R[175]")
  'Fianco2
  Tif(2, 0) = DIA2TIF(Lp("EAPF2_G"), DB) 'SAP
  Tif(2, 1) = Lp("R[285]")               'Fine Zona 1
  Rast(2, 1) = Lp("R[286]")             'Rastremazione Zona 1
  BOMB(2, 1) = -Lp("R[287]")              'Bombatura Zona 1
  Tif(2, 2) = Lp("R[288]")               'Fine Zona 2
  Rast(2, 2) = Lp("R[289]")             'Rastremazione Zona 2
  BOMB(2, 2) = -Lp("R[290]")              'Bombatura Zona 2
  Tif(2, 3) = Lp("R[291]")               'Fine Zona 3
  Rast(2, 3) = Lp("R[292]")             'Rastremazione Zona 3
  BOMB(2, 3) = -Lp("R[293]")              'Bombatura Zona 3
  FHaF(2) = Lp("R[178]")
  'Eventuale conversione Dia -> Tif
  If (TifDia = 2) Then
    For Fianco = 1 To 2
      For i = 1 To MaxZone
        Tif(Fianco, i) = DIA2TIF(Tif(Fianco, i), DB)
      Next i
    Next Fianco
  End If
  XRas(1, 0) = -4 'ICx(Par.Pic.ScaleWidth / 3)
  XRas(2, 0) = 4  'ICx(2 * Par.Pic.ScaleWidth / 3)
  For Fianco = 1 To 2
    'Controllo se il TIF dell'ultima zona � oltre il diametro esterno
    For i = 1 To 3
      If (Tif(Fianco, i) <> 0) Then
        nZone(Fianco) = i
        If (Tif(Fianco, i) > TIFDE) Then
          Tif(Fianco, i) = TIFDE
          Exit For
        End If
      Else
        If (i = 1) Then
          Tif(Fianco, i) = TIFDE
          Rast(Fianco, i) = 0
          BOMB(Fianco, i) = 0
          nZone(Fianco) = i
          Exit For
        End If
      End If
    Next i
  Next Fianco
  '
  Select Case nZone(1)
    Case 1
      Rast(1, 1) = Rast(1, 1) - FHaF(1)
    Case 2
      Rast(1, 1) = Rast(1, 1) - FHaF(1) * (Tif(1, 0) - Tif(1, 1)) / (Tif(1, 0) - Tif(1, 2))
      Rast(1, 2) = Rast(1, 2) - FHaF(1) * (Tif(1, 1) - Tif(1, 2)) / (Tif(1, 0) - Tif(1, 2))
    Case 3
      Rast(1, 1) = Rast(1, 1) - FHaF(1) * (Tif(1, 0) - Tif(1, 1)) / (Tif(1, 0) - Tif(1, 3))
      Rast(1, 2) = Rast(1, 2) - FHaF(1) * (Tif(1, 1) - Tif(1, 2)) / (Tif(1, 0) - Tif(1, 3))
      Rast(1, 3) = Rast(1, 3) - FHaF(1) * (Tif(1, 2) - Tif(1, 3)) / (Tif(1, 0) - Tif(1, 3))
  End Select
  Select Case nZone(2)
    Case 1
      Rast(2, 1) = Rast(2, 1) + FHaF(2)
    Case 2
      Rast(2, 1) = Rast(2, 1) + FHaF(2) * (Tif(2, 0) - Tif(2, 1)) / (Tif(2, 0) - Tif(2, 2))
      Rast(2, 2) = Rast(2, 2) + FHaF(2) * (Tif(2, 1) - Tif(2, 2)) / (Tif(2, 0) - Tif(2, 2))
    Case 3
      Rast(2, 1) = Rast(2, 1) + FHaF(2) * (Tif(2, 0) - Tif(2, 1)) / (Tif(2, 0) - Tif(2, 3))
      Rast(2, 2) = Rast(2, 2) + FHaF(2) * (Tif(2, 1) - Tif(2, 2)) / (Tif(2, 0) - Tif(2, 3))
      Rast(2, 3) = Rast(2, 3) + FHaF(2) * (Tif(2, 2) - Tif(2, 3)) / (Tif(2, 0) - Tif(2, 3))
  End Select
  '
  For Fianco = 1 To 2
    For i = 1 To nZone(Fianco)
      XRas(Fianco, i) = XRas(Fianco, i - 1) + Rast(Fianco, i) * ScalaX
    Next i
  Next Fianco
  PAR.MaxX = XRas(1, 0)
  PAR.MinX = XRas(1, 0)
  PAR.MaxY = Tif(1, 0)
  PAR.MinY = Tif(1, 0)
  For Fianco = 1 To 2
    For i = 1 To nZone(Fianco)
      If XRas(Fianco, i) > PAR.MaxX Then PAR.MaxX = XRas(Fianco, i)
      If XRas(Fianco, i) < PAR.MinX Then PAR.MinX = XRas(Fianco, i)
      If Tif(Fianco, i) > PAR.MaxY Then PAR.MaxY = Tif(Fianco, i)
      If Tif(Fianco, i) < PAR.MinY Then PAR.MinY = Tif(Fianco, i)
    Next i
  Next Fianco
  PAR.MaxX = PAR.MaxX + 2
  PAR.MinX = PAR.MinX - 2
  PAR.ScalaMargine = 0.6
  Call CalcolaParPic(PAR, True, Stampante)
  tX1 = XRas(1, 0) + 2 * StepGriglia * ScalaX
  TX2 = XRas(1, 0) + 3 * StepGriglia * ScalaX
  tY1 = PAR.MinY - 0.5
  Call QuotaH(tX1, tY1, TX2, tY1, "" & StepGriglia, 10, vbBlue)
  For Fianco = 1 To 2
    For i = -3 To 3
      If Fianco = 1 Then
        tX1 = XRas(1, 0) + i * StepGriglia * ScalaX
      Else
        tX1 = XRas(2, 0) + i * StepGriglia * ScalaX
      End If
      Call linea(tX1, PAR.MinY, tX1, PAR.MaxY, RGB(180, 180, 180), , vbDot)
    Next i
    tX1 = XRas(Fianco, 0) - 1
    tY1 = Tif(Fianco, 0)
    TX2 = XRas(Fianco, 0) + 1
    tY2 = Tif(Fianco, 0)
    tColore = vbBlack 'CalcoloColore("R[271];R[272]", "R[286];R[287]", Fianco)
    linea tX1, tY1, TX2, tY2, tColore
    tColore = CalcoloColore("R[44]", "SAPF2_G", Fianco)
    If Fianco = 1 Then
      tX1 = tX1
    Else
      tX1 = TX2
    End If
    Call TestoAll(tX1, tY1, "SAP (" & frmt4(TIF2DIA(Tif(Fianco, 0), DB)) & ")", tColore, tColore = vbRed, Fianco)
    For i = 1 To nZone(Fianco)
      tX1 = XRas(Fianco, i - 1)
      tY1 = Tif(Fianco, i - 1)
      TX2 = XRas(Fianco, i)
      tY2 = Tif(Fianco, i)
      j = 3 * i
        tColore = CalcoloColore("R[" & 269 + j & "]", "R[" & 284 + j & "]", Fianco, vbBlue)
      If Fianco = 1 Then
        Call Disegna_Bombatura(tX1, tY1, TX2, tY2, BOMB(Fianco, i), ScalaX, tColore, 2)
      Else
        Call Disegna_Bombatura(TX2, tY2, tX1, tY1, -BOMB(Fianco, i), ScalaX, tColore, 2)
      End If
      'tColore = CalcoloColore("R[" & 268 + j & "];R[" & 269 + j & "]", "R[" & 283 + j & "];R[" & 284 + j & "]", Fianco)
      tColore = CalcoloColore("R[" & 268 + j & "]", "R[" & 283 + j & "]", Fianco)
      '
      Call linea(tX1, tY1, TX2, tY2, tColore, IIf(tColore <> vbRed, 1, 2))
      Call linea(TX2 - 1, tY2, TX2 + 1, tY2, vbBlack)
      '
      tColore = CalcoloColore("R[" & 267 + 3 * i & "]", "R[" & 282 + 3 * i & "]", Fianco)
      '
      If Tif(Fianco, i) = TIFDE Then
        tTesto = "De" & " (" & frmt4(TIF2DIA(Tif(Fianco, i), DB)) & ")"
      Else
        tTesto = "DIA" & i & " (" & frmt4(TIF2DIA(Tif(Fianco, i), DB)) & ")"
      End If
      If Fianco = 1 Then
        TX2 = TX2 - 1
      Else
        TX2 = TX2 + 1
      End If
      Call TestoAll(TX2, tY2, tTesto, tColore, tColore = vbRed, Fianco)
    Next i
  Next Fianco
  '
  Call linea(PAR.MinX - 1, TifDp, PAR.MaxX + 1, TifDp, vbBlue, , vbDash)
  Call TestoAll((PAR.MinX + PAR.MaxX) / 2, TifDp, "Dp (" & frmt4(TIF2DIA(TifDp, DB)) & ")", vbBlue, False, 3)
  Call TestoAll(XRas(1, 0), PAR.MaxY + 0.5, "F1", vbBlue, True, 3)
  Call TestoAll(XRas(2, 0), PAR.MaxY + 0.5, "F2", vbBlue, True, 3)
  '
  Call TestoInfo("X Scale = " & ScalaX)
  Call TestoInfo("Evaluation = " & IIf(TifDia = 1, "TIF", "DIA"))
  'CALL TestoInfo ("Step Griglia = " & StepGriglia)
  '
On Error GoTo 0
Exit Sub

errDisegna_ProfiloK_INTERNI:
  StopRegieEvents
  MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Sub: Disegna_ProfiloK_INTERNI"
  ResumeRegieEvents
  '
End Sub

Public Sub Disegna_ProfiloK_MOLAVITE(Stampante As Boolean)
'Gestione per Contour Dressing
Dim Tif()       As Double
Dim BOMB()      As Double
Dim Rast()      As Double
Dim XRas()      As Double
Dim XcBo        As Double
Dim YcBo        As Double
Dim RBo         As Double
Dim tL          As Double
Dim tBo         As Double
'
Dim tTesto      As String
Dim tColore     As Long
'
Dim TifDia      As Integer
Dim i           As Integer
Dim j           As Integer
Dim DB          As Double
Dim DE          As Double
Dim DP          As Double
Dim TIFDE       As Double
Dim TifDp       As Double
'
Dim DiamSAP     As Double
Dim TifSAP      As Double
'
Dim nZone(2)    As Integer
Dim MaxZone     As Integer
Dim tX1         As Double
Dim TX2         As Double
Dim tY1         As Double
Dim tY2         As Double
Dim Xme         As Double
Dim Yme         As Double
Dim tVal        As Double
Dim StepGriglia As Double
Dim Fianco      As Integer
Dim ScalaX      As Double
Dim FHaF(2)     As Double
'
On Error GoTo errDisegna_ProfiloK_MOLAVITE
  '
  Call InizializzaVariabili
  '
  PAR.Allineamento = 0
  PAR.ScalaMargine = 0.9
  MaxZone = 4
  StepGriglia = Lp_Opt("iStepGriglia")
  ReDim Tif(2, MaxZone)
  ReDim BOMB(2, MaxZone)
  ReDim Rast(2, MaxZone)
  ReDim XRas(2, MaxZone)
  '
  ScalaX = val(Lp_Opt("iScalaX"))
  If (ScalaX = 0) Then ScalaX = Lp("iScalaX")
  '
  TifDia = Lp("TIFDIAM_CD")
  DB = CalcoloEsterni.DB
  'DE = Lp("DEXT_G")
  DE = Lp("DEAP_G")
  TIFDE = DIA2TIF(DE, DB)
  DP = CalcoloEsterni.DP
  TifDp = DIA2TIF(DP, DB)
  '
  DiamSAP = Lp("DSAP_G")
  TifSAP = DIA2TIF(DiamSAP, DB)
  'Fianco1
  Tif(1, 0) = DIA2TIF(Lp("SAP_F1_CD"), DB)   'SAP
  Tif(1, 1) = Lp("TIF1_F1_CD")               'Fine Zona 1
  Rast(1, 1) = -Lp("RASTR1_F1_CD")           'Rastremazione Zona 1
  BOMB(1, 1) = Lp("BOMB1_F1_CD")             'Bombatura Zona 1
  Tif(1, 2) = Lp("TIF2_F1_CD")               'Fine Zona 2
  Rast(1, 2) = -Lp("RASTR2_F1_CD")           'Rastremazione Zona 2
  BOMB(1, 2) = Lp("BOMB2_F1_CD")             'Bombatura Zona 2
  Tif(1, 3) = Lp("TIF3_F1_CD")               'Fine Zona 3
  Rast(1, 3) = -Lp("RASTR3_F1_CD")           'Rastremazione Zona 3
  BOMB(1, 3) = Lp("BOMB3_F1_CD")             'Bombatura Zona 3
  FHaF(1) = Lp("DEVANG_F1_CD")
  'Fianco2
  Tif(2, 0) = DIA2TIF(Lp("SAP_F2_CD"), DB)   'SAP
  Tif(2, 1) = Lp("TIF1_F2_CD")               'Fine Zona 1
  Rast(2, 1) = Lp("RASTR1_F2_CD")            'Rastremazione Zona 1
  BOMB(2, 1) = -Lp("BOMB1_F2_CD")            'Bombatura Zona 1
  Tif(2, 2) = Lp("TIF2_F2_CD")               'Fine Zona 2
  Rast(2, 2) = Lp("RASTR2_F2_CD")            'Rastremazione Zona 2
  BOMB(2, 2) = -Lp("BOMB2_F2_CD")            'Bombatura Zona 2
  Tif(2, 3) = Lp("TIF3_F2_CD")               'Fine Zona 3
  Rast(2, 3) = Lp("RASTR3_F2_CD")            'Rastremazione Zona 3
  BOMB(2, 3) = -Lp("BOMB3_F2_CD")            'Bombatura Zona 3
  FHaF(2) = Lp("DEVANG_F2_CD")
  'Eventuale conversione Dia -> Tif
  If (TifDia = 2) Then
    For Fianco = 1 To 2
      For i = 1 To MaxZone
        Tif(Fianco, i) = DIA2TIF(Tif(Fianco, i), DB)
      Next i
    Next Fianco
  End If
  XRas(1, 0) = -4 'ICx(Par.Pic.ScaleWidth / 3)
  XRas(2, 0) = 4  'ICx(2 * Par.Pic.ScaleWidth / 3)
  For Fianco = 1 To 2
    'Controllo se il TIF dell'ultima zona � oltre il diametro esterno
    For i = 1 To 3
      If (Tif(Fianco, i) <> 0) Then
        nZone(Fianco) = i
        If (Tif(Fianco, i) > TIFDE) Then
          Tif(Fianco, i) = TIFDE
          Exit For
        End If
      Else
        If (i = 1) Then
          Tif(Fianco, i) = TIFDE
          Rast(Fianco, i) = 0
          BOMB(Fianco, i) = 0
          nZone(Fianco) = i
          Exit For
        End If
      End If
    Next i
  Next Fianco
  '
  Select Case nZone(1)
    Case 1
      Rast(1, 1) = Rast(1, 1) - FHaF(1)
    Case 2
      Rast(1, 1) = Rast(1, 1) - FHaF(1) * (Tif(1, 0) - Tif(1, 1)) / (Tif(1, 0) - Tif(1, 2))
      Rast(1, 2) = Rast(1, 2) - FHaF(1) * (Tif(1, 1) - Tif(1, 2)) / (Tif(1, 0) - Tif(1, 2))
    Case 3
      Rast(1, 1) = Rast(1, 1) - FHaF(1) * (Tif(1, 0) - Tif(1, 1)) / (Tif(1, 0) - Tif(1, 3))
      Rast(1, 2) = Rast(1, 2) - FHaF(1) * (Tif(1, 1) - Tif(1, 2)) / (Tif(1, 0) - Tif(1, 3))
      Rast(1, 3) = Rast(1, 3) - FHaF(1) * (Tif(1, 2) - Tif(1, 3)) / (Tif(1, 0) - Tif(1, 3))
  End Select
  Select Case nZone(2)
    Case 1
      Rast(2, 1) = Rast(2, 1) + FHaF(2)
    Case 2
      Rast(2, 1) = Rast(2, 1) + FHaF(2) * (Tif(2, 0) - Tif(2, 1)) / (Tif(2, 0) - Tif(2, 2))
      Rast(2, 2) = Rast(2, 2) + FHaF(2) * (Tif(2, 1) - Tif(2, 2)) / (Tif(2, 0) - Tif(2, 2))
    Case 3
      Rast(2, 1) = Rast(2, 1) + FHaF(2) * (Tif(2, 0) - Tif(2, 1)) / (Tif(2, 0) - Tif(2, 3))
      Rast(2, 2) = Rast(2, 2) + FHaF(2) * (Tif(2, 1) - Tif(2, 2)) / (Tif(2, 0) - Tif(2, 3))
      Rast(2, 3) = Rast(2, 3) + FHaF(2) * (Tif(2, 2) - Tif(2, 3)) / (Tif(2, 0) - Tif(2, 3))
  End Select
  '
  For Fianco = 1 To 2
    For i = 1 To nZone(Fianco)
      XRas(Fianco, i) = XRas(Fianco, i - 1) + Rast(Fianco, i) * ScalaX
    Next i
  Next Fianco
  PAR.MaxX = XRas(1, 0)
  PAR.MinX = XRas(1, 0)
  PAR.MaxY = Tif(1, 0)
  PAR.MinY = Tif(1, 0)
  For Fianco = 1 To 2
    For i = 1 To nZone(Fianco)
      If XRas(Fianco, i) > PAR.MaxX Then PAR.MaxX = XRas(Fianco, i)
      If XRas(Fianco, i) < PAR.MinX Then PAR.MinX = XRas(Fianco, i)
      If Tif(Fianco, i) > PAR.MaxY Then PAR.MaxY = Tif(Fianco, i)
      If Tif(Fianco, i) < PAR.MinY Then PAR.MinY = Tif(Fianco, i)
    Next i
  Next Fianco
  PAR.MaxX = PAR.MaxX + 2
  PAR.MinX = PAR.MinX - 2
  PAR.ScalaMargine = 0.6
  Call CalcolaParPic(PAR, True, Stampante)
  tX1 = XRas(1, 0) + 2 * StepGriglia * ScalaX
  TX2 = XRas(1, 0) + 3 * StepGriglia * ScalaX
  tY1 = PAR.MinY - 0.5
  Call QuotaH(tX1, tY1, TX2, tY1, "" & StepGriglia, 10, vbBlue)
  For Fianco = 1 To 2
    For i = -3 To 3
      If Fianco = 1 Then
        tX1 = XRas(1, 0) + i * StepGriglia * ScalaX
      Else
        tX1 = XRas(2, 0) + i * StepGriglia * ScalaX
      End If
      Call linea(tX1, PAR.MinY, tX1, PAR.MaxY, RGB(180, 180, 180), , vbDot)
    Next i
    tX1 = XRas(Fianco, 0) - 1
    tY1 = Tif(Fianco, 0)
    TX2 = XRas(Fianco, 0) + 1
    tY2 = Tif(Fianco, 0)
    tColore = vbBlack 'CalcoloColore("R[271];R[272]", "R[286];R[287]", Fianco)
    linea tX1, tY1, TX2, tY2, tColore
    tColore = CalcoloColore("R[43]", "SAPF2_G", Fianco)
    If Fianco = 1 Then
      tX1 = tX1
    Else
      tX1 = TX2
    End If
    Call TestoAll(tX1, tY1, "SAP (" & frmt4(TIF2DIA(TifSAP, DB)) & ")", tColore, tColore = vbRed, Fianco)
    For i = 1 To nZone(Fianco)
      tX1 = XRas(Fianco, i - 1)
      tY1 = Tif(Fianco, i - 1)
      TX2 = XRas(Fianco, i)
      tY2 = Tif(Fianco, i)
      j = 3 * i
        tColore = CalcoloColore("R[" & 269 + j & "]", "R[" & 284 + j & "]", Fianco, vbBlue)
      If Fianco = 1 Then
        Call Disegna_Bombatura(tX1, tY1, TX2, tY2, BOMB(Fianco, i), ScalaX, tColore, 2)
      Else
        Call Disegna_Bombatura(TX2, tY2, tX1, tY1, -BOMB(Fianco, i), ScalaX, tColore, 2)
      End If
      'tColore = CalcoloColore("R[" & 268 + j & "];R[" & 269 + j & "]", "R[" & 283 + j & "];R[" & 284 + j & "]", Fianco)
      tColore = CalcoloColore("R[" & 268 + j & "]", "R[" & 283 + j & "]", Fianco)
      '
      Call linea(tX1, tY1, TX2, tY2, tColore, IIf(tColore <> vbRed, 1, 2))
      Call linea(TX2 - 1, tY2, TX2 + 1, tY2, vbBlack)
      '
      tColore = CalcoloColore("R[" & 267 + 3 * i & "]", "R[" & 282 + 3 * i & "]", Fianco)
      '
      If Tif(Fianco, i) = TIFDE Then
         tTesto = "DEAP" & " (" & frmt4(TIF2DIA(Tif(Fianco, i), DB)) & ")"
        'tTesto = "De" & " (" & frmt4(TIF2DIA(Tif(Fianco, i), db)) & ")"
      Else
        tTesto = "DIA" & i & " (" & frmt4(TIF2DIA(Tif(Fianco, i), DB)) & ")" '"DIA" & i & " F" & Fianco & " (" & frmt4(TIF2DIA(Tif(Fianco, i), DB)) & ")"
      End If
      If Fianco = 1 Then
        TX2 = TX2 - 1
      Else
        TX2 = TX2 + 1
      End If
      Call TestoAll(TX2, tY2, tTesto, tColore, tColore = vbRed, Fianco)
    Next i
  Next Fianco
  '
  Call linea(PAR.MinX - 1, TifDp, PAR.MaxX + 1, TifDp, vbBlue, , vbDash)
  Call TestoAll((PAR.MinX + PAR.MaxX) / 2, TifDp, "Dp (" & frmt4(TIF2DIA(TifDp, DB)) & ")", vbBlue, False, 3)
  Call TestoAll(XRas(1, 0), PAR.MaxY + 0.5, "F1", vbBlue, True, 3)
  Call TestoAll(XRas(2, 0), PAR.MaxY + 0.5, "F2", vbBlue, True, 3)
  '
  Call TestoInfo("X Scale = " & ScalaX)
  Call TestoInfo("Evaluation = " & IIf(TifDia = 1, "TIF", "DIA"))
  '
  On Error GoTo 0
  Exit Sub

errDisegna_ProfiloK_MOLAVITE:
  StopRegieEvents
  MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Sub: Disegna_ProfiloK_MOLAVITE"
  ResumeRegieEvents
  Resume Next
  '
End Sub

'Public Sub Disegna_Info()

'par.MaxX = 50
'par.MinX = 0
'par.MaxY = 50
'par.MinY = 0
'par.ScalaMargine = 1
'par.Allineamento = 0
'Call CalcolaParPic(par)
'par.Pic.Cls
''Call TestoInfo("Diametro interno ottenuto : " & frmt4(diamIntOtt))
''Call TestoInfo(" ")
'Call TestoInfo("Numero punti evolvente F1: " & DGlob.NumPtEvF1)
'Call TestoInfo("Numero punti evolvente F2: " & DGlob.NumPtEvF2)
'Call TestoInfo(" ")
'Call TestoInfo("Lunghezza contatto: " & frmt4(DApp.LcontattoMola))
'
'End Sub

Public Sub Disegna_ZONA_LAVORO_DENTATURA(Stampante As Boolean)
'
Dim nZone As Integer
Dim DE    As Double
Dim Ft    As Double
Dim F1    As Double
Dim F2    As Double
Dim F3    As Double

Dim F1_2 As Double
Dim F2_2 As Double
Dim F3_2 As Double

Dim Re As Double
Dim RS As Double
Dim LS As Double

Dim DiaMola         As Double
Dim LatoPart        As Integer
Dim ExtracorsaIn    As Double
Dim ExtracorsaOut   As Double
Dim tColore   As Long
Dim tX1       As Double
Dim tX        As Double
Dim tY        As Double
Dim i         As Integer
Dim NotScale  As Boolean
Dim QuotaIL   As Double
Dim QuotaFL   As Double
Dim XF0       As Double
Dim XF1       As Double
Dim XF2       As Double
Dim XF3       As Double
Dim XF0_2     As Double
Dim XF1_2     As Double
Dim XF2_2     As Double
Dim XF3_2     As Double
Dim tScala    As Double
Dim YQuota    As Double
Dim Beta      As Double
Dim QZI       As Double
'
Dim iTESTA          As Integer
Dim PENETRAZIONE    As Double
Dim Abilitazioni(4) As Integer
Dim TipoCiclo(4)    As Integer
Dim R194            As Double
'
  Call InizializzaVariabili
  '
  NotScale = False
  '
  nZone = Lp("iNzone")
  DE = Lp("DEXT_G")
  DiaMola = Lp("DIAMOLAMAX_G")
  LatoPart = Lp("R[12]")
  Beta = Lp("BETA_G")
  R194 = Lp("R[194]")
  '
  iTESTA = Lp("INDTAV_G")
  If (iTESTA = 1) Then
    QZI = Lp("QASSIALESTART_G[1,1]")
  Else
    QZI = Lp("QASSIALESTART_G[0,1]")
  End If
  '
  PENETRAZIONE = (Lp("DEXT_G") - Lp("DINT_G")) / 2
  '
  Abilitazioni(1) = Lp("NumPassSGR_G")
  Abilitazioni(2) = Lp("NumPassFIN_G")
  Abilitazioni(3) = Lp("NumPassCiclo3_G")
  Abilitazioni(4) = Lp("NumPassCiclo4_G")
  '
  TipoCiclo(1) = Lp("TipoCicloSG_G")
  TipoCiclo(2) = Lp("TipoCicloFIN_G")
  TipoCiclo(3) = Lp("TipoCicloC3_G")
  TipoCiclo(4) = Lp("TipoCicloC4_G")
  For i = 1 To 4
  If (Abilitazioni(i) = 1) And (TipoCiclo(i) = 3) Then TipoCiclo(0) = 3
  Next i
  '
  F1 = 0:    F2 = 0:    F3 = 0
  F1_2 = 0:  F2_2 = 0:  F3_2 = 0
  Select Case nZone
    Case 1
      F1 = Lp("iFas1F1"):   F1_2 = Lp("iFas1F2")
    Case 2
      F1 = Lp("iFas1F1"):   F2 = Lp("iFas2F1")
      F1_2 = Lp("iFas1F2"): F2_2 = Lp("iFas2F2")
    Case 3
      F1 = Lp("iFas1F1"):   F2 = Lp("iFas2F1"):      F3 = Lp("iFas3F1")
      F1_2 = Lp("iFas1F2"): F2_2 = Lp("iFas2F2"):    F3_2 = Lp("iFas3F2")
  End Select
  Ft = F1 + F2 + F3 'Fascia Totale
  If (Ft = 0) Then
    NotScale = True
    F1 = 50
    F1_2 = 50
    Ft = 50
  End If
  '
  Re = DE / 2
  RS = Re / 4 'Raggio spina
  LS = RS     'Lunghezza spina sporgente
  '
  PAR.MaxY = 6 * LS + Ft
  PAR.MinY = -2 * LS
  PAR.MaxX = Re
  PAR.MinX = 0
  PAR.ScalaMargine = 1
  PAR.Allineamento = 0
  '
  Call CalcolaParPic(PAR, True, Stampante)
  '
  PAR.MargineDxPic = PAR.Pic.TextWidth("Z=888.888")
  PAR.MargineSxPic = PAR.Pic.TextWidth("Z=888.888")
  '
  Dim XMola As Double
  Dim Ymola As Double
  '
  ExtracorsaIn = Lp("CORSAIN") + R194
  ExtracorsaOut = Lp("CORSAOUT")
  '
  If (TipoCiclo(0) = 3) Then
        PAR.MaxY = (2 * LS + Ft + ExtracorsaOut)
        PAR.MinY = -ExtracorsaIn
        PAR.MaxX = 2 * Re
        PAR.MinX = 0
        PAR.ScalaMargine = 1
        PAR.Allineamento = 0
        Call CalcolaParPic(PAR, True, Stampante)
        XMola = LS + Ft + ExtracorsaOut
        Ymola = 0
        QuotaIL = QZI + Ft / 2 + R194
        QuotaFL = QuotaIL
        tScala = PAR.Scala
        If PAR.Pic Is Printer Then tScala = tScala * 45
        YQuota = Re + 350 / tScala
        
        XMola = LS + Ft / 2
        Call QuotaV(YQuota, XMola, YQuota, XMola - ExtracorsaIn, "" & ExtracorsaIn, 12, , 5)
        Call TestoAll(YQuota, XMola + 200 / tScala, "Z=" & QuotaIL, , True, 2)
  Else
    Select Case LatoPart
      Case 1 'ALTO
        PAR.MaxY = (2 * LS + Ft + ExtracorsaOut)
        PAR.MinY = -ExtracorsaIn
        PAR.MaxX = 2 * Re
        PAR.MinX = 0
        PAR.ScalaMargine = 1
        PAR.Allineamento = 0
        Call CalcolaParPic(PAR, True, Stampante)
        XMola = LS + Ft + ExtracorsaOut
        Ymola = 0
        QuotaIL = QZI + Ft + ExtracorsaOut
        QuotaFL = QZI - ExtracorsaIn
        tScala = PAR.Scala
        If PAR.Pic Is Printer Then tScala = tScala * 45
        YQuota = Re + 350 / tScala
        If (Lp("iMetDiv") = 2) Then
          XMola = LS + Ft + ExtracorsaIn
          Call QuotaV(YQuota, XMola, YQuota, XMola - ExtracorsaIn, "" & ExtracorsaIn, 12, , 5)
          QuotaIL = QZI + Ft + ExtracorsaIn
        End If
        Call TestoAll(YQuota, XMola + 200 / tScala, "Z=" & QuotaIL, , True, 2)
      Case -1 'BASSO
        PAR.MaxY = (2 * LS + Ft + ExtracorsaIn)
        PAR.MinY = -ExtracorsaIn
        PAR.MaxX = 2 * Re
        PAR.MinX = 0
        PAR.ScalaMargine = 1
        PAR.Allineamento = 0
        Call CalcolaParPic(PAR, True, Stampante)
        XMola = LS - ExtracorsaOut
        Ymola = 0
        QuotaIL = QZI - ExtracorsaOut
        QuotaFL = QZI + ExtracorsaOut + Ft
        tScala = PAR.Scala
        If PAR.Pic Is Printer Then tScala = tScala * 45
        YQuota = Re + 350 / tScala
        If (Lp("iMetDiv") = 2) Then
          XMola = LS - ExtracorsaIn
          Call QuotaV(YQuota, XMola, YQuota, XMola + ExtracorsaIn, "" & ExtracorsaIn, 12, , 5)
          QuotaIL = QZI - ExtracorsaIn
        End If
        Call TestoAll(YQuota, XMola - 200 / tScala, "Z=" & QuotaIL, , True, 1)
      End Select
  End If
  '
  YQuota = Re + 700 / tScala
  '
  Call linea(YQuota + DiaMola, XMola, Re, XMola, vbRed, 1, vbDash)
  Call Cerchio(Re + DiaMola / 2 - PENETRAZIONE, XMola, DiaMola / 2, vbRed, 2, 2)
  '
  If (LatoPart = -1) Then
    tX1 = ExtracorsaIn / 2
    tX1 = XMola - tX1
  Else
    tX1 = ExtracorsaOut / 2
    tX1 = XMola + tX1
  End If
  '
  'Zone
  XF0 = LS:    XF1 = XF0 + F1:        XF2 = XF1 + F2:        XF3 = XF2 + F3
  XF0_2 = LS:  XF1_2 = XF0_2 + F1_2:  XF2_2 = XF1_2 + F2_2:  XF3_2 = XF2_2 + F3_2
  '
  'Spina sx
  Call linea(-RS, 0, RS, 0)
  Call linea(-RS, 0, -RS, LS)
  Call linea(RS, 0, RS, LS)
  'Ingranaggio
  Call linea(-Re, LS, Re, LS, vbBlue, 2)
  Call linea(-Re, LS, -Re, LS + Ft, vbBlue, 2)
  Call linea(Re, LS, Re, LS + Ft, vbBlue, 2)
  Call linea(-Re, LS + Ft, Re, LS + Ft, vbBlue, 2)
  'VERSO INGRANAGGIO
  Select Case Beta
    Case Is > 0
      For i = 1 To 8
      Call linea(Re / 4 * (-4 + i - 1), LS, Re / 4 * (-4 + i), LS + Ft, vbBlue, 2)
      Next i
    Case Is < 0
      For i = 1 To 8
      Call linea(-Re / 4 * (-4 + i - 1), LS, -Re / 4 * (-4 + i), LS + Ft, vbBlue, 2)
      Next i
    Case Else
      For i = 1 To 8
      Call linea(Re / 4 * (-4 + i - 1), LS, Re / 4 * (-4 + i - 1), LS + Ft, vbBlue, 2)
      Next i
  End Select
  'Spina dx
  Call linea(-RS, LS + Ft, -RS, 2 * LS + Ft)
  Call linea(RS, LS + Ft, RS, 2 * LS + Ft)
  Call linea(-RS, 2 * LS + Ft, RS, 2 * LS + Ft)
  'Contropunta
  Call linea(RS / 2, 2 * LS + Ft, RS, 3 * LS + Ft, RGB(100, 100, 100))
  Call linea(-RS / 2, 2 * LS + Ft, -RS, 3 * LS + Ft, RGB(100, 100, 100))
  Call linea(RS, 3 * LS + Ft, RS, 8 * LS + Ft, RGB(100, 100, 100))
  Call linea(-RS, 3 * LS + Ft, -RS, 8 * LS + Ft, RGB(100, 100, 100))
  '
  YQuota = Re + 350 / tScala
  Call QuotaV(YQuota, LS + Ft, YQuota, LS + Ft + ExtracorsaOut, "" & ExtracorsaOut, 12, , 1)
  Call QuotaV(YQuota, LS - ExtracorsaOut, YQuota, LS, "" & ExtracorsaOut, 12, , 1)
  '
  If Not NotScale Then
    Call QuotaV(YQuota, LS, YQuota, LS + Ft, "" & Ft, 12, , 5)
  Else
    Call QuotaV(YQuota, LS, YQuota, LS + Ft, "?", 12, vbRed, 5)
  End If
  '
  PAR.MargineDxPic = 0
  PAR.MargineSxPic = 0
  If NotScale Then Call TestoInfo("DRAWING NOT IN SCALE ! ", , True, vbRed, True)
  '
  If (iTESTA = 1) Then
    Call TestoInfo("HOBBING on C2", 2, True, vbRed)
  Else
    Call TestoInfo("HOBBING on C1", 2, True, vbRed)
  End If
  Call TestoInfo("Units: [mm]", 3)
  '
End Sub

Public Sub Disegna_CICLI_PROFILA_MOLAVITE(Stampante As Boolean)
Dim NotScale  As Boolean
Dim iTESTA    As Double

Dim DE    As Double
Dim Re As Double
Dim RS As Double
Dim LS As Double
Dim Ft    As Double

Dim AT_DRESSING%
Dim BIAS_ANALISIS(0 To 1) As String
'*********
Dim TIPO_LAVORAZIONE As String
Dim MODALITA              As String
  '
'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  MODALITA = GetInfo("Configurazione", "MODALITA", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))

Const MAXNumeroCicli = 4
'
Dim i     As Integer
Dim iLep  As Integer
Dim k     As Integer
Dim kkk   As Integer
Dim j     As Integer
Dim RTMP  As Double
Dim stmp  As String
Dim riga  As String
'
'Area di stampa
Dim MLeft As Double
Dim MTop  As Double
'
'Parametri per la stampa di linee
Dim X1       As Double
Dim Y1       As Double
Dim X2       As Double
Dim Y2       As Double
Dim iRG      As Double
Dim iCl      As Double
Dim TextFont As String
Dim TextSize As Double
Dim Wnome1   As Double
Dim Wnome2   As Double
Dim Wvalore1 As Double
Dim Wvalore2 As Double
Dim Fattore  As Double
Dim WLine    As Integer
'
Dim DBB As Database
Dim RSS As Recordset
'
Dim MESSAGGI(20)                 As String
Dim Tret1a(MAXNumeroCicli)       As Double
Dim Tempo1A                      As Double
Dim Tret1r(MAXNumeroCicli)       As Double
Dim Tempo1R                      As Double
Dim Tret1rV(MAXNumeroCicli)      As Double
Dim Tempo1RV                     As Double
Dim T1D(MAXNumeroCicli)          As Double
Dim TPSDIV                       As Double
Dim TDIAM(MAXNumeroCicli)        As Double
Dim TtotDiam                     As Double
Dim T(MAXNumeroCicli)            As Double
Dim TotIncY(MAXNumeroCicli)      As Double
Dim TotIncA(MAXNumeroCicli)      As Double
Dim NunPrf(MAXNumeroCicli)       As Integer
'
Dim UsuraRag        As Double
Dim UsuraDia        As Double
'
Dim PassateProfilatura  As Integer
Dim VelocitaProfilatura As Double
'
Dim MolaPezzo  As Double
Dim LDIAM      As Double
Dim FASCIA     As Double
Dim DiaPr      As Double
Dim PassoElica As Double
Dim larg       As Double
Dim TME        As Double
Dim TMU        As Double
Dim VRECAnd    As Double
Dim VRECRit    As Double
Dim NBPE       As Integer
'
'dati macchina
Dim XcentroRullo   As Double 'X centro rullo
Dim DistMolaRullo  As Double 'Interasse mola-asse rullo MAC[0,numand]
Dim DistPezzoRullo As Double 'Interasse pezzo-asse rullo MAC[0,0]
Dim AccX           As Double 'Accelerazione assi X-Y (mm/s2)
Dim AccA           As Double 'Accelerazione asse A  (gr/s2)
Dim AccB           As Double 'Accelerazione asse B  (gr/s2)
Dim VelA           As Double 'Velocit� max asse A (giri/min->gradi/sec)
Dim VelB           As Double 'Velocit� max asse B (giri/min->gradi/sec)
Dim VelX           As Double 'Velocit� max asse X   (mm/min-> mm/sec)
Dim VelY           As Double 'Velocit� max asse Y   (mm/min-> mm/sec)
Dim TempoFas       As Double 'Tempo fasatura          (sec)
Dim TFM            As Double 'Tempo morto ogni mov.   (sec)
Dim DEMEULE        As Double 'Diametro mola
'
'dati profilo
Dim Mr      As Double        'MODULO NORMALE
Dim APRG    As Double        'ANGOLO PRESSIONE NORM. (GRADI)
Dim Apr     As Double        'ANGOLO PRESSIONE NORM. (RADIANTI)
Dim ZDENT   As Double        'NUMERO DENTI
Dim HELPG   As Double        'ANGOLO ELICA (GRADI)
Dim Help    As Double        'ANGOLO ELICA (RADIANTI)
Dim PASSO   As Double        'PASSO
Dim NPF1    As Integer       'NUMERO PUNTI DEL FIANCO 1
Dim Di1     As Double        'DIAMETRO INTERNO
Dim DE1     As Double        'DIAMETRO ESTERNO
'
'dati profilo
Dim sMR      As String       'MODULO NORMALE
Dim sAPRG    As String       'ANGOLO PRESSIONE NORM. (GRADI)
Dim sZDENT   As String       'NUMERO DENTI
Dim sHELPG   As String       'ANGOLO ELICA (GRADI)
Dim sDI1     As String       'DIAMETRO INTERNO
Dim sDE1     As String       'DIAMETRO ESTERNO
Dim sLARG    As String
Dim sLDIAM   As String
Dim sDEMEULE As String
'
'dati lavoro
Dim XinizioLav As Double     'X inizio lavoro
Dim XdivLat    As Double     'Tratto divisione laterale
Dim Xentrata   As Double     'Tratto X entrata
Dim Xuscita    As Double     'Tratto X uscita
Dim NPezzi     As Double     'Numero pezzi
Dim DistPezzi  As Double     'Disanza pezzi
Dim Zrett      As Double     'Denti da rettificare
Dim Zona1      As Double     'Tratto n.1 elica
Dim Zona2      As Double     'Tratto n.2 elica
Dim Zona3      As Double     'Tratto n.3 elica
'
'dati cicli
Dim TipoCiclo(MAXNumeroCicli) As Integer  'Tipo ciclo
Dim Rip(MAXNumeroCicli)       As Integer  'Numero ripetizioni
Dim NPassDiam(MAXNumeroCicli) As Integer  'Numero di passate di diamantatura
Dim IncPrf(MAXNumeroCicli)    As Double   'Incremento profilatura
Dim Vdiam(MAXNumeroCicli)     As Double   'Velocit� profilatura  (mm/min)
Dim NpasCic(MAXNumeroCicli)   As Integer  'N.passate
Dim IncAnd(MAXNumeroCicli)    As Double   'Incremento andata
Dim IncRit(MAXNumeroCicli)    As Double   'Incremento ritorno
Dim Velassa(MAXNumeroCicli)   As Double   'Vel.assi and.
Dim Velassr(MAXNumeroCicli)   As Double   'Vel.assi rit.
Dim Denti2prf(MAXNumeroCicli) As Double   'Denti tra due prof.
Dim QwAnd(MAXNumeroCicli)     As Single
Dim QwRit(MAXNumeroCicli)     As Single

Dim ShiftxMM(MAXNumeroCicli)  As Single
Dim Shift(MAXNumeroCicli)     As Single
Dim RitShift(MAXNumeroCicli)  As Single
Dim ShiftMolaFin(MAXNumeroCicli) As Double
'Dim Diagonal(MAXNumeroCicli)  As Single
Dim VTMola(MAXNumeroCicli)    As Double
Dim VelMola(MAXNumeroCicli)   As Double
Dim AvAss(MAXNumeroCicli)     As Double
Dim qs(MAXNumeroCicli)        As Double
Dim FeedAss(MAXNumeroCicli) As Double

'Molavite
Dim NumPrinc     As Integer
Dim sNumPrinc    As String
Dim Fasciatotale As Double
Dim ModUT        As Double
Dim AlfUT        As Double

'ciclo prof molavite
Dim PassSgrossatura As Integer
Dim IncSgrossatura  As Single
Dim PassFinitura    As Integer
Dim IncFinitura     As Single
Dim PassZero        As Integer
Dim GiriMandrino    As Integer
Dim TipoProfilatura As Integer
Dim LunghezzaMola   As Integer
Dim SpessoreRullo   As Integer

'
Dim TT    As Double
Dim StartX As Double  'FlagMOD
Dim StartY As Double  'FlagMOD
Dim Xlinea As Double  'FlagMOD
Dim Ylinea As Double  'FlagMOD
Dim TabX  As Double
Dim TabX1 As Double
Dim TabX2 As Double
Dim TABx3 As Double
Dim TEMPO As Double
'
Dim PicH      As Double
Dim PicW      As Double
Dim Atmp      As String * 255
'

'
Dim NOME_TIPO             As String
'
Dim TabellaMANDRINI As String
Dim TabellaMACCHINA As String
Dim TabellaPROFILO  As String
Dim TabellaLAVORO   As String
'
Dim TIPO_DIAMANTATORE As Integer
'
Dim TH As Double
Dim Tm As Double
Dim Ts As Double

Dim PosIniSh   As Double
Dim PosFinSh   As Double
Dim HFiletto   As Double
Dim DmolaMax   As Double
Dim DmolaMin   As Double
Dim ShiftPezzo As Double
Dim Lmolautile As Double
Dim NPezziProf As Double
Dim NroProf    As Double
Dim TotIncrPrf As Single
Dim NroPezziMola As Double
Dim sNPezziProf   As String
Dim sNroProf      As String
Dim sNroPezziMola As String
Dim sShiftPezzo   As String
Dim sLmolautile   As String
Dim RitornoSh As Double

Dim TotShift(MAXNumeroCicli)     As Single
'

Dim X_P1          As Double
Dim Y_P1          As Double
Dim X_P2          As Double
Dim Y_P2          As Double

Dim MargineSx     As Double
Dim MargineDx     As Double
Dim MargineUp     As Double
Dim MargineDw     As Double
Dim PosRiga       As Double
Dim DirShift      As Integer

Dim NPezziRes     As Double
Dim NroPezziRet   As Double
Dim sNPezziRes    As String
Dim sNroPezziRet  As String
Dim DmolaInt      As Double
Dim NroProfRes    As Double
Dim LmolaRes      As Double
Dim PosShift      As Double
Dim TotIncrProf   As Single
Dim sTotIncrProf  As String
Dim DmolaExt      As Double
Dim sDmolaExt     As String
Dim LungEvolv     As Double
Dim sInclFiletto  As String
Dim sPASSO        As String


On Error GoTo errDisegna_CICLI_PROFILA_MOLAVITE

'***********
  '
  PG = 4 * Atn(1)
  PI = 4 * Atn(1)
  Set PAR.Pic = PictureDisegni
  Set PAR.Su = OEMX.SuOEM1
  
  Select Case LINGUA
   Case "CH"
     PAR.Pic.Font.Name = "Ms Song"
   Case "RU"
     PAR.Pic.Font.Name = "Arial Cyr"
     PAR.Pic.Font.Charset = 204
   Case Else
     PAR.Pic.Font.Name = "Arial"
  End Select
  
  'PAR.Pic.Font.Italic = True
  PAR.Pic.FontSize = 10
  PAR.ScalaMargine = 1
    
  NotScale = False
  '
  PAR.MaxY = PAR.RegioneY2
  PAR.MinY = 0
  PAR.MaxX = PAR.RegioneX2
  PAR.MinX = 0
  
  PAR.ScalaMargine = 1
  PAR.Allineamento = 0
  '
  Call CalcolaParPic(PAR, True, Stampante)
  '
  PAR.MargineDxPic = PAR.Pic.TextWidth("Z=888.888")
  PAR.MargineSxPic = PAR.Pic.TextWidth("Z=888.888")
  PAR.Pic.CurrentX = 0
  PAR.Pic.CurrentY = 0

  'Leggere Parametri

      'LETTURA DATI PROFILO
        ZDENT = LEP("NUMDENTI_G"):   sZDENT = LNP("NUMDENTI_G") 'NUMERO DENTI
        DE1 = LEP("DEXT_G"):         sDE1 = LNP("DEXT_G")    'DIAMETRO ESTERNO
        Di1 = LEP("DINT_G"):         sDI1 = LNP("DINT_G")    'DIAMETRO INTERNO
        APRG = LEP("ALFA_G"):        sAPRG = LNP("ALFA_G")   'ANGOLO PRESSIONE NORM.
        HELPG = Abs(LEP("BETA_G")):  sHELPG = LNP("BETA_G")  'ANGOLO ELICA
        Mr = LEP("MN_G")
        sMR = "Mn"
        DiaPr = Mr / Cos(HELPG / 180 * PG) * ZDENT           'DIAMETRO PRIMITIVO
        
        NumPrinc = LEP("NUMPRINC_G"):   sNumPrinc = LNP("NUMPRINC_G") 'NUMERO PRINCIPI
        ModUT = LEP("MNUT_G")
        AlfUT = LEP("ALFAUT_G") * PG / 180
        LungEvolv = LEP("LAEVOLV_G")
        
      '

      'Lettura Dati Ciclo profilatura
        PassSgrossatura = LEP("APNSGDIAM_G")
        IncSgrossatura = LEP("APINCSGDIAM_G")
        PassFinitura = LEP("APNFINDIAM_G")
        IncFinitura = LEP("APINCFINDIAM_G")
        PassZero = LEP("APPSI_G")
        GiriMandrino = LEP("APGMANDR_G")
        TipoProfilatura = LEP("DIAMUNID_G")
        LunghezzaMola = LEP("LARGHMOLA_G")
        SpessoreRullo = LEP("SPESSRULLO_G")

        PosIniSh = LEP("PosInizioSh_G")
        PosFinSh = LEP("PosFineSh_G")
        
        Select Case MODALITA
          Case "OFF-LINE": PosShift = Lp("POSSHIFT_G[2]")
          Case "SPF": PosShift = val(OPC_LEGGI_DATO("/ACC/NCK/UGUD/POSSHIFT_G[2]"))
          Case Else: PosShift = Lp("POSSHIFT_G[2]")
        End Select
        
        HFiletto = LEP("HDENTEMOLA_G")
        DmolaMax = LEP("DIAMMOLA_G")  '- HFiletto * 2
        DmolaMin = LEP("DIAMUTMIN_G") '- HFiletto * 2
        DirShift = LEP("DIRSHIFT")
        DmolaInt = 2 * val(OPC_LEGGI_DATO("/ACC/NCK/GUD7/RFONDOMOLA_G"))
        DmolaExt = 2 * val(OPC_LEGGI_DATO("/ACC/NCK/GUD7/REXTMOLA_G"))
        
        TotIncrProf = (PassSgrossatura * IncSgrossatura + PassFinitura * IncFinitura) * TipoProfilatura

If DmolaMax = 0 Then
   DmolaMax = 250
End If
If (DmolaExt = 0) Then
     DmolaExt = DmolaMax
End If

'Calcolo Passo mola e inclinazione
Dim parDiametroMola As Double
Dim parCorrFha1     As Double
Dim parCorrFha2     As Double
Dim parInclFiletto  As Double
Dim parPasso        As Double

If DmolaInt > 0 Then
   parDiametroMola = DmolaInt + HFiletto * 2 - ModUT * 2
Else
  'Modifica temporanea
   DmolaInt = 250
   parDiametroMola = DmolaInt + HFiletto * 2 - ModUT * 2
End If

'Correzione fHa
parCorrFha1 = -(LEP("CORFHA1"))  'val(OPC_LEGGI_DATO("/ACC/NCK/UGUD/CORFHA1"))
parCorrFha2 = -(LEP("CORFHA2"))  'val(OPC_LEGGI_DATO("/ACC/NCK/UGUD/CORFHA2"))

Call CalcoloPasso_Molavite(DGlob.Z, DGlob.Mn, DGlob.ALFA, DGlob.Beta, DGlob.SAP, DGlob.EAP, _
                          NumPrinc, ModUT, AlfUT, parDiametroMola, _
                          parCorrFha1, parCorrFha2, parInclFiletto, parPasso)


       Select Case LINGUA
       Case "IT"
       sTotIncrProf = " Incrementi profilatura"
          sDmolaExt = " Diametro ext mola"
       sInclFiletto = " Inclinazione filetto mola"
             sPASSO = " Passo della mola"
   BIAS_ANALISIS(0) = " Ciclo Prof. Bias"
If (Lp("AT_DRESSING") = 0) Then BIAS_ANALISIS(1) = " Disattivo" Else BIAS_ANALISIS(1) = " Attivo"
       Case "UK"
       sTotIncrProf = " Dressing increments"
          sDmolaExt = " Wheel ext. diameter"
       sInclFiletto = " Wheel thread angle"
             sPASSO = " Wheel pitch"
   BIAS_ANALISIS(0) = " Dres. Bias Cicle"
If (Lp("AT_DRESSING") = 0) Then BIAS_ANALISIS(1) = " Disable" Else BIAS_ANALISIS(1) = " Active"
       Case "GR"
         sTotIncrProf = " Increment Profilieren"
            sDmolaExt = " Scheibekopfdurchmesser"
         sInclFiletto = " Scheibe gewindewinkel"
               sPASSO = " Scheibe gewindesteigung"
     BIAS_ANALISIS(0) = " Abrichten in Biaszyklus"
If (Lp("AT_DRESSING") = 0) Then BIAS_ANALISIS(1) = " Aus" Else BIAS_ANALISIS(1) = " Aktive"
       Case "FR"
        sTotIncrProf = " Increments profilage"
        sDmolaExt = " Diam�tre ext meule"
        sInclFiletto = " Wheel thread angle"
        sPASSO = " Wheel pitch"
   BIAS_ANALISIS(0) = " Dres. Bias Cicle"
If (Lp("AT_DRESSING") = 0) Then BIAS_ANALISIS(1) = " Disable" Else BIAS_ANALISIS(1) = " Active"
       Case Else
       sTotIncrProf = " Dressing increments"
          sDmolaExt = " Wheel ext. diameter"
       sInclFiletto = " Wheel thread angle"
             sPASSO = " Wheel pitch"
   BIAS_ANALISIS(0) = " Dres. Bias Cicle"
If (Lp("AT_DRESSING") = 0) Then BIAS_ANALISIS(1) = " Disable" Else BIAS_ANALISIS(1) = " Active"
       End Select
       


PAR.Pic.FontBold = True
PAR.Pic.ForeColor = vbBlack
PAR.Pic.FontSize = 10
  
Dim Rtmp0 As Double
Dim RTMP1 As Double
Dim CorrRiga As Integer
CorrRiga = 50
  
'Disegna rettangolo
Rtmp0 = PAR.Pic.TextWidth("A")
RTMP1 = PAR.Pic.TextHeight("A")
PAR.Pic.Line (PAR.RegioneX1 + Rtmp0, PAR.RegioneY1 + RTMP1 + CorrRiga)-(PAR.RegioneX1 + Rtmp0, PAR.RegioneY2 - RTMP1)
PAR.Pic.Line (PAR.RegioneX1 + Rtmp0, PAR.RegioneY2 - RTMP1)-(PAR.RegioneX2 - Rtmp0, PAR.RegioneY2 - RTMP1)
PAR.Pic.Line (PAR.RegioneX2 - Rtmp0, PAR.RegioneY2 - RTMP1)-(PAR.RegioneX2 - Rtmp0, PAR.RegioneY1 + RTMP1 + CorrRiga)
PAR.Pic.Line (PAR.RegioneX2 - Rtmp0, PAR.RegioneY1 + RTMP1 + CorrRiga)-(PAR.RegioneX1 + Rtmp0, PAR.RegioneY1 + RTMP1 + CorrRiga)
 
PAR.Pic.FontBold = False
PAR.Pic.FontSize = 10
    
PAR.Pic.CurrentX = PAR.RegioneX1 + PAR.Pic.TextWidth("A")
PAR.Pic.CurrentY = PAR.RegioneY1 + PAR.Pic.TextHeight("A")


Dim LMax As Integer
Dim ncar As Integer
Dim addcar As Integer
Dim Testo01 As String

  LMax = 30
  PAR.Pic.Font.Name = "Arial"

  MargineSx = PAR.RegioneX1 + PAR.Pic.TextWidth("A")
  MargineDx = PAR.RegioneX2 - PAR.Pic.TextWidth("A")
  MargineUp = PAR.RegioneY1 + PAR.Pic.TextHeight("A") + CorrRiga
  MargineDw = PAR.RegioneY2 - PAR.Pic.TextHeight("A")

  PosRiga = MargineUp
  'Totale incementi profilatura
  PAR.Pic.Line (MargineSx, PosRiga)-(MargineDx, PosRiga), vbBlack
  PosRiga = PosRiga + PAR.Pic.TextHeight("A")
  PAR.Pic.CurrentX = MargineSx + 10
  'TotIncrProf = TRUNC(TotIncrProf * 1000) / 1000
    
  ncar = Len(sTotIncrProf)
  If ncar <= LMax Then
     addcar = LMax - ncar
     Testo01 = sTotIncrProf & Space(addcar)
  Else
     Testo01 = sTotIncrProf
  End If
  TESTO = Testo01 & ":" & frmt(TotIncrProf, 3)
    
  PAR.Pic.FontSize = 11
  PAR.Pic.Font.Name = "Courier"
  PAR.Pic.Print (TESTO)
  PAR.Pic.Font.Name = "Arial"
  PAR.Pic.FontSize = 10
  PAR.Pic.Line (MargineSx, PosRiga)-(MargineDx, PosRiga), vbBlack

  'Diametro Mola
  PAR.Pic.Line (MargineSx, PosRiga)-(MargineDx, PosRiga), vbBlack
  PosRiga = PosRiga + PAR.Pic.TextHeight("A")
  PAR.Pic.CurrentX = MargineSx + 10
    
  ncar = Len(sDmolaExt)
  If ncar <= LMax Then
     addcar = LMax - ncar
     Testo01 = sDmolaExt & Space(addcar)
  Else
     Testo01 = sDmolaExt
  End If
  TESTO = Testo01 & ":" & frmt(DmolaExt, 3)
  
  PAR.Pic.FontSize = 11
  PAR.Pic.Font.Name = "Courier"
  PAR.Pic.Print (TESTO)
  PAR.Pic.Font.Name = "Arial"
  PAR.Pic.FontSize = 10
  PAR.Pic.Line (MargineSx, PosRiga)-(MargineDx, PosRiga), vbBlack

  'Inclinazione Filetto Mola
  PAR.Pic.Line (MargineSx, PosRiga)-(MargineDx, PosRiga), vbBlack
  PosRiga = PosRiga + PAR.Pic.TextHeight("A")
  PAR.Pic.CurrentX = MargineSx + 10
    
  ncar = Len(sInclFiletto)
  If ncar <= LMax Then
     addcar = LMax - ncar
     Testo01 = sInclFiletto & Space(addcar)
  Else
     Testo01 = sInclFiletto
  End If
  TESTO = Testo01 & ":" & frmt(parInclFiletto, 3)
  
  PAR.Pic.FontSize = 11
  PAR.Pic.Font.Name = "Courier"
  PAR.Pic.Print (TESTO)
  PAR.Pic.Font.Name = "Arial"
  PAR.Pic.FontSize = 10
  PAR.Pic.Line (MargineSx, PosRiga)-(MargineDx, PosRiga), vbBlack

  'Passo Filetto Mola
  PAR.Pic.Line (MargineSx, PosRiga)-(MargineDx, PosRiga), vbBlack
  PosRiga = PosRiga + PAR.Pic.TextHeight("A")
  PAR.Pic.CurrentX = MargineSx + 10
    
  ncar = Len(sPASSO)
  If ncar <= LMax Then
     addcar = LMax - ncar
     Testo01 = sPASSO & Space(addcar)
  Else
     Testo01 = sPASSO
  End If
  TESTO = Testo01 & ":" & frmt(parPasso, 3)
  
  PAR.Pic.FontSize = 11
  PAR.Pic.Font.Name = "Courier"
  PAR.Pic.Print (TESTO)
  PAR.Pic.Font.Name = "Arial"
  PAR.Pic.FontSize = 10
  PAR.Pic.Line (MargineSx, PosRiga)-(MargineDx, PosRiga), vbBlack
'----------------------------------------------------------------------
  PosRiga = PosRiga + PAR.Pic.TextHeight("A")
  PAR.Pic.CurrentX = MargineSx + 10
 'NroPezziRet = TRUNC(NroPezziRet * 100) / 100
     ncar = Len(BIAS_ANALISIS(0))
   If ncar < LMax Then
      addcar = LMax - ncar
      Testo01 = BIAS_ANALISIS(0) & Space(addcar)
   Else
      Testo01 = BIAS_ANALISIS(0)
   End If
   TESTO = Testo01 & ":" & BIAS_ANALISIS(1)
  
  PAR.Pic.FontSize = 11
  PAR.Pic.Font.Name = "Courier"
  PAR.Pic.Print (TESTO)
  PAR.Pic.Font.Name = "Arial"
  PAR.Pic.FontSize = 10
  PAR.Pic.Line (MargineSx, PosRiga)-(MargineDx, PosRiga), vbBlack
'----------------------------------------------------------------------

  PAR.MargineDxPic = 0
  PAR.MargineSxPic = 0
  If NotScale Then Call TestoInfo("DRAWING NOT IN SCALE ! ", , True, vbRed, True)

Exit Sub
errDisegna_CICLI_PROFILA_MOLAVITE:
 
  WRITE_DIALOG "SUB Disegna_CICLI_PROFILA_MOLAVITE: ERROR -> " & Err
  Resume Next
  'Exit Sub

End Sub

Public Sub Disegna_CICLI_LAVORO_MOLAVITE(Stampante As Boolean)
Dim NotScale  As Boolean
Dim iTESTA    As Double

Dim DE    As Double
Dim Re As Double
Dim RS As Double
Dim LS As Double
Dim Ft    As Double

'*********
Dim TIPO_LAVORAZIONE As String
Dim MODALITA              As String
  '
'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  MODALITA = GetInfo("Configurazione", "MODALITA", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))

Const MAXNumeroCicli = 4
'
Dim i     As Integer
Dim iLep  As Integer
Dim k     As Integer
Dim kkk   As Integer
Dim j     As Integer
Dim RTMP  As Double
Dim stmp  As String
Dim riga  As String
'
'Area di stampa
Dim MLeft As Double
Dim MTop  As Double
'
'Parametri per la stampa di linee
Dim X1       As Double
Dim Y1       As Double
Dim X2       As Double
Dim Y2       As Double
Dim iRG      As Double
Dim iCl      As Double
Dim TextFont As String
Dim TextSize As Double
Dim Wnome1   As Double
Dim Wnome2   As Double
Dim Wvalore1 As Double
Dim Wvalore2 As Double
Dim Fattore  As Double
Dim WLine    As Integer
'
Dim DBB As Database
Dim RSS As Recordset
'
Dim MESSAGGI(20)                 As String
Dim Tret1a(MAXNumeroCicli)       As Double
Dim Tempo1A                      As Double
Dim Tret1r(MAXNumeroCicli)       As Double
Dim Tempo1R                      As Double
Dim Tret1rV(MAXNumeroCicli)      As Double
Dim Tempo1RV                     As Double
Dim T1D(MAXNumeroCicli)          As Double
Dim TPSDIV                       As Double
Dim TDIAM(MAXNumeroCicli)        As Double
Dim TtotDiam                     As Double
Dim T(MAXNumeroCicli)            As Double
Dim TotIncY(MAXNumeroCicli)      As Double
Dim TotIncA(MAXNumeroCicli)      As Double
Dim NunPrf(MAXNumeroCicli)       As Integer
'
Dim UsuraRag        As Double
Dim UsuraDia        As Double
'
Dim PassateProfilatura  As Integer
Dim VelocitaProfilatura As Double
'
Dim MolaPezzo  As Double
Dim LDIAM      As Double
Dim FASCIA     As Double
Dim DiaPr      As Double
Dim PassoElica As Double
Dim larg       As Double
Dim TME        As Double
Dim TMU        As Double
Dim VRECAnd    As Double
Dim VRECRit    As Double
Dim NBPE       As Integer
'
'dati macchina
Dim XcentroRullo   As Double 'X centro rullo
Dim DistMolaRullo  As Double 'Interasse mola-asse rullo MAC[0,numand]
Dim DistPezzoRullo As Double 'Interasse pezzo-asse rullo MAC[0,0]
Dim AccX           As Double 'Accelerazione assi X-Y (mm/s2)
Dim AccA           As Double 'Accelerazione asse A  (gr/s2)
Dim AccB           As Double 'Accelerazione asse B  (gr/s2)
Dim VelA           As Double 'Velocit� max asse A (giri/min->gradi/sec)
Dim VelB           As Double 'Velocit� max asse B (giri/min->gradi/sec)
Dim VelX           As Double 'Velocit� max asse X   (mm/min-> mm/sec)
Dim VelY           As Double 'Velocit� max asse Y   (mm/min-> mm/sec)
Dim TempoFas       As Double 'Tempo fasatura          (sec)
Dim TFM            As Double 'Tempo morto ogni mov.   (sec)
Dim DEMEULE        As Double 'Diametro mola
'
'dati profilo
Dim Mr      As Double        'MODULO NORMALE
Dim APRG    As Double        'ANGOLO PRESSIONE NORM. (GRADI)
Dim Apr     As Double        'ANGOLO PRESSIONE NORM. (RADIANTI)
Dim ZDENT   As Double        'NUMERO DENTI
Dim HELPG   As Double        'ANGOLO ELICA (GRADI)
Dim Help    As Double        'ANGOLO ELICA (RADIANTI)
Dim PASSO   As Double        'PASSO
Dim NPF1    As Integer       'NUMERO PUNTI DEL FIANCO 1
Dim Di1     As Double        'DIAMETRO INTERNO
Dim DE1     As Double        'DIAMETRO ESTERNO
'
'dati profilo
Dim sMR      As String       'MODULO NORMALE
Dim sAPRG    As String       'ANGOLO PRESSIONE NORM. (GRADI)
Dim sZDENT   As String       'NUMERO DENTI
Dim sHELPG   As String       'ANGOLO ELICA (GRADI)
Dim sDI1     As String       'DIAMETRO INTERNO
Dim sDE1     As String       'DIAMETRO ESTERNO
Dim sLARG    As String
Dim sLDIAM   As String
Dim sDEMEULE As String
'
'dati lavoro
Dim XinizioLav As Double     'X inizio lavoro
Dim XdivLat    As Double     'Tratto divisione laterale
Dim Xentrata   As Double     'Tratto X entrata
Dim Xuscita    As Double     'Tratto X uscita
Dim NPezzi     As Double     'Numero pezzi
Dim DistPezzi  As Double     'Disanza pezzi
Dim Zrett      As Double     'Denti da rettificare
Dim Zona1      As Double     'Tratto n.1 elica
Dim Zona2      As Double     'Tratto n.2 elica
Dim Zona3      As Double     'Tratto n.3 elica
'
'dati cicli
Dim TipoCiclo(MAXNumeroCicli) As Integer  'Tipo ciclo
Dim Rip(MAXNumeroCicli)       As Integer  'Numero ripetizioni
Dim NPassDiam(MAXNumeroCicli) As Integer  'Numero di passate di diamantatura
Dim IncPrf(MAXNumeroCicli)    As Double   'Incremento profilatura
Dim Vdiam(MAXNumeroCicli)     As Double   'Velocit� profilatura  (mm/min)
Dim NpasCic(MAXNumeroCicli)   As Integer  'N.passate
Dim IncAnd(MAXNumeroCicli)    As Double   'Incremento andata
Dim IncRit(MAXNumeroCicli)    As Double   'Incremento ritorno
Dim Velassa(MAXNumeroCicli)   As Double   'Vel.assi and.
Dim Velassr(MAXNumeroCicli)   As Double   'Vel.assi rit.
Dim Denti2prf(MAXNumeroCicli) As Double   'Denti tra due prof.
Dim QwAnd(MAXNumeroCicli)     As Single
Dim QwRit(MAXNumeroCicli)     As Single

Dim ShiftxMM(MAXNumeroCicli)  As Single
Dim Shift(MAXNumeroCicli)     As Single
Dim RitShift(MAXNumeroCicli)  As Single
Dim ShiftMolaFin(MAXNumeroCicli) As Double
'Dim Diagonal(MAXNumeroCicli)  As Single
Dim VTMola(MAXNumeroCicli)    As Double
Dim VelMola(MAXNumeroCicli)   As Double
Dim AvAss(MAXNumeroCicli)     As Double
Dim qs(MAXNumeroCicli)        As Double
Dim FeedAss(MAXNumeroCicli) As Double

'Molavite
Dim NumPrinc     As Integer
Dim sNumPrinc    As String
Dim Fasciatotale As Double
'Dim CorsaZ#
Dim CorsaZ       As Double
Dim ModUT        As Double

'ciclo prof molavite
Dim PassSgrossatura As Integer
Dim IncSgrossatura  As Single
Dim PassFinitura    As Integer
Dim IncFinitura     As Single
Dim PassZero        As Integer
Dim GiriMandrino    As Integer
Dim TipoProfilatura As Integer
Dim LunghezzaMola   As Integer
Dim SpessoreRullo   As Integer

'
Dim TT    As Double
Dim StartX As Double  'FlagMOD
Dim StartY As Double  'FlagMOD
Dim Xlinea As Double  'FlagMOD
Dim Ylinea As Double  'FlagMOD
Dim TabX  As Double
Dim TabX1 As Double
Dim TabX2 As Double
Dim TABx3 As Double
Dim TEMPO As Double
'
Dim PicH      As Double
Dim PicW      As Double
Dim Atmp      As String * 255
'

'
Dim NOME_TIPO             As String
'
Dim TabellaMANDRINI As String
Dim TabellaMACCHINA As String
Dim TabellaPROFILO  As String
Dim TabellaLAVORO   As String
'
Dim TIPO_DIAMANTATORE As Integer
'
Dim TH As Double
Dim Tm As Double
Dim Ts As Double

Dim PosIniSh   As Double
Dim PosFinSh   As Double
Dim HFiletto   As Double
Dim DmolaMax   As Double
Dim DmolaMin   As Double
Dim ShiftPezzo As Double
Dim Lmolautile As Double
Dim NPezziProf As Double
Dim NroProf    As Double
Dim TotIncrPrf As Single
Dim NroPezziMola As Double
Dim sNPezziProf   As String
Dim sNroProf      As String
Dim sNroPezziMola As String
Dim sShiftPezzo   As String
Dim sLmolautile   As String
Dim RitornoSh As Double
'
Dim ExCorsaBias#, cY_Zi#, EXTC#, iCB%, RitornoShB#, AT_DRESSING%, CORR_TW_KIND%
Dim BIAS_ANALISIS(0 To 1) As String
'
Dim TotShift(MAXNumeroCicli)     As Single
'

Dim X_P1          As Double
Dim Y_P1          As Double
Dim X_P2          As Double
Dim Y_P2          As Double

Dim MargineSx     As Double
Dim MargineDx     As Double
Dim MargineUp     As Double
Dim MargineDw     As Double
Dim PosRiga       As Double
Dim DirShift      As Integer

Dim NPezziRes     As Double
Dim NroPezziRet   As Double
Dim sNPezziRes    As String
Dim sNroPezziRet  As String
Dim sBias(2)      As String
Dim DmolaInt      As Double
Dim NroProfRes    As Double
Dim LmolaRes      As Double
Dim PosShift      As Double


On Error GoTo errDisegna_CICLI_LAVORO_MOLAVITE

'***********
'  CORR_TW_KIND = Lp("CORR_TW_KIND")
'  If (CORR_TW_KIND = 0) Then
'     Call DB_GUD_AT_Azzera
'  Else
'     Call DB_GUD_AT_Aggiorna
'  End If
  AT_DRESSING = Lp("AT_DRESSING")
  '
  PG = 4 * Atn(1)
  PI = 4 * Atn(1)
  Set PAR.Pic = PictureDisegni
  Set PAR.Su = OEMX.SuOEM1
  
  Select Case LINGUA
   Case "CH"
     PAR.Pic.Font.Name = "Ms Song"
   Case "RU"
     PAR.Pic.Font.Name = "Arial Cyr"
     PAR.Pic.Font.Charset = 204
   Case Else
     PAR.Pic.Font.Name = "Arial"
  End Select
  
  'PAR.Pic.Font.Italic = True
  PAR.Pic.FontSize = 10
  PAR.ScalaMargine = 1
    
  NotScale = False
  '
  PAR.MaxY = PAR.RegioneY2
  PAR.MinY = 0
  PAR.MaxX = PAR.RegioneX2
  PAR.MinX = 0
  
  PAR.ScalaMargine = 1
  PAR.Allineamento = 0
  '
  Call CalcolaParPic(PAR, True, Stampante)
  '
  PAR.MargineDxPic = PAR.Pic.TextWidth("Z=888.888")
  PAR.MargineSxPic = PAR.Pic.TextWidth("Z=888.888")
  PAR.Pic.CurrentX = 0
  PAR.Pic.CurrentY = 0

  'Leggere Parametri

      'LETTURA DATI PROFILO
        ZDENT = LEP("NUMDENTI_G"):   sZDENT = LNP("NUMDENTI_G") 'NUMERO DENTI
        DE1 = LEP("DEXT_G"):         sDE1 = LNP("DEXT_G")    'DIAMETRO ESTERNO
        Di1 = LEP("DINT_G"):         sDI1 = LNP("DINT_G")    'DIAMETRO INTERNO
        APRG = LEP("ALFA_G"):        sAPRG = LNP("ALFA_G")   'ANGOLO PRESSIONE NORM.
        HELPG = Abs(LEP("BETA_G")):  sHELPG = LNP("BETA_G")  'ANGOLO ELICA
        Mr = LEP("MN_G")
        sMR = "Mn"
        DiaPr = Mr / Cos(HELPG / 180 * PG) * ZDENT           'DIAMETRO PRIMITIVO
        
        NumPrinc = LEP("NUMPRINC_G"):   sNumPrinc = LNP("NUMPRINC_G") 'NUMERO PRINCIPI
        ModUT = LEP("MNUT_G")
    
      'LETTURA DATI LAVORO
          XinizioLav = LEP("QASSIALESTART_G[0,1]")   'Z inizio lavoro
             XdivLat = 0                             'Tratto divisione laterale
            Xentrata = LEP("CORSAIN")                'Tratto Z entrata
             Xuscita = LEP("CORSAOUT")               'Tratto Z uscita
              NPezzi = 1                             'Numero Pezzi
           DistPezzi = 0                             'Distanza pezzi
               Zrett = 0                             'Denti da rettificare 20, 40 e 42
        'ELICA
        Fasciatotale = LEP("fasciatotale_g")
              CorsaZ = (Fasciatotale + Xentrata + Xuscita)
           RitornoSh = 0#
          RitornoShB = 0#
                 iCB = 0
         ExCorsaBias = 0#
                EXTC = 3#
               cY_Zi = Xentrata
               If (cY_Zi > 9) Then
                cY_Zi = 9#
               ElseIf (cY_Zi < 3) Then
                cY_Zi = 3#
               End If
               
'LETTURA DATI CICLI
 For i = 1 To MAXNumeroCicli
          iLep = (i - 1) * 20
    ' Tipo ciclo: (0;1;2;10) = (NULLO,UNIDIREZIONALE;BIDIREZIONALE;BIAS)
    TipoCiclo(i) = LEP("CYC[0," & iLep + 1 & "]")   'Tipo ciclo
    If (AT_DRESSING = 0 And TipoCiclo(i) = 10) Then
       TipoCiclo(i) = 0
       Call SP("CYC[0," & iLep + 1 & "]", TipoCiclo(i))
    End If
        Rip(i) = LEP("CYC[0," & iLep + 2 & "]")      'Numero ripetizioni
     IncAnd(i) = LEP("CYC[0," & iLep + 3 & "]")      'Incremento x ripetizione
    Velassa(i) = LEP("CYC[0," & iLep + 4 & "]")      'Avanzamento passata (mm/g)

    'Shift diagonal x 1 mm
    If (TipoCiclo(i) <> 10) Then
        ShiftxMM(i) = LEP("CYC[0," & iLep + 5 & "]")
    Else
        ShiftxMM(i) = Lp("LNG_TW[0," & 1 - 1 & "]") / CorsaZ
        Call SP("CYC[0," & iLep + 5 & "]", ShiftxMM(i))
        Call SP("SHF" & i, Lp("LNG_TW[0," & 1 - 1 & "]"))
        ShiftxMM(i) = 0
    End If
    VTMola(i) = LEP("CYC[0," & iLep + 6 & "]")       'Velocit� Taglio M/s
    'Spostamento mola prima di finitura
    If (TipoCiclo(i) <> 10) Then
       ShiftMolaFin(i) = LEP("CYC[0," & iLep + 8 & "]")
    Else
       ShiftMolaFin(i) = 0#
       Call SP("CYC[0," & iLep + 8 & "]", ShiftMolaFin(i))
    End If
    'Ritorno shift
    If (TipoCiclo(i) <> 10) Then
        RitShift(i) = LEP("CYC[0," & iLep + 9 & "]")
    Else
        RitShift(i) = Lp("LNG_TW[0," & 1 - 1 & "]")
        Call SP("CYC[0," & iLep + 9 & "]", RitShift(i))
        RitShift(i) = 0#
    End If
    Shift(i) = ShiftxMM(i) * CorsaZ        'Shift x passata
    Shift(i) = Int(Shift(i) * 100) / 100
     
     Select Case TipoCiclo(i)
      Case 0:
      Case 1: ShiftPezzo = ShiftPezzo + Rip(i) * Shift(i) + ShiftMolaFin(i): RitornoSh = RitShift(i)
      Case 2: ShiftPezzo = ShiftPezzo + Rip(i) * Shift(i) * 2 + ShiftMolaFin(i): RitornoSh = RitShift(i)
      Case 10: ShiftPezzo = ShiftPezzo + Shift(i) + ShiftMolaFin(i): RitornoShB = RitShift(i): iCB = iCB + 1
     End Select
 Next i
      
      'Lettura Dati Ciclo profilatura
 PassSgrossatura = LEP("APNSGDIAM_G")
  IncSgrossatura = LEP("APINCSGDIAM_G")
    PassFinitura = LEP("APNFINDIAM_G")
     IncFinitura = LEP("APINCFINDIAM_G")
        PassZero = LEP("APPSI_G")
    GiriMandrino = LEP("APGMANDR_G")
 TipoProfilatura = LEP("DIAMUNID_G")
   LunghezzaMola = LEP("LARGHMOLA_G")
   SpessoreRullo = LEP("SPESSRULLO_G")

        DirShift = LEP("DIRSHIFT")
        PosIniSh = LEP("PosInizioSh_G")
        PosFinSh = LEP("PosFineSh_G")

        Select Case MODALITA
          Case "OFF-LINE": PosShift = Lp("POSSHIFT_G[2]")
          Case "SPF": PosShift = val(OPC_LEGGI_DATO("/ACC/NCK/GUD7/PosYPezzo"))
          Case Else: PosShift = Lp("POSSHIFT_G[2]")
        End Select
        
        HFiletto = LEP("HDENTEMOLA_G")
        DmolaMax = LEP("DIAMMOLA_G") - HFiletto * 2
        DmolaMin = LEP("DIAMUTMIN_G")
        DmolaInt = 2 * val(OPC_LEGGI_DATO("/ACC/NCK/GUD7/RFONDOMOLA_G"))
        
        'Shift mola x Pezzo
        If (AT_DRESSING = 0) Then
            ShiftPezzo = ShiftPezzo - RitornoSh
        Else
            ShiftPezzo = ShiftPezzo - RitornoSh - RitornoShB * iCB
        End If
        If TipoProfilatura = 1 Then
          TotIncrPrf = Abs((PassSgrossatura * IncSgrossatura + PassFinitura * IncFinitura))
        End If
        If TipoProfilatura = 2 Then
           TotIncrPrf = Abs((PassSgrossatura * IncSgrossatura + 2 * PassFinitura * IncFinitura) * 2)
        End If
        If (TotIncrPrf = 0) Then
           TotIncrPrf = 0.001
        End If
                
        'Lunghezza mola utile
        If AT_DRESSING = 1 Then
           Lmolautile = DirShift * (PosFinSh - PosIniSh) - RitornoSh * (1 - AT_DRESSING)
        Else
           Lmolautile = DirShift * (PosFinSh - PosIniSh) - RitornoSh
        End If
        'N.ro pezzi x ogni profilatura
        If ShiftPezzo > 0 Then
           NPezziProf = Int(Lmolautile / ShiftPezzo)
        End If
        'N.ro profilature totali con mola nuova
        NroProf = Int((DmolaMax - DmolaMin) / 2 / Abs(TotIncrPrf))
        'N.ro pezzi rettificabili con mola nuova
        NroPezziMola = Int(NroProf * NPezziProf)
        'N.ro profilature residue con mola attuale
        NroProfRes = Int((DmolaInt - DmolaMin) / 2 / Abs(TotIncrPrf))
        'Lunghezza mola residua
        If AT_DRESSING = 1 Then
           LmolaRes = DirShift * (PosShift - PosFinSh) + RitornoSh * (1 - AT_DRESSING)
        Else
           LmolaRes = -DirShift * (PosShift - PosFinSh) - RitornoSh
        End If
        'N.ro pezzi residui prima di profilare
        If ShiftPezzo > 0 Then
           NPezziRes = Int(LmolaRes / ShiftPezzo)
        End If
        'N.pezzi rettificabili con mola attuale
        NroPezziRet = Int(NroProfRes * NPezziProf) + NPezziRes
        
       Select Case LINGUA
       Case "IT"
        sShiftPezzo = " Shift per pezzo"
        sLmolautile = " Lunghezza mola utile"
        sNPezziProf = " Pezzi per profilatura"
           sNroProf = " Profilature con mola nuova"
      sNroPezziMola = " Pezzi con mola nuova"
         sNPezziRes = " Pezzi prima di profilare"
       sNroPezziRet = " Tot. pezzi da rettificare"
           sBias(0) = " Modifica campi Shift Bias"
           sBias(1) = "Proibito"
   BIAS_ANALISIS(0) = " Ciclo Prof. Bias"
If (Lp("AT_DRESSING") = 0) Then BIAS_ANALISIS(1) = " Disattivo" Else BIAS_ANALISIS(1) = " Attivo"
       Case "UK"
        sShiftPezzo = " Shift for piece"
        sLmolautile = " Useful wheel length"
        sNPezziProf = " Pieces for each dressing"
           sNroProf = " Dressing Nr. with new wheel"
      sNroPezziMola = " Total pieces with new wheel"
         sNPezziRes = " Pieces to grind before dress."
       sNroPezziRet = " Total Pieces to grind"
   BIAS_ANALISIS(0) = " Dres. Bias Cicle"
If (Lp("AT_DRESSING") = 0) Then BIAS_ANALISIS(1) = " Disable" Else BIAS_ANALISIS(1) = " Active"
           sBias(0) = " Bias Cycle Shift Records Editing"
           sBias(1) = "Not Allowed"
       Case "GR"
        sShiftPezzo = " Scheibeverschiebung f. W."
        sLmolautile = " Scheibelaenge fuer Schleif"
        sNPezziProf = " Gesamtstuecke fuer Prof."
           sNroProf = " Anzahl Profilieren"
      sNroPezziMola = " Gesamtstuecke fuer Scheibe"
         sNPezziRes = " Werkst.vor dem Profilieren"
       sNroPezziRet = " Gesamtstuecke zu Scleifen"
   BIAS_ANALISIS(0) = " Abrichten in Biaszyklus"
If (Lp("AT_DRESSING") = 0) Then BIAS_ANALISIS(1) = " Aus" Else BIAS_ANALISIS(1) = " Aktive"
           sBias(0) = " Aender.Bias Shiftfelder deakt."
           sBias(1) = "Nicht erlaubt"
       Case Else
        sShiftPezzo = " Shift for piece"
        sLmolautile = " Useful wheel length"
        sNPezziProf = " Pieces for each dressing"
           sNroProf = " Dressing Nr. with new wheel"
      sNroPezziMola = " Total pieces with new wheel"
         sNPezziRes = " Pieces to grind dressing"
       sNroPezziRet = " Total Pieces to grind"
   BIAS_ANALISIS(0) = " Dres. Bias Cicle"
If (Lp("AT_DRESSING") = 0) Then BIAS_ANALISIS(1) = " Disable" Else BIAS_ANALISIS(1) = " Active"
           sBias(0) = " Bias Cycle Shift Records Editing"
           sBias(1) = "Not Allowed"
       End Select


PAR.Pic.FontBold = True
PAR.Pic.ForeColor = vbBlack
PAR.Pic.FontSize = 10
  
Dim Rtmp0 As Double
Dim RTMP1 As Double
Dim CorrRiga As Integer
CorrRiga = 50
  
'Disegna rettangolo
Rtmp0 = PAR.Pic.TextWidth("A")
RTMP1 = PAR.Pic.TextHeight("A")
PAR.Pic.Line (PAR.RegioneX1 + Rtmp0, PAR.RegioneY1 + RTMP1 + CorrRiga)-(PAR.RegioneX1 + Rtmp0, PAR.RegioneY2 - RTMP1)
PAR.Pic.Line (PAR.RegioneX1 + Rtmp0, PAR.RegioneY2 - RTMP1)-(PAR.RegioneX2 - Rtmp0, PAR.RegioneY2 - RTMP1)
PAR.Pic.Line (PAR.RegioneX2 - Rtmp0, PAR.RegioneY2 - RTMP1)-(PAR.RegioneX2 - Rtmp0, PAR.RegioneY1 + RTMP1 + CorrRiga)
PAR.Pic.Line (PAR.RegioneX2 - Rtmp0, PAR.RegioneY1 + RTMP1 + CorrRiga)-(PAR.RegioneX1 + Rtmp0, PAR.RegioneY1 + RTMP1 + CorrRiga)
 
PAR.Pic.FontBold = False
PAR.Pic.FontSize = 10
    
PAR.Pic.CurrentX = PAR.RegioneX1 + PAR.Pic.TextWidth("A")
PAR.Pic.CurrentY = PAR.RegioneY1 + PAR.Pic.TextHeight("A")


Dim LMax As Integer
Dim ncar As Integer
Dim addcar As Integer
Dim Testo01 As String

  LMax = 30
  PAR.Pic.Font.Name = "Arial"

  MargineSx = PAR.RegioneX1 + PAR.Pic.TextWidth("A")
  MargineDx = PAR.RegioneX2 - PAR.Pic.TextWidth("A")
  MargineUp = PAR.RegioneY1 + PAR.Pic.TextHeight("A") + CorrRiga
  MargineDw = PAR.RegioneY2 - PAR.Pic.TextHeight("A")

  PosRiga = MargineUp

  PAR.Pic.Line (MargineSx, PosRiga)-(MargineDx, PosRiga), vbBlack
  PosRiga = PosRiga + PAR.Pic.TextHeight("A")
  PAR.Pic.CurrentX = MargineSx + 10
  ShiftPezzo = TRUNC(ShiftPezzo * 100) / 100
  
  ncar = Len(sShiftPezzo)
  If ncar <= LMax Then
     addcar = LMax - ncar
     Testo01 = sShiftPezzo & Space(addcar)
  Else
     Testo01 = sShiftPezzo
  End If
  TESTO = Testo01 & ":" & ShiftPezzo
  
  PAR.Pic.FontSize = 11
  PAR.Pic.Font.Name = "Courier"
  PAR.Pic.Print (TESTO)
  PAR.Pic.Font.Name = "Arial"
  PAR.Pic.FontSize = 10
  
  PAR.Pic.Line (MargineSx, PosRiga)-(MargineDx, PosRiga), vbBlack
  PosRiga = PosRiga + PAR.Pic.TextHeight("A")
  PAR.Pic.CurrentX = MargineSx + 10
  Lmolautile = TRUNC(Lmolautile * 100) / 100
  
  ncar = Len(sLmolautile)
  If ncar < LMax Then
     addcar = LMax - ncar
     Testo01 = sLmolautile & Space(addcar)
  Else
     Testo01 = sLmolautile
  End If
  TESTO = Testo01 & ":" & Lmolautile
  
  PAR.Pic.FontSize = 11
  PAR.Pic.Font.Name = "Courier"
  PAR.Pic.Print (TESTO)
  PAR.Pic.Font.Name = "Arial"
  PAR.Pic.FontSize = 10

  PAR.Pic.Line (MargineSx, PosRiga)-(MargineDx, PosRiga), vbBlack
  PosRiga = PosRiga + PAR.Pic.TextHeight("A")
  PAR.Pic.CurrentX = MargineSx + 10
  NPezziProf = TRUNC(NPezziProf * 100) / 100
  
  ncar = Len(sNPezziProf)
  If ncar < LMax Then
     addcar = LMax - ncar
     Testo01 = sNPezziProf & Space(addcar)
  Else
     Testo01 = sNPezziProf
  End If
  TESTO = Testo01 & ":" & NPezziProf
  
  PAR.Pic.FontSize = 11
  PAR.Pic.Font.Name = "Courier"
  PAR.Pic.Print (TESTO)
  PAR.Pic.Font.Name = "Arial"
  PAR.Pic.FontSize = 10

  PAR.Pic.Line (MargineSx, PosRiga)-(MargineDx, PosRiga), vbBlack
  PosRiga = PosRiga + PAR.Pic.TextHeight("A")
  PAR.Pic.CurrentX = MargineSx + 10
  NroProf = TRUNC(NroProf * 100) / 100
  
  ncar = Len(sNroProf)
  If ncar < LMax Then
     addcar = LMax - ncar
     Testo01 = sNroProf & Space(addcar)
  Else
     Testo01 = sNroProf
  End If
  TESTO = Testo01 & ":" & NroProf
    
  PAR.Pic.FontSize = 11
  PAR.Pic.Font.Name = "Courier"
  PAR.Pic.Print (TESTO)
  PAR.Pic.Font.Name = "Arial"
  PAR.Pic.FontSize = 10

  PAR.Pic.Line (MargineSx, PosRiga)-(MargineDx, PosRiga), vbBlack
  PosRiga = PosRiga + PAR.Pic.TextHeight("A")
  PAR.Pic.CurrentX = MargineSx + 10
  NroPezziMola = TRUNC(NroPezziMola * 100) / 100
  
  ncar = Len(sNroPezziMola)
  If ncar < LMax Then
     addcar = LMax - ncar
     Testo01 = sNroPezziMola & Space(addcar)
  Else
     Testo01 = sNroPezziMola
  End If
  TESTO = Testo01 & ":" & NroPezziMola
'*
  PAR.Pic.FontSize = 11
  PAR.Pic.Font.Name = "Courier"
  PAR.Pic.Print (TESTO)
  PAR.Pic.Font.Name = "Arial"
  PAR.Pic.FontSize = 10

  PAR.Pic.Line (MargineSx, PosRiga)-(MargineDx, PosRiga), vbBlack
  PosRiga = PosRiga + PAR.Pic.TextHeight("A")
  PAR.Pic.CurrentX = MargineSx + 10
  NPezziRes = TRUNC(NPezziRes * 100) / 100
  
  ncar = Len(sNPezziRes)
  If ncar < LMax Then
     addcar = LMax - ncar
     Testo01 = sNPezziRes & Space(addcar)
  Else
     Testo01 = sNPezziRes
  End If
  TESTO = Testo01 & ":" & NPezziRes
'*
  PAR.Pic.FontSize = 11
  PAR.Pic.Font.Name = "Courier"
  PAR.Pic.Print (TESTO)
  PAR.Pic.Font.Name = "Arial"
  PAR.Pic.FontSize = 10

  PAR.Pic.Line (MargineSx, PosRiga)-(MargineDx, PosRiga), vbBlack
'----------------------------------------------------------------------
  PosRiga = PosRiga + PAR.Pic.TextHeight("A")
  PAR.Pic.CurrentX = MargineSx + 10
  NroPezziRet = TRUNC(NroPezziRet * 100) / 100
  
  ncar = Len(sNroPezziRet)
  If ncar < LMax Then
     addcar = LMax - ncar
     Testo01 = sNroPezziRet & Space(addcar)
  Else
     Testo01 = sNroPezziRet
  End If
  TESTO = Testo01 & ":" & NroPezziRet
'*
  PAR.Pic.FontSize = 11
  PAR.Pic.Font.Name = "Courier"
  PAR.Pic.Print (TESTO)
  PAR.Pic.Font.Name = "Arial"
  PAR.Pic.FontSize = 10
  PAR.Pic.Line (MargineSx, PosRiga)-(MargineDx, PosRiga), vbBlack
'----------------------------------------------------------------------
  PosRiga = PosRiga + PAR.Pic.TextHeight("A")
  PAR.Pic.CurrentX = MargineSx + 10
 'NroPezziRet = TRUNC(NroPezziRet * 100) / 100
     ncar = Len(BIAS_ANALISIS(0))
   If ncar < LMax Then
      addcar = LMax - ncar
      Testo01 = BIAS_ANALISIS(0) & Space(addcar)
   Else
      Testo01 = BIAS_ANALISIS(0)
   End If
   TESTO = Testo01 & ":" & BIAS_ANALISIS(1)
  
  PAR.Pic.FontSize = 11
  PAR.Pic.Font.Name = "Courier"
  PAR.Pic.Print (TESTO)
  PAR.Pic.Font.Name = "Arial"
  PAR.Pic.FontSize = 10
  PAR.Pic.Line (MargineSx, PosRiga)-(MargineDx, PosRiga), vbBlack
'----------------------------------------------------------------------
If (iCB <> 0) Then
  PosRiga = PosRiga + PAR.Pic.TextHeight("A")
  PAR.Pic.CurrentX = MargineSx + 10
 'NroPezziRet = TRUNC(NroPezziRet * 100) / 100
  
  ncar = Len(sBias(0))
  If ncar < LMax Then
     addcar = LMax - ncar
     Testo01 = sBias(0) & Space(addcar)
  Else
     Testo01 = sBias(0)
  End If
  TESTO = Testo01 & ":" & sBias(1)
'*
  PAR.Pic.FontSize = 11
  PAR.Pic.Font.Name = "Courier"
  PAR.Pic.Print (TESTO)
  PAR.Pic.Font.Name = "Arial"
  PAR.Pic.FontSize = 10
  PAR.Pic.Line (MargineSx, PosRiga)-(MargineDx, PosRiga), vbBlack
End If
'----------------------------------------------------------------------
  PAR.MargineDxPic = 0
  PAR.MargineSxPic = 0
  If NotScale Then Call TestoInfo("DRAWING NOT IN SCALE ! ", , True, vbRed, True)
  '
  'If (iTESTA = 1) Then
  '   Call TestoInfo("GRINDING on C2", 2, True, vbRed)
  'Else
  '  Call TestoInfo("GRINDING on C1", 2, True, vbRed)
  'End If
  'Call TestoInfo("Units: [mm]", 3)

Exit Sub
errDisegna_CICLI_LAVORO_MOLAVITE:
 
  WRITE_DIALOG "SUB Disegna_CICLI_LAVORO_MOLAVITE: ERROR -> " & Err
  Resume Next
  'Exit Sub

End Sub
Private Function TimeToMillisecond() As Long
 Dim sAns As String
 Dim typTime As SYSTEMTIME

 On Error Resume Next
 GetSystemTime typTime
 sAns = Hour(Now) * 60 * 60 * 60 + Minute(Now) * 60 * 60 + Second(Now) * 60 + typTime.wMilliseconds
 TimeToMillisecond = sAns
End Function

Public Sub Disegna_BIAS_MOLAVITE(ByVal Pagina%, Stampante As Boolean)
Dim NotScale  As Boolean
Dim iTESTA    As Double

Dim DE    As Double
Dim Re As Double
Dim RS As Double
Dim LS As Double
Dim Ft    As Double

'*********
Dim TIPO_LAVORAZIONE As String
  '
'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
'
Dim i     As Integer
Dim k     As Integer
Dim kkk   As Integer
Dim j     As Integer
Dim RTMP  As Double
Dim stmp  As String
Dim riga  As String
'
'Area di stampa
Dim MLeft As Double
Dim MTop  As Double
'
'Parametri per la stampa di linee
Dim X1       As Double
Dim Y1       As Double
Dim X2       As Double
Dim Y2       As Double
Dim iRG      As Double
Dim iCl      As Double
Dim TextFont As String
Dim TextSize As Double
Dim Wnome1   As Double
Dim Wnome2   As Double
Dim Wvalore1 As Double
Dim Wvalore2 As Double
Dim WLine    As Integer
'
Dim DBB As Database
Dim RSS As Recordset
'
Dim PassoElica As Double
Dim larg       As Double
Dim TME        As Double
Dim TMU        As Double
Dim VRECAnd    As Double
Dim VRECRit    As Double
Dim NBPE       As Integer
'
Dim TH As Double
Dim Tm As Double
Dim Ts As Double


Dim CorrEvolv(1 To 3) As Double
Dim NaturalBias#
Dim AT_DRESSING%, CORR_TW_KIND%
Dim VirtualRotationDEG#
Dim GrindingCrown#
Dim DressingCrown#

Dim vNaturalBias(0 To 2, 0 To 1) As String
Dim BIAS_ANALISIS(0 To 1) As String
Dim sNaturalBias(0 To 3) As String
Dim side(0 To 1) As String
Dim sMICRON As String
Dim sVirtualRotationDEG As String
Dim sGrindingCrown As String
Dim sDressingCrown As String
'
Dim X_P1          As Double
Dim Y_P1          As Double
Dim X_P2          As Double
Dim Y_P2          As Double

Dim MargineSx     As Double
Dim MargineDx     As Double
Dim MargineUp     As Double
Dim MargineDw     As Double
Dim PosRiga       As Double
Dim PI#
Dim RAGGIOMEDIOMOLA_G#, PASSOMOLA_G#, INCLMOLA_G#

Dim LMax(0 To 2)      As Integer
Dim ncar%
Dim addcar%
Dim Text(1 To 3) As String
Dim BoE#, FaS#
Dim LiM As Boolean
Dim sLiM As String
Dim mSec As Long
Dim ShowWarning As Boolean

On Error GoTo errDisegna_BIAS_MOLAVITE
'***********
BoE = Lp("BoE")
FaS = Lp("FASCIATOTALE_G")
LiM = BoE > FaS / 1000#

'***********
 Call CalcoloSvergolamento(BoE, BoE, CorrEvolv)
 sMICRON = "[mm]" '"[�m]"
 For i = 1 To 3
  NaturalBias = (TRUNC(CorrEvolv(i) * 10) / 10) / 1000
  vNaturalBias(i - 1, 0) = CStr(-NaturalBias): vNaturalBias(i - 1, 1) = CStr(NaturalBias)
 Next i
 'vNaturalBias(1, 0) = CStr(0): vNaturalBias(1, 1) = CStr(0)
 'vNaturalBias(2, 0) = CStr(NaturalBias): vNaturalBias(2, 1) = CStr(-NaturalBias)
'***********
CORR_TW_KIND = Lp("CORR_TW_KIND")
If (CORR_TW_KIND = 0) Then
 Call DB_GUD_AT_Azzera
Else
 Call DB_GUD_AT_Aggiorna
End If
 AT_DRESSING = Lp("AT_DRESSING")
  '
  PG = 4 * Atn(1)
  PI = 4 * Atn(1)
  Set PAR.Pic = PictureDisegni
  Set PAR.Su = OEMX.SuOEM1
  
  Select Case LINGUA
   Case "CH"
     PAR.Pic.Font.Name = "Ms Song"
   Case "RU"
     PAR.Pic.Font.Name = "Arial Cyr"
     PAR.Pic.Font.Charset = 204
   Case Else
     PAR.Pic.Font.Name = "Arial"
  End Select
  
  'PAR.Pic.Font.Italic = True
  PAR.Pic.FontSize = 10
  PAR.ScalaMargine = 1
    
  NotScale = False
  '
  PAR.MaxY = PAR.RegioneY2
  PAR.MinY = 0
  PAR.MaxX = PAR.RegioneX2
  PAR.MinX = 0
  
  PAR.ScalaMargine = 1
  PAR.Allineamento = 0
  '
  Call CalcolaParPic(PAR, True, Stampante)
  '
  PAR.MargineDxPic = PAR.Pic.TextWidth("Z=888.888")
  PAR.MargineSxPic = PAR.Pic.TextWidth("Z=888.888")
  PAR.Pic.CurrentX = 0
  PAR.Pic.CurrentY = 0

  'Leggere Parametri
  LMax(0) = 20
  LMax(1) = 8
  LMax(2) = 8
       Select Case LINGUA
       Case "IT"
            side(0) = " S "
            side(1) = " D "
    sNaturalBias(0) = " Naturale - F. Dente"
    sNaturalBias(1) = " Bias Sezione (a)"
    sNaturalBias(2) = " Bias Sezione (b)"
    sNaturalBias(3) = " Bias Sezione (c)"
   BIAS_ANALISIS(0) = " Ciclo Prof. Bias"
If (Lp("AT_DRESSING") = 0) Then BIAS_ANALISIS(1) = " Disattivo" Else BIAS_ANALISIS(1) = " Attivo"
sVirtualRotationDEG = " Bias Virtual Rot[�]"
     sGrindingCrown = " Part 1" '" Grinding Crown[mm]"
     sDressingCrown = " Part 2" '" Dressing Crown[mm]"
 If (LiM) Then sLiM = " Attenzione: stai usando una grande bombatura di elica" Else sLiM = ""
       Case "UK"
            side(0) = " L " 'Left
            side(1) = " R " 'Right
    sNaturalBias(0) = " Natural - Tooth F."
    sNaturalBias(1) = " Bias Section (a)"
    sNaturalBias(2) = " Bias Section (b)"
    sNaturalBias(3) = " Bias Section (c)"
   BIAS_ANALISIS(0) = " Dres. Bias Cicle"
If (Lp("AT_DRESSING") = 0) Then BIAS_ANALISIS(1) = " Disable" Else BIAS_ANALISIS(1) = " Active"
sVirtualRotationDEG = " Bias Virtual Rot[�]"
     sGrindingCrown = " Part 1" '" Grinding Crown[mm]"
     sDressingCrown = " Part 2" '" Dressing Crown[mm]"
 If (LiM) Then sLiM = " Warning: very large ammount of crown" Else sLiM = ""
       Case "GR"
            side(0) = " L " 'Linke
            side(1) = " R " 'Rechte
    sNaturalBias(0) = " Nat�rliche - Zahn F"
    sNaturalBias(1) = " Abschnitt (a)"
    sNaturalBias(2) = " Abschnitt (b)"
    sNaturalBias(3) = " Abschnitt (c)"
   BIAS_ANALISIS(0) = " Abrichten in Biaszyklus"
If (Lp("AT_DRESSING") = 0) Then BIAS_ANALISIS(1) = " Aus" Else BIAS_ANALISIS(1) = " Aktive"
sVirtualRotationDEG = " Drehung Virtuell[�]"
     sGrindingCrown = " Teil 1" ' " Schleiferei Krone[mm]"
     sDressingCrown = " Teil 2" ' " Dressing Krone[mm]"
 If (LiM) Then sLiM = " Achthung: es wird ein hoher Balligkeitswerte" Else sLiM = ""
       Case Else
            side(0) = " L " 'Left
            side(1) = " R " 'Right
    sNaturalBias(0) = " Natural - Tooth F."
    sNaturalBias(1) = " Bias Section (a)"
    sNaturalBias(2) = " Bias Section (b)"
    sNaturalBias(3) = " Bias Section (c)"
   BIAS_ANALISIS(0) = " Dres. Bias Cicle"
If (Lp("AT_DRESSING") = 0) Then BIAS_ANALISIS(1) = " Disable" Else BIAS_ANALISIS(1) = " Active"
sVirtualRotationDEG = " Virtual Rotation[�]"
     sGrindingCrown = " Part 1" ' " Grinding Crown[mm]"
     sDressingCrown = " Part 2" ' " Dressing Crown[mm]"
 If (LiM) Then sLiM = " Warning: very large ammount of crown" Else sLiM = ""
       End Select


PAR.Pic.FontBold = True
PAR.Pic.ForeColor = vbBlack
PAR.Pic.FontSize = 10
  
Dim Rtmp0 As Double
Dim RTMP1 As Double
Dim CorrRiga As Integer
CorrRiga = 50
  
'Disegna rettangolo
Rtmp0 = PAR.Pic.TextWidth("A")
RTMP1 = PAR.Pic.TextHeight("A")
PAR.Pic.Line (PAR.RegioneX1 + Rtmp0, PAR.RegioneY1 + RTMP1 + CorrRiga)-(PAR.RegioneX1 + Rtmp0, PAR.RegioneY2 - RTMP1)
PAR.Pic.Line (PAR.RegioneX1 + Rtmp0, PAR.RegioneY2 - RTMP1)-(PAR.RegioneX2 - Rtmp0, PAR.RegioneY2 - RTMP1)
PAR.Pic.Line (PAR.RegioneX2 - Rtmp0, PAR.RegioneY2 - RTMP1)-(PAR.RegioneX2 - Rtmp0, PAR.RegioneY1 + RTMP1 + CorrRiga)
PAR.Pic.Line (PAR.RegioneX2 - Rtmp0, PAR.RegioneY1 + RTMP1 + CorrRiga)-(PAR.RegioneX1 + Rtmp0, PAR.RegioneY1 + RTMP1 + CorrRiga)
PAR.Pic.Line (PAR.RegioneX1 + Rtmp0, PAR.RegioneY1 + RTMP1 + CorrRiga)-(PAR.RegioneX1 + Rtmp0, PAR.RegioneY2 - RTMP1)
 
PAR.Pic.FontBold = False
PAR.Pic.FontSize = 10
    
PAR.Pic.CurrentX = PAR.RegioneX1 + PAR.Pic.TextWidth("A")
PAR.Pic.CurrentY = PAR.RegioneY1 + PAR.Pic.TextHeight("A")

  PAR.Pic.Font.Name = "Arial"

  MargineSx = PAR.RegioneX1 + PAR.Pic.TextWidth("A")
  MargineDx = PAR.RegioneX2 - PAR.Pic.TextWidth("A")
  MargineUp = PAR.RegioneY1 + PAR.Pic.TextHeight("A") + CorrRiga
  MargineDw = PAR.RegioneY2 - PAR.Pic.TextHeight("A")

  PosRiga = MargineUp

  PAR.Pic.Line (MargineSx, PosRiga)-(MargineDx, PosRiga), vbBlack
  PosRiga = PosRiga + PAR.Pic.TextHeight("A")
  PAR.Pic.CurrentX = MargineSx + 10
  

  
  'If (Lp("AT_DRESSING") = 0) Then
  

  
  'Else
  
  ncar = Len(sNaturalBias(0))
  If ncar <= LMax(0) Then
     addcar = LMax(0) - ncar
     Text(1) = sNaturalBias(0) & Space(addcar)
  Else
     Text(1) = sNaturalBias(0)
  End If

  ncar = Len(side(0) & sMICRON)
  If ncar <= LMax(1) Then
     addcar = LMax(1) - ncar
     Text(2) = side(0) & sMICRON & Space(addcar)
  Else
     Text(2) = side(0) & sMICRON
  End If
  ncar = Len(side(1) & sMICRON)
  If ncar <= LMax(2) Then
     addcar = LMax(2) - ncar
     Text(3) = side(1) & sMICRON & Space(addcar)
  Else
     Text(3) = side(1) & sMICRON
  End If
  TESTO = Text(1) & ":" & Text(2) & "|" & Text(3)
  
  PAR.Pic.FontSize = 11
  PAR.Pic.Font.Name = "Courier"
  PAR.Pic.Print (TESTO)
  PAR.Pic.Font.Name = "Arial"
  PAR.Pic.FontSize = 10

  
  PAR.Pic.Line (MargineSx, PosRiga)-(MargineDx, PosRiga), vbBlack
  PosRiga = PosRiga + PAR.Pic.TextHeight("A")
  PAR.Pic.CurrentX = MargineSx + 10
  'Lmolautile = TRUNC(Lmolautile * 100) / 100
  
  ncar = Len(sNaturalBias(1))
  If ncar < LMax(0) Then
     addcar = LMax(0) - ncar
     Text(1) = sNaturalBias(1) & Space(addcar)
  Else
     Text(1) = sNaturalBias(1)
  End If
  
  ncar = Len(vNaturalBias(0, 0))
  If ncar <= LMax(1) Then
     addcar = LMax(1) - ncar
     Text(2) = vNaturalBias(0, 0) & Space(addcar)
  Else
     Text(2) = vNaturalBias(0, 0)
  End If
  
  ncar = Len(vNaturalBias(0, 1))
  If ncar <= LMax(2) Then
     addcar = LMax(2) - ncar
     Text(3) = vNaturalBias(0, 1) & Space(addcar)
  Else
     Text(3) = vNaturalBias(0, 1)
  End If
  
  TESTO = Text(1) & ":" & Text(2) & "|" & Text(3)

  PAR.Pic.FontSize = 11
  PAR.Pic.Font.Name = "Courier"
  PAR.Pic.Print (TESTO)
  PAR.Pic.Font.Name = "Arial"
  PAR.Pic.FontSize = 10

  PAR.Pic.Line (MargineSx, PosRiga)-(MargineDx, PosRiga), vbBlack
  PosRiga = PosRiga + PAR.Pic.TextHeight("A")
  PAR.Pic.CurrentX = MargineSx + 10
  'NPezziProf = TRUNC(NPezziProf * 100) / 100
  
  ncar = Len(sNaturalBias(2))
  If ncar < LMax(0) Then
     addcar = LMax(0) - ncar
     Text(1) = sNaturalBias(2) & Space(addcar)
  Else
     Text(1) = sNaturalBias(2)
  End If
  ncar = Len(vNaturalBias(1, 0))
  If ncar <= LMax(1) Then
     addcar = LMax(1) - ncar
     Text(2) = vNaturalBias(1, 0) & Space(addcar)
  Else
     Text(2) = vNaturalBias(1, 0)
  End If
  
  ncar = Len(vNaturalBias(1, 1))
  If ncar <= LMax(2) Then
     addcar = LMax(2) - ncar
     Text(3) = vNaturalBias(1, 1) & Space(addcar)
  Else
     Text(3) = vNaturalBias(1, 1)
  End If
  
  TESTO = Text(1) & ":" & Text(2) & "|" & Text(3)
  
  PAR.Pic.FontSize = 11
  PAR.Pic.Font.Name = "Courier"
  PAR.Pic.Print (TESTO)
  PAR.Pic.Font.Name = "Arial"
  PAR.Pic.FontSize = 10

  PAR.Pic.Line (MargineSx, PosRiga)-(MargineDx, PosRiga), vbBlack
  PosRiga = PosRiga + PAR.Pic.TextHeight("A")
  PAR.Pic.CurrentX = MargineSx + 10

  
  ncar = Len(sNaturalBias(3))
  If ncar < LMax(0) Then
     addcar = LMax(0) - ncar
     Text(1) = sNaturalBias(3) & Space(addcar)
  Else
     Text(1) = sNaturalBias(3)
  End If
  ncar = Len(vNaturalBias(2, 0))
  If ncar <= LMax(1) Then
     addcar = LMax(1) - ncar
     Text(2) = vNaturalBias(2, 0) & Space(addcar)
  Else
     Text(2) = vNaturalBias(2, 0)
  End If
  
  ncar = Len(vNaturalBias(2, 1))
  If ncar <= LMax(2) Then
     addcar = LMax(2) - ncar
     Text(3) = vNaturalBias(2, 1) & Space(addcar)
  Else
     Text(3) = vNaturalBias(2, 1)
  End If
  
  TESTO = Text(1) & ":" & Text(2) & "|" & Text(3)
    
  PAR.Pic.FontSize = 11
  PAR.Pic.Font.Name = "Courier"
  PAR.Pic.Print (TESTO)
  PAR.Pic.Font.Name = "Arial"
  PAR.Pic.FontSize = 10

  PAR.Pic.Line (MargineSx, PosRiga)-(MargineDx, PosRiga), vbBlack
  PosRiga = PosRiga + PAR.Pic.TextHeight("A")
  PAR.Pic.CurrentX = MargineSx + 10
  
  'If (Lp("AT_DRESSING") = 0) Then BIAS_ANALISIS(1) = " Disable" Else BIAS_ANALISIS(1) = " Enable"
   
   ncar = Len(BIAS_ANALISIS(0))
   If ncar < LMax(0) Then
      addcar = LMax(0) - ncar
      Text(1) = BIAS_ANALISIS(0) & Space(addcar)
   Else
      Text(1) = BIAS_ANALISIS(0)
   End If
   ncar = Len(BIAS_ANALISIS(1))
   If ncar < LMax(1) Then
      addcar = LMax(1) - ncar
      Text(2) = BIAS_ANALISIS(1) & Space(addcar)
   Else
      Text(2) = BIAS_ANALISIS(1)
   End If
   TESTO = Text(1) & ":" & Text(2)
  
  PAR.Pic.FontSize = 11
  PAR.Pic.Font.Name = "Courier"
  PAR.Pic.Print (TESTO)
  PAR.Pic.Font.Name = "Arial"
  PAR.Pic.FontSize = 10

  PAR.Pic.Line (MargineSx, PosRiga)-(MargineDx, PosRiga), vbBlack
  PosRiga = PosRiga + PAR.Pic.TextHeight("A")
  PAR.Pic.CurrentX = MargineSx + 10
If (Lp("AT_DRESSING") <> 0 And Pagina = 13) Then
' VirtualRotation

15:
RAGGIOMEDIOMOLA_G = val(OPC_LEGGI_DATO("/ACC/NCK/GUD7/RAGGIOMEDIOMOLA_G")): If (RAGGIOMEDIOMOLA_G = 0) Then RAGGIOMEDIOMOLA_G = Lp("DIAMMOLA_G") / 2#
       INCLMOLA_G = val(OPC_LEGGI_DATO("/ACC/NCK/GUD7/INCLMOLA_G")): If (INCLMOLA_G = 0) Then INCLMOLA_G = ASIN(Lp("MnUT_G") * Lp("NumPrinc_G") / (2# * RAGGIOMEDIOMOLA_G)) * 180# / PI
      PASSOMOLA_G = val(OPC_LEGGI_DATO("/ACC/NCK/GUD7/PASSOMOLA_G")): If (PASSOMOLA_G = 0) Then PASSOMOLA_G = Lp("MnUT_G") * Lp("NumPrinc_G") * PI / Cos(INCLMOLA_G * PI / 180)
    
 Call CYC_PRFVITNP_AT_TH(RAGGIOMEDIOMOLA_G, PASSOMOLA_G, INCLMOLA_G, Lp("FHA_TW[0,0]"), VirtualRotationDEG)
  ncar = Len(sVirtualRotationDEG)
  If ncar <= LMax(0) Then
     addcar = LMax(0) - ncar
     Text(1) = sVirtualRotationDEG & Space(addcar)
  Else
     Text(1) = sVirtualRotationDEG
  End If
  TESTO = Text(1) & ":" & Round(VirtualRotationDEG, 4)
  PAR.Pic.FontSize = 11
  PAR.Pic.Font.Name = "Courier"
  PAR.Pic.Print (TESTO)
  PAR.Pic.Font.Name = "Arial"
  PAR.Pic.FontSize = 10

  PAR.Pic.Line (MargineSx, PosRiga)-(MargineDx, PosRiga), vbBlack
  PosRiga = PosRiga + PAR.Pic.TextHeight("A")
  PAR.Pic.CurrentX = MargineSx + 10
  ' Grinding Crown
  GrindingCrown = (1 - Lp("TW_TUNING")) * BoE
  ncar = Len(sGrindingCrown)
  If ncar <= LMax(0) Then
     addcar = LMax(0) - ncar
     Text(1) = sGrindingCrown & Space(addcar)
  Else
     Text(1) = sGrindingCrown
  End If
  TESTO = Text(1) & ":" & Round(GrindingCrown, 4)
  PAR.Pic.FontSize = 11
  PAR.Pic.Font.Name = "Courier"
  PAR.Pic.Print (TESTO)
  PAR.Pic.Font.Name = "Arial"
  PAR.Pic.FontSize = 10

  PAR.Pic.Line (MargineSx, PosRiga)-(MargineDx, PosRiga), vbBlack
  PosRiga = PosRiga + PAR.Pic.TextHeight("A")
  PAR.Pic.CurrentX = MargineSx + 10
  ' Dressing Crown
  DressingCrown = Lp("TW_TUNING") * BoE
  ncar = Len(sGrindingCrown)
  If ncar <= LMax(0) Then
     addcar = LMax(0) - ncar
     Text(1) = sDressingCrown & Space(addcar)
  Else
     Text(1) = sDressingCrown
  End If
  TESTO = Text(1) & ":" & Round(DressingCrown, 4)
  PAR.Pic.FontSize = 11
  PAR.Pic.Font.Name = "Courier"
  PAR.Pic.Print (TESTO)
  PAR.Pic.Font.Name = "Arial"
  PAR.Pic.FontSize = 10

  PAR.Pic.Line (MargineSx, PosRiga)-(MargineDx, PosRiga), vbBlack
  PosRiga = PosRiga + PAR.Pic.TextHeight("A")
  PAR.Pic.CurrentX = MargineSx + 10
'
Else
 Dim nZone%, DiaMola#, LatoPart%, Beta#, QZI#, PENETRAZIONE#, F1_2%, F2_2%, F3_2%, F1%, F2%, F3%, ExtracorsaOut#, ExtracorsaIn#, QuotaIL#, QuotaFL#, tScala%, YQuota#, tX1#
  NotScale = False
  '
  nZone = Lp("iNzone")
  DE = Lp("DEXT_G")
  DiaMola = Lp("DIAMUTMIN_G")
  LatoPart = Lp("LatoPA")
  Beta = Lp("BETA_G")
  '
  iTESTA = Lp("INDTAV_G")
  If (iTESTA = 1) Then
    QZI = Lp("QASSIALESTART_G[1,1]")
  Else
    QZI = Lp("QASSIALESTART_G[0,1]")
  End If
  '
  PENETRAZIONE = (Lp("DEXT_G") - Lp("DINT_G")) / 2
  '
  F1 = 0:    F2 = 0:    F3 = 0
  F1_2 = 0:  F2_2 = 0:  F3_2 = 0
  Select Case nZone
    Case 1
      F1 = Lp("iFas1Fsx"):   F1_2 = Lp("iFas1Fdx")
    Case 2
      F1 = Lp("iFas1Fsx"):   F2 = Lp("iFas2Fsx")
      F1_2 = Lp("iFas1Fdx"): F2_2 = Lp("iFas2Fdx")
    Case 3
      F1 = Lp("iFas1Fsx"):   F2 = Lp("iFas2Fsx"):      F3 = Lp("iFas3Fsx")
      F1_2 = Lp("iFas1Fdx"): F2_2 = Lp("iFas2Fdx"):    F3_2 = Lp("iFas3Fdx")
  End Select
  Ft = F1 + F2 + F3 'Fascia Totale
  If (Ft = 0) Then
    NotScale = True
    F1 = 50
    F1_2 = 50
    Ft = 50
  End If
  '
  Re = DE / 2
  RS = Re / 4 'Raggio spina
  LS = RS     'Lunghezza spina sporgente
  '
    PAR.MaxY = 6 * LS + Ft
    PAR.MinY = -2 * LS
    PAR.MaxX = Re
    PAR.MinX = 0
   PAR.ScalaMargine = 1
  PAR.Allineamento = 0
  '
   Call CalcolaParPic(PAR, False, Stampante)
  '
   '->PAR.MargineDxPic = PAR.Pic.TextWidth("Z=888.888")
   '->PAR.MargineSxPic = PAR.Pic.TextWidth("Z=888.888")
  '
  Dim XMola As Double
  Dim Ymola As Double
  '
  ExtracorsaOut = Lp("CORSAOUT")
  ExtracorsaIn = Lp("CORSAIN")
  '
  Select Case LatoPart
    
    Case 1 'ALTO
       PAR.MaxY = (2 * LS + Ft + ExtracorsaOut) * 2
       PAR.MinY = -ExtracorsaIn
       PAR.MaxX = 2 * Re
       PAR.MinX = 2
       PAR.ScalaMargine = 1
       PAR.Allineamento = 0
     Call CalcolaParPic(PAR, False, Stampante)
      XMola = LS + Ft + ExtracorsaOut
      Ymola = 0
      QuotaIL = QZI + Ft + ExtracorsaOut
      QuotaFL = QZI - ExtracorsaIn
      tScala = PAR.Scala
      If PAR.Pic Is Printer Then tScala = tScala * 45
      YQuota = Re + 350 / tScala
      'If (Lp("iMetDiv") = 2) Then
      '  XMola = LS + Ft + ExtracorsaIn
      '  Call QuotaV(YQuota, XMola, YQuota, XMola - ExtracorsaIn, "" & ExtracorsaIn, 12, , 5)
      '  QuotaIL = QZI + Ft + ExtracorsaIn
      'End If
   ' ->  Call TestoAll(YQuota, XMola + 200 / tScala, "Z=" & QuotaIL, , True, 2)
      '
    Case -1 'BASSO
      PAR.MaxY = (2 * LS + Ft + ExtracorsaIn) * 2
      PAR.MinY = -ExtracorsaIn
      PAR.MaxX = 2 * Re
      PAR.MinX = 2
      PAR.ScalaMargine = 1
      PAR.Allineamento = 0
      Call CalcolaParPic(PAR, False, Stampante)
      XMola = LS - ExtracorsaOut
      Ymola = 0
      QuotaIL = QZI - ExtracorsaOut
      QuotaFL = QZI + ExtracorsaOut + Ft
      tScala = PAR.Scala
      If PAR.Pic Is Printer Then tScala = tScala * 45
      YQuota = Re + 350 / tScala
      'If (Lp("iMetDiv") = 2) Then
      '  XMola = LS - ExtracorsaIn
      '  Call QuotaV(YQuota, XMola, YQuota, XMola + ExtracorsaIn, "" & ExtracorsaIn, 12, , 5)
      '  QuotaIL = QZI - ExtracorsaIn
      'End If
      Call TestoAll(YQuota, XMola - 200 / tScala, "Z=" & QuotaIL, , True, 1)
      '
  End Select
  '
  YQuota = Re + 700 / tScala
  '
 '-> Call linea(YQuota + DiaMola, XMola, Re, XMola, vbRed, 1, vbDash)
 '-> Call Cerchio(Re + DiaMola / 2 - PENETRAZIONE, XMola, DiaMola / 2, vbRed, 2, 2)
  '
  
  If (LatoPart = -1) Then
    tX1 = ExtracorsaIn / 2
    tX1 = XMola - tX1
  Else
    tX1 = ExtracorsaOut / 2
    tX1 = XMola + tX1
  End If
  '
  'Zone
  Dim XF0#, XF1#, XF2#, XF3#, XF0_2#, XF1_2#, XF2_2#, XF3_2#
  XF0 = LS:    XF1 = XF0 + F1:        XF2 = XF1 + F2:        XF3 = XF2 + F3
  XF0_2 = LS:  XF1_2 = XF0_2 + F1_2:  XF2_2 = XF1_2 + F2_2:  XF3_2 = XF2_2 + F3_2
  '
  'Spina sx
  Call linea(-RS, 0, RS, 0)
  Call linea(-RS, 0, -RS, LS)
  Call linea(RS, 0, RS, LS)
  'Ingranaggio
  Call linea(-Re, LS, Re, LS, vbBlue, 2)
  Call linea(-Re, LS, -Re, LS + Ft, vbBlue, 2)
  Call linea(Re, LS, Re, LS + Ft, vbBlue, 2)
  Call linea(-Re, LS + Ft, Re, LS + Ft, vbBlue, 2)
  'VERSO INGRANAGGIO
  Select Case Beta
    Case Is > 0
      For i = 1 To 8
      Call linea(Re / 4 * (-4 + i - 1), LS, Re / 4 * (-4 + i), LS + Ft, vbBlue, 2)
      Next i
    Case Is < 0
      For i = 1 To 8
      Call linea(-Re / 4 * (-4 + i - 1), LS, -Re / 4 * (-4 + i), LS + Ft, vbBlue, 2)
      Next i
    Case Else
      For i = 1 To 8
      Call linea(Re / 4 * (-4 + i - 1), LS, Re / 4 * (-4 + i - 1), LS + Ft, vbBlue, 2)
      Next i
  End Select
  'Spina dx
  Call linea(-RS, LS + Ft, -RS, 2 * LS + Ft)
  Call linea(RS, LS + Ft, RS, 2 * LS + Ft)
  Call linea(-RS, 2 * LS + Ft, RS, 2 * LS + Ft)
  'Contropunta
  Call linea(RS / 2, 2 * LS + Ft, RS, 3 * LS + Ft, RGB(100, 100, 100))
  Call linea(-RS / 2, 2 * LS + Ft, -RS, 3 * LS + Ft, RGB(100, 100, 100))
  Call linea(RS, 3 * LS + Ft, RS, (8 * LS + Ft) * 0 + LS * 6, RGB(100, 100, 100))
  Call linea(-RS, 3 * LS + Ft, -RS, (8 * LS + Ft) * 0 + LS * 6, RGB(100, 100, 100))
  '
  YQuota = Re + 350 / tScala
  'Call QuotaV(YQuota, LS + Ft, YQuota, LS + Ft + ExtracorsaOut, "" & ExtracorsaOut, 12, , 1)
  'Call QuotaV(YQuota, LS - ExtracorsaIn, YQuota, LS, "" & ExtracorsaIn, 12, , 1)
  
  'a
  Dim qa#, qb#, qc#
  qa = Lp("SPC_TW[0,0]")
  qb = Lp("SPC_TW[1,0]")
  qc = Lp("SPC_TW[2,0]")
  Call QuotaV(YQuota + 10, LS, YQuota + 10, LS + qa, "" & qa, 0, , 2)
  Call QuotaV(YQuota + 25, LS, YQuota + 25, LS + qb, "" & qb, 0, , 2)
  Call QuotaV(YQuota + 40, LS, YQuota + 40, LS + qc, "" & qc, 0, , 2)
  
  Call linea(YQuota + 40, LS + qc, 0, LS + qc, , 1, vbDash)
  Call linea(YQuota + 25, LS + qb, 0, LS + qb, , 1, vbDash)
  Call linea(YQuota + 10, LS + qa, 0, LS + qa, , 1, vbDash)
  Call linea(YQuota + 40, LS, 0, LS, , 1, vbDash)
  '
  If Not NotScale Then
   '> Call QuotaV(YQuota, LS, YQuota, LS + Ft, "" & Ft, 12, , 5)
  Else
   '> Call QuotaV(YQuota, LS, YQuota, LS + Ft, "?", 12, vbRed, 5)
  End If
  '
  PAR.MargineDxPic = 0
  PAR.MargineSxPic = 0
 End If 'If (Lp("AT_DRESSING") = 0) Then
'*
  PAR.Pic.FontSize = 11
  PAR.Pic.Font.Name = "Courier"
'  PAR.Pic.Print (TESTO)
  PAR.Pic.Font.Name = "Arial"
  PAR.Pic.FontSize = 10

' PAR.Pic.Line (MargineSx, PosRiga)-(MargineDx, PosRiga), vbBlack
  PosRiga = PosRiga + PAR.Pic.TextHeight("A")
  PAR.Pic.CurrentX = MargineSx + 10
'*
  PAR.Pic.FontSize = 11
  PAR.Pic.Font.Name = "Courier"
'  PAR.Pic.Print (TESTO)
  PAR.Pic.Font.Name = "Arial"
  PAR.Pic.FontSize = 10
' PAR.Pic.Line (MargineSx, PosRiga)-(MargineDx, PosRiga), vbBlack

  PAR.MargineDxPic = 0
  PAR.MargineSxPic = 0
  If NotScale Then Call TestoInfo("DRAWING NOT IN SCALE ! ", , True, vbRed, True)
'

  If (LiM) Then
   mSec = TimeToMillisecond()  'Hour(Now) * 60# * 60 + Minute(Now) * 60 + Second(Now) 'BIAS_MOLAVITE_TIMER
   ShowWarning = BIAS_MOLAVITE_TIMER = 0 Or (mSec - BIAS_MOLAVITE_TIMER) > 500
   If (ShowWarning) Then
    MsgBox sLiM: BIAS_MOLAVITE_TIMER = TimeToMillisecond()
   End If
 '  WRITE_DIALOG sLiM
  End If
  '
  '
Exit Sub
errDisegna_BIAS_MOLAVITE:
 
  WRITE_DIALOG "SUB Disegna_BIAS_MOLAVITE: ERROR -> " & Err
  Resume Next
  'Exit Sub

End Sub

Public Sub Disegna_ZONA_LAVORO_MOLAVITE(Stampante As Boolean)
'
Dim nZone As Integer
Dim DE    As Double
Dim Ft    As Double
Dim F1    As Double
Dim F2    As Double
Dim F3    As Double

Dim F1_2 As Double
Dim F2_2 As Double
Dim F3_2 As Double

Dim Re As Double
Dim RS As Double
Dim LS As Double

Dim DiaMola         As Double
Dim LatoPart        As Integer
Dim ExtracorsaIn    As Double
Dim ExtracorsaOut   As Double
Dim tColore   As Long
Dim tX1       As Double
Dim tX        As Double
Dim tY        As Double
Dim i         As Integer
Dim NotScale  As Boolean
Dim QuotaIL   As Double
Dim QuotaFL   As Double
Dim XF0       As Double
Dim XF1       As Double
Dim XF2       As Double
Dim XF3       As Double
Dim XF0_2     As Double
Dim XF1_2     As Double
Dim XF2_2     As Double
Dim XF3_2     As Double
Dim tScala    As Double
Dim YQuota    As Double
Dim Beta      As Double
Dim QZI       As Double
Dim iTESTA    As Double
Dim PENETRAZIONE As Double
'
  Call InizializzaVariabili
  '
  NotScale = False
  '
  nZone = Lp("iNzone")
  DE = Lp("DEXT_G")
  DiaMola = Lp("DIAMUTMIN_G")
  LatoPart = Lp("LatoPA")
  Beta = Lp("BETA_G")
  '
  iTESTA = Lp("INDTAV_G")
  If (iTESTA = 1) Then
    QZI = Lp("QASSIALESTART_G[1,1]")
  Else
    QZI = Lp("QASSIALESTART_G[0,1]")
  End If
  '
  PENETRAZIONE = (Lp("DEXT_G") - Lp("DINT_G")) / 2
  '
  F1 = 0:    F2 = 0:    F3 = 0
  F1_2 = 0:  F2_2 = 0:  F3_2 = 0
  Select Case nZone
    Case 1
      F1 = Lp("iFas1Fsx"):   F1_2 = Lp("iFas1Fdx")
    Case 2
      F1 = Lp("iFas1Fsx"):   F2 = Lp("iFas2Fsx")
      F1_2 = Lp("iFas1Fdx"): F2_2 = Lp("iFas2Fdx")
    Case 3
      F1 = Lp("iFas1Fsx"):   F2 = Lp("iFas2Fsx"):      F3 = Lp("iFas3Fsx")
      F1_2 = Lp("iFas1Fdx"): F2_2 = Lp("iFas2Fdx"):    F3_2 = Lp("iFas3Fdx")
  End Select
  Ft = F1 + F2 + F3 'Fascia Totale
  If (Ft = 0) Then
    NotScale = True
    F1 = 50
    F1_2 = 50
    Ft = 50
  End If
  '
  Re = DE / 2
  RS = Re / 4 'Raggio spina
  LS = RS     'Lunghezza spina sporgente
  '
  PAR.MaxY = 6 * LS + Ft
  PAR.MinY = -2 * LS
  PAR.MaxX = Re
  PAR.MinX = 0
  PAR.ScalaMargine = 1
  PAR.Allineamento = 0
  '
  Call CalcolaParPic(PAR, True, Stampante)
  '
  PAR.MargineDxPic = PAR.Pic.TextWidth("Z=888.888")
  PAR.MargineSxPic = PAR.Pic.TextWidth("Z=888.888")
  '
  Dim XMola As Double
  Dim Ymola As Double
  '
  ExtracorsaOut = Lp("CORSAOUT")
  ExtracorsaIn = Lp("CORSAIN")
  '
  Select Case LatoPart
    
    Case 1 'ALTO
      PAR.MaxY = (2 * LS + Ft + ExtracorsaOut)
      PAR.MinY = -ExtracorsaIn
      PAR.MaxX = 2 * Re
      PAR.MinX = 0
      PAR.ScalaMargine = 1
      PAR.Allineamento = 0
      Call CalcolaParPic(PAR, True, Stampante)
      XMola = LS + Ft + ExtracorsaOut
      Ymola = 0
      QuotaIL = QZI + Ft + ExtracorsaOut
      QuotaFL = QZI - ExtracorsaIn
      tScala = PAR.Scala
      If PAR.Pic Is Printer Then tScala = tScala * 45
      YQuota = Re + 350 / tScala
      'If (Lp("iMetDiv") = 2) Then
      '  XMola = LS + Ft + ExtracorsaIn
      '  Call QuotaV(YQuota, XMola, YQuota, XMola - ExtracorsaIn, "" & ExtracorsaIn, 12, , 5)
      '  QuotaIL = QZI + Ft + ExtracorsaIn
      'End If
      Call TestoAll(YQuota, XMola + 200 / tScala, "Z=" & QuotaIL, , True, 2)
      '
    Case -1 'BASSO
      PAR.MaxY = (2 * LS + Ft + ExtracorsaIn)
      PAR.MinY = -ExtracorsaIn
      PAR.MaxX = 2 * Re
      PAR.MinX = 0
      PAR.ScalaMargine = 1
      PAR.Allineamento = 0
      Call CalcolaParPic(PAR, True, Stampante)
      XMola = LS - ExtracorsaOut
      Ymola = 0
      QuotaIL = QZI - ExtracorsaOut
      QuotaFL = QZI + ExtracorsaOut + Ft
      tScala = PAR.Scala
      If PAR.Pic Is Printer Then tScala = tScala * 45
      YQuota = Re + 350 / tScala
      'If (Lp("iMetDiv") = 2) Then
      '  XMola = LS - ExtracorsaIn
      '  Call QuotaV(YQuota, XMola, YQuota, XMola + ExtracorsaIn, "" & ExtracorsaIn, 12, , 5)
      '  QuotaIL = QZI - ExtracorsaIn
      'End If
      Call TestoAll(YQuota, XMola - 200 / tScala, "Z=" & QuotaIL, , True, 1)
      '
  End Select
  '
  YQuota = Re + 700 / tScala
  '
  Call linea(YQuota + DiaMola, XMola, Re, XMola, vbRed, 1, vbDash)
  Call Cerchio(Re + DiaMola / 2 - PENETRAZIONE, XMola, DiaMola / 2, vbRed, 2, 2)
  '
  If (LatoPart = -1) Then
    tX1 = ExtracorsaIn / 2
    tX1 = XMola - tX1
  Else
    tX1 = ExtracorsaOut / 2
    tX1 = XMola + tX1
  End If
  '
  'Zone
  XF0 = LS:    XF1 = XF0 + F1:        XF2 = XF1 + F2:        XF3 = XF2 + F3
  XF0_2 = LS:  XF1_2 = XF0_2 + F1_2:  XF2_2 = XF1_2 + F2_2:  XF3_2 = XF2_2 + F3_2
  '
  'Spina sx
  Call linea(-RS, 0, RS, 0)
  Call linea(-RS, 0, -RS, LS)
  Call linea(RS, 0, RS, LS)
  'Ingranaggio
  Call linea(-Re, LS, Re, LS, vbBlue, 2)
  Call linea(-Re, LS, -Re, LS + Ft, vbBlue, 2)
  Call linea(Re, LS, Re, LS + Ft, vbBlue, 2)
  Call linea(-Re, LS + Ft, Re, LS + Ft, vbBlue, 2)
  'VERSO INGRANAGGIO
  Select Case Beta
    Case Is > 0
      For i = 1 To 8
      Call linea(Re / 4 * (-4 + i - 1), LS, Re / 4 * (-4 + i), LS + Ft, vbBlue, 2)
      Next i
    Case Is < 0
      For i = 1 To 8
      Call linea(-Re / 4 * (-4 + i - 1), LS, -Re / 4 * (-4 + i), LS + Ft, vbBlue, 2)
      Next i
    Case Else
      For i = 1 To 8
      Call linea(Re / 4 * (-4 + i - 1), LS, Re / 4 * (-4 + i - 1), LS + Ft, vbBlue, 2)
      Next i
  End Select
  'Spina dx
  Call linea(-RS, LS + Ft, -RS, 2 * LS + Ft)
  Call linea(RS, LS + Ft, RS, 2 * LS + Ft)
  Call linea(-RS, 2 * LS + Ft, RS, 2 * LS + Ft)
  'Contropunta
  Call linea(RS / 2, 2 * LS + Ft, RS, 3 * LS + Ft, RGB(100, 100, 100))
  Call linea(-RS / 2, 2 * LS + Ft, -RS, 3 * LS + Ft, RGB(100, 100, 100))
  Call linea(RS, 3 * LS + Ft, RS, 8 * LS + Ft, RGB(100, 100, 100))
  Call linea(-RS, 3 * LS + Ft, -RS, 8 * LS + Ft, RGB(100, 100, 100))
  '
  YQuota = Re + 350 / tScala
  Call QuotaV(YQuota, LS + Ft, YQuota, LS + Ft + ExtracorsaOut, "" & ExtracorsaOut, 12, , 1)
  Call QuotaV(YQuota, LS - ExtracorsaIn, YQuota, LS, "" & ExtracorsaIn, 12, , 1)
  '
  If Not NotScale Then
    Call QuotaV(YQuota, LS, YQuota, LS + Ft, "" & Ft, 12, , 5)
  Else
    Call QuotaV(YQuota, LS, YQuota, LS + Ft, "?", 12, vbRed, 5)
  End If
  '
  PAR.MargineDxPic = 0
  PAR.MargineSxPic = 0
  If NotScale Then Call TestoInfo("DRAWING NOT IN SCALE ! ", , True, vbRed, True)
  '
  If (iTESTA = 1) Then
    Call TestoInfo("GRINDING on C2", 2, True, vbRed)
  Else
    Call TestoInfo("GRINDING on C1", 2, True, vbRed)
  End If
  Call TestoInfo("Units: [mm]", 3)
  '
End Sub

Public Sub Disegna_ZONA_LAVORO_PROFILO_PER_PUNTI_ESTERNI(Stampante As Boolean)
'
Dim nZone           As Integer
Dim DE              As Double
Dim Ft              As Double
Dim F1              As Double
Dim F2              As Double
Dim F3              As Double
Dim F1_2            As Double
Dim F2_2            As Double
Dim F3_2            As Double
Dim Re              As Double
Dim RS              As Double
Dim LS              As Double
Dim DiaMola         As Double
Dim LatoPart        As Integer
Dim ExtracorsaIn    As Double
Dim ExtracorsaOut   As Double
Dim TrattoDivisione As Double
Dim tColore         As Long
Dim tX1             As Double
Dim tX              As Double
Dim tY              As Double
Dim i               As Integer
Dim NotScale        As Boolean
Dim QuotaIL         As Double
Dim QuotaFL         As Double
Dim XF0             As Double
Dim XF1             As Double
Dim XF2             As Double
Dim XF3             As Double
Dim XF0_2           As Double
Dim XF1_2           As Double
Dim XF2_2           As Double
Dim XF3_2           As Double
Dim tScala          As Double
Dim YQuota          As Double
Dim Beta            As Double
Dim QZI             As Double
'
  Call InizializzaVariabili
  '
  NotScale = False
  'Lettura parametri ingranaggio
  nZone = Lp("iNzone")
  DE = Lp("DEXT_G")
  DiaMola = Lp("iDiaMola")
  LatoPart = Lp("R[12]")
  QZI = Lp("QASSIALESTART_G")
  ExtracorsaIn = Lp("CORSAIN")
  ExtracorsaOut = Lp("CORSAOUT")
  TrattoDivisione = Lp("R[130]")
  Beta = Lp("R[41]")
  '
  F1 = 0:    F2 = 0:    F3 = 0
  F1_2 = 0:  F2_2 = 0:  F3_2 = 0
  Select Case nZone
    Case 1
      F1 = Lp("Fascia1F1"):   F1_2 = Lp("Fascia1F2")
    Case 2
      F1 = Lp("Fascia1F1"):   F2 = Lp("Fascia2F1")
      F1_2 = Lp("Fascia1F2"): F2_2 = Lp("Fascia2F2")
    Case 3
      F1 = Lp("Fascia1F1"):   F2 = Lp("Fascia2F1"):   F3 = Lp("Fascia3F1")
      F1_2 = Lp("Fascia1F2"): F2_2 = Lp("Fascia2F2"): F3_2 = Lp("Fascia3F2")
  End Select
  Ft = F1 + F2 + F3 'Fascia Totale
  If Ft = 0 Then
    NotScale = True
    F1 = 50
    F1_2 = 50
    Ft = 50
  End If
  Re = DE / 2
  RS = Re / 4 'Raggio spina
  LS = RS 'Lunghezza spina sporgente
  '
  PAR.MaxX = 6 * LS + Ft
  PAR.MinX = -2 * LS
  PAR.MaxY = Re '+ 35
  PAR.MinY = 0 ' -0.5 * LS '-Re
  PAR.ScalaMargine = 1
  PAR.Allineamento = 0
  
  Call CalcolaParPic(PAR, True, Stampante)
  
  PAR.MargineDxPic = PAR.Pic.TextWidth("Z=888.888")
  PAR.MargineSxPic = PAR.Pic.TextWidth("Z=888.888")
  '
  Dim XMola As Double
  Dim Ymola As Double
  
  Select Case LatoPart
    
    Case -1 'Destra
      PAR.MaxX = (2 * LS + Ft + ExtracorsaOut + TrattoDivisione)
      PAR.MinX = -ExtracorsaIn
      PAR.MaxY = 2 * Re
      PAR.MinY = 0
      PAR.ScalaMargine = 1
      PAR.Allineamento = 0
      Call CalcolaParPic(PAR, True, Stampante)
      XMola = LS + Ft + ExtracorsaOut
      Ymola = 0
      QuotaIL = QZI + Ft + ExtracorsaOut
      QuotaFL = QZI - ExtracorsaIn
      tScala = PAR.Scala
      If PAR.Pic Is Printer Then tScala = tScala * 45
      YQuota = Re + 350 / tScala
      If (Lp("iMetDiv") = 2) Then
        XMola = XMola + TrattoDivisione
        QuotaH XMola - TrattoDivisione, YQuota, XMola, YQuota, "" & TrattoDivisione, 12, , 3
        QuotaIL = QuotaIL + TrattoDivisione
      End If
      Call TestoAll(XMola + 200 / tScala, YQuota, "Z=" & QuotaIL, , True, 2)
      Call TestoAll(XMola - TrattoDivisione - ExtracorsaIn - ExtracorsaOut - Ft - 200 / tScala, YQuota, "Z=" & QuotaFL, , True, 1)
      '
    Case 1 'Sinistra
      PAR.MaxX = (2 * LS + Ft + ExtracorsaOut)
      PAR.MinX = -ExtracorsaIn - TrattoDivisione
      PAR.MaxY = 2 * Re
      PAR.MinY = 0
      PAR.ScalaMargine = 1
      PAR.Allineamento = 0
      Call CalcolaParPic(PAR, True, Stampante)
      XMola = LS - ExtracorsaIn
      Ymola = 0
      QuotaIL = QZI - ExtracorsaIn
      QuotaFL = QZI + Ft + ExtracorsaOut
      tScala = PAR.Scala
      If PAR.Pic Is Printer Then tScala = tScala * 45
      YQuota = Re + 350 / tScala
      If (Lp("iMetDiv") = 2) Then
        XMola = XMola - TrattoDivisione
        QuotaH XMola, YQuota, XMola + TrattoDivisione, YQuota, "" & TrattoDivisione, 12, , 3
        QuotaIL = QuotaIL - TrattoDivisione
      End If
      Call TestoAll(XMola - 200 / tScala, YQuota, "Z=" & QuotaIL, , True, 1)
      Call TestoAll(XMola + TrattoDivisione + ExtracorsaIn + ExtracorsaOut + Ft + 200 / tScala, YQuota, "Z=" & QuotaFL, , True, 2)
      '
  End Select
  '
  YQuota = Re + 700 / tScala
  '
  'Centro mola
  'Disegno della mola
  Dim SPE     As Double
  Dim spe1    As Double
  Dim spe2    As Double
  Dim Altezza As Double
  '
  SPE = 30
  SPE = SPE / 2
  Altezza = 10
  spe1 = 1 / 2 * SPE
  spe2 = 1 / 4 * SPE
  '
  Call linea(XMola - SPE, Re + 25 + Altezza + DiaMola, XMola - SPE, Re + 25 + Altezza, RGB(0, 0, 0), 1, vbSolid)
  Call linea(XMola - SPE, Re + 25 + Altezza, XMola - spe1, Re + 25 + Altezza, RGB(0, 0, 0), 1, vbSolid)
  Call linea(XMola - spe1, Re + 25 + Altezza, XMola - spe2, Re + 25, RGB(0, 0, 0), 1, vbSolid)
  Call linea(XMola + SPE, Re + 25 + Altezza + DiaMola, XMola + SPE, Re + 25 + Altezza, RGB(0, 0, 0), 1, vbSolid)
  Call linea(XMola + SPE, Re + 25 + Altezza, XMola + spe1, Re + 25 + Altezza, RGB(0, 0, 0), 1, vbSolid)
  Call linea(XMola + spe1, Re + 25 + Altezza, XMola + spe2, Re + 25, RGB(0, 0, 0), 1, vbSolid)
  Call linea(XMola - spe2, Re + 25, XMola + spe2, Re + 25, RGB(0, 0, 0), 1, vbSolid)
  Call linea(XMola, YQuota + DiaMola, XMola, Re, vbRed, 1, vbDash)
  '
  If LatoPart = -1 Then
    tX1 = ExtracorsaIn / 2
    tX1 = XMola - tX1
  Else
    tX1 = ExtracorsaOut / 2
    tX1 = XMola + tX1
  End If
  '
  'Zone
  XF0 = LS
  XF1 = XF0 + F1
  XF2 = XF1 + F2
  XF3 = XF2 + F3
  XF0_2 = LS
  XF1_2 = XF0_2 + F1_2
  XF2_2 = XF1_2 + F2_2
  XF3_2 = XF2_2 + F3_2
  'Spina sx
  Call linea(0, -RS, 0, RS)
  Call linea(0, -RS, LS, -RS)
  Call linea(0, RS, LS, RS)
  'Ingranaggio
  Call linea(LS, -Re, LS, Re, vbBlue, 2)
  Call linea(LS, -Re, LS + Ft, -Re, vbBlue, 2)
  Call linea(LS, Re, LS + Ft, Re, vbBlue, 2)
  Call linea(LS + Ft, -Re, LS + Ft, Re, vbBlue, 2)
  'VERSO INGRANAGGIO
  Select Case Beta
    Case Is > 0
      For i = 1 To 8
      Call linea(LS, -Re / 4 * (-4 + i - 1), LS + Ft, -Re / 4 * (-4 + i), vbBlue, 2)
      Next i
    Case Is < 0
      For i = 1 To 8
      Call linea(LS, Re / 4 * (-4 + i - 1), LS + Ft, Re / 4 * (-4 + i), vbBlue, 2)
      Next i
    Case Else
      For i = 1 To 8
      Call linea(LS, Re / 4 * (-4 + i - 1), LS + Ft, Re / 4 * (-4 + i - 1), vbBlue, 2)
      Next i
  End Select
  'Spina dx
  Call linea(LS + Ft, -RS, 2 * LS + Ft, -RS)
  Call linea(LS + Ft, RS, 2 * LS + Ft, RS)
  Call linea(2 * LS + Ft, -RS, 2 * LS + Ft, RS)
  'Contropunta
  Call linea(2 * LS + Ft, RS / 2, 3 * LS + Ft, RS, RGB(100, 100, 100))
  Call linea(2 * LS + Ft, -RS / 2, 3 * LS + Ft, -RS, RGB(100, 100, 100))
  Call linea(3 * LS + Ft, RS, 8 * LS + Ft, RS, RGB(100, 100, 100))
  Call linea(3 * LS + Ft, -RS, 8 * LS + Ft, -RS, RGB(100, 100, 100))
  YQuota = Re + 350 / tScala
  '
  Call QuotaH(LS + Ft, YQuota, LS + Ft + ExtracorsaOut, YQuota, "" & ExtracorsaOut, 12, , 3)
  Call QuotaH(LS - ExtracorsaIn, YQuota, LS, YQuota, "" & ExtracorsaIn, 12, , 3)
  '
  If Not NotScale Then
    Call QuotaH(LS, YQuota, LS + Ft, YQuota, "" & Ft, 12, , 3)
  Else
    Call QuotaH(LS, YQuota, LS + Ft, YQuota, "?", 12, vbRed, 3)
  End If
  '
  PAR.MargineDxPic = 0
  PAR.MargineSxPic = 0
  If NotScale Then Call TestoInfo("DRAWING NOT IN SCALE ! ", , True, vbRed, True)
  '
  Call TestoInfo("Units: [mm]", 3)
  '
End Sub

Public Sub Disegna_ZONA_LAVORO_INGRANAGGI_ESTERNI(Stampante As Boolean)
'
Dim nZone           As Integer
Dim DE              As Double
Dim Ft              As Double
Dim F1              As Double
Dim F2              As Double
Dim F3              As Double
Dim F1_2            As Double
Dim F2_2            As Double
Dim F3_2            As Double
Dim Re              As Double
Dim RS              As Double
Dim LS              As Double
Dim DiaMola         As Double
Dim AngMola         As Double
Dim LatoPart        As Integer
Dim ExtracorsaIn    As Double
Dim ExtracorsaOut   As Double
Dim TipoDivisione   As Double
Dim TrattoDivisione As Double
Dim tColore         As Long
Dim tX1             As Double
Dim tX              As Double
Dim tY              As Double
Dim i               As Integer
Dim NotScale        As Boolean
Dim QuotaIL         As Double
Dim QuotaFL         As Double
Dim XF0             As Double
Dim XF1             As Double
Dim XF2             As Double
Dim XF3             As Double
Dim XF0_2           As Double
Dim XF1_2           As Double
Dim XF2_2           As Double
Dim XF3_2           As Double
Dim tScala          As Double
Dim YQuota          As Double
Dim Beta            As Double
Dim QZI             As Double
'
  Call InizializzaVariabili
  '
  NotScale = False
  'Lettura parametri ingranaggio
  nZone = Lp("iNzone")
  DE = Lp("DEXT_G")
  DiaMola = Lp("iDiaMola")
  AngMola = Lp("R[258]")
  LatoPart = Lp("R[12]")
  QZI = Lp("QASSIALESTART_G")
  ExtracorsaIn = Lp("CORSAIN")
  ExtracorsaOut = Lp("CORSAOUT")
  '
  TipoDivisione = Lp("iMetDiv")
  TrattoDivisione = Lp("R[130]")
  '
  Beta = Lp("R[41]")
  '
  F1 = 0:    F2 = 0:    F3 = 0
  F1_2 = 0:  F2_2 = 0:  F3_2 = 0
  Select Case nZone
    Case 1
      F1 = Lp("Fascia1F1"):   F1_2 = Lp("Fascia1F2")
    Case 2
      F1 = Lp("Fascia1F1"):   F2 = Lp("Fascia2F1")
      F1_2 = Lp("Fascia1F2"): F2_2 = Lp("Fascia2F2")
    Case 3
      F1 = Lp("Fascia1F1"):   F2 = Lp("Fascia2F1"):   F3 = Lp("Fascia3F1")
      F1_2 = Lp("Fascia1F2"): F2_2 = Lp("Fascia2F2"): F3_2 = Lp("Fascia3F2")
  End Select
  Ft = F1 + F2 + F3 'Fascia Totale
  If Ft = 0 Then
    NotScale = True
    F1 = 50
    F1_2 = 50
    Ft = 50
  End If
  Re = DE / 2
  RS = Re / 4 'Raggio spina
  LS = RS 'Lunghezza spina sporgente
  '
  PAR.MaxX = 6 * LS + Ft
  PAR.MinX = -2 * LS
  PAR.MaxY = Re '+ 35
  PAR.MinY = 0 ' -0.5 * LS '-Re
  PAR.ScalaMargine = 1
  PAR.Allineamento = 0
  
  Call CalcolaParPic(PAR, True, Stampante)
  
  PAR.MargineDxPic = PAR.Pic.TextWidth("Z=888.888")
  PAR.MargineSxPic = PAR.Pic.TextWidth("Z=888.888")
  '
  Dim XMola As Double
  Dim Ymola As Double
  
  Select Case LatoPart
    
    Case -1 'Destra
      PAR.MaxX = (2 * LS + Ft + ExtracorsaOut + TrattoDivisione)
      PAR.MinX = -ExtracorsaIn
      PAR.MaxY = 2 * Re
      PAR.MinY = 0
      PAR.ScalaMargine = 1
      PAR.Allineamento = 0
      Call CalcolaParPic(PAR, True, Stampante)
      XMola = LS + Ft + ExtracorsaOut
      Ymola = 0
      QuotaIL = QZI + Ft + ExtracorsaOut
      QuotaFL = QZI - ExtracorsaIn
      tScala = PAR.Scala
      If PAR.Pic Is Printer Then tScala = tScala * 45
      YQuota = Re + 350 / tScala
      If (TipoDivisione = 2) Then
        XMola = XMola + TrattoDivisione
        QuotaH XMola - TrattoDivisione, YQuota, XMola, YQuota, "" & TrattoDivisione, 12, , 3
        QuotaIL = QuotaIL + TrattoDivisione
      End If
      Call TestoAll(XMola + 200 / tScala, YQuota, "Z=" & QuotaIL, , True, 2)
      Call TestoAll(XMola - TrattoDivisione - ExtracorsaIn - ExtracorsaOut - Ft - 200 / tScala, YQuota, "Z=" & QuotaFL, , True, 1)
      '
    Case 1 'Sinistra
      PAR.MaxX = (2 * LS + Ft + ExtracorsaOut)
      PAR.MinX = -ExtracorsaIn - TrattoDivisione
      PAR.MaxY = 2 * Re
      PAR.MinY = 0
      PAR.ScalaMargine = 1
      PAR.Allineamento = 0
      Call CalcolaParPic(PAR, True, Stampante)
      XMola = LS - ExtracorsaIn
      Ymola = 0
      QuotaIL = QZI - ExtracorsaIn
      QuotaFL = QZI + Ft + ExtracorsaOut
      tScala = PAR.Scala
      If PAR.Pic Is Printer Then tScala = tScala * 45
      YQuota = Re + 350 / tScala
      If (TipoDivisione = 2) Then
        XMola = XMola - TrattoDivisione
        QuotaH XMola, YQuota, XMola + TrattoDivisione, YQuota, "" & TrattoDivisione, 12, , 3
        QuotaIL = QuotaIL - TrattoDivisione
      End If
      Call TestoAll(XMola - 200 / tScala, YQuota, "Z=" & QuotaIL, , True, 1)
      Call TestoAll(XMola + TrattoDivisione + ExtracorsaIn + ExtracorsaOut + Ft + 200 / tScala, YQuota, "Z=" & QuotaFL, , True, 2)
      '
  End Select
  '
  YQuota = Re + 700 / tScala
  Call Disegna_Mola_INGRANAGGI_ESTERNI(XMola, YQuota + DiaMola / 2, vbRed, 2)
  Call linea(XMola, YQuota + DiaMola, XMola, Re, vbRed, 1, vbDash)
  '
  If LatoPart = -1 Then
    tX1 = ExtracorsaIn / 2
    tX1 = XMola - tX1
  Else
    tX1 = ExtracorsaOut / 2
    tX1 = XMola + tX1
  End If
  '
  'Zone
  XF0 = LS
  XF1 = XF0 + F1
  XF2 = XF1 + F2
  XF3 = XF2 + F3
  XF0_2 = LS
  XF1_2 = XF0_2 + F1_2
  XF2_2 = XF1_2 + F2_2
  XF3_2 = XF2_2 + F3_2
  'Spina sx
  Call linea(0, -RS, 0, RS)
  Call linea(0, -RS, LS, -RS)
  Call linea(0, RS, LS, RS)
  'Ingranaggio
  Call linea(LS, -Re, LS, Re, vbBlue, 2)
  Call linea(LS, -Re, LS + Ft, -Re, vbBlue, 2)
  Call linea(LS, Re, LS + Ft, Re, vbBlue, 2)
  Call linea(LS + Ft, -Re, LS + Ft, Re, vbBlue, 2)
  'VERSO INGRANAGGIO
  Select Case Beta
    Case Is > 0
      For i = 1 To 8
      Call linea(LS, -Re / 4 * (-4 + i - 1), LS + Ft, -Re / 4 * (-4 + i), vbBlue, 2)
      Next i
    Case Is < 0
      For i = 1 To 8
      Call linea(LS, Re / 4 * (-4 + i - 1), LS + Ft, Re / 4 * (-4 + i), vbBlue, 2)
      Next i
    Case Else
      For i = 1 To 8
      Call linea(LS, Re / 4 * (-4 + i - 1), LS + Ft, Re / 4 * (-4 + i - 1), vbBlue, 2)
      Next i
  End Select
  'Spina dx
  Call linea(LS + Ft, -RS, 2 * LS + Ft, -RS)
  Call linea(LS + Ft, RS, 2 * LS + Ft, RS)
  Call linea(2 * LS + Ft, -RS, 2 * LS + Ft, RS)
  'Contropunta
  Call linea(2 * LS + Ft, RS / 2, 3 * LS + Ft, RS, RGB(100, 100, 100))
  Call linea(2 * LS + Ft, -RS / 2, 3 * LS + Ft, -RS, RGB(100, 100, 100))
  Call linea(3 * LS + Ft, RS, 8 * LS + Ft, RS, RGB(100, 100, 100))
  Call linea(3 * LS + Ft, -RS, 8 * LS + Ft, -RS, RGB(100, 100, 100))
  YQuota = Re + 350 / tScala
  '
  Call QuotaH(LS + Ft, YQuota, LS + Ft + ExtracorsaOut, YQuota, "" & ExtracorsaOut, 12, , 3)
  Call QuotaH(LS - ExtracorsaIn, YQuota, LS, YQuota, "" & ExtracorsaIn, 12, , 3)
  '
  If Not NotScale Then
    Call QuotaH(LS, YQuota, LS + Ft, YQuota, "" & Ft, 12, , 3)
  Else
    Call QuotaH(LS, YQuota, LS + Ft, YQuota, "?", 12, vbRed, 3)
  End If
  '
  PAR.MargineDxPic = 0
  PAR.MargineSxPic = 0
  If NotScale Then Call TestoInfo("DRAWING NOT IN SCALE ! ", , True, vbRed, True)
  '
  Call TestoInfo("Units: [mm]", 3)
  '
End Sub

Public Sub Disegna_ZONA_LAVORO_INGRANAGGI_ESTERNIO(Stampante As Boolean)
'
Dim nZone           As Integer
Dim DE              As Double
Dim Ft              As Double
Dim F1              As Double
Dim F2              As Double
Dim F3              As Double
Dim F1_2            As Double
Dim F2_2            As Double
Dim F3_2            As Double
Dim Re              As Double
Dim RS              As Double
Dim LS              As Double
Dim DiaMola         As Double
Dim AngMola         As Double
Dim LatoPart        As Integer
Dim ExtracorsaIn    As Double
Dim ExtracorsaOut   As Double
Dim TipoDivisione   As Double
Dim TrattoDivisione As Double
Dim tColore         As Long
Dim tX1             As Double
Dim tX              As Double
Dim tY              As Double
Dim i               As Integer
Dim NotScale        As Boolean
Dim QuotaIL         As Double
Dim QuotaFL         As Double
Dim XF0             As Double
Dim XF1             As Double
Dim XF2             As Double
Dim XF3             As Double
Dim XF0_2           As Double
Dim XF1_2           As Double
Dim XF2_2           As Double
Dim XF3_2           As Double
Dim tScala          As Double
Dim YQuota          As Double
Dim Beta            As Double
Dim QZI             As Double
Dim SALTO           As Integer
Dim stmp            As String
Dim PosMolaY        As Double
'
  Call InizializzaVariabili
  '
  NotScale = False
  'Lettura parametri ingranaggio
  nZone = Lp("iNzone")
  DE = Lp("DEXT_G")
  DiaMola = Lp("iDiaMola")
  AngMola = Lp("R[258]")
  LatoPart = Lp("R[12]")
  QZI = Lp("QASSIALESTART_G")
  ExtracorsaIn = Lp("CORSAIN")
  ExtracorsaOut = Lp("CORSAOUT")
  '
  TipoDivisione = Lp("iMetDiv")
  TrattoDivisione = Lp("R[130]")
  '
  Beta = Lp("R[41]")
  '
  F1 = 0:    F2 = 0:    F3 = 0
  F1_2 = 0:  F2_2 = 0:  F3_2 = 0
  Select Case nZone
    Case 1
      F1 = Lp("Fascia1F1"):   F1_2 = Lp("Fascia1F2")
    Case 2
      F1 = Lp("Fascia1F1"):   F2 = Lp("Fascia2F1")
      F1_2 = Lp("Fascia1F2"): F2_2 = Lp("Fascia2F2")
    Case 3
      F1 = Lp("Fascia1F1"):   F2 = Lp("Fascia2F1"):   F3 = Lp("Fascia3F1")
      F1_2 = Lp("Fascia1F2"): F2_2 = Lp("Fascia2F2"): F3_2 = Lp("Fascia3F2")
  End Select
  Ft = F1 + F2 + F3 'Fascia Totale
  If Ft = 0 Then
    NotScale = True
    F1 = 50
    F1_2 = 50
    Ft = 50
  End If
  Re = DE / 2
  RS = Re / 4 'Raggio spina
  LS = RS 'Lunghezza spina sporgente
  '
  PAR.MaxX = 6 * LS + Ft
  PAR.MinX = -2 * LS
  PAR.MaxY = Re '+ 35
  PAR.MinY = 0 ' -0.5 * LS '-Re
  PAR.ScalaMargine = 1
  PAR.Allineamento = 0
  
  Call CalcolaParPic(PAR, True, Stampante)
  
  PAR.MargineDxPic = PAR.Pic.TextWidth("Z=888.888")
  PAR.MargineSxPic = PAR.Pic.TextWidth("Z=888.888")
  '
  Dim XMola As Double
  Dim Ymola As Double
  
  Select Case LatoPart
    
    Case -1 'Destra
      PAR.MaxX = (2 * LS + Ft + ExtracorsaOut + TrattoDivisione)
      PAR.MinX = -ExtracorsaIn
      PAR.MaxY = 2 * Re
      PAR.MinY = 0
      PAR.ScalaMargine = 1
      PAR.Allineamento = 0
      Call CalcolaParPic(PAR, True, Stampante)
      XMola = LS + Ft + ExtracorsaOut
      Ymola = 0
      QuotaIL = QZI + Ft + ExtracorsaOut
      QuotaFL = QZI - ExtracorsaIn
      tScala = PAR.Scala
      If PAR.Pic Is Printer Then tScala = tScala * 45
      YQuota = Re + 350 / tScala
      If (TipoDivisione = 2) Then
        XMola = XMola + TrattoDivisione
        QuotaH XMola - TrattoDivisione, YQuota, XMola, YQuota, "" & TrattoDivisione, 12, , 3
        QuotaIL = QuotaIL + TrattoDivisione
      End If
      Call TestoAll(XMola + 200 / tScala, YQuota, "Z=" & QuotaIL, , True, 2)
      Call TestoAll(XMola - TrattoDivisione - ExtracorsaIn - ExtracorsaOut - Ft - 200 / tScala, YQuota, "Z=" & QuotaFL, , True, 1)
      '
    Case 1 'Sinistra
      PAR.MaxX = (2 * LS + Ft + ExtracorsaOut)
      PAR.MinX = -ExtracorsaIn - TrattoDivisione
      PAR.MaxY = 2 * Re
      PAR.MinY = 0
      PAR.ScalaMargine = 1
      PAR.Allineamento = 0
      Call CalcolaParPic(PAR, True, Stampante)
      XMola = LS - ExtracorsaIn
      Ymola = 0
      QuotaIL = QZI - ExtracorsaIn
      QuotaFL = QZI + Ft + ExtracorsaOut
      tScala = PAR.Scala
      If PAR.Pic Is Printer Then tScala = tScala * 45
      YQuota = Re + 350 / tScala
      If (TipoDivisione = 2) Then
        XMola = XMola - TrattoDivisione
        QuotaH XMola, YQuota, XMola + TrattoDivisione, YQuota, "" & TrattoDivisione, 12, , 3
        QuotaIL = QuotaIL - TrattoDivisione
      End If
      Call TestoAll(XMola - 200 / tScala, YQuota, "Z=" & QuotaIL, , True, 1)
      Call TestoAll(XMola + TrattoDivisione + ExtracorsaIn + ExtracorsaOut + Ft + 200 / tScala, YQuota, "Z=" & QuotaFL, , True, 2)
      '
  End Select
  '
  YQuota = Re + 700 / tScala
  Select Case ActualGroup
    Case "OEM0_CICLI92"
      SALTO = Lp("NUMDIV_G[0,1]")
      PosMolaY = Lp("POSMOLA_G[0,1]")
      stmp = "M1"
    Case "OEM0_CICLIB92"
      SALTO = Lp("NUMDIV_G[0,2]")
      PosMolaY = Lp("POSMOLA_G[0,2]")
      stmp = "M2"
    Case Else
      SALTO = 1
      PosMolaY = 0
      stmp = ""
  End Select
  '
  'SVG181016
  'SE SONO PARI DEVO INVERTIRE IL PIENO COL VUOTO
  Dim RTMP As Double
  RTMP = SALTO Mod 2
  If (RTMP = 0) Then RTMP = 1 Else RTMP = 0
  '
  For i = 1 To SALTO
    Call Disegna_Mola_INGRANAGGI_ESTERNIO(XMola + CalcoloEsterni.MolaX(1, 1) * RTMP + 2 * CalcoloEsterni.MolaX(1, 1) * (i - Fix(SALTO / 2) - 1), YQuota + DiaMola / 2, vbRed, 2)
  Next i
  XF1 = 2 * CalcoloEsterni.MolaX(1, 1) * (1 - Fix(SALTO / 2) - 1) - CalcoloEsterni.MolaX(1, 1) + CalcoloEsterni.MolaX(1, 1) * RTMP
  XF2 = 2 * CalcoloEsterni.MolaX(2, 1) * (SALTO - Fix(SALTO / 2) - 1) + CalcoloEsterni.MolaX(2, 1) + CalcoloEsterni.MolaX(1, 1) * RTMP
  Call linea(XMola + XF1, YQuota + DiaMola / 2 - CalcoloEsterni.MolaY(1, 1), XMola + XF1, 4 * Re, vbBlack, 1, vbSolid)
  Call linea(XMola + XF2, YQuota + DiaMola / 2 - CalcoloEsterni.MolaY(2, 1), XMola + XF2, 4 * Re, vbRed, 1, vbSolid)
  Call linea(XMola, YQuota + DiaMola, XMola, Re, vbRed, 1, vbDash)
  If (PosMolaY <> 0) Then
    Call TestoAll(XMola + XF2, YQuota, "Y=" & PosMolaY, , True, 2)
  End If
  If (Len(stmp) > 0) Then
    Call TestoAll(XMola, 4 * Re, stmp, , True, 3)
  End If
  '
  If (LatoPart = -1) Then
    tX1 = ExtracorsaIn / 2
    tX1 = XMola - tX1
  Else
    tX1 = ExtracorsaOut / 2
    tX1 = XMola + tX1
  End If
  '
  'Zone
  XF0 = LS
  XF1 = XF0 + F1
  XF2 = XF1 + F2
  XF3 = XF2 + F3
  XF0_2 = LS
  XF1_2 = XF0_2 + F1_2
  XF2_2 = XF1_2 + F2_2
  XF3_2 = XF2_2 + F3_2
  'Spina sx
  Call linea(0, -RS, 0, RS)
  Call linea(0, -RS, LS, -RS)
  Call linea(0, RS, LS, RS)
  'Ingranaggio
  Call linea(LS, -Re, LS, Re, vbBlue, 2)
  Call linea(LS, -Re, LS + Ft, -Re, vbBlue, 2)
  Call linea(LS, Re, LS + Ft, Re, vbBlue, 2)
  Call linea(LS + Ft, -Re, LS + Ft, Re, vbBlue, 2)
  'VERSO INGRANAGGIO
  Select Case Beta
    Case Is > 0
      For i = 1 To 8
      Call linea(LS, -Re / 4 * (-4 + i - 1), LS + Ft, -Re / 4 * (-4 + i), vbBlue, 2)
      Next i
    Case Is < 0
      For i = 1 To 8
      Call linea(LS, Re / 4 * (-4 + i - 1), LS + Ft, Re / 4 * (-4 + i), vbBlue, 2)
      Next i
    Case Else
      For i = 1 To 8
      Call linea(LS, Re / 4 * (-4 + i - 1), LS + Ft, Re / 4 * (-4 + i - 1), vbBlue, 2)
      Next i
  End Select
  'Spina dx
  Call linea(LS + Ft, -RS, 2 * LS + Ft, -RS)
  Call linea(LS + Ft, RS, 2 * LS + Ft, RS)
  Call linea(2 * LS + Ft, -RS, 2 * LS + Ft, RS)
  'Contropunta
  Call linea(2 * LS + Ft, RS / 2, 3 * LS + Ft, RS, RGB(100, 100, 100))
  Call linea(2 * LS + Ft, -RS / 2, 3 * LS + Ft, -RS, RGB(100, 100, 100))
  Call linea(3 * LS + Ft, RS, 8 * LS + Ft, RS, RGB(100, 100, 100))
  Call linea(3 * LS + Ft, -RS, 8 * LS + Ft, -RS, RGB(100, 100, 100))
  YQuota = Re + 350 / tScala
  '
  Call QuotaH(LS + Ft, YQuota, LS + Ft + ExtracorsaOut, YQuota, "" & ExtracorsaOut, 12, , 3)
  Call QuotaH(LS - ExtracorsaIn, YQuota, LS, YQuota, "" & ExtracorsaIn, 12, , 3)
  '
  If Not NotScale Then
    Call QuotaH(LS, YQuota, LS + Ft, YQuota, "" & Ft, 12, , 3)
  Else
    Call QuotaH(LS, YQuota, LS + Ft, YQuota, "?", 12, vbRed, 3)
  End If
  '
  PAR.MargineDxPic = 0
  PAR.MargineSxPic = 0
  If NotScale Then Call TestoInfo("DRAWING NOT IN SCALE ! ", , True, vbRed, True)
  '
  Call TestoInfo("Units: [mm]", 3)
  '
End Sub

Public Sub Disegna_ZONA_LAVORO_PROFILO_RAB_ESTERNI(Stampante As Boolean)
'
Dim nZone           As Integer
Dim DE              As Double
Dim Ft              As Double
Dim F1              As Double
Dim F2              As Double
Dim F3              As Double
Dim F1_2            As Double
Dim F2_2            As Double
Dim F3_2            As Double
Dim Re              As Double
Dim RS              As Double
Dim LS              As Double
Dim DiaMola         As Double
Dim AngMola         As Double
Dim LatoPart        As Integer
Dim ExtracorsaIn    As Double
Dim ExtracorsaOut   As Double
Dim TrattoDivisione As Double
Dim tColore         As Long
Dim tX1             As Double
Dim tX              As Double
Dim tY              As Double
Dim i               As Integer
Dim NotScale        As Boolean
Dim QuotaIL         As Double
Dim QuotaFL         As Double
Dim XF0             As Double
Dim XF1             As Double
Dim XF2             As Double
Dim XF3             As Double
Dim XF0_2           As Double
Dim XF1_2           As Double
Dim XF2_2           As Double
Dim XF3_2           As Double
Dim tScala          As Double
Dim YQuota          As Double
Dim Beta            As Double
Dim QZI             As Double
'
  Call InizializzaVariabili
  '
  NotScale = False
  'Lettura parametri ingranaggio
  nZone = Lp("iNzone")
  DE = Lp("RA[0,1]"): DE = DE * 2
  DiaMola = Lp("iDiaMola")
  AngMola = Lp("R[41]")
  LatoPart = Lp("R[12]")
  QZI = Lp("QASSIALESTART_G")
  ExtracorsaIn = Lp("CORSAIN")
  ExtracorsaOut = Lp("CORSAOUT")
  TrattoDivisione = Lp("R[130]")
  Beta = Lp("R[41]")
  '
  F1 = 0:    F2 = 0:    F3 = 0
  F1_2 = 0:  F2_2 = 0:  F3_2 = 0
  Select Case nZone
    Case 1
      F1 = Lp("Fascia1F1"):   F1_2 = Lp("Fascia1F2")
    Case 2
      F1 = Lp("Fascia1F1"):   F2 = Lp("Fascia2F1")
      F1_2 = Lp("Fascia1F2"): F2_2 = Lp("Fascia2F2")
    Case 3
      F1 = Lp("Fascia1F1"):   F2 = Lp("Fascia2F1"):   F3 = Lp("Fascia3F1")
      F1_2 = Lp("Fascia1F2"): F2_2 = Lp("Fascia2F2"): F3_2 = Lp("Fascia3F2")
  End Select
  Ft = F1 + F2 + F3 'Fascia Totale
  If Ft = 0 Then
    NotScale = True
    F1 = 50
    F1_2 = 50
    Ft = 50
  End If
  Re = DE / 2
  RS = Re / 4 'Raggio spina
  LS = RS 'Lunghezza spina sporgente
  '
  PAR.MaxX = 6 * LS + Ft
  PAR.MinX = -2 * LS
  PAR.MaxY = Re '+ 35
  PAR.MinY = 0 ' -0.5 * LS '-Re
  PAR.ScalaMargine = 1
  PAR.Allineamento = 0
  
  Call CalcolaParPic(PAR, True, Stampante)
  
  PAR.MargineDxPic = PAR.Pic.TextWidth("Z=888.888")
  PAR.MargineSxPic = PAR.Pic.TextWidth("Z=888.888")
  '
  Dim XMola As Double
  Dim Ymola As Double
  
  Select Case LatoPart
    
    Case -1 'Destra
      PAR.MaxX = (2 * LS + Ft + ExtracorsaOut + TrattoDivisione)
      PAR.MinX = -ExtracorsaIn
      PAR.MaxY = 2 * Re
      PAR.MinY = 0
      PAR.ScalaMargine = 1
      PAR.Allineamento = 0
      Call CalcolaParPic(PAR, True, Stampante)
      XMola = LS + Ft + ExtracorsaOut
      Ymola = 0
      QuotaIL = QZI + Ft + ExtracorsaOut
      QuotaFL = QZI - ExtracorsaIn
      tScala = PAR.Scala
      If PAR.Pic Is Printer Then tScala = tScala * 45
      YQuota = Re + 350 / tScala
      If (Lp("iMetDiv") = 2) Then
        XMola = XMola + TrattoDivisione
        QuotaH XMola - TrattoDivisione, YQuota, XMola, YQuota, "" & TrattoDivisione, 12, , 3
        QuotaIL = QuotaIL + TrattoDivisione
      End If
      Call TestoAll(XMola + 200 / tScala, YQuota, "Z=" & QuotaIL, , True, 2)
      Call TestoAll(XMola - TrattoDivisione - ExtracorsaIn - ExtracorsaOut - Ft - 200 / tScala, YQuota, "Z=" & QuotaFL, , True, 1)
      '
    Case 1 'Sinistra
      PAR.MaxX = (2 * LS + Ft + ExtracorsaOut)
      PAR.MinX = -ExtracorsaIn - TrattoDivisione
      PAR.MaxY = 2 * Re
      PAR.MinY = 0
      PAR.ScalaMargine = 1
      PAR.Allineamento = 0
      Call CalcolaParPic(PAR, True, Stampante)
      XMola = LS - ExtracorsaIn
      Ymola = 0
      QuotaIL = QZI - ExtracorsaIn
      QuotaFL = QZI + Ft + ExtracorsaOut
      tScala = PAR.Scala
      If PAR.Pic Is Printer Then tScala = tScala * 45
      YQuota = Re + 350 / tScala
      If (Lp("iMetDiv") = 2) Then
        XMola = XMola - TrattoDivisione
        QuotaH XMola, YQuota, XMola + TrattoDivisione, YQuota, "" & TrattoDivisione, 12, , 3
        QuotaIL = QuotaIL - TrattoDivisione
      End If
      Call TestoAll(XMola - 200 / tScala, YQuota, "Z=" & QuotaIL, , True, 1)
      Call TestoAll(XMola + TrattoDivisione + ExtracorsaIn + ExtracorsaOut + Ft + 200 / tScala, YQuota, "Z=" & QuotaFL, , True, 2)
      '
  End Select
  '
  Call linea(XMola, Re + 20 + DiaMola, XMola, Re, RGB(255, 200, 200), 1, vbDashDot)
  Call Disegna_Mola_PROFILO_RAB(XMola, Re + 5 + PAR.Pic.TextHeight("0") / PAR.Scala + DiaMola / 2, vbRed, 2)
  '
  If LatoPart = -1 Then
    tX1 = ExtracorsaIn / 2
    tX1 = XMola - tX1
  Else
    tX1 = ExtracorsaOut / 2
    tX1 = XMola + tX1
  End If
  '
  'Zone
  XF0 = LS
  XF1 = XF0 + F1
  XF2 = XF1 + F2
  XF3 = XF2 + F3
  XF0_2 = LS
  XF1_2 = XF0_2 + F1_2
  XF2_2 = XF1_2 + F2_2
  XF3_2 = XF2_2 + F3_2
  'Spina sx
  Call linea(0, -RS, 0, RS)
  Call linea(0, -RS, LS, -RS)
  Call linea(0, RS, LS, RS)
  'Ingranaggio
  Call linea(LS, -Re, LS, Re, vbBlue, 2)
  Call linea(LS, -Re, LS + Ft, -Re, vbBlue, 2)
  Call linea(LS, Re, LS + Ft, Re, vbBlue, 2)
  Call linea(LS + Ft, -Re, LS + Ft, Re, vbBlue, 2)
  'VERSO INGRANAGGIO
  Select Case Beta
    Case Is > 0
      For i = 1 To 8
      Call linea(LS, -Re / 4 * (-4 + i - 1), LS + Ft, -Re / 4 * (-4 + i), vbBlue, 2)
      Next i
    Case Is < 0
      For i = 1 To 8
      Call linea(LS, Re / 4 * (-4 + i - 1), LS + Ft, Re / 4 * (-4 + i), vbBlue, 2)
      Next i
    Case Else
      For i = 1 To 8
      Call linea(LS, Re / 4 * (-4 + i - 1), LS + Ft, Re / 4 * (-4 + i - 1), vbBlue, 2)
      Next i
  End Select
  'Spina dx
  Call linea(LS + Ft, -RS, 2 * LS + Ft, -RS)
  Call linea(LS + Ft, RS, 2 * LS + Ft, RS)
  Call linea(2 * LS + Ft, -RS, 2 * LS + Ft, RS)
  'Contropunta
  Call linea(2 * LS + Ft, RS / 2, 3 * LS + Ft, RS, RGB(100, 100, 100))
  Call linea(2 * LS + Ft, -RS / 2, 3 * LS + Ft, -RS, RGB(100, 100, 100))
  Call linea(3 * LS + Ft, RS, 8 * LS + Ft, RS, RGB(100, 100, 100))
  Call linea(3 * LS + Ft, -RS, 8 * LS + Ft, -RS, RGB(100, 100, 100))
  YQuota = Re + 350 / tScala
  '
  Call QuotaH(LS + Ft, YQuota, LS + Ft + ExtracorsaOut, YQuota, "" & ExtracorsaOut, 12, , 3)
  Call QuotaH(LS - ExtracorsaIn, YQuota, LS, YQuota, "" & ExtracorsaIn, 12, , 3)
  '
  If Not NotScale Then
    Call QuotaH(LS, YQuota, LS + Ft, YQuota, "" & Ft, 12, , 3)
  Else
    Call QuotaH(LS, YQuota, LS + Ft, YQuota, "?", 12, vbRed, 3)
  End If
  '
  PAR.MargineDxPic = 0
  PAR.MargineSxPic = 0
  If NotScale Then Call TestoInfo("DRAWING NOT IN SCALE ! ", , True, vbRed, True)
  '
  Call TestoInfo("Units: [mm]", 3)
  '
End Sub

Public Sub Disegna_ZONA_LAVORO_SCANALATI_ESTERNIV(Stampante As Boolean)
'
Dim nZone           As Integer
Dim DE              As Double
Dim DP              As Double
Dim Ft              As Double
Dim F1              As Double
Dim F2              As Double
Dim F3              As Double
Dim F1_2            As Double
Dim F2_2            As Double
Dim F3_2            As Double
Dim Re              As Double
Dim RS              As Double
Dim LS              As Double
Dim DiaMola         As Double
Dim AngMola         As Double
Dim LatoPart        As Integer
Dim ExtracorsaIn    As Double
Dim ExtracorsaOut   As Double
Dim TrattoDivisione As Double
Dim tColore         As Long
Dim tX1             As Double
Dim tX              As Double
Dim tY              As Double
Dim i               As Integer
Dim NotScale        As Boolean
Dim QuotaIL         As Double
Dim QuotaFL         As Double
Dim XF0             As Double
Dim XF1             As Double
Dim XF2             As Double
Dim XF3             As Double
Dim XF0_2           As Double
Dim XF1_2           As Double
Dim XF2_2           As Double
Dim XF3_2           As Double
Dim tScala          As Double
Dim YQuota          As Double
Dim Beta            As Double
Dim QZI             As Double
Dim iTESTA          As Double
'
  Call InizializzaVariabili
  '
  NotScale = False
  'Lettura parametri ingranaggio
  nZone = Lp("iNzone")
  DE = Lp("R[44]")
  DP = (Lp("R[44]") + Lp("R[103]")) / 2
  
  DiaMola = Lp("iDiaMola")
  
  LatoPart = Lp("R[12]")
  '
  iTESTA = Lp("INDTAV_G")
  If (iTESTA = 1) Then
    QZI = Lp("QASSIALESTART_G[1,1]")
  Else
    QZI = Lp("QASSIALESTART_G[0,1]")
  End If
  '
  ExtracorsaIn = Lp("CORSAIN")
  ExtracorsaOut = Lp("CORSAOUT")
  TrattoDivisione = Lp("R[130]")
  '
  F1 = 0:    F2 = 0:    F3 = 0
  F1_2 = 0:  F2_2 = 0:  F3_2 = 0
  Select Case nZone
    Case 1
      F1 = Lp("Fascia1F1"):   F1_2 = Lp("Fascia1F2")
    Case 2
      F1 = Lp("Fascia1F1"):   F2 = Lp("Fascia2F1")
      F1_2 = Lp("Fascia1F2"): F2_2 = Lp("Fascia2F2")
    Case 3
      F1 = Lp("Fascia1F1"):   F2 = Lp("Fascia2F1"):   F3 = Lp("Fascia3F1")
      F1_2 = Lp("Fascia1F2"): F2_2 = Lp("Fascia2F2"): F3_2 = Lp("Fascia3F2")
  End Select
  Ft = F1 + F2 + F3 'Fascia Totale
  If Ft = 0 Then
    NotScale = True
    F1 = 50
    F1_2 = 50
    Ft = 50
  End If
  Re = DE / 2
  RS = Re / 4 'Raggio spina
  LS = RS 'Lunghezza spina sporgente
  '
  PAR.MaxY = 6 * LS + Ft
  PAR.MinY = -2 * LS
  PAR.MaxX = Re '+ 35
  PAR.MinX = 0 ' -0.5 * LS '-Re
  PAR.ScalaMargine = 1
  PAR.Allineamento = 0
  
  Call CalcolaParPic(PAR, True, Stampante)
  
  PAR.MargineDxPic = PAR.Pic.TextWidth("Z=888.888")
  PAR.MargineSxPic = PAR.Pic.TextWidth("Z=888.888")
  '
  Dim XMola As Double
  Dim Ymola As Double
  
  Select Case LatoPart
    
    Case -1 'ALTO
      PAR.MaxY = (2 * LS + Ft + ExtracorsaOut + TrattoDivisione)
      PAR.MinY = -ExtracorsaIn
      PAR.MaxX = 2 * Re
      PAR.MinX = 0
      PAR.ScalaMargine = 1
      PAR.Allineamento = 0
      Call CalcolaParPic(PAR, True, Stampante)
      XMola = LS + Ft + ExtracorsaOut
      Ymola = 0
      QuotaFL = QZI - ExtracorsaIn
      QuotaIL = QZI + Ft + ExtracorsaOut
      tScala = PAR.Scala
      If PAR.Pic Is Printer Then tScala = tScala * 45
      YQuota = Re + 350 / tScala
      If (Lp("iMetDiv") = 2) Then
        XMola = XMola + TrattoDivisione
        Call QuotaV(YQuota, XMola - TrattoDivisione, YQuota, XMola, "" & TrattoDivisione, 12, , 3)
        QuotaIL = QuotaIL + TrattoDivisione
      End If
      Call TestoAll(YQuota, XMola + 200 / tScala, "Z=" & QuotaIL, , True, 2)
      Call TestoAll(YQuota, XMola - TrattoDivisione - ExtracorsaIn - ExtracorsaOut - Ft - 200 / tScala, "Z=" & QuotaFL, , True, 2)
      '
    Case 1 'BASSO
      PAR.MaxY = (2 * LS + Ft + ExtracorsaOut)
      PAR.MinY = -ExtracorsaIn - TrattoDivisione
      PAR.MaxX = 2 * Re
      PAR.MinX = 0
      PAR.ScalaMargine = 1
      PAR.Allineamento = 0
      Call CalcolaParPic(PAR, True, Stampante)
      XMola = LS - ExtracorsaIn
      Ymola = 0
      QuotaIL = QZI - ExtracorsaIn
      QuotaFL = QZI + Ft + ExtracorsaOut
      tScala = PAR.Scala
      If PAR.Pic Is Printer Then tScala = tScala * 45
      YQuota = Re + 350 / tScala
      If (Lp("iMetDiv") = 2) Then
        XMola = XMola - TrattoDivisione
        Call QuotaV(YQuota, XMola, YQuota, XMola + TrattoDivisione, "" & TrattoDivisione, 12, , 3)
        QuotaIL = QuotaIL - TrattoDivisione
      End If
      Call TestoAll(YQuota, XMola - 200 / tScala, "Z=" & QuotaIL, , True, 2)
      Call TestoAll(YQuota, XMola + TrattoDivisione + ExtracorsaIn + ExtracorsaOut + Ft + 200 / tScala, "Z=" & QuotaFL, , True, 2)
      '
  End Select
  '
  
  'Centro mola
  Call linea(Re + 20 + DiaMola, XMola, Re, XMola, RGB(255, 200, 200), 1, vbDashDot)
  Call Cerchio(Re + DiaMola / 4, XMola, DiaMola / 4, vbRed, 2, 2)
  
  'Disegno della mola scanalato
  'Dim SPE     As Double
  'Dim spe1    As Double
  'Dim spe2    As Double
  'Dim Altezza As Double
  '
  'SPE = Lp("SPMOLA_G[0,1]")
  'SPE = SPE / 2
  'Altezza = Lp("R[44]") - Lp("R[103]")
  'Altezza = Altezza / 2
  'spe1 = 1 / 2 * SPE
  'spe2 = 1 / 4 * SPE
  '
  'Call linea(Re + 30 + Altezza + DiaMola, XMola - SPE, Re + 30 + Altezza, XMola - SPE, RGB(0, 0, 0), 1, vbSolid)
  'Call linea(Re + 30 + Altezza, XMola - spe1, Re + 30 + Altezza, XMola - SPE, RGB(0, 0, 0), 1, vbSolid)
  'Call linea(Re + 30 + Altezza, XMola - spe1, Re + 30, XMola - spe2, RGB(0, 0, 0), 1, vbSolid)
  'Call linea(Re + 30 + Altezza + DiaMola, XMola + SPE, Re + 30 + Altezza, XMola + SPE, RGB(0, 0, 0), 1, vbSolid)
  'Call linea(Re + 30 + Altezza, XMola + spe1, Re + 30 + Altezza, XMola + SPE, RGB(0, 0, 0), 1, vbSolid)
  'Call linea(Re + 30 + Altezza, XMola + spe1, Re + 30, XMola + spe2, RGB(0, 0, 0), 1, vbSolid)
  'Call linea(Re + 30, XMola + spe2, Re + 30, XMola - spe2, RGB(0, 0, 0), 1, vbSolid)
  
  If (LatoPart = -1) Then
    tX1 = ExtracorsaIn / 2
    tX1 = XMola - tX1
  Else
    tX1 = ExtracorsaOut / 2
    tX1 = XMola + tX1
  End If
  '
  'Zone
  XF0 = LS:    XF1 = XF0 + F1:        XF2 = XF1 + F2:        XF3 = XF2 + F3
  XF0_2 = LS:  XF1_2 = XF0_2 + F1_2:  XF2_2 = XF1_2 + F2_2:  XF3_2 = XF2_2 + F3_2
  '
  'Spina in basso
  Call linea(-RS, 0, RS, 0)
  Call linea(-RS, 0, -RS, LS)
  Call linea(RS, 0, RS, LS)
  
  'Pezzo
  Call linea(-Re, LS, Re, LS, vbBlue, 2)
  Call linea(-Re, LS, -Re, LS + Ft, vbBlue, 2)
  Call linea(Re, LS, Re, LS + Ft, vbBlue, 2)
  Call linea(-Re, LS + Ft, Re, LS + Ft, vbBlue, 2)
    
  'Spina in alto
  Call linea(-RS, LS + Ft, -RS, 2 * LS + Ft)
  Call linea(RS, LS + Ft, RS, 2 * LS + Ft)
  Call linea(-RS, 2 * LS + Ft, RS, 2 * LS + Ft)
  'Contropunta
  Call linea(RS / 2, 2 * LS + Ft, RS, 3 * LS + Ft, RGB(100, 100, 100))
  Call linea(-RS / 2, 2 * LS + Ft, -RS, 3 * LS + Ft, RGB(100, 100, 100))
  Call linea(RS, 3 * LS + Ft, RS, 8 * LS + Ft, RGB(100, 100, 100))
  Call linea(-RS, 3 * LS + Ft, -RS, 8 * LS + Ft, RGB(100, 100, 100))
  YQuota = Re + 350 / tScala
  '
  
  Call QuotaV(YQuota, LS + Ft, YQuota, LS + Ft + ExtracorsaOut, "" & ExtracorsaOut, 12, , 6)
  Call QuotaV(YQuota, LS - ExtracorsaIn, YQuota, LS, "" & ExtracorsaIn, 12, , 6)
  
  '
  If Not NotScale Then
    Call QuotaV(YQuota, LS, YQuota, LS + Ft, "" & Ft, 12, , 6)
  Else
    Call QuotaV(YQuota, LS, YQuota, LS + Ft, "?", 12, vbRed, 5)
  End If
  '
  PAR.MargineDxPic = 0
  PAR.MargineSxPic = 0
  If NotScale Then Call TestoInfo("DRAWING NOT IN SCALE ! ", , True, vbRed, True)
  '
  If (iTESTA = 1) Then
    Call TestoInfo("GRINDING on C2", 2, True, vbRed)
  Else
    Call TestoInfo("GRINDING on C1", 2, True, vbRed)
  End If
  Call TestoInfo("Units: [mm]", 3)

End Sub

Public Sub Disegna_ZONA_LAVORO_INGRANAGGI_ESTERNIV(Stampante As Boolean)
'
Dim nZone           As Integer
Dim DE              As Double
Dim Ft              As Double
Dim F1              As Double
Dim F2              As Double
Dim F3              As Double
Dim F1_2            As Double
Dim F2_2            As Double
Dim F3_2            As Double
Dim Re              As Double
Dim RS              As Double
Dim LS              As Double
Dim DiaMola         As Double
Dim AngMola         As Double
Dim LatoPart        As Integer
Dim ExtracorsaIn    As Double
Dim ExtracorsaOut   As Double
Dim TrattoDivisione As Double
Dim tColore         As Long
Dim tX1             As Double
Dim tX              As Double
Dim tY              As Double
Dim i               As Integer
Dim NotScale        As Boolean
Dim QuotaIL         As Double
Dim QuotaFL         As Double
Dim XF0             As Double
Dim XF1             As Double
Dim XF2             As Double
Dim XF3             As Double
Dim XF0_2           As Double
Dim XF1_2           As Double
Dim XF2_2           As Double
Dim XF3_2           As Double
Dim tScala          As Double
Dim YQuota          As Double
Dim Beta            As Double
Dim QZI             As Double
Dim iTESTA          As Double
'
  Call InizializzaVariabili
  '
  NotScale = False
  'Lettura parametri ingranaggio
  nZone = Lp("iNzone")
  DE = Lp("DEXT_G")
  DiaMola = Lp("iDiaMola")
  AngMola = Lp("R[258]")
  LatoPart = Lp("R[12]")
  '
  iTESTA = Lp("INDTAV_G")
  If (iTESTA = 1) Then
    QZI = Lp("QASSIALESTART_G[1,1]")
  Else
    QZI = Lp("QASSIALESTART_G[0,1]")
  End If
  '
  ExtracorsaIn = Lp("CORSAIN")
  ExtracorsaOut = Lp("CORSAOUT")
  TrattoDivisione = Lp("R[130]")
  Beta = Lp("R[41]")
  '
  F1 = 0:    F2 = 0:    F3 = 0
  F1_2 = 0:  F2_2 = 0:  F3_2 = 0
  Select Case nZone
    Case 1
      F1 = Lp("Fascia1F1"):   F1_2 = Lp("Fascia1F2")
    Case 2
      F1 = Lp("Fascia1F1"):   F2 = Lp("Fascia2F1")
      F1_2 = Lp("Fascia1F2"): F2_2 = Lp("Fascia2F2")
    Case 3
      F1 = Lp("Fascia1F1"):   F2 = Lp("Fascia2F1"):   F3 = Lp("Fascia3F1")
      F1_2 = Lp("Fascia1F2"): F2_2 = Lp("Fascia2F2"): F3_2 = Lp("Fascia3F2")
  End Select
  Ft = F1 + F2 + F3 'Fascia Totale
  If Ft = 0 Then
    NotScale = True
    F1 = 50
    F1_2 = 50
    Ft = 50
  End If
  Re = DE / 2
  RS = Re / 4 'Raggio spina
  LS = RS 'Lunghezza spina sporgente
  '
  PAR.MaxY = 6 * LS + Ft
  PAR.MinY = -2 * LS
  PAR.MaxX = Re '+ 35
  PAR.MinX = 0 ' -0.5 * LS '-Re
  PAR.ScalaMargine = 1
  PAR.Allineamento = 0
  
  Call CalcolaParPic(PAR, True, Stampante)
  
  PAR.MargineDxPic = PAR.Pic.TextWidth("Z=888.888")
  PAR.MargineSxPic = PAR.Pic.TextWidth("Z=888.888")
  '
  Dim XMola As Double
  Dim Ymola As Double
  
  Select Case LatoPart
    
    Case -1 'ALTO
      PAR.MaxY = (2 * LS + Ft + ExtracorsaOut + TrattoDivisione)
      PAR.MinY = -ExtracorsaIn
      PAR.MaxX = 2 * Re
      PAR.MinX = 0
      PAR.ScalaMargine = 1
      PAR.Allineamento = 0
      Call CalcolaParPic(PAR, True, Stampante)
      XMola = LS + Ft + ExtracorsaOut
      Ymola = 0
      QuotaFL = QZI - ExtracorsaIn
      QuotaIL = QZI + Ft + ExtracorsaOut
      tScala = PAR.Scala
      If PAR.Pic Is Printer Then tScala = tScala * 45
      YQuota = Re + 350 / tScala
      If (Lp("iMetDiv") = 2) Then
        XMola = XMola + TrattoDivisione
        Call QuotaV(YQuota, XMola - TrattoDivisione, YQuota, XMola, "" & TrattoDivisione, 12, , 3)
        QuotaIL = QuotaIL + TrattoDivisione
      End If
      Call TestoAll(YQuota, XMola + 200 / tScala, "Z=" & QuotaIL, , True, 2)
      Call TestoAll(YQuota, XMola - TrattoDivisione - ExtracorsaIn - ExtracorsaOut - Ft - 200 / tScala, "Z=" & QuotaFL, , True, 1)
      '
    Case 1 'BASSO
      PAR.MaxY = (2 * LS + Ft + ExtracorsaOut)
      PAR.MinY = -ExtracorsaIn - TrattoDivisione
      PAR.MaxX = 2 * Re
      PAR.MinX = 0
      PAR.ScalaMargine = 1
      PAR.Allineamento = 0
      Call CalcolaParPic(PAR, True, Stampante)
      XMola = LS - ExtracorsaIn
      Ymola = 0
      QuotaIL = QZI - ExtracorsaIn
      QuotaFL = QZI + Ft + ExtracorsaOut
      tScala = PAR.Scala
      If PAR.Pic Is Printer Then tScala = tScala * 45
      YQuota = Re + 350 / tScala
      If (Lp("iMetDiv") = 2) Then
        XMola = XMola - TrattoDivisione
        Call QuotaV(YQuota, XMola, YQuota, XMola + TrattoDivisione, "" & TrattoDivisione, 12, , 3)
        QuotaIL = QuotaIL - TrattoDivisione
      End If
      Call TestoAll(YQuota, XMola - 200 / tScala, "Z=" & QuotaIL, , True, 1)
      Call TestoAll(YQuota, XMola + TrattoDivisione + ExtracorsaIn + ExtracorsaOut + Ft + 200 / tScala, "Z=" & QuotaFL, , True, 2)
      '
  End Select
  '
  YQuota = Re + 700 / tScala
  '
  Call Disegna_Mola_INGRANAGGI_ESTERNIV(XMola, YQuota + DiaMola / 2, vbRed, 2)
  Call linea(YQuota + DiaMola, XMola, Re, XMola, vbRed, 1, vbDash)
  '
  If (LatoPart = -1) Then
    tX1 = ExtracorsaIn / 2
    tX1 = XMola - tX1
  Else
    tX1 = ExtracorsaOut / 2
    tX1 = XMola + tX1
  End If
  '
  'Zone
  XF0 = LS:    XF1 = XF0 + F1:        XF2 = XF1 + F2:        XF3 = XF2 + F3
  XF0_2 = LS:  XF1_2 = XF0_2 + F1_2:  XF2_2 = XF1_2 + F2_2:  XF3_2 = XF2_2 + F3_2
  '
  'Spina sx
  Call linea(-RS, 0, RS, 0)
  Call linea(-RS, 0, -RS, LS)
  Call linea(RS, 0, RS, LS)
  'Ingranaggio
  Call linea(-Re, LS, Re, LS, vbBlue, 2)
  Call linea(-Re, LS, -Re, LS + Ft, vbBlue, 2)
  Call linea(Re, LS, Re, LS + Ft, vbBlue, 2)
  Call linea(-Re, LS + Ft, Re, LS + Ft, vbBlue, 2)
  'VERSO INGRANAGGIO
  Select Case Beta
    Case Is < 0
      For i = 1 To 8
      Call linea(-Re / 4 * (-4 + i - 1), LS, -Re / 4 * (-4 + i), LS + Ft, vbBlue, 2)
      Next i
    Case Is > 0
      For i = 1 To 8
      Call linea(Re / 4 * (-4 + i - 1), LS, Re / 4 * (-4 + i), LS + Ft, vbBlue, 2)
      Next i
    Case Else
      For i = 1 To 8
      Call linea(Re / 4 * (-4 + i - 1), LS, Re / 4 * (-4 + i - 1), LS + Ft, vbBlue, 2)
      Next i
  End Select
  'Spina dx
  Call linea(-RS, LS + Ft, -RS, 2 * LS + Ft)
  Call linea(RS, LS + Ft, RS, 2 * LS + Ft)
  Call linea(-RS, 2 * LS + Ft, RS, 2 * LS + Ft)
  'Contropunta
  Call linea(RS / 2, 2 * LS + Ft, RS, 3 * LS + Ft, RGB(100, 100, 100))
  Call linea(-RS / 2, 2 * LS + Ft, -RS, 3 * LS + Ft, RGB(100, 100, 100))
  Call linea(RS, 3 * LS + Ft, RS, 8 * LS + Ft, RGB(100, 100, 100))
  Call linea(-RS, 3 * LS + Ft, -RS, 8 * LS + Ft, RGB(100, 100, 100))
  YQuota = Re + 350 / tScala
  '
  Call QuotaV(YQuota, LS + Ft, YQuota, LS + Ft + ExtracorsaOut, "" & ExtracorsaOut, 12, , 3)
  Call QuotaV(YQuota, LS - ExtracorsaIn, YQuota, LS, "" & ExtracorsaIn, 12, , 3)
  '
  If Not NotScale Then
    Call QuotaV(YQuota, LS, YQuota, LS + Ft, "" & Ft, 12, , 3)
  Else
    Call QuotaV(YQuota, LS, YQuota, LS + Ft, "?", 12, vbRed, 3)
  End If
  '
  PAR.MargineDxPic = 0
  PAR.MargineSxPic = 0
  If NotScale Then Call TestoInfo("DRAWING NOT IN SCALE ! ", , True, vbRed, True)
  '
  If (iTESTA = 1) Then
    Call TestoInfo("GRINDING on C2", 2, True, vbRed)
  Else
    Call TestoInfo("GRINDING on C1", 2, True, vbRed)
  End If
  Call TestoInfo("Units: [mm]", 3)
  '
End Sub

Public Sub Disegna_ZONA_LAVORO_ROTORI_ESTERNI(Stampante As Boolean)
'
Dim nZone           As Integer
Dim DE              As Double
Dim Ft              As Double
Dim F1              As Double
Dim F2              As Double
Dim F3              As Double
Dim Re              As Double
Dim RS              As Double
Dim LS              As Double
Dim DiaMola         As Double
Dim AngMola         As Double
Dim LatoPart        As Integer
Dim ExtracorsaIn    As Double
Dim ExtracorsaOut   As Double
Dim TrattoDivisione As Double
Dim tColore         As Long
Dim tX1             As Double
Dim tX              As Double
Dim tY              As Double
Dim i               As Integer
Dim NotScale        As Boolean
Dim QuotaIL         As Double
Dim QuotaFL         As Double
Dim XF0             As Double
Dim XF1             As Double
Dim XF2             As Double
Dim XF3             As Double
Dim QZI             As Double
Dim XMola           As Double
Dim Ymola           As Double
Dim Beta            As Double
Dim PASSO           As Double
Dim SiRICERCA       As String
Dim TOLQUOTAZ       As Double
Dim CORQUOTAZ       As Double
Dim PRINCIPI        As Integer
Dim Con1            As Single
Dim Con2            As Single
Dim Con3            As Single
'
Dim OD_Fascia(5)    As Single
Dim OD_Salto(5)     As Single
Dim OD_Tipo         As Integer
Dim DI              As Single
'
On Error Resume Next
  '
  Call InizializzaVariabili
  '
  NotScale = False
  '
  nZone = Lp("iNzone")
  DE = Lp("R[44]")
  DI = Lp("R[45]")
  DiaMola = Lp("iDiaMola")
  AngMola = Lp("R[258]")
  LatoPart = Lp("R[12]")
  QZI = Lp("QASSIALESTART_G")
  ExtracorsaIn = Lp("CORSAIN")
  ExtracorsaOut = Lp("CORSAOUT")
  TrattoDivisione = Lp("R[130]")
  Beta = Lp("R[258]")
  PASSO = Lp("R[390]")
  PRINCIPI = Lp("R[5]")
  '
  SiRICERCA = GetInfo("Configurazione", "SiRicerca", Path_LAVORAZIONE_INI)
  If (SiRICERCA = "Y") Then
    TOLQUOTAZ = Lp("TOLQUOTAZ")
    CORQUOTAZ = Lp("CORQUOTAZ")
  Else
    TOLQUOTAZ = 0
    CORQUOTAZ = 0
  End If
  '
  F1 = 0: F2 = 0: F3 = 0
  Select Case nZone
    Case 1
      F1 = Lp("Fascia1F1")
      Con1 = Lp("Conic1F1")
      Con2 = 0
      Con3 = 0
    Case 2
      F1 = Lp("Fascia1F1")
      F2 = Lp("Fascia2F1")
      Con1 = Lp("Conic1F1")
      Con2 = Lp("Conic2F1")
      Con3 = 0
    Case 3
      F1 = Lp("Fascia1F1")
      F2 = Lp("Fascia2F1")
      F3 = Lp("Fascia3F1")
      Con1 = Lp("Conic1F1")
      Con2 = Lp("Conic2F1")
      Con3 = Lp("Conic3F1")
  End Select
  '
Dim TIPO_MACCHINA As String
  '
  TIPO_MACCHINA = GetInfo("Configurazione", "TIPO_MACCHINA", Path_LAVORAZIONE_INI)
  TIPO_MACCHINA = UCase$(Trim$(TIPO_MACCHINA))
  If (TIPO_MACCHINA = "G500HL-SEIM") Then
    OD_Tipo = Lp("CYC[0,120]")
    OD_Fascia(1) = Lp("Fascia1"): OD_Salto(1) = Lp("Salto1")
    OD_Fascia(2) = Lp("Fascia2"): OD_Salto(2) = Lp("Salto2")
    OD_Fascia(3) = Lp("Fascia3"): OD_Salto(3) = Lp("Salto3")
    OD_Fascia(4) = Lp("Fascia4"): OD_Salto(4) = Lp("Salto4")
    OD_Fascia(5) = Lp("Fascia5"): OD_Salto(5) = Lp("Salto5")
  End If
  '
  Ft = F1 + F2 + F3 'Fascia Totale
  Re = DE / 2
  RS = Re / 4       'Raggio spina
  LS = RS           'Lunghezza spina sporgente
  '
  If (Ft = 0) Then
    NotScale = True
    F1 = 50
    Ft = 50
  End If
  '
  PAR.MaxX = 6 * LS + Ft
  PAR.MinX = -2 * LS
  PAR.MaxY = Re + 35
  PAR.MinY = 0
  PAR.Allineamento = 0
  Call CalcolaParPic(PAR, True, Stampante)
  '
  PAR.MargineDxPic = PAR.Pic.TextWidth("Z=888.888")
  PAR.MargineSxPic = PAR.Pic.TextWidth("Z=888.888")
  '
  Select Case LatoPart
    
    Case -1 'Destra
      PAR.MaxX = (2 * LS + Ft + ExtracorsaOut + TrattoDivisione)
      PAR.MinX = -ExtracorsaIn
      PAR.MaxY = 2 * Re
      PAR.MinY = 0
      PAR.ScalaMargine = 1
      PAR.Allineamento = 0
      Call CalcolaParPic(PAR, True, Stampante)
      XMola = LS + Ft + ExtracorsaOut
      Ymola = 0
      QuotaIL = QZI + Ft + ExtracorsaOut
      QuotaFL = QZI - ExtracorsaIn
      If (Lp("iMetDiv") = 2) Then
        XMola = XMola + TrattoDivisione
        Call QuotaH(XMola - TrattoDivisione, Re + 10, XMola, Re + 10, "" & TrattoDivisione, 12, , 3)
        QuotaIL = QuotaIL + TrattoDivisione
      End If
      Call TestoAll(XMola + 5, Re + 10, "Z=" & QuotaIL, , True, 2)
      Call TestoAll(XMola - TrattoDivisione - ExtracorsaIn - ExtracorsaOut - Ft - 5, Re + 10, "Z=" & QuotaFL, , True, 1)
        
    Case 1 'Sinistra
      PAR.MaxX = (2 * LS + Ft + ExtracorsaOut)
      PAR.MinX = -ExtracorsaIn - TrattoDivisione
      PAR.MaxY = 2 * Re
      PAR.MinY = 0
      PAR.ScalaMargine = 1
      PAR.Allineamento = 0
      Call CalcolaParPic(PAR, True, Stampante)
      XMola = LS - ExtracorsaIn
      Ymola = 0
      QuotaIL = QZI - ExtracorsaIn
      QuotaFL = QZI + Ft + ExtracorsaOut
      If (Lp("iMetDiv") = 2) Then
        XMola = XMola - TrattoDivisione
        Call QuotaH(XMola, Re + 10, XMola + TrattoDivisione, Re + 10, "" & TrattoDivisione, 12, , 3)
        QuotaIL = QuotaIL - TrattoDivisione
      End If
      Call TestoAll(XMola - 5, Re + 10, "Z=" & QuotaIL, , True, 1)
      Call TestoAll(XMola + TrattoDivisione + ExtracorsaIn + ExtracorsaOut + Ft + 5, Re + 10, "Z=" & QuotaFL, , True, 2)
      
  End Select
  '
  Call linea(XMola, Re + 20 + DiaMola, XMola, Re, RGB(255, 200, 200), 1, vbDashDot)
  Call Disegna_Mola_ROTORI(XMola, Re + 40 + PAR.Pic.TextHeight("0") / PAR.Scala + DiaMola / 2, vbRed, 2)
  '
  If (TOLQUOTAZ > 0) Then
    Call TestoAll(LS - TOLQUOTAZ, Re / 2, "QZI=" & QZI + CORQUOTAZ, , True, 1)
    Call linea(LS + CORQUOTAZ, Re / 2 - 1 * RS / 2, LS + CORQUOTAZ, Re / 2 + 1 * RS / 2, 12, 1)
    Call QuotaH(LS - TOLQUOTAZ, Re / 2, LS + TOLQUOTAZ, Re / 2, 2 * TOLQUOTAZ, , , 2)
  Else
    Call TestoAll(LS, Re / 2, "QZI=" & QZI, , True, 1)
    Call linea(LS, Re / 2 - 1 * RS / 2, LS, Re / 2 + 1 * RS / 2, 12, 1)
    Call linea(LS, Re, LS, Re + 20, RGB(0, 0, 0), 1)
  End If
  '
  If (LatoPart = -1) Then
    tX1 = ExtracorsaIn / 2
    tX1 = XMola - tX1
  Else
    tX1 = ExtracorsaOut / 2
    tX1 = XMola + tX1
  End If
  'Zone
  XF0 = LS
  XF1 = XF0 + F1
  XF2 = XF1 + F2
  XF3 = XF2 + F3
  'Spina sx
  Call linea(0, -RS, 0, RS)
  Call linea(0, -RS, LS, -RS)
  Call linea(0, RS, LS, RS)
  '
  'conicita' sul diametro interno
  Dim DeltaY  As Single
  Dim Scala   As Single
  Dim pX1     As Single
  Dim pX2     As Single
  Dim pY1     As Single
  Dim pY2     As Single
  Dim SpL     As Integer
  '
  pY1 = Con1 + Con2 + Con3
  If (pY1 <> 0) Then
    Scala = Abs(20 / pY1)
  Else
    pY1 = 0: pY2 = 0
    If (Con1 > 0) Then
      pY1 = pY1 + Con1
    Else
      pY2 = pY2 + Con1
    End If
    If (Con2 > 0) Then
      pY1 = pY1 + Con2
    Else
      pY2 = pY2 + Con2
    End If
    If (Con3 > 0) Then
      pY1 = pY1 + Con3
    Else
      pY2 = pY2 + Con3
    End If
    If (pY1 = 0) And (pY2 = 0) Then
      Scala = 0
    End If
    If (pY1 = 0) And (pY2 <> 0) Then
      Scala = Abs(20 / pY2)
    End If
    If (pY1 <> 0) And (pY2 = 0) Then
      Scala = Abs(20 / pY1)
    End If
    If (pY1 <> 0) And (pY2 <> 0) Then
      If (Abs(pY1) > Abs(pY2)) Then
        Scala = Abs(20 / pY1)
      Else
        Scala = Abs(20 / pY2)
      End If
    End If
  End If
  '
  DeltaY = PAR.Pic.TextHeight("0") / PAR.Scala + 40
  If (Con1 <> 0) Or (Con2 <> 0) Or (Con3 <> 0) Then Call linea(LS, Re + DeltaY, LS + Ft, Re + DeltaY, vbBlack, 1, vbDashDot)
  SpL = 1
  If (LatoPart = -1) Then
    'destra
    If (Con1 <> 0) Or (Con2 <> 0) Or (Con3 <> 0) Then Call TestoAll(LS, Re + DeltaY, "RD", , True, 1)
    pX1 = LS + F1 + F2:       pY1 = Re + DeltaY - Con3 * Scala
    pX2 = LS + F1 + F2 + F3:  pY2 = Re + DeltaY
    Call linea(pX1, pY1, pX2, pY2, vbRed, SpL)
    If (Con3 <> 0) Then Call QuotaV(pX1, pY1, pX2, pY2, "" & Con3, 12, , 1)
    pX1 = LS + F1:      pY1 = Re + DeltaY - (Con2 + Con3) * Scala
    pX2 = LS + F1 + F2: pY2 = Re + DeltaY - Con3 * Scala
    Call linea(pX1, pY1, pX2, pY2, vbRed, SpL)
    If (Con2 <> 0) Then Call QuotaV(pX1, pY2, pX2, pY2, "" & Con2, 12, , 1)
    pX1 = LS:       pY1 = Re + DeltaY - (Con1 + Con2 + Con3) * Scala
    pX2 = LS + F1:  pY2 = Re + DeltaY - (Con2 + Con3) * Scala
    Call linea(pX1, pY1, pX2, pY2, vbRed, SpL)
    If (Con1 <> 0) Then Call QuotaV(pX1, pY1, pX2, pY2, "" & Con1, 12, , 1)
  Else
    'sinistra
    If (Con1 <> 0) Or (Con2 <> 0) Or (Con3 <> 0) Then Call TestoAll(LS + Ft, Re + DeltaY, "RD", , True, 2)
    DeltaY = PAR.Pic.TextHeight("0") / PAR.Scala + 40 + (Con1 + Con2 + Con3) * Scala
    pX1 = LS + F1 + F2:       pY1 = Re + DeltaY - Con3 * Scala
    pX2 = LS + F1 + F2 + F3:  pY2 = Re + DeltaY
    Call linea(pX1, pY1, pX2, pY2, vbRed, SpL)
    If (Con3 <> 0) Then Call QuotaV(pX1, pY1, pX2, pY2, "" & Con3, 12, , 1)
    pX1 = LS + F1:      pY1 = Re + DeltaY - (Con2 + Con3) * Scala
    pX2 = LS + F1 + F2: pY2 = Re + DeltaY - Con3 * Scala
    Call linea(pX1, pY1, pX2, pY2, vbRed, SpL)
    If (Con2 <> 0) Then Call QuotaV(pX1, pY1, pX2, pY2, "" & Con2, 12, , 1)
    pX1 = LS:       pY1 = Re + DeltaY - (Con1 + Con2 + Con3) * Scala
    pX2 = LS + F1:  pY2 = Re + DeltaY - (Con2 + Con3) * Scala
    Call linea(pX1, pY1, pX2, pY2, vbRed, SpL)
    If (Con1 <> 0) Then Call QuotaV(pX1, pY1, pX2, pY2, "" & Con1, 12, , 1)
  End If
  '
  'conicita' sul diametro esterno
  If (OD_Tipo > 0) And (ActualParGroup = "OEM0_CICLIB71") Then
    Dim OD_LS As Double
    pY1 = (OD_Salto(5) + OD_Salto(4) + OD_Salto(3) + OD_Salto(2) + OD_Salto(1))
    If (pY1 <> 0) Then
      Scala = Abs(((DE - DI) / 2) / pY1)
    Else
      For i = 1 To 5
        pY1 = 0: pY2 = 0
        If (OD_Salto(i) > 0) Then
          pY1 = pY1 + OD_Salto(i)
        Else
          pY2 = pY2 + OD_Salto(i)
        End If
      Next i
      If (pY1 = 0) And (pY2 = 0) Then
        Scala = 0
      End If
      If (pY1 = 0) And (pY2 <> 0) Then
        Scala = Abs(((DE - DI) / 2) / pY2)
      End If
      If (pY1 <> 0) And (pY2 = 0) Then
        Scala = Abs(((DE - DI) / 2) / pY1)
      End If
      If (pY1 <> 0) And (pY2 <> 0) Then
        If (Abs(pY1) > Abs(pY2)) Then
          Scala = Abs(((DE - DI) / 2) / pY1)
        Else
          Scala = Abs(((DE - DI) / 2) / pY2)
        End If
      End If
    End If
    pX1 = OD_Fascia(5) + OD_Fascia(4) + OD_Fascia(3) + OD_Fascia(2) + OD_Fascia(1)
    OD_LS = LS + (Ft - pX1) / 2
    DeltaY = PAR.Pic.TextHeight("0") / PAR.Scala + 40 + (DE - DI) / 2
    Call linea(OD_LS, Re + DeltaY, OD_LS + pX1, Re + DeltaY, vbBlack, 1, vbDashDot)
    If (LatoPart = -1) Then
      Call TestoAll(OD_LS, Re + DeltaY, "OD", , True, 1)
    Else
      Call TestoAll(OD_LS + pX1, Re + DeltaY, "OD", , True, 2)
    End If
    SpL = 1
    'conico
    Select Case OD_Tipo
      Case 1
        pX1 = OD_LS + OD_Fascia(5) + OD_Fascia(4) + OD_Fascia(3) + OD_Fascia(2):                pY1 = Re + DeltaY + OD_Salto(1) * Scala
        pX2 = OD_LS + OD_Fascia(5) + OD_Fascia(4) + OD_Fascia(3) + OD_Fascia(2) + OD_Fascia(1): pY2 = Re + DeltaY
        Call linea(pX1, pY1, pX2, pY2, vbBlue, SpL)
        pX1 = OD_LS + OD_Fascia(5) + OD_Fascia(4) + OD_Fascia(3):                               pY1 = Re + DeltaY + (OD_Salto(2) + OD_Salto(1)) * Scala
        pX2 = OD_LS + OD_Fascia(5) + OD_Fascia(4) + OD_Fascia(3) + OD_Fascia(2):                pY2 = Re + DeltaY + (OD_Salto(1)) * Scala
        Call linea(pX1, pY1, pX2, pY2, vbBlue, SpL)
        pX1 = OD_LS + OD_Fascia(5) + OD_Fascia(4):                                              pY1 = Re + DeltaY + (OD_Salto(3) + OD_Salto(2) + OD_Salto(1)) * Scala
        pX2 = OD_LS + OD_Fascia(5) + OD_Fascia(4) + OD_Fascia(3):                               pY2 = Re + DeltaY + (OD_Salto(2) + OD_Salto(1)) * Scala
        Call linea(pX1, pY1, pX2, pY2, vbBlue, SpL)
        pX1 = OD_LS + OD_Fascia(5):                                                             pY1 = Re + DeltaY + (OD_Salto(4) + OD_Salto(3) + OD_Salto(2) + OD_Salto(1)) * Scala
        pX2 = OD_LS + OD_Fascia(5) + OD_Fascia(4):                                              pY2 = Re + DeltaY + (OD_Salto(3) + OD_Salto(2) + OD_Salto(1)) * Scala
        Call linea(pX1, pY1, pX2, pY2, vbBlue, SpL)
        pX1 = OD_LS:                                                                            pY1 = Re + DeltaY + (OD_Salto(5) + OD_Salto(4) + OD_Salto(3) + OD_Salto(2) + OD_Salto(1)) * Scala
        pX2 = OD_LS + OD_Fascia(5):                                                             pY2 = Re + DeltaY + (OD_Salto(4) + OD_Salto(3) + OD_Salto(2) + OD_Salto(1)) * Scala
        Call linea(pX1, pY1, pX2, pY2, vbBlue, SpL)
      Case 2
        pX1 = OD_LS + OD_Fascia(5) + OD_Fascia(4) + OD_Fascia(3) + OD_Fascia(2):                pY1 = Re + DeltaY + OD_Salto(1) * Scala
        pX2 = OD_LS + OD_Fascia(5) + OD_Fascia(4) + OD_Fascia(3) + OD_Fascia(2) + OD_Fascia(1): pY2 = Re + DeltaY
        Call linea(pX1, pY1, pX2, pY2, vbBlue, SpL)
        pX1 = OD_LS + OD_Fascia(5) + OD_Fascia(4) + OD_Fascia(3) + OD_Fascia(2):                pY1 = Re + DeltaY + (OD_Salto(2) + OD_Salto(1)) * Scala
        pX2 = OD_LS + OD_Fascia(5) + OD_Fascia(4) + OD_Fascia(3) + OD_Fascia(2):                pY2 = Re + DeltaY + (OD_Salto(1)) * Scala
        Call linea(pX1, pY1, pX2, pY2, vbBlue, SpL)
        pX1 = OD_LS + OD_Fascia(5) + OD_Fascia(4) + OD_Fascia(3):                               pY1 = Re + DeltaY + (OD_Salto(2) + OD_Salto(1)) * Scala
        pX2 = OD_LS + OD_Fascia(5) + OD_Fascia(4) + OD_Fascia(3) + OD_Fascia(2):                pY2 = Re + DeltaY + (OD_Salto(2) + OD_Salto(1)) * Scala
        Call linea(pX1, pY1, pX2, pY2, vbBlue, SpL)
        pX1 = OD_LS + OD_Fascia(5) + OD_Fascia(4) + OD_Fascia(3):                               pY1 = Re + DeltaY + (OD_Salto(3) + OD_Salto(2) + OD_Salto(1)) * Scala
        pX2 = OD_LS + OD_Fascia(5) + OD_Fascia(4) + OD_Fascia(3):                               pY2 = Re + DeltaY + (OD_Salto(2) + OD_Salto(1)) * Scala
        Call linea(pX1, pY1, pX2, pY2, vbBlue, SpL)
        pX1 = OD_LS + OD_Fascia(5) + OD_Fascia(4):                                              pY1 = Re + DeltaY + (OD_Salto(3) + OD_Salto(2) + OD_Salto(1)) * Scala
        pX2 = OD_LS + OD_Fascia(5) + OD_Fascia(4) + OD_Fascia(3):                               pY2 = Re + DeltaY + (OD_Salto(3) + OD_Salto(2) + OD_Salto(1)) * Scala
        Call linea(pX1, pY1, pX2, pY2, vbBlue, SpL)
        pX1 = OD_LS + OD_Fascia(5) + OD_Fascia(4):                                              pY1 = Re + DeltaY + (OD_Salto(4) + OD_Salto(3) + OD_Salto(2) + OD_Salto(1)) * Scala
        pX2 = OD_LS + OD_Fascia(5) + OD_Fascia(4):                                              pY2 = Re + DeltaY + (OD_Salto(3) + OD_Salto(2) + OD_Salto(1)) * Scala
        Call linea(pX1, pY1, pX2, pY2, vbBlue, SpL)
        pX1 = OD_LS + OD_Fascia(5):                                                             pY1 = Re + DeltaY + (OD_Salto(4) + OD_Salto(3) + OD_Salto(2) + OD_Salto(1)) * Scala
        pX2 = OD_LS + OD_Fascia(5) + OD_Fascia(4):                                              pY2 = Re + DeltaY + (OD_Salto(4) + OD_Salto(3) + OD_Salto(2) + OD_Salto(1)) * Scala
        Call linea(pX1, pY1, pX2, pY2, vbBlue, SpL)
        pX1 = OD_LS + OD_Fascia(5):                                                             pY1 = Re + DeltaY + (OD_Salto(5) + OD_Salto(4) + OD_Salto(3) + OD_Salto(2) + OD_Salto(1)) * Scala
        pX2 = OD_LS + OD_Fascia(5):                                                             pY2 = Re + DeltaY + (OD_Salto(4) + OD_Salto(3) + OD_Salto(2) + OD_Salto(1)) * Scala
        Call linea(pX1, pY1, pX2, pY2, vbBlue, SpL)
        pX1 = OD_LS:                                                                            pY1 = Re + DeltaY + (OD_Salto(5) + OD_Salto(4) + OD_Salto(3) + OD_Salto(2) + OD_Salto(1)) * Scala
        pX2 = OD_LS + OD_Fascia(5):                                                             pY2 = Re + DeltaY + (OD_Salto(5) + OD_Salto(4) + OD_Salto(3) + OD_Salto(2) + OD_Salto(1)) * Scala
        Call linea(pX1, pY1, pX2, pY2, vbBlue, SpL)
    End Select
  End If
  '
  'Rotore
  Call linea(LS, -Re, LS, Re, vbBlue, 2)
  Call linea(LS, -Re, LS + Ft, -Re, vbBlue, 2)
  Call linea(LS, Re, LS + Ft, Re, vbBlue, 2)
  Call linea(LS + Ft, -Re, LS + Ft, Re, vbBlue, 2)
  If (Abs(Beta) = 90) Then
    'DRITTO
    For i = 1 To PRINCIPI
      Call linea(LS, -Re + i * 2 * Re / PRINCIPI, LS + Ft, -Re + i * 2 * Re / PRINCIPI, vbBlue, 1)
    Next i
  ElseIf Abs(Beta) < 90 And Abs(Beta) > 50 Then
    'INGRANAGGO
    Select Case Beta
      Case Is > 0
        For i = 1 To PRINCIPI / 2
        Call linea(LS, Re - (i - 0) * 4 * Re / PRINCIPI, LS + Ft, Re - (i - 1) * 4 * Re / PRINCIPI, vbBlue, 2)
        Next i
      Case Is < 0
        For i = 1 To PRINCIPI / 2
        Call linea(LS, Re - (i - 0) * 4 * Re / PRINCIPI, LS + Ft, Re - (i - 1) * 4 * Re / PRINCIPI, vbBlue, 2)
        Next i
    End Select
  Else
    'VITE
    Dim TRATTO As Double
    If PASSO > Ft Then
      TRATTO = Ft
    Else
      TRATTO = PASSO
    End If
    PRINCIPI = Ft / Abs(TRATTO)
    Select Case Beta
      Case Is > 0
        For i = 1 To PRINCIPI
        Call linea(LS + (i - 1) * Abs(TRATTO), Re, LS + i * Abs(TRATTO), -Re)
        Next i
      Case Is < 0
        For i = 1 To PRINCIPI
        Call linea(LS + (i - 1) * Abs(TRATTO), -Re, LS + i * Abs(TRATTO), Re)
        Next i
    End Select
  End If
  'Spina dx
  Call linea(LS + Ft, -RS, 2 * LS + Ft, -RS)
  Call linea(LS + Ft, RS, 2 * LS + Ft, RS)
  Call linea(2 * LS + Ft, -RS, 2 * LS + Ft, RS)
  'Contropunta
  Call linea(2 * LS + Ft, RS / 2, 3 * LS + Ft, RS, RGB(100, 100, 100))
  Call linea(2 * LS + Ft, -RS / 2, 3 * LS + Ft, -RS, RGB(100, 100, 100))
  Call linea(3 * LS + Ft, RS, 8 * LS + Ft, RS, RGB(100, 100, 100))
  Call linea(3 * LS + Ft, -RS, 8 * LS + Ft, -RS, RGB(100, 100, 100))
  '
  Call QuotaH(LS + Ft, Re + 10, LS + Ft + ExtracorsaOut, Re + 10, "" & ExtracorsaOut, 12, , 3)
  Call QuotaH(LS - ExtracorsaIn, Re + 10, LS, Re + 10, "" & ExtracorsaIn, 12, , 3)
  '
  If Not NotScale Then
    Call QuotaH(LS, Re + 10, LS + Ft, Re + 10, "" & Ft, 12, , 3)
  Else
    Call QuotaH(LS, Re + 10, LS + Ft, Re + 10, "?", 12, vbRed, 3)
    Call TestoInfo("DRAWING NOT IN SCALE ! ", , True, vbRed, True)
  End If
  '
  PAR.MargineDxPic = 0
  PAR.MargineSxPic = 0
  '
  Call TestoInfo("Units: [mm]", 3)
  Call TestoInfo(LNP_Tabella1("iDiaMola") & ": " & Lp("iDiaMola"), 1, , vbBlue, True)
  '
End Sub

Public Sub Disegna_PROFILO_RAB_ESTERNI(Stampante As Boolean)
'
Dim nZone    As Integer
Dim DE       As Double
Dim Re       As Double
Dim DiaMola  As Double
Dim AngMola  As Double
Dim tColore  As Long
Dim tX1      As Double
Dim tX       As Double
Dim tY       As Double
Dim i        As Integer
Dim NotScale As Boolean
Dim Beta     As Double
Dim PASSO    As Double
Dim PRINCIPI As Integer
Dim LMola    As Double
Dim npt      As Integer
'
On Error Resume Next
  '
  Call InizializzaVariabili
  '
  NotScale = False
  '
  npt = LEP("R[147]")
  DE = Lp("RA[0,1]") * 2
  DiaMola = Lp("iDiaMola")
  AngMola = Lp("R[41]")
  Beta = Lp("R[41]")
  PASSO = Lp("R[390]")
  PRINCIPI = Lp("R[5]")
  LMola = Lp("SPMOLA_G[0,1]")
  Re = DE / 2
  '
  NotScale = True
  '
  PAR.MaxX = Re
  PAR.MinX = -Re
  PAR.MaxY = Re
  PAR.MinY = -Re
  PAR.Allineamento = 0
  Call CalcolaParPic(PAR, True, Stampante)
  '
  PAR.MargineDxPic = PAR.Pic.TextWidth("Z=888.888")
  PAR.MargineSxPic = PAR.Pic.TextWidth("Z=888.888")
  '
  'Call linea(-0.5, 0.5, 0.5, 0.5, vbRed, 1, 0)
  'Call linea(0.5, 0.5, 0.5, -0.5, vbRed, 1, 0)
  'Call linea(0.5, -0.5, -0.5, -0.5, vbRed, 1, 0)
  'Call linea(-0.5, -0.5, -0.5, 0.5, vbRed, 1, 0)
  '
  Dim RAGGIO1  As Double
  Dim Alfa1    As Double
  Dim RAGGIO2  As Double
  Dim Alfa2    As Double
  Dim dALFA    As Double
  Dim SALTO    As Integer
  '
  If (npt > 39) Then
    SALTO = Int(npt / 39)
  Else
    SALTO = 1
  End If
  '
  If (npt > 0) Then
    'ROTORE
    For i = 1 To npt - SALTO Step SALTO
      RAGGIO1 = LEP("RA[0," & Format$(i, "##0") & "]"):     Alfa1 = LEP("AL[0," & Format$(i, "##0") & "]")
      RAGGIO2 = LEP("RA[0," & Format$(i + SALTO, "##0") & "]"): Alfa2 = LEP("AL[0," & Format$(i + SALTO, "##0") & "]")
      Call linea(RAGGIO1 * Sin(Alfa1 * PG / 180), (RAGGIO1 * Cos(Alfa1 * PG / 180) - Re), RAGGIO2 * Sin(Alfa2 * PG / 180), (RAGGIO2 * Cos(Alfa2 * PG / 180) - Re), vbRed, 2, 0)
    Next i
    If (i + SALTO) > npt Then
      RAGGIO1 = LEP("RA[0," & Format$(i, "##0") & "]"):     Alfa1 = LEP("AL[0," & Format$(i, "##0") & "]")
      RAGGIO2 = LEP("RA[0," & Format$(npt, "##0") & "]"): Alfa2 = LEP("AL[0," & Format$(npt, "##0") & "]")
      Call linea(RAGGIO1 * Sin(Alfa1 * PG / 180), (RAGGIO1 * Cos(Alfa1 * PG / 180) - Re), RAGGIO2 * Sin(Alfa2 * PG / 180), (RAGGIO2 * Cos(Alfa2 * PG / 180) - Re), vbRed, 2, 0)
    End If
    For i = 1 To npt - SALTO Step SALTO
      RAGGIO1 = LEP("RA[0," & Format$(i, "##0") & "]"):     Alfa1 = LEP("AL[0," & Format$(i, "##0") & "]"): Alfa1 = Alfa1 - 2 * 360 / PRINCIPI
      RAGGIO2 = LEP("RA[0," & Format$(i + SALTO, "##0") & "]"): Alfa2 = LEP("AL[0," & Format$(i + SALTO, "##0") & "]"): Alfa2 = Alfa2 - 2 * 360 / PRINCIPI
      Call linea(RAGGIO1 * Sin(Alfa1 * PG / 180), (RAGGIO1 * Cos(Alfa1 * PG / 180) - Re), RAGGIO2 * Sin(Alfa2 * PG / 180), (RAGGIO2 * Cos(Alfa2 * PG / 180) - Re), vbRed, 2, 0)
    Next i
    If (i + SALTO) > npt Then
      RAGGIO1 = LEP("RA[0," & Format$(i, "##0") & "]"):     Alfa1 = LEP("AL[0," & Format$(i, "##0") & "]"): Alfa1 = Alfa1 - 2 * 360 / PRINCIPI
      RAGGIO2 = LEP("RA[0," & Format$(npt, "##0") & "]"): Alfa2 = LEP("AL[0," & Format$(npt, "##0") & "]"): Alfa2 = Alfa2 - 2 * 360 / PRINCIPI
      Call linea(RAGGIO1 * Sin(Alfa1 * PG / 180), (RAGGIO1 * Cos(Alfa1 * PG / 180) - Re), RAGGIO2 * Sin(Alfa2 * PG / 180), (RAGGIO2 * Cos(Alfa2 * PG / 180) - Re), vbRed, 2, 0)
    End If
    For i = 1 To npt - SALTO Step SALTO
      RAGGIO1 = LEP("RA[0," & Format$(i, "##0") & "]"):     Alfa1 = LEP("AL[0," & Format$(i, "##0") & "]"): Alfa1 = Alfa1 - 360 / PRINCIPI
      RAGGIO2 = LEP("RA[0," & Format$(i + SALTO, "##0") & "]"): Alfa2 = LEP("AL[0," & Format$(i + SALTO, "##0") & "]"): Alfa2 = Alfa2 - 360 / PRINCIPI
      Call linea(RAGGIO1 * Sin(Alfa1 * PG / 180), (RAGGIO1 * Cos(Alfa1 * PG / 180) - Re), RAGGIO2 * Sin(Alfa2 * PG / 180), (RAGGIO2 * Cos(Alfa2 * PG / 180) - Re), vbRed, 2, 0)
    Next i
    If (i + SALTO) > npt Then
      RAGGIO1 = LEP("RA[0," & Format$(i, "##0") & "]"):     Alfa1 = LEP("AL[0," & Format$(i, "##0") & "]"): Alfa1 = Alfa1 - 360 / PRINCIPI
      RAGGIO2 = LEP("RA[0," & Format$(npt, "##0") & "]"): Alfa2 = LEP("AL[0," & Format$(npt, "##0") & "]"): Alfa2 = Alfa2 - 360 / PRINCIPI
      Call linea(RAGGIO1 * Sin(Alfa1 * PG / 180), (RAGGIO1 * Cos(Alfa1 * PG / 180) - Re), RAGGIO2 * Sin(Alfa2 * PG / 180), (RAGGIO2 * Cos(Alfa2 * PG / 180) - Re), vbRed, 2, 0)
    End If
    For i = 1 To npt - SALTO Step SALTO
      RAGGIO1 = LEP("RA[0," & Format$(i, "##0") & "]"):     Alfa1 = LEP("AL[0," & Format$(i, "##0") & "]"): Alfa1 = Alfa1 + 360 / PRINCIPI
      RAGGIO2 = LEP("RA[0," & Format$(i + SALTO, "##0") & "]"): Alfa2 = LEP("AL[0," & Format$(i + SALTO, "##0") & "]"): Alfa2 = Alfa2 + 360 / PRINCIPI
      Call linea(RAGGIO1 * Sin(Alfa1 * PG / 180), (RAGGIO1 * Cos(Alfa1 * PG / 180) - Re), RAGGIO2 * Sin(Alfa2 * PG / 180), (RAGGIO2 * Cos(Alfa2 * PG / 180) - Re), vbRed, 2, 0)
    Next i
    If (i + SALTO) > npt Then
      RAGGIO1 = LEP("RA[0," & Format$(i, "##0") & "]"):     Alfa1 = LEP("AL[0," & Format$(i, "##0") & "]"): Alfa1 = Alfa1 + 360 / PRINCIPI
      RAGGIO2 = LEP("RA[0," & Format$(npt, "##0") & "]"): Alfa2 = LEP("AL[0," & Format$(npt, "##0") & "]"): Alfa2 = Alfa2 + 360 / PRINCIPI
      Call linea(RAGGIO1 * Sin(Alfa1 * PG / 180), (RAGGIO1 * Cos(Alfa1 * PG / 180) - Re), RAGGIO2 * Sin(Alfa2 * PG / 180), (RAGGIO2 * Cos(Alfa2 * PG / 180) - Re), vbRed, 2, 0)
    End If
    For i = 1 To npt - SALTO Step SALTO
      RAGGIO1 = LEP("RA[0," & Format$(i, "##0") & "]"):     Alfa1 = LEP("AL[0," & Format$(i, "##0") & "]"): Alfa1 = Alfa1 + 2 * 360 / PRINCIPI
      RAGGIO2 = LEP("RA[0," & Format$(i + SALTO, "##0") & "]"): Alfa2 = LEP("AL[0," & Format$(i + SALTO, "##0") & "]"): Alfa2 = Alfa2 + 2 * 360 / PRINCIPI
      Call linea(RAGGIO1 * Sin(Alfa1 * PG / 180), (RAGGIO1 * Cos(Alfa1 * PG / 180) - Re), RAGGIO2 * Sin(Alfa2 * PG / 180), (RAGGIO2 * Cos(Alfa2 * PG / 180) - Re), vbRed, 2, 0)
    Next i
    If (i + SALTO) > npt Then
      RAGGIO1 = LEP("RA[0," & Format$(i, "##0") & "]"):     Alfa1 = LEP("AL[0," & Format$(i, "##0") & "]"): Alfa1 = Alfa1 + 2 * 360 / PRINCIPI
      RAGGIO2 = LEP("RA[0," & Format$(npt, "##0") & "]"): Alfa2 = LEP("AL[0," & Format$(npt, "##0") & "]"): Alfa2 = Alfa2 + 2 * 360 / PRINCIPI
      Call linea(RAGGIO1 * Sin(Alfa1 * PG / 180), (RAGGIO1 * Cos(Alfa1 * PG / 180) - Re), RAGGIO2 * Sin(Alfa2 * PG / 180), (RAGGIO2 * Cos(Alfa2 * PG / 180) - Re), vbRed, 2, 0)
    End If
    'DIAMETRO ESTERNO
    Dim NpT2 As Integer
    NpT2 = 99
    RAGGIO1 = Re: Alfa1 = LEP("AL[0," & Format$(npt - 1, "##0") & "]")
    RAGGIO2 = Re: Alfa2 = 90
    dALFA = (Alfa2 - Alfa1) / NpT2
    For i = 1 To NpT2 - 1
      Call linea(RAGGIO1 * Sin((Alfa1 + dALFA * i) * PG / 180), (RAGGIO1 * Cos((Alfa1 + dALFA * i) * PG / 180) - Re), RAGGIO2 * Sin((Alfa1 + dALFA * (i + 1)) * PG / 180), (RAGGIO2 * Cos((Alfa1 + dALFA * (i + 1)) * PG / 180) - Re), vbBlue, 1, 0)
    Next i
    RAGGIO1 = Re: Alfa1 = LEP("AL[0,1]")
    RAGGIO2 = Re: Alfa2 = -90
    dALFA = (Alfa2 - Alfa1) / NpT2
    For i = 1 To NpT2 - 1
      Call linea(RAGGIO1 * Sin((Alfa1 + dALFA * i) * PG / 180), (RAGGIO1 * Cos((Alfa1 + dALFA * i) * PG / 180) - Re), RAGGIO2 * Sin((Alfa1 + dALFA * (i + 1)) * PG / 180), (RAGGIO2 * Cos((Alfa1 + dALFA * (i + 1)) * PG / 180) - Re), vbBlue, 1, 0)
    Next i
    Call QuotaH(-RAGGIO1, (-Re), RAGGIO2, (-Re), "" & DE, 12, , 3)
    'MOLA
    Call QuotaH(-LMola / 2, 10, LMola / 2, 10, "" & LMola, 12, , 3)
    Call linea(-LMola / 2, 10, -LMola / 2, -(ParMolaROT.YY(0) - ParMolaROT.YY(1)), vbRed, 1, 0)
    Call linea(-LMola / 2, -(ParMolaROT.YY(0) - ParMolaROT.YY(1)), ParMolaROT.XX(0), -(ParMolaROT.YY(0) - ParMolaROT.YY(1)), vbRed, 1, 0)
    Call linea(LMola / 2, 10, LMola / 2, -(ParMolaROT.YY(npt + 1) - ParMolaROT.YY(1)), vbRed, 1, 0)
    Call linea(LMola / 2, -(ParMolaROT.YY(npt + 1) - ParMolaROT.YY(1)), ParMolaROT.XX(npt + 1), -(ParMolaROT.YY(npt + 1) - ParMolaROT.YY(1)), vbRed, 1, 0)
    For i = 0 To npt + 1 - SALTO Step SALTO
      Call linea(ParMolaROT.XX(i), -(ParMolaROT.YY(i) - ParMolaROT.YY(1)), ParMolaROT.XX(i + SALTO), -(ParMolaROT.YY(i + SALTO) - ParMolaROT.YY(1)), vbBlue, 2, 0)
    Next i
    If (i + SALTO) > (npt + 1) Then
      Call linea(ParMolaROT.XX(i), -(ParMolaROT.YY(i) - ParMolaROT.YY(1)), ParMolaROT.XX(npt + 1), -(ParMolaROT.YY(npt + 1) - ParMolaROT.YY(1)), vbBlue, 2, 0)
    End If
  End If
  '
  Call TestoInfo("DRAWING NOT IN SCALE ! ", 1, True, vbRed, True)
  Call TestoInfo(Format$(SALTO, "###0") & "/" & Format$(npt, "###0"), 2, True, vbBlack, False)
  '
  PAR.MargineDxPic = 0
  PAR.MargineSxPic = 0
  '
End Sub

Public Sub Disegna_ROTORI_ESTERNI(Stampante As Boolean)
'
Dim nZone    As Integer
Dim DE       As Double
Dim Re       As Double
Dim DiaMola  As Double
Dim AngMola  As Double
Dim tColore  As Long
Dim tX1      As Double
Dim tX       As Double
Dim tY       As Double
Dim i        As Integer
Dim Beta     As Double
Dim PASSO    As Double
Dim PRINCIPI As Integer
Dim LMola    As Double
Dim npt      As Integer
Dim NPF1     As Integer
'
On Error Resume Next
  '
  Call InizializzaVariabili
  '
  npt = LEP("R[147]")
  NPF1 = LEP("R[43]")
  DE = Lp("R[44]")
  DiaMola = Lp("iDiaMola")
  AngMola = Lp("R[258]")
  Beta = Lp("R[258]")
  PASSO = Lp("R[390]")
  PRINCIPI = Lp("R[5]")
  LMola = Lp("R[37]")
  Re = DE / 2
  '
  PAR.MaxX = Re * 1.1: PAR.MinX = -Re * 1.1
  PAR.MaxY = Re:       PAR.MinY = -Re
  PAR.Allineamento = 0
  Call CalcolaParPic(PAR, True, Stampante)
  '
  PAR.MargineDxPic = PAR.Pic.TextWidth("Z=888.888")
  PAR.MargineSxPic = PAR.Pic.TextWidth("Z=888.888")
  '
  'Call linea(-15, 15, 15, 15, vbRed, 1, 0)
  'Call linea(15, 15, 15, -15, vbRed, 1, 0)
  'Call linea(15, -15, -15, -15, vbRed, 1, 0)
  'Call linea(-15, -15, -15, 15, vbRed, 1, 0)
  '
  Dim RAGGIO1 As Double
  Dim Alfa1   As Double
  Dim beta1   As Double
  Dim RAGGIO2 As Double
  Dim Alfa2   As Double
  Dim dALFA   As Double
  Dim SALTO   As Integer
  Dim RA()    As Double
  Dim al()    As Double
  Dim SpostaY As Double
  '
Dim PERCORSO    As String
Dim COORDINATE  As String
Dim RADICE      As String
  '
  'LETTURA DEL PERCORSO DI RIFERIMENTO E COORDINATE CLIENTE
  PERCORSO = LEP_STR("PERCORSO")
  PERCORSO = g_chOemPATH & "\" & PERCORSO
  'RICAVO IL NOME DEL ROTORE DAL PERCORSO
  COORDINATE = PERCORSO
  If (Len(COORDINATE) > 0) Then
    i = InStr(COORDINATE, "\")
    While (i <> 0)
      j = i
      i = InStr(i + 1, COORDINATE, "\")
    Wend
    'RICAVO IL NOME DEL ROTORE
    COORDINATE = Right$(COORDINATE, Len(COORDINATE) - j)
  End If
  'RICAVO IL PERCORSO DI LAVORO
  RADICE = PERCORSO
  i = InStr(RADICE, COORDINATE)
  RADICE = Left$(RADICE, i - 2)
  '
  If (Dir$(RADICE & "\PRFROT") = "") Then
    WRITE_DIALOG RADICE & "\PRFROT" & " profile does not exist!!"
    Exit Sub
  End If
  '
  Open RADICE & "\PRFROT" For Input As #1
    i = 0
    While Not EOF(1)
      i = i + 1
      ReDim Preserve RA(i): ReDim Preserve al(i)
      Input #1, RA(i), al(i), beta1
    Wend
  Close #1
  '
  'For i = 1 To npt
  '  ReDim Preserve RA(i): RA(i) = LEP("RA[0," & Format$(i, "##0") & "]")
  '  ReDim Preserve al(i): al(i) = LEP("AL[0," & Format$(i, "##0") & "]")
  'Next i
  'If (npt > 39) Then
  '  SALTO = Int(npt / 39)
  'Else
    SALTO = 1
  'End If
  SpostaY = Re
  If (npt > 0) Then
    'ROTORE
    For i = 1 To npt - SALTO Step SALTO
      RAGGIO1 = RA(i):         Alfa1 = al(i):         Alfa1 = (Alfa1) * PG / 180
      RAGGIO2 = RA(i + SALTO): Alfa2 = al(i + SALTO): Alfa2 = (Alfa2) * PG / 180
      Call linea(RAGGIO1 * Sin(Alfa1), (RAGGIO1 * Cos(Alfa1) - SpostaY), RAGGIO2 * Sin(Alfa2), (RAGGIO2 * Cos(Alfa2) - SpostaY), vbBlack, 2, 0)
    Next i
    If (i + SALTO) > npt Then
      RAGGIO1 = RA(i):         Alfa1 = al(i):         Alfa1 = (Alfa1) * PG / 180
      RAGGIO2 = RA(npt):       Alfa2 = al(npt):       Alfa2 = (Alfa2) * PG / 180
      Call linea(RAGGIO1 * Sin(Alfa1), (RAGGIO1 * Cos(Alfa1) - SpostaY), RAGGIO2 * Sin(Alfa2), (RAGGIO2 * Cos(Alfa2) - SpostaY), vbBlack, 2, 0)
    End If
    For i = 1 To npt - SALTO Step SALTO
      RAGGIO1 = RA(i):         Alfa1 = al(i):         Alfa1 = (Alfa1 - 360 / PRINCIPI) * PG / 180
      RAGGIO2 = RA(i + SALTO): Alfa2 = al(i + SALTO): Alfa2 = (Alfa2 - 360 / PRINCIPI) * PG / 180
      Call linea(RAGGIO1 * Sin(Alfa1), (RAGGIO1 * Cos(Alfa1) - SpostaY), RAGGIO2 * Sin(Alfa2), (RAGGIO2 * Cos(Alfa2) - SpostaY), vbRed, 2, 0)
    Next i
    If (i + SALTO) > npt Then
      RAGGIO1 = RA(i):         Alfa1 = al(i):         Alfa1 = (Alfa1 - 360 / PRINCIPI) * PG / 180
      RAGGIO2 = RA(npt):       Alfa2 = al(npt):       Alfa2 = (Alfa2 - 360 / PRINCIPI) * PG / 180
      Call linea(RAGGIO1 * Sin(Alfa1), (RAGGIO1 * Cos(Alfa1) - SpostaY), RAGGIO2 * Sin(Alfa2), (RAGGIO2 * Cos(Alfa2) - SpostaY), vbRed, 2, 0)
    End If
    For i = 1 To npt - SALTO Step SALTO
      RAGGIO1 = RA(i):         Alfa1 = al(i):         Alfa1 = (Alfa1 + 360 / PRINCIPI) * PG / 180
      RAGGIO2 = RA(i + SALTO): Alfa2 = al(i + SALTO): Alfa2 = (Alfa2 + 360 / PRINCIPI) * PG / 180
      Call linea(RAGGIO1 * Sin(Alfa1), (RAGGIO1 * Cos(Alfa1) - SpostaY), RAGGIO2 * Sin(Alfa2), (RAGGIO2 * Cos(Alfa2) - SpostaY), vbRed, 2, 0)
    Next i
    If (i + SALTO) > npt Then
      RAGGIO1 = RA(i):         Alfa1 = al(i):         Alfa1 = (Alfa1 + 360 / PRINCIPI) * PG / 180
      RAGGIO2 = RA(npt):       Alfa2 = al(npt):       Alfa2 = (Alfa2 + 360 / PRINCIPI) * PG / 180
      Call linea(RAGGIO1 * Sin(Alfa1), (RAGGIO1 * Cos(Alfa1) - SpostaY), RAGGIO2 * Sin(Alfa2), (RAGGIO2 * Cos(Alfa2) - SpostaY), vbRed, 2, 0)
    End If
    'DIAMETRO ESTERNO
    Dim NpT2 As Integer
    NpT2 = 99
    RAGGIO1 = Re: Alfa1 = al(npt - 1)
    RAGGIO2 = Re: Alfa2 = 90
    dALFA = (Alfa2 - Alfa1) / NpT2
    For i = 1 To NpT2 - 1
      Call linea(RAGGIO1 * Sin((Alfa1 + dALFA * i) * PG / 180), (RAGGIO1 * Cos((Alfa1 + dALFA * i) * PG / 180) - SpostaY), RAGGIO2 * Sin((Alfa1 + dALFA * (i + 1)) * PG / 180), (RAGGIO2 * Cos((Alfa1 + dALFA * (i + 1)) * PG / 180) - SpostaY), vbBlue, 1, 0)
    Next i
    RAGGIO1 = Re: Alfa1 = al(1)
    RAGGIO2 = Re: Alfa2 = -90
    dALFA = (Alfa2 - Alfa1) / NpT2
    For i = 1 To NpT2 - 1
      Call linea(RAGGIO1 * Sin((Alfa1 + dALFA * i) * PG / 180), (RAGGIO1 * Cos((Alfa1 + dALFA * i) * PG / 180) - SpostaY), RAGGIO2 * Sin((Alfa1 + dALFA * (i + 1)) * PG / 180), (RAGGIO2 * Cos((Alfa1 + dALFA * (i + 1)) * PG / 180) - SpostaY), vbBlue, 1, 0)
    Next i
    Call QuotaH(-RAGGIO1, (-SpostaY), RAGGIO2, (-SpostaY), "" & DE, 12, , 4)
    i = 1
      RAGGIO1 = RA(i):         Alfa1 = al(i):         Alfa1 = (Alfa1) * PG / 180
      Call linea(RAGGIO1 * Sin(Alfa1), (RAGGIO1 * Cos(Alfa1) - SpostaY), 0, -Re, vbBlue, 1, 1)
    i = NPF1
      RAGGIO1 = RA(i):         Alfa1 = al(i):         Alfa1 = (Alfa1) * PG / 180
      Call linea(RAGGIO1 * Sin(Alfa1), (RAGGIO1 * Cos(Alfa1) - SpostaY), 0, -Re, vbRed, 1, 1)
      Call TestoAll(RAGGIO1 * Sin(Alfa1), (RAGGIO1 * Cos(Alfa1) - SpostaY), frmt4(RAGGIO1), vbRed, 1, 4)
    i = npt
      RAGGIO1 = RA(i):         Alfa1 = al(i):         Alfa1 = (Alfa1) * PG / 180
      Call linea(RAGGIO1 * Sin(Alfa1), (RAGGIO1 * Cos(Alfa1) - SpostaY), 0, -Re, vbBlack, 1, 1)
      Call TestoAll(RAGGIO1 * Sin(Alfa1), (RAGGIO1 * Cos(Alfa1) - SpostaY), frmt4(RAGGIO1), vbBlack, 1, 3)
    'MOLA
    Call QuotaH(-LMola / 2, 10, LMola / 2, 10, "" & LMola, 12, , 3)
    Call linea(-LMola / 2, 10, -LMola / 2, -(ParMolaROT.YY(0) - ParMolaROT.YY(1)), vbRed, 1, 0)
    Call linea(-LMola / 2, -(ParMolaROT.YY(0) - ParMolaROT.YY(1)), ParMolaROT.XX(0), -(ParMolaROT.YY(0) - ParMolaROT.YY(1)), vbRed, 1, 0)
    Call linea(LMola / 2, 10, LMola / 2, -(ParMolaROT.YY(npt + 1) - ParMolaROT.YY(1)), vbRed, 1, 0)
    Call linea(LMola / 2, -(ParMolaROT.YY(npt + 1) - ParMolaROT.YY(1)), ParMolaROT.XX(npt + 1), -(ParMolaROT.YY(npt + 1) - ParMolaROT.YY(1)), vbRed, 1, 0)
    For i = 0 To npt + 1 - SALTO Step SALTO
      Call linea(ParMolaROT.XX(i), -(ParMolaROT.YY(i) - ParMolaROT.YY(1)), ParMolaROT.XX(i + SALTO), -(ParMolaROT.YY(i + SALTO) - ParMolaROT.YY(1)), vbBlue, 2, 0)
    Next i
    If (i + SALTO) > (npt + 1) Then
      Call linea(ParMolaROT.XX(i), -(ParMolaROT.YY(i) - ParMolaROT.YY(1)), ParMolaROT.XX(npt + 1), -(ParMolaROT.YY(npt + 1) - ParMolaROT.YY(1)), vbBlue, 2, 0)
    End If
    Call linea(0, -Re + 6, 0, 0, vbBlack, 1, 1)
  End If
  '
  Call TestoInfo("DRAWING NOT IN SCALE ! ", 1, True, vbRed, True)
  'Call TestoInfo(Format$(SALTO, "###0") & "/" & Format$(npt, "###0"), 2, True, vbBlack, False)
  Call TestoInfo(Format$(npt, "###0"), 2, True, vbBlack, False)
  '
  PAR.MargineDxPic = 0
  PAR.MargineSxPic = 0
  '
End Sub

Public Sub Disegna_ZONA_LAVORO_VITI_ESTERNE(Stampante As Boolean)
'
Dim nZone           As Integer
Dim DE              As Double
Dim DP              As Double
Dim Ft              As Double
Dim F1              As Double
Dim F2              As Double
Dim F3              As Double
Dim Re              As Double
Dim RS              As Double
Dim LS              As Double
Dim DiaMola         As Double
Dim AngMola         As Double
Dim LatoPart        As Integer
Dim ExtracorsaIn    As Double
Dim ExtracorsaOut   As Double
Dim TrattoDivisione As Double
Dim tColore         As Long
Dim tX1             As Double
Dim tX              As Double
Dim tY              As Double
Dim i               As Integer
Dim NotScale        As Boolean
Dim QuotaIL         As Double
Dim QuotaFL         As Double
Dim XF0             As Double
Dim XF1             As Double
Dim XF2             As Double
Dim XF3             As Double
Dim TipoVite        As String
Dim XMola           As Double
Dim Ymola           As Double
Dim QZI             As Double
Dim PRINCIPI        As Integer
Dim Con1            As Double
Dim Con2            As Double
Dim Con3            As Double
Dim iTESTA          As Integer
'
On Error Resume Next
  '
  Call InizializzaVariabili
  '
  NotScale = False
  'Lettura parametri vite
  nZone = Lp("iNzone")
  DE = Lp("R[103]") + 2 * Lp("ADDENDUMF1_G")
  DP = Lp("R[103]")
  TipoVite = LNP_Tabella1("TIPO") & ": " & Lp_Str("TIPO")
  DiaMola = 300
  AngMola = Lp("R[41]")
  LatoPart = Lp("R[12]")
     
  Dim TIPO_MACCHINA As String
  TIPO_MACCHINA = GetInfo("Configurazione", "TIPO_MACCHINA", Path_LAVORAZIONE_INI)
  TIPO_MACCHINA = UCase$(Trim$(TIPO_MACCHINA))
  If (TIPO_MACCHINA = "GR250") Then
     iTESTA = Lp("INDTAV_G")
     If (iTESTA = 1) Then
        QZI = Lp("QASSIALESTART_G[1,1]")
     Else
        QZI = Lp("QASSIALESTART_G[0,1]")
     End If
  Else
     QZI = Lp("QASSIALESTART_G")
  End If
  ExtracorsaIn = Lp("CORSAIN")
  ExtracorsaOut = Lp("CORSAOUT")
  TrattoDivisione = Lp("R[130]")
  PRINCIPI = Lp("NUMDENTIM_G")
  If (PRINCIPI = 0) Then PRINCIPI = 1
  '
  F1 = 0: F2 = 0: F3 = 0
  Select Case nZone
    Case 1
      F1 = Lp("Fascia1F1")
      Con1 = Lp("Conic1F1")
      Con2 = 0
      Con3 = 0
    Case 2
      F1 = Lp("Fascia1F1")
      F2 = Lp("Fascia2F1")
      Con1 = Lp("Conic1F1")
      Con2 = Lp("Conic2F1")
      Con3 = 0
    Case 3
      F1 = Lp("Fascia1F1")
      F2 = Lp("Fascia2F1")
      F3 = Lp("Fascia3F1")
      Con1 = Lp("Conic1F1")
      Con2 = Lp("Conic2F1")
      Con3 = Lp("Conic3F1")
  End Select
  Ft = F1 + F2 + F3
  If Ft = 0 Then
    NotScale = True
    F1 = 50
    Ft = 50
  End If
  '
  Re = DE / 2
  RS = Re / 4       'Raggio spina
  LS = RS           'Lunghezza spina sporgente
  '
  PAR.MaxX = 6 * LS + Ft
  PAR.MinX = -2 * LS
  PAR.MaxY = Re + 35
  PAR.MinY = 0
  PAR.Allineamento = 0
  Call CalcolaParPic(PAR, True, Stampante)
  '
  PAR.MargineDxPic = PAR.Pic.TextWidth("Z=888.888")
  PAR.MargineSxPic = PAR.Pic.TextWidth("Z=888.888")
  '
  Select Case LatoPart
    
    Case -1 'Destra
      PAR.MaxX = (2 * LS + Ft + ExtracorsaOut + TrattoDivisione)
      PAR.MinX = -ExtracorsaIn
      PAR.MaxY = 2 * Re
      PAR.MinY = 0
      PAR.ScalaMargine = 1
      PAR.Allineamento = 0
      Call CalcolaParPic(PAR, True, Stampante)
      XMola = LS + Ft + ExtracorsaOut + TrattoDivisione
      Ymola = 0
      QuotaIL = QZI + Ft + ExtracorsaOut + TrattoDivisione
      QuotaFL = QZI - ExtracorsaIn
      Call TestoAll(XMola + 5, Re + 10, "Z=" & QuotaIL, , True, 2)
      Call TestoAll(XMola - TrattoDivisione - ExtracorsaIn - ExtracorsaOut - Ft - 5, Re + 10, "Z=" & QuotaFL, , True, 1)
      
    Case 1 'Sinistra
      PAR.MaxX = (2 * LS + Ft + ExtracorsaOut)
      PAR.MinX = -ExtracorsaIn - TrattoDivisione
      PAR.MaxY = 2 * Re
      PAR.MinY = 0
      PAR.ScalaMargine = 1
      PAR.Allineamento = 0
      Call CalcolaParPic(PAR, True, Stampante)
      XMola = LS - ExtracorsaIn - TrattoDivisione
      Ymola = 0
      QuotaIL = QZI - ExtracorsaIn - TrattoDivisione
      QuotaFL = QZI + Ft + ExtracorsaOut
      Call TestoAll(XMola - 5, Re + 10, "Z=" & QuotaIL, , True, 1)
      Call TestoAll(XMola + TrattoDivisione + ExtracorsaIn + ExtracorsaOut + Ft + 5, Re + 10, "Z=" & QuotaFL, , True, 2)
      
  End Select
  'Centro mola
  Call linea(XMola, Re + 20 + DiaMola, XMola, Re, RGB(255, 200, 200), 1, vbDashDot)
  '
  'Disegno della mola
  Dim SPE     As Double
  Dim spe1    As Double
  Dim spe2    As Double
  Dim Altezza As Double
  Dim OX      As Double
  '
  SPE = Lp("R[37]")
  SPE = SPE / 2
  Altezza = Lp("ALTEZZAFIANCOF1_G")
  spe1 = 1 / 2 * SPE
  spe2 = 1 / 4 * SPE
  '
  Select Case LatoPart
    
    Case -1 'Destra
      Call linea(XMola - SPE - 2 * SPE * (PRINCIPI - 1), Re + 25 + Altezza + DiaMola, XMola - SPE - 2 * SPE * (PRINCIPI - 1), Re + 25 + Altezza, RGB(0, 0, 0), 1, vbSolid)
      Call linea(XMola + SPE, Re + 25 + Altezza + DiaMola, XMola + SPE, Re + 25 + Altezza, RGB(0, 0, 0), 1, vbSolid)
      For i = 1 To PRINCIPI
        OX = -2 * SPE * (i - 1)
        Call linea(XMola - SPE + OX, Re + 25 + Altezza, XMola - spe1 + OX, Re + 25 + Altezza, RGB(0, 0, 0), 1, vbSolid)
        Call linea(XMola - spe1 + OX, Re + 25 + Altezza, XMola - spe2 + OX, Re + 25, RGB(0, 0, 0), 1, vbSolid)
        Call linea(XMola + SPE + OX, Re + 25 + Altezza, XMola + spe1 + OX, Re + 25 + Altezza, RGB(0, 0, 0), 1, vbSolid)
        Call linea(XMola + spe1 + OX, Re + 25 + Altezza, XMola + spe2 + OX, Re + 25, RGB(0, 0, 0), 1, vbSolid)
        Call linea(XMola - spe2 + OX, Re + 25, XMola + spe2 + OX, Re + 25, RGB(0, 0, 0), 1, vbSolid)
      Next i
      
    Case 1  'Sinistra
      Call linea(XMola - SPE, Re + 25 + Altezza + DiaMola, XMola - SPE, Re + 25 + Altezza, RGB(0, 0, 0), 1, vbSolid)
      Call linea(XMola + SPE + 2 * SPE * (PRINCIPI - 1), Re + 25 + Altezza + DiaMola, XMola + SPE + 2 * SPE * (PRINCIPI - 1), Re + 25 + Altezza, RGB(0, 0, 0), 1, vbSolid)
      For i = 1 To PRINCIPI
        OX = 2 * SPE * (i - 1)
        Call linea(XMola - SPE + OX, Re + 25 + Altezza, XMola - spe1 + OX, Re + 25 + Altezza, RGB(0, 0, 0), 1, vbSolid)
        Call linea(XMola - spe1 + OX, Re + 25 + Altezza, XMola - spe2 + OX, Re + 25, RGB(0, 0, 0), 1, vbSolid)
        Call linea(XMola + SPE + OX, Re + 25 + Altezza, XMola + spe1 + OX, Re + 25 + Altezza, RGB(0, 0, 0), 1, vbSolid)
        Call linea(XMola + spe1 + OX, Re + 25 + Altezza, XMola + spe2 + OX, Re + 25, RGB(0, 0, 0), 1, vbSolid)
        Call linea(XMola - spe2 + OX, Re + 25, XMola + spe2 + OX, Re + 25, RGB(0, 0, 0), 1, vbSolid)
      Next i
      '
  End Select
  '
  'conicita' sul diametro interno
  Dim DeltaY  As Single
  Dim Scala   As Single
  Dim pX1     As Single
  Dim pX2     As Single
  Dim pY1     As Single
  Dim pY2     As Single
  Dim SpL     As Integer
  '
  pY1 = Con1 + Con2 + Con3
  If (pY1 <> 0) Then
    Scala = Abs(20 / pY1)
  Else
    pY1 = 0: pY2 = 0
    If (Con1 > 0) Then
      pY1 = pY1 + Con1
    Else
      pY2 = pY2 + Con1
    End If
    If (Con2 > 0) Then
      pY1 = pY1 + Con2
    Else
      pY2 = pY2 + Con2
    End If
    If (Con3 > 0) Then
      pY1 = pY1 + Con3
    Else
      pY2 = pY2 + Con3
    End If
    If (pY1 = 0) And (pY2 = 0) Then
      Scala = 0
    End If
    If (pY1 = 0) And (pY2 <> 0) Then
      Scala = Abs(20 / pY2)
    End If
    If (pY1 <> 0) And (pY2 = 0) Then
      Scala = Abs(20 / pY1)
    End If
    If (pY1 <> 0) And (pY2 <> 0) Then
      If (Abs(pY1) > Abs(pY2)) Then
        Scala = Abs(20 / pY1)
      Else
        Scala = Abs(20 / pY2)
      End If
    End If
  End If
  '
  DeltaY = PAR.Pic.TextHeight("0") / PAR.Scala + 40
  If (Con1 <> 0) Or (Con2 <> 0) Or (Con3 <> 0) Then Call linea(LS, Re + DeltaY, LS + Ft, Re + DeltaY, vbBlack, 1, vbDashDot)
  SpL = 1
  If (LatoPart = -1) Then
    'destra
    If (Con1 <> 0) Or (Con2 <> 0) Or (Con3 <> 0) Then Call TestoAll(LS, Re + DeltaY, "RD", , True, 1)
    pX1 = LS + F1 + F2:       pY1 = Re + DeltaY - Con3 * Scala
    pX2 = LS + F1 + F2 + F3:  pY2 = Re + DeltaY
    Call linea(pX1, pY1, pX2, pY2, vbRed, SpL)
    If (Con3 <> 0) Then Call QuotaV(pX1, pY1, pX2, pY2, "" & Con3, 12, , 1)
    pX1 = LS + F1:      pY1 = Re + DeltaY - (Con2 + Con3) * Scala
    pX2 = LS + F1 + F2: pY2 = Re + DeltaY - Con3 * Scala
    Call linea(pX1, pY1, pX2, pY2, vbRed, SpL)
    If (Con2 <> 0) Then Call QuotaV(pX1, pY2, pX2, pY2, "" & Con2, 12, , 1)
    pX1 = LS:       pY1 = Re + DeltaY - (Con1 + Con2 + Con3) * Scala
    pX2 = LS + F1:  pY2 = Re + DeltaY - (Con2 + Con3) * Scala
    Call linea(pX1, pY1, pX2, pY2, vbRed, SpL)
    If (Con1 <> 0) Then Call QuotaV(pX1, pY1, pX2, pY2, "" & Con1, 12, , 1)
  Else
    'sinistra
    If (Con1 <> 0) Or (Con2 <> 0) Or (Con3 <> 0) Then Call TestoAll(LS + Ft, Re + DeltaY, "RD", , True, 2)
    DeltaY = PAR.Pic.TextHeight("0") / PAR.Scala + 40 + (Con1 + Con2 + Con3) * Scala
    pX1 = LS + F1 + F2:       pY1 = Re + DeltaY - Con3 * Scala
    pX2 = LS + F1 + F2 + F3:  pY2 = Re + DeltaY
    Call linea(pX1, pY1, pX2, pY2, vbRed, SpL)
    If (Con3 <> 0) Then Call QuotaV(pX1, pY1, pX2, pY2, "" & Con3, 12, , 1)
    pX1 = LS + F1:      pY1 = Re + DeltaY - (Con2 + Con3) * Scala
    pX2 = LS + F1 + F2: pY2 = Re + DeltaY - Con3 * Scala
    Call linea(pX1, pY1, pX2, pY2, vbRed, SpL)
    If (Con2 <> 0) Then Call QuotaV(pX1, pY1, pX2, pY2, "" & Con2, 12, , 1)
    pX1 = LS:       pY1 = Re + DeltaY - (Con1 + Con2 + Con3) * Scala
    pX2 = LS + F1:  pY2 = Re + DeltaY - (Con2 + Con3) * Scala
    Call linea(pX1, pY1, pX2, pY2, vbRed, SpL)
    If (Con1 <> 0) Then Call QuotaV(pX1, pY1, pX2, pY2, "" & Con1, 12, , 1)
  End If
  '
  If LatoPart = -1 Then
    tX1 = ExtracorsaIn / 2
    tX1 = XMola - tX1
  Else
    tX1 = ExtracorsaOut / 2
    tX1 = XMola + tX1
  End If
  'Zone
  XF0 = LS
  XF1 = XF0 + F1
  XF2 = XF1 + F2
  XF3 = XF2 + F3
  'Spina sx
  Call linea(0, -RS, 0, RS)
  Call linea(0, -RS, LS, -RS)
  Call linea(0, RS, LS, RS)
  'Vite
  Call linea(LS, -Re, LS, Re, vbBlue, 2)
  Call linea(LS, -Re, LS + Ft, -Re, vbBlue, 2)
  Call linea(LS, Re, LS + Ft, Re, vbBlue, 2)
  Call linea(LS + Ft, -Re, LS + Ft, Re, vbBlue, 2)
  'primitivo
  Call linea(LS, -DP / 2, LS + Ft, -DP / 2, vbRed, 1)
  Call linea(LS, DP / 2, LS + Ft, DP / 2, vbRed, 1)
  Call linea(LS + Ft / 2, DP / 2, LS + Ft / 2, -DP / 2, vbRed, 1)
  Call linea(LS + Ft / 2 - 1, -DP / 2 + 2, LS + Ft / 2, -DP / 2, vbRed, 1)
  Call linea(LS + Ft / 2 + 1, -DP / 2 + 2, LS + Ft / 2, -DP / 2, vbRed, 1)
  Call linea(LS + Ft / 2 - 1, DP / 2 - 2, LS + Ft / 2, DP / 2, vbRed, 1)
  Call linea(LS + Ft / 2 + 1, DP / 2 - 2, LS + Ft / 2, DP / 2, vbRed, 1)
  Call TestoAll(LS + Ft / 2, 0, "Dp=" & DP, vbRed, True, 1)
  'Spina dx
  Call linea(LS + Ft, -RS, 2 * LS + Ft, -RS)
  Call linea(LS + Ft, RS, 2 * LS + Ft, RS)
  Call linea(2 * LS + Ft, -RS, 2 * LS + Ft, RS)
  'Contropunta
  Call linea(2 * LS + Ft, RS / 2, 3 * LS + Ft, RS, RGB(100, 100, 100))
  Call linea(2 * LS + Ft, -RS / 2, 3 * LS + Ft, -RS, RGB(100, 100, 100))
  Call linea(3 * LS + Ft, RS, 8 * LS + Ft, RS, RGB(100, 100, 100))
  Call linea(3 * LS + Ft, -RS, 8 * LS + Ft, -RS, RGB(100, 100, 100))
  If (LatoPart = -1) And (TrattoDivisione > 0) Then 'DX
    Call QuotaH(LS + Ft + ExtracorsaOut, Re + 10, LS + Ft + ExtracorsaOut + TrattoDivisione, Re + 10, "" & TrattoDivisione, 12, , 3)
  End If
  Call QuotaH(LS + Ft, Re + 10, LS + Ft + ExtracorsaOut, Re + 10, "" & ExtracorsaOut, 12, , 3)
  If (LatoPart = 1) And (TrattoDivisione > 0) Then 'SX
    Call QuotaH(LS - ExtracorsaIn - TrattoDivisione, Re + 10, LS - ExtracorsaIn, Re + 10, "" & TrattoDivisione, 12, , 3)
  End If
  Call QuotaH(LS - ExtracorsaIn, Re + 10, LS, Re + 10, "" & ExtracorsaIn, 12, , 3)
  If Not NotScale Then
    Call QuotaH(LS, Re + 10, LS + Ft, Re + 10, "" & Ft, 12, , 3)
  Else
    Call QuotaH(LS, Re + 10, LS + Ft, Re + 10, "?", 12, vbRed, 3)
  End If
  PAR.MargineDxPic = 0
  PAR.MargineSxPic = 0
  Call TestoInfo(TipoVite, 1, , vbBlue, True)

End Sub

Public Sub Disegna_ZONA_LAVORO_SCANALATI_CBN(Stampante As Boolean)
'
Dim DE              As Double
Dim DI              As Double
Dim Ft              As Double
Dim Re              As Double
Dim RS              As Double
Dim LS              As Double
Dim DiaMola         As Double
Dim LatoPart        As Integer
Dim tColore         As Long
Dim tX1             As Double
Dim tX              As Double
Dim tY              As Double
Dim i               As Integer
Dim NotScale        As Boolean
Dim QuotaIL         As Double
Dim QuotaFL         As Double
Dim XF0             As Double
Dim XMola           As Double
Dim Ymola           As Double
Dim tScala          As Double
Dim YQuota          As Double
Dim QZI             As Double
Dim R93             As Double
Dim F1              As Double
Dim F2              As Double
'
On Error Resume Next
  '
  Call InizializzaVariabili
  '
  NotScale = False
  'Lettura parametri
  DE = Lp("R[44]")
  DI = Lp("R[103]")
  R93 = Lp("R[93]")
  QZI = Lp("R[180]")
  DiaMola = 40
  LatoPart = Lp("R[12]")
  F1 = Lp("R[185]")
  F2 = Lp("R[186]")
  Ft = F1 + F2
  If (Ft = 0) Then
    NotScale = True
    Ft = 50
  End If
  '
  Re = DE / 2
  RS = Re / 4       'Raggio spina
  LS = RS           'Lunghezza spina sporgente
  '
  PAR.MaxX = 6 * LS + Ft
  PAR.MinX = -2 * LS
  PAR.MaxY = Re + 35
  PAR.MinY = 0
  PAR.Allineamento = 0
  Call CalcolaParPic(PAR, True, Stampante)
  '
  PAR.MargineDxPic = PAR.Pic.TextWidth("Z=888.888")
  PAR.MargineSxPic = PAR.Pic.TextWidth("Z=888.888")
  '
  Select Case LatoPart
    
    Case -1 'Destra
      PAR.MaxX = (2 * LS + Ft + 2 * RS)
      PAR.MinX = 0
      PAR.MaxY = 2 * Re
      PAR.MinY = -2 * Re
      PAR.ScalaMargine = 1
      PAR.Allineamento = 0
      Call CalcolaParPic(PAR, True, Stampante)
      XMola = LS + Ft
      Ymola = 0
      QuotaIL = QZI
      QuotaFL = QZI - Ft
      tScala = PAR.Scala
      If PAR.Pic Is Printer Then tScala = tScala * 45
      YQuota = Re + 350 / tScala
      Call TestoAll(XMola + 5, Re + 10, "Z=" & QuotaIL, , True, 2)
      Call TestoAll(XMola - Ft - 5, Re + 10, "Z=" & QuotaFL, , True, 1)
      
    Case 1 'Sinistra
      PAR.MaxX = (2 * LS + Ft + 2 * RS)
      PAR.MinX = 0
      PAR.MaxY = 2 * Re
      PAR.MinY = -2 * Re
      PAR.ScalaMargine = 1
      PAR.Allineamento = 0
      Call CalcolaParPic(PAR, True, Stampante)
      XMola = LS
      Ymola = 0
      QuotaIL = QZI
      QuotaFL = QZI + Ft
      tScala = PAR.Scala
      If PAR.Pic Is Printer Then tScala = tScala * 45
      YQuota = Re + 350 / tScala
      Call TestoAll(XMola - 5, Re + 10, "Z=" & QuotaIL, , True, 1)
      Call TestoAll(XMola + Ft + 5, Re + 10, "Z=" & QuotaFL, , True, 2)
      
  End Select
  'Centro mola
  Call linea(XMola, Re + 20 + DiaMola, XMola, Re, RGB(255, 200, 200), 1, vbDashDot)
  'Disegno della mola
  Dim SPE     As Double
  Dim spe1    As Double
  Dim spe2    As Double
  Dim Altezza As Double
  '
  SPE = 20
  SPE = SPE / 2
  Altezza = Lp("R[44]") - Lp("R[103]")
  Altezza = Altezza / 2
  spe1 = 1 / 2 * SPE
  spe2 = 1 / 4 * SPE
  '
  Call linea(XMola - SPE, Re + 30 + Altezza + DiaMola, XMola - SPE, Re + 30 + Altezza, RGB(0, 0, 0), 1, vbSolid)
  Call linea(XMola - SPE, Re + 30 + Altezza, XMola - spe1, Re + 30 + Altezza, RGB(0, 0, 0), 1, vbSolid)
  Call linea(XMola - spe1, Re + 30 + Altezza, XMola - spe2, Re + 30, RGB(0, 0, 0), 1, vbSolid)
  Call linea(XMola + SPE, Re + 30 + Altezza + DiaMola, XMola + SPE, Re + 30 + Altezza, RGB(0, 0, 0), 1, vbSolid)
  Call linea(XMola + SPE, Re + 30 + Altezza, XMola + spe1, Re + 30 + Altezza, RGB(0, 0, 0), 1, vbSolid)
  Call linea(XMola + spe1, Re + 30 + Altezza, XMola + spe2, Re + 30, RGB(0, 0, 0), 1, vbSolid)
  Call linea(XMola - spe2, Re + 30, XMola + spe2, Re + 30, RGB(0, 0, 0), 1, vbSolid)
  '
  tX1 = XMola
  'Zone
  XF0 = LS
  'Spina sx
  Call linea(0, -RS, 0, RS)
  Call linea(0, -RS, LS, -RS)
  Call linea(0, RS, LS, RS)
  'Vite
  Call linea(LS, -Re, LS, Re, vbBlue, 2)
  Call linea(LS, -Re, LS + Ft, -Re, vbBlue, 2)
  Call linea(LS, Re, LS + Ft, Re, vbBlue, 2)
  Call linea(LS + Ft, -Re, LS + Ft, Re, vbBlue, 2)
  'INTERNO
  Call linea(LS, -DI / 2, LS + Ft, -DI / 2, vbRed, 1)
  Call linea(LS, DI / 2, LS + Ft, DI / 2, vbRed, 1)
  Call linea(LS + Ft / 2, DI / 2, LS + Ft / 2, -DI / 2, vbRed, 1)
  Call linea(LS + Ft / 2 - 1, -DI / 2 + 2, LS + Ft / 2, -DI / 2, vbRed, 1)
  Call linea(LS + Ft / 2 + 1, -DI / 2 + 2, LS + Ft / 2, -DI / 2, vbRed, 1)
  Call linea(LS + Ft / 2 - 1, DI / 2 - 2, LS + Ft / 2, DI / 2, vbRed, 1)
  Call linea(LS + Ft / 2 + 1, DI / 2 - 2, LS + Ft / 2, DI / 2, vbRed, 1)
  Call TestoAll(LS + Ft, 0, "DI=" & DI, vbRed, True, 1)
  'Spina dx
  Call linea(LS + Ft, -RS, 2 * LS + Ft, -RS)
  Call linea(LS + Ft, RS, 2 * LS + Ft, RS)
  Call linea(2 * LS + Ft, -RS, 2 * LS + Ft, RS)
  'Contropunta
  Call linea(2 * LS + Ft, RS / 2, 3 * LS + Ft, RS, RGB(100, 100, 100))
  Call linea(2 * LS + Ft, -RS / 2, 3 * LS + Ft, -RS, RGB(100, 100, 100))
  Call linea(3 * LS + Ft, RS, 8 * LS + Ft, RS, RGB(100, 100, 100))
  Call linea(3 * LS + Ft, -RS, 8 * LS + Ft, -RS, RGB(100, 100, 100))
  'CONICITA'
  If (R93 <> 0) Then
    Dim YY0   As Double
    Dim Scala As Double
    '
    Scala = 500
    If (Abs(R93 * Scala) > Re) Then Scala = Re / Abs(R93)
    '
    YY0 = -Re - Re / 4 - Abs(R93 * Scala)
    Call QuotaV(LS, YY0 + R93 * Scala, LS, YY0, "" & R93, , vbBlack, 1)
    Call linea(LS, YY0, LS + Ft, YY0, vbBlack, 1, 1)
    Call linea(LS, YY0 + R93 * Scala, LS + Ft, YY0, vbRed, 1)
  End If
  '
  If Not NotScale Then
    Call QuotaH(LS, Re + 10, LS + F2, Re + 10, "" & F2, 12, , 3)
    Call QuotaH(LS + F2, Re + 10, LS + F2 + F1, Re + 10, "" & F1, 12, , 3)
  Else
    Call QuotaH(LS, Re + 10, LS + Ft, Re + 10, "?", 12, vbRed, 3)
  End If
  '
  PAR.MargineDxPic = 0
  PAR.MargineSxPic = 0

End Sub

Public Sub Disegna_ZONA_LAVORO_SCANALATI_INTERNI_CBN(Stampante As Boolean)
'
Dim nZone As Integer
Dim Ft    As Double
Dim F1    As Double
Dim F2    As Double
Dim F3    As Double
Dim F1_2  As Double
Dim F2_2  As Double
Dim F3_2  As Double
'
Dim RS              As Double
Dim LS              As Double
Dim DiaMola         As Double
Dim AngMola         As Double
Dim ExtracorsaIn    As Double
Dim ExtracorsaOut   As Double
Dim TrattoDivisione As Double
Dim tColore         As Long
Dim tX1             As Double
Dim tX              As Double
Dim tY              As Double
Dim i               As Integer
Dim NotScale        As Boolean
Dim QuotaIL         As Double
Dim QuotaFL         As Double
Dim XF0             As Double
Dim XF1             As Double
Dim XF2             As Double
Dim XF3             As Double
Dim XF0_2           As Double
Dim XF1_2           As Double
Dim XF2_2           As Double
Dim XF3_2           As Double
Dim tScala          As Double
Dim YQuota          As Double
Dim DiaMolaMin      As Double
Dim ALTEZZA_PROFILO As Double
Dim QZI             As Double
Dim SICUREZZA       As Double
'
  Call InizializzaVariabili
  '
  NotScale = False
  'Lettura parametri ingranaggio
  
  DiaMola = LEP("RA[0,1]") * 2
  ALTEZZA_PROFILO = (LEP("R[45]") - LEP("R[38]")) / 2
  DiaMolaMin = DiaMola
  If (DiaMolaMin <= 0) Then DiaMolaMin = LEP("DIAMOLA_G[0,2]")
  
  ExtracorsaIn = Lp("CORSAIN")   'lato testa
  ExtracorsaOut = Lp("CORSAOUT") 'lato contropunta
  TrattoDivisione = Lp("R[130]")
  QZI = Lp("QASSIALESTART_G")
  SICUREZZA = Lp("R[112]")
  '
  AngMola = 0
  nZone = 1
  
  
  
 
  Ft = Lp("FASCIATOTALE_G") 'Fascia Totale
  LS = 0 'Lunghezza spina sporgente
  If Ft = 0 Then
    NotScale = True
    F1 = 50
    F1_2 = 50
    Ft = 50
  End If
  '
  PAR.MaxX = 6 * LS + Ft
  PAR.MinX = -2 * LS
  PAR.MaxY = 0
  PAR.MinY = 0
  PAR.ScalaMargine = 1
  PAR.Allineamento = 0
  
  Call CalcolaParPic(PAR, True, Stampante)
  
  PAR.MargineDxPic = PAR.Pic.TextWidth("Z=888.888")
  PAR.MargineSxPic = PAR.Pic.TextWidth("Z=888.888")
  Dim XMola As Double
  Dim Ymola As Double
  
  PAR.MaxX = (2 * LS + Ft + ExtracorsaOut + TrattoDivisione + SICUREZZA)
  PAR.MinX = -ExtracorsaIn
  PAR.MaxY = 0
  PAR.MinY = 0
  PAR.ScalaMargine = 1
  PAR.Allineamento = 0
  Call CalcolaParPic(PAR, True, Stampante)
  '
  XMola = LS + Ft + ExtracorsaOut
  Ymola = 0
  '
  tScala = PAR.Scala
  If PAR.Pic Is Printer Then tScala = tScala * 45
  '
  QuotaFL = QZI - Ft - ExtracorsaIn
  QuotaIL = QZI + ExtracorsaOut
  '
  YQuota = 350 / tScala
  If (Lp("iMetDiv") = 2) Then
    XMola = XMola + TrattoDivisione
    Call QuotaH(XMola - TrattoDivisione, YQuota, XMola, YQuota, "" & TrattoDivisione, 12, , 3)
    QuotaIL = QZI + ExtracorsaOut + TrattoDivisione
  End If
  Call TestoAll(XMola - TrattoDivisione - ExtracorsaIn - ExtracorsaOut - Ft, YQuota, "Z=" & QuotaFL, , True, 1)
  '
  If (SICUREZZA > 0) Then Call QuotaH(XMola, YQuota, XMola + SICUREZZA, YQuota, "" & SICUREZZA, 12, , 3)
  '
  Dim X1 As Double
  Dim Y1 As Double
  '
  X1 = XMola - DiaMola / 2
  Y1 = -ALTEZZA_PROFILO - DiaMola / 2
  '
  Call TestoAll(XMola, Y1, "Z=" & QuotaIL, , True, 4)
  If (SICUREZZA > 0) Then Call linea(XMola + SICUREZZA, Y1, XMola + SICUREZZA, -Y1, vbRed, 1, vbDash)
  Call linea(XMola, Y1, XMola, -Y1, vbRed, 1, vbDash)
  Call Cerchio(XMola, Y1, DiaMola / 2, vbGreen, , 2)
  '
  Call linea(X1, Y1 + DiaMolaMin / 2, XMola + 20, Y1 + DiaMolaMin / 2, vbBlack, 1)
  Call linea(X1, Y1 - DiaMolaMin / 2, XMola + 20, Y1 - DiaMolaMin / 2, vbBlack, 1)
  Call linea(X1, Y1 + DiaMolaMin / 2, X1, Y1 - DiaMolaMin / 2, vbBlack, 1)
  '
  tX1 = ExtracorsaIn / 2
  tX1 = XMola - tX1
  'Zone
  XF0 = LS:   XF1 = XF0 + F1:       XF2 = XF1 + F2:        XF3 = XF2 + F3
  XF0_2 = LS: XF1_2 = XF0_2 + F1_2: XF2_2 = XF1_2 + F2_2:  XF3_2 = XF2_2 + F3_2
  Call linea(LS, 0, LS + Ft, 0, vbBlue, 2)
  Call linea(LS, -ALTEZZA_PROFILO, LS + Ft, -ALTEZZA_PROFILO, vbRed, 2)
  Call linea(LS, 0, LS, -50, vbBlack, 1)
  Call linea(LS + Ft, 0, LS + Ft, -50, vbBlack, 1)
  '
  YQuota = 0 + 350 / tScala
  Call QuotaH(LS + Ft, YQuota, LS + Ft + ExtracorsaOut, YQuota, "" & ExtracorsaOut, 12, , 3)
  Call QuotaH(LS - ExtracorsaIn, YQuota, LS, YQuota, "" & ExtracorsaIn, 12, , 3)
  If Not NotScale Then
    Call QuotaH(LS, YQuota, LS + Ft, YQuota, "" & Ft, 12, , 3)
  Else
    Call QuotaH(LS, YQuota, LS + Ft, YQuota, "?", 12, vbRed, 3)
  End If
  '
  PAR.MargineDxPic = 0
  PAR.MargineSxPic = 0
  If NotScale Then Call TestoInfo("DRAWING NOT IN SCALE ! ", , True, vbRed, True)
  '
  Call TestoInfo("Units: [mm]", 3)

End Sub
Public Sub Disegna_ZONA_LAVORO_SCANALATI_ESTERNI(Stampante As Boolean)
'
Dim nZone           As Integer
Dim DE              As Double
Dim DP              As Double
Dim Ft              As Double
Dim F1              As Double
Dim F2              As Double
Dim F3              As Double
Dim Re              As Double
Dim RS              As Double
Dim LS              As Double
Dim DiaMola         As Double
Dim LatoPart        As Integer
Dim ExtracorsaIn    As Double
Dim ExtracorsaOut   As Double
Dim TrattoDivisione As Double
Dim tColore         As Long
Dim tX1             As Double
Dim tX              As Double
Dim tY              As Double
Dim i               As Integer
Dim NotScale        As Boolean
Dim QuotaIL         As Double
Dim QuotaFL         As Double
Dim XF0             As Double
Dim XF1             As Double
Dim XF2             As Double
Dim XF3             As Double
Dim TipoPezzo       As String
Dim XMola           As Double
Dim Ymola           As Double
Dim tScala          As Double
Dim YQuota          As Double
Dim CODSCA          As String
Dim QZI             As Double
'
On Error Resume Next
  '
  Call InizializzaVariabili
  '
  NotScale = False
  'Lettura parametri vite
  nZone = Lp("iNzone")
  DE = Lp("R[44]")
  DP = (Lp("R[44]") + Lp("R[103]")) / 2
  QZI = Lp("QASSIALESTART_G")
  'CODSCA = Lp_Str("TIPOSCA")
  'If (CODSCA = "STD") Then
  '   TipoPezzo = LNP_Tabella1("TIPOSCA") & ": DIN"
  'Else
  '   TipoPezzo = LNP_Tabella1("TIPOSCA") & ": " & CODSCA
  'End If
  '
  DiaMola = 300 'lp("iDiaMola")
  LatoPart = Lp("R[12]")
  ExtracorsaIn = Lp("CORSAIN")
  ExtracorsaOut = Lp("CORSAOUT")
  TrattoDivisione = Lp("R[130]")
  '
  'Fascia Totale
  F1 = 0: F2 = 0: F3 = 0
  Select Case nZone
    Case 1
      F1 = Lp("Fascia1F1")
    Case 2
      F1 = Lp("Fascia1F1")
      F2 = Lp("Fascia2F1")
    Case 3
      F1 = Lp("Fascia1F1")
      F2 = Lp("Fascia2F1")
      F3 = Lp("Fascia3F1")
  End Select
  Ft = F1 + F2 + F3
  If (Ft = 0) Then
    NotScale = True
    F1 = 50
    Ft = 50
  End If
  '
  Re = DE / 2
  RS = Re / 4       'Raggio spina
  LS = RS           'Lunghezza spina sporgente
  '
  PAR.MaxX = 6 * LS + Ft
  PAR.MinX = -2 * LS
  PAR.MaxY = Re + 35
  PAR.MinY = 0
  PAR.Allineamento = 0
  Call CalcolaParPic(PAR, True, Stampante)
  '
  PAR.MargineDxPic = PAR.Pic.TextWidth("Z=888.888")
  PAR.MargineSxPic = PAR.Pic.TextWidth("Z=888.888")
  '
  Select Case LatoPart
    
    Case -1 'Destra
      PAR.MaxX = (2 * LS + Ft + ExtracorsaOut + TrattoDivisione)
      PAR.MinX = -ExtracorsaIn
      PAR.MaxY = 2 * Re
      PAR.MinY = 0
      PAR.ScalaMargine = 1
      PAR.Allineamento = 0
      Call CalcolaParPic(PAR, True, Stampante)
      XMola = LS + Ft + ExtracorsaOut
      Ymola = 0
      QuotaIL = QZI + Ft + ExtracorsaOut
      QuotaFL = QZI - ExtracorsaIn
      tScala = PAR.Scala
      If PAR.Pic Is Printer Then tScala = tScala * 45
      YQuota = Re + 350 / tScala
      If (Lp("iMetDiv") = 2) Then
        XMola = XMola + TrattoDivisione
        Call QuotaH(XMola - TrattoDivisione, YQuota, XMola, YQuota, "" & TrattoDivisione, 12, , 3)
        QuotaIL = QuotaIL + TrattoDivisione
      End If
      Call TestoAll(XMola + 5, Re + 10, "Z=" & QuotaIL, , True, 2)
      Call TestoAll(XMola - TrattoDivisione - ExtracorsaIn - ExtracorsaOut - Ft - 5, Re + 10, "Z=" & QuotaFL, , True, 1)
      
    Case 1 'Sinistra
      PAR.MaxX = (2 * LS + Ft + ExtracorsaOut)
      PAR.MinX = -ExtracorsaIn - TrattoDivisione
      PAR.MaxY = 2 * Re
      PAR.MinY = 0
      PAR.ScalaMargine = 1
      PAR.Allineamento = 0
      Call CalcolaParPic(PAR, True, Stampante)
      XMola = LS - ExtracorsaIn
      Ymola = 0
      QuotaIL = QZI - ExtracorsaIn
      QuotaFL = QZI + Ft + ExtracorsaOut
      tScala = PAR.Scala
      If PAR.Pic Is Printer Then tScala = tScala * 45
      YQuota = Re + 350 / tScala
      If (Lp("iMetDiv") = 2) Then
        XMola = XMola - TrattoDivisione
        QuotaH XMola, YQuota, XMola + TrattoDivisione, YQuota, "" & TrattoDivisione, 12, , 3
        QuotaIL = QuotaIL - TrattoDivisione
      End If
      Call TestoAll(XMola - 5, Re + 10, "Z=" & QuotaIL, , True, 1)
      Call TestoAll(XMola + TrattoDivisione + ExtracorsaIn + ExtracorsaOut + Ft + 5, Re + 10, "Z=" & QuotaFL, , True, 2)
      
  End Select
  'Centro mola
  Call linea(XMola, Re + 20 + DiaMola, XMola, Re, RGB(255, 200, 200), 1, vbDashDot)
  'Disegno della mola
  Dim SPE     As Double
  Dim spe1    As Double
  Dim spe2    As Double
  Dim Altezza As Double
  '
  SPE = Lp("SPMOLA_G[0,1]")
  SPE = SPE / 2
  Altezza = Lp("R[44]") - Lp("R[103]")
  Altezza = Altezza / 2
  spe1 = 1 / 2 * SPE
  spe2 = 1 / 4 * SPE
  '
  Call linea(XMola - SPE, Re + 30 + Altezza + DiaMola, XMola - SPE, Re + 30 + Altezza, RGB(0, 0, 0), 1, vbSolid)
  Call linea(XMola - SPE, Re + 30 + Altezza, XMola - spe1, Re + 30 + Altezza, RGB(0, 0, 0), 1, vbSolid)
  Call linea(XMola - spe1, Re + 30 + Altezza, XMola - spe2, Re + 30, RGB(0, 0, 0), 1, vbSolid)
  Call linea(XMola + SPE, Re + 30 + Altezza + DiaMola, XMola + SPE, Re + 30 + Altezza, RGB(0, 0, 0), 1, vbSolid)
  Call linea(XMola + SPE, Re + 30 + Altezza, XMola + spe1, Re + 30 + Altezza, RGB(0, 0, 0), 1, vbSolid)
  Call linea(XMola + spe1, Re + 30 + Altezza, XMola + spe2, Re + 30, RGB(0, 0, 0), 1, vbSolid)
  Call linea(XMola - spe2, Re + 30, XMola + spe2, Re + 30, RGB(0, 0, 0), 1, vbSolid)
  '
  If (LatoPart = -1) Then
    tX1 = ExtracorsaIn / 2
    tX1 = XMola - tX1
  Else
    tX1 = ExtracorsaOut / 2
    tX1 = XMola + tX1
  End If
  'Zone
  XF0 = LS
  XF1 = XF0 + F1
  XF2 = XF1 + F2
  XF3 = XF2 + F3
  'Spina sx
  Call linea(0, -RS, 0, RS)
  Call linea(0, -RS, LS, -RS)
  Call linea(0, RS, LS, RS)
  'Vite
  Call linea(LS, -Re, LS, Re, vbBlue, 2)
  Call linea(LS, -Re, LS + Ft, -Re, vbBlue, 2)
  Call linea(LS, Re, LS + Ft, Re, vbBlue, 2)
  Call linea(LS + Ft, -Re, LS + Ft, Re, vbBlue, 2)
  'primitivo
  'If (CODSCA <> "STD") Then
    Call linea(LS, -DP / 2, LS + Ft, -DP / 2, vbRed, 1)
    Call linea(LS, DP / 2, LS + Ft, DP / 2, vbRed, 1)
    Call linea(LS + Ft / 2, DP / 2, LS + Ft / 2, -DP / 2, vbRed, 1)
    Call linea(LS + Ft / 2 - 1, -DP / 2 + 2, LS + Ft / 2, -DP / 2, vbRed, 1)
    Call linea(LS + Ft / 2 + 1, -DP / 2 + 2, LS + Ft / 2, -DP / 2, vbRed, 1)
    Call linea(LS + Ft / 2 - 1, DP / 2 - 2, LS + Ft / 2, DP / 2, vbRed, 1)
    Call linea(LS + Ft / 2 + 1, DP / 2 - 2, LS + Ft / 2, DP / 2, vbRed, 1)
    Call TestoAll(LS + Ft / 2, 0, "Dm=" & DP, vbRed, True, 1)
  'End If
  'Spina dx
  Call linea(LS + Ft, -RS, 2 * LS + Ft, -RS)
  Call linea(LS + Ft, RS, 2 * LS + Ft, RS)
  Call linea(2 * LS + Ft, -RS, 2 * LS + Ft, RS)
  'Contropunta
  Call linea(2 * LS + Ft, RS / 2, 3 * LS + Ft, RS, RGB(100, 100, 100))
  Call linea(2 * LS + Ft, -RS / 2, 3 * LS + Ft, -RS, RGB(100, 100, 100))
  Call linea(3 * LS + Ft, RS, 8 * LS + Ft, RS, RGB(100, 100, 100))
  Call linea(3 * LS + Ft, -RS, 8 * LS + Ft, -RS, RGB(100, 100, 100))
  '
  Call QuotaH(LS + Ft, Re + 10, LS + Ft + ExtracorsaOut, Re + 10, "" & ExtracorsaOut, 12, , 3)
  Call QuotaH(LS - ExtracorsaIn, Re + 10, LS, Re + 10, "" & ExtracorsaIn, 12, , 3)
  '
  If Not NotScale Then
    Call QuotaH(LS, Re + 10, LS + Ft, Re + 10, "" & Ft, 12, , 3)
  Else
    Call QuotaH(LS, Re + 10, LS + Ft, Re + 10, "?", 12, vbRed, 3)
  End If
  '
  PAR.MargineDxPic = 0
  PAR.MargineSxPic = 0
  '
  'Call TestoInfo(TipoPezzo, 1, , vbBlue, True)

End Sub

Public Sub Disegna_Profilo_SCANALATO_DRITTO(Stampante As Boolean)
'
Dim DE        As Double
Dim DI        As Double
Dim Re        As Double
Dim RS        As Double
Dim LS        As Double
'
Dim tColore   As Long
Dim tX1       As Double
Dim tX        As Double
Dim tY        As Double
Dim i         As Integer
Dim NotScale  As Boolean
Dim SPE       As Double
Dim HHH       As Double
Dim NS        As Integer
'
On Error Resume Next
  '
  If Not ParametriOK Then Exit Sub
  '
  Call InizializzaVariabili
  '
  NotScale = False
  DE = Lp("R[44]")
  DI = Lp("R[103]")
  NS = Lp("R[5]")
  HHH = (DE - DI) / 2
  SPE = Lp("R[750]")
  '
  PAR.MaxX = DE: PAR.MinX = -DE
  PAR.MaxY = DE: PAR.MinY = -DE
  PAR.Allineamento = 0
  Call CalcolaParPic(PAR, True, Stampante)
  '
  PAR.MargineDxPic = PAR.Pic.TextWidth("Z=888.888")
  PAR.MargineSxPic = PAR.Pic.TextWidth("Z=888.888")
  '
  Call QuotaH(-SPE / 2, DE / 2 + HHH, SPE / 2, DE / 2 + HHH, "" & SPE, 12, , 3)
  Call QuotaH(-DI / 2, 0, DI / 2, 0, "" & DI, 12, , 3)
  '
  Call linea(DE / 2, 0, DE / 2, -DE / 2 - HHH, vbRed, 1, 1)
  Call linea(-DE / 2, 0, -DE / 2, -DE / 2 - HHH, vbRed, 1, 1)
  Call QuotaH(-DE / 2, -DE / 2 - HHH, DE / 2, -DE / 2 - HHH, "" & DE, vbRed, , 4)
  
  Dim ANGOLO As Double
  Dim A1     As Double
  Dim A2     As Double
  Dim X1     As Double
  Dim X2     As Double
  Dim Y1     As Double
  Dim Y2     As Double
  '
  A1 = ASIN(SPE / DE):  A2 = ASIN(SPE / DI)
  For i = 0 To NS - 1
    ANGOLO = 2 * PG / NS * i
    X1 = DI / 2 * Sin(ANGOLO + A2):  Y1 = DI / 2 * Cos(ANGOLO + A2)
    X2 = DE / 2 * Sin(ANGOLO + A1):  Y2 = DE / 2 * Cos(ANGOLO + A1)
    Call linea(X2, Y2, X1, Y1, vbBlack, 1, 0)
    X1 = DI / 2 * Sin(ANGOLO - A2):  Y1 = DI / 2 * Cos(ANGOLO - A2)
    X2 = DE / 2 * Sin(ANGOLO - A1):  Y2 = DE / 2 * Cos(ANGOLO - A1)
    Call linea(X2, Y2, X1, Y1, vbBlack, 1, 0)
    X2 = DE / 2 * Sin(ANGOLO + A1):  Y2 = DE / 2 * Cos(ANGOLO + A1)
    X1 = DE / 2 * Sin(ANGOLO - A1):  Y1 = DE / 2 * Cos(ANGOLO - A1)
    Call linea(X2, Y2, X1, Y1, vbBlack, 1, 0)
  Next i
  Call Cerchio(0, 0, DI / 2, vbRed, 1, 0, 0)
  '
  PAR.MargineDxPic = 0
  PAR.MargineSxPic = 0

End Sub

Public Sub Disegna_Profilo_SCANALATI_INTERNI_CBN(Stampante As Boolean)
'
Dim DE        As Double
Dim DI        As Double
Dim Re        As Double
Dim RS        As Double
Dim LS        As Double
'
Dim tColore   As Long
Dim tX1       As Double
Dim tX        As Double
Dim tY        As Double
Dim i         As Integer
Dim NotScale  As Boolean
Dim SPE       As Double
Dim HHH       As Double
Dim NS        As Integer
'
On Error Resume Next
  '
  If Not ParametriOK Then Exit Sub
  '
  Call InizializzaVariabili
  '
  NotScale = False
  DE = Lp("R[45]") 'Diametro Interno
  DI = Lp("R[38]") 'Diametro Esterno
  NS = Lp("R[5]")
  HHH = Abs(DE - DI) / 2
  SPE = Lp("SPESSOREALETTA_G")
  '
  PAR.MaxX = DE: PAR.MinX = -DE
  PAR.MaxY = DE: PAR.MinY = -DE
  PAR.Allineamento = 0
  Call CalcolaParPic(PAR, True, Stampante)
  '
  PAR.MargineDxPic = PAR.Pic.TextWidth("Z=888.888")
  PAR.MargineSxPic = PAR.Pic.TextWidth("Z=888.888")
  '
  Call QuotaH(-SPE / 2, DE / 2 + HHH, SPE / 2, DE / 2 + HHH, "" & SPE, 12, , 3)
  Call QuotaH(-DI / 2, 0, DI / 2, 0, "" & DI, 12, , 3)
  '
  Call linea(DE / 2, 0, DE / 2, -DE / 2 - HHH, vbRed, 1, 1)
  Call linea(-DE / 2, 0, -DE / 2, -DE / 2 - HHH, vbRed, 1, 1)
  Call QuotaH(-DE / 2, -DE / 2 - HHH, DE / 2, -DE / 2 - HHH, "" & DE, vbRed, , 4)
  
  Dim ANGOLO As Double
  Dim A1     As Double
  Dim A2     As Double
  Dim X1     As Double
  Dim X2     As Double
  Dim Y1     As Double
  Dim Y2     As Double
  Dim ArcoX1 As Double
  Dim ArcoX2 As Double
  Dim ArcoY1 As Double
  Dim ArcoY2 As Double
  '
  
  Dim XCentro As Double
  Dim YCentro As Double
  Dim Raggio As Double
  
  XCentro = PAR.Xc + (0 - PAR.SpostX) * PAR.Scala
  YCentro = PAR.YC + (0 - PAR.SpostY) * PAR.Scala
  Raggio = (DI / 2) * PAR.Scala
  
  
  A1 = ASIN(SPE / DE): A2 = ASIN(SPE / DI)
  For i = 0 To NS - 1
    ANGOLO = 2 * PG / NS * i
    X1 = DI / 2 * Sin(ANGOLO + A2):  Y1 = DI / 2 * Cos(ANGOLO + A2)
    X2 = DE / 2 * Sin(ANGOLO + A1):  Y2 = DE / 2 * Cos(ANGOLO + A1)
    
    Call linea(X2, Y2, X1, Y1, vbBlack, 1, 0)
    
    
    X1 = DI / 2 * Sin(ANGOLO - A2):  Y1 = DI / 2 * Cos(ANGOLO - A2)
    X2 = DE / 2 * Sin(ANGOLO - A1):  Y2 = DE / 2 * Cos(ANGOLO - A1)
    
    Call linea(X2, Y2, X1, Y1, vbBlack, 1, 0)
    
    
    X2 = DE / 2 * Sin(ANGOLO + A1):  Y2 = DE / 2 * Cos(ANGOLO + A1)
    X1 = DE / 2 * Sin(ANGOLO - A1):  Y1 = DE / 2 * Cos(ANGOLO - A1)
    
   
    
   
    
    Dim Angolo1 As Double
    Dim Angolo2 As Double

  Angolo1 = (2 * PI / NS) * i + A2 + PI / 2 '
  Angolo2 = (2 * PI / NS) * (i + 1) - A2 + PI / 2 '

  If Angolo1 > 2 * PI Then Angolo1 = Angolo1 - PI * 2
  If Angolo2 > 2 * PI Then Angolo2 = Angolo2 - PI * 2
  PAR.Pic.Circle (XCentro, YCentro), Raggio, vbBlack, Angolo1, Angolo2
  

  
  Next i

  
  
  Call Cerchio(0, 0, DE / 2, vbBlue, 1, 0, 0)
  

  
  
  '
  PAR.MargineDxPic = 0
  PAR.MargineSxPic = 0
End Sub

Public Sub Disegna_ZONA_LAVORO_VITI_CONICHE(Stampante As Boolean)
'
Dim DE        As Double
Dim DI        As Double
Dim Ft        As Double
Dim Re        As Double
Dim RS        As Double
Dim LS        As Double
Dim DiaMola   As Double
Dim LatoPart  As Integer
Dim tColore   As Long
Dim tX1       As Double
Dim tX        As Double
Dim tY        As Double
Dim i         As Integer
Dim NotScale  As Boolean
Dim QuotaIL   As Double
Dim QuotaFL   As Double
Dim XF0       As Double
Dim XMola     As Double
Dim Ymola     As Double
Dim tScala    As Double
Dim YQuota    As Double
Dim QZFORO    As Double
Dim DISTFORO  As Double
Dim QZI       As Double
Dim F1        As Double
Dim F2        As Double
Dim F3        As Double
Dim F4        As Double
Dim F5        As Double
Dim F6        As Double
Dim F7        As Double
Dim p1        As Double
Dim p3        As Double
Dim P5        As Double
Dim P7        As Double
Dim PASSO     As Double
Dim ELICA     As Integer
Dim Verso     As Integer
Dim N_Principi As Integer
Dim N_Filetti As Integer
'
On Error Resume Next
  '
  Exit Sub
  Call InizializzaVariabili
  '
  NotScale = False
  DE = Lp("R[189]")
  p1 = Lp("R[220]")
  p3 = Lp("R[221]")
  P5 = Lp("R[222]")
  P7 = Lp("R[223]")
  DI = DE - p1 * 2
  'SUMITOMO*******************
  QZFORO = Lp("POSZFORO_G[1]")
  DISTFORO = Lp("DISTFIXRIF")
  QZI = QZFORO - DISTFORO
  'QZI = Lp("R[190]")
  '***************************
  DiaMola = Lp("R[200]")
  ELICA = Lp("R[124]")
  If (ELICA > 0) Then
    Verso = 1
  Else
    Verso = -1
  End If
  N_Principi = Lp("R[5]")
  If (N_Principi <= 0) Then N_Principi = 1
  PASSO = Lp("R[128]")
  If (PASSO > 0) Then
    LatoPart = 1
  Else
    LatoPart = -1
  End If
  F1 = Lp("R[91]")
  F2 = Lp("R[92]")
  F3 = Lp("R[93]")
  F4 = Lp("R[94]")
  F5 = Lp("R[95]")
  F6 = Lp("R[96]")
  F7 = Lp("R[97]")
  Ft = F1 + F2 + F3 + F4 + F5 + F6 + F7
  If (Ft = 0) Then
    NotScale = True
    Ft = 50
  End If
  '
  Re = DE / 2
  RS = Re / 4       'Raggio spina
  LS = RS           'Lunghezza spina sporgente
  '
  PAR.MaxX = 6 * LS + Ft
  PAR.MinX = -3 * LS
  PAR.MaxY = 4 * Re + 35
  PAR.MinY = -4 * Re - 35
  PAR.Allineamento = 0
  Call CalcolaParPic(PAR, True, Stampante)
  '
  PAR.MargineDxPic = PAR.Pic.TextWidth("Z=888.888")
  PAR.MargineSxPic = PAR.Pic.TextWidth("Z=888.888")
  '
  Select Case LatoPart
    
    Case -1 'Destra
      PAR.MaxX = (2 * LS + Ft)
      PAR.MinX = 0
      PAR.MaxY = 2 * Re
      PAR.MinY = -2 * Re
      PAR.ScalaMargine = 1
      PAR.Allineamento = 0
      Call CalcolaParPic(PAR, True, Stampante)
      XMola = LS + Ft
      Ymola = 0
      QuotaIL = QZI + Ft
      QuotaFL = QZI
      tScala = PAR.Scala
      If PAR.Pic Is Printer Then tScala = tScala * 45
      YQuota = Re + 350 / tScala
      Call TestoAll(XMola + 5, Re + 10, "Z=" & QuotaIL, , True, 2)
      Call TestoAll(XMola - Ft - 5, Re + 10, "Z=" & QuotaFL, , True, 1)
      
    Case 1 'Sinistra
      PAR.MaxX = (2 * LS + Ft)
      PAR.MinX = 0
      PAR.MaxY = 2 * Re
      PAR.MinY = -2 * Re
      PAR.ScalaMargine = 1
      PAR.Allineamento = 0
      Call CalcolaParPic(PAR, True, Stampante)
      XMola = LS
      Ymola = 0
      QuotaIL = QZI
      QuotaFL = QZI + Ft
      tScala = PAR.Scala
      If PAR.Pic Is Printer Then tScala = tScala * 45
      YQuota = Re + 350 / tScala
      Call TestoAll(XMola - 5, Re + 10, "Z=" & QuotaIL, , True, 1)
      Call TestoAll(XMola + Ft + 5, Re + 10, "Z=" & QuotaFL, , True, 2)
      
  End Select
  'Centro mola
  Call linea(XMola, Re + 20 + DiaMola, XMola, Re, RGB(255, 200, 200), 1, vbDashDot)
  '
  'Disegno della mola
  Dim spe0    As Double
  Dim spe1    As Double
  Dim spe2    As Double
  Dim Altezza As Double
  Const OFF1 = 100
  '
  spe0 = Lp("R[37]")
  spe0 = spe0 / 2
  Altezza = p1
  spe2 = 1 / 1.5 * spe0
  spe1 = 1 / 1.1 * spe0
  '
  Call linea(XMola + spe1 - spe0, Re + OFF1 + Altezza + DiaMola, XMola + spe1 - spe0, Re + OFF1 + Altezza, RGB(0, 0, 0), 1, vbSolid)
  Call linea(XMola + spe1 - spe0, Re + OFF1 + Altezza, XMola + spe1 - spe1, Re + OFF1 + Altezza, RGB(0, 0, 0), 1, vbSolid)
  Call linea(XMola + spe1 - spe1, Re + OFF1 + Altezza, XMola + spe1 - spe2, Re + OFF1, RGB(0, 0, 0), 1, vbSolid)
  Call linea(XMola + spe1 + spe0, Re + OFF1 + Altezza + DiaMola, XMola + spe1 + spe0, Re + OFF1 + Altezza, RGB(0, 0, 0), 1, vbSolid)
  Call linea(XMola + spe1 + spe0, Re + OFF1 + Altezza, XMola + spe1 + spe1, Re + OFF1 + Altezza, RGB(0, 0, 0), 1, vbSolid)
  Call linea(XMola + spe1 + spe1, Re + OFF1 + Altezza, XMola + spe1 + spe2, Re + OFF1, RGB(0, 0, 0), 1, vbSolid)
  Call linea(XMola + spe1 - spe2, Re + OFF1, XMola + spe1 + spe2, Re + OFF1, RGB(0, 0, 0), 1, vbSolid)
  '
  tX1 = XMola
  'Zone
  XF0 = LS
  'Spina sx
  Call linea(0, -RS, 0, RS)
  Call linea(0, -RS, LS, -RS)
  Call linea(0, RS, LS, RS)
  'Vite
  Call linea(LS, -Re, LS, Re, vbBlue, 2)
  Call linea(LS, -Re, LS + Ft, -Re, vbBlue, 2)
  Call linea(LS, Re, LS + Ft, Re, vbBlue, 2)
  Call linea(LS + Ft, -Re, LS + Ft, Re, vbBlue, 2)
  N_Filetti = Ft / Abs((PASSO / N_Principi))
  For i = 1 To N_Filetti - 1
    Call linea(LS + (i - 1) * Abs((PASSO / N_Principi)), -Verso * Re, LS + i * Abs((PASSO / N_Principi)), Verso * Re)
  Next i
  'Spina dx
  Call linea(LS + Ft, -RS, 2 * LS + Ft, -RS)
  Call linea(LS + Ft, RS, 2 * LS + Ft, RS)
  Call linea(2 * LS + Ft, -RS, 2 * LS + Ft, RS)
  'Contropunta
  Call linea(2 * LS + Ft, RS / 2, 3 * LS + Ft, RS, RGB(100, 100, 100))
  Call linea(2 * LS + Ft, -RS / 2, 3 * LS + Ft, -RS, RGB(100, 100, 100))
  Call linea(3 * LS + Ft, RS, 8 * LS + Ft, RS, RGB(100, 100, 100))
  Call linea(3 * LS + Ft, -RS, 8 * LS + Ft, -RS, RGB(100, 100, 100))
  '
  Const Fattore = 10
  Dim INIZIO As Double
  INIZIO = 2 * DE + 50
  If Not NotScale Then
    Call TestoAll(LS, Re - INIZIO, "DE=" & DE, , True, 4)
      If (F6 > 0) And (F7 > 0) Then
      Call QuotaH(LS, Re + 10, LS + F1, Re + 50, "" & F1, 12, , 3)
      Call QuotaH(LS + F1, Re + 10, LS + F1 + F2, Re + 50, "" & F2, 12, , 3)
      Call QuotaH(LS + F1 + F2, Re + 10, LS + F1 + F2 + F3, Re + 50, "" & F3, 12, , 3)
      Call QuotaH(LS + F1 + F2 + F3, Re + 10, LS + F1 + F2 + F3 + F4, Re + 50, "" & F4, 12, , 3)
      Call QuotaH(LS + F1 + F2 + F3 + F4, Re + 10, LS + F1 + F2 + F3 + F4 + F5, Re + 50, "" & F5, 12, , 3)
      Call QuotaH(LS + F1 + F2 + F3 + F4 + F5, Re + 10, LS + F1 + F2 + F3 + F4 + F5 + F6, Re + 50, "" & F6, 12, , 3)
      Call QuotaH(LS + F1 + F2 + F3 + F4 + F5 + F6, Re + 10, LS + F1 + F2 + F3 + F4 + F5 + F6 + F7, Re + 50, "" & F7, 12, , 3)
      '
      Call QuotaV(LS, Re - INIZIO, LS, Re - INIZIO + p1 * Fattore, "" & p1, 12, , 5)
      Call QuotaV(LS + F1 + F2 + F3, Re - INIZIO, LS + F1 + F2 + F3, Re - INIZIO + p3 * Fattore, "" & p3, 12, , 5)
      Call QuotaV(LS + F1 + F2 + F3 + F4 + F5, Re - INIZIO, LS + F1 + F2 + F3 + F4 + F5, Re - INIZIO + P5 * Fattore, "" & P5, 12, , 5)
      Call QuotaV(LS + F1 + F2 + F3 + F4 + F5 + F6 + F7, Re - INIZIO, LS + F1 + F2 + F3 + F4 + F5 + F6 + F7, Re - INIZIO + P7 * Fattore, "" & P7, 12, , 5)
      '
      Call linea(LS, 0, LS + F1 + F2 + F3 + F4 + F5 + F6 + F7, 0, vbRed, 1) 'ASSE PEZZO
      Call QuotaV(LS + F1 / 2, 0, LS + F1 / 2, Re - INIZIO + p1 * Fattore, "" & (DE - 2 * p1), 12, , 5)
      Call QuotaV(LS + F1 + F2 + F3 / 2, 0, LS + F1 + F2 + F3 / 2, Re - INIZIO + p3 * Fattore, "" & (DE - 2 * p3), 12, , 5)
      Call QuotaV(LS + F1 + F2 + F3 + F4 + F5 / 2, 0, LS + F1 + F2 + F3 + F4 + F5 / 2, Re - INIZIO + P5 * Fattore, "" & (DE - 2 * P5), 12, , 5)
      Call QuotaV(LS + F1 + F2 + F3 + F4 + F5 + F6 + F7 / 2, 0, LS + F1 + F2 + F3 + F4 + F5 + F6 + F7 / 2, Re - INIZIO + P7 * Fattore, "" & (DE - 2 * P7), 12, , 5)
      '
      Call linea(LS, Re - INIZIO, LS + F1 + F2 + F3 + F4 + F5 + F6 + F7, Re - INIZIO, vbRed, 2)
      Call linea(LS + F1, Re - INIZIO, LS + F1, Re - INIZIO + p1 * Fattore, vbRed, 1)
      Call linea(LS, Re - INIZIO + p1 * Fattore, LS + F1, Re - INIZIO + p1 * Fattore, vbRed, 1)
      Call linea(LS + F1, Re - INIZIO + p1 * Fattore, LS + F1 + F2, Re - INIZIO + p3 * Fattore, vbRed, 1)
      Call linea(LS + F1 + F2, Re - INIZIO, LS + F1 + F2, Re - INIZIO + p3 * Fattore, vbRed, 1)
      Call linea(LS + F1 + F2, Re - INIZIO + p3 * Fattore, LS + F1 + F2 + F3, Re - INIZIO + p3 * Fattore, vbRed, 1)
      Call linea(LS + F1 + F2 + F3, Re - INIZIO + p3 * Fattore, LS + F1 + F2 + F3 + F4, Re - INIZIO + P5 * Fattore, vbRed, 1)
      Call linea(LS + F1 + F2 + F3 + F4, Re - INIZIO, LS + F1 + F2 + F3 + F4, Re - INIZIO + P5 * Fattore, vbRed, 1)
      Call linea(LS + F1 + F2 + F3 + F4, Re - INIZIO + P5 * Fattore, LS + F1 + F2 + F3 + F4 + F5, Re - INIZIO + P5 * Fattore, vbRed, 1)
      Call linea(LS + F1 + F2 + F3 + F4 + F5, Re - INIZIO + P5 * Fattore, LS + F1 + F2 + F3 + F4 + F5 + F6, Re - INIZIO + P7 * Fattore, vbRed, 1)
      Call linea(LS + F1 + F2 + F3 + F4 + F5 + F6, Re - INIZIO, LS + F1 + F2 + F3 + F4 + F5 + F6, Re - INIZIO + P7 * Fattore, vbRed, 1)
      Call linea(LS + F1 + F2 + F3 + F4 + F5 + F6, Re - INIZIO + P7 * Fattore, LS + F1 + F2 + F3 + F4 + F5 + F6 + F7, Re - INIZIO + P7 * Fattore, vbRed, 1)
      '
    ElseIf (F4 > 0) And (F5 > 0) Then
      Call QuotaH(LS, Re + 10, LS + F1, Re + 50, "" & F1, 12, , 3)
      Call QuotaH(LS + F1, Re + 10, LS + F1 + F2, Re + 50, "" & F2, 12, , 3)
      Call QuotaH(LS + F1 + F2, Re + 10, LS + F1 + F2 + F3, Re + 50, "" & F3, 12, , 3)
      Call QuotaH(LS + F1 + F2 + F3, Re + 10, LS + F1 + F2 + F3 + F4, Re + 50, "" & F4, 12, , 3)
      Call QuotaH(LS + F1 + F2 + F3 + F4, Re + 10, LS + F1 + F2 + F3 + F4 + F5, Re + 50, "" & F5, 12, , 3)
      '
      Call QuotaV(LS, Re - INIZIO, LS, Re - INIZIO + p1 * Fattore, "" & p1, 12, , 5)
      Call QuotaV(LS + F1 + F2 + F3, Re - INIZIO, LS + F1 + F2 + F3, Re - INIZIO + p3 * Fattore, "" & p3, 12, , 5)
      Call QuotaV(LS + F1 + F2 + F3 + F4 + F5, Re - INIZIO, LS + F1 + F2 + F3 + F4 + F5, Re - INIZIO + P5 * Fattore, "" & P5, 12, , 5)
      '
      Call linea(LS, 0, LS + F1 + F2 + F3 + F4 + F5, 0, vbRed, 1) 'ASSE PEZZO
      Call QuotaV(LS + F1 / 2, 0, LS + F1 / 2, Re - INIZIO + p1 * Fattore, "" & (DE - 2 * p1), 12, , 5)
      Call QuotaV(LS + F1 + F2 + F3 / 2, 0, LS + F1 + F2 + F3 / 2, Re - INIZIO + p3 * Fattore, "" & (DE - 2 * p3), 12, , 5)
      Call QuotaV(LS + F1 + F2 + F3 + F4 + F5 / 2, 0, LS + F1 + F2 + F3 + F4 + F5 / 2, Re - INIZIO + P5 * Fattore, "" & (DE - 2 * P5), 12, , 5)
      '
      Call linea(LS, Re - INIZIO, LS + F1 + F2 + F3 + F4 + F5, Re - INIZIO, vbRed, 2)
      Call linea(LS + F1, Re - INIZIO, LS + F1, Re - INIZIO + p1 * Fattore, vbRed, 1)
      Call linea(LS, Re - INIZIO + p1 * Fattore, LS + F1, Re - INIZIO + p1 * Fattore, vbRed, 1)
      Call linea(LS + F1, Re - INIZIO + p1 * Fattore, LS + F1 + F2, Re - INIZIO + p3 * Fattore, vbRed, 1)
      Call linea(LS + F1 + F2, Re - INIZIO, LS + F1 + F2, Re - INIZIO + p3 * Fattore, vbRed, 1)
      Call linea(LS + F1 + F2, Re - INIZIO + p3 * Fattore, LS + F1 + F2 + F3, Re - INIZIO + p3 * Fattore, vbRed, 1)
      Call linea(LS + F1 + F2 + F3, Re - INIZIO + p3 * Fattore, LS + F1 + F2 + F3 + F4, Re - INIZIO + P5 * Fattore, vbRed, 1)
      Call linea(LS + F1 + F2 + F3 + F4, Re - INIZIO, LS + F1 + F2 + F3 + F4, Re - INIZIO + P5 * Fattore, vbRed, 1)
      Call linea(LS + F1 + F2 + F3 + F4, Re - INIZIO + P5 * Fattore, LS + F1 + F2 + F3 + F4 + F5, Re - INIZIO + P5 * Fattore, vbRed, 1)
      '
    ElseIf (F1 > 0) And (F2 > 0) And (F3 > 0) Then
      Call QuotaH(LS, Re + 10, LS + F1, Re + 50, "" & F1, 12, , 3)
      Call QuotaH(LS + F1, Re + 10, LS + F1 + F2, Re + 50, "" & F2, 12, , 3)
      Call QuotaH(LS + F1 + F2, Re + 10, LS + F1 + F2 + F3, Re + 50, "" & F3, 12, , 3)
      '
      Call QuotaV(LS, Re - INIZIO, LS, Re - INIZIO + p1 * Fattore, "" & p1, 12, , 5)
      Call QuotaV(LS + F1 + F2 + F3, Re - INIZIO, LS + F1 + F2 + F3, Re - INIZIO + p3 * Fattore, "" & p3, 12, , 5)
      '
      Call linea(LS, 0, LS + F1 + F2 + F3, 0, vbRed, 1) 'ASSE PEZZO
      Call QuotaV(LS + F1 / 2, 0, LS + F1 / 2, Re - INIZIO + p1 * Fattore, "" & (DE - 2 * p1), 12, , 5)
      Call QuotaV(LS + F1 + F2 + F3 / 2, 0, LS + F1 + F2 + F3 / 2, Re - INIZIO + p3 * Fattore, "" & (DE - 2 * p3), 12, , 5)
      '
      Call linea(LS, Re - INIZIO, LS + F1 + F2 + F3, Re - INIZIO, vbRed, 2)
      Call linea(LS + F1, Re - INIZIO, LS + F1, Re - INIZIO + p1 * Fattore, vbRed, 1)
      Call linea(LS, Re - INIZIO + p1 * Fattore, LS + F1, Re - INIZIO + p1 * Fattore, vbRed, 1)
      Call linea(LS + F1, Re - INIZIO + p1 * Fattore, LS + F1 + F2, Re - INIZIO + p3 * Fattore, vbRed, 1)
      Call linea(LS + F1 + F2, Re - INIZIO, LS + F1 + F2, Re - INIZIO + p3 * Fattore, vbRed, 1)
      Call linea(LS + F1 + F2, Re - INIZIO + p3 * Fattore, LS + F1 + F2 + F3, Re - INIZIO + p3 * Fattore, vbRed, 1)
      '
    Else
      Call QuotaH(LS, Re + 10, LS + Ft, Re + 50, "" & Ft, 12, , 3)
    End If
  Else
    Call QuotaH(LS, Re + 10, LS + Ft, Re + 50, "?", 12, vbRed, 3)
  End If
  '
  PAR.MargineDxPic = 0
  PAR.MargineSxPic = 0

End Sub

Public Sub Disegna_ZONA_LAVORO_VITI_PASSO_VARIABILE(Stampante As Boolean)
'
Dim DE        As Double
Dim DI        As Double
Dim Ft        As Double
Dim Re        As Double
Dim RS        As Double
Dim LS        As Double
Dim DiaMola   As Double
Dim LatoPart  As Integer
Dim tColore   As Long
Dim tX1       As Double
Dim tX        As Double
Dim tY        As Double
Dim i         As Integer
Dim NotScale  As Boolean
Dim QuotaIL   As Double
Dim QuotaFL   As Double
Dim XF0       As Double
Dim XMola     As Double
Dim Ymola     As Double
Dim tScala    As Double
Dim YQuota    As Double
Dim QZFORO    As Double
Dim DISTFORO  As Double
Dim QZI       As Double
Dim F1        As Double
Dim F2        As Double
Dim F3        As Double
Dim p1        As Double
Dim p3        As Double
Dim Passo1    As Double
Dim Passo3    As Double
Dim Verso     As Integer
Dim N_Filetti1 As Integer
Dim N_Filetti3 As Integer
'
On Error Resume Next
  '
  Exit Sub
  Call InizializzaVariabili
  '
  NotScale = False
  DE = Lp("R[189]")
  p1 = Lp("R[113]")
  p3 = Lp("R[114]")
  DI = DE - p1 * 2
  'SUMITOMO*******************
  QZFORO = Lp("POSZFORO_G[2]")
  DISTFORO = Lp("DISTFIXRIF")
  QZI = QZFORO - DISTFORO
  'QZI = Lp("R[190]")
  '***************************
  DiaMola = Lp("R[200]")
  'LatoPart = Lp("R[17]")
  Passo1 = Lp("R[214]")
  Passo3 = Lp("R[215]")
  Verso = 1
  'If (Passo1 >= Passo3) Then
  '  LatoPart = -1
  'Else
    LatoPart = 1
  'End If
  F1 = Lp("R[210]")
  F2 = Lp("R[211]")
  F3 = Lp("R[212]")
  Ft = F1 + F2 + F3
  If (Ft = 0) Then
    NotScale = True
    Ft = 50
  End If
  '
  Re = DE / 2
  RS = Re / 4       'Raggio spina
  LS = RS           'Lunghezza spina sporgente
  '
  PAR.MaxX = 6 * LS + Ft
  PAR.MinX = -2 * LS
  PAR.MaxY = Re + 35
  PAR.MinY = 0
  PAR.Allineamento = 0
  Call CalcolaParPic(PAR, True, Stampante)
  '
  PAR.MargineDxPic = PAR.Pic.TextWidth("Z=888.888")
  PAR.MargineSxPic = PAR.Pic.TextWidth("Z=888.888")
  '
  Select Case LatoPart
    
    Case -1 'Destra
      PAR.MaxX = (2 * LS + Ft)
      PAR.MinX = 0
      PAR.MaxY = 2 * Re
      PAR.MinY = 0
      PAR.ScalaMargine = 1
      PAR.Allineamento = 0
      Call CalcolaParPic(PAR, True, Stampante)
      XMola = LS + Ft
      Ymola = 0
      QuotaIL = QZI + Ft
      QuotaFL = QZI
      tScala = PAR.Scala
      If PAR.Pic Is Printer Then tScala = tScala * 45
      YQuota = Re + 350 / tScala
      Call TestoAll(XMola + 5, Re + 10, "Z=" & QuotaIL, , True, 2)
      Call TestoAll(XMola - Ft - 5, Re + 10, "Z=" & QuotaFL, , True, 1)
      
    Case 1 'Sinistra
      PAR.MaxX = (2 * LS + Ft)
      PAR.MinX = 0
      PAR.MaxY = 2 * Re
      PAR.MinY = 0
      PAR.ScalaMargine = 1
      PAR.Allineamento = 0
      Call CalcolaParPic(PAR, True, Stampante)
      XMola = LS
      Ymola = 0
      QuotaIL = QZI
      QuotaFL = QZI + Ft
      tScala = PAR.Scala
      If PAR.Pic Is Printer Then tScala = tScala * 45
      YQuota = Re + 350 / tScala
      Call TestoAll(XMola - 5, Re + 10, "Z=" & QuotaIL, , True, 1)
      Call TestoAll(XMola + Ft + 5, Re + 10, "Z=" & QuotaFL, , True, 2)
      
  End Select
  'Centro mola
  Call linea(XMola, Re + 20 + DiaMola, XMola, Re, RGB(255, 200, 200), 1, vbDashDot)
  '
  'Disegno della mola
  Dim spe0    As Double
  Dim spe1    As Double
  Dim spe2    As Double
  Dim Altezza As Double
  Const OFF1 = 100
  '
  spe0 = Lp("R[37]")
  spe0 = spe0 / 2
  Altezza = p1
  spe2 = 1 / 1.5 * spe0
  spe1 = 1 / 1.1 * spe0
  '
  Call linea(XMola + spe1 - spe0, Re + OFF1 + Altezza + DiaMola, XMola + spe1 - spe0, Re + OFF1 + Altezza, RGB(0, 0, 0), 1, vbSolid)
  Call linea(XMola + spe1 - spe0, Re + OFF1 + Altezza, XMola + spe1 - spe1, Re + OFF1 + Altezza, RGB(0, 0, 0), 1, vbSolid)
  Call linea(XMola + spe1 - spe1, Re + OFF1 + Altezza, XMola + spe1 - spe2, Re + OFF1, RGB(0, 0, 0), 1, vbSolid)
  Call linea(XMola + spe1 + spe0, Re + OFF1 + Altezza + DiaMola, XMola + spe1 + spe0, Re + OFF1 + Altezza, RGB(0, 0, 0), 1, vbSolid)
  Call linea(XMola + spe1 + spe0, Re + OFF1 + Altezza, XMola + spe1 + spe1, Re + OFF1 + Altezza, RGB(0, 0, 0), 1, vbSolid)
  Call linea(XMola + spe1 + spe1, Re + OFF1 + Altezza, XMola + spe1 + spe2, Re + OFF1, RGB(0, 0, 0), 1, vbSolid)
  Call linea(XMola + spe1 - spe2, Re + OFF1, XMola + spe1 + spe2, Re + OFF1, RGB(0, 0, 0), 1, vbSolid)
  '
  tX1 = XMola
  'Zone
  XF0 = LS
  'Spina sx
  Call linea(0, -RS, 0, RS)
  Call linea(0, -RS, LS, -RS)
  Call linea(0, RS, LS, RS)
  'Vite
  Call linea(LS, -Re, LS, Re, vbBlue, 2)
  Call linea(LS, -Re, LS + Ft, -Re, vbBlue, 2)
  Call linea(LS, Re, LS + Ft, Re, vbBlue, 2)
  Call linea(LS + Ft, -Re, LS + Ft, Re, vbBlue, 2)
  N_Filetti1 = F1 / Abs(Passo1)
  For i = 1 To N_Filetti1
    Call linea(LS + (i - 1) * Abs(Passo1), -Verso * Re, LS + i * Abs(Passo1), Verso * Re)
  Next i
  N_Filetti3 = F3 / Abs(Passo3)
  For i = 1 To N_Filetti3
    Call linea(LS + F1 + F2 + (i - 1) * Abs(Passo3), -Verso * Re, LS + F1 + F2 + i * Abs(Passo3), Verso * Re)
  Next i
  'Spina dx
  Call linea(LS + Ft, -RS, 2 * LS + Ft, -RS)
  Call linea(LS + Ft, RS, 2 * LS + Ft, RS)
  Call linea(2 * LS + Ft, -RS, 2 * LS + Ft, RS)
  'Contropunta
  Call linea(2 * LS + Ft, RS / 2, 3 * LS + Ft, RS, RGB(100, 100, 100))
  Call linea(2 * LS + Ft, -RS / 2, 3 * LS + Ft, -RS, RGB(100, 100, 100))
  Call linea(3 * LS + Ft, RS, 8 * LS + Ft, RS, RGB(100, 100, 100))
  Call linea(3 * LS + Ft, -RS, 8 * LS + Ft, -RS, RGB(100, 100, 100))
  '
  Const Fattore = 30
  Const INIZIO = 200
  If Not NotScale Then
    Call TestoAll(LS, Re - INIZIO, "DE=" & DE, , True, 1)
      If (F1 > 0) And (F2 > 0) And (F3 > 0) Then
      Call QuotaH(LS, Re + 10, LS + F1, Re + 50, "" & F1, 12, , 3)
      Call QuotaH(LS + F1, Re + 10, LS + F1 + F2, Re + 50, "" & F2, 12, , 3)
      Call QuotaH(LS + F1 + F2, Re + 10, LS + F1 + F2 + F3, Re + 50, "" & F3, 12, , 3)
      '
      Call QuotaV(LS, Re - INIZIO, LS, Re - INIZIO + p1 * Fattore, "" & p1, 12, , 5)
      Call QuotaV(LS + F1 + F2 + F3, Re - INIZIO, LS + F1 + F2 + F3, Re - INIZIO + p3 * Fattore, "" & p3, 12, , 5)
      '
      Call linea(LS, Re - INIZIO, LS + F1 + F2 + F3, Re - INIZIO, vbRed, 2)
      Call linea(LS + F1, Re - INIZIO, LS + F1, Re - INIZIO + p1 * Fattore, vbRed, 1)
      Call linea(LS, Re - INIZIO + p1 * Fattore, LS + F1, Re - INIZIO + p1 * Fattore, vbRed, 1)
      Call linea(LS + F1, Re - INIZIO + p1 * Fattore, LS + F1 + F2, Re - INIZIO + p3 * Fattore, vbRed, 1)
      Call linea(LS + F1 + F2, Re - INIZIO, LS + F1 + F2, Re - INIZIO + p3 * Fattore, vbRed, 1)
      Call linea(LS + F1 + F2, Re - INIZIO + p3 * Fattore, LS + F1 + F2 + F3, Re - INIZIO + p3 * Fattore, vbRed, 1)
      '
    Else
      Call QuotaH(LS, Re + 10, LS + Ft, Re + 50, "" & Ft, 12, , 3)
    End If
  Else
    Call QuotaH(LS, Re + 10, LS + Ft, Re + 50, "?", 12, vbRed, 3)
  End If
  '
  PAR.MargineDxPic = 0
  PAR.MargineSxPic = 0

End Sub

Public Sub Disegna_ZONA_LAVORO_BS5L(Stampante As Boolean)
'
Dim DE        As Double
Dim DI        As Double
Dim Ft        As Double
Dim Re        As Double
Dim RS        As Double
Dim LS        As Double
Dim DiaMola   As Double
Dim LatoPart  As Integer
Dim tColore   As Long
Dim tX1       As Double
Dim tX        As Double
Dim tY        As Double
Dim i         As Integer
Dim NotScale  As Boolean
Dim QuotaIL   As Double
Dim QuotaFL   As Double
Dim XF0       As Double
Dim XMola     As Double
Dim Ymola     As Double
Dim tScala    As Double
Dim YQuota    As Double
Dim QZI       As Double

Dim F1        As Double
Dim F2        As Double
Dim F3        As Double
Dim F4        As Double
Dim F5        As Double
Dim F6        As Double
Dim F7        As Double
Dim F8        As Double
Dim F9        As Double
'
Dim p1_LA     As Double
Dim p2_LA     As Double
Dim P3_LA     As Double
Dim P4_LA     As Double
'
Dim p1_LB     As Double
Dim p2_LB     As Double
Dim P3_LB     As Double
Dim P4_LB     As Double
'
Dim Passo1     As Double
Dim Passo2     As Double
Dim Passo3     As Double
'
Dim ELICA     As Integer
Dim Verso     As Integer
Dim N_Filetti As Integer
'
On Error Resume Next
  '
  Call InizializzaVariabili
  '
  NotScale = False
  DE = Lp("R[189]")
  '
  p1_LA = Lp("R[420]")
  p2_LA = Lp("R[421]")
  P3_LA = Lp("R[422]")
  P4_LA = Lp("R[423]")
  '
  p1_LB = Lp("R[220]")
  p2_LB = Lp("R[221]")
  P3_LB = Lp("R[222]")
  P4_LB = Lp("R[223]")
  
  DI = DE - p1_LA * 2
  
  QZI = Lp("R[190]")
  
  DiaMola = Lp("R[200]")
  
  ELICA = Lp("R[124]")
  If (ELICA > 0) Then
    Verso = 1
  Else
    Verso = -1
  End If
  
  Passo1 = Lp("R[128]")
  Passo2 = Lp("R[132]")
  Passo3 = Lp("R[133]")
  
  LatoPart = -1
  
  F1 = Lp("R[91]")
  F2 = Lp("R[92]")
  F3 = Lp("R[93]")
  F4 = Lp("R[94]")
  F5 = Lp("R[95]")
  F6 = Lp("R[96]")
  F7 = Lp("R[97]")
  F7 = F7 - Passo2
  F8 = Passo2
  F9 = Lp("R[98]")
  Ft = F1 + F2 + F3 + F4 + F5 + F6 + F7 + F8 + F9
  If (Ft = 0) Then
    NotScale = True
    Ft = 50
  End If
  '
  Re = DE / 2
  RS = Re / 4       'Raggio spina
  LS = RS           'Lunghezza spina sporgente
  '
  PAR.MaxX = 6 * LS + Ft
  PAR.MinX = -2 * LS
  PAR.MaxY = Re + 35
  PAR.MinY = 0
  PAR.Allineamento = 0
  Call CalcolaParPic(PAR, True, Stampante)
  '
  PAR.MargineDxPic = PAR.Pic.TextWidth("Z=888.888")
  PAR.MargineSxPic = PAR.Pic.TextWidth("Z=888.888")
  '
  'Sinistra
  PAR.MaxX = (2 * LS + Ft)
  PAR.MinX = 0
  PAR.MaxY = 2 * Re
  PAR.MinY = -2 * Re
  PAR.ScalaMargine = 1
  PAR.Allineamento = 0
  Call CalcolaParPic(PAR, True, Stampante)
  
  XMola = LS
  Ymola = 0
  QuotaIL = QZI
  QuotaFL = QZI + Ft
  tScala = PAR.Scala
  'If PAR.Pic Is Printer Then tScala = tScala * 45
  YQuota = Re + 350 / tScala
  Call TestoAll(XMola - 5, Re + 10, "Z=" & QuotaIL, , True, 1)
  Call TestoAll(XMola + Ft + 5, Re + 10, "Z=" & QuotaFL, , True, 2)
  '
  'Centro mola
  Call linea(XMola, Re + 20 + DiaMola, XMola, Re, RGB(255, 200, 200), 1, vbDashDot)
  '
  'Disegno della mola
  Dim spe0    As Double
  Dim spe1    As Double
  Dim spe2    As Double
  Dim Altezza As Double
  Const OFF1 = 100
  '
  spe0 = Lp("R[37]")
  spe0 = spe0 / 2
  Altezza = p1_LA
  spe2 = 1 / 1.5 * spe0
  spe1 = 1 / 1.1 * spe0
  '
  Call linea(XMola - spe0, Re + OFF1 + Altezza + DiaMola, XMola - spe0, Re + OFF1 + Altezza, RGB(0, 0, 0), 1, vbSolid)
  Call linea(XMola - spe0, Re + OFF1 + Altezza, XMola - spe1, Re + OFF1 + Altezza, RGB(0, 0, 0), 1, vbSolid)
  Call linea(XMola - spe1, Re + OFF1 + Altezza, XMola - spe2, Re + OFF1, RGB(0, 0, 0), 1, vbSolid)
  Call linea(XMola + spe0, Re + OFF1 + Altezza + DiaMola, XMola + spe0, Re + OFF1 + Altezza, RGB(0, 0, 0), 1, vbSolid)
  Call linea(XMola + spe0, Re + OFF1 + Altezza, XMola + spe1, Re + OFF1 + Altezza, RGB(0, 0, 0), 1, vbSolid)
  Call linea(XMola + spe1, Re + OFF1 + Altezza, XMola + spe2, Re + OFF1, RGB(0, 0, 0), 1, vbSolid)
  Call linea(XMola - spe2, Re + OFF1, XMola + spe2, Re + OFF1, RGB(0, 0, 0), 1, vbSolid)
  '
  tX1 = XMola
  'Zone
  XF0 = LS
  'Spina sx
  Call linea(0, -RS, 0, RS)
  Call linea(0, -RS, LS, -RS)
  Call linea(0, RS, LS, RS)
  '
  'Vite
  Call linea(LS, -Re, LS, Re, vbBlue, 2)
  Call linea(LS, -Re, LS + Ft, -Re, vbBlue, 2)
  Call linea(LS, Re, LS + Ft, Re, vbBlue, 2)
  Call linea(LS + Ft, -Re, LS + Ft, Re, vbBlue, 2)
  N_Filetti = F1 / Abs(Passo1)
  For i = 1 To N_Filetti
    Call linea(LS + (i - 1) * Abs(Passo1), -Verso * Re, LS + i * Abs(Passo1), Verso * Re, vbBlue, 2)
  Next i
  N_Filetti = (Ft - F1) / Abs(Passo2)
  For i = 1 To N_Filetti
    Call linea(LS + (i - 1) * Abs(Passo2) + F1, -Verso * Re, LS + i * Abs(Passo2) + F1, Verso * Re, vbBlue, 2)
  Next i
  N_Filetti = (F2 + F3 + F4 + F5 + F6) / Abs(Passo3)
  For i = 1 To N_Filetti
    Call linea(LS + (i - 1) * Abs(Passo3) + F1, -Verso * Re, LS + i * Abs(Passo3) + F1, Verso * Re, vbRed, 2)
  Next i
  Call QuotaH(LS, Re + 10, LS + F1, Re + 50, "" & F1, 12, , 3)
  Call QuotaH(LS + F1, Re + 10, LS + F1 + F2, Re + 50, "" & F2, 12, , 3)
  Call QuotaH(LS + F1 + F2, Re + 10, LS + F1 + F2 + F3, Re + 50, "" & F3, 12, , 3)
  Call QuotaH(LS + F1 + F2 + F3, Re + 10, LS + F1 + F2 + F3 + F4, Re + 50, "" & F4, 12, , 3)
  Call QuotaH(LS + F1 + F2 + F3 + F4, Re + 10, LS + F1 + F2 + F3 + F4 + F5, Re + 50, "" & F5, 12, , 3)
  Call QuotaH(LS + F1 + F2 + F3 + F4 + F5, Re + 10, LS + F1 + F2 + F3 + F4 + F5 + F6, Re + 50, "" & F6, 12, , 3)
  Call QuotaH(LS + F1 + F2 + F3 + F4 + F5 + F6, Re + 10, LS + F1 + F2 + F3 + F4 + F5 + F6 + F7, Re + 50, "" & F7, 12, , 3)
  Call QuotaH(LS + F1 + F2 + F3 + F4 + F5 + F6 + F7, Re + 10, LS + F1 + F2 + F3 + F4 + F5 + F6 + F7 + F8, Re + 50, "" & F8, 12, , 3)
  Call QuotaH(LS + F1 + F2 + F3 + F4 + F5 + F6 + F7 + F8, Re + 10, LS + F1 + F2 + F3 + F4 + F5 + F6 + F7 + F8 + F9, Re + 50, "" & F9, 12, , 3)
  '
  'Spina dx
  Call linea(LS + Ft, -RS, 2 * LS + Ft, -RS)
  Call linea(LS + Ft, RS, 2 * LS + Ft, RS)
  Call linea(2 * LS + Ft, -RS, 2 * LS + Ft, RS)
  
  'Contropunta
  Call linea(2 * LS + Ft, RS / 2, 3 * LS + Ft, RS, RGB(100, 100, 100))
  Call linea(2 * LS + Ft, -RS / 2, 3 * LS + Ft, -RS, RGB(100, 100, 100))
  Call linea(3 * LS + Ft, RS, 8 * LS + Ft, RS, RGB(100, 100, 100))
  Call linea(3 * LS + Ft, -RS, 8 * LS + Ft, -RS, RGB(100, 100, 100))
  '
  Const Fattore = 10
  Dim INIZIO As Double
  INIZIO = 200
  If Not NotScale Then
    Call TestoAll(LS, Re - INIZIO, "DE=" & DE, , True, 1)
    'RIFERIMENTO ESTERNO
    Call linea(LS, Re - INIZIO, LS + F1 + F2 + F3 + F4 + F5 + F6 + F7 + F8 + F9, Re - INIZIO, vbRed, 2)
    'BS5L-A
    Call linea(LS + F1, Re - INIZIO, LS + F1, Re - INIZIO + p1_LA * Fattore, vbBlue, 1)
    Call linea(LS, Re - INIZIO + p1_LA * Fattore, LS + F1, Re - INIZIO + p1_LA * Fattore, vbBlue, 1)
    Call linea(LS + F1, Re - INIZIO + p1_LA * Fattore, LS + F1 + F2, Re - INIZIO + p2_LA * Fattore, vbBlue, 1)
    Call linea(LS + F1 + F2, Re - INIZIO, LS + F1 + F2, Re - INIZIO + p2_LA * Fattore, vbBlue, 1)
    Call linea(LS + F1 + F2, Re - INIZIO + p2_LA * Fattore, LS + F1 + F2 + F3, Re - INIZIO + P3_LA * Fattore, vbBlue, 1)
    Call linea(LS + F1 + F2 + F3, Re - INIZIO + P3_LA * Fattore, LS + F1 + F2 + F3 + F4 + F5 + F6 + F7 + F8 + F9, Re - INIZIO + P3_LA * Fattore, vbBlue, 1)
    'BS5L-B
    Call linea(LS + F1 + F2 + F3 + F4, Re - INIZIO, LS + F1 + F2 + F3 + F4, Re - INIZIO + p1_LB * Fattore, vbRed, 1)
    Call linea(LS + F1 + F2 + F3 + F4, Re - INIZIO + p1_LB * Fattore, LS + F1 + F2 + F3 + F4 + F5, Re - INIZIO + p2_LB * Fattore, vbRed, 1)
    Call linea(LS + F1 + F2 + F3 + F4 + F5, Re - INIZIO, LS + F1 + F2 + F3 + F4 + F5, Re - INIZIO + p2_LB * Fattore, vbRed, 1)
    Call linea(LS + F1 + F2 + F3 + F4 + F5, Re - INIZIO + p2_LB * Fattore, LS + F1 + F2 + F3 + F4 + F5 + F6, Re - INIZIO + P3_LB * Fattore, vbRed, 1)
    Call linea(LS + F1 + F2 + F3 + F4 + F5 + F6, Re - INIZIO, LS + F1 + F2 + F3 + F4 + F5 + F6, Re - INIZIO + P3_LB * Fattore, vbRed, 1)
    Call linea(LS + F1 + F2 + F3 + F4 + F5 + F6, Re - INIZIO + P3_LB * Fattore, LS + F1 + F2 + F3 + F4 + F5 + F6 + F7, Re - INIZIO + P3_LB * Fattore, vbRed, 1)
    Call linea(LS + F1 + F2 + F3 + F4 + F5 + F6 + F7, Re - INIZIO, LS + F1 + F2 + F3 + F4 + F5 + F6 + F7, Re - INIZIO + P3_LB * Fattore, vbRed, 1)
    Call linea(LS + F1 + F2 + F3 + F4 + F5 + F6 + F7, Re - INIZIO + P3_LB * Fattore, LS + F1 + F2 + F3 + F4 + F5 + F6 + F7 + F8, Re - INIZIO + P4_LB * Fattore, vbRed, 1)
    Call linea(LS + F1 + F2 + F3 + F4 + F5 + F6 + F7 + F8, Re - INIZIO, LS + F1 + F2 + F3 + F4 + F5 + F6 + F7 + F8, Re - INIZIO + P4_LB * Fattore, vbRed, 1)
    Call linea(LS + F1 + F2 + F3 + F4 + F5 + F6 + F7 + F8, Re - INIZIO + P4_LB * Fattore, LS + F1 + F2 + F3 + F4 + F5 + F6 + F7 + F8 + F9, Re - INIZIO + P4_LB * Fattore, vbRed, 1)
  Else
    Call QuotaH(LS, Re + 10, LS + Ft, Re + 50, "?", 12, vbRed, 3)
  End If
  '
  PAR.MargineDxPic = 0
  PAR.MargineSxPic = 0

End Sub

Public Sub Disegna_ZONA_LAVORO_BS5S(Stampante As Boolean)
'
Dim DE        As Double
Dim DI        As Double
Dim Ft        As Double
Dim Re        As Double
Dim RS        As Double
Dim LS        As Double
Dim DiaMola   As Double
Dim tColore   As Long
Dim tX        As Double
Dim tY        As Double
Dim i         As Integer
Dim NotScale  As Boolean
Dim QuotaIL   As Double
Dim QuotaFL   As Double
Dim XMola     As Double
Dim Ymola     As Double
Dim tScala    As Double
Dim YQuota    As Double
Dim QZI       As Double

Dim F1        As Double
Dim F2        As Double
Dim F3        As Double
Dim F4        As Double
Dim F5        As Double
Dim F6        As Double
Dim F7        As Double
Dim F8        As Double
Dim F9        As Double
'
Dim p1_SA     As Double
Dim p2_SA     As Double
Dim P3_SA     As Double
'
Dim p1_SB     As Double
Dim p2_SB     As Double
Dim P3_SB     As Double
'
Dim Passo1    As Double
Dim Passo2    As Double
Dim Passo3    As Double
Dim ELICA     As Integer
Dim Verso     As Integer
Dim N_Filetti As Integer
Dim AA1       As Double
Dim AA2       As Double
'
On Error Resume Next
  '
  Call InizializzaVariabili
  '
  NotScale = False
  DE = Lp("R[189]")
  '
  p1_SA = Lp("R[520]")
  p2_SA = Lp("R[521]")
  P3_SA = Lp("R[522]")
  '
  p1_SB = Lp("R[320]")
  p2_SB = Lp("R[321]")
  P3_SB = Lp("R[322]")
  '
  AA1 = Lp("R[41]"): AA1 = AA1 * PG / 180
  AA2 = Lp("R[51]"): AA2 = AA2 * PG / 180
  '
  DI = DE - p1_SA * 2
  QZI = Lp("R[190]")
  DiaMola = Lp("R[200]")
  ELICA = Lp("R[124]")
  If (ELICA > 0) Then
    Verso = 1
  Else
    Verso = -1
  End If
  '
  Passo1 = Lp("R[128]")
  Passo2 = Lp("R[132]")
  Passo3 = Lp("R[133]")
  '
  F1 = Lp("R[91]")
  F2 = Lp("R[92]")
  F3 = Lp("R[93]")
  F4 = Lp("R[94]")
  F5 = Lp("R[95]")
  F6 = Lp("R[96]")
  F7 = Lp("R[97]") - Passo2
  F8 = Passo2
  F9 = Lp("R[98]")
  Ft = F1 + F2 + F3 + F4 + F5 + F6 + F7 + F8 + F9
  If (Ft = 0) Then
    NotScale = True
    Ft = 50
  End If
  '
  Re = DE / 2
  RS = Re / 4       'Raggio spina
  LS = RS           'Lunghezza spina sporgente
  '
  PAR.MaxX = 6 * LS + Ft
  PAR.MinX = -2 * LS
  PAR.MaxY = Re + 35
  PAR.MinY = 0
  PAR.Allineamento = 0
  Call CalcolaParPic(PAR, True, Stampante)
  '
  PAR.MargineDxPic = PAR.Pic.TextWidth("Z=888.888")
  PAR.MargineSxPic = PAR.Pic.TextWidth("Z=888.888")
  '
  'Sinistra
  PAR.MaxX = (2 * LS + Ft)
  PAR.MinX = 0
  PAR.MaxY = 2 * Re
  PAR.MinY = -2 * Re
  PAR.ScalaMargine = 1
  PAR.Allineamento = 0
  Call CalcolaParPic(PAR, True, Stampante)
  XMola = LS
  Ymola = 0
  QuotaIL = QZI
  QuotaFL = QZI + Ft
  tScala = PAR.Scala
  If PAR.Pic Is Printer Then tScala = tScala * 45
  YQuota = Re + 350 / tScala
  Call TestoAll(XMola - 5, Re + 10, "Z=" & QuotaIL, , True, 1)
  Call TestoAll(XMola + Ft + 5, Re + 10, "Z=" & QuotaFL, , True, 2)
  '
  'Centro mola
  Call linea(XMola, Re + 20 + DiaMola, XMola, Re, RGB(255, 200, 200), 1, vbDashDot)
  '
  'Disegno della mola
  Dim spe0    As Double
  Dim spe1    As Double
  Dim spe2    As Double
  Dim Altezza As Double
  Const OFF1 = 250
  '
  spe0 = Lp("R[37]")
  spe0 = spe0 / 2
  Altezza = p2_SA * 2
  spe2 = 5
  spe1 = Lp("R[38]")
  spe1 = spe1 / 2
  '
  Call linea(XMola - spe0, Re + OFF1 + Altezza + DiaMola, XMola - spe0, Re + OFF1 + Altezza, RGB(0, 0, 0), 1, vbSolid)
  Call linea(XMola - spe0, Re + OFF1 + Altezza, XMola - spe1, Re + OFF1 + Altezza, RGB(0, 0, 0), 1, vbSolid)
  Call linea(XMola - spe1, Re + OFF1 + Altezza, XMola - spe2, Re + OFF1, RGB(0, 0, 0), 1, vbSolid)
  Call linea(XMola + spe0, Re + OFF1 + Altezza + DiaMola, XMola + spe0, Re + OFF1 + Altezza, RGB(0, 0, 0), 1, vbSolid)
  Call linea(XMola + spe0, Re + OFF1 + Altezza, XMola + spe1, Re + OFF1 + Altezza, RGB(0, 0, 0), 1, vbSolid)
  Call linea(XMola + spe1, Re + OFF1 + Altezza, XMola + spe2, Re + OFF1, RGB(0, 0, 0), 1, vbSolid)
  Call linea(XMola - spe2, Re + OFF1, XMola + spe2, Re + OFF1, RGB(0, 0, 0), 1, vbSolid)
  '
  'CANALE SOLIDI
  Const OFF2 = 200
  Call linea(LS, Passo2 + OFF2, LS + F1, Passo2 + OFF2, vbBlue, 1)
  Call linea(LS, OFF2, LS + F1, OFF2, vbBlue, 1)
  Call linea(LS, Passo2 + OFF2, LS, OFF2, vbBlue, 1)
  Call linea(LS + F1, OFF2, LS + F1 + F2 + F3 + F4, OFF2, vbBlue, 1)
  Call linea(LS + F1, Passo2 + OFF2, LS + F1, OFF2, vbBlue, 1)
  Call linea(LS + F1, Passo2 + OFF2, LS + F1 + F2 + F3 + F4, OFF2, vbBlue, 1)
  'CANALE FUSI
  Call linea(LS + F1 + F2 + F3, Passo2 + OFF2, LS + F1 + F2 + F3 + F4 + F5, Passo2 + OFF2, vbRed, 1)
  Call linea(LS + F1 + F2 + F3, Passo2 + OFF2, LS + F1 + F2 + F3 + F4 + F5, OFF2, vbRed, 1)
  Call linea(LS + F1 + F2 + F3 + F4 + F5, Passo2 + OFF2, LS + F1 + F2 + F3 + F4 + F5, OFF2, vbRed, 1)
  Call linea(LS + F1 + F2 + F3 + F4 + F5, Passo2 + OFF2, LS + Ft, Passo2 + OFF2, vbRed, 1)
  Call linea(LS + Ft, Passo2 + OFF2, LS + Ft, OFF2, vbRed, 1)
  Call linea(LS + F1 + F2 + F3 + F4 + F5, OFF2, LS + Ft, OFF2, vbRed, 1)
  '
  'Spina sx
  Call linea(0, -RS, 0, RS)
  Call linea(0, -RS, LS, -RS)
  Call linea(0, RS, LS, RS)
  'Vite
  Call linea(LS, -Re, LS, Re, vbBlue, 2)
  Call linea(LS, -Re, LS + Ft, -Re, vbBlue, 2)
  Call linea(LS, Re, LS + Ft, Re, vbBlue, 2)
  Call linea(LS + Ft, -Re, LS + Ft, Re, vbBlue, 2)
  N_Filetti = F1 / Abs(Passo1)
  For i = 1 To N_Filetti
    Call linea(LS + (i - 1) * Abs(Passo1), -Verso * Re, LS + i * Abs(Passo1), Verso * Re, vbBlue, 2)
  Next i
  N_Filetti = (Ft - F1) / Abs(Passo2)
  For i = 1 To N_Filetti
    Call linea(LS + (i - 1) * Abs(Passo2) + F1, -Verso * Re, LS + i * Abs(Passo2) + F1, Verso * Re, vbBlue, 2)
  Next i
  N_Filetti = (F2 + F3 + F4 + F5 + F6) / Abs(Passo3)
  For i = 1 To N_Filetti
    Call linea(LS + (i - 1) * Abs(Passo3) + F1, -Verso * Re, LS + i * Abs(Passo3) + F1, Verso * Re, vbRed, 2)
  Next i
  Call QuotaH(LS, Re + 10, LS + F1, Re + 50, "" & F1, 12, , 3)
  Call QuotaH(LS + F1, Re + 10, LS + F1 + F2, Re + 50, "" & F2, 12, , 3)
  Call QuotaH(LS + F1 + F2, Re + 10, LS + F1 + F2 + F3, Re + 50, "" & F3, 12, , 3)
  Call QuotaH(LS + F1 + F2 + F3, Re + 10, LS + F1 + F2 + F3 + F4, Re + 50, "" & F4, 12, , 3)
  Call QuotaH(LS + F1 + F2 + F3 + F4, Re + 10, LS + F1 + F2 + F3 + F4 + F5, Re + 50, "" & F5, 12, , 3)
  Call QuotaH(LS + F1 + F2 + F3 + F4 + F5, Re + 10, LS + F1 + F2 + F3 + F4 + F5 + F6, Re + 50, "" & F6, 12, , 3)
  Call QuotaH(LS + F1 + F2 + F3 + F4 + F5 + F6, Re + 10, LS + F1 + F2 + F3 + F4 + F5 + F6 + F7, Re + 50, "" & F7, 12, , 3)
  Call QuotaH(LS + F1 + F2 + F3 + F4 + F5 + F6 + F7, Re + 10, LS + F1 + F2 + F3 + F4 + F5 + F6 + F7 + F8, Re + 50, "" & F8, 12, , 3)
  Call QuotaH(LS + F1 + F2 + F3 + F4 + F5 + F6 + F7 + F8, Re + 10, LS + F1 + F2 + F3 + F4 + F5 + F6 + F7 + F8 + F9, Re + 50, "" & F9, 12, , 3)
  '
  'Spina dx
  Call linea(LS + Ft, -RS, 2 * LS + Ft, -RS)
  Call linea(LS + Ft, RS, 2 * LS + Ft, RS)
  Call linea(2 * LS + Ft, -RS, 2 * LS + Ft, RS)
  '
  'Contropunta
  Call linea(2 * LS + Ft, RS / 2, 3 * LS + Ft, RS, RGB(100, 100, 100))
  Call linea(2 * LS + Ft, -RS / 2, 3 * LS + Ft, -RS, RGB(100, 100, 100))
  Call linea(3 * LS + Ft, RS, 8 * LS + Ft, RS, RGB(100, 100, 100))
  Call linea(3 * LS + Ft, -RS, 8 * LS + Ft, -RS, RGB(100, 100, 100))
  '
  Const Fattore = 10
  Dim INIZIO As Double
  INIZIO = 200
  If Not NotScale Then
    Call TestoAll(LS, Re - INIZIO, "DE=" & DE, , True, 1)
    'RIFERIMENTO ESTERNO
    Call linea(LS, Re - INIZIO, LS + Ft, Re - INIZIO, vbRed, 2)
    'BS5S-A
    Call linea(LS + F1 + F2 + F3 + F4 + F5, Re - INIZIO + p1_SA * Fattore, LS + F1 + F2 + F3 + F4 + F5, Re - INIZIO, vbBlue, 1)
    Call linea(LS + F1 + F2 + F3 + F4 + F5, Re - INIZIO + p1_SA * Fattore, LS + F1 + F2, Re - INIZIO + p2_SA * Fattore, vbBlue, 1)
    Call linea(LS + F1 + F2, Re - INIZIO, LS + F1 + F2, Re - INIZIO + p2_SA * Fattore, vbBlue, 1)
    Call linea(LS + F1 + F2, Re - INIZIO + p2_SA * Fattore, LS + F1, Re - INIZIO + P3_SA * Fattore, vbBlue, 1)
    Call linea(LS + F1, Re - INIZIO, LS + F1, Re - INIZIO + P3_SA * Fattore, vbBlue, 1)
    'BS5S-B
    Call linea(LS + F1 + F2 + F3, Re - INIZIO, LS + F1 + F2 + F3, Re - INIZIO + p1_SB * Fattore, vbRed, 1)
    Call linea(LS + F1 + F2 + F3, Re - INIZIO + p1_SB * Fattore, LS + F1 + F2 + F3 + F4, Re - INIZIO + p2_SB * Fattore, vbRed, 1)
    Call linea(LS + F1 + F2 + F3 + F4, Re - INIZIO, LS + F1 + F2 + F3 + F4, Re - INIZIO + p2_SB * Fattore, vbRed, 1)
    Call linea(LS + F1 + F2 + F3 + F4, Re - INIZIO + p2_SB * Fattore, LS + F1 + F2 + F3 + F4 + F5 + F6, Re - INIZIO + P3_SB * Fattore, vbRed, 1)
    Call linea(LS + F1 + F2 + F3 + F4 + F5 + F6, Re - INIZIO, LS + F1 + F2 + F3 + F4 + F5 + F6, Re - INIZIO + P3_SB * Fattore, vbRed, 1)
  Else
    Call QuotaH(LS, Re + 10, LS + Ft, Re + 50, "?", 12, vbRed, 3)
  End If
  '
  PAR.MargineDxPic = 0
  PAR.MargineSxPic = 0

End Sub

Public Sub Disegna_CorrElica(Stampante As Boolean)
'
Dim nZone  As Integer
Dim X()    As Double
Dim Y()    As Double
Dim Con1F1 As Double
Dim Con2F1 As Double
Dim Con3F1 As Double
Dim Con1F2 As Double
Dim Con2F2 As Double
Dim Con3F2 As Double
Dim Bom1F1 As Double
Dim Bom2F1 As Double
Dim Bom3F1 As Double
Dim Bom1F2 As Double
Dim Bom2F2 As Double
Dim Bom3F2 As Double
Dim MaxX1  As Double
Dim MaxX2  As Double
Dim scalaxEl As Double
Dim Pos As Double
'Dim XMola As Double
'Dim Ymola As Double
'Zone
Dim XF0 As Double
Dim XF1 As Double
Dim XF2 As Double
Dim XF3 As Double
Dim XF0_2 As Double
Dim XF1_2 As Double
Dim XF2_2 As Double
Dim XF3_2 As Double
Dim Ft As Double
Dim F1 As Double
Dim F2 As Double
Dim F3 As Double

Dim F1_2 As Double
Dim F2_2 As Double
Dim F3_2 As Double

Dim Re As Double
Dim RS As Double
Dim LS As Double

Dim tColore    As Long
Dim tX1       As Double
Dim tX        As Double
Dim tY        As Double
Dim i         As Integer
Dim NotScale  As Boolean
Dim Lin       As Double
'
On Error Resume Next
  '
  Call InizializzaVariabili
  '
  NotScale = False
  nZone = Lp("iNzone")
  F1 = 0:   F2 = 0:   F3 = 0
  F1_2 = 0: F2_2 = 0: F3_2 = 0
  'iFas1
  Select Case nZone
    Case 1
      F1 = Lp("Fascia1F1")
      F1_2 = Lp("Fascia1F2")
    Case 2
      F1 = Lp("Fascia1F1")
      F2 = Lp("Fascia2F1")
      F1_2 = Lp("Fascia1F2")
      F2_2 = Lp("Fascia2F2")
    Case 3
      F1 = Lp("Fascia1F1")
      F2 = Lp("Fascia2F1")
      F3 = Lp("Fascia3F1")
      F1_2 = Lp("Fascia1F2")
      F2_2 = Lp("Fascia2F2")
      F3_2 = Lp("Fascia3F2")
  End Select
  Ft = F1 + F2 + F3 'Fascia Totale
  Re = 10:  RS = 10:  LS = 10
  Dim Ft2 As Double
  Ft2 = F1_2 + F2_2 + F3_2
  XF0 = LS:    XF1 = XF0 + F1:        XF2 = XF1 + F2:        XF3 = XF2 + F3
  XF0_2 = LS:  XF1_2 = XF0_2 + F1_2:  XF2_2 = XF1_2 + F2_2:  XF3_2 = XF2_2 + F3_2
  'Disegno correzioni elica
  ReDim X(45):  ReDim Y(45)
  scalaxEl = val(Lp_Opt("iScalaX"))
  If (scalaxEl = 0) Then scalaxEl = Lp("iScalaX")
  '
  Con1F1 = Lp("Conic1F1"): Con2F1 = Lp("Conic2F1"): Con3F1 = Lp("Conic3F1")
  Con1F2 = Lp("Conic1F2"): Con2F2 = Lp("Conic2F2"): Con3F2 = Lp("Conic3F2")
  '
  Bom1F1 = Lp("Bomb1F1"):  Bom2F1 = Lp("Bomb2F1"):  Bom3F1 = Lp("Bomb3F1")
  Bom1F2 = Lp("Bomb1F2"):  Bom2F2 = Lp("Bomb2F2"):  Bom3F2 = Lp("Bomb3F2")
  '
  Pos = RS
  X(0) = XF0
  X(1) = XF1
  X(2) = XF2
  X(3) = XF3
  X(16) = XF3
  Y(0) = Pos
  Y(1) = Pos + Con1F1 * scalaxEl
  Y(2) = Pos + (Con1F1 + Con2F1) * scalaxEl
  Y(3) = Pos + (Con1F1 + Con2F1 + Con3F1) * scalaxEl
  Y(16) = Pos
  X(4) = XF0_2
  X(5) = XF1_2
  X(6) = XF2_2
  X(7) = XF3_2
  X(17) = XF3_2
  Y(4) = -Pos
  Y(5) = -Pos - Con1F2 * scalaxEl
  Y(6) = -Pos - (Con1F2 + Con2F2) * scalaxEl
  Y(7) = -Pos - (Con1F2 + Con2F2 + Con3F2) * scalaxEl
  Y(17) = -Pos
  'Lineette
  X(8) = X(1)
  X(9) = X(1)
  X(12) = X(5)
  X(13) = X(5)
  X(10) = X(2)
  X(11) = X(2)
  X(14) = X(6)
  X(15) = X(6)
  Lin = 3
  Y(8) = Y(1) + Lin
  Y(9) = Y(1) - Lin
  Y(12) = Y(5) + Lin
  Y(13) = Y(5) - Lin
  Y(10) = Y(2) + Lin
  Y(11) = Y(2) - Lin
  Y(14) = Y(6) + Lin
  Y(15) = Y(6) - Lin
  Dim StepGriglia As Double
  StepGriglia = 0.05
  For i = -3 To 3
    Y(21 + i) = Y(0) + i * StepGriglia * scalaxEl
    Y(28 + i) = Y(0) + i * StepGriglia * scalaxEl
    Y(35 + i) = Y(4) + i * StepGriglia * scalaxEl
    Y(42 + i) = Y(4) + i * StepGriglia * scalaxEl
    X(21 + i) = XF0
    X(28 + i) = XF3
    X(35 + i) = XF0
    X(42 + i) = XF3
  Next i
  PAR.MaxY = Y(0)
  PAR.MinY = Y(4)
  For i = 1 To 3
    If Y(i) > PAR.MaxY Then PAR.MaxY = Y(i)
    If Y(i + 4) < PAR.MinY Then PAR.MinY = Y(i + 4)
  Next i
  If Ft >= Ft2 Then
    PAR.MaxX = Ft + LS
  Else
    PAR.MaxX = Ft2 + LS
  End If
  PAR.MinX = LS / 2
  'par.MaxY = Re / 2
  'par.MinY = -Re / 2
  PAR.ScalaMargine = 0.9
  PAR.Allineamento = 0
  Call CalcolaParPic(PAR, True, Stampante)
  'par.Pic.Cls
  'linea XMola, Re + 20 + DiaMola, XMola, Re, vbRed, 1, vbDash
  MaxX1 = X(1)
  MaxX2 = X(5)
  If Not NotScale Then
    Select Case nZone 'Disegno linee verticali che delimitano le zone
      Case 2
        Call linea(XF1, 0, XF1, Re, RGB(180, 180, 180), 1, vbDash)
        Call QuotaH(XF0, Re, XF1, Re, "" & F1, 12, CalcoloColore("Fascia1F1"), 3)
        Call QuotaH(XF1, Re, XF2, Re, "" & F2, 12, CalcoloColore("Fascia2F1"), 3)
        Call linea(XF1_2, -Re, XF1_2, 0, RGB(180, 180, 180), 1, vbDash)
        Call QuotaH(XF0_2, -Re, XF1_2, -Re, "" & F1_2, 12, CalcoloColore("Fascia1F2"), 3)
        Call QuotaH(XF1_2, -Re, XF2_2, -Re, "" & F2_2, 12, CalcoloColore("Fascia2F2"), 3)
        MaxX1 = X(2)
        MaxX2 = X(6)
      Case 3
        Call linea(XF1, 0, XF1, Re, RGB(180, 180, 180), 1, vbDash)
        Call linea(XF2, 0, XF2, Re, RGB(180, 180, 180), 1, vbDash)
        Call QuotaH(XF0, Re, XF1, Re, "" & F1, 12, CalcoloColore("Fascia1F1"), 3)
        Call QuotaH(XF1, Re, XF2, Re, "" & F2, 12, CalcoloColore("Fascia2F1"), 3)
        Call QuotaH(XF2, Re, XF3, Re, "" & F3, 12, CalcoloColore("Fascia3F1"), 3)
        Call linea(XF1_2, -Re, XF1_2, 0, RGB(180, 180, 180), 1, vbDash)
        Call linea(XF2_2, -Re, XF2_2, 0, RGB(180, 180, 180), 1, vbDash)
        Call QuotaH(XF0_2, -Re, XF1_2, -Re, "" & F1_2, 12, CalcoloColore("Fascia1F2"), 3)
        Call QuotaH(XF1_2, -Re, XF2_2, -Re, "" & F2_2, 12, CalcoloColore("Fascia2F2"), 3)
        Call QuotaH(XF2_2, -Re, XF3_2, -Re, "" & F3_2, 12, CalcoloColore("Fascia3F2"), 3)
        MaxX1 = X(3)
        MaxX2 = X(7)
    End Select
  End If
  ' Asse pezzo
  Call linea(LS, 0, LS + Ft, 0, RGB(180, 180, 180), 1, vbDashDot)
  If Not NotScale Then
    Call QuotaH(LS, PAR.MaxY + 8, LS + Ft, PAR.MaxY + 8, "" & Ft, 12, , 3)
  End If
  Dim tidP1   As Integer
  Dim tidP2   As Integer
  Dim tidC    As Long
  Dim tidst   As Integer
  Dim tisp    As Integer
  tColore = vbBlue
  tidst = vbSolid
  'Linee (griglia)
  For i = -3 To 3
    tidP1 = 21 + i
    tidP2 = 28 + i
    Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), RGB(200, 200, 200), 1, vbDot)
    tidP1 = 35 + i
    tidP2 = 42 + i
    Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), RGB(200, 200, 200), 1, vbDot)
  Next i
  'Fianco1 Fascia1
  tidP1 = 0
  tidP2 = 1
  tidC = CalcoloColore("Conic1F1;Bomb1F1", , , tColore)
  tisp = IIf(tidC = vbRed, 2, 1)
  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack)
  If Bom1F1 <> 0 Then
    Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), Bom1F1, scalaxEl, tidC, tisp)
  End If
  'Fianco1 Fascia2
  tidP1 = 1
  tidP2 = 2
  tidC = CalcoloColore("Conic2F1;Bomb2F1", , , tColore)
  tisp = IIf(tidC = vbRed, 2, 1)
  linea X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack
  If Bom2F1 <> 0 Then
    Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), Bom2F1, scalaxEl, tidC, tisp)
  End If
  'Fianco1 Fascia3
  tidP1 = 2
  tidP2 = 3
  tidC = CalcoloColore("Conic3F1;Bomb3F1", , , tColore)
  tisp = IIf(tidC = vbRed, 2, 1)
  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack)
  If Bom3F1 <> 0 Then
    Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), Bom3F1, scalaxEl, tidC, tisp)
  End If
  'Fianco 1 Separazione Zone 1 e 2
  tidC = tColore
  tisp = 1
  tidP1 = 8
  tidP2 = 9
  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, tidst)
  'Fianco 1 Separazione Zone 2 e 3
  tidP1 = 10
  tidP2 = 11
  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, tidst)
  'Fianco 1 Linea tra primo e ultimo punto
  tidP1 = 0
  tidP2 = 16
  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, vbDot)
  'Fianco2 Fascia1
  tidP1 = 4
  tidP2 = 5
  tidC = CalcoloColore("Conic1F2;Bomb1F2", , , tColore)
  tisp = IIf(tidC = vbRed, 2, 1)
  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack)
  If Bom1F2 <> 0 Then
    Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), -Bom1F2, scalaxEl, tidC, tisp)
  End If
  'Fianco2 Fascia2
  tidP1 = 5
  tidP2 = 6
  tidC = CalcoloColore("Conic2F2;Bomb2F2", , , tColore)
  tisp = IIf(tidC = vbRed, 2, 1)
  'linea x(tidP1), Y(tidP1), x(tidP2), Y(tidP2), tidC, tisp, tidst
  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack)
  If Bom2F2 <> 0 Then
    Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), -Bom2F2, scalaxEl, tidC, tisp)
  End If
  'Fianco2 Fascia3
  tidP1 = 6
  tidP2 = 7
  tidC = CalcoloColore("Conic3F2;Bomb3F2", , , tColore)
  tisp = IIf(tidC = vbRed, 2, 1)
  'linea x(tidP1), Y(tidP1), x(tidP2), Y(tidP2), tidC, tisp, tidst
  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack)
  If Bom3F2 <> 0 Then
    Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), -Bom3F2, scalaxEl, tidC, tisp)
  End If
  tidC = tColore
  tisp = 1
  'Fianco 2 Separazione Zone 1 e 2
  tidP1 = 12
  tidP2 = 13
  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, tidst)
  'Fianco 2 Separazione Zone 2 e 3
  tidP1 = 14
  tidP2 = 15
  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, tidst)
  'Fianco 1 Linea tra primo e ultimo punto
  tidP1 = 4
  tidP2 = 17
  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, vbDot)
  'Scritte F1 e F2
  Call TestoAll(LS, RS, "F1", , True, 1)
  Call TestoAll(LS, -RS, "F2", , True, 1)
  If NotScale Then Call TestoInfo("DRAWING NOT IN SCALE ! ", , True, vbRed, True)
  Call TestoInfo("Corr. Scale: " & scalaxEl, 1)
  Call TestoInfo("Units: [mm]", 3)
  If Ft2 > Ft Then
    Call TestoInfo("WARNING: (FT F1 = " & Ft & ") < (FT F2 = " & Ft2 & ")", , True, vbRed, True)
    Call QuotaH(MaxX1, Pos, MaxX2, Pos, "?", , vbRed, 3)
  ElseIf Ft2 < Ft Then
    Call TestoInfo("WARNING: (FT F1 = " & Ft & ") > (FT F2 = " & Ft2 & ")", , True, vbRed, True)
    Call QuotaH(MaxX2, -Pos, MaxX1, -Pos, "?", , vbRed, 3)
  End If
  '
End Sub

Public Sub Disegna_CorrElica_SKIVING(Stampante As Boolean)
Dim LatoPart  As Integer
Dim nZone     As Integer
Dim X()       As Double
Dim Y()       As Double
Dim Con1F1    As Double
Dim Con2F1    As Double
Dim Con3F1    As Double
Dim Con1F2    As Double
Dim Con2F2    As Double
Dim Con3F2    As Double
Dim Bom1F1    As Double
Dim Bom2F1    As Double
Dim Bom3F1    As Double
Dim Bom1F2    As Double
Dim Bom2F2    As Double
Dim Bom3F2    As Double
Dim MaxX1     As Double
Dim MaxX2     As Double
Dim scalaxEl  As Double
Dim Pos       As Double
Dim XF0       As Double
Dim XF1       As Double
Dim XF2       As Double
Dim XF3       As Double
Dim XF0_2     As Double
Dim XF1_2     As Double
Dim XF2_2     As Double
Dim XF3_2     As Double
Dim Ft        As Double
Dim Ft2       As Double
Dim F1        As Double
Dim F2        As Double
Dim F3        As Double
Dim F1_2      As Double
Dim F2_2      As Double
Dim F3_2      As Double
Dim Re        As Double
Dim RS        As Double
Dim LS        As Double
Dim tColore   As Long
Dim tX1       As Double
Dim tX        As Double
Dim tY        As Double
Dim i         As Integer
Dim NotScale  As Boolean
Dim Lin       As Double
Dim CORSA_O   As Double
Dim CORSA_I   As Double
'
On Error Resume Next
  '
  Call InizializzaVariabili
  '
  NotScale = False
  '
  scalaxEl = Lp("iScalaX_H")
  If (scalaxEl = 0) Then scalaxEl = Lp("iScalaX_H")
  LatoPart = Lp("LatoPA_H")
  CORSA_I = Lp("CORSAIN_H")
  CORSA_O = Lp("CORSAOUT_H")
  nZone = Lp("iNzoneH")
  F1 = 0:   F2 = 0:   F3 = 0
  F1_2 = 0: F2_2 = 0: F3_2 = 0
  Select Case nZone
    Case 1
        F1 = CORSA_I + Lp("iFas1F1H") + CORSA_O: F1_2 = CORSA_I + Lp("iFas1F2H") + CORSA_O
    Case 2
        F1 = Lp("iFas1F1H") + CORSA_I: F1_2 = Lp("iFas1F2H") + CORSA_I
        F2 = Lp("iFas2F1H") + CORSA_O: F2_2 = Lp("iFas2F2H") + CORSA_O
    Case 3
        F1 = Lp("iFas1F1H") + CORSA_I: F1_2 = Lp("iFas1F2H") + CORSA_I
        F2 = Lp("iFas2F1H"):           F2_2 = Lp("iFas2F2H")
        F3 = Lp("iFas3F1H") + CORSA_O: F3_2 = Lp("iFas3F2H") + CORSA_O
  End Select
  Re = 10:  RS = 10:  LS = 10
  '
  Ft = F1 + F2 + F3         'Fascia Totale F1
  Ft2 = F1_2 + F2_2 + F3_2  'Fascia Totale F2
  XF0 = LS:    XF1 = XF0 + F1:        XF2 = XF1 + F2:        XF3 = XF2 + F3
  XF0_2 = LS:  XF1_2 = XF0_2 + F1_2:  XF2_2 = XF1_2 + F2_2:  XF3_2 = XF2_2 + F3_2
  '
  'Disegno correzioni elica
  ReDim X(45):  ReDim Y(45)
  '
  Con1F1 = Lp("iCon1F1H"):  Con2F1 = Lp("iCon2F1H"):  Con3F1 = Lp("iCon3F1H")
  Con1F2 = Lp("iCon1F2H"):  Con2F2 = Lp("iCon2F2H"):  Con3F2 = Lp("iCon3F2H")
  '
  Bom1F1 = Lp("iBom1F1H"):  Bom2F1 = Lp("iBom2F1H"):  Bom3F1 = Lp("iBom3F1H")
  Bom1F2 = Lp("iBom1F2H"):  Bom2F2 = Lp("iBom2F2H"):  Bom3F2 = Lp("iBom3F2H")
  '
  Pos = RS
  X(0) = XF0
  X(1) = XF1
  X(2) = XF2
  X(3) = XF3
  X(16) = XF3
  Y(0) = Pos
  Y(1) = Pos + (Con1F1 * 1 + Con2F1 * 0 + Con3F1 * 0) * scalaxEl
  Y(2) = Pos + (Con1F1 * 1 + Con2F1 * 1 + Con3F1 * 0) * scalaxEl
  Y(3) = Pos + (Con1F1 * 1 + Con2F1 * 1 + Con3F1 * 1) * scalaxEl
  Y(16) = Pos
  X(4) = XF0_2
  X(5) = XF1_2
  X(6) = XF2_2
  X(7) = XF3_2
  X(17) = XF3_2
  Y(4) = -Pos
  Y(5) = -Pos - (Con1F2 * 1 + Con2F2 * 0 + Con3F2 * 0) * scalaxEl
  Y(6) = -Pos - (Con1F2 * 1 + Con2F2 * 1 + Con3F2 * 0) * scalaxEl
  Y(7) = -Pos - (Con1F2 * 1 + Con2F2 * 1 + Con3F2 * 1) * scalaxEl
  Y(17) = -Pos
  'Lineette
  X(8) = X(1)
  X(9) = X(1)
  X(12) = X(5)
  X(13) = X(5)
  X(10) = X(2)
  X(11) = X(2)
  X(14) = X(6)
  X(15) = X(6)
  Lin = 3
  Y(8) = Y(1) + Lin
  Y(9) = Y(1) - Lin
  Y(12) = Y(5) + Lin
  Y(13) = Y(5) - Lin
  Y(10) = Y(2) + Lin
  Y(11) = Y(2) - Lin
  Y(14) = Y(6) + Lin
  Y(15) = Y(6) - Lin
  Dim StepGriglia As Double
  StepGriglia = 0.05
  For i = -3 To 3
    Y(21 + i) = Y(0) + i * StepGriglia * scalaxEl
    Y(28 + i) = Y(0) + i * StepGriglia * scalaxEl
    Y(35 + i) = Y(4) + i * StepGriglia * scalaxEl
    Y(42 + i) = Y(4) + i * StepGriglia * scalaxEl
    X(21 + i) = XF0
    X(28 + i) = XF3
    X(35 + i) = XF0
    X(42 + i) = XF3
  Next i
  PAR.MaxY = Y(0)
  PAR.MinY = Y(4)
  For i = 1 To 3
    If Y(i) > PAR.MaxY Then PAR.MaxY = Y(i)
    If Y(i + 4) < PAR.MinY Then PAR.MinY = Y(i + 4)
  Next i
  If Ft >= Ft2 Then
    PAR.MaxX = Ft + LS
  Else
    PAR.MaxX = Ft2 + LS
  End If
  PAR.MinX = LS / 2
  'par.MaxY = Re / 2
  'par.MinY = -Re / 2
  PAR.ScalaMargine = 0.9
  PAR.Allineamento = 0
  Call CalcolaParPic(PAR, True, Stampante)
  'par.Pic.Cls
  'linea XMola, Re + 20 + DiaMola, XMola, Re, vbRed, 1, vbDash
  MaxX1 = X(1)
  MaxX2 = X(5)
  If Not NotScale Then
    Select Case nZone 'Disegno linee verticali che delimitano le zone
      Case 2
        Call linea(XF1, 0, XF1, Re, RGB(180, 180, 180), 1, vbDash)
        Call QuotaH(XF0, Re, XF1, Re, "" & F1, 12, CalcoloColore("iFas1F1H"), 3)
        Call QuotaH(XF1, Re, XF2, Re, "" & F2, 12, CalcoloColore("iFas2F1H"), 3)
        Call linea(XF1_2, -Re, XF1_2, 0, RGB(180, 180, 180), 1, vbDash)
        Call QuotaH(XF0_2, -Re, XF1_2, -Re, "" & F1_2, 12, CalcoloColore("iFas1F2H"), 3)
        Call QuotaH(XF1_2, -Re, XF2_2, -Re, "" & F2_2, 12, CalcoloColore("iFas2F2H"), 3)
        MaxX1 = X(2)
        MaxX2 = X(6)
      Case 3
        Call linea(XF1, 0, XF1, Re, RGB(180, 180, 180), 1, vbDash)
        Call linea(XF2, 0, XF2, Re, RGB(180, 180, 180), 1, vbDash)
        Call QuotaH(XF0, Re, XF1, Re, "" & F1, 12, CalcoloColore("iFas1F1H"), 3)
        Call QuotaH(XF1, Re, XF2, Re, "" & F2, 12, CalcoloColore("iFas2F1H"), 3)
        Call QuotaH(XF2, Re, XF3, Re, "" & F3, 12, CalcoloColore("iFas3F1H"), 3)
        Call linea(XF1_2, -Re, XF1_2, 0, RGB(180, 180, 180), 1, vbDash)
        Call linea(XF2_2, -Re, XF2_2, 0, RGB(180, 180, 180), 1, vbDash)
        Call QuotaH(XF0_2, -Re, XF1_2, -Re, "" & F1_2, 12, CalcoloColore("iFas1F2H"), 3)
        Call QuotaH(XF1_2, -Re, XF2_2, -Re, "" & F2_2, 12, CalcoloColore("iFas2F2H"), 3)
        Call QuotaH(XF2_2, -Re, XF3_2, -Re, "" & F3_2, 12, CalcoloColore("iFas3F2H"), 3)
        MaxX1 = X(3)
        MaxX2 = X(7)
    End Select
  End If
  ' Asse pezzo
  Call linea(LS, 0, LS + Ft, 0, RGB(180, 180, 180), 1, vbDashDot)
  If Not NotScale Then
    Call QuotaH(LS, PAR.MaxY + 8, LS + Ft, PAR.MaxY + 8, "" & Ft, 12, , 3)
  End If
  Dim tidP1   As Integer
  Dim tidP2   As Integer
  Dim tidC    As Long
  Dim tidst   As Integer
  Dim tisp    As Integer
  tColore = vbBlue
  tidst = vbSolid
  'Linee (griglia)
  For i = -3 To 3
    tidP1 = 21 + i
    tidP2 = 28 + i
    Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), RGB(200, 200, 200), 1, vbDot)
    tidP1 = 35 + i
    tidP2 = 42 + i
    Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), RGB(200, 200, 200), 1, vbDot)
  Next i
  'Fianco1 Fascia1
  tidP1 = 0
  tidP2 = 1
  tidC = CalcoloColore("iCon1F1H;iBom1F1H", , , tColore)
  tisp = IIf(tidC = vbRed, 2, 1)
  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack)
  If Bom1F1 <> 0 Then
    Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), Bom1F1, scalaxEl, tidC, tisp)
  End If
  'Fianco1 Fascia2
  tidP1 = 1
  tidP2 = 2
  tidC = CalcoloColore("iCon2F1H;iBom2F1H", , , tColore)
  tisp = IIf(tidC = vbRed, 2, 1)
  linea X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack
  If Bom2F1 <> 0 Then
    Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), Bom2F1, scalaxEl, tidC, tisp)
  End If
  'Fianco1 Fascia3
  tidP1 = 2
  tidP2 = 3
  tidC = CalcoloColore("iCon3F1H;iBom3F1H", , , tColore)
  tisp = IIf(tidC = vbRed, 2, 1)
  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack)
  If Bom3F1 <> 0 Then
    Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), Bom3F1, scalaxEl, tidC, tisp)
  End If
  'Fianco 1 Separazione Zone 1 e 2
  tidC = tColore
  tisp = 1
  tidP1 = 8
  tidP2 = 9
  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, tidst)
  'Fianco 1 Separazione Zone 2 e 3
  tidP1 = 10
  tidP2 = 11
  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, tidst)
  'Fianco 1 Linea tra primo e ultimo punto
  tidP1 = 0
  tidP2 = 16
  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, vbDot)
  'Fianco2 Fascia1
  tidP1 = 4
  tidP2 = 5
  tidC = CalcoloColore("iCon1F2H;iBom1F2H", , , tColore)
  tisp = IIf(tidC = vbRed, 2, 1)
  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack)
  If Bom1F2 <> 0 Then
    Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), -Bom1F2, scalaxEl, tidC, tisp)
  End If
  'Fianco2 Fascia2
  tidP1 = 5
  tidP2 = 6
  tidC = CalcoloColore("iCon2F2H;iBom2F2H", , , tColore)
  tisp = IIf(tidC = vbRed, 2, 1)
  'linea x(tidP1), Y(tidP1), x(tidP2), Y(tidP2), tidC, tisp, tidst
  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack)
  If Bom2F2 <> 0 Then
    Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), -Bom2F2, scalaxEl, tidC, tisp)
  End If
  'Fianco2 Fascia3
  tidP1 = 6
  tidP2 = 7
  tidC = CalcoloColore("iCon3F2H;iBom3F2H", , , tColore)
  tisp = IIf(tidC = vbRed, 2, 1)
  'linea x(tidP1), Y(tidP1), x(tidP2), Y(tidP2), tidC, tisp, tidst
  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack)
  If Bom3F2 <> 0 Then
    Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), -Bom3F2, scalaxEl, tidC, tisp)
  End If
  tidC = tColore
  tisp = 1
  'Fianco 2 Separazione Zone 1 e 2
  tidP1 = 12
  tidP2 = 13
  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, tidst)
  'Fianco 2 Separazione Zone 2 e 3
  tidP1 = 14
  tidP2 = 15
  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, tidst)
  'Fianco 1 Linea tra primo e ultimo punto
  tidP1 = 4
  tidP2 = 17
  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, vbDot)
  'Scritte F1 e F2
  Call TestoAll(LS, RS, "F1", , True, 1)
  Call TestoAll(LS, -RS, "F2", , True, 1)
  If NotScale Then Call TestoInfo("DRAWING NOT IN SCALE ! ", , True, vbRed, True)
  Call TestoInfo("Corr. Scale: " & scalaxEl, 1)
  Call TestoInfo("Units: [mm]", 3)
  If Ft2 > Ft Then
    Call TestoInfo("WARNING: (FT F1 = " & Ft & ") < (FT F2 = " & Ft2 & ")", , True, vbRed, True)
    Call QuotaH(MaxX1, Pos, MaxX2, Pos, "?", , vbRed, 3)
  ElseIf Ft2 < Ft Then
    Call TestoInfo("WARNING: (FT F1 = " & Ft & ") > (FT F2 = " & Ft2 & ")", , True, vbRed, True)
    Call QuotaH(MaxX2, -Pos, MaxX1, -Pos, "?", , vbRed, 3)
  End If


End Sub

Public Sub Disegna_Mola_MOLAVITE(Stampante As Boolean)

Dim NotScale       As Boolean
Dim PX()           As Double
Dim PY()           As Double
Dim HTotProfilo    As Double
Dim Angrullo       As Double
Dim RaggioTesta    As Double
Dim SpeTesta       As Double
Dim MargXDx        As Integer
Dim HL1            As Double
Dim HL2            As Double
Dim HParz          As Double
Dim MargXSx        As Double
Dim LSpallette     As Double
Dim SpessoreRullo  As Double

Dim ShiftBias      As Double
Dim AT_DRESSING%, CORR_TW_KIND%
Dim TipoCiclo%, iLep%, i%
Dim Xentrata#, Xuscita#

Dim HRasamento     As Double
Dim MargYSu        As Double
Dim MargYGiu       As Double
Dim SpeBaseProfilo As Double
Dim SpeTestaP14    As Double
Dim HL14           As Double
Dim HL15           As Double
Dim Distanziale    As Double
Dim Laprof         As Double
Dim CorP           As Double
Dim BB0            As Double
Dim CC0            As Double
Dim bb1            As Double
Dim CC1            As Double
Dim RagTmp         As Double
Dim PTmp           As Double
Dim PassoM         As Double
Dim ShiftY         As Double
Dim SemiPassoM     As Double
Dim Radius         As Double
Dim Scala_X        As Double
Dim Scala_Y        As Double
Dim X_C            As Double
Dim Y_C            As Double
Dim X_1            As Double
Dim Y_1            As Double
Dim X_2            As Double
Dim Y_2            As Double
Dim DirShiftMola   As Integer
Dim SensoFiletto   As Double
Dim LMola          As Double
Dim Shf_IN         As Double
Dim Shf_OUT        As Double
Dim Lung_Utile     As Double
Dim POS_IN         As Double
Dim POS_OUT        As Double
Dim POS_SH         As Double
Dim X_IN           As Double
Dim X_OUT          As Double
Dim LU_X           As Double
Dim LPOS           As Double
Dim XPOS           As Double
'
On Error GoTo errDisegna_Mola_MOLAVITE
  '
'CORR_TW_KIND = Lp("CORR_TW_KIND")
'If (CORR_TW_KIND = 0) Then
' Call DB_GUD_AT_Azzera
'Else
' Call DB_GUD_AT_Aggiorna
'End If
AT_DRESSING = Lp("AT_DRESSING")
  '
  For i = 1 To 4
      iLep = (i - 1) * 20
      TipoCiclo = LEP("CYC[0," & iLep + 1 & "]")       ' Tipo ciclo: (0;1;2;10) = (NULLO,UNIDIREZIONALE;BIDIREZIONALE;BIAS)
      If (AT_DRESSING = 0 And TipoCiclo = 10) Then TipoCiclo = 0: Call SP("CYC[0," & iLep + 1 & "]", TipoCiclo)
  Next i
  '
  Call InizializzaVariabili
  '
  NotScale = False

  ReDim PX(100): ReDim PY(100)
   
  CorP = 0.15
  PAR.MaxX = 50
  PAR.MaxY = 50
  PAR.MinX = 0
  PAR.MinY = 0

  PAR.ScalaMargine = 0.9
  PAR.Allineamento = 0
  Call CalcolaParPic(PAR, True, Stampante)
  
  PX(0) = PAR.MinX + 5: PX(1) = PAR.MaxX + 2: PY(0) = PAR.MaxY / 2: PY(1) = PY(0)
    
  'linea centrale
  Call linea(PX(0), PY(0), PX(1), PY(1), vbBlue, , vbDot)

  'Mola Standard
  SpessoreRullo = 6
  ShiftBias = 8
  HTotProfilo = 5
  Angrullo = 20
  RaggioTesta = 1
  HRasamento = 0
  MargYSu = 3
  MargYGiu = 3
  MargXDx = 5
  Distanziale = 5
  Laprof = 5
  
  
  SpeTesta = 2 * RaggioTesta * Cos(Angrullo * PG / 180)
  HL1 = RaggioTesta * Sin(Angrullo * PG / 180)
  HL2 = RaggioTesta - HL1
  HParz = HTotProfilo - HL2
  SpeBaseProfilo = 2 * HParz * Tan(Angrullo * PG / 180) + SpeTesta
  SpeTestaP14 = 2 * RaggioTesta * Sin((90 - Angrullo) / 2 * PG / 180)
  HL14 = RaggioTesta * Cos((90 - Angrullo) / 2 * PG / 180)
  HL15 = RaggioTesta - HL14
  
  PX(11) = PAR.MaxX / 2 + SpeBaseProfilo / 2
  PX(10) = PAR.MaxX / 2 + SpessoreRullo / 2
  PX(9) = PX(10)
  PY(10) = PAR.MaxY - (MargYSu + HTotProfilo + HRasamento)
  PY(9) = PY(10) - MargYGiu
  
  PY(11) = PY(10)
  PY(12) = PAR.MaxY - (MargYSu + HTotProfilo)
  PX(12) = PX(11)
  PY(13) = PAR.MaxY - (MargYSu + HL2)
  PX(13) = PAR.MaxX / 2 + SpeTesta / 2
  PX(14) = PAR.MaxX / 2 + SpeTestaP14 / 2
  PY(14) = PAR.MaxY - (MargYSu + HL15)
  PX(15) = PAR.MaxX / 2
  PY(15) = PAR.MaxY - MargYSu
  PX(16) = PAR.MaxX / 2 - SpeTestaP14 / 2
  PY(16) = PAR.MaxY - (MargYSu + HL15)
  PY(17) = PAR.MaxY - (MargYSu + HL2)
  PX(17) = PAR.MaxX / 2 - SpeTesta / 2
  
  PX(19) = PAR.MaxX / 2 - SpeBaseProfilo / 2
  PX(20) = PAR.MaxX / 2 - SpessoreRullo / 2
  PX(21) = PX(20)
  PY(20) = PAR.MaxY - (MargYSu + HTotProfilo + HRasamento)
  PY(21) = PY(20) - MargYGiu
  
  PY(19) = PY(20)
  PY(18) = PAR.MaxY - (MargYSu + HTotProfilo)
  PX(18) = PX(19)
        
  'Profilo inferiore
  PY(60) = PAR.MaxY / 2 - (PY(10) - PAR.MaxY / 2)
  PY(62) = PAR.MaxY / 2 - (PY(12) - PAR.MaxY / 2)
  PY(63) = PAR.MaxY / 2 - (PY(13) - PAR.MaxY / 2)
  PY(64) = PAR.MaxY / 2 - (PY(14) - PAR.MaxY / 2)
  PY(65) = PAR.MaxY / 2 - (PY(15) - PAR.MaxY / 2)
  PY(66) = PAR.MaxY / 2 - (PY(16) - PAR.MaxY / 2)
  PY(67) = PAR.MaxY / 2 - (PY(17) - PAR.MaxY / 2)
  PY(68) = PAR.MaxY / 2 - (PY(18) - PAR.MaxY / 2)
  PY(70) = PAR.MaxY / 2 - (PY(20) - PAR.MaxY / 2)

  'Ribalto i profili superiore e inferiore
  ShiftY = PY(10) - PY(65)
  'Coordinate Y profilo inferiore
  PY(10) = PY(10) - ShiftY
  PY(12) = PY(12) - ShiftY
  PY(13) = PY(13) - ShiftY
  PY(14) = PY(14) - ShiftY
  PY(15) = PY(15) - ShiftY
  PY(16) = PY(16) - ShiftY
  PY(17) = PY(17) - ShiftY
  PY(18) = PY(18) - ShiftY
  PY(20) = PY(20) - ShiftY
  
  
  'Coordinate Y profilo superiore
  PY(60) = PY(60) + ShiftY
  PY(62) = PY(62) + ShiftY
  PY(63) = PY(63) + ShiftY
  PY(64) = PY(64) + ShiftY
  PY(65) = PY(65) + ShiftY
  PY(66) = PY(66) + ShiftY
  PY(67) = PY(67) + ShiftY
  PY(68) = PY(68) + ShiftY
  PY(70) = PY(70) + ShiftY
  
  PX(71) = PX(15) + (6 / 2) * SpessoreRullo
  PY(71) = PY(65)
  PX(72) = PX(15) - (6 / 2) * SpessoreRullo
  PY(72) = PY(65)
  
  PX(73) = PX(20) - (5 / 2) * SpessoreRullo
  PY(73) = PY(20)
  PX(74) = PX(20) + (7 / 2) * SpessoreRullo
  PY(74) = PY(20)


  SemiPassoM = SpessoreRullo / 2
  
  'Profilo Inferiore
  'Disegna Fianco 1
  Call linea(PX(10) + SemiPassoM, PY(10), PX(12) + SemiPassoM, PY(12), vbBlack)
  Call linea(PX(12) + SemiPassoM, PY(12), PX(13) + SemiPassoM, PY(13), vbBlack)
  'Raggio
  Call linea(PX(13) + SemiPassoM, PY(13), PX(14) + SemiPassoM, PY(14), vbBlue)
  Call linea(PX(14) + SemiPassoM, PY(14), PX(15) + SemiPassoM, PY(15), vbBlue)

  'Disegna Fianco 2
  Call linea(PX(20) + SemiPassoM, PY(20), PX(18) + SemiPassoM, PY(18), vbBlack)
  Call linea(PX(18) + SemiPassoM, PY(18), PX(17) + SemiPassoM, PY(17), vbBlack)
  'Raggio
  Call linea(PX(17) + SemiPassoM, PY(17), PX(16) + SemiPassoM, PY(16), vbBlue)
  Call linea(PX(16) + SemiPassoM, PY(16), PX(15) + SemiPassoM, PY(15), vbBlue)

  'Profilo Superiore
  'Disegna Fianco 1
  Call linea(PX(10), PY(60), PX(12), PY(62), vbBlack)
  Call linea(PX(12), PY(62), PX(13), PY(63), vbBlack)
  'Raggio
  Call linea(PX(13), PY(63), PX(14), PY(64), vbBlue)
  Call linea(PX(14), PY(64), PX(15), PY(65), vbBlue)

  'Disegna Fianco 2
  Call linea(PX(20), PY(70), PX(18), PY(68), vbBlack)
  Call linea(PX(18), PY(68), PX(17), PY(67), vbBlack)
  'Raggio
  Call linea(PX(17), PY(67), PX(16), PY(66), vbBlue)
  Call linea(PX(16), PY(66), PX(15), PY(65), vbBlue)

  
   PassoM = SpessoreRullo
   
 'Profilo Inferiore
  
  For i = 1 To 2
  'Ripeti Fianco 1 a destra 2 volte
  'Disegna Fianco 1
  Call linea(PX(10) + SemiPassoM + i * PassoM, PY(10), PX(12) + SemiPassoM + i * PassoM, PY(12), vbBlack)
  Call linea(PX(12) + SemiPassoM + i * PassoM, PY(12), PX(13) + SemiPassoM + i * PassoM, PY(13), vbBlack)
  'Raggio
  Call linea(PX(13) + SemiPassoM + i * PassoM, PY(13), PX(14) + SemiPassoM + i * PassoM, PY(14), vbBlue)
  Call linea(PX(14) + SemiPassoM + i * PassoM, PY(14), PX(15) + SemiPassoM + i * PassoM, PY(15), vbBlue)
  
  'Ripeti Fianco 2 a destra 2 volte
  'Disegna Fianco 2
  Call linea(PX(20) + SemiPassoM + i * PassoM, PY(20), PX(18) + SemiPassoM + i * PassoM, PY(18), vbBlack)
  Call linea(PX(18) + SemiPassoM + i * PassoM, PY(18), PX(17) + SemiPassoM + i * PassoM, PY(17), vbBlack)
  'Raggio
  Call linea(PX(17) + SemiPassoM + i * PassoM, PY(17), PX(16) + SemiPassoM + i * PassoM, PY(16), vbBlue)
  Call linea(PX(16) + SemiPassoM + i * PassoM, PY(16), PX(15) + SemiPassoM + i * PassoM, PY(15), vbBlue)
  Next i
  
  For i = 1 To 3
  'Ripeti Fianco 1 a sinistra 3 volte
  'Disegna Fianco 1
  Call linea(PX(10) + SemiPassoM - i * PassoM, PY(10), PX(12) + SemiPassoM - i * PassoM, PY(12), vbBlack)
  Call linea(PX(12) + SemiPassoM - i * PassoM, PY(12), PX(13) + SemiPassoM - i * PassoM, PY(13), vbBlack)
  'Raggio
  Call linea(PX(13) + SemiPassoM - i * PassoM, PY(13), PX(14) + SemiPassoM - i * PassoM, PY(14), vbBlue)
  Call linea(PX(14) + SemiPassoM - i * PassoM, PY(14), PX(15) + SemiPassoM - i * PassoM, PY(15), vbBlue)
  
  'Ripeti Fianco 2 a sinistra 3 volte
  'Disegna Fianco 2
  Call linea(PX(20) + SemiPassoM - i * PassoM, PY(20), PX(18) + SemiPassoM - i * PassoM, PY(18), vbBlack)
  Call linea(PX(18) + SemiPassoM - i * PassoM, PY(18), PX(17) + SemiPassoM - i * PassoM, PY(17), vbBlack)
  'Raggio
  Call linea(PX(17) + SemiPassoM - i * PassoM, PY(17), PX(16) + SemiPassoM - i * PassoM, PY(16), vbBlue)
  Call linea(PX(16) + SemiPassoM - i * PassoM, PY(16), PX(15) + SemiPassoM - i * PassoM, PY(15), vbBlue)
  Next i



 'Profilo Superiore

  For i = 1 To 3
  'Ripeti Fianco 2 a destra 3 volte
  'Disegna Fianco 2
  Call linea(PX(20) + i * PassoM, PY(70), PX(18) + i * PassoM, PY(68), vbBlack)
  Call linea(PX(18) + i * PassoM, PY(68), PX(17) + i * PassoM, PY(67), vbBlack)
  'Raggio
  Call linea(PX(17) + i * PassoM, PY(67), PX(16) + i * PassoM, PY(66), vbBlue)
  Call linea(PX(16) + i * PassoM, PY(66), PX(15) + i * PassoM, PY(65), vbBlue)


  'Ripeti Fianco 1 a sinistra 3 volte
  'Disegna Fianco 1
  Call linea(PX(10) - i * PassoM, PY(60), PX(12) - i * PassoM, PY(62), vbBlack)
  Call linea(PX(12) - i * PassoM, PY(62), PX(13) - i * PassoM, PY(63), vbBlack)
  'Raggio
  Call linea(PX(13) - i * PassoM, PY(63), PX(14) - i * PassoM, PY(64), vbBlue)
  Call linea(PX(14) - i * PassoM, PY(64), PX(15) - i * PassoM, PY(65), vbBlue)
  Next i
  
  For i = 1 To 2
  'Ripeti Fianco 1 a destra 2 volte
  'Disegna Fianco 1
  Call linea(PX(10) + i * PassoM, PY(60), PX(12) + i * PassoM, PY(62), vbBlack)
  Call linea(PX(12) + i * PassoM, PY(62), PX(13) + i * PassoM, PY(63), vbBlack)
  'Raggio
  Call linea(PX(13) + i * PassoM, PY(63), PX(14) + i * PassoM, PY(64), vbBlue)
  Call linea(PX(14) + i * PassoM, PY(64), PX(15) + i * PassoM, PY(65), vbBlue)
  
  'Ripeti Fianco 2 a sinistra 2 volte
  'Disegna Fianco 2
  Call linea(PX(20) - i * PassoM, PY(70), PX(18) - i * PassoM, PY(68), vbBlack)
  Call linea(PX(18) - i * PassoM, PY(68), PX(17) - i * PassoM, PY(67), vbBlack)
  'Raggio
  Call linea(PX(17) - i * PassoM, PY(67), PX(16) - i * PassoM, PY(66), vbBlue)
  Call linea(PX(16) - i * PassoM, PY(66), PX(15) - i * PassoM, PY(65), vbBlue)
    
  Next i

  'Linee Laterali Mola
  Call linea(PX(71), PY(71), PX(74), PY(74), vbBlack, 1)
  Call linea(PX(72), PY(72), PX(73), PY(73), vbBlack, 1)
  
  'Filetti
  PX(75) = PX(15) - (5 / 2) * SpessoreRullo
  PX(77) = PX(15) - (3 / 2) * SpessoreRullo
  PX(79) = PX(15) - (1 / 2) * SpessoreRullo
  PX(84) = PX(15) + (5 / 2) * SpessoreRullo
  PX(82) = PX(15) + (3 / 2) * SpessoreRullo
  PX(80) = PX(15) + (1 / 2) * SpessoreRullo
  
  PX(76) = PX(15) - (4 / 2) * SpessoreRullo
  PX(78) = PX(15) - (2 / 2) * SpessoreRullo
  PX(81) = PX(15) + (2 / 2) * SpessoreRullo
  PX(83) = PX(15) + (4 / 2) * SpessoreRullo

  
  SensoFiletto = Lp("SENSOFIL_G")
  
  If SensoFiletto = 0 Then
  'Filetto destro
    Call linea(PX(72), PY(72), PX(75), PY(15), CalcoloColore("SENSOFIL_G"), 1)
    Call linea(PX(76), PY(65), PX(77), PY(15), CalcoloColore("SENSOFIL_G"), 1)
    Call linea(PX(78), PY(65), PX(79), PY(15), CalcoloColore("SENSOFIL_G"), 1)
    Call linea(PX(15), PY(65), PX(80), PY(15), CalcoloColore("SENSOFIL_G"), 1)
    Call linea(PX(81), PY(65), PX(82), PY(15), CalcoloColore("SENSOFIL_G"), 1)
    Call linea(PX(83), PY(65), PX(84), PY(15), CalcoloColore("SENSOFIL_G"), 1)
  Else
    If SensoFiletto = 1 Then
    'Filetto sinistro
    Call linea(PX(76), PY(65), PX(75), PY(15), CalcoloColore("SENSOFIL_G"), 1)
    Call linea(PX(78), PY(65), PX(77), PY(15), CalcoloColore("SENSOFIL_G"), 1)
    Call linea(PX(15), PY(65), PX(79), PY(15), CalcoloColore("SENSOFIL_G"), 1)
    Call linea(PX(81), PY(65), PX(80), PY(15), CalcoloColore("SENSOFIL_G"), 1)
    Call linea(PX(83), PY(65), PX(82), PY(15), CalcoloColore("SENSOFIL_G"), 1)
    Call linea(PX(71), PY(65), PX(84), PY(15), CalcoloColore("SENSOFIL_G"), 1)
    End If
  End If

'Larghezza Mola
  Call linea(PX(74), PY(74), PX(74), PY(74) - 5.5, vbBlack, 1, vbDot)
  Call linea(PX(73), PY(73), PX(73), PY(73) - 5.5, vbBlack, 1, vbDot)
  Call QuotaH(PX(73), PY(73) - 5, PX(74), PY(74) - 5, "LM", 12, CalcoloColore("LARGHMOLA_G"), 3, 1, 0)
'Diametro Mola
  Call linea(PX(72) - 8, PY(60), PX(72) + 3, PY(60), vbBlack, 1, vbDot)
  Call linea(PX(73) - 8, PY(73), PX(73), PY(73), vbBlack, 1, vbDot)
  Call QuotaV(PX(72) - 8, PY(73), PX(72) - 8, PY(60), "Dm", 12, CalcoloColore("DIAMMOLA_G"), 5, 0)

' Spina porta mola
  PY(85) = (PY(65) - PAR.MaxY / 2) / 2 + PAR.MaxY / 2
  PY(86) = -(PY(65) - PAR.MaxY / 2) / 2 + PAR.MaxY / 2
  'lato dx
  Call linea(PX(71), PY(85), PX(71) + 7, PY(85), vbBlue, 2, 0)
  Call linea(PX(71), PY(86), PX(71) + 7, PY(86), vbBlue, 2, 0)
  Call linea(PX(71) + 7, PY(86), PX(71) + 7, PY(85), vbBlue, 2, 0)
  Call linea(PX(71) + 0.2, PY(86) - 1, PX(71) + 0.2, PY(85) + 1, vbBlue, 2, 0)
  'HSK
  PY(87) = PAR.MaxY / 2 + (PY(85) - PAR.MaxY / 2) / 2
  PY(88) = PAR.MaxY / 2 - (PY(85) - PAR.MaxY / 2) / 2
  Call linea(PX(71) + 7, PY(87), PX(71) + 8.5, PY(87), vbBlue, 2, 0)
  Call linea(PX(71) + 7, PY(88), PX(71) + 8.5, PY(88), vbBlue, 2, 0)
  Call linea(PX(71) + 8.5, PY(88), PX(71) + 8.5, PY(87), vbBlue, 2, 0)
  'lato sx
  Call linea(PX(72), PY(85), PX(72) - 1, PY(85), vbBlue, 2, 0)
  Call linea(PX(72), PY(86), PX(72) - 1, PY(86), vbBlue, 2, 0)
  Call linea(PX(72) - 1, PY(86), PX(72) - 1, PY(85), vbBlue, 2, 0)
  Call linea(PX(72), PY(86) - 1, PX(72), PY(85) + 1, vbBlue, 2, 0)
  
' Posizione Mola (W)
  Call linea(PX(71) + 7, PY(85), PX(71) + 7, PY(60), vbBlack, 1, vbDot)
  Call QuotaH(PX(71), PY(71) - 2, PX(71) + 7, PY(71) - 2, "W", 12, CalcoloColore("WMOLA_G"), 4, 1, 0)
  
'Diametro Minimo Mola
  Call linea(PX(72) - 4, PY(85) + 1.5, PX(72), PY(85) + 1.5, vbBlack, 1, vbDot)
  Call linea(PX(73) - 4, PY(86) - 1.5, PX(73), PY(86) - 1.5, vbBlack, 1, vbDot)
  Call QuotaV(PX(73) - 3.5, PY(86) - 1.5, PX(73) - 3.5, PY(85) + 1.5, "Dmin", 12, CalcoloColore("DIAMUTMIN_G"), 5, 0)

'Rif. Zero Y
  Radius = 2
  Scala_X = 1
  Scala_Y = 1
  For i = 1 To 9
  X_C = PX(71) + 7
  Y_C = PY(60)
  X_1 = X_C - Radius
  Y_1 = PY(60)
  X_2 = X_C
  Y_2 = PY(60) + Radius
  'Arco in senso Orario
  Call Disegna_Arco(X_C, Y_C, Radius, X_1, Y_1, X_2, Y_2, False, 9, Scala_X, vbRed, 2, Scala_Y)
  X_1 = X_C
  Y_1 = PY(60) + Radius
  X_2 = X_C + Radius
  Y_2 = PY(60)
  Call Disegna_Arco(X_C, Y_C, Radius, X_1, Y_1, X_2, Y_2, False, 9, Scala_X, vbBlack, 2, Scala_Y)
  X_1 = X_C + Radius
  Y_1 = PY(60)
  X_2 = X_C
  Y_2 = PY(60) - Radius
  Call Disegna_Arco(X_C, Y_C, Radius, X_1, Y_1, X_2, Y_2, False, 9, Scala_X, vbRed, 2, Scala_Y)
  X_1 = X_C
  Y_1 = PY(60) - Radius
  X_2 = X_C - Radius
  Y_2 = PY(60)
  Call Disegna_Arco(X_C, Y_C, Radius, X_1, Y_1, X_2, Y_2, False, 9, Scala_X, vbBlack, 2, Scala_Y)
  Radius = Radius - 0.2
  Next i
  
  Radius = 2
  X_1 = X_C - Radius
  Y_1 = PY(60)
  X_2 = X_C + Radius
  Y_2 = PY(60)
  Call linea(X_1, Y_1, X_2, Y_2, vbBlack, 2, 0)

  X_1 = X_C
  Y_1 = PY(60) - Radius
  X_2 = X_C
  Y_2 = PY(60) + Radius
  Call linea(X_1, Y_1, X_2, Y_2, vbBlack, 2, 0)

'Shift inizio/fine mola
  
  Call linea(PX(74), PY(74), PX(74), PY(74) - 2, vbBlack, 1, vbDot)
  Call linea(PX(74) - SpessoreRullo, PY(74) - 2, PX(74) - SpessoreRullo, PY(60) + 4, vbBlack, 1, vbDot)
  Call QuotaH(PX(74) - SpessoreRullo, PY(74) - 1, PX(74), PY(74) - 1, "Shf_I", 12, CalcoloColore("iSHIFTIN_G"), 4, 1, 0)

  Call linea(PX(73), PY(73), PX(73), PY(73) - 2, vbBlack, 1, vbDot)
  Call linea(PX(73) + SpessoreRullo, PY(73) - 2, PX(73) + SpessoreRullo, PY(60) + 8, vbBlack, 1, vbDot)
  Call QuotaH(PX(73), PY(73) - 1, PX(73) + SpessoreRullo, PY(73) - 1, "Shf_O", 12, CalcoloColore("iSHIFTOUT_G"), 4, 1, 0)

'Pos Bias Shift MOla
If (AT_DRESSING = 1) Then
 Call linea(PX(73) + SpessoreRullo + ShiftBias, PY(73) - 2, PX(73) + SpessoreRullo + ShiftBias, PY(60) + 8, vbBlack, 1, vbDot)
 Call QuotaH(PX(73) + SpessoreRullo, PY(73) - 1, PX(73) + SpessoreRullo + ShiftBias, PY(73) - 1, "Shf_B", 12, CalcoloColore("iSHIFTOUT_G"), 4, 1, 0)
End If

'Linea zero Y
  X_1 = X_C
  Y_1 = PY(60) + Radius
  X_2 = X_C
  Y_2 = PY(60) + 5
  Call linea(X_1, Y_1, X_2, Y_2 + 4, vbBlack, 1, vbDot)
              
              Shf_IN = Lp("iSHIFTIN_G")
             Shf_OUT = Lp("iSHIFTOUT_G")

             Xentrata = LEP("CORSAIN")                'Tratto Z entrata
             Xuscita = LEP("CORSAOUT")               'Tratto Z uscita
        DirShiftMola = Lp("DirShift")
              POS_IN = Lp("POSINIZIOSH_G")
             POS_OUT = Lp("POSFINESH_G")
              POS_SH = Lp("POSSHIFT_G[2]")

If DirShiftMola < 0 Then
'Pos Inizio Shift MOla
  Call QuotaH(PX(71) - SpessoreRullo, Y_2 - 1, X_2, Y_2 - 1, "Pos_I", 12, CalcoloColore("POSINIZIOSH_G"), 1, 1, 1)
  X_IN = PX(71) - SpessoreRullo
'Pos Fine Shift MOla
 If (AT_DRESSING = 1) Then
    Call QuotaH(PX(72) + SpessoreRullo + ShiftBias * AT_DRESSING, Y_2 + 3, X_2, Y_2 + 3, "Pos_O", 12, CalcoloColore("POSFINESH_G"), 1, 1, 1)
    X_OUT = PX(72) + SpessoreRullo + ShiftBias * AT_DRESSING
 Else
    Call QuotaH(PX(72) + SpessoreRullo, Y_2 + 3, X_2, Y_2 + 3, "Pos_O", 12, CalcoloColore("POSFINESH_G"), 1, 1, 1)
    X_OUT = PX(72) + SpessoreRullo
 End If
 LU_X = X_IN - X_OUT
 LPOS = POS_IN - POS_SH
Else
'Pos Fine Shift MOla
  Call QuotaH(PX(71) - SpessoreRullo, Y_2 - 1, X_2, Y_2 - 1, "Pos_O", 12, CalcoloColore("POSFINESH_G"), 1, 1, 1)
  X_OUT = PX(71) - SpessoreRullo
'Pos Inizio Shift MOla
 If (AT_DRESSING = 1) Then
    Call QuotaH(PX(72) + SpessoreRullo + ShiftBias * AT_DRESSING, Y_2 + 3, X_2, Y_2 + 3, "Pos_I", 12, CalcoloColore("POSINIZIOSH_G"), 1, 1, 1)
    X_IN = PX(72) + SpessoreRullo + ShiftBias * AT_DRESSING
 Else
    Call QuotaH(PX(72) + SpessoreRullo, Y_2 + 3, X_2, Y_2 + 3, "Pos_I", 12, CalcoloColore("POSINIZIOSH_G"), 1, 1, 1)
    X_IN = PX(72) + SpessoreRullo
 End If
 LU_X = X_OUT - X_IN
 LPOS = POS_SH - POS_IN
End If

'Altezza Filetto Mola
  Call QuotaV(PX(15), PY(65), PX(15), PY(60), "H", 12, CalcoloColore("HDENTEMOLA_G"), 5, 0)

'Posizione Shift

 LMola = Lp("LARGHMOLA_G")
 If (AT_DRESSING = 1) Then
    Lung_Utile = Abs(POS_OUT - POS_IN)
 Else
    Lung_Utile = LMola - Shf_IN - Shf_OUT
 End If
 If LPOS <> 0 Then
    XPOS = frmt(X_IN + DirShiftMola * (LPOS / Lung_Utile * LU_X), 1)
 Else
    XPOS = X_IN
 End If
  
'Y_2 = PY(60) + 5
 
If DirShiftMola < 0 Then
   '
   Call linea(XPOS, PY(60), XPOS, Y_2 + 3, CalcoloColore("POSSHIFT_G[2]", , , vbBlue), 2, 0)
   ' |<-- PosSh
   Call QuotaH(XPOS, Y_2 + 1, XPOS + 2, Y_2 + 1, "PosSh", 12, CalcoloColore("POSSHIFT_G[2]", , , vbBlue), 2, 1, 1)
   If VarCorr = "DIRSHIFT" Then
      Call QuotaH(XPOS, Y_2 + 1, XPOS + 2, Y_2 + 1, "PosSh", 12, CalcoloColore("DIRSHIFT"), 2, 1, 1)
   End If
   If (AT_DRESSING = 1) Then
    Call linea(PX(72) + SpessoreRullo + ShiftBias, PY(60), PX(72) + SpessoreRullo + ShiftBias, Y_2 + 3, CalcoloColore("POSSHIFT_G[2]", , , vbBlue), 2, 0)
   ' PosShB-->|
    Call QuotaH(PX(72) + SpessoreRullo + ShiftBias, Y_2 + 1, PX(72) + SpessoreRullo + ShiftBias + 2, Y_2 + 1, "PosShB", 12, CalcoloColore("POSSHIFT_G[2]", , , vbBlue), 2, 1, 1)
    If VarCorr = "DIRSHIFT" Then
 '     Call QuotaH(XPOS - 2 - ShiftBias, Y_2 + 1, XPOS - ShiftBias, Y_2 + 1, "PosShB", 12, CalcoloColore("DIRSHIFT"), 1, 1, 2)
    End If
   End If
Else
   '
   Call linea(XPOS, PY(60), XPOS, Y_2 + 3, CalcoloColore("POSSHIFT_G[2]", , , vbBlue), 2, 0)
   ' PosSh -->|
   Call QuotaH(XPOS - 2, Y_2 + 1, XPOS, Y_2 + 1, "PosSh", 12, CalcoloColore("POSSHIFT_G[2]", , , vbBlue), 1, 1, 2)
   If VarCorr = "DIRSHIFT" Then
      Call QuotaH(XPOS - 2, Y_2 + 1, XPOS, Y_2 + 1, "PosSh", 12, CalcoloColore("DIRSHIFT"), 1, 1, 2)
   End If
   If (AT_DRESSING = 1) Then
    Call linea(X_IN - ShiftBias, PY(60), X_IN - ShiftBias, Y_2 + 3, CalcoloColore("POSSHIFT_G[2]", , , vbBlue), 2, 0)
   ' PosShB-->| XPOS
    Call QuotaH(X_IN - 2 - ShiftBias, Y_2 + 1, X_IN - ShiftBias, Y_2 + 1, "PosShB", 12, CalcoloColore("POSSHIFT_G[2]", , , vbBlue), 1, 1, 2)
    If VarCorr = "DIRSHIFT" Then
 '     Call QuotaH(XPOS - 2 - ShiftBias, Y_2 + 1, XPOS - ShiftBias, Y_2 + 1, "PosShB", 12, CalcoloColore("DIRSHIFT"), 1, 1, 2)
    End If
   End If
End If



'Calcolo Passo mola e inclinazione

Dim parDiametroMola As Double
Dim parCorrFha1     As Double
Dim parCorrFha2     As Double
Dim parInclFiletto  As Double
Dim parPasso        As Double
Dim NumPrinc        As Double
Dim ModUT           As Double
Dim AlfUT           As Double
Dim HFiletto        As Double
Dim DmolaInt        As Double

NumPrinc = LEP("NUMPRINC_G")
ModUT = LEP("MNUT_G")
AlfUT = LEP("ALFAUT_G") * PG / 180
HFiletto = LEP("HDENTEMOLA_G")
DmolaInt = 2 * val(OPC_LEGGI_DATO("/ACC/NCK/GUD7/RFONDOMOLA_G"))

If DmolaInt > 0 Then
   parDiametroMola = DmolaInt + HFiletto * 2 - ModUT * 2
Else
  'Modifica temporanea
   DmolaInt = 250
   parDiametroMola = DmolaInt + HFiletto * 2 - ModUT * 2
End If
'Corr. fHa
parCorrFha1 = -(LEP("CORFHA1"))
parCorrFha2 = -(LEP("CORFHA2"))

Call CalcoloPasso_Molavite(DGlob.Z, DGlob.Mn, DGlob.ALFA, DGlob.Beta, DGlob.SAP, DGlob.EAP, _
                          NumPrinc, ModUT, AlfUT, parDiametroMola, _
                          parCorrFha1, parCorrFha2, parInclFiletto, parPasso)



Call TestoInfo("Pitch : " & frmt(parPasso, 3), 4)
Call TestoInfo("Tread angle : " & frmt(parInclFiletto, 3), 3)


Exit Sub

errDisegna_Mola_MOLAVITE:
  MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Sub: Disegna_Mola_MOLAVITE"
  Exit Sub

End Sub

Public Sub Disegna_Rullo_Molavite(Stampante As Boolean)

Dim NotScale      As Boolean
Dim PX()          As Double
Dim PY()          As Double
Dim HTotProfilo   As Double
Dim Angrullo      As Double
Dim RaggioTesta   As Double
Dim SpeTesta      As Double
Dim MargXDx       As Integer
Dim HL1           As Double
Dim HL2           As Double
Dim HParz         As Double
Dim MargXSx       As Double
Dim LSpallette    As Double
Dim SpessoreRullo As Double
Dim HRasamento    As Double
Dim MargYSu       As Double
Dim MargYGiu       As Double
Dim SpeBaseProfilo As Double
Dim SpeTestaP14    As Double
Dim HL14           As Double
Dim HL15           As Double
Dim Distanziale    As Double
Dim Laprof         As Double
Dim CorP           As Double
Dim BB0            As Double
Dim CC0            As Double
Dim bb1            As Double
Dim CC1            As Double
Dim RagTmp         As Double
Dim PTmp           As Double
Dim TipoRullo      As String
Dim DY12           As Double
Dim DY13           As Double
Dim DY14           As Double
Dim DY15           As Double
Dim DY16           As Double
Dim DY17           As Double
Dim DY18           As Double
Dim DXSpe          As Double
'
Dim X2 As Long
Dim Y2 As Long
Dim BorderColor As Variant


On Error GoTo errDisegna_Rullo_Molavite
  '
  Call InizializzaVariabili
  '
  NotScale = False

  ReDim PX(150): ReDim PY(150)
   
  CorP = 0.15
  PAR.MaxX = 50
  PAR.MaxY = 50
  PAR.MinX = 0
  PAR.MinY = 0

  PAR.ScalaMargine = 0.9
  PAR.Allineamento = 0
  Call CalcolaParPic(PAR, True, Stampante)
  
  PX(0) = PAR.MinX + 5: PX(1) = PAR.MaxX - 5: PY(0) = PAR.MaxY / 2: PY(1) = PY(0)
    
  'linea centrale
  'Call linea(PX(0), PY(0), PX(1), PY(1), vbBlack, , vbDot)
  
  
  'Test linea in alto
  'Call linea(Px(0), PAR.MaxY - 5, Px(1), PAR.MaxY - 5, vbRed)

  'Test linea in basso
  'Call linea(Px(0), PAR.MinY + 5, PAR.MaxX / 2, PAR.MinY + 5, vbRed)
  
  TipoRullo = Lp_Str("TIPORULLO_G")
  
  Select Case TipoRullo
  
  Case "1" 'Standard
  
  'Rullo Standard
  SpessoreRullo = 30
  HTotProfilo = 25
  Angrullo = 20
  RaggioTesta = 2
  HRasamento = 2
  MargYSu = 3
  MargYGiu = 5
  MargXDx = 5
  Distanziale = 5
  Laprof = 5
  
  
  SpeTesta = 2 * RaggioTesta * Cos(Angrullo * PG / 180)
  HL1 = RaggioTesta * Sin(Angrullo * PG / 180)
  HL2 = RaggioTesta - HL1
  HParz = HTotProfilo - HL2
  SpeBaseProfilo = 2 * HParz * Tan(Angrullo * PG / 180) + SpeTesta
  SpeTestaP14 = 2 * RaggioTesta * Sin((90 - Angrullo) / 2 * PG / 180)
  HL14 = RaggioTesta * Cos((90 - Angrullo) / 2 * PG / 180)
  HL15 = RaggioTesta - HL14
  
  PX(11) = PAR.MaxX / 2 + SpeBaseProfilo / 2
  PX(10) = PAR.MaxX / 2 + SpessoreRullo / 2
  PX(9) = PX(10)
  PY(10) = PAR.MaxY - (MargYSu + HTotProfilo + HRasamento)
  PY(9) = PY(10) - MargYGiu
  
  PY(11) = PY(10)
  PY(12) = PAR.MaxY - (MargYSu + HTotProfilo)
  PX(12) = PX(11)
  PY(13) = PAR.MaxY - (MargYSu + HL2)
  PX(13) = PAR.MaxX / 2 + SpeTesta / 2
  PX(14) = PAR.MaxX / 2 + SpeTestaP14 / 2
  PY(14) = PAR.MaxY - (MargYSu + HL15)
  PX(15) = PAR.MaxX / 2
  PY(15) = PAR.MaxY - MargYSu
  PX(16) = PAR.MaxX / 2 - SpeTestaP14 / 2
  PY(16) = PAR.MaxY - (MargYSu + HL15)
  PY(17) = PAR.MaxY - (MargYSu + HL2)
  PX(17) = PAR.MaxX / 2 - SpeTesta / 2
  
  PX(19) = PAR.MaxX / 2 - SpeBaseProfilo / 2
  PX(20) = PAR.MaxX / 2 - SpessoreRullo / 2
  PX(21) = PX(20)
  PY(20) = PAR.MaxY - (MargYSu + HTotProfilo + HRasamento)
  PY(21) = PY(20) - MargYGiu
  
  PY(19) = PY(20)
  PY(18) = PAR.MaxY - (MargYSu + HTotProfilo)
  PX(18) = PX(19)
  
  PY(42) = (PY(13) + PY(12)) / 2
  PX(42) = PX(13) + (PY(13) - PY(42)) * Tan(Angrullo * PG / 180)
  PY(41) = PY(42)
  PX(41) = PX(17) - (PY(17) - PY(41)) * Tan(Angrullo * PG / 180)
  
  'Disegna Fianco 1
  Call linea(PX(9), PY(9), PX(10), PY(10), vbBlack)
  Call linea(PX(10), PY(10), PX(11), PY(11), vbBlack)
  Call linea(PX(11), PY(11), PX(12), PY(12), vbBlack)
  Call linea(PX(12), PY(12), PX(13), PY(13), vbBlack)
  'Raggio
  Call linea(PX(13), PY(13), PX(14), PY(14), vbBlue)
  Call linea(PX(14), PY(14), PX(15), PY(15), vbBlue)

  'Disegna Fianco 2
  Call linea(PX(21) + CorP, PY(21), PX(20) + CorP, PY(20), vbBlack)
  Call linea(PX(20) + CorP, PY(20), PX(19), PY(19), vbBlack)
  Call linea(PX(19), PY(19), PX(18), PY(18), vbBlack)
  Call linea(PX(18), PY(18), PX(17), PY(17), vbBlack)
  'Raggio
  Call linea(PX(17), PY(17), PX(16), PY(16), vbBlue)
  Call linea(PX(16), PY(16), PX(15), PY(15), vbBlue)

  'Riempimento
  Call linea(PX(21) + CorP, PY(21), PX(9), PY(9), RGB(225, 240, 255))
  Call linea(PX(14), PY(14), PX(16), PY(16), RGB(225, 240, 255))
  
  PAR.Pic.FillStyle = 0
  PAR.Pic.FillColor = RGB(225, 240, 255) 'vbRed  'vbBlue
  
  If Stampante = False Then
  BorderColor = PAR.Pic.Point(2472, 1500)
  X2 = PAR.Pic.ScaleX(2472, PAR.Pic.ScaleMode, vbPixels)
  Y2 = PAR.Pic.ScaleY(1500, PAR.Pic.ScaleMode, vbPixels)
  ExtFloodFill PAR.Pic.hdc, X2, Y2, BorderColor, FLOODFILLSURFACE
  End If
  
  'Disegna Spessore e Rasamento Profilatore
  PX(22) = PX(20) - Distanziale
  PY(22) = PY(20)
  PX(23) = PX(22)
  PY(23) = PY(21)
  PX(24) = PX(22) - Laprof
  PY(24) = PY(20)
    
  'Distanziale
  Call linea(PX(20) - CorP, PY(20), PX(22) + CorP, PY(22), vbBlue, 2)
  Call linea(PX(22) + CorP, PY(22), PX(23) + CorP, PY(23), vbBlue, 2)
  Call linea(PX(20) - CorP, PY(20), PX(21) - CorP, PY(21), vbBlue, 2)
    
  'Profilatore
  Call linea(PX(22) - CorP, PY(22), PX(24), PY(24), vbBlack, 2)
  Call linea(PX(22) - CorP, PY(22), PX(23) - CorP, PY(23), vbBlack, 2)
  
  'Parametri
  'LR
  Call QuotaH(PX(20), PY(15), PX(15), PY(15), "LCR", 12, CalcoloColore("DIST_CR"), 3, 2, 0)
  Call linea(PX(21), PY(21), PX(21), PY(15), vbBlack, 1, vbDot)
  'Da Rullo
  Call QuotaV(PAR.MaxX, PY(9), PAR.MaxX, PY(15), "DaR", 12, CalcoloColore("DRULLOPROF_G"), , 1)
  Call linea(PX(15), PY(15), PAR.MaxX, PY(15), vbBlack, 1, vbDot)
  'S Distanziale
  Call QuotaH(PX(23), PY(15), PX(21), PY(15), "SR", 12, CalcoloColore("DISTANZRULLO"), 3, 2, 0)
  Call linea(PX(23), PY(23), PX(23), PY(15), vbBlack, 1, vbDot)
  'Spessore Profilo
  Call QuotaH(PX(41), PY(41), PX(42), PY(42), "L", 12, CalcoloColore("SP_RIF_RULLO"), 3, 2, 0)
  
  'Rag Rif Rullo
  Call QuotaV(PAR.MaxX - MargXDx, PY(9), PAR.MaxX - MargXDx, PY(42), "RefR", 12, CalcoloColore("RG_RIF_RULLO"), , 1)
  Call linea(PX(42), PY(42), PAR.MaxX - MargXDx, PY(42), vbBlack, 1, vbDot)
  
  'Rag Testa
  If VarCorr = "RG_RAC_TESTA_RULLO" Then
     Call linea(PX(15), PY(13) - HL1, PX(13), PY(13), vbRed, 2)
     Call linea(PX(15), PY(13) - HL1, PX(15), PY(15), vbRed, 2)
     Call linea(PX(17), PY(17), PX(16), PY(16), vbRed)
     Call linea(PX(16), PY(16), PX(15), PY(15), vbRed)
     Call linea(PX(13), PY(13), PX(14), PY(14), vbRed, 2)
     Call linea(PX(14), PY(14), PX(15), PY(15), vbRed, 2)
     Call QuotaH(PX(15), PY(13) - HL1, PX(15), PY(13) - HL1, "Rt", 12, CalcoloColore("RG_RAC_TESTA_RULLO"), 4, 2, 4)
  Else
     Call linea(PX(15), PY(13) - HL1, PX(13), PY(13), vbBlack, 1)
     Call linea(PX(15), PY(13) - HL1, PX(15), PY(15), vbBlack, 1)
     Call QuotaH(PX(15), PY(13) - HL1, PX(15), PY(13) - HL1, "Rt", 12, vbBlack, 4, 2, 4)
  End If
  
  'Ang Pressione
  PY(43) = (PY(42) + PY(12)) / 2
  PX(43) = PX(42)
  RagTmp = PY(42) - PY(43)
  BB0 = RagTmp * Sin(Angrullo * PG / 180)
  CC0 = RagTmp * Cos(Angrullo * PG / 180)
  PX(45) = PX(43) + BB0: PY(45) = PY(42) - CC0
  bb1 = RagTmp * Sin(Angrullo / 2 * PG / 180)
  CC1 = RagTmp * Cos(Angrullo / 2 * PG / 180)
  PX(44) = PX(43) + bb1: PY(44) = PY(42) - CC1

  If VarCorr = "AP_RULLO" Then
     Call linea(PX(42), PY(42), PX(42), PY(12), vbRed, 1, vbDot)
     Call linea(PX(43), PY(43), PX(44), PY(44), vbRed, 2, vbDot)
     Call linea(PX(44), PY(44), PX(45), PY(45), vbRed, 2, vbDot)
     Call QuotaH(PX(44), PY(44), PX(44), PY(44), "a", 12, vbRed, 4, 2, 4)
  Else
     Call linea(PX(42), PY(42), PX(42), PY(12), vbBlack, 1, vbDot)
     Call linea(PX(43), PY(43), PX(44), PY(44), vbBlack, 1)
     Call linea(PX(44), PY(44), PX(45), PY(45), vbBlack, 1)
     Call QuotaH(PX(44), PY(44), PX(44), PY(44), "a", 12, vbBlack, 4, 2, 4)
  End If
  
  'HRullo
  Call linea(PX(20), PY(18), PX(18), PY(18), vbBlack, 1, vbDot)
  PTmp = (PX(20) + PX(18)) / 2
  Call QuotaV(PTmp, PY(18), PTmp, PY(15), "HR", 12, CalcoloColore("H_RIF_RULLO"), , 0)
    
  Case "2" 'Full Form
  
  'Profilo Standard
  SpessoreRullo = 24 '30
  HTotProfilo = 20 '25
  Angrullo = 20
  RaggioTesta = 1.6 '2
  HRasamento = 2
  MargYSu = 3
  MargYGiu = 5
  MargXDx = 5
  Distanziale = 5
  Laprof = 4 '5
  
  
  SpeTesta = 2 * RaggioTesta * Cos(Angrullo * PG / 180)
  HL1 = RaggioTesta * Sin(Angrullo * PG / 180)
  HL2 = RaggioTesta - HL1
  HParz = HTotProfilo - HL2
  SpeBaseProfilo = 2 * HParz * Tan(Angrullo * PG / 180) + SpeTesta
  SpeTestaP14 = 2 * RaggioTesta * Sin((90 - Angrullo) / 2 * PG / 180)
  HL14 = RaggioTesta * Cos((90 - Angrullo) / 2 * PG / 180)
  HL15 = RaggioTesta - HL14
  
  PX(11) = PAR.MaxX / 2 + SpeBaseProfilo / 2
  PX(10) = PAR.MaxX / 2 + SpessoreRullo / 2
  PX(9) = PX(10)
  PY(10) = PAR.MaxY - (MargYSu + HTotProfilo + HRasamento)
  PY(9) = PY(10) - MargYGiu
  
  PY(11) = PY(10)
  PY(12) = PAR.MaxY - (MargYSu + HTotProfilo)
  PX(12) = PX(11)
  PY(13) = PAR.MaxY - (MargYSu + HL2)
  PX(13) = PAR.MaxX / 2 + SpeTesta / 2
  PX(14) = PAR.MaxX / 2 + SpeTestaP14 / 2
  PY(14) = PAR.MaxY - (MargYSu + HL15)
  PX(15) = PAR.MaxX / 2
  PY(15) = PAR.MaxY - MargYSu
  PX(16) = PAR.MaxX / 2 - SpeTestaP14 / 2
  PY(16) = PAR.MaxY - (MargYSu + HL15)
  PY(17) = PAR.MaxY - (MargYSu + HL2)
  PX(17) = PAR.MaxX / 2 - SpeTesta / 2
  
  PX(19) = PAR.MaxX / 2 - SpeBaseProfilo / 2
  PX(20) = PAR.MaxX / 2 - SpessoreRullo / 2
  PX(21) = PX(20)
  PY(20) = PAR.MaxY - (MargYSu + HTotProfilo + HRasamento)
  PY(21) = PY(20) - MargYGiu
  
  PY(19) = PY(20)
  PY(18) = PAR.MaxY - (MargYSu + HTotProfilo)
  PX(18) = PX(19)
  
  PY(42) = (PY(13) + PY(12)) / 2
  PX(42) = PX(13) + (PY(13) - PY(42)) * Tan(Angrullo * PG / 180)
  PY(41) = PY(42)
  PX(41) = PX(17) - (PY(17) - PY(41)) * Tan(Angrullo * PG / 180)
  
'Disegna Spessore e Rasamento Profilatore
  PX(22) = PX(20) - Distanziale
  PY(22) = PY(20)
  PX(23) = PX(22)
  PY(23) = PY(21)
  PX(24) = PX(22) - Laprof
  PY(24) = PY(20)
  
'Profilo Full Form
 'Punti Fianco 2
 DY12 = PY(12) - PAR.MaxY / 2
 PY(112) = PAR.MaxY / 2 + DY12 + HParz
 PY(113) = PY(13) - HParz
 PY(142) = PY(42)
 DY14 = PY(14) - PY(13)
 PY(114) = PY(113) - DY14
 DY15 = PY(15) - PY(14)
 PY(115) = PY(114) - DY15
  
 'Disegna Fianco 2
 Call linea(PX(12), PY(112), PX(13), PY(113), vbBlack)
 'Call linea(PX(13), PY(113), PX(13), PY(113) - 3, vbBlack)
 
  
 'Punti Fianco 1
 DY18 = PY(18) - PAR.MaxY / 2
 PY(118) = PAR.MaxY / 2 + DY18 + HParz
 PY(117) = PY(17) - HParz
 DY16 = PY(16) - PY(17)
 PY(116) = PY(117) - DY16
 PY(141) = PY(41)
 
 Call linea(PX(13), PY(113), PX(13), PY(118) - HTotProfilo, vbBlack)
 
 'Disegna Fianco 1
 Call linea(PX(18), PY(118), PX(17), PY(117), vbBlack)
 'Call linea(PX(17), PY(117), PX(17), PY(117) - 3, vbBlack)
 Call linea(PX(17), PY(117), PX(17), PY(118) - HTotProfilo, vbBlack)
 
 Call linea(PX(13), PY(118) - HTotProfilo, PX(17), PY(118) - HTotProfilo, vbBlack)
 
 
 DXSpe = PX(42) - PX(41)
 Call linea(PX(12), PY(112), PX(18) + DXSpe * 2, PY(118), vbBlack)
 Call linea(PX(12) - DXSpe * 2, PY(112), PX(18), PY(118), vbBlack)
 
 'Disegna Fianco 1 spostato con tratteggio
 Call linea(PX(18) + DXSpe * 2, PY(118), PX(17) + DXSpe * 2, PY(117), vbBlack, , vbDot)
 Call linea(PX(18) + DXSpe * 2, PY(118), PX(18) + DXSpe * 2, PY(118) - HTotProfilo, vbBlack)
 
 Call linea(PX(18) + DXSpe * 2, PY(118) - HTotProfilo, PX(17) + DXSpe * 2 - 1, PY(118) - HTotProfilo, vbBlack)
 Call linea(PX(17) + DXSpe * 2 - 1, PY(118) - HTotProfilo, PX(17) + DXSpe * 2 - 1, PY(117), vbBlack)
 Call linea(PX(17) + DXSpe * 2 - 1, PY(117), PX(17) + DXSpe * 2, PY(117), vbBlack)
 
 
 'Raggio
 Call linea(PX(17) + DXSpe * 2, PY(117), PX(16) + DXSpe * 2, PY(116), vbBlack)
 Call linea(PX(16) + DXSpe * 2, PY(116), PX(15) + DXSpe * 2, PY(115), vbBlack)
 'Raggio
 Call linea(PX(14) + DXSpe * 2, PY(114), PX(15) + DXSpe * 2, PY(115), vbBlack)
 Call linea(PX(14) + DXSpe * 2, PY(114), PX(14) + DXSpe * 2, PY(115) - 25, vbBlack)
     
    
 'Disegna Fianco 2 spostato con tratteggio
 Call linea(PX(12) - DXSpe * 2, PY(112), PX(13) - DXSpe * 2, PY(113), vbBlack, , vbDot)
 Call linea(PX(12) - DXSpe * 2, PY(112), PX(12) - DXSpe * 2, PY(112) - HTotProfilo, vbBlack)
 
 Call linea(PX(12) - DXSpe * 2, PY(112) - HTotProfilo, PX(13) - DXSpe * 2 + 1, PY(112) - HTotProfilo, vbBlack)
 Call linea(PX(13) - DXSpe * 2 + 1, PY(112) - HTotProfilo, PX(13) - DXSpe * 2 + 1, PY(113), vbBlack)
 Call linea(PX(13) - DXSpe * 2 + 1, PY(113), PX(13) - DXSpe * 2, PY(113), vbBlack)
 
 
 'Raggio
 Call linea(PX(13) - DXSpe * 2, PY(113), PX(14) - DXSpe * 2, PY(114), vbBlack)
 Call linea(PX(14) - DXSpe * 2, PY(114), PX(15) - DXSpe * 2, PY(115), vbBlack)
 'Call linea(PX(17) - DXSpe * 2, PY(117), PX(16) - DXSpe * 2, PY(116), vbBlue)
 Call linea(PX(16) - DXSpe * 2, PY(116), PX(15) - DXSpe * 2, PY(115), vbBlack)
 Call linea(PX(16) - DXSpe * 2, PY(116), PX(16) - DXSpe * 2 - 2, PY(116), vbBlack)
 
 Call linea(PX(16) - DXSpe * 2 - 2, PY(116), PX(16) - DXSpe * 2 - 2, PY(116) - 25, vbBlack)
 'Riempimento
 
 'Chiudo in basso
 Call linea(PX(16) - DXSpe * 2 - 2, PY(118) - HTotProfilo, PX(14) + DXSpe * 2, PY(118) - HTotProfilo, RGB(225, 240, 255))
 Call linea(PX(16) - DXSpe * 2 - 2, PY(116) - 25, PX(14) + DXSpe * 2, PY(116) - 25, RGB(225, 240, 255))
    
 'test
 'PAR.Pic.Line (2700, 3200)-(1500, 3200), vbBlue
    
    
    ' Enforce new properties
     PAR.Pic.FillStyle = 0
     PAR.Pic.FillColor = RGB(225, 240, 255) 'vbRed  'vbBlue
     
     If Stampante = False Then
     BorderColor = PAR.Pic.Point(2700, 3200)
     X2 = PAR.Pic.ScaleX(2700, PAR.Pic.ScaleMode, vbPixels)
     Y2 = PAR.Pic.ScaleY(3200, PAR.Pic.ScaleMode, vbPixels)
     ExtFloodFill PAR.Pic.hdc, X2, Y2, BorderColor, FLOODFILLSURFACE
    
    'riempio fianco sx
     BorderColor = PAR.Pic.Point(1500, 2000)
     X2 = PAR.Pic.ScaleX(1500, PAR.Pic.ScaleMode, vbPixels)
     Y2 = PAR.Pic.ScaleY(2000, PAR.Pic.ScaleMode, vbPixels)
     ExtFloodFill PAR.Pic.hdc, X2, Y2, BorderColor, FLOODFILLSURFACE
    
    'riempio fianco dx
     BorderColor = PAR.Pic.Point(3200, 2000)
     X2 = PAR.Pic.ScaleX(3200, PAR.Pic.ScaleMode, vbPixels)
     Y2 = PAR.Pic.ScaleY(2000, PAR.Pic.ScaleMode, vbPixels)
     ExtFloodFill PAR.Pic.hdc, X2, Y2, BorderColor, FLOODFILLSURFACE
     End If
 'Ripristino linee del contorno
 Call linea(PX(12) - DXSpe * 2, PY(112) - HTotProfilo, PX(13) - DXSpe * 2 + 1, PY(112) - HTotProfilo, vbBlack)
 Call linea(PX(18) + DXSpe * 2, PY(118) - HTotProfilo, PX(17) + DXSpe * 2 - 1, PY(118) - HTotProfilo, vbBlack)
 Call linea(PX(13), PY(118) - HTotProfilo, PX(17), PY(118) - HTotProfilo, vbBlack)
 
 'Distanziale
 Call linea(PX(16) - DXSpe * 2 - 6, PY(116), PX(16) - DXSpe * 2 - 6, PY(116) - 10, vbBlue)
 Call linea(PX(16) - DXSpe * 2 - 2, PY(116), PX(16) - DXSpe * 2 - 6, PY(116), vbBlue)
    
'
'  'Profilatore
'  Call linea(PX(22) - CorP, PY(22), PX(24), PY(24), vbBlack, 2)
'  Call linea(PX(22) - CorP, PY(22), PX(23) - CorP, PY(23), vbBlack, 2)
'
'  'Parametri
  'LR
  Call QuotaH(PX(16) - DXSpe * 2 - 2, PY(112) + 2, PX(15), PY(112) + 2, "LCR", 12, CalcoloColore("DIST_CR"), 3, 2, 0)
  Call linea(PX(16) - DXSpe * 2 - 2, PY(21), PX(16) - DXSpe * 2 - 2, PY(112) + 2, vbBlack, 1, vbDot)
  Call linea(PX(15), PY(21), PX(15), PY(112) + 2, vbBlack, 1, vbDot)

  'SR
  Call linea(PX(16) - DXSpe * 2 - 6, PY(21), PX(16) - DXSpe * 2 - 6, PY(112) + 2, vbBlack, 1, vbDot)

  Call QuotaH(PX(16) - DXSpe * 2 - 2, PY(112) + 2, PX(16) - DXSpe * 2 - 6, PY(112) + 2, "SR", 12, CalcoloColore("DISTANZRULLO"), 3, 2, 0)

'  'Da Rullo
  Call QuotaV(PAR.MaxX, PY(9), PAR.MaxX, PY(112), "DaR", 12, CalcoloColore("DRULLOPROF_G"), , 1)
  Call linea(PX(15), PY(112), PAR.MaxX, PY(112), vbBlack, 1, vbDot)

'  'Spessore Profilo
  Call QuotaH(PX(41), PY(41), PX(42), PY(42), "L", 12, CalcoloColore("SP_RIF_RULLO"), 3, 2, 0)
'
'  'Rag Rif Rullo
  Call QuotaV(PAR.MaxX - MargXDx + 2.5, PY(9), PAR.MaxX - MargXDx + 2.5, PY(42), "RefR", 12, CalcoloColore("RG_RIF_RULLO"), , 1)
  Call linea(PX(42), PY(42), PAR.MaxX - MargXDx + 2.5, PY(42), vbBlack, 1, vbDot)
  
  
'  'Ang Pressione
  PY(43) = (PY(42) + PY(12)) / 2
  PX(43) = PX(41)
  RagTmp = PY(42) - PY(43)
  BB0 = RagTmp * Sin(Angrullo * PG / 180)
  CC0 = RagTmp * Cos(Angrullo * PG / 180)
  PX(45) = PX(43) + BB0: PY(45) = PY(42) - CC0
  bb1 = RagTmp * Sin(Angrullo / 2 * PG / 180)
  CC1 = RagTmp * Cos(Angrullo / 2 * PG / 180)
  PX(44) = PX(43) + bb1: PY(44) = PY(42) - CC1

  If VarCorr = "AP_RULLO" Then
     Call linea(PX(41), PY(42), PX(41), PY(12), vbRed, 1, vbDot)
     Call linea(PX(43), PY(43), PX(44), PY(44), vbRed, 2, vbDot)
     Call linea(PX(44), PY(44), PX(45), PY(45), vbRed, 2, vbDot)
     Call QuotaH(PX(44), PY(44), PX(44), PY(44), "a", 12, vbRed, 4, 2, 4)
  Else
     Call linea(PX(41), PY(42), PX(41), PY(12), vbBlack, 1, vbDot)
     Call linea(PX(43), PY(43), PX(44), PY(44), vbBlack, 1)
     Call linea(PX(44), PY(44), PX(45), PY(45), vbBlack, 1)
     Call QuotaH(PX(44), PY(44), PX(44), PY(44), "a", 12, vbBlack, 4, 2, 4)
  End If
'
'  'HRullo
  'Call linea(PX(20), PY(18), PX(18), PY(18), vbBlack, 1, vbDot)
  Call linea(PX(15) - DXSpe * 2 - 2, PY(115), PX(15) - DXSpe * 2 + 2, PY(115), vbBlack, 1, vbDot)
  PTmp = PX(15) - DXSpe * 2
  Call QuotaV(PTmp, PY(115), PTmp, PY(112), "HR", 12, CalcoloColore("H_RIF_RULLO"), , 0)
    
  
        
GoTo ESCI
    ' enforce new properties
     PAR.Pic.FillStyle = 0
     PAR.Pic.FillColor = RGB(225, 240, 255) 'vbRed  'vbBlue
    
     BorderColor = PAR.Pic.Point(2700, 3100)
     X2 = PAR.Pic.ScaleX(2700, PAR.Pic.ScaleMode, vbPixels)
     Y2 = PAR.Pic.ScaleY(3100, PAR.Pic.ScaleMode, vbPixels)
     ExtFloodFill PAR.Pic.hdc, X2, Y2, BorderColor, FLOODFILLSURFACE
        
        
        
 'Call linea(PX(16) - DXSpe * 2 - 2, PY(116) - 10, PX(14) + DXSpe * 2, PY(115) - 10, vbBlack)
        
        
ESCI:
        
  Case Else
    'Altri tipi di rulli da disegnare
    
  End Select
  
Exit Sub

errDisegna_Rullo_Molavite:
  MsgBox "Error " & Err.Number & " (" & Err.Description & ") in Sub: Disegna_Rullo_Molavite"
    'Resume Next
  Exit Sub


End Sub
Public Sub Disegna_Correttori_Elica_MVV(Stampante As Boolean)
'
Dim LatoPart  As Integer
Dim nZone     As Integer
Dim X()       As Double
Dim Y()       As Double
Dim Con1F1    As Double
Dim Con2F1    As Double
Dim Con3F1    As Double
Dim Con1F2    As Double
Dim Con2F2    As Double
Dim Con3F2    As Double
Dim Bom1F1    As Double
Dim Bom2F1    As Double
Dim Bom3F1    As Double
Dim Bom1F2    As Double
Dim Bom2F2    As Double
Dim Bom3F2    As Double
Dim MaxX1     As Double
Dim MaxX2     As Double
Dim scalaxEl  As Double
Dim Pos       As Double
Dim XF0       As Double
Dim XF1       As Double
Dim XF2       As Double
Dim XF3       As Double
Dim XF0_2     As Double
Dim XF1_2     As Double
Dim XF2_2     As Double
Dim XF3_2     As Double
Dim Ft        As Double
Dim Ft2       As Double
Dim F1        As Double
Dim F2        As Double
Dim F3        As Double
Dim F1_2      As Double
Dim F2_2      As Double
Dim F3_2      As Double
Dim Re        As Double
Dim RS        As Double
Dim LS        As Double
Dim tColore   As Long
Dim tX1       As Double
Dim tX        As Double
Dim tY        As Double
Dim i         As Integer
Dim NotScale  As Boolean
Dim Lin       As Double
Dim CORSA_O   As Double
Dim CORSA_I   As Double
Dim F1T1      As Double
Dim F1T2      As Double
Dim F1T3      As Double
Dim F2T1      As Double
Dim F2T2      As Double
Dim F2T3      As Double
Dim QuoStart1 As Double
Dim QuoStart2 As Double
Dim CodTesta  As Integer
Dim nriga     As Long
Dim nstr      As Long
Dim riga      As String
Dim CorCon1Fsx    As Double
Dim CorCon2Fsx    As Double
Dim CorCon3Fsx    As Double
Dim CorCon1Fdx    As Double
Dim CorCon2Fdx    As Double
Dim CorCon3Fdx    As Double
Dim CorBom1Fsx    As Double
Dim CorBom2Fsx    As Double
Dim CorBom3Fsx    As Double
Dim CorBom1Fdx    As Double
Dim CorBom2Fdx    As Double
Dim CorBom3Fdx    As Double
Dim VCon1F1       As Double
Dim VCon2F1       As Double
Dim VCon3F1       As Double
Dim VCon1F2       As Double
Dim VCon2F2       As Double
Dim VCon3F2       As Double
Dim VBom1F1       As Double
Dim VBom2F1       As Double
Dim VBom3F1       As Double
Dim VBom1F2       As Double
Dim VBom2F2       As Double
Dim VBom3F2       As Double
Dim FiancoSx      As Integer

Dim XP0           As Double
Dim XP1           As Double
Dim XP2           As Double
Dim Xp3           As Double
Dim Xt            As Double
Dim Yt            As Double
Dim Valore        As String
Dim Vbmb          As Double
'
On Error Resume Next
  '
  Call InizializzaVariabili
  '
  NotScale = False
  '
  scalaxEl = Lp("iScalaX")
  If (scalaxEl = 0) Then scalaxEl = Lp("iScalaX")
  LatoPart = Lp("LatoPA")
  CORSA_I = Lp("CORSAIN")
  CORSA_O = Lp("CORSAOUT")
  nZone = Lp("iNzone")
  
  CodTesta = Lp("INDTAV_G")
  If (CodTesta = 1) Then
    QuoStart2 = Lp("QASSIALESTART_G[1,1]")
  Else
    QuoStart1 = Lp("QASSIALESTART_G[0,1]")
  End If
  
  
  F1 = 0:   F2 = 0:   F3 = 0
  F1_2 = 0: F2_2 = 0: F3_2 = 0
  
  '
  'Disegno correzioni elica
  ReDim X(60):  ReDim Y(60)
  
  'Valori impostati
  VCon1F1 = Lp("iCon1Fsx"):  VCon2F1 = Lp("iCon2Fsx"):  VCon3F1 = Lp("iCon3Fsx")
  VCon1F2 = Lp("iCon1Fdx"):  VCon2F2 = Lp("iCon2Fdx"):  VCon3F2 = Lp("iCon3Fdx")
  '
  VBom1F1 = Lp("iBom1Fsx"):  VBom2F1 = Lp("iBom2Fsx"):  VBom3F1 = Lp("iBom3Fsx")
  VBom1F2 = Lp("iBom1Fdx"):  VBom2F2 = Lp("iBom2Fdx"):  VBom3F2 = Lp("iBom3Fdx")
  
  'Solo Correzioni
  CorCon1Fsx = Lp("iCorCon1Fsx") / 1000: CorCon2Fsx = Lp("iCorCon2Fsx") / 1000: CorCon3Fsx = Lp("iCorCon3Fsx") / 1000
  CorCon1Fdx = Lp("iCorCon1Fdx") / 1000: CorCon2Fdx = Lp("iCorCon2Fdx") / 1000: CorCon3Fdx = Lp("iCorCon3Fdx") / 1000
  '
  CorBom1Fsx = Lp("iCorBom1Fsx") / 1000: CorBom2Fsx = Lp("iCorBom2Fsx") / 1000: CorBom3Fsx = Lp("iCorBom3Fsx") / 1000
  CorBom1Fdx = Lp("iCorBom1Fdx") / 1000: CorBom2Fdx = Lp("iCorBom2Fdx") / 1000: CorBom3Fdx = Lp("iCorBom3Fdx") / 1000
  
  
  'Gestione Lato serraggio - Inverto solo le correzione
  FiancoSx = Lp("iFiancoSX")
  
  If FiancoSx = 1 Then
  
  'Correzioni Elica Totali se F1=Fsx - Lato Serraggio in misura � in Basso
  Con1F1 = VCon1F1 + CorCon1Fsx: Con2F1 = VCon2F1 + CorCon2Fsx: Con3F1 = VCon3F1 + CorCon3Fsx
  Con1F2 = VCon1F2 + CorCon1Fdx: Con2F2 = VCon2F2 + CorCon2Fdx: Con3F2 = VCon3F2 + CorCon3Fdx
  '
  Bom1F1 = VBom1F1 + CorBom1Fsx: Bom2F1 = VBom2F1 + CorBom2Fsx: Bom3F1 = VBom3F1 + CorBom3Fsx
  Bom1F2 = VBom1F2 + CorBom1Fdx: Bom2F2 = VBom2F2 + CorBom2Fdx: Bom3F2 = VBom3F2 + CorBom3Fdx
  
  Else
  'Correzioni Elica Totali se F2=Fsx - Lato Serraggio in misura � in Alto
  
  'Valore Totale del Fianco sx del grafico = Valore Teorico Fianco 2 + Corr. Fianco sx del grafico
  Con1F1 = VCon1F2 + CorCon1Fsx: Con2F1 = VCon2F2 + CorCon2Fsx: Con3F1 = VCon3F2 + CorCon3Fsx

  'Valore Totale del Fianco dx del grafico = Valore Teorico Fianco 1 + Corr. Fianco dx del grafico
  Con1F2 = VCon1F1 + CorCon1Fdx: Con2F2 = VCon2F1 + CorCon2Fdx: Con3F2 = VCon3F1 + CorCon3Fdx
  '
  Bom1F1 = VBom1F2 + CorBom1Fsx: Bom2F1 = VBom2F2 + CorBom2Fsx: Bom3F1 = VBom3F2 + CorBom3Fsx
  Bom1F2 = VBom1F1 + CorBom1Fdx: Bom2F2 = VBom2F1 + CorBom2Fdx: Bom3F2 = VBom3F1 + CorBom3Fdx

  End If
  

  
  'Calcolo delle Zone comprese le Extracorse
  
  Select Case nZone
    Case 1
        F1 = CORSA_I + Lp("iFas1Fsx") + CORSA_O: F1_2 = CORSA_I + Lp("iFas1Fdx") + CORSA_O
    Case 2
        F1T1 = Lp("iFas1Fsx"): F1T2 = Lp("iFas2Fsx")
        If (F1T1 > 0) And (F1T2 > 0) Then
           F1 = F1T1 + CORSA_I
           F2 = F1T2 + CORSA_O
        Else
          If (F1T1 > 0) And (F1T2 = 0) Then
             F1 = F1T1 + CORSA_I + CORSA_O
             F2 = F1T2
          Else
             If (F1T1 = 0) And (F1T2 > 0) Then
                F1 = F1T1
                F2 = F1T2 + CORSA_I + CORSA_O
             Else
                F1 = F1T1 + CORSA_I
                F2 = F1T2 + CORSA_O
             End If
          End If
        End If
        
        F2T1 = Lp("iFas1Fdx"): F2T2 = Lp("iFas2Fdx")
        If (F2T1 > 0) And (F2T2 > 0) Then
           F1_2 = F2T1 + CORSA_I
           F2_2 = F2T2 + CORSA_O
        Else
          If (F2T1 > 0) And (F2T2 = 0) Then
             F1_2 = F2T1 + CORSA_I + CORSA_O
             F2_2 = F2T2
          Else
             If (F2T1 = 0) And (F2T2 > 0) Then
                F1_2 = F2T1
                F2_2 = F2T2 + CORSA_I + CORSA_O
             Else
                F1_2 = F2T1 + CORSA_I
                F2_2 = F2T2 + CORSA_O
             End If
          End If
        End If
    
    Case 3
        
        F1T1 = Lp("iFas1Fsx"): F1T2 = Lp("iFas2Fsx"): F1T3 = Lp("iFas3Fsx")
        If (F1T1 > 0) And (F1T2 > 0) And (F1T3 > 0) Then
           F1 = F1T1 + CORSA_I
           F2 = F1T2
           F3 = F1T3 + CORSA_O
        Else
          If (F1T1 > 0) And (F1T2 > 0) And (F1T3 = 0) Then
             F1 = F1T1 + CORSA_I
             F2 = F1T2 + CORSA_O
             F3 = F1T3
          Else
            If (F1T1 > 0) And (F1T2 = 0) And (F1T3 = 0) Then
               F1 = F1T1 + CORSA_I + CORSA_O
               F2 = F1T2
               F3 = F1T3
            Else
               If (F1T1 > 0) And (F1T2 = 0) And (F1T3 > 0) Then
                  F1 = F1T1 + CORSA_I
                  F2 = F1T2
                  F3 = F1T3 + CORSA_O
               Else
                  If (F1T1 = 0) And (F1T2 > 0) And (F1T3 > 0) Then
                     F1 = F1T1
                     F2 = F1T2 + CORSA_I
                     F3 = F1T3 + CORSA_O
                  Else
                     If (F1T1 = 0) And (F1T2 = 0) And (F1T3 > 0) Then
                        F1 = F1T1
                        F2 = F1T2
                        F3 = F1T3 + CORSA_I + CORSA_O
                     Else
                        F1 = F1T1 + CORSA_I
                        F2 = F1T2
                        F3 = F1T3 + CORSA_O
                     End If
                  End If
               End If
            End If
          End If
        End If
          
        F2T1 = Lp("iFas1Fdx"): F2T2 = Lp("iFas2Fdx"): F2T3 = Lp("iFas3Fdx")
        If (F2T1 > 0) And (F2T2 > 0) And (F2T3 > 0) Then
           F1_2 = F2T1 + CORSA_I
           F2_2 = F2T2
           F3_2 = F2T3 + CORSA_O
        Else
          If (F2T1 > 0) And (F2T2 > 0) And (F2T3 = 0) Then
             F1_2 = F2T1 + CORSA_I
             F2_2 = F2T2 + CORSA_O
             F3_2 = F2T3
          Else
            If (F2T1 > 0) And (F2T2 = 0) And (F2T3 = 0) Then
               F1_2 = F2T1 + CORSA_I + CORSA_O
               F2_2 = F2T2
               F3_2 = F2T3
            Else
               If (F2T1 > 0) And (F2T2 = 0) And (F2T3 > 0) Then
                  F1_2 = F2T1 + CORSA_I
                  F2_2 = F2T2
                  F3_2 = F2T3 + CORSA_O
               Else
                  If (F2T1 = 0) And (F2T2 > 0) And (F2T3 > 0) Then
                     F1_2 = F2T1
                     F2_2 = F2T2 + CORSA_I
                     F3_2 = F2T3 + CORSA_O
                  Else
                     If (F2T1 = 0) And (F2T2 = 0) And (F2T3 > 0) Then
                        F1_2 = F2T1
                        F2_2 = F2T2
                        F3_2 = F2T3 + CORSA_I + CORSA_O
                     Else
                        F1_2 = F2T1 + CORSA_I
                        F2_2 = F2T2
                        F3_2 = F2T3 + CORSA_O
                     End If
                  End If
               End If
            End If
          End If
        End If
            
  End Select
  
  Re = 10:  RS = 10:  LS = 10
  '
  Ft = F1 + F2 + F3         'Fascia Totale F1 con Extracorse
  Ft2 = F1_2 + F2_2 + F3_2  'Fascia Totale F2 con Extracorse
  
  'Posizioni X della fine di ciascuna Zona
  If FiancoSx = 1 Then
     XF0 = LS:    XF1 = XF0 + F1:        XF2 = XF1 + F2:        XF3 = XF2 + F3
     XF0_2 = LS:  XF1_2 = XF0_2 + F1_2:  XF2_2 = XF1_2 + F2_2:  XF3_2 = XF2_2 + F3_2
  Else
     XF3 = LS:    XF2 = XF3 + F3:        XF1 = XF2 + F2:        XF0 = XF1 + F1
     XF3_2 = LS:  XF2_2 = XF3_2 + F3_2:  XF1_2 = XF2_2 + F2_2:  XF0_2 = XF1_2 + F1_2
  End If
  
  '
  Pos = RS
  'Assegno le pos. X ad un vettore X(n)
  If FiancoSx = 1 Then
  'Zona 1 inizia in basso
  X(0) = XF0
  X(1) = XF1
  X(2) = XF2
  X(3) = XF3
  X(16) = XF3
  Else
  'Zona 1 in inizia in alto
  X(0) = XF3
  X(1) = XF2
  X(2) = XF1
  X(3) = XF0
  X(16) = XF0
  End If
  
  If FiancoSx = 1 Then
    Y(0) = -Pos
    Y(1) = -Pos - (Con1F1 * 1 + Con2F1 * 0 + Con3F1 * 0) * scalaxEl
    Y(2) = -Pos - (Con1F1 * 1 + Con2F1 * 1 + Con3F1 * 0) * scalaxEl
    Y(3) = -Pos - (Con1F1 * 1 + Con2F1 * 1 + Con3F1 * 1) * scalaxEl
  Else
    Y(3) = -Pos
    Y(2) = -Pos - (Con1F1 * 1 + Con2F1 * 0 + Con3F1 * 0) * scalaxEl
    Y(1) = -Pos - (Con1F1 * 1 + Con2F1 * 1 + Con3F1 * 0) * scalaxEl
    Y(0) = -Pos - (Con1F1 * 1 + Con2F1 * 1 + Con3F1 * 1) * scalaxEl
  End If
  Y(16) = -Pos
  
  If FiancoSx = 1 Then
  X(4) = XF0_2
  X(5) = XF1_2
  X(6) = XF2_2
  X(7) = XF3_2
  X(17) = XF3_2
  Else
  X(4) = XF3_2
  X(5) = XF2_2
  X(6) = XF1_2
  X(7) = XF0_2
  X(17) = XF0_2
  End If
  
  If FiancoSx = 1 Then
    Y(4) = Pos
    Y(5) = Pos + (Con1F2 * 1 + Con2F2 * 0 + Con3F2 * 0) * scalaxEl
    Y(6) = Pos + (Con1F2 * 1 + Con2F2 * 1 + Con3F2 * 0) * scalaxEl
    Y(7) = Pos + (Con1F2 * 1 + Con2F2 * 1 + Con3F2 * 1) * scalaxEl
  Else
    Y(7) = Pos
    Y(6) = Pos + (Con1F2 * 1 + Con2F2 * 0 + Con3F2 * 0) * scalaxEl
    Y(5) = Pos + (Con1F2 * 1 + Con2F2 * 1 + Con3F2 * 0) * scalaxEl
    Y(4) = Pos + (Con1F2 * 1 + Con2F2 * 1 + Con3F2 * 1) * scalaxEl
  End If
  Y(17) = Pos
  
  'Lineette
  X(8) = X(1)
  X(9) = X(1)
  X(12) = X(5)
  X(13) = X(5)
  X(10) = X(2)
  X(11) = X(2)
  X(14) = X(6)
  X(15) = X(6)
  Lin = 3
  Y(8) = Y(1) + Lin
  Y(9) = Y(1) - Lin
  Y(12) = Y(5) + Lin
  Y(13) = Y(5) - Lin
  Y(10) = Y(2) + Lin
  Y(11) = Y(2) - Lin
  Y(14) = Y(6) + Lin
  Y(15) = Y(6) - Lin
  
  
  Dim StepGriglia As Double
  StepGriglia = 0.05
  For i = -3 To 3
    Y(21 + i) = Y(0) + i * StepGriglia * scalaxEl
    Y(28 + i) = Y(0) + i * StepGriglia * scalaxEl
    Y(35 + i) = Y(4) + i * StepGriglia * scalaxEl
    Y(42 + i) = Y(4) + i * StepGriglia * scalaxEl
    X(21 + i) = XF0
    X(28 + i) = XF3
    X(35 + i) = XF0
    X(42 + i) = XF3
  Next i
  
  'Pos. extracorse
  If FiancoSx = 1 Then
    X(51) = XF0 + CORSA_I
    X(52) = X(51)
    X(53) = XF3 - CORSA_O
    X(54) = X(53)
  Else
    X(51) = XF0 - CORSA_I
    X(52) = X(51)
    X(53) = XF3 + CORSA_O
    X(54) = X(53)
  End If
  
  'Linee extracorsa
  If FiancoSx = 1 Then
  Y(51) = Y(0)
  Y(52) = Y(0)
  Y(53) = Y(0)
  Else
  Y(51) = Y(3)
  Y(52) = Y(3)
  Y(53) = Y(3)
  End If
  
  If FiancoSx = 1 Then
  PAR.MaxX = Y(4)
  PAR.MinX = Y(0)
  Else
  PAR.MaxX = Y(7)
  PAR.MinX = Y(3)
  End If
  
  For i = 1 To 3
    If Y(i) > PAR.MaxX Then PAR.MaxX = Y(i)
    If Y(i + 4) < PAR.MinX Then PAR.MinX = Y(i + 4)
  Next i
  
  If Ft >= Ft2 Then
    PAR.MaxY = Ft + LS
  Else
    PAR.MaxY = Ft2 + LS
  End If
  PAR.MinY = LS
  
  'par.MaxY = Re / 2
  'par.MinY = -Re / 2
  PAR.ScalaMargine = 0.75
  PAR.Allineamento = 0
  Call CalcolaParPic(PAR, True, Stampante)
  PAR.Pic.Cls
  'linea XMola, Re + 20 + DiaMola, XMola, Re, vbRed, 1, vbDash
  MaxX1 = X(1)
  MaxX2 = X(5)
  
  If FiancoSx = 1 Then
  XP0 = XF0
  XP1 = XF1
  Else
  XP0 = XF2
  XP1 = XF3
  End If
  
  
  If Not NotScale Then
    Select Case nZone 'Disegno ed evidenzio in rosso le corse delle singole Zone
      Case 1
'        Call QuotaH(XF0, Re, XF1, Re, "" & F1, 12, CalcoloColore("iFas1Fsx"), 4)
'        Call QuotaH(XF0_2, -Re, XF1_2, -Re, "" & F1_2, 12, CalcoloColore("iFas1Fdx"), 3)
'        'Verticale
        Call QuotaVV(-Re, XF0, -Re, XF1, "" & F1, 12, CalcoloColore("iFas1Fsx"), 7)
        Call QuotaVV(Re, XF0_2, Re, XF1_2, "" & F1_2, 12, CalcoloColore("iFas1Fdx"), 6)
      
      Case 2
'        Call QuotaH(XF0, Re, XF1, Re, "" & F1, 12, CalcoloColore("iFas1Fsx"), 4)
'        Call QuotaH(XF1, Re, XF2, Re, "" & F2, 12, CalcoloColore("iFas2Fsx"), 4)
'        Call QuotaH(XF0_2, -Re, XF1_2, -Re, "" & F1_2, 12, CalcoloColore("iFas1Fdx"), 3)
'        Call QuotaH(XF1_2, -Re, XF2_2, -Re, "" & F2_2, 12, CalcoloColore("iFas2Fdx"), 3)
        'Verticale
        Call QuotaVV(-Re, XF0, -Re, XF1, "" & F1, 12, CalcoloColore("iFas1Fsx"), 7)
        Call QuotaVV(-Re, XF1, -Re, XF2, "" & F2, 12, CalcoloColore("iFas2Fsx"), 7)
        Call QuotaVV(Re, XF0_2, Re, XF1_2, "" & F1_2, 12, CalcoloColore("iFas1Fdx"), 6)
        Call QuotaVV(Re, XF1_2, Re, XF2_2, "" & F2_2, 12, CalcoloColore("iFas2Fdx"), 6)
        
        MaxX1 = X(2)
        MaxX2 = X(6)
      Case 3
        
        If (VarCorr = "iFas1Fsx") Then
           If FiancoSx = 1 Then
             'Call QuotaH(XF0, Re, XF1, Re, "" & F1, 12, CalcoloColore("iFas1Fsx"), 4, 1)
              Call QuotaVV(-Re, XF0, -Re, XF1, "" & F1, 12, CalcoloColore("iFas1Fsx"), 7, 1)
           Else
             'Call QuotaH(XF1, Re, XF0, Re, "" & F1, 12, CalcoloColore("iFas1Fsx"), 4, 1)
              Call QuotaVV(-Re, XF1, -Re, XF0, "" & F1, 12, CalcoloColore("iFas1Fsx"), 7, 1)
           End If
        Else
           If FiancoSx = 1 Then
              'Call QuotaH(XF0, Re, XF1, Re, "" & F1, 12, CalcoloColore("iFas1Fsx"), 4)
              Call QuotaVV(-Re, XF0, -Re, XF1, "" & F1, 12, CalcoloColore("iFas1Fsx"), 7)
           Else
              'Call QuotaH(XF1, Re, XF0, Re, "" & F1, 12, CalcoloColore("iFas1Fsx"), 4)
              Call QuotaVV(-Re, XF1, -Re, XF0, "" & F1, 12, CalcoloColore("iFas1Fsx"), 7)
           End If
        End If
        
        If (VarCorr = "iFas2Fsx") Then
           If FiancoSx = 1 Then
             'Call QuotaH(XF1, Re, XF2, Re, "" & F2, 12, CalcoloColore("iFas2Fsx"), 4, 1)
              Call QuotaVV(-Re, XF1, -Re, XF2, "" & F2, 12, CalcoloColore("iFas2Fsx"), 7, 1)
           Else
             'Call QuotaH(XF2, Re, XF1, Re, "" & F2, 12, CalcoloColore("iFas2Fsx"), 4, 1)
              Call QuotaVV(-Re, XF2, -Re, XF1, "" & F2, 12, CalcoloColore("iFas2Fsx"), 7, 1)
           End If
        Else
           If FiancoSx = 1 Then
             'Call QuotaH(XF1, Re, XF2, Re, "" & F2, 12, CalcoloColore("iFas2Fsx"), 4)
              Call QuotaVV(-Re, XF1, -Re, XF2, "" & F2, 12, CalcoloColore("iFas2Fsx"), 7)
           Else
             'Call QuotaH(XF2, Re, XF1, Re, "" & F2, 12, CalcoloColore("iFas2Fsx"), 4)
              Call QuotaVV(-Re, XF2, -Re, XF1, "" & F2, 12, CalcoloColore("iFas2Fsx"), 7)
           End If
        End If
        
        If (VarCorr = "iFas3Fsx") Then
           If FiancoSx = 1 Then
             'Call QuotaH(XF2, Re, XF3, Re, "" & F3, 12, CalcoloColore("iFas3Fsx"), 4, 1)
              Call QuotaVV(-Re, XF2, -Re, XF3, "" & F3, 12, CalcoloColore("iFas3Fsx"), 7, 1)
           Else
             'Call QuotaH(XF3, Re, XF2, Re, "" & F3, 12, CalcoloColore("iFas3Fsx"), 4, 1)
              Call QuotaVV(-Re, XF3, -Re, XF2, "" & F3, 12, CalcoloColore("iFas3Fsx"), 7, 1)
           End If
        Else
           If FiancoSx = 1 Then
             'Call QuotaH(XF2, Re, XF3, Re, "" & F3, 12, CalcoloColore("iFas3Fsx"), 4)
              Call QuotaVV(-Re, XF2, -Re, XF3, "" & F3, 12, CalcoloColore("iFas3Fsx"), 7)
           Else
             'Call QuotaH(XF3, Re, XF2, Re, "" & F3, 12, CalcoloColore("iFas3Fsx"), 4)
              Call QuotaVV(-Re, XF3, -Re, XF2, "" & F3, 12, CalcoloColore("iFas3Fsx"), 7)
           End If
        End If
        
        If FiancoSx = 1 Then
          'Call QuotaH(XF0_2, -Re, XF1_2, -Re, "" & F1_2, 12, CalcoloColore("iFas1Fdx"), 3)
          'Call QuotaH(XF1_2, -Re, XF2_2, -Re, "" & F2_2, 12, CalcoloColore("iFas2Fdx"), 3)
          'Call QuotaH(XF2_2, -Re, XF3_2, -Re, "" & F3_2, 12, CalcoloColore("iFas3Fdx"), 3)
        
           Call QuotaVV(Re, XF0_2, Re, XF1_2, "" & F1_2, 12, CalcoloColore("iFas1Fdx"), 6)
           Call QuotaVV(Re, XF1_2, Re, XF2_2, "" & F2_2, 12, CalcoloColore("iFas2Fdx"), 6)
           Call QuotaVV(Re, XF2_2, Re, XF3_2, "" & F3_2, 12, CalcoloColore("iFas3Fdx"), 6)

        Else
          'Call QuotaH(XF1_2, -Re, XF0_2, -Re, "" & F1_2, 12, CalcoloColore("iFas1Fdx"), 3)
          'Call QuotaH(XF2_2, -Re, XF1_2, -Re, "" & F2_2, 12, CalcoloColore("iFas2Fdx"), 3)
          'Call QuotaH(XF3_2, -Re, XF2_2, -Re, "" & F3_2, 12, CalcoloColore("iFas3Fdx"), 3)
        
           Call QuotaVV(Re, XF1_2, Re, XF0_2, "" & F1_2, 12, CalcoloColore("iFas1Fdx"), 6)
           Call QuotaVV(Re, XF2_2, Re, XF1_2, "" & F2_2, 12, CalcoloColore("iFas2Fdx"), 6)
           Call QuotaVV(Re, XF3_2, Re, XF2_2, "" & F3_2, 12, CalcoloColore("iFas3Fdx"), 6)
        
        End If
        
        MaxX1 = X(3)
        MaxX2 = X(7)
    End Select
  End If
  
  ' Asse pezzo
  'Call linea(LS, 0, LS + Ft, 0, RGB(180, 180, 180), 1, vbDashDot)
  'Verticale
  Call lineaV(0, LS, 0, LS + Ft, RGB(180, 180, 180), 1, vbDashDot)
  
  Y(51) = PAR.MaxY + 8
  
  Dim tidC    As Long
  
  If Not NotScale Then
    'Disegno Extracorsa lato testa
    tidC = CalcoloColore("iCORSAIN", , , tColore)
    If FiancoSx = 1 Then
      'Call QuotaH(XF0, 0, X(51), 0, "" & CORSA_I, 12, tidC, 3)
       Call QuotaVV(0, XF0, 0, X(51), "" & CORSA_I, 12, tidC, 7)
    Else
      'Call QuotaH(X(51), 0, XF0, 0, "" & CORSA_I, 12, tidC, 3)
       Call QuotaVV(0, X(51), 0, XF0, "" & CORSA_I, 12, tidC, 7)
    End If
    
    'Disegna fascia totale
    tidC = CalcoloColore("FASCIATOTALE_G", , , tColore)
    If FiancoSx = 1 Then
      'Call QuotaH(X(51), 0, X(53), 0, "" & (Ft - CORSA_I - CORSA_O), 12, tidC, 3)
       Call QuotaVV(0, X(51), 0, X(53), "" & (Ft - CORSA_I - CORSA_O), 12, tidC, 7)
    Else
      'Call QuotaH(X(53), 0, X(51), 0, "" & (Ft - CORSA_I - CORSA_O), 12, tidC, 3)
       Call QuotaVV(0, X(53), 0, X(51), "" & (Ft - CORSA_I - CORSA_O), 12, tidC, 7)
    End If
    
    'Disegno Extracorsa lato contropunta
    tidC = CalcoloColore("iCORSAOUT", , , tColore)
    If FiancoSx = 1 Then
      'Call QuotaH(X(53), 0, XF3, 0, "" & CORSA_O, 12, tidC, 3)
       Call QuotaVV(0, X(53), 0, XF3, "" & CORSA_O, 12, tidC, 7)
    Else
      'Call QuotaH(XF3, 0, X(53), 0, "" & CORSA_O, 12, tidC, 3)
       Call QuotaVV(0, XF3, 0, X(53), "" & CORSA_O, 12, tidC, 7)
    End If
  End If
  
  Dim tidP1   As Integer
  Dim tidP2   As Integer
  Dim tidst   As Integer
  Dim tisp    As Integer
  tColore = vbBlue
  tidst = vbSolid
  
  'Linee (griglia) A cosa servono?
  For i = -3 To 3
    tidP1 = 21 + i
    tidP2 = 28 + i
    'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), RGB(200, 200, 200), 1, vbDot)
    tidP1 = 35 + i
    tidP2 = 42 + i
    'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), RGB(200, 200, 200), 1, vbDot)
  Next i
    
  'Fianco1 Fascia1
  If FiancoSx = 1 Then
     tidP1 = 0
     tidP2 = 1
  Else
     tidP1 = 2
     tidP2 = 3
  End If
    
  If (VarCorr = "iFas1Fsx") Or (VarCorr = "iCorCon1Fsx") Then
     tidC = vbRed
     If (VarCorr = "iFas1Fsx") Then
        If Con1F1 <> 0 Then
          'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), vbBlue)
        Else
          'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), tidC)
        End If
     End If
     If (VarCorr = "iCorCon1Fsx") Then
        If Con1F1 <> 0 Then
          'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 2)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), tidC, 2)
           Xt = (X(tidP1) + X(tidP2)) / 2: Yt = (Y(tidP1) + Y(tidP2)) / 2
           Valore = frmt(Con1F1 * 1000, 1) & "�"
          'Call TestoAll(Xt, Yt, Valore, vbBlack, , 3)
           Call TestoAll_V(Yt, Xt, Valore, vbBlack, , 3)
        Else
          'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), vbBlack)
        End If
     End If
     tidC = vbBlue
  Else
     tidC = CalcoloColore("iCorBom1Fsx", , , tColore)
    'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
     Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), vbBlue)
  End If
  tisp = IIf(tidC = vbRed, 2, 1)
  If Bom1F1 <> 0 Then
   'Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), Bom1F1, scalaxEl, tidC, tisp)
    Call Disegna_BombaturaElica_V(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), Bom1F1, scalaxEl, tidC, tisp)
     If (VarCorr = "iCorBom1Fsx") Then
        Xt = (X(tidP1) + X(tidP2)) / 2
        If (Y(tidP1) >= Y(tidP2)) Then 'Yt = (Y(tidP1) + Y(tidP2)) / 2
        Yt = Y(tidP1)
        Else
        Yt = Y(tidP2)
        End If
        Valore = frmt(Bom1F1 * 1000, 1) & "�"
        Vbmb = Bom1F1 * 1000
        Call TestoAll_V(Yt, Xt, Valore, vbBlack, , 8, Vbmb)
     End If
  End If
  
  'Fianco1 Fascia2
  tidP1 = 1
  tidP2 = 2
  If VarCorr = "iFas2Fsx" Or (VarCorr = "iCorCon2Fsx") Then
     tidC = vbRed
     If (VarCorr = "iFas2Fsx") Then
        If Con2F1 <> 0 Then
          'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), vbBlue)
        Else
          'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), tidC)
        End If
     End If
     If (VarCorr = "iCorCon2Fsx") Then
        If Con2F1 <> 0 Then
          'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 2)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), tidC, 2)
           Xt = (X(tidP1) + X(tidP2)) / 2: Yt = (Y(tidP1) + Y(tidP2)) / 2
           Valore = frmt(Con2F1 * 1000, 1) & "�"
          'Call TestoAll(Xt, Yt, Valore, vbBlack, , 3)
           Call TestoAll_V(Yt, Xt, Valore, vbBlack, , 3)
        Else
          'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), vbBlack)
        End If
     End If
     tidC = vbBlue
  Else
     tidC = CalcoloColore("iCorBom2Fsx", , , tColore)
    'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
     Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), vbBlue)
  End If
  tisp = IIf(tidC = vbRed, 2, 1)
  If Bom2F1 <> 0 Then
   'Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), Bom2F1, scalaxEl, tidC, tisp)
    Call Disegna_BombaturaElica_V(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), Bom2F1, scalaxEl, tidC, tisp)
     
     If (VarCorr = "iCorBom2Fsx") Then
        Xt = (X(tidP1) + X(tidP2)) / 2
        If (Y(tidP1) >= Y(tidP2)) Then
        Yt = Y(tidP1)
        Else
        Yt = Y(tidP2)
        End If
        Valore = frmt(Bom2F1 * 1000, 1) & "�"
        Vbmb = Bom2F1 * 1000
        Call TestoAll_V(Yt, Xt, Valore, vbBlack, , 8, Vbmb)
     End If
  End If
  
  'Fianco1 Fascia3
  If FiancoSx = 1 Then
     tidP1 = 2
     tidP2 = 3
  Else
     tidP1 = 0
     tidP2 = 1
  End If
  
  If VarCorr = "iFas3Fsx" Or (VarCorr = "iCorCon3Fsx") Then
     tidC = vbRed
     If (VarCorr = "iFas3Fsx") Then
        If Con3F1 <> 0 Then
          'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), vbBlue)
        Else
          'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), tidC)
        End If
     End If
     If (VarCorr = "iCorCon3Fsx") Then
        If Con3F1 <> 0 Then
          'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 2)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), tidC, 2)
           Xt = (X(tidP1) + X(tidP2)) / 2: Yt = (Y(tidP1) + Y(tidP2)) / 2
           Valore = frmt(Con3F1 * 1000, 1) & "�"
           'Call TestoAll(Xt, Yt, Valore, vbBlack, , 3)
           Call TestoAll_V(Yt, Xt, Valore, vbBlack, , 3)
        Else
          'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), vbBlack)
        End If
     End If
     tidC = vbBlue
  Else
     tidC = CalcoloColore("iCorBom3Fsx", , , tColore)
    'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
     Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), vbBlue)
  End If
  tisp = IIf(tidC = vbRed, 2, 1)
  If Bom3F1 <> 0 Then
    'Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), Bom3F1, scalaxEl, tidC, tisp)
    Call Disegna_BombaturaElica_V(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), Bom3F1, scalaxEl, tidC, tisp)
     If (VarCorr = "iCorBom3Fsx") Then
        Xt = (X(tidP1) + X(tidP2)) / 2
        If (Y(tidP1) >= Y(tidP2)) Then
        Yt = Y(tidP1)
        Else
        Yt = Y(tidP2)
        End If
        Valore = frmt(Bom3F1 * 1000, 1) & "�"
        Vbmb = Bom3F1 * 1000
        Call TestoAll_V(Yt, Xt, Valore, vbBlack, , 8, Vbmb)
     End If
  End If
  
  'Fianco 1 Separazione Zone 1 e 2
  tidC = tColore
  tisp = 1
  tidP1 = 8
  tidP2 = 9
 'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, tidst)
  Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), tidC, 1, tidst)
    
  'Fianco 1 Separazione Zone 2 e 3
  tidP1 = 10
  tidP2 = 11
 'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, tidst)
  Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), tidC, 1, tidst)
  'Fianco 1 Linea tra primo e ultimo punto
  tidP1 = 0
  tidP2 = 16
  
  'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, vbDot)
  
  'Fianco2 Fascia1
  If FiancoSx = 1 Then
     tidP1 = 4
     tidP2 = 5
  Else
     tidP1 = 6
     tidP2 = 7
  End If
  
  If (VarCorr = "iFas1Fdx") Or (VarCorr = "iCorCon1Fdx") Then
     tidC = vbRed
     If (VarCorr = "iFas1Fdx") Then
        If Con1F2 <> 0 Then
          'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), vbBlue)
        Else
          'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), tidC)
        End If
     End If
     If (VarCorr = "iCorCon1Fdx") Then
        If Con1F2 <> 0 Then
          'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 2)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), tidC, 2)
           Xt = (X(tidP1) + X(tidP2)) / 2: Yt = (Y(tidP1) + Y(tidP2)) / 2
           Valore = frmt(Con1F2 * 1000, 1) & "�"
          'Call TestoAll(Xt, Yt, Valore, vbBlack, , 4)
           Call TestoAll(Yt, Xt, Valore, vbBlack, , 4)
        Else
          'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), vbBlack)
        End If
     End If
     tidC = vbBlue
  Else
     tidC = CalcoloColore("iCorBom1Fdx", , , tColore)
    'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
     Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), vbBlue)
  End If
  tisp = IIf(tidC = vbRed, 2, 1)
  If Bom1F2 <> 0 Then
   'Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), -Bom1F2, scalaxEl, tidC, tisp)
    Call Disegna_BombaturaElica_V(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), -Bom1F2, scalaxEl, tidC, tisp)
     If (VarCorr = "iCorBom1Fdx") Then
        Xt = (X(tidP1) + X(tidP2)) / 2
        If (Y(tidP1) <= Y(tidP2)) Then
        Yt = Y(tidP1)
        Else
        Yt = Y(tidP2)
        End If
        Valore = frmt(Bom1F2 * 1000, 1) & "�"
        Vbmb = Bom1F2 * 1000
        Call TestoAll_V(Yt, Xt, Valore, vbBlack, , 9, Vbmb)
     End If
  End If
  
  'Fianco2 Fascia2
  tidP1 = 5
  tidP2 = 6
  If (VarCorr = "iFas2Fdx") Or (VarCorr = "iCorCon2Fdx") Then
     tidC = vbRed
     If (VarCorr = "iFas2Fdx") Then
        If Con2F2 <> 0 Then
          'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), vbBlue)
        Else
          'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), tidC)
        End If
     End If
     If (VarCorr = "iCorCon2Fdx") Then
        If Con2F2 <> 0 Then
          'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 2)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), tidC, 2)
           Xt = (X(tidP1) + X(tidP2)) / 2: Yt = (Y(tidP1) + Y(tidP2)) / 2
           Valore = frmt(Con2F2 * 1000, 1) & "�"
          'Call TestoAll(Xt, Yt, Valore, vbBlack, , 4)
           Call TestoAll_V(Yt, Xt, Valore, vbBlack, , 4)
        Else
          'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), vbBlack)
        End If
     End If
     tidC = vbBlue
  Else
     tidC = CalcoloColore("iCorBom2Fdx", , , tColore)
    'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
     Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), vbBlue)
  End If
  tisp = IIf(tidC = vbRed, 2, 1)
  If Bom2F2 <> 0 Then
   'Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), -Bom2F2, scalaxEl, tidC, tisp)
    Call Disegna_BombaturaElica_V(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), -Bom2F2, scalaxEl, tidC, tisp)
     If (VarCorr = "iCorBom2Fdx") Then
        Xt = (X(tidP1) + X(tidP2)) / 2
        If (Y(tidP1) <= Y(tidP2)) Then
        Yt = Y(tidP1)
        Else
        Yt = Y(tidP2)
        End If
        Valore = frmt(Bom2F2 * 1000, 1) & "�"
        Vbmb = Bom2F2 * 1000
        Call TestoAll_V(Yt, Xt, Valore, vbBlack, , 9, Vbmb)
     End If
  End If
  
  'Fianco2 Fascia3
  tidP1 = 6
  tidP2 = 7
  If FiancoSx = 1 Then
     tidP1 = 6
     tidP2 = 7
  Else
     tidP1 = 4
     tidP2 = 5
  End If
  If (VarCorr = "iFas3Fdx") Or (VarCorr = "iCorCon3Fdx") Then
     tidC = vbRed
     If (VarCorr = "iFas3Fdx") Then
        If Con3F2 <> 0 Then
          'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), vbBlue)
        Else
          'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), tidC)
        End If
     End If
     If (VarCorr = "iCorCon3Fdx") Then
        If Con3F2 <> 0 Then
          'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 2)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), tidC, 2)
           Xt = (X(tidP1) + X(tidP2)) / 2: Yt = (Y(tidP1) + Y(tidP2)) / 2
           Valore = frmt(Con3F2 * 1000, 1) & "�"
          'Call TestoAll(Xt, Yt, Valore, vbBlack, , 4)
           Call TestoAll_V(Yt, Xt, Valore, vbBlack, , 4)
        Else
          'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), vbBlack)
        End If
     End If
     tidC = vbBlue
  Else
     tidC = CalcoloColore("iCorBom3Fdx", , , tColore)
    'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
     Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), vbBlue)
  End If
  tisp = IIf(tidC = vbRed, 2, 1)
  If Bom3F2 <> 0 Then
   'Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), -Bom3F2, scalaxEl, tidC, tisp)
    Call Disegna_BombaturaElica_V(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), -Bom3F2, scalaxEl, tidC, tisp)
     If (VarCorr = "iCorBom3Fdx") Then
        Xt = (X(tidP1) + X(tidP2)) / 2
        If (Y(tidP1) <= Y(tidP2)) Then
        Yt = Y(tidP1)
        Else
        Yt = Y(tidP2)
        End If
        Valore = frmt(Bom3F2 * 1000, 1) & "�"
        Vbmb = Bom3F2 * 1000
        Call TestoAll_V(Yt, Xt, Valore, vbBlack, , 9, Vbmb)
     End If
  End If
  tidC = tColore
  tisp = 1
  
  'Fianco 2 Separazione Zone 1 e 2
  tidP1 = 12
  tidP2 = 13
 'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, tidst)
  Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), tidC, 1, tidst)
  'Fianco 2 Separazione Zone 2 e 3
  tidP1 = 14
  tidP2 = 15
 'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, tidst)
  Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), tidC, 1, tidst)
  'Fianco 1 Linea tra primo e ultimo punto
  tidP1 = 4
  tidP2 = 17
  
  'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, vbDot)
  
  'Scritte F1 e F2
 ' Call TestoAll(LS, RS, "FL", , True, 1)
 ' Call TestoAll(LS, -RS, "FR", , True, 1)
  Call TestoAll_V(-RS, LS, "FL", , True, 4)
  Call TestoAll_V(RS, LS, "FR", , True, 4)
  
  If NotScale Then Call TestoInfo("DRAWING NOT IN SCALE ! ", , True, vbRed, True)
  nriga = 1530
  nstr = LoadString(g_hLanguageLibHandle, nriga, riga, 255)
  If nstr = 0 Then
     riga = "Down"
  End If
  Call TestoInfo(riga, 4)
  nriga = 1531
  nstr = LoadString(g_hLanguageLibHandle, nriga, riga, 255)
  If nstr = 0 Then
     riga = "Up"
  End If
  Call TestoInfo(riga, 1)
  '
  If FiancoSx = 1 Then
     If LINGUA = "GR" Then
      Call TestoInfo("Spannseite", 3)
     Else
      Call TestoInfo("Clamping Side", 3)
     End If
     Call TestoInfo("Units: [mm]", 2)
  Else
     If LINGUA = "GR" Then
      Call TestoInfo("Spannseite", 2)
     Else
      Call TestoInfo("Clamping Side", 2)
     End If
     Call TestoInfo("Units: [mm]", 3)
  End If
  If Ft2 > Ft Then
    Call TestoInfo("WARNING: (FT FL = " & Ft & ") < (FT FR = " & Ft2 & ")", , True, vbRed, True)
    Call QuotaH(MaxX1, Pos, MaxX2, Pos, "?", , vbRed, 3)
  ElseIf Ft2 < Ft Then
    Call TestoInfo("WARNING: (FT FL = " & Ft & ") > (FT FR = " & Ft2 & ")", , True, vbRed, True)
    Call QuotaH(MaxX2, -Pos, MaxX1, -Pos, "?", , vbRed, 3)
  End If
  'Inizio Pezzo
    X(55) = X(51)
    Y(55) = 0
    X(56) = X(51)
    Y(56) = 0 + 5
   
   Dim stmp As String
   
    If (CodTesta = 1) Then
       stmp = str(QuoStart2)
    Else
       stmp = str(QuoStart1)
    End If
   

End Sub


Public Sub Disegna_Correttori_Elica_MV(Stampante As Boolean)

'
Dim LatoPart  As Integer
Dim nZone     As Integer
Dim X()       As Double
Dim Y()       As Double
Dim Con1F1    As Double
Dim Con2F1    As Double
Dim Con3F1    As Double
Dim Con1F2    As Double
Dim Con2F2    As Double
Dim Con3F2    As Double
Dim Bom1F1    As Double
Dim Bom2F1    As Double
Dim Bom3F1    As Double
Dim Bom1F2    As Double
Dim Bom2F2    As Double
Dim Bom3F2    As Double
Dim MaxX1     As Double
Dim MaxX2     As Double
Dim scalaxEl  As Double
Dim Pos       As Double
Dim XF0       As Double
Dim XF1       As Double
Dim XF2       As Double
Dim XF3       As Double
Dim XF0_2     As Double
Dim XF1_2     As Double
Dim XF2_2     As Double
Dim XF3_2     As Double
Dim Ft        As Double
Dim Ft2       As Double
Dim F1        As Double
Dim F2        As Double
Dim F3        As Double
Dim F1_2      As Double
Dim F2_2      As Double
Dim F3_2      As Double
Dim Re        As Double
Dim RS        As Double
Dim LS        As Double
Dim tColore   As Long
Dim tX1       As Double
Dim tX        As Double
Dim tY        As Double
Dim i         As Integer
Dim NotScale  As Boolean
Dim Lin       As Double
Dim CORSA_O   As Double
Dim CORSA_I   As Double
Dim F1T1      As Double
Dim F1T2      As Double
Dim F1T3      As Double
Dim F2T1      As Double
Dim F2T2      As Double
Dim F2T3      As Double
Dim QuoStart1 As Double
Dim QuoStart2 As Double
Dim CodTesta  As Integer
Dim nriga     As Long
Dim nstr      As Long
Dim riga      As String
Dim CorCon1Fsx    As Double
Dim CorCon2Fsx    As Double
Dim CorCon3Fsx    As Double
Dim CorCon1Fdx    As Double
Dim CorCon2Fdx    As Double
Dim CorCon3Fdx    As Double
Dim CorBom1Fsx    As Double
Dim CorBom2Fsx    As Double
Dim CorBom3Fsx    As Double
Dim CorBom1Fdx    As Double
Dim CorBom2Fdx    As Double
Dim CorBom3Fdx    As Double
Dim VCon1F1       As Double
Dim VCon2F1       As Double
Dim VCon3F1       As Double
Dim VCon1F2       As Double
Dim VCon2F2       As Double
Dim VCon3F2       As Double
Dim VBom1F1       As Double
Dim VBom2F1       As Double
Dim VBom3F1       As Double
Dim VBom1F2       As Double
Dim VBom2F2       As Double
Dim VBom3F2       As Double
Dim FiancoSx      As Integer

Dim XP0           As Double
Dim XP1           As Double
Dim XP2           As Double
Dim Xp3           As Double
Dim Xt            As Double
Dim Yt            As Double
Dim Valore        As String

'
On Error Resume Next
  '
  Call InizializzaVariabili
  '
  NotScale = False
  '
  scalaxEl = Lp("iScalaX")
  If (scalaxEl = 0) Then scalaxEl = Lp("iScalaX")
  LatoPart = Lp("LatoPA")
  CORSA_I = Lp("CORSAIN")
  CORSA_O = Lp("CORSAOUT")
  nZone = Lp("iNzone")
  
  CodTesta = Lp("INDTAV_G")
  If (CodTesta = 1) Then
    QuoStart2 = Lp("QASSIALESTART_G[1,1]")
  Else
    QuoStart1 = Lp("QASSIALESTART_G[0,1]")
  End If
  
  
  F1 = 0:   F2 = 0:   F3 = 0
  F1_2 = 0: F2_2 = 0: F3_2 = 0
  
  '
  'Disegno correzioni elica
  ReDim X(60):  ReDim Y(60)
  
  'Valori impostati
  VCon1F1 = Lp("iCon1Fsx"):  VCon2F1 = Lp("iCon2Fsx"):  VCon3F1 = Lp("iCon3Fsx")
  VCon1F2 = Lp("iCon1Fdx"):  VCon2F2 = Lp("iCon2Fdx"):  VCon3F2 = Lp("iCon3Fdx")
  '
  VBom1F1 = Lp("iBom1Fsx"):  VBom2F1 = Lp("iBom2Fsx"):  VBom3F1 = Lp("iBom3Fsx")
  VBom1F2 = Lp("iBom1Fdx"):  VBom2F2 = Lp("iBom2Fdx"):  VBom3F2 = Lp("iBom3Fdx")
  
  'Solo Correzioni
  CorCon1Fsx = Lp("iCorCon1Fsx") / 1000: CorCon2Fsx = Lp("iCorCon2Fsx") / 1000: CorCon3Fsx = Lp("iCorCon3Fsx") / 1000
  CorCon1Fdx = Lp("iCorCon1Fdx") / 1000: CorCon2Fdx = Lp("iCorCon2Fdx") / 1000: CorCon3Fdx = Lp("iCorCon3Fdx") / 1000
  '
  CorBom1Fsx = Lp("iCorBom1Fsx") / 1000: CorBom2Fsx = Lp("iCorBom2Fsx") / 1000: CorBom3Fsx = Lp("iCorBom3Fsx") / 1000
  CorBom1Fdx = Lp("iCorBom1Fdx") / 1000: CorBom2Fdx = Lp("iCorBom2Fdx") / 1000: CorBom3Fdx = Lp("iCorBom3Fdx") / 1000
  
  
  'Gestione Lato serraggio - Inverto solo le correzione
  FiancoSx = Lp("iFiancoSX")
  
  If FiancoSx = 1 Then
  
  'Correzioni Elica Totali se F1=Fsx - Lato Serraggio in misura � in Basso
  Con1F1 = VCon1F1 + CorCon1Fsx: Con2F1 = VCon2F1 + CorCon2Fsx: Con3F1 = VCon3F1 + CorCon3Fsx
  Con1F2 = VCon1F2 + CorCon1Fdx: Con2F2 = VCon2F2 + CorCon2Fdx: Con3F2 = VCon3F2 + CorCon3Fdx
  '
  Bom1F1 = VBom1F1 + CorBom1Fsx: Bom2F1 = VBom2F1 + CorBom2Fsx: Bom3F1 = VBom3F1 + CorBom3Fsx
  Bom1F2 = VBom1F2 + CorBom1Fdx: Bom2F2 = VBom2F2 + CorBom2Fdx: Bom3F2 = VBom3F2 + CorBom3Fdx
  
  Else
  'Correzioni Elica Totali se F2=Fsx - Lato Serraggio in misura � in Alto
  
  'Valore Totale del Fianco sx del grafico = Valore Teorico Fianco 2 + Corr. Fianco sx del grafico
  Con1F1 = VCon1F2 + CorCon1Fsx: Con2F1 = VCon2F2 + CorCon2Fsx: Con3F1 = VCon3F2 + CorCon3Fsx

  'Valore Totale del Fianco dx del grafico = Valore Teorico Fianco 1 + Corr. Fianco dx del grafico
  Con1F2 = VCon1F1 + CorCon1Fdx: Con2F2 = VCon2F1 + CorCon2Fdx: Con3F2 = VCon3F1 + CorCon3Fdx
  '
  Bom1F1 = VBom1F2 + CorBom1Fsx: Bom2F1 = VBom2F2 + CorBom2Fsx: Bom3F1 = VBom3F2 + CorBom3Fsx
  Bom1F2 = VBom1F1 + CorBom1Fdx: Bom2F2 = VBom2F1 + CorBom2Fdx: Bom3F2 = VBom3F1 + CorBom3Fdx

  End If
  

  
  'Calcolo delle Zone comprese le Extracorse
  
  Select Case nZone
    Case 1
        F1 = CORSA_I + Lp("iFas1Fsx") + CORSA_O: F1_2 = CORSA_I + Lp("iFas1Fdx") + CORSA_O
    Case 2
        F1T1 = Lp("iFas1Fsx"): F1T2 = Lp("iFas2Fsx")
        If (F1T1 > 0) And (F1T2 > 0) Then
           F1 = F1T1 + CORSA_I
           F2 = F1T2 + CORSA_O
        Else
          If (F1T1 > 0) And (F1T2 = 0) Then
             F1 = F1T1 + CORSA_I + CORSA_O
             F2 = F1T2
          Else
             If (F1T1 = 0) And (F1T2 > 0) Then
                F1 = F1T1
                F2 = F1T2 + CORSA_I + CORSA_O
             Else
                F1 = F1T1 + CORSA_I
                F2 = F1T2 + CORSA_O
             End If
          End If
        End If
        
        F2T1 = Lp("iFas1Fdx"): F2T2 = Lp("iFas2Fdx")
        If (F2T1 > 0) And (F2T2 > 0) Then
           F1_2 = F2T1 + CORSA_I
           F2_2 = F2T2 + CORSA_O
        Else
          If (F2T1 > 0) And (F2T2 = 0) Then
             F1_2 = F2T1 + CORSA_I + CORSA_O
             F2_2 = F2T2
          Else
             If (F2T1 = 0) And (F2T2 > 0) Then
                F1_2 = F2T1
                F2_2 = F2T2 + CORSA_I + CORSA_O
             Else
                F1_2 = F2T1 + CORSA_I
                F2_2 = F2T2 + CORSA_O
             End If
          End If
        End If
    
    Case 3
        
        F1T1 = Lp("iFas1Fsx"): F1T2 = Lp("iFas2Fsx"): F1T3 = Lp("iFas3Fsx")
        If (F1T1 > 0) And (F1T2 > 0) And (F1T3 > 0) Then
           F1 = F1T1 + CORSA_I
           F2 = F1T2
           F3 = F1T3 + CORSA_O
        Else
          If (F1T1 > 0) And (F1T2 > 0) And (F1T3 = 0) Then
             F1 = F1T1 + CORSA_I
             F2 = F1T2 + CORSA_O
             F3 = F1T3
          Else
            If (F1T1 > 0) And (F1T2 = 0) And (F1T3 = 0) Then
               F1 = F1T1 + CORSA_I + CORSA_O
               F2 = F1T2
               F3 = F1T3
            Else
               If (F1T1 > 0) And (F1T2 = 0) And (F1T3 > 0) Then
                  F1 = F1T1 + CORSA_I
                  F2 = F1T2
                  F3 = F1T3 + CORSA_O
               Else
                  If (F1T1 = 0) And (F1T2 > 0) And (F1T3 > 0) Then
                     F1 = F1T1
                     F2 = F1T2 + CORSA_I
                     F3 = F1T3 + CORSA_O
                  Else
                     If (F1T1 = 0) And (F1T2 = 0) And (F1T3 > 0) Then
                        F1 = F1T1
                        F2 = F1T2
                        F3 = F1T3 + CORSA_I + CORSA_O
                     Else
                        F1 = F1T1 + CORSA_I
                        F2 = F1T2
                        F3 = F1T3 + CORSA_O
                     End If
                  End If
               End If
            End If
          End If
        End If
          
        F2T1 = Lp("iFas1Fdx"): F2T2 = Lp("iFas2Fdx"): F2T3 = Lp("iFas3Fdx")
        If (F2T1 > 0) And (F2T2 > 0) And (F2T3 > 0) Then
           F1_2 = F2T1 + CORSA_I
           F2_2 = F2T2
           F3_2 = F2T3 + CORSA_O
        Else
          If (F2T1 > 0) And (F2T2 > 0) And (F2T3 = 0) Then
             F1_2 = F2T1 + CORSA_I
             F2_2 = F2T2 + CORSA_O
             F3_2 = F2T3
          Else
            If (F2T1 > 0) And (F2T2 = 0) And (F2T3 = 0) Then
               F1_2 = F2T1 + CORSA_I + CORSA_O
               F2_2 = F2T2
               F3_2 = F2T3
            Else
               If (F2T1 > 0) And (F2T2 = 0) And (F2T3 > 0) Then
                  F1_2 = F2T1 + CORSA_I
                  F2_2 = F2T2
                  F3_2 = F2T3 + CORSA_O
               Else
                  If (F2T1 = 0) And (F2T2 > 0) And (F2T3 > 0) Then
                     F1_2 = F2T1
                     F2_2 = F2T2 + CORSA_I
                     F3_2 = F2T3 + CORSA_O
                  Else
                     If (F2T1 = 0) And (F2T2 = 0) And (F2T3 > 0) Then
                        F1_2 = F2T1
                        F2_2 = F2T2
                        F3_2 = F2T3 + CORSA_I + CORSA_O
                     Else
                        F1_2 = F2T1 + CORSA_I
                        F2_2 = F2T2
                        F3_2 = F2T3 + CORSA_O
                     End If
                  End If
               End If
            End If
          End If
        End If
            
  End Select
  
  Re = 10:  RS = 10:  LS = 10
  '
  Ft = F1 + F2 + F3         'Fascia Totale F1 con Extracorse
  Ft2 = F1_2 + F2_2 + F3_2  'Fascia Totale F2 con Extracorse
  
  'Posizioni X della fine di ciascuna Zona
  If FiancoSx = 1 Then
     XF0 = LS:    XF1 = XF0 + F1:        XF2 = XF1 + F2:        XF3 = XF2 + F3
     XF0_2 = LS:  XF1_2 = XF0_2 + F1_2:  XF2_2 = XF1_2 + F2_2:  XF3_2 = XF2_2 + F3_2
  Else
     XF3 = LS:    XF2 = XF3 + F3:        XF1 = XF2 + F2:        XF0 = XF1 + F1
     XF3_2 = LS:  XF2_2 = XF3_2 + F3_2:  XF1_2 = XF2_2 + F2_2:  XF0_2 = XF1_2 + F1_2
  End If
  
  '
  Pos = RS
  'Assegno le pos. X ad un vettore X(n)
  If FiancoSx = 1 Then
  'Zona 1 inizia in basso
  X(0) = XF0
  X(1) = XF1
  X(2) = XF2
  X(3) = XF3
  X(16) = XF3
  Else
  'Zona 1 in inizia in alto
  X(0) = XF3
  X(1) = XF2
  X(2) = XF1
  X(3) = XF0
  X(16) = XF0
  End If
  
  If FiancoSx = 1 Then
    Y(0) = Pos
    Y(1) = Pos + (Con1F1 * 1 + Con2F1 * 0 + Con3F1 * 0) * scalaxEl
    Y(2) = Pos + (Con1F1 * 1 + Con2F1 * 1 + Con3F1 * 0) * scalaxEl
    Y(3) = Pos + (Con1F1 * 1 + Con2F1 * 1 + Con3F1 * 1) * scalaxEl
  Else
    Y(3) = Pos
    Y(2) = Pos + (Con1F1 * 1 + Con2F1 * 0 + Con3F1 * 0) * scalaxEl
    Y(1) = Pos + (Con1F1 * 1 + Con2F1 * 1 + Con3F1 * 0) * scalaxEl
    Y(0) = Pos + (Con1F1 * 1 + Con2F1 * 1 + Con3F1 * 1) * scalaxEl
  End If
  Y(16) = Pos
  
  If FiancoSx = 1 Then
  X(4) = XF0_2
  X(5) = XF1_2
  X(6) = XF2_2
  X(7) = XF3_2
  X(17) = XF3_2
  Else
  X(4) = XF3_2
  X(5) = XF2_2
  X(6) = XF1_2
  X(7) = XF0_2
  X(17) = XF0_2
  End If
  
  If FiancoSx = 1 Then
    Y(4) = -Pos
    Y(5) = -Pos - (Con1F2 * 1 + Con2F2 * 0 + Con3F2 * 0) * scalaxEl
    Y(6) = -Pos - (Con1F2 * 1 + Con2F2 * 1 + Con3F2 * 0) * scalaxEl
    Y(7) = -Pos - (Con1F2 * 1 + Con2F2 * 1 + Con3F2 * 1) * scalaxEl
  Else
    Y(7) = -Pos
    Y(6) = -Pos - (Con1F2 * 1 + Con2F2 * 0 + Con3F2 * 0) * scalaxEl
    Y(5) = -Pos - (Con1F2 * 1 + Con2F2 * 1 + Con3F2 * 0) * scalaxEl
    Y(4) = -Pos - (Con1F2 * 1 + Con2F2 * 1 + Con3F2 * 1) * scalaxEl
  End If
  Y(17) = -Pos
  
  'Lineette
  X(8) = X(1)
  X(9) = X(1)
  X(12) = X(5)
  X(13) = X(5)
  X(10) = X(2)
  X(11) = X(2)
  X(14) = X(6)
  X(15) = X(6)
  Lin = 3
  Y(8) = Y(1) + Lin
  Y(9) = Y(1) - Lin
  Y(12) = Y(5) + Lin
  Y(13) = Y(5) - Lin
  Y(10) = Y(2) + Lin
  Y(11) = Y(2) - Lin
  Y(14) = Y(6) + Lin
  Y(15) = Y(6) - Lin
  
  
  Dim StepGriglia As Double
  StepGriglia = 0.05
  For i = -3 To 3
    Y(21 + i) = Y(0) + i * StepGriglia * scalaxEl
    Y(28 + i) = Y(0) + i * StepGriglia * scalaxEl
    Y(35 + i) = Y(4) + i * StepGriglia * scalaxEl
    Y(42 + i) = Y(4) + i * StepGriglia * scalaxEl
    X(21 + i) = XF0
    X(28 + i) = XF3
    X(35 + i) = XF0
    X(42 + i) = XF3
  Next i
  
  'Pos. extracorse
  If FiancoSx = 1 Then
    X(51) = XF0 + CORSA_I
    X(52) = X(51)
    X(53) = XF3 - CORSA_O
    X(54) = X(53)
  Else
    X(51) = XF0 - CORSA_I
    X(52) = X(51)
    X(53) = XF3 + CORSA_O
    X(54) = X(53)
  End If
  
  'Linee extracorsa
  If FiancoSx = 1 Then
  Y(51) = Y(0)
  Y(52) = Y(0)
  Y(53) = Y(0)
  Else
  Y(51) = Y(3)
  Y(52) = Y(3)
  Y(53) = Y(3)
  End If
  
  If FiancoSx = 1 Then
  PAR.MaxY = Y(0)
  PAR.MinY = Y(4)
  Else
  PAR.MaxY = Y(3)
  PAR.MinY = Y(7)
  End If
  
  For i = 1 To 3
    If Y(i) > PAR.MaxY Then PAR.MaxY = Y(i)
    If Y(i + 4) < PAR.MinY Then PAR.MinY = Y(i + 4)
  Next i
  If Ft >= Ft2 Then
    PAR.MaxX = Ft + LS
  Else
    PAR.MaxX = Ft2 + LS
  End If
  PAR.MinX = LS / 2
  'par.MaxY = Re / 2
  'par.MinY = -Re / 2
  PAR.ScalaMargine = 0.9
  PAR.Allineamento = 0
  Call CalcolaParPic(PAR, True, Stampante)
  'par.Pic.Cls
  'linea XMola, Re + 20 + DiaMola, XMola, Re, vbRed, 1, vbDash
  MaxX1 = X(1)
  MaxX2 = X(5)
  
  If FiancoSx = 1 Then
  XP0 = XF0
  XP1 = XF1
  Else
  XP0 = XF2
  XP1 = XF3
  End If
  
  
  If Not NotScale Then
    Select Case nZone 'Disegno ed evidenzio in rosso le corse delle singole Zone
      Case 1
        Call QuotaH(XF0, Re, XF1, Re, "" & F1, 12, CalcoloColore("iFas1Fsx"), 4)
        Call QuotaH(XF0_2, -Re, XF1_2, -Re, "" & F1_2, 12, CalcoloColore("iFas1Fdx"), 3)
      
      Case 2
        Call QuotaH(XF0, Re, XF1, Re, "" & F1, 12, CalcoloColore("iFas1Fsx"), 4)
        Call QuotaH(XF1, Re, XF2, Re, "" & F2, 12, CalcoloColore("iFas2Fsx"), 4)
        Call QuotaH(XF0_2, -Re, XF1_2, -Re, "" & F1_2, 12, CalcoloColore("iFas1Fdx"), 3)
        Call QuotaH(XF1_2, -Re, XF2_2, -Re, "" & F2_2, 12, CalcoloColore("iFas2Fdx"), 3)
        MaxX1 = X(2)
        MaxX2 = X(6)
      Case 3
        
        If (VarCorr = "iFas1Fsx") Then
           If FiancoSx = 1 Then
              Call QuotaH(XF0, Re, XF1, Re, "" & F1, 12, CalcoloColore("iFas1Fsx"), 4, 1)
           Else
              Call QuotaH(XF1, Re, XF0, Re, "" & F1, 12, CalcoloColore("iFas1Fsx"), 4, 1)
           End If
        Else
           If FiancoSx = 1 Then
              Call QuotaH(XF0, Re, XF1, Re, "" & F1, 12, CalcoloColore("iFas1Fsx"), 4)
           Else
              Call QuotaH(XF1, Re, XF0, Re, "" & F1, 12, CalcoloColore("iFas1Fsx"), 4)
           End If
        End If
        
        If (VarCorr = "iFas2Fsx") Then
           If FiancoSx = 1 Then
              Call QuotaH(XF1, Re, XF2, Re, "" & F2, 12, CalcoloColore("iFas2Fsx"), 4, 1)
           Else
              Call QuotaH(XF2, Re, XF1, Re, "" & F2, 12, CalcoloColore("iFas2Fsx"), 4, 1)
           End If
        Else
           If FiancoSx = 1 Then
              Call QuotaH(XF1, Re, XF2, Re, "" & F2, 12, CalcoloColore("iFas2Fsx"), 4)
           Else
              Call QuotaH(XF2, Re, XF1, Re, "" & F2, 12, CalcoloColore("iFas2Fsx"), 4)
           End If
        End If
        
        If (VarCorr = "iFas3Fsx") Then
           If FiancoSx = 1 Then
              Call QuotaH(XF2, Re, XF3, Re, "" & F3, 12, CalcoloColore("iFas3Fsx"), 4, 1)
           Else
              Call QuotaH(XF3, Re, XF2, Re, "" & F3, 12, CalcoloColore("iFas3Fsx"), 4, 1)
           End If
        Else
           If FiancoSx = 1 Then
              Call QuotaH(XF2, Re, XF3, Re, "" & F3, 12, CalcoloColore("iFas3Fsx"), 4)
           Else
              Call QuotaH(XF3, Re, XF2, Re, "" & F3, 12, CalcoloColore("iFas3Fsx"), 4)
           End If
        End If
        
        If FiancoSx = 1 Then
           Call QuotaH(XF0_2, -Re, XF1_2, -Re, "" & F1_2, 12, CalcoloColore("iFas1Fdx"), 3)
           Call QuotaH(XF1_2, -Re, XF2_2, -Re, "" & F2_2, 12, CalcoloColore("iFas2Fdx"), 3)
           Call QuotaH(XF2_2, -Re, XF3_2, -Re, "" & F3_2, 12, CalcoloColore("iFas3Fdx"), 3)
        Else
           Call QuotaH(XF1_2, -Re, XF0_2, -Re, "" & F1_2, 12, CalcoloColore("iFas1Fdx"), 3)
           Call QuotaH(XF2_2, -Re, XF1_2, -Re, "" & F2_2, 12, CalcoloColore("iFas2Fdx"), 3)
           Call QuotaH(XF3_2, -Re, XF2_2, -Re, "" & F3_2, 12, CalcoloColore("iFas3Fdx"), 3)
        End If
        
        MaxX1 = X(3)
        MaxX2 = X(7)
    End Select
  End If
  
  ' Asse pezzo
  Call linea(LS, 0, LS + Ft, 0, RGB(180, 180, 180), 1, vbDashDot)
  
  Y(51) = PAR.MaxY + 8
  
  Dim tidC    As Long
  
  If Not NotScale Then
    'Disegno Extracorsa lato testa
    tidC = CalcoloColore("iCORSAIN", , , tColore)
    If FiancoSx = 1 Then
       Call QuotaH(XF0, 0, X(51), 0, "" & CORSA_I, 12, tidC, 3)
    Else
       Call QuotaH(X(51), 0, XF0, 0, "" & CORSA_I, 12, tidC, 3)
    End If
    
    'Disegna fascia totale
    tidC = CalcoloColore("FASCIATOTALE_G", , , tColore)
    If FiancoSx = 1 Then
       Call QuotaH(X(51), 0, X(53), 0, "" & (Ft - CORSA_I - CORSA_O), 12, tidC, 3)
    Else
       Call QuotaH(X(53), 0, X(51), 0, "" & (Ft - CORSA_I - CORSA_O), 12, tidC, 3)
    End If
    
    'Disegno Extracorsa lato contropunta
    tidC = CalcoloColore("iCORSAOUT", , , tColore)
    If FiancoSx = 1 Then
       Call QuotaH(X(53), 0, XF3, 0, "" & CORSA_O, 12, tidC, 3)
    Else
       Call QuotaH(XF3, 0, X(53), 0, "" & CORSA_O, 12, tidC, 3)
    End If
  End If
  
  Dim tidP1   As Integer
  Dim tidP2   As Integer
  Dim tidst   As Integer
  Dim tisp    As Integer
  tColore = vbBlue
  tidst = vbSolid
  
  'Linee (griglia) A cosa servono?
  For i = -3 To 3
    tidP1 = 21 + i
    tidP2 = 28 + i
    'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), RGB(200, 200, 200), 1, vbDot)
    tidP1 = 35 + i
    tidP2 = 42 + i
    'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), RGB(200, 200, 200), 1, vbDot)
  Next i
    
  'Fianco1 Fascia1
  If FiancoSx = 1 Then
     tidP1 = 0
     tidP2 = 1
  Else
     tidP1 = 2
     tidP2 = 3
  End If
    
  If (VarCorr = "iFas1Fsx") Or (VarCorr = "iCorCon1Fsx") Then
     tidC = vbRed
     If (VarCorr = "iFas1Fsx") Then
        If Con1F1 <> 0 Then
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
        Else
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC)
        End If
     End If
     If (VarCorr = "iCorCon1Fsx") Then
        If Con1F1 <> 0 Then
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 2)
           Xt = (X(tidP1) + X(tidP2)) / 2: Yt = (Y(tidP1) + Y(tidP2)) / 2
           Valore = frmt(Con1F1 * 1000, 1) & "�"
           Call TestoAll(Xt, Yt, Valore, vbBlack, , 3)
        Else
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack)
        End If
     End If
     tidC = vbBlue
  Else
     tidC = CalcoloColore("iCorBom1Fsx", , , tColore)
     Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
  End If
  tisp = IIf(tidC = vbRed, 2, 1)
  If Bom1F1 <> 0 Then
    Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), Bom1F1, scalaxEl, tidC, tisp)
     If (VarCorr = "iCorBom1Fsx") Then
        Xt = (X(tidP1) + X(tidP2)) / 2
        If (Y(tidP1) >= Y(tidP2)) Then 'Yt = (Y(tidP1) + Y(tidP2)) / 2
        Yt = Y(tidP1)
        Else
        Yt = Y(tidP2)
        End If
        Valore = frmt(Bom1F1 * 1000, 1) & "�"
        Call TestoAll(Xt, Yt, Valore, vbBlack, , 3)
     End If
  End If
  
  'Fianco1 Fascia2
  tidP1 = 1
  tidP2 = 2
  If VarCorr = "iFas2Fsx" Or (VarCorr = "iCorCon2Fsx") Then
     tidC = vbRed
     If (VarCorr = "iFas2Fsx") Then
        If Con2F1 <> 0 Then
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
        Else
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC)
        End If
     End If
     If (VarCorr = "iCorCon2Fsx") Then
        If Con2F1 <> 0 Then
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 2)
           Xt = (X(tidP1) + X(tidP2)) / 2: Yt = (Y(tidP1) + Y(tidP2)) / 2
           Valore = frmt(Con2F1 * 1000, 1) & "�"
           Call TestoAll(Xt, Yt, Valore, vbBlack, , 3)
        Else
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack)
        End If
     End If
     tidC = vbBlue
  Else
     tidC = CalcoloColore("iCorBom2Fsx", , , tColore)
     Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
  End If
  tisp = IIf(tidC = vbRed, 2, 1)
  If Bom2F1 <> 0 Then
    Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), Bom2F1, scalaxEl, tidC, tisp)
     If (VarCorr = "iCorBom2Fsx") Then
        Xt = (X(tidP1) + X(tidP2)) / 2
        If (Y(tidP1) >= Y(tidP2)) Then
        Yt = Y(tidP1)
        Else
        Yt = Y(tidP2)
        End If
        Valore = frmt(Bom2F1 * 1000, 1) & "�"
        Call TestoAll(Xt, Yt, Valore, vbBlack, , 3)
     End If
  End If
  
  'Fianco1 Fascia3
  If FiancoSx = 1 Then
     tidP1 = 2
     tidP2 = 3
  Else
     tidP1 = 0
     tidP2 = 1
  End If
  
  If VarCorr = "iFas3Fsx" Or (VarCorr = "iCorCon3Fsx") Then
     tidC = vbRed
     If (VarCorr = "iFas3Fsx") Then
        If Con3F1 <> 0 Then
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
        Else
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC)
        End If
     End If
     If (VarCorr = "iCorCon3Fsx") Then
        If Con3F1 <> 0 Then
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 2)
           Xt = (X(tidP1) + X(tidP2)) / 2: Yt = (Y(tidP1) + Y(tidP2)) / 2
           Valore = frmt(Con3F1 * 1000, 1) & "�"
           Call TestoAll(Xt, Yt, Valore, vbBlack, , 3)
        Else
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack)
        End If
     End If
     tidC = vbBlue
  Else
     tidC = CalcoloColore("iCorBom3Fsx", , , tColore)
     Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
  End If
  tisp = IIf(tidC = vbRed, 2, 1)
  If Bom3F1 <> 0 Then
    Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), Bom3F1, scalaxEl, tidC, tisp)
     If (VarCorr = "iCorBom3Fsx") Then
        Xt = (X(tidP1) + X(tidP2)) / 2
        If (Y(tidP1) >= Y(tidP2)) Then
        Yt = Y(tidP1)
        Else
        Yt = Y(tidP2)
        End If
        Valore = frmt(Bom3F1 * 1000, 1) & "�"
        Call TestoAll(Xt, Yt, Valore, vbBlack, , 3)
     End If
  End If
  
  'Fianco 1 Separazione Zone 1 e 2
  tidC = tColore
  tisp = 1
  tidP1 = 8
  tidP2 = 9
  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, tidst)
    
  'Fianco 1 Separazione Zone 2 e 3
  tidP1 = 10
  tidP2 = 11
  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, tidst)
  'Fianco 1 Linea tra primo e ultimo punto
  tidP1 = 0
  tidP2 = 16
  
  'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, vbDot)
  
  'Fianco2 Fascia1
  If FiancoSx = 1 Then
     tidP1 = 4
     tidP2 = 5
  Else
     tidP1 = 6
     tidP2 = 7
  End If
  
  If (VarCorr = "iFas1Fdx") Or (VarCorr = "iCorCon1Fdx") Then
     tidC = vbRed
     If (VarCorr = "iFas1Fdx") Then
        If Con1F2 <> 0 Then
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
        Else
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC)
        End If
     End If
     If (VarCorr = "iCorCon1Fdx") Then
        If Con1F2 <> 0 Then
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 2)
           Xt = (X(tidP1) + X(tidP2)) / 2: Yt = (Y(tidP1) + Y(tidP2)) / 2
           Valore = frmt(Con1F2 * 1000, 1) & "�"
           Call TestoAll(Xt, Yt, Valore, vbBlack, , 4)
        Else
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack)
        End If
     End If
     tidC = vbBlue
  Else
     tidC = CalcoloColore("iCorBom1Fdx", , , tColore)
     Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
  End If
  tisp = IIf(tidC = vbRed, 2, 1)
  If Bom1F2 <> 0 Then
    Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), -Bom1F2, scalaxEl, tidC, tisp)
     If (VarCorr = "iCorBom1Fdx") Then
        Xt = (X(tidP1) + X(tidP2)) / 2
        If (Y(tidP1) <= Y(tidP2)) Then
        Yt = Y(tidP1)
        Else
        Yt = Y(tidP2)
        End If
        Valore = frmt(Bom1F2 * 1000, 1) & "�"
        Call TestoAll(Xt, Yt, Valore, vbBlack, , 4)
     End If
  End If
  
  'Fianco2 Fascia2
  tidP1 = 5
  tidP2 = 6
  If (VarCorr = "iFas2Fdx") Or (VarCorr = "iCorCon2Fdx") Then
     tidC = vbRed
     If (VarCorr = "iFas2Fdx") Then
        If Con2F2 <> 0 Then
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
        Else
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC)
        End If
     End If
     If (VarCorr = "iCorCon2Fdx") Then
        If Con2F2 <> 0 Then
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 2)
           Xt = (X(tidP1) + X(tidP2)) / 2: Yt = (Y(tidP1) + Y(tidP2)) / 2
           Valore = frmt(Con2F2 * 1000, 1) & "�"
           Call TestoAll(Xt, Yt, Valore, vbBlack, , 4)
        Else
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack)
        End If
     End If
     tidC = vbBlue
  Else
     tidC = CalcoloColore("iCorBom2Fdx", , , tColore)
     Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
  End If
  tisp = IIf(tidC = vbRed, 2, 1)
  If Bom2F2 <> 0 Then
    Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), -Bom2F2, scalaxEl, tidC, tisp)
     If (VarCorr = "iCorBom2Fdx") Then
        Xt = (X(tidP1) + X(tidP2)) / 2
        If (Y(tidP1) <= Y(tidP2)) Then
        Yt = Y(tidP1)
        Else
        Yt = Y(tidP2)
        End If
        Valore = frmt(Bom2F2 * 1000, 1) & "�"
        Call TestoAll(Xt, Yt, Valore, vbBlack, , 4)
     End If
  End If
  
  'Fianco2 Fascia3
  tidP1 = 6
  tidP2 = 7
  If FiancoSx = 1 Then
     tidP1 = 6
     tidP2 = 7
  Else
     tidP1 = 4
     tidP2 = 5
  End If
  If (VarCorr = "iFas3Fdx") Or (VarCorr = "iCorCon3Fdx") Then
     tidC = vbRed
     If (VarCorr = "iFas3Fdx") Then
        If Con3F2 <> 0 Then
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
        Else
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC)
        End If
     End If
     If (VarCorr = "iCorCon3Fdx") Then
        If Con3F2 <> 0 Then
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 2)
           Xt = (X(tidP1) + X(tidP2)) / 2: Yt = (Y(tidP1) + Y(tidP2)) / 2
           Valore = frmt(Con3F2 * 1000, 1) & "�"
           Call TestoAll(Xt, Yt, Valore, vbBlack, , 4)
        Else
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack)
        End If
     End If
     tidC = vbBlue
  Else
     tidC = CalcoloColore("iCorBom3Fdx", , , tColore)
     Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
  End If
  tisp = IIf(tidC = vbRed, 2, 1)
  If Bom3F2 <> 0 Then
    Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), -Bom3F2, scalaxEl, tidC, tisp)
     If (VarCorr = "iCorBom3Fdx") Then
        Xt = (X(tidP1) + X(tidP2)) / 2
        If (Y(tidP1) <= Y(tidP2)) Then
        Yt = Y(tidP1)
        Else
        Yt = Y(tidP2)
        End If
        Valore = frmt(Bom3F2 * 1000, 1) & "�"
        Call TestoAll(Xt, Yt, Valore, vbBlack, , 4)
     End If
  End If
  tidC = tColore
  tisp = 1
  
  'Fianco 2 Separazione Zone 1 e 2
  tidP1 = 12
  tidP2 = 13
  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, tidst)
  'Fianco 2 Separazione Zone 2 e 3
  tidP1 = 14
  tidP2 = 15
  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, tidst)
  'Fianco 1 Linea tra primo e ultimo punto
  tidP1 = 4
  tidP2 = 17
  
  'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, vbDot)
  
  'Scritte F1 e F2
  Call TestoAll(LS, RS, "FL", , True, 1)
  Call TestoAll(LS, -RS, "FR", , True, 1)
  If NotScale Then Call TestoInfo("DRAWING NOT IN SCALE ! ", , True, vbRed, True)
  nriga = 1530
  nstr = LoadString(g_hLanguageLibHandle, nriga, riga, 255)
  If nstr = 0 Then
     riga = "Down"
  End If
  Call TestoInfo(riga, 1)
  nriga = 1531
  nstr = LoadString(g_hLanguageLibHandle, nriga, riga, 255)
  If nstr = 0 Then
     riga = "Up"
  End If
  Call TestoInfo(riga, 2)
  '
  If FiancoSx = 1 Then
     If LINGUA = "GR" Then
      Call TestoInfo("Spannseite", 4)
     Else
      Call TestoInfo("Clamping Side", 4)
     End If
     Call TestoInfo("Units: [mm]", 3)
  Else
     If LINGUA = "GR" Then
      Call TestoInfo("Spannseite", 3)
     Else
      Call TestoInfo("Clamping Side", 3)
     End If
     Call TestoInfo("Units: [mm]", 4)
  End If
  If Ft2 > Ft Then
    Call TestoInfo("WARNING: (FT FL = " & Ft & ") < (FT FR = " & Ft2 & ")", , True, vbRed, True)
    Call QuotaH(MaxX1, Pos, MaxX2, Pos, "?", , vbRed, 3)
  ElseIf Ft2 < Ft Then
    Call TestoInfo("WARNING: (FT FL = " & Ft & ") > (FT FR = " & Ft2 & ")", , True, vbRed, True)
    Call QuotaH(MaxX2, -Pos, MaxX1, -Pos, "?", , vbRed, 3)
  End If
  'Inizio Pezzo
    X(55) = X(51)
    Y(55) = 0
    X(56) = X(51)
    Y(56) = 0 + 5
   
   Dim stmp As String
   
    If (CodTesta = 1) Then
       stmp = str(QuoStart2)
    Else
       stmp = str(QuoStart1)
    End If


'   If (VarCorr = "QASSIALESTART_G[0,1]") Or (VarCorr = "QASSIALESTART_G[1,1]") Then
'      Call linea(X(55), Y(55), X(56), Y(56), vbRed, 2, vbDot)
'      Call QuotaH(X(55), Y(55), X(55), Y(55), "(Z)", , vbRed, 4, 2, 4)
'      If (VarCorr = "QASSIALESTART_G[0,1]") Then
'          QuoStart1 = Lp("QASSIALESTART_G[0,1]")
'          stmp = str(QuoStart1)
'          Call QuotaH(X(55), Y(55) - 2, X(55), Y(55) - 2, stmp, , vbBlack, 4, 2, 4)
'      Else
'          QuoStart2 = Lp("QASSIALESTART_G[1,1]")
'          stmp = str(QuoStart2)
'          Call QuotaH(X(55), Y(55) - 2, X(55), Y(55) - 2, stmp, , vbBlack, 4, 2, 4)
'      End If
'   Else
'      Call linea(X(55), Y(55), X(56), Y(56), vbRed, 1, vbDot)
'      Call QuotaH(X(55), Y(55), X(55), Y(55), "(Z)", , vbBlack, 4, , 4)
'      Call QuotaH(X(55), Y(55) - 2, X(55), Y(55) - 2, stmp, , vbBlack, 4, 2, 4)
'   End If
   
   Dim QuoPartenza As Double
   
   'Inizio Lavoro
'   If LatoPart = -1 Then
'     QuoPartenza = val(stmp) - CORSA_I
'     Call QuotaH(XF0, Y(55) - 2, XF0, Y(55) - 2, str(QuoPartenza), , vbBlack, 4, , 4)
'
'     If (VarCorr = "LatoPA") Then
'        Call linea(XF0, Y(55), XF0, Y(56), vbRed, 2, vbDot)
'        Call QuotaH(XF0, Y(55), XF0, Y(55), "(S)", , vbRed, 4, 2, 4)
'     Else
'        Call linea(XF0, Y(55), XF0, Y(56), vbRed, 1, vbDot)
'        Call QuotaH(XF0, Y(55), XF0, Y(55), "(S)", , vbBlack, 4, , 4)
'     End If
'   Else
'     QuoPartenza = val(stmp) - CORSA_I + Ft
'     Call QuotaH(XF3, Y(55) - 2, XF3, Y(55) - 2, str(QuoPartenza), , vbBlack, 4, , 4)
'
'     If (VarCorr = "LatoPA") Then
'        Call linea(XF3, Y(55), XF3, Y(56), vbRed, 2, vbDot)
'        Call QuotaH(XF3, Y(55), XF3, Y(55), "(S)", , vbRed, 4, 2, 4)
'     Else
'        Call linea(XF3, Y(55), XF3, Y(56), vbRed, 1, vbDot)
'        Call QuotaH(XF3, Y(55), XF3, Y(55), "(S)", , vbBlack, 4, , 4)
'     End If
'   End If
     
  '

End Sub

Public Sub Disegna_CorrElica_MOLAVITE_V(Stampante As Boolean)
Dim LatoPart  As Integer
Dim nZone     As Integer
Dim X()       As Double
Dim Y()       As Double
Dim Con1F1    As Double
Dim Con2F1    As Double
Dim Con3F1    As Double
Dim Con1F2    As Double
Dim Con2F2    As Double
Dim Con3F2    As Double
Dim Bom1F1    As Double
Dim Bom2F1    As Double
Dim Bom3F1    As Double
Dim Bom1F2    As Double
Dim Bom2F2    As Double
Dim Bom3F2    As Double
Dim MaxX1     As Double
Dim MaxX2     As Double
Dim scalaxEl  As Double
Dim Pos       As Double
Dim XF0       As Double
Dim XF1       As Double
Dim XF2       As Double
Dim XF3       As Double
Dim XF0_2     As Double
Dim XF1_2     As Double
Dim XF2_2     As Double
Dim XF3_2     As Double
Dim Ft        As Double
Dim Ft2       As Double
Dim F1        As Double
Dim F2        As Double
Dim F3        As Double
Dim F1_2      As Double
Dim F2_2      As Double
Dim F3_2      As Double
Dim Re        As Double
Dim RS        As Double
Dim LS        As Double
Dim tColore   As Long
Dim tX1       As Double
Dim tX        As Double
Dim tY        As Double
Dim i         As Integer
Dim NotScale  As Boolean
Dim Lin       As Double
Dim CORSA_O   As Double
Dim CORSA_I   As Double
Dim F1T1      As Double
Dim F1T2      As Double
Dim F1T3      As Double
Dim F2T1      As Double
Dim F2T2      As Double
Dim F2T3      As Double
Dim QuoStart1 As Double
Dim QuoStart2 As Double
Dim CodTesta  As Integer
Dim nriga     As Long
Dim nstr      As Long
Dim riga      As String

'
On Error Resume Next
  '
  Call InizializzaVariabili
  '
  NotScale = False
  '
  scalaxEl = Lp("iScalaX")
  If (scalaxEl = 0) Then scalaxEl = Lp("iScalaX")
  LatoPart = Lp("LatoPA")
  CORSA_I = Lp("CORSAIN")
  CORSA_O = Lp("CORSAOUT")
  nZone = Lp("iNzone")
  
  CodTesta = Lp("INDTAV_G")
  If (CodTesta = 1) Then
    QuoStart2 = Lp("QASSIALESTART_G[1,1]")
  Else
    QuoStart1 = Lp("QASSIALESTART_G[0,1]")
  End If
  
  
  F1 = 0:   F2 = 0:   F3 = 0
  F1_2 = 0: F2_2 = 0: F3_2 = 0
  
  '
  'Disegno correzioni elica
  ReDim X(60):  ReDim Y(60)
  '
  Con1F1 = Lp("iCon1Fsx"):  Con2F1 = Lp("iCon2Fsx"):  Con3F1 = Lp("iCon3Fsx")
  Con1F2 = Lp("iCon1Fdx"):  Con2F2 = Lp("iCon2Fdx"):  Con3F2 = Lp("iCon3Fdx")
  '
  Bom1F1 = Lp("iBom1Fsx"):  Bom2F1 = Lp("iBom2Fsx"):  Bom3F1 = Lp("iBom3Fsx")
  Bom1F2 = Lp("iBom1Fdx"):  Bom2F2 = Lp("iBom2Fdx"):  Bom3F2 = Lp("iBom3Fdx")
  '
  
  Select Case nZone
    Case 1
        F1 = CORSA_I + Lp("iFas1Fsx") + CORSA_O: F1_2 = CORSA_I + Lp("iFas1Fdx") + CORSA_O
    Case 2
        
        F1T1 = Lp("iFas1Fsx"): F1T2 = Lp("iFas2Fsx")
        If (F1T1 > 0) And (F1T2 > 0) Then
           F1 = F1T1 + CORSA_I
           F2 = F1T2 + CORSA_O
        Else
          If (F1T1 > 0) And (F1T2 = 0) Then
             F1 = F1T1 + CORSA_I + CORSA_O
             F2 = F1T2
          Else
             If (F1T1 = 0) And (F1T2 > 0) Then
                F1 = F1T1
                F2 = F1T2 + CORSA_I + CORSA_O
             Else
                F1 = F1T1 + CORSA_I
                F2 = F1T2 + CORSA_O
             End If
          End If
        End If
        
        F2T1 = Lp("iFas1Fdx"): F2T2 = Lp("iFas2Fdx")
        If (F2T1 > 0) And (F2T2 > 0) Then
           F1_2 = F2T1 + CORSA_I
           F2_2 = F2T2 + CORSA_O
        Else
          If (F2T1 > 0) And (F2T2 = 0) Then
             F1_2 = F2T1 + CORSA_I + CORSA_O
             F2_2 = F2T2
          Else
             If (F2T1 = 0) And (F2T2 > 0) Then
                F1_2 = F2T1
                F2_2 = F2T2 + CORSA_I + CORSA_O
             Else
                F1_2 = F2T1 + CORSA_I
                F2_2 = F2T2 + CORSA_O
             End If
          End If
        End If
    
    Case 3
        
        F1T1 = Lp("iFas1Fsx"): F1T2 = Lp("iFas2Fsx"): F1T3 = Lp("iFas3Fsx")
        
        If (F1T1 > 0) And (F1T2 > 0) And (F1T3 > 0) Then
           F1 = F1T1 + CORSA_I
           F2 = F1T2
           F3 = F1T3 + CORSA_O
        Else
          If (F1T1 > 0) And (F1T2 > 0) And (F1T3 = 0) Then
             F1 = F1T1 + CORSA_I
             F2 = F1T2 + CORSA_O
             F3 = F1T3
          Else
            If (F1T1 > 0) And (F1T2 = 0) And (F1T3 = 0) Then
               F1 = F1T1 + CORSA_I + CORSA_O
               F2 = F1T2
               F3 = F1T3
            Else
               If (F1T1 > 0) And (F1T2 = 0) And (F1T3 > 0) Then
                  F1 = F1T1 + CORSA_I
                  F2 = F1T2
                  F3 = F1T3 + CORSA_O
               Else
                  If (F1T1 = 0) And (F1T2 > 0) And (F1T3 > 0) Then
                     F1 = F1T1
                     F2 = F1T2 + CORSA_I
                     F3 = F1T3 + CORSA_O
                  Else
                     If (F1T1 = 0) And (F1T2 = 0) And (F1T3 > 0) Then
                        F1 = F1T1
                        F2 = F1T2
                        F3 = F1T3 + CORSA_I + CORSA_O
                     Else
                        F1 = F1T1 + CORSA_I
                        F2 = F1T2
                        F3 = F1T3 + CORSA_O
                     End If
                  End If
               End If
            End If
          End If
        End If
          
        
        F2T1 = Lp("iFas1Fdx"): F2T2 = Lp("iFas2Fdx"): F2T3 = Lp("iFas3Fdx")
        
        If (F2T1 > 0) And (F2T2 > 0) And (F2T3 > 0) Then
           F1_2 = F2T1 + CORSA_I
           F2_2 = F2T2
           F3_2 = F2T3 + CORSA_O
        Else
          If (F2T1 > 0) And (F2T2 > 0) And (F2T3 = 0) Then
             F1_2 = F2T1 + CORSA_I
             F2_2 = F2T2 + CORSA_O
             F3_2 = F2T3
          Else
            If (F2T1 > 0) And (F2T2 = 0) And (F2T3 = 0) Then
               F1_2 = F2T1 + CORSA_I + CORSA_O
               F2_2 = F2T2
               F3_2 = F2T3
            Else
               If (F2T1 > 0) And (F2T2 = 0) And (F2T3 > 0) Then
                  F1_2 = F2T1 + CORSA_I
                  F2_2 = F2T2
                  F3_2 = F2T3 + CORSA_O
               Else
                  If (F2T1 = 0) And (F2T2 > 0) And (F2T3 > 0) Then
                     F1_2 = F2T1
                     F2_2 = F2T2 + CORSA_I
                     F3_2 = F2T3 + CORSA_O
                  Else
                     If (F2T1 = 0) And (F2T2 = 0) And (F2T3 > 0) Then
                        F1_2 = F2T1
                        F2_2 = F2T2
                        F3_2 = F2T3 + CORSA_I + CORSA_O
                     Else
                        F1_2 = F2T1 + CORSA_I
                        F2_2 = F2T2
                        F3_2 = F2T3 + CORSA_O
                     End If
                  End If
               End If
            End If
          End If
        End If
            
  End Select
  
  Re = 10:  RS = 10:  LS = 10
  '
  Ft = F1 + F2 + F3         'Fascia Totale F1
  Ft2 = F1_2 + F2_2 + F3_2  'Fascia Totale F2
  XF0 = LS:    XF1 = XF0 + F1:        XF2 = XF1 + F2:        XF3 = XF2 + F3
  XF0_2 = LS:  XF1_2 = XF0_2 + F1_2:  XF2_2 = XF1_2 + F2_2:  XF3_2 = XF2_2 + F3_2
  
  
  'Testa verticale OK
  Pos = RS
  X(0) = XF0
  X(1) = XF1
  X(2) = XF2
  X(3) = XF3
  X(16) = XF3
  Y(0) = -Pos
  Y(1) = -Pos - (Con1F1 * 1 + Con2F1 * 0 + Con3F1 * 0) * scalaxEl
  Y(2) = -Pos - (Con1F1 * 1 + Con2F1 * 1 + Con3F1 * 0) * scalaxEl
  Y(3) = -Pos - (Con1F1 * 1 + Con2F1 * 1 + Con3F1 * 1) * scalaxEl
  Y(16) = -Pos

  X(4) = XF0_2
  X(5) = XF1_2
  X(6) = XF2_2
  X(7) = XF3_2
  X(17) = XF3_2
  Y(4) = Pos
  Y(5) = Pos + (Con1F2 * 1 + Con2F2 * 0 + Con3F2 * 0) * scalaxEl
  Y(6) = Pos + (Con1F2 * 1 + Con2F2 * 1 + Con3F2 * 0) * scalaxEl
  Y(7) = Pos + (Con1F2 * 1 + Con2F2 * 1 + Con3F2 * 1) * scalaxEl
  Y(17) = Pos
 
  'Lineette
  X(8) = X(1)
  X(9) = X(1)
  X(12) = X(5)
  X(13) = X(5)
  X(10) = X(2)
  X(11) = X(2)
  X(14) = X(6)
  X(15) = X(6)
  Lin = 3
  Y(8) = Y(1) + Lin
  Y(9) = Y(1) - Lin
  Y(12) = Y(5) + Lin
  Y(13) = Y(5) - Lin
  Y(10) = Y(2) + Lin
  Y(11) = Y(2) - Lin
  Y(14) = Y(6) + Lin
  Y(15) = Y(6) - Lin
  
  
  Dim StepGriglia As Double
  StepGriglia = 0.05
  For i = -3 To 3
    Y(21 + i) = Y(0) + i * StepGriglia * scalaxEl
    Y(28 + i) = Y(0) + i * StepGriglia * scalaxEl
    Y(35 + i) = Y(4) + i * StepGriglia * scalaxEl
    Y(42 + i) = Y(4) + i * StepGriglia * scalaxEl
    X(21 + i) = XF0
    X(28 + i) = XF3
    X(35 + i) = XF0
    X(42 + i) = XF3
  Next i
  
  'Pos. extracorse (Testato OK)
  X(51) = XF0 + CORSA_I
  X(52) = X(51)
  X(53) = XF3 - CORSA_O
  X(54) = X(53)
  
'Linee extracorsa (Testato OK)
  Y(51) = Y(0)
  Y(52) = Y(0)
  Y(53) = Y(0)
      
'(Testato OK)
  PAR.MaxX = Y(4)
  PAR.MinX = Y(0)
  For i = 1 To 3
    If Y(i) > PAR.MaxX Then PAR.MaxX = Y(i)
    If Y(i + 4) < PAR.MinX Then PAR.MinX = Y(i + 4)
  Next i

  If Ft >= Ft2 Then
    PAR.MaxY = Ft + LS
  Else
    PAR.MaxY = Ft2 + LS
  End If
  PAR.MinY = LS

  PAR.ScalaMargine = 0.75
  PAR.Allineamento = 0
  Call CalcolaParPic(PAR, True, Stampante)
  
  'Pos. Max della fascia
  MaxX1 = X(1)
  MaxX2 = X(5)
  If Not NotScale Then
    Select Case nZone 'Disegno linee verticali che delimitano le zone
      Case 1
        'Orizzontale
        'Call QuotaH(XF0, Re, XF1, Re, "" & F1, 12, CalcoloColore("iFas1Fsx"), 4)
        'Call QuotaH(XF0_2, -Re, XF1_2, -Re, "" & F1_2, 12, CalcoloColore("iFas1Fdx"), 3)
        
        'Verticale
        Call QuotaVV(-Re, XF0, -Re, XF1, "" & F1, 12, CalcoloColore("iFas1Fsx"), 7)
        Call QuotaVV(Re, XF0_2, Re, XF1_2, "" & F1_2, 12, CalcoloColore("iFas1Fdx"), 6)
      
      Case 2
        'Orizzontale
'        Call QuotaH(XF0, Re, XF1, Re, "" & F1, 12, CalcoloColore("iFas1Fsx"), 4)
'        Call QuotaH(XF1, Re, XF2, Re, "" & F2, 12, CalcoloColore("iFas2Fsx"), 4)
'        Call QuotaH(XF0_2, -Re, XF1_2, -Re, "" & F1_2, 12, CalcoloColore("iFas1Fdx"), 3)
'        Call QuotaH(XF1_2, -Re, XF2_2, -Re, "" & F2_2, 12, CalcoloColore("iFas2Fdx"), 3)
        
        'Verticale
        Call QuotaVV(-Re, XF0, -Re, XF1, "" & F1, 12, CalcoloColore("iFas1Fsx"), 7)
        Call QuotaVV(-Re, XF1, -Re, XF2, "" & F2, 12, CalcoloColore("iFas2Fsx"), 7)
        Call QuotaVV(Re, XF0_2, Re, XF1_2, "" & F1_2, 12, CalcoloColore("iFas1Fdx"), 6)
        Call QuotaVV(Re, XF1_2, Re, XF2_2, "" & F2_2, 12, CalcoloColore("iFas2Fdx"), 6)
        
        MaxX1 = X(2)
        MaxX2 = X(6)
      Case 3
        If (VarCorr = "iFas1Fsx") Then
          'Call QuotaH(XF0, Re, XF1, Re, "" & F1, 12, CalcoloColore("iFas1Fsx"), 4, 1)
           Call QuotaVV(-Re, XF0, -Re, XF1, "" & F1, 12, CalcoloColore("iFas1Fsx"), 7, 1)
        Else
           'Call QuotaH(XF0, Re, XF1, Re, "" & F1, 12, CalcoloColore("iFas1Fsx"), 4)
           Call QuotaVV(-Re, XF0, -Re, XF1, "" & F1, 12, CalcoloColore("iFas1Fsx"), 7)
        End If
        If (VarCorr = "iFas2Fsx") Then
            'Call QuotaH(XF1, Re, XF2, Re, "" & F2, 12, CalcoloColore("iFas2Fsx"), 4, 1)
            Call QuotaVV(-Re, XF1, -Re, XF2, "" & F2, 12, CalcoloColore("iFas2Fsx"), 7, 1)
        Else
            'Call QuotaH(XF1, Re, XF2, Re, "" & F2, 12, CalcoloColore("iFas2Fsx"), 4)
            Call QuotaVV(-Re, XF1, -Re, XF2, "" & F2, 12, CalcoloColore("iFas2Fsx"), 7)
        End If
        If (VarCorr = "iFas3Fsx") Then
           'Call QuotaH(XF2, Re, XF3, Re, "" & F3, 12, CalcoloColore("iFas3Fsx"), 4, 1)
            Call QuotaVV(-Re, XF2, -Re, XF3, "" & F3, 12, CalcoloColore("iFas3Fsx"), 7, 1)
        Else
           'Call QuotaH(XF2, Re, XF3, Re, "" & F3, 12, CalcoloColore("iFas3Fsx"), 4)
            Call QuotaVV(-Re, XF2, -Re, XF3, "" & F3, 12, CalcoloColore("iFas3Fsx"), 7)
        End If
        
        'Call QuotaH(XF0_2, -Re, XF1_2, -Re, "" & F1_2, 12, CalcoloColore("iFas1Fdx"), 3)
        'Call QuotaH(XF1_2, -Re, XF2_2, -Re, "" & F2_2, 12, CalcoloColore("iFas2Fdx"), 3)
        'Call QuotaH(XF2_2, -Re, XF3_2, -Re, "" & F3_2, 12, CalcoloColore("iFas3Fdx"), 3)
        
        Call QuotaVV(Re, XF0_2, Re, XF1_2, "" & F1_2, 12, CalcoloColore("iFas1Fdx"), 6)
        Call QuotaVV(Re, XF1_2, Re, XF2_2, "" & F2_2, 12, CalcoloColore("iFas2Fdx"), 6)
        Call QuotaVV(Re, XF2_2, Re, XF3_2, "" & F3_2, 12, CalcoloColore("iFas3Fdx"), 6)
        
        MaxX1 = X(3)
        MaxX2 = X(7)
    End Select
  End If
  ' Asse pezzo
  'Orizzontale
  'Call linea(LS, 0, LS + Ft, 0, RGB(180, 180, 180), 1, vbDashDot)
  'Verticale
  Call lineaV(0, LS, 0, LS + Ft, RGB(180, 180, 180), 1, vbDashDot)
  
  '????
  'Y(51) = PAR.MaxY + 8
  'New
  'Y(51) = PAR.MaxY + 8
  
  Dim tidC    As Long
  
  If Not NotScale Then
    'Verticale OK
    tidC = CalcoloColore("iCORSAIN", , , tColore)
    Call QuotaVV(0, LS, 0, X(51), "" & CORSA_I, 12, tidC, 7)
    tidC = CalcoloColore("FASCIATOTALE_G", , , tColore)
    Call QuotaVV(0, X(51), 0, X(53), "" & (Ft - CORSA_I - CORSA_O), 12, tidC, 7)
    tidC = CalcoloColore("iCORSAOUT", , , tColore)
    Call QuotaVV(0, X(53), 0, LS + Ft, "" & CORSA_O, 12, tidC, 7)
  End If
  
  Dim tidP1   As Integer
  Dim tidP2   As Integer
  Dim tidst   As Integer
  Dim tisp    As Integer
  tColore = vbBlue
  tidst = vbSolid
  
  'Linee (griglia) A cosa servono?
  For i = -3 To 3
    tidP1 = 21 + i
    tidP2 = 28 + i
    'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), RGB(200, 200, 200), 1, vbDot)
    tidP1 = 35 + i
    tidP2 = 42 + i
    'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), RGB(200, 200, 200), 1, vbDot)
  Next i
    
  'Fianco1 Fascia1
  tidP1 = 0
  tidP2 = 1
  
  If (VarCorr = "iFas1Fsx") Or (VarCorr = "iCon1Fsx") Then
     tidC = vbRed
     If (VarCorr = "iFas1Fsx") Then
        If Con1F1 <> 0 Then
           'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), vbBlue)
        Else
           'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), tidC)
        End If
     End If
     If (VarCorr = "iCon1Fsx") Then
        If Con1F1 <> 0 Then
           'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 2)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), tidC, 2)
        Else
           'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), vbBlack)
        End If
     End If
     tidC = vbBlue
  Else
     tidC = CalcoloColore("iBom1Fsx", , , tColore)
     'Call linea(X(tidP1), Abs(Y(tidP1)), X(tidP2), Abs(Y(tidP2)), vbBlue)
     Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), vbBlue)
  
  End If
  tisp = IIf(tidC = vbRed, 2, 1)
  If Bom1F1 <> 0 Then
    'Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), Bom1F1, scalaxEl, tidC, tisp)
    Call Disegna_BombaturaElica_V(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), Bom1F1, scalaxEl, tidC, tisp)
  End If
  
  'Fianco1 Fascia2
  tidP1 = 1
  tidP2 = 2
  If VarCorr = "iFas2Fsx" Or (VarCorr = "iCon2Fsx") Then
     tidC = vbRed
     If (VarCorr = "iFas2Fsx") Then
        If Con2F1 <> 0 Then
           'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), vbBlue)
        Else
           'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), tidC)
        End If
     End If
     If (VarCorr = "iCon2Fsx") Then
        If Con2F1 <> 0 Then
           'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 2)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), tidC, 2)
        Else
           'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), vbBlack)
        End If
     End If
     tidC = vbBlue
  Else
     tidC = CalcoloColore("iBom2Fsx", , , tColore)
     'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
     Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), vbBlue)
  End If
  tisp = IIf(tidC = vbRed, 2, 1)
  If Bom2F1 <> 0 Then
    'Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), Bom2F1, scalaxEl, tidC, tisp)
    Call Disegna_BombaturaElica_V(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), Bom2F1, scalaxEl, tidC, tisp)
  End If
  
  'Fianco1 Fascia3
  tidP1 = 2
  tidP2 = 3
  If VarCorr = "iFas3Fsx" Or (VarCorr = "iCon3Fsx") Then
     tidC = vbRed
     If (VarCorr = "iFas3Fsx") Then
        If Con3F1 <> 0 Then
           'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), vbBlue)
        Else
           'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), tidC)
        End If
     End If
     If (VarCorr = "iCon3Fsx") Then
        If Con3F1 <> 0 Then
           'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 2)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), tidC, 2)
        Else
           'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), vbBlack)
        End If
     End If
     tidC = vbBlue
  Else
     tidC = CalcoloColore("iBom3Fsx", , , tColore)
     'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
     Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), vbBlue)
  End If
  tisp = IIf(tidC = vbRed, 2, 1)
  If Bom3F1 <> 0 Then
    'Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), Bom3F1, scalaxEl, tidC, tisp)
    Call Disegna_BombaturaElica_V(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), Bom3F1, scalaxEl, tidC, tisp)
  End If
  
  'Fianco 1 Separazione Zone 1 e 2
  tidC = tColore
  tisp = 1
  tidP1 = 8
  tidP2 = 9
  'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, tidst)
  Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), tidC, 1, tidst)
    
  'Fianco 1 Separazione Zone 2 e 3
  tidP1 = 10
  tidP2 = 11
  'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, tidst)
  Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), tidC, 1, tidst)
  
  'Fianco 1 Linea tra primo e ultimo punto
  tidP1 = 0
  tidP2 = 16
  'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, vbDot)
  
  'Fianco2 Fascia1
  tidP1 = 4
  tidP2 = 5
  If (VarCorr = "iFas1Fdx") Or (VarCorr = "iCon1Fdx") Then
     tidC = vbRed
     If (VarCorr = "iFas1Fdx") Then
        If Con1F2 <> 0 Then
           'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), vbBlue)
        Else
           'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), tidC)
        End If
     End If
     If (VarCorr = "iCon1Fdx") Then
        If Con1F2 <> 0 Then
           'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 2)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), tidC, 2)
        Else
           'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), vbBlack)
        End If
     End If
     tidC = vbBlue
  Else
     tidC = CalcoloColore("iBom1Fdx", , , tColore)
     'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
     Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), vbBlue)
  End If
  tisp = IIf(tidC = vbRed, 2, 1)
  If Bom1F2 <> 0 Then
    'Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), -Bom1F2, scalaxEl, tidC, tisp)
    Call Disegna_BombaturaElica_V(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), -Bom1F2, scalaxEl, tidC, tisp)
  End If
  
  'Fianco2 Fascia2
  tidP1 = 5
  tidP2 = 6
  If (VarCorr = "iFas2Fdx") Or (VarCorr = "iCon2Fdx") Then
     tidC = vbRed
     If (VarCorr = "iFas2Fdx") Then
        If Con2F2 <> 0 Then
           'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), vbBlue)
        Else
           'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), tidC)
        End If
     End If
     If (VarCorr = "iCon2Fdx") Then
        If Con2F2 <> 0 Then
           'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 2)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), tidC, 2)
        Else
           'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), vbBlack)
        End If
     End If
     tidC = vbBlue
  Else
     tidC = CalcoloColore("iBom2Fdx", , , tColore)
     'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
     Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), vbBlue)
  End If
  tisp = IIf(tidC = vbRed, 2, 1)
  If Bom2F2 <> 0 Then
    'Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), -Bom2F2, scalaxEl, tidC, tisp)
    Call Disegna_BombaturaElica_V(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), -Bom2F2, scalaxEl, tidC, tisp)
  End If
  
  'Fianco2 Fascia3
  tidP1 = 6
  tidP2 = 7
  If (VarCorr = "iFas3Fdx") Or (VarCorr = "iCon3Fdx") Then
     tidC = vbRed
     If (VarCorr = "iFas3Fdx") Then
        If Con3F2 <> 0 Then
           'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), vbBlue)
        Else
           'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), tidC)
        End If
     End If
     If (VarCorr = "iCon3Fdx") Then
        If Con3F2 <> 0 Then
           'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 2)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), tidC, 2)
        Else
           'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack)
           Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), vbBlack)
        End If
     End If
     tidC = vbBlue
  Else
     tidC = CalcoloColore("iBom3Fdx", , , tColore)
     'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
     Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), vbBlue)
  End If
  tisp = IIf(tidC = vbRed, 2, 1)
  If Bom3F2 <> 0 Then
    'Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), -Bom3F2, scalaxEl, tidC, tisp)
    Call Disegna_BombaturaElica_V(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), -Bom3F2, scalaxEl, tidC, tisp)
  End If
  tidC = tColore
  tisp = 1
  
  'Fianco 2 Separazione Zone 1 e 2
  tidP1 = 12
  tidP2 = 13
  'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, tidst)
  Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), tidC, 1, tidst)
  
  'Fianco 2 Separazione Zone 2 e 3
  tidP1 = 14
  tidP2 = 15
  'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, tidst)
  Call lineaV(Y(tidP1), X(tidP1), Y(tidP2), X(tidP2), tidC, 1, tidst)
  
  'Fianco 1 Linea tra primo e ultimo punto
  tidP1 = 4
  tidP2 = 17
  
  'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, vbDot)
  
  'Scritte Fsx e Fdx
  Call TestoAll_V(-RS, LS, "FL", , True, 4)
  Call TestoAll_V(RS, LS, "FR", , True, 4)
  
  If NotScale Then Call TestoInfo("DRAWING NOT IN SCALE ! ", , True, vbRed, True)
  nriga = 1530
  nstr = LoadString(g_hLanguageLibHandle, nriga, riga, 255)
  If nstr = 0 Then
     riga = "Down"
  End If
  Call TestoInfo(riga, 4)
  nriga = 1531
  nstr = LoadString(g_hLanguageLibHandle, nriga, riga, 255)
  If nstr = 0 Then
     riga = "Up"
  End If
  Call TestoInfo(riga, 1)
  '
  'Call TestoInfo("Corr. Scale: " & scalaxEl, 4)
  If LINGUA = "GR" Then
  Call TestoInfo("Spannseite", 3)
  Call TestoInfo("Units: [mm]", 2)
  Else
  Call TestoInfo("Clamping side", 3)
  Call TestoInfo("Units: [mm]", 2)
  End If
  
  If Ft2 > Ft Then
    Call TestoInfo("WARNING: (FT FL = " & Ft & ") < (FT FR = " & Ft2 & ")", , True, vbRed, True)
    Call QuotaH(MaxX1, Pos, MaxX2, Pos, "?", , vbRed, 3)
  ElseIf Ft2 < Ft Then
    Call TestoInfo("WARNING: (FT FL = " & Ft & ") > (FT FR = " & Ft2 & ")", , True, vbRed, True)
    Call QuotaH(MaxX2, -Pos, MaxX1, -Pos, "?", , vbRed, 3)
  End If
  
  'Inizio Pezzo
    X(55) = X(51)
    Y(55) = 0
    X(56) = X(51)
    Y(56) = 0 + 2
   
   Dim stmp As String
   
    If (CodTesta = 1) Then
       stmp = str(frmt(QuoStart2, 2))
    Else
       stmp = str(frmt(QuoStart1, 2))
    End If


   If (VarCorr = "QASSIALESTART_G[0,1]") Or (VarCorr = "QASSIALESTART_G[1,1]") Then
      'Call linea(X(55), Y(55), X(56), Y(56), vbRed, 2, vbDot)
      'Call QuotaH(X(55), Y(55), X(55), Y(55), "(Z)", , vbRed, 4, 2, 4)
      Call lineaV(Y(55), X(55), Y(56), X(56), vbRed, 2, vbDot)
      Call QuotaVV(Y(56), X(56), Y(56), X(56), "(Z)", , vbRed, 7, 2, 4)
      
      If (VarCorr = "QASSIALESTART_G[0,1]") Then
          QuoStart1 = Lp("QASSIALESTART_G[0,1]")
          stmp = str(frmt(QuoStart1, 2))
          'Call QuotaH(X(55), Y(55) - 2, X(55), Y(55) - 2, stmp, , vbBlack, 4, 2, 4)
          Call QuotaVV(Y(55) - 3, X(55), Y(55) - 3, X(55), stmp, , vbBlack, 6, 2, 4)
      Else
          QuoStart2 = Lp("QASSIALESTART_G[1,1]")
          stmp = str(frmt(QuoStart2, 2))
          'Call QuotaH(X(55), Y(55) - 2, X(55), Y(55) - 2, stmp, , vbBlack, 4, 2, 4)
          Call QuotaVV(Y(55) - 3, X(55), Y(55) - 3, X(55), stmp, , vbBlack, 6, 2, 4)
      End If
   Else
      'Call linea(X(55), Y(55), X(56), Y(56), vbRed, 1, vbDot)
      'Call QuotaH(X(55), Y(55), X(55), Y(55), "(Z)", , vbBlack, 4, , 4)
      'Call QuotaH(X(55), Y(55) - 2, X(55), Y(55) - 2, stmp, , vbBlack, 4, 2, 4)
      Call lineaV(Y(55), X(55), Y(56), X(56), vbRed, 1, vbDot)
      Call QuotaVV(Y(56), X(56), Y(56), X(56), "(Z)", , vbBlack, 7, 2, 4)
      Call QuotaVV(Y(55) - 3, X(55), Y(55) - 3, X(55), stmp, , vbBlack, 6, 2, 4)
   
   End If
   
   Dim QuoPartenza As Double
   
   'Inizio Lavoro
   If LatoPart = -1 Then
     QuoPartenza = val(stmp) - CORSA_I
     'Call QuotaH(XF0, Y(55) - 2, XF0, Y(55) - 2, str(QuoPartenza), , vbBlack, 4, , 4)
     Call QuotaVV(Y(55) - 3, XF0, Y(55) - 3, XF0, str(QuoPartenza), , vbBlack, 6, , 4)
     
     If (VarCorr = "LatoPA") Then
'        Call linea(XF0, Y(55), XF0, Y(56), vbRed, 2, vbDot)
'        Call QuotaH(XF0, Y(55), XF0, Y(55), "(S)", , vbRed, 4, 2, 4)
        Call lineaV(Y(55), XF0, Y(56), XF0, vbRed, 2, vbDot)
        Call QuotaVV(Y(56), XF0, Y(56), XF0, "(S)", , vbRed, 7, 2, 4)
     Else
'        Call linea(XF0, Y(55), XF0, Y(56), vbRed, 1, vbDot)
'        Call QuotaH(XF0, Y(55), XF0, Y(55), "(S)", , vbBlack, 4, , 4)
        Call lineaV(Y(55), XF0, Y(56), XF0, vbRed, 1, vbDot)
        Call QuotaVV(Y(56), XF0, Y(56), XF0, "(S)", , vbBlack, 7, , 4)
     End If
   Else
     QuoPartenza = val(stmp) - CORSA_I + Ft
     'Call QuotaH(XF3, Y(55) - 2, XF3, Y(55) - 2, str(QuoPartenza), , vbBlack, 4, , 4)
     Call QuotaVV(Y(55) - 3, XF3, Y(55) - 3, XF3, str(QuoPartenza), , vbBlack, 6, , 4)
     
     If (VarCorr = "LatoPA") Then
        'Call linea(XF3, Y(55), XF3, Y(56), vbRed, 2, vbDot)
        'Call QuotaH(XF3, Y(55), XF3, Y(55), "(S)", , vbRed, 4, 2, 4)
        Call lineaV(Y(55), XF3, Y(56), XF3, vbRed, 2, vbDot)
        Call QuotaVV(Y(56), XF3, Y(56), XF3, "(S)", , vbRed, 7, 2, 4)
     Else
'        Call linea(XF3, Y(55), XF3, Y(56), vbRed, 1, vbDot)
'        Call QuotaH(XF3, Y(55), XF3, Y(55), "(S)", , vbBlack, 4, , 4)
        Call lineaV(Y(55), XF3, Y(56), XF3, vbRed, 1, vbDot)
        Call QuotaVV(Y(56), XF3, Y(56), XF3, "(S)", , vbBlack, 7, , 4)
     End If
   End If
     
'
End Sub


Public Sub Disegna_CorrElica_MOLAVITE(Stampante As Boolean)
'
Dim LatoPart  As Integer
Dim nZone     As Integer
Dim X()       As Double
Dim Y()       As Double
Dim Con1F1    As Double
Dim Con2F1    As Double
Dim Con3F1    As Double
Dim Con1F2    As Double
Dim Con2F2    As Double
Dim Con3F2    As Double
Dim Bom1F1    As Double
Dim Bom2F1    As Double
Dim Bom3F1    As Double
Dim Bom1F2    As Double
Dim Bom2F2    As Double
Dim Bom3F2    As Double
Dim MaxX1     As Double
Dim MaxX2     As Double
Dim scalaxEl  As Double
Dim Pos       As Double
Dim XF0       As Double
Dim XF1       As Double
Dim XF2       As Double
Dim XF3       As Double
Dim XF0_2     As Double
Dim XF1_2     As Double
Dim XF2_2     As Double
Dim XF3_2     As Double
Dim Ft        As Double
Dim Ft2       As Double
Dim F1        As Double
Dim F2        As Double
Dim F3        As Double
Dim F1_2      As Double
Dim F2_2      As Double
Dim F3_2      As Double
Dim Re        As Double
Dim RS        As Double
Dim LS        As Double
Dim tColore   As Long
Dim tX1       As Double
Dim tX        As Double
Dim tY        As Double
Dim i         As Integer
Dim NotScale  As Boolean
Dim Lin       As Double
Dim CORSA_O   As Double
Dim CORSA_I   As Double
Dim F1T1      As Double
Dim F1T2      As Double
Dim F1T3      As Double
Dim F2T1      As Double
Dim F2T2      As Double
Dim F2T3      As Double
Dim QuoStart1 As Double
Dim QuoStart2 As Double
Dim CodTesta  As Integer
Dim nriga     As Long
Dim nstr      As Long
Dim riga      As String

'
On Error Resume Next
  '
  Call InizializzaVariabili
  '
  NotScale = False
  '
  scalaxEl = Lp("iScalaX")
  If (scalaxEl = 0) Then scalaxEl = Lp("iScalaX")
  LatoPart = Lp("LatoPA")
  CORSA_I = Lp("CORSAIN")
  CORSA_O = Lp("CORSAOUT")
  nZone = Lp("iNzone")
  
  CodTesta = Lp("INDTAV_G")
  If (CodTesta = 1) Then
    QuoStart2 = Lp("QASSIALESTART_G[1,1]")
  Else
    QuoStart1 = Lp("QASSIALESTART_G[0,1]")
  End If
  
  
  F1 = 0:   F2 = 0:   F3 = 0
  F1_2 = 0: F2_2 = 0: F3_2 = 0
  
  '
  'Disegno correzioni elica
  ReDim X(60):  ReDim Y(60)
  '
  Con1F1 = Lp("iCon1Fsx"):  Con2F1 = Lp("iCon2Fsx"):  Con3F1 = Lp("iCon3Fsx")
  Con1F2 = Lp("iCon1Fdx"):  Con2F2 = Lp("iCon2Fdx"):  Con3F2 = Lp("iCon3Fdx")
  '
  Bom1F1 = Lp("iBom1Fsx"):  Bom2F1 = Lp("iBom2Fsx"):  Bom3F1 = Lp("iBom3Fsx")
  Bom1F2 = Lp("iBom1Fdx"):  Bom2F2 = Lp("iBom2Fdx"):  Bom3F2 = Lp("iBom3Fdx")
  '
  
  Select Case nZone
    Case 1
        F1 = CORSA_I + Lp("iFas1Fsx") + CORSA_O: F1_2 = CORSA_I + Lp("iFas1Fdx") + CORSA_O
    Case 2
        
        F1T1 = Lp("iFas1Fsx"): F1T2 = Lp("iFas2Fsx")
        If (F1T1 > 0) And (F1T2 > 0) Then
           F1 = F1T1 + CORSA_I
           F2 = F1T2 + CORSA_O
        Else
          If (F1T1 > 0) And (F1T2 = 0) Then
             F1 = F1T1 + CORSA_I + CORSA_O
             F2 = F1T2
          Else
             If (F1T1 = 0) And (F1T2 > 0) Then
                F1 = F1T1
                F2 = F1T2 + CORSA_I + CORSA_O
             Else
                F1 = F1T1 + CORSA_I
                F2 = F1T2 + CORSA_O
             End If
          End If
        End If
        
        F2T1 = Lp("iFas1Fdx"): F2T2 = Lp("iFas2Fdx")
        If (F2T1 > 0) And (F2T2 > 0) Then
           F1_2 = F2T1 + CORSA_I
           F2_2 = F2T2 + CORSA_O
        Else
          If (F2T1 > 0) And (F2T2 = 0) Then
             F1_2 = F2T1 + CORSA_I + CORSA_O
             F2_2 = F2T2
          Else
             If (F2T1 = 0) And (F2T2 > 0) Then
                F1_2 = F2T1
                F2_2 = F2T2 + CORSA_I + CORSA_O
             Else
                F1_2 = F2T1 + CORSA_I
                F2_2 = F2T2 + CORSA_O
             End If
          End If
        End If
    
    Case 3
        
        F1T1 = Lp("iFas1Fsx"): F1T2 = Lp("iFas2Fsx"): F1T3 = Lp("iFas3Fsx")
        
        If (F1T1 > 0) And (F1T2 > 0) And (F1T3 > 0) Then
           F1 = F1T1 + CORSA_I
           F2 = F1T2
           F3 = F1T3 + CORSA_O
        Else
          If (F1T1 > 0) And (F1T2 > 0) And (F1T3 = 0) Then
             F1 = F1T1 + CORSA_I
             F2 = F1T2 + CORSA_O
             F3 = F1T3
          Else
            If (F1T1 > 0) And (F1T2 = 0) And (F1T3 = 0) Then
               F1 = F1T1 + CORSA_I + CORSA_O
               F2 = F1T2
               F3 = F1T3
            Else
               If (F1T1 > 0) And (F1T2 = 0) And (F1T3 > 0) Then
                  F1 = F1T1 + CORSA_I
                  F2 = F1T2
                  F3 = F1T3 + CORSA_O
               Else
                  If (F1T1 = 0) And (F1T2 > 0) And (F1T3 > 0) Then
                     F1 = F1T1
                     F2 = F1T2 + CORSA_I
                     F3 = F1T3 + CORSA_O
                  Else
                     If (F1T1 = 0) And (F1T2 = 0) And (F1T3 > 0) Then
                        F1 = F1T1
                        F2 = F1T2
                        F3 = F1T3 + CORSA_I + CORSA_O
                     Else
                        F1 = F1T1 + CORSA_I
                        F2 = F1T2
                        F3 = F1T3 + CORSA_O
                     End If
                  End If
               End If
            End If
          End If
        End If
          
        
        F2T1 = Lp("iFas1Fdx"): F2T2 = Lp("iFas2Fdx"): F2T3 = Lp("iFas3Fdx")
        
        If (F2T1 > 0) And (F2T2 > 0) And (F2T3 > 0) Then
           F1_2 = F2T1 + CORSA_I
           F2_2 = F2T2
           F3_2 = F2T3 + CORSA_O
        Else
          If (F2T1 > 0) And (F2T2 > 0) And (F2T3 = 0) Then
             F1_2 = F2T1 + CORSA_I
             F2_2 = F2T2 + CORSA_O
             F3_2 = F2T3
          Else
            If (F2T1 > 0) And (F2T2 = 0) And (F2T3 = 0) Then
               F1_2 = F2T1 + CORSA_I + CORSA_O
               F2_2 = F2T2
               F3_2 = F2T3
            Else
               If (F2T1 > 0) And (F2T2 = 0) And (F2T3 > 0) Then
                  F1_2 = F2T1 + CORSA_I
                  F2_2 = F2T2
                  F3_2 = F2T3 + CORSA_O
               Else
                  If (F2T1 = 0) And (F2T2 > 0) And (F2T3 > 0) Then
                     F1_2 = F2T1
                     F2_2 = F2T2 + CORSA_I
                     F3_2 = F2T3 + CORSA_O
                  Else
                     If (F2T1 = 0) And (F2T2 = 0) And (F2T3 > 0) Then
                        F1_2 = F2T1
                        F2_2 = F2T2
                        F3_2 = F2T3 + CORSA_I + CORSA_O
                     Else
                        F1_2 = F2T1 + CORSA_I
                        F2_2 = F2T2
                        F3_2 = F2T3 + CORSA_O
                     End If
                  End If
               End If
            End If
          End If
        End If
            
  End Select
  
  Re = 10:  RS = 10:  LS = 10
  '
  Ft = F1 + F2 + F3         'Fascia Totale F1
  Ft2 = F1_2 + F2_2 + F3_2  'Fascia Totale F2
  XF0 = LS:    XF1 = XF0 + F1:        XF2 = XF1 + F2:        XF3 = XF2 + F3
  XF0_2 = LS:  XF1_2 = XF0_2 + F1_2:  XF2_2 = XF1_2 + F2_2:  XF3_2 = XF2_2 + F3_2
  
  
  '
  Pos = RS
  X(0) = XF0
  X(1) = XF1
  X(2) = XF2
  X(3) = XF3
  X(16) = XF3
  Y(0) = Pos
  Y(1) = Pos + (Con1F1 * 1 + Con2F1 * 0 + Con3F1 * 0) * scalaxEl
  Y(2) = Pos + (Con1F1 * 1 + Con2F1 * 1 + Con3F1 * 0) * scalaxEl
  Y(3) = Pos + (Con1F1 * 1 + Con2F1 * 1 + Con3F1 * 1) * scalaxEl
  Y(16) = Pos
  X(4) = XF0_2
  X(5) = XF1_2
  X(6) = XF2_2
  X(7) = XF3_2
  X(17) = XF3_2
  Y(4) = -Pos
  Y(5) = -Pos - (Con1F2 * 1 + Con2F2 * 0 + Con3F2 * 0) * scalaxEl
  Y(6) = -Pos - (Con1F2 * 1 + Con2F2 * 1 + Con3F2 * 0) * scalaxEl
  Y(7) = -Pos - (Con1F2 * 1 + Con2F2 * 1 + Con3F2 * 1) * scalaxEl
  Y(17) = -Pos
  
  'Lineette
  X(8) = X(1)
  X(9) = X(1)
  X(12) = X(5)
  X(13) = X(5)
  X(10) = X(2)
  X(11) = X(2)
  X(14) = X(6)
  X(15) = X(6)
  Lin = 3
  Y(8) = Y(1) + Lin
  Y(9) = Y(1) - Lin
  Y(12) = Y(5) + Lin
  Y(13) = Y(5) - Lin
  Y(10) = Y(2) + Lin
  Y(11) = Y(2) - Lin
  Y(14) = Y(6) + Lin
  Y(15) = Y(6) - Lin
  
  
  Dim StepGriglia As Double
  StepGriglia = 0.05
  For i = -3 To 3
    Y(21 + i) = Y(0) + i * StepGriglia * scalaxEl
    Y(28 + i) = Y(0) + i * StepGriglia * scalaxEl
    Y(35 + i) = Y(4) + i * StepGriglia * scalaxEl
    Y(42 + i) = Y(4) + i * StepGriglia * scalaxEl
    X(21 + i) = XF0
    X(28 + i) = XF3
    X(35 + i) = XF0
    X(42 + i) = XF3
  Next i
  
  'Pos. extracorse
  X(51) = XF0 + CORSA_I
  X(52) = X(51)
  X(53) = XF3 - CORSA_O
  X(54) = X(53)
  
  'Linee extracorsa
  'Y(51) = Y(0) + (Con1F1 * CORSA_I / (Lp("iFas1F1") + CORSA_I))
  Y(51) = Y(0)
  Y(52) = Y(0)
  Y(53) = Y(0)
  
  PAR.MaxY = Y(0)
  PAR.MinY = Y(4)
  For i = 1 To 3
    If Y(i) > PAR.MaxY Then PAR.MaxY = Y(i)
    If Y(i + 4) < PAR.MinY Then PAR.MinY = Y(i + 4)
  Next i
  If Ft >= Ft2 Then
    PAR.MaxX = Ft + LS
  Else
    PAR.MaxX = Ft2 + LS
  End If
  PAR.MinX = LS / 2
  'par.MaxY = Re / 2
  'par.MinY = -Re / 2
  PAR.ScalaMargine = 0.9
  PAR.Allineamento = 0
  Call CalcolaParPic(PAR, True, Stampante)
  'par.Pic.Cls
  'linea XMola, Re + 20 + DiaMola, XMola, Re, vbRed, 1, vbDash
  MaxX1 = X(1)
  MaxX2 = X(5)
  If Not NotScale Then
    Select Case nZone 'Disegno linee verticali che delimitano le zone
      Case 1
        'Call linea(XF1, 0, XF1, Re, RGB(180, 180, 180), 1, vbDash)
        Call QuotaH(XF0, Re, XF1, Re, "" & F1, 12, CalcoloColore("iFas1Fsx"), 4)
        'Call linea(XF1_2, -Re, XF1_2, 0, RGB(180, 180, 180), 1, vbDash)
        Call QuotaH(XF0_2, -Re, XF1_2, -Re, "" & F1_2, 12, CalcoloColore("iFas1Fdx"), 3)
      
      Case 2
        'Call linea(XF1, 0, XF1, Re, RGB(180, 180, 180), 1, vbDash)
        Call QuotaH(XF0, Re, XF1, Re, "" & F1, 12, CalcoloColore("iFas1Fsx"), 4)
        Call QuotaH(XF1, Re, XF2, Re, "" & F2, 12, CalcoloColore("iFas2Fsx"), 4)
        'Call linea(XF1_2, -Re, XF1_2, 0, RGB(180, 180, 180), 1, vbDash)
        Call QuotaH(XF0_2, -Re, XF1_2, -Re, "" & F1_2, 12, CalcoloColore("iFas1Fdx"), 3)
        Call QuotaH(XF1_2, -Re, XF2_2, -Re, "" & F2_2, 12, CalcoloColore("iFas2Fdx"), 3)
        MaxX1 = X(2)
        MaxX2 = X(6)
      Case 3
        'Call linea(XF1, 0, XF1, Re, RGB(180, 180, 180), 1, vbDash)
        'Call linea(XF2, 0, XF2, Re, RGB(180, 180, 180), 1, vbDash)
        If (VarCorr = "iFas1Fsx") Then
           Call QuotaH(XF0, Re, XF1, Re, "" & F1, 12, CalcoloColore("iFas1Fsx"), 4, 1)
        Else
           Call QuotaH(XF0, Re, XF1, Re, "" & F1, 12, CalcoloColore("iFas1Fsx"), 4)
        End If
        If (VarCorr = "iFas2Fsx") Then
            Call QuotaH(XF1, Re, XF2, Re, "" & F2, 12, CalcoloColore("iFas2Fsx"), 4, 1)
        Else
            Call QuotaH(XF1, Re, XF2, Re, "" & F2, 12, CalcoloColore("iFas2Fsx"), 4)
        End If
        If (VarCorr = "iFas3Fsx") Then
           Call QuotaH(XF2, Re, XF3, Re, "" & F3, 12, CalcoloColore("iFas3Fsx"), 4, 1)
        Else
           Call QuotaH(XF2, Re, XF3, Re, "" & F3, 12, CalcoloColore("iFas3Fsx"), 4)
        End If
        'Call linea(XF1_2, -Re, XF1_2, 0, RGB(180, 180, 180), 1, vbDash)
        'Call linea(XF2_2, -Re, XF2_2, 0, RGB(180, 180, 180), 1, vbDash)
        Call QuotaH(XF0_2, -Re, XF1_2, -Re, "" & F1_2, 12, CalcoloColore("iFas1Fdx"), 3)
        Call QuotaH(XF1_2, -Re, XF2_2, -Re, "" & F2_2, 12, CalcoloColore("iFas2Fdx"), 3)
        Call QuotaH(XF2_2, -Re, XF3_2, -Re, "" & F3_2, 12, CalcoloColore("iFas3Fdx"), 3)
        MaxX1 = X(3)
        MaxX2 = X(7)
    End Select
  End If
  ' Asse pezzo
  Call linea(LS, 0, LS + Ft, 0, RGB(180, 180, 180), 1, vbDashDot)
  
  Y(51) = PAR.MaxY + 8
  
  Dim tidC    As Long
  
  If Not NotScale Then
    'Call QuotaH(LS, PAR.MaxY + 8, LS + Ft, PAR.MaxY + 8, "" & Ft, 12, , 3)
    
    tidC = CalcoloColore("iCORSAIN", , , tColore)
    Call QuotaH(LS, 0, X(51), 0, "" & CORSA_I, 12, tidC, 3)
    
    
    tidC = CalcoloColore("FASCIATOTALE_G", , , tColore)
    Call QuotaH(X(51), 0, X(53), 0, "" & (Ft - CORSA_I - CORSA_O), 12, tidC, 3)
    
    'Call QuotaH(X(51), 0, X(51), 0, "" & (Ft), 12, , 4)
    
    tidC = CalcoloColore("iCORSAOUT", , , tColore)
    Call QuotaH(X(53), 0, LS + Ft, 0, "" & CORSA_O, 12, tidC, 3)
  
  End If
  
  Dim tidP1   As Integer
  Dim tidP2   As Integer
  Dim tidst   As Integer
  Dim tisp    As Integer
  tColore = vbBlue
  tidst = vbSolid
  
  'Linee (griglia) A cosa servono?
  For i = -3 To 3
    tidP1 = 21 + i
    tidP2 = 28 + i
    'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), RGB(200, 200, 200), 1, vbDot)
    tidP1 = 35 + i
    tidP2 = 42 + i
    'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), RGB(200, 200, 200), 1, vbDot)
  Next i
    
  'Fianco1 Fascia1
  tidP1 = 0
  tidP2 = 1
  If (VarCorr = "iFas1Fsx") Or (VarCorr = "iCon1Fsx") Then
     tidC = vbRed
     If (VarCorr = "iFas1Fsx") Then
        If Con1F1 <> 0 Then
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
        Else
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC)
        End If
     End If
     If (VarCorr = "iCon1Fsx") Then
        If Con1F1 <> 0 Then
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 2)
        Else
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack)
        End If
     End If
     tidC = vbBlue
  Else
     tidC = CalcoloColore("iBom1Fsx", , , tColore)
     Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
  End If
  tisp = IIf(tidC = vbRed, 2, 1)
  If Bom1F1 <> 0 Then
    Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), Bom1F1, scalaxEl, tidC, tisp)
  End If
  
  'Fianco1 Fascia2
  tidP1 = 1
  tidP2 = 2
  If VarCorr = "iFas2Fsx" Or (VarCorr = "iCon2Fsx") Then
     tidC = vbRed
     If (VarCorr = "iFas2Fsx") Then
        If Con2F1 <> 0 Then
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
        Else
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC)
        End If
     End If
     If (VarCorr = "iCon2Fsx") Then
        If Con2F1 <> 0 Then
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 2)
        Else
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack)
        End If
     End If
     tidC = vbBlue
  Else
     tidC = CalcoloColore("iBom2Fsx", , , tColore)
     Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
  End If
  tisp = IIf(tidC = vbRed, 2, 1)
  If Bom2F1 <> 0 Then
    Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), Bom2F1, scalaxEl, tidC, tisp)
  End If
  
  'Fianco1 Fascia3
  tidP1 = 2
  tidP2 = 3
  If VarCorr = "iFas3Fsx" Or (VarCorr = "iCon3Fsx") Then
     tidC = vbRed
     If (VarCorr = "iFas3Fsx") Then
        If Con3F1 <> 0 Then
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
        Else
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC)
        End If
     End If
     If (VarCorr = "iCon3Fsx") Then
        If Con3F1 <> 0 Then
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 2)
        Else
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack)
        End If
     End If
     tidC = vbBlue
  Else
     tidC = CalcoloColore("iBom3Fsx", , , tColore)
     Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
  End If
  tisp = IIf(tidC = vbRed, 2, 1)
  If Bom3F1 <> 0 Then
    Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), Bom3F1, scalaxEl, tidC, tisp)
  End If
  
  'Fianco 1 Separazione Zone 1 e 2
  tidC = tColore
  tisp = 1
  tidP1 = 8
  tidP2 = 9
  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, tidst)
    
  'Fianco 1 Separazione Zone 2 e 3
  tidP1 = 10
  tidP2 = 11
  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, tidst)
  'Fianco 1 Linea tra primo e ultimo punto
  tidP1 = 0
  tidP2 = 16
  
  'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, vbDot)
  
  'Fianco2 Fascia1
  tidP1 = 4
  tidP2 = 5
  If (VarCorr = "iFas1Fdx") Or (VarCorr = "iCon1Fdx") Then
     tidC = vbRed
     If (VarCorr = "iFas1Fdx") Then
        If Con1F2 <> 0 Then
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
        Else
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC)
        End If
     End If
     If (VarCorr = "iCon1Fdx") Then
        If Con1F2 <> 0 Then
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 2)
        Else
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack)
        End If
     End If
     tidC = vbBlue
  Else
     tidC = CalcoloColore("iBom1Fdx", , , tColore)
     Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
  End If
  tisp = IIf(tidC = vbRed, 2, 1)
  If Bom1F2 <> 0 Then
    Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), -Bom1F2, scalaxEl, tidC, tisp)
  End If
  
  'Fianco2 Fascia2
  tidP1 = 5
  tidP2 = 6
  If (VarCorr = "iFas2Fdx") Or (VarCorr = "iCon2Fdx") Then
     tidC = vbRed
     If (VarCorr = "iFas2Fdx") Then
        If Con2F2 <> 0 Then
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
        Else
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC)
        End If
     End If
     If (VarCorr = "iCon2Fdx") Then
        If Con2F2 <> 0 Then
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 2)
        Else
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack)
        End If
     End If
     tidC = vbBlue
  Else
     tidC = CalcoloColore("iBom2Fdx", , , tColore)
     Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
  End If
  tisp = IIf(tidC = vbRed, 2, 1)
  If Bom2F2 <> 0 Then
    Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), -Bom2F2, scalaxEl, tidC, tisp)
  End If
  
  'Fianco2 Fascia3
  tidP1 = 6
  tidP2 = 7
  If (VarCorr = "iFas3Fdx") Or (VarCorr = "iCon3Fdx") Then
     tidC = vbRed
     If (VarCorr = "iFas3Fdx") Then
        If Con3F2 <> 0 Then
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
        Else
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC)
        End If
     End If
     If (VarCorr = "iCon3Fdx") Then
        If Con3F2 <> 0 Then
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 2)
        Else
           Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack)
        End If
     End If
     tidC = vbBlue
  Else
     tidC = CalcoloColore("iBom3Fdx", , , tColore)
     Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlue)
  End If
  tisp = IIf(tidC = vbRed, 2, 1)
  If Bom3F2 <> 0 Then
    Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), -Bom3F2, scalaxEl, tidC, tisp)
  End If
  tidC = tColore
  tisp = 1
  
  'Fianco 2 Separazione Zone 1 e 2
  tidP1 = 12
  tidP2 = 13
  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, tidst)
  'Fianco 2 Separazione Zone 2 e 3
  tidP1 = 14
  tidP2 = 15
  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, tidst)
  'Fianco 1 Linea tra primo e ultimo punto
  tidP1 = 4
  tidP2 = 17
  
  'Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, vbDot)
  
  'Scritte F1 e F2
  Call TestoAll(LS, RS, "FL", , True, 1)
  Call TestoAll(LS, -RS, "FR", , True, 1)
  If NotScale Then Call TestoInfo("DRAWING NOT IN SCALE ! ", , True, vbRed, True)
  nriga = 1530
  nstr = LoadString(g_hLanguageLibHandle, nriga, riga, 255)
  If nstr = 0 Then
     riga = "Down"
  End If
  Call TestoInfo(riga, 1)
  nriga = 1531
  nstr = LoadString(g_hLanguageLibHandle, nriga, riga, 255)
  If nstr = 0 Then
     riga = "Up"
  End If
  Call TestoInfo(riga, 2)
  '
  'Call TestoInfo("Corr. Scale: " & scalaxEl, 4)
  If LINGUA = "GR" Then
  Call TestoInfo("Spannseite", 4)
  Call TestoInfo("Units: [mm]", 3)
  Else
  Call TestoInfo("Clamping side", 4)
  Call TestoInfo("Units: [mm]", 3)
  End If
  
  If Ft2 > Ft Then
    Call TestoInfo("WARNING: (FT FL = " & Ft & ") < (FT FR = " & Ft2 & ")", , True, vbRed, True)
    Call QuotaH(MaxX1, Pos, MaxX2, Pos, "?", , vbRed, 3)
  ElseIf Ft2 < Ft Then
    Call TestoInfo("WARNING: (FT FL = " & Ft & ") > (FT FR = " & Ft2 & ")", , True, vbRed, True)
    Call QuotaH(MaxX2, -Pos, MaxX1, -Pos, "?", , vbRed, 3)
  End If
  'Inizio Pezzo
    X(55) = X(51)
    Y(55) = 0
    X(56) = X(51)
    Y(56) = 0 + 5
   
   Dim stmp As String
   
    If (CodTesta = 1) Then
       stmp = str(QuoStart2)
    Else
       stmp = str(QuoStart1)
    End If


   If (VarCorr = "QASSIALESTART_G[0,1]") Or (VarCorr = "QASSIALESTART_G[1,1]") Then
      Call linea(X(55), Y(55), X(56), Y(56), vbRed, 2, vbDot)
      Call QuotaH(X(55), Y(55), X(55), Y(55), "(Z)", , vbRed, 4, 2, 4)
      If (VarCorr = "QASSIALESTART_G[0,1]") Then
          QuoStart1 = Lp("QASSIALESTART_G[0,1]")
          stmp = str(QuoStart1)
          Call QuotaH(X(55), Y(55) - 2, X(55), Y(55) - 2, stmp, , vbBlack, 4, 2, 4)
      Else
          QuoStart2 = Lp("QASSIALESTART_G[1,1]")
          stmp = str(QuoStart2)
          Call QuotaH(X(55), Y(55) - 2, X(55), Y(55) - 2, stmp, , vbBlack, 4, 2, 4)
      End If
   Else
      Call linea(X(55), Y(55), X(56), Y(56), vbRed, 1, vbDot)
      Call QuotaH(X(55), Y(55), X(55), Y(55), "(Z)", , vbBlack, 4, , 4)
      Call QuotaH(X(55), Y(55) - 2, X(55), Y(55) - 2, stmp, , vbBlack, 4, 2, 4)
   End If
   
   Dim QuoPartenza As Double
   
   'Inizio Lavoro
   If LatoPart = -1 Then
     QuoPartenza = val(stmp) - CORSA_I
     Call QuotaH(XF0, Y(55) - 2, XF0, Y(55) - 2, str(QuoPartenza), , vbBlack, 4, , 4)
     
     If (VarCorr = "LatoPA") Then
        Call linea(XF0, Y(55), XF0, Y(56), vbRed, 2, vbDot)
        Call QuotaH(XF0, Y(55), XF0, Y(55), "(S)", , vbRed, 4, 2, 4)
     Else
        Call linea(XF0, Y(55), XF0, Y(56), vbRed, 1, vbDot)
        Call QuotaH(XF0, Y(55), XF0, Y(55), "(S)", , vbBlack, 4, , 4)
     End If
   Else
     QuoPartenza = val(stmp) - CORSA_I + Ft
     Call QuotaH(XF3, Y(55) - 2, XF3, Y(55) - 2, str(QuoPartenza), , vbBlack, 4, , 4)
     
     If (VarCorr = "LatoPA") Then
        Call linea(XF3, Y(55), XF3, Y(56), vbRed, 2, vbDot)
        Call QuotaH(XF3, Y(55), XF3, Y(55), "(S)", , vbRed, 4, 2, 4)
     Else
        Call linea(XF3, Y(55), XF3, Y(56), vbRed, 1, vbDot)
        Call QuotaH(XF3, Y(55), XF3, Y(55), "(S)", , vbBlack, 4, , 4)
     End If
   End If
     
  '
End Sub

Public Sub Disegna_CorrElica_DENTATURA(Stampante As Boolean)
'
Dim nZone     As Integer
Dim X()       As Double
Dim Y()       As Double
Dim Con1F1    As Double
Dim Con2F1    As Double
Dim Con3F1    As Double
Dim Con1F2    As Double
Dim Con2F2    As Double
Dim Con3F2    As Double
Dim Bom1F1    As Double
Dim Bom2F1    As Double
Dim Bom3F1    As Double
Dim Bom1F2    As Double
Dim Bom2F2    As Double
Dim Bom3F2    As Double
Dim MaxX1     As Double
Dim MaxX2     As Double
Dim scalaxEl  As Double
Dim Pos       As Double
Dim XF0       As Double
Dim XF1       As Double
Dim XF2       As Double
Dim XF3       As Double
Dim XF0_2     As Double
Dim XF1_2     As Double
Dim XF2_2     As Double
Dim XF3_2     As Double
Dim Ft        As Double
Dim Ft2       As Double
Dim F1        As Double
Dim F2        As Double
Dim F3        As Double
Dim F1_2      As Double
Dim F2_2      As Double
Dim F3_2      As Double
Dim Re        As Double
Dim RS        As Double
Dim LS        As Double
Dim tColore   As Long
Dim tX1       As Double
Dim tX        As Double
Dim tY        As Double
Dim i         As Integer
Dim NotScale  As Boolean
Dim Lin       As Double
Dim CORSA     As Double
'
On Error Resume Next
  '
  Call InizializzaVariabili
  '
  NotScale = False
  F1 = 0:   F2 = 0:   F3 = 0
  F1_2 = 0: F2_2 = 0: F3_2 = 0
  '
  scalaxEl = Lp("iScalaX")
  If (scalaxEl = 0) Then scalaxEl = Lp("iScalaX")
  '
  CORSA = Lp("CORSAOUT")
  nZone = Lp("iNzone")
  Select Case nZone
    Case 1
        F1 = Lp("iFas1F1") + 2 * CORSA: F1_2 = Lp("iFas1F2") + 2 * CORSA
    Case 2
        F1 = Lp("iFas1F1") + 1 * CORSA: F1_2 = Lp("iFas1F2") + 1 * CORSA
        F2 = Lp("iFas2F1") + 1 * CORSA: F2_2 = Lp("iFas2F2") + 1 * CORSA
    Case 3
        F1 = Lp("iFas1F1") + 1 * CORSA: F1_2 = Lp("iFas1F2") + 1 * CORSA
        F2 = Lp("iFas2F1"):             F2_2 = Lp("iFas2F2")
        F3 = Lp("iFas3F1") + 1 * CORSA: F3_2 = Lp("iFas3F2") + 1 * CORSA
  End Select
  Ft = F1 + F2 + F3         'Fascia Totale F1
  Ft2 = F1_2 + F2_2 + F3_2  'Fascia Totale F2
  '
  Re = 10:  RS = 10:  LS = 10
  XF0 = LS:    XF1 = XF0 + F1:        XF2 = XF1 + F2:        XF3 = XF2 + F3
  XF0_2 = LS:  XF1_2 = XF0_2 + F1_2:  XF2_2 = XF1_2 + F2_2:  XF3_2 = XF2_2 + F3_2
  '
  ReDim X(45):  ReDim Y(45)
  '
  Con1F1 = Lp("iCon1F1"):  Con2F1 = Lp("iCon2F1"):  Con3F1 = Lp("iCon3F1")
  Con1F2 = Lp("iCon1F2"):  Con2F2 = Lp("iCon2F2"):  Con3F2 = Lp("iCon3F2")
  '
  Bom1F1 = Lp("iBom1F1"):  Bom2F1 = Lp("iBom2F1"):  Bom3F1 = Lp("iBom3F1")
  Bom1F2 = Lp("iBom1F2"):  Bom2F2 = Lp("iBom2F2"):  Bom3F2 = Lp("iBom3F2")
  '
  Pos = RS
  X(0) = XF0
  X(1) = XF1
  X(2) = XF2
  X(3) = XF3
  X(16) = XF3
  Y(0) = Pos
  Y(1) = Pos + (Con1F1 * 1 + Con2F1 * 0 + Con3F1 * 0) * scalaxEl
  Y(2) = Pos + (Con1F1 * 1 + Con2F1 * 1 + Con3F1 * 0) * scalaxEl
  Y(3) = Pos + (Con1F1 * 1 + Con2F1 * 1 + Con3F1 * 1) * scalaxEl
  Y(16) = Pos
  X(4) = XF0_2
  X(5) = XF1_2
  X(6) = XF2_2
  X(7) = XF3_2
  X(17) = XF3_2
  Y(4) = -Pos
  Y(5) = -Pos - (Con1F2 * 1 + Con2F2 * 0 + Con3F2 * 0) * scalaxEl
  Y(6) = -Pos - (Con1F2 * 1 + Con2F2 * 1 + Con3F2 * 0) * scalaxEl
  Y(7) = -Pos - (Con1F2 * 1 + Con2F2 * 1 + Con3F2 * 1) * scalaxEl
  Y(17) = -Pos
  'Lineette
  X(8) = X(1)
  X(9) = X(1)
  X(12) = X(5)
  X(13) = X(5)
  X(10) = X(2)
  X(11) = X(2)
  X(14) = X(6)
  X(15) = X(6)
  Lin = 3
  Y(8) = Y(1) + Lin
  Y(9) = Y(1) - Lin
  Y(12) = Y(5) + Lin
  Y(13) = Y(5) - Lin
  Y(10) = Y(2) + Lin
  Y(11) = Y(2) - Lin
  Y(14) = Y(6) + Lin
  Y(15) = Y(6) - Lin
  Dim StepGriglia As Double
  StepGriglia = Lp("iStepGriglia")
  For i = -3 To 3
    Y(21 + i) = Y(0) + i * StepGriglia * scalaxEl
    Y(28 + i) = Y(0) + i * StepGriglia * scalaxEl
    Y(35 + i) = Y(4) + i * StepGriglia * scalaxEl
    Y(42 + i) = Y(4) + i * StepGriglia * scalaxEl
    X(21 + i) = XF0
    X(28 + i) = XF3
    X(35 + i) = XF0
    X(42 + i) = XF3
  Next i
  PAR.MaxY = Y(0)
  PAR.MinY = Y(4)
  For i = 1 To 3
    If Y(i) > PAR.MaxY Then PAR.MaxY = Y(i)
    If Y(i + 4) < PAR.MinY Then PAR.MinY = Y(i + 4)
  Next i
  If Ft >= Ft2 Then
    PAR.MaxX = Ft + LS
  Else
    PAR.MaxX = Ft2 + LS
  End If
  PAR.MinX = LS / 2
  'par.MaxY = Re / 2
  'par.MinY = -Re / 2
  PAR.ScalaMargine = 0.9
  PAR.Allineamento = 0
  Call CalcolaParPic(PAR, True, Stampante)
  'par.Pic.Cls
  'linea XMola, Re + 20 + DiaMola, XMola, Re, vbRed, 1, vbDash
  MaxX1 = X(1)
  MaxX2 = X(5)
  If Not NotScale Then
    Select Case nZone 'Disegno linee verticali che delimitano le zone
      Case 2
        Call linea(XF1, 0, XF1, Re, RGB(180, 180, 180), 1, vbDash)
        Call QuotaH(XF0, Re, XF1, Re, "" & F1, 12, CalcoloColore("iFas1F1"), 3)
        Call QuotaH(XF1, Re, XF2, Re, "" & F2, 12, CalcoloColore("iFas2F1"), 3)
        Call linea(XF1_2, -Re, XF1_2, 0, RGB(180, 180, 180), 1, vbDash)
        Call QuotaH(XF0_2, -Re, XF1_2, -Re, "" & F1_2, 12, CalcoloColore("iFas1F2"), 3)
        Call QuotaH(XF1_2, -Re, XF2_2, -Re, "" & F2_2, 12, CalcoloColore("iFas2F2"), 3)
        MaxX1 = X(2)
        MaxX2 = X(6)
      Case 3
        Call linea(XF1, 0, XF1, Re, RGB(180, 180, 180), 1, vbDash)
        Call linea(XF2, 0, XF2, Re, RGB(180, 180, 180), 1, vbDash)
        Call QuotaH(XF0, Re, XF1, Re, "" & F1, 12, CalcoloColore("iFas1F1"), 3)
        Call QuotaH(XF1, Re, XF2, Re, "" & F2, 12, CalcoloColore("iFas2F1"), 3)
        Call QuotaH(XF2, Re, XF3, Re, "" & F3, 12, CalcoloColore("iFas3F1"), 3)
        Call linea(XF1_2, -Re, XF1_2, 0, RGB(180, 180, 180), 1, vbDash)
        Call linea(XF2_2, -Re, XF2_2, 0, RGB(180, 180, 180), 1, vbDash)
        Call QuotaH(XF0_2, -Re, XF1_2, -Re, "" & F1_2, 12, CalcoloColore("iFas1F2"), 3)
        Call QuotaH(XF1_2, -Re, XF2_2, -Re, "" & F2_2, 12, CalcoloColore("iFas2F2"), 3)
        Call QuotaH(XF2_2, -Re, XF3_2, -Re, "" & F3_2, 12, CalcoloColore("iFas3F2"), 3)
        MaxX1 = X(3)
        MaxX2 = X(7)
    End Select
  End If
  ' Asse pezzo
  linea LS, 0, LS + Ft, 0, RGB(180, 180, 180), 1, vbDashDot
  If Not NotScale Then
    Call QuotaH(LS, PAR.MaxY + 8, LS + Ft, PAR.MaxY + 8, "" & Ft, 12, , 3)
  End If
  Dim tidP1   As Integer
  Dim tidP2   As Integer
  Dim tidC    As Long
  Dim tidst   As Integer
  Dim tisp    As Integer
  tColore = vbBlue
  tidst = vbSolid
  'Linee (griglia)
  For i = -3 To 3
    tidP1 = 21 + i
    tidP2 = 28 + i
    Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), RGB(200, 200, 200), 1, vbDot)
    tidP1 = 35 + i
    tidP2 = 42 + i
    Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), RGB(200, 200, 200), 1, vbDot)
  Next i
  'Fianco1 Fascia1
  tidP1 = 0
  tidP2 = 1
  tidC = CalcoloColore("iCon1F1;iBom1F1", , , tColore)
  tisp = IIf(tidC = vbRed, 2, 1)
  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack)
  If Bom1F1 <> 0 Then
    Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), Bom1F1, scalaxEl, tidC, tisp)
  End If
  'Fianco1 Fascia2
  tidP1 = 1
  tidP2 = 2
  tidC = CalcoloColore("iCon2F1;iBom2F1", , , tColore)
  tisp = IIf(tidC = vbRed, 2, 1)
  linea X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack
  If Bom2F1 <> 0 Then
    Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), Bom2F1, scalaxEl, tidC, tisp)
  End If
  'Fianco1 Fascia3
  tidP1 = 2
  tidP2 = 3
  tidC = CalcoloColore("iCon3F1;iBom3F1", , , tColore)
  tisp = IIf(tidC = vbRed, 2, 1)
  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack)
  If Bom3F1 <> 0 Then
    Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), Bom3F1, scalaxEl, tidC, tisp)
  End If
  'Fianco 1 Separazione Zone 1 e 2
  tidC = tColore
  tisp = 1
  tidP1 = 8
  tidP2 = 9
  linea X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, tidst
  'Fianco 1 Separazione Zone 2 e 3
  tidP1 = 10
  tidP2 = 11
  linea X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, tidst
  'Fianco 1 Linea tra primo e ultimo punto
  tidP1 = 0
  tidP2 = 16
  linea X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, vbDot
  'Fianco2 Fascia1
  tidP1 = 4
  tidP2 = 5
  tidC = CalcoloColore("iCon1F2;iBom1F2", , , tColore)
  tisp = IIf(tidC = vbRed, 2, 1)
  linea X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack
  If Bom1F2 <> 0 Then
    Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), -Bom1F2, scalaxEl, tidC, tisp)
  End If
  'Fianco2 Fascia2
  tidP1 = 5
  tidP2 = 6
  tidC = CalcoloColore("iCon2F2;iBom2F2", , , tColore)
  tisp = IIf(tidC = vbRed, 2, 1)
  'linea x(tidP1), Y(tidP1), x(tidP2), Y(tidP2), tidC, tisp, tidst
  linea X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack
  If Bom2F2 <> 0 Then
    Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), -Bom2F2, scalaxEl, tidC, tisp)
  End If
  'Fianco2 Fascia3
  tidP1 = 6
  tidP2 = 7
  tidC = CalcoloColore("iCon3F2;iBom3F2", , , tColore)
  tisp = IIf(tidC = vbRed, 2, 1)
  'linea x(tidP1), Y(tidP1), x(tidP2), Y(tidP2), tidC, tisp, tidst
  linea X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), vbBlack
  If Bom3F2 <> 0 Then
    Call Disegna_BombaturaElica(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), -Bom3F2, scalaxEl, tidC, tisp)
  End If
  tidC = tColore
  tisp = 1
  'Fianco 2 Separazione Zone 1 e 2
  tidP1 = 12
  tidP2 = 13
  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, tidst)
  'Fianco 2 Separazione Zone 2 e 3
  tidP1 = 14
  tidP2 = 15
  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, tidst)
  'Fianco 1 Linea tra primo e ultimo punto
  tidP1 = 4
  tidP2 = 17
  Call linea(X(tidP1), Y(tidP1), X(tidP2), Y(tidP2), tidC, 1, vbDot)
  'Scritte F1 e F2
  Call TestoAll(LS, RS, "F1", , True, 1)
  Call TestoAll(LS, -RS, "F2", , True, 1)
  If NotScale Then Call TestoInfo("DRAWING NOT IN SCALE ! ", , True, vbRed, True)
  Call TestoInfo("Corr. Scale: " & scalaxEl, 1)
  Call TestoInfo("Units: [mm]", 3)
  '
  If (Ft2 > Ft) Then
    Call TestoInfo("WARNING: (FT F1 = " & Ft & ") < (FT F2 = " & Ft2 & ")", , True, vbRed, True)
    Call QuotaH(MaxX1, Pos, MaxX2, Pos, "?", , vbRed, 3)
  ElseIf Ft2 < Ft Then
    Call TestoInfo("WARNING: (FT F1 = " & Ft & ") > (FT F2 = " & Ft2 & ")", , True, vbRed, True)
    Call QuotaH(MaxX2, -Pos, MaxX1, -Pos, "?", , vbRed, 3)
  End If
  '
End Sub

Public Sub RidisegnaPagina(Pagina As Integer, Optional Stampante As Boolean = False)
'
Dim TIPO_LAVORAZIONE As String
'
On Error Resume Next
  '
  'LETTURA DEL TIPO DI LAVORAZIONE
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  Select Case TIPO_LAVORAZIONE
    Case "MOLAVITE":                  Call RidisegnaPagina_MOLAVITE(Pagina, Stampante)
    Case "MOLAVITEG160":              Call RidisegnaPagina_MOLAVITEG160(Pagina, Stampante)
    Case "MOLAVITE_SG":               Call RidisegnaPagina_MOLAVITE_SG(Pagina, Stampante)
    Case "DENTATURA":                 Call RidisegnaPagina_DENTATURA(Pagina, Stampante)
    Case "INGR380_ESTERNI":           Call RidisegnaPagina_INGRANAGGI_ESTERNI(Pagina, Stampante)
    Case "INGRANAGGI_ESTERNIV":       Call RidisegnaPagina_INGRANAGGI_ESTERNIV(Pagina, Stampante)
    Case "INGRANAGGI_ESTERNIO":       Call RidisegnaPagina_INGRANAGGI_ESTERNIO(Pagina, Stampante)
    Case "PROFILO_RAB_ESTERNI":       Call RidisegnaPagina_PROFILO_RAB_ESTERNI(Pagina, Stampante)
    Case "SCANALATIEVO":              Call RidisegnaPagina_SCANALATIEVO(Pagina, Stampante)
    Case "SCANALATI_ESTERNI":         Call RidisegnaPagina_SCANALATI_ESTERNI(Pagina, Stampante)
    Case "SCANALATI_CBN":             Call RidisegnaPagina_SCANALATI_CBN(Pagina, Stampante)
    Case "SCANALATI_INTERNI_CBN":     Call RidisegnaPagina_SCANALATI_INTERNI_CBN(Pagina, Stampante)
    Case "INGRANAGGI_INTERNI":        Call RidisegnaPagina_INGRANAGGI_INTERNI(Pagina, Stampante)
    Case "ROTORI_ESTERNI":            Call RidisegnaPagina_ROTORI_ESTERNI(Pagina, Stampante)
    Case "VITI_ESTERNE":              Call RidisegnaPagina_VITI_ESTERNE(Pagina, Stampante)
    Case "VITIV_ESTERNE":             Call RidisegnaPagina_VITIV_ESTERNE(Pagina, Stampante)
    Case "PROFILO_PER_PUNTI_ESTERNI": Call RidisegnaPagina_PROFILO_PER_PUNTI_ESTERNI(Pagina, Stampante)
    Case "VITI_CONICHE":              Call RidisegnaPagina_VITI_CONICHE(Pagina, Stampante)
    Case "VITI_PASSO_VARIABILE":      Call RidisegnaPagina_VITI_PASSO_VARIABILE(Pagina, Stampante)
    Case "BS5L":                      Call RidisegnaPagina_BS5L(Pagina, Stampante)
    Case "BS5S":                      Call RidisegnaPagina_BS5S(Pagina, Stampante)
    Case "VITI_BARRIERA3":            Call RidisegnaPagina_VITI_BARRIERA3(Pagina, Stampante)
    Case "VITI_BARRIERA6":            Call RidisegnaPagina_VITI_BARRIERA6(Pagina, Stampante)
    Case "PROFILO_PER_PUNTI_INTERNI": Call RidisegnaPagina_PROFILO_PER_PUNTI_INTERNI(Pagina, Stampante)
  End Select
  '
End Sub

Public Sub RidisegnaPagina_INGRANAGGI_ESTERNI(Pagina As Integer, Optional Stampante As Boolean = False)
'
Dim Z     As Double
Dim Mn    As Double
Dim ALFA  As Double
Dim ALFAt As Double
Dim Beta  As Double
'
Dim DP    As Double
Dim DB    As Double
Dim DE    As Double
Dim DI    As Double
Dim dis   As String
'
On Error Resume Next
  '
  Call InizializzaVariabili
  '
  If Not ParametriOK Then
    If Not Stampante Then PAR.Pic.Cls
    Exit Sub
  End If
  '
  If Stampante Then
    PAR.RegioneX1 = StampaLeft
    PAR.RegioneX2 = StampaLeft + StampaWidth
    PAR.RegioneY1 = StampaTop
    PAR.RegioneY2 = StampaTop + StampaHeight
    PAR.Printing = True
    PAR.AllBlack = True
  Else
    Call DefRegione(PAR.Pic)
    PAR.Printing = False
    PAR.AllBlack = False
  End If
  '
  If (CurrVis = 0) Or Stampante Then
    'LastPage
    Select Case Pagina
      Case 1, 2, 3:  CurrVis = Pagina
      Case 4:        CurrVis = 6
    End Select
  End If
  '
  Select Case ActualParGroup
    Case "OEM1_PROFILO91":    Call Disegna_DenteIngranaggio(Stampante)
    Case "OEM1_PROFILOK91":   Call Disegna_ProfiloK_ESTERNI(Stampante)
    Case "OEM1_LAVORO91":     Call Disegna_ZONA_LAVORO_INGRANAGGI_ESTERNI(Stampante)
    'Case "1":                 Call Disegna_Contatto(Stampante)
    'Case "2":                 Call Disegna_Info
    Case "OEM1_CORRELICA91":  Call Disegna_CorrElica(Stampante)
  End Select
  '
  If Stampante Then PAR.Su.SetFocus
  '
Exit Sub

End Sub

Public Sub RidisegnaPagina_INGRANAGGI_ESTERNIO(Pagina As Integer, Optional Stampante As Boolean = False)
'
Dim Z     As Double
Dim Mn    As Double
Dim ALFA  As Double
Dim ALFAt As Double
Dim Beta  As Double
'
Dim DP    As Double
Dim DB    As Double
Dim DE    As Double
Dim DI    As Double
Dim dis   As String
'
On Error Resume Next
  '
  Call InizializzaVariabili
  '
  If Not ParametriOK Then
    If Not Stampante Then PAR.Pic.Cls
    Exit Sub
  End If
  '
  If Stampante Then
    PAR.RegioneX1 = StampaLeft
    PAR.RegioneX2 = StampaLeft + StampaWidth
    PAR.RegioneY1 = StampaTop
    PAR.RegioneY2 = StampaTop + StampaHeight
    PAR.Printing = True
    PAR.AllBlack = True
  Else
    Call DefRegione(PAR.Pic)
    PAR.Printing = False
    PAR.AllBlack = False
  End If
  '
  If (CurrVis = 0) Or Stampante Then
    'LastPage
    Select Case Pagina
      Case 1, 2, 3:  CurrVis = Pagina
      Case 4:        CurrVis = 6
    End Select
  End If
  '
  Select Case ActualParGroup
    Case "OEM1_PROFILO92":    Call Disegna_DenteIngranaggio(Stampante)
    Case "OEM1_PROFILOK92":   Call Disegna_ProfiloK_ESTERNI(Stampante)
    Case "OEM1_LAVORO92":     Call Disegna_ZONA_LAVORO_INGRANAGGI_ESTERNIO(Stampante)
    Case "OEM0_CICLI92":      Call Disegna_ZONA_LAVORO_INGRANAGGI_ESTERNIO(Stampante)
    Case "OEM0_CICLIB92":     Call Disegna_ZONA_LAVORO_INGRANAGGI_ESTERNIO(Stampante)
    'Case "1":                 Call Disegna_Contatto(Stampante)
    'Case "2":                 Call Disegna_Info
    Case "OEM1_CORRELICA92":  Call Disegna_CorrElica(Stampante)
    Case "OEM1_MOLA72":       Call Disegna_Mola_RDE(Stampante)
  End Select
  '
  If Stampante Then PAR.Su.SetFocus
  '
Exit Sub

End Sub

Public Sub RidisegnaPagina_INGRANAGGI_ESTERNIV(Pagina As Integer, Optional Stampante As Boolean = False)
'
Dim Z     As Double
Dim Mn    As Double
Dim ALFA  As Double
Dim ALFAt As Double
Dim Beta  As Double
'
Dim DP    As Double
Dim DB    As Double
Dim DE    As Double
Dim DI    As Double
Dim dis   As String
'
On Error Resume Next
  '
  Call InizializzaVariabili
  '
  If Not ParametriOK Then
    If Not Stampante Then PAR.Pic.Cls
    Exit Sub
  End If
  '
  If Stampante Then
    PAR.RegioneX1 = StampaLeft
    PAR.RegioneX2 = StampaLeft + StampaWidth
    PAR.RegioneY1 = StampaTop
    PAR.RegioneY2 = StampaTop + StampaHeight
    PAR.Printing = True
    PAR.AllBlack = True
  Else
    Call DefRegione(PAR.Pic)
    PAR.Printing = False
    PAR.AllBlack = False
  End If
  '
  If (CurrVis = 0) Or Stampante Then
    'LastPage
    Select Case Pagina
      Case 1, 2, 3:  CurrVis = Pagina
      Case 4:        CurrVis = 6
    End Select
  End If
  '
  Select Case ActualParGroup
    Case "OEM1_PROFILO91":    Call Disegna_DenteIngranaggio(Stampante)
    Case "OEM1_PROFILOK91":   Call Disegna_ProfiloK_ESTERNI(Stampante)
    Case "OEM1_LAVORO91":     Call Disegna_ZONA_LAVORO_INGRANAGGI_ESTERNIV(Stampante)
    'Case "1":                 Call Disegna_Contatto(Stampante)
    'Case "2":                 Call Disegna_Info
    Case "OEM1_CORRELICA91":  Call Disegna_CorrElica(Stampante)
  End Select
  '
  If Stampante Then PAR.Su.SetFocus
  '
Exit Sub

End Sub

Public Sub RidisegnaPagina_PROFILO_RAB_ESTERNI(Pagina As Integer, Optional Stampante As Boolean = False)
'
On Error Resume Next
  '
  Call InizializzaVariabili
  '
  If Not ParametriOK Then
    If Not Stampante Then PAR.Pic.Cls
    Exit Sub
  End If
  '
  If Stampante Then
    PAR.RegioneX1 = StampaLeft
    PAR.RegioneX2 = StampaLeft + StampaWidth
    PAR.RegioneY1 = StampaTop
    PAR.RegioneY2 = StampaTop + StampaHeight
    PAR.Printing = True
    PAR.AllBlack = True
  Else
    Call DefRegione(PAR.Pic)
    PAR.Printing = False
    PAR.AllBlack = False
  End If
  '
  Select Case ActualParGroup
    Case "OEM0_CORRETTORI7":         Call Disegna_PROFILO_RAB_ESTERNI(Stampante)
    Case "INP1_PROFILO_RAB_ESTERNI": Call Disegna_PROFILO_RAB_ESTERNI(Stampante)
    Case "OEM1_MACCHINA7":           Call Disegna_ZONA_LAVORO_PROFILO_RAB_ESTERNI(Stampante)
    Case "OEM1_LAVORO7":             Call Disegna_ZONA_LAVORO_PROFILO_RAB_ESTERNI(Stampante)
    Case "OEM1_CORRELICA7":          Call Disegna_CorrElica(Stampante)
  End Select
  '
  If Stampante Then PAR.Su.SetFocus
  '
Exit Sub

End Sub

Public Sub RidisegnaPagina_SCANALATIEVO(Pagina As Integer, Optional Stampante As Boolean = False)
'
Dim Z     As Double
Dim Mn    As Double
Dim ALFA  As Double
Dim ALFAt As Double
Dim Beta  As Double
Dim PG    As Double
Dim DP    As Double
Dim DB    As Double
Dim DE    As Double
Dim DI    As Double
Dim dis   As String
'
On Error Resume Next
  '
  Call InizializzaVariabili
  '
  If Not ParametriOK Then
    If Not Stampante Then PAR.Pic.Cls
    Exit Sub
  End If
  '
  If Stampante Then
    PAR.RegioneX1 = StampaLeft
    PAR.RegioneX2 = StampaLeft + StampaWidth
    PAR.RegioneY1 = StampaTop
    PAR.RegioneY2 = StampaTop + StampaHeight
    PAR.Printing = True
    PAR.AllBlack = True
  Else
    Call DefRegione(PAR.Pic)
    PAR.Printing = False
    PAR.AllBlack = False
  End If
  '
  If (CurrVis = 0) Or Stampante Then
    'LastPage
    Select Case Pagina
      Case 1, 2, 3:  CurrVis = Pagina
      Case 4:        CurrVis = 6
    End Select
  End If
  '
  Select Case ActualParGroup
    Case "OEM1_PROFILO13":    Call Disegna_DenteIngranaggio(Stampante)
    Case "OEM1_PROFILOK13":   Call Disegna_ProfiloK_ESTERNI(Stampante)
    Case "OEM1_LAVORO13":     Call Disegna_ZONA_LAVORO_INGRANAGGI_ESTERNI(Stampante)
    'Case "1":                 Call Disegna_Contatto(Stampante)
    'Case "2":                 Call Disegna_Info
    Case "OEM1_CORRELICA13":  Call Disegna_CorrElica(Stampante)
  End Select
  '
  If Stampante Then PAR.Su.SetFocus
  '
Exit Sub

End Sub

Public Sub RidisegnaPagina_PROFILO_PER_PUNTI_ESTERNI(Pagina As Integer, Optional Stampante As Boolean = False)
'
Dim Z     As Double
Dim Mn    As Double
Dim ALFA  As Double
Dim ALFAt As Double
Dim Beta  As Double
Dim PG    As Double
Dim DP    As Double
Dim DB    As Double
Dim DE    As Double
Dim DI    As Double
Dim dis   As String
'
On Error Resume Next
  '
  Call InizializzaVariabili
  '
  If Not ParametriOK Then
    If Not Stampante Then PAR.Pic.Cls
    Exit Sub
  End If
  '
  If Stampante Then
    PAR.RegioneX1 = StampaLeft
    PAR.RegioneX2 = StampaLeft + StampaWidth
    PAR.RegioneY1 = StampaTop
    PAR.RegioneY2 = StampaTop + StampaHeight
    PAR.Printing = True
    PAR.AllBlack = True
  Else
    Call DefRegione(PAR.Pic)
    PAR.Printing = False
    PAR.AllBlack = False
  End If
  '
  If (CurrVis = 0) Or Stampante Then
    'LastPage
    Select Case Pagina
      Case 1, 2, 3:  CurrVis = Pagina
      Case 4:        CurrVis = 6
    End Select
  End If
  '
  Select Case ActualParGroup
    Case "OEM1_CORRELICA12":   Call Disegna_CorrElica(Stampante)
    Case "OEM1_LAVORO12":      Call Disegna_ZONA_LAVORO_PROFILO_PER_PUNTI_ESTERNI(Stampante)
  End Select
  '
  If Stampante Then PAR.Su.SetFocus
  '
Exit Sub

End Sub

Public Sub RidisegnaPagina_PROFILO_PER_PUNTI_INTERNI(Pagina As Integer, Optional Stampante As Boolean = False)
'
Dim Z     As Double
Dim Mn    As Double
Dim ALFA  As Double
Dim ALFAt As Double
Dim Beta  As Double
Dim PG    As Double
Dim DP    As Double
Dim DB    As Double
Dim DE    As Double
Dim DI    As Double
Dim dis   As String
'
On Error Resume Next
  '
  Call InizializzaVariabili
  '
  If Not ParametriOK Then
    If Not Stampante Then PAR.Pic.Cls
    Exit Sub
  End If
  '
  If Stampante Then
    PAR.RegioneX1 = StampaLeft
    PAR.RegioneX2 = StampaLeft + StampaWidth
    PAR.RegioneY1 = StampaTop
    PAR.RegioneY2 = StampaTop + StampaHeight
    PAR.Printing = True
    PAR.AllBlack = True
  Else
    Call DefRegione(PAR.Pic)
    PAR.Printing = False
    PAR.AllBlack = False
  End If
  '
  If (CurrVis = 0) Or Stampante Then
    'LastPage
    Select Case Pagina
      Case 1, 2, 3:  CurrVis = Pagina
      Case 4:        CurrVis = 6
    End Select
  End If
  '
  Select Case ActualParGroup
    Case "OEM1_CORRELICA22":   Call Disegna_CorrElica(Stampante)
    Case "OEM1_LAVORO22":      Call Disegna_ZONA_LAVORO_PROFILO_PER_PUNTI_INTERNI(Stampante)
  End Select
  '
  If Stampante Then PAR.Su.SetFocus
  '
Exit Sub

End Sub

Public Sub RidisegnaPagina_SCANALATI_ESTERNI(Pagina As Integer, Optional Stampante As Boolean = False)
'
Dim Z     As Double
Dim Mn    As Double
Dim ALFA  As Double
Dim ALFAt As Double
Dim Beta  As Double
Dim PG    As Double
Dim DP    As Double
Dim DB    As Double
Dim DE    As Double
Dim DI    As Double
Dim dis   As String
Dim stmp  As String
'
On Error Resume Next
  '
  Call InizializzaVariabili
  '
  If Not ParametriOK Then
    If Not Stampante Then PAR.Pic.Cls
    Exit Sub
  End If
  '
  If Stampante Then
    PAR.RegioneX1 = StampaLeft
    PAR.RegioneX2 = StampaLeft + StampaWidth
    PAR.RegioneY1 = StampaTop
    PAR.RegioneY2 = StampaTop + StampaHeight
    PAR.Printing = True
    PAR.AllBlack = True
  Else
    Call DefRegione(PAR.Pic)
    PAR.Printing = False
    PAR.AllBlack = False
  End If
  '
  If (CurrVis = 0) Or Stampante Then
    'LastPage
    Select Case Pagina
      Case 1, 2, 3:  CurrVis = Pagina
      Case 4:        CurrVis = 6
    End Select
  End If
  '
  'LETTURA Tipo Macchina
  stmp = GetInfo("Configurazione", "TipoMacchina", Path_LAVORAZIONE_INI)
  stmp = UCase$(Trim$(stmp))
  Select Case ActualParGroup
    Case "OEM1_PROFILO33"
      If (stmp = "ORIZZONTALE") Then
        Call Disegna_ZONA_LAVORO_SCANALATI_ESTERNI(Stampante)
      Else
        Call Disegna_ZONA_LAVORO_SCANALATI_ESTERNIV(Stampante)
      End If
    Case "OEM1_LAVORO3"
      If (stmp = "ORIZZONTALE") Then
        Call Disegna_ZONA_LAVORO_SCANALATI_ESTERNI(Stampante)
      Else
        Call Disegna_ZONA_LAVORO_SCANALATI_ESTERNIV(Stampante)
      End If
    Case "OEM1_CORRELICA3"
      Call Disegna_CorrElica(Stampante)
  End Select
  '
  If Stampante Then PAR.Su.SetFocus
  '
Exit Sub

End Sub

Public Sub RidisegnaPagina_SCANALATI_CBN(Pagina As Integer, Optional Stampante As Boolean = False)
'
On Error Resume Next
  '
  Call InizializzaVariabili
  '
  If Stampante Then
    PAR.RegioneX1 = StampaLeft
    PAR.RegioneX2 = StampaLeft + StampaWidth
    PAR.RegioneY1 = StampaTop
    PAR.RegioneY2 = StampaTop + StampaHeight
    PAR.Printing = True
    PAR.AllBlack = True
  Else
    Call DefRegione(PAR.Pic)
    PAR.Printing = False
    PAR.AllBlack = False
  End If
  '
  If (CurrVis = 0) Or Stampante Then
    'LastPage
    Select Case Pagina
      Case 1, 2, 3:  CurrVis = Pagina
      Case 4:        CurrVis = 6
    End Select
  End If
  '
  Select Case ActualParGroup
    Case "OEM1_PROFILO33":  Call Disegna_Profilo_SCANALATO_DRITTO(Stampante)
    Case "OEM1_LAVORO33":   Call Disegna_ZONA_LAVORO_SCANALATI_CBN(Stampante)
  End Select
  '
  If Stampante Then PAR.Su.SetFocus
  '
Exit Sub

End Sub

Public Sub RidisegnaPagina_SCANALATI_INTERNI_CBN(Pagina As Integer, Optional Stampante As Boolean = False)
'
On Error Resume Next
  '
  Call InizializzaVariabili
  '
  If Stampante Then
    PAR.RegioneX1 = StampaLeft
    PAR.RegioneX2 = StampaLeft + StampaWidth
    PAR.RegioneY1 = StampaTop
    PAR.RegioneY2 = StampaTop + StampaHeight
    PAR.Printing = True
    PAR.AllBlack = True
  Else
    Call DefRegione(PAR.Pic)
    PAR.Printing = False
    PAR.AllBlack = False
  End If
  '
  If (CurrVis = 0) Or Stampante Then
    'LastPage
    Select Case Pagina
      Case 1, 2, 3:  CurrVis = Pagina
      Case 4:        CurrVis = 6
    End Select
  End If
  '
  Select Case ActualParGroup
    Case "OEM1_PROFILO34":  Call Disegna_Profilo_SCANALATI_INTERNI_CBN(Stampante)
    Case "OEM1_LAVORO34":   Call Disegna_ZONA_LAVORO_SCANALATI_INTERNI_CBN(Stampante)
  End Select
  '
  If Stampante Then PAR.Su.SetFocus
  '
Exit Sub

End Sub

Public Sub RidisegnaPagina_VITI_CONICHE(Pagina As Integer, Optional Stampante As Boolean = False)
'
On Error Resume Next
  '
  Call InizializzaVariabili
  '
  If Stampante Then
    PAR.RegioneX1 = StampaLeft
    PAR.RegioneX2 = StampaLeft + StampaWidth
    PAR.RegioneY1 = StampaTop
    PAR.RegioneY2 = StampaTop + StampaHeight
    PAR.Printing = True
    PAR.AllBlack = True
  Else
    Call DefRegione(PAR.Pic)
    PAR.Printing = False
    PAR.AllBlack = False
  End If
  '
  Select Case ActualParGroup
    Case "OEM1_MACCHINA41": Call Disegna_ZONA_LAVORO_VITI_CONICHE(Stampante)
    Case "OEM0_CICLI41":    Call Disegna_ZONA_LAVORO_VITI_CONICHE(Stampante)
  End Select
  '
  If Stampante Then PAR.Su.SetFocus
  '
Exit Sub

End Sub

Public Sub RidisegnaPagina_VITI_PASSO_VARIABILE(Pagina As Integer, Optional Stampante As Boolean = False)
'
On Error Resume Next
  '
  Call InizializzaVariabili
  '
  If Stampante Then
    PAR.RegioneX1 = StampaLeft
    PAR.RegioneX2 = StampaLeft + StampaWidth
    PAR.RegioneY1 = StampaTop
    PAR.RegioneY2 = StampaTop + StampaHeight
    PAR.Printing = True
    PAR.AllBlack = True
  Else
    Call DefRegione(PAR.Pic)
    PAR.Printing = False
    PAR.AllBlack = False
  End If
  '
  Select Case ActualParGroup
    Case "OEM1_MACCHINA45": Call Disegna_ZONA_LAVORO_VITI_PASSO_VARIABILE(Stampante)
    Case "OEM0_CICLI45":    Call Disegna_ZONA_LAVORO_VITI_PASSO_VARIABILE(Stampante)
  End Select
  '
  If Stampante Then PAR.Su.SetFocus
  '
Exit Sub

End Sub

Public Sub RidisegnaPagina_VITI_BARRIERA3(Pagina As Integer, Optional Stampante As Boolean = False)
'
On Error Resume Next
  '
  Call InizializzaVariabili
  '
  If Stampante Then
    PAR.RegioneX1 = StampaLeft
    PAR.RegioneX2 = StampaLeft + StampaWidth
    PAR.RegioneY1 = StampaTop
    PAR.RegioneY2 = StampaTop + StampaHeight
    PAR.Printing = True
    PAR.AllBlack = True
  Else
    Call DefRegione(PAR.Pic)
    PAR.Printing = False
    PAR.AllBlack = False
  End If
  '
  If Stampante Then PAR.Su.SetFocus
  '
Exit Sub

End Sub

Public Sub RidisegnaPagina_VITI_BARRIERA6(Pagina As Integer, Optional Stampante As Boolean = False)
'
On Error Resume Next
  '
  Call InizializzaVariabili
  '
  If Stampante Then
    PAR.RegioneX1 = StampaLeft
    PAR.RegioneX2 = StampaLeft + StampaWidth
    PAR.RegioneY1 = StampaTop
    PAR.RegioneY2 = StampaTop + StampaHeight
    PAR.Printing = True
    PAR.AllBlack = True
  Else
    Call DefRegione(PAR.Pic)
    PAR.Printing = False
    PAR.AllBlack = False
  End If
  '
  If Stampante Then PAR.Su.SetFocus
  '
Exit Sub

End Sub

Public Sub RidisegnaPagina_BS5L(Pagina As Integer, Optional Stampante As Boolean = False)
'
On Error Resume Next
  '
  Call InizializzaVariabili
  '
  If Stampante Then
    PAR.RegioneX1 = StampaLeft
    PAR.RegioneX2 = StampaLeft + StampaWidth
    PAR.RegioneY1 = StampaTop
    PAR.RegioneY2 = StampaTop + StampaHeight
    PAR.Printing = True
    PAR.AllBlack = True
  Else
    Call DefRegione(PAR.Pic)
    PAR.Printing = False
    PAR.AllBlack = False
  End If
  '
  Select Case ActualParGroup
    Case "OEM1_MACCHINA48": Call Disegna_ZONA_LAVORO_BS5L(Stampante)
    Case "OEM0_CICLI48":    Call Disegna_ZONA_LAVORO_BS5L(Stampante)
  End Select
  '
  If Stampante Then PAR.Su.SetFocus
  '
Exit Sub

End Sub

Public Sub RidisegnaPagina_BS5S(Pagina As Integer, Optional Stampante As Boolean = False)
'
On Error Resume Next
  '
  Call InizializzaVariabili
  '
  If Stampante Then
    PAR.RegioneX1 = StampaLeft
    PAR.RegioneX2 = StampaLeft + StampaWidth
    PAR.RegioneY1 = StampaTop
    PAR.RegioneY2 = StampaTop + StampaHeight
    PAR.Printing = True
    PAR.AllBlack = True
  Else
    Call DefRegione(PAR.Pic)
    PAR.Printing = False
    PAR.AllBlack = False
  End If
  '
  Select Case ActualParGroup
    Case "OEM1_MACCHINA49": Call Disegna_ZONA_LAVORO_BS5S(Stampante)
    Case "OEM0_CICLI49":    Call Disegna_ZONA_LAVORO_BS5S(Stampante)
  End Select
  '
  If Stampante Then PAR.Su.SetFocus
  '
Exit Sub

End Sub

Public Sub RidisegnaPagina_INGRANAGGI_INTERNI(Pagina As Integer, Optional Stampante As Boolean = False)
'
Dim Z     As Double
Dim Mn    As Double
Dim ALFA  As Double
Dim ALFAt As Double
Dim Beta  As Double
'
Dim DP    As Double
Dim DB    As Double
Dim DE    As Double
Dim DI    As Double
Dim dis   As String
'
On Error Resume Next
  '
  Call InizializzaVariabili
  '
  If Not ParametriOK Then
    If Not Stampante Then PAR.Pic.Cls
    Exit Sub
  End If
  '
  If Stampante Then
    PAR.RegioneX1 = StampaLeft
    PAR.RegioneX2 = StampaLeft + StampaWidth
    PAR.RegioneY1 = StampaTop
    PAR.RegioneY2 = StampaTop + StampaHeight
    PAR.Printing = True
    PAR.AllBlack = True
  Else
    Call DefRegione(PAR.Pic)
    PAR.Printing = False
    PAR.AllBlack = False
  End If
  '
  If (CurrVis = 0) Or Stampante Then
    'LastPage
    Select Case Pagina
      Case 1, 2, 3
        CurrVis = Pagina
      Case 4
        CurrVis = 6
    End Select
  End If
  '
  Select Case ActualParGroup
    Case "OEM1_PROFILO2":   Call Disegna_DenteIngranaggio(Stampante)
    Case "OEM1_PROFILOK2":  Call Disegna_ProfiloK_INTERNI(Stampante)
    Case "OEM1_LAVORO2":    Call Disegna_ZONA_LAVORO_INGRANAGGI_INTERNI(Stampante)
    Case "OEM1_CORRELICA2": If Not Stampante Then PAR.Pic.Cls
                            Call Disegna_CorrElica(Stampante)
  End Select
  '
  If Stampante Then PAR.Su.SetFocus
  '
Exit Sub

End Sub

Public Sub RidisegnaPagina_ROTORI_ESTERNI(Pagina As Integer, Optional Stampante As Boolean = False)
'
Dim k      As Integer
Dim j      As Integer
Dim nCICLI As Integer
Dim SI_RAB As Integer
'
On Error Resume Next
  '
  Call InizializzaVariabili
  '
  If Stampante Then
    PAR.RegioneX1 = StampaLeft
    PAR.RegioneX2 = StampaLeft + StampaWidth
    PAR.RegioneY1 = StampaTop
    PAR.RegioneY2 = StampaTop + StampaHeight
    PAR.Printing = True
    PAR.AllBlack = True
  Else
    Call DefRegione(PAR.Pic)
    PAR.Printing = False
    PAR.AllBlack = False
  End If
  '
  'CONTROLLO SE E' STATA DEFINITA LA TABELLA OEM0_CORRETTORI71 PER IL PROFILO RAB
  SI_RAB = 0
  For j = 0 To UBound(Tabella1.Pagine)
    If (Tabella1.Pagine(j).Nomegruppo = "OEM0_CORRETTORI71") Then
      SI_RAB = 1: Exit For
    End If
  Next j
  '
  nCICLI = GetInfo("Configurazione", "MAXNumeroCicli", Path_LAVORAZIONE_INI)
  '
  k = 0
  If (ActualParGroup = "OEM1_LAVORO71") And (nCICLI <> 3) Then k = 1
  If (ActualParGroup = "OEM0_CICLI71") Then k = 1
  If (k = 1) Then Call Disegna_ZONA_LAVORO_ROTORI_ESTERNI(Stampante)
  '
  k = 0
  If (ActualParGroup = "OEM1_LAVORO71") And (nCICLI = 3) Then k = 1
  If (ActualParGroup = "OEM1_PROFILO71") Then k = 1
  If (ActualParGroup = "OEM0_CORRETTORI71") Then k = 1
  If (ActualParGroup = "INP1_ROTORI_ESTERNI") Then k = 1
  If (k = 1) Then Call Disegna_ROTORI_ESTERNI(Stampante)
  '
  If (ActualParGroup = "OEM0_CICLIB71") Then
    If (SI_RAB = 0) Then
      Call Disegna_ROTORI_ESTERNI(Stampante)
    Else
      Call Disegna_ZONA_LAVORO_ROTORI_ESTERNI(Stampante)
    End If
  End If
  If Stampante Then PAR.Su.SetFocus
  '
Exit Sub

End Sub

Public Sub RidisegnaPagina_VITI_ESTERNE(Pagina As Integer, Optional Stampante As Boolean = False)
'
On Error Resume Next
  '
  Call InizializzaVariabili
  '
  If Stampante Then
    PAR.RegioneX1 = StampaLeft
    PAR.RegioneX2 = StampaLeft + StampaWidth
    PAR.RegioneY1 = StampaTop
    PAR.RegioneY2 = StampaTop + StampaHeight
    PAR.Printing = True
    PAR.AllBlack = True
  Else
    Call DefRegione(PAR.Pic)
    PAR.Printing = False
    PAR.AllBlack = False
  End If
  '
  Select Case ActualParGroup
    Case "OEM1_MOLA4":    WRITE_DIALOG "DISEGNA MOLA"
    Case "OEM1_PROFILO4":    Call Disegna_ZONA_LAVORO_VITI_ESTERNE(Stampante)
    Case "OEM1_LAVORO4":     Call Disegna_ZONA_LAVORO_VITI_ESTERNE(Stampante)
    Case "OEM1_CORRELICA4":  Call Disegna_ZONA_LAVORO_VITI_ESTERNE(Stampante)
    Case "OEM1_MOLAB4":      Call Disegna_ZONA_LAVORO_VITI_ESTERNE(Stampante)
  End Select
  '
  If Stampante Then PAR.Su.SetFocus
  '
Exit Sub

End Sub

Public Sub RidisegnaPagina_VITIV_ESTERNE(Pagina As Integer, Optional Stampante As Boolean = False)
'
On Error Resume Next
  '
  Call InizializzaVariabili
  '
  If Stampante Then
    PAR.RegioneX1 = StampaLeft
    PAR.RegioneX2 = StampaLeft + StampaWidth
    PAR.RegioneY1 = StampaTop
    PAR.RegioneY2 = StampaTop + StampaHeight
    PAR.Printing = True
    PAR.AllBlack = True
  Else
    Call DefRegione(PAR.Pic)
    PAR.Printing = False
    PAR.AllBlack = False
  End If
  '
  Select Case ActualParGroup
    Case "OEM1_MOLA84":    WRITE_DIALOG "DISEGNA MOLA"
    'Case "OEM1_PROFILO84":    Call Disegna_ZONA_LAVORO_VITI_ESTERNE(Stampante)
    Case "OEM1_LAVORO84":     Call Disegna_ZONA_LAVORO_VITI_ESTERNE(Stampante)
    Case "OEM1_CORRELICA84":  Call Disegna_ZONA_LAVORO_VITI_ESTERNE(Stampante)
    Case "OEM1_MOLAB84":      Call Disegna_ZONA_LAVORO_VITI_ESTERNE(Stampante)
  End Select
  '
  If Stampante Then PAR.Su.SetFocus
  '
Exit Sub

End Sub

Public Sub RidisegnaPagina_DENTATURA(Pagina As Integer, Optional Stampante As Boolean = False)
'
Dim Z     As Double
Dim Mn    As Double
Dim ALFA  As Double
Dim ALFAt As Double
Dim Beta  As Double
Dim PG    As Double
Dim DP    As Double
Dim DB    As Double
Dim DE    As Double
Dim DI    As Double
Dim dis   As String
'
On Error Resume Next
  '
  Call InizializzaVariabili
  '
  If Not ParametriOK Then
    If Not Stampante Then PAR.Pic.Cls
    Exit Sub
  End If
  '
  If Stampante Then
    PAR.RegioneX1 = StampaLeft
    PAR.RegioneX2 = StampaLeft + StampaWidth
    PAR.RegioneY1 = StampaTop
    PAR.RegioneY2 = StampaTop + StampaHeight
    PAR.Printing = True
    PAR.AllBlack = True
  Else
    Call DefRegione(PAR.Pic)
    PAR.Printing = False
    PAR.AllBlack = False
  End If
  '
  If (CurrVis = 0) Or Stampante Then
    'LastPage
    Select Case Pagina
      Case 1, 2, 3:  CurrVis = Pagina
      Case 4:        CurrVis = 6
    End Select
  End If
  '
  Select Case ActualParGroup
    Case "OEM0_CICLI81":      Call Disegna_ZONA_LAVORO_DENTATURA(Stampante)
    Case "OEM1_MACCHINA81":   Call Disegna_ZONA_LAVORO_DENTATURA(Stampante)
    Case "OEM1_PROFILO81":    Call Disegna_DenteIngranaggio_MOLAVITE(Stampante)
    Case "OEM1_MOLA81":       Call Disegna_DenteIngranaggio_MOLAVITE(Stampante)
    Case "OEM1_LAVORO81":     Call Disegna_ZONA_LAVORO_DENTATURA(Stampante)
    Case "OEM1_CORRELICA81":  Call Disegna_CorrElica_DENTATURA(Stampante)
  End Select
  '
  If Stampante Then PAR.Su.SetFocus
  '
Exit Sub

End Sub

Public Sub RidisegnaPagina_MOLAVITE_SG(Pagina As Integer, Optional Stampante As Boolean = False)
'
Dim Z     As Double
Dim Mn    As Double
Dim ALFA  As Double
Dim ALFAt As Double
Dim Beta  As Double
'
Dim DP    As Double
Dim DB    As Double
Dim DE    As Double
Dim DI    As Double
Dim dis   As String
'
On Error Resume Next
  '
  Call InizializzaVariabili
  '
  If Not ParametriOK Then
    If Not Stampante Then PAR.Pic.Cls
    Exit Sub
  End If
  '
  If Stampante Then
    PAR.RegioneX1 = StampaLeft
    PAR.RegioneX2 = StampaLeft + StampaWidth
    PAR.RegioneY1 = StampaTop
    PAR.RegioneY2 = StampaTop + StampaHeight
    PAR.Printing = True
    PAR.AllBlack = True
  Else
    Call DefRegione(PAR.Pic)
    PAR.Printing = False
    PAR.AllBlack = False
  End If
  '
  If (CurrVis = 0) Or Stampante Then
    'LastPage
    Select Case Pagina
      Case 1, 2, 3:  CurrVis = Pagina
      Case 4:        CurrVis = 6
    End Select
  End If
  '
  Select Case ActualParGroup
    Case "OEM1_PROFILO85":    Call Disegna_DenteIngranaggio_MOLAVITE(Stampante)
    Case "OEM1_LAVORO85":     Call Disegna_ZONA_LAVORO_MOLAVITE(Stampante)
    'Case "OEM0_CICLI85":      Call Disegna_ZONA_LAVORO_MOLAVITE(Stampante)
    Case "OEM0_CICLI85":      Call Disegna_CICLI_LAVORO_MOLAVITE(Stampante)
    Case "OEM1_CORRELICA85"
         If Pagina = 4 Then
           Call Disegna_CorrElica_MOLAVITE(Stampante)
         End If
         If Pagina = 5 Then
           Call Disegna_CorrElica_SKIVING(Stampante)
         End If
    'Modifica Contour Dressing
    Case "OEM1_PROFILOK85":   Call Disegna_ProfiloK_MOLAVITE(Stampante)
    Case "OEM1_BIAS85":       Call Disegna_BIAS_MOLAVITE(Pagina, Stampante)
  End Select
  '
  If Stampante Then PAR.Su.SetFocus
  '
Exit Sub

End Sub
Public Sub RidisegnaPagina_MOLAVITEG160(Pagina As Integer, Optional Stampante As Boolean = False)
Dim Z     As Double
Dim Mn    As Double
Dim ALFA  As Double
Dim ALFAt As Double
Dim Beta  As Double
'
Dim DP    As Double
Dim DB    As Double
Dim DE    As Double
Dim DI    As Double
Dim dis   As String
'
On Error Resume Next
  '
  Call InizializzaVariabili
  '
  If Not ParametriOK Then
    If Not Stampante Then PAR.Pic.Cls
    Exit Sub
  End If
  '
  If Stampante Then
    PAR.RegioneX1 = StampaLeft
    PAR.RegioneX2 = StampaLeft + StampaWidth
    PAR.RegioneY1 = StampaTop
    PAR.RegioneY2 = StampaTop + StampaHeight
    PAR.Printing = True
    PAR.AllBlack = True
  Else
    Call DefRegione(PAR.Pic)
    PAR.Printing = False
    PAR.AllBlack = False
  End If
  '
  If (CurrVis = 0) Or Stampante Then
    'LastPage
    Select Case Pagina
      Case 1, 2, 3:  CurrVis = Pagina
      Case 4:        CurrVis = 6
    End Select
  End If
  '
  Select Case ActualParGroup
    Case "OEM1_PROFILO86":    Call Disegna_DenteIngranaggio_MOLAVITE(Stampante)
    Case "OEM1_LAVORO86"
         'verifico Nomegruppo2 invece che Pagina
         Select Case ActualParGroup2
           Case "CORRELICA"
                'Visualizzazione Orizzontale
                'Call Disegna_Correttori_Elica_MV(Stampante)
                'Visualizzazione Verticale
                Call Disegna_Correttori_Elica_MVV(Stampante)
           Case Else
                Call Disegna_ZONA_LAVORO_MOLAVITE(Stampante)
         End Select
    
    Case "OEM0_CICLI86":      Call Disegna_CICLI_LAVORO_MOLAVITE(Stampante)
    Case "OEM1_DIAM86":       Call Disegna_CICLI_PROFILA_MOLAVITE(Stampante)
    'Case "OEM1_CORRELICA85":  Call Disegna_CorrElica_MOLAVITE(Stampante)
    Case "OEM1_CORRELICA86"
          Call Disegna_CorrElica_MOLAVITE_V(Stampante)
    Case "OEM1_RULLO86":      Call Disegna_Rullo_Molavite(Stampante)
    Case "OEM1_MOLA86":       Call Disegna_Mola_MOLAVITE(Stampante)
    
    'Modifica Contour Dressing
    Case "OEM1_PROFILOK86":   Call Disegna_ProfiloK_MOLAVITE(Stampante)
    ''Case "OEM1_BIAS86":       Call Disegna_BIAS_MOLAVITE(Pagina, Stampante)
  End Select
  '
  If Stampante Then PAR.Su.SetFocus
  '
Exit Sub

End Sub


Public Sub RidisegnaPagina_MOLAVITE(Pagina As Integer, Optional Stampante As Boolean = False)
'
Dim Z     As Double
Dim Mn    As Double
Dim ALFA  As Double
Dim ALFAt As Double
Dim Beta  As Double
'
Dim DP    As Double
Dim DB    As Double
Dim DE    As Double
Dim DI    As Double
Dim dis   As String
'
On Error Resume Next
  '
  Call InizializzaVariabili
  '
  If Not ParametriOK Then
    If Not Stampante Then PAR.Pic.Cls
    Exit Sub
  End If
  '
  If Stampante Then
    PAR.RegioneX1 = StampaLeft
    PAR.RegioneX2 = StampaLeft + StampaWidth
    PAR.RegioneY1 = StampaTop
    PAR.RegioneY2 = StampaTop + StampaHeight
    PAR.Printing = True
    PAR.AllBlack = True
  Else
    Call DefRegione(PAR.Pic)
    PAR.Printing = False
    PAR.AllBlack = False
  End If
  '
  If (CurrVis = 0) Or Stampante Then
    'LastPage
    Select Case Pagina
      Case 1, 2, 3:  CurrVis = Pagina
      Case 4:        CurrVis = 6
    End Select
  End If
  '
  Select Case ActualParGroup
    Case "OEM1_PROFILO85":    Call Disegna_DenteIngranaggio_MOLAVITE(Stampante)
    Case "OEM1_LAVORO85"
         'verifico Nomegruppo2 invece che Pagina
         Select Case ActualParGroup2
           Case "CORRELICA"
                'Visualizzazione Orizzontale
                'Call Disegna_Correttori_Elica_MV(Stampante)
                'Visualizzazione Verticale
                Call Disegna_Correttori_Elica_MVV(Stampante)
           Case Else
                Call Disegna_ZONA_LAVORO_MOLAVITE(Stampante)
         End Select
    
    Case "OEM0_CICLI85":      Call Disegna_CICLI_LAVORO_MOLAVITE(Stampante)
    Case "OEM1_DIAM85":       Call Disegna_CICLI_PROFILA_MOLAVITE(Stampante)
    'Case "OEM1_CORRELICA85":  Call Disegna_CorrElica_MOLAVITE(Stampante)
    Case "OEM1_CORRELICA85"
          Call Disegna_CorrElica_MOLAVITE_V(Stampante)
    Case "OEM1_RULLO85":      Call Disegna_Rullo_Molavite(Stampante)
    Case "OEM1_MOLA85":       Call Disegna_Mola_MOLAVITE(Stampante)
    
    'Modifica Contour Dressing
    Case "OEM1_PROFILOK85":   Call Disegna_ProfiloK_MOLAVITE(Stampante)
    Case "OEM1_BIAS85":       Call Disegna_BIAS_MOLAVITE(Pagina, Stampante)
    ''Case "OEM1_HGKINETIC85":  Call Disegna_HGKINETIC_MOLAVITE(Pagina, Stampante)
  End Select
  '
  If Stampante Then PAR.Su.SetFocus
  '
Exit Sub

End Sub

' Fill a region using the current color or brush
'   OBJ can be a form or a control that exposes
'       a device context (eg a picture box)
'   X,Y are given in the current system coordinates
'
' If BORDERCOLOR is specified, fills the area
'   enclosed by a border of that color
' If BORDERCOLOR is omitted, the area is filled with
'   the color now at coordinates (x,y), and any
'   different color is considered to a the border

Sub AreaFill(obj As Object, ByVal X As Long, ByVal Y As Long, ByVal colorCode As Long, Optional BorderColor As Variant)
    
Dim X2 As Long
Dim Y2 As Long
Dim saveFillStyle As Long
Dim saveFillColor As Long
    
    With obj
        ' convert into pixel coordinates
        X2 = .ScaleX(X, .ScaleMode, vbPixels)
        Y2 = .ScaleY(Y, .ScaleMode, vbPixels)
        
        ' save FillStyle and FillColor properties
        saveFillStyle = .FillStyle
        saveFillColor = .FillColor
        ' enforce new properties
        .FillStyle = 0
        .FillColor = colorCode
        
        If IsMissing(BorderColor) Then
            ' get color at given coordinates
            BorderColor = .Point(X, Y)
            ' change all the pixels with that color
            ExtFloodFill .hdc, X2, Y2, BorderColor, FLOODFILLSURFACE
        Else
            ExtFloodFill .hdc, X2, Y2, BorderColor, FLOODFILLBORDER
        End If

        ' restore properties
        .FillStyle = saveFillStyle
        .FillColor = saveFillColor
    End With

End Sub
