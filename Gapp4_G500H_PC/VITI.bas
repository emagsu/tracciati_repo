Attribute VB_Name = "VITI"
Option Explicit

Public Const FileTEMPORANEO = "TEMP.PNT"
Public Const NPE = 51
Public Const vd = 999       'dimensione vettori
'
Public X(vd)            As Double
Public Y(vd)            As Double
Public B(vd)            As Double
Public XP1(vd)          As Double
Public YP1(vd)          As Double
Public Bp1(vd)          As Double
Public XP2(vd)          As Double
Public YP2(vd)          As Double
Public Bp2(vd)          As Double
Public MM(vd)           As Double
Public NFil             As Long   'Numero filetti vite
Public MnormV           As Double 'Modulo normale vite
Public MAssV            As Double 'Modulo assiale vite
Public ApVite           As Double 'Angolo di pressione (Deg.)
Public ApViteRad        As Double 'Angolo di pressione (Rad.)
Public ApNormV          As Double 'Angolo di pressione normale vite (Deg.)
Public ApNormVrad       As Double 'Angolo di pressione normale vite (Rad.)
Public ApAssV           As Double 'Angolo di pressione assiale vite (Deg.)
Public ApAssVRad        As Double 'Angolo di pressione assiale vite (Rad.)
Public DprimV           As Double 'Diametro primitivo vite
Public IncV             As Double
Public DIntV            As Double 'Diametro interno vite
Public SpNormV          As Double 'Spessore normale vite
Public DR               As Double 'Diametro rulli vite
Public QR               As Double 'Quota rulli
Public PnormV           As Double 'Passo vite normale
Public PassV            As Double 'Passo vite assiale
Public HelbV            As Double
Public DbaseV           As Double 'Diametro di base vite
Public TipoVite         As String ' "ZN"  "ZI"  "ZK"  "ZA" "ZC"
Public RmolPrim         As Double
Public EmolPrim         As Double
Public EntMolaVis       As Double
Public Yutens           As Double
Public Xutens           As Double
Public YutProfN         As Double
Public DiaMolEff        As Double
Public DiaMolTeo        As Double
Public RagFiancoMola    As Double
Public BetaIng          As Double
Public BetaIngRad       As Double
Public BetaV            As Double
Public BetaVRad         As Double
Public DiaAttivoPiede   As Double
Public DEXTV            As Double
Public DiaSmusso        As Double
Public RT               As Double
Public RF               As Double
Public DILAV            As Double
Public AngRaccordoTesta As Double
Public Rprim            As Double
Public Rb               As Double
Public Betab            As Double
Public PassoElica       As Double
Public IL               As Double 'Interasse di lavoro mola-vite
Public SONt             As Double
Public Ax               As Double
Public Sx               As Double
Public VX               As Double
Public RX               As Double
Public T0               As Double
Public FI0              As Double
Public XCT              As Double
Public YCT              As Double
Public Np               As Integer
Public p1               As Integer
Public p2               As Integer
Public SAP              As Double
Public BSap             As Double
Public XSap             As Double
Public YSap             As Double
Public EAP              As Double
Public BEap             As Double
Public XEap             As Double
Public YEap             As Double
Public E38V             As Double 'Spessore mola su primitivo
Public E43F1V           As Double 'Addendum fianco 1
Public E43F2V           As Double 'Addendum fianco 2
Public E44F1V           As Double 'Altezza fianco mola (compreso raggio testa) fianco 1
Public E44F2V           As Double 'Altezza fianco mola (compreso raggio testa) fianco 2
Public E45              As Double 'Posizione (%) del punto massimo della cavit�
Public E46              As Double 'Entit� cavit� fianco mola (-  = fianco bombato)
Public BombF1V          As Double 'Cavit� fianco 1 V
Public BombF2V          As Double 'Cavit� fianco 1 V
Public PBF1V            As Double 'Posizione cavit� fianco 1
Public PBF2V            As Double 'Posizione cavit� fianco 2
Public AngMedF1V        As Double 'Angolo mola F1
Public AngMedF2V        As Double 'Angolo mola F2
Public CoordinataXV     As Single
Public CoordinataYV     As Single
Public RaggioFiancoV    As Single
'
Public XFilMetr(200) As Double
Public YFilMetr(200) As Double
Public ApFilMetr(200) As Double
'
Public RR(NPE) As Double
Public AA(NPE) As Double
Public BB(NPE) As Double
Public VV(NPE) As Double
Public TT(NPE) As Double
'
Public iDefSp As Integer
'
Dim Session As IMCDomain

Sub INVIO_POSIZIONE_CONTROPUNTA()
'
Dim NomeTb    As String
Dim DB        As Database
Dim TB1       As Recordset
Dim TB2       As Recordset

Dim k1        As Integer
Dim k2        As Integer
Dim stmp      As String

Dim TIPO_LAVORAZIONE      As String
'
Dim NomePezzo As String
Dim MODALITA As String
'
On Error Resume Next
  '
  'LEGGO IL TIPO DI LAVORAZIONE
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  'LEGGO LA MODALITA DI TRASFERIMENTO DATI
  MODALITA = GetInfo("Configurazione", "MODALITA", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))
  'LETTURA DEL NOME PEZZO
  If (MODALITA = "OFF-LINE") Then
    NomePezzo = GetInfo(TIPO_LAVORAZIONE, "NomePezzoInserimento", Path_LAVORAZIONE_INI)
  Else
    NomePezzo = LEGGI_NomePezzo(TIPO_LAVORAZIONE)
  End If
  'APRO IL DATABASE
  Set DB = OpenDatabase(PathDB, True, False)
  Set TB1 = DB.OpenRecordset("ARCHIVIO_" & TIPO_LAVORAZIONE)
  Set TB2 = DB.OpenRecordset("OEM1_LAVORO4")
  'L'ABILITAZIONE DEL PARAMETRO
  TB2.Index = "INDICE"
  TB2.Seek "=", 21
  If Not TB2.NoMatch Then
    Dim ABILITAZIONE As String * 1
    Dim sDDEparametro As String
    Dim vDDEparametro As String
    'LEGGO ABILITAZIONE PARAMETRO
    ABILITAZIONE = UCase$(Trim$(TB2.Fields("ABILITATO").Value))
    If (ABILITAZIONE = "Y") Then
      'LEGGO ITEM DDE PER SCRITTURA
      sDDEparametro = UCase$(Trim$(TB2.Fields("ITEMDDE_1").Value))
      If Len(sDDEparametro) > 0 Then
        'LEGGO IL VALORE NELL'ARCHIVIO
        TB1.Index = "NOME_PEZZO"
        TB1.Seek "=", NomePezzo
        If Not TB1.NoMatch Then
          stmp = Trim$(TB1.Fields("OEM1_LAVORO").Value)
          'Write_Dialog stmp
          k1 = 1
          For i = 1 To 12
            k1 = InStr(k1, stmp, ";")
            k2 = InStr(k1 + 1, stmp, ";")
            'MsgBox Mid(stmp, k1 + 1, K2 - k1 - 1), vbInformation, STR(i)
            vDDEparametro = Mid(stmp, k1 + 1, k2 - k1 - 1)
            k1 = k2
          Next i
          Call OPC_SCRIVI_DATO(sDDEparametro, vDDEparametro)
        End If
      End If
    End If
  End If
  'CHIUDO IL DATABASE
  TB2.Close
  TB1.Close
  DB.Close
  '
End Sub

Sub VITI_ESTERNE_CALCOLO_CONTROLLO()
'
Dim ControlloOk As Boolean
Dim ret         As Long
Dim MODALITA    As String
'
Dim riga        As String
Dim NomeDIR     As String
'
On Error GoTo errVITI_ESTERNE_CALCOLO_CONTROLLO
  '
  'LEGGO IL PERCORSO PER IL DIRETTORIO DEI CONTROLLI
  riga = GetInfo("VITI_ESTERNE", "PathMIS", Path_LAVORAZIONE_INI): riga = UCase$(Trim$(riga))
  If (Len(riga) <= 0) Then Exit Sub
  '
  'LEGGO LA MODALITA DI TRASFERIMENTO DATI
  MODALITA = GetInfo("Configurazione", "MODALITA", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))
  '
  AngRaccordoTesta = LEP("ANGR")
  PassoElica = LEP("PassoElica")
  DEXTV = LEP("DEXT")
  DprimV = LEP("DprimV")
  '
  TipoVite = UT(LEP_STR("TIPO"))
  '
  NFil = LEP("NPRI")
  MnormV = LEP("MN")
  ApVite = LEP("ALFA")
  BetaV = LEP("BETA")
  '
  If (TipoVite <> "ZA") Then
    ApNormV = ApVite
    ApNormVrad = ApNormV * (PG / 180)
    '
    ApViteRad = ApNormV * (PG / 180)
  Else
    ApAssV = ApVite
    ApAssVRad = ApAssV * (PG / 180)
    '
    ApViteRad = ApVite * (PG / 180)
  End If
  '
  BetaV = Abs(BetaV)
  BetaVRad = BetaV * (PG / 180)
  '
  BetaIng = 90 - BetaV
  BetaIngRad = BetaIng * (PG / 180)
  '
  DIntV = LEP("DINT")
  DEXTV = LEP("DEXT")
  '
  SpNormV = LEP("S0N")
  QR = LEP("QR")
  DR = LEP("DR")
  '
  CoordinataXV = LEP("XC")
  CoordinataYV = LEP("YC")
  RaggioFiancoV = LEP("RC")
  RagFiancoMola = RaggioFiancoV
  '
  If (TipoVite <> "BALL") Then
    'CALCOLO DEL MODULO NORMALE DAL PASSO ELICA
    If (PassoElica <> 0) Then
      MnormV = (PassoElica * Sin(BetaIngRad)) / (PG * NFil)
    End If
    'CALCOLO DEL MODULO NORMALE DAL DIAMETRO PRIMITIVO
    If (DprimV <> 0) Then
      MnormV = DprimV * Cos(BetaIngRad) / NFil
    End If
  Else
    'NON FACCIO CALCOLI SUL MODULO
  End If
  '
  DiaMolTeo = LEP("DTEO")
  DiaMolEff = LEP("DEFF")
  RT = LEP("RT")
  RF = LEP("RF")
  '
  Select Case TipoVite
    '
    Case "ZI"
      ApAssVRad = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      ApAssV = ApAssVRad * (180 / PG)
      Rprim = MnormV * NFil / 2 / Cos(BetaIngRad)          'R primitivo
      Betab = Atn(Tan(BetaIngRad) * Cos(ApAssVRad))        'Elica base
      Rb = Rprim * Cos(ApAssVRad)                          'R base
      DbaseV = 2 * Rb                                      'DBaseV
      DprimV = 2 * Rprim                                   'D prim.
      If (BetaIngRad <> 0) Then
        PassoElica = PG * MnormV * NFil / Sin(BetaIngRad)
      Else
        PassoElica = 0
      End If
      ControlloOk = True
      If (SpNormV > 0) Then
        If (DR > 0) Then ControlloOk = QuotaRulli("W")
      Else
        If (DR > 0) Then ControlloOk = QuotaRulli("E")
      End If
      If (ControlloOk = False) Then Exit Sub
      ControlloOk = CALCOLO_VITE_ZI_CONTROLLO
      '
    Case "ZA"
      Dim ApTrasversale As Double
      ApTrasversale = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      Rprim = MnormV * NFil / 2 / Cos(BetaIngRad)              'R primitivo
      Betab = Atn(Tan(BetaIngRad) * Cos(ApTrasversale))        'Elica base
      Rb = Rprim * Cos(ApTrasversale)                          'R base
      DbaseV = 2 * Rb                                          'DBaseV
      DprimV = 2 * Rprim                                       'D prim.
      If (BetaIngRad <> 0) Then
        PassoElica = PG * MnormV * NFil / Sin(BetaIngRad)
      Else
        PassoElica = 0
      End If
      ControlloOk = True
      If (SpNormV > 0) Then
        If (DR > 0) Then ControlloOk = QuotaRulli("W")
      Else
        If (DR > 0) Then ControlloOk = QuotaRulli("E")
      End If
      If (ControlloOk = False) Then Exit Sub
      ControlloOk = CALCOLO_VITE_ZA_CONTROLLO
      '
    Case "ZK"
      ApAssVRad = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      ApAssV = ApAssVRad * (180 / PG)
      Rprim = MnormV * NFil / 2 / Cos(BetaIngRad)          'R primitivo
      Betab = Atn(Tan(BetaIngRad) * Cos(ApAssVRad))        'Elica base
      Rb = Rprim * Cos(ApAssVRad)                          'R base
      DbaseV = 2 * Rb                                      'DBaseV
      DprimV = 2 * Rprim                                   'D prim.
      If (BetaIngRad <> 0) Then
        PassoElica = PG * MnormV * NFil / Sin(BetaIngRad)
      Else
        PassoElica = 0
      End If
      ControlloOk = True
      If (SpNormV > 0) Then
        If (DR > 0) Then ControlloOk = QuotaRulli("W")
      Else
        If (DR > 0) Then ControlloOk = QuotaRulli("E")
      End If
      If (ControlloOk = False) Then Exit Sub
      ControlloOk = CALCOLO_VITE_ZK_CONTROLLO
      '
    Case "ZC"
      ApAssVRad = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      ApAssV = ApAssVRad * (180 / PG)
      Rprim = MnormV * NFil / 2 / Cos(BetaIngRad)          'R primitivo
      Betab = Atn(Tan(BetaIngRad) * Cos(ApAssVRad))        'Elica base
      Rb = Rprim * Cos(ApAssVRad)                          'R base
      DbaseV = 2 * Rb                                      'DBaseV
      DprimV = 2 * Rprim                                   'D prim.
      If (BetaIngRad <> 0) Then
        PassoElica = PG * MnormV * NFil / Sin(BetaIngRad)
      Else
        PassoElica = 0
      End If
      ControlloOk = True
      If (SpNormV > 0) Then
        If (DR > 0) Then ControlloOk = QuotaRulli("W")
      Else
        If (DR > 0) Then ControlloOk = QuotaRulli("E")
      End If
      If (ControlloOk = False) Then Exit Sub
      ControlloOk = CALCOLO_VITE_ZC_CONTROLLO
      '
    Case "ZN"
      ApAssVRad = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      ApAssV = ApAssVRad * (180 / PG)
      Rprim = MnormV * NFil / 2 / Cos(BetaIngRad)          'R primitivo
      Betab = Atn(Tan(BetaIngRad) * Cos(ApAssVRad))        'Elica base
      Rb = Rprim * Cos(ApAssVRad)                          'R base
      DbaseV = 2 * Rb                                      'DBaseV
      DprimV = 2 * Rprim                                   'D prim.
      If (BetaIngRad <> 0) Then
        PassoElica = PG * MnormV * NFil / Sin(BetaIngRad)
      Else
        PassoElica = 0
      End If
      ControlloOk = True
      If (SpNormV > 0) Then
        If (DR > 0) Then ControlloOk = QuotaRulli("W")
      Else
        If (DR > 0) Then ControlloOk = QuotaRulli("E")
      End If
      If (ControlloOk = False) Then Exit Sub
      ControlloOk = CALCOLO_VITE_ZN_CONTROLLO
      '
    Case "BALL"
      WRITE_DIALOG "EVALUATION NOT POSSIBLE!!!"
      Exit Sub
      '
    Case "STANDARD"
      WRITE_DIALOG "EVALUATION NOT POSSIBLE!!!"
      Exit Sub
      '
    Case "ISO"
      ApAssVRad = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      ApAssV = ApAssVRad * (180 / PG)
      Rprim = MnormV * NFil / 2 / Cos(BetaIngRad)          'R primitivo
      Betab = Atn(Tan(BetaIngRad) * Cos(ApAssVRad))        'Elica base
      Rb = Rprim * Cos(ApAssVRad)                          'R base
      DbaseV = 2 * Rb                                      'DBaseV
      DprimV = 2 * Rprim                                   'D prim.
      ControlloOk = CALCOLO_VITE_ISO_CONTROLLO
      '
        Case "SPIROID"
      WRITE_DIALOG "EVALUATION NOT POSSIBLE!!!"
      Exit Sub
      '
    Case "ARBURG"
      WRITE_DIALOG "EVALUATION NOT POSSIBLE!!!"
      Exit Sub
      '
  End Select
  If (ControlloOk = False) Then Exit Sub
  '
  'INVIO DEL PROFILO ASSIALE NEL DIRETTORIO SUL CNC
  NomeDIR = LEGGI_DIRETTORIO_WKS("VITI_ESTERNE")
  If (Len(NomeDIR) > 0) Then
    Set Session = GetObject("@SinHMIMCDomain.MCDomain")
    Session.CopyNC g_chOemPATH & "\VITE.SPF", "/NC/WKS.DIR/" & NomeDIR & ".WPD/VITE.SPF", MCDOMAIN_COPY_NC
    Set Session = Nothing
    WRITE_DIALOG TipoVite & " -> " & "(Pe=" & frmt(PassoElica, 8) & " Dp=" & frmt(DprimV, 6) & " Db=" & frmt(DbaseV, 6) & ")" & " (VITE.SPF -> CNC)"
  End If
  '
Exit Sub

errVITI_ESTERNE_CALCOLO_CONTROLLO:
  WRITE_DIALOG "Sub VITI_ESTERNE_CALCOLO_CONTROLLO: ERROR -> " & Err
  Resume Next

End Sub
Sub VITIV_ESTERNE_CALCOLO_CONTROLLO()
'
Dim ControlloOk As Boolean
Dim ret         As Long
Dim MODALITA    As String
'
Dim riga        As String
Dim NomeDIR     As String
'
On Error GoTo errVITIV_ESTERNE_CALCOLO_CONTROLLO
  '
  'LEGGO IL PERCORSO PER IL DIRETTORIO DEI CONTROLLI
  riga = GetInfo("VITIV_ESTERNE", "PathMIS", Path_LAVORAZIONE_INI): riga = UCase$(Trim$(riga))
  If (Len(riga) <= 0) Then Exit Sub
  '
  'LEGGO LA MODALITA DI TRASFERIMENTO DATI
  MODALITA = GetInfo("Configurazione", "MODALITA", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))
  '
  AngRaccordoTesta = LEP("ANGR")
  PassoElica = LEP("PassoElica")
  DEXTV = LEP("DEXT")
  DprimV = LEP("DprimV")
  '
  TipoVite = UT(LEP_STR("TIPO"))
  '
  NFil = LEP("NPRI")
  MnormV = LEP("MN")
  ApVite = LEP("ALFA")
  BetaV = LEP("BETA")
  '
  If (TipoVite <> "ZA") Then
    ApNormV = ApVite
    ApNormVrad = ApNormV * (PG / 180)
    '
    ApViteRad = ApNormV * (PG / 180)
  Else
    ApAssV = ApVite
    ApAssVRad = ApAssV * (PG / 180)
    '
    ApViteRad = ApVite * (PG / 180)
  End If
  '
  BetaV = Abs(BetaV)
  BetaVRad = BetaV * (PG / 180)
  '
  BetaIng = 90 - BetaV
  BetaIngRad = BetaIng * (PG / 180)
  '
  DIntV = LEP("DINT")
  DEXTV = LEP("DEXT")
  '
  SpNormV = LEP("S0N")
  QR = LEP("QR")
  DR = LEP("DR")
  '
  CoordinataXV = LEP("XC")
  CoordinataYV = LEP("YC")
  RaggioFiancoV = LEP("RC")
  RagFiancoMola = RaggioFiancoV
  '
  If (TipoVite <> "BALL") Then
    'CALCOLO DEL MODULO NORMALE DAL PASSO ELICA
    If (PassoElica <> 0) Then
      MnormV = (PassoElica * Sin(BetaIngRad)) / (PG * NFil)
    End If
    'CALCOLO DEL MODULO NORMALE DAL DIAMETRO PRIMITIVO
    If (DprimV <> 0) Then
      MnormV = DprimV * Cos(BetaIngRad) / NFil
    End If
  Else
    'NON FACCIO CALCOLI SUL MODULO
  End If
  '
  DiaMolTeo = LEP("DTEO")
  DiaMolEff = LEP("DEFF")
  RT = LEP("RT")
  RF = LEP("RF")
  '
  Select Case TipoVite
    '
    Case "ZI"
      ApAssVRad = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      ApAssV = ApAssVRad * (180 / PG)
      Rprim = MnormV * NFil / 2 / Cos(BetaIngRad)          'R primitivo
      Betab = Atn(Tan(BetaIngRad) * Cos(ApAssVRad))        'Elica base
      Rb = Rprim * Cos(ApAssVRad)                          'R base
      DbaseV = 2 * Rb                                      'DBaseV
      DprimV = 2 * Rprim                                   'D prim.
      If (BetaIngRad <> 0) Then
        PassoElica = PG * MnormV * NFil / Sin(BetaIngRad)
      Else
        PassoElica = 0
      End If
      ControlloOk = True
      If (SpNormV > 0) Then
        If (DR > 0) Then ControlloOk = QuotaRulli("W")
      Else
        If (DR > 0) Then ControlloOk = QuotaRulli("E")
      End If
      If (ControlloOk = False) Then Exit Sub
      ControlloOk = CALCOLO_VITE_ZI_CONTROLLO
      '
    Case "ZA"
      Dim ApTrasversale As Double
      ApTrasversale = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      Rprim = MnormV * NFil / 2 / Cos(BetaIngRad)              'R primitivo
      Betab = Atn(Tan(BetaIngRad) * Cos(ApTrasversale))        'Elica base
      Rb = Rprim * Cos(ApTrasversale)                          'R base
      DbaseV = 2 * Rb                                          'DBaseV
      DprimV = 2 * Rprim                                       'D prim.
      If (BetaIngRad <> 0) Then
        PassoElica = PG * MnormV * NFil / Sin(BetaIngRad)
      Else
        PassoElica = 0
      End If
      ControlloOk = True
      If (SpNormV > 0) Then
        If (DR > 0) Then ControlloOk = QuotaRulli("W")
      Else
        If (DR > 0) Then ControlloOk = QuotaRulli("E")
      End If
      If (ControlloOk = False) Then Exit Sub
      ControlloOk = CALCOLO_VITE_ZA_CONTROLLO
      '
    Case "ZK"
      ApAssVRad = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      ApAssV = ApAssVRad * (180 / PG)
      Rprim = MnormV * NFil / 2 / Cos(BetaIngRad)          'R primitivo
      Betab = Atn(Tan(BetaIngRad) * Cos(ApAssVRad))        'Elica base
      Rb = Rprim * Cos(ApAssVRad)                          'R base
      DbaseV = 2 * Rb                                      'DBaseV
      DprimV = 2 * Rprim                                   'D prim.
      If (BetaIngRad <> 0) Then
        PassoElica = PG * MnormV * NFil / Sin(BetaIngRad)
      Else
        PassoElica = 0
      End If
      ControlloOk = True
      If (SpNormV > 0) Then
        If (DR > 0) Then ControlloOk = QuotaRulli("W")
      Else
        If (DR > 0) Then ControlloOk = QuotaRulli("E")
      End If
      If (ControlloOk = False) Then Exit Sub
      ControlloOk = CALCOLO_VITE_ZK_CONTROLLO
      '
    Case "ZC"
      ApAssVRad = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      ApAssV = ApAssVRad * (180 / PG)
      Rprim = MnormV * NFil / 2 / Cos(BetaIngRad)          'R primitivo
      Betab = Atn(Tan(BetaIngRad) * Cos(ApAssVRad))        'Elica base
      Rb = Rprim * Cos(ApAssVRad)                          'R base
      DbaseV = 2 * Rb                                      'DBaseV
      DprimV = 2 * Rprim                                   'D prim.
      If (BetaIngRad <> 0) Then
        PassoElica = PG * MnormV * NFil / Sin(BetaIngRad)
      Else
        PassoElica = 0
      End If
      ControlloOk = True
      If (SpNormV > 0) Then
        If (DR > 0) Then ControlloOk = QuotaRulli("W")
      Else
        If (DR > 0) Then ControlloOk = QuotaRulli("E")
      End If
      If (ControlloOk = False) Then Exit Sub
      ControlloOk = CALCOLO_VITE_ZC_CONTROLLO
      '
    Case "ZN"
      ApAssVRad = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      ApAssV = ApAssVRad * (180 / PG)
      Rprim = MnormV * NFil / 2 / Cos(BetaIngRad)          'R primitivo
      Betab = Atn(Tan(BetaIngRad) * Cos(ApAssVRad))        'Elica base
      Rb = Rprim * Cos(ApAssVRad)                          'R base
      DbaseV = 2 * Rb                                      'DBaseV
      DprimV = 2 * Rprim                                   'D prim.
      If (BetaIngRad <> 0) Then
        PassoElica = PG * MnormV * NFil / Sin(BetaIngRad)
      Else
        PassoElica = 0
      End If
      ControlloOk = True
      If (SpNormV > 0) Then
        If (DR > 0) Then ControlloOk = QuotaRulli("W")
      Else
        If (DR > 0) Then ControlloOk = QuotaRulli("E")
      End If
      If (ControlloOk = False) Then Exit Sub
      ControlloOk = CALCOLO_VITE_ZN_CONTROLLO
      '
    Case "BALL"
      WRITE_DIALOG "EVALUATION NOT POSSIBLE!!!"
      Exit Sub
      '
    Case "STANDARD"
      WRITE_DIALOG "EVALUATION NOT POSSIBLE!!!"
      Exit Sub
      '
    Case "ISO"
      ApAssVRad = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      ApAssV = ApAssVRad * (180 / PG)
      Rprim = MnormV * NFil / 2 / Cos(BetaIngRad)          'R primitivo
      Betab = Atn(Tan(BetaIngRad) * Cos(ApAssVRad))        'Elica base
      Rb = Rprim * Cos(ApAssVRad)                          'R base
      DbaseV = 2 * Rb                                      'DBaseV
      DprimV = 2 * Rprim                                   'D prim.
      ControlloOk = CALCOLO_VITE_ISO_CONTROLLO
      '
        Case "SPIROID"
      WRITE_DIALOG "EVALUATION NOT POSSIBLE!!!"
      Exit Sub
      '
    Case "ARBURG"
      WRITE_DIALOG "EVALUATION NOT POSSIBLE!!!"
      Exit Sub
      '
  End Select
  If (ControlloOk = False) Then Exit Sub
  '
  'INVIO DEL PROFILO ASSIALE NEL DIRETTORIO SUL CNC
  NomeDIR = LEGGI_DIRETTORIO_WKS("VITIV_ESTERNE")
  If (Len(NomeDIR) > 0) Then
    Set Session = GetObject("@SinHMIMCDomain.MCDomain")
    Session.CopyNC g_chOemPATH & "\VITE.SPF", "/NC/WKS.DIR/" & NomeDIR & ".WPD/VITE.SPF", MCDOMAIN_COPY_NC
    Set Session = Nothing
    WRITE_DIALOG TipoVite & " -> " & "(Pe=" & frmt(PassoElica, 8) & " Dp=" & frmt(DprimV, 6) & " Db=" & frmt(DbaseV, 6) & ")" & " (VITE.SPF -> CNC)"
  End If
  '
Exit Sub

errVITIV_ESTERNE_CALCOLO_CONTROLLO:
  WRITE_DIALOG "Sub VITIV_ESTERNE_CALCOLO_CONTROLLO: ERROR -> " & Err
  Resume Next

End Sub

Public Function CALCOLO_VITE_ZI_CONTROLLO() As Boolean
'
Dim NumeroPuntiFianco As Integer
Dim diaSAP As Double
Dim diaEAP As Double
'
Dim hu1     As Double
Dim PM      As Integer
Dim PmB     As Integer
Dim Bmax    As Double
Dim XX      As Double
Dim MM1     As Double
Dim MM2     As Double
Dim da      As Double
Dim A       As Double
Dim XCF     As Double
Dim YCF     As Double
Dim RFM     As Double
Dim Sonx    As Double
'
Dim E38     As Double
Dim AngMed  As Double
Dim E43     As Double
Dim E44     As Double
'
Dim PB      As Double
Dim BOMB    As Double
Dim Dia_Ext As Double
Dim Apx     As Double
Dim Tangax  As Double
'
Dim stmp    As String
'
Dim TIPO_LAVORAZIONE As String


Const nPr = 10
'
'Const TIPO_LAVORAZIONE = "VITI_ESTERNE"
'
On Error GoTo errCALCOLO_VITE_ZI_CONTROLLO
  '
  'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  '
  diaSAP = LEP("COR[0,78]")
  diaEAP = LEP("COR[0,79]")
  NumeroPuntiFianco = LEP("COR[0,80]")
  '
  CALCOLO_VITE_ZI_CONTROLLO = False
  '
  DiaSmusso = DEXTV
  Dia_Ext = DiaSmusso
  '
  SONt = SpNormV / Cos(BetaIngRad)
  '
  DiaAttivoPiede = DIntV + 2 * RF * (1 - Sin(ApNormVrad))    'SAP
  If DiaAttivoPiede < diaSAP Then DiaAttivoPiede = diaSAP
  If DiaAttivoPiede < DbaseV Then DiaAttivoPiede = DbaseV + 0.1
  '
  'ricerca EAP da Dia_Ext -> DiaSmusso
  RX = Dia_Ext / 2
  Do
    RX = RX - 0.01
    Apx = Acs(Rb / RX)      'ang.press.trasversale
    Tangax = Atn(Tan(Apx) * PassoElica / 2 / PG / RX * Cos(BetaVRad))
    XX = 2 * (RX + RT * (1 - Sin(Tangax)))  'dia est calcolato da EAP
  Loop Until XX < Dia_Ext
  DiaSmusso = RX * 2
  If (diaEAP < DiaSmusso) And (diaEAP > DiaAttivoPiede) Then DiaSmusso = diaEAP
  '
  MM1 = Sqr(DiaAttivoPiede ^ 2 - DbaseV ^ 2) / 2
  MM2 = Sqr(DiaSmusso ^ 2 - DbaseV ^ 2) / 2
  '
  IL = (DiaMolEff + DIntV) / 2       'interasse
  PassoElica = MnormV * PG / Sin(BetaIngRad) * NFil
  '
  If (NumeroPuntiFianco > 1) And (NumeroPuntiFianco <= NPE - 2) Then
    Np = NumeroPuntiFianco
  Else
    Np = NPE - 2
  End If
  '
  p1 = 0
  '
  Dim MMpre As Double
  Dim MMsuc As Double
  Dim iPNT  As Integer
  '
  Dim MODALITA         As String
  '
  'LEGGO LA MODALITA DI TRASFERIMENTO DATI
  MODALITA = GetInfo("Configurazione", "MODALITA", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))
  'LETTURA DEL NOME PEZZO
  If (MODALITA = "OFF-LINE") Then
    stmp = GetInfo(TIPO_LAVORAZIONE, "NomePezzoInserimento", Path_LAVORAZIONE_INI)
  Else
    stmp = LEGGI_NomePezzo(TIPO_LAVORAZIONE)
  End If
  '
  Open g_chOemPATH & "\VITE.SPF" For Output As #1
  Print #1, ";PROFILO ASSIALE " & stmp & " (" & TipoVite & ")"
  Print #1, ";VERSIONE: " & Date
  Print #1, ";FIANCO 1"
  '
  'Calcolo fianco mola
  iPNT = -1
  For i = 0 To Np - 1
    '
    MM(i) = MM1 + (MM2 - MM1) * i / (Np - 1)
    RX = Sqr(Rb ^ 2 + MM(i) ^ 2)
    '
    MMsuc = MM(i)
    '
    GoSub CALCOLO_T0_FI0    'calcolo punto ZI
    '
    'SCRIVO NEL FILE SOLO UN NUMERO FINITO DI PUNTI
    'If (MMsuc - MMpre) >= 0.5 Or (i = 0) Or (i = Np - 1) Then
      '
      iPNT = iPNT + 1
      MMpre = MMsuc
      '
      'RAB
      RR(iPNT) = RX
      AA(iPNT) = 90 - T0 * 180 / PG
      BB(iPNT) = T0 * 180 / PG - FI0 * 180 / PG
      '
      'RVT
      VV(iPNT) = AA(iPNT) / 360 * PassoElica
      TT(iPNT) = Atn(Tan(T0 - FI0) * PassoElica / (2 * PG * RR(iPNT))) * 180 / PG
      stmp = ""
      stmp = stmp & "YY[0," & Format$(iPNT + 1, "###0") & "]=" & frmt(RR(iPNT), 4) & " "
      stmp = stmp & "XX[0," & Format$(iPNT + 1, "###0") & "]=" & frmt(-VV(iPNT), 4) & " "
      stmp = stmp & "TT[0," & Format$(iPNT + 1, "###0") & "]=" & frmt(TT(iPNT), 4) & " "
      Print #1, stmp
      '
    'End If
    '
    Call CALCOLO_PUNTO_MOLA(IL, BetaVRad, PassoElica, RX, T0, FI0, X(i), Y(i), B(i))    'calcolo punto mola Xm,Ym
    '
  Next i
  '
  'CALCOLO SUL PRIMITIVO
  RX = Rprim
  MM(i) = Sqr(RX ^ 2 - Rb ^ 2)
  GoSub CALCOLO_T0_FI0
    '
    'PRIMITIVO FIANCO 1
    'RAB
    RR(NPE) = RX
    AA(NPE) = 90 - T0 * 180 / PG
    BB(NPE) = T0 * 180 / PG - FI0 * 180 / PG
    '
    'RVT
    VV(NPE) = AA(NPE) / 360 * PassoElica
    TT(NPE) = (Tan(T0 - FI0) * PassoElica / (2 * PG * RR(NPE))) * 180 / PG
    '
    stmp = ""
    stmp = stmp & "YY[0,0]=" & frmt(RR(NPE), 4) & " "
    stmp = stmp & "XX[0,0]=" & frmt(-VV(NPE), 4) & " "
    stmp = stmp & "TT[0,0]=" & frmt(TT(NPE), 4) & " "
    Print #1, stmp
    '
  Call CALCOLO_PUNTO_MOLA(IL, BetaVRad, PassoElica, RX, T0, FI0, X(i), Y(i), B(i)) 'calcolo punto mola Xm,Ym
  '
  Print #1, ";"
  Print #1, ";FIANCO 2"
  For i = 0 To iPNT
    stmp = ""
    stmp = stmp & "YY[0," & Format$(51 + i, "###0") & "]=" & frmt(RR(i), 4) & " "
    stmp = stmp & "XX[0," & Format$(51 + i, "###0") & "]=" & frmt(VV(i), 4) & " "
    stmp = stmp & "TT[0," & Format$(51 + i, "###0") & "]=" & frmt(TT(i), 4) & " "
    Print #1, stmp
  Next i
    'PRIMITIVO FIANCO 2
    stmp = ""
    stmp = stmp & "YY[0,50]=" & frmt(RR(NPE), 4) & " "
    stmp = stmp & "XX[0,50]=" & frmt(VV(NPE), 4) & " "
    stmp = stmp & "TT[0,50]=" & frmt(TT(NPE), 4) & " "
    Print #1, stmp
    '
  Print #1, ";"
  Print #1, ";RAGGIO INTERNO ESTERNO E PUNTI"
    stmp = ""
    stmp = stmp & "YY[0,100]=" & frmt(DIntV / 2, 4) & " "
    stmp = stmp & "XX[0,100]=" & frmt(DEXTV / 2, 4) & " "
    stmp = stmp & "TT[0,100]=" & Format(iPNT + 1, "##0") & " "
    Print #1, stmp
    '
  Print #1, ";"
  Print #1, "M17"
  Close #1
  '
  CALCOLO_VITE_ZI_CONTROLLO = True
  '
Exit Function

errCALCOLO_VITE_ZI_CONTROLLO:
  WRITE_DIALOG "SUB: CALCOLO_VITE_ZI_CONTROLLO " & Error(Err)
  Exit Function

CALCOLO_T0_FI0:
Dim InvPrim As Double
Dim ApxT    As Double
Dim InvAx   As Double
  '
  Apx = Acs(Rb / RX)
  '
  InvPrim = Tan(ApAssVRad) - ApAssVRad
  InvAx = Tan(Apx) - Apx
  '
  Sx = (SONt / 2 / Rprim + InvPrim - InvAx) * 2 * RX
  VX = 2 * PG * RX / NFil - Sx
  '
  T0 = PG / 2 - VX / 2 / RX
  FI0 = T0 - Apx
  If T0 < 0 Then FI0 = FI0 - 2 * PG
  If FI0 > PG Then FI0 = FI0 - PG * 2
  If FI0 < -PG Then FI0 = FI0 + PG * 2
  '
Return

End Function

Public Function CALCOLO_VITE_ZA_CONTROLLO() As Boolean
'
Dim NumeroPuntiFianco As Integer
Dim diaSAP As Double
Dim diaEAP As Double
'
Dim Sonx    As Double
'
Dim Dia_Ext As Double
Dim Apx     As Double
Dim Tangax  As Double
'
Dim stmp    As String
Dim TIPO_LAVORAZIONE As String
'
'Const TIPO_LAVORAZIONE = "VITI_ESTERNE"
'
On Error GoTo errCALCOLO_VITE_ZA_CONTROLLO
  '
  'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  '
  diaSAP = LEP("COR[0,78]")
  diaEAP = LEP("COR[0,79]")
  NumeroPuntiFianco = LEP("COR[0,80]")
  '
  CALCOLO_VITE_ZA_CONTROLLO = False
  '
  DiaSmusso = DEXTV
  Dia_Ext = DiaSmusso
  '
  SONt = SpNormV / Cos(BetaIngRad)
  '
  DiaAttivoPiede = DIntV + 2 * RF * (1 - Sin(ApAssVRad))   'SAP
  DiaSmusso = DEXTV - 2 * RT * (1 - Sin(ApAssVRad))        'EAP
  '
  If DiaAttivoPiede < diaSAP Then DiaAttivoPiede = diaSAP
  '
  If (diaEAP < DiaSmusso) And (diaEAP > DiaAttivoPiede) Then DiaSmusso = diaEAP
  '
  'controllo tra DiaAttivoPiede e DiaSmusso
  '
  'IL = (DiaMolEff + DintV) / 2       'interasse
  PassoElica = MnormV * PG / Sin(BetaIngRad) * NFil
  '
  If (NumeroPuntiFianco > 1) And (NumeroPuntiFianco <= NPE - 2) Then
    Np = NumeroPuntiFianco
  Else
    Np = NPE - 2
  End If
  '
  p1 = 0
  '
  Dim MMpre As Double
  Dim MMsuc As Double
  Dim iPNT  As Integer
  '
  Dim MODALITA         As String
  '
  'LEGGO LA MODALITA DI TRASFERIMENTO DATI
  MODALITA = GetInfo("Configurazione", "MODALITA", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))
  'LETTURA DEL NOME PEZZO
  If (MODALITA = "OFF-LINE") Then
    stmp = GetInfo(TIPO_LAVORAZIONE, "NomePezzoInserimento", Path_LAVORAZIONE_INI)
  Else
    stmp = LEGGI_NomePezzo(TIPO_LAVORAZIONE)
  End If
  '
  Open g_chOemPATH & "\VITE.SPF" For Output As #1
  '
  Print #1, ";PROFILO ASSIALE " & stmp & " (" & TipoVite & ")"
  Print #1, ";VERSIONE: " & Date
  Print #1, ";FIANCO 1"
  '
  'Calcolo fianco mola
  iPNT = -1
  For i = 0 To Np - 1
    '
    RX = (DiaAttivoPiede + (DiaSmusso - DiaAttivoPiede) * i / (Np - 1)) / 2
    '
    GoSub CALCOLO_T0_FI0_A    'calcolo punto ZA
    '
    'SCRIVO NEL FILE SOLO UN NUMERO FINITO DI PUNTI
    '
    iPNT = iPNT + 1
    'RAB
    RR(iPNT) = RX
    AA(iPNT) = 90 - T0 * 180 / PG
    '
    'RVT
    VV(iPNT) = AA(iPNT) / 360 * PassoElica
    TT(iPNT) = ApAssV
    '
    stmp = ""
    stmp = stmp & "YY[0," & Format$(iPNT + 1, "###0") & "]=" & frmt(RR(iPNT), 4) & " "
    stmp = stmp & "XX[0," & Format$(iPNT + 1, "###0") & "]=" & frmt(-VV(iPNT), 4) & " "
    stmp = stmp & "TT[0," & Format$(iPNT + 1, "###0") & "]=" & frmt(TT(iPNT), 4) & " "
    Print #1, stmp
    '
  Next i
  '
  'CALCOLO SUL PRIMITIVO
  RX = Rprim
  GoSub CALCOLO_T0_FI0_A
    '
    'PRIMITIVO FIANCO 1
    'RAB
    RR(NPE) = RX
    AA(NPE) = 90 - T0 * 180 / PG
    '
    'RVT
    VV(NPE) = AA(NPE) / 360 * PassoElica
    TT(NPE) = ApAssV
    '
    stmp = ""
    stmp = stmp & "YY[0,0]=" & frmt(RR(NPE), 4) & " "
    stmp = stmp & "XX[0,0]=" & frmt(-VV(NPE), 4) & " "
    stmp = stmp & "TT[0,0]=" & frmt(TT(NPE), 4) & " "
    Print #1, stmp
    '
  Print #1, ";"
  Print #1, ";FIANCO 2"
  For i = 0 To iPNT
    stmp = ""
    stmp = stmp & "YY[0," & Format$(51 + i, "###0") & "]=" & frmt(RR(i), 4) & " "
    stmp = stmp & "XX[0," & Format$(51 + i, "###0") & "]=" & frmt(VV(i), 4) & " "
    stmp = stmp & "TT[0," & Format$(51 + i, "###0") & "]=" & frmt(TT(i), 4) & " "
    Print #1, stmp
  Next i
    'PRIMITIVO FIANCO 2
    stmp = ""
    stmp = stmp & "YY[0,50]=" & frmt(RR(NPE), 4) & " "
    stmp = stmp & "XX[0,50]=" & frmt(VV(NPE), 4) & " "
    stmp = stmp & "TT[0,50]=" & frmt(TT(NPE), 4) & " "
    Print #1, stmp
    '
  Print #1, ";"
  Print #1, ";RAGGIO INTERNO ESTERNO E PUNTI"
    stmp = ""
    stmp = stmp & "YY[0,100]=" & frmt(DIntV / 2, 4) & " "
    stmp = stmp & "XX[0,100]=" & frmt(DEXTV / 2, 4) & " "
    stmp = stmp & "TT[0,100]=" & Format(iPNT + 1, "##0") & " "
    Print #1, stmp
    '
  Print #1, ";"
  Print #1, "M17"
  Close #1
  '
  CALCOLO_VITE_ZA_CONTROLLO = True
  '
Exit Function

errCALCOLO_VITE_ZA_CONTROLLO:
  WRITE_DIALOG "SUB: CALCOLO_VITE_ZA_CONTROLLO " & Error(Err)
  Exit Function

CALCOLO_T0_FI0_A:
  Sonx = SpNormV / Cos(PG / 2 - BetaIngRad)
  Sx = Sonx + (Rprim - RX) * 2 * Tan(ApAssVRad)
  VX = PassoElica / NFil - Sx
  T0 = PG / 2 - FnRad(VX * 180 / PassoElica)
  Ax = Atn((Tan(ApAssVRad) * 2 * PG * RX) / PassoElica)
  FI0 = T0 - Ax: If T0 < 0 Then FI0 = FI0 - 2 * PG
  If FI0 > PG Then FI0 = FI0 - PG * 2
  If FI0 < -PG Then FI0 = FI0 + PG * 2
  '
Return
'
End Function

Public Function CALCOLO_VITE_ZC_CONTROLLO() As Boolean
'
Dim stmp  As String
Dim Ltang As Double
'
Dim NumeroPuntiFianco As Integer
Dim diaSAP As Double
Dim diaEAP As Double
'
Dim XX    As Double
Dim Beta  As Double
Dim RpMol As Double
Dim Xcent As Double
Dim Rcent As Double
Dim Apx   As Double
Dim Yrf   As Double
'
Dim PasNorm       As Double
Dim Rxm           As Double
Dim Exm           As Double
Dim Rayon         As Double
Dim ALFA          As Double
Dim APM           As Double
Dim Ixv           As Double
Dim Eobt          As Double
Dim RmMini        As Double
Dim RMmaxi        As Double
Dim Nbp           As Integer
Dim Dia_Ext       As Double
Dim TIPO_LAVORAZIONE As String

'
'Const TIPO_LAVORAZIONE = "VITI_ESTERNE"
'
On Error GoTo errCALCOLO_VITE_ZC_CONTROLLO
  'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  '
  CALCOLO_VITE_ZC_CONTROLLO = False
  '
  diaSAP = LEP("COR[0,78]")
  diaEAP = LEP("COR[0,79]")
  NumeroPuntiFianco = LEP("COR[0,80]")
  '
  DiaSmusso = DEXTV
  Dia_Ext = DiaSmusso
  '
  'controllo tra DiaAttivoPiede e DiaSmusso
  DiaAttivoPiede = DIntV + 2 * RF * (1 - Sin(ApNormVrad))   'SAP
  DiaSmusso = DEXTV - 2 * RT * (1 - Sin(ApNormVrad))        'EAP
  '
  If DiaAttivoPiede < diaSAP Then DiaAttivoPiede = diaSAP
  If DiaAttivoPiede < DbaseV Then DiaAttivoPiede = DbaseV + 0.1
  '
  If (diaEAP < DiaSmusso) And (diaEAP > DiaAttivoPiede) Then DiaSmusso = diaEAP
  '
  If (NumeroPuntiFianco > 1) And (NumeroPuntiFianco <= NPE - 2) Then
    Np = NumeroPuntiFianco
  Else
    Np = NPE - 2
  End If
  '
  PassoElica = MnormV * PG / Sin(BetaIngRad) * NFil
  '
  RpMol = DiaMolTeo / 2
  PasNorm = PassoElica / NFil * Cos(BetaVRad)
  If Abs(FnRad(BetaV) - FnRad(Asn(PasNorm * NFil / DprimV / PG))) > 0.5 Then
    WRITE_DIALOG "CHECK LEAD, Mn and Dp ! (diaSAP: " & frmt(diaSAP, 4) & ")"
    Exit Function
  End If
  IL = RpMol + DprimV / 2
  Rcent = RpMol - RagFiancoMola * Sin(ApNormVrad)
  '
  ' Ricerca di Xcent sulla mola teorica per ottenere Spessore
  Xcent = RagFiancoMola * Cos(ApNormVrad) - (PasNorm - SpNormV) / 2
  For i = 1 To 10
     Rxm = Rcent + RagFiancoMola * Sin(ApNormVrad)
     Exm = RagFiancoMola * Cos(ApNormVrad) - Xcent
     Call CALCOLO_VITE_DA_MOLA(IL, BetaVRad, PassoElica, Rayon, ALFA, Beta, Ixv, Apx, Exm, Rxm, ApNormVrad, Ltang)
     Eobt = PasNorm - Ixv * PasNorm * NFil / PassoElica * 2
     Xcent = Xcent - (Eobt - SpNormV) / 2
  Next i
  ' Ricerca Diametri attivi di piede e di testa
  Rxm = RpMol
  Do
    Rxm = Rxm + 0.01
    APM = Asn((Rxm - Rcent) / RagFiancoMola)
    Exm = RagFiancoMola * Cos(APM) - Xcent
    Call CALCOLO_VITE_DA_MOLA(IL, BetaVRad, PassoElica, Rayon, ALFA, Beta, Ixv, Apx, Exm, Rxm, APM, Ltang)
    XX = 2 * Rayon
  Loop While XX > DiaAttivoPiede
  RmMini = Rxm      'Rxm per diametro attivo di piede
  '
  Rxm = RpMol
  Do
    Rxm = Rxm - 0.01
    APM = Asn((Rxm - Rcent) / RagFiancoMola)
    Exm = RagFiancoMola * Cos(APM) - Xcent
    Call CALCOLO_VITE_DA_MOLA(IL, BetaVRad, PassoElica, Rayon, ALFA, Beta, Ixv, Apx, Exm, Rxm, APM, Ltang)
    XX = 2 * Rayon
  Loop While XX < DiaSmusso
  RMmaxi = Rxm      'Rxm per diametro attivo di testa
  '
  For i = 0 To Np - 1
    Rxm = RmMini + (RMmaxi - RmMini) * i / (Np - 1)
    APM = Asn((Rxm - Rcent) / RagFiancoMola)
    Exm = RagFiancoMola * Cos(APM) - Xcent
    Call CALCOLO_VITE_DA_MOLA(IL, BetaVRad, PassoElica, Rayon, ALFA, Beta, Ixv, Apx, Exm, Rxm, APM, Ltang)
    RR(i) = Rayon: AA(i) = ALFA: BB(i) = Beta 'profilo vite trasversale
  Next i
  'i = NP  primitivo
  Rxm = RpMol
  APM = Asn((Rxm - Rcent) / RagFiancoMola)
  Exm = RagFiancoMola * Cos(APM) - Xcent
  Call CALCOLO_VITE_DA_MOLA(IL, BetaVRad, PassoElica, Rayon, ALFA, Beta, Ixv, Apx, Exm, Rxm, APM, Ltang)
  RR(i) = Rayon: AA(i) = ALFA: BB(i) = Beta
  '
  Dim MODALITA         As String
  '
  'LEGGO LA MODALITA DI TRASFERIMENTO DATI
  MODALITA = GetInfo("Configurazione", "MODALITA", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))
  'LETTURA DEL NOME PEZZO
  If (MODALITA = "OFF-LINE") Then
    stmp = GetInfo(TIPO_LAVORAZIONE, "NomePezzoInserimento", Path_LAVORAZIONE_INI)
  Else
    stmp = LEGGI_NomePezzo(TIPO_LAVORAZIONE)
  End If
  '
  Open g_chOemPATH & "\VITE.SPF" For Output As #1
  Print #1, ";PROFILO ASSIALE " & stmp & " (" & TipoVite & ")"
  Print #1, ";VERSIONE: " & Date
  Print #1, ";FIANCO 1"
  '
  PassoElica = MnormV * PG / Sin(BetaIngRad) * NFil
  '
  For i = 0 To Np - 1
    'SEZIONE assiale: RVT
    VV(i) = AA(i) / 2 / PG * PassoElica
    TT(i) = Atn(Tan(BB(i)) * PassoElica / (2 * PG * RR(i))) * 180 / PG
    stmp = ""
    stmp = stmp & "YY[0," & Format$(i + 1, "###0") & "]=" & frmt(RR(i), 4) & " "
    stmp = stmp & "XX[0," & Format$(i + 1, "###0") & "]=" & frmt(-VV(i), 4) & " "
    stmp = stmp & "TT[0," & Format$(i + 1, "###0") & "]=" & frmt(TT(i), 4) & " "
    Print #1, stmp
  Next i
  '
  'PRIMITIVO: NP
    VV(i) = AA(i) / 2 / PG * PassoElica
    TT(i) = Atn(Tan(BB(i)) * PassoElica / (2 * PG * RR(i))) * 180 / PG
    '
    stmp = ""
    stmp = stmp & "YY[0,0]=" & frmt(RR(i), 4) & " "
    stmp = stmp & "XX[0,0]=" & frmt(-VV(i), 4) & " "
    stmp = stmp & "TT[0,0]=" & frmt(TT(i), 4) & " "
    Print #1, stmp
    '
  Print #1, ";"
  Print #1, ";FIANCO 2"
  For i = 0 To Np - 1
    stmp = ""
    stmp = stmp & "YY[0," & Format$(51 + i, "###0") & "]=" & frmt(RR(i), 4) & " "
    stmp = stmp & "XX[0," & Format$(51 + i, "###0") & "]=" & frmt(VV(i), 4) & " "
    stmp = stmp & "TT[0," & Format$(51 + i, "###0") & "]=" & frmt(TT(i), 4) & " "
    Print #1, stmp
  Next i
    'PRIMITIVO FIANCO 2
    stmp = ""
    stmp = stmp & "YY[0,50]=" & frmt(RR(i), 4) & " "
    stmp = stmp & "XX[0,50]=" & frmt(VV(i), 4) & " "
    stmp = stmp & "TT[0,50]=" & frmt(TT(i), 4) & " "
    Print #1, stmp
    '
  Print #1, ";"
  Print #1, ";RAGGIO INTERNO ESTERNO E PUNTI"
    stmp = ""
    stmp = stmp & "YY[0,100]=" & frmt(DIntV / 2, 4) & " "
    stmp = stmp & "XX[0,100]=" & frmt(DEXTV / 2, 4) & " "
    stmp = stmp & "TT[0,100]=" & Format(Np, "##0") & " "
    Print #1, stmp
    '
  Print #1, ";"
  Print #1, "M17"
  Close #1
  '
  CALCOLO_VITE_ZC_CONTROLLO = True
  '
Exit Function

errCALCOLO_VITE_ZC_CONTROLLO:
  WRITE_DIALOG "SUB : CALCOLO_VITE_ZC_CONTROLLO " & Error(Err)
  Exit Function

End Function

Public Function CALCOLO_VITE_ZK_CONTROLLO() As Boolean
'
Dim Ltang     As Double
Dim DiaTesta  As Double
'
Dim NumeroPuntiFianco As Integer
Dim diaSAP As Double
Dim diaEAP As Double
'
Dim hu1     As Double
Dim PM      As Integer
Dim PmB     As Integer
Dim Bmax    As Double
Dim XX      As Double
Dim MM1     As Double
Dim MM2     As Double
Dim da      As Double
Dim A       As Double
Dim XCF     As Double
Dim YCF     As Double
Dim RFM     As Double
Dim Sonx    As Double
'
Dim E38     As Double
Dim AngMed  As Double
Dim E43     As Double
Dim E44     As Double
'
Dim PB      As Double
Dim BOMB    As Double
Dim Dia_Ext As Double
Dim Apx     As Double
Dim Tangax  As Double
'
Dim stmp    As String
Dim TIPO_LAVORAZIONE As String
'
Const nPr = 10
'Const TIPO_LAVORAZIONE = "VITI_ESTERNE"
'
On Error GoTo errCALCOLO_VITE_ZK_CONTROLLO
  'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  '
  diaSAP = LEP("COR[0,78]")
  diaEAP = LEP("COR[0,79]")
  NumeroPuntiFianco = LEP("COR[0,80]")
  '
  If (NumeroPuntiFianco > 1) And (NumeroPuntiFianco <= NPE - 2) Then
    Np = NumeroPuntiFianco
  Else
    Np = NPE - 2
  End If
  '
  CALCOLO_VITE_ZK_CONTROLLO = False
  '
  DiaSmusso = DEXTV
  Dia_Ext = DiaSmusso
  '
  SONt = SpNormV / Cos(BetaIngRad)
  '
  DiaAttivoPiede = DIntV + 2 * RF * (1 - Sin(ApNormVrad))    'SAP
  If DiaAttivoPiede < diaSAP Then DiaAttivoPiede = diaSAP
  If DiaAttivoPiede < DbaseV Then DiaAttivoPiede = DbaseV + 0.1
  '
  PassoElica = MnormV * PG / Sin(BetaIngRad) * NFil
  '
  'calcolo fianco mola
  'primitivo mola
  IL = (DIntV + DiaMolTeo) / 2          'considero mola di DiaMolTeo
  i = Np
  X(i) = (PassoElica * Cos(PG / 2 - BetaIngRad) / NFil - SpNormV) / 2
  Y(i) = IL - Rprim
  B(i) = FnGrad(ApNormVrad)
  Call CALCOLO_VITE_DA_MOLA(IL, BetaVRad, PassoElica, RR(i), AA(i), BB(i), XP1(i), Bp1(i), X(i), Y(i), FnRad(B(i)), Ltang)
  'ricerca diametro di testa vite
  '(DiaSmusso ha il significato di diametro esterno di input)
  '(DiaTesta ha il significato di diametro attivo di testa - EAP)
  i = Np - 1
  RX = DiaSmusso / 2 - RT * (1 - Sin(ApNormVrad))
  Y(i) = IL - RX
  X(i) = X(Np) + (RX - Rprim) * Tan(ApNormVrad)
  B(i) = FnGrad(ApNormVrad)
  Call CALCOLO_VITE_DA_MOLA(IL, BetaVRad, PassoElica, RR(i), AA(i), BB(i), XP1(i), Bp1(i), X(i), Y(i), FnRad(B(i)), Ltang)
  If RR(i) < RX Then RR(i) = RR(i) + (RX - RR(i)) * 2
  DiaTesta = RR(i) * 2
  RX = RR(i)
  XX = 0.001           'step Rx
  Do
    RX = RX - XX
    Y(i) = IL - RX
    X(i) = X(Np) + (RX - Rprim) * Tan(ApNormVrad)
    B(i) = FnGrad(ApNormVrad)
    Call CALCOLO_VITE_DA_MOLA(IL, BetaVRad, PassoElica, RR(i), AA(i), BB(i), XP1(i), Bp1(i), X(i), Y(i), FnRad(B(i)), Ltang)
    DiaTesta = RR(i) * 2
  Loop Until DiaTesta < diaEAP + XX
  '
  DiaTesta = Int((RX * 2 + XX) * 1000) / 1000
  '
  'ricerca diametro attivo di piede
  i = 0
  RX = DIntV / 2 + RF * (1 - Sin(ApNormVrad))
  Y(i) = IL - RX
  X(i) = X(Np) + (RX - Rprim) * Tan(ApNormVrad)
  B(i) = FnGrad(ApNormVrad)
  Call CALCOLO_VITE_DA_MOLA(IL, BetaVRad, PassoElica, RR(i), AA(i), BB(i), XP1(i), Bp1(i), X(i), Y(i), FnRad(B(i)), Ltang)
  If RR(i) < RX Then RR(i) = RR(i) + (RX - RR(i)) * 2
  DiaAttivoPiede = RR(i) * 2
  RX = RR(i)
  XX = 0.001           'step Rx
  Do
    RX = RX + XX
    Y(i) = IL - RX
    X(i) = X(Np) + (RX - Rprim) * Tan(ApNormVrad)
    B(i) = FnGrad(ApNormVrad)
    Call CALCOLO_VITE_DA_MOLA(IL, BetaVRad, PassoElica, RR(i), AA(i), BB(i), XP1(i), Bp1(i), X(i), Y(i), FnRad(B(i)), Ltang)
    DiaAttivoPiede = RR(i) * 2
  Loop Until DiaAttivoPiede > diaSAP + XX
  '
  DiaAttivoPiede = Int((RX * 2 + XX) * 1000) / 1000
  '
  ' fianco
  XX = (DiaTesta - DiaAttivoPiede) / (Np - 1) / 2
  For i = 0 To Np - 1
    RX = DiaAttivoPiede / 2 + XX * i
    Y(i) = IL - RX
    X(i) = X(Np) + (RX - Rprim) * Tan(ApNormVrad)
    B(i) = FnGrad(ApNormVrad)
    Call CALCOLO_VITE_DA_MOLA(IL, BetaVRad, PassoElica, RR(i), AA(i), BB(i), XP1(i), Bp1(i), X(i), Y(i), FnRad(B(i)), Ltang)
  Next i
  '
  Dim MODALITA         As String
  '
  'LEGGO LA MODALITA DI TRASFERIMENTO DATI
  MODALITA = GetInfo("Configurazione", "MODALITA", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))
  'LETTIRA DEL NOME PEZZO
  If (MODALITA = "OFF-LINE") Then
    stmp = GetInfo(TIPO_LAVORAZIONE, "NomePezzoInserimento", Path_LAVORAZIONE_INI)
  Else
    stmp = LEGGI_NomePezzo(TIPO_LAVORAZIONE)
  End If
  '
  Open g_chOemPATH & "\VITE.SPF" For Output As #1
  Print #1, ";PROFILO ASSIALE " & stmp & " (" & TipoVite & ")"
  Print #1, ";VERSIONE: " & Date
  Print #1, ";FIANCO 1"
  '
  For i = 0 To Np - 1
    '
    'sezione assiale: RVT
    VV(i) = AA(i) / 2 / PG * PassoElica
    TT(i) = Atn(Tan(BB(i)) * PassoElica / (2 * PG * RR(i))) * 180 / PG
    '
    stmp = ""
    stmp = stmp & "YY[0," & Format$(i + 1, "###0") & "]=" & frmt(RR(i), 4) & " "
    stmp = stmp & "XX[0," & Format$(i + 1, "###0") & "]=" & frmt(-VV(i), 4) & " "
    stmp = stmp & "TT[0," & Format$(i + 1, "###0") & "]=" & frmt(TT(i), 4) & " "
    Print #1, stmp
    '
  Next i
  '
  'PRIMITIVO: NP
    VV(i) = AA(i) / 2 / PG * PassoElica
    TT(i) = Atn(Tan(BB(i)) * PassoElica / (2 * PG * RR(i))) * 180 / PG
    '
    stmp = ""
    stmp = stmp & "YY[0,0]=" & frmt(RR(i), 4) & " "
    stmp = stmp & "XX[0,0]=" & frmt(-VV(i), 4) & " "
    stmp = stmp & "TT[0,0]=" & frmt(TT(i), 4) & " "
    Print #1, stmp
    '
  Print #1, ";"
  Print #1, ";FIANCO 2"
  For i = 0 To Np - 1
    stmp = ""
    stmp = stmp & "YY[0," & Format$(51 + i, "###0") & "]=" & frmt(RR(i), 4) & " "
    stmp = stmp & "XX[0," & Format$(51 + i, "###0") & "]=" & frmt(VV(i), 4) & " "
    stmp = stmp & "TT[0," & Format$(51 + i, "###0") & "]=" & frmt(TT(i), 4) & " "
    Print #1, stmp
  Next i
    'PRIMITIVO FIANCO 2
    stmp = ""
    stmp = stmp & "YY[0,50]=" & frmt(RR(i), 4) & " "
    stmp = stmp & "XX[0,50]=" & frmt(VV(i), 4) & " "
    stmp = stmp & "TT[0,50]=" & frmt(TT(i), 4) & " "
    Print #1, stmp
    '
  Print #1, ";"
  Print #1, ";RAGGIO INTERNO ESTERNO E PUNTI"
    stmp = ""
    stmp = stmp & "YY[0,100]=" & frmt(DIntV / 2, 4) & " "
    stmp = stmp & "XX[0,100]=" & frmt(DEXTV / 2, 4) & " "
    stmp = stmp & "TT[0,100]=" & Format(Np, "##0") & " "
    Print #1, stmp
    '
  Print #1, ";"
  Print #1, "M17"
  Close #1
  '
  CALCOLO_VITE_ZK_CONTROLLO = True
  '
Exit Function

errCALCOLO_VITE_ZK_CONTROLLO:
  WRITE_DIALOG "SUB: CALCOLO_VITE_ZK_CONTROLLO " & Error(Err)
  Exit Function

End Function

Public Function CALCOLO_VITE_ZN_CONTROLLO() As Boolean
'
Dim NumeroPuntiFianco As Integer
Dim diaSAP As Double
Dim diaEAP As Double
'
Dim MyYStart  As Double
Dim ARGOMENTO As Double
Dim BC        As Double
'
Dim Dia_Ext As Double
Dim Apx     As Double
Dim Tangax  As Double
Dim TIPO_LAVORAZIONE As String
'
Dim stmp    As String
'
'Const TIPO_LAVORAZIONE = "VITI_ESTERNE"
'
On Error GoTo errCALCOLO_VITE_ZN_CONTROLLO
  'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  '
  diaSAP = LEP("COR[0,78]")
  diaEAP = LEP("COR[0,79]")
  NumeroPuntiFianco = LEP("COR[0,80]")
  '
  CALCOLO_VITE_ZN_CONTROLLO = False
  '
  DiaSmusso = DEXTV
  Dia_Ext = DiaSmusso
  '
  SONt = SpNormV / Cos(BetaIngRad)
  '
  DiaAttivoPiede = DIntV + 2 * RF * (1 - Sin(ApNormVrad))   'SAP
  DiaSmusso = DEXTV - 2 * RT * (1 - Sin(ApNormVrad))        'EAP
  '
  If DiaAttivoPiede < diaSAP Then DiaAttivoPiede = diaSAP
  If DiaAttivoPiede < DbaseV Then DiaAttivoPiede = DbaseV + 0.1
  '
  If (diaEAP < DiaSmusso) And (diaEAP > DiaAttivoPiede) Then DiaSmusso = diaEAP
  '
  'controllo tra DiaAttivoPiede e DiaSmusso
  '
  'IL = (DiaMolEff + DintV) / 2       'interasse
  PassoElica = MnormV * PG / Sin(BetaIngRad) * NFil
  '
  If (NumeroPuntiFianco > 1) And (NumeroPuntiFianco <= NPE - 2) Then
    Np = NumeroPuntiFianco
  Else
    Np = NPE - 2
  End If
  '
  Dim MODALITA         As String
  '
  'LEGGO LA MODALITA DI TRASFERIMENTO DATI
  MODALITA = GetInfo("Configurazione", "MODALITA", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))
  'LETTURA DEL NOME PEZZO
  If (MODALITA = "OFF-LINE") Then
    stmp = GetInfo(TIPO_LAVORAZIONE, "NomePezzoInserimento", Path_LAVORAZIONE_INI)
  Else
    stmp = LEGGI_NomePezzo(TIPO_LAVORAZIONE)
  End If
  '
  Open g_chOemPATH & "\VITE.SPF" For Output As #1
  '
  Print #1, ";PROFILO ASSIALE " & stmp & " (" & TipoVite & ")"
  Print #1, ";VERSIONE: " & Date
  Print #1, ";FIANCO 1"
  '
  CALCOLO_VITE_ZN_CONTROLLO = False
  '
  SONt = SpNormV / Cos(BetaIngRad)
  IL = (DiaMolEff + DIntV) / 2       'interasse
  PassoElica = MnormV * PG / Sin(BetaIngRad) * NFil
  '
  Call PROFILO_VITE_ZN_CONTROLLO(Np)
  '
  For i = 0 To Np - 1
    '
    'sezione assiale: RVT
    VV(i) = AA(i) / 2 / PG * PassoElica
    TT(i) = Atn(Tan(BB(i)) * PassoElica / (2 * PG * RR(i))) * 180 / PG
    '
    stmp = ""
    stmp = stmp & "YY[0," & Format$(i + 1, "###0") & "]=" & frmt(RR(i), 4) & " "
    stmp = stmp & "XX[0," & Format$(i + 1, "###0") & "]=" & frmt(-VV(i), 4) & " "
    stmp = stmp & "TT[0," & Format$(i + 1, "###0") & "]=" & frmt(TT(i), 4) & " "
    Print #1, stmp
    '
  Next i
  '
  'PRIMITIVO: NP
    VV(i) = AA(i) / 2 / PG * PassoElica
    TT(i) = Atn(Tan(BB(i)) * PassoElica / (2 * PG * RR(i))) * 180 / PG
    '
    stmp = ""
    stmp = stmp & "YY[0,0]=" & frmt(RR(i), 4) & " "
    stmp = stmp & "XX[0,0]=" & frmt(-VV(i), 4) & " "
    stmp = stmp & "TT[0,0]=" & frmt(TT(i), 4) & " "
    Print #1, stmp
    '
  Print #1, ";"
  Print #1, ";FIANCO 2"
  For i = 0 To Np - 1
    stmp = ""
    stmp = stmp & "YY[0," & Format$(51 + i, "###0") & "]=" & frmt(RR(i), 4) & " "
    stmp = stmp & "XX[0," & Format$(51 + i, "###0") & "]=" & frmt(VV(i), 4) & " "
    stmp = stmp & "TT[0," & Format$(51 + i, "###0") & "]=" & frmt(TT(i), 4) & " "
    Print #1, stmp
  Next i
    'PRIMITIVO FIANCO 2
    stmp = ""
    stmp = stmp & "YY[0,50]=" & frmt(RR(i), 4) & " "
    stmp = stmp & "XX[0,50]=" & frmt(VV(i), 4) & " "
    stmp = stmp & "TT[0,50]=" & frmt(TT(i), 4) & " "
    Print #1, stmp
    '
  Print #1, ";"
  Print #1, ";RAGGIO INTERNO ESTERNO E PUNTI"
    stmp = ""
    stmp = stmp & "YY[0,100]=" & frmt(DIntV / 2, 4) & " "
    stmp = stmp & "XX[0,100]=" & frmt(DEXTV / 2, 4) & " "
    stmp = stmp & "TT[0,100]=" & Format(Np, "##0") & " "
    Print #1, stmp
    '
  Print #1, ";"
  Print #1, "M17"
  Close #1
  '
  CALCOLO_VITE_ZN_CONTROLLO = True
  '
Exit Function

errCALCOLO_VITE_ZN_CONTROLLO:
  WRITE_DIALOG "SUB: CALCOLO_VITE_ZN_CONTROLLO " & Error(Err)
  Exit Function
  '
End Function

Public Sub PROFILO_VITE_ZN_CONTROLLO(ByVal Np)
'
Dim Aux     As Double
Dim Aux1    As Double
Dim Alfa2   As Double
Dim PasRx   As Double
Dim PasAlfa As Double
Dim Helx    As Double
Dim Rayon   As Double
Dim ALFA    As Double
Dim Beta    As Double
Dim Hr      As Double
Dim Alfa1   As Double
Dim Xc      As Double
Dim AlfaC   As Double
Dim Iax     As Double
Dim ApAx    As Double
Dim Ltang   As Double
Dim Xutens  As Double
Dim Yutens  As Double
Dim Xutens_Sap  As Double
Dim Yutens_Sap  As Double
Dim Xutens_Eap  As Double
Dim Yutens_Eap  As Double
Dim Dia_Ext As Double
'
On Error Resume Next
  '
  ApNormV = ApVite
  ApNormVrad = ApNormV * (PG / 180)
  ApViteRad = ApNormV * (PG / 180)
  '
  ' BetaV = Abs(BetaV) * 180 / PG
  BetaVRad = BetaV * (PG / 180)
  '
  BetaIng = 90 - BetaV
  BetaIngRad = BetaIng * (PG / 180)
  '
  IncV = Asn(NFil * MnormV / DprimV)
  MAssV = MnormV / Cos(IncV)
  PassV = MAssV * NFil * PG
  '
  ' 1 ) Cerco YutProfN
  Alfa2 = 0.1
  Aux = (PassV / NFil - SpNormV / Cos(IncV)) / 2
  PasAlfa = 0.01
  Do
    Alfa2 = Alfa2 - PasAlfa
    PasAlfa = PasAlfa / 10
    Do
      Alfa2 = Alfa2 + PasAlfa
      Aux1 = DprimV / 2 * Sin(Alfa2) / Tan(IncV) + PassV * Alfa2 / 2 / PG
    Loop While Aux1 < Aux
  Loop While PasAlfa > 0.000001
  Aux = (Aux - PassV * Alfa2 / 2 / PG) / Cos(IncV)
  YutProfN = DprimV / 2 * Cos(Alfa2) - Aux / Tan(ApNormVrad)
  '
  ' 2 ) Calcolo profilo vite : Sap e Eap
  PasRx = 0.01
  Yutens = YutProfN
  Do
    i = 0
    Yutens = Yutens + PasRx
    Xutens = (Yutens - YutProfN) * Tan(ApNormVrad)
    Call VisObtN(Yutens, Xutens, ApNormVrad, IncV, PassV, YP1(i), ALFA, Bp1(i), XP1(i), ApAx)
  Loop While YP1(i) < DiaAttivoPiede / 2
  'SAP : Yutens_SAP, Xutens_SAP
  Yutens_Sap = Yutens: Xutens_Sap = Xutens
  '
  PasRx = 0.01
  Do
    i = i + 1
    Yutens = Yutens + PasRx
    Xutens = (Yutens - YutProfN) * Tan(ApNormVrad)
    Call VisObtN(Yutens, Xutens, ApNormVrad, IncV, PassV, YP1(i), ALFA, Bp1(i), XP1(i), ApAx)
  Loop While YP1(i) < DiaSmusso / 2
  'EAP : Yutens_EAP, Xutens_EAP
  Yutens_Eap = Yutens: Xutens_Eap = Xutens
  'punti fianco
  PasRx = (Yutens_Eap - Yutens_Sap) / (Np - 1)
  For i = 0 To Np - 1
    Yutens = Yutens_Sap + PasRx * i
    Xutens = (Yutens - YutProfN) * Tan(ApNormVrad)
    Call VisObtN(Yutens, Xutens, ApNormVrad, IncV, PassV, YP1(i), ALFA, Bp1(i), XP1(i), ApAx)
    RR(i) = YP1(i)
    AA(i) = ALFA
    BB(i) = Bp1(i)
  Next i
  '
  'primitivo
  i = Np
    Yutens = DprimV / 2
    Xutens = (Yutens - YutProfN) * Tan(ApNormVrad)
    Call VisObtN(Yutens, Xutens, ApNormVrad, IncV, PassV, YP1(i), ALFA, Bp1(i), XP1(i), ApAx)
    RR(i) = YP1(i)
    AA(i) = ALFA
    BB(i) = Bp1(i)
  '
End Sub
'
'RICAVATO DA CALCOLO_VITE_ZK_CONTROLLO
Public Function CALCOLO_VITE_ISO_CONTROLLO() As Boolean
'
Dim Ltang     As Double
Dim DiaTesta  As Double
'
Dim NumeroPuntiFianco As Integer
Dim diaSAP As Double
Dim diaEAP As Double
'
Dim hu1     As Double
Dim PM      As Integer
Dim PmB     As Integer
Dim Bmax    As Double
Dim XX      As Double
Dim MM1     As Double
Dim MM2     As Double
Dim da      As Double
Dim A       As Double
Dim XCF     As Double
Dim YCF     As Double
Dim RFM     As Double
Dim Sonx    As Double
'
Dim E38     As Double
Dim AngMed  As Double
Dim E43     As Double
Dim E44     As Double
'
Dim PB      As Double
Dim BOMB    As Double
Dim Dia_Ext As Double
Dim Apx     As Double
Dim Tangax  As Double
'
Dim stmp    As String
Dim TIPO_LAVORAZIONE As String
'
Const nPr = 10

'Const TIPO_LAVORAZIONE = "VITI_ESTERNE"
'
On Error GoTo errCALCOLO_VITE_ISO_CONTROLLO
  'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  '
  diaSAP = LEP("COR[0,78]")
  diaEAP = LEP("COR[0,79]")
  NumeroPuntiFianco = LEP("COR[0,80]")
  '
  If (NumeroPuntiFianco > 1) And (NumeroPuntiFianco <= NPE - 2) Then
    Np = NumeroPuntiFianco
  Else
    Np = NPE - 2
  End If
  '
  CALCOLO_VITE_ISO_CONTROLLO = False
  '
  DiaSmusso = DEXTV
  Dia_Ext = DiaSmusso
  '
  SONt = SpNormV / Cos(BetaIngRad)
  '
  DiaAttivoPiede = DIntV + 2 * RF * (1 - Sin(ApNormVrad))    'SAP
  If DiaAttivoPiede < diaSAP Then DiaAttivoPiede = diaSAP
  If DiaAttivoPiede < DbaseV Then DiaAttivoPiede = DbaseV + 0.1
  '
  PassoElica = MnormV * PG / Sin(BetaIngRad) * NFil
  '
  'calcolo fianco mola
  'primitivo mola
  IL = (DIntV + DiaMolTeo) / 2          'considero mola di DiaMolTeo
  i = Np
  X(i) = (PassoElica * Cos(PG / 2 - BetaIngRad) / NFil - SpNormV) / 2
  Y(i) = IL - Rprim
  B(i) = FnGrad(ApNormVrad)
  Call CALCOLO_VITE_DA_MOLA(IL, BetaVRad, PassoElica, RR(i), AA(i), BB(i), XP1(i), Bp1(i), X(i), Y(i), FnRad(B(i)), Ltang)
  'ricerca diametro di testa vite
  '(DiaSmusso ha il significato di diametro esterno di input)
  '(DiaTesta ha il significato di diametro attivo di testa - EAP)
  i = Np - 1
  RX = DiaSmusso / 2 - RT * (1 - Sin(ApNormVrad))
  Y(i) = IL - RX
  X(i) = X(Np) + (RX - Rprim) * Tan(ApNormVrad)
  B(i) = FnGrad(ApNormVrad)
  Call CALCOLO_VITE_DA_MOLA(IL, BetaVRad, PassoElica, RR(i), AA(i), BB(i), XP1(i), Bp1(i), X(i), Y(i), FnRad(B(i)), Ltang)
  If RR(i) < RX Then RR(i) = RR(i) + (RX - RR(i)) * 2
  DiaTesta = RR(i) * 2
  RX = RR(i)
  XX = 0.001           'step Rx
  Do
    RX = RX - XX
    Y(i) = IL - RX
    X(i) = X(Np) + (RX - Rprim) * Tan(ApNormVrad)
    B(i) = FnGrad(ApNormVrad)
    Call CALCOLO_VITE_DA_MOLA(IL, BetaVRad, PassoElica, RR(i), AA(i), BB(i), XP1(i), Bp1(i), X(i), Y(i), FnRad(B(i)), Ltang)
    DiaTesta = RR(i) * 2
  Loop Until DiaTesta < diaEAP + XX
  '
  DiaTesta = Int((RX * 2 + XX) * 1000) / 1000
  '
  'ricerca diametro attivo di piede
  i = 0
  RX = DIntV / 2 + RF * (1 - Sin(ApNormVrad))
  Y(i) = IL - RX
  X(i) = X(Np) + (RX - Rprim) * Tan(ApNormVrad)
  B(i) = FnGrad(ApNormVrad)
  Call CALCOLO_VITE_DA_MOLA(IL, BetaVRad, PassoElica, RR(i), AA(i), BB(i), XP1(i), Bp1(i), X(i), Y(i), FnRad(B(i)), Ltang)
  If RR(i) < RX Then RR(i) = RR(i) + (RX - RR(i)) * 2
  DiaAttivoPiede = RR(i) * 2
  RX = RR(i)
  XX = 0.001           'step Rx
  Do
    RX = RX + XX
    Y(i) = IL - RX
    X(i) = X(Np) + (RX - Rprim) * Tan(ApNormVrad)
    B(i) = FnGrad(ApNormVrad)
    Call CALCOLO_VITE_DA_MOLA(IL, BetaVRad, PassoElica, RR(i), AA(i), BB(i), XP1(i), Bp1(i), X(i), Y(i), FnRad(B(i)), Ltang)
    DiaAttivoPiede = RR(i) * 2
  Loop Until DiaAttivoPiede > diaSAP + XX
  '
  DiaAttivoPiede = Int((RX * 2 + XX) * 1000) / 1000
  '
  ' fianco
  XX = (DiaTesta - DiaAttivoPiede) / (Np - 1) / 2
  For i = 0 To Np - 1
    RX = DiaAttivoPiede / 2 + XX * i
    Y(i) = IL - RX
    X(i) = X(Np) + (RX - Rprim) * Tan(ApNormVrad)
    B(i) = FnGrad(ApNormVrad)
    Call CALCOLO_VITE_DA_MOLA(IL, BetaVRad, PassoElica, RR(i), AA(i), BB(i), XP1(i), Bp1(i), X(i), Y(i), FnRad(B(i)), Ltang)
  Next i
  '
  Dim MODALITA         As String
  '
  'LEGGO LA MODALITA DI TRASFERIMENTO DATI
  MODALITA = GetInfo("Configurazione", "MODALITA", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))
  'LETTIRA DEL NOME PEZZO
  If (MODALITA = "OFF-LINE") Then
    stmp = GetInfo(TIPO_LAVORAZIONE, "NomePezzoInserimento", Path_LAVORAZIONE_INI)
  Else
    stmp = LEGGI_NomePezzo(TIPO_LAVORAZIONE)
  End If
  '
  Open g_chOemPATH & "\VITE.SPF" For Output As #1
  Print #1, ";PROFILO ASSIALE " & stmp & " (" & TipoVite & ")"
  Print #1, ";VERSIONE: " & Date
  Print #1, ";FIANCO 1"
  '
  For i = 0 To Np - 1
    '
    'sezione assiale: RVT
    VV(i) = AA(i) / 2 / PG * PassoElica
    TT(i) = Atn(Tan(BB(i)) * PassoElica / (2 * PG * RR(i))) * 180 / PG
    '
    stmp = ""
    stmp = stmp & "YY[0," & Format$(i + 1, "###0") & "]=" & frmt(RR(i), 4) & " "
    stmp = stmp & "XX[0," & Format$(i + 1, "###0") & "]=" & frmt(-VV(i), 4) & " "
    stmp = stmp & "TT[0," & Format$(i + 1, "###0") & "]=" & frmt(TT(i), 4) & " "
    Print #1, stmp
    '
  Next i
  '
  'PRIMITIVO: NP
    VV(i) = AA(i) / 2 / PG * PassoElica
    TT(i) = Atn(Tan(BB(i)) * PassoElica / (2 * PG * RR(i))) * 180 / PG
    '
    stmp = ""
    stmp = stmp & "YY[0,0]=" & frmt(RR(i), 4) & " "
    stmp = stmp & "XX[0,0]=" & frmt(-VV(i), 4) & " "
    stmp = stmp & "TT[0,0]=" & frmt(TT(i), 4) & " "
    Print #1, stmp
    '
  Print #1, ";"
  Print #1, ";FIANCO 2"
  For i = 0 To Np - 1
    stmp = ""
    stmp = stmp & "YY[0," & Format$(51 + i, "###0") & "]=" & frmt(RR(i), 4) & " "
    stmp = stmp & "XX[0," & Format$(51 + i, "###0") & "]=" & frmt(VV(i), 4) & " "
    stmp = stmp & "TT[0," & Format$(51 + i, "###0") & "]=" & frmt(TT(i), 4) & " "
    Print #1, stmp
  Next i
    'PRIMITIVO FIANCO 2
    stmp = ""
    stmp = stmp & "YY[0,50]=" & frmt(RR(i), 4) & " "
    stmp = stmp & "XX[0,50]=" & frmt(VV(i), 4) & " "
    stmp = stmp & "TT[0,50]=" & frmt(TT(i), 4) & " "
    Print #1, stmp
    '
  Print #1, ";"
  Print #1, ";RAGGIO INTERNO ESTERNO E PUNTI"
    stmp = ""
    stmp = stmp & "YY[0,100]=" & frmt(DIntV / 2, 4) & " "
    stmp = stmp & "XX[0,100]=" & frmt(DEXTV / 2, 4) & " "
    stmp = stmp & "TT[0,100]=" & Format(Np, "##0") & " "
    Print #1, stmp
    '
  Print #1, ";"
  Print #1, "M17"
  Close #1
  '
  CALCOLO_VITE_ISO_CONTROLLO = True
  '
Exit Function

errCALCOLO_VITE_ISO_CONTROLLO:
  WRITE_DIALOG "SUB: CALCOLO_VITE_ISO_CONTROLLO " & Error(Err)
  Exit Function

End Function

Sub SOTTOPROGRAMMA_CAD_SALVARE()
'
Dim DB As Database
Dim TB As Recordset
Dim k  As Integer
'
Dim TIPO_LAVORAZIONE As String
Dim MODALITA         As String
'
Dim NomePezzo As String
'
On Error GoTo errSOTTOPROGRAMMA_CAD_SALVARE
  '
  'LEGGO LA MODALITA DI TRASFERIMENTO DATI
  MODALITA = GetInfo("Configurazione", "MODALITA", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))
  'LEGGO IL TIPO DI LAVORAZIONE
  TIPO_LAVORAZIONE = GetInfo("CONFIGURAZIONE", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  'LEGGO IL NOME DEL PEZZO
  If (MODALITA = "OFF-LINE") Then
    NomePezzo = GetInfo(TIPO_LAVORAZIONE, "NomePezzoInserimento", Path_LAVORAZIONE_INI)
  Else
    NomePezzo = LEGGI_NomePezzo(TIPO_LAVORAZIONE)
  End If
  'APRO IL DATABASE
  Set DB = OpenDatabase(PathDB, True, False)
  'CONTROLLO SE ESISTE UN SOTTOPROGRAMMA DA SALVARE
  'APRO LA TABELLA DI ARCHIVIAZIONE DATI
  Set TB = DB.OpenRecordset("ARCHIVIO_" & TIPO_LAVORAZIONE)
  'RICERCO NELLA TABELLA ARCHIVIO SE ESISTE UNA ARCHIVIAZIONE RELATIVA A NOMEPEZZO
  TB.Index = "NOME_PEZZO"
  TB.Seek "=", NomePezzo
  If TB.NoMatch Then
    WRITE_DIALOG NomePezzo & " not found."
    'CHIUDO IL DATABASE
    TB.Close
  Else
    For k = 0 To TB.Fields.count - 1
      'CREAZIONE DEL SOTTOPROGRAMMA CAD
      If InStr(TB.Fields(k).Name, "SOTTOPROGRAMMA") > 0 Then
        Open g_chOemPATH & "\" & "CAD.SWP" For Output As #1
          If Len(TB.Fields("SOTTOPROGRAMMA").Value) > 0 Then
            Print #1, TB.Fields("SOTTOPROGRAMMA").Value;
          Else
            Print #1, "";
          End If
        Close #1
      End If
    Next k
  End If
  TB.Close
  DB.Close
  'SEGNALAZIONE UTENTE
  WRITE_DIALOG NomeTb & ": OK!!!"
  '
Exit Sub

errSOTTOPROGRAMMA_CAD_SALVARE:
  WRITE_DIALOG "Sub SOTTOPROGRAMMA_CAD_SALVARE: Error -> " & Err
  Exit Sub

End Sub

Sub SOTTOPROGRAMMA_CAD_LETTURA()
'
Dim DB  As Database
Dim TB  As Recordset
Dim k   As Integer
'
Dim TIPO_LAVORAZIONE As String
Dim MODALITA         As String
'
Dim CODICE_PEZZO As String
'
On Error GoTo errSOTTOPROGRAMMA_CAD_LETTURA
  '
  'LETTURA DEL TIPO DI LAVORAZIONE
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  'LEGGO LA MODALITA DI TRASFERIMENTO DATI
  MODALITA = GetInfo("Configurazione", "MODALITA", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))
  'LETTURA DEL NOME PEZZO
  If (MODALITA = "OFF-LINE") Then
    CODICE_PEZZO = GetInfo(TIPO_LAVORAZIONE, "NomePezzoInserimento", Path_LAVORAZIONE_INI)
  Else
    CODICE_PEZZO = LEGGI_NomePezzo(TIPO_LAVORAZIONE)
  End If
  '
  'CREAZIONE DEL FILE CAD DAL CAMPO SOTTOPROGRAMMA
  If Len(CODICE_PEZZO) > 0 Then
    'APRO IL DATABASE
    Set DB = OpenDatabase(PathDB, True, False)
    Set TB = DB.OpenRecordset("ARCHIVIO_VITI_MULTIFILETTO")
    'RICERCO NELLA TABELLA ARCHIVIO SE ESISTE UNA ARCHIVIAZIONE
    TB.Index = "NOME_PEZZO"
    TB.Seek "=", CODICE_PEZZO
    If TB.NoMatch Then
      'RECORD NON TROVATO: NON SI PUO LEGGERE NIENTE
      WRITE_DIALOG CODICE_PEZZO & " not found --> HD"
      TB.Close
      DB.Close
      Exit Sub
    Else
      'RECORD TROVATO: CREO I FILE DI APPOGGIO
      Open g_chOemPATH & "\" & "CAD.TXT" For Output As #1
        If Len(TB.Fields("SOTTOPROGRAMMA").Value) > 0 Then
          Print #1, TB.Fields("SOTTOPROGRAMMA").Value;
        Else
          Print #1, "";
        End If
      Close #1
    End If
    TB.Close
    DB.Close
    WRITE_DIALOG CODICE_PEZZO & ": CAD SUBPROGRAM (read)"
  Else
    WRITE_DIALOG CODICE_PEZZO & " NOT POSSIBLE: Operation aborted !!!"
  End If
  '
Exit Sub

errSOTTOPROGRAMMA_CAD_LETTURA:
  WRITE_DIALOG "Sub SOTTOPROGRAMMA_CAD_LETTURA: Error -> " & Err
  Exit Sub

End Sub

Sub SOTTOPROGRAMMA_CAD_SCRITTURA()
'
Dim DB  As Database
Dim TB  As Recordset
Dim k   As Integer
'
Dim TIPO_LAVORAZIONE As String
Dim MODALITA         As String
Dim NomePezzo        As String
Dim COMMENTO         As String
'
On Error GoTo errSOTTOPROGRAMMA_CAD_SCRITTURA
  '
  'LETTURA DEL TIPO DI LAVORAZIONE
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  '
  COMMENTO = OEMX.OEM1txtHELP.Text
  Do
    If Right$(COMMENTO, 1) = Chr$(10) Or Right$(COMMENTO, 1) = Chr$(13) Then
      COMMENTO = Left$(COMMENTO, Len(COMMENTO) - 1)
    Else
      Exit Do
    End If
  Loop
  'SALVO NEL FILE TEMPORANEO
  Open g_chOemPATH & "\" & "CAD.TXT" For Output As #1
    Print #1, Trim$(COMMENTO);
  Close #1
  WRITE_DIALOG "CAD SUBPROGRAM --> temporary file"
  '
  'LEGGO LA MODALITA DI TRASFERIMENTO DATI
  MODALITA = GetInfo("Configurazione", "MODALITA", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))
  'LETTURA DEL NOME PEZZO
  If (MODALITA <> "OFF-LINE") Then
    NomePezzo = LEGGI_NomePezzo(TIPO_LAVORAZIONE)
  Else
    NomePezzo = GetInfo(TIPO_LAVORAZIONE, "NomePezzoInserimento", Path_LAVORAZIONE_INI)
  End If
  '
  If Len(NomePezzo) > 0 Then
    '
    'APRO IL DATABASE
    Set DB = OpenDatabase(PathDB, True, False)
    Set TB = DB.OpenRecordset("ARCHIVIO_" & TIPO_LAVORAZIONE)
    'RICERCO NELLA TABELLA ARCHIVIO SE ESISTE UNA ARCHIVIAZIONE
    TB.Index = "NOME_PEZZO"
    TB.Seek "=", NomePezzo
    If TB.NoMatch Then
      'AGGIUNGE UN NUOVO ELEMENTO
      'TB.AddNew
      WRITE_DIALOG NomePezzo & " not found --> HD"
      TB.Close
      DB.Close
      Exit Sub
    Else
      'AGGIORNA UN VECCHIO ELEMENTO
      TB.Edit
    End If
    TB.Fields("NOME_PEZZO").Value = NomePezzo
    TB.Fields("DATA_ARCHIVIAZIONE").Value = Now
    TB.Fields("SOTTOPROGRAMMA").Value = Trim$(COMMENTO)
    TB.Update
    TB.Close
    DB.Close
    WRITE_DIALOG NomePezzo & ": CAD SUBPROGRAM (updated)"
  Else
    WRITE_DIALOG NomePezzo & " NOT POSSIBLE: Operation aborted !!!"
  End If
  '
Exit Sub

errSOTTOPROGRAMMA_CAD_SCRITTURA:
  WRITE_DIALOG "Sub SOTTOPROGRAMMA_CAD_SCRITTURA: Error -> " & Err
  Exit Sub

End Sub

Sub SOTTOPROGRAMMA_CAD_AGGIORNA_CAMPO()
'
Dim DB  As Database
Dim TB  As Recordset
'
Dim TIPO_LAVORAZIONE As String
Dim MODALITA         As String
Dim NomePezzo        As String
'
On Error GoTo errSOTTOPROGRAMMA_CAD_AGGIORNA_CAMPO
  '
  'LETTURA DEL TIPO DI LAVORAZIONE
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  '
  'MODALITA DI TRASFERIMENTO DATI
  MODALITA = GetInfo("Configurazione", "MODALITA", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))
  'NOME PEZZO
  If (MODALITA = "OFF-LINE") Then
    NomePezzo = GetInfo(TIPO_LAVORAZIONE, "NomePezzoInserimento", Path_LAVORAZIONE_INI)
  Else
    NomePezzo = LEGGI_NomePezzo(TIPO_LAVORAZIONE)
  End If
  '
  'APRO IL DATABASE
  Set DB = OpenDatabase(PathDB, True, False)
  '
  'CONTROLLO SE ESISTE UN SOTTOPROGRAMMA DA SALVARE
  'APRO LA TABELLA DI ARCHIVIAZIONE DATI
  Set TB = DB.OpenRecordset("ARCHIVIO_VITI_MULTIFILETTO")
  'RICERCO NELLA TABELLA ARCHIVIO SE ESISTE UNA ARCHIVIAZIONE RELATIVA A NOMEPEZZO
  TB.Index = "NOME_PEZZO"
  TB.Seek "=", NomePezzo
  If TB.NoMatch Then
    WRITE_DIALOG NomePezzo & " not found."
    'CHIUDO IL DATABASE
    TB.Close
    Exit Sub
  Else
    'AGGIORNA UN VECCHIO ELEMENTO
    TB.Edit
  End If
  TB.Fields("NOME_PEZZO").Value = NomePezzo
  TB.Fields("DATA_ARCHIVIAZIONE").Value = Now
  TB.Fields("SOTTOPROGRAMMA").Value = ""
  Open g_chOemPATH & "\" & "CAD.TXT" For Input As #1
    TB.Fields("SOTTOPROGRAMMA").Value = Input$(LOF(1), 1)
  Close #1
  TB.Update
  TB.Close
  '
  DB.Close
  '
  WRITE_DIALOG NomePezzo & ": CAD SUBPROGRAM (updated)"
  '
Exit Sub

errSOTTOPROGRAMMA_CAD_AGGIORNA_CAMPO:
  If FreeFile > 0 Then Close
  WRITE_DIALOG "Sub SOTTOPROGRAMMA_CAD_AGGIORNA_CAMPO: Error -> " & Err
  Exit Sub

End Sub

Sub SOTTOPROGRAMMA_CAD_INVIO_VITI_MULTIFILETTO()
'
Dim NomeDIR As String
Dim stmp    As String
Dim k       As Long
Dim XXpre   As Double
Dim YYpre   As Double
Dim XXcor   As Double
Dim YYcor   As Double
Dim XX      As Double
Dim sXX     As String
Dim YY      As Double
Dim sYY     As String
Dim XXc     As Double
Dim sXXc    As String
Dim YYc     As Double
Dim sYYc    As String
Dim Raggio  As Double
Dim COMANDO As String
'
Dim MODALITA      As String
Dim CODICE_PEZZO  As String
Dim TIPO_LAVORAZIONE As String
'
On Error GoTo errSOTTOPROGRAMMA_CAD_INVIO_VITI_MULTIFILETTO
  '
  'LETTURA DEL TIPO DI LAVORAZIONE
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  '
  'MODALITA DI TRASFERIMENTO DATI
  MODALITA = GetInfo("Configurazione", "MODALITA", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))
  'NOME PEZZO
  If (MODALITA = "OFF-LINE") Then
    CODICE_PEZZO = GetInfo(TIPO_LAVORAZIONE, "NomePezzoInserimento", Path_LAVORAZIONE_INI)
  Else
    CODICE_PEZZO = LEGGI_NomePezzo(TIPO_LAVORAZIONE)
  End If
  '
  Open g_chOemPATH & "\" & "CAD.TXT" For Input As #1
  Open g_chOemPATH & "\" & "CAD.SPF" For Output As #2
    Print #2, ";PERCORSO CAD PER 840D"
    Print #2, ";CODICE PEZZO: " & CODICE_PEZZO
    Print #2, ";VERSIONE    : " & Date & " " & Time
    '
    'POSIZIONAMENTO INIZIALE
    Do While Not EOF(1)
      Line Input #1, stmp
      stmp = UCase$(Trim$(stmp))
        If (InStr(stmp, "G0") > 0) Then
          COMANDO = "G0"
          GoSub CORREZIONE_UTENSILE
          k = InStr(stmp, "X")
          If (k > 0) Then
            Call ESTRAI_VALORE(k, stmp, sXX, XXcor)
            COMANDO = COMANDO & " Z=" & frmt(-XXcor, 4)
          Else
            XXcor = XXpre
          End If
          k = InStr(stmp, "Y")
          If (k > 0) Then
            Call ESTRAI_VALORE(k, stmp, sYY, YYcor)
            COMANDO = COMANDO & " X=" & frmt(-YYcor, 4)
          Else
            YYcor = YYpre
          End If
          Print #2, COMANDO
          XXpre = XXcor: YYpre = YYcor
          Exit Do
          '
      ElseIf (InStr(stmp, "G1") > 0) Then
          COMANDO = "G1"
          GoSub CORREZIONE_UTENSILE
          k = InStr(stmp, "X")
          If (k > 0) Then
            Call ESTRAI_VALORE(k, stmp, sXX, XXcor)
            COMANDO = COMANDO & " Z=" & frmt(-XXcor, 4)
          Else
            XXcor = XXpre
          End If
          k = InStr(stmp, "Y")
          If (k > 0) Then
            Call ESTRAI_VALORE(k, stmp, sYY, YYcor)
            COMANDO = COMANDO & " X=" & frmt(-YYcor, 4)
          Else
            YYcor = YYpre
          End If
          Print #2, COMANDO
          XXpre = XXcor: YYpre = YYcor
          Exit Do
          '
      End If
    Loop
    '
    'PERCORSO
    Do While Not EOF(1)
      Line Input #1, stmp
      stmp = UCase$(Trim$(stmp))
      '
        If (InStr(stmp, "G0") > 0) Then
          COMANDO = "G0"
          GoSub CORREZIONE_UTENSILE
          k = InStr(stmp, "X")
          If (k > 0) Then
            Call ESTRAI_VALORE(k, stmp, sXX, XXcor)
            COMANDO = COMANDO & " Z=" & frmt(-XXcor, 4)
          Else
            XXcor = XXpre
          End If
          k = InStr(stmp, "Y")
          If (k > 0) Then
            Call ESTRAI_VALORE(k, stmp, sYY, YYcor)
            COMANDO = COMANDO & " X=" & frmt(-YYcor, 4)
          Else
            YYcor = YYpre
          End If
          Print #2, COMANDO
          XXpre = XXcor: YYpre = YYcor
          '
      ElseIf (InStr(stmp, "G1") > 0) Then
          COMANDO = "G1"
          GoSub CORREZIONE_UTENSILE
          k = InStr(stmp, "X")
          If (k > 0) Then
            Call ESTRAI_VALORE(k, stmp, sXX, XXcor)
            COMANDO = COMANDO & " Z=" & frmt(-XXcor, 4)
          Else
            XXcor = XXpre
          End If
          k = InStr(stmp, "Y")
          If (k > 0) Then
            Call ESTRAI_VALORE(k, stmp, sYY, YYcor)
            COMANDO = COMANDO & " X=" & frmt(-YYcor, 4)
          Else
            YYcor = YYpre
          End If
          Print #2, COMANDO
          XXpre = XXcor: YYpre = YYcor
          '
      ElseIf (InStr(stmp, "G3") > 0) Then
          COMANDO = "G3"
          'Comando = "G2"
          GoSub CORREZIONE_UTENSILE
          k = InStr(stmp, "X")
          If (k > 0) Then Call ESTRAI_VALORE(k, stmp, sXX, XXcor)
          k = InStr(stmp, "Y")
          If (k > 0) Then Call ESTRAI_VALORE(k, stmp, sYY, YYcor)
          k = InStr(stmp, "I")
          If (k > 0) Then Call ESTRAI_VALORE(k, stmp, sXXc, XXc)
          k = InStr(stmp, "J")
          If (k > 0) Then Call ESTRAI_VALORE(k, stmp, sYYc, YYc)
          Raggio = Sqr((XXcor - XXc) ^ 2 + (YYcor - YYc) ^ 2)
          'Comando = Comando & " Z=" & sXX & " X=" & sYY & " I=" & sXXc & " J=" & sYYc
          COMANDO = COMANDO & " Z=" & frmt(-XXcor, 4) & " X=" & frmt(-YYcor, 4) & " CR=" & frmt(Raggio, 4)
          Print #2, COMANDO
          XXpre = XXcor: YYpre = YYcor
          '
      ElseIf (InStr(stmp, "G2") > 0) Then
          COMANDO = "G2"
          'Comando = "G3"
          GoSub CORREZIONE_UTENSILE
          k = InStr(stmp, "X")
          If (k > 0) Then Call ESTRAI_VALORE(k, stmp, sXX, XXcor)
          k = InStr(stmp, "Y")
          If (k > 0) Then Call ESTRAI_VALORE(k, stmp, sYY, YYcor)
          k = InStr(stmp, "I")
          If (k > 0) Then Call ESTRAI_VALORE(k, stmp, sXXc, XXc)
          k = InStr(stmp, "J")
          If (k > 0) Then Call ESTRAI_VALORE(k, stmp, sYYc, YYc)
          Raggio = Sqr((XXcor - XXc) ^ 2 + (YYcor - YYc) ^ 2)
          'Comando = Comando & " Z=" & sXX & " X=" & sYY & " I=" & sXXc & " J=" & sYYc
          COMANDO = COMANDO & " Z=" & frmt(-XXcor, 4) & " X=" & frmt(-YYcor, 4) & " CR=" & frmt(Raggio, 4)
          Print #2, COMANDO
          XXpre = XXcor: YYpre = YYcor
          '
      End If
    Loop
    Print #2, "RET"
  Close #2
  Close #1
  '
  NomeDIR = LEGGI_DIRETTORIO_WKS(TIPO_LAVORAZIONE)
  Set Session = GetObject("@SinHMIMCDomain.MCDomain")
  Session.CopyNC g_chOemPATH & "\" & "CAD.SPF", "/NC/WKS.DIR/" & NomeDIR & ".WPD/" & "CAD.SPF", MCDOMAIN_COPY_NC
  Set Session = Nothing
  '
Exit Sub

errSOTTOPROGRAMMA_CAD_INVIO_VITI_MULTIFILETTO:
  WRITE_DIALOG "SUB : SOTTOPROGRAMMA_CAD_INVIO_VITI_MULTIFILETTO " & Error(Err)
  Exit Sub

CORREZIONE_UTENSILE:
  If (InStr(stmp, "G41") > 0) Then COMANDO = COMANDO & " "
  If (InStr(stmp, "G42") > 0) Then COMANDO = COMANDO & " "
  If (InStr(stmp, "G40") > 0) Then COMANDO = COMANDO & " "
Return

End Sub

Sub MemorisPunto(XX, YY, Ap)
    
  ' Memorise Xx , Yy , Ap   et Incremente NbpTotal
  NbpTotal = NbpTotal + 1
  XFilMetr(NbpTotal) = XX
  YFilMetr(NbpTotal) = YY  '+ ParMola(1).DRotolamento
  ApFilMetr(NbpTotal) = Ap

End Sub

Sub CalcPuntiFianco(Xdep, Ydep, Xfin, Yfin, Ap, Nbp)
'
Dim i As Integer
Dim XX As Double
Dim YY As Double
'
 For i = 2 To Nbp
   YY = Ydep - (Ydep - Yfin) / (Nbp - 1) * (i - 1)
   XX = Xdep - (Xdep - Xfin) / (Nbp - 1) * (i - 1)
   Call MemorisPunto(XX, YY, Ap)
 Next i

End Sub

Sub CalcPuntiRaggio(R, XR, YR, Adep, Afin, Nbp)
'
Dim i As Integer
Dim XX As Double
Dim YY As Double
Dim Aux
  '
  For i = 2 To Nbp  ' le point N.1 est calcule pour la zone precedente
    Aux = (Afin - Adep) / (Nbp - 1) * (i - 1) + Adep
    XX = XR + R * Sin(Aux)
    YY = YR + R * Cos(Aux)
    Call MemorisPunto(XX, YY, PG / 2 - Aux)
  Next i
  '
End Sub

Sub VITI_ESTERNE_CALCOLO_CORQR(CorQR As Double)
'
Dim ControlloOk   As Boolean
Dim CorrezioneQR  As Double
'
On Error GoTo errVITI_ESTERNE_CALCOLO_CORQR
  '
  CorrezioneQR = CorQR
  '
  TipoVite = UT(LEP_STR("TIPO"))
  NFil = LEP("NPRI")
  MnormV = LEP("MN")
  ApVite = LEP("ALFA")
  PassoElica = LEP("PassoElica")
  DprimV = LEP("DprimV")
  BetaV = LEP("BETA")
  '
  If (TipoVite <> "ZA") Then
    ApNormV = ApVite
    ApNormVrad = ApNormV * (PG / 180)
    '
    ApViteRad = ApNormV * (PG / 180)
  Else
    ApAssV = ApVite
    ApAssVRad = ApAssV * (PG / 180)
    '
    ApViteRad = ApVite * (PG / 180)
  End If
  '
  BetaV = Abs(BetaV)
  BetaVRad = BetaV * (PG / 180)
  '
  BetaIng = 90 - BetaV
  BetaIngRad = BetaIng * (PG / 180)
  '
  DIntV = LEP("DINT")
  DEXTV = LEP("DEXT")
  '
  iDefSp = LEP("iDefSp")
  SpNormV = LEP("S0N")
  QR = LEP("QR")
  DR = LEP("DR")
  '
  CoordinataXV = LEP("XC")
  CoordinataYV = LEP("YC")
  RaggioFiancoV = LEP("RC")
  RagFiancoMola = RaggioFiancoV
  '
  If (TipoVite <> "BALL") Then
    'CALCOLO DEL MODULO NORMALE DAL PASSO ELICA
    If (PassoElica <> 0) Then
      MnormV = (PassoElica * Sin(BetaIngRad)) / (PG * NFil)
      Call SP("MN", frmt(MnormV, 8))
    End If
    'CALCOLO DEL MODULO NORMALE DAL DIAMETRO PRIMITIVO
    If (DprimV <> 0) Then
      MnormV = DprimV * Cos(BetaIngRad) / NFil
      Call SP("MN", frmt(MnormV, 8))
    End If
  Else
    'NON FACCIO CALCOLI SUL MODULO
  End If
  '
  DiaMolTeo = LEP("DTEO")
  DiaMolEff = LEP("DEFF")
  AngRaccordoTesta = LEP("ANGR")
  RT = LEP("RT")
  RF = LEP("RF")
  '
  Select Case TipoVite
    '
    Case "ZI"
      ApAssVRad = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      ApAssV = ApAssVRad * (180 / PG)
      Rprim = MnormV * NFil / 2 / Cos(BetaIngRad)          'R primitivo
      Betab = Atn(Tan(BetaIngRad) * Cos(ApAssVRad))        'Elica base
      Rb = Rprim * Cos(ApAssVRad)                          'R base
      DbaseV = 2 * Rb                                      'DBaseV
      DprimV = 2 * Rprim                                   'D prim.
      If (BetaIngRad <> 0) Then
        PassoElica = PG * MnormV * NFil / Sin(BetaIngRad)
      Else
        PassoElica = 0
      End If
      ControlloOk = True
      If (iDefSp = 1) Then  'Input:Spessore dente
        If (SpNormV > 0) Then
          'cal. QR da SON
          If (DR > 0) Then ControlloOk = QuotaRulli("W")
        Else
          'cal. SON da QR
          If (DR > 0) And (QR > 0) Then ControlloOk = CorQuotaRulli("E", CorrezioneQR)
        End If
      Else
          'cal. SON da QR
          If (DR > 0) And (QR > 0) Then ControlloOk = CorQuotaRulli("E", CorrezioneQR)
      End If
      If (ControlloOk = False) Then Exit Sub
      ControlloOk = CALCOLO_VITE_ZI
      '
    Case "ZA"
      Dim ApTrasversale As Double
      ApTrasversale = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      Rprim = MnormV * NFil / 2 / Cos(BetaIngRad)              'R primitivo
      Betab = Atn(Tan(BetaIngRad) * Cos(ApTrasversale))        'Elica base
      Rb = Rprim * Cos(ApTrasversale)                          'R base
      DbaseV = 2 * Rb                                          'DBaseV
      DprimV = 2 * Rprim                                       'D prim.
      If (BetaIngRad <> 0) Then
        PassoElica = PG * MnormV * NFil / Sin(BetaIngRad)
      Else
        PassoElica = 0
      End If
      ControlloOk = True
      If (iDefSp = 1) Then  'Input:Spessore dente
        If (SpNormV > 0) Then
          'cal. QR da SON
          If (DR > 0) Then ControlloOk = QuotaRulli("W")
        Else
          'cal. SON da QR
          If (DR > 0) And (QR > 0) Then ControlloOk = CorQuotaRulli("E", CorrezioneQR)
        End If
      Else
          'cal. SON da QR
          If (DR > 0) And (QR > 0) Then ControlloOk = CorQuotaRulli("E", CorrezioneQR)
      End If
      If (ControlloOk = False) Then Exit Sub
      ControlloOk = CALCOLO_VITE_ZA
      '
    Case "ZK"
      ApAssVRad = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      ApAssV = ApAssVRad * (180 / PG)
      Rprim = MnormV * NFil / 2 / Cos(BetaIngRad)          'R primitivo
      Betab = Atn(Tan(BetaIngRad) * Cos(ApAssVRad))        'Elica base
      Rb = Rprim * Cos(ApAssVRad)                          'R base
      DbaseV = 2 * Rb                                      'DBaseV
      DprimV = 2 * Rprim                                   'D prim.
      If (BetaIngRad <> 0) Then
        PassoElica = PG * MnormV * NFil / Sin(BetaIngRad)
      Else
        PassoElica = 0
      End If
      ControlloOk = True
      If (iDefSp = 1) Then  'Input:Spessore dente
        If (SpNormV > 0) Then
          'cal. QR da SON
          If (DR > 0) Then ControlloOk = QuotaRulli("W")
        Else
          'cal. SON da QR
          If (DR > 0) And (QR > 0) Then ControlloOk = CorQuotaRulli("E", CorrezioneQR)
        End If
      Else
          'cal. SON da QR
          If (DR > 0) And (QR > 0) Then ControlloOk = CorQuotaRulli("E", CorrezioneQR)
      End If
      If (ControlloOk = False) Then Exit Sub
      ControlloOk = CALCOLO_VITE_ZK
      '
    Case "ZC"
      ApAssVRad = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      ApAssV = ApAssVRad * (180 / PG)
      Rprim = MnormV * NFil / 2 / Cos(BetaIngRad)          'R primitivo
      Betab = Atn(Tan(BetaIngRad) * Cos(ApAssVRad))        'Elica base
      Rb = Rprim * Cos(ApAssVRad)                          'R base
      DbaseV = 2 * Rb                                      'DBaseV
      DprimV = 2 * Rprim                                   'D prim.
      If (BetaIngRad <> 0) Then
        PassoElica = PG * MnormV * NFil / Sin(BetaIngRad)
      Else
        PassoElica = 0
      End If
      ControlloOk = True
      If (iDefSp = 1) Then  'Input:Spessore dente
        If (SpNormV > 0) Then
          'cal. QR da SON
          If (DR > 0) Then ControlloOk = QuotaRulli("W")
        Else
          'cal. SON da QR
          If (DR > 0) And (QR > 0) Then ControlloOk = CorQuotaRulli("E", CorrezioneQR)
        End If
      Else
          'cal. SON da QR
          If (DR > 0) And (QR > 0) Then ControlloOk = CorQuotaRulli("E", CorrezioneQR)
      End If
      If (ControlloOk = False) Then Exit Sub
      ControlloOk = CALCOLO_VITE_ZC
      '
    Case "ZN"
      ApAssVRad = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      ApAssV = ApAssVRad * (180 / PG)
      Rprim = MnormV * NFil / 2 / Cos(BetaIngRad)          'R primitivo
      Betab = Atn(Tan(BetaIngRad) * Cos(ApAssVRad))        'Elica base
      Rb = Rprim * Cos(ApAssVRad)                          'R base
      DbaseV = 2 * Rb                                      'DBaseV
      DprimV = 2 * Rprim                                   'D prim.
      If (BetaIngRad <> 0) Then
        PassoElica = PG * MnormV * NFil / Sin(BetaIngRad)
      Else
        PassoElica = 0
      End If
      ControlloOk = True
      If (iDefSp = 1) Then  'Input:Spessore dente
        If (SpNormV > 0) Then
          'cal. QR da SON
          If (DR > 0) Then ControlloOk = QuotaRulli("W")
        Else
          'cal. SON da QR
          If (DR > 0) And (QR > 0) Then ControlloOk = CorQuotaRulli("E", CorrezioneQR)
        End If
      Else
          'cal. SON da QR
          If (DR > 0) And (QR > 0) Then ControlloOk = CorQuotaRulli("E", CorrezioneQR)
      End If
      If (ControlloOk = False) Then Exit Sub
      ControlloOk = CALCOLO_VITE_ZN
      '
    Case "BALL"
      ControlloOk = CALCOLO_VITE_BALL(1)
      '
    Case "STANDARD"
      ApAssVRad = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      ApAssV = ApAssVRad * (180 / PG)
      Rprim = MnormV * NFil / 2 / Cos(BetaIngRad)          'R primitivo
      Betab = Atn(Tan(BetaIngRad) * Cos(ApAssVRad))        'Elica base
      Rb = Rprim * Cos(ApAssVRad)                          'R base
      DbaseV = 2 * Rb                                      'DBaseV
      DprimV = 2 * Rprim                                   'D prim.
      If (BetaIngRad <> 0) Then
        PassoElica = PG * MnormV * NFil / Sin(BetaIngRad)
      Else
        PassoElica = 0
      End If
      ControlloOk = True
      If (iDefSp = 1) Then  'Input:Spessore dente
        If (SpNormV > 0) Then
          'cal. QR da SON
          If (DR > 0) Then ControlloOk = QuotaRulli("W")
        Else
          'cal. SON da QR
          If (DR > 0) And (QR > 0) Then ControlloOk = CorQuotaRulli("E", CorrezioneQR)
        End If
      Else
        'cal. SON da QR
        If (DR > 0) And (QR > 0) Then ControlloOk = CorQuotaRulli("E", CorrezioneQR)
      End If
      '
    Case "ARBURG"
      ControlloOk = CALCOLO_VITE_ARBURG()
      '
  End Select
  If (ControlloOk = False) Then Exit Sub
  '
  Call SP("LARGMOLAPR_G", frmt(E38V, 4))
  Call OEMX.SuOEM1.DisegnaOEM
  '
  WRITE_DIALOG TipoVite & " -> " & "(Pe=" & frmt(PassoElica, 8) & " Dp=" & frmt(DprimV, 6) & " Db=" & frmt(DbaseV, 6) & " Spm=" & frmt(E38V, 4) & ")"
  '
Exit Sub

errVITI_ESTERNE_CALCOLO_CORQR:
  WRITE_DIALOG "Sub VITI_ESTERNE_CALCOLO_CORQR: ERROR -> " & Err
  Exit Sub

End Sub

Sub VITI_ESTERNE_CALCOLO()
'
Dim ControlloOk As Boolean
Dim ret         As Long
Dim MODALITA    As String
'
Dim H3          As Double  'ALTEZZA DENTE
Dim Rtesta      As Double  'RAGGIO DI TESTA
Dim Rfondo      As Double  'RAGGIO DI FONDO
Dim TIPO_MACCHINA As String
'
On Error GoTo errVITI_ESTERNE_CALCOLO
  '
  'LETTURA DELLA MODALITA DI MODIFICA DATI
  MODALITA = GetInfo("Configurazione", "Modalita", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))
  If (MODALITA <> "OFF-LINE") Then WRITE_DIALOG "SOFTKEY DISENABLED !!!": Exit Sub
  '
  TIPO_MACCHINA = GetInfo("Configurazione", "TIPO_MACCHINA", Path_LAVORAZIONE_INI)
  TIPO_MACCHINA = UCase$(Trim$(TIPO_MACCHINA))
  
  AngRaccordoTesta = LEP("ANGR")
  PassoElica = LEP("PassoElica")
  DEXTV = LEP("DEXT")
  DprimV = LEP("DprimV")
  '
  TipoVite = UT(LEP_STR("TIPO"))
  If (TipoVite = "ISO") Then
    Call PROFILO_VITE_ISO(H3, Rtesta, Rfondo)
    Call SP("NPRI", "1")
    Call SP("ALFA", frmt(ApVite, 4))
    Call SP("BETA", frmt(BetaV, 4))
    Call SP("DprimV", frmt(DprimV, 4))
    Call SP("S0N", frmt(SpNormV, 4))
    Call SP("DINT", frmt(DIntV, 4))
    Call SP("RT", frmt(Rtesta, 4))
    Call SP("RF", frmt(Rfondo, 4))
    '
    Call SP("H3", frmt(H3, 4))
    Call SP("DINT", frmt(DIntV, 4))
    Call SP("DIAINT", frmt(DIntV, 4))
    Call SP("R[103]", frmt(DprimV, 4))
    Call SP("SM", frmt(SpNormV, 4))
    Call SP("RTT", frmt(Rtesta, 4))
    Call SP("RFF", frmt(Rfondo, 4))
  End If
  '
  NFil = LEP("NPRI")
  MnormV = LEP("MN")
  ApVite = LEP("ALFA")
  BetaV = LEP("BETA")
  If (BetaV = 0) Then
    If (PassoElica <> 0) And (DprimV <> 0) Then
      BetaIngRad = Atn(PG * DprimV / PassoElica)
      BetaIng = BetaIngRad * (180 / PG)
      BetaV = 90 - BetaIng
      Call SP("BETA", frmt(BetaV, 8))
    End If
  End If
  '
  If (TipoVite <> "ZA") Then
    ApNormV = ApVite
    ApNormVrad = ApNormV * (PG / 180)
    '
    ApViteRad = ApNormV * (PG / 180)
  Else
    ApAssV = ApVite
    ApAssVRad = ApAssV * (PG / 180)
    '
    ApViteRad = ApVite * (PG / 180)
  End If
  '
  BetaV = Abs(BetaV)
  BetaVRad = BetaV * (PG / 180)
  '
  BetaIng = 90 - BetaV
  BetaIngRad = BetaIng * (PG / 180)
  '
  DIntV = LEP("DINT")
  DEXTV = LEP("DEXT")
  '
  iDefSp = LEP("iDefSp")
  SpNormV = LEP("S0N")
  QR = LEP("QR")
  DR = LEP("DR")
  '
  CoordinataXV = LEP("XC")
  CoordinataYV = LEP("YC")
  RaggioFiancoV = LEP("RC")
  RagFiancoMola = RaggioFiancoV
  '
  If (TipoVite <> "BALL") Then
    'CALCOLO DEL MODULO NORMALE DAL PASSO ELICA
    If (PassoElica <> 0) Then
      MnormV = (PassoElica * Sin(BetaIngRad)) / (PG * NFil)
      Call SP("MN", frmt(MnormV, 8))
    End If
    'CALCOLO DEL MODULO NORMALE DAL DIAMETRO PRIMITIVO
    If (DprimV <> 0) Then
      MnormV = DprimV * Cos(BetaIngRad) / NFil
      Call SP("MN", frmt(MnormV, 8))
    End If
  Else
    'NON FACCIO CALCOLI SUL MODULO
  End If
  '
  DiaMolTeo = LEP("DTEO")
  DiaMolEff = LEP("DEFF")
  RT = LEP("RT")
  RF = LEP("RF")
  '
  Call SP("R[34]", "0")
  Call SP("CORQRULLI", "0")
  Select Case TipoVite
    '
    Case "ZI"
      ApAssVRad = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      ApAssV = ApAssVRad * (180 / PG)
      Rprim = MnormV * NFil / 2 / Cos(BetaIngRad)          'R primitivo
      Betab = Atn(Tan(BetaIngRad) * Cos(ApAssVRad))        'Elica base
      Rb = Rprim * Cos(ApAssVRad)                          'R base
      DbaseV = 2 * Rb                                      'DBaseV
      DprimV = 2 * Rprim                                   'D prim.
      If (BetaIngRad <> 0) Then
        PassoElica = PG * MnormV * NFil / Sin(BetaIngRad)
      Else
        PassoElica = 0
      End If
      '
      If (SpNormV = 0) And (DR = 0) And (iDefSp = 1) Then
        Dim TT As Double
        Dim TH As Double
        Dim OD As Double
        OD = LEP("OD") 'Diametro esterno per calibro
        TH = LEP("TH") 'Profondit� dal diametro esterno del calibro
        TT = LEP("TT") 'Spessore misurato col calibro
        If (OD = 0) Or (TT) = 0 Then
          'Calcolo dello spessore dal modulo normale
          SpNormV = PG * MnormV / 2
        Else
          'Calcolo dello spessore dal calibro
          Dim InvPrim     As Double
          Dim Apx         As Double
          Dim ApxT        As Double
          Dim InvAx       As Double
          Dim BetaIngRadx As Double
          Dim RX          As Double
          Dim Sxnt        As Double
          RX = (OD - 2 * TH) / 2
          BetaIngRadx = Atn(PG * 2 * RX / PassoElica)
          Sxnt = TT / Cos(BetaIngRadx)
          Apx = Acs(Rb / RX)
          InvPrim = Tan(ApAssVRad) - ApAssVRad
          InvAx = Tan(Apx) - Apx
          SpNormV = (Sxnt / 2 / RX + InvAx - InvPrim) * 2 * Rprim * Cos(BetaIngRad)
        End If
        Call SP("S0N", frmt(SpNormV, 8))
      End If
      '
      ControlloOk = True
      If (iDefSp = 1) Then  'Input:Spessore dente
        If (SpNormV > 0) Then
          'cal. QR da SON
          If (DR > 0) Then ControlloOk = QuotaRulli("W")
        Else
          'cal. SON da QR
          If (DR > 0) And (QR > 0) Then ControlloOk = QuotaRulli("E")
        End If
      Else
          'cal. SON da QR
          If (DR > 0) And (QR > 0) Then ControlloOk = QuotaRulli("E")
      End If
      If (ControlloOk = False) Then Exit Sub
      ControlloOk = CALCOLO_VITE_ZI
      '
    Case "ZA"
      Dim ApTrasversale As Double
      ApTrasversale = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      Rprim = MnormV * NFil / 2 / Cos(BetaIngRad)              'R primitivo
      Betab = Atn(Tan(BetaIngRad) * Cos(ApTrasversale))        'Elica base
      Rb = Rprim * Cos(ApTrasversale)                          'R base
      DbaseV = 2 * Rb                                          'DBaseV
      DprimV = 2 * Rprim                                       'D prim.
      If (BetaIngRad <> 0) Then
        PassoElica = PG * MnormV * NFil / Sin(BetaIngRad)
      Else
        PassoElica = 0
      End If
      ControlloOk = True
      If (iDefSp = 1) Then  'Input:Spessore dente
        If (SpNormV > 0) Then
          'cal. QR da SON
          If (DR > 0) Then ControlloOk = QuotaRulli("W")
        Else
          'cal. SON da QR
          If (DR > 0) And (QR > 0) Then ControlloOk = QuotaRulli("E")
        End If
      Else
          'cal. SON da QR
          If (DR > 0) And (QR > 0) Then ControlloOk = QuotaRulli("E")
      End If
      If (ControlloOk = False) Then Exit Sub
      ControlloOk = CALCOLO_VITE_ZA
      '
    Case "ZK"
      ApAssVRad = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      ApAssV = ApAssVRad * (180 / PG)
      Rprim = MnormV * NFil / 2 / Cos(BetaIngRad)          'R primitivo
      Betab = Atn(Tan(BetaIngRad) * Cos(ApAssVRad))        'Elica base
      Rb = Rprim * Cos(ApAssVRad)                          'R base
      DbaseV = 2 * Rb                                      'DBaseV
      DprimV = 2 * Rprim                                   'D prim.
      If (BetaIngRad <> 0) Then
        PassoElica = PG * MnormV * NFil / Sin(BetaIngRad)
      Else
        PassoElica = 0
      End If
      ControlloOk = True
      If (iDefSp = 1) Then  'Input:Spessore dente
        If (SpNormV > 0) Then
          'cal. QR da SON
          If (DR > 0) Then ControlloOk = QuotaRulli("W")
        Else
          'cal. SON da QR
          If (DR > 0) And (QR > 0) Then ControlloOk = QuotaRulli("E")
        End If
      Else
          'cal. SON da QR
          If (DR > 0) And (QR > 0) Then ControlloOk = QuotaRulli("E")
      End If
      If (ControlloOk = False) Then Exit Sub
      ControlloOk = CALCOLO_VITE_ZK
      '
    Case "ZC"
      ApAssVRad = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      ApAssV = ApAssVRad * (180 / PG)
      Rprim = MnormV * NFil / 2 / Cos(BetaIngRad)          'R primitivo
      Betab = Atn(Tan(BetaIngRad) * Cos(ApAssVRad))        'Elica base
      Rb = Rprim * Cos(ApAssVRad)                          'R base
      DbaseV = 2 * Rb                                      'DBaseV
      DprimV = 2 * Rprim                                   'D prim.
      If (BetaIngRad <> 0) Then
        PassoElica = PG * MnormV * NFil / Sin(BetaIngRad)
      Else
        PassoElica = 0
      End If
      ControlloOk = True
      If (iDefSp = 1) Then  'Input:Spessore dente
        If (SpNormV > 0) Then
          'cal. QR da SON
          If (DR > 0) Then ControlloOk = QuotaRulli("W")
        Else
          'cal. SON da QR
          If (DR > 0) And (QR > 0) Then ControlloOk = QuotaRulli("E")
        End If
      Else
          'cal. SON da QR
          If (DR > 0) And (QR > 0) Then ControlloOk = QuotaRulli("E")
      End If
      If (ControlloOk = False) Then Exit Sub
      ControlloOk = CALCOLO_VITE_ZC
      '
    Case "ZN"
      ApAssVRad = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      ApAssV = ApAssVRad * (180 / PG)
      Rprim = MnormV * NFil / 2 / Cos(BetaIngRad)          'R primitivo
      Betab = Atn(Tan(BetaIngRad) * Cos(ApAssVRad))        'Elica base
      Rb = Rprim * Cos(ApAssVRad)                          'R base
      DbaseV = 2 * Rb                                      'DBaseV
      DprimV = 2 * Rprim                                   'D prim.
      If (BetaIngRad <> 0) Then
        PassoElica = PG * MnormV * NFil / Sin(BetaIngRad)
      Else
        PassoElica = 0
      End If
      ControlloOk = True
      If (iDefSp = 1) Then  'Input:Spessore dente
        If (SpNormV > 0) Then
          'cal. QR da SON
          If (DR > 0) Then ControlloOk = QuotaRulli("W")
        Else
          'cal. SON da QR
          If (DR > 0) And (QR > 0) Then ControlloOk = QuotaRulli("E")
        End If
      Else
          'cal. SON da QR
          If (DR > 0) And (QR > 0) Then ControlloOk = QuotaRulli("E")
      End If
      If (ControlloOk = False) Then Exit Sub
      ControlloOk = CALCOLO_VITE_ZN
      '
    Case "BALL"
      ControlloOk = CALCOLO_VITE_BALL(1)
      '
    Case "STANDARD"
      ApAssVRad = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      ApAssV = ApAssVRad * (180 / PG)
      Rprim = MnormV * NFil / 2 / Cos(BetaIngRad)          'R primitivo
      Betab = Atn(Tan(BetaIngRad) * Cos(ApAssVRad))        'Elica base
      Rb = Rprim * Cos(ApAssVRad)                          'R base
      DbaseV = 2 * Rb                                      'DBaseV
      DprimV = 2 * Rprim                                   'D prim.
      ControlloOk = True
      '
    Case "SPIROID"
      ApAssVRad = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      ApAssV = ApAssVRad * (180 / PG)
      '
      AngMedF1V = ApViteRad
      AngMedF2V = ApViteRad
      E44F1V = RT + (DEXTV - DIntV) / 2
      E44F2V = RT + (DEXTV - DIntV) / 2
      E43F1V = E44F1V - (DprimV - DIntV) / 2
      E43F2V = E44F2V - (DprimV - DIntV) / 2
      PBF1V = 0.5
      PBF2V = 0.5
      BombF1V = 0
      BombF2V = 0
      E38V = SpNormV
      '
    Case "ARBURG"
      ApAssVRad = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      ApAssV = ApAssVRad * (180 / PG)
      Rprim = MnormV * NFil / 2 / Cos(BetaIngRad)          'R primitivo
      Betab = Atn(Tan(BetaIngRad) * Cos(ApAssVRad))        'Elica base
      Rb = Rprim * Cos(ApAssVRad)                          'R base
      DbaseV = 2 * Rb                                      'DBaseV
      DprimV = 2 * Rprim                                   'D prim.
      ControlloOk = CALCOLO_VITE_ARBURG
      '
    Case "ISO"
      ApAssVRad = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      ApAssV = ApAssVRad * (180 / PG)
      Rprim = MnormV * NFil / 2 / Cos(BetaIngRad)          'R primitivo
      Betab = Atn(Tan(BetaIngRad) * Cos(ApAssVRad))        'Elica base
      Rb = Rprim * Cos(ApAssVRad)                          'R base
      DbaseV = 2 * Rb                                      'DBaseV
      DprimV = 2 * Rprim                                   'D prim.
      ControlloOk = CALCOLO_VITE_ISO
      '
  End Select
  If (ControlloOk = False) Then Exit Sub
  '
  'VISUALIZZAZIONE QUOTA RULLI
  If (QR <> 0) Then Call SP("QR", frmt(QR, 8))
  'VISUALIZZAZIONE SPESSORE NORMALE
  If (SpNormV <> 0) Then Call SP("S0N", frmt(SpNormV, 8))
  'SCRITTURA DEI PARAMETRI CALCOLATI NEGLI ALTRI SOTTOPROGRAMMI
  Call SP("DIAMEXT", frmt(DEXTV, 3))
  Call SP("DIAINT", frmt(DIntV, 3))
  Call SP("PASSOELICA_G", frmt(PassoElica, 4))
  
  If (TIPO_MACCHINA = "GR250") Then
     Call SP("R[41]", frmt(BetaV, 4))
  Else
     Call SP("R[41]", frmt(BetaIng, 4))
  End If
  
  Call SP("R[5]", Format(NFil, "######0"))
  Call SP("R[103]", frmt(DprimV, 4))
  Call SP("COR[0,161]", "200")
  Call SP("COR[0,162]", "200")
  Call SP("LARGMOLAPR_G", frmt(E38V, 3))
  Call SP("R[6]", "1")
  Call SP("R[204]", "20")
  'Call SP("MAC[0,60]", "0")
  Call SP("ANGRACCORDOTESTAF1_G", frmt(AngRaccordoTesta, 3))
  Call SP("ANGRACCORDOTESTAF2_G", frmt(AngRaccordoTesta, 3))
  Call SP("ANGFIANCOF1_G", frmt(AngMedF1V * (180 / PG), 3))
  Call SP("ANGFIANCOF2_G", frmt(AngMedF2V * (180 / PG), 3))
  Call SP("ADDENDUMF1_G", frmt(E43F1V, 3))
  Call SP("ADDENDUMF2_G", frmt(E43F2V, 3))
  Call SP("ALTEZZAFIANCOF1_G", frmt(E44F1V, 3))
  Call SP("ALTEZZAFIANCOF2_G", frmt(E44F2V, 3))
  Call SP("POSBOMBFIANCOF1_G", frmt(PBF1V, 3))
  Call SP("POSBOMBFIANCOF2_G", frmt(PBF2V, 3))
  Call SP("BOMBFIANCOF1_G", frmt(-BombF1V, 3))
  Call SP("BOMBFIANCOF2_G", frmt(-BombF2V, 3))
  Call SP("RTESTA_G", frmt(RT, 3))
  Call SP("RTESTA2_G", frmt(RT, 3))
  Call SP("RFONDO_G", frmt(RF, 3))
  Call SP("RFONDO2_G", frmt(RF, 3))
  Call SP("R[34]", "0")
  Call SP("CORQRULLI", "0")
  Call OEMX.SuOEM1.DisegnaOEM
  '
  WRITE_DIALOG TipoVite & " -> " & "(Pe=" & frmt(PassoElica, 8) & " Dp=" & frmt(DprimV, 6) & " Db=" & frmt(DbaseV, 6) & " Spm=" & frmt(E38V, 4) & ")"
  '
Exit Sub

errVITI_ESTERNE_CALCOLO:
  WRITE_DIALOG "Sub VITI_ESTERNE_CALCOLO: ERROR -> " & Err
  Exit Sub

End Sub

Sub VITIV_ESTERNE_CALCOLO()
'
Dim ControlloOk As Boolean
Dim ret         As Long
Dim MODALITA    As String
'
Dim H3          As Double  'ALTEZZA DENTE
Dim Rtesta      As Double  'RAGGIO DI TESTA
Dim Rfondo      As Double  'RAGGIO DI FONDO
Dim TIPO_MACCHINA As String
'
On Error GoTo errVITIV_ESTERNE_CALCOLO
  '
  'LETTURA DELLA MODALITA DI MODIFICA DATI
  MODALITA = GetInfo("Configurazione", "Modalita", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))
  If (MODALITA <> "OFF-LINE") Then WRITE_DIALOG "SOFTKEY DISENABLED !!!": Exit Sub
  '
  TIPO_MACCHINA = GetInfo("Configurazione", "TIPO_MACCHINA", Path_LAVORAZIONE_INI)
  TIPO_MACCHINA = UCase$(Trim$(TIPO_MACCHINA))
  
  AngRaccordoTesta = LEP("ANGR")
  PassoElica = LEP("PassoElica")
  DEXTV = LEP("DEXT")
  DprimV = LEP("DprimV")
  '
  TipoVite = UT(LEP_STR("TIPO"))
  If (TipoVite = "ISO") Then
    Call PROFILO_VITE_ISO(H3, Rtesta, Rfondo)
    Call SP("NPRI", "1")
    Call SP("ALFA", frmt(ApVite, 4))
    Call SP("BETA", frmt(BetaV, 4))
    Call SP("DprimV", frmt(DprimV, 4))
    Call SP("S0N", frmt(SpNormV, 4))
    Call SP("DINT", frmt(DIntV, 4))
    Call SP("RT", frmt(Rtesta, 4))
    Call SP("RF", frmt(Rfondo, 4))
    '
    Call SP("H3", frmt(H3, 4))
    Call SP("DINT", frmt(DIntV, 4))
    Call SP("DIAINT", frmt(DIntV, 4))
    Call SP("R[103]", frmt(DprimV, 4))
    Call SP("SM", frmt(SpNormV, 4))
    Call SP("RTT", frmt(Rtesta, 4))
    Call SP("RFF", frmt(Rfondo, 4))
  End If
  '
  NFil = LEP("NPRI")
  MnormV = LEP("MN")
  ApVite = LEP("ALFA")
  BetaV = LEP("BETA")
  If (BetaV = 0) Then
    If (PassoElica <> 0) And (DprimV <> 0) Then
      BetaIngRad = Atn(PG * DprimV / PassoElica)
      BetaIng = BetaIngRad * (180 / PG)
      BetaV = 90 - BetaIng
      Call SP("BETA", frmt(BetaV, 8))
    End If
  End If
  '
  If (TipoVite <> "ZA") Then
    ApNormV = ApVite
    ApNormVrad = ApNormV * (PG / 180)
    '
    ApViteRad = ApNormV * (PG / 180)
  Else
    ApAssV = ApVite
    ApAssVRad = ApAssV * (PG / 180)
    '
    ApViteRad = ApVite * (PG / 180)
  End If
  '
  BetaV = Abs(BetaV)
  BetaVRad = BetaV * (PG / 180)
  '
  BetaIng = 90 - BetaV
  BetaIngRad = BetaIng * (PG / 180)
  '
  DIntV = LEP("DINT")
  DEXTV = LEP("DEXT")
  '
  iDefSp = LEP("iDefSp")
  SpNormV = LEP("S0N")
  QR = LEP("QR")
  DR = LEP("DR")
  '
  CoordinataXV = LEP("XC")
  CoordinataYV = LEP("YC")
  RaggioFiancoV = LEP("RC")
  RagFiancoMola = RaggioFiancoV
  '
  If (TipoVite <> "BALL") Then
    'CALCOLO DEL MODULO NORMALE DAL PASSO ELICA
    If (PassoElica <> 0) Then
      MnormV = (PassoElica * Sin(BetaIngRad)) / (PG * NFil)
      Call SP("MN", frmt(MnormV, 8))
    End If
    'CALCOLO DEL MODULO NORMALE DAL DIAMETRO PRIMITIVO
    If (DprimV <> 0) Then
      MnormV = DprimV * Cos(BetaIngRad) / NFil
      Call SP("MN", frmt(MnormV, 8))
    End If
  Else
    'NON FACCIO CALCOLI SUL MODULO
  End If
  '
  DiaMolTeo = LEP("DTEO")
  DiaMolEff = LEP("DEFF")
  RT = LEP("RT")
  RF = LEP("RF")
  '
  Call SP("R[34]", "0")
  Call SP("CORQRULLI", "0")
  Select Case TipoVite
    '
    Case "ZI"
      ApAssVRad = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      ApAssV = ApAssVRad * (180 / PG)
      Rprim = MnormV * NFil / 2 / Cos(BetaIngRad)          'R primitivo
      Betab = Atn(Tan(BetaIngRad) * Cos(ApAssVRad))        'Elica base
      Rb = Rprim * Cos(ApAssVRad)                          'R base
      DbaseV = 2 * Rb                                      'DBaseV
      DprimV = 2 * Rprim                                   'D prim.
      If (BetaIngRad <> 0) Then
        PassoElica = PG * MnormV * NFil / Sin(BetaIngRad)
      Else
        PassoElica = 0
      End If
      '
      If (SpNormV = 0) And (DR = 0) And (iDefSp = 1) Then
        Dim TT As Double
        Dim TH As Double
        Dim OD As Double
        OD = LEP("OD") 'Diametro esterno per calibro
        TH = LEP("TH") 'Profondit� dal diametro esterno del calibro
        TT = LEP("TT") 'Spessore misurato col calibro
        If (OD = 0) Or (TT) = 0 Then
          'Calcolo dello spessore dal modulo normale
          SpNormV = PG * MnormV / 2
        Else
          'Calcolo dello spessore dal calibro
          Dim InvPrim     As Double
          Dim Apx         As Double
          Dim ApxT        As Double
          Dim InvAx       As Double
          Dim BetaIngRadx As Double
          Dim RX          As Double
          Dim Sxnt        As Double
          RX = (OD - 2 * TH) / 2
          BetaIngRadx = Atn(PG * 2 * RX / PassoElica)
          Sxnt = TT / Cos(BetaIngRadx)
          Apx = Acs(Rb / RX)
          InvPrim = Tan(ApAssVRad) - ApAssVRad
          InvAx = Tan(Apx) - Apx
          SpNormV = (Sxnt / 2 / RX + InvAx - InvPrim) * 2 * Rprim * Cos(BetaIngRad)
        End If
        Call SP("S0N", frmt(SpNormV, 8))
      End If
      '
      ControlloOk = True
      If (iDefSp = 1) Then  'Input:Spessore dente
        If (SpNormV > 0) Then
          'cal. QR da SON
          If (DR > 0) Then ControlloOk = QuotaRulli("W")
        Else
          'cal. SON da QR
          If (DR > 0) And (QR > 0) Then ControlloOk = QuotaRulli("E")
        End If
      Else
          'cal. SON da QR
          If (DR > 0) And (QR > 0) Then ControlloOk = QuotaRulli("E")
      End If
      If (ControlloOk = False) Then Exit Sub
      ControlloOk = CALCOLO_VITE_ZI
      '
    Case "ZA"
      Dim ApTrasversale As Double
      ApTrasversale = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      Rprim = MnormV * NFil / 2 / Cos(BetaIngRad)              'R primitivo
      Betab = Atn(Tan(BetaIngRad) * Cos(ApTrasversale))        'Elica base
      Rb = Rprim * Cos(ApTrasversale)                          'R base
      DbaseV = 2 * Rb                                          'DBaseV
      DprimV = 2 * Rprim                                       'D prim.
      If (BetaIngRad <> 0) Then
        PassoElica = PG * MnormV * NFil / Sin(BetaIngRad)
      Else
        PassoElica = 0
      End If
      ControlloOk = True
      If (iDefSp = 1) Then  'Input:Spessore dente
        If (SpNormV > 0) Then
          'cal. QR da SON
          If (DR > 0) Then ControlloOk = QuotaRulli("W")
        Else
          'cal. SON da QR
          If (DR > 0) And (QR > 0) Then ControlloOk = QuotaRulli("E")
        End If
      Else
          'cal. SON da QR
          If (DR > 0) And (QR > 0) Then ControlloOk = QuotaRulli("E")
      End If
      If (ControlloOk = False) Then Exit Sub
      ControlloOk = CALCOLO_VITE_ZA
      '
    Case "ZK"
      ApAssVRad = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      ApAssV = ApAssVRad * (180 / PG)
      Rprim = MnormV * NFil / 2 / Cos(BetaIngRad)          'R primitivo
      Betab = Atn(Tan(BetaIngRad) * Cos(ApAssVRad))        'Elica base
      Rb = Rprim * Cos(ApAssVRad)                          'R base
      DbaseV = 2 * Rb                                      'DBaseV
      DprimV = 2 * Rprim                                   'D prim.
      If (BetaIngRad <> 0) Then
        PassoElica = PG * MnormV * NFil / Sin(BetaIngRad)
      Else
        PassoElica = 0
      End If
      ControlloOk = True
      If (iDefSp = 1) Then  'Input:Spessore dente
        If (SpNormV > 0) Then
          'cal. QR da SON
          If (DR > 0) Then ControlloOk = QuotaRulli("W")
        Else
          'cal. SON da QR
          If (DR > 0) And (QR > 0) Then ControlloOk = QuotaRulli("E")
        End If
      Else
          'cal. SON da QR
          If (DR > 0) And (QR > 0) Then ControlloOk = QuotaRulli("E")
      End If
      If (ControlloOk = False) Then Exit Sub
      ControlloOk = CALCOLO_VITE_ZK
      '
    Case "ZC"
      ApAssVRad = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      ApAssV = ApAssVRad * (180 / PG)
      Rprim = MnormV * NFil / 2 / Cos(BetaIngRad)          'R primitivo
      Betab = Atn(Tan(BetaIngRad) * Cos(ApAssVRad))        'Elica base
      Rb = Rprim * Cos(ApAssVRad)                          'R base
      DbaseV = 2 * Rb                                      'DBaseV
      DprimV = 2 * Rprim                                   'D prim.
      If (BetaIngRad <> 0) Then
        PassoElica = PG * MnormV * NFil / Sin(BetaIngRad)
      Else
        PassoElica = 0
      End If
      ControlloOk = True
      If (iDefSp = 1) Then  'Input:Spessore dente
        If (SpNormV > 0) Then
          'cal. QR da SON
          If (DR > 0) Then ControlloOk = QuotaRulli("W")
        Else
          'cal. SON da QR
          If (DR > 0) And (QR > 0) Then ControlloOk = QuotaRulli("E")
        End If
      Else
          'cal. SON da QR
          If (DR > 0) And (QR > 0) Then ControlloOk = QuotaRulli("E")
      End If
      If (ControlloOk = False) Then Exit Sub
      ControlloOk = CALCOLO_VITE_ZC
      '
    Case "ZN"
      ApAssVRad = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      ApAssV = ApAssVRad * (180 / PG)
      Rprim = MnormV * NFil / 2 / Cos(BetaIngRad)          'R primitivo
      Betab = Atn(Tan(BetaIngRad) * Cos(ApAssVRad))        'Elica base
      Rb = Rprim * Cos(ApAssVRad)                          'R base
      DbaseV = 2 * Rb                                      'DBaseV
      DprimV = 2 * Rprim                                   'D prim.
      If (BetaIngRad <> 0) Then
        PassoElica = PG * MnormV * NFil / Sin(BetaIngRad)
      Else
        PassoElica = 0
      End If
      ControlloOk = True
      If (iDefSp = 1) Then  'Input:Spessore dente
        If (SpNormV > 0) Then
          'cal. QR da SON
          If (DR > 0) Then ControlloOk = QuotaRulli("W")
        Else
          'cal. SON da QR
          If (DR > 0) And (QR > 0) Then ControlloOk = QuotaRulli("E")
        End If
      Else
          'cal. SON da QR
          If (DR > 0) And (QR > 0) Then ControlloOk = QuotaRulli("E")
      End If
      If (ControlloOk = False) Then Exit Sub
      ControlloOk = CALCOLO_VITE_ZN
      '
    Case "BALL"
      ControlloOk = CALCOLO_VITE_BALL(1)
      '
    Case "STANDARD"
      ApAssVRad = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      ApAssV = ApAssVRad * (180 / PG)
      Rprim = MnormV * NFil / 2 / Cos(BetaIngRad)          'R primitivo
      Betab = Atn(Tan(BetaIngRad) * Cos(ApAssVRad))        'Elica base
      Rb = Rprim * Cos(ApAssVRad)                          'R base
      DbaseV = 2 * Rb                                      'DBaseV
      DprimV = 2 * Rprim                                   'D prim.
      ControlloOk = True
      '
    Case "SPIROID"
      ApAssVRad = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      ApAssV = ApAssVRad * (180 / PG)
      '
      AngMedF1V = ApViteRad
      AngMedF2V = ApViteRad
      E44F1V = RT + (DEXTV - DIntV) / 2
      E44F2V = RT + (DEXTV - DIntV) / 2
      E43F1V = E44F1V - (DprimV - DIntV) / 2
      E43F2V = E44F2V - (DprimV - DIntV) / 2
      PBF1V = 0.5
      PBF2V = 0.5
      BombF1V = 0
      BombF2V = 0
      E38V = SpNormV
      '
    Case "ARBURG"
      ApAssVRad = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      ApAssV = ApAssVRad * (180 / PG)
      Rprim = MnormV * NFil / 2 / Cos(BetaIngRad)          'R primitivo
      Betab = Atn(Tan(BetaIngRad) * Cos(ApAssVRad))        'Elica base
      Rb = Rprim * Cos(ApAssVRad)                          'R base
      DbaseV = 2 * Rb                                      'DBaseV
      DprimV = 2 * Rprim                                   'D prim.
      ControlloOk = CALCOLO_VITE_ARBURG
      '
    Case "ISO"
      ApAssVRad = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      ApAssV = ApAssVRad * (180 / PG)
      Rprim = MnormV * NFil / 2 / Cos(BetaIngRad)          'R primitivo
      Betab = Atn(Tan(BetaIngRad) * Cos(ApAssVRad))        'Elica base
      Rb = Rprim * Cos(ApAssVRad)                          'R base
      DbaseV = 2 * Rb                                      'DBaseV
      DprimV = 2 * Rprim                                   'D prim.
      ControlloOk = CALCOLO_VITE_ISO
      '
  End Select
  If (ControlloOk = False) Then Exit Sub
  '
  'VISUALIZZAZIONE QUOTA RULLI
  If (QR <> 0) Then Call SP("QR", frmt(QR, 8))
  'VISUALIZZAZIONE SPESSORE NORMALE
  If (SpNormV <> 0) Then Call SP("S0N", frmt(SpNormV, 8))
  'SCRITTURA DEI PARAMETRI CALCOLATI NEGLI ALTRI SOTTOPROGRAMMI
  Call SP("DIAMEXT", frmt(DEXTV, 3))
  Call SP("DIAINT", frmt(DIntV, 3))
  Call SP("PASSOELICA_G", frmt(PassoElica, 4))
  
  'If (TIPO_MACCHINA = "GR250") Then
   
   Call SP("R[41]", frmt(BetaV, 4))
  
  'Else
  '   Call SP("R[41]", frmt(BetaIng, 4))
  'End If
  
  Dim HFiancoMola As Double
  HFiancoMola = (E44F1V + E44F2V) / 2
  Call SP("HFIANCOMOLA", frmt(HFiancoMola, 3))
  
  Call SP("R[5]", Format(NFil, "######0"))
  Call SP("R[103]", frmt(DprimV, 4))
  Call SP("COR[0,161]", "200")
  Call SP("COR[0,162]", "200")
  Call SP("LARGMOLAPR_G", frmt(E38V, 3))
  Call SP("R[6]", "1")
  Call SP("R[204]", "20")
  'Call SP("MAC[0,60]", "0")
  Call SP("ANGRACCORDOTESTAF1_G", frmt(AngRaccordoTesta, 3))
  Call SP("ANGRACCORDOTESTAF2_G", frmt(AngRaccordoTesta, 3))
  Call SP("ANGFIANCOF1_G", frmt(AngMedF1V * (180 / PG), 3))
  Call SP("ANGFIANCOF2_G", frmt(AngMedF2V * (180 / PG), 3))
  Call SP("ADDENDUMF1_G", frmt(E43F1V, 3))
  Call SP("ADDENDUMF2_G", frmt(E43F2V, 3))
  Call SP("ALTEZZAFIANCOF1_G", frmt(E44F1V, 3))
  Call SP("ALTEZZAFIANCOF2_G", frmt(E44F2V, 3))
  Call SP("POSBOMBFIANCOF1_G", frmt(PBF1V, 3))
  Call SP("POSBOMBFIANCOF2_G", frmt(PBF2V, 3))
  Call SP("BOMBFIANCOF1_G", frmt(-BombF1V, 3))
  Call SP("BOMBFIANCOF2_G", frmt(-BombF2V, 3))
  Call SP("RTESTA_G", frmt(RT, 3))
  Call SP("RTESTA2_G", frmt(RT, 3))
  Call SP("RFONDO_G", frmt(RF, 3))
  Call SP("RFONDO2_G", frmt(RF, 3))
  Call SP("R[34]", "0")
  Call SP("CORQRULLI", "0")
  '
  
  
  Call OEMX.SuOEM1.DisegnaOEM
  '
  WRITE_DIALOG TipoVite & " -> " & "(Pe=" & frmt(PassoElica, 8) & " Dp=" & frmt(DprimV, 6) & " Db=" & frmt(DbaseV, 6) & " Spm=" & frmt(E38V, 4) & ")"
  '
Exit Sub

errVITIV_ESTERNE_CALCOLO:
  WRITE_DIALOG "Sub VITIV_ESTERNE_CALCOLO: ERROR -> " & Err
  Exit Sub

End Sub

Public Function CALCOLO_VITE_BALL(ByVal ESTERNA As Integer) As Boolean
'
Dim AR     As Double
Dim ARR    As Double
Dim A3     As Double
Dim XP12   As Double
Dim YP12   As Double
Dim L      As Double
Dim L2     As Double
Dim Xc2    As Double
Dim Yc2    As Double
Dim Xc22   As Double
Dim Yc22   As Double
Dim Xc3    As Double
Dim Yc3    As Double
Dim Xp13   As Double
Dim Yp13   As Double
Dim Ang    As Double
Dim XP2    As Double
Dim YP2    As Double
Dim Xp3    As Double
Dim Yp3    As Double
Dim Xp23   As Double
Dim Yp23   As Double
Dim Xp6    As Double
Dim Yp6    As Double
Dim Xp5    As Double
Dim Yp5    As Double
Dim Yp7    As Double
Dim Yp8    As Double
Dim Xp7    As Double
Dim Xp8    As Double
Dim Xp78   As Double
Dim Yp78   As Double
Dim HFU    As Double
Dim YP57   As Double
Dim XP57   As Double
Dim L1     As Double
Dim RB1    As Double
Dim Xc1    As Double
Dim Yc1    As Double
Dim Step41 As Double
'
Dim MyCoordinataYV As Double
'
On Error GoTo errCALCOLO_VITE_BALL
  '
  CALCOLO_VITE_BALL = False
  '
  'Riporto coordinata Y rispetto diametro esterno
  If (ESTERNA = 1) Then
    MyCoordinataYV = CoordinataYV + (QR - DR - DEXTV) / 2
  Else
    MyCoordinataYV = CoordinataYV + (DEXTV - (QR + DR)) / 2
  End If
  '
  'Calcolo
  AR = Asn((MyCoordinataYV + RT) / (RaggioFiancoV + RT))
  Xc2 = -CoordinataXV + (RaggioFiancoV + RT) * Cos(AR)
  Yc2 = -MyCoordinataYV + (RaggioFiancoV + RT) * Sin(AR)
  XP12 = -CoordinataXV + RaggioFiancoV * Cos(AR)
  YP12 = -MyCoordinataYV + RaggioFiancoV * Sin(AR)
  '
  A3 = Asn(CoordinataXV / (RaggioFiancoV - RF))
  Xc3 = 0 '- CoordinataXV + (RaggioFiancoV - RFF1V) * Sin(A3)
  Yc3 = -MyCoordinataYV + (RaggioFiancoV - RF) * Cos(A3)
  Xp13 = Xc3 + RF * Sin(A3)
  Yp13 = Yc3 + RF * Cos(A3)
  '
  'Parametri mola
  AngMedF1V = (PG / 2 - A3 - AR) / 2 + AR
  YP57 = -MyCoordinataYV + RaggioFiancoV * Sin(AngMedF1V)
  XP57 = -CoordinataXV + RaggioFiancoV * Cos(AngMedF1V)
  L = Sqr((Xp5 - Xp7) ^ 2 + (Yp5 - Yp7) ^ 2)
  BombF1V = -(RaggioFiancoV - Sqr(RaggioFiancoV ^ 2 - (L / 2) ^ 2))
  YP57 = YP57 + BombF1V * Sin(AngMedF1V)
  XP57 = XP57 + BombF1V * Cos(AngMedF1V)
  E38V = XP57 * 2
  'Loop ricerca AngMedF1V
  Step41 = 0.01
  Do
    AngMedF1V = AngMedF1V - Step41
    XP57 = E38V / 2
    Yp7 = RT * (1 - Sin(AngMedF1V))
    Yp5 = Yc3 + RF * Sin(AngMedF1V)
    YP57 = (Yp5 + Yp7) / 2
    Xp7 = XP57 + (YP57 - Yp7) * Tan(AngMedF1V)
    Xp5 = XP57 + (YP57 - Yp5) * Tan(AngMedF1V)
    L = Sqr((Xp5 - Xp7) ^ 2 + (Yp5 - Yp7) ^ 2)
    BombF1V = -(RaggioFiancoV - Sqr(RaggioFiancoV ^ 2 - (L / 2) ^ 2))
    YP57 = YP57 - BombF1V * Sin(AngMedF1V)  'punto su cerchio
    XP57 = XP57 - BombF1V * Cos(AngMedF1V)
    Xc1 = XP57 - RaggioFiancoV * Cos(AngMedF1V)
    Yc1 = YP57 - RaggioFiancoV * Sin(AngMedF1V)
  Loop While Yc1 < -MyCoordinataYV
  Step41 = Step41 / 100
  Do
    AngMedF1V = AngMedF1V + Step41
    XP57 = E38V / 2
    Yp7 = RT * (1 - Sin(AngMedF1V))
    Yp5 = Yc3 + RF * Sin(AngMedF1V)
    YP57 = (Yp5 + Yp7) / 2
    Xp7 = XP57 + (YP57 - Yp7) * Tan(AngMedF1V)
    Xp5 = XP57 + (YP57 - Yp5) * Tan(AngMedF1V)
    L = Sqr((Xp5 - Xp7) ^ 2 + (Yp5 - Yp7) ^ 2)
    BombF1V = -(RaggioFiancoV - Sqr(RaggioFiancoV ^ 2 - (L / 2) ^ 2))
    YP57 = YP57 - BombF1V * Sin(AngMedF1V)  'punto su cerchio
    XP57 = XP57 - BombF1V * Cos(AngMedF1V)
    Xc1 = XP57 - RaggioFiancoV * Cos(AngMedF1V)
    Yc1 = YP57 - RaggioFiancoV * Sin(AngMedF1V)
  Loop While Yc1 > -MyCoordinataYV
  '
  E38V = Xp5 + Xp7 - (CoordinataXV + Xc1) * 2
  '
  E43F1V = (Yp5 + Yp7) / 2
  E44F1V = Yc3 + RF
  PBF1V = 0.5
  '
  'CALCOLO DEL DIAMETRO PRIMITIVO
  If ESTERNA = 1 Then
    DprimV = DEXTV - E43F1V * 2
    DIntV = DEXTV - E44F1V * 2
  Else
    DprimV = DEXTV + E43F1V * 2
    DIntV = DEXTV + E44F1V * 2
  End If
  '
  AngMedF2V = AngMedF1V
  E43F2V = E43F1V
  E44F2V = E44F1V
  PBF2V = PBF1V
  BombF2V = BombF1V
  '
  CALCOLO_VITE_BALL = True
  '
Exit Function

errCALCOLO_VITE_BALL:
  WRITE_DIALOG "SUB: CALCOLO_VITE_BALL " & Error(Err)
  Exit Function

End Function

Function NewPrendi(ByVal INDICE As Integer) As String
'
Dim NomeTabella As String
Dim Campo       As String
Dim stmp        As String
Dim DB          As Database
Dim TB          As Recordset
'
On Error Resume Next
  '
  NomeTabella = "MESSAGGI_VITI"
  stmp = UCase$(Trim$(g_chLanguageAbbr))
  Select Case stmp
    Case "IT", "CH", "FR", "PO", "GR", "UK": Campo = "NOME_" & stmp
    Case Else:                               Campo = "NOME_" & "IT"
  End Select
  '
  Set DB = OpenDatabase(PathDB, True, False)
  Set TB = DB.OpenRecordset(NomeTabella)
  TB.Index = "INDICE"
  TB.Seek "=", INDICE
  If Not TB.NoMatch Then
    If Len(TB.Fields(Campo).Value) > 0 Then
      NewPrendi = TB.Fields(Campo).Value
    Else
      NewPrendi = ""
    End If
  Else
    NewPrendi = ""
  End If
  TB.Close
  DB.Close
  '
End Function
Public Sub VITI_ESTERNE_VISUALIZZA_MOLA_ADDIN(ByVal Controllo As Control, ByVal Scala As Double, ByVal ZeroPezzoX As Single, ByVal ZeroPezzoY As Single, ByVal SiStampa As String, ByVal SiSpezzata As Integer, ByVal FileDati As String, Optional ByVal SiMsg As Boolean = True, Optional SiConfronta As Boolean = False)

Dim R37                   As Double
Dim R34                   As Double
Dim HFU                   As Double
Dim PP0C2                 As Double
Dim PP8C2                 As Double
Dim ang8                  As Double
Dim ang78                 As Double
Dim PP0P78                As Double
Dim PP10C12               As Double
Dim PP18C12               As Double
Dim ang18                 As Double
Dim ang1718               As Double
Dim PP10P1718             As Double
Dim LL1                   As Double
Dim LL2                   As Double
Dim TEMPVAR               As Double
Dim DXX                   As Double
Dim XX(1, 99)             As Double
Dim YY(1, 99)             As Double
Dim RR(1, 99)             As Double
Dim LARGMOLAPR_G          As Double
Dim ANGRACCORDOTESTAF1_G  As Double
Dim ALTEZZAFIANCOF1_G     As Double
Dim ADDENDUMF1_G          As Double
Dim RFONDO_G              As Double
Dim RTESTA_G              As Double
Dim ANGFIANCOF1_G         As Double
Dim POSBOMBFIANCOF1_G     As Double
Dim BOMBFIANCOF1_G        As Double
Dim ANGRACCORDOTESTAF2_G  As Double
Dim ALTEZZAFIANCOF2_G     As Double
Dim ADDENDUMF2_G          As Double
Dim RFONDO2_G             As Double
Dim RTESTA2_G             As Double
Dim ANGFIANCOF2_G         As Double
Dim POSBOMBFIANCOF2_G     As Double
Dim BOMBFIANCOF2_G        As Double
Dim CORRBOMBF1_G          As Double
Dim CORRBOMBF2_G          As Double
Dim CORRFHAF1_G           As Double
Dim CORRFHAF2_G           As Double
Dim R197                  As Double
Dim R207                  As Double
Dim R103                  As Double
'
Dim hSMUSSOF1             As Double
Dim hSMUSSOF2             As Double
'
Dim X9SMUSSOF1            As Double
Dim Y9SMUSSOF1            As Double
Dim X8SMUSSOF1            As Double
Dim Y8SMUSSOF1            As Double
'
Dim X19SMUSSOF2           As Double
Dim Y19SMUSSOF2           As Double
Dim X18SMUSSOF2           As Double
Dim Y18SMUSSOF2           As Double
Dim GruppoTMP             As String
Dim SiSMUSSO              As String
Dim ColoreCurva           As Integer
'
Dim HF1               As Double
Dim HF2               As Double
Dim HMED              As Double
Dim ADDF1             As Double
Dim ADDF2             As Double
Dim ADDMED            As Double
Dim ANGF1             As Double
Dim ANGF2             As Double
Dim ANGPRF            As Double
Dim ANGRAD            As Double
Dim SM                As Double
Dim DEDMED            As Double
Dim DEDENDUMF1        As Double
Dim DIFFSM            As Double
Dim DIFFADD           As Double
Dim DIFFDED           As Double
Dim CORRORIG          As Double
Dim CodiceMolaAddin   As String
Dim ANGRACF1          As Double
Dim SPEMOLA           As Double
Dim AltSmusso         As Double
Dim X8                As Double
Dim Y8                As Double
Dim X9                As Double
Dim Y9                As Double
Dim HSMU              As Double
Dim DIFFSMU           As Double

'
On Error GoTo errVITI_ESTERNE_VISUALIZZA_MOLA_ADDIN
  '
'  If (SiStampa <> "Y") Then Controllo.Cls
'  Controllo.BackColor = &HFFFFFF
  '
  'EQUIVALENTE DEL PROGRAMMA INIZVITE.SPF
  
  R197 = 0: R207 = 0
  R103 = LEP("R[103]")
  R34 = 0 ' LEP("R[34]")
  
  'FileDati = "ADDIN"
  'FileDati = "S400G"

  GruppoTMP = GetInfo("OEM1", "TB_CONFRONTA_MOLE", Path_LAVORAZIONE_INI)
  GruppoTMP = UCase$(Trim$(GruppoTMP)) 'NOME DEL CAMPO
  GruppoTMP = GetInfo("OEM1", GruppoTMP, Path_LAVORAZIONE_INI)
  GruppoTMP = UCase$(Trim$(GruppoTMP)) 'NOME DELLA TABELLA/SEZIONE FILE INI
  
  'Profilo Mola ADDIN
  R37 = val(GetInfo(GruppoTMP, "R[37]", g_chOemPATH & "\" & FileDati & ".INI"))
  CORRFHAF1_G = GetInfo(GruppoTMP, "CORRFHAF1_G", g_chOemPATH & "\" & FileDati & ".INI")
  CORRFHAF2_G = GetInfo(GruppoTMP, "CORRFHAF2_G", g_chOemPATH & "\" & FileDati & ".INI")
  CORRBOMBF1_G = GetInfo(GruppoTMP, "CORRBOMBF1_G", g_chOemPATH & "\" & FileDati & ".INI")
  CORRBOMBF2_G = GetInfo(GruppoTMP, "CORRBOMBF2_G", g_chOemPATH & "\" & FileDati & ".INI")
  LARGMOLAPR_G = GetInfo(GruppoTMP, "LARGMOLAPR_G", g_chOemPATH & "\" & FileDati & ".INI")
  'FIANCO 1
  ANGRACCORDOTESTAF1_G = GetInfo(GruppoTMP, "ANGRACCORDOTESTAF1_G", g_chOemPATH & "\" & FileDati & ".INI")
  ANGRACCORDOTESTAF1_G = FnRad(ANGRACCORDOTESTAF1_G)
  ANGFIANCOF1_G = GetInfo(GruppoTMP, "ANGFIANCOF1_G", g_chOemPATH & "\" & FileDati & ".INI")
  ANGFIANCOF1_G = FnRad(ANGFIANCOF1_G)
  ADDENDUMF1_G = GetInfo(GruppoTMP, "ADDENDUMF1_G", g_chOemPATH & "\" & FileDati & ".INI")
  ALTEZZAFIANCOF1_G = GetInfo(GruppoTMP, "ALTEZZAFIANCOF1_G", g_chOemPATH & "\" & FileDati & ".INI")
  POSBOMBFIANCOF1_G = GetInfo(GruppoTMP, "POSBOMBFIANCOF1_G", g_chOemPATH & "\" & FileDati & ".INI")
  BOMBFIANCOF1_G = GetInfo(GruppoTMP, "BOMBFIANCOF1_G", g_chOemPATH & "\" & FileDati & ".INI")
  RTESTA_G = GetInfo(GruppoTMP, "RTESTA_G", g_chOemPATH & "\" & FileDati & ".INI")
  RFONDO_G = GetInfo(GruppoTMP, "RFONDO_G", g_chOemPATH & "\" & FileDati & ".INI")
  
  'FIANCO 2
  ANGRACCORDOTESTAF2_G = GetInfo(GruppoTMP, "ANGRACCORDOTESTAF2_G", g_chOemPATH & "\" & FileDati & ".INI")
  ANGRACCORDOTESTAF2_G = FnRad(ANGRACCORDOTESTAF2_G)
  
  ANGFIANCOF2_G = GetInfo(GruppoTMP, "ANGFIANCOF2_G", g_chOemPATH & "\" & FileDati & ".INI")
  ANGFIANCOF2_G = FnRad(ANGFIANCOF2_G)
  ADDENDUMF2_G = GetInfo(GruppoTMP, "ADDENDUMF2_G", g_chOemPATH & "\" & FileDati & ".INI")
  ALTEZZAFIANCOF2_G = GetInfo(GruppoTMP, "ALTEZZAFIANCOF2_G", g_chOemPATH & "\" & FileDati & ".INI")
  POSBOMBFIANCOF2_G = GetInfo(GruppoTMP, "POSBOMBFIANCOF2_G", g_chOemPATH & "\" & FileDati & ".INI")
  BOMBFIANCOF2_G = GetInfo(GruppoTMP, "BOMBFIANCOF2_G", g_chOemPATH & "\" & FileDati & ".INI")
  RTESTA2_G = GetInfo(GruppoTMP, "RTESTA2_G", g_chOemPATH & "\" & FileDati & ".INI")
  RFONDO2_G = GetInfo(GruppoTMP, "RFONDO2_G", g_chOemPATH & "\" & FileDati & ".INI")
  BOMBFIANCOF1_G = BOMBFIANCOF1_G + CORRBOMBF1_G
  BOMBFIANCOF2_G = BOMBFIANCOF2_G + CORRBOMBF2_G
  '
  '
  SiSMUSSO = GetInfo("Configurazione", "SiSMUSSO", Path_LAVORAZIONE_INI)
  SiSMUSSO = UCase$(Trim$(SiSMUSSO))
  If (SiSMUSSO = "Y") Then
    hSMUSSOF1 = GetInfo(GruppoTMP, "HSMUSSOF1", g_chOemPATH & "\" & FileDati & ".INI")
    hSMUSSOF2 = GetInfo(GruppoTMP, "HSMUSSOF2", g_chOemPATH & "\" & FileDati & ".INI")
  End If
  '
  'Ricalcolo angolo fianco
  HFU = ALTEZZAFIANCOF1_G - (RTESTA_G + RFONDO_G) * Sin(ANGFIANCOF1_G)
  ANGFIANCOF1_G = ANGFIANCOF1_G - ASIN(CORRFHAF1_G * Cos(ANGFIANCOF1_G) / HFU)
  HFU = ALTEZZAFIANCOF2_G - (RTESTA2_G + RFONDO2_G) * Sin(ANGFIANCOF2_G) 'modifica SVG170214
  ANGFIANCOF2_G = ANGFIANCOF2_G - ASIN(CORRFHAF2_G * Cos(ANGFIANCOF2_G) / HFU)
    
  'EQUIVALENTE DEL PROGRAMMA CALPRFVIT.SPF
  'FIANCO 1
  XX(0, 6) = (LARGMOLAPR_G - R34) / 2
  YY(0, 2) = ALTEZZAFIANCOF1_G - ADDENDUMF1_G
  YY(0, 5) = YY(0, 2) - RFONDO_G * (1 - Sin(ANGFIANCOF1_G))
  YY(0, 7) = -ADDENDUMF1_G + RTESTA_G * (1 - Sin(ANGFIANCOF1_G))
  XX(0, 7) = XX(0, 6) - YY(0, 7) * Tan(ANGFIANCOF1_G)
  HFU = YY(0, 5) - YY(0, 7)
  '******************************
  YY(0, 25) = YY(0, 7) + HFU * POSBOMBFIANCOF1_G
  XX(0, 25) = XX(0, 7) - HFU * POSBOMBFIANCOF1_G * Tan(ANGFIANCOF1_G)
  LL1 = (YY(0, 5) - YY(0, 25)) / Cos(ANGFIANCOF1_G)
  LL2 = (YY(0, 25) - YY(0, 7)) / Cos(ANGFIANCOF1_G)
  YY(0, 25) = YY(0, 25) + BOMBFIANCOF1_G * Sin(ANGFIANCOF1_G)
  XX(0, 25) = XX(0, 25) + BOMBFIANCOF1_G * Cos(ANGFIANCOF1_G)
  '******************************
  If (BOMBFIANCOF1_G <> 0) Then
    RR(0, 1) = -(POT(LL1) + POT(BOMBFIANCOF1_G)) / 2 / BOMBFIANCOF1_G
    RR(0, 2) = -(POT(LL2) + POT(BOMBFIANCOF1_G)) / 2 / BOMBFIANCOF1_G
  Else
    RR(0, 1) = 999999
    RR(0, 2) = 999999
  End If
  XX(0, 51) = XX(0, 25) + RR(0, 1) * Cos(ANGFIANCOF1_G)
  YY(0, 51) = YY(0, 25) + RR(0, 1) * Sin(ANGFIANCOF1_G)
  XX(0, 52) = XX(0, 25) + RR(0, 2) * Cos(ANGFIANCOF1_G)
  YY(0, 52) = YY(0, 25) + RR(0, 2) * Sin(ANGFIANCOF1_G)
  'SPOSTAMENTO CAVITA/BOMBATURA F1 SUL PRIMITIVO
  If (YY(0, 25) > 0) Then
    If (BOMBFIANCOF1_G > 0) Then
      DXX = XX(0, 6) - (Sqr(POT(RR(0, 2)) - POT(YY(0, 52))) + XX(0, 52))
    Else
      DXX = XX(0, 6) - (-Sqr(POT(RR(0, 2)) - POT(YY(0, 52))) + XX(0, 52))
    End If
  End If
  If (YY(0, 25) < 0) Then
    If (BOMBFIANCOF1_G > 0) Then
      DXX = XX(0, 6) - (Sqr(POT(RR(0, 1)) - POT(YY(0, 51))) + XX(0, 51))
    Else
      DXX = XX(0, 6) - (-Sqr(POT(RR(0, 1)) - POT(YY(0, 51))) + XX(0, 51))
    End If
  End If
  If (YY(0, 25) = 0) Then
    DXX = XX(0, 6) - XX(0, 25)
  End If
  XX(0, 7) = XX(0, 7) + DXX
  XX(0, 25) = XX(0, 25) + DXX
  XX(0, 51) = XX(0, 51) + DXX
  XX(0, 52) = XX(0, 52) + DXX
  YY(0, 3) = YY(0, 2)
  If ((RR(0, 1) > 0) And (YY(0, 51) < YY(0, 3))) Or ((RR(0, 1) < 0) And ((YY(0, 3) - YY(0, 51)) > Abs(RR(0, 1)))) Then
    'Write_Dialog "ERROR: Raccordo fondo F1 non possibile !"
    WRITE_DIALOG "ERROR: ROOT RADIUS NOT POSSIBLE !"
    Exit Sub
  End If
  XX(0, 3) = XX(0, 51) - RR(0, 1) * Cos(ASIN((YY(0, 3) - YY(0, 51)) / RR(0, 1)))
  YY(0, 8) = YY(0, 7) + RTESTA_G * (Sin(ANGFIANCOF1_G) - Cos(ANGRACCORDOTESTAF1_G))
  XX(0, 8) = XX(0, 7) + RTESTA_G * (Cos(ANGFIANCOF1_G) - Sin(ANGRACCORDOTESTAF1_G))
  'If (XX(0, 8) > (R37 / 2 - 0.5)) Then
  '  WRITE_DIALOG "ERROR: WHEEL WIDTH TOO SMALL - F1"
  '  Exit Sub
  'End If
  TEMPVAR = ATAN2((Abs((YY(0, 52) - YY(0, 8)) / (XX(0, 52) - XX(0, 8)))), 1)
  ang8 = PG / 2 - ANGRACCORDOTESTAF1_G - TEMPVAR
  PP8C2 = Sqr(POT(XX(0, 8) - XX(0, 52)) + POT(YY(0, 8) - YY(0, 52)))
  If (((RR(0, 2) > 0) And (PP8C2 > RR(0, 2))) Or ((RR(0, 2) < 0) And (YY(0, 52) > YY(0, 8)))) Then
    'Write_Dialog "ERROR: Raccordo testa F1 non possibile !"
    WRITE_DIALOG "ERROR: TIP RADIUS F1 NOT POSSIBLE !"
    Exit Sub
  End If
  PP0C2 = PP8C2 * Cos(ang8)
  PP0P78 = Sqr(POT(RR(0, 2)) - POT(PP0C2))
  ang78 = ASIN(PP0P78 / Abs(RR(0, 2))) + ANGRACCORDOTESTAF1_G
  XX(0, 28) = XX(0, 52) - RR(0, 2) * Sin(ang78)
  YY(0, 28) = YY(0, 52) - RR(0, 2) * Cos(ang78)
  XX(0, 9) = R37 / 2 + R197 '$TC_DP6[1,1]
  If (hSMUSSOF1 > 0) Then
    X8SMUSSOF1 = XX(0, 8) + hSMUSSOF1 * Cos(ANGRACCORDOTESTAF1_G)
    Y8SMUSSOF1 = YY(0, 8) - hSMUSSOF1
    X9SMUSSOF1 = X8SMUSSOF1 + RTESTA_G * Sin(ANGRACCORDOTESTAF1_G)
    Y9SMUSSOF1 = Y8SMUSSOF1 - RTESTA_G * (1 - Cos(ANGRACCORDOTESTAF1_G))
    YY(0, 9) = Y9SMUSSOF1
  Else
    YY(0, 9) = YY(0, 28) - (XX(0, 9) - XX(0, 28)) * Tan(ANGRACCORDOTESTAF1_G)
  End If
  XX(0, 1) = -XX(0, 3)
  'Fianco 2
  XX(0, 16) = -(LARGMOLAPR_G - R34) / 2
  YY(0, 12) = ALTEZZAFIANCOF2_G - ADDENDUMF2_G
  YY(0, 15) = YY(0, 12) - RFONDO2_G * (1 - Sin(ANGFIANCOF2_G))
  YY(0, 17) = -ADDENDUMF2_G + RTESTA2_G * (1 - Sin(ANGFIANCOF2_G))
  XX(0, 17) = XX(0, 16) + YY(0, 17) * Tan(ANGFIANCOF2_G)
  HFU = YY(0, 15) - YY(0, 17)
  '***********************
  YY(0, 26) = YY(0, 17) + HFU * POSBOMBFIANCOF2_G
  XX(0, 26) = XX(0, 17) + HFU * POSBOMBFIANCOF2_G * Tan(ANGFIANCOF2_G)
  LL1 = (YY(0, 15) - YY(0, 26)) / Cos(ANGFIANCOF2_G)
  LL2 = (YY(0, 26) - YY(0, 17)) / Cos(ANGFIANCOF2_G)
  YY(0, 26) = YY(0, 26) + BOMBFIANCOF2_G * Sin(ANGFIANCOF2_G)
  XX(0, 26) = XX(0, 26) - BOMBFIANCOF2_G * Cos(ANGFIANCOF2_G)
  '***********************
  If (BOMBFIANCOF2_G <> 0) Then
    RR(0, 11) = -(POT(LL1) + POT(BOMBFIANCOF2_G)) / 2 / BOMBFIANCOF2_G
    RR(0, 12) = -(POT(LL2) + POT(BOMBFIANCOF2_G)) / 2 / BOMBFIANCOF2_G
  Else
    RR(0, 11) = 999999
    RR(0, 12) = 999999
  End If
  XX(0, 61) = XX(0, 26) - RR(0, 11) * Cos(ANGFIANCOF2_G)
  YY(0, 61) = YY(0, 26) + RR(0, 11) * Sin(ANGFIANCOF2_G)
  XX(0, 62) = XX(0, 26) - RR(0, 12) * Cos(ANGFIANCOF2_G)
  YY(0, 62) = YY(0, 26) + RR(0, 12) * Sin(ANGFIANCOF2_G)
  'SPOSTAMENTO CAVITA/BOMBATURA F2 SUL PRIMITIVO
  If (YY(0, 26) > 0) Then
    If (BOMBFIANCOF2_G > 0) Then
      DXX = XX(0, 16) - (-Sqr(POT(RR(0, 12)) - POT(YY(0, 62))) + XX(0, 62))
    Else
      DXX = XX(0, 16) - (Sqr(POT(RR(0, 12)) - POT(YY(0, 62))) + XX(0, 62))
    End If
  End If
  If (YY(0, 26) < 0) Then
    If (BOMBFIANCOF2_G > 0) Then
      DXX = XX(0, 16) - (-Sqr(POT(RR(0, 11)) - POT(YY(0, 61))) + XX(0, 61))
    Else
      DXX = XX(0, 16) - (Sqr(POT(RR(0, 11)) - POT(YY(0, 61))) + XX(0, 61))
    End If
  End If
  If (YY(0, 26) = 0) Then
    DXX = XX(0, 16) - XX(0, 26)
  End If
  XX(0, 17) = XX(0, 17) + DXX
  XX(0, 26) = XX(0, 26) + DXX
  XX(0, 61) = XX(0, 61) + DXX
  XX(0, 62) = XX(0, 62) + DXX
  YY(0, 13) = YY(0, 12)
  If ((RR(0, 11) > 0) And (YY(0, 61) < YY(0, 13))) Or ((RR(0, 11) < 0) And ((YY(0, 13) - YY(0, 61)) > Abs(RR(0, 11)))) Then
    'Write_Dialog "ERROR: Raccordo fondo F2 non possibile !"
    WRITE_DIALOG "ERROR: ROOT RADIUS F2 NOT POSSIBLE !"
    Exit Sub
  End If
  XX(0, 13) = XX(0, 61) + RR(0, 11) * Cos(ASIN(Abs((YY(0, 13) - YY(0, 61)) / RR(0, 11))))
  YY(0, 18) = YY(0, 17) + RTESTA2_G * (Sin(ANGFIANCOF2_G) - Cos(ANGRACCORDOTESTAF2_G))
  XX(0, 18) = XX(0, 17) - RTESTA2_G * (Cos(ANGFIANCOF2_G) - Sin(ANGRACCORDOTESTAF2_G))
  'If ((Abs(XX(0, 18)) > (R37 / 2 - 0.5))) Then
  '  WRITE_DIALOG "ERROR: WHEEL WIDTH TOO SMALL - F2"
  '  Exit Sub
  'End If
  TEMPVAR = ATAN2((Abs((YY(0, 18) - YY(0, 62)) / (XX(0, 62) - XX(0, 18)))), 1)
  ang18 = PG / 2 - ANGRACCORDOTESTAF2_G - TEMPVAR
  PP18C12 = Sqr(POT(XX(0, 18) - XX(0, 62)) + POT(YY(0, 18) - YY(0, 62)))
  If (((RR(0, 12) > 0) And (PP18C12 > RR(0, 12))) Or ((RR(0, 12) < 0) And (YY(0, 62) > YY(0, 18)))) Then
    'Write_Dialog "ERROR: Raccordo testa F2 non possibile !"
    WRITE_DIALOG "ERROR: TIP RADIUS (F2) NOT POSSIBLE !"
    Exit Sub
  End If
  PP10C12 = PP18C12 * Cos(ang18)
  PP10P1718 = Sqr(POT(RR(0, 12)) - POT(PP10C12))
  ang1718 = ASIN(PP10P1718 / Abs(RR(0, 12))) + ANGRACCORDOTESTAF2_G
  XX(0, 27) = XX(0, 62) + RR(0, 12) * Sin(ang1718)
  YY(0, 27) = YY(0, 62) - RR(0, 12) * Cos(ang1718)
  XX(0, 19) = -R37 / 2 - R207 '$TC_DP6[1,2]
  If (hSMUSSOF2 > 0) Then
    X18SMUSSOF2 = XX(0, 18) - hSMUSSOF2 * Cos(ANGRACCORDOTESTAF2_G)
    Y18SMUSSOF2 = YY(0, 18) - hSMUSSOF2
    X19SMUSSOF2 = X18SMUSSOF2 - RTESTA2_G * Sin(ANGRACCORDOTESTAF2_G)
    Y19SMUSSOF2 = Y18SMUSSOF2 - RTESTA2_G * (1 - Cos(ANGRACCORDOTESTAF2_G))
    YY(0, 19) = Y19SMUSSOF2
  Else
    YY(0, 19) = YY(0, 27) + (XX(0, 19) - XX(0, 27)) * Tan(ANGRACCORDOTESTAF2_G)
  End If
  XX(0, 11) = -XX(0, 13)
  '
  '****************************************
  'Calcolo Corr. Profilatura
  If SiConfronta Then GoSub Confronta
  '*****************************************
  '
  '
  'VISUALIZZAZIONE MOLA: EQUIVALENTE DEL PROGRAMMA PRFVIT.SPF
  '
  Dim X1            As Double
  Dim Y1            As Double
  Dim X2            As Double
  Dim Y2            As Double
  Dim X3            As Double
  Dim Y3            As Double
  Dim X4            As Double
  Dim Y4            As Double
  Dim Ymin          As Double
  Dim Dy            As Double
  Dim PicW          As Single
  Dim PicH          As Single
  Dim ScalaX        As Single
  Dim ScalaY        As Single
  Dim stmp          As String
  Dim TipoCarattere As String
  Dim M1            As Double
  Dim Q1            As Double
  Dim Q2            As Double
  Dim angc2         As Double
  Dim angc12        As Double
  Dim ang5          As Double
  Dim ang15         As Double
  Dim Xc7           As Double
  Dim Yc7           As Double
  Dim Xc17          As Double
  Dim Yc17          As Double
  Dim Xc3           As Double
  Dim Yc3           As Double
  Dim Xc13          As Double
  Dim Yc13          As Double
  Dim Raggio        As Double
  Dim Xc            As Double
  Dim YC            As Double
  Dim pX1           As Double
  Dim pY1           As Double
  Dim pX2           As Double
  Dim pY2           As Double
  Dim DeltaAng      As Double
  Dim Ang           As Double
  Dim SAPF1         As Double
  Dim SAPF2         As Double
  Dim EAPF1         As Double
  Dim EAPF2         As Double
  Dim EXTF1         As Double
  Dim EXTF2         As Double
  Const NumeroArchi = 50
  '
  'DEVO CALCOLARE I PUNTI DI TANGENZA PERCHE' SUL CNC SONO RACCORDATI
  '
  'Fianco 1
  'ricerca tangenza raggio di testa con testa e fianco
  M1 = Tan(ANGRACCORDOTESTAF1_G): Q1 = YY(0, 8) - M1 * XX(0, 8) 'retta uscita
  Y1 = YY(0, 28)
  Dy = 0.0001
  Do
    Y1 = Y1 + Dy
    angc2 = Asn((YY(0, 52) - Y1) / RR(0, 2))
    X1 = XX(0, 52) - RR(0, 2) * Cos(angc2)
    Xc7 = X1 + RTESTA_G * Cos(angc2)
    Yc7 = Y1 + RTESTA_G * Sin(angc2)
    Q2 = Yc7 - M1 * Xc7
  Loop While (Abs(Q2 - Q1) * Cos(ANGRACCORDOTESTAF1_G)) < RTESTA_G
  'xx(0, 7) = X1
  'YY(0, 7) = Y1
  'xx(0, 8) = Xc7 - RTESTA_G * Sin(ANGRACCORDOTESTAF1_G)
  'YY(0, 8) = Yc7 - RTESTA_G * Cos(ANGRACCORDOTESTAF1_G)
  '
  'ricerca tangenza raggio di fondo con fianco
  YY(0, 5) = YY(0, 2)
  Dy = 0.0001
  Do
    YY(0, 5) = YY(0, 5) - Dy
    ang5 = Asn((YY(0, 51) - YY(0, 5)) / RR(0, 1))
    Yc3 = YY(0, 5) - RFONDO_G * Sin(ang5)
  Loop While Yc3 > (YY(0, 2) - RFONDO_G)
  Xc3 = XX(0, 51) - (RR(0, 1) + RFONDO_G) * Cos(ang5)
  XX(0, 77) = XX(0, 51) - RR(0, 1) * Cos(ang5)
  YY(0, 77) = YY(0, 51) - RR(0, 1) * Sin(ang5)
  XX(0, 78) = Xc3
  YY(0, 78) = Yc3 + RFONDO_G
  '
  'Fianco 2
  'ricerca tangenza raggio di testa con testa e fianco
  M1 = -Tan(ANGRACCORDOTESTAF2_G): Q1 = YY(0, 18) - M1 * XX(0, 18)  'retta uscita
  Y1 = YY(0, 27)
  Dy = 0.0001
  Do
    Y1 = Y1 + Dy
    angc12 = Asn((YY(0, 62) - Y1) / RR(0, 12))
    X1 = XX(0, 62) + RR(0, 12) * Cos(angc12)
    Xc17 = X1 - RTESTA2_G * Cos(angc12)
    Yc17 = Y1 + RTESTA2_G * Sin(angc12)
    Q2 = Yc17 - M1 * Xc17
  Loop While (Abs(Q2 - Q1) * Cos(ANGRACCORDOTESTAF2_G)) < RTESTA2_G
  'xx(0, 17) = X1
  'YY(0, 17) = Y1
  'xx(0, 18) = Xc17 + RTESTA2_G * Sin(ANGRACCORDOTESTAF2_G)
  'YY(0, 18) = Yc17 - RTESTA2_G * Cos(ANGRACCORDOTESTAF2_G)
  '
  'ricerca tangenza raggio di fondo con fianco
  YY(0, 15) = YY(0, 12)
  Dy = 0.0001
  Do
    YY(0, 15) = YY(0, 15) - Dy
    ang15 = Asn((YY(0, 61) - YY(0, 15)) / RR(0, 11))
    Yc13 = YY(0, 15) - RFONDO2_G * Sin(ang15)
  Loop While Yc13 > (YY(0, 12) - RFONDO2_G)
  Xc13 = XX(0, 61) + (RR(0, 11) + RFONDO2_G) * Cos(ang15)
  XX(0, 87) = XX(0, 61) + RR(0, 11) * Cos(ang15)
  YY(0, 87) = YY(0, 61) - RR(0, 11) * Sin(ang15)
  XX(0, 88) = Xc13
  YY(0, 88) = Yc13 + RFONDO2_G
  '
  'UNIFORMO LA SCALA PER LA VISUALIZZAZIONE
  ScalaX = Scala: ScalaY = Scala
  'Acquisizione area grafica
  stmp = GetInfo("DIS0", "PicW", Path_LAVORAZIONE_INI)
  If (Len(stmp) > 0) Then PicW = val(stmp) Else PicW = 190
  stmp = GetInfo("DIS0", "PicH", Path_LAVORAZIONE_INI)
  If (Len(stmp) > 0) Then PicH = val(stmp) Else PicH = 95
  'TIPO DI CARATTERE PER I COMMENTI E VALORI
      If (LINGUA = "CH") Then
    TipoCarattere = "MS Song"
  ElseIf (LINGUA = "RU") Then
    TipoCarattere = "Arial Cyr"
  Else
    TipoCarattere = "Courier New"
  End If
  'Preparazione grafico
  If (SiStampa = "Y") Then
    For i = 1 To 2
      Printer.FontName = TipoCarattere
      Printer.FontSize = 8
      Printer.FontBold = False
    Next i
    WRITE_DIALOG Printer.DeviceName & " PRINTING....."
    If PicW = 190 And PicH = 95 Then
      Printer.Orientation = 1
    Else
      Printer.Orientation = 2
    End If
    Printer.ScaleMode = 6
  Else
'    Controllo.Cls
    Controllo.ScaleWidth = PicW
    Controllo.ScaleHeight = PicH
    For i = 1 To 2
      Controllo.FontName = TipoCarattere
      Controllo.FontSize = 8
      Controllo.FontBold = False
    Next i
'    Controllo.Refresh
  End If
  '
  If (SiStampa = "Y") Then
    Printer.Line (0, PicH / 2)-(PicW, PicH / 2)
    Printer.Line (PicW / 2, 0)-(PicW / 2, PicH)
  Else
    Controllo.Line (0, PicH / 2)-(PicW, PicH / 2)
    Controllo.Line (PicW / 2, 0)-(PicW / 2, PicH)
  End If
  '
  '
  'FIANCO 1
  ColoreCurva = 0
  '
  If (hSMUSSOF1 > 0) Then
  X1 = (-XX(0, 9) - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (YY(0, 9) - ZeroPezzoY) * ScalaY + PicH / 2
  X2 = (-X9SMUSSOF1 - ZeroPezzoX) * ScalaX + PicW / 2
  Y2 = (Y9SMUSSOF1 - ZeroPezzoY) * ScalaY + PicH / 2
  If (SiStampa = "Y") Then
    Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
  Else
    Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
  End If
  X1 = (-X8SMUSSOF1 - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (Y8SMUSSOF1 - ZeroPezzoY) * ScalaY + PicH / 2
  X2 = (-XX(0, 8) - ZeroPezzoX) * ScalaX + PicW / 2
  Y2 = (YY(0, 8) - ZeroPezzoY) * ScalaY + PicH / 2
  If (SiStampa = "Y") Then
    Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
  Else
    Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
  End If
  Else
  X1 = (-XX(0, 9) - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (YY(0, 9) - ZeroPezzoY) * ScalaY + PicH / 2
  X2 = (-XX(0, 8) - ZeroPezzoX) * ScalaX + PicW / 2
  Y2 = (YY(0, 8) - ZeroPezzoY) * ScalaY + PicH / 2
  If (SiStampa = "Y") Then
    Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
  Else
    Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
  End If
  End If
  '
  SAPF1 = R103 - YY(0, 77) * 2
  stmp = " " & frmt(SAPF1, 3) & " "
  X1 = (-XX(0, 77) - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (YY(0, 77) - ZeroPezzoY) * ScalaY + PicH / 2
  If (SiStampa = "Y") Then
    Printer.CurrentX = X1 - 5 - Controllo.TextWidth(stmp)
    Printer.CurrentY = Y1 - Printer.TextHeight(stmp) / 2
    If SiMsg Then
    Printer.Print stmp
    Printer.Line (X1 - 5, Y1)-(X1 + 5, Y1), QBColor(ColoreCurva)
    End If
  Else
    Controllo.CurrentX = X1 - 1 - Controllo.TextWidth(stmp)
    Controllo.CurrentY = Y1 - Controllo.TextHeight(stmp) / 2
    If SiMsg Then
    Controllo.Print stmp
    Controllo.Line (X1 - 1, Y1)-(X1 + 1, Y1), QBColor(ColoreCurva)
    End If
  End If
  '
  EAPF1 = R103 - YY(0, 7) * 2
  stmp = " " & frmt(EAPF1, 3) & " "
  X1 = (-XX(0, 7) - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (YY(0, 7) - ZeroPezzoY) * ScalaY + PicH / 2
  If (SiStampa = "Y") Then
    Printer.CurrentX = X1 - 5 - Controllo.TextWidth(stmp)
    Printer.CurrentY = Y1 - Printer.TextHeight(stmp) / 2
    If SiMsg Then
    Printer.Print stmp
    Printer.Line (X1 - 5, Y1)-(X1 + 5, Y1), QBColor(ColoreCurva)
    End If
  Else
    Controllo.CurrentX = X1 - 1 - Controllo.TextWidth(stmp)
    Controllo.CurrentY = Y1 - Controllo.TextHeight(stmp) / 2
    If SiMsg Then
    Controllo.Print stmp
    Controllo.Line (X1 - 1, Y1)-(X1 + 1, Y1), QBColor(ColoreCurva)
    End If
  End If
  '
  EXTF1 = R103 - YY(0, 8) * 2
  stmp = " " & frmt(EXTF1, 3) & " "
  X1 = (-XX(0, 8) - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (YY(0, 8) - ZeroPezzoY) * ScalaY + PicH / 2
  If (SiStampa = "Y") Then
    Printer.CurrentX = X1 - 5 - Controllo.TextWidth(stmp)
    Printer.CurrentY = Y1 - Printer.TextHeight(stmp) / 2
    If SiMsg Then
    Printer.Print stmp
    Printer.Line (X1 - 5, Y1)-(X1 + 5, Y1), QBColor(ColoreCurva)
    End If
  Else
    Controllo.CurrentX = X1 - 1 - Controllo.TextWidth(stmp)
    Controllo.CurrentY = Y1 - Controllo.TextHeight(stmp) / 2
    If SiMsg Then
    'Controllo.Print stmp
    Controllo.Line (X1 - 1, Y1)-(X1 + 1, Y1), QBColor(ColoreCurva)
    End If
  End If
  '
  If (SiSpezzata = 1) Then
    '
    X1 = (-XX(0, 9) - ZeroPezzoX) * ScalaX + PicW / 2
    Y1 = (YY(0, 9) - ZeroPezzoY) * ScalaY + PicH / 2
    X2 = (-XX(0, 28) - ZeroPezzoX) * ScalaX + PicW / 2
    Y2 = (YY(0, 28) - ZeroPezzoY) * ScalaY + PicH / 2
    If (SiStampa = "Y") Then
      Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    Else
      Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    End If
    '
    X1 = (-XX(0, 28) - ZeroPezzoX) * ScalaX + PicW / 2
    Y1 = (YY(0, 28) - ZeroPezzoY) * ScalaY + PicH / 2
    X2 = (-XX(0, 25) - ZeroPezzoX) * ScalaX + PicW / 2
    Y2 = (YY(0, 25) - ZeroPezzoY) * ScalaY + PicH / 2
    If (SiStampa = "Y") Then
      Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    Else
      Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    End If
    '
    X1 = (-XX(0, 7) - ZeroPezzoX) * ScalaX + PicW / 2
    Y1 = (YY(0, 7) - ZeroPezzoY) * ScalaY + PicH / 2
    X2 = (-XX(0, 25) - ZeroPezzoX) * ScalaX + PicW / 2
    Y2 = (YY(0, 25) - ZeroPezzoY) * ScalaY + PicH / 2
    If (SiStampa = "Y") Then
      Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    Else
      Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    End If
    '
    X1 = (-XX(0, 25) - ZeroPezzoX) * ScalaX + PicW / 2
    Y1 = (YY(0, 25) - ZeroPezzoY) * ScalaY + PicH / 2
    X2 = (-XX(0, 3) - ZeroPezzoX) * ScalaX + PicW / 2
    Y2 = (YY(0, 3) - ZeroPezzoY) * ScalaY + PicH / 2
    If (SiStampa = "Y") Then
      Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    Else
      Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    End If
  End If
  '
  X1 = (-XX(0, 3) - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (YY(0, 3) - ZeroPezzoY) * ScalaY + PicH / 2
  X2 = (-XX(0, 1) - ZeroPezzoX) * ScalaX + PicW / 2
  Y2 = (YY(0, 3) - ZeroPezzoY) * ScalaY + PicH / 2
  If (SiStampa = "Y") Then
    Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
  Else
    Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
  End If
  '
  If (hSMUSSOF1 > 0) Then
  Call RACCORDA_PROFILO(Controllo, Scala, -X9SMUSSOF1, -Y9SMUSSOF1, -X8SMUSSOF1, -Y8SMUSSOF1, RTESTA_G, "SX", SiStampa, ZeroPezzoX, -ZeroPezzoY)
  End If
  Call RACCORDA_PROFILO(Controllo, Scala, -XX(0, 8), -YY(0, 8), -XX(0, 7), -YY(0, 7), RTESTA_G, "SX", SiStampa, ZeroPezzoX, -ZeroPezzoY)
  If (BOMBFIANCOF1_G > 0) Then stmp = "DX" Else stmp = "SX"
  Call RACCORDA_PROFILO(Controllo, Scala, -XX(0, 7), -YY(0, 7), -XX(0, 25), -YY(0, 25), RR(0, 2), stmp, SiStampa, ZeroPezzoX, -ZeroPezzoY)
  Call RACCORDA_PROFILO(Controllo, Scala, -XX(0, 25), -YY(0, 25), -XX(0, 77), -YY(0, 77), RR(0, 1), stmp, SiStampa, ZeroPezzoX, -ZeroPezzoY)
  Call RACCORDA_PROFILO(Controllo, Scala, -XX(0, 77), -YY(0, 77), -XX(0, 78), -YY(0, 78), RFONDO_G, "DX", SiStampa, ZeroPezzoX, -ZeroPezzoY)
  '
  'FIANCO 2
  'ColoreCurva = 9
  '
  If (hSMUSSOF2 > 0) Then
  X1 = (-XX(0, 19) - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (YY(0, 19) - ZeroPezzoY) * ScalaY + PicH / 2
  X2 = (-X19SMUSSOF2 - ZeroPezzoX) * ScalaX + PicW / 2
  Y2 = (Y19SMUSSOF2 - ZeroPezzoY) * ScalaY + PicH / 2
  If (SiStampa = "Y") Then
    Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
  Else
    Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
  End If
  X1 = (-X18SMUSSOF2 - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (Y18SMUSSOF2 - ZeroPezzoY) * ScalaY + PicH / 2
  X2 = (-XX(0, 18) - ZeroPezzoX) * ScalaX + PicW / 2
  Y2 = (YY(0, 18) - ZeroPezzoY) * ScalaY + PicH / 2
  If (SiStampa = "Y") Then
    Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
  Else
    Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
  End If
  Else
  X1 = (-XX(0, 19) - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (YY(0, 19) - ZeroPezzoY) * ScalaY + PicH / 2
  X2 = (-XX(0, 18) - ZeroPezzoX) * ScalaX + PicW / 2
  Y2 = (YY(0, 18) - ZeroPezzoY) * ScalaY + PicH / 2
  If (SiStampa = "Y") Then
    Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
  Else
    Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
  End If
  End If
  '
  SAPF2 = R103 - YY(0, 87) * 2
  stmp = " " & frmt(SAPF2, 3) & " "
  X1 = (-XX(0, 87) - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (YY(0, 87) - ZeroPezzoY) * ScalaY + PicH / 2
  If (SiStampa = "Y") Then
    Printer.CurrentX = X1 + 5
    Printer.CurrentY = Y1 - Printer.TextHeight(stmp) / 2
    If SiMsg Then
    Printer.Print stmp
    Printer.Line (X1 - 5, Y1)-(X1 + 5, Y1), QBColor(ColoreCurva)
    End If
  Else
    Controllo.CurrentX = X1 + 1
    Controllo.CurrentY = Y1 - Controllo.TextHeight(stmp) / 2
    If SiMsg Then
    Controllo.Print stmp
    Controllo.Line (X1 - 1, Y1)-(X1 + 1, Y1), QBColor(ColoreCurva)
    End If
  End If
  '
  EAPF2 = R103 - YY(0, 17) * 2
  stmp = " " & frmt(EAPF2, 3) & " "
  X1 = (-XX(0, 17) - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (YY(0, 17) - ZeroPezzoY) * ScalaY + PicH / 2
  If (SiStampa = "Y") Then
    Printer.CurrentX = X1 + 5
    Printer.CurrentY = Y1 - Printer.TextHeight(stmp) / 2
    If SiMsg Then
    Printer.Print stmp
    Printer.Line (X1 - 5, Y1)-(X1 + 5, Y1), QBColor(ColoreCurva)
    End If
  Else
    Controllo.CurrentX = X1 + 1
    Controllo.CurrentY = Y1 - Controllo.TextHeight(stmp) / 2
    If SiMsg Then
    Controllo.Print stmp
    Controllo.Line (X1 - 1, Y1)-(X1 + 1, Y1), QBColor(ColoreCurva)
    End If
  End If
  '
  EXTF2 = R103 - YY(0, 18) * 2
  stmp = " " & frmt(EXTF2, 3) & " "
  X1 = (-XX(0, 18) - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (YY(0, 18) - ZeroPezzoY) * ScalaY + PicH / 2
  If (SiStampa = "Y") Then
    Printer.CurrentX = X1 + 5
    Printer.CurrentY = Y1 - Printer.TextHeight(stmp) / 2
    If SiMsg Then
    Printer.Print stmp
    Printer.Line (X1 - 5, Y1)-(X1 + 5, Y1), QBColor(ColoreCurva)
    End If
  Else
    Controllo.CurrentX = X1 + 1
    Controllo.CurrentY = Y1 - Controllo.TextHeight(stmp) / 2
    If SiMsg Then
    'Controllo.Print stmp
    Controllo.Line (X1 - 1, Y1)-(X1 + 1, Y1), QBColor(ColoreCurva)
    End If
  End If
  '
  If (SiSpezzata = 1) Then
    '
    X1 = (-XX(0, 19) - ZeroPezzoX) * ScalaX + PicW / 2
    Y1 = (YY(0, 19) - ZeroPezzoY) * ScalaY + PicH / 2
    X2 = (-XX(0, 27) - ZeroPezzoX) * ScalaX + PicW / 2
    Y2 = (YY(0, 27) - ZeroPezzoY) * ScalaY + PicH / 2
    If (SiStampa = "Y") Then
      Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    Else
      Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    End If
    '
    X1 = (-XX(0, 27) - ZeroPezzoX) * ScalaX + PicW / 2
    Y1 = (YY(0, 27) - ZeroPezzoY) * ScalaY + PicH / 2
    X2 = (-XX(0, 26) - ZeroPezzoX) * ScalaX + PicW / 2
    Y2 = (YY(0, 26) - ZeroPezzoY) * ScalaY + PicH / 2
    If (SiStampa = "Y") Then
      Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    Else
      Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    End If
    '
    X1 = (-XX(0, 17) - ZeroPezzoX) * ScalaX + PicW / 2
    Y1 = (YY(0, 17) - ZeroPezzoY) * ScalaY + PicH / 2
    X2 = (-XX(0, 26) - ZeroPezzoX) * ScalaX + PicW / 2
    Y2 = (YY(0, 26) - ZeroPezzoY) * ScalaY + PicH / 2
    If (SiStampa = "Y") Then
      Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    Else
      Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    End If
    '
    X1 = (-XX(0, 26) - ZeroPezzoX) * ScalaX + PicW / 2
    Y1 = (YY(0, 26) - ZeroPezzoY) * ScalaY + PicH / 2
    X2 = (-XX(0, 13) - ZeroPezzoX) * ScalaX + PicW / 2
    Y2 = (YY(0, 13) - ZeroPezzoY) * ScalaY + PicH / 2
    If (SiStampa = "Y") Then
    Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    Else
      Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    End If
  End If
  '
  X1 = (-XX(0, 13) - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (YY(0, 13) - ZeroPezzoY) * ScalaY + PicH / 2
  X2 = (-XX(0, 11) - ZeroPezzoX) * ScalaX + PicW / 2
  Y2 = (YY(0, 13) - ZeroPezzoY) * ScalaY + PicH / 2
  If (SiStampa = "Y") Then
    Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
  Else
    Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
  End If
  If (hSMUSSOF2 > 0) Then
  Call RACCORDA_PROFILO(Controllo, Scala, -X19SMUSSOF2, -Y19SMUSSOF2, -X18SMUSSOF2, -Y18SMUSSOF2, RTESTA2_G, "DX", SiStampa, ZeroPezzoX, -ZeroPezzoY)
  End If
  Call RACCORDA_PROFILO(Controllo, Scala, -XX(0, 18), -YY(0, 18), -XX(0, 17), -YY(0, 17), RTESTA2_G, "DX", SiStampa, ZeroPezzoX, -ZeroPezzoY)
  If (BOMBFIANCOF2_G > 0) Then stmp = "SX" Else stmp = "DX"
  Call RACCORDA_PROFILO(Controllo, Scala, -XX(0, 17), -YY(0, 17), -XX(0, 26), -YY(0, 26), RR(0, 12), stmp, SiStampa, ZeroPezzoX, -ZeroPezzoY)
  Call RACCORDA_PROFILO(Controllo, Scala, -XX(0, 26), -YY(0, 26), -XX(0, 87), -YY(0, 87), RR(0, 11), stmp, SiStampa, ZeroPezzoX, -ZeroPezzoY)
  Call RACCORDA_PROFILO(Controllo, Scala, -XX(0, 87), -YY(0, 87), -XX(0, 88), -YY(0, 88), RFONDO2_G, "SX", SiStampa, ZeroPezzoX, -ZeroPezzoY)
  '
  'FIANCO 1
  'Raggio = RTESTA_G
  'Xc = -(xx(0, 8) + RTESTA_G * Sin(ANGRACCORDOTESTAF1_G))
  'YC = -(YY(0, 8) + RTESTA_G * Cos(ANGRACCORDOTESTAF1_G))
  'GoSub DISEGNA_CERCHIO
  '
  'FIANCO 2
  'Raggio = RTESTA_G
  'Xc = -(xx(0, 18) + RTESTA_G * Sin(ANGRACCORDOTESTAF2_G))
  'YC = -(YY(0, 18) + RTESTA_G * Cos(ANGRACCORDOTESTAF2_G))
  'GoSub DISEGNA_CERCHIO
  '
  Const AF = 30
  Const DF = 2
  Const NumeroRette = 30
  '
  'TESTA DELLA MOLA
  X1 = (-XX(0, 9) - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (YY(0, 9) - ZeroPezzoY) * ScalaY + PicH / 2
  Y3 = (YY(0, 9) - 0.5 - ZeroPezzoY) * ScalaY + PicH / 2
  X2 = (-XX(0, 19) - ZeroPezzoX) * ScalaX + PicW / 2
  Y2 = (YY(0, 19) - ZeroPezzoY) * ScalaY + PicH / 2
  Y4 = (YY(0, 19) - 0.5 - ZeroPezzoY) * ScalaY + PicH / 2
  If Y4 < Y3 Then Y3 = Y4 Else Y4 = Y3
  stmp = " " & frmt(R37, 4) & " "
  If (SiStampa = "Y") Then
    Printer.CurrentX = PicW / 2 - Printer.TextWidth(stmp) / 2
    Printer.CurrentY = Y3 - Printer.TextHeight(stmp)
    Printer.Print stmp
    Printer.Line (X1, Y3)-(X2, Y4), QBColor(ColoreCurva)
    Printer.Line (X1, Y3)-(X1, Y1), QBColor(ColoreCurva)
    Printer.Line (X2, Y4)-(X2, Y2), QBColor(ColoreCurva)
  Else
    Controllo.CurrentX = PicW / 2 - Controllo.TextWidth(stmp) / 2
    Controllo.CurrentY = Y3 - Controllo.TextHeight(stmp)
    Controllo.Print stmp
    Controllo.Line (X1, Y3)-(X2, Y4), QBColor(ColoreCurva)
    Controllo.Line (X1, Y3)-(X1, Y1), QBColor(ColoreCurva)
    Controllo.Line (X2, Y4)-(X2, Y2), QBColor(ColoreCurva)
  End If
  '
  'Freccia a sinistra del testo
  Ang = AF / 180 * PG
  DeltaAng = Ang / NumeroRette
  X1 = (-XX(0, 9) - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (YY(0, 9) - 0.5 - ZeroPezzoY) * ScalaY + PicH / 2
  For i = -NumeroRette To NumeroRette
    X2 = (-XX(0, 9) - ZeroPezzoX) * ScalaX + DF * Cos(Ang) + PicW / 2
    Y2 = (YY(0, 9) - 0.5 - ZeroPezzoY) * ScalaY - DF * Sin(i * DeltaAng) + PicH / 2
    If (SiStampa = "Y") Then
      Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    Else
      Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    End If
  Next i
  'Freccia a destra del testo
  Ang = AF / 180 * PG
  DeltaAng = Ang / NumeroRette
  X1 = (-XX(0, 19) - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (YY(0, 19) - 0.5 - ZeroPezzoY) * ScalaY + PicH / 2
  For i = -NumeroRette To NumeroRette
    X2 = (-XX(0, 19) - ZeroPezzoX) * ScalaX - DF * Cos(Ang) + PicW / 2
    Y2 = (YY(0, 19) - 0.5 - ZeroPezzoY) * ScalaY - DF * Sin(i * DeltaAng) + PicH / 2
    If (SiStampa = "Y") Then
      Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    Else
      Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    End If
  Next i
  '
  'SPESSORE DELLA MOLA
  ColoreCurva = 0
  X1 = (-XX(0, 6) - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (YY(0, 6) - ZeroPezzoY) * ScalaY + PicH / 2
  X2 = (-XX(0, 16) - ZeroPezzoX) * ScalaX + PicW / 2
  Y2 = (YY(0, 16) - ZeroPezzoY) * ScalaY + PicH / 2
  If Y2 < Y1 Then Y1 = Y2 Else Y2 = Y1
  stmp = " " & frmt(XX(0, 6) - XX(0, 16), 4) & " "
  If (SiStampa = "Y") Then
    Printer.CurrentX = PicW / 2 - Printer.TextWidth(stmp) / 2
    Printer.CurrentY = Y1 - Printer.TextHeight(stmp)
    If SiMsg Then Printer.Print stmp
    Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
  Else
    Controllo.CurrentX = PicW / 2 - Controllo.TextWidth(stmp) / 2
    Controllo.CurrentY = Y1 - Controllo.TextHeight(stmp)
    If SiMsg Then Controllo.Print stmp
    Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
  End If
  '
  If SiMsg Then
  'Freccia a sinistra del testo
  Ang = AF / 180 * PG
  DeltaAng = Ang / NumeroRette
  X1 = (-XX(0, 6) - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (YY(0, 6) - ZeroPezzoY) * ScalaY + PicH / 2
  For i = -NumeroRette To NumeroRette
    X2 = (-XX(0, 6) - ZeroPezzoX) * ScalaX + DF * Cos(Ang) + PicW / 2
    Y2 = (YY(0, 6) - ZeroPezzoY) * ScalaY - DF * Sin(i * DeltaAng) + PicH / 2
    If (SiStampa = "Y") Then
      Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    Else
      Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    End If
  Next i
  If (SiStampa = "Y") Then
    Printer.Line (X1, Y1 + DF / 2)-(X1, Y1 - DF / 2), QBColor(ColoreCurva)
  Else
    Controllo.Line (X1, Y1 + DF / 2)-(X1, Y1 - DF / 2), QBColor(ColoreCurva)
  End If
  End If
  '
  If SiMsg Then
  'Freccia a destra del testo
  Ang = AF / 180 * PG
  DeltaAng = Ang / NumeroRette
  X1 = (-XX(0, 16) - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (YY(0, 16) - ZeroPezzoY) * ScalaY + PicH / 2
  For i = -NumeroRette To NumeroRette
    X2 = (-XX(0, 16) - ZeroPezzoX) * ScalaX - DF * Cos(Ang) + PicW / 2
    Y2 = (YY(0, 16) - ZeroPezzoY) * ScalaY - DF * Sin(i * DeltaAng) + PicH / 2
    If (SiStampa = "Y") Then
      Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    Else
      Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    End If
  Next i
  If (SiStampa = "Y") Then
    Printer.Line (X1, Y1 + DF / 2)-(X1, Y1 - DF / 2), QBColor(ColoreCurva)
  Else
    Controllo.Line (X1, Y1 + DF / 2)-(X1, Y1 - DF / 2), QBColor(ColoreCurva)
  End If
  End If
  '
  'FONDO DELLA MOLA
  X1 = (-XX(0, 78) - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (YY(0, 78) - ZeroPezzoY) * ScalaY + PicH / 2
  Y3 = (YY(0, 78) + 1.5 - ZeroPezzoY) * ScalaY + PicH / 2
  X2 = (-XX(0, 88) - ZeroPezzoX) * ScalaX + PicW / 2
  Y2 = (YY(0, 88) - ZeroPezzoY) * ScalaY + PicH / 2
  Y4 = (YY(0, 88) + 1.5 - ZeroPezzoY) * ScalaY + PicH / 2
  If Y4 < Y3 Then Y3 = Y4 Else Y4 = Y3
  stmp = " " & frmt(XX(0, 78) - XX(0, 88), 2) & " "
  If (SiStampa = "Y") Then
    Printer.CurrentX = PicW / 2 - Printer.TextWidth(stmp) / 2
    Printer.CurrentY = Y3 - Printer.TextHeight(stmp)
    If SiMsg Then
    Printer.Print stmp
    Printer.Line (X1, Y3)-(X2, Y4), QBColor(ColoreCurva)
    Printer.Line (X1, Y3)-(X1, Y1), QBColor(ColoreCurva)
    Printer.Line (X2, Y4)-(X2, Y2), QBColor(ColoreCurva)
    End If
  Else
    Controllo.CurrentX = PicW / 2 - Controllo.TextWidth(stmp) / 2
    Controllo.CurrentY = Y3 - 0.5 - Controllo.TextHeight(stmp)
    If SiMsg Then
    Controllo.Print stmp
    Controllo.DrawStyle = vbDot
    Controllo.Line (X1, Y3)-(X2, Y4), QBColor(ColoreCurva)
    Controllo.Line (X1, Y3)-(X1, Y1), QBColor(ColoreCurva)
    Controllo.Line (X2, Y4)-(X2, Y2), QBColor(ColoreCurva)
    Controllo.DrawStyle = 0
    End If
  End If
  ColoreCurva = 0
  'Freccia a sinistra del testo
  If SiMsg Then
  Ang = AF / 180 * PG
  DeltaAng = Ang / NumeroRette
  X1 = (-XX(0, 78) - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (YY(0, 78) + 1.5 - ZeroPezzoY) * ScalaY + PicH / 2
  For i = -NumeroRette To NumeroRette
    X2 = (-XX(0, 78) - ZeroPezzoX) * ScalaX + DF * Cos(Ang) + PicW / 2
    Y2 = (YY(0, 78) + 1.5 - ZeroPezzoY) * ScalaY - DF * Sin(i * DeltaAng) + PicH / 2
    If (SiStampa = "Y") Then
      Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    Else
      Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    End If
  Next i
  'Freccia a destra del testo
  Ang = AF / 180 * PG
  DeltaAng = Ang / NumeroRette
  X1 = (-XX(0, 88) - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (YY(0, 88) + 1.5 - ZeroPezzoY) * ScalaY + PicH / 2
  For i = -NumeroRette To NumeroRette
    X2 = (-XX(0, 88) - ZeroPezzoX) * ScalaX - DF * Cos(Ang) + PicW / 2
    Y2 = (YY(0, 88) + 1.5 - ZeroPezzoY) * ScalaY - DF * Sin(i * DeltaAng) + PicH / 2
    If (SiStampa = "Y") Then
      Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    Else
      Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    End If
  Next i
  End If
  '
  If (SiStampa = "Y") Then
    'FIANCO 1
    stmp = " F1 "
    Printer.CurrentX = 0
    Printer.CurrentY = 0
    Printer.Print stmp
    If SiMsg = True Then
    If (RR(0, 1) <> 999999) And (RR(0, 2) <> 999999) Then
      stmp = " RR[0,1] = " & frmt(RR(0, 1), 2) & " "
      Printer.CurrentX = 0
      Printer.CurrentY = PicH / 2 - 2 * Printer.TextHeight(stmp)
      Printer.Print stmp
      stmp = " RR[0,2] = " & frmt(RR(0, 2), 2) & " "
      Printer.CurrentX = 0
      Printer.CurrentY = PicH / 2 - 1 * Printer.TextHeight(stmp)
      Printer.Print stmp
    End If
    End If
    'FIANCO 2
    stmp = " F2 "
    Printer.CurrentX = PicW - Printer.TextWidth(stmp)
    Printer.CurrentY = 0
    Printer.Print stmp
    If SiMsg = True Then
    If (RR(0, 11) <> 999999) And (RR(0, 12) <> 999999) Then
      stmp = " RR[0,11] = " & frmt(RR(0, 11), 2) & " "
      Printer.CurrentX = PicW - Printer.TextWidth(stmp)
      Printer.CurrentY = PicH / 2 - 2 * Printer.TextHeight(stmp)
      Printer.Print stmp
      stmp = " RR[0,12] = " & frmt(RR(0, 12), 2) & " "
      Printer.CurrentX = PicW - Printer.TextWidth(stmp)
      Printer.CurrentY = PicH / 2 - 1 * Printer.TextHeight(stmp)
      Printer.Print stmp
    End If
    End If
  Else
    'FIANCO 1
    stmp = " F1 "
    Controllo.CurrentX = 0
    Controllo.CurrentY = 0
    Controllo.Print stmp
    If SiMsg = True Then
    If (RR(0, 1) <> 999999) And (RR(0, 2) <> 999999) Then
      stmp = " RR[0,1] = " & frmt(RR(0, 1), 2) & " "
      Controllo.CurrentX = 0
      Controllo.CurrentY = PicH / 2 - 2 * Controllo.TextHeight(stmp)
      Controllo.Print stmp
      stmp = " RR[0,2] = " & frmt(RR(0, 2), 2) & " "
      Controllo.CurrentX = 0
      Controllo.CurrentY = PicH / 2 - 1 * Controllo.TextHeight(stmp)
      Controllo.Print stmp
    End If
    End If
    'FIANCO 2
    stmp = " F2 "
    Controllo.CurrentX = PicW - Controllo.TextWidth(stmp)
    Controllo.CurrentY = 0
    Controllo.Print stmp
    If SiMsg = True Then
    If (RR(0, 11) <> 999999) And (RR(0, 12) <> 999999) Then
      stmp = " RR[0,11] = " & frmt(RR(0, 11), 2) & " "
      Controllo.CurrentX = PicW - Controllo.TextWidth(stmp)
      Controllo.CurrentY = PicH / 2 - 2 * Controllo.TextHeight(stmp)
      Controllo.Print stmp
      stmp = " RR[0,12] = " & frmt(RR(0, 12), 2) & " "
      Controllo.CurrentX = PicW - Controllo.TextWidth(stmp)
      Controllo.CurrentY = PicH / 2 - 1 * Controllo.TextHeight(stmp)
      Controllo.Print stmp
    End If
    End If
  End If
  
  If SiConfronta Then
  stmp = "Corr. Incremento Profilatura : " & frmt(CORRORIG, 2)
  Controllo.CurrentX = PicW - Controllo.TextWidth(stmp)
  Controllo.CurrentY = PicH - 2 * Controllo.TextHeight(stmp)
  Controllo.Print stmp
  
  CodiceMolaAddin = (GetInfo(GruppoTMP, "CODICE_MOLA", g_chOemPATH & "\" & FileDati & ".INI"))
  stmp = "Codice Mola: " & CodiceMolaAddin
  Controllo.CurrentX = 0
  Controllo.CurrentY = PicH - 2 * Controllo.TextHeight(stmp)
  Controllo.Print stmp
  
  End If
  '
  'Segnalazione della fine delle operazioni
  If (SiStampa = "Y") Then GoSub FineDocumento
  '
Exit Sub

errVITI_ESTERNE_VISUALIZZA_MOLA_ADDIN:
  WRITE_DIALOG "SUB : VITI_ESTERNE_VISUALIZZA_MOLA_ADDIN " & Error(Err)
  Exit Sub

DISEGNA_CERCHIO:
  pX1 = Xc + Raggio: pY1 = YC: DeltaAng = 2 * PG / NumeroArchi
  For j = 1 To NumeroArchi
    Ang = j * DeltaAng
    pX2 = Xc + Raggio * Cos(Ang): pY2 = YC + Raggio * Sin(Ang)
    'Adattamento nella finestra grafica
    pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
    'Traccio la linea
    If (SiStampa = "Y") Then
      If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
        Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
      End If
    Else
      If (pY1 <= Controllo.ScaleHeight) And (pX1 <= Controllo.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
        Controllo.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
      End If
    End If
    'Aggiorno Px1 e Py1
    pX1 = Xc + Raggio * Cos(Ang): pY1 = YC + Raggio * Sin(Ang)
  Next j
Return

FineDocumento:
  'Dimensione dei caratteri
  For i = 1 To 2
    Printer.FontName = TipoCarattere
    Printer.FontSize = 14
    Printer.FontBold = True
  Next i
  'Logo
  'Printer.PaintPicture OEM1.PicLogo, 0, PicH, 20, 20
  'Nome del documento
  stmp = Date & " " & Time
  Printer.CurrentX = 0 + 25
  Printer.CurrentY = PicH
  Printer.Print stmp
  'Scala X
  If OEMX.lblINP(0).Visible = True Then
    stmp = OEMX.lblINP(0).Caption & ": x" & OEMX.txtINP(0).Text
    Printer.CurrentX = 0 + 25
    Printer.CurrentY = PicH + 10
    Printer.Print stmp
  End If
  'Scala Y
  If OEMX.lblINP(1).Visible = True Then
    stmp = OEMX.lblINP(1).Caption & ": x" & OEMX.txtINP(1).Text
    Printer.CurrentX = 0 + PicW / 2
    Printer.CurrentY = PicH + 10
    Printer.Print stmp
  End If
  'Parametri
  OEMX.SuOEM1.Visible = True
  Call OEMX.SuOEM1.STAMPA(False, , , , 150)
  OEMX.SuOEM1.Visible = False
  Printer.EndDoc
  WRITE_DIALOG ""
  '
Return

Confronta:

'Leggo parametri profilo da realizzare
HF1 = LEP("ALTEZZAFIANCOF1_G")
HF2 = LEP("ALTEZZAFIANCOF2_G")
HMED = (HF1 + HF2) / 2
ADDF1 = LEP("ADDENDUMF1_G")
ADDF2 = LEP("ADDENDUMF2_G")
ADDMED = (ADDF1 + ADDF2) / 2
SM = LEP("LARGMOLAPR_G")
ANGF1 = LEP("ANGFIANCOF1_G")
ANGF2 = LEP("ANGFIANCOF2_G")
ANGPRF = (ANGF1 + ANGF2) / 2
ANGRAD = ANGPRF * 3.141592654 / 180
DEDMED = HMED - ADDMED
SPEMOLA = LEP("R[37]")

'Punto tangenza raggio testa e angolo raccordo
X8 = val(GetInfo("OEM1", "X[0,8]", Path_LAVORAZIONE_INI))
Y8 = val(GetInfo("OEM1", "Y[0,8]", Path_LAVORAZIONE_INI))
'Punto fine angolo raccordo
X9 = val(GetInfo("OEM1", "X[0,9]", Path_LAVORAZIONE_INI))
Y9 = val(GetInfo("OEM1", "Y[0,9]", Path_LAVORAZIONE_INI))

AltSmusso = Abs(Y9 - Y8)

'Mola montata
DEDENDUMF1 = ALTEZZAFIANCOF1_G - ADDENDUMF1_G
HSMU = Abs(YY(0, 9) - YY(0, 8))

If SM < LARGMOLAPR_G Then
  DIFFSM = (LARGMOLAPR_G - SM) / 2 / Tan(ANGRAD)
  CORRORIG = DIFFSM
  If ADDMED > ADDENDUMF1_G Then
    DIFFADD = ADDMED - ADDENDUMF1_G
    If DEDMED < DEDENDUMF1 Then
       DIFFDED = DEDENDUMF1 - DEDMED
       If DIFFADD > DIFFDED Then
        If DIFFSM > DIFFADD Then
           CORRORIG = DIFFSM
        Else
           CORRORIG = DIFFADD
        End If
       Else
        If DIFFSM > DIFFDED Then
           CORRORIG = DIFFSM
        Else
           CORRORIG = DIFFDED
        End If
       End If
    Else
       If DIFFSM > DIFFADD Then
          CORRORIG = DIFFSM
       Else
          CORRORIG = DIFFADD
       End If
    End If
  Else 'ADDMED < ADDENDUMF1_G
    If DEDMED < DEDENDUMF1 Then
       DIFFDED = DEDENDUMF1 - DEDMED
       If DIFFSM > DIFFDED Then
          CORRORIG = DIFFSM
       Else
          CORRORIG = DIFFDED
       End If
    End If
  End If
Else
  If ADDMED > ADDENDUMF1_G Then
    DIFFADD = ADDMED - ADDENDUMF1_G
    If DEDMED < DEDENDUMF1 Then
       DIFFDED = DEDENDUMF1 - DEDMED
       If DIFFADD > DIFFDED Then
          CORRORIG = DIFFADD
       Else
          CORRORIG = DIFFDED
       End If
    Else
       CORRORIG = DIFFADD
    End If
  Else
    If DEDMED < DEDENDUMF1 Then
       DIFFDED = DEDENDUMF1 - DEDMED
       CORRORIG = DIFFDED
    End If
  End If
End If
'
If (AltSmusso + ADDMED) > (HSMU + ADDENDUMF1_G) Then
   DIFFSMU = (AltSmusso + ADDMED) - (HSMU + ADDENDUMF1_G)
   If CORRORIG < DIFFSMU Then
      CORRORIG = DIFFSMU
   End If
End If
'
'
If CORRORIG = 0 Then
If SM > LARGMOLAPR_G Then
  DIFFSM = Abs((LARGMOLAPR_G - SM) / 2 / Tan(ANGRAD))
  CORRORIG = -DIFFSM
  If ADDMED < ADDENDUMF1_G Then
    DIFFADD = Abs(ADDMED - ADDENDUMF1_G)
    If DEDMED > DEDENDUMF1 Then
       DIFFDED = Abs(DEDENDUMF1 - DEDMED)
       If DIFFADD < DIFFDED Then
        If DIFFSM < DIFFADD Then
           CORRORIG = -DIFFSM
        Else
           CORRORIG = -DIFFADD
        End If
       Else
        If DIFFSM < DIFFDED Then
           CORRORIG = -DIFFSM
        Else
           CORRORIG = -DIFFDED
        End If
       End If
    Else
       If DIFFSM < DIFFADD Then
          CORRORIG = -DIFFSM
       Else
          CORRORIG = -DIFFADD
       End If
    End If
  Else 'ADDMED > ADDENDUMF1_G
    If DEDMED > DEDENDUMF1 Then
       DIFFDED = Abs(DEDENDUMF1 - DEDMED)
       If DIFFSM < DIFFDED Then
          CORRORIG = -DIFFSM
       Else
          CORRORIG = -DIFFDED
       End If
    End If
  End If
Else
  If ADDMED < ADDENDUMF1_G Then
    DIFFADD = Abs(ADDMED - ADDENDUMF1_G)
    If DEDMED > DEDENDUMF1 Then
       DIFFDED = Abs(DEDENDUMF1 - DEDMED)
       If DIFFADD < DIFFDED Then
          CORRORIG = -DIFFADD
       Else
          CORRORIG = -DIFFDED
       End If
    Else
       CORRORIG = -DIFFADD
    End If
  Else
    If DEDMED > DEDENDUMF1 Then
       DIFFDED = Abs(DEDENDUMF1 - DEDMED)
       CORRORIG = -DIFFDED
    End If
  End If
End If

End If


Return
End Sub


Public Sub VITI_ESTERNE_VISUALIZZA_MOLA(ByVal Controllo As Control, ByVal Scala As Double, ByVal ZeroPezzoX As Single, ByVal ZeroPezzoY As Single, ByVal SiStampa As String, ByVal SiSpezzata As Integer)
'
Dim R37                   As Double
Dim R34                   As Double
Dim HFU                   As Double
Dim PP0C2                 As Double
Dim PP8C2                 As Double
Dim ang8                  As Double
Dim ang78                 As Double
Dim PP0P78                As Double
Dim PP10C12               As Double
Dim PP18C12               As Double
Dim ang18                 As Double
Dim ang1718               As Double
Dim PP10P1718             As Double
Dim LL1                   As Double
Dim LL2                   As Double
Dim TEMPVAR               As Double
Dim DXX                   As Double
Dim XX(1, 99)             As Double
Dim YY(1, 99)             As Double
Dim RR(1, 99)             As Double
Dim LARGMOLAPR_G          As Double
Dim ANGRACCORDOTESTAF1_G  As Double
Dim ALTEZZAFIANCOF1_G     As Double
Dim ADDENDUMF1_G          As Double
Dim RFONDO_G              As Double
Dim RTESTA_G              As Double
Dim ANGFIANCOF1_G         As Double
Dim POSBOMBFIANCOF1_G     As Double
Dim BOMBFIANCOF1_G        As Double
Dim ANGRACCORDOTESTAF2_G  As Double
Dim ALTEZZAFIANCOF2_G     As Double
Dim ADDENDUMF2_G          As Double
Dim RFONDO2_G             As Double
Dim RTESTA2_G             As Double
Dim ANGFIANCOF2_G         As Double
Dim POSBOMBFIANCOF2_G     As Double
Dim BOMBFIANCOF2_G        As Double
Dim CORRBOMBF1_G          As Double
Dim CORRBOMBF2_G          As Double
Dim CORRFHAF1_G           As Double
Dim CORRFHAF2_G           As Double
Dim R197                  As Double
Dim R207                  As Double
Dim R103                  As Double
'
Dim hSMUSSOF1             As Double
Dim hSMUSSOF2             As Double
'
Dim X9SMUSSOF1            As Double
Dim Y9SMUSSOF1            As Double
Dim X8SMUSSOF1            As Double
Dim Y8SMUSSOF1            As Double
'
Dim X19SMUSSOF2           As Double
Dim Y19SMUSSOF2           As Double
Dim X18SMUSSOF2           As Double
Dim Y18SMUSSOF2           As Double
'
On Error GoTo errVITI_ESTERNE_VISUALIZZA_MOLA
  '
  If (SiStampa <> "Y") Then Controllo.Cls
  Controllo.BackColor = &HFFFFFF
  '
  'EQUIVALENTE DEL PROGRAMMA INIZVITE.SPF
  R37 = LEP("R[37]")
  R197 = 0: R207 = 0
  R103 = LEP("R[103]")
  R34 = LEP("R[34]")
  CORRFHAF1_G = LEP("CORRFHAF1_G")
  CORRFHAF2_G = LEP("CORRFHAF2_G")
  CORRBOMBF1_G = LEP("CORRBOMBF1_G")
  CORRBOMBF2_G = LEP("CORRBOMBF2_G")
  LARGMOLAPR_G = LEP("LARGMOLAPR_G")
  ANGRACCORDOTESTAF1_G = LEP("ANGRACCORDOTESTAF1_G")
  ANGRACCORDOTESTAF1_G = FnRad(ANGRACCORDOTESTAF1_G)
  ANGFIANCOF1_G = LEP("ANGFIANCOF1_G")
  ANGFIANCOF1_G = FnRad(ANGFIANCOF1_G)
  ADDENDUMF1_G = LEP("ADDENDUMF1_G")
  ALTEZZAFIANCOF1_G = LEP("ALTEZZAFIANCOF1_G")
  POSBOMBFIANCOF1_G = LEP("POSBOMBFIANCOF1_G")
  BOMBFIANCOF1_G = LEP("BOMBFIANCOF1_G")
  RTESTA_G = LEP("RTESTA_G")
  RFONDO_G = LEP("RFONDO_G")
  ANGRACCORDOTESTAF2_G = LEP("ANGRACCORDOTESTAF2_G")
  ANGRACCORDOTESTAF2_G = FnRad(ANGRACCORDOTESTAF2_G)
  ANGFIANCOF2_G = LEP("ANGFIANCOF2_G")
  ANGFIANCOF2_G = FnRad(ANGFIANCOF2_G)
  ADDENDUMF2_G = LEP("ADDENDUMF2_G")
  ALTEZZAFIANCOF2_G = LEP("ALTEZZAFIANCOF2_G")
  POSBOMBFIANCOF2_G = LEP("POSBOMBFIANCOF2_G")
  BOMBFIANCOF2_G = LEP("BOMBFIANCOF2_G")
  RTESTA2_G = LEP("RTESTA2_G")
  RFONDO2_G = LEP("RFONDO2_G")
  BOMBFIANCOF1_G = BOMBFIANCOF1_G + CORRBOMBF1_G
  BOMBFIANCOF2_G = BOMBFIANCOF2_G + CORRBOMBF2_G
  '
Dim SiSMUSSO As String
  '
  SiSMUSSO = GetInfo("Configurazione", "SiSMUSSO", Path_LAVORAZIONE_INI)
  SiSMUSSO = UCase$(Trim$(SiSMUSSO))
  If (SiSMUSSO = "Y") Then
    hSMUSSOF1 = LEP("HSMUSSOF1")
    hSMUSSOF2 = LEP("HSMUSSOF2")
  End If
  '
  'Ricalcolo angolo fianco
  HFU = ALTEZZAFIANCOF1_G - (RTESTA_G + RFONDO_G) * Sin(ANGFIANCOF1_G)
  ANGFIANCOF1_G = ANGFIANCOF1_G - ASIN(CORRFHAF1_G * Cos(ANGFIANCOF1_G) / HFU)
  HFU = ALTEZZAFIANCOF2_G - (RTESTA2_G + RFONDO2_G) * Sin(ANGFIANCOF2_G) 'modifica SVG170214
  ANGFIANCOF2_G = ANGFIANCOF2_G - ASIN(CORRFHAF2_G * Cos(ANGFIANCOF2_G) / HFU)
  'EQUIVALENTE DEL PROGRAMMA CALPRFVIT.SPF
  'FIANCO 1
  XX(0, 6) = (LARGMOLAPR_G - R34) / 2
  YY(0, 2) = ALTEZZAFIANCOF1_G - ADDENDUMF1_G
  YY(0, 5) = YY(0, 2) - RFONDO_G * (1 - Sin(ANGFIANCOF1_G))
  YY(0, 7) = -ADDENDUMF1_G + RTESTA_G * (1 - Sin(ANGFIANCOF1_G))
  XX(0, 7) = XX(0, 6) - YY(0, 7) * Tan(ANGFIANCOF1_G)
  HFU = YY(0, 5) - YY(0, 7)
  '******************************
  YY(0, 25) = YY(0, 7) + HFU * POSBOMBFIANCOF1_G
  XX(0, 25) = XX(0, 7) - HFU * POSBOMBFIANCOF1_G * Tan(ANGFIANCOF1_G)
  LL1 = (YY(0, 5) - YY(0, 25)) / Cos(ANGFIANCOF1_G)
  LL2 = (YY(0, 25) - YY(0, 7)) / Cos(ANGFIANCOF1_G)
  YY(0, 25) = YY(0, 25) + BOMBFIANCOF1_G * Sin(ANGFIANCOF1_G)
  XX(0, 25) = XX(0, 25) + BOMBFIANCOF1_G * Cos(ANGFIANCOF1_G)
  '******************************
  If (BOMBFIANCOF1_G <> 0) Then
    RR(0, 1) = -(POT(LL1) + POT(BOMBFIANCOF1_G)) / 2 / BOMBFIANCOF1_G
    RR(0, 2) = -(POT(LL2) + POT(BOMBFIANCOF1_G)) / 2 / BOMBFIANCOF1_G
  Else
    RR(0, 1) = 999999
    RR(0, 2) = 999999
  End If
  XX(0, 51) = XX(0, 25) + RR(0, 1) * Cos(ANGFIANCOF1_G)
  YY(0, 51) = YY(0, 25) + RR(0, 1) * Sin(ANGFIANCOF1_G)
  XX(0, 52) = XX(0, 25) + RR(0, 2) * Cos(ANGFIANCOF1_G)
  YY(0, 52) = YY(0, 25) + RR(0, 2) * Sin(ANGFIANCOF1_G)
  'SPOSTAMENTO CAVITA/BOMBATURA F1 SUL PRIMITIVO
  If (YY(0, 25) > 0) Then
    If (BOMBFIANCOF1_G > 0) Then
      DXX = XX(0, 6) - (Sqr(POT(RR(0, 2)) - POT(YY(0, 52))) + XX(0, 52))
    Else
      DXX = XX(0, 6) - (-Sqr(POT(RR(0, 2)) - POT(YY(0, 52))) + XX(0, 52))
    End If
  End If
  If (YY(0, 25) < 0) Then
    If (BOMBFIANCOF1_G > 0) Then
      DXX = XX(0, 6) - (Sqr(POT(RR(0, 1)) - POT(YY(0, 51))) + XX(0, 51))
    Else
      DXX = XX(0, 6) - (-Sqr(POT(RR(0, 1)) - POT(YY(0, 51))) + XX(0, 51))
    End If
  End If
  If (YY(0, 25) = 0) Then
    DXX = XX(0, 6) - XX(0, 25)
  End If
  XX(0, 7) = XX(0, 7) + DXX
  XX(0, 25) = XX(0, 25) + DXX
  XX(0, 51) = XX(0, 51) + DXX
  XX(0, 52) = XX(0, 52) + DXX
  YY(0, 3) = YY(0, 2)
  If ((RR(0, 1) > 0) And (YY(0, 51) < YY(0, 3))) Or ((RR(0, 1) < 0) And ((YY(0, 3) - YY(0, 51)) > Abs(RR(0, 1)))) Then
    'Write_Dialog "ERROR: Raccordo fondo F1 non possibile !"
    WRITE_DIALOG "ERROR: ROOT RADIUS NOT POSSIBLE !"
    Exit Sub
  End If
  XX(0, 3) = XX(0, 51) - RR(0, 1) * Cos(ASIN((YY(0, 3) - YY(0, 51)) / RR(0, 1)))
  YY(0, 8) = YY(0, 7) + RTESTA_G * (Sin(ANGFIANCOF1_G) - Cos(ANGRACCORDOTESTAF1_G))
  XX(0, 8) = XX(0, 7) + RTESTA_G * (Cos(ANGFIANCOF1_G) - Sin(ANGRACCORDOTESTAF1_G))
  'If (XX(0, 8) > (R37 / 2 - 0.5)) Then
  '  WRITE_DIALOG "ERROR: WHEEL WIDTH TOO SMALL - F1"
  '  Exit Sub
  'End If
  TEMPVAR = ATAN2((Abs((YY(0, 52) - YY(0, 8)) / (XX(0, 52) - XX(0, 8)))), 1)
  ang8 = PG / 2 - ANGRACCORDOTESTAF1_G - TEMPVAR
  PP8C2 = Sqr(POT(XX(0, 8) - XX(0, 52)) + POT(YY(0, 8) - YY(0, 52)))
  If (((RR(0, 2) > 0) And (PP8C2 > RR(0, 2))) Or ((RR(0, 2) < 0) And (YY(0, 52) > YY(0, 8)))) Then
    'Write_Dialog "ERROR: Raccordo testa F1 non possibile !"
    WRITE_DIALOG "ERROR: TIP RADIUS F1 NOT POSSIBLE !"
    Exit Sub
  End If
  PP0C2 = PP8C2 * Cos(ang8)
  PP0P78 = Sqr(POT(RR(0, 2)) - POT(PP0C2))
  ang78 = ASIN(PP0P78 / Abs(RR(0, 2))) + ANGRACCORDOTESTAF1_G
  XX(0, 28) = XX(0, 52) - RR(0, 2) * Sin(ang78)
  YY(0, 28) = YY(0, 52) - RR(0, 2) * Cos(ang78)
  XX(0, 9) = R37 / 2 + R197 '$TC_DP6[1,1]
  If (hSMUSSOF1 > 0) Then
    X8SMUSSOF1 = XX(0, 8) + hSMUSSOF1 * Cos(ANGRACCORDOTESTAF1_G)
    Y8SMUSSOF1 = YY(0, 8) - hSMUSSOF1
    X9SMUSSOF1 = X8SMUSSOF1 + RTESTA_G * Sin(ANGRACCORDOTESTAF1_G)
    Y9SMUSSOF1 = Y8SMUSSOF1 - RTESTA_G * (1 - Cos(ANGRACCORDOTESTAF1_G))
    YY(0, 9) = Y9SMUSSOF1
  Else
    YY(0, 9) = YY(0, 28) - (XX(0, 9) - XX(0, 28)) * Tan(ANGRACCORDOTESTAF1_G)
  End If
  XX(0, 1) = -XX(0, 3)
  'Fianco 2
  XX(0, 16) = -(LARGMOLAPR_G - R34) / 2
  YY(0, 12) = ALTEZZAFIANCOF2_G - ADDENDUMF2_G
  YY(0, 15) = YY(0, 12) - RFONDO2_G * (1 - Sin(ANGFIANCOF2_G))
  YY(0, 17) = -ADDENDUMF2_G + RTESTA2_G * (1 - Sin(ANGFIANCOF2_G))
  XX(0, 17) = XX(0, 16) + YY(0, 17) * Tan(ANGFIANCOF2_G)
  HFU = YY(0, 15) - YY(0, 17)
  '***********************
  YY(0, 26) = YY(0, 17) + HFU * POSBOMBFIANCOF2_G
  XX(0, 26) = XX(0, 17) + HFU * POSBOMBFIANCOF2_G * Tan(ANGFIANCOF2_G)
  LL1 = (YY(0, 15) - YY(0, 26)) / Cos(ANGFIANCOF2_G)
  LL2 = (YY(0, 26) - YY(0, 17)) / Cos(ANGFIANCOF2_G)
  YY(0, 26) = YY(0, 26) + BOMBFIANCOF2_G * Sin(ANGFIANCOF2_G)
  XX(0, 26) = XX(0, 26) - BOMBFIANCOF2_G * Cos(ANGFIANCOF2_G)
  '***********************
  If (BOMBFIANCOF2_G <> 0) Then
    RR(0, 11) = -(POT(LL1) + POT(BOMBFIANCOF2_G)) / 2 / BOMBFIANCOF2_G
    RR(0, 12) = -(POT(LL2) + POT(BOMBFIANCOF2_G)) / 2 / BOMBFIANCOF2_G
  Else
    RR(0, 11) = 999999
    RR(0, 12) = 999999
  End If
  XX(0, 61) = XX(0, 26) - RR(0, 11) * Cos(ANGFIANCOF2_G)
  YY(0, 61) = YY(0, 26) + RR(0, 11) * Sin(ANGFIANCOF2_G)
  XX(0, 62) = XX(0, 26) - RR(0, 12) * Cos(ANGFIANCOF2_G)
  YY(0, 62) = YY(0, 26) + RR(0, 12) * Sin(ANGFIANCOF2_G)
  'SPOSTAMENTO CAVITA/BOMBATURA F2 SUL PRIMITIVO
  If (YY(0, 26) > 0) Then
    If (BOMBFIANCOF2_G > 0) Then
      DXX = XX(0, 16) - (-Sqr(POT(RR(0, 12)) - POT(YY(0, 62))) + XX(0, 62))
    Else
      DXX = XX(0, 16) - (Sqr(POT(RR(0, 12)) - POT(YY(0, 62))) + XX(0, 62))
    End If
  End If
  If (YY(0, 26) < 0) Then
    If (BOMBFIANCOF2_G > 0) Then
      DXX = XX(0, 16) - (-Sqr(POT(RR(0, 11)) - POT(YY(0, 61))) + XX(0, 61))
    Else
      DXX = XX(0, 16) - (Sqr(POT(RR(0, 11)) - POT(YY(0, 61))) + XX(0, 61))
    End If
  End If
  If (YY(0, 26) = 0) Then
    DXX = XX(0, 16) - XX(0, 26)
  End If
  XX(0, 17) = XX(0, 17) + DXX
  XX(0, 26) = XX(0, 26) + DXX
  XX(0, 61) = XX(0, 61) + DXX
  XX(0, 62) = XX(0, 62) + DXX
  YY(0, 13) = YY(0, 12)
  If ((RR(0, 11) > 0) And (YY(0, 61) < YY(0, 13))) Or ((RR(0, 11) < 0) And ((YY(0, 13) - YY(0, 61)) > Abs(RR(0, 11)))) Then
    'Write_Dialog "ERROR: Raccordo fondo F2 non possibile !"
    WRITE_DIALOG "ERROR: ROOT RADIUS F2 NOT POSSIBLE !"
    Exit Sub
  End If
  XX(0, 13) = XX(0, 61) + RR(0, 11) * Cos(ASIN(Abs((YY(0, 13) - YY(0, 61)) / RR(0, 11))))
  YY(0, 18) = YY(0, 17) + RTESTA2_G * (Sin(ANGFIANCOF2_G) - Cos(ANGRACCORDOTESTAF2_G))
  XX(0, 18) = XX(0, 17) - RTESTA2_G * (Cos(ANGFIANCOF2_G) - Sin(ANGRACCORDOTESTAF2_G))
  'If ((Abs(XX(0, 18)) > (R37 / 2 - 0.5))) Then
  '  WRITE_DIALOG "ERROR: WHEEL WIDTH TOO SMALL - F2"
  '  Exit Sub
  'End If
  TEMPVAR = ATAN2((Abs((YY(0, 18) - YY(0, 62)) / (XX(0, 62) - XX(0, 18)))), 1)
  ang18 = PG / 2 - ANGRACCORDOTESTAF2_G - TEMPVAR
  PP18C12 = Sqr(POT(XX(0, 18) - XX(0, 62)) + POT(YY(0, 18) - YY(0, 62)))
  If (((RR(0, 12) > 0) And (PP18C12 > RR(0, 12))) Or ((RR(0, 12) < 0) And (YY(0, 62) > YY(0, 18)))) Then
    'Write_Dialog "ERROR: Raccordo testa F2 non possibile !"
    WRITE_DIALOG "ERROR: TIP RADIUS (F2) NOT POSSIBLE !"
    Exit Sub
  End If
  PP10C12 = PP18C12 * Cos(ang18)
  PP10P1718 = Sqr(POT(RR(0, 12)) - POT(PP10C12))
  ang1718 = ASIN(PP10P1718 / Abs(RR(0, 12))) + ANGRACCORDOTESTAF2_G
  XX(0, 27) = XX(0, 62) + RR(0, 12) * Sin(ang1718)
  YY(0, 27) = YY(0, 62) - RR(0, 12) * Cos(ang1718)
  XX(0, 19) = -R37 / 2 - R207 '$TC_DP6[1,2]
  If (hSMUSSOF2 > 0) Then
    X18SMUSSOF2 = XX(0, 18) - hSMUSSOF2 * Cos(ANGRACCORDOTESTAF2_G)
    Y18SMUSSOF2 = YY(0, 18) - hSMUSSOF2
    X19SMUSSOF2 = X18SMUSSOF2 - RTESTA2_G * Sin(ANGRACCORDOTESTAF2_G)
    Y19SMUSSOF2 = Y18SMUSSOF2 - RTESTA2_G * (1 - Cos(ANGRACCORDOTESTAF2_G))
    YY(0, 19) = Y19SMUSSOF2
  Else
    YY(0, 19) = YY(0, 27) + (XX(0, 19) - XX(0, 27)) * Tan(ANGRACCORDOTESTAF2_G)
  End If
  XX(0, 11) = -XX(0, 13)
  '
  'VISUALIZZAZIONE MOLA: EQUIVALENTE DEL PROGRAMMA PRFVIT.SPF
  '
  Dim X1            As Double
  Dim Y1            As Double
  Dim X2            As Double
  Dim Y2            As Double
  Dim X3            As Double
  Dim Y3            As Double
  Dim X4            As Double
  Dim Y4            As Double
  Dim Ymin          As Double
  Dim Dy            As Double
  Dim PicW          As Single
  Dim PicH          As Single
  Dim ScalaX        As Single
  Dim ScalaY        As Single
  Dim stmp          As String
  Dim TipoCarattere As String
  Dim M1            As Double
  Dim Q1            As Double
  Dim Q2            As Double
  Dim angc2         As Double
  Dim angc12        As Double
  Dim ang5          As Double
  Dim ang15         As Double
  Dim Xc7           As Double
  Dim Yc7           As Double
  Dim Xc17          As Double
  Dim Yc17          As Double
  Dim Xc3           As Double
  Dim Yc3           As Double
  Dim Xc13          As Double
  Dim Yc13          As Double
  Dim Raggio        As Double
  Dim Xc            As Double
  Dim YC            As Double
  Dim pX1           As Double
  Dim pY1           As Double
  Dim pX2           As Double
  Dim pY2           As Double
  Dim DeltaAng      As Double
  Dim Ang           As Double
  Dim SAPF1         As Double
  Dim SAPF2         As Double
  Dim EAPF1         As Double
  Dim EAPF2         As Double
  Dim EXTF1         As Double
  Dim EXTF2         As Double
  Const NumeroArchi = 50
  '
  'DEVO CALCOLARE I PUNTI DI TANGENZA PERCHE' SUL CNC SONO RACCORDATI
  '
  'Fianco 1
  'ricerca tangenza raggio di testa con testa e fianco
  M1 = Tan(ANGRACCORDOTESTAF1_G): Q1 = YY(0, 8) - M1 * XX(0, 8) 'retta uscita
  Y1 = YY(0, 28)
  Dy = 0.0001
  Do
    Y1 = Y1 + Dy
    angc2 = Asn((YY(0, 52) - Y1) / RR(0, 2))
    X1 = XX(0, 52) - RR(0, 2) * Cos(angc2)
    Xc7 = X1 + RTESTA_G * Cos(angc2)
    Yc7 = Y1 + RTESTA_G * Sin(angc2)
    Q2 = Yc7 - M1 * Xc7
  Loop While (Abs(Q2 - Q1) * Cos(ANGRACCORDOTESTAF1_G)) < RTESTA_G
  'xx(0, 7) = X1
  'YY(0, 7) = Y1
  'xx(0, 8) = Xc7 - RTESTA_G * Sin(ANGRACCORDOTESTAF1_G)
  'YY(0, 8) = Yc7 - RTESTA_G * Cos(ANGRACCORDOTESTAF1_G)
  '
  'ricerca tangenza raggio di fondo con fianco
  YY(0, 5) = YY(0, 2)
  Dy = 0.0001
  Do
    YY(0, 5) = YY(0, 5) - Dy
    ang5 = Asn((YY(0, 51) - YY(0, 5)) / RR(0, 1))
    Yc3 = YY(0, 5) - RFONDO_G * Sin(ang5)
  Loop While Yc3 > (YY(0, 2) - RFONDO_G)
  Xc3 = XX(0, 51) - (RR(0, 1) + RFONDO_G) * Cos(ang5)
  XX(0, 77) = XX(0, 51) - RR(0, 1) * Cos(ang5)
  YY(0, 77) = YY(0, 51) - RR(0, 1) * Sin(ang5)
  XX(0, 78) = Xc3
  YY(0, 78) = Yc3 + RFONDO_G
  '
  'Fianco 2
  'ricerca tangenza raggio di testa con testa e fianco
  M1 = -Tan(ANGRACCORDOTESTAF2_G): Q1 = YY(0, 18) - M1 * XX(0, 18)  'retta uscita
  Y1 = YY(0, 27)
  Dy = 0.0001
  Do
    Y1 = Y1 + Dy
    angc12 = Asn((YY(0, 62) - Y1) / RR(0, 12))
    X1 = XX(0, 62) + RR(0, 12) * Cos(angc12)
    Xc17 = X1 - RTESTA2_G * Cos(angc12)
    Yc17 = Y1 + RTESTA2_G * Sin(angc12)
    Q2 = Yc17 - M1 * Xc17
  Loop While (Abs(Q2 - Q1) * Cos(ANGRACCORDOTESTAF2_G)) < RTESTA2_G
  'xx(0, 17) = X1
  'YY(0, 17) = Y1
  'xx(0, 18) = Xc17 + RTESTA2_G * Sin(ANGRACCORDOTESTAF2_G)
  'YY(0, 18) = Yc17 - RTESTA2_G * Cos(ANGRACCORDOTESTAF2_G)
  '
  'ricerca tangenza raggio di fondo con fianco
  YY(0, 15) = YY(0, 12)
  Dy = 0.0001
  Do
    YY(0, 15) = YY(0, 15) - Dy
    ang15 = Asn((YY(0, 61) - YY(0, 15)) / RR(0, 11))
    Yc13 = YY(0, 15) - RFONDO2_G * Sin(ang15)
  Loop While Yc13 > (YY(0, 12) - RFONDO2_G)
  Xc13 = XX(0, 61) + (RR(0, 11) + RFONDO2_G) * Cos(ang15)
  XX(0, 87) = XX(0, 61) + RR(0, 11) * Cos(ang15)
  YY(0, 87) = YY(0, 61) - RR(0, 11) * Sin(ang15)
  XX(0, 88) = Xc13
  YY(0, 88) = Yc13 + RFONDO2_G
  '
  'UNIFORMO LA SCALA PER LA VISUALIZZAZIONE
  ScalaX = Scala: ScalaY = Scala
  'Acquisizione area grafica
  stmp = GetInfo("DIS0", "PicW", Path_LAVORAZIONE_INI)
  If (Len(stmp) > 0) Then PicW = val(stmp) Else PicW = 190
  stmp = GetInfo("DIS0", "PicH", Path_LAVORAZIONE_INI)
  If (Len(stmp) > 0) Then PicH = val(stmp) Else PicH = 95
  'TIPO DI CARATTERE PER I COMMENTI E VALORI
      If (LINGUA = "CH") Then
    TipoCarattere = "MS Song"
  ElseIf (LINGUA = "RU") Then
    TipoCarattere = "Arial Cyr"
  Else
    TipoCarattere = "Courier New"
  End If
  'Preparazione grafico
  If (SiStampa = "Y") Then
    For i = 1 To 2
      Printer.FontName = TipoCarattere
      Printer.FontSize = 8
      Printer.FontBold = False
    Next i
    WRITE_DIALOG Printer.DeviceName & " PRINTING....."
    If PicW = 190 And PicH = 95 Then
      Printer.Orientation = 1
    Else
      Printer.Orientation = 2
    End If
    Printer.ScaleMode = 6
  Else
    Controllo.Cls
    Controllo.ScaleWidth = PicW
    Controllo.ScaleHeight = PicH
    For i = 1 To 2
      Controllo.FontName = TipoCarattere
      Controllo.FontSize = 8
      Controllo.FontBold = False
    Next i
    Controllo.Refresh
  End If
  '
  If (SiStampa = "Y") Then
    Printer.Line (0, PicH / 2)-(PicW, PicH / 2)
    Printer.Line (PicW / 2, 0)-(PicW / 2, PicH)
  Else
    Controllo.Line (0, PicH / 2)-(PicW, PicH / 2)
    Controllo.Line (PicW / 2, 0)-(PicW / 2, PicH)
  End If
  '
  Dim ColoreCurva As Integer
  '
  'FIANCO 1
  ColoreCurva = 12
  '
  If (hSMUSSOF1 > 0) Then
  X1 = (-XX(0, 9) - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (YY(0, 9) - ZeroPezzoY) * ScalaY + PicH / 2
  X2 = (-X9SMUSSOF1 - ZeroPezzoX) * ScalaX + PicW / 2
  Y2 = (Y9SMUSSOF1 - ZeroPezzoY) * ScalaY + PicH / 2
  If (SiStampa = "Y") Then
    Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
  Else
    Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
  End If
  X1 = (-X8SMUSSOF1 - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (Y8SMUSSOF1 - ZeroPezzoY) * ScalaY + PicH / 2
  X2 = (-XX(0, 8) - ZeroPezzoX) * ScalaX + PicW / 2
  Y2 = (YY(0, 8) - ZeroPezzoY) * ScalaY + PicH / 2
  If (SiStampa = "Y") Then
    Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
  Else
    Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
  End If
  Else
  X1 = (-XX(0, 9) - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (YY(0, 9) - ZeroPezzoY) * ScalaY + PicH / 2
  X2 = (-XX(0, 8) - ZeroPezzoX) * ScalaX + PicW / 2
  Y2 = (YY(0, 8) - ZeroPezzoY) * ScalaY + PicH / 2
  If (SiStampa = "Y") Then
    Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
  Else
    Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
  End If
  End If
  '
  SAPF1 = R103 - YY(0, 77) * 2
  stmp = " " & frmt(SAPF1, 3) & " "
  X1 = (-XX(0, 77) - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (YY(0, 77) - ZeroPezzoY) * ScalaY + PicH / 2
  If (SiStampa = "Y") Then
    Printer.CurrentX = X1 - 5 - Controllo.TextWidth(stmp)
    Printer.CurrentY = Y1 - Printer.TextHeight(stmp) / 2
    Printer.Print stmp
    Printer.Line (X1 - 5, Y1)-(X1 + 5, Y1), QBColor(ColoreCurva)
  Else
    Controllo.CurrentX = X1 - 2 - Controllo.TextWidth(stmp)
    Controllo.CurrentY = Y1 - Controllo.TextHeight(stmp) / 2
    Controllo.Print stmp
    Controllo.Line (X1 - 2, Y1)-(X1 + 2, Y1), QBColor(ColoreCurva)
  End If
  '
  EAPF1 = R103 - YY(0, 7) * 2
  stmp = " " & frmt(EAPF1, 3) & " "
  X1 = (-XX(0, 7) - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (YY(0, 7) - ZeroPezzoY) * ScalaY + PicH / 2
  If (SiStampa = "Y") Then
    Printer.CurrentX = X1 - 5 - Controllo.TextWidth(stmp)
    Printer.CurrentY = Y1 - Printer.TextHeight(stmp) / 2
    Printer.Print stmp
    Printer.Line (X1 - 5, Y1)-(X1 + 5, Y1), QBColor(ColoreCurva)
  Else
    Controllo.CurrentX = X1 - 2 - Controllo.TextWidth(stmp)
    Controllo.CurrentY = Y1 - Controllo.TextHeight(stmp) / 2
    Controllo.Print stmp
    Controllo.Line (X1 - 2, Y1)-(X1 + 2, Y1), QBColor(ColoreCurva)
  End If
  '
  EXTF1 = R103 - YY(0, 8) * 2
  stmp = " " & frmt(EXTF1, 3) & " "
  X1 = (-XX(0, 8) - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (YY(0, 8) - ZeroPezzoY) * ScalaY + PicH / 2
  If (SiStampa = "Y") Then
    Printer.CurrentX = X1 - 5 - Controllo.TextWidth(stmp)
    Printer.CurrentY = Y1 - Printer.TextHeight(stmp) / 2
    Printer.Print stmp
    Printer.Line (X1 - 5, Y1)-(X1 + 5, Y1), QBColor(ColoreCurva)
  Else
    Controllo.CurrentX = X1 - 2 - Controllo.TextWidth(stmp)
    Controllo.CurrentY = Y1 - Controllo.TextHeight(stmp) / 2
    'Controllo.Print stmp
    Controllo.Line (X1 - 2, Y1)-(X1 + 2, Y1), QBColor(ColoreCurva)
  End If
  '
  If (SiSpezzata = 1) Then
    '
    X1 = (-XX(0, 9) - ZeroPezzoX) * ScalaX + PicW / 2
    Y1 = (YY(0, 9) - ZeroPezzoY) * ScalaY + PicH / 2
    X2 = (-XX(0, 28) - ZeroPezzoX) * ScalaX + PicW / 2
    Y2 = (YY(0, 28) - ZeroPezzoY) * ScalaY + PicH / 2
    If (SiStampa = "Y") Then
      Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    Else
      Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    End If
    '
    X1 = (-XX(0, 28) - ZeroPezzoX) * ScalaX + PicW / 2
    Y1 = (YY(0, 28) - ZeroPezzoY) * ScalaY + PicH / 2
    X2 = (-XX(0, 25) - ZeroPezzoX) * ScalaX + PicW / 2
    Y2 = (YY(0, 25) - ZeroPezzoY) * ScalaY + PicH / 2
    If (SiStampa = "Y") Then
      Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    Else
      Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    End If
    '
    X1 = (-XX(0, 7) - ZeroPezzoX) * ScalaX + PicW / 2
    Y1 = (YY(0, 7) - ZeroPezzoY) * ScalaY + PicH / 2
    X2 = (-XX(0, 25) - ZeroPezzoX) * ScalaX + PicW / 2
    Y2 = (YY(0, 25) - ZeroPezzoY) * ScalaY + PicH / 2
    If (SiStampa = "Y") Then
      Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    Else
      Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    End If
    '
    X1 = (-XX(0, 25) - ZeroPezzoX) * ScalaX + PicW / 2
    Y1 = (YY(0, 25) - ZeroPezzoY) * ScalaY + PicH / 2
    X2 = (-XX(0, 3) - ZeroPezzoX) * ScalaX + PicW / 2
    Y2 = (YY(0, 3) - ZeroPezzoY) * ScalaY + PicH / 2
    If (SiStampa = "Y") Then
      Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    Else
      Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    End If
  End If
  '
  X1 = (-XX(0, 3) - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (YY(0, 3) - ZeroPezzoY) * ScalaY + PicH / 2
  X2 = (-XX(0, 1) - ZeroPezzoX) * ScalaX + PicW / 2
  Y2 = (YY(0, 3) - ZeroPezzoY) * ScalaY + PicH / 2
  If (SiStampa = "Y") Then
    Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
  Else
    Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
  End If
  '
  If (hSMUSSOF1 > 0) Then
  Call RACCORDA(Controllo, Scala, -X9SMUSSOF1, -Y9SMUSSOF1, -X8SMUSSOF1, -Y8SMUSSOF1, RTESTA_G, "SX", SiStampa, ZeroPezzoX, -ZeroPezzoY)
  End If
  Call RACCORDA(Controllo, Scala, -XX(0, 8), -YY(0, 8), -XX(0, 7), -YY(0, 7), RTESTA_G, "SX", SiStampa, ZeroPezzoX, -ZeroPezzoY)
  If (BOMBFIANCOF1_G > 0) Then stmp = "DX" Else stmp = "SX"
  Call RACCORDA(Controllo, Scala, -XX(0, 7), -YY(0, 7), -XX(0, 25), -YY(0, 25), RR(0, 2), stmp, SiStampa, ZeroPezzoX, -ZeroPezzoY)
  Call RACCORDA(Controllo, Scala, -XX(0, 25), -YY(0, 25), -XX(0, 77), -YY(0, 77), RR(0, 1), stmp, SiStampa, ZeroPezzoX, -ZeroPezzoY)
  Call RACCORDA(Controllo, Scala, -XX(0, 77), -YY(0, 77), -XX(0, 78), -YY(0, 78), RFONDO_G, "DX", SiStampa, ZeroPezzoX, -ZeroPezzoY)
  '
  'FIANCO 2
  ColoreCurva = 9
  '
  If (hSMUSSOF2 > 0) Then
  X1 = (-XX(0, 19) - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (YY(0, 19) - ZeroPezzoY) * ScalaY + PicH / 2
  X2 = (-X19SMUSSOF2 - ZeroPezzoX) * ScalaX + PicW / 2
  Y2 = (Y19SMUSSOF2 - ZeroPezzoY) * ScalaY + PicH / 2
  If (SiStampa = "Y") Then
    Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
  Else
    Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
  End If
  X1 = (-X18SMUSSOF2 - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (Y18SMUSSOF2 - ZeroPezzoY) * ScalaY + PicH / 2
  X2 = (-XX(0, 18) - ZeroPezzoX) * ScalaX + PicW / 2
  Y2 = (YY(0, 18) - ZeroPezzoY) * ScalaY + PicH / 2
  If (SiStampa = "Y") Then
    Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
  Else
    Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
  End If
  Else
  X1 = (-XX(0, 19) - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (YY(0, 19) - ZeroPezzoY) * ScalaY + PicH / 2
  X2 = (-XX(0, 18) - ZeroPezzoX) * ScalaX + PicW / 2
  Y2 = (YY(0, 18) - ZeroPezzoY) * ScalaY + PicH / 2
  If (SiStampa = "Y") Then
    Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
  Else
    Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
  End If
  End If
  '
  SAPF2 = R103 - YY(0, 87) * 2
  stmp = " " & frmt(SAPF2, 3) & " "
  X1 = (-XX(0, 87) - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (YY(0, 87) - ZeroPezzoY) * ScalaY + PicH / 2
  If (SiStampa = "Y") Then
    Printer.CurrentX = X1 + 5
    Printer.CurrentY = Y1 - Printer.TextHeight(stmp) / 2
    Printer.Print stmp
    Printer.Line (X1 - 5, Y1)-(X1 + 5, Y1), QBColor(ColoreCurva)
  Else
    Controllo.CurrentX = X1 + 2
    Controllo.CurrentY = Y1 - Controllo.TextHeight(stmp) / 2
    Controllo.Print stmp
    Controllo.Line (X1 - 2, Y1)-(X1 + 2, Y1), QBColor(ColoreCurva)
  End If
  '
  EAPF2 = R103 - YY(0, 17) * 2
  stmp = " " & frmt(EAPF2, 3) & " "
  X1 = (-XX(0, 17) - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (YY(0, 17) - ZeroPezzoY) * ScalaY + PicH / 2
  If (SiStampa = "Y") Then
    Printer.CurrentX = X1 + 5
    Printer.CurrentY = Y1 - Printer.TextHeight(stmp) / 2
    Printer.Print stmp
    Printer.Line (X1 - 5, Y1)-(X1 + 5, Y1), QBColor(ColoreCurva)
  Else
    Controllo.CurrentX = X1 + 2
    Controllo.CurrentY = Y1 - Controllo.TextHeight(stmp) / 2
    Controllo.Print stmp
    Controllo.Line (X1 - 2, Y1)-(X1 + 2, Y1), QBColor(ColoreCurva)
  End If
  '
  EXTF2 = R103 - YY(0, 18) * 2
  stmp = " " & frmt(EXTF2, 3) & " "
  X1 = (-XX(0, 18) - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (YY(0, 18) - ZeroPezzoY) * ScalaY + PicH / 2
  If (SiStampa = "Y") Then
    Printer.CurrentX = X1 + 5
    Printer.CurrentY = Y1 - Printer.TextHeight(stmp) / 2
    Printer.Print stmp
    Printer.Line (X1 - 5, Y1)-(X1 + 5, Y1), QBColor(ColoreCurva)
  Else
    Controllo.CurrentX = X1 + 2
    Controllo.CurrentY = Y1 - Controllo.TextHeight(stmp) / 2
    'Controllo.Print stmp
    Controllo.Line (X1 - 2, Y1)-(X1 + 2, Y1), QBColor(ColoreCurva)
  End If
  '
  If (SiSpezzata = 1) Then
    '
    X1 = (-XX(0, 19) - ZeroPezzoX) * ScalaX + PicW / 2
    Y1 = (YY(0, 19) - ZeroPezzoY) * ScalaY + PicH / 2
    X2 = (-XX(0, 27) - ZeroPezzoX) * ScalaX + PicW / 2
    Y2 = (YY(0, 27) - ZeroPezzoY) * ScalaY + PicH / 2
    If (SiStampa = "Y") Then
      Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    Else
      Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    End If
    '
    X1 = (-XX(0, 27) - ZeroPezzoX) * ScalaX + PicW / 2
    Y1 = (YY(0, 27) - ZeroPezzoY) * ScalaY + PicH / 2
    X2 = (-XX(0, 26) - ZeroPezzoX) * ScalaX + PicW / 2
    Y2 = (YY(0, 26) - ZeroPezzoY) * ScalaY + PicH / 2
    If (SiStampa = "Y") Then
      Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    Else
      Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    End If
    '
    X1 = (-XX(0, 17) - ZeroPezzoX) * ScalaX + PicW / 2
    Y1 = (YY(0, 17) - ZeroPezzoY) * ScalaY + PicH / 2
    X2 = (-XX(0, 26) - ZeroPezzoX) * ScalaX + PicW / 2
    Y2 = (YY(0, 26) - ZeroPezzoY) * ScalaY + PicH / 2
    If (SiStampa = "Y") Then
      Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    Else
      Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    End If
    '
    X1 = (-XX(0, 26) - ZeroPezzoX) * ScalaX + PicW / 2
    Y1 = (YY(0, 26) - ZeroPezzoY) * ScalaY + PicH / 2
    X2 = (-XX(0, 13) - ZeroPezzoX) * ScalaX + PicW / 2
    Y2 = (YY(0, 13) - ZeroPezzoY) * ScalaY + PicH / 2
    If (SiStampa = "Y") Then
    Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    Else
      Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    End If
  End If
  '
  X1 = (-XX(0, 13) - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (YY(0, 13) - ZeroPezzoY) * ScalaY + PicH / 2
  X2 = (-XX(0, 11) - ZeroPezzoX) * ScalaX + PicW / 2
  Y2 = (YY(0, 13) - ZeroPezzoY) * ScalaY + PicH / 2
  If (SiStampa = "Y") Then
    Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
  Else
    Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
  End If
  If (hSMUSSOF2 > 0) Then
  Call RACCORDA(Controllo, Scala, -X19SMUSSOF2, -Y19SMUSSOF2, -X18SMUSSOF2, -Y18SMUSSOF2, RTESTA2_G, "DX", SiStampa, ZeroPezzoX, -ZeroPezzoY)
  End If
  Call RACCORDA(Controllo, Scala, -XX(0, 18), -YY(0, 18), -XX(0, 17), -YY(0, 17), RTESTA2_G, "DX", SiStampa, ZeroPezzoX, -ZeroPezzoY)
  If (BOMBFIANCOF2_G > 0) Then stmp = "SX" Else stmp = "DX"
  Call RACCORDA(Controllo, Scala, -XX(0, 17), -YY(0, 17), -XX(0, 26), -YY(0, 26), RR(0, 12), stmp, SiStampa, ZeroPezzoX, -ZeroPezzoY)
  Call RACCORDA(Controllo, Scala, -XX(0, 26), -YY(0, 26), -XX(0, 87), -YY(0, 87), RR(0, 11), stmp, SiStampa, ZeroPezzoX, -ZeroPezzoY)
  Call RACCORDA(Controllo, Scala, -XX(0, 87), -YY(0, 87), -XX(0, 88), -YY(0, 88), RFONDO2_G, "SX", SiStampa, ZeroPezzoX, -ZeroPezzoY)
  '
  'FIANCO 1
  'Raggio = RTESTA_G
  'Xc = -(xx(0, 8) + RTESTA_G * Sin(ANGRACCORDOTESTAF1_G))
  'YC = -(YY(0, 8) + RTESTA_G * Cos(ANGRACCORDOTESTAF1_G))
  'GoSub DISEGNA_CERCHIO
  '
  'FIANCO 2
  'Raggio = RTESTA_G
  'Xc = -(xx(0, 18) + RTESTA_G * Sin(ANGRACCORDOTESTAF2_G))
  'YC = -(YY(0, 18) + RTESTA_G * Cos(ANGRACCORDOTESTAF2_G))
  'GoSub DISEGNA_CERCHIO
  '
  Const AF = 30
  Const DF = 2
  Const NumeroRette = 30
  '
  'TESTA DELLA MOLA
  X1 = (-XX(0, 9) - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (YY(0, 9) - ZeroPezzoY) * ScalaY + PicH / 2
  Y3 = (YY(0, 9) - 0.5 - ZeroPezzoY) * ScalaY + PicH / 2
  X2 = (-XX(0, 19) - ZeroPezzoX) * ScalaX + PicW / 2
  Y2 = (YY(0, 19) - ZeroPezzoY) * ScalaY + PicH / 2
  Y4 = (YY(0, 19) - 0.5 - ZeroPezzoY) * ScalaY + PicH / 2
  If Y4 < Y3 Then Y3 = Y4 Else Y4 = Y3
  stmp = " " & frmt(R37, 3) & " "
  If (SiStampa = "Y") Then
    Printer.CurrentX = PicW / 2 - Printer.TextWidth(stmp) / 2
    Printer.CurrentY = Y3 - Printer.TextHeight(stmp)
    Printer.Print stmp
    Printer.Line (X1, Y3)-(X2, Y4), QBColor(12)
    Printer.Line (X1, Y3)-(X1, Y1), QBColor(12)
    Printer.Line (X2, Y4)-(X2, Y2), QBColor(12)
  Else
    Controllo.CurrentX = PicW / 2 - Controllo.TextWidth(stmp) / 2
    Controllo.CurrentY = Y3 - Controllo.TextHeight(stmp)
    Controllo.Print stmp
    Controllo.Line (X1, Y3)-(X2, Y4), QBColor(12)
    Controllo.Line (X1, Y3)-(X1, Y1), QBColor(12)
    Controllo.Line (X2, Y4)-(X2, Y2), QBColor(12)
  End If
  '
  ColoreCurva = 12
  'Freccia a sinistra del testo
  Ang = AF / 180 * PG
  DeltaAng = Ang / NumeroRette
  X1 = (-XX(0, 9) - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (YY(0, 9) - 0.5 - ZeroPezzoY) * ScalaY + PicH / 2
  For i = -NumeroRette To NumeroRette
    X2 = (-XX(0, 9) - ZeroPezzoX) * ScalaX + DF * Cos(Ang) + PicW / 2
    Y2 = (YY(0, 9) - 0.5 - ZeroPezzoY) * ScalaY - DF * Sin(i * DeltaAng) + PicH / 2
    If (SiStampa = "Y") Then
      Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    Else
      Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    End If
  Next i
  'Freccia a destra del testo
  Ang = AF / 180 * PG
  DeltaAng = Ang / NumeroRette
  X1 = (-XX(0, 19) - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (YY(0, 19) - 0.5 - ZeroPezzoY) * ScalaY + PicH / 2
  For i = -NumeroRette To NumeroRette
    X2 = (-XX(0, 19) - ZeroPezzoX) * ScalaX - DF * Cos(Ang) + PicW / 2
    Y2 = (YY(0, 19) - 0.5 - ZeroPezzoY) * ScalaY - DF * Sin(i * DeltaAng) + PicH / 2
    If (SiStampa = "Y") Then
      Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    Else
      Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    End If
  Next i
  '
  'SPESSORE DELLA MOLA
  ColoreCurva = 0
  X1 = (-XX(0, 6) - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (YY(0, 6) - ZeroPezzoY) * ScalaY + PicH / 2
  X2 = (-XX(0, 16) - ZeroPezzoX) * ScalaX + PicW / 2
  Y2 = (YY(0, 16) - ZeroPezzoY) * ScalaY + PicH / 2
  If Y2 < Y1 Then Y1 = Y2 Else Y2 = Y1
  stmp = " " & frmt(XX(0, 6) - XX(0, 16), 3) & " "
  If (SiStampa = "Y") Then
    Printer.CurrentX = PicW / 2 - Printer.TextWidth(stmp) / 2
    Printer.CurrentY = Y1 - Printer.TextHeight(stmp)
    Printer.Print stmp
    Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
  Else
    Controllo.CurrentX = PicW / 2 - Controllo.TextWidth(stmp) / 2
    Controllo.CurrentY = Y1 - Controllo.TextHeight(stmp)
    Controllo.Print stmp
    Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
  End If
  'Freccia a sinistra del testo
  Ang = AF / 180 * PG
  DeltaAng = Ang / NumeroRette
  X1 = (-XX(0, 6) - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (YY(0, 6) - ZeroPezzoY) * ScalaY + PicH / 2
  For i = -NumeroRette To NumeroRette
    X2 = (-XX(0, 6) - ZeroPezzoX) * ScalaX + DF * Cos(Ang) + PicW / 2
    Y2 = (YY(0, 6) - ZeroPezzoY) * ScalaY - DF * Sin(i * DeltaAng) + PicH / 2
    If (SiStampa = "Y") Then
      Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    Else
      Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    End If
  Next i
  If (SiStampa = "Y") Then
    Printer.Line (X1, Y1 + DF / 2)-(X1, Y1 - DF / 2), QBColor(ColoreCurva)
  Else
    Controllo.Line (X1, Y1 + DF / 2)-(X1, Y1 - DF / 2), QBColor(ColoreCurva)
  End If
  'Freccia a destra del testo
  Ang = AF / 180 * PG
  DeltaAng = Ang / NumeroRette
  X1 = (-XX(0, 16) - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (YY(0, 16) - ZeroPezzoY) * ScalaY + PicH / 2
  For i = -NumeroRette To NumeroRette
    X2 = (-XX(0, 16) - ZeroPezzoX) * ScalaX - DF * Cos(Ang) + PicW / 2
    Y2 = (YY(0, 16) - ZeroPezzoY) * ScalaY - DF * Sin(i * DeltaAng) + PicH / 2
    If (SiStampa = "Y") Then
      Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    Else
      Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    End If
  Next i
  If (SiStampa = "Y") Then
    Printer.Line (X1, Y1 + DF / 2)-(X1, Y1 - DF / 2), QBColor(ColoreCurva)
  Else
    Controllo.Line (X1, Y1 + DF / 2)-(X1, Y1 - DF / 2), QBColor(ColoreCurva)
  End If
  '
  'FONDO DELLA MOLA
  X1 = (-XX(0, 78) - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (YY(0, 78) - ZeroPezzoY) * ScalaY + PicH / 2
  Y3 = (YY(0, 78) + 1 - ZeroPezzoY) * ScalaY + PicH / 2
  X2 = (-XX(0, 88) - ZeroPezzoX) * ScalaX + PicW / 2
  Y2 = (YY(0, 88) - ZeroPezzoY) * ScalaY + PicH / 2
  Y4 = (YY(0, 88) + 1 - ZeroPezzoY) * ScalaY + PicH / 2
  If Y4 < Y3 Then Y3 = Y4 Else Y4 = Y3
  stmp = " " & frmt(XX(0, 78) - XX(0, 88), 2) & " "
  If (SiStampa = "Y") Then
    Printer.CurrentX = PicW / 2 - Printer.TextWidth(stmp) / 2
    Printer.CurrentY = Y3 - Printer.TextHeight(stmp)
    Printer.Print stmp
    Printer.Line (X1, Y3)-(X2, Y4), QBColor(12)
    Printer.Line (X1, Y3)-(X1, Y1), QBColor(12)
    Printer.Line (X2, Y4)-(X2, Y2), QBColor(12)
  Else
    Controllo.CurrentX = PicW / 2 - Controllo.TextWidth(stmp) / 2
    Controllo.CurrentY = Y3 - Controllo.TextHeight(stmp)
    Controllo.Print stmp
    Controllo.Line (X1, Y3)-(X2, Y4), QBColor(12)
    Controllo.Line (X1, Y3)-(X1, Y1), QBColor(12)
    Controllo.Line (X2, Y4)-(X2, Y2), QBColor(12)
  End If
  ColoreCurva = 12
  'Freccia a sinistra del testo
  Ang = AF / 180 * PG
  DeltaAng = Ang / NumeroRette
  X1 = (-XX(0, 78) - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (YY(0, 78) + 1 - ZeroPezzoY) * ScalaY + PicH / 2
  For i = -NumeroRette To NumeroRette
    X2 = (-XX(0, 78) - ZeroPezzoX) * ScalaX + DF * Cos(Ang) + PicW / 2
    Y2 = (YY(0, 78) + 1 - ZeroPezzoY) * ScalaY - DF * Sin(i * DeltaAng) + PicH / 2
    If (SiStampa = "Y") Then
      Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    Else
      Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    End If
  Next i
  'Freccia a destra del testo
  Ang = AF / 180 * PG
  DeltaAng = Ang / NumeroRette
  X1 = (-XX(0, 88) - ZeroPezzoX) * ScalaX + PicW / 2
  Y1 = (YY(0, 88) + 1 - ZeroPezzoY) * ScalaY + PicH / 2
  For i = -NumeroRette To NumeroRette
    X2 = (-XX(0, 88) - ZeroPezzoX) * ScalaX - DF * Cos(Ang) + PicW / 2
    Y2 = (YY(0, 88) + 1 - ZeroPezzoY) * ScalaY - DF * Sin(i * DeltaAng) + PicH / 2
    If (SiStampa = "Y") Then
      Printer.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    Else
      Controllo.Line (X1, Y1)-(X2, Y2), QBColor(ColoreCurva)
    End If
  Next i
  '
  If (SiStampa = "Y") Then
    'FIANCO 1
    stmp = " F1 "
    Printer.CurrentX = 0
    Printer.CurrentY = 0
    Printer.Print stmp
    If (RR(0, 1) <> 999999) And (RR(0, 2) <> 999999) Then
      stmp = " RR[0,1] = " & frmt(RR(0, 1), 2) & " "
      Printer.CurrentX = 0
      Printer.CurrentY = PicH / 2 - 2 * Printer.TextHeight(stmp)
      Printer.Print stmp
      stmp = " RR[0,2] = " & frmt(RR(0, 2), 2) & " "
      Printer.CurrentX = 0
      Printer.CurrentY = PicH / 2 - 1 * Printer.TextHeight(stmp)
      Printer.Print stmp
    End If
    'FIANCO 2
    stmp = " F2 "
    Printer.CurrentX = PicW - Printer.TextWidth(stmp)
    Printer.CurrentY = 0
    Printer.Print stmp
    If (RR(0, 11) <> 999999) And (RR(0, 12) <> 999999) Then
      stmp = " RR[0,11] = " & frmt(RR(0, 11), 2) & " "
      Printer.CurrentX = PicW - Printer.TextWidth(stmp)
      Printer.CurrentY = PicH / 2 - 2 * Printer.TextHeight(stmp)
      Printer.Print stmp
      stmp = " RR[0,12] = " & frmt(RR(0, 12), 2) & " "
      Printer.CurrentX = PicW - Printer.TextWidth(stmp)
      Printer.CurrentY = PicH / 2 - 1 * Printer.TextHeight(stmp)
      Printer.Print stmp
    End If
  Else
    'FIANCO 1
    stmp = " F1 "
    Controllo.CurrentX = 0
    Controllo.CurrentY = 0
    Controllo.Print stmp
    If (RR(0, 1) <> 999999) And (RR(0, 2) <> 999999) Then
      stmp = " RR[0,1] = " & frmt(RR(0, 1), 2) & " "
      Controllo.CurrentX = 0
      Controllo.CurrentY = PicH / 2 - 2 * Controllo.TextHeight(stmp)
      Controllo.Print stmp
      stmp = " RR[0,2] = " & frmt(RR(0, 2), 2) & " "
      Controllo.CurrentX = 0
      Controllo.CurrentY = PicH / 2 - 1 * Controllo.TextHeight(stmp)
      Controllo.Print stmp
    End If
    'FIANCO 2
    stmp = " F2 "
    Controllo.CurrentX = PicW - Controllo.TextWidth(stmp)
    Controllo.CurrentY = 0
    Controllo.Print stmp
    If (RR(0, 11) <> 999999) And (RR(0, 12) <> 999999) Then
      stmp = " RR[0,11] = " & frmt(RR(0, 11), 2) & " "
      Controllo.CurrentX = PicW - Controllo.TextWidth(stmp)
      Controllo.CurrentY = PicH / 2 - 2 * Controllo.TextHeight(stmp)
      Controllo.Print stmp
      stmp = " RR[0,12] = " & frmt(RR(0, 12), 2) & " "
      Controllo.CurrentX = PicW - Controllo.TextWidth(stmp)
      Controllo.CurrentY = PicH / 2 - 1 * Controllo.TextHeight(stmp)
      Controllo.Print stmp
    End If
  End If
  
  Dim ret As Integer
  
  ret = WritePrivateProfileString("OEM1", "X[0,8]", frmt(XX(0, 8), 3), Path_LAVORAZIONE_INI)
  ret = WritePrivateProfileString("OEM1", "Y[0,8]", frmt(YY(0, 8), 3), Path_LAVORAZIONE_INI)
  ret = WritePrivateProfileString("OEM1", "X[0,9]", frmt(XX(0, 9), 3), Path_LAVORAZIONE_INI)
  ret = WritePrivateProfileString("OEM1", "Y[0,9]", frmt(YY(0, 9), 3), Path_LAVORAZIONE_INI)
  
  '
  'Segnalazione della fine delle operazioni
  If (SiStampa = "Y") Then GoSub FineDocumento
  '
Exit Sub

errVITI_ESTERNE_VISUALIZZA_MOLA:
  WRITE_DIALOG "SUB : VITI_ESTERNE_VISUALIZZA_MOLA " & Error(Err)
  Exit Sub

DISEGNA_CERCHIO:
  pX1 = Xc + Raggio: pY1 = YC: DeltaAng = 2 * PG / NumeroArchi
  For j = 1 To NumeroArchi
    Ang = j * DeltaAng
    pX2 = Xc + Raggio * Cos(Ang): pY2 = YC + Raggio * Sin(Ang)
    'Adattamento nella finestra grafica
    pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
    'Traccio la linea
    If (SiStampa = "Y") Then
      If (pY1 <= Printer.ScaleHeight) And (pX1 <= Printer.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
        Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
      End If
    Else
      If (pY1 <= Controllo.ScaleHeight) And (pX1 <= Controllo.ScaleWidth) And (pX1 >= 0) And (pY1 >= 0) Then
        Controllo.Line (pX1, pY1)-(pX2, pY2), QBColor(ColoreCurva)
      End If
    End If
    'Aggiorno Px1 e Py1
    pX1 = Xc + Raggio * Cos(Ang): pY1 = YC + Raggio * Sin(Ang)
  Next j
Return

FineDocumento:
  'Dimensione dei caratteri
  For i = 1 To 2
    Printer.FontName = TipoCarattere
    Printer.FontSize = 14
    Printer.FontBold = True
  Next i
  'Logo
  'Printer.PaintPicture OEM1.PicLogo, 0, PicH, 20, 20
  'Nome del documento
  stmp = Date & " " & Time
  Printer.CurrentX = 0 + 25
  Printer.CurrentY = PicH
  Printer.Print stmp
  'Scala X
  If OEMX.lblINP(0).Visible = True Then
    stmp = OEMX.lblINP(0).Caption & ": x" & OEMX.txtINP(0).Text
    Printer.CurrentX = 0 + 25
    Printer.CurrentY = PicH + 10
    Printer.Print stmp
  End If
  'Scala Y
  If OEMX.lblINP(1).Visible = True Then
    stmp = OEMX.lblINP(1).Caption & ": x" & OEMX.txtINP(1).Text
    Printer.CurrentX = 0 + PicW / 2
    Printer.CurrentY = PicH + 10
    Printer.Print stmp
  End If
  'Parametri
  OEMX.SuOEM1.Visible = True
  Call OEMX.SuOEM1.STAMPA(False, , , , 150)
  OEMX.SuOEM1.Visible = False
  Printer.EndDoc
  WRITE_DIALOG ""
  '
Return

End Sub

Function ASIN(ByVal Valore As Double) As Double
'
On Error Resume Next
  '
  ASIN = Asn(Valore)
  '
End Function

Function ATAN2(ByVal SENO As Double, ByVal COSENO As Double) As Double
'
On Error Resume Next
  '
  ATAN2 = Atn(SENO / COSENO)
  '
End Function

Function POT(ByVal Valore As Double) As Double
'
On Error Resume Next
  '
  POT = Valore * Valore
  '
End Function

Function Acs(Aux As Double) As Double
'
Dim Aux1 As Double
'
On Error Resume Next
  '
  Aux1 = (1 - Aux * Aux)
  If (Aux = 0) Then
    Acs = PG / 2
  Else
    If (Aux > 0) Then
      Acs = Atn(Sqr(1 - Aux * Aux) / Aux)
    Else
      Aux = Abs(Aux)
      Acs = Atn(Aux / Sqr(1 - Aux * Aux)) + PG / 2
    End If
  End If
  '
End Function

Function Asn(Aux As Double) As Double
'
Dim MyAux1 As Double
'
On Error Resume Next
  '
  MyAux1 = (1 - Aux * Aux)
  If (Aux = 1) Then Asn = PG / 2 Else Asn = Atn(Aux / Sqr(MyAux1))
  '
End Function

Function FnGrad(A As Double) As Double
'
On Error Resume Next
  '
  FnGrad = A / PG * 180
  '
End Function

Function inv(Aux As Double) As Double
'
On Error Resume Next
  '
  inv = Tan(Aux) - Aux
  '
End Function

Function Ainv(ByVal Aux As Double) As Double
'
Dim i      As Integer
Dim Ymini  As Double
Dim Ymaxi  As Double
Dim Ymoyen As Double
Dim X1     As Double
'
On Error Resume Next
  '
  Ymini = 0: Ymaxi = PG / 2
  For i = 1 To 25
    Ymoyen = (Ymini + Ymaxi) / 2: X1 = inv(Ymoyen)
    If X1 < Aux Then Ymini = Ymoyen Else: Ymaxi = Ymoyen
  Next i
  Ainv = Ymoyen
  '
End Function

Function FnRad(ByVal A As Double) As Double
'
On Error Resume Next
  '
  FnRad = A / 180 * PG
  '
End Function

Public Function CALCOLO_VITE_ZA() As Boolean
'
Dim hu1     As Double
Dim PM      As Integer
Dim PmB     As Integer
Dim Bmax    As Double
Dim XX      As Double
Dim Aux1    As Double
Dim Aux2    As Double
Dim Aux3    As Double
Dim da      As Double
Dim A       As Double
Dim XCF     As Double
Dim YCF     As Double
Dim RFM     As Double
Dim Sonx    As Double
Dim E38     As Double
Dim AngMed  As Double
Dim E43     As Double
Dim E44     As Double
Dim PB      As Double
Dim BOMB    As Double
Dim Dia_Ext As Double
Dim tangn   As Double
'
Const nPr = 10
'
On Error GoTo errCALCOLO_VITE_ZA
  '
  CALCOLO_VITE_ZA = False
  '(*i*)
  '(*f*)
  DiaSmusso = DEXTV
  '(*i*)
  Dia_Ext = DiaSmusso
  '(*f*)
  ApNormV = Atn(Tan(ApAssVRad) * Cos(BetaVRad))
  SONt = SpNormV / Cos(BetaVRad)
  DiaAttivoPiede = DIntV + 2 * RF * (1 - Sin(ApNormVrad))    'SAP
  IL = (DiaMolEff + DIntV) / 2       'interasse
  PassoElica = MnormV * PG * NFil / Sin(BetaIngRad)
  '
  Np = NPE
  '
  p1 = 0
  'calcolo fianco mola
  '(*i*)
  DiaSmusso = Dia_Ext - 2 * (RT * (1 - Sin(ApAssVRad)))
  '(*f*)
  XX = (DiaSmusso - DiaAttivoPiede) / (Np - 1) / 2
  For i = 0 To Np - 1
    RX = DiaAttivoPiede / 2 + XX * i
    CALCOLO_T0_FI0_A    'calcolo punto ZA (assiale)
    Call CALCOLO_PUNTO_MOLA(IL, BetaVRad, PassoElica, RX, T0, FI0, X(i), Y(i), B(i)) 'calcolo punto mola Xm,Ym
  Next i
  'punto su Primitivo
  RX = Rprim
  MM(i) = Sqr(RX ^ 2 - Rb ^ 2)
  CALCOLO_T0_FI0_A
  Call CALCOLO_PUNTO_MOLA(IL, BetaVRad, PassoElica, RX, T0, FI0, X(i), Y(i), B(i)) 'calcolo punto mola Xm,Ym
  ' Ricerca punto SAP
  i = 0
  DILAV = 0
  While DILAV < DIntV And i < Np
    DILAV = IL * 2 - (Y(i) + RF * (1 - Sin(B(i) * PG / 180))) * 2
    i = i + 1
  Wend
  If i > 1 Then
    p1 = i - 2  ' Punto del SAP
  Else
    p1 = i - 1
  End If
  BSap = (B(p1) + B(p1 + 1)) / 2  'Tangente al SAP
  YSap = IL - DIntV / 2 - RF * (1 - Sin(BSap * PG / 180))
  XSap = X(p1 + 1) + (Y(p1 + 1) - YSap) * Tan(BSap * PG / 180)
  X(p1) = XSap: Y(p1) = YSap: B(p1) = BSap
  Call CALCOLO_VITE_DA_MOLA(IL, BetaVRad, PassoElica, YP1(p1), Aux1, Aux2, XP1(p1), Bp1(p1), X(p1), Y(p1), FnRad(B(p1)), Aux3)
  SAP = Sqr(XP1(p1) ^ 2 + YP1(p1) ^ 2) * 2
  'Spessore
  E38 = X(Np) * 2
  'Testa
  XCT = X(Np - 1) + RT * Cos(FnRad(B(Np - 1)))
  YCT = Y(Np - 1) + RT * Sin(FnRad(B(Np - 1)))
  'Addendum,altezza
  E43 = Y(Np) - (YCT - RT)
  E44 = IL - DIntV / 2 - YCT + RT
  For i = Np - 1 To p1 Step -1: Y(i) = Y(p1) - Y(i): Next i
  hu1 = Y(Np - 1) - Y(p1)
  AngMed = Atn((X(Np - 1) - X(p1)) / hu1)
  PM = Int((Np - p1) / 2)
  PmB = 0: Bmax = 0
  For i = p1 + 1 To Np - 2
    XX = X(i) - X(p1) - Y(i) * Tan(AngMed)
    If Abs(XX) > Abs(Bmax) Then Bmax = XX: PmB = i
  Next i
  BOMB = -(Bmax) * Cos(AngMed)         '+ : cavit�, - : bombatura
  PB = 1 - (Y(PmB) - Y(p1)) / hu1     'posizione bombatura/cavit�
  If BOMB = 0 Then PB = 0.5
  If PB > 0.9 Then PB = 0.9
  If PB < 0.1 Then PB = 0.1
  'Raggio testa/fondo
  For i = 1 To nPr
    da = (PG / 2 - B(Np - 1) * PG / 180) / nPr
    A = (PG / 2 - B(Np - 1) * PG / 180) - da * i
    X(Np - 1 + i) = XCT - RT * Sin(A)
    Y(Np - 1 + i) = YSap - (YCT - RT * Cos(A))
  Next i
  XCF = X(p1) - RF * Cos(BSap * PG / 180)
  YCF = Y(p1) + RF * Sin(BSap * PG / 180)
  For i = 1 To nPr
    da = (PG / 2 - B(p1) * PG / 180) / nPr
    A = (PG / 2 - B(p1) * PG / 180) - da * i
    X(Np - 1 + nPr + i) = XCF + RF * Sin(A)
    Y(Np - 1 + nPr + i) = YCF - RF * Cos(A)
  Next i
  '
  'registra punti su file
  Open g_chOemPATH & "\" & FileTEMPORANEO For Output As #1
    '
    For i = p2 + 1 + 2 * nPr - 1 To nPr + p2 + 1 Step -1
      Print #1, X(i), Y(i)
    Next i
    For i = p1 To p2
      Print #1, X(i), Y(i)
    Next i
    For i = p2 + 1 To nPr + p2
      Print #1, X(i), Y(i)
    Next i
    '
  Close #1
  '
  Np = p2 + 1 + nPr * 2 - p1
  '
  Open g_chOemPATH & "\" & FileTEMPORANEO For Input As #1
    For i = 1 To Np - 1
      Input #1, X(i), Y(i)
    Next i
  Close #1
  '
  E38V = E38
  AngMedF1V = AngMed
  AngMedF2V = AngMed
  E43F1V = E43
  E43F2V = E43
  E44F1V = E44
  E44F2V = E44
  '
  PBF1V = PB
  PBF2V = PB
  BombF1V = BOMB
  BombF2V = BOMB
  '
  CALCOLO_VITE_ZA = True
  '
Exit Function

errCALCOLO_VITE_ZA:
  WRITE_DIALOG "SUB: CALCOLO_VITE_ZA " & Error(Err)
  Exit Function

End Function

Public Sub Leggi_CodMola()

'Leggo dati da area di memoria

Dim TIPO_LAVORAZIONE  As String
Dim DB                As Database
Dim TB                As Recordset
Dim itmDDE            As String
Dim SEZIONE           As String
Dim CodiceMola        As String
Dim DiamExtMola       As Double
Dim IncrProfilatura   As Double
Dim stmp              As String
Dim i                 As Integer
Dim k                 As Integer
Dim stmp1             As String
Dim NroRec(1)         As String
Dim NomeVariabile(20) As String
Dim ValVariabile(20)  As String
Dim ret               As Integer
Dim CodiceTMP(4)      As String
Dim rsp               As Long
Dim Carattere         As String

On Error GoTo errLeggi_CodMola

'Lettura da area memoria RFID
'Aprire Database della lavorazione
'Cercare nella tabella della lavorazione il nome della variabile tramite indice Record
'se ONLINE aggiorna la variabile GUD e campo Actual  altrimenti solo campo Actual
'chiudere DB
'Viene startato un Timer per avere conferma della lettura

TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))


'Lettura Codice Mola da Area Memoria Dati RFID
For i = 0 To 15
    stmp1 = Trim(str(i))
    stmp = OPC_LEGGI_DATO("DB1300.DBB" & stmp1)
    Carattere = Chr(val(stmp))
    Carattere = Trim(Carattere)
    If Carattere <> "" Then
       CodiceMola = CodiceMola & Carattere
    End If
Next i


If CodiceMola = "" Then CodiceMola = "VUOTO"

ValVariabile(0) = UCase$(Trim$(CodiceMola))

stmp = GetInfo(TIPO_LAVORAZIONE, "READVARCHIP", Path_LAVORAZIONE_INI)
stmp = UCase$(Trim$(stmp))

SEZIONE = "CNC_" & LETTERA_LAVORAZIONE

NroRec(0) = stmp

'APRO IL DATABASE
Set DB = OpenDatabase(PathDB, True, False)
Set TB = DB.OpenRecordset(TIPO_LAVORAZIONE)
'
  i = 0
  TB.Index = "INDICE"
  TB.Seek "=", NroRec(i)
  If TB.NoMatch Then
     WRITE_DIALOG "Record number : " & NroRec(i) & " not found."
  Else
     TB.Edit
     NomeVariabile(i) = TB.Fields("VARIABILE").Value
     If TB.Fields("NOMESPF").Value = "ONLINE" Then
        itmDDE = TB.Fields("ITEMDDE").Value
        Call OPC_SCRIVI_DATO(itmDDE, ValVariabile(i))
        'Aggiorno campo Actual della Tabella
        TB.Fields("ACTUAL").Value = ValVariabile(i)
     Else
        'Aggiorno campo Actual della Tabella
        TB.Fields("ACTUAL").Value = ValVariabile(i)
        'Aggiorno file TIPO_LAVORAZIONE.INI
        ret = WritePrivateProfileString(SEZIONE, NomeVariabile(i), ValVariabile(i), g_chOemPATH & "\" & TIPO_LAVORAZIONE & ".INI")
     End If
  End If

TB.Update
'
TB.Close
DB.Close


Exit Sub
errLeggi_CodMola:
  WRITE_DIALOG "SUB: Leggi_CodMola " & Error(Err)
  Exit Sub


End Sub


Public Sub LeggiChipMola()

'Leggo dati da area di memoria

Dim TIPO_LAVORAZIONE  As String
Dim DB                As Database
Dim TB                As Recordset
Dim itmDDE            As String
Dim SEZIONE           As String
Dim CodiceMola        As String
Dim DiamExtMola       As Double
Dim IncrProfilatura   As Double
Dim stmp              As String
Dim i                 As Integer
Dim k                 As Integer
Dim stmp1             As String
Dim NroRec()          As String
Dim NomeVariabile(20) As String
Dim ValVariabile(20)  As String
Dim ret               As Integer


On Error GoTo errLeggiChipMola

'Lettura da area memoria RFID
'Aprire Database della lavorazione
'Cercare nella tabella della lavorazione il nome della variabile tramite indice Record
'se ONLINE aggiorna la variabile GUD e campo Actual  altrimenti solo campo Actual
'chiudere DB
'Viene startato un Timer per avere conferma della lettura

TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))

'Lettura Codice Mola
For i = 1 To 4
    k = 4 * i - 4
    stmp1 = Trim(str(k))
    stmp = OPC_LEGGI_DATO("DB1300.DBD" & stmp1)
    If stmp <> "" Then
       CodiceMola = CodiceMola & stmp
    End If
Next i

If CodiceMola = "" Then CodiceMola = "VUOTO"

ValVariabile(0) = UCase$(Trim$(CodiceMola))

'Diametro Mola
stmp = val(OPC_LEGGI_DATO("DB1300.DBD48"))
DiamExtMola = val(stmp) / 1000
ValVariabile(1) = DiamExtMola

'Incrementi profilatura
stmp = OPC_LEGGI_DATO("DB1300.DBD52")
IncrProfilatura = val(stmp) / 1000
ValVariabile(2) = IncrProfilatura

stmp = GetInfo(TIPO_LAVORAZIONE, "READVARCHIP", Path_LAVORAZIONE_INI)
stmp = UCase$(Trim$(stmp))

SEZIONE = "CNC_" & LETTERA_LAVORAZIONE

NroRec = Split(stmp, ",")

'APRO IL DATABASE
Set DB = OpenDatabase(PathDB, True, False)
Set TB = DB.OpenRecordset(TIPO_LAVORAZIONE)
'
For i = 0 To UBound(NroRec)
  TB.Index = "INDICE"
  TB.Seek "=", NroRec(i)
  If TB.NoMatch Then
     WRITE_DIALOG "Record number : " & NroRec(i) & " not found."
  Else
     TB.Edit
     NomeVariabile(i) = TB.Fields("VARIABILE").Value
     If TB.Fields("NOMESPF").Value = "ONLINE" Then
        itmDDE = TB.Fields("ITEMDDE").Value
        Call OPC_SCRIVI_DATO(itmDDE, ValVariabile(i))
        'Aggiorno campo Actual della Tabella
        TB.Fields("ACTUAL").Value = ValVariabile(i)
     Else
        'Aggiorno campo Actual della Tabella
        TB.Fields("ACTUAL").Value = ValVariabile(i)
        'Aggiorno file TIPO_LAVORAZIONE.INI
        ret = WritePrivateProfileString(SEZIONE, NomeVariabile(i), ValVariabile(i), g_chOemPATH & "\" & TIPO_LAVORAZIONE & ".INI")
     End If
  End If
Next i
TB.Update
'
TB.Close
DB.Close

'Start tempo per comando lettura su Chip
OEMX.Timer2.Enabled = True
OEMX.Timer2.Interval = 10000
OEMX.Timer2.Tag = "R"

Exit Sub
errLeggiChipMola:
  WRITE_DIALOG "SUB: LeggiChipMola " & Error(Err)
  Exit Sub
End Sub
Public Sub ScriviChipMola()

'Scrittura variabili su area di memoria
'
'1. Leggere dal file INI indice record variabile nel DB e ITEMDDE zona memoria RFID
'2. Tramite indice record cercare nel DB
'   Se ONLINE
'      2.1 l'ITEMDDE della variabile se ONLINE
'      2.2 Leggere il valore della variabile tramite ITEMDDE
'   Se non � ONLINE
'      2.1 Leggere il valore della variabile da campo 'Actual'
'3. Scrivere in zona memoria RFID i valori delle variabili
'

Dim TIPO_LAVORAZIONE  As String
Dim DB                As Database
Dim TB                As Recordset
Dim itmDDE            As String
Dim SEZIONE           As String
Dim CodiceMola        As String
Dim DiamExtMola       As Double
Dim IncrProfilatura   As Double
Dim stmp              As String
Dim i                 As Integer
Dim j                 As Integer
Dim k                 As Integer
Dim stmp1             As String
Dim NroRec()          As String
Dim ItemChip()        As String
Dim NomeVariabile(20) As String
Dim ValVariabile(20)  As String
Dim ret               As Integer
Dim TipoVar           As String
Dim Valtmp            As Double
'
On Error GoTo errScriviChipMola

TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))

'1. Leggere dal file INI indice record variabile
stmp = GetInfo(TIPO_LAVORAZIONE, "WRITEVARCHIP", Path_LAVORAZIONE_INI)
stmp = UCase$(Trim$(stmp))
NroRec = Split(stmp, ",")

stmp = GetInfo(TIPO_LAVORAZIONE, "WRITEITEMCHIP", Path_LAVORAZIONE_INI)
stmp = UCase$(Trim$(stmp))
ItemChip = Split(stmp, ",")

'2. Tramite indice record cercare nel DB
Set DB = OpenDatabase(PathDB, True, False)
Set TB = DB.OpenRecordset(TIPO_LAVORAZIONE)
'
For i = 0 To UBound(NroRec)
  TB.Index = "INDICE"
  TB.Seek "=", NroRec(i)
  If TB.NoMatch Then
     WRITE_DIALOG "Record number : " & NroRec(i) & " not found."
  Else
     TB.Edit
     NomeVariabile(i) = TB.Fields("VARIABILE").Value
     If TB.Fields("NOMESPF").Value = "ONLINE" Then
        itmDDE = TB.Fields("ITEMDDE").Value
        ValVariabile(i) = OPC_LEGGI_DATO(itmDDE)
     Else
        'Leggo campo Actual della Tabella
        ValVariabile(i) = TB.Fields("ACTUAL").Value
     End If
                
     TipoVar = TB.Fields("TIPO").Value
     If TipoVar = "N" Then
        Valtmp = Int(val(ValVariabile(i)) * 1000)
        stmp = Trim$(str(Valtmp))
        Call OPC_SCRIVI_DATO(ItemChip(i), stmp)
     End If
     If TipoVar = "S" Then
        For j = 1 To 4
           k = 4 * j - 4
           stmp = Mid(ValVariabile(i), k + 1, 4)
           If stmp <> "" Then
              stmp1 = Trim(str(k))
              Call OPC_SCRIVI_DATO(ItemChip(i) & stmp1, stmp)
           End If
        Next j
     End If
  End If
Next i
TB.Update
'
TB.Close
DB.Close



'Start tempo per comando scrittura su Chip
OEMX.Timer2.Enabled = True
OEMX.Timer2.Interval = 10000
OEMX.Timer2.Tag = "W"

Exit Sub
errScriviChipMola:
  WRITE_DIALOG "SUB: ScriviChipMola " & Error(Err)
  Exit Sub

End Sub
Public Sub ChkCmdOnChip(TipoCmd As String, CmdOK As Boolean)

On Error Resume Next

Call OEMX.SuOEM1.DisegnaOEM
Call OEMX.SuOEM1.SELEZIONA_SuGrid1

If CmdOK Then
   If TipoCmd = "R" Then
      WRITE_DIALOG "Lettura dati mola OK!"
   Else
    If TipoCmd = "W" Then
       WRITE_DIALOG "Scrittura dati mola OK!"
    End If
   End If
Else
   If TipoCmd = "R" Then
      WRITE_DIALOG "Lettura dati mola fallita!"
   Else
    If TipoCmd = "W" Then
       WRITE_DIALOG "Scrittura dati mola fallita!"
    End If
   End If
End If

'Abilito Softkey
For j = 0 To 7
    MDIForm1.Picture3(j).Enabled = True
Next j
For j = 0 To 7
     MDIForm1.Picture1(j).Enabled = True
Next j
'

End Sub


Public Function CALCOLO_VITE_ZI() As Boolean
'
Dim hu1   As Double
Dim PM    As Integer
Dim PmB   As Integer
Dim Bmax  As Double
Dim XX    As Double
Dim MM1   As Double
Dim MM2   As Double
Dim da    As Double
Dim A     As Double
Dim XCF   As Double
Dim YCF   As Double
Dim RFM   As Double
Dim Sonx  As Double
'
Dim E38     As Double
Dim AngMed  As Double
Dim E43     As Double
Dim E44     As Double
'
Dim PB      As Double
Dim BOMB    As Double
Dim Dia_Ext As Double
Dim Apx     As Double
Dim Tangax  As Double
'
Const nPr = 10
'
On Error GoTo errCALCOLO_VITE_ZI
  '
  CALCOLO_VITE_ZI = False
  '
  DiaSmusso = DEXTV
  Dia_Ext = DiaSmusso
  '
  SONt = SpNormV / Cos(BetaIngRad)
  DiaAttivoPiede = DIntV + 2 * RF * (1 - Sin(ApNormVrad))    'SAP
  If DiaAttivoPiede < DbaseV Then DiaAttivoPiede = DbaseV + 0.1
  '
  'ricerca EAP da Dia_Ext -> DiaSmusso
  RX = Dia_Ext / 2
  Do
    RX = RX - 0.01
    Apx = Acs(Rb / RX)      'ang.press.trasversale
    Tangax = Atn(Tan(Apx) * PassoElica / 2 / PG / RX * Cos(BetaVRad))
    XX = 2 * (RX + RT * (1 - Sin(Tangax)))  'dia est calcolato da EAP
  Loop Until XX < Dia_Ext
  DiaSmusso = RX * 2
  '
  MM1 = Sqr(DiaAttivoPiede ^ 2 - DbaseV ^ 2) / 2
  MM2 = Sqr(DiaSmusso ^ 2 - DbaseV ^ 2) / 2
  '
  IL = (DiaMolEff + DIntV) / 2       'interasse
  PassoElica = MnormV * PG / Sin(BetaIngRad) * NFil
  '
  Np = NPE
  p1 = 0
  'calcolo fianco mola
  For i = 0 To Np - 1
    MM(i) = MM1 + (MM2 - MM1) * i / (Np - 1)
    RX = Sqr(Rb ^ 2 + MM(i) ^ 2)
    CALCOLO_T0_FI0_I    'calcolo punto ZI
    Call CALCOLO_PUNTO_MOLA(IL, BetaVRad, PassoElica, RX, T0, FI0, X(i), Y(i), B(i))    'calcolo punto mola Xm,Ym
  Next i
  '
  'punto su Primitivo
  RX = Rprim
  MM(i) = Sqr(RX ^ 2 - Rb ^ 2)
  CALCOLO_T0_FI0_I
  Call CALCOLO_PUNTO_MOLA(IL, BetaVRad, PassoElica, RX, T0, FI0, X(i), Y(i), B(i)) 'calcolo punto mola Xm,Ym
  '
  ' Ricerca punto SAP
  i = 0
  DILAV = 0
  While DILAV < DIntV And i < Np
    DILAV = IL * 2 - (Y(i) + RF * (1 - Sin(B(i) * PG / 180))) * 2
    i = i + 1
  Wend
  If (i > 1) Then
    p1 = i - 2  ' Punto del SAP
  Else
    p1 = i - 1
  End If
  '
  BSap = (B(p1) + B(p1 + 1)) / 2  'Tangente al SAP
  YSap = IL - DIntV / 2 - RF * (1 - Sin(BSap * PG / 180))
  XSap = X(p1 + 1) + (Y(p1 + 1) - YSap) * Tan(BSap * PG / 180)
  X(p1) = XSap: Y(p1) = YSap: B(p1) = BSap
  SAP = (Sqr(MM(p1 + 1) ^ 2 + Rb ^ 2) - Y(p1) + Y(p1 + 1)) * 2
  '
  'Spessore
  E38 = X(Np) * 2
  '
  'Testa
  XCT = X(Np - 1) + RT * Cos(FnRad(B(Np - 1)))
  YCT = Y(Np - 1) + RT * Sin(FnRad(B(Np - 1)))
  '
  'Addendum,altezza
  E43 = Y(Np) - (YCT - RT)
  E44 = IL - DIntV / 2 - YCT + RT
  '
  For i = Np - 1 To p1 Step -1: Y(i) = Y(p1) - Y(i): Next i
  hu1 = Y(Np - 1) - Y(p1)
  AngMed = Atn((X(Np - 1) - X(p1)) / hu1)
  PM = Int((Np - p1) / 2)
  PmB = 0: Bmax = 0
  For i = p1 + 1 To Np - 2
    XX = X(i) - X(p1) - Y(i) * Tan(AngMed)
    If Abs(XX) > Abs(Bmax) Then Bmax = XX: PmB = i
  Next i
  BOMB = -(Bmax) * Cos(AngMed)  '+ : cavit�, - : bombatura
  PB = 1 - (Y(PmB) - Y(p1)) / hu1     'posizione bombatura/cavit�
  If (BOMB = 0) Then PB = 0.5
  If (PB > 0.9) Then PB = 0.9
  If (PB < 0.1) Then PB = 0.1
  '
  'Raggio testa/fondo
  For i = 1 To nPr
    da = (PG / 2 - B(Np - 1) * PG / 180) / nPr
    A = (PG / 2 - B(Np - 1) * PG / 180) - da * i
    X(Np - 1 + i) = XCT - RT * Sin(A)
    Y(Np - 1 + i) = YSap - (YCT - RT * Cos(A))
  Next i
  XCF = X(p1) - RF * Cos(BSap * PG / 180)
  YCF = Y(p1) + RF * Sin(BSap * PG / 180)
  For i = 1 To nPr
    da = (PG / 2 - B(p1) * PG / 180) / nPr
    A = (PG / 2 - B(p1) * PG / 180) - da * i
    X(Np - 1 + nPr + i) = XCF + RF * Sin(A)
    Y(Np - 1 + nPr + i) = YCF - RF * Cos(A)
  Next i
  '
  'registra punti su file
  Open g_chOemPATH & "\" & FileTEMPORANEO For Output As #1
    '
    For i = p2 + 1 + 2 * nPr - 1 To nPr + p2 + 1 Step -1
      Print #1, X(i), Y(i)
    Next i
    For i = p1 To p2
      Print #1, X(i), Y(i)
    Next i
    For i = p2 + 1 To nPr + p2
      Print #1, X(i), Y(i)
    Next i
    '
  Close #1
  '
  Np = p2 + 1 + nPr * 2 - p1
  '
  Open g_chOemPATH & "\" & FileTEMPORANEO For Input As #1
    For i = 1 To Np - 1
      Input #1, X(i), Y(i)
    Next i
  Close #1
  '
  E38V = E38
  AngMedF1V = AngMed
  AngMedF2V = AngMed
  E43F1V = E43
  E43F2V = E43
  E44F1V = E44
  E44F2V = E44
  '
  PBF1V = PB
  PBF2V = PB
  BombF1V = BOMB
  BombF2V = BOMB
  '
  CALCOLO_VITE_ZI = True
  '
Exit Function
  
errCALCOLO_VITE_ZI:
  WRITE_DIALOG "SUB: CALCOLO_VITE_ZI " & Error(Err)
  Exit Function

End Function

Public Sub CALCOLO_T0_FI0_I()
'
Dim InvPrim As Double
Dim Apx     As Double
Dim ApxT    As Double
Dim InvAx   As Double
'
On Error Resume Next
  '
  Apx = Acs(Rb / RX)
  InvPrim = Tan(ApAssVRad) - ApAssVRad
  InvAx = Tan(Apx) - Apx
  Sx = (SONt / 2 / Rprim + InvPrim - InvAx) * 2 * RX
  VX = 2 * PG * RX / NFil - Sx
  T0 = PG / 2 - VX / 2 / RX
  FI0 = T0 - Apx
  If T0 < 0 Then FI0 = FI0 - 2 * PG
  If FI0 > PG Then FI0 = FI0 - PG * 2
  If FI0 < -PG Then FI0 = FI0 + PG * 2
  '
End Sub
'
'CALCOLO DEL PROFILO ASSIALE: PLANETENROLLE 26X7-LH
Public Function CALCOLO_VITE_ARBURG() As Boolean
'
'VARIE
Dim i     As Long
Dim dALFA As Double
Dim ALFA  As Double
'
'FIANCO 1
Dim R41   As Double
Dim R43   As Double
Dim R44   As Double
Dim R46   As Double
Dim R48   As Double
Dim R49   As Double
'FIANCO 2
Dim R51   As Double
Dim R53   As Double
Dim R54   As Double
Dim R56   As Double
Dim R58   As Double
Dim R59   As Double
'
Dim XX(0, 99) As Double
Dim YY(0, 99) As Double
Dim RR(0, 99) As Double
'
Dim SCARTO            As Double
Dim SCARTO_PRECEDENTE As Double
Dim XXp1              As Double
Dim YYp1              As Double
Dim RExtTEMP          As Double
Dim RIntTEMP          As Double
Dim ATTF2             As Double
Dim ATTF1             As Double
Dim ATFF2             As Double
Dim ATFF1             As Double
'
On Error GoTo errCALCOLO_VITE_ARBURG
  '
  CALCOLO_VITE_ARBURG = False
  '
  If (RaggioFiancoV <= 0) Then RaggioFiancoV = 50
  '
  'FIANCO 1
  R41 = ApVite               'ANGOLO SUL FIANCO 1
  R43 = (DEXTV - DprimV) / 2 'ADDENDUM 1
  R44 = (DEXTV - DIntV) / 2  'ALTEZZA PROFILO 1
  R46 = RaggioFiancoV        'RAGGIO SUL FIANCO 1
  R48 = RT                   'RAGGIO DI TESTA 1
  R49 = RF                   'RAGGIO DI FONDO 1
  'FIANCO 2
  R51 = ApVite               'ANGOLO SUL FIANCO 2
  R53 = (DEXTV - DprimV) / 2 'ADDENDUM 2
  R54 = (DEXTV - DIntV) / 2  'ALTEZZA PROFILO 2
  R56 = RaggioFiancoV        'RAGGIO SUL FIANCO 2
  R58 = RT                   'RAGGIO DI TESTA 2
  R59 = RF                   'RAGGIO DI FONDO 2
  '
  'CONVERSIONE GRADI --> RADIANTI
  R41 = R41 / 180 * PG
  'CONVERSIONE GRADI --> RADIANTI
  R51 = R51 / 180 * PG
  '
  'FIANCO 2
  'CERCHIO SUL FIANCO
  XX(0, 51) = SpNormV / 2 + R56 * Cos(R51)
  YY(0, 51) = DprimV / 2 - R56 * Sin(R51)
  '
  'CERCHIO DI TESTA
  XX(0, 52) = SpNormV / 2 + R58 * Cos(R51)
  YY(0, 52) = DprimV / 2 - R58 * Sin(R51)
  RExtTEMP = YY(0, 52) + R58
  ALFA = R51: dALFA = 0.00001: i = 0
  Do
    ATTF2 = ALFA
    XX(0, 52) = XX(0, 51) - (R56 - R58) * Cos(ALFA)
    YY(0, 52) = YY(0, 51) + (R56 - R58) * Sin(ALFA)
    RExtTEMP = YY(0, 52) + R58
    ALFA = ALFA + dALFA
  Loop While (RExtTEMP < DEXTV / 2)
  'VALORE PRECEDENTE
  ATTF2 = ATTF2 - dALFA
  XX(0, 52) = XX(0, 51) - (R56 - R58) * Cos(ATTF2)
  YY(0, 52) = YY(0, 51) + (R56 - R58) * Sin(ATTF2)
  'PUNTO TANGENTE SUL FIANCO
  XX(0, 62) = XX(0, 51) - R56 * Cos(ATTF2)
  YY(0, 62) = YY(0, 51) + R56 * Sin(ATTF2)
  'PUNTO SUL DIAMETRO ESTERNO
  XX(0, 60) = XX(0, 52)
  YY(0, 60) = YY(0, 52) + R58
  '
  'CERCHIO DI FONDO
  XX(0, 53) = SpNormV / 2 - R59 * Cos(R51)
  YY(0, 53) = DprimV / 2 + R59 * Sin(R51)
  RIntTEMP = YY(0, 53) - R59
  ALFA = R51: dALFA = 0.00001: i = 0
  Do
    ATFF2 = ALFA
    XX(0, 53) = XX(0, 51) - (R56 + R59) * Cos(ALFA)
    YY(0, 53) = YY(0, 51) + (R56 + R59) * Sin(ALFA)
    RIntTEMP = YY(0, 53) - R59
    ALFA = ALFA - dALFA
  Loop While (RIntTEMP > DIntV / 2)
  'VALORE PRECEDENTE
  ATFF2 = ATFF2 + dALFA
  XX(0, 53) = XX(0, 51) - (R56 + R59) * Cos(ATFF2)
  YY(0, 53) = YY(0, 51) + (R56 + R59) * Sin(ATFF2)
  'PUNTO TANGENTE SUL FIANCO
  XX(0, 63) = XX(0, 51) - R56 * Cos(ATFF2)
  YY(0, 63) = YY(0, 51) + R56 * Sin(ATFF2)
  'PUNTO SUL DIAMETRO INTERNO
  XX(0, 61) = XX(0, 53)
  YY(0, 61) = YY(0, 53) - R59
  '
  'FIANCO 1
  'CERCHIO SUL FIANCO
  XX(0, 11) = -SpNormV / 2 - R46 * Cos(R41)
  YY(0, 11) = DprimV / 2 - R46 * Sin(R41)
  '
  'CERCHIO DI TESTA
  XX(0, 12) = SpNormV / 2 - R48 * Cos(R41)
  YY(0, 12) = DprimV / 2 - R48 * Sin(R41)
  RExtTEMP = YY(0, 52) + R48
  ALFA = R51: dALFA = 0.00001: i = 0
  Do
    ATTF1 = ALFA
    XX(0, 12) = XX(0, 11) + (R46 - R48) * Cos(ALFA)
    YY(0, 12) = YY(0, 11) + (R46 - R48) * Sin(ALFA)
    RExtTEMP = YY(0, 12) + R48
    ALFA = ALFA + dALFA
  Loop While (RExtTEMP < DEXTV / 2)
  'VALORE PRECEDENTE
  ATTF1 = ATTF1 - dALFA
  XX(0, 12) = XX(0, 11) + (R46 - R48) * Cos(ATTF1)
  YY(0, 12) = YY(0, 11) + (R46 - R48) * Sin(ATTF1)
  'PUNTO TANGENTE SUL FIANCO
  XX(0, 22) = XX(0, 11) + R46 * Cos(ATTF1)
  YY(0, 22) = YY(0, 11) + R46 * Sin(ATTF1)
  'PUNTO SUL DIAMETRO ESTERNO
  XX(0, 20) = XX(0, 12)
  YY(0, 20) = YY(0, 12) + R48
  '
  'CERCHIO DI FONDO
  XX(0, 13) = -SpNormV / 2 + R49 * Cos(R41)
  YY(0, 13) = DprimV / 2 + R49 * Sin(R41)
  RIntTEMP = YY(0, 13) - R49
  ALFA = R41: dALFA = 0.00001: i = 0
  Do
    ATFF1 = ALFA
    XX(0, 13) = XX(0, 11) + (R46 + R49) * Cos(ALFA)
    YY(0, 13) = YY(0, 11) + (R46 + R49) * Sin(ALFA)
    RIntTEMP = YY(0, 13) - R49
    ALFA = ALFA - dALFA
  Loop While (RIntTEMP > DIntV / 2)
  'VALORE PRECEDENTE
  ATFF1 = ATFF1 + dALFA
  XX(0, 13) = XX(0, 11) + (R46 + R49) * Cos(ATFF1)
  YY(0, 13) = YY(0, 11) + (R46 + R49) * Sin(ATFF1)
  'PUNTO TANGENTE SUL FIANCO
  XX(0, 23) = XX(0, 11) + R46 * Cos(ATFF1)
  YY(0, 23) = YY(0, 11) + R46 * Sin(ATFF1)
  'PUNTO SUL DIAMETRO INTERNO
  XX(0, 21) = XX(0, 13)
  YY(0, 21) = YY(0, 13) - R49
  '
  'ASSEGNAZIONE PARAMETRI MOLA
  E38V = SpNormV
  '
  AngMedF1V = Atn((XX(0, 23) - XX(0, 22)) / (YY(0, 22) - YY(0, 23)))
  AngMedF2V = Atn((XX(0, 62) - XX(0, 63)) / (YY(0, 62) - YY(0, 63)))
  '
  E43F1V = R43
  E43F2V = R43
  E44F1V = R44
  E44F2V = R44
  '
  SCARTO = Sqr((XX(0, 22) - XX(0, 23)) ^ 2 + (YY(0, 22) - YY(0, 23)) ^ 2)
  ALFA = Asn(SCARTO / (2 * R46))
  PBF1V = 0.5
  BombF1V = R46 * (1 - Cos(ALFA))
  '
  SCARTO = Sqr((XX(0, 62) - XX(0, 63)) ^ 2 + (YY(0, 62) - YY(0, 63)) ^ 2)
  ALFA = Asn(SCARTO / (2 * R56))
  PBF2V = 0.5
  BombF2V = R56 * (1 - Cos(ALFA))
  '
  CALCOLO_VITE_ARBURG = True
  '
Exit Function
    
errCALCOLO_VITE_ARBURG:
  WRITE_DIALOG "SUB : CALCOLO_VITE_ARBURG " & Error(Err)
  Exit Function

End Function

Public Function CALCOLO_VITE_ZN() As Boolean
'
Dim hu1   As Double
Dim PM    As Integer
Dim PmB   As Integer
Dim Bmax  As Double
Dim XX    As Double
'
Dim da    As Double
Dim A     As Double
Dim XCF   As Double
Dim YCF   As Double
Dim RFM   As Double
Dim Sonx  As Double
'
Dim E38     As Double
Dim AngMed  As Double
Dim E43     As Double
Dim E44     As Double
Dim PB      As Double
Dim BOMB    As Double
'
Dim MyYStart  As Double
Dim ARGOMENTO As Double
Dim BC        As Double
'
Const nPr = 10
'
On Error GoTo errCALCOLO_VITE_ZN
  '
  CALCOLO_VITE_ZN = False
  '
  SONt = SpNormV / Cos(BetaIngRad)
  DiaAttivoPiede = DIntV + 2 * RF * (1 - Sin(ApNormVrad))    'SAP
  DiaSmusso = DEXTV
  IL = (DiaMolEff + DIntV) / 2       'interasse
  PassoElica = MnormV * PG / Sin(BetaIngRad) * NFil
  Np = NPE
  p1 = 0
  'profilo assiale vano vite
  IL = (DIntV + DiaMolEff) / 2
  '
  Call PROFILO_VITE_ZN(Np)
  '
  ' Ricerca punto SAP
  i = 1
  DILAV = 0
  While DILAV < DIntV And i < Np
    DILAV = IL * 2 - (Y(i) + RF * (1 - Sin(B(i) * PG / 180))) * 2
    i = i + 1
  Wend
  If i > 1 Then
    p1 = i - 2  ' Punto del SAP
  Else
    p1 = i - 1
  End If
  '  Ricerca punto EAP
  MyYStart = Y(Np - 1)
  i = Np - 2
  Do
    'Calcolo tangente
    ARGOMENTO = (RT - (Abs(Y(i) - MyYStart))) / RT
    BC = (Atn(-ARGOMENTO / Sqr(-ARGOMENTO * ARGOMENTO + 1)) + 2 * Atn(1)) * 180 / PG 'Tangente cfr. di testa
    i = i - 1
  Loop Until B(i) <= BC Or i = 0
  If i <> 0 Then p2 = i + 1 Else p2 = i ' Punto del EAP
  BEap = (B(p2) + B(p2 - 1)) / 2 'Tangente al SAP
  YEap = IL - DEXTV / 2 + RT * (1 - Sin(BEap * PG / 180))
  XEap = X(p2 - 1) + (Y(p2 - 1) - YEap) * Tan(BEap * PG / 180)
  X(p2) = XEap: Y(p2) = YEap: B(p2) = BEap
  '
  For i = p1 To p2
    If Y(i) < Y(p2) Then
      X(i) = XEap: Y(i) = YEap: B(i) = BEap
      p2 = i
      Exit For
    End If
  Next i
  '
  'Primitivo
  X(p2 + 1) = X(Np)
  Y(p2 + 1) = Y(Np)
  B(p2 + 1) = B(Np)
  EAP = (IL - Sqr(XEap ^ 2 + YEap ^ 2)) * 2
  BSap = (B(p1) + B(p1 + 1)) / 2  'Tangente al SAP
  YSap = IL - DIntV / 2 - RF * (1 - Sin(FnRad(BSap)))
  XSap = X(p1 + 1) + (Y(p1 + 1) - YSap) * Tan(FnRad(BSap))
  X(p1) = XSap: Y(p1) = YSap: B(p1) = BSap
  SAP = (IL - Sqr(XSap ^ 2 + YSap ^ 2)) * 2
  '
  'Spessore
  E38 = X(p2 + 1) * 2
  '
  'Testa
  XCT = X(p2) + RT * Cos(FnRad(B(p2)))
  YCT = Y(p2) + RT * Sin(FnRad(B(p2)))
  '
  'Piede
  XCF = X(p1) - RF * Cos(FnRad(B(p2)))
  YCF = Y(p1) - RF * Sin(FnRad(B(p2)))
  '
  'Addendum,altezza
  E43 = Y(p2 + 1) - (YCT - (RT * Cos(AngRaccordoTesta * (PG / 180))))
  E44 = (YCF + RF) - (YCT - (RT * Cos(AngRaccordoTesta * (PG / 180))))
  '
  'Variabili per part program
  'QY2 = Y(P2 + 1) - Y(P2)
  'QY3 = Y(P1) - Y(P2 + 1)
  '
  For i = p2 To p1 Step -1: Y(i) = Y(p1) - Y(i): Next i
  '
  hu1 = Y(p2) - Y(p1)
  AngMed = Atn((X(p2) - X(p1)) / hu1)
  '
  PM = Int((Np - p1) / 2)
  PmB = 0: Bmax = 0
  '
  For i = p1 + 1 To p2 - 1
    XX = X(i) - X(p1) - Y(i) * Tan(AngMed)
    If Abs(XX) > Abs(Bmax) Then Bmax = XX: PmB = i
  Next i
  BOMB = -(Bmax) * Cos(AngMed)  '+ : cavit�, - : bombatura
  PB = 1 - (Y(PmB) - Y(p1)) / hu1     'posizione bombatura/cavit�
  If BOMB = 0 Then PB = 0.5
  If PB > 0.9 Then PB = 0.9
  If PB < 0.1 Then PB = 0.1
  '
  For i = 1 To nPr
    da = (PG / 2 - B(p2) * PG / 180) / nPr
    A = (PG / 2 - B(p2) * PG / 180) - da * i
    X(p2 + i) = XCT - RT * Sin(A)
    Y(p2 + i) = YSap - (YCT - RT * Cos(A))
  Next i
  XCF = X(p1) - RF * Cos(BSap * PG / 180)
  YCF = Y(p1) + RF * Sin(BSap * PG / 180)
  For i = 1 To nPr
    da = (PG / 2 - B(p1) * PG / 180) / nPr
    A = (PG / 2 - B(p1) * PG / 180) - da * i
    X(p2 + nPr + i) = XCF + RF * Sin(A)
    Y(p2 + nPr + i) = YCF - RF * Cos(A)
  Next i
  'Raggio testa/fondo
  '
  'registra punti su file
  Open g_chOemPATH & "\" & FileTEMPORANEO For Output As #1
    '
    For i = p2 + 1 + 2 * nPr - 1 To nPr + p2 + 1 Step -1
      Print #1, X(i), Y(i)
    Next i
    For i = p1 To p2
      Print #1, X(i), Y(i)
    Next i
    For i = p2 + 1 To nPr + p2
      Print #1, X(i), Y(i)
    Next i
    '
  Close #1
  '
  Np = p2 + 1 + nPr * 2 - p1
  '
  Open g_chOemPATH & "\" & FileTEMPORANEO For Input As #1
    For i = 1 To Np - 1
      Input #1, X(i), Y(i)
    Next i
  Close #1
  '
  E38V = E38
  AngMedF1V = AngMed
  AngMedF2V = AngMed
  E43F1V = E43
  E43F2V = E43
  E44F1V = E44
  E44F2V = E44
  '
  PBF1V = PB
  PBF2V = PB
  BombF1V = BOMB
  BombF2V = BOMB
  '
  CALCOLO_VITE_ZN = True
  '
Exit Function
    
errCALCOLO_VITE_ZN:
  WRITE_DIALOG "SUB : CALCOLO_VITE_ZN " & Error(Err)
  Exit Function

End Function

Public Function CALCOLO_VITE_ZK() As Boolean
'
Dim hu1   As Double
Dim PM    As Integer
Dim PmB   As Integer
Dim Bmax  As Double
Dim XX    As Double
Dim da    As Double
Dim A     As Double
Dim XCF   As Double
Dim YCF   As Double
Dim RFM   As Double
Dim Sonx  As Double
Dim Aux1  As Double
Dim Aux2  As Double
Dim Ltang  As Double
'
Dim DiaTesta  As Double
Dim Bv(100)   As Double
Dim E38       As Double
'
Dim AngMed As Double
Dim E43    As Double
Dim E44    As Double
'
Dim PB    As Double
Dim BOMB  As Double
'
Const nPr = 10
'
On Error GoTo errCALCOLO_VITE_ZK
  '
  CALCOLO_VITE_ZK = False
  '
  DiaSmusso = DEXTV
  '
  SONt = SpNormV / Cos(BetaIngRad)
  DiaAttivoPiede = DIntV + 2 * RF * (1 - Sin(ApNormVrad))    'SAP
  '
  IL = (DiaMolEff + DIntV) / 2       'interasse
  PassoElica = MnormV * PG / Sin(BetaIngRad) * NFil
  '
  Np = NPE
  p1 = 0
  'calcolo fianco mola
  'primitivo
  IL = (DIntV + DiaMolTeo) / 2 'considero mola di DiaMolTeo
  X(Np) = (PassoElica * Cos(PG / 2 - BetaIngRad) / NFil - SpNormV) / 2
  Y(Np) = IL - Rprim
  B(Np) = FnGrad(ApNormVrad)
  '(*i*)
  'ricerca diametro di testa vite
  '(DiaSmusso ha il significato di diametro esterno di input)
  '(DiaTesta ha il significato di diametro attivo di testa - EAP)
  i = Np - 1
  RX = DiaSmusso / 2 - RT * (1 - Sin(ApNormVrad))
  Y(i) = IL - RX
  X(i) = X(Np) + (RX - Rprim) * Tan(ApNormVrad)
  B(i) = FnGrad(ApNormVrad)
  Call CALCOLO_VITE_DA_MOLA(IL, BetaVRad, PassoElica, YP1(i), Aux1, Aux2, XP1(i), Bp1(i), X(i), Y(i), FnRad(B(i)), Ltang)
  If YP1(i) < RX Then YP1(i) = YP1(i) + (RX - YP1(i)) * 2
  DiaTesta = YP1(i) * 2
  'If DiaTesta < DiaSmusso Then DiaTesta = DiaTesta + (DiaSmusso - DiaTesta) * 2
  'Rx = DiaTesta / 2    'Raggio vite di partenza
  RX = YP1(i)
  XX = 0.001           'step Rx
  Do
    RX = RX - XX
    Y(i) = IL - RX
    X(i) = X(Np) + (RX - Rprim) * Tan(ApNormVrad)
    B(i) = FnGrad(ApNormVrad)
    Call CALCOLO_VITE_DA_MOLA(IL, BetaVRad, PassoElica, YP1(i), Aux1, Aux2, XP1(i), Bp1(i), X(i), Y(i), FnRad(B(i)), Ltang)
    DiaTesta = YP1(i) * 2
  Loop Until DiaTesta + 2 * (RT * (1 - Sin(Bp1(i)))) < DiaSmusso + XX
  '(*f*)
  DiaTesta = RX * 2 + XX
  ' fianco
  XX = (DiaTesta - DiaAttivoPiede) / (Np - 1) / 2
  For i = 0 To Np - 1
    RX = DiaAttivoPiede / 2 + XX * i
    Y(i) = IL - RX
    X(i) = X(Np) + (RX - Rprim) * Tan(ApNormVrad)
    B(i) = FnGrad(ApNormVrad)
    Call CALCOLO_VITE_DA_MOLA(IL, BetaVRad, PassoElica, YP1(i), Aux1, Bv(i), XP1(i), Bp1(i), X(i), Y(i), FnRad(B(i)), Ltang)
  Next i
  ' primitivo
  'i=NP
  Call CALCOLO_VITE_DA_MOLA(IL, BetaVRad, PassoElica, YP1(i), Aux1, Bv(i), XP1(i), Bp1(i), X(i), Y(i), FnRad(B(i)), Ltang)
  IL = (DIntV + DiaMolEff) / 2
  For i = 0 To Np
    RX = YP1(i)
    T0 = PG / 2 - FnRad(XP1(i) * 360 / PassoElica)
    'Ax = Atn((Tan(Bp1(I)) * 2 * PG * Yp1(I)) / PassoElica)
    Ax = Bv(i)
    FI0 = T0 - Ax: If T0 < 0 Then FI0 = FI0 - 2 * PG
    If FI0 > PG Then FI0 = FI0 - PG * 2
    If FI0 < -PG Then FI0 = FI0 + PG * 2
    Call CALCOLO_PUNTO_MOLA(IL, BetaVRad, PassoElica, RX, T0, FI0, X(i), Y(i), B(i))
    'Debug.Print "COORDINATE MOLA EFFETTIVA:", I, X(I), Y(I), B(I)
  Next i
  ' Ricerca punto SAP
  i = 0
  DILAV = 0
  While DILAV < DIntV And i < Np
    DILAV = IL * 2 - (Y(i) + RF * (1 - Sin(B(i) * PG / 180))) * 2
    i = i + 1
  Wend
  If i > 1 Then
    p1 = i - 2  ' Punto del SAP
  Else
    p1 = i - 1
  End If
  BSap = (B(p1) + B(p1 + 1)) / 2  'Tangente al SAP
  YSap = IL - DIntV / 2 - RF * (1 - Sin(BSap * PG / 180))
  XSap = X(p1 + 1) + (Y(p1 + 1) - YSap) * Tan(BSap * PG / 180)
  X(p1) = XSap: Y(p1) = YSap: B(p1) = BSap
  Call CALCOLO_VITE_DA_MOLA(IL, BetaVRad, PassoElica, YP1(p1), Aux1, Aux2, XP1(p1), Bp1(p1), X(p1), Y(p1), FnRad(B(p1)), Ltang)
  SAP = Sqr(XP1(p1) ^ 2 + YP1(p1) ^ 2) * 2
  'Spessore
  E38 = X(Np) * 2
  'Testa
  XCT = X(Np - 1) + RT * Cos(FnRad(B(Np - 1)))
  YCT = Y(Np - 1) + RT * Sin(FnRad(B(Np - 1)))
  'Addendum,altezza
  E43 = Y(Np) - (YCT - RT)
  E44 = IL - DIntV / 2 - YCT + RT
  For i = Np - 1 To p1 Step -1: Y(i) = Y(p1) - Y(i): Next i
  hu1 = Y(Np - 1) - Y(p1)
  AngMed = Atn((X(Np - 1) - X(p1)) / hu1)
  PM = Int((Np - p1) / 2)
  PmB = 0: Bmax = 0
  For i = p1 + 1 To Np - 2
    XX = X(i) - X(p1) - Y(i) * Tan(AngMed)
    If Abs(XX) > Abs(Bmax) Then Bmax = XX: PmB = i
  Next i
  BOMB = -(Bmax) * Cos(AngMed)         '+ : cavit�, - : bombatura
  PB = 1 - (Y(PmB) - Y(p1)) / hu1     'posizione bombatura/cavit�
  If BOMB = 0 Then PB = 0.5
  If PB > 0.9 Then PB = 0.9
  If PB < 0.1 Then PB = 0.1
  'Raggio testa/fondo
  For i = 1 To nPr
    da = (PG / 2 - B(Np - 1) * PG / 180) / nPr
    A = (PG / 2 - B(Np - 1) * PG / 180) - da * i
    X(Np - 1 + i) = XCT - RT * Sin(A)
    Y(Np - 1 + i) = YSap - (YCT - RT * Cos(A))
  Next i
  XCF = X(p1) - RF * Cos(BSap * PG / 180)
  YCF = Y(p1) + RF * Sin(BSap * PG / 180)
  For i = 1 To nPr
    da = (PG / 2 - B(p1) * PG / 180) / nPr
    A = (PG / 2 - B(p1) * PG / 180) - da * i
    X(Np - 1 + nPr + i) = XCF + RF * Sin(A)
    Y(Np - 1 + nPr + i) = YCF - RF * Cos(A)
  Next i
  '
  'registra punti su file
  Open g_chOemPATH & "\" & FileTEMPORANEO For Output As #1
    '
    For i = p2 + 1 + 2 * nPr - 1 To nPr + p2 + 1 Step -1
      Print #1, X(i), Y(i)
    Next i
    For i = p1 To p2
      Print #1, X(i), Y(i)
    Next i
    For i = p2 + 1 To nPr + p2
      Print #1, X(i), Y(i)
    Next i
    '
  Close #1
  '
  Np = p2 + 1 + nPr * 2 - p1
  '
  Open g_chOemPATH & "\" & FileTEMPORANEO For Input As #1
    For i = 1 To Np - 1
      Input #1, X(i), Y(i)
    Next i
  Close #1
  '
  E38V = E38
  AngMedF1V = AngMed
  AngMedF2V = AngMed
  E43F1V = E43
  E43F2V = E43
  E44F1V = E44
  E44F2V = E44
  '
  PBF1V = PB
  PBF2V = PB
  BombF1V = BOMB
  BombF2V = BOMB
  '
  CALCOLO_VITE_ZK = True
  '
  Exit Function
    
errCALCOLO_VITE_ZK:
  WRITE_DIALOG "SUB : CALCOLO_VITE_ZK " & Error(Err)
  Exit Function

End Function

Public Function CALCOLO_VITE_ZC() As Boolean
'
Dim hu1   As Double
Dim PM    As Integer
Dim PmB   As Integer
Dim Bmax  As Double
Dim XX    As Double
Dim PmB1  As Double
Dim PmB2  As Double
Dim ang1  As Double
Dim ANG2  As Double
Dim Bomb1 As Double
Dim Bomb2 As Double
Dim da    As Double
Dim A     As Double
Dim XCF   As Double
Dim YCF   As Double
Dim RFM   As Double
Dim Sonx  As Double
Dim Beta  As Double
Dim RpMol As Double
Dim Xcent As Double
Dim Rcent As Double
Dim Apx   As Double
Dim Yrf   As Double
'
Dim PasNorm       As Double
Dim Rxm           As Double
Dim Exm           As Double
Dim Rayon         As Double
Dim ALFA          As Double
Dim APM           As Double
Dim Ixv           As Double
Dim Eobt          As Double
Dim RmMini        As Double
Dim RMmaxi        As Double
Dim Nbp           As Integer
Dim Ltang         As Double
Dim ApAxialMedio  As Double
Dim E38           As Double
Dim AngMed        As Double
Dim E43           As Double
Dim E44           As Double
Dim PB            As Double
Dim BOMB          As Double
Dim Dia_Ext       As Double
'
Const nPr = 10
'
On Error GoTo errCALCOLO_VITE_ZC
  '
  CALCOLO_VITE_ZC = False
  '
  '(*f*)
  DiaSmusso = DEXTV
  '(*i*)
  Dia_Ext = DiaSmusso
  '(*f*)
  DiaAttivoPiede = DIntV + 2 * RF * (1 - Sin(ApNormVrad))    'SAP
  If DiaAttivoPiede < DbaseV Then DiaAttivoPiede = DbaseV + 0.1
  PassoElica = MnormV * PG / Sin(BetaIngRad) * NFil
  Np = NPE
  p1 = 0
  RpMol = DiaMolTeo / 2
  PasNorm = PassoElica / NFil * Cos(BetaVRad)
  If Abs(FnRad(BetaV) - FnRad(Asn(PasNorm * NFil / DprimV / PG))) > 0.5 Then Stop
  IL = RpMol + DprimV / 2
  Rcent = RpMol - RagFiancoMola * Sin(ApNormVrad)
  '
  ' Ricerca di Xcent sulla mola teorica per ottenere Spessore
  Xcent = RagFiancoMola * Cos(ApNormVrad) - (PasNorm - SpNormV) / 2
  For i = 1 To 10
     Rxm = Rcent + RagFiancoMola * Sin(ApNormVrad)
     Exm = RagFiancoMola * Cos(ApNormVrad) - Xcent
     Call CALCOLO_VITE_DA_MOLA(IL, BetaVRad, PassoElica, Rayon, ALFA, Beta, Ixv, Apx, Exm, Rxm, ApNormVrad, Ltang)
     Eobt = PasNorm - Ixv * PasNorm * NFil / PassoElica * 2
     Xcent = Xcent - (Eobt - SpNormV) / 2
  Next i
  ' Calcolo per punti del profilo assiale
  'ricerca diametro attivo di testa
  '(*i*)
  ' Ricerca di D.attivo di testa e RmMini coniugato
  RmMini = RpMol
  Do
    RmMini = RmMini - 0.01
    APM = Asn((RmMini - Rcent) / RagFiancoMola)
    Exm = RagFiancoMola * Cos(APM) - Xcent
    Call CALCOLO_VITE_DA_MOLA(IL, BetaVRad, PassoElica, Rayon, ALFA, Beta, Ixv, Apx, Exm, RmMini, APM, Ltang)
    'RmMini = RmMini - (DiaSmusso / 2 - Rayon) / 2
    XX = 2 * (Rayon + RT * (1 - Sin(Apx))) ' dia. est.
  Loop While XX < Dia_Ext
  DiaSmusso = Rayon * 2
  '
  '(*f*)
  RMmaxi = RpMol + (Rprim - DIntV / 2)
  Nbp = NPE   ' Numero di punti del fianco
  For i = 0 To Nbp - 1
    '(*i*)
    Rxm = RMmaxi - (RMmaxi - RmMini) * i / (Nbp - 1)
    '(*f*)
    APM = Asn((Rxm - Rcent) / RagFiancoMola)
    Exm = RagFiancoMola * Cos(APM) - Xcent
    Call CALCOLO_VITE_DA_MOLA(IL, BetaVRad, PassoElica, Rayon, ALFA, Beta, Ixv, Apx, Exm, Rxm, APM, Ltang)
    YP1(i) = Rayon: XP1(i) = Ixv: Bp1(i) = Apx  'profilo vite
    X(i) = Exm: Y(i) = Rxm: B(i) = APM          'profilo mola di riferimento
  Next i
  'i = Nbp
  'primitivo
  Rxm = RpMol
  APM = Asn((Rxm - Rcent) / RagFiancoMola)
  Exm = RagFiancoMola * Cos(APM) - Xcent
  Call CALCOLO_VITE_DA_MOLA(IL, BetaVRad, PassoElica, Rayon, ALFA, Beta, Ixv, Apx, Exm, Rxm, APM, Ltang)
  YP1(i) = Rayon: XP1(i) = Ixv: Bp1(i) = Apx
  X(i) = Exm: Y(i) = Rxm: B(i) = APM
  '
  ApAxialMedio = Atn((XP1(1) - XP1(Nbp)) / (YP1(1) - YP1(Nbp)))
  '
  'calcolo profilo mola effettiva
  IL = (DiaMolEff + DIntV) / 2       'interasse
  For i = 0 To Nbp
    RX = YP1(i)
    T0 = PG / 2 - FnRad(XP1(i) * 360 / PassoElica)
    Ax = Atn((Tan(Bp1(i)) * 2 * PG * YP1(i)) / PassoElica)
    FI0 = T0 - Ax: If T0 < 0 Then FI0 = FI0 - 2 * PG
    If FI0 > PG Then FI0 = FI0 - PG * 2
    If FI0 < -PG Then FI0 = FI0 + PG * 2
    Call CALCOLO_PUNTO_MOLA(IL, BetaVRad, PassoElica, RX, T0, FI0, X(i), Y(i), B(i))
  Next i
  ' Ricerca punto SAP
  i = 0
  DILAV = 0
  While DILAV < DIntV And i < Np
    DILAV = IL * 2 - (Y(i) + RF * (1 - Sin(B(i) * PG / 180))) * 2
    i = i + 1
  Wend
  If i > 1 Then
    p1 = i - 2  ' Punto del SAP
  Else
    p1 = i - 1
  End If
  BSap = (B(p1) + B(p1 + 1)) / 2  'Tangente al SAP
  YSap = IL - DIntV / 2 - RF * (1 - Sin(BSap * PG / 180))
  XSap = X(p1 + 1) + (Y(p1 + 1) - YSap) * Tan(BSap * PG / 180)
  X(p1) = XSap: Y(p1) = YSap: B(p1) = BSap
  SAP = (IL - Sqr(XSap ^ 2 + YSap ^ 2)) * 2
  '
  'Spessore
  E38 = X(Np) * 2
  'Testa
  XCT = X(Np - 1) + RT * Cos(FnRad(B(Np - 1)))
  YCT = Y(Np - 1) + RT * Sin(FnRad(B(Np - 1)))
  'Addendum,altezza
  E43 = Y(Np) - (YCT - RT)
  E44 = IL - DIntV / 2 - YCT + RT
  '
  For i = Np - 1 To p1 Step -1: Y(i) = Y(p1) - Y(i): Next i
  hu1 = Y(Np - 1) - Y(p1)
  AngMed = Atn((X(Np - 1) - X(p1)) / hu1)
  PM = Int((Np - p1) / 2)
  PmB = 0: Bmax = 0
  For i = p1 + 1 To Np - 2
    XX = X(i) - X(p1) - Y(i) * Tan(AngMed)
    If Abs(XX) > Abs(Bmax) Then Bmax = XX: PmB = i
  Next i
  BOMB = -(Bmax + Bomb1 - Bomb2) * Cos(AngMed) '+ : cavit�, - : bombatura
  If PmB2 = 0 Then
    PB = 1 - (Y(PmB) - Y(p1)) / hu1     'posizione bombatura/cavit�
  Else
    PB = 1 - (YP2(PmB2) - YP2(0)) / hu1
  End If
  If BOMB = 0 Then PB = 0.5
  If PB > 0.9 Then PB = 0.9
  If PB < 0.1 Then PB = 0.1
  'Raggio testa/fondo
  For i = 1 To nPr
    da = (PG / 2 - B(Np - 1) * PG / 180) / nPr
    A = (PG / 2 - B(Np - 1) * PG / 180) - da * i
    X(Np - 1 + i) = XCT - RT * Sin(A)
    Y(Np - 1 + i) = YSap - (YCT - RT * Cos(A))
  Next i
  XCF = X(p1) - RF * Cos(BSap * PG / 180)
  YCF = Y(p1) + RF * Sin(BSap * PG / 180)
  For i = 1 To nPr
    da = (PG / 2 - B(p1) * PG / 180) / nPr
    A = (PG / 2 - B(p1) * PG / 180) - da * i
    X(Np - 1 + nPr + i) = XCF + RF * Sin(A)
    Y(Np - 1 + nPr + i) = YCF - RF * Cos(A)
  Next i
  '
  'registra punti su file
  Open g_chOemPATH & "\" & FileTEMPORANEO For Output As #1
    '
    For i = p2 + 1 + 2 * nPr - 1 To nPr + p2 + 1 Step -1
      Print #1, X(i), Y(i)
    Next i
    For i = p1 To p2
      Print #1, X(i), Y(i)
    Next i
    For i = p2 + 1 To nPr + p2
      Print #1, X(i), Y(i)
    Next i
    '
  Close #1
  '
  Np = p2 + 1 + nPr * 2 - p1
  '
  Open g_chOemPATH & "\" & FileTEMPORANEO For Input As #1
    For i = 1 To Np - 1
      Input #1, X(i), Y(i)
    Next i
  Close #1
  '
  E38V = E38
  AngMedF1V = AngMed
  AngMedF2V = AngMed
  E43F1V = E43
  E43F2V = E43
  E44F1V = E44
  E44F2V = E44
  '
  PBF1V = PB
  PBF2V = PB
  BombF1V = BOMB
  BombF2V = BOMB
  '
  CALCOLO_VITE_ZC = True
  '
Exit Function
   
errCALCOLO_VITE_ZC:
  WRITE_DIALOG "SUB : CALCOLO_VITE_ZC " & Error(Err)
  Exit Function
  
End Function

Public Sub CALCOLO_T0_FI0_A()
'
Dim Sonx As Double
'
On Error Resume Next
  '
  Sonx = SpNormV / Cos(PG / 2 - BetaIngRad)
  Sx = Sonx + (Rprim - RX) * 2 * Tan(ApAssVRad)
  VX = PassoElica / NFil - Sx
  T0 = PG / 2 - FnRad(VX * 180 / PassoElica)
  Ax = Atn((Tan(ApAssVRad) * 2 * PG * RX) / PassoElica)
  FI0 = T0 - Ax
  If T0 < 0 Then FI0 = FI0 - 2 * PG
  If FI0 > PG Then FI0 = FI0 - PG * 2
  If FI0 < -PG Then FI0 = FI0 + PG * 2
  '
End Sub
'
'INPUT : SIGMA, PassoElica, Rx, T0, FI0
'OUTPUT: Wprof, Rprof, Tpromolag
'
Public Sub CALCOLO_PUNTO_MOLA(ByVal IL As Double, ByVal SIGMA As Double, ByVal PassoElica As Double, ByVal RX As Double, ByVal T0 As Double, ByVal FI0 As Double, Wprof As Double, Rprof As Double, Tpromolag As Double)
'
Dim Rmin      As Double
Dim Tp        As Double
Dim DRM       As Double
Dim RMPD      As Double
Dim RQC       As Double
Dim AA        As Double
Dim BB        As Double
Dim cc        As Double
Dim brid      As Double
Dim Rd        As Double
Dim Crid      As Double
Dim RAD       As Double
Dim T1        As Double
Dim T2        As Double
Dim Ti        As Double
Dim TSI       As Double
Dim TSIa      As Double
Dim F         As Double
Dim FP        As Double
Dim TT        As Double
Dim XS        As Double
Dim Ys        As Double
Dim Zs        As Double
Dim TSPF      As Double
Dim Ap        As Double
Dim Lp        As Double
Dim ArgSeno   As Double
Dim Bp        As Double
Dim Cp        As Double
Dim Tpromola  As Double
Dim Uprof     As Double
Dim Vprof     As Double
Dim Zprof     As Double
'
On Error Resume Next
  '
  Rmin = PassoElica / (2 * PG * Tan(SIGMA))
  '
  Tp = PassoElica / 2 / PG
  DRM = IL * Rmin
  RMPD = Rmin + IL
  '
  RQC = RX * RX * Cos(T0 - FI0)
  '
  AA = RQC * Sin(T0) + (2 * Tp * Tp + DRM) * Sin(FI0)
  BB = (DRM + Tp * Tp) * Cos(FI0) + RQC * Cos(T0)
  cc = 2 * (-DRM * Sin(FI0) - RQC * Sin(T0) + RX * RMPD * Cos(T0 - FI0))
  '
  brid = BB / AA
  Crid = cc / AA
  RAD = brid * brid - Crid
  '
  RAD = Sqr(RAD)
  T1 = brid + RAD: T2 = brid - RAD
  Ti = T2
  If Abs(T2) > Abs(T1) Then Ti = T1
  If T0 < 0 Then Ti = T1
  If Ti >= PG Then Ti = PG * 0.93
  TSIa = Ti
  For j = 1 To 10
    TSI = TSIa
    F = RQC * Sin(T0 + TSI) + DRM * Sin(FI0 + TSI) + Tp * Tp * TSI * Cos(FI0 + TSI) - RX * RMPD * Cos(T0 - FI0)
    FP = RQC * Cos(T0 + TSI) + DRM * Cos(FI0 + TSI) + Tp * Tp * Cos(FI0 + TSI) - Tp * Tp * TSI * Sin(FI0 + TSI)
    TSIa = TSI - F / FP
  Next j
  Ti = TSIa
  TT = T0 + Ti
  XS = RX * Cos(TT): Ys = RX * Sin(TT): Zs = Tp * Ti
  TSPF = Ti + FI0
  Ap = Tan(TSPF) / XS: Bp = -1 / XS: Cp = (1 + Ys * Tan(TSPF) / XS) / Tp
  Lp = Tan(SIGMA)
  ArgSeno = (Ap * Lp + Cp) / (Sqr(Ap * Ap + Bp * Bp + Cp * Cp) * Sqr(Lp * Lp + 1))
  Tpromola = Abs(Asn(ArgSeno))
  Tpromolag = 90 - Tpromola * 180 / PG
  Uprof = XS * Cos(SIGMA) - Zs * Sin(SIGMA): Vprof = Ys - IL
  Wprof = Zs * Cos(SIGMA) + XS * Sin(SIGMA)
  Rprof = Sqr(Uprof ^ 2 + Vprof ^ 2)
  '
End Sub

Sub CALCOLO_VITE_DA_MOLA(ByVal EntMolaVis As Double, ByVal IncV As Double, ByVal PAS As Double, Rayon As Double, ALFA As Double, Beta As Double, Iax As Double, ApAx As Double, ExMol As Double, RxMol As Double, APF As Double, Ltang As Double)
'
Dim SENS  As Integer
Dim v7    As Double
Dim Y1    As Double
Dim X1    As Double
Dim ang1  As Double
Dim YAPP  As Double
Dim XAPP  As Double
Dim Helx  As Double
Dim V12   As Double
Dim V13   As Double
Dim V14   As Double
Dim V15   As Double
Dim V16   As Double
'
Dim BataIng_LOC As Double
'
On Error Resume Next
  '
  BataIng_LOC = PG / 2 - IncV
  '
  If (ExMol < 0) Then SENS = -1 Else SENS = 1
  ExMol = ExMol * SENS: APF = APF * SENS: v7 = PAS / 2 / PG
  '
  RxMol = RxMol + 0.02: ExMol = ExMol - 0.02 * Tan(APF)
  GoSub CoordonneeVisObtenue: Y1 = YAPP: X1 = XAPP
  '
  RxMol = RxMol - 0.04: ExMol = ExMol + 0.04 * Tan(APF)
  GoSub CoordonneeVisObtenue: ang1 = Atn((X1 - XAPP) / (YAPP - Y1))
  '
  RxMol = RxMol + 0.02: ExMol = ExMol - 0.02 * Tan(APF)
  GoSub CoordonneeVisObtenue
  If (YAPP < Y1) Then ang1 = PG + ang1
  '
  Helx = Atn(PAS / PG / Rayon / 2)
  Beta = (ang1 - ALFA) * SENS
  ApAx = Atn(Tan(Beta) * Tan(Helx))
  ALFA = ALFA * SENS
  '
Exit Sub

CoordonneeVisObtenue:
  '
  V12 = -Tan(APF) * (EntMolaVis * Sin(BataIng_LOC) + v7 * Cos(BataIng_LOC))
  V13 = v7 * Sin(BataIng_LOC) - EntMolaVis * Cos(BataIng_LOC)
  V14 = (RxMol - ExMol * Tan(APF)) * Cos(BataIng_LOC)
  'V15 = (V14 ^ 2) + (V12 ^ 2) - (V13 ^ 2)
  V15 = Sqr((V14 ^ 2) + (V12 ^ 2) - (V13 ^ 2))
  V16 = -(V15 + V12) / (V13 - V14)
  V15 = (V15 - V12) / (V13 - V14)
  If (Abs(V15) < Abs(V16)) Then
    V12 = 2 * Atn(V15)
  Else
    V12 = 2 * Atn(V16)
  End If
  V13 = (RxMol * Sin(BataIng_LOC) * Sin(V12)) + ExMol * Cos(BataIng_LOC)
  V13 = Atn(V13 / (RxMol * Cos(V12) - EntMolaVis))
  Rayon = (EntMolaVis - RxMol * Cos(V12)) / Cos(V13)
  Ltang = ExMol * Sin(BataIng_LOC) - RxMol * Sin(V12) * Cos(BataIng_LOC)
  ' Ltang : Distance axe meule / plan tangent
  Iax = Ltang - v7 * V13
  ALFA = 2 * PG * Iax / PAS
  ' Alfa = Iax / V7  '  idem
  YAPP = Rayon * Cos(ALFA): XAPP = -Rayon * Sin(ALFA)
  '
Return

End Sub

Sub VisObtN(Yutens As Double, Xutens As Double, ApUtens As Double, InUtens As Double, PAS As Double, Rayon As Double, ALFA As Double, Beta As Double, Iax As Double, ApAx As Double)
'
' Connus
Dim Aux   As Double
Dim Rmax  As Double
Dim iMax  As Double
Dim Rmin  As Double
Dim iMIN  As Double
Dim Pr    As Double
Dim Helx  As Double
'
On Error Resume Next
  '
  Pr = 0.1
  '
  Aux = Atn(((Xutens + Pr * Tan(ApUtens)) * Sin(InUtens)) / (Yutens + Pr))
  Rmax = (Yutens + Pr) / Cos(Aux)
  iMax = (Xutens + Pr * Tan(ApUtens)) * Cos(InUtens) + PAS * Aux / 2 / PG
  '
  Aux = Atn(((Xutens - Pr * Tan(ApUtens)) * Sin(InUtens)) / (Yutens - Pr))
  Rmin = (Yutens - Pr) / Cos(Aux)
  iMIN = (Xutens - Pr * Tan(ApUtens)) * Cos(InUtens) + PAS * Aux / 2 / PG
  '
  ApAx = Atn((iMax - iMIN) / (Rmax - Rmin))
  '
  Aux = Atn((Xutens * Sin(InUtens)) / Yutens)
  Rayon = Yutens / Cos(Aux)
  Iax = Xutens * Cos(InUtens) + PAS * Aux / 2 / PG
  '
  ALFA = 2 * PG * Iax / PAS
  Helx = Atn(PAS / PG / Rayon / 2)
  Beta = Atn(Tan(ApAx) / Tan(Helx))
  '
End Sub

Public Sub QR_Vite(Code As String, SpesN As Double, QRvite As Double, MnormV As Double, ApNormVrad As Double, DprimV As Double, NFil As Integer, DR As Double)
'
' Code = "E": Calcolo di SpesN conoscendo QRvite
' Code = "W": Calcolo di QRvite conoscendo SpesN
'
Dim Aux     As Double
Dim Aux1    As Double
Dim Alfa2   As Double
Dim i       As Integer
Dim PasRx   As Double
Dim PasAlfa As Double
Dim Helx    As Double
Dim Rayon   As Double
Dim ALFA    As Double
Dim Beta    As Double
Dim Hr      As Double
Dim Alfa1   As Double
Dim Xc      As Double
Dim AlfaC   As Double
Dim Iax     As Double
Dim ApAx    As Double
Dim Ltang   As Double
Dim Xutens  As Double
Dim Yutens  As Double
'
On Error GoTo errQuotaRulli
  '
  If (TipoVite = "ZC") Then
    'Calcolo spessore da quota rulli non presente
    If SpesN = 0 Then
      WRITE_DIALOG NewPrendi(40)
      Exit Sub
    End If
  End If
  '
  If (TipoVite = "ZN") Then
    'Noti : MnormV , apnormvrad , DprimV , Nfil , DR
    'QRvite o SpesN
    IncV = Asn(NFil * MnormV / DprimV)
    MAssV = MnormV / Cos(IncV)
    PassV = MAssV * NFil * PG
    If (Code = "W") Then
      ' Calcolo di QRvite conoscendo SpesN
      ' 1 ) calcolo YutProfN ( Distanza vertice fianchi utensile / asse vite )
      Alfa2 = 0.1
      Aux = (PassV / NFil - SpesN / Cos(IncV)) / 2
      PasAlfa = 0.1
      Do
        Alfa2 = Alfa2 - PasAlfa
        PasAlfa = PasAlfa / 10
        Do
          Alfa2 = Alfa2 + PasAlfa
          Aux1 = DprimV / 2 * Sin(Alfa2) / Tan(IncV) + PassV * Alfa2 / 2 / PG
        Loop While Aux1 < Aux
      Loop While PasAlfa > 0.000001
      Aux = (Aux - PassV * Alfa2 / 2 / PG) / Cos(IncV)
      YutProfN = DprimV / 2 * Cos(Alfa2) - Aux / Tan(ApNormVrad)
      ' 2 ) Calcolo Quota
      PasRx = 1
      Yutens = YutProfN + PasRx
      Do
        Yutens = Yutens - PasRx
        PasRx = PasRx / 10
        Do
          Yutens = Yutens + PasRx
          Xutens = (Yutens - YutProfN) * Tan(ApNormVrad)
          Call VisObtN(Yutens, Xutens, ApNormVrad, IncV, PassV, Rayon, ALFA, Beta, Iax, ApAx)
          GoSub CalcoloXcAlfaC
        Loop While AlfaC < 0
      Loop While PasRx > 0.00001
      QRvite = Xc * 2 + DR ' = QRvite quando AlfaC = 0
    Else
      ' Calcolo di SpesN conoscendo QRvite
      ' 1 ) Calcolo YutProfN
      YutProfN = (QRvite - DR) / 2 - DR / 2 / Sin(ApNormVrad)
      If YutProfN > (DiaSmusso / 2) Or (QRvite - DR) < DIntV Then
        WRITE_DIALOG NewPrendi(41)
        SpesN = 0
        Exit Sub
      End If
      For i = 1 To 10
        PasRx = 1
        Yutens = YutProfN
        Do
          Yutens = Yutens - PasRx
          PasRx = PasRx / 10
          Do
            Yutens = Yutens + PasRx
            Xutens = (Yutens - YutProfN) * Tan(ApNormVrad)
            Call VisObtN(Yutens, Xutens, ApNormVrad, IncV, PassV, Rayon, ALFA, Beta, Iax, ApAx)
            GoSub CalcoloXcAlfaC
          Loop While AlfaC < 0
        Loop While PasRx > 0.00001
        YutProfN = YutProfN + (QRvite / 2 - (Xc + DR / 2))
      Next i
      ' 2 ) Calcolo quota SpesN
      Dim A As Double
      Dim B As Double
      Dim C As Double
      A = Tan(ApNormVrad) * Sin(IncV)
      B = A ^ 2 * YutProfN
      C = (A ^ 2 * YutProfN ^ 2 - (DprimV / 2) ^ 2)
      Yutens = (B + Sqr(B ^ 2 - (1 + A ^ 2) * C)) / (1 + A ^ 2)
      Xutens = (Yutens - YutProfN) * Tan(ApNormVrad)
      Alfa2 = Asn(Xutens * Sin(IncV) / (DprimV / 2))
      Aux = PassV * Alfa2 / 2 / PG + Xutens * Cos(IncV)
      SpesN = (PassV / NFil - Aux * 2) * Cos(IncV)
    End If
  End If
  '
  If (TipoVite = "ZI") Then
    ' Connus : MnormV , apnormvrad , DprimV , Nfil , DR
    ' QRvite o SpesN
    Dim Pba As Double
    Dim Eb  As Double
    IncV = Asn(NFil * MnormV / DprimV)
    MAssV = MnormV / Cos(IncV)
    HelbV = Acs(Cos(ApNormVrad) * Cos(IncV))
    DbaseV = MAssV * NFil / Tan(HelbV)
    Pba = PG * DbaseV / NFil
    If (Code = "W") Then
      Aux = SpesN / Sin(IncV) / DprimV
      Eb = (Aux + inv(Acs(DbaseV / DprimV))) * DbaseV
      Aux = ((Eb - Pba) + DR / Sin(HelbV)) / DbaseV
      QRvite = DbaseV / Cos(Ainv(Aux)) + DR
    Else
      Aux = Acs(DbaseV / (QRvite - DR))
      Eb = DbaseV * inv(Aux) - DR / Sin(HelbV) + Pba
      Aux = Eb / DbaseV - inv(Acs(DbaseV / DprimV))
      SpesN = Aux * DprimV * Sin(IncV)
    End If
  End If
  '
  If (TipoVite = "ZK") Then
    ' Connus : MnormV , DintV , DiaMolTeo , DprimV , Nfil , DR
    ' QRvite o SpesN
    Dim RxMol As Double
    Dim ExMol As Double
    Dim InMol As Double
    Dim Esm   As Double
    EntMolaVis = (DIntV + DiaMolTeo) / 2
    RmolPrim = EntMolaVis - DprimV / 2
    IncV = Asn(NFil * MnormV / DprimV)
    MAssV = MnormV / Cos(IncV)
    PassV = MAssV * NFil * PG
    If (Code = "W") Then
      ' Calcul de QRvite connaissant SpesN
      EmolPrim = MnormV * PG - SpesN
      For i = 1 To 5
        ' Calcul de l'epaisseur de la meule
        Call CALCOLO_VITE_DA_MOLA(EntMolaVis, IncV, PassV, Rayon, ALFA, Beta, Iax, ApAx, EmolPrim / 2, RmolPrim, ApNormVrad, Ltang)
        Aux = Iax + (DprimV / 2 - Rayon) * Tan(ApAx)
        Aux = (MAssV * PG - Aux * 2) * Cos(IncV)
        EmolPrim = EmolPrim + Aux - SpesN
      Next i
      Esm = EmolPrim / 2 - (DiaMolTeo / 2 - RmolPrim) * Tan(ApNormVrad)
      RxMol = DiaMolTeo / 2: ExMol = Esm
      Call CALCOLO_VITE_DA_MOLA(EntMolaVis, IncV, PassV, Rayon, ALFA, Beta, Iax, ApAx, ExMol, RxMol, ApNormVrad, Ltang)
      GoSub CalcoloXcAlfaC
      If (AlfaC > 0) Then
        QRvite = 0
      Else
        PasRx = 0.1
        Do
          RxMol = RxMol + PasRx
          PasRx = PasRx / 10
          Do
            RxMol = RxMol - PasRx
            ExMol = Esm + (DiaMolTeo / 2 - RxMol) * Tan(ApNormVrad)
            Call CALCOLO_VITE_DA_MOLA(EntMolaVis, IncV, PassV, Rayon, ALFA, Beta, Iax, ApAx, ExMol, RxMol, ApNormVrad, Ltang)
            GoSub CalcoloXcAlfaC
          Loop While AlfaC < 0
        Loop While PasRx > 0.00001
        QRvite = Xc * 2 + DR ' = QRvite quand AlfaC = 0
      End If
    Else
      ' Calcul de SpesN connaissant QRvite
      Aux = (QRvite - DIntV) / 2 - DR / 2
      Esm = DR / 2 / Cos(ApNormVrad) - Aux * Tan(ApNormVrad) - 1
      Do
        ' calcul de l'epaisseur de la meule
        PasRx = 1
        RxMol = DiaMolTeo / 2 - PasRx
        Do
          RxMol = RxMol + PasRx
          PasRx = PasRx / 10
          Do
            RxMol = RxMol - PasRx
            ExMol = Esm + (DiaMolTeo / 2 - RxMol) * Tan(ApNormVrad)
            Call CALCOLO_VITE_DA_MOLA(EntMolaVis, IncV, PassV, Rayon, ALFA, Beta, Iax, ApAx, ExMol, RxMol, ApNormVrad, Ltang)
            GoSub CalcoloXcAlfaC
          Loop While AlfaC < 0
        Loop While PasRx > 0.00001
        Aux = Xc * 2 + DR
        Esm = Esm + (Aux - QRvite) * Tan(ApNormVrad) / 2
      Loop While Abs(Aux - QRvite) > 0.0001
      RxMol = DiaMolTeo / 2
      PasRx = 1
      Do
        RxMol = RxMol + PasRx
        PasRx = PasRx / 10
        Do
          RxMol = RxMol - PasRx
          ExMol = Esm + (DiaMolTeo / 2 - RxMol) * Tan(ApNormVrad)
          Call CALCOLO_VITE_DA_MOLA(EntMolaVis, IncV, PassV, Rayon, ALFA, Beta, Iax, ApAx, ExMol, RxMol, ApNormVrad, Ltang)
        Loop While Rayon < DprimV / 2 Or Beta < 0
      Loop While PasRx > 0.0001
      Aux = PG / NFil
      Aux = (Aux - ALFA) / Aux
      SpesN = MnormV * PG * Aux
    End If
  End If
  '
  If (TipoVite = "ZA") Then
    'Connus : MnormV , apassvrad , DprimV , Nfil , DR
    'QRvite o SpesN
    IncV = Asn(NFil * MnormV / DprimV)
    MAssV = MnormV / Cos(IncV)
    PassV = MAssV * NFil * PG
    If (Code = "W") Then
      'Calcul de QRvite connaissant SpesN
      Rayon = 10
      PasRx = 10
      Do
        Rayon = Rayon - PasRx
        PasRx = PasRx / 10
        Do
          Rayon = Rayon + PasRx
          Helx = Atn(PassV / PG / Rayon / 2)
          Beta = Atn(Tan(ApAssVRad) / Tan(Helx))
          Aux = (DprimV / 2 - Rayon) * Tan(ApAssVRad)
          Aux = (MAssV * PG - SpesN / Cos(IncV)) / 2 - Aux
          ALFA = 2 * PG * Aux / PassV
          GoSub CalcoloXcAlfaC
        Loop While AlfaC < 0
      Loop While PasRx > 0.00001
      Aux = AlfaC ' pour tester
      QRvite = Xc * 2 + DR ' = QRvite quand AlfaC = 0
    Else
      'Calcul de SpesN connaissant QRvite
      SpesN = 0
      For i = 1 To 10
        Rayon = 10
        PasRx = 10
        Do
          Rayon = Rayon - PasRx
          PasRx = PasRx / 10
          Do
            Rayon = Rayon + PasRx
            Helx = Atn(PassV / PG / Rayon / 2)
            Beta = Atn(Tan(ApAssVRad) / Tan(Helx))
            Aux = (DprimV / 2 - Rayon) * Tan(ApAssVRad)
            Aux = (MAssV * PG - SpesN / Cos(IncV)) / 2 - Aux
            ALFA = 2 * PG * Aux / PassV
            GoSub CalcoloXcAlfaC
          Loop While AlfaC < 0
        Loop While PasRx > 0.00001
        Aux = AlfaC ' pour tester
        SpesN = (QRvite - (Xc * 2 + DR)) * Tan(ApAssVRad) + SpesN ' = QRvite quand AlfaC = 0
      Next i
    End If
  End If
  '
Exit Sub

errQuotaRulli:
  WRITE_DIALOG "SUB : QR_Vite " & Error(Err)
  Exit Sub

CalcoloXcAlfaC:
   ' Il s'agit de la formule que j'avais etablie en 1993
   ' pour le controle profil de forme sur rectifieuse de profil
   ' ( DR etant la bille du palpeur )
   Hr = Atn(2 * PG * Rayon * Cos(Beta) / PassV)
   Aux = DR / 2 * Cos(Hr)
   Alfa1 = Atn(Cos(Beta) / (Sin(Beta) + Rayon / Aux))
   Aux = Cos(Hr) * Sin(Alfa1 + Beta)
   Xc = Rayon * Cos(Alfa1) + DR / 2 * Aux
   AlfaC = ALFA - Alfa1 - DR * Sin(Hr) * PG / PassV
Return

End Sub

Public Function CorQuotaRulli(Code$, CorQ As Double) As Boolean
' Code$ = "E": Calcolo di SpNormV conoscendo QR
' Code$ = "W": Calcolo di QR conoscendo SpNormV

Dim Aux     As Double
Dim Aux1    As Double
Dim Alfa2   As Double
Dim i       As Integer
Dim PasRx   As Double
Dim PasAlfa As Double
Dim Helx    As Double
Dim Rayon   As Double
Dim ALFA    As Double
Dim Beta    As Double
Dim Hr      As Double
Dim Alfa1   As Double
Dim Xc      As Double
Dim AlfaC   As Double
Dim Iax     As Double
Dim ApAx    As Double
Dim Ltang   As Double
Dim Xutens  As Double
Dim Yutens  As Double
'
Dim NewQR As Double

'
On Error GoTo errCorQuotaRulli
  '
  Code$ = "E"
  CorQuotaRulli = False
  '
  
  If (TipoVite = "ZN") Then
    '
    ' Noti : MnormV , apnormvrad , DprimV , Nfil , DR
    ' QR o SpNormV
    '
    IncV = Asn(NFil * MnormV / DprimV)
    MAssV = MnormV / Cos(IncV)
    PassV = MAssV * NFil * PG
    '
    If (Code$ = "W") Then
      '
      ' Calcolo di QR conoscendo SpNormV
      '
      ' 1 ) calcolo YutProfN ( Distanza vertice fianchi utensile / asse vite )
      Alfa2 = 0.1
      Aux = (PassV / NFil - SpNormV / Cos(IncV)) / 2
      PasAlfa = 0.1
      Do
        Alfa2 = Alfa2 - PasAlfa
        PasAlfa = PasAlfa / 10
        Do
          Alfa2 = Alfa2 + PasAlfa
          Aux1 = DprimV / 2 * Sin(Alfa2) / Tan(IncV) + PassV * Alfa2 / 2 / PG
        Loop While Aux1 < Aux
      Loop While PasAlfa > 0.000001
      Aux = (Aux - PassV * Alfa2 / 2 / PG) / Cos(IncV)
      YutProfN = DprimV / 2 * Cos(Alfa2) - Aux / Tan(ApNormVrad)
      '
      ' 2 ) Calcolo Quota
      PasRx = 1
      Yutens = YutProfN + PasRx
      Do
        Yutens = Yutens - PasRx
        PasRx = PasRx / 10
        Do
          Yutens = Yutens + PasRx
          Xutens = (Yutens - YutProfN) * Tan(ApNormVrad)
          Call VisObtN(Yutens, Xutens, ApNormVrad, IncV, PassV, Rayon, ALFA, Beta, Iax, ApAx)
          GoSub CalcoloXcAlfaC
        Loop While AlfaC < 0
      Loop While PasRx > 0.00001
      QR = Xc * 2 + DR ' = QR quando AlfaC = 0
      '
    Else
      '
      ' Calcolo di SpNormV conoscendo QR
      '
      NewQR = QR + CorQ
      ' 1 ) Calcolo YutProfN
      YutProfN = (NewQR - DR) / 2 - DR / 2 / Sin(ApNormVrad)
      If YutProfN > (DEXTV / 2) Or (NewQR - DR) < DIntV Then
        WRITE_DIALOG NewPrendi(41)
        SpNormV = 0
        Exit Function
      End If
      For i = 1 To 10
        PasRx = 1
        Yutens = YutProfN
        Do
          Yutens = Yutens - PasRx
          PasRx = PasRx / 10
          Do
            Yutens = Yutens + PasRx
            Xutens = (Yutens - YutProfN) * Tan(ApNormVrad)
            Call VisObtN(Yutens, Xutens, ApNormVrad, IncV, PassV, Rayon, ALFA, Beta, Iax, ApAx)
            GoSub CalcoloXcAlfaC
          Loop While AlfaC < 0
        Loop While PasRx > 0.00001
        YutProfN = YutProfN + (NewQR / 2 - (Xc + DR / 2))
      Next i
      '
      ' 2 ) Calcolo quota SpNormV
      Dim A As Double
      Dim B As Double
      Dim C As Double
      '
      A = Tan(ApNormVrad) * Sin(IncV)
      B = A ^ 2 * YutProfN
      C = (A ^ 2 * YutProfN ^ 2 - (DprimV / 2) ^ 2)
      Yutens = (B + Sqr(B ^ 2 - (1 + A ^ 2) * C)) / (1 + A ^ 2)
      Xutens = (Yutens - YutProfN) * Tan(ApNormVrad)
      Alfa2 = Asn(Xutens * Sin(IncV) / (DprimV / 2))
      Aux = PassV * Alfa2 / 2 / PG + Xutens * Cos(IncV)
      SpNormV = (PassV / NFil - Aux * 2) * Cos(IncV)
      '
    End If
    '
  End If
  '
  If (TipoVite = "ZI") Then
    ' Connus : MnormV , apnormvrad , DprimV , Nfil , DR
    ' QR o SpNormV
    Dim Pba As Double
    Dim Eb  As Double
    IncV = Asn(NFil * MnormV / DprimV)
    MAssV = MnormV / Cos(IncV)
    HelbV = Acs(Cos(ApNormVrad) * Cos(IncV))
    DbaseV = MAssV * NFil / Tan(HelbV)
    Pba = PG * DbaseV / NFil
    If (Code$ = "W") Then
      Aux = SpNormV / Sin(IncV) / DprimV
      Eb = (Aux + inv(Acs(DbaseV / DprimV))) * DbaseV
      Aux = ((Eb - Pba) + DR / Sin(HelbV)) / DbaseV
      QR = DbaseV / Cos(Ainv(Aux)) + DR
    Else
      NewQR = QR + CorQ
      Aux = Acs(DbaseV / (NewQR - DR))
      Eb = DbaseV * inv(Aux) - DR / Sin(HelbV) + Pba
      Aux = Eb / DbaseV - inv(Acs(DbaseV / DprimV))
      SpNormV = Aux * DprimV * Sin(IncV)
    End If
  End If
  '
  If (TipoVite = "ZK") Then
    '
    ' Connus : MnormV , DintV , DiaMolTeo , DprimV , Nfil , DR
    ' QR o SpNormV
    '
    Dim RxMol As Double
    Dim ExMol As Double
    Dim InMol As Double
    Dim Esm   As Double
    '
    EntMolaVis = (DIntV + DiaMolTeo) / 2
    RmolPrim = EntMolaVis - DprimV / 2
    IncV = Asn(NFil * MnormV / DprimV)
    MAssV = MnormV / Cos(IncV)
    PassV = MAssV * NFil * PG
    '
    If (Code$ = "W") Then
      '
      ' Calcul de QR connaissant SpNormV
      EmolPrim = MnormV * PG - SpNormV
      For i = 1 To 5
        ' Calcul de l'epaisseur de la meule
        Call CALCOLO_VITE_DA_MOLA(EntMolaVis, IncV, PassV, Rayon, ALFA, Beta, Iax, ApAx, EmolPrim / 2, RmolPrim, ApNormVrad, Ltang)
        Aux = Iax + (DprimV / 2 - Rayon) * Tan(ApAx)
        Aux = (MAssV * PG - Aux * 2) * Cos(IncV)
        EmolPrim = EmolPrim + Aux - SpNormV
      Next i
      Esm = EmolPrim / 2 - (DiaMolTeo / 2 - RmolPrim) * Tan(ApNormVrad)
      RxMol = DiaMolTeo / 2: ExMol = Esm
      Call CALCOLO_VITE_DA_MOLA(EntMolaVis, IncV, PassV, Rayon, ALFA, Beta, Iax, ApAx, ExMol, RxMol, ApNormVrad, Ltang)
      GoSub CalcoloXcAlfaC
      '
      If (AlfaC > 0) Then
        QR = 0
      Else
        PasRx = 0.1
        Do
          RxMol = RxMol + PasRx
          PasRx = PasRx / 10
          Do
            RxMol = RxMol - PasRx
            ExMol = Esm + (DiaMolTeo / 2 - RxMol) * Tan(ApNormVrad)
            Call CALCOLO_VITE_DA_MOLA(EntMolaVis, IncV, PassV, Rayon, ALFA, Beta, Iax, ApAx, ExMol, RxMol, ApNormVrad, Ltang)
            GoSub CalcoloXcAlfaC
          Loop While AlfaC < 0
        Loop While PasRx > 0.00001
        QR = Xc * 2 + DR ' = QR quand AlfaC = 0
      End If
      '
    Else
      '
      ' Calcul de SpNormV connaissant QR
      NewQR = QR + CorQ
      Aux = (NewQR - DIntV) / 2 - DR / 2
      Esm = DR / 2 / Cos(ApNormVrad) - Aux * Tan(ApNormVrad) - 1
      Do
        ' calcul de l'epaisseur de la meule
        PasRx = 1
        RxMol = DiaMolTeo / 2 - PasRx
        Do
          RxMol = RxMol + PasRx
          PasRx = PasRx / 10
          Do
            RxMol = RxMol - PasRx
            ExMol = Esm + (DiaMolTeo / 2 - RxMol) * Tan(ApNormVrad)
            Call CALCOLO_VITE_DA_MOLA(EntMolaVis, IncV, PassV, Rayon, ALFA, Beta, Iax, ApAx, ExMol, RxMol, ApNormVrad, Ltang)
            GoSub CalcoloXcAlfaC
          Loop While AlfaC < 0
        Loop While PasRx > 0.00001
        Aux = Xc * 2 + DR
        Esm = Esm + (Aux - NewQR) * Tan(ApNormVrad) / 2
      Loop While Abs(Aux - NewQR) > 0.0001
      RxMol = DiaMolTeo / 2
      PasRx = 1
      Do
        RxMol = RxMol + PasRx
        PasRx = PasRx / 10
        Do
          RxMol = RxMol - PasRx
          ExMol = Esm + (DiaMolTeo / 2 - RxMol) * Tan(ApNormVrad)
          Call CALCOLO_VITE_DA_MOLA(EntMolaVis, IncV, PassV, Rayon, ALFA, Beta, Iax, ApAx, ExMol, RxMol, ApNormVrad, Ltang)
        Loop While Rayon < DprimV / 2 Or Beta < 0
      Loop While PasRx > 0.0001
      Aux = PG / NFil
      Aux = (Aux - ALFA) / Aux
      SpNormV = MnormV * PG * Aux
      '
    End If
    '
  End If
  '
  If (TipoVite = "ZA") Then
    '
    ' Connus : MnormV , apassvrad , DprimV , Nfil , DR
    ' QR o SpNormV
    '
    IncV = Asn(NFil * MnormV / DprimV)
    MAssV = MnormV / Cos(IncV)
    PassV = MAssV * NFil * PG
    '
    If (Code$ = "W") Then
      '
      ' Calcul de QR connaissant SpNormV
      Rayon = 10
      PasRx = 10
      Do
        Rayon = Rayon - PasRx
        PasRx = PasRx / 10
        Do
          Rayon = Rayon + PasRx
          Helx = Atn(PassV / PG / Rayon / 2)
          Beta = Atn(Tan(ApAssVRad) / Tan(Helx))
          Aux = (DprimV / 2 - Rayon) * Tan(ApAssVRad)
          Aux = (MAssV * PG - SpNormV / Cos(IncV)) / 2 - Aux
          ALFA = 2 * PG * Aux / PassV
          GoSub CalcoloXcAlfaC
        Loop While AlfaC < 0
      Loop While PasRx > 0.00001
      Aux = AlfaC ' pour tester
      QR = Xc * 2 + DR ' = QR quand AlfaC = 0
      '
    Else
      '
      ' Calcul de SpNormV connaissant QR
      SpNormV = 0
      For i = 1 To 10
        Rayon = 10
        PasRx = 10
        Do
          Rayon = Rayon - PasRx
          PasRx = PasRx / 10
          Do
            Rayon = Rayon + PasRx
            Helx = Atn(PassV / PG / Rayon / 2)
            Beta = Atn(Tan(ApAssVRad) / Tan(Helx))
            Aux = (DprimV / 2 - Rayon) * Tan(ApAssVRad)
            Aux = (MAssV * PG - SpNormV / Cos(IncV)) / 2 - Aux
            ALFA = 2 * PG * Aux / PassV
            GoSub CalcoloXcAlfaC
          Loop While AlfaC < 0
        Loop While PasRx > 0.00001
        Aux = AlfaC ' pour tester
        NewQR = QR + CorQ
        SpNormV = (NewQR - (Xc * 2 + DR)) * Tan(ApAssVRad) + SpNormV ' = QR quand AlfaC = 0
      Next i
      '
    End If
    '
  End If
  '
  CorQuotaRulli = True
  '
Exit Function

errCorQuotaRulli:
  WRITE_DIALOG "SUB : CorQuotaRulli " & Error(Err)

CalcoloXcAlfaC:
  '
  ' Il s'agit de la formule que j'avais etablie en 1993
  ' pour le controle profil de forme sur rectifieuse de profil
  ' ( DR etant la bille du palpeur )
  Hr = Atn(2 * PG * Rayon * Cos(Beta) / PassV)
  Aux = DR / 2 * Cos(Hr)
  Alfa1 = Atn(Cos(Beta) / (Sin(Beta) + Rayon / Aux))
  Aux = Cos(Hr) * Sin(Alfa1 + Beta)
  Xc = Rayon * Cos(Alfa1) + DR / 2 * Aux
  AlfaC = ALFA - Alfa1 - DR * Sin(Hr) * PG / PassV
Return

End Function

' Code$ = "E": Calcolo di SpNormV conoscendo QR
' Code$ = "W": Calcolo di QR conoscendo SpNormV
Public Function QuotaRulli(Code$) As Boolean
'
Dim Aux     As Double
Dim Aux1    As Double
Dim Alfa2   As Double
Dim i       As Integer
Dim PasRx   As Double
Dim PasAlfa As Double
Dim Helx    As Double
Dim Rayon   As Double
Dim ALFA    As Double
Dim Beta    As Double
Dim Hr      As Double
Dim Alfa1   As Double
Dim Xc      As Double
Dim AlfaC   As Double
Dim Iax     As Double
Dim ApAx    As Double
Dim Ltang   As Double
Dim Xutens  As Double
Dim Yutens  As Double
'
On Error GoTo errQuotaRulli
  '
  QuotaRulli = False
  '
  If (TipoVite = "ZC") Then
    '
    'Calcolo spessore da quota rulli non presente
    If (SpNormV = 0) Then
      WRITE_DIALOG NewPrendi(40)
      Exit Function
    End If
    '
  End If
  '
  If (TipoVite = "ZN") Then
    '
    ' Noti : MnormV , apnormvrad , DprimV , Nfil , DR
    ' QR o SpNormV
    '
    IncV = Asn(NFil * MnormV / DprimV)
    MAssV = MnormV / Cos(IncV)
    PassV = MAssV * NFil * PG
    '
    If (Code$ = "W") Then
      '
      ' Calcolo di QR conoscendo SpNormV
      '
      ' 1 ) calcolo YutProfN ( Distanza vertice fianchi utensile / asse vite )
      Alfa2 = 0.1
      Aux = (PassV / NFil - SpNormV / Cos(IncV)) / 2
      PasAlfa = 0.1
      Do
        Alfa2 = Alfa2 - PasAlfa
        PasAlfa = PasAlfa / 10
        Do
          Alfa2 = Alfa2 + PasAlfa
          Aux1 = DprimV / 2 * Sin(Alfa2) / Tan(IncV) + PassV * Alfa2 / 2 / PG
        Loop While Aux1 < Aux
      Loop While PasAlfa > 0.000001
      Aux = (Aux - PassV * Alfa2 / 2 / PG) / Cos(IncV)
      YutProfN = DprimV / 2 * Cos(Alfa2) - Aux / Tan(ApNormVrad)
      '
      ' 2 ) Calcolo Quota
      PasRx = 1
      Yutens = YutProfN + PasRx
      Do
        Yutens = Yutens - PasRx
        PasRx = PasRx / 10
        Do
          Yutens = Yutens + PasRx
          Xutens = (Yutens - YutProfN) * Tan(ApNormVrad)
          Call VisObtN(Yutens, Xutens, ApNormVrad, IncV, PassV, Rayon, ALFA, Beta, Iax, ApAx)
          GoSub CalcoloXcAlfaC
        Loop While AlfaC < 0
      Loop While PasRx > 0.00001
      QR = Xc * 2 + DR ' = QR quando AlfaC = 0
      '
    Else
      '
      ' Calcolo di SpNormV conoscendo QR
      '
      ' 1 ) Calcolo YutProfN
      'DiaSmusso = DExtV 16.04.09
      YutProfN = (QR - DR) / 2 - DR / 2 / Sin(ApNormVrad)
      If YutProfN > (DEXTV / 2) Or (QR - DR) < DIntV Then
        WRITE_DIALOG NewPrendi(41)
        SpNormV = 0
        Exit Function
      End If
      For i = 1 To 10
        PasRx = 1
        Yutens = YutProfN
        Do
          Yutens = Yutens - PasRx
          PasRx = PasRx / 10
          Do
            Yutens = Yutens + PasRx
            Xutens = (Yutens - YutProfN) * Tan(ApNormVrad)
            Call VisObtN(Yutens, Xutens, ApNormVrad, IncV, PassV, Rayon, ALFA, Beta, Iax, ApAx)
            GoSub CalcoloXcAlfaC
          Loop While AlfaC < 0
        Loop While PasRx > 0.00001
        YutProfN = YutProfN + (QR / 2 - (Xc + DR / 2))
      Next i
      '
      ' 2 ) Calcolo quota SpNormV
      Dim A As Double
      Dim B As Double
      Dim C As Double
      '
      A = Tan(ApNormVrad) * Sin(IncV)
      B = A ^ 2 * YutProfN
      C = (A ^ 2 * YutProfN ^ 2 - (DprimV / 2) ^ 2)
      Yutens = (B + Sqr(B ^ 2 - (1 + A ^ 2) * C)) / (1 + A ^ 2)
      Xutens = (Yutens - YutProfN) * Tan(ApNormVrad)
      Alfa2 = Asn(Xutens * Sin(IncV) / (DprimV / 2))
      Aux = PassV * Alfa2 / 2 / PG + Xutens * Cos(IncV)
      SpNormV = (PassV / NFil - Aux * 2) * Cos(IncV)
      '
    End If
    '
  End If
  '
  If (TipoVite = "ZI") Then
    ' Connus : MnormV , apnormvrad , DprimV , Nfil , DR
    ' QR o SpNormV
    Dim Pba As Double
    Dim Eb  As Double
    IncV = Asn(NFil * MnormV / DprimV)
    MAssV = MnormV / Cos(IncV)
    HelbV = Acs(Cos(ApNormVrad) * Cos(IncV))
    DbaseV = MAssV * NFil / Tan(HelbV)
    Pba = PG * DbaseV / NFil
    If (Code$ = "W") Then
      Aux = SpNormV / Sin(IncV) / DprimV
      Eb = (Aux + inv(Acs(DbaseV / DprimV))) * DbaseV
      Aux = ((Eb - Pba) + DR / Sin(HelbV)) / DbaseV
      QR = DbaseV / Cos(Ainv(Aux)) + DR
    Else
      Aux = Acs(DbaseV / (QR - DR))
      Eb = DbaseV * inv(Aux) - DR / Sin(HelbV) + Pba
      Aux = Eb / DbaseV - inv(Acs(DbaseV / DprimV))
      SpNormV = Aux * DprimV * Sin(IncV)
    End If
  End If
  '
  If (TipoVite = "ZK") Then
    '
    ' Connus : MnormV , DintV , DiaMolTeo , DprimV , Nfil , DR
    ' QR o SpNormV
    '
    Dim RxMol As Double
    Dim ExMol As Double
    Dim InMol As Double
    Dim Esm   As Double
    '
    EntMolaVis = (DIntV + DiaMolTeo) / 2
    RmolPrim = EntMolaVis - DprimV / 2
    IncV = Asn(NFil * MnormV / DprimV)
    MAssV = MnormV / Cos(IncV)
    PassV = MAssV * NFil * PG
    '
    If (Code$ = "W") Then
      '
      ' Calcul de QR connaissant SpNormV
      EmolPrim = MnormV * PG - SpNormV
      For i = 1 To 5
        ' Calcul de l'epaisseur de la meule
        Call CALCOLO_VITE_DA_MOLA(EntMolaVis, IncV, PassV, Rayon, ALFA, Beta, Iax, ApAx, EmolPrim / 2, RmolPrim, ApNormVrad, Ltang)
        Aux = Iax + (DprimV / 2 - Rayon) * Tan(ApAx)
        Aux = (MAssV * PG - Aux * 2) * Cos(IncV)
        EmolPrim = EmolPrim + Aux - SpNormV
      Next i
      Esm = EmolPrim / 2 - (DiaMolTeo / 2 - RmolPrim) * Tan(ApNormVrad)
      RxMol = DiaMolTeo / 2: ExMol = Esm
      Call CALCOLO_VITE_DA_MOLA(EntMolaVis, IncV, PassV, Rayon, ALFA, Beta, Iax, ApAx, ExMol, RxMol, ApNormVrad, Ltang)
      GoSub CalcoloXcAlfaC
      '
      If (AlfaC > 0) Then
        QR = 0
      Else
        PasRx = 0.1
        Do
          RxMol = RxMol + PasRx
          PasRx = PasRx / 10
          Do
            RxMol = RxMol - PasRx
            ExMol = Esm + (DiaMolTeo / 2 - RxMol) * Tan(ApNormVrad)
            Call CALCOLO_VITE_DA_MOLA(EntMolaVis, IncV, PassV, Rayon, ALFA, Beta, Iax, ApAx, ExMol, RxMol, ApNormVrad, Ltang)
            GoSub CalcoloXcAlfaC
          Loop While AlfaC < 0
        Loop While PasRx > 0.00001
        QR = Xc * 2 + DR ' = QR quand AlfaC = 0
      End If
      '
    Else
      '
      ' Calcul de SpNormV connaissant QR
      Aux = (QR - DIntV) / 2 - DR / 2
      Esm = DR / 2 / Cos(ApNormVrad) - Aux * Tan(ApNormVrad) - 1
      Do
        ' calcul de l'epaisseur de la meule
        PasRx = 1
        RxMol = DiaMolTeo / 2 - PasRx
        Do
          RxMol = RxMol + PasRx
          PasRx = PasRx / 10
          Do
            RxMol = RxMol - PasRx
            ExMol = Esm + (DiaMolTeo / 2 - RxMol) * Tan(ApNormVrad)
            Call CALCOLO_VITE_DA_MOLA(EntMolaVis, IncV, PassV, Rayon, ALFA, Beta, Iax, ApAx, ExMol, RxMol, ApNormVrad, Ltang)
            GoSub CalcoloXcAlfaC
          Loop While AlfaC < 0
        Loop While PasRx > 0.00001
        Aux = Xc * 2 + DR
        Esm = Esm + (Aux - QR) * Tan(ApNormVrad) / 2
      Loop While Abs(Aux - QR) > 0.0001
      RxMol = DiaMolTeo / 2
      PasRx = 1
      Do
        RxMol = RxMol + PasRx
        PasRx = PasRx / 10
        Do
          RxMol = RxMol - PasRx
          ExMol = Esm + (DiaMolTeo / 2 - RxMol) * Tan(ApNormVrad)
          Call CALCOLO_VITE_DA_MOLA(EntMolaVis, IncV, PassV, Rayon, ALFA, Beta, Iax, ApAx, ExMol, RxMol, ApNormVrad, Ltang)
        Loop While Rayon < DprimV / 2 Or Beta < 0
      Loop While PasRx > 0.0001
      Aux = PG / NFil
      Aux = (Aux - ALFA) / Aux
      SpNormV = MnormV * PG * Aux
      '
    End If
    '
  End If
  '
  If (TipoVite = "ZA") Then
    '
    ' Connus : MnormV , apassvrad , DprimV , Nfil , DR
    ' QR o SpNormV
    '
    IncV = Asn(NFil * MnormV / DprimV)
    MAssV = MnormV / Cos(IncV)
    PassV = MAssV * NFil * PG
    '
    If (Code$ = "W") Then
      '
      ' Calcul de QR connaissant SpNormV
      Rayon = 10
      PasRx = 10
      Do
        Rayon = Rayon - PasRx
        PasRx = PasRx / 10
        Do
          Rayon = Rayon + PasRx
          Helx = Atn(PassV / PG / Rayon / 2)
          Beta = Atn(Tan(ApAssVRad) / Tan(Helx))
          Aux = (DprimV / 2 - Rayon) * Tan(ApAssVRad)
          Aux = (MAssV * PG - SpNormV / Cos(IncV)) / 2 - Aux
          ALFA = 2 * PG * Aux / PassV
          GoSub CalcoloXcAlfaC
        Loop While AlfaC < 0
      Loop While PasRx > 0.00001
      Aux = AlfaC ' pour tester
      QR = Xc * 2 + DR ' = QR quand AlfaC = 0
      '
    Else
      '
      ' Calcul de SpNormV connaissant QR
      SpNormV = 0
      For i = 1 To 10
        Rayon = 10
        PasRx = 10
        Do
          Rayon = Rayon - PasRx
          PasRx = PasRx / 10
          Do
            Rayon = Rayon + PasRx
            Helx = Atn(PassV / PG / Rayon / 2)
            Beta = Atn(Tan(ApAssVRad) / Tan(Helx))
            Aux = (DprimV / 2 - Rayon) * Tan(ApAssVRad)
            Aux = (MAssV * PG - SpNormV / Cos(IncV)) / 2 - Aux
            ALFA = 2 * PG * Aux / PassV
            GoSub CalcoloXcAlfaC
          Loop While AlfaC < 0
        Loop While PasRx > 0.00001
        Aux = AlfaC ' pour tester
        SpNormV = (QR - (Xc * 2 + DR)) * Tan(ApAssVRad) + SpNormV ' = QR quand AlfaC = 0
      Next i
      '
    End If
    '
  End If
  '
  QuotaRulli = True
  '
Exit Function

errQuotaRulli:
  WRITE_DIALOG "SUB : QuotaRulli " & Error(Err)
  Exit Function

CalcoloXcAlfaC:
  '
  ' Il s'agit de la formule que j'avais etablie en 1993
  ' pour le controle profil de forme sur rectifieuse de profil
  ' ( DR etant la bille du palpeur )
  Hr = Atn(2 * PG * Rayon * Cos(Beta) / PassV)
  Aux = DR / 2 * Cos(Hr)
  Alfa1 = Atn(Cos(Beta) / (Sin(Beta) + Rayon / Aux))
  Aux = Cos(Hr) * Sin(Alfa1 + Beta)
  Xc = Rayon * Cos(Alfa1) + DR / 2 * Aux
  AlfaC = ALFA - Alfa1 - DR * Sin(Hr) * PG / PassV
Return

End Function

Public Sub PROFILO_VITE_ZN(i As Integer)
'
Dim Aux     As Double
Dim Aux1    As Double
Dim Alfa2   As Double
Dim PasRx   As Double
Dim PasAlfa As Double
Dim Helx    As Double
Dim Rayon   As Double
Dim ALFA    As Double
Dim Beta    As Double
Dim Hr      As Double
Dim Alfa1   As Double
Dim Xc      As Double
Dim AlfaC   As Double
Dim Iax     As Double
Dim ApAx    As Double
Dim Ltang   As Double
Dim Xutens  As Double
Dim Yutens  As Double
Dim Dia_Ext As Double
'
  On Error Resume Next
  '
  IncV = Asn(NFil * MnormV / DprimV)
  MAssV = MnormV / Cos(IncV)
  PassV = MAssV * NFil * PG
  '
  ' 1 ) Cerco YutProfN
  Alfa2 = 0.1
  Aux = (PassV / NFil - SpNormV / Cos(IncV)) / 2
  PasAlfa = 0.1
  Do
    Alfa2 = Alfa2 - PasAlfa
    PasAlfa = PasAlfa / 10
    Do
      Alfa2 = Alfa2 + PasAlfa
      Aux1 = DprimV / 2 * Sin(Alfa2) / Tan(IncV) + PassV * Alfa2 / 2 / PG
    Loop While Aux1 < Aux
  Loop While PasAlfa > 0.000001
  Aux = (Aux - PassV * Alfa2 / 2 / PG) / Cos(IncV)
  YutProfN = DprimV / 2 * Cos(Alfa2) - Aux / Tan(ApNormVrad)
  '
  ' 2 ) Calcolo profilo vite
  PasRx = 0.1
  Yutens = YutProfN
  Do
    i = 0
    Yutens = Yutens + PasRx
    Xutens = (Yutens - YutProfN) * Tan(ApNormVrad)
    Call VisObtN(Yutens, Xutens, ApNormVrad, IncV, PassV, YP1(i), ALFA, Bp1(i), XP1(i), ApAx)
  Loop While YP1(i) < DIntV / 2
  '
  PasRx = 0.1
  Do
    i = i + 1
    Yutens = Yutens + PasRx
    Xutens = (Yutens - YutProfN) * Tan(ApNormVrad)
    Call VisObtN(Yutens, Xutens, ApNormVrad, IncV, PassV, YP1(i), ALFA, Bp1(i), XP1(i), ApAx)
    RX = YP1(i)
    T0 = PG / 2 - ALFA
    FI0 = T0 - Bp1(i): If T0 < 0 Then FI0 = FI0 - 2 * PG
    If FI0 > PG Then FI0 = FI0 - PG * 2
    If FI0 < -PG Then FI0 = FI0 + PG * 2
    Call CALCOLO_PUNTO_MOLA(IL, IncV, PassoElica, RX, T0, FI0, X(i), Y(i), B(i))
    '
    ApAx = Atn(Tan(Bp1(i)) * PassoElica / 2 / PG / RX * Cos(BetaVRad))
    Dia_Ext = 2 * (YP1(i) + PasRx + RT * (1 - Sin(ApAx)))
  Loop While Dia_Ext < DiaSmusso
  '
  PasRx = 0.001
  Do
    Yutens = Yutens + PasRx
    Xutens = (Yutens - YutProfN) * Tan(ApNormVrad)
    Call VisObtN(Yutens, Xutens, ApNormVrad, IncV, PassV, YP1(i), ALFA, Bp1(i), XP1(i), ApAx)
    RX = YP1(i)
    T0 = PG / 2 - ALFA
    FI0 = T0 - Bp1(i): If T0 < 0 Then FI0 = FI0 - 2 * PG
    If FI0 > PG Then FI0 = FI0 - PG * 2
    If FI0 < -PG Then FI0 = FI0 + PG * 2
    Call CALCOLO_PUNTO_MOLA(IL, IncV, PassoElica, RX, T0, FI0, X(i), Y(i), B(i))
    ApAx = Atn(Tan(Bp1(i)) * PassoElica / 2 / PG / RX * Cos(BetaVRad))
    Dia_Ext = 2 * (YP1(i) + RT * (1 - Sin(ApAx)))
  Loop While Dia_Ext < DiaSmusso
  '
  'primitivo
  i = i + 1
    Yutens = DprimV / 2
    Xutens = (Yutens - YutProfN) * Tan(ApNormVrad)
    'Xutens = (PassV / NFil - SpNormV / Cos(IncV)) / 2
    'Yutens = YutProfN + Xutens / Tan(apnormvrad)
    Call VisObtN(Yutens, Xutens, ApNormVrad, IncV, PassV, YP1(i), ALFA, Bp1(i), XP1(i), ApAx)
    RX = YP1(i)
    T0 = PG / 2 - FnRad(XP1(i) * 360 / PassoElica)
    FI0 = T0 - Bp1(i): If T0 < 0 Then FI0 = FI0 - 2 * PG
    If FI0 > PG Then FI0 = FI0 - PG * 2
    If FI0 < -PG Then FI0 = FI0 + PG * 2
    Call CALCOLO_PUNTO_MOLA(IL, IncV, PassoElica, RX, T0, FI0, X(i), Y(i), B(i))
  '
End Sub

Public Sub SOTTOPROGRAMMA_CAD_VISUALIZZA(ByVal Scala As Single, ByVal ZeroPezzoX As Single, ByVal ZeroPezzoY As Single, ByVal SiStampa As String)
StopRegieEvents
MsgBox ("Sub: Not S380 ready")
ResumeRegieEvents

''
'Dim PicW    As Single
'Dim PicH    As Single
'Dim ScalaX  As Single
'Dim ScalaY  As Single
'Dim stmp    As String
''
'Dim TipoCarattere As String
''
'Dim CENTRO_PROFILO  As Double
'Dim ALTEZZA_MOLA    As Double
'Dim LARGHEZZA_MOLA  As Double
''
'On Error GoTo errSOTTOPROGRAMMA_CAD_VISUALIZZA
'  '
'  CENTRO_PROFILO = val(OEM1.List1(1).List(5))
'  ALTEZZA_MOLA = val(OEM1.List1(1).List(4))
'  LARGHEZZA_MOLA = val(OEM1.List1(1).List(0))
'  '
'  ScalaX = Scala: ScalaY = Scala
'  '
'  'Acquisizione area grafica
'  stmp = GetInfo("DIS0", "PicW", Path_LAVORAZIONE_INI)
'  If Len(stmp) > 0 Then PicW = val(stmp) Else PicW = 190
'  stmp = GetInfo("DIS0", "PicH", Path_LAVORAZIONE_INI)
'  If Len(stmp) > 0 Then PicH = val(stmp) Else PicH = 95
'  '
'  TipoCarattere = GetInfo("DIS0", "TextFont", Path_LAVORAZIONE_INI)
'  '
'  'Preparazione grafico
'  If (SiStampa = "Y") Then
'    For i = 1 To 2
'      Printer.FontName = TipoCarattere
'      Printer.FontSize = 8
'      Printer.FontBold = False
'    Next i
'    Write_Dialog Printer.DeviceName & " PRINTING....."
'    If PicW = 190 And PicH = 95 Then
'      Printer.Orientation = 1
'    Else
'      Printer.Orientation = 2
'    End If
'    Printer.ScaleMode = 6
'  Else
'    OEM1.OEM1PicCALCOLO.Cls
'    OEM1.OEM1PicCALCOLO.ScaleWidth = PicW
'    OEM1.OEM1PicCALCOLO.ScaleHeight = PicH
'    For i = 1 To 2
'      OEM1.OEM1PicCALCOLO.FontName = TipoCarattere
'      OEM1.OEM1PicCALCOLO.FontSize = 8
'      OEM1.OEM1PicCALCOLO.FontBold = False
'    Next i
'    OEM1.OEM1PicCALCOLO.Refresh
'  End If
'  '
'  If (SiStampa = "Y") Then
'    Printer.Line (0, PicH / 2)-(PicW, PicH / 2)
'    Printer.Line (PicW / 2, 0)-(PicW / 2, PicH)
'  Else
'    OEM1.OEM1PicCALCOLO.Line (0, PicH / 2)-(PicW, PicH / 2)
'    OEM1.OEM1PicCALCOLO.Line (PicW / 2, 0)-(PicW / 2, PicH)
'  End If
'  '
'  Dim ANGOLO As Single
'  Dim NumeroRette As Integer
'  Dim ftmp1 As Double
'  '
'  Dim pX1 As Single
'  Dim pX2 As Single
'  Dim pY1 As Single
'  Dim pY2 As Single
'  '
'  ANGOLO = 30
'  NumeroRette = 30
'  '
'  stmp = " WHEEL "
'  If (SiStampa = "Y") Then
'    Printer.CurrentX = PicW / 2 - Printer.TextWidth(stmp) / 2
'    Printer.CurrentY = PicH - Printer.TextHeight(stmp)
'    Printer.Print stmp
'  Else
'    OEM1.OEM1PicCALCOLO.CurrentX = PicW / 2 - OEM1.OEM1PicCALCOLO.TextWidth(stmp) / 2
'    OEM1.OEM1PicCALCOLO.CurrentY = PicH - OEM1.OEM1PicCALCOLO.TextHeight(stmp)
'    OEM1.OEM1PicCALCOLO.Print stmp
'  End If
'  '
'  stmp = " F1 "
'  If (SiStampa = "Y") Then
'    Printer.CurrentX = 0
'    Printer.CurrentY = PicH - Printer.TextHeight(stmp)
'    Printer.Print stmp
'  Else
'    OEM1.OEM1PicCALCOLO.CurrentX = 0
'    OEM1.OEM1PicCALCOLO.CurrentY = PicH - OEM1.OEM1PicCALCOLO.TextHeight(stmp)
'    OEM1.OEM1PicCALCOLO.Print stmp
'  End If
'  '
'  stmp = " F2 "
'  If (SiStampa = "Y") Then
'    Printer.CurrentX = PicW - Printer.TextWidth(stmp)
'    Printer.CurrentY = PicH - Printer.TextHeight(stmp)
'    Printer.Print stmp
'  Else
'    OEM1.OEM1PicCALCOLO.CurrentX = PicW - OEM1.OEM1PicCALCOLO.TextWidth(stmp)
'    OEM1.OEM1PicCALCOLO.CurrentY = PicH - OEM1.OEM1PicCALCOLO.TextHeight(stmp)
'    OEM1.OEM1PicCALCOLO.Print stmp
'  End If
'  '
'  If (SiStampa = "Y") Then
'    'FRECCIA X
'    pX1 = PicW
'    pY1 = PicH / 2
'    ftmp1 = (ANGOLO / 180 * PG) / NumeroRette
'    For i = -NumeroRette To NumeroRette
'      pX2 = pX1 - 3 * Cos((ANGOLO / 180 * PG))
'      pY2 = pY1 + 3 * Sin(i * ftmp1)
'      Printer.Line (pX1, pY1)-(pX2, pY2)
'    Next i
'    'NOME DELL'ASSE X
'    stmp = " X "
'    Printer.CurrentX = PicW - Printer.TextWidth(stmp)
'    Printer.CurrentY = PicH / 2 + 3 '- Printer.TextHeight(stmp)
'    Printer.Print stmp
'    'FRECCIA Y
'    pX1 = PicW / 2
'    pY1 = 0
'    ftmp1 = (ANGOLO / 180 * PG) / NumeroRette
'    For i = -NumeroRette To NumeroRette
'      pX2 = pX1 + 3 * Sin(i * ftmp1)
'      pY2 = pY1 + 3 * Cos((ANGOLO / 180 * PG))
'      Printer.Line (pX1, pY1)-(pX2, pY2)
'    Next i
'    'NOME DELL'ASSE Y
'    stmp = " Y "
'    Printer.CurrentX = PicW / 2 + 3
'    Printer.CurrentY = 0
'    Printer.Print stmp
'  Else
'    'FRECCIA X
'    pX1 = PicW
'    pY1 = PicH / 2
'    ftmp1 = (ANGOLO / 180 * PG) / NumeroRette
'    For i = -NumeroRette To NumeroRette
'      pX2 = pX1 - 3 * Cos((ANGOLO / 180 * PG))
'      pY2 = pY1 + 3 * Sin(i * ftmp1)
'      OEM1.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2)
'    Next i
'    'NOME DELL'ASSE X
'    stmp = " X "
'    OEM1.OEM1PicCALCOLO.CurrentX = PicW - OEM1.OEM1PicCALCOLO.TextWidth(stmp)
'    OEM1.OEM1PicCALCOLO.CurrentY = PicH / 2 + 3 '- OEM1.OEM1PicCALCOLO.TextHeight(stmp)
'    OEM1.OEM1PicCALCOLO.Print stmp
'    'FRECCIA Y
'    pX1 = PicW / 2
'    pY1 = 0
'    ftmp1 = (ANGOLO / 180 * PG) / NumeroRette
'    For i = -NumeroRette To NumeroRette
'      pX2 = pX1 + 3 * Sin(i * ftmp1)
'      pY2 = pY1 + 3 * Cos((ANGOLO / 180 * PG))
'      OEM1.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2)
'    Next i
'    'NOME DELL'ASSE Y
'    stmp = " Y "
'    OEM1.OEM1PicCALCOLO.CurrentX = PicW / 2 + 3
'    OEM1.OEM1PicCALCOLO.CurrentY = 0
'    OEM1.OEM1PicCALCOLO.Print stmp
'  End If
'  '
'  'CENTRO DEL PROFILO
'  pX1 = (CENTRO_PROFILO - ZeroPezzoX) * ScalaX + PicW / 2
'  If (pX1 <= PicW) And (pX1 >= 0) Then
'    If (SiStampa = "Y") Then
'      Printer.Line (pX1, 0)-(pX1, PicH), QBColor(9)
'    Else
'      OEM1.OEM1PicCALCOLO.Line (pX1, 0)-(pX1, PicH), QBColor(9)
'    End If
'  End If
'  '
'  'ALTEZZA FISSATA DELLA MOLA
'  pY1 = -(ALTEZZA_MOLA - ZeroPezzoY) * ScalaY + PicH / 2
'  If (pY1 <= PicH) And (pY1 >= 0) Then
'    If (SiStampa = "Y") Then
'      Printer.Line (0, pY1)-(PicW, pY1), QBColor(9)
'    Else
'      OEM1.OEM1PicCALCOLO.Line (0, pY1)-(PicW, pY1), QBColor(9)
'    End If
'  End If
'  '
'  Dim k As Long
'  '
'  Dim XXpre As Double, YYpre As Double
'  Dim XXcor As Double, YYcor As Double
'  '
'  Dim xx As Double, sXX As String
'  Dim YY As Double, sYY As String
'  '
'  Dim XXc As Double, sXXc As String
'  Dim YYc As Double, sYYc As String
'  '
'  Dim Raggio As Double
'  Dim Comando As String
'  '
'  Dim CODICE_PEZZO As String
'  Dim MODALITA     As String
'  Dim TIPO_LAVORAZIONE As String
'  '
'  Dim DB As Database
'  Dim TB As Recordset
'  '
'  'LETTURA DEL TIPO DI LAVORAZIONE
'  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
'  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
'  '
'  'LEGGO LA MODALITA DI TRASFERIMENTO DATI
'  MODALITA = GetInfo("Configurazione", "MODALITA", PathFILEINI)
'  MODALITA = UCase$(Trim$(MODALITA))
'  'LETTURA DEL NOME PEZZO
'  If (MODALITA = "OFF-LINE") Then
'    CODICE_PEZZO = GetInfo(TIPO_LAVORAZIONE, "NomePezzoInserimento", Path_LAVORAZIONE_INI)
'  Else
'    CODICE_PEZZO = LEGGI_NomePezzo(TIPO_LAVORAZIONE)
'  End If
'  '
'  Dim dx As Double
'  Dim dy As Double
'  stmp = GetInfo("DIS0", "DF", Path_LAVORAZIONE_INI)
'  dx = val(GetInfo("DIS0", "DF", Path_LAVORAZIONE_INI))
'  dy = dx
'  '
'  'CREAZIONE DEL FILE CAD DAL CAMPO SOTTOPROGRAMMA
'  If Len(CODICE_PEZZO) > 0 Then
'    '
'    'APRO IL DATABASE
'    Set DB = OpenDatabase(PathDB, True, False)
'    Set TB = DB.OpenRecordset("ARCHIVIO_VITI_MULTIFILETTO")
'    '
'    'RICERCO NELLA TABELLA ARCHIVIO SE ESISTE UNA ARCHIVIAZIONE
'    TB.Index = "NOME_PEZZO"
'    TB.Seek "=", CODICE_PEZZO
'    '
'    If TB.NoMatch Then
'      '
'      'RECORD NON TROVATO: NON SI PUO LEGGERE NIENTE
'      Write_Dialog CODICE_PEZZO & " not found --> HD"
'      '
'      'CHIUDO IL DATABASE
'      TB.Close
'      DB.Close
'      '
'      Exit Sub
'      '
'    Else
'      '
'      'RECORD TROVATO: CREO I FILE DI APPOGGIO
'      Open g_chOemPATH & "\" & "CAD.TXT" For Output As #1
'        If Len(TB.Fields("SOTTOPROGRAMMA").Value) > 0 Then
'          Print #1, TB.Fields("SOTTOPROGRAMMA").Value;
'        Else
'          Print #1, "";
'        End If
'      Close #1
'      '
'    End If
'    '
'    'CHIUDO IL DATABASE
'    TB.Close
'    DB.Close
'    '
'    Write_Dialog CODICE_PEZZO & ": CAD SUBPROGRAM (read)"
'    '
'  Else
'    '
'    Write_Dialog CODICE_PEZZO & " NOT POSSIBLE: Operation aborted !!!"
'    '
'  End If
'  '
'  Dim XXmax   As Double
'  Dim YYmax   As Double
'  Dim XXmin   As Double
'  Dim YYmin   As Double
'  Dim RRmaxI  As Double
'  Dim RRminI  As Double
'  Dim RRmaxE  As Double
'  Dim RRminE  As Double
'  '
'  XXmax = -1E+308:  XXmin = 1E+308
'  YYmax = -1E+308:  YYmin = 1E+308
'  RRmaxI = -1E+308: RRminI = 1E+308
'  RRmaxE = -1E+308: RRminE = 1E+308
'  '
'  Open g_chOemPATH & "\" & "CAD.TXT" For Input As #1
'  'Open g_chOemPATH & "\" & "CAD.SPF" For Output As #2
'    'Print #2, ";PERCORSO CAD PER 840D"
'    'Print #2, ";CODICE PEZZO: " & CODICE_PEZZO
'    'Print #2, ";VERSIONE    : " & Date & " " & Time
'    '
'    'POSIZIONAMENTO INIZIALE
'    Do While Not EOF(1)
'      Line Input #1, stmp
'      stmp = UCase$(Trim$(stmp))
'        '
'        If (InStr(stmp, "G0") > 0) Then
'          Comando = "G0"
'          GoSub CORREZIONE_UTENSILE
'          k = InStr(stmp, "X")
'          If (k > 0) Then
'            Call ESTRAI_VALORE(k, stmp, sXX, XXcor)
'            Comando = Comando & " Z=" & frmt(-XXcor, 4)
'          Else
'            XXcor = XXpre
'          End If
'          k = InStr(stmp, "Y")
'          If (k > 0) Then
'            Call ESTRAI_VALORE(k, stmp, sYY, YYcor)
'            Comando = Comando & " X=" & frmt(-YYcor, 4)
'          Else
'            YYcor = YYpre
'          End If
'          pX1 = (XXcor - ZeroPezzoX) * ScalaX + PicW / 2
'          pY1 = -(YYcor - ZeroPezzoY) * ScalaY + PicH / 2
'          'Print #2, Comando
'          If (pY1 <= PicH) And (pX1 <= PicW) And (pX1 >= 0) And (pY1 >= 0) Then
'            If (SiStampa = "Y") Then
'              Printer.Line (pX1 - dx / 2, pY1)-(pX1 + dx / 2, pY1), QBColor(12)
'              Printer.Line (pX1, pY1 + dy / 2)-(pX1, pY1 - dy / 2), QBColor(12)
'              Printer.Circle (pX1, pY1), dx, QBColor(12)
'            Else
'              OEM1.OEM1PicCALCOLO.Line (pX1 - dx / 2, pY1)-(pX1 + dx / 2, pY1), QBColor(12)
'              OEM1.OEM1PicCALCOLO.Line (pX1, pY1 + dy / 2)-(pX1, pY1 - dy / 2), QBColor(12)
'              OEM1.OEM1PicCALCOLO.Circle (pX1, pY1), dx, QBColor(12)
'            End If
'          End If
'          XXpre = XXcor: YYpre = YYcor
'          GoSub AGGIORNA_MASSIMO_MINIMO
'          Exit Do
'          '
'      ElseIf (InStr(stmp, "G1") > 0) Then
'          Comando = "G1"
'          GoSub CORREZIONE_UTENSILE
'          k = InStr(stmp, "X")
'          If (k > 0) Then
'            Call ESTRAI_VALORE(k, stmp, sXX, XXcor)
'            Comando = Comando & " Z=" & frmt(-XXcor, 4)
'          Else
'            XXcor = XXpre
'          End If
'          k = InStr(stmp, "Y")
'          If (k > 0) Then
'            Call ESTRAI_VALORE(k, stmp, sYY, YYcor)
'            Comando = Comando & " X=" & frmt(-YYcor, 4)
'          Else
'            YYcor = YYpre
'          End If
'          'Print #2, Comando
'          pX1 = (XXcor - ZeroPezzoX) * ScalaX + PicW / 2
'          pY1 = -(YYcor - ZeroPezzoY) * ScalaY + PicH / 2
'          'Print #2, Comando
'          If (pY1 <= PicH) And (pX1 <= PicW) And (pX1 >= 0) And (pY1 >= 0) Then
'            If (SiStampa = "Y") Then
'              Printer.Line (pX1 - dx / 2, pY1)-(pX1 + dx / 2, pY1), QBColor(12)
'              Printer.Line (pX1, pY1 + dy / 2)-(pX1, pY1 - dy / 2), QBColor(12)
'              Printer.Circle (pX1, pY1), dx, QBColor(12)
'            Else
'              OEM1.OEM1PicCALCOLO.Line (pX1 - dx / 2, pY1)-(pX1 + dx / 2, pY1), QBColor(12)
'              OEM1.OEM1PicCALCOLO.Line (pX1, pY1 + dy / 2)-(pX1, pY1 - dy / 2), QBColor(12)
'              OEM1.OEM1PicCALCOLO.Circle (pX1, pY1), dx, QBColor(12)
'            End If
'          End If
'          XXpre = XXcor: YYpre = YYcor
'          GoSub AGGIORNA_MASSIMO_MINIMO
'          Exit Do
'          '
'      End If
'    Loop
'    '
'    'PERCORSO
'    Do While Not EOF(1)
'      Line Input #1, stmp
'      stmp = UCase$(Trim$(stmp))
'      '
'        If (InStr(stmp, "G0") > 0) Then
'          Comando = "G0"
'          GoSub CORREZIONE_UTENSILE
'          k = InStr(stmp, "X")
'          If (k > 0) Then
'            Call ESTRAI_VALORE(k, stmp, sXX, XXcor)
'            Comando = Comando & " Z=" & frmt(-XXcor, 4)
'          Else
'            XXcor = XXpre
'          End If
'          k = InStr(stmp, "Y")
'          If (k > 0) Then
'            Call ESTRAI_VALORE(k, stmp, sYY, YYcor)
'            Comando = Comando & " X=" & frmt(-YYcor, 4)
'          Else
'            YYcor = YYpre
'          End If
'          'Print #2, Comando
'          pX1 = (XXpre - ZeroPezzoX) * ScalaX + PicW / 2
'          pY1 = -(YYpre - ZeroPezzoY) * ScalaY + PicH / 2
'          pX2 = (XXcor - ZeroPezzoX) * ScalaX + PicW / 2
'          pY2 = -(YYcor - ZeroPezzoY) * ScalaY + PicH / 2
'          If (SiStampa = "Y") Then Printer.Line (pX1, pY1)-(pX2, pY2) Else OEM1.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2)
'          If (pY1 <= PicH) And (pX1 <= PicW) And (pX1 >= 0) And (pY1 >= 0) Then
'            If (SiStampa = "Y") Then
'              Printer.Line (pX1 - dx / 2, pY1)-(pX1 + dx / 2, pY1)
'              Printer.Line (pX1, pY1 + dy / 2)-(pX1, pY1 - dy / 2)
'            Else
'              OEM1.OEM1PicCALCOLO.Line (pX1 - dx / 2, pY1)-(pX1 + dx / 2, pY1)
'              OEM1.OEM1PicCALCOLO.Line (pX1, pY1 + dy / 2)-(pX1, pY1 - dy / 2)
'            End If
'          End If
'          If (pY2 <= PicH) And (pX2 <= PicW) And (pX2 >= 0) And (pY2 >= 0) Then
'            If (SiStampa = "Y") Then
'              Printer.Line (pX2 - dx / 2, pY2)-(pX2 + dx / 2, pY2)
'              Printer.Line (pX2, pY2 + dy / 2)-(pX2, pY2 - dy / 2)
'            Else
'              OEM1.OEM1PicCALCOLO.Line (pX2 - dx / 2, pY2)-(pX2 + dx / 2, pY2)
'              OEM1.OEM1PicCALCOLO.Line (pX2, pY2 + dy / 2)-(pX2, pY2 - dy / 2)
'            End If
'          End If
'          XXpre = XXcor: YYpre = YYcor
'          GoSub AGGIORNA_MASSIMO_MINIMO
'          '
'      ElseIf (InStr(stmp, "G1") > 0) Then
'          Comando = "G1"
'          GoSub CORREZIONE_UTENSILE
'          k = InStr(stmp, "X")
'          If (k > 0) Then
'            Call ESTRAI_VALORE(k, stmp, sXX, XXcor)
'            Comando = Comando & " Z=" & frmt(-XXcor, 4)
'          Else
'            XXcor = XXpre
'          End If
'          k = InStr(stmp, "Y")
'          If (k > 0) Then
'            Call ESTRAI_VALORE(k, stmp, sYY, YYcor)
'            Comando = Comando & " X=" & frmt(-YYcor, 4)
'          Else
'            YYcor = YYpre
'          End If
'          'Print #2, Comando
'          pX1 = (XXpre - ZeroPezzoX) * ScalaX + PicW / 2
'          pY1 = -(YYpre - ZeroPezzoY) * ScalaY + PicH / 2
'          pX2 = (XXcor - ZeroPezzoX) * ScalaX + PicW / 2
'          pY2 = -(YYcor - ZeroPezzoY) * ScalaY + PicH / 2
'          If (SiStampa = "Y") Then Printer.Line (pX1, pY1)-(pX2, pY2) Else OEM1.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2)
'          If (pY1 <= PicH) And (pX1 <= PicW) And (pX1 >= 0) And (pY1 >= 0) Then
'            If (SiStampa = "Y") Then
'              Printer.Line (pX1 - dx / 2, pY1)-(pX1 + dx / 2, pY1)
'              Printer.Line (pX1, pY1 + dy / 2)-(pX1, pY1 - dy / 2)
'            Else
'              OEM1.OEM1PicCALCOLO.Line (pX1 - dx / 2, pY1)-(pX1 + dx / 2, pY1)
'              OEM1.OEM1PicCALCOLO.Line (pX1, pY1 + dy / 2)-(pX1, pY1 - dy / 2)
'            End If
'          End If
'          If (pY2 <= PicH) And (pX2 <= PicW) And (pX2 >= 0) And (pY2 >= 0) Then
'            If (SiStampa = "Y") Then
'              Printer.Line (pX2 - dx / 2, pY2)-(pX2 + dx / 2, pY2)
'              Printer.Line (pX2, pY2 + dy / 2)-(pX2, pY2 - dy / 2)
'            Else
'              OEM1.OEM1PicCALCOLO.Line (pX2 - dx / 2, pY2)-(pX2 + dx / 2, pY2)
'              OEM1.OEM1PicCALCOLO.Line (pX2, pY2 + dy / 2)-(pX2, pY2 - dy / 2)
'            End If
'          End If
'          XXpre = XXcor: YYpre = YYcor
'          GoSub AGGIORNA_MASSIMO_MINIMO
'          '
'      ElseIf (InStr(stmp, "G3") > 0) Then
'          Comando = "G3"
'          'Comando = "G2"
'          GoSub CORREZIONE_UTENSILE
'          k = InStr(stmp, "X")
'          If (k > 0) Then Call ESTRAI_VALORE(k, stmp, sXX, XXcor)
'          k = InStr(stmp, "Y")
'          If (k > 0) Then Call ESTRAI_VALORE(k, stmp, sYY, YYcor)
'          k = InStr(stmp, "I")
'          If (k > 0) Then Call ESTRAI_VALORE(k, stmp, sXXc, XXc)
'          k = InStr(stmp, "J")
'          If (k > 0) Then Call ESTRAI_VALORE(k, stmp, sYYc, YYc)
'          Raggio = Sqr((XXcor - XXc) ^ 2 + (YYcor - YYc) ^ 2)
'          Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, XXpre, YYpre, XXcor, YYcor, Raggio, "DX", SiStampa, ZeroPezzoX, ZeroPezzoY)
'          'Comando = Comando & " Z=" & sXX & " X=" & sYY & " I=" & sXXc & " J=" & sYYc
'          Comando = Comando & " Z=" & frmt(-XXcor, 4) & " X=" & frmt(-YYcor, 4) & " CR=" & frmt(Raggio, 4)
'          'Print #2, Comando
'          XXpre = XXcor: YYpre = YYcor
'          GoSub AGGIORNA_MASSIMO_MINIMO
'          '
'          If Raggio > RRmaxI Then RRmaxI = Raggio
'          If Raggio < RRminI Then RRminI = Raggio
'          '
'      ElseIf (InStr(stmp, "G2") > 0) Then
'          Comando = "G2"
'          'Comando = "G3"
'          GoSub CORREZIONE_UTENSILE
'          k = InStr(stmp, "X")
'          If (k > 0) Then Call ESTRAI_VALORE(k, stmp, sXX, XXcor)
'          k = InStr(stmp, "Y")
'          If (k > 0) Then Call ESTRAI_VALORE(k, stmp, sYY, YYcor)
'          k = InStr(stmp, "I")
'          If (k > 0) Then Call ESTRAI_VALORE(k, stmp, sXXc, XXc)
'          k = InStr(stmp, "J")
'          If (k > 0) Then Call ESTRAI_VALORE(k, stmp, sYYc, YYc)
'          Raggio = Sqr((XXcor - XXc) ^ 2 + (YYcor - YYc) ^ 2)
'          Call RACCORDA(OEM1.OEM1PicCALCOLO, Scala, XXpre, YYpre, XXcor, YYcor, Raggio, "SX", SiStampa, ZeroPezzoX, ZeroPezzoY)
'          'Comando = Comando & " Z=" & sXX & " X=" & sYY & " I=" & sXXc & " J=" & sYYc
'          Comando = Comando & " Z=" & frmt(-XXcor, 4) & " X=" & frmt(-YYcor, 4) & " CR=" & frmt(Raggio, 4)
'          'Print #2, Comando
'          XXpre = XXcor: YYpre = YYcor
'          GoSub AGGIORNA_MASSIMO_MINIMO
'          '
'          If Raggio > RRmaxE Then RRmaxE = Raggio
'          If Raggio < RRminE Then RRminE = Raggio
'          '
'      End If
'    Loop
'    'Print #2, "RET"
'  'Close #2
'  Close #1
'  '
'    'ALTEZZA
'    stmp = " H= " & frmt(YYmax, 4) & " - " & frmt(YYmin, 4) & " = " & frmt(YYmax - YYmin, 4)
'    OEM1.OEM1PicCALCOLO.CurrentX = 0
'    OEM1.OEM1PicCALCOLO.CurrentY = OEM1.OEM1PicCALCOLO.TextHeight(stmp) * 0
'    OEM1.OEM1PicCALCOLO.Print stmp
'    If (YYmax - YYmin) >= ALTEZZA_MOLA Then
'      'AVVERTO DI IMPOSTARE UNA ALTEZZA PIU' GRANDE
'      stmp = " E R R O R: H >= " & OEM1.List1(0).List(4)
'      OEM1.OEM1PicCALCOLO.CurrentX = 0
'      OEM1.OEM1PicCALCOLO.CurrentY = OEM1.OEM1PicCALCOLO.TextHeight(stmp) * 1
'      OEM1.OEM1PicCALCOLO.Print stmp
'    End If
'    '
'    'LARGHEZZA
'    stmp = " W= " & frmt(XXmax, 4) & " - " & frmt(XXmin, 4) & " = " & frmt(XXmax - XXmin, 4)
'    OEM1.OEM1PicCALCOLO.CurrentX = 0
'    OEM1.OEM1PicCALCOLO.CurrentY = OEM1.OEM1PicCALCOLO.TextHeight(stmp) * 2
'    OEM1.OEM1PicCALCOLO.Print stmp
'    If (XXmax - XXmin) >= LARGHEZZA_MOLA Then
'      'AVVERTO DI IMPOSTARE UNA LARGHEZZA PIU' GRANDE
'      stmp = " E R R O R: W <= " & OEM1.List1(0).List(0)
'      OEM1.OEM1PicCALCOLO.CurrentX = 0
'      OEM1.OEM1PicCALCOLO.CurrentY = OEM1.OEM1PicCALCOLO.TextHeight(stmp) * 3
'      OEM1.OEM1PicCALCOLO.Print stmp
'    End If
'    '
'    'RAGGIO MASSIMO INTERNO
'    stmp = " Rmax INT= " & frmt(RRmaxI, 4)
'    OEM1.OEM1PicCALCOLO.CurrentX = 0
'    OEM1.OEM1PicCALCOLO.CurrentY = OEM1.OEM1PicCALCOLO.TextHeight(stmp) * 4
'    OEM1.OEM1PicCALCOLO.Print stmp
'    'RAGGIO MINIMO INTERNO
'    stmp = " Rmin INT= " & frmt(RRminI, 4)
'    OEM1.OEM1PicCALCOLO.CurrentX = 0
'    OEM1.OEM1PicCALCOLO.CurrentY = OEM1.OEM1PicCALCOLO.TextHeight(stmp) * 5
'    OEM1.OEM1PicCALCOLO.Print stmp
'    'RAGGIO MASSIMO ESTERNO
'    stmp = " Rmax EXT= " & frmt(RRmaxE, 4)
'    OEM1.OEM1PicCALCOLO.CurrentX = 0
'    OEM1.OEM1PicCALCOLO.CurrentY = OEM1.OEM1PicCALCOLO.TextHeight(stmp) * 6
'    OEM1.OEM1PicCALCOLO.Print stmp
'    'RAGGIO MINIMO ESTERNO
'    stmp = " Rmin EXT= " & frmt(RRminE, 4)
'    OEM1.OEM1PicCALCOLO.CurrentX = 0
'    OEM1.OEM1PicCALCOLO.CurrentY = OEM1.OEM1PicCALCOLO.TextHeight(stmp) * 7
'    OEM1.OEM1PicCALCOLO.Print stmp
'
'  '
'  'Segnalazione della fine delle operazioni
'  If (SiStampa = "Y") Then GoSub FineDocumento
'
'Exit Sub
'
'errSOTTOPROGRAMMA_CAD_VISUALIZZA:
'  Write_Dialog "SUB : SOTTOPROGRAMMA_CAD_VISUALIZZA " & Error(Err)
'  Resume Next
'
'CORREZIONE_UTENSILE:
'  If (InStr(stmp, "G41") > 0) Then Comando = Comando & " "
'  If (InStr(stmp, "G42") > 0) Then Comando = Comando & " "
'  If (InStr(stmp, "G40") > 0) Then Comando = Comando & " "
'Return
'
'AGGIORNA_MASSIMO_MINIMO:
'  If XXcor > XXmax Then XXmax = XXcor
'  If XXcor < XXmin Then XXmin = XXcor
'  If YYcor > YYmax Then YYmax = YYcor
'  If YYcor < YYmin Then YYmin = YYcor
'Return
'
'FineDocumento:
'  'Dimensione dei caratteri
'    For i = 1 To 2
'      Printer.FontName = TipoCarattere
'      Printer.FontSize = 14
'      Printer.FontBold = True
'    Next i
'    'Logo
'    Printer.PaintPicture OEM1.PicLogo, 0, PicH, 20, 20
'    'Nome del documento
'    stmp = Date & " " & Time
'    Printer.CurrentX = 0 + 25
'    Printer.CurrentY = PicH
'    Printer.Print stmp
'    'Scala X
'    If OEM1.lblINP(0).Visible = True Then
'      stmp = OEM1.lblINP(0).Caption & ": x" & OEM1.txtINP(0).Text
'      Printer.CurrentX = 0 + 25
'      Printer.CurrentY = PicH + 10
'      Printer.Print stmp
'    End If
'    'OX
'    If OEM1.lblOX.Visible = True Then
'      stmp = OEM1.OEM1lblOX.Caption & ": " & OEM1.OEM1txtOX.Text
'      Printer.CurrentX = 0 + 25
'      Printer.CurrentY = PicH + 20
'      Printer.Print stmp
'    End If
'    'Scala Y
'    If OEM1.lblINP(1).Visible = True Then
'      stmp = OEM1.lblINP(1).Caption & ": x" & OEM1.txtINP(1).Text
'      Printer.CurrentX = 0 + PicW / 2
'      Printer.CurrentY = PicH + 10
'      Printer.Print stmp
'    End If
'    'OY
'    If OEM1.OEM1lblOY.Visible = True Then
'      stmp = OEM1.OEM1lblOY.Caption & ": " & OEM1.OEM1txtOY.Text
'      Printer.CurrentX = 0 + PicW / 2
'      Printer.CurrentY = PicH + 20
'      Printer.Print stmp
'    End If
'    Printer.EndDoc
'    Write_Dialog ""
'
'Return

End Sub

Sub SOTTOPROGRAMMA_CAD_STAMPA()
'
Dim X    As Single
Dim i    As Integer
Dim k1   As Integer
Dim k2   As Integer
Dim k3   As Integer
Dim k4   As Integer
Dim j    As Integer
Dim rsp  As Integer
'
Dim MLeft As Single
Dim MTop  As Single
Dim Box0W As Single
Dim Box0H As Single
'
Dim X1 As Single
Dim Y1 As Single
Dim X2 As Single
Dim Y2 As Single
'
Dim Box1L As Single
Dim Box1T As Single
Dim Box1W As Single
Dim Box1H As Single
'
Dim iRG As Single
Dim iCl As Single
'
Dim riga  As String
Dim stmp  As String
'
On Error Resume Next
  '
  Printer.ScaleMode = 7
  For i = 1 To 2
    Printer.FontName = "Courier New"
    Printer.FontSize = 10
    Printer.FontBold = False
  Next i
  WRITE_DIALOG Printer.DeviceName & " PRINTING....."
  '
  MLeft = 2: MTop = 1
  Box0W = 17: Box0H = 27
  iRG = Printer.TextHeight("A")
  iCl = Printer.TextWidth("A")
  Box1L = 5: Box1T = 1
  Box1W = 12: Box1H = 12
  '
  GoSub STAMPA_INTESTAZIONE
  '
  Dim NrigheMax As Integer
  Dim iiRG As Integer
  Dim iiPG As Integer
  '
  NrigheMax = (Box0H - 3) / iRG
  iiPG = 1
  riga = ""
  i = 1
  k1 = 1
  k4 = 1
  Do
    'terminatore di riga
    k2 = InStr(k1, OEMX.OEM1txtHELP.Text, Chr(13))
    k3 = InStr(k1, OEMX.OEM1txtHELP.Text, Chr(10))
    'prendo il minimo
    If k3 >= k2 Then k4 = k2 Else k4 = k3
    If k4 > k1 Then
      riga = Mid$(OEMX.txtHELP.Text, k1, k4 - k1)
      riga = Trim$(riga)
      If k3 >= k2 Then k1 = k3 + 1 Else k1 = k2 + 1
      i = i + 1
      iiRG = (i - 1) Mod NrigheMax
      Printer.CurrentX = X1 + iCl
      Printer.CurrentY = Y1 + (iiRG + 3) * iRG
      riga = Right(String(6, " ") & Format$(i - 1, "#####0"), 6) & "] " & riga
      Printer.Print riga
      If i = iiPG * NrigheMax Then
        Printer.NewPage
        GoSub STAMPA_INTESTAZIONE
        iiPG = iiPG + 1
      End If
    Else
      Exit Do
    End If
  Loop
  Printer.EndDoc
  WRITE_DIALOG ""
  '
Exit Sub

STAMPA_INTESTAZIONE:
  'Squadratura della pagina
  X1 = MLeft: Y1 = MTop
  X2 = MLeft + Box0W: Y2 = MTop + Box0H
  'Printer.Line (X1, Y1)-(X2, Y2), QBColor(0), B
  'Intestazione della pagina
  Printer.CurrentX = X1 + iCl
  Printer.CurrentY = Y1 + iRG
  stmp = ""
  stmp = stmp & " Date: " & Format$(Now, "dd/mm/yy")
  stmp = stmp & " Time: " & Format$(Now, "hh:mm:ss")
  Printer.Print stmp
  'Linea di separazione
  X1 = MLeft: Y1 = MTop + 1
  X2 = MLeft + Box0W: Y2 = MTop + 1
  Printer.Line (X1, Y1)-(X2, Y2), QBColor(0)
  '
  Printer.CurrentX = X1 + iCl
  Printer.CurrentY = Y1 + 1 * iRG
  Printer.Print "CAD SUBPROGRAM"
  '
Return

End Sub

Sub SOTTOPROGRAMMA_CAD_STAMPA_CAMPO(ByVal CODICE_PEZZO As String)
'
Dim DB  As Database
Dim TB  As Recordset
'
Dim X    As Single
Dim i    As Integer
Dim k1   As Integer
Dim k2   As Integer
Dim k3   As Integer
Dim k4   As Integer
Dim j    As Integer
Dim rsp  As Integer
'
Dim MLeft As Single
Dim MTop  As Single
Dim Box0W As Single
Dim Box0H As Single
'
Dim X1 As Single
Dim Y1 As Single
Dim X2 As Single
Dim Y2 As Single
'
Dim Box1L As Single
Dim Box1T As Single
Dim Box1W As Single
Dim Box1H As Single
'
Dim iRG As Single
Dim iCl As Single
'
Dim riga  As String
Dim stmp  As String
Dim TIPO_LAVORAZIONE As String

'
On Error Resume Next
  '
  'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  
  'CREAZIONE DEL FILE CAD DAL CAMPO SOTTOPROGRAMMA
  If Len(CODICE_PEZZO) > 0 Then
    'APRO IL DATABASE
    Set DB = OpenDatabase(PathDB, True, False)
    'Set TB = DB.OpenRecordset("ARCHIVIO_VITI_ESTERNE")
    stmp = "ARCHIVIO_" & TIPO_LAVORAZIONE
    Set TB = DB.OpenRecordset(stmp)
    'RICERCO NELLA TABELLA ARCHIVIO SE ESISTE UNA ARCHIVIAZIONE
    TB.Index = "NOME_PEZZO"
    TB.Seek "=", CODICE_PEZZO
    If TB.NoMatch Then
      'RECORD NON TROVATO: NON SI PUO LEGGERE NIENTE
      WRITE_DIALOG CODICE_PEZZO & " not found --> HD"
      TB.Close
      DB.Close
      Exit Sub
    Else
      'RECORD TROVATO: CREO I FILE DI APPOGGIO
      Open g_chOemPATH & "\" & "CAD.TXT" For Output As #1
        If Len(TB.Fields("SOTTOPROGRAMMA").Value) > 0 Then
          Print #1, TB.Fields("SOTTOPROGRAMMA").Value;
        Else
          Print #1, "";
        End If
      Close #1
    End If
    TB.Close
    DB.Close
    WRITE_DIALOG CODICE_PEZZO & ": CAD SUBPROGRAM (read)"
  Else
    WRITE_DIALOG CODICE_PEZZO & " NOT POSSIBLE: Operation aborted !!!"
    Exit Sub
  End If
  '
  Printer.ScaleMode = 7
  For i = 1 To 2
    Printer.FontName = "Courier New"
    Printer.FontSize = 10
    Printer.FontBold = False
  Next i
  WRITE_DIALOG Printer.DeviceName & " PRINTING....."
  '
  MLeft = 2: MTop = 1
  Box0W = 17: Box0H = 27
  iRG = Printer.TextHeight("A")
  iCl = Printer.TextWidth("A")
  Box1L = 5: Box1T = 1
  Box1W = 12: Box1H = 12
  '
  GoSub STAMPA_INTESTAZIONE
  '
  Dim NrigheMax As Integer
  Dim iiRG As Integer
  Dim iiPG As Integer
  '
  Open g_chOemPATH & "\" & "CAD.TXT" For Input As #1
  NrigheMax = (Box0H - 3) / iRG
  iiPG = 1
  riga = ""
  i = 1
  While Not EOF(1)
    Line Input #1, riga
    riga = Trim$(riga)
    '
    i = i + 1
    iiRG = (i - 1) Mod NrigheMax
    '
    Printer.CurrentX = X1 + iCl
    Printer.CurrentY = Y1 + (iiRG + 3) * iRG
    '
    riga = Right(String(6, " ") & Format$(i - 1, "#####0"), 6) & "] " & riga
    Printer.Print riga
    '
    If i = iiPG * NrigheMax Then
      Printer.NewPage
      GoSub STAMPA_INTESTAZIONE
      iiPG = iiPG + 1
    End If
    '
  Wend
  Close #1
  '
  Printer.EndDoc
  WRITE_DIALOG ""
  '
Exit Sub

STAMPA_INTESTAZIONE:
  'Squadratura della pagina
  X1 = MLeft: Y1 = MTop
  X2 = MLeft + Box0W: Y2 = MTop + Box0H
  'Printer.Line (X1, Y1)-(X2, Y2), QBColor(0), B
  'Intestazione della pagina
  Printer.CurrentX = X1 + iCl
  Printer.CurrentY = Y1 + iRG
  stmp = ""
  stmp = stmp & " Date: " & Format$(Now, "dd/mm/yy")
  stmp = stmp & " Time: " & Format$(Now, "hh:mm:ss")
  Printer.Print stmp
  'Linea di separazione
  X1 = MLeft: Y1 = MTop + 1
  X2 = MLeft + Box0W: Y2 = MTop + 1
  Printer.Line (X1, Y1)-(X2, Y2), QBColor(0)
  '
  Printer.CurrentX = X1 + iCl
  Printer.CurrentY = Y1 + 1 * iRG
  Printer.Print "CAD SUBPROGRAM"
  '
Return

End Sub

Sub ESTRAI_VALORE(k1 As Long, riga As String, sVALORE As String, Valore As Double)
'
Dim ELEMENTI(12)  As String
Dim sCIFRA        As String * 1
Dim LEGGI_CIFRA   As String * 1
Dim j             As Integer
'
On Error GoTo errESTRAI_VALORE
  '
  ELEMENTI(0) = "0"
  ELEMENTI(1) = "1"
  ELEMENTI(2) = "2"
  ELEMENTI(3) = "3"
  ELEMENTI(4) = "4"
  ELEMENTI(5) = "5"
  ELEMENTI(6) = "6"
  ELEMENTI(7) = "7"
  ELEMENTI(8) = "8"
  ELEMENTI(9) = "9"
  ELEMENTI(10) = "."
  ELEMENTI(11) = "-"
  ELEMENTI(12) = "+"
  '
  sVALORE = "": Valore = 0: j = 0
  If (k1 > 0) Then
    Do
      sCIFRA = Mid(riga, k1 + 1, 1)
      If (sCIFRA <> "") Or (sCIFRA <> " ") Then
        For i = 0 To 12
          If (sCIFRA = ELEMENTI(i)) Then
            LEGGI_CIFRA = "Y"
            sVALORE = sVALORE & sCIFRA
            k1 = k1 + 1
            j = j + 1
            Exit For
          Else
            LEGGI_CIFRA = "N"
          End If
        Next i
      Else
        LEGGI_CIFRA = "N"
      End If
    Loop While (LEGGI_CIFRA = "Y")
    If (j > 0) Then Valore = val(sVALORE)
  End If
  '
Exit Sub

errESTRAI_VALORE:
  WRITE_DIALOG "Sub ESTRAI_VALORE: Error -> " & Err
  Exit Sub

End Sub

Sub CALCOLO_TEMPI_VITI_MULTIFILETTO(ByVal NumeroMandrino As String, ByVal SiStampa As String)

StopRegieEvents
MsgBox ("Sub: Not S380 ready")
ResumeRegieEvents

''
'Const MAXNumeroCicli = 6
''
'Dim i     As Integer
'Dim k     As Integer
'Dim kkk   As Integer
'Dim j     As Integer
'Dim stmp  As String
'Dim riga  As String
''
''Area di stampa
'Dim MLeft As Double
'Dim MTop  As Double
''
''Parametri per la stampa di linee
'Dim X1        As Double
'Dim Y1        As Double
'Dim X2        As Double
'Dim Y2        As Double
'Dim IRg       As Double
'Dim ICl       As Double
'Dim TextFont  As String
'Dim TextSize  As Double
'Dim Wnome1    As Double
'Dim Wnome2    As Double
'Dim Wvalore1  As Double
'Dim Wvalore2  As Double
'Dim Fattore   As Double
'Dim WLine     As Integer
''
'Dim DBB As Database
'Dim RSS As Recordset
''
'Dim MESSAGGI(9)             As String
'Dim Tret1a(MAXNumeroCicli)  As Double
'Dim Tempo1A                 As Double
'Dim Tret1r(MAXNumeroCicli)  As Double
'Dim Tempo1R                 As Double
'Dim Tret1rV(MAXNumeroCicli) As Double
'Dim Tempo1RV                As Double
'Dim T1D(MAXNumeroCicli)     As Double
'Dim TPSDIV                  As Double
'Dim TDIAM(MAXNumeroCicli)   As Double
'Dim TtotDiam                As Double
'Dim T(MAXNumeroCicli)       As Double
'Dim TotIncY(MAXNumeroCicli) As Double
'Dim TotIncA(MAXNumeroCicli) As Double
'Dim TotIncrementiY          As Double
'Dim TotIncrementiA          As Double
''
'Dim UsuraRag                As Double
'Dim UsuraDia                As Double
''
'Dim PassateProfilatura      As Integer
'Dim VelocitaProfilatura     As Double
''
'Dim MolaPezzo               As Double
'Dim LDIAM                   As Double
'Dim Fascia                  As Double
'Dim NunPrf(MAXNumeroCicli)  As Integer
'Dim DiaPr                   As Double
'Dim PassoElica              As Double
'Dim LARG                    As Double
'Dim TME                     As Double
'Dim TMU                     As Double
'Dim VRECAnd                 As Double
'Dim VRECRit                 As Double
'Dim NBPE                    As Integer
''
''dati macchina
'Dim XcentroRullo   As Double 'X centro rullo
'Dim DistMolaRullo  As Double 'Interasse mola-asse rullo MAC[0,numand]
'Dim DistPezzoRullo As Double 'Interasse pezzo-asse rullo MAC[0,0]
'Dim AccX           As Double 'Accelerazione assi X-Y (mm/s2)
'Dim AccA           As Double 'Accelerazione asse A  (gr/s2)
'Dim AccB           As Double 'Accelerazione asse B  (gr/s2)
'Dim VelA           As Double 'Velocit� max asse A (giri/min->gradi/sec)
'Dim VelB           As Double 'Velocit� max asse B (giri/min->gradi/sec)
'Dim VelX           As Double 'Velocit� max asse X   (mm/min-> mm/sec)
'Dim VelY           As Double 'Velocit� max asse Y   (mm/min-> mm/sec)
'Dim TempoFas       As Double 'Tempo fasatura          (sec)
'Dim TFM            As Double 'Tempo morto ogni mov.   (sec)
'Dim DEMEULE        As Double 'Diametro mola
''
''dati profilo
'Dim Mr         As Double      'MODULO NORMALE
'Dim ZDENT_TEO  As Double      'NUMERO DENTI TEORICI
'Dim HELPG      As Double      'ANGOLO ELICA (GRADI)
'Dim HELP       As Double      'ANGOLO ELICA (RADIANTI)
'Dim DI1        As Double      'DIAMETRO INTERNO
'Dim Hdent      As Double
'Dim INTERASSE  As Double
''
''dati profilo
'Dim sMR      As String       'MODULO NORMALE
'Dim sZDENT   As String       'NUMERO DENTI TEORICI
'Dim sHELPG   As String       'ANGOLO ELICA (GRADI)
'Dim sDI1     As String       'DIAMETRO INTERNO
'Dim sLARG    As String
'Dim sLDIAM   As String
'Dim sDEMEULE As String
''
''dati lavoro
'Dim XinizioLav As Double     'X inizio lavoro
'Dim XdivLat    As Double     'Tratto divisione laterale
'Dim Xentrata   As Double     'Tratto X entrata
'Dim Xuscita    As Double     'Tratto X uscita
'Dim Zona1      As Double     'Tratto n.1 elica
'Dim Zona2      As Double     'Tratto n.2 elica
'Dim Zona3      As Double     'Tratto n.3 elica
'Dim Zona4      As Double     'Tratto n.4 elica
''
''dati cicli
'Dim TipoCiclo(MAXNumeroCicli) As Integer  'Tipo ciclo
'Dim Rip(MAXNumeroCicli)       As Integer  'Numero ripetizioni
'Dim RipTEO(MAXNumeroCicli)    As Integer  'Numero ripetizioni
'Dim NPassDiam(MAXNumeroCicli) As Integer  'Numero di passate di diamantatura
'Dim IncPrf(MAXNumeroCicli)    As Double   'Incremento profilatura
'Dim Vdiam(MAXNumeroCicli)     As Double   'Velocit� profilatura  (mm/min)
'Dim NpasCic(MAXNumeroCicli)   As Integer  'N.passate
'Dim IncAnd(MAXNumeroCicli)    As Double   'Incremento andata
'Dim IncRit(MAXNumeroCicli)    As Double   'Incremento ritorno
'Dim Velassa(MAXNumeroCicli)   As Double   'Vel.assi and.
'Dim Velassr(MAXNumeroCicli)   As Double   'Vel.assi rit.
'Dim Denti2prf(MAXNumeroCicli) As Double   'Denti tra due prof.
'Dim R55(MAXNumeroCicli)       As Double   'RECUPERO RADIALE
'Dim ZDENT_DIV(MAXNumeroCicli) As Double   'NUMERO DENTI PER DIVISIONE
''
'Dim TT    As Double
'Dim TabX  As Double
'Dim TABx1 As Double
'Dim TABx2 As Double
'Dim TABx3 As Double
'Dim TEMPO As Double
''
'Dim PicH  As Double
'Dim PicW  As Double
'Dim Atmp  As String * 255
''
'Dim MODALITA              As String
'Dim TIPO_LAVORAZIONE      As String
'Dim NOME_TIPO             As String
''
'Dim TabellaMANDRINI As String
'Dim TabellaMACCHINA As String
'Dim TabellaPROFILO  As String
'Dim TabellaLAVORO   As String
''
'Dim TIPO_DIAMANTATORE   As Integer
'Dim sTIPO_DIAMANTATORE  As String
''
'Dim TH As Double
'Dim Tm As Double
'Dim Ts As Double
''
'On Error GoTo errCALCOLO_TEMPI_VITI_MULTIFILETTO
'  '
'  'LEGGO LA MODALITA DI TRASFERIMENTO DATI
'  MODALITA = GetInfo("Configurazione", "MODALITA", PathFILEINI)
'  MODALITA = UCase$(Trim$(MODALITA))
'  '
'  'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
'  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
'  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
'  '
'  OEM0.chkINPUT(0).Visible = False
'  OEM0.chkINPUT(1).Visible = False
'  OEM0.chkINPUT(2).Visible = False
'  '
'  OEM0.lblOX.Visible = False
'  OEM0.lblOY.Visible = False
'  OEM0.txtOX.Visible = False
'  OEM0.txtOY.Visible = False
'  '
'  OEM0.lblMANDRINI.Visible = True
'  OEM0.McCombo1.Visible = True
'  '
'  'Intestazione frame
'  i = LoadString(g_hLanguageLibHandle, 1013, Atmp, 255)
'  If i > 0 Then OEM0.frameCalcolo.Caption = Left$(Atmp, i)
'  i = LoadString(g_hLanguageLibHandle, 1014, Atmp, 255)
'  If i > 0 Then OEM0.lblMANDRINI.Caption = Left$(Atmp, i)
'  '
'  'LARGHEZZA
'  stmp = GetInfo("TEMPI_" & TIPO_LAVORAZIONE, "Larghezza", Path_LAVORAZIONE_INI)
'  If val(stmp) > 0 Then PicW = val(stmp) Else PicW = 190
'  OEM0.PicCALCOLO.ScaleWidth = PicW
'  'ALTEZZA
'  stmp = GetInfo("TEMPI_" & TIPO_LAVORAZIONE, "Altezza", Path_LAVORAZIONE_INI)
'  If val(stmp) > 0 Then PicH = val(stmp) Else PicH = 95
'  OEM0.PicCALCOLO.ScaleHeight = PicH
'  '
'  'IMPOSTAZIONE STAMPANTE E STAMPA DEI CICLI
'  TextFont = GetInfo("StampaParametri", "TextFont", PathFILEINI)
'  TextSize = val(GetInfo("StampaParametri", "TextSize", PathFILEINI))
'  '
'  If (SiStampa = "Y") Then
'    '
'    NOME_TIPO = RICERCA_NOME_TIPO(TIPO_LAVORAZIONE)
'    Write_Dialog OEM0.frameCalcolo.Caption & " --> " & Printer.DeviceName
'    '
'    MLeft = val(GetInfo("StampaParametri", "MLeft", PathFILEINI))
'    MTop = val(GetInfo("StampaParametri", "MTop", PathFILEINI))
'    Fattore = val(GetInfo("StampaParametri", "fattore", PathFILEINI))
'    Wnome1 = val(GetInfo("StampaParametri", "Wnome1", PathFILEINI))
'    Wnome2 = val(GetInfo("StampaParametri", "Wnome2", PathFILEINI))
'    Wvalore1 = val(GetInfo("StampaParametri", "Wvalore1", PathFILEINI))
'    Wvalore2 = val(GetInfo("StampaParametri", "Wvalore2", PathFILEINI))
'    'CM --> MM
'    MLeft = MLeft * 10
'    MTop = MTop * 10
'    Wnome1 = Wnome1 * 10
'    Wnome2 = Wnome2 * 10
'    Wvalore1 = Wvalore1 * 10
'    Wvalore2 = Wvalore2 * 10
'    WLine = Abs(val(GetInfo("StampaParametri", "Spessore", PathFILEINI)))
'    '--------
'    'Scala in cm per il foglio
'    Printer.ScaleMode = 6
'    'FontSize (1440/567) [cm] = 1 [inch]
'    For i = 1 To 2
'      Printer.FontName = TextFont
'      Printer.FontSize = TextSize
'      Printer.FontBold = False
'    Next i
'    'Dimensioni delle rige e colonne per ciascun carattere
'    IRg = Printer.TextHeight("A")
'    ICl = Printer.TextWidth("A")
'    'Posiziono il cursore sulla stampante
'    X1 = MLeft
'    Y1 = MTop
'    'Logo
'    Printer.PaintPicture OEM0.PicLogo.Picture, X1, Y1, 20, 20
'    GoSub SCRIVI_HEADER
'    For j = 0 To 5
'      'Posiziono il cursore sulla stampante
'      If j > 0 Then
'        Printer.CurrentX = MLeft + ICl + (j - 1) * Wvalore1 + Wnome1
'      Else
'        Printer.CurrentX = MLeft + ICl
'      End If
'      Printer.CurrentY = MTop
'      If OEM0.Label3(j).Visible = True Then Printer.Print OEM0.Label3(j).Caption
'    Next j
'    For i = 0 To OEM0.List1(0).ListCount - 1
'      'Posiziono il cursore sulla stampante
'      X1 = MLeft + ICl
'      Y1 = MTop + (i + 1) * IRg
'      Printer.CurrentX = X1
'      Printer.CurrentY = Y1
'      Printer.Print OEM0.List1(0).List(i)
'      'Traccio la linea per evidenziare il valore
'      X1 = MLeft + ICl + Printer.TextWidth(OEM0.List1(0).List(i))
'      Y1 = MTop + (i + 1 + Fattore) * IRg
'      X2 = MLeft + ICl + Wnome1
'      Y2 = Y1
'      Printer.Line (X1, Y1)-(X2, Y2)
'      For j = 1 To MAXNumeroCicli
'        '
'        'LEGGO IL CAMPO RELATIVO AL VALORE DEL PARAMETRO
'        stmp = OEM0.List1(j).List(i)
'        '
'        'Posiziono il cursore sulla stampante
'        X1 = MLeft + ICl + (j - 1) * Wvalore1 + Wnome1
'        Y1 = MTop + (i + 1) * IRg
'        Printer.CurrentX = X1
'        Printer.CurrentY = Y1
'        Printer.Print stmp
'        '
'        If (j < Nlist1) Then
'          'Traccio la linea per evidenziare il valore
'          X1 = MLeft + ICl + Wnome1 + (j - 1) * Wvalore1 + Printer.TextWidth(stmp)
'          Y1 = MTop + (i + 1 + Fattore) * IRg
'          X2 = MLeft + ICl + Wnome1 + j * Wvalore1
'          Y2 = Y1
'          Printer.Line (X1, Y1)-(X2, Y2)
'        End If
'        '
'      Next j
'    Next i
'    '
'  Else
'    '
'    OEM0.PicCALCOLO.FontName = TextFont
'    OEM0.PicCALCOLO.FontSize = TextSize
'    OEM0.PicCALCOLO.FontBold = False
'    '
'    'Dimensioni delle rige e colonne per ciascun carattere
'    IRg = OEM0.PicCALCOLO.TextHeight("A")
'    ICl = OEM0.PicCALCOLO.TextWidth("A")
'    '
'    OEM0.PicCALCOLO.Cls
'    '
'  End If
'  '
'  'messaggi
'  i = LoadString(g_hLanguageLibHandle, 1010, Atmp, 255)
'  If i > 0 Then MESSAGGI(0) = Left$(Atmp, i)
'  i = LoadString(g_hLanguageLibHandle, 1011, Atmp, 255)
'  If i > 0 Then MESSAGGI(1) = Left$(Atmp, i)
'  i = LoadString(g_hLanguageLibHandle, 1012, Atmp, 255)
'  If i > 0 Then MESSAGGI(2) = Left$(Atmp, i)
'  i = LoadString(g_hLanguageLibHandle, 1015, Atmp, 255)
'  If i > 0 Then sLARG = Left$(Atmp, i)
'  i = LoadString(g_hLanguageLibHandle, 1016, Atmp, 255)
'  If i > 0 Then sLDIAM = Left$(Atmp, i)
'  i = LoadString(g_hLanguageLibHandle, 1017, Atmp, 255)
'  If i > 0 Then MESSAGGI(3) = Left$(Atmp, i)
'  i = LoadString(g_hLanguageLibHandle, 1018, Atmp, 255)
'  If i > 0 Then MESSAGGI(4) = Left$(Atmp, i)
'  i = LoadString(g_hLanguageLibHandle, 1020, Atmp, 255)
'  If i > 0 Then MESSAGGI(5) = Left$(Atmp, i)
'  i = LoadString(g_hLanguageLibHandle, 1021, Atmp, 255)
'  If i > 0 Then MESSAGGI(6) = Left$(Atmp, i)
'  '
'  'Dati macchina
'  'Accelerazione assi X-Y (mm/s2)
'  stmp = GetInfo("TEMPI_" & TIPO_LAVORAZIONE, "AccX", Path_LAVORAZIONE_INI)
'  AccX = val(stmp)
'  'Accelerazione asse A   (gr/s2)
'  stmp = GetInfo("TEMPI_" & TIPO_LAVORAZIONE, "AccA", Path_LAVORAZIONE_INI)
'  AccA = val(stmp)
'  'Accelerazione asse B   (gr/s2)
'  stmp = GetInfo("TEMPI_" & TIPO_LAVORAZIONE, "AccB", Path_LAVORAZIONE_INI)
'  AccB = val(stmp)
'  'Velocit� max asse A (giri/min->gradi/sec)
'  stmp = GetInfo("TEMPI_" & TIPO_LAVORAZIONE, "VelA", Path_LAVORAZIONE_INI)
'  VelA = val(stmp)
'  VelA = VelA * 6
'  'Velocit� max asse B (giri/min->gradi/sec)
'  stmp = GetInfo("TEMPI_" & TIPO_LAVORAZIONE, "VelB", Path_LAVORAZIONE_INI)
'  VelB = val(stmp)
'  VelB = VelB * 6
'  'Velocit� max asse X (mm/min-> mm/sec)
'  stmp = GetInfo("TEMPI_" & TIPO_LAVORAZIONE, "VelX", Path_LAVORAZIONE_INI)
'  VelX = val(stmp)
'  VelX = VelX / 60
'  'Velocit� max asse Y (mm/min-> mm/sec)
'  stmp = GetInfo("TEMPI_" & TIPO_LAVORAZIONE, "VelY", Path_LAVORAZIONE_INI)
'  VelY = val(stmp)
'  VelY = VelY / 60
'  'Tempo fasatura        (sec)
'  stmp = GetInfo("TEMPI_" & TIPO_LAVORAZIONE, "TempoFas", Path_LAVORAZIONE_INI)
'  TempoFas = val(stmp)
'  'Tempo morto ogni mov. (sec)
'  stmp = GetInfo("TEMPI_" & TIPO_LAVORAZIONE, "TFM", Path_LAVORAZIONE_INI)
'  TFM = val(stmp)
'  '
'  'Diametro mola
'  DEMEULE = val(GetInfo("TEMPI_" & TIPO_LAVORAZIONE, "DiametroEsternoMola", Path_LAVORAZIONE_INI))
'  i = LoadString(g_hLanguageLibHandle, 1200, Atmp, 255)
'  If i > 0 Then sDEMEULE = Left$(Atmp, i)
'  '
'  'APRO IL DATABASE
'  Set DBB = OpenDatabase(PathDB, True, False)
'  '
'  'INTERASSI MACCHINA
'  TabellaMANDRINI = GetInfo("TEMPI_" & TIPO_LAVORAZIONE, "TabellaMANDRINI", Path_LAVORAZIONE_INI)
'  If Len(TabellaMANDRINI) > 0 Then
'    Set RSS = DBB.OpenRecordset(TabellaMANDRINI)
'    RSS.Index = "INDICE"
'    '
'    RSS.Seek "=", 1
'    DistPezzoRullo = val(RSS.Fields("ACTUAL_1").Value)  'Interasse pezzo-asse rullo MAC[0,0]
'    '
'    i = val(NumeroMandrino) + 1
'    '
'    RSS.Seek "=", i
'    DistMolaRullo = val(RSS.Fields("ACTUAL_1").Value)   'Interasse mola-asse rullo MAC[0,numand]
'    '
'    RSS.Close
'  End If
'  '
'  'DISTANZA CENTRO MOLA - CENTRO RULLO
'  TabellaMACCHINA = GetInfo("TEMPI_" & TIPO_LAVORAZIONE, "TabellaMACCHINA", Path_LAVORAZIONE_INI)
'  If Len(TabellaMACCHINA) > 0 Then
'    Set RSS = DBB.OpenRecordset(TabellaMACCHINA)
'    RSS.Index = "INDICE"
'    '
'    RSS.Seek "=", 1
'    XcentroRullo = val(RSS.Fields("ACTUAL_1").Value) 'X centro rullo
'    '
'    RSS.Seek "=", 6
'    TIPO_DIAMANTATORE = val(RSS.Fields("ACTUAL_1").Value)
'    sTIPO_DIAMANTATORE = RSS.Fields("NOME_" & Lingua).Value
'    '
'    RSS.Close
'  End If
'  '
'  'PROFILO PEZZO
'  TabellaPROFILO = GetInfo("TEMPI_" & TIPO_LAVORAZIONE, "TabellaPROFILO", Path_LAVORAZIONE_INI)
'  If Len(TabellaPROFILO) > 0 Then
'    Set RSS = DBB.OpenRecordset(TabellaPROFILO)
'    RSS.Index = "INDICE"
'    '
'    RSS.Seek "=", 1
'    ZDENT_TEO = val(RSS.Fields("ACTUAL_1").Value) 'NUMERO DENTI
'    sZDENT = RSS.Fields("NOME_" & Lingua).Value
'    '
'    RSS.Seek "=", 4
'    HELPG = val(RSS.Fields("ACTUAL_1").Value)   'ANGOLO ELICA
'    sHELPG = RSS.Fields("NOME_" & Lingua).Value
'    '
'    RSS.Seek "=", 5
'    DI1 = val(RSS.Fields("ACTUAL_1").Value)   'DIAMETRO DI RIFERIMENTO
'    sDI1 = RSS.Fields("NOME_" & Lingua).Value   'DIAMETRO DI RIFERIMENTO
'    '
'    RSS.Seek "=", 6
'    PassoElica = val(RSS.Fields("ACTUAL_1").Value)   'PASSO ASSIALE
'    '
'    Mr = Abs(PassoElica) * Sin(Abs(HELPG) / 180 * PG) / ZDENT_TEO / PG 'MODULO NORMALE
'    sMR = "Mn"   'MODULO NORMALE
'    '
'    RSS.Close
'    '
'    Set RSS = DBB.OpenRecordset("OEM1_MOLA40")
'    RSS.Index = "INDICE"
'    '
'    RSS.Seek "=", 6
'    Hdent = val(RSS.Fields("ACTUAL_1").Value)
'    '
'    If (Hdent <= 0) Then Hdent = 10
'    '
'    RSS.Close
'    '
'  End If
'  '
'  'Dati lavoro
'  TabellaLAVORO = GetInfo("TEMPI_" & TIPO_LAVORAZIONE, "TabellaLAVORO", Path_LAVORAZIONE_INI)
'  If Len(TabellaLAVORO) > 0 Then
'    Set RSS = DBB.OpenRecordset(TabellaLAVORO)
'    RSS.Index = "INDICE"
'    '
'    RSS.Seek "=", 1: XinizioLav = val(RSS.Fields("ACTUAL_1").Value) 'X inizio lavoro
'    RSS.Seek "=", 4: XdivLat = val(RSS.Fields("ACTUAL_1").Value)    'Tratto divisione laterale
'    RSS.Seek "=", 5: Xentrata = val(RSS.Fields("ACTUAL_1").Value)   'Tratto X entrata
'    RSS.Seek "=", 6: Xuscita = val(RSS.Fields("ACTUAL_1").Value)    'Tratto X uscita
'    '
'    'ELICA
'    RSS.Seek "=", 15: Zona1 = val(RSS.Fields("ACTUAL_2").Value) 'Tratto n.1 elica
'    RSS.Seek "=", 18: Zona2 = val(RSS.Fields("ACTUAL_2").Value) 'Tratto n.2 elica
'    RSS.Seek "=", 21: Zona3 = val(RSS.Fields("ACTUAL_2").Value) 'Tratto n.3 elica
'    RSS.Seek "=", 24: Zona4 = val(RSS.Fields("ACTUAL_2").Value) 'Tratto n.4 elica
'    '
'    RSS.Close
'  End If
'  DBB.Close
'  '
'  'Dati cicli
'  Dim RTMP      As Double
'  '
'  For i = 1 To MAXNumeroCicli
'    TipoCiclo(i) = val(OEM0.List1(i).List(0))     'Tipo ciclo
'    '
'    Rip(i) = val(OEM0.List1(i).List(1))        'Numero ripetizioni
'    RipTEO(i) = val(OEM0.List1(i).List(1))     'Numero ripetizioni
'    '
'    NPassDiam(i) = val(OEM0.List1(i).List(2))  'Numero di passate di diamantatura
'    IncPrf(i) = val(OEM0.List1(i).List(3))     'Incremento profilatura
'    Vdiam(i) = val(OEM0.List1(i).List(4))      'Velocit� profilatura  (mm/min)
'    '
'    NpasCic(i) = val(OEM0.List1(i).List(6))    'N.passate
'    IncAnd(i) = val(OEM0.List1(i).List(7))     'Incremento andata
'    Velassa(i) = val(OEM0.List1(i).List(8))    'Vel.assi and.
'    IncRit(i) = val(OEM0.List1(i).List(9))     'Incremento ritorno
'    Velassr(i) = val(OEM0.List1(i).List(10))   'Vel.assi rit.
'    '
'    Denti2prf(i) = val(OEM0.List1(i).List(13)) 'Denti tra due prof.
'    ZDENT_DIV(i) = val(OEM0.List1(i).List(15))
'    R55(i) = val(OEM0.List1(i).List(16))       'Recupero radiale
'    '
'    If ZDENT_DIV(i) = 0 Then ZDENT_DIV(i) = 1
'    RTMP = ZDENT_TEO / ZDENT_DIV(i) - Int(ZDENT_TEO / ZDENT_DIV(i))
'    If (RTMP > 0) Then
'      ZDENT_DIV(i) = Int(ZDENT_TEO / ZDENT_DIV(i)) + 1
'    Else
'      ZDENT_DIV(i) = Int(ZDENT_TEO / ZDENT_DIV(i))
'    End If
'    '
'    If TipoCiclo(i) = 13 Then IncRit(i) = 0
'    '
'    'RICALCOLO RIPETIZIONI
'    If (R55(i) > 0) And (NpasCic(i) > 0) And ((IncAnd(i) > 0) Or (IncRit(i) > 0)) Then
'      RTMP = R55(i) / (NpasCic(i) * (IncAnd(i) + IncRit(i))) - Int(R55(i) / (NpasCic(i) * (IncAnd(i) + IncRit(i))))
'      If (RTMP > 0) Then
'        RTMP = Int(R55(i) / (NpasCic(i) * (IncAnd(i) + IncRit(i)))) + 1
'      Else
'        RTMP = Int(R55(i) / (NpasCic(i) * (IncAnd(i) + IncRit(i))))
'      End If
'      Rip(i) = Rip(i) + RTMP
'      R55(i) = RTMP * (NpasCic(i) * (IncAnd(i) + IncRit(i)))
'    End If
'    '
'  Next i
'  '
'  'Calcoli preliminari
'  HELP = FnGR(HELPG): If HELP = 0 Then HELP = 0.00001
'  MolaPezzo = DistMolaRullo + DistPezzoRullo            'Interasse pezzo-mola
'  LDIAM = Abs(XinizioLav - XcentroRullo)                'Distanza zona profil. - inizio lavoro
'  Fascia = Xentrata + Zona1 + Zona2 + Zona3 + Zona4 + Xuscita  'Lunghezza fascia rett.
'  INTERASSE = (DEMEULE + DI1) / 2
'  '
'  For i = 1 To MAXNumeroCicli
'    If (TipoCiclo(i) <> 0) Then
'      If Denti2prf(i) = 0 Then
'        NunPrf(i) = 1
'      Else
'        NunPrf(i) = Int(ZDENT_DIV(i) / Denti2prf(i) + 1)
'      End If
'      NunPrf(i) = NunPrf(i) * Rip(i)
'    Else
'      NunPrf(i) = 0
'    End If
'  Next i
'  '
'  'Lunghezza lungo elica
'  DiaPr = Mr * ZDENT_TEO / Cos(HELP)            'Diametro primitivo
'  LARG = Fascia / PassoElica * Sqr(PassoElica ^ 2 + (PG * DiaPr) ^ 2)
'  LARG = Abs(LARG)
'  XdivLat = XdivLat / PassoElica * Sqr(PassoElica ^ 2 + (PG * DiaPr) ^ 2)
'  XdivLat = Abs(XdivLat)
'  If Xentrata = 0 Then TME = 0.2 Else TME = 0   'Sosta entrata
'  If Xuscita = 0 Then TMU = 0.2 Else TMU = 0    'Sosta uscita
'  '
'  'Calcolo tempo rettifica
'  '
'  Dim TPSRECUL   As Double
'  Dim TPSROTT    As Double
'  Dim TPSPOSD    As Double
'  '
'  For i = 1 To MAXNumeroCicli
'    '
'    If (NpasCic(i) > 0) And (TipoCiclo(i) <> 0) Then
'      '
'      VRECAnd = Velassa(i)
'      VRECRit = Velassr(i)
'      NBPE = NpasCic(i)
'      PassateProfilatura = NPassDiam(i)
'      VelocitaProfilatura = Vdiam(i) / 60
'      GoSub TempoCiclo
'      Tret1a(i) = Tempo1A
'      Tret1r(i) = Tempo1R
'      Tret1rV(i) = Tempo1RV
'      T1D(i) = TPSDIV
'      '
'      If (TIPO_DIAMANTATORE = 2) Then
'        If (PassateProfilatura > 0) Then
'          TtotDiam = (TPSPOSD + TPSROTT + TPSRECUL) * 2
'        Else
'          TtotDiam = 0
'        End If
'        TDIAM(i) = Int(TtotDiam + PassateProfilatura * Vdiam(i))
'      Else
'        TDIAM(i) = Int(TtotDiam)
'      End If
'      '
'    End If
'  Next i
'  '
'  For i = 1 To MAXNumeroCicli
'    Select Case TipoCiclo(i)
'      Case 0
'        T(i) = 0
'      Case 12
'        T(i) = (T1D(i) + NpasCic(i) * (Tret1a(i) + Tret1r(i))) * ZDENT_DIV(i)
'      Case 13
'        T(i) = (T1D(i) + Tret1a(i) + Tret1rV(i)) * ZDENT_DIV(i) * NpasCic(i)
'      Case Else  '10
'        T(i) = (T1D(i) + Tret1a(i) + Tret1r(i)) * ZDENT_DIV(i) * NpasCic(i)
'    End Select
'    T(i) = TDIAM(i) * NunPrf(i) + T(i) * Rip(i)
'  Next i
'  '
'  'Calcolo Incrementi totali Y
'  For i = 1 To MAXNumeroCicli
'    Select Case TipoCiclo(i)
'      Case 10, 12
'        TotIncY(i) = RipTEO(i) * NpasCic(i) * (IncAnd(i) + IncRit(i))
'      Case 13
'        TotIncY(i) = RipTEO(i) * NpasCic(i) * IncAnd(i)
'    End Select
'  Next i
'  '
'  'Calcolo consumo mola
'  UsuraRag = 0
'  For i = 1 To MAXNumeroCicli
'    If (TipoCiclo(i) <> 0) Then
'      If (Denti2prf(i) = 0) Then
'        UsuraRag = UsuraRag - (Rip(i) * NPassDiam(i) * IncPrf(i))
'      Else
'        UsuraRag = UsuraRag - (Rip(i) * NPassDiam(i) * IncPrf(i) * Int(ZDENT_DIV(i) / Denti2prf(i) + 1))
'      End If
'    End If
'  Next i
'  UsuraDia = UsuraRag * 2
'  '
'  'Output risultati
'  If (SiStampa = "Y") Then
'    GoSub STAMPA_DATI
'    Write_Dialog OEM0.frameCalcolo.Caption & " --> " & Printer.DeviceName & " OK!!"
'    Printer.EndDoc
'  Else
'    GoSub VISUALIZZA_DATI
'  End If
'
'Exit Sub
'
'errCALCOLO_TEMPI_VITI_MULTIFILETTO:
'  Write_Dialog "SUB CALCOLO_TEMPI_VITI_MULTIFILETTO: ERROR -> " & Err
'  Err = 0
'  'Exit Sub
'  Resume Next
'
'TempoCiclo:
'Dim TaccE      As Double
'Dim LunAccAnd  As Double
'Dim TaccF      As Double
'Dim LunAccRit  As Double
'Dim TaccX      As Double
'Dim LaccX      As Double
'Dim TaccY      As Double
'Dim LaccY      As Double
'Dim TaccA      As Double
'Dim LaccA      As Double
'Dim TaccB      As Double
'Dim LaccB      As Double
'Dim Xdivisione As Double
'Dim TdivY      As Double
'Dim a          As Double
'Dim A1         As Double
'Dim LRECUL     As Double
'Dim lprof      As Double
'Dim TPSDIAM    As Double
'  '
'  VRECAnd = VRECAnd / 60: VRECRit = VRECRit / 60
'  TaccE = VRECAnd / AccX              'Tempo   per arrivare a VRECAnd in s
'  LunAccAnd = AccX * (TaccE ^ 2 / 2)  'Lunghezza ---- ------- - ----- in mm
'  TaccF = VRECRit / AccX              'Tempo   per arrivare a VRECRit in s
'  LunAccRit = AccX * (TaccF ^ 2 / 2)  'Lunghezza ---- ------- - ----- in mm
'  TaccX = VelX / AccX                 'Tempo   per arrivare a VelX in s
'  LaccX = AccX * (TaccX ^ 2 / 2)      'Lunghezza ---- ------- - ----- in mm
'  If LaccX > XdivLat Then
'    TaccX = Sqr(2 * XdivLat / AccX)
'    LaccX = XdivLat
'  End If
'  TaccY = VelY / AccX                 'Tempo   per arrivare a VelY in s
'  LaccY = AccX * (TaccY ^ 2 / 2)      'Lunghezza ---- ------- - ----- in mm
'  TaccA = VelA / AccA                 'Tempo   per arrivare a VelA  in s
'  LaccA = AccA * (TaccA ^ 2 / 2)      'Lunghezza ---- ------- - ----- in degres
'  TaccB = VelB / AccB                 'Tempo   per arrivare a VelB  in s
'  LaccB = AccB * (TaccB ^ 2 / 2)      'Lunghezza ---- ------- - ----- in degr�s
'  '
'  ' divisione in Y ?
'  If XdivLat = 0 Then
'    Xdivisione = 0
'    TdivY = (2 * TaccY + (Hdent - LaccY + 2) / VelY) * 2
'  Else
'    TdivY = 0
'    Xdivisione = XdivLat - LaccX
'  End If
'  'Tempo1A: Tempo totale andata per 1 dente
'  Tempo1A = TaccX + (Xdivisione / VelX) + TaccE + (LARG - LunAccAnd) / VRECAnd + 2 * TFM + TME
'  'Tempo1R: Tempo totale 1 dente lavoro ritorno
'  Tempo1R = TaccX + (Xdivisione / VelX) + TaccF + (LARG - LunAccRit) / VRECRit + 2 * TFM + TMU
'  'Tempo1RV: Tempo totale 1 dente ritorno rapido
'  Tempo1RV = TaccX + (Fascia + XdivLat - LaccX) / VelX + 2 * TFM
'  a = 360 / ZDENT_TEO: A1 = a - LaccA * 2
'  'TPSDIV : Tempo divisione per 1 dente
'  If A1 > 0 Then TPSDIV = A1 / VelA + TaccA * 2 + 4 * TFM + TdivY
'  If A1 < 0.1 Then TPSDIV = Sqr(((a / 2) / AccA) * 2) * 2 + 4 * TFM + TdivY
'  If TipoCiclo(i) = 13 Then
'    TPSDIV = TdivY
'  End If
'  '
'  LRECUL = MolaPezzo - INTERASSE
'  '
'  'Tempo per muoversi da posizione Y lavoro a quella di diamantatura
'  TPSRECUL = (LRECUL - LaccY * 2) / VelY + TaccY * 2 + 2 * TFM
'  'TPSROTT : tempo per rotazione asse B
'  TPSROTT = (90 - FnRG(HELP) - LaccB * 2) / VelB + TaccB * 2 + 2 * TFM
'  'TPSPOSD : tempo per andare e tornare posizione X diamantatura
'  TPSPOSD = (LDIAM - LaccX * 2) / VelX + 2 * TaccX + 2 * TFM
'  '
'  'TPSDIAM : tempo per profilare
'  'IL CALCOLO DOVREBBE TENERE CONTO DELL'EFFETTIVA LUNGHEZZA DEL DISEGNO CAD
'  lprof = Hdent * 2 + PG * Mr + 2
'  If lprof < 30 + Hdent * 2 Then lprof = 30 + Hdent * 2
'  If VelocitaProfilatura > 0 Then TPSDIAM = lprof / VelocitaProfilatura + 8 Else TPSDIAM = 0
'  If PassateProfilatura > 0 Then
'    TtotDiam = Int((TPSPOSD + TPSROTT + TPSRECUL) * 2 + TPSDIAM * PassateProfilatura)
'  Else
'    TtotDiam = 0
'  End If
'  '
'Return
'
'VISUALIZZA_DATI:
'  '
'  OEM0.PicCALCOLO.Cls
'  '
'  OEM0.PicCALCOLO.FontName = TextFont
'  OEM0.PicCALCOLO.FontSize = TextSize
'  OEM0.PicCALCOLO.FontBold = True
'  '
'  IRg = OEM0.PicCALCOLO.TextHeight("X")
'  TabX = 0
'  '*********
'  i = 0
'  stmp = " " & sMR
'  If OEM0.PicCALCOLO.TextWidth(stmp) > TabX Then TabX = OEM0.PicCALCOLO.TextWidth(stmp)
'  OEM0.PicCALCOLO.CurrentX = 0
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  OEM0.PicCALCOLO.Print stmp
'  '*********
'  i = i + 1
'  stmp = " " & sZDENT
'  If OEM0.PicCALCOLO.TextWidth(stmp) > TabX Then TabX = OEM0.PicCALCOLO.TextWidth(stmp)
'  OEM0.PicCALCOLO.CurrentX = 0
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  OEM0.PicCALCOLO.Print stmp
'  '*********
'  i = i + 1
'  stmp = " " & sHELPG
'  If OEM0.PicCALCOLO.TextWidth(stmp) > TabX Then TabX = OEM0.PicCALCOLO.TextWidth(stmp)
'  OEM0.PicCALCOLO.CurrentX = 0
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  OEM0.PicCALCOLO.Print stmp
'  '*********
'  i = i + 1
'  stmp = " " & sDI1
'  If OEM0.PicCALCOLO.TextWidth(stmp) > TabX Then TabX = OEM0.PicCALCOLO.TextWidth(stmp)
'  OEM0.PicCALCOLO.CurrentX = 0
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  OEM0.PicCALCOLO.Print stmp
'  '*********
'  i = i + 1
'  stmp = " " & sLDIAM
'  If OEM0.PicCALCOLO.TextWidth(stmp) > TabX Then TabX = OEM0.PicCALCOLO.TextWidth(stmp)
'  OEM0.PicCALCOLO.CurrentX = 0
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  OEM0.PicCALCOLO.Print stmp
'  '*********
'  i = i + 1
'  stmp = " " & sLARG
'  If OEM0.PicCALCOLO.TextWidth(stmp) > TabX Then TabX = OEM0.PicCALCOLO.TextWidth(stmp)
'  OEM0.PicCALCOLO.CurrentX = 0
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  OEM0.PicCALCOLO.Print stmp
'  '*********
'  i = i + 1
'  stmp = " " & sTIPO_DIAMANTATORE
'  If OEM0.PicCALCOLO.TextWidth(stmp) > TabX Then TabX = OEM0.PicCALCOLO.TextWidth(stmp)
'  OEM0.PicCALCOLO.CurrentX = 0
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  OEM0.PicCALCOLO.Print stmp
'  '*********
'  OEM0.lblINPUT.Caption = sDEMEULE
'  '*********
'  i = 0
'  stmp = ": " & frmt$(Mr, 4)
'  OEM0.PicCALCOLO.CurrentX = TabX
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  OEM0.PicCALCOLO.Print stmp
'  '*********
'  i = i + 1
'  stmp = ": " & Format$(ZDENT_TEO, "###0")
'  OEM0.PicCALCOLO.CurrentX = TabX
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  OEM0.PicCALCOLO.Print stmp
'  '*********
'  i = i + 1
'  stmp = ": " & frmt$(HELPG, 4)
'  OEM0.PicCALCOLO.CurrentX = TabX
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  OEM0.PicCALCOLO.Print stmp
'  '*********
'  i = i + 1
'  stmp = ": " & frmt$(DI1, 4)
'  OEM0.PicCALCOLO.CurrentX = TabX
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  OEM0.PicCALCOLO.Print stmp
'  '*********
'  i = i + 1
'  stmp = ": " & frmt$(LDIAM, 4)
'  OEM0.PicCALCOLO.CurrentX = TabX
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  OEM0.PicCALCOLO.Print stmp
'  '*********
'  i = i + 1
'  stmp = ": " & frmt$(LARG, 4)
'  OEM0.PicCALCOLO.CurrentX = TabX
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  OEM0.PicCALCOLO.Print stmp
'  '*********
'  i = i + 1
'  stmp = ": " & Format$(TIPO_DIAMANTATORE, "0")
'  OEM0.PicCALCOLO.CurrentX = TabX
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  OEM0.PicCALCOLO.Print stmp
'  '*********
'  OEM0.txtINPUT.Text = frmt$(DEMEULE, 4)
'  '
'  'TEMPI DI DIVISIONE
'  j = i
'  i = j + 2
'  OEM0.PicCALCOLO.CurrentX = 0
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  stmp = MESSAGGI(5)
'  OEM0.PicCALCOLO.Print stmp
'  '
'  OEM0.PicCALCOLO.CurrentX = PicW / 4
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  stmp = MESSAGGI(6)
'  OEM0.PicCALCOLO.Print stmp
'  OEM0.PicCALCOLO.Line (0, (i + 1) * IRg)-(PicW / 2, (i + 1) * IRg)
'  '
'  TabX = 0
'  For k = 1 To MAXNumeroCicli
'    If (TipoCiclo(k) <> 0) Then
'      i = i + 1
'      stmp = " " & Trim$(OEM0.Label3(k).Caption)
'      If OEM0.PicCALCOLO.TextWidth(stmp) > TabX Then TabX = OEM0.PicCALCOLO.TextWidth(stmp)
'      OEM0.PicCALCOLO.CurrentX = 0
'      OEM0.PicCALCOLO.CurrentY = i * IRg
'      OEM0.PicCALCOLO.Print stmp
'    End If
'  Next k
'  '
'  i = i + 1
'  OEM0.PicCALCOLO.CurrentX = 0
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  stmp = " " & MESSAGGI(2)
'  If OEM0.PicCALCOLO.TextWidth(stmp) > TabX Then TabX = OEM0.PicCALCOLO.TextWidth(stmp)
'  OEM0.PicCALCOLO.Print stmp
'  '
'  i = j + 2
'  For k = 1 To MAXNumeroCicli
'    If (TipoCiclo(k) <> 0) Then
'      i = i + 1
'      Select Case TipoCiclo(k)
'        Case 12
'          TEMPO = T1D(k) * ZDENT_DIV(k)
'        Case 13
'          TEMPO = T1D(k) * ZDENT_DIV(k) * NpasCic(k)
'        Case Else  '10
'          TEMPO = T1D(k) * ZDENT_DIV(k) * NpasCic(k)
'      End Select
'      stmp = ": " & Format$(Int(TEMPO / 60), "#0") & "'"
'      If OEM0.PicCALCOLO.TextWidth(stmp) > TABx1 Then TABx1 = OEM0.PicCALCOLO.TextWidth(stmp)
'      OEM0.PicCALCOLO.CurrentX = TabX
'      OEM0.PicCALCOLO.CurrentY = i * IRg
'      OEM0.PicCALCOLO.Print stmp
'    End If
'  Next k
'  '
'  i = j + 2
'  For k = 1 To MAXNumeroCicli
'    If (TipoCiclo(k) <> 0) Then
'      i = i + 1
'      Select Case TipoCiclo(k)
'        Case 12
'          TEMPO = T1D(k) * ZDENT_DIV(k)
'        Case 13
'          TEMPO = T1D(k) * ZDENT_DIV(k) * NpasCic(k)
'        Case Else  '10
'          TEMPO = T1D(k) * ZDENT_DIV(k) * NpasCic(k)
'      End Select
'      stmp = " " & Format(Int(TEMPO - Int(TEMPO / 60) * 60), "###0") & Chr$(34)
'      If OEM0.PicCALCOLO.TextWidth(stmp) > TABx2 Then TABx2 = OEM0.PicCALCOLO.TextWidth(stmp)
'      OEM0.PicCALCOLO.CurrentX = TabX + TABx1
'      OEM0.PicCALCOLO.CurrentY = i * IRg
'      OEM0.PicCALCOLO.Print stmp
'      'DIVISIONE SINGOLA
'      stmp = " " & frmt(T1D(k), 2) & Chr$(34)
'      OEM0.PicCALCOLO.CurrentX = PicW / 4
'      OEM0.PicCALCOLO.CurrentY = i * IRg
'      OEM0.PicCALCOLO.Print stmp
'    End If
'  Next k
'  i = i + 1
'  '
'  TEMPO = 0
'  For k = 1 To MAXNumeroCicli
'    If (TipoCiclo(k) <> 0) Then
'      Select Case TipoCiclo(k)
'        Case 12
'          TEMPO = TEMPO + T1D(k) * ZDENT_DIV(k)
'        Case 13
'          TEMPO = TEMPO + T1D(k) * ZDENT_DIV(k) * NpasCic(k)
'        Case Else  '10
'          TEMPO = TEMPO + T1D(k) * ZDENT_DIV(k) * NpasCic(k)
'      End Select
'    End If
'  Next k
'  stmp = ": " & Format$(Int(TEMPO / 60), "#0") & "'"
'  If OEM0.PicCALCOLO.TextWidth(stmp) > TABx1 Then TABx1 = OEM0.PicCALCOLO.TextWidth(stmp)
'  OEM0.PicCALCOLO.CurrentX = TabX
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  OEM0.PicCALCOLO.Print stmp
'  '
'  TEMPO = 0
'  For k = 1 To MAXNumeroCicli
'    If (TipoCiclo(k) <> 0) Then
'      Select Case TipoCiclo(k)
'        Case 12
'          TEMPO = TEMPO + T1D(k) * ZDENT_DIV(k)
'        Case 13
'          TEMPO = TEMPO + T1D(k) * ZDENT_DIV(k) * NpasCic(k)
'        Case Else  '10
'          TEMPO = TEMPO + T1D(k) * ZDENT_DIV(k) * NpasCic(k)
'      End Select
'    End If
'  Next k
'  stmp = " " & Format(Int(TEMPO - Int(TEMPO / 60) * 60), "###0") & Chr$(34)
'  If OEM0.PicCALCOLO.TextWidth(stmp) > TABx2 Then TABx2 = OEM0.PicCALCOLO.TextWidth(stmp)
'  OEM0.PicCALCOLO.CurrentX = TabX + TABx1
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  OEM0.PicCALCOLO.Print stmp
'  OEM0.PicCALCOLO.Line (0, i * IRg)-(PicW / 2, i * IRg)
'  '
'  'CALCOLO TEMPO TOTALE DI RETTIFICA
'  i = 0
'  OEM0.PicCALCOLO.CurrentX = PicW / 2
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  stmp = " " & MESSAGGI(0)
'  OEM0.PicCALCOLO.Print stmp
'  '
'  i = 1
'  OEM0.PicCALCOLO.Line (PicW / 2, i * IRg)-(PicW, i * IRg)
'  OEM0.PicCALCOLO.CurrentX = PicW / 2
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  '
'  TabX = 0
'  For i = 1 To MAXNumeroCicli
'    If (TipoCiclo(i) <> 0) Then
'      OEM0.PicCALCOLO.CurrentX = PicW / 2
'      OEM0.PicCALCOLO.CurrentY = i * IRg
'      stmp = " " & Trim$(OEM0.Label3(i).Caption)
'      If OEM0.PicCALCOLO.TextWidth(stmp) > TabX Then TabX = OEM0.PicCALCOLO.TextWidth(stmp)
'      OEM0.PicCALCOLO.Print stmp
'    End If
'  Next i
'  '
'  OEM0.PicCALCOLO.CurrentX = PicW / 2
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  If (Lingua = "CH") Then
'    stmp = " " & MESSAGGI(3)
'  Else
'    stmp = " " & Left$(MESSAGGI(3), 7)
'  End If
'  If OEM0.PicCALCOLO.TextWidth(stmp) > TabX Then TabX = OEM0.PicCALCOLO.TextWidth(stmp)
'  OEM0.PicCALCOLO.Print stmp
'  '
'  i = i + 1
'  OEM0.PicCALCOLO.CurrentX = PicW / 2
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  stmp = " " & MESSAGGI(2)
'  If OEM0.PicCALCOLO.TextWidth(stmp) > TabX Then TabX = OEM0.PicCALCOLO.TextWidth(stmp)
'  OEM0.PicCALCOLO.Print stmp
'  '
'  TABx1 = 0
'  TT = 0
'  For i = 1 To MAXNumeroCicli
'    If (TipoCiclo(i) <> 0) Then
'      OEM0.PicCALCOLO.CurrentX = PicW / 2 + TabX
'      OEM0.PicCALCOLO.CurrentY = i * IRg
'      TH = Int(T(i) / 60 / 60)
'      stmp = " " & Format$(TH, "#0") & "h"
'      If OEM0.PicCALCOLO.TextWidth(stmp) > TABx1 Then TABx1 = OEM0.PicCALCOLO.TextWidth(stmp)
'      '
'      If (TH > 0) Then OEM0.PicCALCOLO.Print stmp
'      '
'      TT = TT + T(i)
'    End If
'  Next i
'  TT = TT + TempoFas
'  '
'  OEM0.PicCALCOLO.CurrentX = PicW / 2 + TabX + TABx1
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  stmp = " " & Format$(Int(TempoFas / 60), "#0") & "'"
'  If OEM0.PicCALCOLO.TextWidth(stmp) > TABx1 Then TABx1 = OEM0.PicCALCOLO.TextWidth(stmp)
'  OEM0.PicCALCOLO.Print stmp
'  '
'  TT = 0
'  TABx2 = 0
'  For i = 1 To MAXNumeroCicli
'    If (TipoCiclo(i) <> 0) Then
'      OEM0.PicCALCOLO.CurrentX = PicW / 2 + TabX + TABx1
'      OEM0.PicCALCOLO.CurrentY = i * IRg
'      TH = Int(T(i) / 60 / 60)
'      Tm = Int(T(i) / 60 - 60 * TH)
'      stmp = " " & Format(Tm, "###0") & "'"
'      If OEM0.PicCALCOLO.TextWidth(stmp) > TABx2 Then TABx2 = OEM0.PicCALCOLO.TextWidth(stmp)
'      OEM0.PicCALCOLO.Print stmp
'      TT = TT + T(i)
'    End If
'  Next i
'  TT = TT + TempoFas
'  '
'  TT = 0
'  TABx3 = 0
'  For i = 1 To MAXNumeroCicli
'    If (TipoCiclo(i) <> 0) Then
'      OEM0.PicCALCOLO.CurrentX = PicW / 2 + TabX + TABx1 + TABx2
'      OEM0.PicCALCOLO.CurrentY = i * IRg
'      TH = Int(T(i) / 60 / 60)
'      Tm = Int(T(i) / 60 - 60 * TH)
'      Ts = T(i) - 60 * 60 * TH - 60 * Tm
'      stmp = " " & Format(Ts, "###0") & Chr$(34)
'      If OEM0.PicCALCOLO.TextWidth(stmp) > TABx3 Then TABx3 = OEM0.PicCALCOLO.TextWidth(stmp)
'      OEM0.PicCALCOLO.Print stmp
'      TT = TT + T(i)
'    End If
'  Next i
'  TT = TT + TempoFas
'  '
'  OEM0.PicCALCOLO.CurrentX = PicW / 2 + TabX + TABx1 + TABx2
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  stmp = " " & Format(Int(TempoFas - Int(TempoFas / 60) * 60), "###0") & Chr$(34)
'  If OEM0.PicCALCOLO.TextWidth(stmp) > TABx3 Then TABx3 = OEM0.PicCALCOLO.TextWidth(stmp)
'  OEM0.PicCALCOLO.Print stmp
'  '
'  i = i + 1
'  OEM0.PicCALCOLO.Line (PicW / 2, i * IRg)-(PicW, i * IRg)
'  OEM0.PicCALCOLO.CurrentX = PicW / 2 + TabX
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  TH = Int(TT / 60 / 60)
'  Tm = Int(TT / 60 - 60 * TH)
'  Ts = TT - 60 * 60 * TH - 60 * Tm
'  stmp = ""
'  If (TH > 0) Then
'    stmp = stmp & " " & Format$(TH, "###0") & "h"
'  Else
'    stmp = stmp & "    "
'  End If
'  stmp = stmp & " " & Format$(Tm, "###0") & "'"
'  stmp = stmp & " " & Format$(Ts, "###0") & Chr$(34)
'  OEM0.PicCALCOLO.Print stmp
'  '
'  'CALCOLO ASPORTAZIONI
'  Dim DELTA_X As Double
'  '
'  i = 0: DELTA_X = 2 * ICl
'  OEM0.PicCALCOLO.CurrentX = 3 * PicW / 4 + DELTA_X
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  stmp = MESSAGGI(1)
'  OEM0.PicCALCOLO.Print stmp
'  '
'  TabX = 0
'  For i = 1 To MAXNumeroCicli
'    If (TipoCiclo(i) <> 0) Then
'      OEM0.PicCALCOLO.CurrentX = 3 * PicW / 4 + DELTA_X
'      OEM0.PicCALCOLO.CurrentY = i * IRg
'      stmp = " X=" & frmt(TotIncY(i), 4)
'      If OEM0.PicCALCOLO.TextWidth(stmp) > TabX Then TabX = OEM0.PicCALCOLO.TextWidth(stmp)
'      OEM0.PicCALCOLO.Print stmp
'    End If
'  Next i
'  '
'  TABx1 = 0
'  For i = 1 To MAXNumeroCicli
'    If (TipoCiclo(i) <> 0) And (Rip(i) - RipTEO(i) <> 0) Then
'      OEM0.PicCALCOLO.CurrentX = 3 * PicW / 4 + DELTA_X + TabX
'      OEM0.PicCALCOLO.CurrentY = i * IRg
'      'stmp = " Rip/RD=" & frmt(Rip(i) - RipTEO(i), 4) & "/" & frmt(R55(i), 4)
'      stmp = " (" & frmt(Rip(i) - RipTEO(i), 4) & "/" & frmt(R55(i), 4) & ")"
'      If OEM0.PicCALCOLO.TextWidth(stmp) > TABx1 Then TABx1 = OEM0.PicCALCOLO.TextWidth(stmp)
'      OEM0.PicCALCOLO.Print stmp
'    End If
'  Next i
'  '
'  i = i + 1
'  '
'  TotIncY(0) = 0
'  For kkk = 1 To MAXNumeroCicli
'    TotIncY(0) = TotIncY(0) + TotIncY(kkk)
'  Next kkk
'  '
'  stmp = " X=" & frmt(TotIncY(0), 4)
'  OEM0.PicCALCOLO.CurrentX = 3 * PicW / 4 + DELTA_X
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  OEM0.PicCALCOLO.Print stmp
'  '
'  OEM0.PicCALCOLO.Line (PicW / 2, 0 * IRg)-(PicW / 2, (i + 2) * IRg)
'  OEM0.PicCALCOLO.Line (3 * PicW / 4 + DELTA_X / 2, 0 * IRg)-(3 * PicW / 4 + DELTA_X / 2, (i + 2) * IRg)
'  '
'  i = i + 2
'  stmp = " " & MESSAGGI(4) & ": " & frmt(UsuraDia, 4) & " "
'  OEM0.PicCALCOLO.CurrentX = 3 * PicW / 4 - OEM0.PicCALCOLO.TextWidth(stmp) / 2
'  OEM0.PicCALCOLO.CurrentY = i * IRg
'  OEM0.PicCALCOLO.Print stmp
'  TabX = OEM0.PicCALCOLO.TextWidth(stmp)
'  OEM0.PicCALCOLO.Line (PicW / 2, i * IRg)-(PicW, (i + 1) * IRg), , B
'  '
'Return
'
'SCRIVI_HEADER:
'  'Posiziono il cursore sulla stampante
'  X1 = MLeft
'  Y1 = MTop
'  'INTESTAZIONE DELLA PAGINA: riga 1
'  stmp = NOME_TIPO
'  Printer.CurrentX = X1 + ICl + 20
'  Printer.CurrentY = Y1 + 0
'  Printer.Print stmp
'  'INTESTAZIONE DELLA PAGINA: riga 1
'  If (MODALITA = "OFF-LINE") Then stmp = "OFF-LINE" Else stmp = "CNC " & LETTERA_LAVORAZIONE
'  Printer.CurrentX = PicW - Printer.TextWidth(stmp)
'  Printer.CurrentY = Y1 + 0
'  Printer.Print stmp
'  'INTESTAZIONE DELLA PAGINA: riga 2
'  riga = GetInfo("Configurazione", "AZIENDA", PathFILEINI)
'  stmp = riga & " "
'  If (MODALITA = "OFF-LINE") Then
'    riga = GetInfo(TIPO_LAVORAZIONE, "NomePezzoInserimento", Path_LAVORAZIONE_INI)
'  Else
'    riga = LEGGI_NomePezzo(TIPO_LAVORAZIONE)
'  End If
'  stmp = stmp & riga
'  stmp = stmp & " " & Format$(Now, "dd/mm/yyyy")
'  stmp = stmp & " " & Format$(Now, "hh:mm:ss")
'  Printer.CurrentX = X1 + ICl + 20
'  Printer.CurrentY = Y1 + 10
'  Printer.Print stmp
'  'Ritorno linea
'  MTop = Printer.CurrentY + 2 * IRg
'  'Separatore
'  Printer.DrawWidth = WLine
'  Printer.Line (MLeft, MTop)-(PicW, MTop)
'  Printer.DrawWidth = 1
'  'Ritorno linea
'  MTop = MTop + IRg
'Return
'
'STAMPA_DATI:
'  '
'  Const DELTA_Y = 140
'  '
'  'INTESTAZIONE
'  stmp = OEM0.frameCalcolo.Caption
'  Printer.CurrentX = MLeft
'  Printer.CurrentY = -4 * IRg + DELTA_Y
'  Printer.Print stmp
'  'Separatore
'  Printer.DrawWidth = WLine
'  Printer.Line (MLeft, -3 * IRg + DELTA_Y)-(PicW, -3 * IRg + DELTA_Y)
'  Printer.DrawWidth = 1
'  'MANDRINO
'  stmp = " " & OEM0.lblMANDRINI.Caption & " " & NumeroMandrino & " "
'  Printer.CurrentX = MLeft
'  Printer.CurrentY = -2 * IRg + DELTA_Y
'  Printer.Print stmp
'  Printer.Line (MLeft, -2 * IRg + DELTA_Y)-(MLeft + Printer.TextWidth(stmp), -1 * IRg + DELTA_Y), , B
'  '
'  TabX = 0
'  '*********
'  i = 0
'  stmp = " " & sMR
'  If Printer.TextWidth(stmp) > TabX Then TabX = Printer.TextWidth(stmp)
'  Printer.CurrentX = MLeft
'  Printer.CurrentY = i * IRg + DELTA_Y
'  Printer.Print stmp
'  '*********
'  i = i + 1
'  stmp = " " & sZDENT
'  If Printer.TextWidth(stmp) > TabX Then TabX = Printer.TextWidth(stmp)
'  Printer.CurrentX = MLeft
'  Printer.CurrentY = i * IRg + DELTA_Y
'  Printer.Print stmp
'  '*********
'  i = i + 1
'  stmp = " " & sHELPG
'  If Printer.TextWidth(stmp) > TabX Then TabX = Printer.TextWidth(stmp)
'  Printer.CurrentX = MLeft
'  Printer.CurrentY = i * IRg + DELTA_Y
'  Printer.Print stmp
'  '*********
'  i = i + 1
'  stmp = " " & sDI1
'  If Printer.TextWidth(stmp) > TabX Then TabX = Printer.TextWidth(stmp)
'  Printer.CurrentX = MLeft
'  Printer.CurrentY = i * IRg + DELTA_Y
'  Printer.Print stmp
'  '*********
'  i = i + 1
'  stmp = " " & sLDIAM
'  If Printer.TextWidth(stmp) > TabX Then TabX = Printer.TextWidth(stmp)
'  Printer.CurrentX = MLeft
'  Printer.CurrentY = i * IRg + DELTA_Y
'  Printer.Print stmp
'  '*********
'  i = i + 1
'  stmp = " " & sLARG
'  If Printer.TextWidth(stmp) > TabX Then TabX = Printer.TextWidth(stmp)
'  Printer.CurrentX = MLeft
'  Printer.CurrentY = i * IRg + DELTA_Y
'  Printer.Print stmp
'  '*********
'  i = i + 1
'  stmp = " " & sTIPO_DIAMANTATORE
'  If Printer.TextWidth(stmp) > TabX Then TabX = Printer.TextWidth(stmp)
'  Printer.CurrentX = MLeft
'  Printer.CurrentY = i * IRg + DELTA_Y
'  Printer.Print stmp
'  '*********
'  i = i + 1
'  stmp = " " & sDEMEULE
'  If Printer.TextWidth(stmp) > TabX Then TabX = Printer.TextWidth(stmp)
'  Printer.CurrentX = MLeft
'  Printer.CurrentY = i * IRg + DELTA_Y
'  Printer.Print stmp
'  '*********
'  i = 0
'  stmp = ": " & frmt$(Mr, 4)
'  Printer.CurrentX = MLeft + TabX
'  Printer.CurrentY = i * IRg + DELTA_Y
'  Printer.Print stmp
'  '*********
'  i = i + 1
'  stmp = ": " & Format$(ZDENT_TEO, "###0")
'  Printer.CurrentX = MLeft + TabX
'  Printer.CurrentY = i * IRg + DELTA_Y
'  Printer.Print stmp
'  '*********
'  i = i + 1
'  stmp = ": " & frmt$(HELPG, 4)
'  Printer.CurrentX = MLeft + TabX
'  Printer.CurrentY = i * IRg + DELTA_Y
'  Printer.Print stmp
'  '*********
'  i = i + 1
'  stmp = ": " & frmt$(DI1, 4)
'  Printer.CurrentX = MLeft + TabX
'  Printer.CurrentY = i * IRg + DELTA_Y
'  Printer.Print stmp
'  '*********
'  i = i + 1
'  stmp = ": " & frmt$(LDIAM, 4)
'  Printer.CurrentX = MLeft + TabX
'  Printer.CurrentY = i * IRg + DELTA_Y
'  Printer.Print stmp
'  '*********
'  i = i + 1
'  stmp = ": " & frmt$(LARG, 4)
'  Printer.CurrentX = MLeft + TabX
'  Printer.CurrentY = i * IRg + DELTA_Y
'  Printer.Print stmp
'  '*********
'  i = i + 1
'  stmp = ": " & Format(TIPO_DIAMANTATORE, "0")
'  Printer.CurrentX = MLeft + TabX
'  Printer.CurrentY = i * IRg + DELTA_Y
'  Printer.Print stmp
'  '*********
'  i = i + 1
'  stmp = ": " & frmt$(DEMEULE, 4)
'  Printer.CurrentX = MLeft + TabX
'  Printer.CurrentY = i * IRg + DELTA_Y
'  Printer.Print stmp
'  '
'  'TEMPI DI DIVISIONE
'  j = i
'  i = j + 2
'  Printer.CurrentX = MLeft + 0
'  Printer.CurrentY = i * IRg + DELTA_Y
'  stmp = MESSAGGI(5)
'  Printer.Print stmp
'  Printer.CurrentX = MLeft + PicW / 4
'  Printer.CurrentY = i * IRg + DELTA_Y
'  stmp = MESSAGGI(6)
'  Printer.Print stmp
'  Printer.DrawWidth = WLine
'  Printer.Line (MLeft + 0, (i + 1) * IRg + DELTA_Y)-(MLeft + PicW / 2, (i + 1) * IRg + DELTA_Y)
'  Printer.DrawWidth = 1
'  TabX = 0
'  For k = 1 To MAXNumeroCicli
'    If (TipoCiclo(k) <> 0) Then
'      i = i + 1
'      stmp = " " & Trim$(OEM0.Label3(k).Caption)
'      If Printer.TextWidth(stmp) > TabX Then TabX = Printer.TextWidth(stmp)
'      Printer.CurrentX = MLeft + 0
'      Printer.CurrentY = i * IRg + DELTA_Y
'      Printer.Print stmp
'    End If
'  Next k
'  i = i + 1
'  Printer.CurrentX = MLeft + 0
'  Printer.CurrentY = i * IRg + DELTA_Y
'  stmp = " " & MESSAGGI(2)
'  If Printer.TextWidth(stmp) > TabX Then TabX = Printer.TextWidth(stmp)
'  Printer.Print stmp
'  i = j + 2
'  For k = 1 To MAXNumeroCicli
'    If (TipoCiclo(k) <> 0) Then
'      i = i + 1
'      Select Case TipoCiclo(k)
'        Case 12
'          TEMPO = T1D(k) * ZDENT_DIV(k)
'        Case 13
'          TEMPO = T1D(k) * ZDENT_DIV(k) * NpasCic(k)
'        Case Else  '10
'          TEMPO = T1D(k) * ZDENT_DIV(k) * NpasCic(k)
'      End Select
'      stmp = ": " & Format$(Int(TEMPO / 60), "#0") & "'"
'      If Printer.TextWidth(stmp) > TABx1 Then TABx1 = Printer.TextWidth(stmp)
'      Printer.CurrentX = MLeft + TabX
'      Printer.CurrentY = i * IRg + DELTA_Y
'      Printer.Print stmp
'    End If
'  Next k
'  i = j + 2
'  For k = 1 To MAXNumeroCicli
'    If (TipoCiclo(k) <> 0) Then
'      i = i + 1
'      Select Case TipoCiclo(k)
'        Case 12
'          TEMPO = T1D(k) * ZDENT_DIV(k)
'        Case 13
'          TEMPO = T1D(k) * ZDENT_DIV(k) * NpasCic(k)
'        Case Else  '10
'          TEMPO = T1D(k) * ZDENT_DIV(k) * NpasCic(k)
'      End Select
'      stmp = " " & Format(Int(TEMPO - Int(TEMPO / 60) * 60), "###0") & Chr$(34)
'      If Printer.TextWidth(stmp) > TABx2 Then TABx2 = Printer.TextWidth(stmp)
'      Printer.CurrentX = MLeft + TabX + TABx1
'      Printer.CurrentY = i * IRg + DELTA_Y
'      Printer.Print stmp
'      'DIVISIONE SINGOLA
'      stmp = " " & frmt(T1D(k), 2) & Chr$(34)
'      Printer.CurrentX = MLeft + PicW / 4
'      Printer.CurrentY = i * IRg + DELTA_Y
'      Printer.Print stmp
'    End If
'  Next k
'  i = i + 1
'  TEMPO = 0
'  For k = 1 To MAXNumeroCicli
'    If (TipoCiclo(k) <> 0) Then
'      Select Case TipoCiclo(k)
'        Case 12
'          TEMPO = TEMPO + T1D(k) * ZDENT_DIV(k)
'        Case 13
'          TEMPO = TEMPO + T1D(k) * ZDENT_DIV(k) * NpasCic(k)
'        Case Else  '10
'          TEMPO = TEMPO + T1D(k) * ZDENT_DIV(k) * NpasCic(k)
'      End Select
'    End If
'  Next k
'  stmp = ": " & Format$(Int(TEMPO / 60), "#0") & "'"
'  If Printer.TextWidth(stmp) > TABx1 Then TABx1 = Printer.TextWidth(stmp)
'  Printer.CurrentX = MLeft + TabX
'  Printer.CurrentY = i * IRg + DELTA_Y
'  Printer.Print stmp
'  TEMPO = 0
'  For k = 1 To MAXNumeroCicli
'    If (TipoCiclo(k) <> 0) Then
'      Select Case TipoCiclo(k)
'        Case 12
'          TEMPO = TEMPO + T1D(k) * ZDENT_DIV(k)
'        Case 13
'          TEMPO = TEMPO + T1D(k) * ZDENT_DIV(k) * NpasCic(k)
'        Case Else  '10
'          TEMPO = TEMPO + T1D(k) * ZDENT_DIV(k) * NpasCic(k)
'      End Select
'    End If
'  Next k
'  stmp = " " & Format(Int(TEMPO - Int(TEMPO / 60) * 60), "###0") & Chr$(34)
'  If Printer.TextWidth(stmp) > TABx2 Then TABx2 = Printer.TextWidth(stmp)
'  Printer.CurrentX = MLeft + TabX + TABx1
'  Printer.CurrentY = i * IRg + DELTA_Y
'  Printer.Print stmp
'  Printer.Line (MLeft + 0, i * IRg + DELTA_Y)-(MLeft + PicW / 2, i * IRg + DELTA_Y)
'  '
'  'CALCOLO TEMPO TOTALE DI RETTIFICA
'  Printer.CurrentX = MLeft + PicW / 2
'  Printer.CurrentY = DELTA_Y
'  stmp = MESSAGGI(0)
'  Printer.Print stmp
'  i = 1
'  Printer.DrawWidth = WLine
'  Printer.Line (MLeft + PicW / 2, i * IRg + DELTA_Y)-(PicW, i * IRg + DELTA_Y)
'  Printer.DrawWidth = 1
'  Printer.CurrentX = MLeft + PicW / 2
'  Printer.CurrentY = i * IRg + DELTA_Y
'  TabX = 0
'  For i = 1 To MAXNumeroCicli
'    If (TipoCiclo(i) <> 0) Then
'      Printer.CurrentX = MLeft + PicW / 2
'      Printer.CurrentY = i * IRg + DELTA_Y
'      stmp = " " & Trim$(OEM0.Label3(i).Caption)
'      If Printer.TextWidth(stmp) > TabX Then TabX = Printer.TextWidth(stmp)
'      Printer.Print stmp
'    End If
'  Next i
'  Printer.CurrentX = MLeft + PicW / 2
'  Printer.CurrentY = i * IRg + DELTA_Y
'  If (Lingua = "CH") Then
'    stmp = " " & MESSAGGI(3)
'  Else
'    stmp = " " & Left$(MESSAGGI(3), 7)
'  End If
'  If Printer.TextWidth(stmp) > TabX Then TabX = Printer.TextWidth(stmp)
'  Printer.Print stmp
'  '
'  i = i + 1
'  Printer.CurrentX = MLeft + PicW / 2
'  Printer.CurrentY = i * IRg + DELTA_Y
'  stmp = " " & MESSAGGI(2) & " "
'  If Printer.TextWidth(stmp) > TabX Then TabX = Printer.TextWidth(stmp)
'  Printer.Print stmp
'  '
'  TABx1 = 0
'  TT = 0
'  For i = 1 To MAXNumeroCicli
'    If (TipoCiclo(i) <> 0) Then
'      Printer.CurrentX = MLeft + PicW / 2 + TabX
'      Printer.CurrentY = i * IRg + DELTA_Y
'      TH = Int(T(i) / 60 / 60)
'      stmp = " " & Format$(TH, "#0") & "h"
'      If Printer.TextWidth(stmp) > TABx1 Then TABx1 = Printer.TextWidth(stmp)
'      '
'      If (TH > 0) Then Printer.Print stmp
'      '
'      TT = TT + T(i)
'    End If
'  Next i
'  TT = TT + TempoFas
'  '
'  Printer.CurrentX = MLeft + PicW / 2 + TabX + TABx1
'  Printer.CurrentY = i * IRg + DELTA_Y
'  stmp = " " & Format$(Int(TempoFas / 60), "#0") & "'"
'  If Printer.TextWidth(stmp) > TABx1 Then TABx1 = Printer.TextWidth(stmp)
'  Printer.Print stmp
'  '
'  TT = 0
'  TABx2 = 0
'  For i = 1 To MAXNumeroCicli
'    If (TipoCiclo(i) <> 0) Then
'      Printer.CurrentX = MLeft + PicW / 2 + TabX + TABx1
'      Printer.CurrentY = i * IRg + DELTA_Y
'      TH = Int(T(i) / 60 / 60)
'      Tm = Int(T(i) / 60 - 60 * TH)
'      stmp = " " & Format(Tm, "###0") & "'"
'      If Printer.TextWidth(stmp) > TABx2 Then TABx2 = Printer.TextWidth(stmp)
'      Printer.Print stmp
'      TT = TT + T(i)
'    End If
'  Next i
'  TT = TT + TempoFas
'  '
'  TT = 0
'  TABx3 = 0
'  For i = 1 To MAXNumeroCicli
'    If (TipoCiclo(i) <> 0) Then
'      Printer.CurrentX = MLeft + PicW / 2 + TabX + TABx1 + TABx2
'      Printer.CurrentY = i * IRg + DELTA_Y
'      TH = Int(T(i) / 60 / 60)
'      Tm = Int(T(i) / 60 - 60 * TH)
'      Ts = T(i) - 60 * 60 * TH - 60 * Tm
'      stmp = " " & Format(Ts, "###0") & Chr$(34)
'      If Printer.TextWidth(stmp) > TABx3 Then TABx3 = Printer.TextWidth(stmp)
'      Printer.Print stmp
'      TT = TT + T(i)
'    End If
'  Next i
'  TT = TT + TempoFas
'  '
'  Printer.CurrentX = MLeft + PicW / 2 + TabX + TABx1 + TABx2
'  Printer.CurrentY = i * IRg + DELTA_Y
'  stmp = " " & Format(Int(TempoFas - Int(TempoFas / 60) * 60), "###0") & Chr$(34)
'  If Printer.TextWidth(stmp) > TABx3 Then TABx3 = Printer.TextWidth(stmp)
'  Printer.Print stmp
'  '
'  i = i + 1
'  Printer.Line (MLeft + PicW / 2, i * IRg + DELTA_Y)-(MLeft + PicW, i * IRg + DELTA_Y)
'  Printer.CurrentX = MLeft + PicW / 2 + TabX
'  Printer.CurrentY = i * IRg + DELTA_Y
'  TH = Int(TT / 60 / 60)
'  Tm = Int(TT / 60 - 60 * TH)
'  Ts = TT - 60 * 60 * TH - 60 * Tm
'  stmp = ""
'  If (TH > 0) Then
'    stmp = stmp & " " & Format$(TH, "###0") & "h"
'  Else
'    stmp = stmp & "    "
'  End If
'  stmp = stmp & " " & Format$(Tm, "###0") & "'"
'  stmp = stmp & " " & Format$(Ts, "###0") & Chr$(34)
'  Printer.Print stmp
'  '
'  'CALCOLO ASPORTAZIONI
'  i = 0
'  Printer.CurrentX = MLeft + 3 * PicW / 4
'  Printer.CurrentY = i * IRg + DELTA_Y
'  stmp = MESSAGGI(1)
'  Printer.Print stmp
'  '
'  TabX = 0
'  For i = 1 To MAXNumeroCicli
'    If (TipoCiclo(i) <> 0) Then
'      Printer.CurrentX = MLeft + 3 * PicW / 4
'      Printer.CurrentY = i * IRg + DELTA_Y
'      stmp = " X=" & frmt(TotIncY(i), 4)
'      If Printer.TextWidth(stmp) > TabX Then TabX = Printer.TextWidth(stmp)
'      Printer.Print stmp
'    End If
'  Next i
'  '
'  TABx1 = 0
'  For i = 1 To MAXNumeroCicli
'    If (TipoCiclo(i) <> 0) And (Rip(i) - RipTEO(i) <> 0) Then
'      Printer.CurrentX = MLeft + 3 * PicW / 4 + TabX
'      Printer.CurrentY = i * IRg + DELTA_Y
'      'stmp = " Rip/RD = " & frmt(Rip(i) - RipTEO(i), 4) & "/" & frmt(R55(i), 4)
'      stmp = " (" & frmt(Rip(i) - RipTEO(i), 4) & "/" & frmt(R55(i), 4) & ")"
'      If Printer.TextWidth(stmp) > TABx1 Then TABx1 = Printer.TextWidth(stmp)
'      Printer.Print stmp
'    End If
'  Next i
'  '
'  i = i + 1
'  '
'  TotIncY(0) = 0
'  For kkk = 1 To MAXNumeroCicli
'    TotIncY(0) = TotIncY(0) + TotIncY(kkk)
'  Next kkk
'  '
'  stmp = " X = " & frmt(TotIncY(0), 4)
'  Printer.CurrentX = MLeft + 3 * PicW / 4
'  Printer.CurrentY = i * IRg + DELTA_Y
'  Printer.Print stmp
'  '
'  Printer.Line (MLeft + PicW / 2, DELTA_Y + 0 * IRg)-(MLeft + PicW / 2, DELTA_Y + (i + 2) * IRg)
'  Printer.Line (MLeft + 3 * PicW / 4 + DELTA_X / 2, DELTA_Y + 0 * IRg)-(MLeft + 3 * PicW / 4 + DELTA_X / 2, DELTA_Y + (i + 2) * IRg)
'  '
'  i = i + 2
'  stmp = " " & MESSAGGI(4) & ": " & frmt(UsuraDia, 4) & " "
'  Printer.CurrentX = MLeft + 3 * PicW / 4 - Printer.TextWidth(stmp) / 2
'  Printer.CurrentY = i * IRg + DELTA_Y
'  Printer.Print stmp
'  TabX = Printer.TextWidth(stmp)
'  Printer.Line (MLeft + PicW / 2, i * IRg + DELTA_Y)-(MLeft + PicW, (i + 1) * IRg + DELTA_Y), , B
'  '
'Return

End Sub
Public Function CALCOLO_VITE_ISO() As Boolean
'
'FILETTATURA METRICA ISO
'DATI DI INPUT
Dim H3      As Double  'ALTEZZA DENTE
Dim Rtesta  As Double  'RAGGIO DI TESTA
Dim Rfondo  As Double  'RAGGIO DI FONDO
'DATI CALCOLATI
Dim RtMola        As Double
Dim RfMola        As Double
Dim AddMola       As Double
Dim AltMola       As Double
Dim LargMola      As Double
Dim LargMolaTMP   As Double
Dim L             As Double
'
On Error GoTo errCALCOLO_VITE_ISO
  '
  CALCOLO_VITE_ISO = False
  '
  Call PROFILO_VITE_ISO(H3, Rtesta, Rfondo)
  '
  RtMola = Rtesta
  RfMola = Rfondo
  '
  AddMola = (DEXTV - DprimV) / 2 + Rtesta
  AltMola = AddMola + (DprimV - DIntV) / 2
  '
  LargMolaTMP = LEP("R[37]")
  L = RtMola * Tan((PG / 2 - (ApVite * PG / 180)) / 2)
  LargMola = SpNormV + 2 * AddMola * Tan(ApVite * PG / 180) + 2 * L
  LargMola = Int(LargMola + 1) + 1
  If (LargMola > LargMolaTMP) Then Call SP("R[37]", frmt(LargMola, 4))
  '
  E38V = SpNormV
  '
  AngMedF1V = ApVite * PG / 180
  AngMedF2V = ApVite * PG / 180
  '
  E43F1V = AddMola
  E43F2V = AddMola
  E44F1V = AltMola
  E44F2V = AltMola
  '
  PBF1V = 0.5
  PBF2V = 0.5
  BombF1V = 0
  BombF2V = 0
  RF = RfMola
  RT = RtMola
  '
  CALCOLO_VITE_ISO = True
  '
Exit Function
  
errCALCOLO_VITE_ISO:
  WRITE_DIALOG "SUB: CALCOLO_VITE_ISO " & Error(Err)
  Exit Function

End Function
'
Sub PROFILO_VITE_ISO(H3 As Double, Rtesta As Double, Rfondo As Double)
'
Const Coeff1 = 0.61343 'calcolo dell'altezza dente
Const Coeff2 = 0.64952 'calcolo del diametro medio
Const Coeff3 = 0.14434 'calcolo del raggio di fondo
Dim Rc   As Double     'RAGGIO DI USCITA
'
On Error Resume Next
  '
  'CALCOLO I PARAMETRI DEL PROFILO VITE
  Rc = LEP("RUSCITA")
  H3 = PassoElica * Coeff1             'ALTEZZA DENTE
  DIntV = DEXTV - 2 * H3               'DIAMETRO INTERNO
  SpNormV = PassoElica / 2             'SPESSORE NORMALE DENTE
  DprimV = DEXTV - Coeff2 * PassoElica 'DIAMETRO PRMITIVO/MEDIO
  DprimV = Int((DprimV + 0.0005) * 1000) / 1000
  Rfondo = Coeff3 * PassoElica         'RAGGIO DI FONDO VITE
  Rtesta = Rc + 0.2                    'RAGGIO DI TESTA VITE
  '
  BetaVRad = Atn(PassoElica / PG / DprimV)
  BetaV = BetaVRad * (180 / PG)        'INCLINAZIONE MOLA
  ApVite = 30                          'ANGOLO DI PRESSIONE
  '
End Sub

Public Sub VITI_ESTERNE_VISUALIZZA_PROFILO_ISO(ByVal Controllo As Control)
'
Dim PicW   As Single
Dim PicH   As Single
Dim Xm     As Double
Dim Ym     As Double
Dim Scala  As Double
'
Dim Ap     As Double
Dim Sw     As Double
Dim HW     As Double
Dim Hkw    As Double
Dim RT     As Double
Dim RF     As Double
Dim PASSO  As Double
'
Dim XiFondo As Double, YiFondo As Double
Dim XfFondo As Double, YfFondo As Double
Dim XiRf    As Double, YiRf    As Double
Dim XfRf    As Double, YfRf    As Double
Dim XrRf    As Double, YrRf    As Double
Dim XiF     As Double, YiF     As Double
Dim XfF     As Double, YfF     As Double
Dim XiRt    As Double, YiRt    As Double
Dim XfRt    As Double, YfRt    As Double
Dim XrRt    As Double, YrRt    As Double
Dim XiTesta As Double, YiTesta As Double
Dim XfTesta As Double, YfTesta As Double
Dim XrFondo As Double, Yrfondo As Double
Dim L       As Double
'
Dim Xdep As Double
Dim Ydep As Double
Dim Xfin As Double
Dim Yfin As Double
'
Dim Xmin As Double
Dim Xmax As Double
Dim Ymin As Double
Dim Ymax As Double
'
Dim X1 As Double
Dim Y1 As Double
Dim X2 As Double
Dim Y2 As Double
'
On Error GoTo errVITI_ESTERNE_VISUALIZZA_PROFILO_ISO
  '
  Controllo.BackColor = &HFFFFFF
  '
  Controllo.ScaleWidth = 190
  Controllo.ScaleHeight = 95
  '
  PicH = Controllo.ScaleHeight
  PicW = Controllo.ScaleWidth
  '
  Scala = GetInfo("DIS0", "ScalaMOLA", Path_LAVORAZIONE_INI)
  If (Scala <= 0) Then Scala = 1
  '
  Xm = 0: Ym = 0
  '
  Dim H3      As Double 'ALTEZZA DENTE
  Dim D3      As Double 'DIAMETRO DI FONDO
  Dim D2      As Double
  Dim Rtesta  As Double
  Dim Rfondo  As Double
  Dim s       As Double
  Dim TIPO_MACCHINA As String
  
  TIPO_MACCHINA = GetInfo("Configurazione", "TIPO_MACCHINA", Path_LAVORAZIONE_INI)
  TIPO_MACCHINA = UCase$(Trim$(TIPO_MACCHINA))
    
  PASSO = LEP("PassoElica")
  ApVite = LEP("ALFA")
  H3 = LEP("H3")     'ALTEZZA DENTE
  D3 = LEP("DIAINT") 'DIAMETRO FONDO
  D2 = LEP("R[103]") 'DIAMETRO MEDIO
  s = LEP("SM")
  Rtesta = LEP("RTT")
  Rfondo = LEP("RFF")
  'CALCOLO PROFILO MOLA
  Dim AddMola     As Double
  Dim AltMola     As Double
  Dim LargMolaTMP As Double
  Dim LargMola    As Double
  DEXTV = D3 + 2 * H3
  DprimV = D2
  BetaVRad = Atn(PASSO / PG / DprimV)
  BetaV = BetaVRad * (180 / PG)
  BetaIng = 90 - BetaV
  AddMola = (DEXTV - DprimV) / 2 + Rtesta
  AltMola = AddMola + (DprimV - D3) / 2
  LargMolaTMP = LEP("R[37]")
  L = Rtesta * Tan((PG / 2 - ApVite * 180 * PG) / 2)
  LargMola = s + 2 * AddMola * Tan(ApVite * 180 * PG) + 2 * L
  LargMola = Int(LargMola + 1) + 1
  If (LargMola > LargMolaTMP) Then Call SP("R[37]", frmt(LargMola, 4))
  'AGGIORNAMENTO PARAMETRI
  Call SP("S0N", frmt(s, 8))
  Call SP("DIAMEXT", frmt(DEXTV, 4))
  Call SP("DIAINT", frmt(DIntV, 4))
  Call SP("PASSOELICA_G", frmt(PASSO, 6))
  If (TIPO_MACCHINA = "GR250") Then
     Call SP("R[41]", frmt(BetaV, 4))
  Else
     Call SP("R[41]", frmt(BetaIng, 4))
  End If
  Call SP("R[103]", frmt(DprimV, 4))
  Call SP("LARGMOLAPR_G", frmt(s, 4))
  Call SP("ANGFIANCOF1_G", frmt(ApVite, 4))
  Call SP("ANGFIANCOF2_G", frmt(ApVite, 4))
  Call SP("ADDENDUMF1_G", frmt(AddMola, 4))
  Call SP("ADDENDUMF2_G", frmt(AddMola, 4))
  Call SP("ALTEZZAFIANCOF1_G", frmt(AltMola, 4))
  Call SP("ALTEZZAFIANCOF2_G", frmt(AltMola, 4))
  Call SP("POSBOMBFIANCOF1_G", "0.5")
  Call SP("POSBOMBFIANCOF2_G", "0.5")
  Call SP("BOMBFIANCOF1_G", "0")
  Call SP("BOMBFIANCOF2_G", "0")
  Call SP("RTESTA_G", frmt(Rtesta, 4))
  Call SP("RTESTA2_G", frmt(Rtesta, 4))
  Call SP("RFONDO_G", frmt(Rfondo, 4))
  Call SP("RFONDO2_G", frmt(Rfondo, 4))
  Call SP("R[34]", "0")
  Call SP("CORQRULLI", "0")
  'CALCOLO DEI PUNTI DEL PROFILO
  Ap = ApVite
  Ap = Ap / 180 * PG
  Sw = s
  HW = (DEXTV - D3) / 2
  Hkw = (DEXTV - D2) / 2
  RT = 0
  RF = Rfondo
  '
  L = RF * Tan((PG / 2 - Ap) / 2)
  XiFondo = PASSO / 2
  YiFondo = HW - Hkw
  XfFondo = Sw / 2 + (HW - Hkw) * Tan(Ap) + L
  YfFondo = HW - Hkw
  XiRf = XfFondo
  YiRf = YfFondo
  XfRf = Sw / 2 + (HW - Hkw) * Tan(Ap) - L * Sin(Ap)
  YfRf = HW - Hkw - L * Cos(Ap)
  XrRf = XfFondo
  YrRf = HW - Hkw - RF
  XiF = XfRf
  YiF = YfRf
  '
  L = RT * Tan((PG / 2 - Ap) / 2)
  XfF = Sw / 2 - Hkw * Tan(Ap) + L * Sin(Ap)
  YfF = Hkw - L * Cos(Ap)
  XiRt = XfF
  YiRt = YfF
  XfRt = Sw / 2 - Hkw * Tan(Ap) - L
  YfRt = Hkw
  XrRt = XfRt
  YrRt = Hkw - RT
  XiTesta = XfRt
  YiTesta = YfRt
  XfTesta = 0
  YfTesta = Hkw
  '
  NbpTotal = 0
  For i = 1 To 100: XFilMetr(i) = 0: YFilMetr(i) = 0: Next
  '
  Xdep = XiFondo: Ydep = -YiFondo
  Call MemorisPunto(Xdep, Ydep, PG / 2)
  Xdep = XiFondo: Ydep = -YiFondo
  Xfin = XfFondo: Yfin = -YfFondo
  Call CalcPuntiFianco(Xdep, Ydep, Xfin, Yfin, PG / 2, 5)
  Call CalcPuntiRaggio(-RF, XrRf, -YrRf, 0, PG / 2 - Ap, 5)
  Xdep = XiF: Ydep = -YiF
  Xfin = XfF: Yfin = YfF
  Call CalcPuntiFianco(Xdep, Ydep, Xfin, Yfin, Ap, 3)
  Call CalcPuntiRaggio(-RT, XrRt, YrRt, PG / 2 - Ap, 0, 5)
  Xdep = XiTesta: Ydep = YiTesta
  Xfin = XfTesta: Yfin = YfTesta
  Call CalcPuntiFianco(Xdep, Ydep, Xfin, Yfin, PG / 2, 5)
  'RICERMA MASSIMI E MINIMI
  Xmin = 999: Xmax = -999
  Ymin = 999: Ymax = -999
  For i = 1 To NbpTotal - 1
    If (YFilMetr(i) > Ymax) Then Ymax = YFilMetr(i)
    If (YFilMetr(i) < Ymin) Then Ymin = YFilMetr(i)
    If (XFilMetr(i) > Xmax) Then Xmax = XFilMetr(i)
    If (XFilMetr(i) < Xmin) Then Xmin = XFilMetr(i)
  Next
  Xm = 0: Ym = -(Ymin + (Ymax - Ymin) / 2)
  'VISUALIZZO 3 PASSI DEL PROFILO DELLA VITE
  For j = -1 To 1
    For i = 1 To NbpTotal - 1
      Controllo.ForeColor = vbBlue
      X1 = (XFilMetr(i) + PASSO * j - Xm) * Scala + PicW / 2
      Y1 = (-YFilMetr(i) - Ym) * Scala + PicH / 2
      X2 = (XFilMetr(i + 1) + PASSO * j - Xm) * Scala + PicW / 2
      Y2 = (-YFilMetr(i + 1) - Ym) * Scala + PicH / 2
      Controllo.Line (X1, Y1)-(X2, Y2)
      X1 = (-XFilMetr(i) + PASSO * j - Xm) * Scala + PicW / 2
      Y1 = (-YFilMetr(i) - Ym) * Scala + PicH / 2
      X2 = (-XFilMetr(i + 1) + PASSO * j - Xm) * Scala + PicW / 2
      Y2 = (-YFilMetr(i + 1) - Ym) * Scala + PicH / 2
      Controllo.Line (X1, Y1)-(X2, Y2)
    Next i
  Next
  'VISUALIZZO IL DIAMETRO MEDIO
  Controllo.ForeColor = vbBlack
  Y1 = (0 - Ym) * Scala + PicH / 2
  Controllo.Line (0, Y1)-(PicW, Y1)
  'VISUALIZZO GLI ENTI
  'INIZIO FONDO
  X1 = (XiFondo - Xm) * Scala + PicW / 2: Y1 = (YiFondo - Ym) * Scala + PicH / 2
  Controllo.Circle (X1, Y1), 0.01 * Scala, vbRed
  X1 = (-XiFondo - Xm) * Scala + PicW / 2: Y1 = (YiFondo - Ym) * Scala + PicH / 2
  Controllo.Circle (X1, Y1), 0.01 * Scala, vbRed
  'INIZIO RAGGIO DI FONDO
  X1 = (XiRf - Xm) * Scala + PicW / 2: Y1 = (YiRf - Ym) * Scala + PicH / 2
  Controllo.Circle (X1, Y1), 0.01 * Scala, vbRed
  X1 = (-XiRf - Xm) * Scala + PicW / 2: Y1 = (YiRf - Ym) * Scala + PicH / 2
  Controllo.Circle (X1, Y1), 0.01 * Scala, vbRed
  'INIZIO FIANCO
  X1 = (XiF - Xm) * Scala + PicW / 2: Y1 = (YiF - Ym) * Scala + PicH / 2
  Controllo.Circle (X1, Y1), 0.01 * Scala, vbRed
  X1 = (-XiF - Xm) * Scala + PicW / 2: Y1 = (YiF - Ym) * Scala + PicH / 2
  Controllo.Circle (X1, Y1), 0.01 * Scala, vbRed
  'INIZIO RAGGIO DI TESTA
  X1 = (XiRt - Xm) * Scala + PicW / 2: Y1 = (-YiRt - Ym) * Scala + PicH / 2
  Controllo.Circle (X1, Y1), 0.01 * Scala, vbRed
  X1 = (-XiRt - Xm) * Scala + PicW / 2: Y1 = (-YiRt - Ym) * Scala + PicH / 2
  Controllo.Circle (X1, Y1), 0.01 * Scala, vbRed
  'FINE TESTA
  X1 = (XfTesta - Xm) * Scala + PicW / 2: Y1 = (-YfTesta - Ym) * Scala + PicH / 2
  Controllo.Circle (X1, Y1), 0.01 * Scala, vbRed
  X1 = (-XfTesta - Xm) * Scala + PicW / 2: Y1 = (-YfTesta - Ym) * Scala + PicH / 2
  Controllo.Circle (X1, Y1), 0.01 * Scala, vbRed
  'INVIO SOTTOPROGRAMMA MODIFICATO
  Dim MODALITA As String
  Dim sRESET   As String
  Dim iRESET   As Integer
  'LEGGO LA MODALITA
  MODALITA = GetInfo("Configurazione", "Modalita", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))
  If (MODALITA = "SPF") Then
    'CONTROLLO SE SONO NELLO DI RESET PER ACCETTARE LA MODIFICA
    'LETTURA DELL'ITEM DDE PER CONTROLLO STATO DI RESET
    sRESET = GetInfo("CONFIGURAZIONE", "sRESET", PathFILEINI)
    If (Len(sRESET) > 0) Then
      iRESET = val(OPC_LEGGI_DATO(sRESET))
    Else
      iRESET = 1
    End If
    'INVIO IL SOTTOPROGRAMMA MODIFICATO
    If (iRESET = 1) Then
      Call OEMX.SuOEM1.CreaSPF
    Else
      'SEGNALAZIONE UTENTE
      WRITE_DIALOG "Data cannot be modified: CNC is not on RESET state"
    End If
  End If
  '
Exit Sub

errVITI_ESTERNE_VISUALIZZA_PROFILO_ISO:
  WRITE_DIALOG "SUB: VITI_ESTERNE_VISUALIZZA_PROFILO_ISO " & Error(Err)
  Exit Sub

End Sub

Public Sub VITI_ESTERNE_VISUALIZZA_PROFILO_ZI(ByVal Controllo As Control)
'
Dim NumeroPuntiFianco As Integer
Dim diaSAP As Double
Dim diaEAP As Double
'
Dim hu1     As Double
Dim PM      As Integer
Dim PmB     As Integer
Dim Bmax    As Double
Dim XX      As Double
Dim MM1     As Double
Dim MM2     As Double
Dim da      As Double
Dim A       As Double
Dim XCF     As Double
Dim YCF     As Double
Dim RFM     As Double
Dim Sonx    As Double
'
Dim E38     As Double
Dim AngMed  As Double
Dim E43     As Double
Dim E44     As Double
'
Dim PB      As Double
Dim BOMB    As Double
Dim Dia_Ext As Double
Dim Apx     As Double
Dim Tangax  As Double
'
Dim stmp    As String
Dim TIPO_LAVORAZIONE As String
'
Const nPr = 10
'
'Const TIPO_LAVORAZIONE = "VITI_ESTERNE"
'
On Error GoTo errVITI_ESTERNE_VISUALIZZA_PROFILO_ZI
  '
  'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  
  diaSAP = LEP("COR[0,78]")
  diaEAP = LEP("COR[0,79]")
  NumeroPuntiFianco = LEP("COR[0,80]")
  '
  DiaSmusso = DEXTV
  Dia_Ext = DiaSmusso
  '
  SONt = SpNormV / Cos(BetaIngRad)
  '
  DiaAttivoPiede = DIntV + 2 * RF * (1 - Sin(ApNormVrad))    'SAP
  If DiaAttivoPiede < diaSAP Then DiaAttivoPiede = diaSAP
  If DiaAttivoPiede < DbaseV Then DiaAttivoPiede = DbaseV + 0.1
  '
  'ricerca EAP da Dia_Ext -> DiaSmusso
  RX = Dia_Ext / 2
  Do
    RX = RX - 0.01
    Apx = Acs(Rb / RX)      'ang.press.trasversale
    Tangax = Atn(Tan(Apx) * PassoElica / 2 / PG / RX * Cos(BetaVRad))
    XX = 2 * (RX + RT * (1 - Sin(Tangax)))  'dia est calcolato da EAP
  Loop Until XX < Dia_Ext
  DiaSmusso = RX * 2
  If (diaEAP < DiaSmusso) And (diaEAP > DiaAttivoPiede) Then DiaSmusso = diaEAP
  '
  MM1 = Sqr(DiaAttivoPiede ^ 2 - DbaseV ^ 2) / 2
  MM2 = Sqr(DiaSmusso ^ 2 - DbaseV ^ 2) / 2
  '
  IL = (DiaMolEff + DIntV) / 2       'interasse
  PassoElica = MnormV * PG / Sin(BetaIngRad) * NFil
  '
  If (NumeroPuntiFianco > 1) And (NumeroPuntiFianco <= NPE - 2) Then
    Np = NumeroPuntiFianco
  Else
    Np = NPE - 2
  End If
  '
  p1 = 0
  '
  Dim MMpre As Double
  Dim MMsuc As Double
  Dim iPNT  As Integer
  '
  Dim MODALITA         As String
  '
  'LEGGO LA MODALITA DI TRASFERIMENTO DATI
  MODALITA = GetInfo("Configurazione", "MODALITA", PathFILEINI)
  MODALITA = UCase$(Trim$(MODALITA))
  'LETTURA DEL NOME PEZZO
  If (MODALITA = "OFF-LINE") Then
    stmp = GetInfo(TIPO_LAVORAZIONE, "NomePezzoInserimento", Path_LAVORAZIONE_INI)
  Else
    stmp = LEGGI_NomePezzo(TIPO_LAVORAZIONE)
  End If
  '
  'Open g_chOemPATH & "\VITE.SPF" For Output As #1
  'Print #1, ";PROFILO ASSIALE " & stmp & " (" & TipoVite & ")"
  'Print #1, ";VERSIONE: " & Date
  'Print #1, ";FIANCO 1"
  '
  'Calcolo fianco mola
  iPNT = -1
  For i = 0 To Np - 1
    '
    MM(i) = MM1 + (MM2 - MM1) * i / (Np - 1)
    RX = Sqr(Rb ^ 2 + MM(i) ^ 2)
    '
    MMsuc = MM(i)
    '
    GoSub CALCOLO_T0_FI0    'calcolo punto ZI
    '
    'SCRIVO NEL FILE SOLO UN NUMERO FINITO DI PUNTI
    'If (MMsuc - MMpre) >= 0.5 Or (i = 0) Or (i = Np - 1) Then
      '
      iPNT = iPNT + 1
      MMpre = MMsuc
      '
      'RAB
      RR(iPNT) = RX
      AA(iPNT) = 90 - T0 * 180 / PG
      BB(iPNT) = T0 * 180 / PG - FI0 * 180 / PG
      '
      'RVT
      VV(iPNT) = AA(iPNT) / 360 * PassoElica
      TT(iPNT) = Atn(Tan(T0 - FI0) * PassoElica / (2 * PG * RR(iPNT))) * 180 / PG
      'stmp = ""
      'stmp = stmp & "YY[0," & Format$(iPNT + 1, "###0") & "]=" & frmt(RR(iPNT), 4) & " "
      'stmp = stmp & "XX[0," & Format$(iPNT + 1, "###0") & "]=" & frmt(-VV(iPNT), 4) & " "
      'stmp = stmp & "TT[0," & Format$(iPNT + 1, "###0") & "]=" & frmt(TT(iPNT), 4) & " "
      'Print #1, stmp
      '
    'End If
    '
    Call CALCOLO_PUNTO_MOLA(IL, BetaVRad, PassoElica, RX, T0, FI0, X(i), Y(i), B(i))    'calcolo punto mola Xm,Ym
    '
  Next i
  '
  'CALCOLO SUL PRIMITIVO
  RX = Rprim
  MM(i) = Sqr(RX ^ 2 - Rb ^ 2)
  GoSub CALCOLO_T0_FI0
    '
    'PRIMITIVO FIANCO 1
    'RAB
    RR(NPE) = RX
    AA(NPE) = 90 - T0 * 180 / PG
    BB(NPE) = T0 * 180 / PG - FI0 * 180 / PG
    '
    'RVT
    VV(NPE) = AA(NPE) / 360 * PassoElica
    TT(NPE) = (Tan(T0 - FI0) * PassoElica / (2 * PG * RR(NPE))) * 180 / PG
    '
    'stmp = ""
    'stmp = stmp & "YY[0,0]=" & frmt(RR(NPE), 4) & " "
    'stmp = stmp & "XX[0,0]=" & frmt(-VV(NPE), 4) & " "
    'stmp = stmp & "TT[0,0]=" & frmt(TT(NPE), 4) & " "
    'Print #1, stmp
    '
  Call CALCOLO_PUNTO_MOLA(IL, BetaVRad, PassoElica, RX, T0, FI0, X(i), Y(i), B(i)) 'calcolo punto mola Xm,Ym
  '
  'Print #1, ";"
  'Print #1, ";FIANCO 2"
  'For i = 0 To iPNT
  '  stmp = ""
  '  stmp = stmp & "YY[0," & Format$(51 + i, "###0") & "]=" & frmt(RR(i), 4) & " "
  '  stmp = stmp & "XX[0," & Format$(51 + i, "###0") & "]=" & frmt(VV(i), 4) & " "
  '  stmp = stmp & "TT[0," & Format$(51 + i, "###0") & "]=" & frmt(TT(i), 4) & " "
  '  Print #1, stmp
  'Next i
  '  'PRIMITIVO FIANCO 2
  '  stmp = ""
  '  stmp = stmp & "YY[0,50]=" & frmt(RR(NPE), 4) & " "
  '  stmp = stmp & "XX[0,50]=" & frmt(VV(NPE), 4) & " "
  '  stmp = stmp & "TT[0,50]=" & frmt(TT(NPE), 4) & " "
  '  Print #1, stmp
  '  '
  'Print #1, ";"
  'Print #1, ";RAGGIO INTERNO ESTERNO E PUNTI"
  '  stmp = ""
  '  stmp = stmp & "YY[0,100]=" & frmt(DIntV / 2, 4) & " "
  '  stmp = stmp & "XX[0,100]=" & frmt(DExtV / 2, 4) & " "
  '  stmp = stmp & "TT[0,100]=" & Format(iPNT + 1, "##0") & " "
  '  Print #1, stmp
  '  '
  'Print #1, ";"
  'Print #1, "M17"
  'Close #1
  '
  'VISUALIZZAZIONE ASSIALE PROFILO VITE ZI
  Dim Scala As Single
  Dim PicH  As Single
  Dim PicW  As Single
  Dim Xm    As Single
  Dim Ym    As Single
  Dim X1    As Single
  Dim Y1    As Single
  Dim X2    As Single
  Dim Y2    As Single
  Dim ZeroPezzoX As Single
  Dim ZeroPezzoY As Single
  '
  Controllo.BackColor = &HFFFFFF
  '
  Controllo.ScaleWidth = 190
  Controllo.ScaleHeight = 95
  '
  PicH = Controllo.ScaleHeight
  PicW = Controllo.ScaleWidth
  '
  Scala = GetInfo("DIS0", "ScalaMOLA", Path_LAVORAZIONE_INI)
  If (Scala <= 0) Then Scala = 1
  '
  'PRIMITIVO DELLA VITE
  X1 = 0
  Y1 = PicH / 2
  X2 = PicW
  Y2 = PicH / 2
  Controllo.Line (X1, Y1)-(X2, Y2)

Dim j As Integer
For j = -1 To 1
  ZeroPezzoX = (PassoElica / NFil) * j
  ZeroPezzoY = Rprim
  'FIANCO 1
  i = iPNT
    X1 = (-VV(i) - ZeroPezzoX) * Scala + PicW / 2
    Y1 = (RR(i) - ZeroPezzoY) * Scala + PicH / 2
    X2 = (-PassoElica / NFil / 2 - ZeroPezzoX) * Scala + PicW / 2
    Y2 = (RR(i) - ZeroPezzoY) * Scala + PicH / 2
    Controllo.Line (X1, Y1)-(X2, Y2)
  For i = 1 To iPNT - 1
    X1 = (-VV(i) - ZeroPezzoX) * Scala + PicW / 2
    Y1 = (RR(i) - ZeroPezzoY) * Scala + PicH / 2
    X2 = (-VV(i + 1) - ZeroPezzoX) * Scala + PicW / 2
    Y2 = (RR(i + 1) - ZeroPezzoY) * Scala + PicH / 2
    Controllo.Line (X1, Y1)-(X2, Y2)
  Next i
  i = 1
    X1 = (-VV(i) - ZeroPezzoX) * Scala + PicW / 2
    Y1 = (RR(i) - ZeroPezzoY) * Scala + PicH / 2
    X2 = (0 - ZeroPezzoX) * Scala + PicW / 2
    Y2 = (RR(i) - ZeroPezzoY) * Scala + PicH / 2
    Controllo.Line (X1, Y1)-(X2, Y2)
  'FIANCO 2
  i = iPNT
    X1 = (VV(i) - ZeroPezzoX) * Scala + PicW / 2
    Y1 = (RR(i) - ZeroPezzoY) * Scala + PicH / 2
    X2 = (PassoElica / NFil / 2 - ZeroPezzoX) * Scala + PicW / 2
    Y2 = (RR(i) - ZeroPezzoY) * Scala + PicH / 2
    Controllo.Line (X1, Y1)-(X2, Y2)
  For i = 1 To iPNT - 1
    X1 = (VV(i) - ZeroPezzoX) * Scala + PicW / 2
    Y1 = (RR(i) - ZeroPezzoY) * Scala + PicH / 2
    X2 = (VV(i + 1) - ZeroPezzoX) * Scala + PicW / 2
    Y2 = (RR(i + 1) - ZeroPezzoY) * Scala + PicH / 2
    Controllo.Line (X1, Y1)-(X2, Y2)
  Next i
  i = 1
    X1 = (VV(i) - ZeroPezzoX) * Scala + PicW / 2
    Y1 = (RR(i) - ZeroPezzoY) * Scala + PicH / 2
    X2 = (0 - ZeroPezzoX) * Scala + PicW / 2
    Y2 = (RR(i) - ZeroPezzoY) * Scala + PicH / 2
    Controllo.Line (X1, Y1)-(X2, Y2)
Next j
  '
Exit Sub

errVITI_ESTERNE_VISUALIZZA_PROFILO_ZI:
  WRITE_DIALOG "SUB: VITI_ESTERNE_VISUALIZZA_PROFILO_ZI " & Error(Err)
  Exit Sub

CALCOLO_T0_FI0:
Dim InvPrim As Double
Dim ApxT    As Double
Dim InvAx   As Double
  '
  Apx = Acs(Rb / RX)
  '
  InvPrim = Tan(ApAssVRad) - ApAssVRad
  InvAx = Tan(Apx) - Apx
  '
  Sx = (SONt / 2 / Rprim + InvPrim - InvAx) * 2 * RX
  VX = 2 * PG * RX / NFil - Sx
  '
  T0 = PG / 2 - VX / 2 / RX
  FI0 = T0 - Apx
  If T0 < 0 Then FI0 = FI0 - 2 * PG
  If FI0 > PG Then FI0 = FI0 - PG * 2
  If FI0 < -PG Then FI0 = FI0 + PG * 2
  '
Return

End Sub

Sub VITI_ESTERNE_VISUALIZZA_PROFILO(ByVal Controllo As Control, ByVal SiStampa As String)
'
On Error GoTo errVITI_ESTERNE_VISUALIZZA_PROFILO
  '
  AngRaccordoTesta = LEP("ANGR")
  PassoElica = LEP("PassoElica")
  DEXTV = LEP("DEXT")
  DprimV = LEP("DprimV")
  '
  TipoVite = UT(LEP_STR("TIPO"))
  '
  NFil = LEP("NPRI")
  MnormV = LEP("MN")
  ApVite = LEP("ALFA")
  BetaV = LEP("BETA")
  '
  If (TipoVite <> "ZA") Then
    ApNormV = ApVite
    ApNormVrad = ApNormV * (PG / 180)
    '
    ApViteRad = ApNormV * (PG / 180)
  Else
    ApAssV = ApVite
    ApAssVRad = ApAssV * (PG / 180)
    '
    ApViteRad = ApVite * (PG / 180)
  End If
  '
  BetaV = Abs(BetaV)
  BetaVRad = BetaV * (PG / 180)
  '
  BetaIng = 90 - BetaV
  BetaIngRad = BetaIng * (PG / 180)
  '
  DIntV = LEP("DINT")
  DEXTV = LEP("DEXT")
  '
  iDefSp = LEP("iDefSp")
  SpNormV = LEP("S0N")
  QR = LEP("QR")
  DR = LEP("DR")
  '
  CoordinataXV = LEP("XC")
  CoordinataYV = LEP("YC")
  RaggioFiancoV = LEP("RC")
  RagFiancoMola = RaggioFiancoV
  '
  DiaMolTeo = LEP("DTEO")
  DiaMolEff = LEP("DEFF")
  RT = LEP("RT")
  RF = LEP("RF")
  '
  Select Case TipoVite
    '
    Case "ZI"
      ApAssVRad = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      ApAssV = ApAssVRad * (180 / PG)
      Rprim = MnormV * NFil / 2 / Cos(BetaIngRad)          'R primitivo
      Betab = Atn(Tan(BetaIngRad) * Cos(ApAssVRad))        'Elica base
      Rb = Rprim * Cos(ApAssVRad)                          'R base
      DbaseV = 2 * Rb                                      'DBaseV
      DprimV = 2 * Rprim                                   'D prim.
      If (BetaIngRad <> 0) Then
        PassoElica = PG * MnormV * NFil / Sin(BetaIngRad)
      Else
        PassoElica = 0
      End If
      Call VITI_ESTERNE_VISUALIZZA_PROFILO_ZI(Controllo)
      '
    Case "ZA"
      Dim ApTrasversale As Double
      ApTrasversale = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      Rprim = MnormV * NFil / 2 / Cos(BetaIngRad)              'R primitivo
      Betab = Atn(Tan(BetaIngRad) * Cos(ApTrasversale))        'Elica base
      Rb = Rprim * Cos(ApTrasversale)                          'R base
      DbaseV = 2 * Rb                                          'DBaseV
      DprimV = 2 * Rprim                                       'D prim.
      If (BetaIngRad <> 0) Then
        PassoElica = PG * MnormV * NFil / Sin(BetaIngRad)
      Else
        PassoElica = 0
      End If
      '
    Case "ZK"
      ApAssVRad = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      ApAssV = ApAssVRad * (180 / PG)
      Rprim = MnormV * NFil / 2 / Cos(BetaIngRad)          'R primitivo
      Betab = Atn(Tan(BetaIngRad) * Cos(ApAssVRad))        'Elica base
      Rb = Rprim * Cos(ApAssVRad)                          'R base
      DbaseV = 2 * Rb                                      'DBaseV
      DprimV = 2 * Rprim                                   'D prim.
      If (BetaIngRad <> 0) Then
        PassoElica = PG * MnormV * NFil / Sin(BetaIngRad)
      Else
        PassoElica = 0
      End If
      '
    Case "ZC"
      ApAssVRad = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      ApAssV = ApAssVRad * (180 / PG)
      Rprim = MnormV * NFil / 2 / Cos(BetaIngRad)          'R primitivo
      Betab = Atn(Tan(BetaIngRad) * Cos(ApAssVRad))        'Elica base
      Rb = Rprim * Cos(ApAssVRad)                          'R base
      DbaseV = 2 * Rb                                      'DBaseV
      DprimV = 2 * Rprim                                   'D prim.
      If (BetaIngRad <> 0) Then
        PassoElica = PG * MnormV * NFil / Sin(BetaIngRad)
      Else
        PassoElica = 0
      End If
      '
    Case "ZN"
      ApAssVRad = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      ApAssV = ApAssVRad * (180 / PG)
      Rprim = MnormV * NFil / 2 / Cos(BetaIngRad)          'R primitivo
      Betab = Atn(Tan(BetaIngRad) * Cos(ApAssVRad))        'Elica base
      Rb = Rprim * Cos(ApAssVRad)                          'R base
      DbaseV = 2 * Rb                                      'DBaseV
      DprimV = 2 * Rprim                                   'D prim.
      If (BetaIngRad <> 0) Then
        PassoElica = PG * MnormV * NFil / Sin(BetaIngRad)
      Else
        PassoElica = 0
      End If
      '
    Case "BALL"
      '
    Case "STANDARD"
      ApAssVRad = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      ApAssV = ApAssVRad * (180 / PG)
      Rprim = MnormV * NFil / 2 / Cos(BetaIngRad)          'R primitivo
      Betab = Atn(Tan(BetaIngRad) * Cos(ApAssVRad))        'Elica base
      Rb = Rprim * Cos(ApAssVRad)                          'R base
      DbaseV = 2 * Rb                                      'DBaseV
      DprimV = 2 * Rprim                                   'D prim.
      '
    Case "ISO"
      ApAssVRad = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      ApAssV = ApAssVRad * (180 / PG)
      Rprim = MnormV * NFil / 2 / Cos(BetaIngRad)          'R primitivo
      Betab = Atn(Tan(BetaIngRad) * Cos(ApAssVRad))        'Elica base
      Rb = Rprim * Cos(ApAssVRad)                          'R base
      DbaseV = 2 * Rb                                      'DBaseV
      DprimV = 2 * Rprim                                   'D prim.
      Call VITI_ESTERNE_VISUALIZZA_PROFILO_ISO(Controllo)
      '
    Case "ARBURG"
      ApAssVRad = Atn(Tan(ApViteRad) / Cos(BetaIngRad))    'ALFA trasv. (RAD.)
      ApAssV = ApAssVRad * (180 / PG)
      Rprim = MnormV * NFil / 2 / Cos(BetaIngRad)          'R primitivo
      Betab = Atn(Tan(BetaIngRad) * Cos(ApAssVRad))        'Elica base
      Rb = Rprim * Cos(ApAssVRad)                          'R base
      DbaseV = 2 * Rb                                      'DBaseV
      DprimV = 2 * Rprim                                   'D prim.
      Call VITI_ESTERNE_VISUALIZZA_PROFILO_ARBURG
      '
  End Select
  WRITE_DIALOG TipoVite & " -> " & "Pe=" & frmt(PassoElica, 8) & " Dp=" & frmt(DprimV, 6) & " Mn=" & frmt(MnormV, 8)
  '
Exit Sub

errVITI_ESTERNE_VISUALIZZA_PROFILO:
  WRITE_DIALOG "Sub VITI_ESTERNE_VISUALIZZA_PROFILO: ERROR -> " & Err
  Exit Sub

End Sub

Sub VITI_ESTERNE_VISUALIZZA_PROFILO_ARBURG()
'
'VARIE
Dim i     As Long
Dim dALFA As Double
Dim ALFA  As Double
Dim Beta  As Double
Dim BASE  As Double
'
'FIANCO 1
Dim R41   As Double
Dim R43   As Double
Dim R44   As Double
Dim R46   As Double
Dim R48   As Double
Dim R49   As Double
'FIANCO 2
Dim R51   As Double
Dim R53   As Double
Dim R54   As Double
Dim R56   As Double
Dim R58   As Double
Dim R59   As Double
'
Dim XX(0, 99) As Double
Dim YY(0, 99) As Double
Dim RR(0, 99) As Double
'
Dim SCARTO            As Double
Dim SCARTO_PRECEDENTE As Double
Dim XXp1              As Double
Dim YYp1              As Double
Dim RExtTEMP          As Double
Dim RIntTEMP          As Double
Dim ATTF2             As Double
Dim ATTF1             As Double
Dim ATFF2             As Double
Dim ATFF1             As Double
'
On Error GoTo errVITI_ESTERNE_VISUALIZZA_PROFILO_ARBURG
  '
  If (RaggioFiancoV <= 0) Then RaggioFiancoV = 50
  '
  'FIANCO 1
  R41 = ApVite               'ANGOLO SUL FIANCO 1
  R43 = (DEXTV - DprimV) / 2 'ADDENDUM 1
  R44 = (DEXTV - DIntV) / 2  'ALTEZZA PROFILO 1
  R46 = RaggioFiancoV        'RAGGIO SUL FIANCO 1
  R48 = RT                   'RAGGIO DI TESTA 1
  R49 = RF                   'RAGGIO DI FONDO 1
  'FIANCO 2
  R51 = ApVite               'ANGOLO SUL FIANCO 2
  R53 = (DEXTV - DprimV) / 2 'ADDENDUM 2
  R54 = (DEXTV - DIntV) / 2  'ALTEZZA PROFILO 2
  R56 = RaggioFiancoV        'RAGGIO SUL FIANCO 2
  R58 = RT                   'RAGGIO DI TESTA 2
  R59 = RF                   'RAGGIO DI FONDO 2
  '
  'CONVERSIONE GRADI --> RADIANTI
  R41 = R41 / 180 * PG
  'CONVERSIONE GRADI --> RADIANTI
  R51 = R51 / 180 * PG
  '
  'FIANCO 2
  'CERCHIO SUL FIANCO
  XX(0, 51) = SpNormV / 2 + R56 * Cos(R51)
  YY(0, 51) = DprimV / 2 - R56 * Sin(R51)
  '
  'CERCHIO DI TESTA
  XX(0, 52) = SpNormV / 2 + R58 * Cos(R51)
  YY(0, 52) = DprimV / 2 - R58 * Sin(R51)
  RExtTEMP = YY(0, 52) + R58
  ALFA = R51: dALFA = 0.00001: i = 0
  Do
    ATTF2 = ALFA
    XX(0, 52) = XX(0, 51) - (R56 - R58) * Cos(ALFA)
    YY(0, 52) = YY(0, 51) + (R56 - R58) * Sin(ALFA)
    RExtTEMP = YY(0, 52) + R58
    ALFA = ALFA + dALFA
  Loop While (RExtTEMP < DEXTV / 2)
  'VALORE PRECEDENTE
  ATTF2 = ATTF2 - dALFA
  XX(0, 52) = XX(0, 51) - (R56 - R58) * Cos(ATTF2)
  YY(0, 52) = YY(0, 51) + (R56 - R58) * Sin(ATTF2)
  'PUNTO TANGENTE SUL FIANCO
  XX(0, 62) = XX(0, 51) - R56 * Cos(ATTF2)
  YY(0, 62) = YY(0, 51) + R56 * Sin(ATTF2)
  'PUNTO SUL DIAMETRO ESTERNO
  XX(0, 60) = XX(0, 52)
  YY(0, 60) = YY(0, 52) + R58
  '
  'CERCHIO DI FONDO
  XX(0, 53) = SpNormV / 2 - R59 * Cos(R51)
  YY(0, 53) = DprimV / 2 + R59 * Sin(R51)
  RIntTEMP = YY(0, 53) - R59
  ALFA = R51: dALFA = 0.00001: i = 0
  Do
    ATFF2 = ALFA
    XX(0, 53) = XX(0, 51) - (R56 + R59) * Cos(ALFA)
    YY(0, 53) = YY(0, 51) + (R56 + R59) * Sin(ALFA)
    RIntTEMP = YY(0, 53) - R59
    ALFA = ALFA - dALFA
  Loop While (RIntTEMP > DIntV / 2)
  'VALORE PRECEDENTE
  ATFF2 = ATFF2 + dALFA
  XX(0, 53) = XX(0, 51) - (R56 + R59) * Cos(ATFF2)
  YY(0, 53) = YY(0, 51) + (R56 + R59) * Sin(ATFF2)
  'PUNTO TANGENTE SUL FIANCO
  XX(0, 63) = XX(0, 51) - R56 * Cos(ATFF2)
  YY(0, 63) = YY(0, 51) + R56 * Sin(ATFF2)
  'PUNTO SUL DIAMETRO INTERNO
  XX(0, 61) = XX(0, 53)
  YY(0, 61) = YY(0, 53) - R59
  '
  'FIANCO 1
  'CERCHIO SUL FIANCO
  XX(0, 11) = -SpNormV / 2 - R46 * Cos(R41)
  YY(0, 11) = DprimV / 2 - R46 * Sin(R41)
  '
  'CERCHIO DI TESTA
  XX(0, 12) = SpNormV / 2 - R48 * Cos(R41)
  YY(0, 12) = DprimV / 2 - R48 * Sin(R41)
  RExtTEMP = YY(0, 52) + R48
  ALFA = R51: dALFA = 0.00001: i = 0
  Do
    ATTF1 = ALFA
    XX(0, 12) = XX(0, 11) + (R46 - R48) * Cos(ALFA)
    YY(0, 12) = YY(0, 11) + (R46 - R48) * Sin(ALFA)
    RExtTEMP = YY(0, 12) + R48
    ALFA = ALFA + dALFA
  Loop While (RExtTEMP < DEXTV / 2)
  'VALORE PRECEDENTE
  ATTF1 = ATTF1 - dALFA
  XX(0, 12) = XX(0, 11) + (R46 - R48) * Cos(ATTF1)
  YY(0, 12) = YY(0, 11) + (R46 - R48) * Sin(ATTF1)
  'PUNTO TANGENTE SUL FIANCO
  XX(0, 22) = XX(0, 11) + R46 * Cos(ATTF1)
  YY(0, 22) = YY(0, 11) + R46 * Sin(ATTF1)
  'PUNTO SUL DIAMETRO ESTERNO
  XX(0, 20) = XX(0, 12)
  YY(0, 20) = YY(0, 12) + R48
  '
  'CERCHIO DI FONDO
  XX(0, 13) = -SpNormV / 2 + R49 * Cos(R41)
  YY(0, 13) = DprimV / 2 + R49 * Sin(R41)
  RIntTEMP = YY(0, 13) - R49
  ALFA = R41: dALFA = 0.00001: i = 0
  Do
    ATFF1 = ALFA
    XX(0, 13) = XX(0, 11) + (R46 + R49) * Cos(ALFA)
    YY(0, 13) = YY(0, 11) + (R46 + R49) * Sin(ALFA)
    RIntTEMP = YY(0, 13) - R49
    ALFA = ALFA - dALFA
  Loop While (RIntTEMP > DIntV / 2)
  'VALORE PRECEDENTE
  ATFF1 = ATFF1 + dALFA
  XX(0, 13) = XX(0, 11) + (R46 + R49) * Cos(ATFF1)
  YY(0, 13) = YY(0, 11) + (R46 + R49) * Sin(ATFF1)
  'PUNTO TANGENTE SUL FIANCO
  XX(0, 23) = XX(0, 11) + R46 * Cos(ATFF1)
  YY(0, 23) = YY(0, 11) + R46 * Sin(ATFF1)
  'PUNTO SUL DIAMETRO INTERNO
  XX(0, 21) = XX(0, 13)
  YY(0, 21) = YY(0, 13) - R49
  '
  Dim AF            As Double
  Dim DF            As Double
  Dim NumeroRette   As Integer
  Dim NumeroArchi   As Integer
  Dim TipoCarattere As String
  Dim Scala         As Double
  Dim ScalaX        As Double
  Dim ScalaY        As Double
  Dim stmp          As String
  Dim USCITA        As Control
  Dim PicW          As Double
  Dim PicH          As Double
  Dim ZeroPezzoX    As Double
  Dim ZeroPezzoY    As Double
  Dim pX1           As Double
  Dim pY1           As Double
  Dim pX2           As Double
  Dim pY2           As Double
  Dim COLORE        As Integer
  '
  Set USCITA = OEMX.OEM1PicCALCOLO
  '
  DF = val(GetInfo("DIS0", "DF", Path_LAVORAZIONE_INI))
  AF = val(GetInfo("DIS0", "AF", Path_LAVORAZIONE_INI))
  NumeroRette = val(GetInfo("DIS0", "NumeroRette", Path_LAVORAZIONE_INI))
  NumeroArchi = val(GetInfo("DIS0", "NumeroArchi", Path_LAVORAZIONE_INI))
  '
  If DF = 0 Then DF = 2
  If AF = 0 Then AF = 30
  If NumeroRette = 0 Then NumeroRette = 20
  If NumeroArchi = 0 Then NumeroArchi = 50
  '
  TipoCarattere = GetInfo("DIS0", "TextFont", Path_LAVORAZIONE_INI)
  If (LINGUA = "CH") Then TipoCarattere = "MS Song"
  If (LINGUA = "RU") Then TipoCarattere = "Arial Cyr"
  If (Len(TipoCarattere) <= 0) Then TipoCarattere = "Arial"
  '
  stmp = GetInfo("DIS0", "ScalaMOLA", Path_LAVORAZIONE_INI)
  Scala = val(stmp)
  If (Scala <= 0) Then Scala = 1
  ScalaX = Scala: ScalaY = Scala
  'Acquisizione area grafica
  stmp = GetInfo("DIS0", "PicW", Path_LAVORAZIONE_INI):  If Len(stmp) > 0 Then PicW = val(stmp) Else PicW = 190
  stmp = GetInfo("DIS0", "PicH", Path_LAVORAZIONE_INI):  If Len(stmp) > 0 Then PicH = val(stmp) Else PicH = 95
  '
  USCITA.Cls
  '
  USCITA.ScaleWidth = PicW
  USCITA.ScaleHeight = PicH
  For i = 1 To 2
    USCITA.FontName = TipoCarattere
    USCITA.FontSize = 8
    USCITA.FontBold = False
  Next i
  USCITA.Refresh
  '
  USCITA.Line (PicW / 2, 0)-(PicW / 2, PicH), QBColor(0)
  USCITA.Line (0, PicH / 2)-(PicW, PicH / 2), QBColor(0)
  '
  ZeroPezzoY = DprimV / 2
  '
  'FIANCO 1
  COLORE = 12
  XXp1 = XX(0, 20): YYp1 = YY(0, 20): GoSub DISEGNA_PUNTO
  XXp1 = XX(0, 22): YYp1 = YY(0, 22): GoSub DISEGNA_PUNTO
  XXp1 = XX(0, 23): YYp1 = YY(0, 23): GoSub DISEGNA_PUNTO
  XXp1 = XX(0, 21): YYp1 = YY(0, 21): GoSub DISEGNA_PUNTO
  'ARCO DI TESTA
  COLORE = 0
  dALFA = ATTF1 / NumeroRette
  For i = 0 To NumeroRette - 1
    ALFA = dALFA * i
    pX1 = XX(0, 12) + R48 * Sin(ALFA)
    pY1 = YY(0, 12) + R48 * Cos(ALFA)
    ALFA = dALFA * (i + 1)
    pX2 = XX(0, 12) + R48 * Sin(ALFA)
    pY2 = YY(0, 12) + R48 * Cos(ALFA)
    pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
    USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
  Next i
  'ARCO DI FIANCO
  COLORE = 0
  XXp1 = XX(0, 22) - XX(0, 11)
  Beta = ASIN(XXp1 / R46)
  BASE = Sqr((XX(0, 22) - XX(0, 23)) ^ 2 + (YY(0, 22) - YY(0, 23)) ^ 2)
  ALFA = 2 * ASIN(BASE / (2 * R46))
  dALFA = ALFA / NumeroRette
  For i = 0 To NumeroRette - 1
    ALFA = Beta + dALFA * i
    pX1 = XX(0, 11) + R46 * Sin(ALFA)
    pY1 = YY(0, 11) + R46 * Cos(ALFA)
    ALFA = Beta + dALFA * (i + 1)
    pX2 = XX(0, 11) + R46 * Sin(ALFA)
    pY2 = YY(0, 11) + R46 * Cos(ALFA)
    pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
    USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
  Next i
  'ARCO DI FONDO
  COLORE = 0
  dALFA = ATFF1 / NumeroRette
  For i = 0 To NumeroRette - 1
    ALFA = dALFA * i
    pX1 = XX(0, 13) - R49 * Sin(ALFA)
    pY1 = YY(0, 13) - R49 * Cos(ALFA)
    ALFA = dALFA * (i + 1)
    pX2 = XX(0, 13) - R49 * Sin(ALFA)
    pY2 = YY(0, 13) - R49 * Cos(ALFA)
    pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
    USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
  Next i
  '
  'FIANCO 2
  COLORE = 0
  XXp1 = XX(0, 60): YYp1 = YY(0, 60): GoSub DISEGNA_PUNTO
  XXp1 = XX(0, 61): YYp1 = YY(0, 61): GoSub DISEGNA_PUNTO
  XXp1 = XX(0, 62): YYp1 = YY(0, 62): GoSub DISEGNA_PUNTO
  XXp1 = XX(0, 63): YYp1 = YY(0, 63): GoSub DISEGNA_PUNTO
  'ARCO DI TESTA
  COLORE = 0
  dALFA = ATTF2 / NumeroRette
  For i = 0 To NumeroRette - 1
    ALFA = dALFA * i
    pX1 = XX(0, 52) - R58 * Sin(ALFA)
    pY1 = YY(0, 52) + R58 * Cos(ALFA)
    ALFA = dALFA * (i + 1)
    pX2 = XX(0, 52) - R58 * Sin(ALFA)
    pY2 = YY(0, 52) + R58 * Cos(ALFA)
    pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
    USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
  Next i
  'ARCO DI FIANCO
  COLORE = 0
  XXp1 = XX(0, 51) - XX(0, 62)
  Beta = ASIN(XXp1 / R56)
  BASE = Sqr((XX(0, 62) - XX(0, 63)) ^ 2 + (YY(0, 62) - YY(0, 63)) ^ 2)
  ALFA = 2 * ASIN(BASE / (2 * R56))
  dALFA = ALFA / NumeroRette
  For i = 0 To NumeroRette - 1
    ALFA = Beta + dALFA * i
    pX1 = XX(0, 51) - R56 * Sin(ALFA)
    pY1 = YY(0, 51) + R56 * Cos(ALFA)
    ALFA = Beta + dALFA * (i + 1)
    pX2 = XX(0, 51) - R56 * Sin(ALFA)
    pY2 = YY(0, 51) + R56 * Cos(ALFA)
    pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
    USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
  Next i
  'ARCO DI FONDO
  COLORE = 0
  dALFA = ATFF2 / NumeroRette
  For i = 0 To NumeroRette - 1
    ALFA = dALFA * i
    pX1 = XX(0, 53) + R59 * Sin(ALFA)
    pY1 = YY(0, 53) - R59 * Cos(ALFA)
    ALFA = dALFA * (i + 1)
    pX2 = XX(0, 53) + R59 * Sin(ALFA)
    pY2 = YY(0, 53) - R59 * Cos(ALFA)
    pX1 = (pX1 - ZeroPezzoX) * ScalaX + PicW / 2
    pY1 = -(pY1 - ZeroPezzoY) * ScalaY + PicH / 2
    pX2 = (pX2 - ZeroPezzoX) * ScalaX + PicW / 2
    pY2 = -(pY2 - ZeroPezzoY) * ScalaY + PicH / 2
    USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
  Next i
  '
Exit Sub
    
errVITI_ESTERNE_VISUALIZZA_PROFILO_ARBURG:
  WRITE_DIALOG "SUB : VITI_ESTERNE_VISUALIZZA_PROFILO_ARBURG " & Error(Err)
  Exit Sub

DISEGNA_PUNTO:
  pX1 = (XXp1 + 0.1 - ZeroPezzoX) * ScalaX + PicW / 2
  pY1 = -(YYp1 - ZeroPezzoY) * ScalaY + PicH / 2
  pX2 = (XXp1 - 0.1 - ZeroPezzoX) * ScalaX + PicW / 2
  pY2 = -(YYp1 - ZeroPezzoY) * ScalaY + PicH / 2
  USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
  pX1 = (XXp1 - ZeroPezzoX) * ScalaX + PicW / 2
  pY1 = -(YYp1 + 0.1 - ZeroPezzoY) * ScalaY + PicH / 2
  pX2 = (XXp1 - ZeroPezzoX) * ScalaX + PicW / 2
  pY2 = -(YYp1 - 0.1 - ZeroPezzoY) * ScalaY + PicH / 2
  USCITA.Line (pX1, pY1)-(pX2, pY2), QBColor(COLORE)
Return

End Sub
