VERSION 5.00
Begin VB.Form OPTest 
   Caption         =   "OPTest"
   ClientHeight    =   3780
   ClientLeft      =   120
   ClientTop       =   456
   ClientWidth     =   6912
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   3780
   ScaleWidth      =   6912
   Begin VB.TextBox Text1 
      Height          =   285
      Left            =   600
      TabIndex        =   15
      Top             =   2640
      Width           =   615
   End
   Begin VB.CommandButton btnEND 
      Caption         =   "END"
      Height          =   495
      Left            =   240
      TabIndex        =   14
      Top             =   1200
      Width           =   735
   End
   Begin VB.TextBox txtStep 
      Height          =   375
      Left            =   2880
      TabIndex        =   13
      Text            =   "10"
      Top             =   2880
      Width           =   615
   End
   Begin VB.CommandButton Command10 
      Height          =   375
      Left            =   4560
      TabIndex        =   12
      Top             =   1800
      Width           =   375
   End
   Begin VB.CommandButton Command9 
      Height          =   375
      Left            =   3360
      TabIndex        =   11
      Top             =   1800
      Width           =   375
   End
   Begin VB.CommandButton Command8 
      Height          =   375
      Left            =   3960
      TabIndex        =   10
      Top             =   2280
      Width           =   375
   End
   Begin VB.CommandButton Command7 
      Height          =   375
      Left            =   3960
      TabIndex        =   9
      Top             =   1320
      Width           =   375
   End
   Begin VB.CommandButton Command6 
      Height          =   375
      Left            =   2520
      TabIndex        =   8
      Top             =   1800
      Width           =   375
   End
   Begin VB.CommandButton Command5 
      Height          =   375
      Left            =   1320
      TabIndex        =   7
      Top             =   1800
      Width           =   375
   End
   Begin VB.CommandButton Command4 
      Height          =   375
      Left            =   1920
      TabIndex        =   6
      Top             =   2280
      Width           =   375
   End
   Begin VB.CommandButton Command3 
      Height          =   375
      Left            =   1920
      TabIndex        =   5
      Top             =   1320
      Width           =   375
   End
   Begin VB.Frame Frame1 
      Caption         =   "Frame1"
      Height          =   855
      Left            =   240
      TabIndex        =   0
      Top             =   240
      Width           =   6615
      Begin VB.CommandButton Command2 
         Caption         =   "Scrivi"
         Height          =   255
         Left            =   5640
         TabIndex        =   4
         Top             =   360
         Width           =   735
      End
      Begin VB.TextBox txtVar 
         Height          =   285
         Left            =   240
         TabIndex        =   3
         Text            =   "/channel/parameter/r[u1,1]"
         Top             =   360
         Width           =   3015
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Leggi"
         Height          =   255
         Left            =   4800
         TabIndex        =   2
         Top             =   360
         Width           =   735
      End
      Begin VB.TextBox txtVal 
         Height          =   285
         Left            =   3480
         TabIndex        =   1
         Top             =   360
         Width           =   1095
      End
   End
End
Attribute VB_Name = "OPTest"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub btnEND_Click()
  
  End

End Sub

Private Sub Command1_Click()

  txtVal = SuOP.OPC_LEGGI_DATO(txtVar)
  
End Sub

Private Sub Command10_Click()
  
  MDIForm1.MeWidth = MDIForm1.MeWidth + val(txtStep)
  Call WritePrivateProfileString("GAPP4PC", "Width", MDIForm1.MeWidth, PathFILEINIOEM)
  MDIForm1.GoToAPP
  
End Sub

Private Sub Command2_Click()

  Call SuOP.OPC_SCRIVI_DATO(txtVar, txtVal)

End Sub

Private Sub Command3_Click()

  MDIForm1.MeTOP = MDIForm1.MeTOP - val(txtStep)
  Call WritePrivateProfileString("GAPP4PC", "y", MDIForm1.MeTOP, PathFILEINIOEM)
  MDIForm1.GoToAPP
  
End Sub

Private Sub Command4_Click()
  
  MDIForm1.MeTOP = MDIForm1.MeTOP + val(txtStep)
  Call WritePrivateProfileString("GAPP4PC", "y", MDIForm1.MeTOP, PathFILEINIOEM)
  MDIForm1.GoToAPP
  
End Sub

Private Sub Command5_Click()
  
  MDIForm1.MeLeft = MDIForm1.MeLeft - val(txtStep)
  Call WritePrivateProfileString("GAPP4PC", "x", MDIForm1.MeLeft, PathFILEINIOEM)
  MDIForm1.GoToAPP
  
End Sub

Private Sub Command6_Click()
    
  MDIForm1.MeLeft = MDIForm1.MeLeft + val(txtStep)
  Call WritePrivateProfileString("GAPP4PC", "x", MDIForm1.MeLeft, PathFILEINIOEM)
  MDIForm1.GoToAPP
  
End Sub

Private Sub Command7_Click()
  
  MDIForm1.MeHeight = MDIForm1.MeHeight + val(txtStep)
  Call WritePrivateProfileString("GAPP4PC", "Height", MDIForm1.MeHeight, PathFILEINIOEM)
  MDIForm1.GoToAPP
  
End Sub

Private Sub Command8_Click()
  
  MDIForm1.MeHeight = MDIForm1.MeHeight - val(txtStep)
  Call WritePrivateProfileString("GAPP4PC", "Height", MDIForm1.MeHeight, PathFILEINIOEM)
  MDIForm1.GoToAPP
  
End Sub

Private Sub Command9_Click()
  
  MDIForm1.MeWidth = MDIForm1.MeWidth - val(txtStep)
  Call WritePrivateProfileString("GAPP4PC", "Width", MDIForm1.MeWidth, PathFILEINIOEM)
  MDIForm1.GoToAPP
  
End Sub

Private Sub Form_GotFocus()
  
  Call SetFocusFRM

End Sub

Private Sub Text1_KeyUp(KeyCode As Integer, Shift As Integer)
  
  MsgBox ("Shift: " & Shift & vbCrLf & "Keycode: " & KeyCode)

End Sub
