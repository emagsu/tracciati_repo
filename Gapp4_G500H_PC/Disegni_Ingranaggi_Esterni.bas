Attribute VB_Name = "Disegni_Ingranaggi_Esterni"
Option Explicit

Public Type DatiCalcolo
  MolaX() As Double
  MolaY() As Double
  IngrX() As Double
  IngrY() As Double
  LCont() As Double
  DCont() As Double
  DB As Double
  DP As Double
  DI As Double
  DE As Double
  DIOtt As Double
  Np As Integer
End Type


'Public Type DatiFianchi
'  XmF1() As Double
'  YmF1() As Double
'  XmF2() As Double
'  YmF2() As Double
'End Type

Public CalcoloEsterni As DatiCalcolo
'Public PuntiMola As DatiFianchi
'Public PuntiIngr As DatiFianchi


'---------------------------------------------------------------------------------------
' Procedura : CALCOLO_INGRANAGGIO_ESTERNO_380
' Data e ora: 28/06/2007 14:27
' Autore    :
' Note      : Lettura parametri dal DB e non dalle liste
'---------------------------------------------------------------------------------------

Public Sub CALCOLO_INGRANAGGIO_ESTERNO_380_NEW(ByVal SiStampa As Integer)
'
Dim stmpCal As String
  '
  stmpCal = GetInfo("INGR380_ESTERNI", "Calcolo_De", Path_LAVORAZIONE_INI)
  If (stmpCal <> "") Then
    Dim Calcolo_De As Boolean
    stmpCal = Trim(stmpCal)
    Calcolo_De = IIf(stmpCal = "1", True, False)
    If Calcolo_De Then
      Call CALCOLO_INGRANAGGIO_ESTERNO_DE_380(SiStampa)
      Exit Sub
    End If
  End If
  '
Dim DBB As Database
Dim RSS As Recordset
Dim Np As Integer
Dim NpC As Integer
'
Dim ALFAn       As Double
Dim Beta        As Double
Dim AM          As Double
Dim Z           As Double
Dim DEm         As Double
Dim D1          As Double
Dim D2          As Double
Dim S0Nn        As Double
Dim W           As Double
Dim N           As Double
Dim Q           As Double
Dim DR          As Double
Dim RF          As Double
Dim DIint       As Double
Dim DIPU        As Double
Dim Valutazione As Integer
Dim BETAmG      As Double
'
Dim AngR         As Double
Dim CorrSpessore As Double
Dim FHaF(2)      As Double
'
Dim Tif(2, 3)           As Double
Dim Rastremazione(2, 3) As Double
Dim Bombatura(2, 3)     As Double
'
Dim stmp  As String
Dim stmp1 As String
Dim stmp2 As String
Dim i     As Integer
Dim j     As Integer
Dim jj    As Integer
Dim Val1  As Single
Dim Val2  As Single
'
Dim SAPmin  As Double
Dim EAPmin  As Double
Dim DIPUmin As Double
'
Dim NPR1  As Integer
'
Dim BETAmR As Double
Dim ALFAt As Double
Dim F3    As Double
Dim Rp    As Double
Dim Rb    As Double
Dim PB    As Double
Dim DB    As Double
Dim DP    As Double
Dim Betab As Double
Dim MM0   As Double
Dim MM1   As Double
Dim MM2   As Double
'
Dim IL    As Double
Dim S0Nt  As Double
Dim Sb    As Double
Dim iB    As Double
'
Dim Correzione As Double
'
Dim E1    As Double
Dim H2    As Double
Dim H3    As Double
Dim F2    As Double
'
Dim MM() As Double
Dim CT() As Double
Dim CE() As Double
Dim CB() As Double
Dim CK() As Double
'
Dim InvAx  As Double
'
Dim Ax  As Double
Dim Sx  As Double
Dim VX  As Double
Dim RX  As Double
Dim X0  As Double
Dim Y0  As Double
Dim T0  As Double
Dim FI0 As Double
Dim R0  As Double
'
Dim L()     As Double
Dim B()     As Double
Dim a()     As Double
Dim D()     As Double
Dim AlfaM() As Double
Dim API()   As Double
Dim x()     As Double
Dim Y()     As Double
Dim XD()    As Double
Dim YD()    As Double
'
Dim PAS     As Double
Dim Xi      As Double
Dim Xtr     As Double
Dim Yi      As Double
Dim Ytr     As Double
Dim Ri      As Double
Dim vi      As Double
'
Dim hu1     As Double
Dim HU2     As Double
Dim AngMed  As Double
Dim Sh      As Double
Dim PM      As Double
Dim B1      As Double
'
Dim dALFAm  As Double
Dim MMest   As Double
Dim MMp     As Double
'
Dim E17  As Integer
Dim E260 As Double
Dim E262 As Double
Dim E264 As Double
Dim E266 As Double
Dim E268 As Double
Dim E19  As Integer
Dim E261 As Double
Dim E263 As Double
Dim E265 As Double
Dim E267 As Double
Dim E269 As Double
'
Dim TanA As Double
'
Dim SIGMA As Double
Dim Rmin  As Double
'
Dim TP   As Double
Dim DRM  As Double
Dim RMPD As Double
Dim RQC  As Double
'
Dim AA  As Double
Dim BB  As Double
Dim cc  As Double
Dim RAD As Double
Dim T1  As Double
Dim T2  As Double
Dim ti  As Double
Dim TSI As Double
'
Dim fp1 As Double
Dim fp2 As Double
'
Dim XS  As Double
Dim Ys  As Double
Dim Zs  As Double
'
Dim Ap  As Double
Dim Bp  As Double
Dim Cp  As Double
Dim Lp  As Double
'
Dim ArgSeno As Double
Dim SINa2   As Double
'
Dim Uprof   As Double
Dim Wprof   As Double
Dim Vprof   As Double
Dim Rprof   As Double
'
Dim ENTRX   As Double
Dim inf     As Double
Dim EXF     As Double
Dim RXF     As Double
Dim APFD    As Double
Dim Apf     As Double
Dim Rayon   As Double
Dim ALFAs   As Double
Dim AngS    As Double
Dim BETAi   As Double
Dim Sens    As Double
'
Dim V7  As Double
Dim V12 As Double
Dim V13 As Double
Dim V17 As Double
Dim V14 As Double
Dim V15 As Double
Dim VPB As Double
Dim V16 As Double
'
Dim RXV As Double
Dim EXV As Double
Dim Ang As Double
'
Dim X1   As Double
Dim Y1   As Double
Dim Xapp As Double
Dim Yapp As Double
'
Dim Atmp  As String * 255
'
Dim dtmp1 As Double
Dim dtmp2 As Double
'
Dim BETAseg  As Integer
Dim Fianco   As Integer
Dim TextFont As String
Dim ret      As Integer
Dim RulloMax(2) As Double
'
Dim NomeTabellaCK   As String
Dim IndiceTabellaCK As Integer
Dim NomeTabellaCE   As String
Dim IndiceTabellaCE As Integer
'
On Error GoTo errCALCOLO_INGRANAGGIO_ESTERNO_380
  '
  Write_Dialog ""
  '
'  OEM1.OEM1PicCALCOLO.Cls
'  '
'  For i = 0 To 4
'    OEM1.txtINP(i).Visible = False
'    OEM1.lblINP(i).Visible = False
'  Next i
'  '
'  OEM1.OEM1chkINPUT(0).Visible = False
'  OEM1.OEM1chkINPUT(1).Visible = False
'  '
'  OEM1.OEM1lblOX.Visible = False
'  OEM1.OEM1lblOY.Visible = False
'  OEM1.OEM1txtOX.Visible = False
'  OEM1.OEM1txtOY.Visible = False
'  '
'  OEM1.OEM1Check2.Visible = False
'  OEM1.OEM1Command2.Visible = False
'  OEM1.Label2.Visible = False
'  OEM1.OEM1fraOpzioni.Visible = False
'  OEM1.OEM1frameCalcolo.Caption = ""
  '
  'Numero dei punti del raggio di testa della mola
  NPR1 = val(GetInfo("CALCOLO", "NumeroPuntiRaggio", PathFILEINI))
  DEm = LEP("iDiaMola")  'val(GetInfo("CALCOLO", "DiametroEsternoMola", PathFILEINI))
  '
  For Fianco = 1 To 2
    'PROFILO INGRANAGGIO
    Z = LEP("R[5]") 'val(OEM1.List1(1).List(0))
    AM = LEP("R[42]") 'val(OEM1.List1(1).List(1))
    ALFAn = LEP("R[40]") 'val(OEM1.List1(1).List(2))
    Beta = LEP("R[41]") 'val(OEM1.List1(1).List(3))
    D1 = LEP("R[43]") 'val(OEM1.List1(1).List(4))
    D2 = LEP("DEXT_G") 'val(OEM1.List1(1).List(5))
    DIint = LEP("R[45]") 'val(OEM1.List1(1).List(6))
    S0Nn = LEP("R[46]") 'val(OEM1.List1(1).List(7))
    W = LEP("R[47]") 'val(OEM1.List1(1).List(8))
    N = LEP("R[48]") 'val(OEM1.List1(1).List(9))
    Q = LEP("R[49]") 'val(OEM1.List1(1).List(10))
    DR = LEP("R[50]") 'val(OEM1.List1(1).List(11))
    RF = LEP("R[52]") 'val(OEM1.List1(1).List(13))
    DIPU = LEP("R[194]") 'val(OEM1.List1(1).List(14))
    Valutazione = LEP("R[901]") 'val(OEM1.List1(1).List(15))
    BETAmG = LEP("R[258]") 'val(GetInfo("CALCOLO", "InclinazioneMola", PathFILEINI))
    'CORREZIONI RACCORDO FONDO DENTE
    AngR = LEP("R[56]") 'val(OEM1.List2(2).List(1))
    CorrSpessore = LEP("CORSPED_G[0,1]") 'val(OEM1.List2(2).List(2))
    FHaF(1) = LEP("R[175]") 'val(OEM1.List2(2).List(3))
    FHaF(2) = LEP("R[178]") 'val(OEM1.List2(2).List(4))
    'CORREZIONI FIANCO 1
    Tif(1, 1) = LEP("R[270]") 'val(OEM1.List3(1).List(0))
    Rastremazione(1, 1) = LEP("R[271]") 'val(OEM1.List3(1).List(1))
    Bombatura(1, 1) = LEP("R[272]") 'val(OEM1.List3(1).List(2))
    Tif(1, 2) = LEP("R[273]") 'val(OEM1.List3(1).List(3))
    Rastremazione(1, 2) = LEP("R[274]") 'val(OEM1.List3(1).List(4))
    Bombatura(1, 2) = LEP("R[275]") 'val(OEM1.List3(1).List(5))
    Tif(1, 3) = LEP("R[276]") 'val(OEM1.List3(1).List(6))
    Rastremazione(1, 3) = LEP("R[277]") 'val(OEM1.List3(1).List(7))
    Bombatura(1, 3) = LEP("R[278]") 'val(OEM1.List3(1).List(8))
    'CORREZIONI FIANCO 2
    Tif(2, 1) = LEP("R[285]") 'val(OEM1.List3(2).List(0))
    Rastremazione(2, 1) = LEP("R[286]") 'val(OEM1.List3(2).List(1))
    Bombatura(2, 1) = LEP("R[287]") 'val(OEM1.List3(2).List(2))
    Tif(2, 2) = LEP("R[288]") 'val(OEM1.List3(2).List(3))
    Rastremazione(2, 2) = LEP("R[289]") 'val(OEM1.List3(2).List(4))
    Bombatura(2, 2) = LEP("R[290]") 'val(OEM1.List3(2).List(5))
    Tif(2, 3) = LEP("R[291]") 'val(OEM1.List3(2).List(6))
    Rastremazione(2, 3) = LEP("R[292]") 'val(OEM1.List3(2).List(7))
    Bombatura(2, 3) = LEP("R[293]") 'val(OEM1.List3(2).List(8))
    'TRASFORMAZIONE IN RADIANTI
    ALFAn = ALFAn * (PG / 180)
    'CONSIDERO UN INGRANAGGIO DRITTO CON UN ELICA PICCOLISSIMA
    If Beta = 0 Then Beta = (0.00001) * (PG / 180) Else Beta = Beta * (PG / 180)
    If BETAmG = 0 Then BETAmR = (0.00001) * (PG / 180) Else BETAmR = BETAmG * (PG / 180)
    ALFAt = Atn(Tan(ALFAn) / Cos(Beta))
    T1 = Tan(ALFAt)
    F3 = T1 - Atn(T1)                   'INVOLUTA(ALFAON)
    Rp = AM * Z / 2 / Cos(Beta)         'Raggio primitivo
    Betab = Atn(Tan(Beta) * Cos(ALFAt)) 'Elica di base
    Rb = Rp * Cos(ALFAt)                'Raggio di base
    PB = (Rb * 2 * PG) / Z              'Passo di base
    DB = 2 * Rb                         'Diametro di base
    DP = 2 * Rp                         'Diametro primitivo
    'SALVO IL SEGNO DI BETA
    If Beta < 0 Then BETAseg = -1 Else BETAseg = 1
    'CONSIDERO SOLO IL VALORE ASSOLUTO
    Beta = Abs(Beta)
    'CALCOLO DEL SAP MINIMO
    SAPmin = 2 * Sqr(Rb ^ 2 + DIPU ^ 2)
    If D1 < SAPmin Then
      Write_Dialog "D1 < SAPmin " & frmt(SAPmin, 4)
      ParametriOK = False
      Exit Sub
    End If
    MM1 = Sqr(D1 ^ 2 - DB ^ 2) / 2
    'CALCOLO DEL EAP MINIMO
    EAPmin = 2 * Sqr(Rb ^ 2 + (DIPU * 3 + MM1) ^ 2)
    If D2 < EAPmin Then
      Write_Dialog "D2 < EAPmin " & frmt(EAPmin, 4)
      ParametriOK = False
      Exit Sub
    End If
    MM2 = Sqr(D2 ^ 2 - DB ^ 2) / 2
    'CONTROLLO DISTANZA PUNTI MININA: max 50 per CNC
    DIPUmin = (MM2 - MM1) / 50
    If DIPU < DIPUmin Then
      Write_Dialog "DIPU < DIPUmin: max 50 X CNC " & frmt(DIPUmin, 4)
    End If
    'CALCOLO DEL S0Nn, W e Q
    If S0Nn <= 0 Then
      If W > 0 Then
        'TRASFORMAZIONE_W_SON
        iB = N * PB - W / Cos(Betab)      'Vano Base
        Sb = PB - iB                      'Spessore base
        S0Nt = Rp * 2 * (Sb / Rb / 2 - F3)
        S0Nn = S0Nt * Cos(Beta)
        S0Nn = Int(S0Nn * 10000 + 0.5) / 10000
      Else
        'TRASFORMAZIONE_Q_SON
        If Int(Z / 2) = Z / 2 Then E1 = 0 Else E1 = PG / 2 / Z
        H2 = AM * Z * Cos(ALFAt) * Cos(E1) / ((Q - DR) * Cos(Beta))
        H3 = Tan(FnARC(H2))
        F2 = (F3 + DR / (AM * Z * Cos(ALFAn)) - H3 + Atn(H3)) * AM * Z
        S0Nn = (PG * AM - F2)
      End If
    End If
    'UTILIZZO LA CORREZIONE DI SPESSORE
    S0Nn = S0Nn + CorrSpessore
    IL = (DEm + DIint) / 2
    S0Nt = S0Nn / Cos(Beta)
    Sb = Rb * (S0Nt / Rp + 2 * F3)
    iB = PB - Sb
    'CALCOLO IL PUNTO PRIMA DELL'INIZIO DEL PROFILO ATTIVO
    MM0 = MM1 - DIPU
    'CALCOLO DEL NUMERO DEI PUNTI
    Np = Int((MM2 - MM0) / DIPU) + 1 + 1
    'RICALCOLO DELLA FINE DEL PROFILO ATTIVO
    MM2 = MM0 + (Np - 1) * DIPU
    If (Fianco = 1) Then
      ReDim MM(Np)
      ReDim CT(2, Np): ReDim CE(2, 9): ReDim CB(2, Np): ReDim CK(2, Np)
      ReDim a(2, Np + NPR1 + 1): ReDim B(Np)
      ReDim L(2, Np + NPR1 + 1)
      ReDim D(Np + NPR1 + 1)
      ReDim AlfaM(2, Np + NPR1 + 1)
      ReDim API(Np + NPR1 + 1)
      ReDim x(2, Np + NPR1 + 1)
      ReDim Y(2, Np + NPR1 + 1)
      ReDim XD(2, Np + NPR1 + 1)
      ReDim YD(2, Np + NPR1 + 1)
      'COSTRUZIONE DELLA TABELLA DELLE CORREZIONI DEI PUNTI
      NomeTabellaCK = GetInfo("CALCOLO", "NomeTab380CK", PathFILEINI)
      IndiceTabellaCK = val(GetInfo("CALCOLO", "IndiceTab380CK", PathFILEINI))
      If (Len(NomeTabellaCK) > 0) Then
        If (Np > 50) Then NpC = 50 Else NpC = Np
        Set DBB = OpenDatabase(PathDB, True, False)
        Set RSS = DBB.OpenRecordset(NomeTabellaCK)
        RSS.Index = "INDICE"
        For i = 1 To NpC
          RSS.Seek "=", IndiceTabellaCK + (i - 1)
          If Not RSS.NoMatch Then
            If Len(RSS.Fields("ACTUAL").Value) > 0 Then
              stmp1 = Split(RSS.Fields("ACTUAL").Value, ";")(0)
              stmp2 = Split(RSS.Fields("ACTUAL").Value, ";")(1)
            Else
              stmp1 = ""
              stmp2 = ""
            End If
          Else
            stmp1 = ""
            stmp2 = ""
          End If
          CK(1, i) = val(stmp1)
          CK(2, i) = val(stmp2)
        Next i
        RSS.Close
        DBB.Close
      End If
      'COSTRUZIONE DELLA TABELLA DELLE CORREZIONI ELICA
      NomeTabellaCE = GetInfo("CALCOLO", "NomeTab380CE", PathFILEINI)
      IndiceTabellaCE = val(GetInfo("CALCOLO", "IndiceTab380CE", PathFILEINI))
      If (Len(NomeTabellaCE) > 0) Then
        Set DBB = OpenDatabase(PathDB, True, False)
        Set RSS = DBB.OpenRecordset(NomeTabellaCE)
        RSS.Index = "INDICE"
        For i = 1 To 9
          RSS.Seek "=", IndiceTabellaCE + (i - 1)
          If Not RSS.NoMatch Then
            If Len(RSS.Fields("ACTUAL").Value) > 0 Then
              stmp1 = Split(RSS.Fields("ACTUAL").Value, ";")(0)
              stmp2 = Split(RSS.Fields("ACTUAL").Value, ";")(1)
            Else
              stmp1 = ""
              stmp2 = ""
            End If
          Else
            stmp1 = ""
            stmp2 = ""
          End If
          CE(1, i) = val(stmp1)
          CE(2, i) = val(stmp2)
        Next i
        RSS.Close
        DBB.Close
      End If
    End If
    'CONVERSIONE TIF/DIA IN FUNZIONE DELLA VALUTAZIONE
    If Valutazione = 2 Then
      If Tif(Fianco, 1) > DB Then Tif(Fianco, 1) = Sqr(Tif(Fianco, 1) ^ 2 / 4 - Rb ^ 2)
      If Tif(Fianco, 2) > DB Then Tif(Fianco, 2) = Sqr(Tif(Fianco, 2) ^ 2 / 4 - Rb ^ 2)
      If Tif(Fianco, 3) > DB Then Tif(Fianco, 3) = Sqr(Tif(Fianco, 3) ^ 2 / 4 - Rb ^ 2)
    End If
    GoSub CALCOLO_DELLE_CORREZIONI_PROFILO_K
    '
    'CALCOLO MOLA x FIANCO
    For i = 1 To Np
      'SVILUPPO LINEARE DEL PROFILO INGRANAGGIO
      MM(i) = MM0 + DIPU * (Np - i)
      'CORREZIONE EVOLVENTE
      Correzione = CB(Fianco, Np - i)                                  'PROFILO K
      Correzione = Correzione + CK(Fianco, Np - i)                     'PUNTI
      Correzione = Correzione + FHaF(Fianco) * (Np - 1 - i) / (Np - 2) 'Fha
      CT(Fianco, Np - i) = Correzione
      GoSub CALCOLO_T0_FI0
      'ULTIMO PUNTO
      If i = Np Then X0 = RX * Cos(T0): Y0 = RX * Sin(T0): R0 = RX
      GoSub PUREA
      'CONVERSIONI
      L(Fianco, i) = (ti * PAS) / (PG * 2)
      D(i) = 2 * Sqr(MM(i) ^ 2 + Rb ^ 2)
      AlfaM(Fianco, i) = B(i) * (PG / 180)
      API(i) = Atn(MM(i) / Rb) * (180 / PG)
    Next i
    '
    'PRIMITIVO
    i = 0
    MM(i) = Sqr(Rp ^ 2 - Rb ^ 2)
    RX = Rp
    API(i) = (PG / 2 - Atn((Rb / Rp) / Sqr(1 - (Rb / Rp) ^ 2))) * 180 / PG
    Correzione = 0: GoSub CALCOLO_T0_FI0
    GoSub PUREA
    'CONVERSIONI
    L(Fianco, i) = (ti * PAS) / (PG * 2)
    D(i) = Rp * 2
    AlfaM(Fianco, i) = B(i) * (PG / 180)
    '
    'MODIFICA RACCORDO FONDO
    AlfaM(Fianco, Np) = AlfaM(Fianco, Np) + AngR * (PG / 180)
    Yi = IL - DIint / 2
    Ytr = Yi - RF * (1 - Sin(AlfaM(Fianco, Np)))
    If Y(Fianco, Np) < Ytr Then
      Xtr = x(Fianco, Np) - (Ytr - Y(Fianco, Np)) * Tan(AlfaM(Fianco, Np))
      Xi = Xtr - RF * Cos(AlfaM(Fianco, Np))
    End If
    If Y(Fianco, Np) >= Ytr Then
      Xtr = x(Fianco, Np)
      Ytr = Y(Fianco, Np)
      Yi = Y(Fianco, Np) + RF * (1 - Sin(AlfaM(Fianco, Np)))
      Xi = x(Fianco, Np) - RF * Cos(AlfaM(Fianco, Np))
    End If
    Ri = IL - Yi
    vi = Xi * 2
    hu1 = Ytr - Y(Fianco, 1)
    HU2 = hu1 + RF * (1 - Sin(AlfaM(Fianco, Np)))
    AngMed = Atn((x(Fianco, 1) - Xtr) / hu1)
    Sh = hu1 / Cos(AngMed) / 2
    PM = Int(Np / 2)
    B1 = Xtr - (Y(Fianco, PM) - Ytr) * Tan(AngMed) - x(Fianco, PM)
    B1 = B1 * Cos(AngMed)
    Rp = (Sh ^ 2 + B1 ^ 2) / 2 / B1
    '
    'RAGGIO DI FONDO
    dALFAm = (PG / 2 - AlfaM(Fianco, Np)) / (NPR1 - 1)
    'PROFILO INGRANAGGIO SEZ. TRASVERSALE
    For j = 0 To NPR1 - 1
      jj = Np + j + 1
      AlfaM(Fianco, jj) = AlfaM(Fianco, Np) + dALFAm * j
      x(Fianco, jj) = Xi + RF * Cos(AlfaM(Fianco, jj))
      Y(Fianco, jj) = Yi - RF * (1 - Sin(AlfaM(Fianco, jj)))
      GoSub 19000
    Next j
    '
    jj = Np + NPR1 + 1
    AlfaM(Fianco, jj) = PG / 2
    x(Fianco, jj) = 0
    Y(Fianco, jj) = Yi
    GoSub 19000: HU2 = Yi - Y(Fianco, 1)
    MMest = Sqr(D2 ^ 2 / 4 - Rb ^ 2)
    L(Fianco, jj) = 0
    '
    '-----------
    Call RMaxRullo(Fianco, Beta, MM0, Np, x, Y, RulloMax(Fianco))
    '
  Next Fianco
  '
  'CONTROLLO RISULTATI IMPORTANTI
  stmp = "WARNING: "
  'DIAMETRO INTERNO MINIMO
  If 2 * Ri - DIint < -0.0001 Then
      ret = LoadString(g_hLanguageLibHandle, 1208, Atmp, 255)
      If ret > 0 Then stmp = Left$(Atmp, ret) Else stmp = "Root Diameter"
      stmp = stmp & " ( = " & frmt(Ri * 2, 4) & " )"
      stmp = stmp & "  <  " & frmt(DIint, 4)
      Write_Dialog "WARNING:  " & stmp
  End If
  '
  'If 2 * Ri - DIint < -0.0001 Then
  '  'Diametro interno
  '  stmp1 = OEM1.List1(0).List(6) & " = "
  '  stmp = stmp & stmp1 & frmt(Ri * 2, 4)
  '  stmp = stmp & "  "
  'End If
  ''PIATTINO
  'If Vi < 0 Then
  '  'Piattino di testa mola
  '  stmp1 = "Piattino di testa mola" & " = "
  '  stmp = stmp & stmp1 & frmt(Vi, 4)
  'End If
  'SEGNALO QUANDO ALMENO UNA DELLE CONDIZIONI E' VERIFICATA
  'If Len(stmp) > 10 Then Write_Dialog stmp
  '
  If DIPU < DIPUmin Then
      Write_Dialog "DIPU < DIPUmin: max 50 X CNC " & frmt(DIPUmin, 4)
  End If
  '
  'SALVATAGGIO DEL CALCOLO SU FILE ASCII
  Open g_chOemPATH & "\DATI.TEO" For Output As #1
  For Fianco = 1 To 2
    Print #1, String(41, "-") & " F" & Format$(Fianco, "0") & " " & String(41, "-")
    Print #1, Left$("Nr" & String(6, " "), 6);
    Print #1, Left$("D " & String(10, " "), 10);
    Print #1, Left$("MM" & String(10, " "), 10);
    Print #1, Left$("Ap" & String(10, " "), 10);
    Print #1, Left$("Ra" & String(10, " "), 10);
    Print #1, Left$("Xm" & String(10, " "), 10);
    Print #1, Left$("Ym" & String(10, " "), 10);
    Print #1, Left$("Tm" & String(10, " "), 10);
    Print #1, Left$("Corr." & String(10, " "), 10)
    Print #1, String(6 + 8 * 10, "-")
    For i = 0 To Np + NPR1
      If i = 0 Then
        Print #1, Left$("DP(" & Format$(Fianco, "0") & ")" & String(6, " "), 6);
      ElseIf i <= Np Then
        Print #1, Left$(Format$(Np - i, "#####0") & String(6, " "), 6);
      Else
        Print #1, Left$(String(6, " "), 6);
      End If
      Print #1, Left$(frmt(D(i), 4) & String(10, " "), 10);
      If i <= Np Then
        Print #1, Left$(frmt(MM(i), 4) & String(10, " "), 10);
        Print #1, Left$(frmt(API(i), 4) & String(10, " "), 10);
        Print #1, Left$(frmt(Tan(API(i) * PG / 180) * 180 / PG, 4) & String(10, " "), 10);
      Else
        Print #1, Left$(String(10, " "), 10);
        Print #1, Left$(String(10, " "), 10);
        Print #1, Left$(String(10, " "), 10);
      End If
      Print #1, Left$(frmt(x(Fianco, i), 4) & String(10, " "), 10);
      Print #1, Left$(frmt(Y(Fianco, i), 4) & String(10, " "), 10);
      Print #1, Left$(frmt((AlfaM(Fianco, i) * (180 / PG)), 4) & String(10, " "), 10);
      If (i > 0) And (i <= Np) Then
        Print #1, Left$(frmt(CT(Fianco, Np - i), 4) & String(10, " "), 10)
      Else
        Print #1, Left$(String(10, " "), 10)
      End If
    Next i
    i = Np + NPR1 + 1
      Print #1, Left$(String(6, " "), 6);
      Print #1, Left$(frmt((IL - Y(Fianco, i - 1)) * 2, 4) & String(10, " "), 10);
      Print #1, Left(String(10, " "), 10);
      Print #1, Left(String(10, " "), 10);
      Print #1, Left(String(10, " "), 10);
      Print #1, Left$(frmt(0, 4) & String(10, " "), 10);
      Print #1, Left$(frmt(Y(Fianco, i - 1), 4) & String(10, " "), 10);
      Print #1, Left$(frmt((AlfaM(Fianco, i - 1) * (180 / PG)), 4) & String(10, " "), 10);
      Print #1, Left$(String(10, " "), 10)
    Next Fianco
  Close #1
  '
  'SALVATAGGIO DEL CALCOLO SU FILE ASCII
  Dim XX1           As Single
  Dim YY1           As Single
  Dim XX2           As Single
  Dim YY2           As Single
  Dim DISTANZA      As Single
  Dim ANGOLO        As Single
  Dim AngoloSoglia  As Single
  AngoloSoglia = val(GetInfo("CALCOLO", "AngoloSoglia", PathFILEINI))
  If AngoloSoglia = 0 Then AngoloSoglia = 80
  For Fianco = 1 To 2
    Open g_chOemPATH & "\MOLA_F" & Format$(Fianco, "0") & ".TEO" For Output As #1
    'Fianco della mola
    For i = 1 To Np
      If (AlfaM(Fianco, i) * (180 / PG)) <= AngoloSoglia Then
        Print #1, Left$(frmt(x(Fianco, i), 6) & String(20, " "), 20);
        Print #1, Left$(frmt(Y(Fianco, i), 6) & String(20, " "), 20);
        Print #1, Left$(frmt((AlfaM(Fianco, i) * (180 / PG)), 6) & String(20, " "), 20)
      End If
    Next i
    'Tratto rettilineo prima del raggio di testa
    i = Np        'ULTIMO PUNTO DEL PROFILO INGRANAGGIO DOPO IL SAP
    XX1 = x(Fianco, i): YY1 = Y(Fianco, i)
    i = Np + 1    'ULTIMO PUNTO DEL TRATTO RETTILINEO AGGIUNTO
    XX2 = x(Fianco, i): YY2 = Y(Fianco, i)
    DISTANZA = Sqr((XX2 - XX1) ^ 2 + (YY2 - YY1) ^ 2)
    ANGOLO = AlfaM(Fianco, i)
    For j = 1 To NPR1 - 1
      XX2 = XX1 - (DISTANZA / NPR1) * j * Sin(AlfaM(Fianco, i))
      YY2 = YY1 + (DISTANZA / NPR1) * j * Cos(AlfaM(Fianco, i))
      If (AlfaM(Fianco, i) * (180 / PG)) <= AngoloSoglia Then
        Print #1, Left$(frmt(XX2, 6) & String(20, " "), 20);
        Print #1, Left$(frmt(YY2, 6) & String(20, " "), 20);
        Print #1, Left$(frmt(ANGOLO * (180 / PG), 6) & String(20, " "), 20)
      End If
    Next j
    'Raggio di testa della mola
    For i = Np + 1 To Np + NPR1
      If (AlfaM(Fianco, i) * (180 / PG)) <= AngoloSoglia Then
        Print #1, Left$(frmt(x(Fianco, i), 6) & String(20, " "), 20);
        Print #1, Left$(frmt(Y(Fianco, i), 6) & String(20, " "), 20);
        Print #1, Left$(frmt((AlfaM(Fianco, i) * (180 / PG)), 6) & String(20, " "), 20)
      End If
    Next i
    Close #1
  Next Fianco
'  '
'  'Visualizzazione e/o stampa dei risultati --------------------------------------------------
  Dim pX1       As Single
  Dim pY1       As Single
  Dim pX2       As Single
  Dim pY2       As Single
  Dim Altezza   As Single
  Dim Larghezza As Single
  Dim ScalaX    As Single
  Dim ScalaY    As Single
  Dim Xmax(2)   As Single
  Dim Ymax(2)   As Single
  Dim Xmin(2)   As Single
  Dim Ymin(2)   As Single
  Dim PicH      As Single
  Dim PicW      As Single
  Dim DA        As Double


'  '
'  'LARGHEZZA
'  stmp = GetInfo("CALCOLO", "Larghezza", PathFILEINI)
'  If val(stmp) > 0 Then PicW = val(stmp) Else PicW = 190
'  OEM1.OEM1PicCALCOLO.ScaleWidth = PicW
'  'ALTEZZA
'  stmp = GetInfo("CALCOLO", "Altezza", PathFILEINI)
'  If val(stmp) > 0 Then PicH = val(stmp) Else PicH = 95
'  OEM1.OEM1PicCALCOLO.ScaleHeight = PicH
'  'CARATTERE DI STAMPA TESTO
'  TextFont = GetInfo("CALCOLO", "TextFont", PathFILEINI)
'  '
'  If (SiStampa = 1) Then
'    For i = 1 To 2
'      Printer.FontName = TextFont
'      Printer.FontSize = 12
'      Printer.FontBold = False
'    Next i
'  Else
'    For i = 1 To 2
'      OEM1.OEM1PicCALCOLO.FontName = TextFont
'      OEM1.OEM1PicCALCOLO.FontSize = 12
'      OEM1.OEM1PicCALCOLO.FontBold = False
'    Next i
'  End If
'  '
'  'SELEZIONE DEL TIPO DI GRAFICO
'  Select Case OEM1.OEM1Combo1.ListIndex
'    '
'    Case 0    '"MOLA"
'      OEM1.lblINP(0).Visible = True
'      OEM1.txtINP(0).Visible = True
'      OEM1.lblINP(1).Visible = False
'      OEM1.txtINP(1).Visible = False
'      GoSub ETICHETTE_GRAFICI
'      'Estremi dei fianchi
'      For Fianco = 1 To 2
'        Xmax(Fianco) = x(Fianco, 1)
'        Ymax(Fianco) = Y(Fianco, 1)
'        Xmin(Fianco) = x(Fianco, 1)
'        Ymin(Fianco) = Y(Fianco, 1)
'        For i = 1 To (Np + NPR1 + 1)
'          If x(Fianco, i) >= Xmax(Fianco) Then Xmax(Fianco) = x(Fianco, i)
'          If Y(Fianco, i) >= Ymax(Fianco) Then Ymax(Fianco) = Y(Fianco, i)
'          If x(Fianco, i) <= Xmin(Fianco) Then Xmin(Fianco) = x(Fianco, i)
'          If Y(Fianco, i) <= Ymin(Fianco) Then Ymin(Fianco) = Y(Fianco, i)
'        Next i
'      Next Fianco
'      GoSub Estremi_Massimi
'      ScalaX = val(GetInfo("CALCOLO", "ScalaMOLA", PathFILEINI))
'      ScalaY = ScalaX
'      OEM1.txtINP(0).Text = frmt(ScalaX, 4)
'      OEM1.txtINP(1).Text = frmt(ScalaY, 4)
'      'Fianco 1
'      stmp = "F1"
'      For i = 1 To (Np + NPR1 + 1) - 1
'        pX1 = -x(1, i) * ScalaX + PicW / 2
'        pY1 = -(Y(1, i) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
'        pX2 = -x(1, i + 1) * ScalaX + PicW / 2
'        pY2 = -(Y(1, i + 1) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
'        GoSub GRAFICO_MOLA_o_INGRANAGGIO_FIANCO
'      Next i
'      'Fianco 2
'      stmp = "F2"
'      For i = 1 To (Np + NPR1 + 1) - 1
'        pX1 = x(2, i) * ScalaX + PicW / 2
'        pY1 = -(Y(2, i) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
'        pX2 = x(2, i + 1) * ScalaX + PicW / 2
'        pY2 = -(Y(2, i + 1) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
'        GoSub GRAFICO_MOLA_o_INGRANAGGIO_FIANCO
'      Next i
'      pX1 = -Xmax(1) * ScalaX + PicW / 2
'      pY1 = -((Ymax(0) - Ymin(0)) / 2) * ScalaY + PicH / 2
'      pX2 = Xmax(2) * ScalaX + PicW / 2
'      pY2 = ((Ymax(0) - Ymin(0)) / 2) * ScalaY + PicH / 2
'      If (SiStampa = 1) Then
'        'Nome
'        stmp = "F1"
'        Printer.CurrentX = pX1 - Printer.TextWidth(stmp)
'        Printer.CurrentY = pY2 - Printer.TextHeight(stmp)
'        Printer.Print stmp
'        stmp = "F2"
'        Printer.CurrentX = pX2
'        Printer.CurrentY = pY2 - Printer.TextHeight(stmp)
'        Printer.Print stmp
'        'Larghezza mola: valore
'        stmp = "W=" & frmt(Xmax(1) + Xmax(2), 4) & "mm  "
'        Printer.CurrentX = PicW / 2 - Printer.TextWidth(stmp)
'        Printer.CurrentY = pY2 - Printer.TextHeight(stmp)
'        Printer.Print stmp
'        'Altezza mola: valore
'        stmp = " H=" & frmt(Ymax(0) - Ymin(0), 4) & "mm"
'        Printer.CurrentX = PicW / 2
'        Printer.CurrentY = pY2 - Printer.TextHeight(stmp)
'        Printer.Print stmp
'        Printer.DrawStyle = 4
'        'Larghezza mola: linea
'        Printer.Line (pX1, pY2)-(pX2, pY2)
'        'Altezza mola: linea
'        Printer.Line (PicW / 2, pY1)-(PicW / 2, pY2)
'        Printer.DrawStyle = 0
'      Else
'        'Nome
'        stmp = "F1"
'        OEM1.OEM1PicCALCOLO.CurrentX = pX1 - OEM1.OEM1PicCALCOLO.TextWidth(stmp)
'        OEM1.OEM1PicCALCOLO.CurrentY = pY2 - OEM1.OEM1PicCALCOLO.TextHeight(stmp)
'        OEM1.OEM1PicCALCOLO.Print stmp
'        stmp = "F2"
'        OEM1.OEM1PicCALCOLO.CurrentX = pX2
'        OEM1.OEM1PicCALCOLO.CurrentY = pY2 - OEM1.OEM1PicCALCOLO.TextHeight(stmp)
'        OEM1.OEM1PicCALCOLO.Print stmp
'        'Larghezza mola: valore
'        stmp = "W=" & frmt(Xmax(1) + Xmax(2), 4) & "mm  "
'        OEM1.OEM1PicCALCOLO.CurrentX = PicW / 2 - OEM1.OEM1PicCALCOLO.TextWidth(stmp)
'        OEM1.OEM1PicCALCOLO.CurrentY = pY2 - OEM1.OEM1PicCALCOLO.TextHeight(stmp)
'        OEM1.OEM1PicCALCOLO.Print stmp
'        'Altezza mola: valore
'        stmp = " H=" & frmt(Ymax(0) - Ymin(0), 4) & "mm"
'        OEM1.OEM1PicCALCOLO.CurrentX = PicW / 2
'        OEM1.OEM1PicCALCOLO.CurrentY = pY2 - OEM1.OEM1PicCALCOLO.TextHeight(stmp)
'        OEM1.OEM1PicCALCOLO.Print stmp
'        OEM1.OEM1PicCALCOLO.DrawStyle = 4
'        'Larghezza mola: linea
'        OEM1.OEM1PicCALCOLO.Line (pX1, pY2)-(pX2, pY2)
'        'Altezza mola: linea
'        OEM1.OEM1PicCALCOLO.Line (PicW / 2, pY1)-(PicW / 2, pY2)
'        OEM1.OEM1PicCALCOLO.DrawStyle = 0
'      End If
'      GoSub FineDocumento
'      '
'    Case 1    '"INGRANAGGIO"
'      OEM1.lblINP(0).Visible = True
'      OEM1.txtINP(0).Visible = True
'      OEM1.lblINP(1).Visible = False
'      OEM1.txtINP(1).Visible = False
'      GoSub ETICHETTE_GRAFICI
'      'Estremi dei fianchi
'      For Fianco = 1 To 2
'        Xmax(Fianco) = XD(Fianco, 1)
'        Ymax(Fianco) = YD(Fianco, 1)
'        Xmin(Fianco) = XD(Fianco, 1)
'        Ymin(Fianco) = YD(Fianco, 1)
'        For i = 1 To (Np + NPR1 + 1)
'          If XD(Fianco, i) >= Xmax(Fianco) Then Xmax(Fianco) = XD(Fianco, i)
'          If YD(Fianco, i) >= Ymax(Fianco) Then Ymax(Fianco) = YD(Fianco, i)
'          If XD(Fianco, i) <= Xmin(Fianco) Then Xmin(Fianco) = XD(Fianco, i)
'          If YD(Fianco, i) <= Ymin(Fianco) Then Ymin(Fianco) = YD(Fianco, i)
'        Next i
'      Next Fianco
'      GoSub Estremi_Massimi
'      ScalaX = val(GetInfo("CALCOLO", "ScalaINGRANAGGIO", PathFILEINI))
'      ScalaY = ScalaX
'      OEM1.txtINP(0).Text = frmt(ScalaX, 4)
'      OEM1.txtINP(1).Text = frmt(ScalaY, 4)
'      'Fianco 1
'      For i = 1 To (Np + NPR1 + 1) - 1
'        pX1 = -XD(1, i) * ScalaX + PicW / 2
'        pY1 = -(YD(1, i) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
'        pX2 = -XD(1, i + 1) * ScalaX + PicW / 2
'        pY2 = -(YD(1, i + 1) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
'        GoSub GRAFICO_MOLA_o_INGRANAGGIO_FIANCO
'      Next i
'      'Fianco 1
'      'ruotato di (2 * PG / Z) in senso orario rispetto all'asse
'      For i = 1 To (Np + NPR1 + 1) - 1
'        DA = -2 * PG / Z
'        pX1 = -(XD(1, i) * Cos(DA) + YD(1, i) * Sin(DA)) * ScalaX + PicW / 2
'        pY1 = -(YD(1, i) * Cos(DA) - XD(1, i) * Sin(DA) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
'        pX2 = -(XD(1, i + 1) * Cos(DA) + YD(1, i + 1) * Sin(DA)) * ScalaX + PicW / 2
'        pY2 = -(YD(1, i + 1) * Cos(DA) - XD(1, i + 1) * Sin(DA) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
'        GoSub GRAFICO_MOLA_o_INGRANAGGIO_FIANCO
'      Next i
'      'Fianco 2
'      For i = 1 To (Np + NPR1 + 1) - 1
'        pX1 = XD(2, i) * ScalaX + PicW / 2
'        pY1 = -(YD(2, i) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
'        pX2 = XD(2, i + 1) * ScalaX + PicW / 2
'        pY2 = -(YD(2, i + 1) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
'        GoSub GRAFICO_MOLA_o_INGRANAGGIO_FIANCO
'      Next i
'      'Fianco 2
'      'ruotato di (2 * PG / Z) in senso antiorario rispetto all'asse
'      For i = 1 To (Np + NPR1 + 1) - 1
'        DA = -2 * PG / Z
'        pX1 = (XD(2, i) * Cos(DA) + YD(2, i) * Sin(DA)) * ScalaX + PicW / 2
'        pY1 = -(YD(2, i) * Cos(DA) - XD(2, i) * Sin(DA) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
'        pX2 = (XD(2, i + 1) * Cos(DA) + YD(2, i + 1) * Sin(DA)) * ScalaX + PicW / 2
'        pY2 = -(YD(2, i + 1) * Cos(DA) - XD(2, i + 1) * Sin(DA) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
'        GoSub GRAFICO_MOLA_o_INGRANAGGIO_FIANCO
'      Next i
'      pX1 = -Xmax(1) * ScalaX + PicW / 2
'      pY2 = -((Ymax(0) - Ymin(0)) / 2) * ScalaY + PicH / 2
'      pX2 = Xmax(2) * ScalaX + PicW / 2
'      pY1 = ((Ymax(0) - Ymin(0)) / 2) * ScalaY + PicH / 2
'      If (SiStampa = 1) Then
'        'Nome
'        stmp = "F1"
'        Printer.CurrentX = pX1 - Printer.TextWidth(stmp)
'        Printer.CurrentY = pY2
'        Printer.Print stmp
'        stmp = "F2"
'        Printer.CurrentX = pX2
'        Printer.CurrentY = pY2
'        Printer.Print stmp
'        'Larghezza mola: valore
'        stmp = "W=" & frmt(Xmax(1) + Xmax(2), 4) & "mm  "
'        Printer.CurrentX = PicW / 2 - Printer.TextWidth(stmp)
'        Printer.CurrentY = pY2
'        Printer.Print stmp
'        'Altezza mola: valore
'        stmp = " H=" & frmt(Ymax(0) - Ymin(0), 4) & "mm"
'        Printer.CurrentX = PicW / 2
'        Printer.CurrentY = pY2
'        Printer.Print stmp
'        Printer.DrawStyle = 4
'        'Larghezza mola: linea
'        Printer.Line (pX1, pY2)-(pX2, pY2)
'        'Altezza mola: linea
'        Printer.Line (PicW / 2, pY1)-(PicW / 2, pY2)
'        Printer.DrawStyle = 0
'      Else
'        'Nome
'        stmp = "F1"
'        OEM1.OEM1PicCALCOLO.CurrentX = pX1 - OEM1.OEM1PicCALCOLO.TextWidth(stmp)
'        OEM1.OEM1PicCALCOLO.CurrentY = pY2
'        OEM1.OEM1PicCALCOLO.Print stmp
'        stmp = "F2"
'        OEM1.OEM1PicCALCOLO.CurrentX = pX2
'        OEM1.OEM1PicCALCOLO.CurrentY = pY2
'        OEM1.OEM1PicCALCOLO.Print stmp
'        'Larghezza mola: valore
'        stmp = "W=" & frmt(Xmax(1) + Xmax(2), 4) & "mm  "
'        OEM1.OEM1PicCALCOLO.CurrentX = PicW / 2 - OEM1.OEM1PicCALCOLO.TextWidth(stmp)
'        OEM1.OEM1PicCALCOLO.CurrentY = pY2
'        OEM1.OEM1PicCALCOLO.Print stmp
'        'Altezza mola: valore
'        stmp = " H=" & frmt(Ymax(0) - Ymin(0), 4) & "mm"
'        OEM1.OEM1PicCALCOLO.CurrentX = PicW / 2
'        OEM1.OEM1PicCALCOLO.CurrentY = pY2
'        OEM1.OEM1PicCALCOLO.Print stmp
'        OEM1.OEM1PicCALCOLO.DrawStyle = 4
'        'Larghezza mola: linea
'        OEM1.OEM1PicCALCOLO.Line (pX1, pY2)-(pX2, pY2)
'        'Altezza mola: linea
'        OEM1.OEM1PicCALCOLO.Line (PicW / 2, pY1)-(PicW / 2, pY2)
'        OEM1.OEM1PicCALCOLO.DrawStyle = 0
'      End If
'      GoSub FineDocumento
'      '
'    Case 2    '"CONTATTO"
'      OEM1.lblINP(0).Visible = True
'      OEM1.txtINP(0).Visible = True
'      OEM1.lblINP(1).Visible = True
'      OEM1.txtINP(1).Visible = True
'      GoSub ETICHETTE_GRAFICI
'      'Estremi dei fianchi
'      For Fianco = 1 To 2
'        Xmax(Fianco) = D(1)
'        Ymax(Fianco) = L(Fianco, 1)
'        Xmin(Fianco) = D(1)
'        Ymin(Fianco) = L(Fianco, 1)
'        For i = 1 To (Np + NPR1 + 1)
'          If L(Fianco, i) >= Ymax(Fianco) Then Ymax(Fianco) = L(Fianco, i)
'          If D(i) >= Xmax(Fianco) Then Xmax(Fianco) = D(i)
'          If L(Fianco, i) <= Ymin(Fianco) Then Ymin(Fianco) = L(Fianco, i)
'          If D(i) <= Xmin(Fianco) Then Xmin(Fianco) = D(i)
'        Next i
'      Next Fianco
'      GoSub Estremi_Massimi
'      ScalaX = val(GetInfo("CALCOLO", "ScalaCONTATTOx", PathFILEINI))
'      ScalaY = val(GetInfo("CALCOLO", "ScalaCONTATTOy", PathFILEINI))
'      OEM1.txtINP(0).Text = frmt(ScalaX, 4)
'      OEM1.txtINP(1).Text = frmt(ScalaY, 4)
'      'FIANCO 1
'      For i = 1 To Np + NPR1 + 1 - 1
'        pX1 = -(D(i) - Xmin(0)) * ScalaX + PicW / 2
'        pY1 = -L(1, i) * ScalaY * BETAseg + PicH / 2
'        pX2 = -(D(i + 1) - Xmin(0)) * ScalaX + PicW / 2
'        pY2 = -L(1, i + 1) * ScalaY * BETAseg + PicH / 2
'        If (SiStampa = 1) Then
'          Printer.Line (pX1, pY1)-(pX2, pY2)
'        Else
'          OEM1.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2)
'        End If
'      Next i
'      i = 1:  stmp = "EAP": GoSub LineaF1
'      i = 0:  stmp = "Pri": GoSub LineaF1
'      i = Np: stmp = "SAP": GoSub LineaF1
'      'FIANCO 2
'      For i = 1 To Np + NPR1 + 1 - 1
'        pX1 = (D(i) - Xmin(0)) * ScalaX + PicW / 2
'        pY1 = L(2, i) * ScalaY * BETAseg + PicH / 2
'        pX2 = (D(i + 1) - Xmin(0)) * ScalaX + PicW / 2
'        pY2 = L(2, i + 1) * ScalaY * BETAseg + PicH / 2
'        If (SiStampa = 1) Then
'          Printer.Line (pX1, pY1)-(pX2, pY2)
'        Else
'          OEM1.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2)
'        End If
'      Next i
'      i = 1:  stmp = "EAP": GoSub LineaF2
'      i = 0:  stmp = "Pri": GoSub LineaF2
'      i = Np: stmp = "SAP": GoSub LineaF2
'      'Larghezza ed estensione
'      pX1 = -(D(1) - Xmin(0)) * ScalaX + PicW / 2
'      pY1 = PicH / 2
'      pX2 = (D(1) - Xmin(0)) * ScalaX + PicW / 2
'      pY2 = PicH / 2
'        If (SiStampa = 1) Then
'          Printer.DrawStyle = 2
'          Printer.Line (pX1, pY1)-(pX2, pY2)
'          Printer.DrawStyle = 0
'          stmp = "F1"
'          Printer.CurrentX = pX1
'          Printer.CurrentY = pY1
'          Printer.Print stmp
'          stmp = "F2"
'          Printer.CurrentX = pX2
'          Printer.CurrentY = pY2
'          Printer.Print stmp
'        Else
'          OEM1.OEM1PicCALCOLO.DrawStyle = 2
'          OEM1.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2)
'          OEM1.OEM1PicCALCOLO.DrawStyle = 0
'          stmp = "F1"
'          OEM1.OEM1PicCALCOLO.CurrentX = pX1
'          OEM1.OEM1PicCALCOLO.CurrentY = pY1
'          OEM1.OEM1PicCALCOLO.Print stmp
'          stmp = "F2"
'          OEM1.OEM1PicCALCOLO.CurrentX = pX2
'          OEM1.OEM1PicCALCOLO.CurrentY = pY2
'          OEM1.OEM1PicCALCOLO.Print stmp
'        End If
'      i = 0
'      stmp = "Lmax=" & frmt(L(1, 1) + L(2, 1), 4) & "mm "
'      GoSub ScriviCONTATTO
'      stmp = "Leap-Lsap=" & frmt(L(1, 1) / 2 - L(1, Np) / 2 + L(2, 1) / 2 - L(2, Np) / 2, 4) & "mm "
'      GoSub ScriviCONTATTO
'      stmp = "Lpri=" & frmt((L(1, 0) + L(2, 0)) / 2, 4) & "mm "
'      GoSub ScriviCONTATTO
'      stmp = "F1 :Leap=" & frmt(L(1, 1), 4) & "mm "
'      GoSub ScriviCONTATTO
'      stmp = "F2 :Leap=" & frmt(L(2, 1), 4) & "mm "
'      GoSub ScriviCONTATTO
'      stmp = "F1 :Lsap=" & frmt(L(1, Np), 4) & "mm "
'      GoSub ScriviCONTATTO
'      stmp = "F2 :Lsap=" & frmt(L(2, Np), 4) & "mm "
'      GoSub ScriviCONTATTO
'      GoSub FineDocumento
'      '
''    Case 3    '"CORREZIONI"
''      OEM1.lblINP(0).Visible = True
''      OEM1.txtINP(0).Visible = True
''      OEM1.lblINP(1).Visible = True
''      OEM1.txtINP(1).Visible = True
''      GoSub ETICHETTE_GRAFICI
''      'Estremi dei fianchi
''      For Fianco = 1 To 2
''        XMax(Fianco) = CT(Fianco, 1)
''        Ymax(Fianco) = MM(Np)
''        XMIN(Fianco) = CT(Fianco, 1)
''        YMIN(Fianco) = MM(Np)
''        For i = 1 To Np
''          If CT(Fianco, i) >= XMax(Fianco) Then XMax(Fianco) = CT(Fianco, i)
''          If MM(Np + 1 - i) >= Ymax(Fianco) Then Ymax(Fianco) = MM(Np + 1 - i)
''          If CT(Fianco, i) <= XMIN(Fianco) Then XMIN(Fianco) = CT(Fianco, i)
''          If MM(Np + 1 - i) <= YMIN(Fianco) Then YMIN(Fianco) = MM(Np + 1 - i)
''        Next i
''      Next Fianco
''      GoSub Estremi_Massimi
''      ScalaX = val(GetInfo("CALCOLO", "ScalaCORREZIONIx", PathFILEINI))
''      ScalaY = val(GetInfo("CALCOLO", "ScalaCORREZIONIy", PathFILEINI))
''      OEM1.txtINP(0).Text = frmt(ScalaX, 4)
''      OEM1.txtINP(1).Text = frmt(ScalaY, 4)
''      For Fianco = 1 To 2
''        stmp = "F" & Format$(Fianco, "0")
''        For i = 1 To Np - 1
''          pX1 = -CT(Fianco, i - 1) * ScalaX * (3 - 2 * Fianco) + PicW / 2 - PicW / 4 * (3 - 2 * Fianco)
''          pY1 = -(MM(Np + 1 - i) - (Ymax(0) + YMIN(0)) / 2) * ScalaY + PicH / 2
''          pX2 = -CT(Fianco, i) * ScalaX * (3 - 2 * Fianco) + PicW / 2 - PicW / 4 * (3 - 2 * Fianco)
''          pY2 = -(MM(Np + 1 - i - 1) - (Ymax(0) + YMIN(0)) / 2) * ScalaY + PicH / 2
''          If (SiStampa = 1) Then
''            If i = 1 Then
''              Printer.CurrentX = PicW / 2 - PicW / 4 * (3 - 2 * Fianco) - Printer.TextWidth(stmp) / 2
''              Printer.CurrentY = pY1
''              Printer.Print stmp
''            End If
''            Printer.Line (pX1, pY1)-(pX2, pY2)
''            Printer.DrawStyle = 2
''            Printer.Line (PicW / 2 - PicW / 4 * (3 - 2 * Fianco), pY2)-(pX2, pY2)
''            Printer.DrawStyle = 0
''            Printer.Line (PicW / 2 - PicW / 4 * (3 - 2 * Fianco), pY1)-(PicW / 2 - PicW / 4 * (3 - 2 * Fianco), pY2)
''          Else
''            If i = 1 Then
''              OEM1.OEM1PicCALCOLO.CurrentX = PicW / 2 - PicW / 4 * (3 - 2 * Fianco) - OEM1.OEM1PicCALCOLO.TextWidth(stmp) / 2
''              OEM1.OEM1PicCALCOLO.CurrentY = pY1
''              OEM1.OEM1PicCALCOLO.Print stmp
''            End If
''            OEM1.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2)
''            OEM1.OEM1PicCALCOLO.DrawStyle = 2
''            OEM1.OEM1PicCALCOLO.Line (PicW / 2 - PicW / 4 * (3 - 2 * Fianco), pY2)-(pX2, pY2)
''            OEM1.OEM1PicCALCOLO.DrawStyle = 0
''            OEM1.OEM1PicCALCOLO.Line (PicW / 2 - PicW / 4 * (3 - 2 * Fianco), pY1)-(PicW / 2 - PicW / 4 * (3 - 2 * Fianco), pY2)
''          End If
''        Next i
''      Next Fianco
''      If (SiStampa = 1) Then
''        stmp = " + "
''        Printer.CurrentX = 0
''        Printer.CurrentY = 0
''        Printer.Print stmp
''        stmp = " - "
''        Printer.CurrentX = PicW / 2 - Printer.TextWidth(stmp) / 2
''        Printer.CurrentY = 0
''        Printer.Print stmp
''        stmp = " + "
''        Printer.CurrentX = PicW - Printer.TextWidth(stmp)
''        Printer.CurrentY = 0
''        Printer.Print stmp
''      End If
''      stmp = " + "
''      OEM1.OEM1PicCALCOLO.CurrentX = 0
''      OEM1.OEM1PicCALCOLO.CurrentY = 0
''      OEM1.OEM1PicCALCOLO.Print stmp
''      stmp = " - "
''      OEM1.OEM1PicCALCOLO.CurrentX = PicW / 2 - OEM1.OEM1PicCALCOLO.TextWidth(stmp) / 2
''      OEM1.OEM1PicCALCOLO.CurrentY = 0
''      OEM1.OEM1PicCALCOLO.Print stmp
''      stmp = " + "
''      OEM1.OEM1PicCALCOLO.CurrentX = PicW - OEM1.OEM1PicCALCOLO.TextWidth(stmp)
''      OEM1.OEM1PicCALCOLO.CurrentY = 0
''      OEM1.OEM1PicCALCOLO.Print stmp
''      Call TIF_di_riferimento(D2, MMest, ScalaY, Ymax(0), YMIN(0), SiStampa) 'MM fine profilo
''      Call TIF_di_riferimento(DP, MMp, ScalaY, Ymax(0), YMIN(0), SiStampa) 'MM primitivo
''      Call TIF_di_riferimento(D1, MM1, ScalaY, Ymax(0), YMIN(0), SiStampa) 'MM inizio profilo
''      For Fianco = 1 To 2
''        If Valutazione = 2 Then
''          Val1 = val(OEM1.List3(Fianco).List(0))
''          If Val1 > 0 Then
''            Val2 = Sqr(Val1 ^ 2 / 4 - Rb ^ 2)
''            Call TIF_di_riferimento_Fianco(Val1, Val2, ScalaY, Ymax(0), YMIN(0), SiStampa, Fianco, Valutazione)
''          End If
''          Val1 = val(OEM1.List3(Fianco).List(3))
''          If Val1 > 0 Then
''            Val2 = Sqr(Val1 ^ 2 / 4 - Rb ^ 2)
''            Call TIF_di_riferimento_Fianco(Val1, Val2, ScalaY, Ymax(0), YMIN(0), SiStampa, Fianco, Valutazione)
''          End If
''          Val1 = val(OEM1.List3(Fianco).List(6))
''          If Val1 > 0 Then
''            Val2 = Sqr(Val1 ^ 2 / 4 - Rb ^ 2)
''            Call TIF_di_riferimento_Fianco(Val1, Val2, ScalaY, Ymax(0), YMIN(0), SiStampa, Fianco, Valutazione)
''          End If
''        Else
''          Val2 = val(OEM1.List3(Fianco).List(0))
''          If Val2 > 0 Then
''            Val1 = 2 * Sqr(Val2 ^ 2 + Rb ^ 2)
''            Call TIF_di_riferimento_Fianco(Val1, Val2, ScalaY, Ymax(0), YMIN(0), SiStampa, Fianco, Valutazione)
''          End If
''          Val2 = val(OEM1.List3(Fianco).List(3))
''          If Val2 > 0 Then
''            Val1 = 2 * Sqr(Val2 ^ 2 + Rb ^ 2)
''            Call TIF_di_riferimento_Fianco(Val1, Val2, ScalaY, Ymax(0), YMIN(0), SiStampa, Fianco, Valutazione)
''          End If
''          Val2 = val(OEM1.List3(Fianco).List(6))
''          If Val2 > 0 Then
''            Val1 = 2 * Sqr(Val2 ^ 2 + Rb ^ 2)
''            Call TIF_di_riferimento_Fianco(Val1, Val2, ScalaY, Ymax(0), YMIN(0), SiStampa, Fianco, Valutazione)
''          End If
''        End If
''      Next Fianco
''      GoSub FineDocumento
'      '
'    Case 3    '"INFORMAZIONI1"
'      GoSub ETICHETTE_GRAFICI
'      OEM1.lblINP(0).Visible = True
'      OEM1.txtINP(0).Visible = True
'      OEM1.lblINP(0).Caption = "Dem"
'      OEM1.txtINP(0).Text = frmt(DEm, 4)
'      OEM1.lblINP(1).Visible = True
'      OEM1.txtINP(1).Visible = True
'      OEM1.lblINP(1).Caption = "Im"
'      OEM1.txtINP(1).Text = frmt(BETAmG, 4)
'      i = 0
'        pY2 = OEM1.OEM1PicCALCOLO.TextHeight("X")
'        pX1 = 1
'        pY1 = 1
'        'stmp = "Diametro di base"
'        ret = LoadString(g_hLanguageLibHandle, 1201, Atmp, 255)
'        If ret > 0 Then stmp = Left$(Atmp, ret)
'        stmp = stmp & " = "
'        stmp = stmp & frmt(DB, 4)
'        j = WritePrivateProfileString("CALCOLO", "Dato(0)", frmt(DB, 4), PathFILEINI)
'        GoSub Scrivi
'        'stmp = "Elica di base"
'        ret = LoadString(g_hLanguageLibHandle, 1202, Atmp, 255)
'        If ret > 0 Then stmp = Left$(Atmp, ret)
'        stmp = stmp & " = "
'        stmp = stmp & frmt(Betab * 180 / PG, 4)
'        j = WritePrivateProfileString("CALCOLO", "Dato(1)", frmt(Betab * 180 / PG, 4), PathFILEINI)
'        GoSub Scrivi
'        'stmp = "Diametro primitivo"
'        ret = LoadString(g_hLanguageLibHandle, 1203, Atmp, 255)
'        If ret > 0 Then stmp = Left$(Atmp, ret)
'        stmp = stmp & " = "
'        stmp = stmp & frmt(DP, 4)
'        j = WritePrivateProfileString("CALCOLO", "Dato(2)", frmt(DP, 4), PathFILEINI)
'        GoSub Scrivi
'        'stmp = "Diametro esterno mola"
'        ret = LoadString(g_hLanguageLibHandle, 1200, Atmp, 255)
'        If ret > 0 Then stmp = Left$(Atmp, ret)
'        stmp = stmp & " (Dem)"
'        stmp = stmp & " = "
'        stmp = stmp & frmt(DEm, 4)
'        GoSub Scrivi
'        'S0N normale
'        stmp = LNp("R[46]") 'OEM1.List1(0).List(7)
'        stmp = stmp & " = "
'        stmp = stmp & frmt(S0Nt * Cos(Beta), 4)
'        GoSub Scrivi
'        'SAP
'        stmp = LNp("R[43]") 'OEM1.List1(0).List(4)
'        stmp = stmp & " = "
'        stmp = stmp & frmt(D1, 4)
'        GoSub Scrivi
'        'EAP
'        stmp = LNp("DEXT_G") 'OEM1.List1(0).List(5)
'        stmp = stmp & " = "
'        stmp = stmp & frmt(D2, 4)
'        GoSub Scrivi
'        'Raggio di fondo
'        stmp = LNp("R[52]") 'OEM1.List1(0).List(13)
'        stmp = stmp & " = "
'        stmp = stmp & frmt(RF, 4)
'        GoSub Scrivi
'        'Diametro interno
'        ret = LoadString(g_hLanguageLibHandle, 1208, Atmp, 255)
'        If ret > 0 Then stmp = Left$(Atmp, ret) Else stmp = ""
'        stmp = stmp & " = "
'        stmp = stmp & frmt(Ri * 2, 4)
'        GoSub Scrivi
'        'Raggio Max Rullo Profilatore
'        ret = LoadString(g_hLanguageLibHandle, 1209, Atmp, 255)
'        If ret > 0 Then stmp = Left$(Atmp, ret) Else stmp = ""
'        stmp = stmp & " = "
'        If RulloMax(1) < RulloMax(2) Then
'          stmp = stmp & frmt(Fix(RulloMax(1) * 10) / 10, 1)
'        Else
'          stmp = stmp & frmt(Fix(RulloMax(2) * 10) / 10, 1)
'        End If
'        GoSub Scrivi
'      If (Len(NomeTabellaCE) > 0) Then
'        'Set DBB = OpenDatabase(PathDB, True, False)
'        'Set RSS = DBB.OpenRecordset(NomeTabellaCE)
'        'RSS.Index = "INDICE"
'        For Fianco = 1 To 2
'          'Correzione asse ELICA X Fianco
'          If CE(Fianco, 1) > 0 And CE(Fianco, 4) = 0 And CE(Fianco, 7) = 0 And CE(Fianco, 2) <> 0 Then
'            dtmp1 = CE(Fianco, 1) / Cos(Beta) / 2
'            dtmp2 = (dtmp1 * dtmp1 + CE(Fianco, 2) * CE(Fianco, 2)) / (2 * CE(Fianco, 2))
'            dtmp1 = Atn(dtmp1 / dtmp2) * 180 / PG
'            '
'            'RSS.Seek "=", 16 + Fianco
'            stmp = IIf(Fianco = 1, LEP("COR[0,158]"), LEP("COR[0,159]")) 'RSS.Fields("NOME_" & LINGUA).Value
'            'stmp = "Corr. A F" & Format$(Fianco, "0")
'            '
'            stmp = stmp & " = " & frmt(dtmp1, 4)
'            GoSub Scrivi
'          End If
'        Next Fianco
'        'RSS.Close
'        'DBB.Close
'      End If
'      '
'      i = 0
'        pY2 = OEM1.OEM1PicCALCOLO.TextHeight("X")
'        pX1 = PicW / 2
'        pY1 = 1
'        'stmp = "Piattino di testa mola"
'        ret = LoadString(g_hLanguageLibHandle, 1205, Atmp, 255)
'        If ret > 0 Then stmp = Left$(Atmp, ret)
'        stmp = stmp & " = "
'        stmp = stmp & frmt(vi, 4)
'        GoSub Scrivi
'        'stmp = "Inclinazione asse mola"
'        stmp = LNp("R[258]") 'OEM1.List1(0).List(16)
'        stmp = stmp & " = "
'        stmp = stmp & frmt(BETAmG, 4)
'        GoSub Scrivi
'      '
'      If (BETAmR <> Beta) Then
'        '
'        Dim ALF     As Double
'        Dim DpNew   As Double
'        Dim AlfaNew As Double
'        Dim AlfaNe  As Double
'        Dim MNew    As Double
'        Dim SwNew   As Double
'        Dim IvN     As Double
'        '
'        ALF = ALFAt                       'ALFA trasv.
'        T1 = Tan(ALF)
'        F3 = T1 - Atn(T1)                 'INVOLUTA
'        Rp = AM * Z / 2 / Cos(Beta)       'Raggio primitivo
'        Betab = Atn(Tan(Beta) * Cos(ALF)) 'Elica base
'        Rb = Rp * Cos(ALF)                'Raggio di base
'        PB = (Rb * 2 * PG) / Z            'Passo di base
'        DB = 2 * Rb                       'Dbase
'        DP = 2 * Rp                       'D prim.
'        PAS = PG * DP / Tan(Beta)         'Passo Elica
'        DpNew = Tan(BETAmR) * PAS / PG    'Nuovo diametro primitivo
'        AlfaNew = FnARC(DB / DpNew)       'Nuovo ALFA trasv.
'        AlfaNe = Atn(Tan(AlfaNew) * Cos(BETAmR))
'        MNew = DpNew * Cos(BETAmR) / Z    'Nuovo Modulo
'        IvN = Tan(AlfaNew) - AlfaNew      'Nuova Involuta ALFA
'        SwNew = (S0Nn / DP / Cos(Beta) + F3 - IvN) * DpNew * Cos(BETAmR)
'        '
'        'Inclinazione asse mola: valore modificato da leggere e stampare
'        ret = WritePrivateProfileString("CALCOLO", "item(0)", frmt(BETAmG, 4), PathFILEINI)
'        '
'        'Modulo normale
'        stmp = LNp("R[42]") 'OEM1.List1(0).List(1)
'        stmp = stmp & " = "
'        stmp = stmp & frmt(MNew, 4)
'        ret = WritePrivateProfileString("CALCOLO", "item(1)", frmt(MNew, 4), PathFILEINI)
'        GoSub Scrivi
'        'Angolo di pressione
'        stmp = LNp("R[40]") 'OEM1.List1(0).List(2)
'        stmp = stmp & " = "
'        stmp = stmp & frmt(AlfaNe * 180 / PG, 4)
'        ret = WritePrivateProfileString("CALCOLO", "item(2)", frmt(AlfaNe * 180 / PG, 4), PathFILEINI)
'        GoSub Scrivi
'        'stmp = "Diametro primitivo"
'        ret = LoadString(g_hLanguageLibHandle, 1203, Atmp, 255)
'        If ret > 0 Then stmp = Left$(Atmp, ret)
'        stmp = stmp & " = "
'        stmp = stmp & frmt(DpNew, 4)
'        ret = WritePrivateProfileString("CALCOLO", "item(3)", frmt(DpNew, 4), PathFILEINI)
'        GoSub Scrivi
'        'S0N normale
'        stmp = LNp("R[46]") 'OEM1.List1(0).List(7)
'        stmp = stmp & " = "
'        stmp = stmp & frmt(SwNew, 4)
'        ret = WritePrivateProfileString("CALCOLO", "item(4)", frmt(SwNew, 4), PathFILEINI)
'        GoSub Scrivi
'        'stmp = "Passo"
'        ret = LoadString(g_hLanguageLibHandle, 1204, Atmp, 255)
'        If ret > 0 Then stmp = Left$(Atmp, ret)
'        stmp = stmp & " = "
'        stmp = stmp & frmt(PAS, 4)
'        ret = WritePrivateProfileString("CALCOLO", "item(5)", frmt(PAS, 4), PathFILEINI)
'        GoSub Scrivi
'        '
'      Else
'        '
'        'Scrivo "N" nel file ini per evitare di stampare quando Im non � modificata
'        For i = 0 To 5
'          ret = WritePrivateProfileString("CALCOLO", "item(" & Format$(i, "0") & ")", "N", PathFILEINI)
'        Next i
'        '
'      End If
'      GoSub FineDocumento
'      '
'    Case 4    '"INFORMAZIONI2"
'      'CARATTERE DI STAMPA TESTO
'      TextFont = GetInfo("CALCOLO", "TextFont", PathFILEINI)
'      If (SiStampa = 1) Then
'        For i = 1 To 2
'          Printer.FontName = "Courier New"
'          Printer.FontSize = 8
'          Printer.FontBold = False
'        Next i
'      Else
'        For i = 1 To 2
'          '--- FlagMOD 024 ---
'          'OEM1.OEM1PicCALCOLO.FontName = TextFont
'          'OEM1.OEM1PicCALCOLO.FontSize = 12
'          'OEM1.OEM1PicCALCOLO.FontBold = False
'          OEM1.txtHELP.FontName = "Ms Song" 'TextFont
'          OEM1.txtHELP.FontSize = 9  '12
'          OEM1.txtHELP.FontBold = False
'          '--- ------- --- ---
'        Next i
'      End If
'      OEM1.lblINP(0).Visible = False
'      OEM1.txtINP(0).Visible = False
'      OEM1.lblINP(1).Visible = False
'      OEM1.txtINP(1).Visible = False
'      GoSub ETICHETTE_GRAFICI
'      OEM1.txtHELP.Visible = True
'      OEM1.txtHELP.ZOrder (0)
'      OEM1.txtHELP.Move OEM1.OEM1PicCALCOLO.Left, OEM1.OEM1PicCALCOLO.Top, OEM1.OEM1PicCALCOLO.Width, OEM1.OEM1PicCALCOLO.Height
'      Open g_chOemPATH & "\DATI.TEO" For Input As 1
'        OEM1.txtHELP.Text = ""
'        OEM1.txtHELP.Text = Input$(LOF(1), 1)
'      Close #1
'      If (SiStampa = 1) Then
'        Open g_chOemPATH & "\DATI.TEO" For Input As 1
'          While Not EOF(1)
'            Line Input #1, stmp
'            Printer.Print stmp
'            Write_Dialog stmp
'          Wend
'        Close #1
'        Printer.EndDoc
'        Write_Dialog OEM1.OEM1Combo1.Text & " --> " & Printer.DeviceName & " OK!!"
'      End If
'      '
'  End Select
  '
  
'PuntiMola
'With PuntiMola
'  ReDim .XmF1(UBound(x, 2))
'  ReDim .XmF2(UBound(x, 2))
'  ReDim .YmF1(UBound(Y, 2))
'  ReDim .YmF2(UBound(Y, 2))
'  For i = 0 To UBound(.XmF1)
'    .XmF1(i) = x(1, i)
'    .XmF2(i) = x(2, i)
'    .YmF1(i) = Y(1, i)
'    .YmF2(i) = Y(2, i)
'  Next i
'End With
Dim SensoElica As Integer

With CalcoloEsterni
  .MolaX = x
  .MolaY = Y
  .IngrX = XD
  .IngrY = YD
  .DCont = D
  .LCont = L
  SensoElica = LEP("iSensoElica")
  For i = 1 To UBound(.LCont, 2)
    .LCont(1, i) = -SensoElica * .LCont(1, i)
    .LCont(2, i) = -SensoElica * .LCont(2, i)
  Next i
  .DB = DB
  .DP = DP
  .DE = D2
  .DI = DIint
  .DIOtt = Ri * 2
  .Np = Np
End With

'PuntiIngranaggio
'With PuntiIngr
'  ReDim .XmF1(UBound(XD, 2))
'  ReDim .XmF2(UBound(XD, 2))
'  ReDim .YmF1(UBound(YD, 2))
'  ReDim .YmF2(UBound(YD, 2))
'  For i = 0 To UBound(.XmF1)
'    .XmF1(i) = XD(1, i)
'    .XmF2(i) = XD(2, i)
'    .YmF1(i) = YD(1, i)
'    .YmF2(i) = YD(2, i)
'  Next i
'End With


Exit Sub

errCALCOLO_INGRANAGGIO_ESTERNO_380:
  If FreeFile > 0 Then Close
  Write_Dialog "Sub CALCOLO_INGRANAGGIO_ESTERNO_380_NEW: ERROR -> " & Err
  Err = 0
  ParametriOK = False
  'Resume Next
  Exit Sub
  
CALCOLO_DELLE_CORREZIONI_PROFILO_K:
  ' 13.04.98
  ' Fianco 1
  E265 = 0: E19 = 1
  For i = 0 To Np
    CB(Fianco, i) = 0
  Next i
  ' Controllo TIF1
  If Tif(Fianco, 1) = 0 Then GoTo CALCK2
  If Tif(Fianco, 1) < MM1 Then GoTo CALCK2
  If Tif(Fianco, 1) > MM2 Then Tif(Fianco, 1) = MM2
  E260 = Int((Tif(Fianco, 1) - MM1 + DIPU / 2) / DIPU) * DIPU
  E261 = E260 / DIPU + 1
  If Bombatura(Fianco, 1) <> 0 Then
    E262 = (E260 * E260 / 4 + Bombatura(Fianco, 1) * Bombatura(Fianco, 1)) / 2 / Bombatura(Fianco, 1) ' RB ( - , + )
  End If
  E263 = 0: E17 = 0
ALCK1:
  E263 = E263 + 1: E17 = E17 + 1
  CB(Fianco, E17) = Rastremazione(Fianco, 1) * (E263 - 1) / (E261 - 1)
  If Bombatura(Fianco, 1) <> 0 Then
    E264 = Abs(E260 / 2 - (E263 - 1) * DIPU)
    CB(Fianco, E17) = CB(Fianco, E17) + Bombatura(Fianco, 1) + Sqr(E262 * E262 - E264 * E264) * E262 / Abs(E262) - E262
  End If
  If E263 < E261 Then GoTo ALCK1
  E265 = CB(Fianco, E17)
  ' Tratto 2
  E19 = E261
  ' Tratto 2:TIF2, RASTR2, BOMB2
  If Tif(Fianco, 2) = 0 Then GoTo AZCK
  If Tif(Fianco, 2) <= Tif(Fianco, 1) Then
    Write_Dialog "ERROR: TIF2 <= TIF1"
    ParametriOK = False
    Exit Sub     ' ERRORE se TIF2 <= TIF1
  End If
  If Tif(Fianco, 2) > MM2 Then Tif(Fianco, 2) = MM2
  E266 = Int((Tif(Fianco, 2) - MM1 - E260 + DIPU / 2) / DIPU) * DIPU
  E267 = E266 / DIPU + 1
  If Bombatura(Fianco, 2) <> 0 Then E262 = (E266 * E266 / 4 + Bombatura(Fianco, 2) * Bombatura(Fianco, 2)) / 2 / Bombatura(Fianco, 2)
  E263 = 1
ALCK2:
  E263 = E263 + 1
  E17 = E17 + 1
  CB(Fianco, E17) = Rastremazione(Fianco, 2) * (E263 - 1) / (E267 - 1) + E265
  If Bombatura(Fianco, 2) <> 0 Then
    E264 = Abs(E266 / 2 - (E263 - 1) * DIPU)
    CB(Fianco, E17) = CB(Fianco, E17) + Bombatura(Fianco, 2) + Sqr(E262 * E262 - E264 * E264) * E262 / Abs(E262) - E262
  End If
  If E263 < E267 Then GoTo ALCK2
  E265 = CB(Fianco, E17): E19 = E17 - 1
  If Tif(Fianco, 3) = 0 Then GoTo AZCK
  If Tif(Fianco, 3) <= Tif(Fianco, 2) Then
    ParametriOK = False
    Write_Dialog "ERROR: TIF3 <= TIF2"
    Exit Sub
  End If
  If Tif(Fianco, 3) > MM2 Then Tif(Fianco, 3) = MM2
  E268 = Int((Tif(Fianco, 3) - MM1 - E260 - E266 + DIPU / 2) / DIPU) * DIPU
  E269 = E268 / DIPU + 1
  If Bombatura(Fianco, 3) <> 0 Then E262 = (E268 * E268 / 4 + Bombatura(Fianco, 3) * Bombatura(Fianco, 3)) / 2 / Bombatura(Fianco, 3)
  E263 = 1
ALCK3:
  E263 = E263 + 1: E17 = E17 + 1
  CB(Fianco, E17) = Rastremazione(Fianco, 3) * (E263 - 1) / (E269 - 1) + E265
  If Bombatura(Fianco, 3) <> 0 Then
    E264 = Abs(E268 / 2 - (E263 - 1) * DIPU)
    CB(Fianco, E17) = CB(Fianco, E17) + Bombatura(Fianco, 3) + Sqr(E262 * E262 - E264 * E264) * E262 / Abs(E262) - E262
  End If
  If E263 < E269 Then GoTo ALCK3
  E19 = E17 - 1:  E265 = CB(Fianco, E17)
AZCK:
  E17 = 1 + E19: E19 = Np - E19
  If E19 < 1 Then GoTo CALCK2
  For i = 1 To E19
    CB(Fianco, E17) = E265
    E17 = E17 + 1
  Next i
CALCK2:
  MMp = Sqr(Rp * Rp - Rb * Rb)
  E261 = Int((MMp - MM0 + 0.5) / DIPU)
  If E261 < 0 Then E261 = 0
  If E261 >= Np Then E261 = Np - 1
  E262 = CB(Fianco, E261): E17 = 1
  For i = 1 To Np
    CB(Fianco, E17) = CB(Fianco, E17) - E262
    E17 = E17 + 1
  Next i
  CB(Fianco, 0) = CB(Fianco, 1) + (CB(Fianco, 1) - CB(Fianco, 2))
Return

CALCOLO_T0_FI0:
  RX = Sqr(MM(i) ^ 2 + Rb ^ 2)
  TanA = Sqr((RX / Rb) ^ 2 - 1)
  Ax = Atn(TanA)
  InvAx = TanA - Ax
  Sx = (S0Nt / 2 / Rp + F3 - InvAx) * 2 * RX
  VX = 2 * PG * RX / Z - Sx
  If Abs(Beta) < PG / 4 Then
    T0 = PG / 2 - VX / (2 * RX) + Atn(Correzione / Rb)
  Else
    'Calcolo del passo assiale
    PAS = Tan(PG / 2 - Beta) * 2 * PG * Rp
    T0 = PG / 2 - VX / (2 * RX) + (2 * PG * Correzione) / (Sin(Beta) * PAS * Cos(ALFAn))
  End If
  FI0 = T0 - Ax
  If T0 < 0 Then FI0 = FI0 - 2 * PG
7092:
  If FI0 > PG Then FI0 = FI0 - PG * 2: GoTo 7092
  If FI0 < -PG Then FI0 = FI0 + PG * 2: GoTo 7092
  a(Fianco, i) = PG / 2 - T0
Return

PUREA:
  PAS = Tan(PG / 2 - Beta) * 2 * PG * Rp
  SIGMA = (90 - BETAmR * 180 / PG) * PG / 180
  Rmin = PAS / ((PG * 2) * Tan(SIGMA))
  TP = PAS / (PG * 2)
  DRM = IL * Rmin
  RMPD = Rmin + IL
  YD(Fianco, i) = RX * Sin(T0)
  XD(Fianco, i) = RX * Cos(T0)
  RQC = RX * RX * Cos(T0 - FI0)
  AA = RQC * Sin(T0) + (2 * TP * TP + DRM) * Sin(FI0)
  BB = (DRM + TP * TP) * Cos(FI0) + RQC * Cos(T0)
  cc = 2 * (-DRM * Sin(FI0) - RQC * Sin(T0) + RX * RMPD * Cos(T0 - FI0))
  RAD = Sqr((BB / AA) * (BB / AA) - (cc / AA))
  T1 = BB / AA + RAD
  T2 = BB / AA - RAD
  ti = T2
  If Abs(T2) > Abs(T1) Then ti = T1
  If T0 < 0 Then ti = T1
  While ti >= PG: ti = PG * 0.9: Wend
  TSI = ti
  For j = 1 To 10
    fp1 = RQC * Sin(T0 + TSI) + DRM * Sin(FI0 + TSI) + TP * TP * TSI * Cos(FI0 + TSI) - RX * RMPD * Cos(T0 - FI0)
    fp2 = RQC * Cos(T0 + TSI) + DRM * Cos(FI0 + TSI) - TP * TP * TSI * Sin(FI0 + TSI) + TP * TP * Cos(FI0 + TSI)
    TSI = TSI - fp1 / fp2
  Next j
  ti = TSI
  XS = RX * Cos(T0 + ti)
  Ys = RX * Sin(T0 + ti)
  Zs = TP * ti
  'CALCOLO TANGENTE AL PROFILO MOLA IN TETAS
  Ap = Tan(ti + FI0) / XS
  Bp = -1 / XS
  Cp = (1 + Ys * Tan(ti + FI0) / XS) / TP
  Lp = Tan(SIGMA)
  ArgSeno = (Ap * Lp + Cp) / (Sqr(Ap * Ap + Bp * Bp + Cp * Cp) * Sqr(Lp * Lp + 1))
  SINa2 = ArgSeno * ArgSeno
  TanA = Sqr(SINa2 / (1 - SINa2))
  B(i) = 90 - (Atn(TanA) * (180 / PG))
  Uprof = XS * Cos(SIGMA) - Zs * Sin(SIGMA)
  Vprof = Ys - IL
  Wprof = Zs * Cos(SIGMA) + XS * Sin(SIGMA)
  Rprof = Sqr(Uprof ^ 2 + Vprof ^ 2)
  x(Fianco, i) = Wprof
  Y(Fianco, i) = Rprof
Return

19000:
  'MOLA --> INGRANAGGIO (sez.trasversale)
  'INPUT : X(JJ), Y(JJ), ALFAM(JJ)
  'OUTPUT: XD(JJ), YD(JJ), R(JJ), A(JJ), API(JJ)
  ENTRX = IL:  inf = BETAmR:  EXF = x(Fianco, jj): RXF = Y(Fianco, jj): APFD = AlfaM(Fianco, jj) * 180 / PG
  If Abs(APFD) > 87 Then APFD = Sgn(APFD) * 87
  If EXF < 0 Then APFD = -APFD
  If APFD < 0 Then Apf = ((180 + APFD) * (PG / 180)) Else Apf = ((APFD) * (PG / 180))
  GoSub 19200
  L(Fianco, jj) = V17: D(jj) = Rayon * 2: a(Fianco, jj) = ALFAs: API(jj) = BETAi * 180 / PG
  XD(Fianco, jj) = Rayon * Sin(ALFAs): YD(Fianco, jj) = Rayon * Cos(ALFAs)
Return

19200: '************** S/PAS ************ CALCUL PIECE D'APRES FRAISE
  Sens = 1: If EXF < 0 Then Sens = -1
  EXF = EXF * Sens: Apf = Apf * Sens: V7 = PAS / 2 / PG
  GoSub 20500: Rayon = RXV: ALFAs = Ang * Sens: AngS = Ang
  RXF = RXF + 0.01: EXF = EXF - 0.01 * Tan(Apf): GoSub 20500: Y1 = Yapp: X1 = Xapp
  RXF = RXF - 0.02: EXF = EXF + 0.02 * Tan(Apf): GoSub 20500
  RXF = RXF + 0.01: EXF = EXF - 0.01 * Tan(Apf)
  BETAi = (Atn((X1 - Xapp) / (Yapp - Y1)) - AngS) * Sens
  Apf = Apf * Sens: EXF = EXF * Sens
Return

20500:
  V12 = -Tan(Apf) * (ENTRX * Sin(inf) + V7 * Cos(inf))
  V13 = V7 * Sin(inf) - ENTRX * Cos(inf)
  V14 = Cos(inf) * ((-Tan(Apf) * EXF) + RXF)
  V15 = (V14 ^ 2) + (V12 ^ 2) - (V13 ^ 2)
  If V15 > 0 Then
    V15 = Sqr(V15)
    V16 = -(V15 + V12) / (V13 - V14)
    V15 = (V15 - V12) / (V13 - V14)
    VPB = V16: If Abs(V15) < Abs(V16) Then VPB = V15
    V12 = 2 * Atn(VPB)
    V13 = (RXF * Sin(inf) * Sin(V12)) + EXF * Cos(inf)
    V13 = Atn(V13 / ((RXF * Cos(V12)) - ENTRX))
    'DISTANZA ASSE MOLA PIANO TANGENTE --------------
    V17 = EXF * Sin(inf) - RXF * Sin(V12) * Cos(inf)
    '------------------------------------------------
    RXV = (ENTRX - RXF * Cos(V12)) / Cos(V13)
    EXV = EXF * Sin(inf) - V7 * V13 - RXF * Sin(V12) * Cos(inf)
    Ang = 2 * PG * EXV / PAS
    Yapp = RXV * Cos(Ang): Xapp = -RXV * Sin(Ang)
  Else
    Write_Dialog "EVALUATION NOT POSSIBLE!!!"
    ParametriOK = False
    Exit Sub
  End If
Return

Estremi_Massimi:
  Xmax(0) = Xmax(1): Ymax(0) = Ymax(1): Xmin(0) = Xmin(1): Ymin(0) = Ymin(1)
  For Fianco = 1 To 2
    If Xmax(Fianco) >= Xmax(0) Then Xmax(0) = Xmax(Fianco)
    If Ymax(Fianco) >= Ymax(0) Then Ymax(0) = Ymax(Fianco)
    If Xmin(Fianco) <= Xmin(0) Then Xmin(0) = Xmin(Fianco)
    If Ymin(Fianco) <= Ymin(0) Then Ymin(0) = Ymin(Fianco)
  Next Fianco
Return

LineaF1:
'  pX1 = -(D(i) - Xmin(0)) * ScalaX + PicW / 2
'  pY1 = -L(1, i) * ScalaY * BETAseg + PicH / 2
'  pX2 = -(D(i) - Xmin(0)) * ScalaX + PicW / 2
'  pY2 = 0 + PicH / 2
'  Call LINEA_FIANCO(1, SiStampa, stmp, pX1, pY1, pX2, pY2)
  'If (SiStampa = 1) Then
  '  Printer.DrawStyle = 2
  '  Printer.Line (pX1, pY1)-(pX2, pY2)
  '  Printer.CurrentX = pX1 - Printer.TextWidth(stmp) / 2
  '  Printer.CurrentY = pY1
  '  Printer.Print stmp
  '  Printer.DrawStyle = 0
  'Else
  '  OEM1.OEM1PicCALCOLO.DrawStyle = 2
  '  OEM1.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2)
  '  OEM1.OEM1PicCALCOLO.CurrentX = pX1 - OEM1.OEM1PicCALCOLO.TextWidth(stmp) / 2
  '  OEM1.OEM1PicCALCOLO.CurrentY = pY1
  '  OEM1.OEM1PicCALCOLO.Print stmp
  '  OEM1.OEM1PicCALCOLO.DrawStyle = 0
  'End If
Return

LineaF2:
'  pX1 = (D(i) - Xmin(0)) * ScalaX + PicW / 2
'  pY1 = L(2, i) * ScalaY * BETAseg + PicH / 2
'  pX2 = (D(i) - Xmin(0)) * ScalaX + PicW / 2
'  pY2 = 0 + PicH / 2
'  Call LINEA_FIANCO(2, SiStampa, stmp, pX1, pY1, pX2, pY2)
  'If (SiStampa = 1) Then
  '  Printer.DrawStyle = 2
  '  Printer.Line (pX1, pY1)-(pX2, pY2)
  '  Printer.CurrentX = pX1 - Printer.TextWidth(stmp) / 2
  '  Printer.CurrentY = pY1 - Printer.TextHeight(stmp)
  '  Printer.Print stmp
  '  Printer.DrawStyle = 0
  'Else
  '  OEM1.OEM1PicCALCOLO.DrawStyle = 2
  '  OEM1.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2)
  '  OEM1.OEM1PicCALCOLO.CurrentX = pX1 - OEM1.OEM1PicCALCOLO.TextWidth(stmp) / 2
  '  OEM1.OEM1PicCALCOLO.CurrentY = pY1 - OEM1.OEM1PicCALCOLO.TextHeight(stmp)
  '  OEM1.OEM1PicCALCOLO.Print stmp
  '  OEM1.OEM1PicCALCOLO.DrawStyle = 0
  'End If
Return

FineDocumento:
'  'Dimensione dei caratteri
'  If (SiStampa = 1) Then
'    For i = 1 To 2
'      Printer.FontName = TextFont
'      Printer.FontSize = 14
'      Printer.FontBold = True
'    Next i
'    'Logo
'    Printer.PaintPicture OEM1.PicLogo, 0, PicH, 20, 20
'    'Nome del documento
'    stmp = Date & " " & Time & "                  " & OEM1.OEM1Combo1.Text
'    Printer.CurrentX = 0 + 25
'    Printer.CurrentY = PicH
'    Printer.Print stmp
'    'Scala X
'    If OEM1.lblINP(0).Visible = True Then
'      stmp = OEM1.lblINP(0).Caption & ": x" & OEM1.txtINP(0).Text
'      Printer.CurrentX = 0 + 25
'      Printer.CurrentY = PicH + 10
'      Printer.Print stmp
'    End If
'    'Scala Y
'    If OEM1.lblINP(1).Visible = True Then
'      stmp = OEM1.lblINP(1).Caption & ": x" & OEM1.txtINP(1).Text
'      Printer.CurrentX = 0 + PicW / 2
'      Printer.CurrentY = PicH + 10
'      Printer.Print stmp
'    End If
'    'Parametri
'    Call STAMPA_INGRANAGGIO_ESTERNO_380(TextFont, PicW, PicH)
'    Printer.EndDoc
'    Write_Dialog OEM1.OEM1Combo1.Text & " --> " & Printer.DeviceName & " OK!!"
'  End If
Return

Scrivi:
'  'Visualizza nella finestra di calcolo
'  OEM1.OEM1PicCALCOLO.CurrentX = pX1
'  OEM1.OEM1PicCALCOLO.CurrentY = pY1 + i * pY2
'  OEM1.OEM1PicCALCOLO.Print stmp
'  If (SiStampa = 1) Then
'    'Invia alla stampante
'    Printer.CurrentX = pX1
'    Printer.CurrentY = pY1 + i * pY2
'    Printer.Print stmp
'  End If
'  i = i + 1
Return

ETICHETTE_GRAFICI:
'  'NASCONDO LA CASELLA DI TESTO
'  OEM1.txtHELP.Visible = False
'  'VISUALIZZO LE SCALE
'  i = LoadString(g_hLanguageLibHandle, 1000, Atmp, 255)
'  If i > 0 Then OEM1.lblINP(0).Caption = Left$(Atmp, i)
'  i = LoadString(g_hLanguageLibHandle, 1001, Atmp, 255)
'  If i > 0 Then OEM1.lblINP(1).Caption = Left$(Atmp, i)
'  If (SiStampa = 1) Then
'    Printer.ScaleMode = 6
'    Write_Dialog OEM1.OEM1Combo1.Text & " --> " & Printer.DeviceName
'  Else
'    OEM1.OEM1PicCALCOLO.Cls
'  End If
Return

GRAFICO_MOLA_o_INGRANAGGIO_FIANCO:
'  If (SiStampa = 1) Then
'    'Profilo
'    Printer.Line (pX1, pY1)-(pX2, pY2)
'    'Croci
'    Printer.Line (pX2 - 0.5, pY2)-(pX2 + 0.5, pY2)
'    Printer.Line (pX2, pY2 - 0.5)-(pX2, pY2 + 0.5)
'  Else
'    'Profilo
'    OEM1.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2)
'    'Croci
'    OEM1.OEM1PicCALCOLO.Line (pX2 - 0.5, pY2)-(pX2 + 0.5, pY2)
'    OEM1.OEM1PicCALCOLO.Line (pX2, pY2 - 0.5)-(pX2, pY2 + 0.5)
'  End If
Return
  
ScriviCONTATTO:
'  If (SiStampa = 1) Then
'    If BETAseg > 0 Then
'      Printer.CurrentX = PicW - Printer.TextWidth(stmp)
'    Else
'      Printer.CurrentX = 0
'    End If
'    Printer.CurrentY = Printer.TextHeight(stmp) * i
'    Printer.Print stmp
'  Else
'    If BETAseg > 0 Then
'      OEM1.OEM1PicCALCOLO.CurrentX = PicW - OEM1.OEM1PicCALCOLO.TextWidth(stmp)
'    Else
'      OEM1.OEM1PicCALCOLO.CurrentX = 0
'    End If
'    OEM1.OEM1PicCALCOLO.CurrentY = OEM1.OEM1PicCALCOLO.TextHeight(stmp) * i
'    OEM1.OEM1PicCALCOLO.Print stmp
'  End If
'  i = i + 1
Return

End Sub

