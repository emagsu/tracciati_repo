VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SUOperate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Function OPC_LEGGI_DATO(ByVal sOPC As String, Optional ErroreSuDialog As Boolean = False) As String
'
On Error GoTo errLEGGI_DATO_DDE
  '
  OPC_LEGGI_DATO = GetInfo("VARIABILI_STATO", sOPC, g_chOemPATH & "\CNC.INI")
  '
Exit Function

errLEGGI_DATO_DDE:
  WRITE_DIALOG "Function LEGGI_DATO_DDE: ERROR -> " & Err
  OPC_LEGGI_DATO = ""
  Exit Function

End Function

Public Sub OPC_SCRIVI_DATO(ByVal sOPC As String, ByVal Valore As String)
'
Dim ret As Integer

On Error GoTo errOPC_SCRIVI_DATO
  '
  Valore = Replace(Valore, ",", ".")
  ret = WritePrivateProfileString("VARIABILI_STATO", sOPC, Valore, g_chOemPATH & "\CNC.INI")
  '
Exit Sub

errOPC_SCRIVI_DATO:
  WRITE_DIALOG "Sub OPC_SCRIVI_DATO: ERROR -> " & Err
  Err = 0
  Exit Sub

End Sub

Public Sub SelPrg(sCMD_PI)
'
Dim ret As Integer
  '
  ret = WritePrivateProfileString("VARIABILI_STATO", "/Channel/ProgramPointer/progName[u1]", sCMD_PI, g_chOemPATH & "\CNC.INI")
  '
End Sub

Public Sub SwitchToTask(AREA As String)

End Sub

Public Sub SwitchToHmi()

End Sub

Public Sub Language()

End Sub

