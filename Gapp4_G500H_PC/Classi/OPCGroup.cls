VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "OPCGroup"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Event DataChange(ByVal TransactionID As Long, ByVal NumItems As Long, ClientHandles() As Long, ItemValues() As Variant, Qualities() As Long, TimeStamps() As Date)

Public count As Integer
Public IsActive As Boolean
Public IsSubscribed As Boolean
Public DefaultGroupUpdateRate As Long
Public DefaultGroupIsActive As Boolean
Public OPCItems As OPCItem

Public Sub RemoveAll()

End Sub

Public Function Add(A) As OPCGroup

End Function

Public Function Remove(A, HndGruppo, lngErrors)

End Function

Public Sub SyncRead(A, B, C, D, E)

End Sub
