VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IMCDomain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Sub CopyNC(SORGENTE, DESTINAZIONE, C)
'
Dim ii    As Integer
Dim stmp  As String
Dim NOME  As String
'
On Error Resume Next
  '
  'MODIFICA DEL PERCORSO
  If (InStr(DESTINAZIONE, "NC/") > 0) Then
    'VERSO NC
    DESTINAZIONE = Replace(DESTINAZIONE, "/", "\")
    DESTINAZIONE = g_chOemPATH & DESTINAZIONE
    If (Dir$(DESTINAZIONE) = "") Then
      'CREAZIONE DEL PERCORSO
      If (Dir$(DESTINAZIONE, 16) = "") Then
        ii = 1
        Do
          NOME = stmp
          stmp = Right$(DESTINAZIONE, ii)
          ii = ii + 1
        Loop While (InStr(stmp, "\") <= 0)
        ii = InStr(DESTINAZIONE, "\")
        While (ii <> 0)
          stmp = Left$(DESTINAZIONE, ii - 1)
          If (Dir$(stmp, 16) = "") And (InStr(stmp, NOME) <= 0) Then Call MkDir(stmp)
          ii = InStr(ii + 1, DESTINAZIONE, "\")
        Wend
      End If
    End If
  Else
    'VERSO HD
    SORGENTE = Replace(SORGENTE, "/", "\")
    SORGENTE = g_chOemPATH & SORGENTE
  End If
  If (Dir$(SORGENTE) <> "") Then
    If (Dir$(DESTINAZIONE) <> "") Then Call Kill(DESTINAZIONE)
    Call FileCopy(SORGENTE, DESTINAZIONE)
  End If
  '
End Sub

Public Sub MapACC_NC(Source, dest, Area, DataBlock, TOut, Strgud)

End Sub
