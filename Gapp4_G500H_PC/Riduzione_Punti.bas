Attribute VB_Name = "Riduzione_Punti"
Option Explicit
'------------- Var Globali

Global GX()     As Double   ' Vettori X, Y e TG mola non ridotta...
Global GY()    As Double   ' ...
Global GTG()   As Double   ' ...
Global GN()    As Integer  ' Vettore indici punti scelti
Global GR()    As Double   ' Vettori R A B rotore non ridotto...
Global GA()    As Double   ' ...
Global GB()    As Double   ' ...
Global Gim     As Integer
Const PG = 3.14159265
Const PI = 3.14159265
Private Declare Function WritePrivateProfileString Lib "Kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpString As Any, ByVal lpFileName As String) As Long
Private Declare Function GetPrivateProfileString Lib "Kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long

Sub RIDUZIONE_TEST(PIn As Integer, PFi As Integer, PMant As Integer, JRid As Integer, X() As Double, Y() As Double, _
                MaxDisPunti As Single, R2() As Double, _
                A() As Double, B() As Double, MODO As Integer, _
                Risoluzione As Single, Fianco As String, _
                XR() As Double, YR() As Double, RR() As Double, N() As Integer, _
                ERRORE_MASSIMO As Double, DDX_MASSIMO As Double, DDY_MASSIMO As Double)

Dim j       As Integer
Dim PDebut  As Integer
Dim PFin    As Integer
Dim PS      As Integer
Dim ErMax   As Double
Dim DDX     As Double
Dim DDY     As Double
Dim R0      As Double
Dim Xc      As Double
Dim YC      As Double
Dim i       As Integer
Dim i2      As Integer
Dim DDX_RID As Double
Dim DDY_RID As Double
Dim TERNAOK As Boolean
Dim TERNE() As Integer
Dim PARI    As Boolean

'************ Variabili per scelta punto medio minimizzando l'errore
Dim abc     As Double
Dim abd     As Double
Dim erromax As Single
Dim abcd    As Double
Dim abcdi   As Integer
Dim j3      As Integer

'************
DDX_RID = DDX_MASSIMO
DDY_RID = DDY_MASSIMO

j = 1
If PIn <> 1 Then j = JRid

ReDim TERNE(3, 0)
ReDim Preserve N(PFi - PIn + 1 + j)
ReDim Preserve XR(PFi - PIn + 1 + j)
ReDim Preserve YR(PFi - PIn + 1 + j)
ReDim Preserve RR(PFi - PIn + 1 + j)

PDebut = PIn
PFin = PDebut + 2
'Debug.Print "-----------"
While PFin <= PFi
    DDX = 0
    DDY = 0
    TERNAOK = False
        ' Ricerca punto a distana max da quello iniziale
        While ((DDX < MaxDisPunti) And (DDY < MaxDisPunti)) And (PFin < PFi)
            PFin = PFin + 1
            DDY = Abs(Y(PFin) - Y(PDebut))
            DDX = Abs(X(PFin) - X(PDebut))
        Wend
        
    While Not TERNAOK
        If (PFin < PFi) And (PFin - PDebut > 2) Then
                If (PFin = PFi) And (PFin - PDebut > 3) Then
                    PFin = PFin - 2
                    PS = PDebut + Int((PFin - PDebut) / 2)
                Else
                    ' Se i tre punti non sono consecutivi torno indietro di uno per rispettare
                    ' la distanza massima
                    PFin = PFin - 1
                    PS = PDebut + Int((PFin - PDebut) / 2)
                End If
        End If
        If PFin = PDebut + 2 Then
            ' Se i tre punti sono consecutivi la terna � Pdebut PS PFin � OK
            PS = PDebut + Int((PFin - PDebut) / 2)
        '   CALCOLO_SCARTO_MASSIMO ErMax, X(), Y(), PDebut, PFin, R2(), A(), B(), MODO, Risoluzione, R0
        '   If (ErMax > ERRORE_MASSIMO) Then ERRORE_MASSIMO = ErMax
            TERNAOK = True
        Else
            ' I tre punti non sono consecutivi, se l'errore � OK altrimenti torno indietro
            PS = PDebut + Int((PFin - PDebut) / 2)
            Call CALCOLO_SCARTO_MASSIMO(ErMax, X(), Y(), PDebut, PFin, R2(), A(), B(), MODO, Risoluzione, R0)
            If ErMax < Risoluzione Then
                TERNAOK = True
            Else
                PFin = PFin - 1
                PS = PDebut + Int((PFin - PDebut) / 2)
            End If
        End If
        If TERNAOK Then
            ' Verifico se il punto da mantenere si trova nell'ultima terna...
            If (PDebut < PMant) And (PFin > PMant) Then
                ' ...e se posso prenderlo come medio
                Call CALCOLO_SCARTO_MASSIMO_TEST(ErMax, X(), Y(), PDebut, PMant, PFin, R2(), A(), B(), MODO, Risoluzione, R0)
                If ErMax < Risoluzione Then
                    PS = PMant
                Else
                    TERNAOK = False
                End If
            End If
        End If
    Wend
    ' Memorizzo la terna scelta
    ReDim Preserve TERNE(3, UBound(TERNE, 2) + 1)
    TERNE(1, UBound(TERNE, 2)) = PDebut: TERNE(2, UBound(TERNE, 2)) = PS: TERNE(3, UBound(TERNE, 2)) = PFin
    
    ' Se il punto finale � il penultimo, cerco una terna con almeno un punto non scelto
    ' diminuisco il suo punto finale e scelgo la terna
    If (PFin = PFi - 1) Then
        i = UBound(TERNE, 2)
        TERNAOK = False
        While (i > 0) And (TERNAOK = False)
            If TERNE(3, i) - TERNE(1, i) > 2 Then
                TERNE(3, i) = TERNE(3, i) - 1
                PDebut = TERNE(1, i): PFin = TERNE(3, i)
'************** Diminuendo il punto finale potrei avere PS=Pfin ricalcolo PS solo in questo caso
'               perch� negli altri casi PS potrebbe essere il punto che voglio mantenere
                If PS = PFin Then
                    PS = PDebut + Int((PFin - PDebut) / 2)
                    TERNE(2, i) = PS
                Else
                    PS = TERNE(2, i)
                End If
'*********************************
                ReDim Preserve TERNE(3, i)
                Call CALCOLO_SCARTO_MASSIMO_TEST(ErMax, X(), Y(), PDebut, PS, PFin, R2(), A(), B(), MODO, Risoluzione, R0)
                If ErMax < Risoluzione Then
                    TERNAOK = True
                Else
                    MsgBox ("Error F" & Fianco)
                End If
            End If
            i = i - 1
        Wend
    End If
    PDebut = PFin
    PFin = PDebut + 2
    PS = PDebut + Int((PFin - PDebut) / 2)
Wend

' Memorizzo le terne scelte nei vettori N, XR, YR, RR
For i = 1 To UBound(TERNE, 2)
    PDebut = TERNE(1, i)
    PS = TERNE(2, i)
    PFin = TERNE(3, i)
    Call CALCOLO_SCARTO_MASSIMO_TEST(ErMax, X(), Y(), PDebut, PS, PFin, R2(), A(), B(), MODO, Risoluzione, R0)
    If j = 1 Then
        XR(j) = X(PDebut): YR(j) = Y(PDebut): RR(j) = R0: N(j) = PDebut
    End If
    DDY = Abs(Y(PFin) - Y(PDebut))
    DDX = Abs(X(PFin) - X(PDebut))
    If (DDY > DDY_RID) Then DDY_RID = DDY
    If (DDX > DDX_RID) Then DDX_RID = DDX
    j = j + 1
    XR(j) = X(PS): YR(j) = Y(PS): RR(j) = R0: N(j) = PS
    j = j + 1
    XR(j) = X(PFin): YR(j) = Y(PFin): RR(j) = R0: N(j) = PFin
Next i

ReDim Preserve N(j)
ReDim Preserve XR(j)
ReDim Preserve YR(j)
ReDim Preserve RR(j)

'************
Dim i3      As Integer
Dim IndEscl As Integer
Dim EEMin   As Double
Dim EEmax   As Double
Dim EETmp   As Double
Dim sca()   As Integer
Dim Norig() As Integer

EEMin = X(UBound(X))
'If (PFi - PIn + 1) Mod 2 = 0 Then
'    PARI = True
'End If
If JRid = 0 Then JRid = 1
If (N(UBound(N)) = PFi - 1) Then
'    For i = JRid + 1 To UBound(N)
'        DDTmp = Sqr((Y(N(i)) - Y(N(i - 1))) ^ 2 + (X(N(i)) - X(N(i - 1))) ^ 2)
'        If (DDTmp < DDMin) And (N(i) <> PMant) Then
'            DDMin = DDTmp
'            IndEscl = N(i)
'        End If
'    Next i
    Norig = N
    For i2 = N(JRid + 1) To N(j)
        N = Norig
        For i = i2 To UBound(N)
            N(i) = N(i) + 1
        Next i
        EEmax = 0
        For i = JRid To (j - 1) / 2
            Call CALCOLO_TERNA(i, N(), PDebut, PS, PFin, sca())
            Call CALCOLO_SCARTO_MASSIMO_TEST(EETmp, X(), Y(), PDebut, PS, PFin, R2(), A(), B(), MODO, Risoluzione, R0)
            If EEmax < EETmp Then
                    EEmax = EETmp
            End If
        Next i
        If EEmax < EEMin Then
            EEMin = EEmax
            IndEscl = i2
        End If
    Next i2
    'StopRegieEvents
    'MsgBox ("Point with min error is (" & Fianco & "): " & IndEscl & " -> " & frmt(EEMin, 4))
    'ResumeRegieEvents
    N = Norig
    For i = IndEscl To UBound(N)
        N(i) = N(i) + 1
    Next i
End If
'************
JRid = j

If DDX_RID > DDX_MASSIMO Then DDX_MASSIMO = DDX_RID
If DDY_RID > DDY_MASSIMO Then DDY_MASSIMO = DDY_RID

End Sub

Sub MODIFICA_PUNTI(X() As Double, Y() As Double, PIn As Integer, PFi As Integer, _
                   MODO As Integer, ErMax As Double, R2() As Double, A() As Double, B() As Double, ERRORE_MASSIMO As Single)
'
Dim CONTINUA As String
Dim DDX As Double
Dim DDY As Double
Dim stmp As String
Dim lRet As Integer
Dim PS As Integer
Dim R0 As Double
  '
  If (MODO = 3) Then
    '
    CONTINUA = "Y"  'RIDUZIONE MANUALE DEI PUNTI
    '
    Do
      '
      stmp = "CURRENT P1=" & Format$(PIn, "######0")
      stmp = stmp & Chr(13) & Chr(13)
      stmp = stmp & "INPUT P2"
      stmp = InputBox(stmp, " dY=" & frmt(Abs(Y(PFi) - Y(PIn)), 4) & " dX=" & frmt(Abs(X(PFi) - X(PIn)), 4), PFi)
      stmp = Trim$(stmp)
      If (val(stmp) > 0) Then PFi = val(stmp)
      '
      If (PFi < PIn + 2) Then PFi = PIn + 2
      '
      'CALCOLO INDICE DEL PUNTO MEDIO
      PS = PIn + Int((PFi - PIn) / 2)
      '
      
      DDY = Abs(Y(PFi) - Y(PIn))
      DDX = Abs(X(PFi) - X(PIn))
      CALCOLO_SCARTO_MASSIMO ErMax, X(), Y(), PIn, PFi, R2(), A(), B(), MODO, ERRORE_MASSIMO, R0
      '
      stmp = "CIRCLE ON POINTS: " & Format$(PIn, "######0") & ", "
      stmp = stmp & Format$(PS, "######0") & ", "
      stmp = stmp & Format$(PFi, "######0") & ", "
      stmp = stmp & Chr(13) & Chr(13)
      stmp = stmp & " CONTINUE ?"
      '
      lRet = MsgBox(stmp, vbQuestion + vbYesNo + vbDefaultButton2, "WARNING")
      '
      If (lRet = vbYes) Then
        '
        CONTINUA = "Y"
        '
      Else
        '
        CONTINUA = "N"
        '
      End If
      '
    Loop While (CONTINUA = "Y")
    '
  Else
    '
    'RIDUZIONE AUTOMATICA DEI PUNTI
    '
  End If
  '
End Sub


Sub CALCOLO_RAGGIO_TERNA_MOLA_JPG(PInX As Double, PInY As Double, _
                                  PMeX As Double, PMeY As Double, _
                                  PFiX As Double, PFiY As Double, _
                                  Xc As Double, YC As Double, R0 As Double)
Dim X1 As Double
Dim X2 As Double
Dim X3 As Double
Dim X4 As Double
Dim X5 As Double
Dim X8 As Double
Dim X9 As Double
Dim Y1 As Double
Dim Y2 As Double
Dim Y3 As Double
Dim Y4 As Double
Dim Y5 As Double
Dim Y8 As Double
Dim Y9 As Double
Dim M1 As Double
Dim M2 As Double
Dim Q1 As Double
Dim Q2 As Double

Dim A1 As Double
Dim A2 As Double


  'CALCOLO RAGGIO
  X1 = PInX: X3 = PMeX: X5 = PFiX
  Y1 = PInY: Y3 = PMeY: Y5 = PFiY
  If Y1 = Y3 Then Y1 = Y3 + 0.00001
  If Y5 = Y3 Then Y5 = Y3 + 0.00001
  M1 = -((X3 - X1) / (Y3 - Y1))
  M2 = -((X5 - X3) / (Y5 - Y3))
  X8 = (X1 + X3) / 2: Y8 = (Y1 + Y3) / 2
  X9 = (X5 + X3) / 2: Y9 = (Y5 + Y3) / 2
  Q1 = Y8 - M1 * X8: Q2 = Y9 - M2 * X9
  If M1 = M2 Then M1 = M1 + 0.00001
  Xc = (Q2 - Q1) / (M1 - M2): YC = M1 * Xc + Q1
  R0 = Sqr((Xc - X3) ^ 2 + (YC - Y3) ^ 2)
'CALCOLO ANGOLO AL CENTRO
  'Q1=quadrante I-2     Q2=quadrante I
  If PInX >= Xc And PInY >= YC Then Q1 = 1
  If PInX >= Xc And PInY < YC Then Q1 = 4
  If PInX < Xc And PInY > YC Then Q1 = 2
  If PInX < Xc And PInY < YC Then Q1 = 3
  If PFiX >= Xc And PFiY >= YC Then Q2 = 1
  If PFiX >= Xc And PFiY < YC Then Q2 = 4
  If PFiX < Xc And PFiY > YC Then Q2 = 2
  If PFiX < Xc And PFiY < YC Then Q2 = 3
  If PInX = Xc Then
    A1 = PG / 2
  Else
    A1 = Atn((PInY - YC) / (PInX - Xc))
  End If
  If Q1 = 3 Then A1 = A1 + PG
  If Q1 = 2 Then A1 = A1 + PG
  If Q1 = 4 Then A1 = A1 + 2 * PG
  If PFiX = Xc Then
    A2 = PG / 2
  Else
    A2 = Atn((PFiY - YC) / (PFiX - Xc))
  End If
  If Q2 = 3 Then A2 = A2 + PG
  If Q2 = 2 Then A2 = A2 + PG
  If Q2 = 4 Then A2 = A2 + 2 * PG
  If A1 < 0 Then A1 = A1 + 2 * PG
  If A2 < 0 Then A2 = A2 + 2 * PG
  A1 = A1 * 180 / PG: A2 = A2 * 180 / PG
  '
  If A1 > A2 Then R0 = -R0
  '

End Sub




Sub CALCOLO_SCARTO_MASSIMO(ErMax As Double, X() As Double, Y() As Double, _
                           PIn As Integer, PFi As Integer, R2() As Double, _
                           A() As Double, B() As Double, MODO As Integer, ErroreMassimo As Single, R0 As Double)

Dim Risultato As String
Dim Titolo    As String
Dim ECX       As Double
Dim k         As Integer
Dim jj        As Integer

Dim Xc        As Double
Dim YC        As Double
'Dim R0        As Double
Dim PMe       As Integer

Dim DDX       As Double
Dim DDY       As Double

PMe = PIn + Int((PFi - PIn) / 2)

CALCOLO_RAGGIO_TERNA_MOLA_JPG X(PIn), Y(PIn), X(PMe), Y(PMe), X(PFi), Y(PFi), Xc, YC, R0

  '
  Titolo = ""
  Titolo = Titolo & "R =" & frmt(R0, 4) & " "
  Titolo = Titolo & "Xc=" & frmt(Xc, 4) & " "
  Titolo = Titolo & "Yc=" & frmt(YC, 4)
  Risultato = "Nr.       ERROR" & Chr(13) & Chr(13)
  '
  k = 1:  ErMax = 0
  For jj = PIn To PFi
    '
    ECX = (Sqr((Xc - X(jj)) ^ 2 + (YC - Y(jj)) ^ 2) - Abs(R0)) * 1000
    '
    If (Abs(ECX) > ErMax) Then ErMax = Abs(ECX)
    '
    Risultato = Risultato & Format$(jj, "###0")
    '
    If (jj = PIn) Then
      Risultato = Risultato & " (P1)"
    ElseIf (jj = PMe) Then
      Risultato = Risultato & " (PS)"
    ElseIf (jj = PFi) Then
      Risultato = Risultato & " (P2)"
    Else
      Risultato = Risultato & "   ||"
    End If
    '
    Risultato = Risultato & " --> "
    If (ECX >= 0) Then
      Risultato = Risultato & "+" & frmt0(Abs(ECX), 4)
    Else
      Risultato = Risultato & "-" & frmt0(Abs(ECX), 4)
    End If
    '
    If (jj = PIn) Or (jj = PMe) Or (jj = PFi) Then
      Risultato = Risultato & " " & frmt0(R2(jj), 4) & " " & frmt0(A(jj), 4) & " " & frmt0(B(jj), 4)
    End If
    '
    Risultato = Risultato & Chr(13)
    '
    k = k + 1
    '
  Next jj
  '
  DDY = Abs(Y(PFi) - Y(PIn))
  DDX = Abs(X(PFi) - X(PIn))
  '
  Risultato = Risultato & Chr(13)
  Risultato = Risultato & "P1: " & Format$(PIn, "####0") & " "
  Risultato = Risultato & "PS: " & Format$(PMe, "####0") & " "
  Risultato = Risultato & "P2: " & Format$(PFi, "####0")
  Risultato = Risultato & " --> Max err (micron) : " & frmt(ErMax, 4) & Chr(13) & Chr(13)
  Risultato = Risultato & "Dy: " & frmt(DDY, 4) & "mm Dx: " & frmt(DDX, 4) & "mm" & Chr(13)
  '
  'If (MODO >= 2) Then
  '  '
  '  MsgBox Risultato, 64, Titolo
  '  '
  'End If
  '
Exit Sub


End Sub

'Sub ANGOLI_CENTRO(PInX As Double, PInY As Double, PFiX As Double, PFiY As Double, _
'                                  Xc As Double, YC As Double, R0 As Double, _
'                                  A1 As Double, A2 As Double)
'Dim Q1 As Double
'Dim Q2 As Double
'
'  If PInX >= Xc And PInY >= YC Then Q1 = 1
'  If PInX >= Xc And PInY < YC Then Q1 = 4
'  If PInX < Xc And PInY > YC Then Q1 = 2
'  If PInX < Xc And PInY < YC Then Q1 = 3
'  If PFiX >= Xc And PFiY >= YC Then Q2 = 1
'  If PFiX >= Xc And PFiY < YC Then Q2 = 4
'  If PFiX < Xc And PFiY > YC Then Q2 = 2
'  If PFiX < Xc And PFiY < YC Then Q2 = 3
'  If PInX = Xc Then
'    A1 = PG / 2
'  Else
'    A1 = Atn((PInY - YC) / (PInX - Xc))
'  End If
'  If Q1 = 3 Then A1 = A1 + PG
'  If Q1 = 2 Then A1 = A1 + PG
'  If Q1 = 4 Then A1 = A1 + 2 * PG
'  If PFiX = Xc Then
'    A2 = PG / 2
'  Else
'    A2 = Atn((PFiY - YC) / (PFiX - Xc))
'  End If
'  If Q2 = 3 Then A2 = A2 + PG
'  If Q2 = 2 Then A2 = A2 + PG
'  If Q2 = 4 Then A2 = A2 + 2 * PG
'  If A1 < 0 Then A1 = A1 + 2 * PG
'  If A2 < 0 Then A2 = A2 + 2 * PG
'
'End Sub

Sub CALCOLO_SCARTO_MASSIMO_TEST(ErMax As Double, X() As Double, Y() As Double, _
                           PIn As Integer, PMe As Integer, PFi As Integer, R2() As Double, _
                           A() As Double, B() As Double, MODO As Integer, ErroreMassimo As Single, R0 As Double)

Dim Risultato As String
Dim Titolo    As String
Dim ECX       As Double
Dim k         As Integer
Dim jj        As Integer

Dim Xc        As Double
Dim YC        As Double

Dim DDX       As Double
Dim DDY       As Double


    CALCOLO_RAGGIO_TERNA_MOLA_JPG X(PIn), Y(PIn), X(PMe), Y(PMe), X(PFi), Y(PFi), Xc, YC, R0

  '
  Titolo = ""
  Titolo = Titolo & "R =" & frmt(R0, 4) & " "
  Titolo = Titolo & "Xc=" & frmt(Xc, 4) & " "
  Titolo = Titolo & "Yc=" & frmt(YC, 4)
  Risultato = "Nr.       ERROR" & Chr(13) & Chr(13)
  '
  k = 1:  ErMax = 0
  For jj = PIn To PFi
    '
    ECX = (Sqr((Xc - X(jj)) ^ 2 + (YC - Y(jj)) ^ 2) - Abs(R0)) * 1000
    
    'ReDim Preserve Gerr(jj)
    'If (jj <> PIn) And (jj <> PMe) And (jj <> PFi) Then
     '   Gerr(jj) = Abs(ECX)
    'End If
    
    
    '
    If (Abs(ECX) > ErMax) Then ErMax = Abs(ECX)
    '
    Risultato = Risultato & Format$(jj, "###0")
    '
    If (jj = PIn) Then
      Risultato = Risultato & " (P1)"
    ElseIf (jj = PMe) Then
      Risultato = Risultato & " (PS)"
    ElseIf (jj = PFi) Then
      Risultato = Risultato & " (P2)"
    Else
      Risultato = Risultato & "   ||"
    End If
    '
    Risultato = Risultato & " --> "
    If (ECX >= 0) Then
      Risultato = Risultato & "+" & frmt0(Abs(ECX), 4)
    Else
      Risultato = Risultato & "-" & frmt0(Abs(ECX), 4)
    End If
    '
    If (jj = PIn) Or (jj = PMe) Or (jj = PFi) Then
      Risultato = Risultato & " " & frmt0(R2(jj), 4) & " " & frmt0(A(jj), 4) & " " & frmt0(B(jj), 4)
    End If
    '
    Risultato = Risultato & Chr(13)
    '
    k = k + 1
    '
  Next jj
  '
  DDY = Abs(Y(PFi) - Y(PIn))
  DDX = Abs(X(PFi) - X(PIn))
  '
  Risultato = Risultato & Chr(13)
  Risultato = Risultato & "P1: " & Format$(PIn, "####0") & " "
  Risultato = Risultato & "PS: " & Format$(PMe, "####0") & " "
  Risultato = Risultato & "P2: " & Format$(PFi, "####0")
  Risultato = Risultato & " --> Max err (micron) : " & frmt(ErMax, 4) & Chr(13) & Chr(13)
  Risultato = Risultato & "Dy: " & frmt(DDY, 4) & "mm Dx: " & frmt(DDX, 4) & "mm" & Chr(13)
  '
  'If (MODO >= 2) Then
  '  '
  '  MsgBox Risultato, 64, Titolo
  '  '
  'End If
  '
Exit Sub

End Sub


Sub RIDUZIONE_TOT(PuntoSeparazione As Integer, PMant1 As Integer, PMant2 As Integer, _
                X() As Double, Y() As Double, TG() As Double, _
                MaxDisPunti As Single, R2() As Double, _
                A() As Double, B() As Double, MODO As Integer, _
                Risoluzione As Single, Gerr() As Double, _
                XR() As Double, YR() As Double, RR() As Double, N() As Integer, _
                ERRORE_MASSIMO As Double, DDX_MASSIMO As Double, DDY_MASSIMO As Double, _
                DDX_MASSIMO_1 As Double, DDY_MASSIMO_1 As Double, _
                DDX_MASSIMO_2 As Double, DDY_MASSIMO_2 As Double, _
                NRidTot As Integer, NRidF1 As Integer, NRidF2 As Integer)


Dim N_2()     As Integer
Dim X_2()     As Double
Dim Y_2()     As Double
Dim XR_2()    As Double
Dim YR_2()    As Double
Dim RR_2()    As Double
Dim R2_2()    As Double
Dim A_2()     As Double
Dim B_2()     As Double
Dim Gerr_1()  As Double
Dim i         As Integer
Dim i2        As Integer
Dim j         As Integer
Dim tj        As Integer
Dim NmaxI     As Integer
Dim PFin      As Integer
Dim PDebut    As Integer
Dim PS        As Integer
Dim stmp      As String
'
Dim XX As Double
Dim YY As Double
Dim TT As Double
Dim R0 As Double
Dim Xc As Double
Dim YC As Double

'
Dim Ntot    As Integer
Dim ErMax   As Double
Dim NRid    As Integer
'
Dim YYmax   As Double
Dim iYYmax  As Integer
Dim YYmax2  As Double
Dim iYYmax2 As Integer

Dim tPIn  As Integer
Dim tPFi  As Integer
Dim tPMe  As Integer
Dim sca() As Integer
Dim Ti    As Integer

Dim PEscluso1 As Integer
Dim PEscluso2 As Integer

Dim DueMax  As Boolean
Dim tdis    As Double
Dim k       As Integer
  '
  DDX_MASSIMO = 0
  DDY_MASSIMO = 0
  DDX_MASSIMO_2 = 0
  DDY_MASSIMO_2 = 0
  '
  Ntot = UBound(X)
  '
  YYmax = Y(1): iYYmax = 1
  YYmax2 = Y(Ntot): iYYmax2 = Ntot
  '
  DueMax = False
  For i = 1 To UBound(X)
    If (YYmax < Y(i)) Then
      YYmax = Y(i)
      iYYmax = i
    End If
    If (YYmax2 < Y(Ntot - i + 1)) Then
      YYmax2 = Y(Ntot - i + 1)
      iYYmax2 = i
    End If
  Next i
  tdis = Sqr((X(PuntoSeparazione) - X(Ntot - iYYmax2 + 1)) ^ 2 + (Y(PuntoSeparazione) - Y(Ntot - iYYmax2 + 1)) ^ 2)
  If (Ntot - PuntoSeparazione + 1 - iYYmax2 > 1) And (iYYmax = PuntoSeparazione) And (iYYmax2 = PuntoSeparazione) And (tdis > 0.01) Then
    DueMax = True
  End If
  '
  'EVITA DI RIMUOVERE DUE PUNTI QUANDO NON SI RIDUCE SVG220114
  PEscluso1 = -2
  PEscluso2 = -2
  '
If Not DueMax Then
  '
  ' Riduzione profilo con un solo massimo
  '
  ' RIDUZIONE PUNTI DEL FIANCO 1
  '
  j = 0
  '
  Call RIDUZIONE_TEST(1, PuntoSeparazione, PMant1, j, X(), Y(), MaxDisPunti, R2(), A(), B(), MODO, Risoluzione, "1", XR(), YR(), RR(), N(), ERRORE_MASSIMO, DDX_MASSIMO_1, DDY_MASSIMO_1)
  '
  NRidF1 = j
  '
  ' RIDUZIONE PUNTI DEL FIANCO 2
  '
  Call Inv_Dbl(X(), X_2())
  Call Inv_Dbl(Y(), Y_2())
  Call Inv_Dbl(R2(), R2_2())
  Call Inv_Dbl(A(), A_2())
  Call Inv_Dbl(B(), B_2())
  Call Inv_Dbl(XR(), XR_2())
  Call Inv_Dbl(YR(), YR_2())
  Call Inv_Dbl(RR(), RR_2())
  '
  Call RIDUZIONE_TEST(1, Ntot - PuntoSeparazione + 1, PMant2, j, X_2(), Y_2(), MaxDisPunti, R2_2(), A_2(), B_2(), MODO, Risoluzione, "2", XR_2(), YR_2(), RR_2(), N_2(), ERRORE_MASSIMO, DDX_MASSIMO_2, DDY_MASSIMO_2)
  '
Else

  '
  ' Riduzione profilo con due massimi
  '
  ' RIDUZIONE PUNTI DEL FIANCO 1
  '
  j = 0
  '
  stmp = GetInfo("Configurazione", "RIDUZIONE_LEISTRITZ", Path_LAVORAZIONE_INI)
  stmp = UCase$(Trim$(stmp))
  If (stmp = "Y") Then
      If (MODO = 0) Then
  
    PEscluso1 = PuntoSeparazione
    PEscluso2 = Ntot - PuntoSeparazione + 1
  
    PuntoSeparazione = PuntoSeparazione - 1 - 1
  
  Else
  
    PEscluso1 = PuntoSeparazione
    PEscluso2 = Ntot - PuntoSeparazione + 1
  
    PuntoSeparazione = PuntoSeparazione - 1
  
  End If
  End If
  '
  Call RIDUZIONE_TEST(1, PuntoSeparazione, PMant1, j, X(), Y(), MaxDisPunti, R2(), A(), B(), MODO, Risoluzione, "1a", XR(), YR(), RR(), N(), ERRORE_MASSIMO, DDX_MASSIMO_1, DDY_MASSIMO_1)
  Call RIDUZIONE_TEST(PuntoSeparazione, (Ntot + 1) \ 2, PMant1, j, X(), Y(), MaxDisPunti, R2(), A(), B(), MODO, Risoluzione, "1b", XR(), YR(), RR(), N(), ERRORE_MASSIMO, DDX_MASSIMO_1, DDY_MASSIMO_1)
  '
  NRidF1 = j
  '
  'RIDUZIONE PUNTI DEL FIANCO 2
  '
  Call Inv_Dbl(X(), X_2())
  Call Inv_Dbl(Y(), Y_2())
  Call Inv_Dbl(R2(), R2_2())
  Call Inv_Dbl(A(), A_2())
  Call Inv_Dbl(B(), B_2())
  Call Inv_Dbl(XR(), XR_2())
  Call Inv_Dbl(YR(), YR_2())
  Call Inv_Dbl(RR(), RR_2())
  '
  Call RIDUZIONE_TEST(1, PuntoSeparazione, PMant2, j, X_2(), Y_2(), MaxDisPunti, R2_2(), A_2(), B_2(), MODO, Risoluzione, "2a", XR_2(), YR_2(), RR_2(), N_2(), ERRORE_MASSIMO, DDX_MASSIMO_2, DDY_MASSIMO_2)
  Call RIDUZIONE_TEST(PuntoSeparazione, (Ntot + 1) \ 2, PMant2, j, X_2(), Y_2(), MaxDisPunti, R2_2(), A_2(), B_2(), MODO, Risoluzione, "2b", XR_2(), YR_2(), RR_2(), N_2(), ERRORE_MASSIMO, DDX_MASSIMO_2, DDY_MASSIMO_2)
  '
End If
  '
  NRidF2 = j
  '
  Call Add_Int(N(), N_2(), Ntot)
  Call Add_Dbl(XR(), XR_2())
  Call Add_Dbl(YR(), YR_2())
  Call Add_Dbl(RR(), RR_2())
  '
  ERRORE_MASSIMO = 0
  NRidTot = UBound(N)
  NRid = UBound(N)
  ReDim Gerr(Ntot) 'Calcolo errore su ogni punto in base alle scelte fatte
  For i = 1 To (NRid - 1) / 2
    Call CALCOLO_TERNA(i, N(), PDebut, PS, PFin, sca())
    Call CALCOLO_RAGGIO_TERNA_MOLA_JPG(X(PDebut), Y(PDebut), X(PS), Y(PS), X(PFin), Y(PFin), Xc, YC, R0)
    For i2 = PDebut To PFin
        Gerr(i2) = Abs((Sqr((Xc - X(i2)) ^ 2 + (YC - Y(i2)) ^ 2) - Abs(R0)) * 1000)
        If ERRORE_MASSIMO < Gerr(i2) Then
            ERRORE_MASSIMO = Gerr(i2)
        End If
    Next i2
  Next i
  GX = X
  GY = Y

  'GN = N
  ReDim GN(0)
  ReDim XR_2(0)
  ReDim YR_2(0)
  ReDim RR_2(0)
  '
  k = 0
  '
      If MODO = 0 Then
    
    For i = 0 To UBound(N)
      Select Case N(i)
        Case PEscluso1, PEscluso2
        Case PEscluso1 - 1, PEscluso2 + 1
          '
        Case Else
          ReDim Preserve GN(k)
          ReDim Preserve XR_2(k)
          ReDim Preserve YR_2(k)
          ReDim Preserve RR_2(k)
          GN(k) = N(i)
          XR_2(k) = XR(i)
          YR_2(k) = YR(i)
          RR_2(k) = RR(i)
          k = k + 1
      End Select
    Next i
    
  Else
  
    For i = 0 To UBound(N)
      Select Case N(i)
        Case PEscluso1, PEscluso2
          '
        Case Else
          ReDim Preserve GN(k)
          ReDim Preserve XR_2(k)
          ReDim Preserve YR_2(k)
          ReDim Preserve RR_2(k)
          GN(k) = N(i)
          XR_2(k) = XR(i)
          YR_2(k) = YR(i)
          RR_2(k) = RR(i)
          k = k + 1
      End Select
    Next i
        
  End If
  '
  XR = XR_2
  YR = YR_2
  RR = RR_2
  N = GN
  GR = R2
  GA = A
  GB = B
  GTG = TG
  Gim = PuntoSeparazione
  DDX_MASSIMO = DDX_MASSIMO_1
  DDY_MASSIMO = DDY_MASSIMO_1
  If DDX_MASSIMO_2 > DDX_MASSIMO Then DDX_MASSIMO = DDX_MASSIMO_2
  If DDY_MASSIMO_2 > DDY_MASSIMO Then DDY_MASSIMO = DDY_MASSIMO_2
  '
End Sub

Sub RIDUZIONE_TOT_CORRENTE(PuntoSeparazione As Integer, PMant1 As Integer, PMant2 As Integer, _
                X() As Double, Y() As Double, TG() As Double, _
                MaxDisPunti As Single, R2() As Double, _
                A() As Double, B() As Double, MODO As Integer, _
                Risoluzione As Single, Gerr() As Double, _
                XR() As Double, YR() As Double, RR() As Double, N() As Integer, _
                ERRORE_MASSIMO As Double, DDX_MASSIMO As Double, DDY_MASSIMO As Double, _
                DDX_MASSIMO_1 As Double, DDY_MASSIMO_1 As Double, _
                DDX_MASSIMO_2 As Double, DDY_MASSIMO_2 As Double, _
                NRidTot As Integer, NRidF1 As Integer, NRidF2 As Integer)


Dim N_2()   As Integer
Dim X_2()   As Double
Dim Y_2()   As Double
Dim XR_2()  As Double
Dim YR_2()  As Double
Dim RR_2()  As Double
Dim R2_2()  As Double
Dim A_2()   As Double
Dim B_2()   As Double
Dim Gerr_1() As Double
Dim i       As Integer
Dim i2      As Integer
Dim j       As Integer
Dim tj      As Integer
Dim NmaxI   As Integer
Dim PFin    As Integer
Dim PDebut  As Integer
Dim PS      As Integer
Dim stmp    As String
'
Dim XX As Double
Dim YY As Double
Dim TT As Double
Dim R0 As Double
Dim Xc As Double
Dim YC As Double
'
Dim Ntot    As Integer
Dim ErMax   As Double
Dim NRid    As Integer
'
Dim YYmax   As Double
Dim iYYmax  As Integer
Dim tPIn    As Integer
Dim tPFi    As Integer
Dim tPMe    As Integer
Dim sca()   As Integer
Dim Ti      As Integer

    DDX_MASSIMO = 0
    DDY_MASSIMO = 0
    DDX_MASSIMO_2 = 0
    DDY_MASSIMO_2 = 0
'
'RIDUZIONE PUNTI DEL FIANCO 1
'
j = 0
Ntot = UBound(X)
RIDUZIONE_TEST 1, PuntoSeparazione, PMant1, j, X(), Y(), MaxDisPunti, R2(), A(), B(), MODO, Risoluzione, "1", XR(), YR(), RR(), N(), ERRORE_MASSIMO, DDX_MASSIMO_1, DDY_MASSIMO_1

NRidF1 = j
'
'RIDUZIONE PUNTI DEL FIANCO 2
'
Inv_Dbl X(), X_2()
Inv_Dbl Y(), Y_2()
Inv_Dbl R2(), R2_2()
Inv_Dbl A(), A_2()
Inv_Dbl B(), B_2()
Inv_Dbl XR(), XR_2()
Inv_Dbl YR(), YR_2()
Inv_Dbl RR(), RR_2()

YYmax = Y_2(1): iYYmax = 1

For i = 1 To UBound(Y_2)
'Debug.Print i, Y(i), Y_2(i)
    If (Y_2(i) > YYmax) Then
        YYmax = Y_2(i): iYYmax = i
    End If
Next i
Dim tdis As Double
'If (iYYmax <> Ntot - PuntoSeparazione + 1) And (MsgBox("Dividere l'analisi in 3 parti per" & Chr(10) & Chr(13) & "mantenere la simmetria sui fianchi?", vbYesNo + vbQuestion, "Rilevati 2 massimi - F1: " & PuntoSeparazione & " F2: " & iYYmax) = vbYes) Then
'If (iYYmax <> Ntot - PuntoSeparazione + 1) And (iYYmax = PuntoSeparazione) Then

tdis = Sqr((X(PuntoSeparazione) - X_2(iYYmax)) ^ 2 + (Y(PuntoSeparazione) - Y_2(iYYmax)) ^ 2)

If (Ntot - PuntoSeparazione + 1 - iYYmax > 1) And (iYYmax = PuntoSeparazione) And (tdis > 0.01) Then
  iYYmax = iYYmax - 1
  ' Rilevata presenza di almeno due massimi
  RIDUZIONE_TEST 1, iYYmax, PMant2, j, X_2(), Y_2(), MaxDisPunti, R2_2(), A_2(), B_2(), MODO, Risoluzione, "2a", XR_2(), YR_2(), RR_2(), N_2(), ERRORE_MASSIMO, DDX_MASSIMO_2, DDY_MASSIMO_2
  RIDUZIONE_TEST iYYmax, Ntot - PuntoSeparazione + 1, PMant2, j, X_2(), Y_2(), MaxDisPunti, R2_2(), A_2(), B_2(), MODO, Risoluzione, "2b", XR_2(), YR_2(), RR_2(), N_2(), ERRORE_MASSIMO, DDX_MASSIMO_2, DDY_MASSIMO_2
Else
  RIDUZIONE_TEST 1, Ntot - PuntoSeparazione + 1, PMant2, j, X_2(), Y_2(), MaxDisPunti, R2_2(), A_2(), B_2(), MODO, Risoluzione, "2", XR_2(), YR_2(), RR_2(), N_2(), ERRORE_MASSIMO, DDX_MASSIMO_2, DDY_MASSIMO_2
End If
NRidF2 = j
Add_Int N(), N_2(), Ntot
Add_Dbl XR(), XR_2()
Add_Dbl YR(), YR_2()
Add_Dbl RR(), RR_2()

ERRORE_MASSIMO = 0
NRidTot = UBound(N)
NRid = UBound(N)
ReDim Gerr(Ntot) 'Calcolo errore su ogni punto in base alle scelte fatte
For i = 1 To (NRid - 1) / 2
    CALCOLO_TERNA i, N(), PDebut, PS, PFin, sca()
    CALCOLO_RAGGIO_TERNA_MOLA_JPG X(PDebut), Y(PDebut), X(PS), Y(PS), X(PFin), Y(PFin), Xc, YC, R0
    For i2 = PDebut To PFin
        Gerr(i2) = Abs((Sqr((Xc - X(i2)) ^ 2 + (YC - Y(i2)) ^ 2) - Abs(R0)) * 1000)
        If ERRORE_MASSIMO < Gerr(i2) Then
            ERRORE_MASSIMO = Gerr(i2)
        End If
    Next i2
Next i
GX = X
GY = Y
GN = N
GR = R2
GA = A
GB = B
GTG = TG
Gim = PuntoSeparazione
DDX_MASSIMO = DDX_MASSIMO_1
DDY_MASSIMO = DDY_MASSIMO_1
If DDX_MASSIMO_2 > DDX_MASSIMO Then DDX_MASSIMO = DDX_MASSIMO_2
If DDY_MASSIMO_2 > DDY_MASSIMO Then DDY_MASSIMO = DDY_MASSIMO_2

End Sub
'
Sub CALCOLO_TERNA(NT As Integer, N() As Integer, PIn As Integer, PMe As Integer, PFi As Integer, Scartati() As Integer)

Dim i As Integer
   
  ReDim Scartati(0)
  PIn = N(1 + (2 * (NT - 1)))
  PMe = N(2 + (2 * (NT - 1)))
  PFi = N(3 + (2 * (NT - 1)))
  For i = PIn + 1 To PMe - 1
    ReDim Preserve Scartati(UBound(Scartati) + 1)
    Scartati(UBound(Scartati)) = i
  Next i
  For i = PMe + 1 To PFi - 1
    ReDim Preserve Scartati(UBound(Scartati) + 1)
    Scartati(UBound(Scartati)) = i
  Next i
  
End Sub

Sub Inv_Dbl(Vin() As Double, VFi() As Double)

Dim i As Integer
  
  ReDim VFi(UBound(Vin))
  
  For i = 1 To UBound(Vin)
  VFi(UBound(Vin) - i + 1) = Vin(i)
  Next i
  
End Sub

Sub Add_Int(V1() As Integer, V2() As Integer, Ntot As Integer)

Dim i As Integer

  ReDim Preserve V1(UBound(V1) + UBound(V2) - 1)
  
  For i = 1 To UBound(V2) - 1 ' Escludo il punto max che � gi� in vin
    V1(UBound(V1) - i + 1) = Ntot + 1 - V2(i)
  Next i
  
End Sub

Sub Add_Dbl(V1() As Double, V2() As Double)

Dim i As Integer

  ReDim Preserve V1(UBound(V1) + UBound(V2) - 1)
  
  For i = 1 To UBound(V2) - 1 ' Escludo il punto max che � gi� in vin
    V1(UBound(V1) - i + 1) = V2(i)
  Next i

End Sub

