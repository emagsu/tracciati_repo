VERSION 5.00
Begin VB.Form CHK0 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BackColor       =   &H00EBE1D7&
   BorderStyle     =   0  'None
   Caption         =   "CHK0"
   ClientHeight    =   12420
   ClientLeft      =   2004
   ClientTop       =   1980
   ClientWidth     =   19308
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   13.2
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H80000008&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   12420
   ScaleWidth      =   19308
   ShowInTaskbar   =   0   'False
   Tag             =   "CHK0"
   Begin VB.Frame fraTOLLERANZA 
      BackColor       =   &H00EBE1D7&
      Caption         =   "fraTOLLERANZA"
      BeginProperty Font 
         Name            =   "MS Song"
         Size            =   10.2
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5000
      Left            =   8160
      TabIndex        =   17
      Top             =   6840
      Visible         =   0   'False
      Width           =   11640
      Begin VB.ListBox lstARCHIVIO 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3792
         Left            =   7920
         TabIndex        =   91
         Top             =   360
         Visible         =   0   'False
         Width           =   4830
      End
      Begin VB.CheckBox ChkBOMBATURA 
         Appearance      =   0  'Flat
         BackColor       =   &H00EBE1D7&
         Caption         =   "Crowning eva."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   13.8
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   400
         Left            =   300
         TabIndex        =   90
         Top             =   4200
         Width           =   2865
      End
      Begin VB.Frame fraFIANCO2 
         BackColor       =   &H00EBE1D7&
         Caption         =   "F2"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.4
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3600
         Left            =   300
         TabIndex        =   54
         Top             =   500
         Width           =   3500
         Begin VB.TextBox txtPXXF2 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   0
            Left            =   585
            TabIndex        =   78
            Text            =   "txtPXXF2"
            Top             =   2880
            Width           =   900
         End
         Begin VB.TextBox txtPXXF2 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   1
            Left            =   585
            TabIndex        =   77
            Text            =   "txtPXXF2"
            Top             =   2520
            Width           =   900
         End
         Begin VB.TextBox txtPXXF2 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   2
            Left            =   585
            TabIndex        =   76
            Text            =   "txtPXXF2"
            Top             =   2160
            Width           =   900
         End
         Begin VB.TextBox txtPXXF2 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   3
            Left            =   585
            TabIndex        =   75
            Text            =   "txtPXXF2"
            Top             =   1800
            Width           =   900
         End
         Begin VB.TextBox txtPXXF2 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   4
            Left            =   585
            TabIndex        =   74
            Text            =   "txtPXXF2"
            Top             =   1440
            Width           =   900
         End
         Begin VB.TextBox txtPXXF2 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   5
            Left            =   585
            TabIndex        =   73
            Text            =   "txtPXXF2"
            Top             =   1080
            Width           =   900
         End
         Begin VB.TextBox txtPXXF2 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   6
            Left            =   585
            TabIndex        =   72
            Text            =   "txtPXXF2"
            Top             =   720
            Width           =   900
         End
         Begin VB.TextBox txtPXXF2 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   7
            Left            =   585
            TabIndex        =   71
            Text            =   "txtPXXF2"
            Top             =   360
            Width           =   900
         End
         Begin VB.TextBox txtPYYF2 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   0
            Left            =   1505
            TabIndex        =   70
            Text            =   "txtPYYF2"
            Top             =   2880
            Width           =   900
         End
         Begin VB.TextBox txtPYYF2 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   1
            Left            =   1505
            TabIndex        =   69
            Text            =   "txtPYYF2"
            Top             =   2520
            Width           =   900
         End
         Begin VB.TextBox txtPYYF2 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   2
            Left            =   1505
            TabIndex        =   68
            Text            =   "txtPYYF2"
            Top             =   2160
            Width           =   900
         End
         Begin VB.TextBox txtPYYF2 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   3
            Left            =   1505
            TabIndex        =   67
            Text            =   "txtPYYF2"
            Top             =   1800
            Width           =   900
         End
         Begin VB.TextBox txtPYYF2 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   4
            Left            =   1505
            TabIndex        =   66
            Text            =   "txtPYYF2"
            Top             =   1440
            Width           =   900
         End
         Begin VB.TextBox txtPYYF2 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   5
            Left            =   1505
            TabIndex        =   65
            Text            =   "txtPYYF2"
            Top             =   1080
            Width           =   900
         End
         Begin VB.TextBox txtPYYF2 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   6
            Left            =   1505
            TabIndex        =   64
            Text            =   "txtPYYF2"
            Top             =   720
            Width           =   900
         End
         Begin VB.TextBox txtPYYF2 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   7
            Left            =   1505
            TabIndex        =   63
            Text            =   "txtPYYF2"
            Top             =   360
            Width           =   900
         End
         Begin VB.TextBox txtPBBF2 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   6
            Left            =   2425
            TabIndex        =   62
            Text            =   "txtPBBF2"
            Top             =   540
            Width           =   900
         End
         Begin VB.TextBox txtPBBF2 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   5
            Left            =   2425
            TabIndex        =   61
            Text            =   "txtPBBF2"
            Top             =   900
            Width           =   900
         End
         Begin VB.TextBox txtPBBF2 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   4
            Left            =   2425
            TabIndex        =   60
            Text            =   "txtPBBF2"
            Top             =   1260
            Width           =   900
         End
         Begin VB.TextBox txtPBBF2 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   3
            Left            =   2425
            TabIndex        =   59
            Text            =   "txtPBBF2"
            Top             =   1620
            Width           =   900
         End
         Begin VB.TextBox txtPBBF2 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   2
            Left            =   2425
            TabIndex        =   58
            Text            =   "txtPBBF2"
            Top             =   1980
            Width           =   900
         End
         Begin VB.TextBox txtPBBF2 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   1
            Left            =   2425
            TabIndex        =   57
            Text            =   "txtPBBF2"
            Top             =   2340
            Width           =   900
         End
         Begin VB.TextBox txtPBBF2 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   0
            Left            =   2425
            TabIndex        =   56
            Text            =   "txtPBBF2"
            Top             =   2700
            Width           =   900
         End
         Begin VB.TextBox txtPBBF2 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   7
            Left            =   2425
            TabIndex        =   55
            Text            =   "txtPBBF2"
            Top             =   360
            Visible         =   0   'False
            Width           =   900
         End
         Begin VB.Label IF2 
            BackColor       =   &H00EBE1D7&
            Caption         =   "IF2"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   7
            Left            =   90
            TabIndex        =   89
            Top             =   360
            Width           =   3315
         End
         Begin VB.Label IF2 
            BackColor       =   &H00EBE1D7&
            Caption         =   "IF2"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   6
            Left            =   90
            TabIndex        =   88
            Top             =   720
            Width           =   3315
         End
         Begin VB.Label IF2 
            BackColor       =   &H00EBE1D7&
            Caption         =   "IF2"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   5
            Left            =   90
            TabIndex        =   87
            Top             =   1080
            Width           =   3315
         End
         Begin VB.Label IF2 
            BackColor       =   &H00EBE1D7&
            Caption         =   "IF2"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   4
            Left            =   90
            TabIndex        =   86
            Top             =   1440
            Width           =   3315
         End
         Begin VB.Label IF2 
            BackColor       =   &H00EBE1D7&
            Caption         =   "IF2"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   3
            Left            =   90
            TabIndex        =   85
            Top             =   1800
            Width           =   3315
         End
         Begin VB.Label IF2 
            BackColor       =   &H00EBE1D7&
            Caption         =   "IF2"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   2
            Left            =   90
            TabIndex        =   84
            Top             =   2160
            Width           =   3315
         End
         Begin VB.Label IF2 
            BackColor       =   &H00EBE1D7&
            Caption         =   "IF2"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   1
            Left            =   90
            TabIndex        =   83
            Top             =   2523
            Width           =   3315
         End
         Begin VB.Label IF2 
            BackColor       =   &H00EBE1D7&
            Caption         =   "IF2"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   0
            Left            =   90
            TabIndex        =   82
            Top             =   2880
            Width           =   3315
         End
         Begin VB.Label lblXX 
            Alignment       =   2  'Center
            BackColor       =   &H00EBE1D7&
            Caption         =   "XX"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   1
            Left            =   585
            TabIndex        =   81
            Top             =   3225
            Width           =   935
         End
         Begin VB.Label lblYY 
            Alignment       =   2  'Center
            BackColor       =   &H00EBE1D7&
            Caption         =   "YY"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   1
            Left            =   1505
            TabIndex        =   80
            Top             =   3225
            Width           =   935
         End
         Begin VB.Label lblBB 
            Alignment       =   2  'Center
            BackColor       =   &H00EBE1D7&
            Caption         =   "BB"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   1
            Left            =   2425
            TabIndex        =   79
            Top             =   3225
            Width           =   935
         End
      End
      Begin VB.Frame fraFIANCO1 
         BackColor       =   &H00EBE1D7&
         Caption         =   "F1"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.4
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3600
         Left            =   4000
         TabIndex        =   18
         Top             =   500
         Width           =   3500
         Begin VB.TextBox txtPXXF1 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   0
            Left            =   585
            TabIndex        =   42
            Text            =   "txtPXXF1"
            Top             =   2880
            Width           =   900
         End
         Begin VB.TextBox txtPXXF1 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   1
            Left            =   585
            TabIndex        =   41
            Text            =   "txtPXXF1"
            Top             =   2520
            Width           =   900
         End
         Begin VB.TextBox txtPXXF1 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   2
            Left            =   585
            TabIndex        =   40
            Text            =   "txtPXXF1"
            Top             =   2160
            Width           =   900
         End
         Begin VB.TextBox txtPXXF1 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   3
            Left            =   585
            TabIndex        =   39
            Text            =   "txtPXXF1"
            Top             =   1800
            Width           =   900
         End
         Begin VB.TextBox txtPXXF1 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   4
            Left            =   585
            TabIndex        =   38
            Text            =   "txtPXXF1"
            Top             =   1440
            Width           =   900
         End
         Begin VB.TextBox txtPXXF1 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   5
            Left            =   585
            TabIndex        =   37
            Text            =   "txtPXXF1"
            Top             =   1080
            Width           =   900
         End
         Begin VB.TextBox txtPXXF1 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   6
            Left            =   585
            TabIndex        =   36
            Text            =   "txtPXXF1"
            Top             =   720
            Width           =   900
         End
         Begin VB.TextBox txtPXXF1 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   7
            Left            =   585
            TabIndex        =   35
            Text            =   "txtPXXF1"
            Top             =   360
            Width           =   900
         End
         Begin VB.TextBox txtPYYF1 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   0
            Left            =   1505
            TabIndex        =   34
            Text            =   "txtPYYF1"
            Top             =   2880
            Width           =   900
         End
         Begin VB.TextBox txtPYYF1 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   1
            Left            =   1505
            TabIndex        =   33
            Text            =   "txtPYYF1"
            Top             =   2520
            Width           =   900
         End
         Begin VB.TextBox txtPYYF1 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   2
            Left            =   1505
            TabIndex        =   32
            Text            =   "txtPYYF1"
            Top             =   2160
            Width           =   900
         End
         Begin VB.TextBox txtPYYF1 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   3
            Left            =   1505
            TabIndex        =   31
            Text            =   "txtPYYF1"
            Top             =   1800
            Width           =   900
         End
         Begin VB.TextBox txtPYYF1 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   4
            Left            =   1505
            TabIndex        =   30
            Text            =   "txtPYYF1"
            Top             =   1440
            Width           =   900
         End
         Begin VB.TextBox txtPYYF1 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   5
            Left            =   1505
            TabIndex        =   29
            Text            =   "txtPYYF1"
            Top             =   1080
            Width           =   900
         End
         Begin VB.TextBox txtPYYF1 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   6
            Left            =   1505
            TabIndex        =   28
            Text            =   "txtPYYF1"
            Top             =   720
            Width           =   900
         End
         Begin VB.TextBox txtPYYF1 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   7
            Left            =   1505
            TabIndex        =   27
            Text            =   "txtPYYF1"
            Top             =   360
            Width           =   900
         End
         Begin VB.TextBox txtPBBF1 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   0
            Left            =   2425
            TabIndex        =   26
            Text            =   "txtPBBF1"
            Top             =   2700
            Width           =   900
         End
         Begin VB.TextBox txtPBBF1 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   1
            Left            =   2425
            TabIndex        =   25
            Text            =   "txtPBBF1"
            Top             =   2340
            Width           =   900
         End
         Begin VB.TextBox txtPBBF1 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   2
            Left            =   2425
            TabIndex        =   24
            Text            =   "txtPBBF1"
            Top             =   1980
            Width           =   900
         End
         Begin VB.TextBox txtPBBF1 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   3
            Left            =   2425
            TabIndex        =   23
            Text            =   "txtPBBF1"
            Top             =   1620
            Width           =   900
         End
         Begin VB.TextBox txtPBBF1 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   4
            Left            =   2425
            TabIndex        =   22
            Text            =   "txtPBBF1"
            Top             =   1260
            Width           =   900
         End
         Begin VB.TextBox txtPBBF1 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   5
            Left            =   2425
            TabIndex        =   21
            Text            =   "txtPBBF1"
            Top             =   900
            Width           =   900
         End
         Begin VB.TextBox txtPBBF1 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   6
            Left            =   2425
            TabIndex        =   20
            Text            =   "txtPBBF1"
            Top             =   540
            Width           =   900
         End
         Begin VB.TextBox txtPBBF1 
            Appearance      =   0  'Flat
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   10.2
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   372
            Index           =   7
            Left            =   2425
            TabIndex        =   19
            Text            =   "txtPBBF1"
            Top             =   360
            Visible         =   0   'False
            Width           =   900
         End
         Begin VB.Label IF1 
            BackColor       =   &H00EBE1D7&
            Caption         =   "IF1"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   7
            Left            =   90
            TabIndex        =   53
            Top             =   360
            Width           =   3315
         End
         Begin VB.Label IF1 
            BackColor       =   &H00EBE1D7&
            Caption         =   "IF1"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   6
            Left            =   90
            TabIndex        =   52
            Top             =   720
            Width           =   3315
         End
         Begin VB.Label IF1 
            BackColor       =   &H00EBE1D7&
            Caption         =   "IF1"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   5
            Left            =   90
            TabIndex        =   51
            Top             =   1080
            Width           =   3315
         End
         Begin VB.Label IF1 
            BackColor       =   &H00EBE1D7&
            Caption         =   "IF1"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   4
            Left            =   90
            TabIndex        =   50
            Top             =   1440
            Width           =   3315
         End
         Begin VB.Label IF1 
            BackColor       =   &H00EBE1D7&
            Caption         =   "IF1"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   3
            Left            =   90
            TabIndex        =   49
            Top             =   1800
            Width           =   3315
         End
         Begin VB.Label IF1 
            BackColor       =   &H00EBE1D7&
            Caption         =   "IF1"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   2
            Left            =   90
            TabIndex        =   48
            Top             =   2160
            Width           =   3315
         End
         Begin VB.Label IF1 
            BackColor       =   &H00EBE1D7&
            Caption         =   "IF1"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   1
            Left            =   90
            TabIndex        =   47
            Top             =   2520
            Width           =   3315
         End
         Begin VB.Label IF1 
            BackColor       =   &H00EBE1D7&
            Caption         =   "IF1"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   0
            Left            =   90
            TabIndex        =   46
            Top             =   2880
            Width           =   3315
         End
         Begin VB.Label lblXX 
            Alignment       =   2  'Center
            BackColor       =   &H00EBE1D7&
            Caption         =   "XX"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   0
            Left            =   585
            TabIndex        =   45
            Top             =   3240
            Width           =   935
         End
         Begin VB.Label lblYY 
            Alignment       =   2  'Center
            BackColor       =   &H00EBE1D7&
            Caption         =   "YY"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   0
            Left            =   1505
            TabIndex        =   44
            Top             =   3240
            Width           =   935
         End
         Begin VB.Label lblBB 
            Alignment       =   2  'Center
            BackColor       =   &H00EBE1D7&
            Caption         =   "BB"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   12
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   330
            Index           =   0
            Left            =   2425
            TabIndex        =   43
            Top             =   3240
            Width           =   935
         End
      End
      Begin VB.Label lblTOLLERANZA 
         BackColor       =   &H00EBE1D7&
         Caption         =   "lblTOLLERANZA"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Left            =   840
         TabIndex        =   163
         Top             =   240
         Width           =   5000
      End
      Begin VB.Label lblARCHIVIO 
         Appearance      =   0  'Flat
         BackColor       =   &H000000FF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "lblARCHIVIO"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000014&
         Height          =   600
         Left            =   6090
         TabIndex        =   92
         Top             =   240
         Width           =   3270
      End
      Begin VB.Image Image3 
         Appearance      =   0  'Flat
         BorderStyle     =   1  'Fixed Single
         Height          =   525
         Left            =   9885
         Stretch         =   -1  'True
         Top             =   255
         Width           =   1020
      End
   End
   Begin VB.TextBox Text1 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   7.8
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   288
      Index           =   5
      Left            =   9500
      TabIndex        =   100
      Text            =   "0"
      Top             =   2090
      Width           =   915
   End
   Begin VB.TextBox Text1 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   7.8
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   288
      Index           =   4
      Left            =   9500
      TabIndex        =   99
      Text            =   "0"
      Top             =   1780
      Width           =   915
   End
   Begin VB.TextBox Text1 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   7.8
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   288
      Index           =   3
      Left            =   9500
      TabIndex        =   5
      Text            =   "0"
      Top             =   1490
      Width           =   915
   End
   Begin VB.TextBox Text1 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   7.8
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   288
      Index           =   2
      Left            =   9500
      TabIndex        =   4
      Text            =   "0"
      Top             =   1160
      Width           =   915
   End
   Begin VB.CommandButton cmdABILITA 
      Caption         =   "DIS/ENABLE FEEDBACK"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.4
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   3720
      TabIndex        =   162
      Top             =   6720
      Visible         =   0   'False
      Width           =   2460
   End
   Begin VB.ListBox lstCORRETTORIONOFF2 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.4
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   600
      ItemData        =   "Chk0.frx":0000
      Left            =   2640
      List            =   "Chk0.frx":0007
      MultiSelect     =   2  'Extended
      TabIndex        =   161
      Top             =   6240
      Visible         =   0   'False
      Width           =   800
   End
   Begin VB.CommandButton cmdCORRETTORIONOFF2 
      Caption         =   "ENABLE ALL"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   10.2
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   400
      Left            =   5100
      TabIndex        =   158
      Top             =   5820
      Visible         =   0   'False
      Width           =   1860
   End
   Begin VB.Frame fraVANI 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBE1D7&
      Caption         =   "fraVANI"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1450
      Left            =   15345
      TabIndex        =   150
      Top             =   9375
      Visible         =   0   'False
      Width           =   2085
      Begin VB.OptionButton optVANI 
         Appearance      =   0  'Flat
         BackColor       =   &H00EBE1D7&
         Caption         =   "VANI"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   7.8
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   300
         Index           =   3
         Left            =   200
         TabIndex        =   154
         Top             =   1100
         Width           =   1700
      End
      Begin VB.OptionButton optVANI 
         Appearance      =   0  'Flat
         BackColor       =   &H00EBE1D7&
         Caption         =   "VANI"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   7.8
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   300
         Index           =   2
         Left            =   200
         TabIndex        =   153
         Top             =   800
         Width           =   1700
      End
      Begin VB.OptionButton optVANI 
         Appearance      =   0  'Flat
         BackColor       =   &H00EBE1D7&
         Caption         =   "VANI"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   7.8
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   300
         Index           =   1
         Left            =   200
         TabIndex        =   152
         Top             =   500
         Width           =   1700
      End
      Begin VB.OptionButton optVANI 
         Appearance      =   0  'Flat
         BackColor       =   &H00EBE1D7&
         Caption         =   "VANI"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   7.8
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   300
         Index           =   0
         Left            =   200
         TabIndex        =   151
         Top             =   200
         Width           =   1700
      End
   End
   Begin VB.TextBox txtDIAMETRO 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.4
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   14235
      TabIndex        =   149
      Text            =   "0"
      Top             =   10250
      Visible         =   0   'False
      Width           =   915
   End
   Begin VB.TextBox txtDIAMETRO 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.4
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   14235
      TabIndex        =   148
      Text            =   "0"
      Top             =   9930
      Visible         =   0   'False
      Width           =   915
   End
   Begin VB.TextBox txtDIAMETRO 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.4
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   14235
      TabIndex        =   145
      Text            =   "0"
      Top             =   9600
      Visible         =   0   'False
      Width           =   915
   End
   Begin VB.Frame fraCONTROLLOONOFF 
      BackColor       =   &H00EBE1D7&
      Caption         =   "fraCONTROLLOONOFF"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2010
      Left            =   14640
      TabIndex        =   138
      Top             =   240
      Visible         =   0   'False
      Width           =   3975
      Begin VB.ListBox lstCONTROLLOONOFF 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.4
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   408
         ItemData        =   "Chk0.frx":0020
         Left            =   3015
         List            =   "Chk0.frx":0027
         MultiSelect     =   2  'Extended
         TabIndex        =   140
         Top             =   975
         Width           =   800
      End
      Begin VB.CommandButton cmdCONTROLLOONOFF 
         Caption         =   "ENABLE ALL"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.4
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   180
         TabIndex        =   139
         Top             =   1470
         Width           =   1260
      End
      Begin GAPP4PC.Grafico grfCONTROLLOONOFF 
         Height          =   1095
         Left            =   165
         TabIndex        =   141
         Top             =   270
         Visible         =   0   'False
         Width           =   2715
         _ExtentX        =   4784
         _ExtentY        =   1926
      End
      Begin VB.Label lblCONTROLLOONOFF 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H000000FF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "lblCONTROLLOONOFF"
         BeginProperty Font 
            Name            =   "MS Song"
            Size            =   8.4
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   705
         Left            =   3000
         TabIndex        =   142
         Top             =   285
         Width           =   795
      End
   End
   Begin VB.Frame fraCORRETTORIONOFF 
      BackColor       =   &H00EBE1D7&
      Caption         =   "fraCORRETTORIONOFF"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2010
      Left            =   10560
      TabIndex        =   133
      Top             =   240
      Visible         =   0   'False
      Width           =   3975
      Begin VB.TextBox txtSCALA 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.4
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   1920
         TabIndex        =   156
         Text            =   "50"
         Top             =   1470
         Width           =   540
      End
      Begin VB.CommandButton cmdCORRETTORIONOFF 
         Caption         =   "ENABLE ALL"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.4
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   180
         TabIndex        =   137
         Top             =   1470
         Width           =   1260
      End
      Begin VB.ListBox lstCORRETTORIONOFF 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.4
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   408
         ItemData        =   "Chk0.frx":003E
         Left            =   3015
         List            =   "Chk0.frx":0040
         MultiSelect     =   2  'Extended
         TabIndex        =   135
         Top             =   975
         Width           =   800
      End
      Begin GAPP4PC.Grafico grfCORRETTORIONOFF 
         Height          =   1095
         Left            =   180
         TabIndex        =   134
         Top             =   315
         Visible         =   0   'False
         Width           =   2715
         _ExtentX        =   4784
         _ExtentY        =   1926
      End
      Begin VB.Label lblCORRETTORIONOFF 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H000000FF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "lblCORRETTORIONOFF"
         BeginProperty Font 
            Name            =   "MS Song"
            Size            =   8.4
            Charset         =   134
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   705
         Left            =   3000
         TabIndex        =   136
         Top             =   285
         Width           =   795
      End
   End
   Begin VB.Frame fraVISUALIZZAZIONE_CORRETTORI 
      BackColor       =   &H00EBE1D7&
      Caption         =   "fraVISUALIZZAZIONE_CORRETTORI"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5500
      Left            =   9000
      TabIndex        =   125
      Top             =   3960
      Visible         =   0   'False
      Width           =   9732
      Begin VB.VScrollBar hsCORROT 
         Height          =   3400
         Left            =   8050
         TabIndex        =   143
         Top             =   800
         Width           =   1000
      End
      Begin VB.TextBox txtCORROT 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   1
         Left            =   8050
         TabIndex        =   128
         Text            =   "0"
         Top             =   480
         Width           =   1000
      End
      Begin VB.TextBox txtCORROT 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Index           =   0
         Left            =   8050
         TabIndex        =   127
         Text            =   "0"
         Top             =   150
         Width           =   1000
      End
      Begin VB.PictureBox PicCORROT 
         Appearance      =   0  'Flat
         AutoRedraw      =   -1  'True
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.4
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   4500
         Left            =   50
         ScaleHeight     =   95.212
         ScaleMode       =   0  'User
         ScaleWidth      =   174.653
         TabIndex        =   126
         TabStop         =   0   'False
         Top             =   800
         Width           =   8000
      End
      Begin VB.Label lblINTESTAZIONE 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "lblINTESTAZIONE"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   660
         Left            =   50
         TabIndex        =   131
         Top             =   150
         Width           =   5000
      End
      Begin VB.Label lblCORROT 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "lblCORROT"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   330
         Index           =   1
         Left            =   5050
         TabIndex        =   130
         Top             =   480
         Width           =   3000
      End
      Begin VB.Label lblCORROT 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "lblCORROT"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   9.6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   330
         Index           =   0
         Left            =   5050
         TabIndex        =   129
         Top             =   150
         Width           =   3000
      End
   End
   Begin VB.TextBox txtHELP 
      Appearance      =   0  'Flat
      BackColor       =   &H00000000&
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   10.2
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   630
      Left            =   6240
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   16
      Top             =   6600
      Visible         =   0   'False
      Width           =   1776
   End
   Begin VB.Frame fraTABELLA_CORRETTORI 
      BackColor       =   &H00EBE1D7&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5300
      Left            =   120
      TabIndex        =   104
      Top             =   7320
      Visible         =   0   'False
      Width           =   8500
      Begin VB.CommandButton cmdCORR 
         Caption         =   "SAVE"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   7.8
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Index           =   2
         Left            =   6200
         TabIndex        =   155
         Top             =   1550
         Width           =   1550
      End
      Begin GAPP4PC.Grafico GraficoCORR 
         Height          =   1635
         Left            =   100
         TabIndex        =   132
         Top             =   2000
         Visible         =   0   'False
         Width           =   2745
         _ExtentX        =   4847
         _ExtentY        =   2879
      End
      Begin VB.CommandButton cmdCORR 
         Caption         =   "ENABLE ALL"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   7.8
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Index           =   1
         Left            =   6200
         TabIndex        =   119
         Top             =   900
         Width           =   1550
      End
      Begin VB.CommandButton cmdCORR 
         Caption         =   "SAVE"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   7.8
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   450
         Index           =   0
         Left            =   6200
         TabIndex        =   118
         Top             =   300
         Width           =   1550
      End
      Begin VB.ListBox List3 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   10.2
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   708
         Index           =   0
         Left            =   1200
         TabIndex        =   112
         TabStop         =   0   'False
         Top             =   1200
         Width           =   800
      End
      Begin VB.ListBox List3 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   10.2
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   708
         Index           =   1
         Left            =   2000
         TabIndex        =   111
         TabStop         =   0   'False
         Top             =   1200
         Width           =   1500
      End
      Begin VB.ListBox List3 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   10.2
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   708
         Index           =   2
         Left            =   3500
         TabIndex        =   110
         TabStop         =   0   'False
         Top             =   1200
         Width           =   1500
      End
      Begin VB.TextBox txtCORR 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Index           =   1
         Left            =   2000
         TabIndex        =   108
         Text            =   "txtCORR"
         Top             =   700
         Width           =   1500
      End
      Begin VB.TextBox txtCORR 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Index           =   2
         Left            =   3500
         TabIndex        =   109
         Text            =   "txtCORR"
         Top             =   700
         Width           =   1500
      End
      Begin VB.TextBox txtCORR 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   12
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   500
         Index           =   0
         Left            =   1200
         TabIndex        =   107
         Text            =   "txtCORR"
         Top             =   700
         Width           =   800
      End
      Begin VB.ListBox List3 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   10.2
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   708
         Index           =   3
         Left            =   100
         MultiSelect     =   2  'Extended
         TabIndex        =   106
         Top             =   1200
         Width           =   1100
      End
      Begin VB.ListBox List3 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   10.2
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   708
         Index           =   4
         Left            =   5000
         MultiSelect     =   2  'Extended
         TabIndex        =   105
         Top             =   1200
         Width           =   1100
      End
      Begin VB.Label Label7 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H000000FF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Label7"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   10.2
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   900
         Index           =   4
         Left            =   5000
         TabIndex        =   117
         Top             =   300
         Width           =   1100
      End
      Begin VB.Label Label7 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H000000FF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Label7"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   10.2
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   900
         Index           =   3
         Left            =   100
         TabIndex        =   116
         Top             =   300
         Width           =   1100
      End
      Begin VB.Label Label7 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H000000FF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Label7"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   10.2
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   400
         Index           =   2
         Left            =   3500
         TabIndex        =   115
         Top             =   300
         Width           =   1500
      End
      Begin VB.Label Label7 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H000000FF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Label7"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   10.2
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   400
         Index           =   1
         Left            =   2000
         TabIndex        =   114
         Top             =   300
         Width           =   1500
      End
      Begin VB.Label Label7 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H000000FF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Label7"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   10.2
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   400
         Index           =   0
         Left            =   1200
         TabIndex        =   113
         Top             =   300
         Width           =   800
      End
   End
   Begin VB.Frame fraEDITOR 
      BackColor       =   &H00EBE1D7&
      Caption         =   "fraEDITOR"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.4
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2085
      Left            =   8835
      TabIndex        =   120
      Top             =   2490
      Visible         =   0   'False
      Width           =   8325
      Begin VB.TextBox txtEDITOR 
         Appearance      =   0  'Flat
         BackColor       =   &H00000000&
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   10.2
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   1800
         Left            =   285
         MultiLine       =   -1  'True
         ScrollBars      =   3  'Both
         TabIndex        =   123
         Text            =   "Chk0.frx":0042
         Top             =   345
         Width           =   5100
      End
      Begin VB.CommandButton cmdEDITOR_STAMPA 
         Caption         =   "cmdEDITOR_STAMPA"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   7.8
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   5500
         TabIndex        =   122
         Top             =   825
         Width           =   2500
      End
      Begin VB.CommandButton cmdEDITOR_SALVA 
         Caption         =   "cmdEDITOR_SALVA"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   7.8
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   5500
         TabIndex        =   121
         Top             =   345
         Width           =   2500
      End
      Begin VB.Label lblEDITOR_INDICE 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H000000FF&
         BorderStyle     =   1  'Fixed Single
         Caption         =   "lblEDITOR_INDICE"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   7.8
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   345
         Left            =   5500
         TabIndex        =   124
         Top             =   1230
         Width           =   2500
      End
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   5200
      Left            =   -3240
      ScaleHeight     =   16.064
      ScaleMode       =   0  'User
      ScaleWidth      =   23.121
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   960
      Width           =   7500
   End
   Begin VB.CheckBox ChkTAN 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBE1D7&
      Caption         =   "fh"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.4
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   7800
      MaskColor       =   &H00FFFFFF&
      TabIndex        =   103
      Top             =   2425
      Width           =   500
   End
   Begin VB.CheckBox ChkGRID2 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBE1D7&
      Caption         =   "Grid 2"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.4
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   6960
      TabIndex        =   102
      Top             =   2425
      Width           =   800
   End
   Begin VB.TextBox Text2 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   10.2
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   372
      Left            =   4725
      TabIndex        =   1
      Top             =   270
      Width           =   2500
   End
   Begin VB.PictureBox PicLogo2 
      BackColor       =   &H00EBE1D7&
      BorderStyle     =   0  'None
      Height          =   525
      Left            =   2070
      ScaleHeight     =   9.313
      ScaleMode       =   6  'Millimeter
      ScaleWidth      =   33.655
      TabIndex        =   93
      TabStop         =   0   'False
      Top             =   5070
      Visible         =   0   'False
      Width           =   1905
   End
   Begin VB.ComboBox Combo1 
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   13.8
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   432
      Left            =   1080
      TabIndex        =   0
      Text            =   "Combo1"
      Top             =   285
      Width           =   3600
   End
   Begin VB.TextBox Text1 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   7.8
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   288
      Index           =   1
      Left            =   8500
      TabIndex        =   7
      Text            =   "Text1"
      Top             =   1490
      Width           =   915
   End
   Begin VB.TextBox Text1 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   7.8
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   288
      Index           =   0
      Left            =   8500
      TabIndex        =   6
      Text            =   "Text1"
      Top             =   1160
      Width           =   915
   End
   Begin VB.FileListBox File1 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.4
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1452
      Left            =   5400
      MultiSelect     =   2  'Extended
      TabIndex        =   8
      Top             =   3360
      Width           =   3000
   End
   Begin VB.TextBox txtScY 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   7.8
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   288
      Left            =   9500
      TabIndex        =   3
      Text            =   "txtScY"
      Top             =   850
      Width           =   915
   End
   Begin VB.TextBox txtScX 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   7.8
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   288
      Left            =   9500
      TabIndex        =   2
      Text            =   "txtScX"
      Top             =   540
      Width           =   915
   End
   Begin VB.PictureBox PicLogo 
      BackColor       =   &H00EBE1D7&
      BorderStyle     =   0  'None
      Height          =   630
      Left            =   90
      ScaleHeight     =   11.218
      ScaleMode       =   6  'Millimeter
      ScaleWidth      =   33.655
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   5010
      Visible         =   0   'False
      Width           =   1905
   End
   Begin VB.CheckBox ChkGRID1 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBE1D7&
      Caption         =   "Grid 1"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.4
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   240
      Left            =   6165
      TabIndex        =   101
      Top             =   2425
      Width           =   800
   End
   Begin GAPP4PC.Grafico Grafico1 
      Height          =   1095
      Left            =   6405
      TabIndex        =   157
      TabStop         =   0   'False
      Top             =   5355
      Visible         =   0   'False
      Width           =   2715
      _ExtentX        =   4784
      _ExtentY        =   1926
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Label2"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   10.2
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   288
      Index           =   5
      Left            =   7000
      TabIndex        =   98
      Top             =   2090
      Width           =   1230
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Label2"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   10.2
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   288
      Index           =   4
      Left            =   7000
      TabIndex        =   97
      Top             =   1780
      Width           =   1230
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Label2"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   10.2
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   288
      Index           =   3
      Left            =   7000
      TabIndex        =   94
      Top             =   1490
      Width           =   1230
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Label2"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   10.2
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   288
      Index           =   2
      Left            =   7000
      TabIndex        =   95
      Top             =   1160
      Width           =   1230
   End
   Begin VB.Label lblCORRETTORIONOFF2 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBE1D7&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   10.2
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   345
      Index           =   1
      Left            =   3900
      TabIndex        =   160
      Top             =   6150
      Visible         =   0   'False
      Width           =   1185
   End
   Begin VB.Label lblCORRETTORIONOFF2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H000000FF&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   345
      Index           =   0
      Left            =   5115
      TabIndex        =   159
      Top             =   6150
      Visible         =   0   'False
      Width           =   1185
   End
   Begin VB.Label lblDIAMETRO 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "lblDIAMETRO"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.4
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Index           =   2
      Left            =   13000
      TabIndex        =   147
      Top             =   10250
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.Label lblDIAMETRO 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "lblDIAMETRO"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.4
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Index           =   1
      Left            =   13000
      TabIndex        =   146
      Top             =   9930
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.Label lblDIAMETRO 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "lblDIAMETRO"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.4
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   285
      Index           =   0
      Left            =   13000
      TabIndex        =   144
      Top             =   9600
      Visible         =   0   'False
      Width           =   1230
   End
   Begin VB.Label lblScY 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "lblScY"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   10.2
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   288
      Left            =   7000
      TabIndex        =   96
      Top             =   850
      Width           =   1230
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Label2"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   10.2
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   288
      Index           =   1
      Left            =   6165
      TabIndex        =   15
      Top             =   1490
      Width           =   1230
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Label2"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   10.2
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   288
      Index           =   0
      Left            =   6165
      TabIndex        =   14
      Top             =   1160
      Width           =   1230
   End
   Begin VB.Label Intestazione 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H000000FF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Intestazione"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   350
      Left            =   1005
      TabIndex        =   13
      Top             =   0
      Width           =   8800
      WordWrap        =   -1  'True
   End
   Begin VB.Image Image1 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   800
      Left            =   0
      Stretch         =   -1  'True
      Top             =   0
      Width           =   1200
   End
   Begin VB.Label lblScX 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "lblScX"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   10.2
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   288
      Left            =   7000
      TabIndex        =   11
      Top             =   540
      Width           =   1230
   End
   Begin VB.Image Image2 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   372
      Left            =   5280
      Stretch         =   -1  'True
      Top             =   2880
      Width           =   912
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Appearance      =   0  'Flat
      BackColor       =   &H00EBE1D7&
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   15.6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   372
      Left            =   6240
      TabIndex        =   12
      Top             =   2880
      Width           =   2088
   End
End
Attribute VB_Name = "CHK0"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim npt       As Integer
Dim CORvsCHK  As String

Private Sub ChkBOMBATURA_Click()
'
Dim ret As Long
'
On Error Resume Next
  '
  Select Case CHK0.Combo1.ListIndex
    Case 0: ret = WritePrivateProfileString("CHK0", "BOMBATURA_PROFILO", CHK0.ChkBOMBATURA.Value, Path_LAVORAZIONE_INI)
    Case 1: ret = WritePrivateProfileString("CHK0", "BOMBATURA_ELICA", CHK0.ChkBOMBATURA.Value, Path_LAVORAZIONE_INI)
  End Select
  
End Sub

Private Sub ChkBOMBATURA_GotFocus()
'
On Error Resume Next
  '
  CHK0.ChkBOMBATURA.BackColor = QBColor(12)
  CHK0.ChkBOMBATURA.ForeColor = QBColor(15)

End Sub

Private Sub ChkBOMBATURA_LostFocus()
'
On Error Resume Next
  '
  CHK0.ChkBOMBATURA.BackColor = &HEBE1D7
  CHK0.ChkBOMBATURA.ForeColor = &H80000008
  
End Sub

Private Sub ChkGRID1_Click()

Dim ret As Long

On Error Resume Next

  ret = WritePrivateProfileString("CHK0", "Griglia1", CHK0.ChkGRID1.Value, Path_LAVORAZIONE_INI)
  Call VISUALIZZA_CONTROLLO
  
End Sub

Private Sub ChkGRID2_Click()

Dim ret As Long

On Error Resume Next

  ret = WritePrivateProfileString("CHK0", "Griglia2", CHK0.ChkGRID2.Value, Path_LAVORAZIONE_INI)
  Call VISUALIZZA_CONTROLLO
  
End Sub

Private Sub ChkGRID1_GotFocus()

On Error Resume Next

  CHK0.ChkGRID1.BackColor = QBColor(12)
  CHK0.ChkGRID1.ForeColor = QBColor(15)

End Sub

Private Sub ChkGRID2_GotFocus()

On Error Resume Next

  CHK0.ChkGRID2.BackColor = QBColor(12)
  CHK0.ChkGRID2.ForeColor = QBColor(15)

End Sub

Private Sub ChkGRID1_LostFocus()

On Error Resume Next

  CHK0.ChkGRID1.BackColor = &HEBE1D7
  CHK0.ChkGRID1.ForeColor = &H80000008
  
End Sub

Private Sub ChkGRID2_LostFocus()

On Error Resume Next

  CHK0.ChkGRID2.BackColor = &HEBE1D7
  CHK0.ChkGRID2.ForeColor = &H80000008
  
End Sub

Private Sub ChkTAN_Click()

Dim ret As Long

On Error Resume Next

  ret = WritePrivateProfileString("CHK0", "SiOpt(4,0)", CHK0.ChkTAN.Value, Path_LAVORAZIONE_INI)
  ret = WritePrivateProfileString("CHK0", "SiOpt(4,1)", CHK0.ChkTAN.Value, Path_LAVORAZIONE_INI)
  SiOpt(4, 0) = CHK0.ChkTAN.Value
  SiOpt(4, 1) = CHK0.ChkTAN.Value
  Call VISUALIZZA_CONTROLLO
  
End Sub

Private Sub ChkTAN_GotFocus()

On Error Resume Next

  CHK0.ChkTAN.BackColor = QBColor(12)
  CHK0.ChkTAN.ForeColor = QBColor(15)

End Sub

Private Sub ChkTAN_LostFocus()

On Error Resume Next

  CHK0.ChkTAN.BackColor = &HEBE1D7
  CHK0.ChkTAN.ForeColor = &H80000008
  
End Sub

Private Sub cmdABILITA_Click()
'
Dim Atmp  As String * 255
Dim stmp1 As String
Dim stmp2 As String
Dim ret   As Long
Dim rsp   As Long
Dim riga  As String
'
On Error Resume Next
    '
    Dim pp As String
    pp = GetInfo("Configurazione", "PathWINMGR", PathFILEINI): pp = UCase$(Trim$(pp))
    If (Len(pp) <= 0) Then WRITE_DIALOG "Operation cannot be performed!!!": Exit Sub
    '
    stmp2 = GetInfo("CONTROLLO", "PERCORSO_CONTROLLI", pp): stmp2 = UCase$(Trim$(stmp2))
    If (Len(stmp2) > 0) Then
      'FEEDBACK ABILITATO
      ret = WritePrivateProfileString("CONTROLLO", "PERCORSO_CONTROLLI", "", pp)
      WRITE_DIALOG "FEEDBACK DISENABLED!!!!!"
      StopRegieEvents
      ret = LoadString(g_hLanguageLibHandle, 1004, Atmp, 255)
      If (ret > 0) Then riga = Left$(Atmp, ret)
      riga = riga & " CORREZIONI DI PROCESSO" & " ? "
      rsp = MsgBox(riga, 256 + 4 + 32, "WARNING")
      ResumeRegieEvents
      If (rsp = vbYes) Then
        Dim DDECX  As String
        Dim DDECY  As String
        DDECX = GetInfo("Configurazione", "DDECX", Path_LAVORAZIONE_INI)
        DDECX = UCase$(Trim$(DDECX))
        DDECY = GetInfo("Configurazione", "DDECY", Path_LAVORAZIONE_INI)
        DDECY = UCase$(Trim$(DDECY))
        If (Len(DDECX) > 0) And (Len(DDECY) > 0) Then
          'SCRITTURA DELLE CORREZIONI
          For i = 0 To 401
            Call OPC_SCRIVI_DATO(DDECX & "[" & Format$(i + 1, "##0") & "]", "0")
            Call OPC_SCRIVI_DATO(DDECY & "[" & Format$(i + 1, "##0") & "]", "0")
            WRITE_DIALOG i
          Next i
        End If
        WRITE_DIALOG "CORREZIONI DI PROCESSO CANCELLATE!!!"
      End If
    Else
      'FEEDBACK NON ABILITATO
      stmp1 = GetInfo("Configurazione", "PERCORSO_CONTROLLI", Path_LAVORAZIONE_INI): stmp1 = UCase$(Trim$(stmp1))
      'CANCELLO IL RISULTATO DEL CONTROLLO
      riga = LEGGI_NomePezzo("ROTORI_ESTERNI")
      If (Dir$(stmp1 & "\" & riga & ".CHK") <> "") Then Kill stmp1 & "\" & riga & ".CHK"
      ret = WritePrivateProfileString("CONTROLLO", "PERCORSO_CONTROLLI", stmp1, pp)
      WRITE_DIALOG "FEEDBACK ENABLED!!!!!"
    End If

End Sub

Private Sub cmdCONTROLLOONOFF_Click()
'
Dim COORDINATE         As String
Dim RADICE             As String
Dim PERCORSO           As String
Dim PERCORSO_CONTROLLI As String
Dim PERCORSO_PROFILI   As String
Dim sequenza           As String
Dim npt                As Integer
Dim TY_ROT             As String
Dim PASSO              As Double
Dim CONTROLLORE        As String
Dim DM_ROT             As String
Dim INVERSIONE         As Integer
'
Dim riga  As String
Dim i     As Integer
Dim X     As Single
Dim ret   As Long
Dim stmp1 As String
Dim stmp2 As String
Dim TIPO_LAVORAZIONE   As String

'
On Error Resume Next
  '
  'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  Select Case TIPO_LAVORAZIONE
     Case "ROTORI_ESTERNI"
          Call LEGGI_INFO_ROTORI(PERCORSO, COORDINATE, RADICE, PERCORSO_CONTROLLI, PERCORSO_PROFILI, sequenza, npt, TY_ROT, PASSO, CONTROLLORE, DM_ROT, INVERSIONE)
     Case "PROFILO_RAB_ESTERNI"
          Call LEGGI_INFO_RAB(RADICE, npt, NP1, TY_ROT, PASSO, CONTROLLORE, DM_ROT)
  End Select
    
  'ABILITAZIONE DI TUTTI I CORRETTORI
  CHK0.lstCONTROLLOONOFF.Clear
  For i = 1 To npt
    Call grfCONTROLLOONOFF.SerieNRem(i, "1")
    Call CHK0.lstCONTROLLOONOFF.AddItem(Right$(String$(3, " ") & Format$(i, "##0"), 3) & " -")
  Next i
  'SALVATAGGIO DEI PUNTI CONTROLLATI
  If (Dir$(RADICE & "\CHKRAB.OLD") <> "") Then Call Kill(RADICE & "\CHKRAB.OLD")
  If (Dir$(RADICE & "\CHKRAB.ROT") <> "") Then Call FileCopy(RADICE & "\CHKRAB.ROT", RADICE & "\CHKRAB.OLD")
  'SALVATAGGIO DEI PUNTI DI CONTROLLO ABILITATI
  Open RADICE & "\CHKRAB.ROT" For Output As #1
    Print #1, "[PuntiControllo]"
    For i = 1 To npt
      If (Right$(CHK0.lstCONTROLLOONOFF.List(i - 1), 1) = "X") Then
        Print #1, "CHK[0," & CStr(i) & "]=0"
      Else
        Print #1, "CHK[0," & CStr(i) & "]=1"
      End If
    Next i
  Close #1
  'CALCOLO PROFILO SOLO SE DIVERSO
  Open RADICE & "\CHKRAB.OLD" For Input As #1
    stmp1 = Input$(LOF(1), 1)
  Close #1
  Open RADICE & "\CHKRAB.ROT" For Input As #1
    stmp2 = Input$(LOF(1), 1)
  Close #1
  'CONFRONTO FILE
  If (stmp2 <> stmp1) Then
    Select Case TIPO_LAVORAZIONE
      Case "ROTORI_ESTERNI"
          Call CARICA_DATI_CONTROLLO
      Case "PROFILO_RAB_ESTERNI"
          Call CARICA_DATI_CONTROLLO_RAB
    End Select
  End If
  
  WRITE_DIALOG " All points CHECKED!! "

End Sub

Private Sub cmdCORR_Click(Index As Integer)
'
Dim COORDINATE         As String
Dim RADICE             As String
Dim PERCORSO           As String
Dim PERCORSO_CONTROLLI As String
Dim PERCORSO_PROFILI   As String
Dim sequenza           As String
Dim npt                As Integer
Dim TY_ROT             As String
Dim PASSO              As Double
Dim CONTROLLORE        As String
Dim DM_ROT             As String
Dim INVERSIONE         As Integer
Dim riga               As String
Dim stmp1              As String
Dim stmp2              As String
Dim i                  As Integer
Dim X                  As Single
Dim ret                As Long
Dim TIPO_LAVORAZIONE   As String

'
On Error Resume Next
  '
  'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  Select Case TIPO_LAVORAZIONE
     Case "ROTORI_ESTERNI"
          Call LEGGI_INFO_ROTORI(PERCORSO, COORDINATE, RADICE, PERCORSO_CONTROLLI, PERCORSO_PROFILI, sequenza, npt, TY_ROT, PASSO, CONTROLLORE, DM_ROT, INVERSIONE)
     Case "PROFILO_RAB_ESTERNI"
          Call LEGGI_INFO_RAB(RADICE, npt, NP1, TY_ROT, PASSO, CONTROLLORE, DM_ROT)
  End Select
  '
  Select Case Index
  
    Case 0
      'SEGNALAZIONE UTENTE
      WRITE_DIALOG ""
      'SALVATAGGIO MODIFICHE IN CORROT.BAK
      Open RADICE & "\CORROT.BAK" For Output As #1
        For i = 1 To UBound(CorXX)
          riga = ""
          X = CorXX(i)
          If X >= 0 Then riga = riga & "+" Else riga = riga & "-"
          riga = riga & frmt0(Abs(X), 4)
          X = CorYY(i)
          If X >= 0 Then riga = riga & "+" Else riga = riga & "-"
          riga = riga & frmt0(Abs(X), 4)
          Print #1, riga
        Next i
      Close #1
      'SALVATAGGIO DEI CORRETTORI ABILITATI
      Open RADICE & "\ABILITA.COR" For Output As #1
        For i = 0 To CHK0.List3(4).ListCount - 1
          If (CHK0.List3(4).List(i) = "X") Then
            Print #1, "1"
          Else
            Print #1, "0"
          End If
        Next i
      Close #1
      'SALVATAGGIO DEI PUNTI CONTROLLATI
      If (Dir$(RADICE & "\CHKRAB.OLD") <> "") Then Call Kill(RADICE & "\CHKRAB.OLD")
      If (Dir$(RADICE & "\CHKRAB.ROT") <> "") Then Call FileCopy(RADICE & "\CHKRAB.ROT", RADICE & "\CHKRAB.OLD")
      Open RADICE & "\CHKRAB.ROT" For Output As #1
        Print #1, "[PuntiControllo]"
        For i = 1 To CHK0.List3(3).ListCount - 1 - 1
          If (CHK0.List3(3).List(i) = " ") Then
            Print #1, "CHK[0," & CStr(i) & "]=1"
          Else
            Print #1, "CHK[0," & CStr(i) & "]=0"
          End If
        Next i
      Close #1
      'CALCOLO PROFILO SOLO SE DIVERSO
      Open RADICE & "\CHKRAB.OLD" For Input As #1
        stmp1 = Input$(LOF(1), 1)
      Close #1
      Open RADICE & "\CHKRAB.ROT" For Input As #1
        stmp2 = Input$(LOF(1), 1)
      Close #1
      'CONFRONTO FILE
      If (stmp2 <> stmp1) Then
        Select Case TIPO_LAVORAZIONE
          Case "ROTORI_ESTERNI"
            Call CARICA_DATI_CONTROLLO
          Case "PROFILO_RAB_ESTERNI"
            Call CARICA_DATI_CONTROLLO_RAB
          End Select
      End If
      
    Case 1
      'ABILITAZIONE DI TUTTI I CORRETTORI
      For i = 0 To CHK0.List3(4).ListCount - 1
        CHK0.List3(4).List(i) = "X"
      Next i
      'SALVATAGGIO DEI CORRETTORI ABILITATI
      Open RADICE & "\ABILITA.COR" For Output As #1
        For i = 0 To CHK0.List3(4).ListCount - 1
          If (CHK0.List3(4).List(i) = "X") Then
            Print #1, "1"
          Else
            Print #1, "0"
          End If
        Next i
      Close #1
      'ABILITAZIONE CONTROLLO DI TUTTI I PUNTI
        i = 0
          CHK0.List3(3).List(i) = "X"
        For i = 1 To CHK0.List3(3).ListCount - 1 - 1
          CHK0.List3(3).List(i) = " "
        Next i
        i = CHK0.List3(3).ListCount - 1
          CHK0.List3(3).List(i) = "X"
      'SALVATAGGIO DEI PUNTI ABILITATI
      Open RADICE & "\CHKRAB.ROT" For Output As #1
        Print #1, "[PuntiControllo]"
        For i = 1 To CHK0.List3(3).ListCount - 1 - 1
          Print #1, "CHK[0," & CStr(i) & "]=1"
        Next i
      Close #1
      'SEGNALAZIONE UTENTE
      WRITE_DIALOG ""
      
    Case 2
      'SEGNALAZIONE UTENTE
      WRITE_DIALOG ""
      'SALVATAGGIO DEI CORRETTORI ABILITATI
      Open RADICE & "\ABILITA.COR" For Output As #1
        For i = 0 To CHK0.List3(4).ListCount - 1
          If (CHK0.List3(4).List(i) = "X") Then
            Print #1, "1"
          Else
            Print #1, "0"
          End If
        Next i
      Close #1
      'SALVATAGGIO DEI PUNTI CONTROLLATI
      If (Dir$(RADICE & "\CHKRAB.OLD") <> "") Then Call Kill(RADICE & "\CHKRAB.OLD")
      If (Dir$(RADICE & "\CHKRAB.ROT") <> "") Then Call FileCopy(RADICE & "\CHKRAB.ROT", RADICE & "\CHKRAB.OLD")
      Open RADICE & "\CHKRAB.ROT" For Output As #1
        Print #1, "[PuntiControllo]"
        For i = 1 To CHK0.List3(3).ListCount - 1 - 1
          If (CHK0.List3(3).List(i) = " ") Then
            Print #1, "CHK[0," & CStr(i) & "]=1"
          Else
            Print #1, "CHK[0," & CStr(i) & "]=0"
          End If
        Next i
      Close #1
      'CALCOLO PROFILO SOLO SE DIVERSO
      Open RADICE & "\CHKRAB.OLD" For Input As #1
        stmp1 = Input$(LOF(1), 1)
      Close #1
      Open RADICE & "\CHKRAB.ROT" For Input As #1
        stmp2 = Input$(LOF(1), 1)
      Close #1
      'CONFRONTO FILE
      If (stmp2 <> stmp1) Then
        Select Case TIPO_LAVORAZIONE
          Case "ROTORI_ESTERNI"
            Call CARICA_DATI_CONTROLLO
          Case "PROFILO_RAB_ESTERNI"
            Call CARICA_DATI_CONTROLLO_RAB
        End Select
      End If
      
  End Select

End Sub

Private Sub cmdCORRETTORIONOFF_Click()
'
Dim COORDINATE         As String
Dim RADICE             As String
Dim PERCORSO           As String
Dim PERCORSO_CONTROLLI As String
Dim PERCORSO_PROFILI   As String
Dim sequenza           As String
Dim npt                As Integer
Dim TY_ROT             As String
Dim PASSO              As Double
Dim CONTROLLORE        As String
Dim DM_ROT             As String
Dim INVERSIONE         As Integer
Dim TIPO_LAVORAZIONE   As String
'
Dim riga  As String
Dim i     As Integer
Dim X     As Single
Dim ret   As Long
'
On Error Resume Next
  '
  Call LEGGI_INFO_ROTORI(PERCORSO, COORDINATE, RADICE, PERCORSO_CONTROLLI, PERCORSO_PROFILI, sequenza, npt, TY_ROT, PASSO, CONTROLLORE, DM_ROT, INVERSIONE)
  'ABILITAZIONE DI TUTTI I CORRETTORI
  CHK0.lstCORRETTORIONOFF.Clear
  For i = 0 To npt + 1
    Call CHK0.lstCORRETTORIONOFF.AddItem(Right$(String$(3, " ") & Format$(i, "##0"), 3) & " X")
  Next i
  'SALVATAGGIO DEI CORRETTORI ABILITATI
  Open RADICE & "\ABILITA.COR" For Output As #1
    For i = 1 To npt + 2
      Call grfCORRETTORIONOFF.SerieNRem(i, "1")
      If (Right$(CHK0.lstCORRETTORIONOFF.List(i - 1), 1) = "X") Then
        Print #1, "1"
      Else
        Print #1, "0"
      End If
    Next i
  Close #1
  'SEGNALAZIONE UTENTE
  WRITE_DIALOG "ALL CORRECTORS HAVE BEEN ENABLED!!"
      
End Sub

Private Sub cmdCORRETTORIONOFF2_Click()
'
Dim COORDINATE         As String
Dim RADICE             As String
Dim PERCORSO           As String
Dim PERCORSO_CONTROLLI As String
Dim PERCORSO_PROFILI   As String
Dim sequenza           As String
Dim npt                As Integer
Dim TY_ROT             As String
Dim PASSO              As Double
Dim CONTROLLORE        As String
Dim DM_ROT             As String
Dim INVERSIONE         As Integer
Dim TIPO_LAVORAZIONE   As String
'
Dim riga  As String
Dim i     As Integer
Dim X     As Single
Dim ret   As Long
'
On Error Resume Next
  '
  'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  Select Case TIPO_LAVORAZIONE
     Case "ROTORI_ESTERNI"
          Call LEGGI_INFO_ROTORI(PERCORSO, COORDINATE, RADICE, PERCORSO_CONTROLLI, PERCORSO_PROFILI, sequenza, npt, TY_ROT, PASSO, CONTROLLORE, DM_ROT, INVERSIONE)
     Case "PROFILO_RAB_ESTERNI"
          Call LEGGI_INFO_RAB(RADICE, npt, NP1, TY_ROT, PASSO, CONTROLLORE, DM_ROT)
  End Select
  
  'Call LEGGI_INFO_ROTORI(PERCORSO, COORDINATE, RADICE, PERCORSO_CONTROLLI, PERCORSO_PROFILI, sequenza, npt, TY_ROT, PASSO, CONTROLLORE, DM_ROT, INVERSIONE)
  
  'SALVATAGGIO DEI CORRETTORI ABILITATI
  Open RADICE & "\ABILITA.COR" For Output As #1
    For i = 1 To npt + 2
      Call CHK0.Grafico1.SerieNRem(i, "1")
      Print #1, "1"
      CHK0.lstCORRETTORIONOFF2.List(i) = Right$(String$(4, " ") & Format$(i, "##0"), 4) & " X"
    Next i
  Close #1
  CHK0.lblCORRETTORIONOFF2(0).ForeColor = CHK0.lblScY.ForeColor
  CHK0.lblCORRETTORIONOFF2(0).BackColor = CHK0.lblScY.BackColor
  CHK0.lblCORRETTORIONOFF2(0).Caption = "All correctors ENABLED"
  'SEGNALAZIONE UTENTE
  WRITE_DIALOG "ALL CORRECTORS HAVE BEEN ENABLED!!"
      
End Sub

Private Sub cmdEDITOR_STAMPA_Click()
'
Dim X    As Single
Dim i    As Integer
Dim k1   As Integer
Dim k2   As Integer
Dim k3   As Integer
Dim k4   As Integer
Dim j    As Integer
Dim rsp  As Integer
'
Dim MLeft As Single
Dim MTop  As Single
Dim Box0W As Single
Dim Box0H As Single
'
'Parametri per la stampa di linee
Dim X1 As Single
Dim Y1 As Single
Dim X2 As Single
Dim Y2 As Single
'
'Area di stampa dei grafici
Dim Box1L As Single
Dim Box1T As Single
Dim Box1W As Single
Dim Box1H As Single
'
Dim iRG As Single
Dim iCl As Single
'
Dim riga As String
Dim stmp As String
'
On Error Resume Next
  '
  Printer.ScaleMode = 7
  Call ImpostaPrinter("Courier New", 10)
  MLeft = 2: MTop = 1
  Box0W = 17: Box0H = 27
  iRG = Printer.TextHeight("A")
  iCl = Printer.TextWidth("A")
  Box1L = 5: Box1T = 1
  Box1W = 12: Box1H = 12
  '
  GoSub STAMPA_INTESTAZIONE
  '
  Dim NrigheMax As Integer
  Dim iiRG As Integer
  Dim iiPG As Integer
  '
  NrigheMax = (Box0H - 3) / iRG
  iiPG = 1
  riga = ""
  i = 1
  k1 = 1
  k4 = 1
  Do
    'terminatore di riga
    k2 = InStr(k1, txtEDITOR.Text, Chr(13))
    k3 = InStr(k1, txtEDITOR.Text, Chr(10))
    'prendo il minimo
    If k3 >= k2 Then k4 = k2 Else k4 = k3
    If k4 > k1 Then
      riga = Mid$(txtEDITOR.Text, k1, k4 - k1)
      'riga = Trim$(riga)
      If k3 >= k2 Then k1 = k3 + 1 Else k1 = k2 + 1
      i = i + 1
      iiRG = (i - 1) Mod NrigheMax
      Printer.CurrentX = X1 + iCl
      Printer.CurrentY = Y1 + (iiRG + 3) * iRG
      riga = Right(String(6, " ") & Format$(i - 1, "#####0"), 6) & "] " & riga
      Printer.Print riga
      If i = iiPG * NrigheMax Then
        Printer.NewPage
        GoSub STAMPA_INTESTAZIONE
        iiPG = iiPG + 1
      End If
    Else
      Exit Do
    End If
  Loop
  Printer.EndDoc

Exit Sub

STAMPA_INTESTAZIONE:
  'Squadratura della pagina
  X1 = MLeft: Y1 = MTop
  X2 = MLeft + Box0W: Y2 = MTop + Box0H
  'Printer.Line (X1, Y1)-(X2, Y2), QBColor(0), B
  'Intestazione della pagina
  Printer.CurrentX = X1 + iCl
  Printer.CurrentY = Y1 + iRG
  stmp = ""
  stmp = stmp & " Date: " & Format$(Now, "dd/mm/yy")
  stmp = stmp & " Time: " & Format$(Now, "hh:mm:ss")
  Printer.Print CHK0.INTESTAZIONE.Caption & " [" & CHK0.Text2.Text & "] " & stmp
  'Linea di separazione
  X1 = MLeft: Y1 = MTop + 1
  X2 = MLeft + Box0W: Y2 = MTop + 1
  Printer.Line (X1, Y1)-(X2, Y2), QBColor(0)
  Printer.CurrentX = X1 + iCl
  Printer.CurrentY = Y1 + 1 * iRG
  Printer.Print fraEDITOR.Caption
Return

End Sub

Private Sub cmdEDITOR_SALVA_Click()
'
Dim stmp  As String
Dim ret   As Long
Dim riga  As String
'
On Error Resume Next
  '
  'Chiedo di salvare
  stmp = "SAVE " & Chr(13) & Chr(13)
  ret = MsgBox(stmp & " " & CHK0.fraEDITOR.Caption & " ?", 36 + 256, "WARNING")
  If (ret = 6) Then
    Call SALVA_CASELLA_SU_FILE(CHK0.txtEDITOR, CHK0.fraEDITOR.Caption)
  Else
    WRITE_DIALOG "Operation aborted by user!!"
  End If
  '
End Sub

Private Sub Combo1_Click()
'
On Error Resume Next
  '
  Call VISUALIZZA_CONTROLLO
  If fraTOLLERANZA.Visible = True Then fraTOLLERANZA.Visible = False
  '
End Sub

Private Sub File1_DblClick()

On Error Resume Next

  Call CARICAMENTO_CONTROLLO_DA_ARCHIVIO

End Sub

Private Sub File1_KeyPress(KeyAscii As Integer)

On Error Resume Next

  If (KeyAscii = 13) Then Call CARICAMENTO_CONTROLLO_DA_ARCHIVIO
    
End Sub

Private Sub File1_KeyUp(KeyCode As Integer, Shift As Integer)

Dim PathFILE As String
Dim NomeFile As String
Dim stmp     As String
Dim rsp      As Integer

On Error Resume Next
  '
  If (CHK0.File1.ListIndex >= 0) Then
    '
    NomeFile = CHK0.File1.List(CHK0.File1.ListIndex)
    PathFILE = CHK0.File1.Path & "\" & NomeFile
    '
    Select Case KeyCode
      '
      Case 33
        If (CHK0.txtHELP.Visible = True) Then CHK0.txtHELP.Visible = False
        'INIZIO LISTA
        stmp = FileDateTime(PathFILE)
        WRITE_DIALOG NomeFile & " --> " & stmp
        '
      Case 34
        If (CHK0.txtHELP.Visible = True) Then CHK0.txtHELP.Visible = False
        'FINE LISTA
        stmp = FileDateTime(PathFILE)
        WRITE_DIALOG NomeFile & " --> " & stmp
        '
      Case 40
        If (CHK0.txtHELP.Visible = True) Then CHK0.txtHELP.Visible = False
        'AVANTI
        stmp = FileDateTime(PathFILE)
        WRITE_DIALOG NomeFile & " --> " & stmp
        '
      Case 38
        If (CHK0.txtHELP.Visible = True) Then CHK0.txtHELP.Visible = False
        'INDIETRO
        stmp = FileDateTime(PathFILE)
        WRITE_DIALOG NomeFile & " --> " & stmp
        '
      Case 13
        If (Shift = 4) Then
          'EDITOR PATHFILE
          If (CHK0.txtHELP.Visible = True) Then
            'SALVATAGGIO MODIFICHE
            CHK0.txtHELP.Visible = False
            StopRegieEvents
            rsp = MsgBox("SAVE " & PathFILE & " ?", vbDefaultButton2 + vbQuestion + vbYesNo, "WARNING")
            ResumeRegieEvents
            If (rsp = 6) Then
              'SCRIVO IL CONTENUTO DELLA CASELLA DI TESTO
              stmp = CHK0.txtHELP.Text
              Do
                If Right$(stmp, 1) = Chr$(10) Or Right$(stmp, 1) = Chr$(13) Then
                  stmp = Left$(stmp, Len(stmp) - 1)
                Else
                  Exit Do
                End If
              Loop
              Open PathFILE For Output As #1
                Print #1, Trim$(stmp)
              Close #1
            End If
          Else
            'VISUALIZZAZIONE DEL FILE
            CHK0.txtHELP.BackColor = &H0&
            CHK0.txtHELP.ForeColor = &HFFFFFF
            CHK0.txtHELP.Visible = True
            CHK0.txtHELP.Text = ""
            Open PathFILE For Input As #1
              CHK0.txtHELP.Text = Input$(LOF(1), 1)
            Close #1
          End If
        Else
          'FalgROT 019  (04.12.06)
          'If (CHK0.txtHELP.Visible = True) Then CHK0.txtHELP.Visible = False
        End If  'Shift = 4
        '
    End Select
    '
  End If
    
End Sub

Private Sub File1_GotFocus()
'
On Error Resume Next
  '
  CHK0.Label1.BackColor = QBColor(12)
  CHK0.Label1.ForeColor = QBColor(15)
  CHK0.Label1.Caption = " <--- "
  '
End Sub

Private Sub File1_LostFocus()
'
On Error Resume Next
  '
  WRITE_DIALOG ""
  CHK0.Label1.BackColor = &HEBE1D7
  CHK0.Label1.ForeColor = QBColor(0)
  CHK0.Label1.Caption = ""
  '
End Sub

Private Sub Form_Activate()
'
On Error Resume Next
  '
  Call INIZIALIZZA_CHK0_PARTE2
  Call ChildActivate(Me)
  Call VISUALIZZA_CONTROLLO
  '
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  '
  Call GENERA_EVENTO_CLICK(KeyCode, Shift)
  '
End Sub

Private Sub Grafico1_SelChgByMouse()
'
Dim i       As Integer
Dim tmpN()  As Integer
'
Dim PERCORSO           As String
Dim COORDINATE         As String
Dim RADICE             As String
Dim PERCORSO_CONTROLLI As String
Dim PERCORSO_PROFILI   As String
Dim sequenza           As String
Dim TY_ROT             As String
Dim PASSO              As Double
Dim CONTROLLORE        As String
Dim DM_ROT             As String
Dim INVERSIONE         As Integer
Dim IdxCHKtmp()        As Integer
Dim npt                As Integer
Dim NptABILITATI       As Integer
Dim TIPO_LAVORAZIONE   As String
'
On Error Resume Next
  '
  'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  Select Case TIPO_LAVORAZIONE
     Case "ROTORI_ESTERNI"
          Call LEGGI_INFO_ROTORI(PERCORSO, COORDINATE, RADICE, PERCORSO_CONTROLLI, PERCORSO_PROFILI, sequenza, npt, TY_ROT, PASSO, CONTROLLORE, DM_ROT, INVERSIONE)
     Case "PROFILO_RAB_ESTERNI"
          Call LEGGI_INFO_RAB(RADICE, npt, NP1, TY_ROT, PASSO, CONTROLLORE, DM_ROT)
  End Select
  
  'Call LEGGI_INFO_ROTORI(PERCORSO, COORDINATE, RADICE, PERCORSO_CONTROLLI, PERCORSO_PROFILI, sequenza, npt, TY_ROT, PASSO, CONTROLLORE, DM_ROT, INVERSIONE)
    
  ReDim IdxCHKtmp(npt + 2)
  For i = 1 To npt + 2
    IdxCHKtmp(i) = 1
  Next i
  tmpN = CHK0.Grafico1.SerieGetN("1")
  For i = 1 To UBound(tmpN)
    IdxCHKtmp(tmpN(i)) = 0
  Next i
  'SALVATAGGIO DEI CORRETTORI ABILITATI
  NptABILITATI = 0
  Open RADICE & "\ABILITA.COR" For Output As #1
    For i = 1 To npt + 2
      If (IdxCHKtmp(i) = 1) Then
        Print #1, "1"
        NptABILITATI = NptABILITATI + 1
      Else
        Print #1, "0"
      End If
    Next i
  Close #1
  If (NptABILITATI < npt + 2) Then
    CHK0.lblCORRETTORIONOFF2(0).ForeColor = QBColor(15)
    CHK0.lblCORRETTORIONOFF2(0).BackColor = QBColor(12)
    CHK0.lblCORRETTORIONOFF2(0).Caption = " ENABLED " & Format$(NptABILITATI, "######0") & "/" & Format$(npt + 2, "######0") & " "
  Else
    CHK0.lblCORRETTORIONOFF2(0).ForeColor = CHK0.lblScY.ForeColor
    CHK0.lblCORRETTORIONOFF2(0).BackColor = CHK0.lblScY.BackColor
    CHK0.lblCORRETTORIONOFF2(0).Caption = "All correctors ENABLED"
  End If
  'SEGNALAZIONE UTENTE
  WRITE_DIALOG "SELECTED CORRECTORS HAVE BEEN SAVED!!"
  '
End Sub

Private Sub GraficoCORR_SelChgByMouse()
'
Dim i       As Integer
Dim tmpN()  As Integer
  '
  ReDim IdxCHKtmp(npt)
  '
  For i = 1 To npt
    IdxCHKtmp(i) = 1
  Next i
  '
  tmpN = CHK0.GraficoCORR.SerieGetN("1")
  '
  For i = 1 To UBound(tmpN)
    IdxCHKtmp(tmpN(i)) = 0
  Next i
  '
  Select Case CORvsCHK
    
    Case "COR"
      'AGGIORNAMENTO LISTA PUNTI DA NON CORREGGERE
      CHK0.List3(4).Clear
      Call CHK0.List3(4).AddItem("X")
      For i = 1 To npt
        Call CHK0.List3(4).AddItem(IIf(IdxCHKtmp(i) = 0, "-", "X"))
      Next i
      Call CHK0.List3(4).AddItem("X")
      
    Case "CHK"
      'AGGIORNAMENTO LISTA PUNTI DA NON CONTROLLARE
      CHK0.List3(3).Clear
      Call CHK0.List3(3).AddItem("X")
      For i = 1 To npt
        Call CHK0.List3(3).AddItem(IIf(IdxCHKtmp(i) = 1, " ", "X"))
      Next i
      Call CHK0.List3(3).AddItem("X")
      
  End Select
  '
End Sub

Private Sub grfCONTROLLOONOFF_SelChgByMouse()
'
Dim i       As Integer
Dim tmpN()  As Integer
'
Dim PERCORSO           As String
Dim COORDINATE         As String
Dim RADICE             As String
Dim PERCORSO_CONTROLLI As String
Dim PERCORSO_PROFILI   As String
Dim sequenza           As String
Dim TY_ROT             As String
Dim PASSO              As Double
Dim CONTROLLORE        As String
Dim DM_ROT             As String
Dim INVERSIONE         As Integer
Dim IdxCHKtmp()        As Integer
Dim tX()               As Double
Dim tY()               As Double
Dim tN()               As Integer
Dim dtmp               As Double
Dim Raggio             As Double
Dim ANGOLO             As Double
Dim PRESSIONE          As Double
Dim npt                As Integer
Dim TIPO_LAVORAZIONE   As String
'
Dim stmp  As String
Dim stmp1 As String
Dim stmp2 As String
  '
  
  'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  Select Case TIPO_LAVORAZIONE
     Case "ROTORI_ESTERNI"
          Call LEGGI_INFO_ROTORI(PERCORSO, COORDINATE, RADICE, PERCORSO_CONTROLLI, PERCORSO_PROFILI, sequenza, npt, TY_ROT, PASSO, CONTROLLORE, DM_ROT, INVERSIONE)
     Case "PROFILO_RAB_ESTERNI"
          Call LEGGI_INFO_RAB(RADICE, npt, NP1, TY_ROT, PASSO, CONTROLLORE, DM_ROT)
  End Select
  
  'Call LEGGI_INFO_ROTORI(PERCORSO, COORDINATE, RADICE, PERCORSO_CONTROLLI, PERCORSO_PROFILI, sequenza, npt, TY_ROT, PASSO, CONTROLLORE, DM_ROT, INVERSIONE)
  'Call LEGGI_INFO_RAB(RADICE, npt, NP1, TY_ROT, PASSO, CONTROLLORE, DM_ROT)
  
  CHK0.lstCONTROLLOONOFF.Clear
  ReDim IdxCHKtmp(npt)
  For i = 1 To npt
    IdxCHKtmp(i) = 0
  Next i
  tmpN = CHK0.grfCONTROLLOONOFF.SerieGetN("1")
  For i = 1 To UBound(tmpN)
    IdxCHKtmp(tmpN(i)) = 1
  Next i
  'AGGIORNAMENTO LISTA PUNTI DA NON CORREGGERE
  CHK0.lstCONTROLLOONOFF.Clear
  For i = 1 To npt
    If (IdxCHKtmp(i) = 0) Then
      Call CHK0.lstCONTROLLOONOFF.AddItem(Right$(String$(3, " ") & Format$(i, "##0"), 3) & " -")
    Else
      Call CHK0.lstCONTROLLOONOFF.AddItem(Right$(String$(3, " ") & Format$(i, "##0"), 3) & " X")
    End If
  Next i
  'SALVATAGGIO DEI PUNTI CONTROLLATI
  If (Dir$(RADICE & "\CHKRAB.OLD") <> "") Then Call Kill(RADICE & "\CHKRAB.OLD")
  If (Dir$(RADICE & "\CHKRAB.ROT") <> "") Then Call FileCopy(RADICE & "\CHKRAB.ROT", RADICE & "\CHKRAB.OLD")
  'SALVATAGGIO DEI PUNTI DI CONTROLLO ABILITATI
  Dim NptCONTROLLATI As Long
  Open RADICE & "\CHKRAB.ROT" For Output As #1
    Print #1, "[PuntiControllo]"
    For i = 1 To npt
      If (Right$(CHK0.lstCONTROLLOONOFF.List(i - 1), 1) = "X") Then
        Print #1, "CHK[0," & CStr(i) & "]=0"
      Else
        Print #1, "CHK[0," & CStr(i) & "]=1"
        NptCONTROLLATI = NptCONTROLLATI + 1
      End If
    Next i
  Close #1
  'CALCOLO PROFILO SOLO SE DIVERSO
  Open RADICE & "\CHKRAB.OLD" For Input As #1
    stmp1 = Input$(LOF(1), 1)
  Close #1
  Open RADICE & "\CHKRAB.ROT" For Input As #1
    stmp2 = Input$(LOF(1), 1)
  Close #1
  
  'CONFRONTO FILE
  If (stmp2 <> stmp1) Then
    Select Case TIPO_LAVORAZIONE
      Case "ROTORI_ESTERNI"
          Call CARICA_DATI_CONTROLLO
      Case "PROFILO_RAB_ESTERNI"
          Call CARICA_DATI_CONTROLLO_RAB
    End Select
  End If
  
  If (NptCONTROLLATI < npt) Then
    stmp = " CHECKED " & Format$(NptCONTROLLATI, "######0") & "/" & Format$(npt, "######0") & " "
  Else
    stmp = " All points CHECKED!! "
  End If
  WRITE_DIALOG stmp
  
End Sub

Private Sub grfCORRETTORIONOFF_SelChgByMouse()
'
Dim i       As Integer
Dim tmpN()  As Integer
'
Dim PERCORSO           As String
Dim COORDINATE         As String
Dim RADICE             As String
Dim PERCORSO_CONTROLLI As String
Dim PERCORSO_PROFILI   As String
Dim sequenza           As String
Dim TY_ROT             As String
Dim PASSO              As Double
Dim CONTROLLORE        As String
Dim DM_ROT             As String
Dim INVERSIONE         As Integer
Dim IdxCHKtmp()        As Integer
Dim tX()               As Double
Dim tY()               As Double
Dim tN()               As Integer
Dim dtmp               As Double
Dim Raggio             As Double
Dim ANGOLO             As Double
Dim PRESSIONE          As Double
Dim npt                As Integer
Dim TIPO_LAVORAZIONE   As String
'
  Call LEGGI_INFO_ROTORI(PERCORSO, COORDINATE, RADICE, PERCORSO_CONTROLLI, PERCORSO_PROFILI, sequenza, npt, TY_ROT, PASSO, CONTROLLORE, DM_ROT, INVERSIONE)
  CHK0.lstCORRETTORIONOFF.Clear
  ReDim IdxCHKtmp(npt + 2)
  For i = 1 To npt + 2
    IdxCHKtmp(i) = 1
  Next i
  tmpN = CHK0.grfCORRETTORIONOFF.SerieGetN("1")
  For i = 1 To UBound(tmpN)
    IdxCHKtmp(tmpN(i)) = 0
  Next i
  'AGGIORNAMENTO LISTA PUNTI DA NON CORREGGERE
  CHK0.lstCORRETTORIONOFF.Clear
  For i = 1 To npt + 2
    If (IdxCHKtmp(i) = 0) Then
      Call CHK0.lstCORRETTORIONOFF.AddItem(Right$(String$(3, " ") & Format$(i - 1, "##0"), 3) & " -")
    Else
      Call CHK0.lstCORRETTORIONOFF.AddItem(Right$(String$(3, " ") & Format$(i - 1, "##0"), 3) & " X")
    End If
  Next i
  'SALVATAGGIO DEI CORRETTORI ABILITATI
  Open RADICE & "\ABILITA.COR" For Output As #1
    For i = 0 To npt + 1
      If (Right$(CHK0.lstCORRETTORIONOFF.List(i), 1) = "X") Then
        Print #1, "1"
      Else
        Print #1, "0"
      End If
    Next i
  Close #1
  'SEGNALAZIONE UTENTE
  WRITE_DIALOG "SELECTED CORRECTORS HAVE BEEN SAVED!!"

End Sub

Private Sub hsCORROT_Change()
'
On Error Resume Next
  '
  Call VISUALIZZA_MOLA_CORRETTA(hsCORROT.Value, "N")
  '
End Sub

Private Sub Label7_Click(Index As Integer)
'
Dim i     As Integer
'
Dim COORDINATE         As String
Dim RADICE             As String
Dim PERCORSO           As String
Dim PERCORSO_CONTROLLI As String
Dim PERCORSO_PROFILI   As String
Dim sequenza           As String
Dim TY_ROT             As String
Dim PASSO              As Double
Dim CONTROLLORE        As String
Dim DM_ROT             As String
Dim INVERSIONE         As Integer
Dim IdxCHKtmp()        As Integer
Dim tX()               As Double
Dim tY()               As Double
Dim tN()               As Integer
Dim dtmp               As Double
Dim Raggio             As Double
Dim ANGOLO             As Double
Dim PRESSIONE          As Double
Dim TIPO_LAVORAZIONE   As String
'
On Error Resume Next
  '
  'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  Select Case TIPO_LAVORAZIONE
     Case "ROTORI_ESTERNI"
          Call LEGGI_INFO_ROTORI(PERCORSO, COORDINATE, RADICE, PERCORSO_CONTROLLI, PERCORSO_PROFILI, sequenza, npt, TY_ROT, PASSO, CONTROLLORE, DM_ROT, INVERSIONE)
     Case "PROFILO_RAB_ESTERNI"
          Call LEGGI_INFO_RAB(RADICE, npt, NP1, TY_ROT, PASSO, CONTROLLORE, DM_ROT)
  End Select
  
  'ON/OFF CORRETTORI DEI PUNTI
  If (Index = 4) Then
    CORvsCHK = "COR"
    If (CHK0.GraficoCORR.Visible = False) Then
      CHK0.GraficoCORR.Visible = True
      '
      CHK0.GraficoCORR.Left = 0
      CHK0.GraficoCORR.Top = 0
      CHK0.GraficoCORR.Width = 7000
      CHK0.GraficoCORR.Height = CHK0.Label7(4).Top + CHK0.Label7(4).Height + CHK0.List3(4).Height
      '
      CHK0.Label7(4).Left = CHK0.GraficoCORR.Left + CHK0.GraficoCORR.Width
      CHK0.List3(4).Left = CHK0.GraficoCORR.Left + CHK0.GraficoCORR.Width
      '
      'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
'      TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
'      TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
'      Select Case TIPO_LAVORAZIONE
'        Case "ROTORI_ESTERNI"
'          Call LEGGI_INFO_ROTORI(PERCORSO, COORDINATE, RADICE, PERCORSO_CONTROLLI, PERCORSO_PROFILI, sequenza, npt, TY_ROT, PASSO, CONTROLLORE, DM_ROT, INVERSIONE)
'        Case "PROFILO_RAB_ESTERNI"
'          Call LEGGI_INFO_RAB(RADICE, npt, NP1, TY_ROT, PASSO, CONTROLLORE, DM_ROT)
'      End Select
'
      'Call LEGGI_INFO_ROTORI(PERCORSO, COORDINATE, RADICE, PERCORSO_CONTROLLI, PERCORSO_PROFILI, sequenza, npt, TY_ROT, PASSO, CONTROLLORE, DM_ROT, INVERSIONE)
      '
      ReDim IdxCHKtmp(npt)
      For i = 1 To npt
        If (CHK0.List3(4).List(i) = "X") Then
          IdxCHKtmp(i) = 1  'DA CORREGGERE
        Else
          IdxCHKtmp(i) = 0  'DA NON CORREGGERE
        End If
      Next i
      '
      ReDim tX(npt): ReDim tY(npt)
      ReDim tN(0)
      Open RADICE & "\PRFROT" For Input As #1
      For i = 1 To npt
        Input #1, Raggio, ANGOLO, PRESSIONE
            If (TY_ROT = "FRONT") Or (CONTROLLORE = "INTERNAL") Then
          tX(i) = Raggio * Cos(ANGOLO * PG / 180)
          tY(i) = Raggio * Sin(ANGOLO * PG / 180)
        ElseIf (TY_ROT = "AXIAL") And (CONTROLLORE <> "INTERNAL") Then
          tX(i) = Raggio
          tY(i) = ANGOLO * PASSO / 360
        End If
        If (IdxCHKtmp(i) = 0) Then
          ReDim Preserve tN(UBound(tN) + 1)
          tN(UBound(tN)) = i
        End If
      Next i
      Close #1
      Call GraficoCORR.SetLingua("UK")
      Call GraficoCORR.AddSerieXY(tY, tX, tX, fiblack, True, True, "1")  'PROFILO TEORICO
      Call GraficoCORR.SelPunti("1", tN)
      GraficoCORR.griglia = True
      GraficoCORR.Agganciare = True
    Else
      CHK0.GraficoCORR.Visible = False
      CHK0.Label7(4).Left = CHK0.Label7(2).Left + CHK0.Label7(2).Width
      CHK0.List3(4).Left = CHK0.Label7(4).Left
    End If
  End If
  '
  'ON/OFF CONTROLLO PUNTI
  If (Index = 3) Then
    CORvsCHK = "CHK"
    If (CHK0.GraficoCORR.Visible = False) Then
      CHK0.GraficoCORR.Visible = True
      '
      CHK0.GraficoCORR.Left = CHK0.Label7(3).Left + CHK0.Label7(3).Width
      CHK0.GraficoCORR.Top = 0
      CHK0.GraficoCORR.Width = 7000
      CHK0.GraficoCORR.Height = CHK0.Label7(3).Top + CHK0.Label7(3).Height + CHK0.List3(3).Height
      '
      'Call LEGGI_INFO_ROTORI(PERCORSO, COORDINATE, RADICE, PERCORSO_CONTROLLI, PERCORSO_PROFILI, sequenza, npt, TY_ROT, PASSO, CONTROLLORE, DM_ROT, INVERSIONE)
      '
      ReDim IdxCHKtmp(npt)
      For i = 1 To npt
        If (CHK0.List3(3).List(i) = "X") Then
          IdxCHKtmp(i) = 0  'DA NON CONTROLLARE
        Else
          IdxCHKtmp(i) = 1  'DA CONTROLLARE
        End If
      Next i
      '
      ReDim tX(npt): ReDim tY(npt)
      ReDim tN(0)
      '
      Open RADICE & "\PRFROT" For Input As #1
      For i = 1 To npt
        Input #1, Raggio, ANGOLO, PRESSIONE
            If (TY_ROT = "FRONT") Or (CONTROLLORE = "INTERNAL") Then
          tX(i) = Raggio * Cos(ANGOLO * PG / 180)
          tY(i) = Raggio * Sin(ANGOLO * PG / 180)
        ElseIf (TY_ROT = "AXIAL") And (CONTROLLORE <> "INTERNAL") Then
          tX(i) = Raggio
          tY(i) = ANGOLO * PASSO / 360
        End If
        If (IdxCHKtmp(i) = 0) Then
          ReDim Preserve tN(UBound(tN) + 1)
          tN(UBound(tN)) = i
        End If
      Next i
      Close #1
      Call GraficoCORR.SetLingua("UK")
      Call GraficoCORR.AddSerieXY(tY, tX, tX, fiblack, True, True, "1")  'PROFILO TEORICO
      Call GraficoCORR.SelPunti("1", tN)
      GraficoCORR.griglia = True
      GraficoCORR.Agganciare = True
    Else
      CHK0.GraficoCORR.Visible = False
      Const POSX = 50
      Const POSY = 150
      CHK0.Label7(3).Left = POSX
      CHK0.List3(3).Left = CHK0.Label7(3).Left
    End If
  End If

End Sub

Private Sub lblCORRETTORIONOFF2_Click(Index As Integer)
'
Dim COORDINATE         As String
Dim RADICE             As String
Dim PERCORSO           As String
Dim PERCORSO_CONTROLLI As String
Dim PERCORSO_PROFILI   As String
Dim sequenza           As String
Dim npt                As Integer
Dim TY_ROT             As String
Dim PASSO              As Double
Dim CONTROLLORE        As String
Dim DM_ROT             As String
Dim INVERSIONE         As Integer
'
Dim DDEDIAMETRO(2)     As String
'
Dim stmp  As String
Dim i     As Integer
Dim X     As Single
Dim ret   As Long
Dim TIPO_LAVORAZIONE   As String
'
On Error Resume Next
  '
  'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  Select Case TIPO_LAVORAZIONE
     Case "ROTORI_ESTERNI"
          Call LEGGI_INFO_ROTORI(PERCORSO, COORDINATE, RADICE, PERCORSO_CONTROLLI, PERCORSO_PROFILI, sequenza, npt, TY_ROT, PASSO, CONTROLLORE, DM_ROT, INVERSIONE)
     Case "PROFILO_RAB_ESTERNI"
          Call LEGGI_INFO_RAB(RADICE, npt, NP1, TY_ROT, PASSO, CONTROLLORE, DM_ROT)
  End Select
  
  
  Select Case Index
    Case 0:  CHK0.PrintForm
    Case 1
      If (CHK0.lstCORRETTORIONOFF2.Visible = False) Then
        'Call LEGGI_INFO_ROTORI(PERCORSO, COORDINATE, RADICE, PERCORSO_CONTROLLI, PERCORSO_PROFILI, sequenza, npt, TY_ROT, PASSO, CONTROLLORE, DM_ROT, INVERSIONE)
        CHK0.lblScY.Visible = False
        CHK0.txtScY.Visible = False
        CHK0.lblDIAMETRO(0).Visible = False
        CHK0.txtDIAMETRO(0).Visible = False
        CHK0.lblDIAMETRO(1).Visible = False
        CHK0.txtDIAMETRO(1).Visible = False
        CHK0.lblDIAMETRO(2).Visible = False
        CHK0.txtDIAMETRO(2).Visible = False
        CHK0.fraVANI.Visible = False
        If (Dir$(RADICE & "\ABILITA.COR") = "") Then
          Open RADICE & "\ABILITA.COR" For Output As #1
            For i = 1 To npt + 2
              Print #1, "1"
            Next i
          Close #1
        End If
        CHK0.lstCORRETTORIONOFF2.Clear
        Open RADICE & "\ABILITA.COR" For Input As #2
          'ABILITAZIONE DEL PRIMO PUNTO DELLA MOLA
          i = 0
          Line Input #2, stmp
          If (val(stmp) = 0) Then
            Call CHK0.lstCORRETTORIONOFF2.AddItem(Right$(String$(4, " ") & Format$(i, "##0"), 4) & " -")
          Else
            Call CHK0.lstCORRETTORIONOFF2.AddItem(Right$(String$(4, " ") & Format$(i, "##0"), 4) & " X")
          End If
          'ABILITAZIONE DEI PUNTI MOLA CORRISPONDENTI A QUELLI DEL ROTORE
          For i = 1 To npt
            Line Input #2, stmp
            If (val(stmp) = 0) Then
              Call CHK0.lstCORRETTORIONOFF2.AddItem(Right$(String$(4, " ") & Format$(i, "##0"), 4) & " -")
            Else
              Call CHK0.lstCORRETTORIONOFF2.AddItem(Right$(String$(4, " ") & Format$(i, "##0"), 4) & " X")
            End If
          Next i
          'ABILITAZIONE DELL'ULTIMO PUNTO DELLA MOLA
          i = npt + 1
          Line Input #2, stmp
        Close #2
        If (val(stmp) = 0) Then
          Call CHK0.lstCORRETTORIONOFF2.AddItem(Right$(String$(4, " ") & Format$(i, "##0"), 4) & " -")
        Else
          Call CHK0.lstCORRETTORIONOFF2.AddItem(Right$(String$(4, " ") & Format$(i, "##0"), 4) & " X")
        End If
        CHK0.lstCORRETTORIONOFF2.Left = CHK0.lblScY.Left
        CHK0.lstCORRETTORIONOFF2.Top = CHK0.lblScY.Top
        CHK0.lstCORRETTORIONOFF2.Width = CHK0.lblScY.Width + CHK0.txtScY.Width
        CHK0.lstCORRETTORIONOFF2.Height = CHK0.lblScY.Height + CHK0.Label2(2).Height + CHK0.Label2(3).Height + CHK0.Label2(4).Height + CHK0.fraVANI.Height
        CHK0.lstCORRETTORIONOFF2.Font.Name = "Courier New"
        CHK0.lstCORRETTORIONOFF2.Visible = True
        WRITE_DIALOG "List selection enabled!!!"
      Else
        CHK0.lstCORRETTORIONOFF2.Visible = False
        CHK0.lblScY.Visible = True
        CHK0.txtScY.Visible = True
        DDEDIAMETRO(0) = GetInfo("CONTROLLO", "DDEDIAMETRO(0)", Path_LAVORAZIONE_INI): DDEDIAMETRO(0) = Trim$(DDEDIAMETRO(0))
        If (Len(DDEDIAMETRO(0)) > 0) Then
          CHK0.lblDIAMETRO(0).Visible = True
          CHK0.txtDIAMETRO(0).Visible = True
        End If
        DDEDIAMETRO(1) = GetInfo("CONTROLLO", "DDEDIAMETRO(1)", Path_LAVORAZIONE_INI): DDEDIAMETRO(1) = Trim$(DDEDIAMETRO(1))
        If (Len(DDEDIAMETRO(1)) > 0) Then
          CHK0.lblDIAMETRO(1).Visible = True
          CHK0.txtDIAMETRO(1).Visible = True
        End If
        DDEDIAMETRO(2) = GetInfo("CONTROLLO", "DDEDIAMETRO(2)", Path_LAVORAZIONE_INI): DDEDIAMETRO(2) = Trim$(DDEDIAMETRO(2))
        If (Len(DDEDIAMETRO(2)) > 0) Then
          CHK0.lblDIAMETRO(2).Visible = True
          CHK0.txtDIAMETRO(2).Visible = True
        End If
        stmp = GetInfo("CONTROLLO", "SiMEDIA", Path_LAVORAZIONE_INI)
        If (Len(stmp) > 0) Then CHK0.fraVANI.Visible = True
        WRITE_DIALOG ""
      End If
  End Select
  '
End Sub

Private Sub List3_Click(Index As Integer)
'
Dim i As Integer
'
On Error Resume Next
  '
  For i = 0 To 4
    If (i <> Index) Then List3(i).ListIndex = List3(Index).ListIndex
  Next i
  txtCORR(0).Text = List3(0).List(List3(Index).ListIndex)
  txtCORR(1).Text = List3(1).List(List3(Index).ListIndex)
  txtCORR(2).Text = List3(2).List(List3(Index).ListIndex)
  '
End Sub

Private Sub List3_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
'
Dim LISTAtmp() As String
Dim i          As Integer
Dim nMAX       As Integer
'
On Error Resume Next
  '
  If (KeyCode = 13) Then
    If (Index > 0) Then txtCORR(Index).SetFocus
  End If
  'MODIFICA DELLA LISTA DEI CORRETTORI ABILITATI
  If (KeyCode = 46) And (Index = 4) Then
    nMAX = List3(Index).ListCount
    ReDim Preserve LISTAtmp(nMAX)
    For i = 1 To nMAX
      If (List3(Index).Selected(i - 1) = True) Then
        If (List3(Index).List(i - 1) = "X") Then
          LISTAtmp(i) = "-"
        Else
          LISTAtmp(i) = "X"
        End If
      Else
        'NON MODIFICA QUELLI NON SELEZIONATI
        LISTAtmp(i) = List3(Index).List(i - 1)
      End If
    Next i
    List3(Index).Clear
    For i = 1 To nMAX
      Call List3(Index).AddItem(LISTAtmp(i))
    Next i
  End If
  'MODIFICA DELLA LISTA DEI PUNTI CONTROLLATI
  If (KeyCode = 46) And (Index = 3) Then
    nMAX = List3(Index).ListCount
    ReDim Preserve LISTAtmp(nMAX)
    For i = 1 To nMAX
      If (List3(Index).Selected(i - 1) = True) Then
        If (List3(Index).List(i - 1) = "X") Then
          LISTAtmp(i) = " "
        Else
          LISTAtmp(i) = "X"
        End If
      Else
        'NON MODIFICA QUELLI NON SELEZIONATI
        LISTAtmp(i) = List3(Index).List(i - 1)
      End If
    Next i
    'IL PRIMO ED ULTIMO DEVEONO ESSERE SEMPRE X
    LISTAtmp(1) = "X": LISTAtmp(nMAX) = "X"
    List3(Index).Clear
    For i = 1 To nMAX
      Call List3(Index).AddItem(LISTAtmp(i))
    Next i
  End If
  '
End Sub

Private Sub lstARCHIVIO_Click()

On Error Resume Next

  CHK0.lblARCHIVIO.Caption = "PRESS INPUT TO ASSIGN --> [" & CHK0.lstARCHIVIO.List(CHK0.lstARCHIVIO.ListIndex) & "] TO [" & CHK0.Text2.Text & "]"
  
End Sub

Private Sub lstARCHIVIO_DblClick()

On Error Resume Next

  SendKeys "{ENTER}"
  
End Sub

Private Sub lstARCHIVIO_GotFocus()

On Error Resume Next

  CHK0.lblARCHIVIO.Caption = "PRESS INPUT TO ASSIGN --> [" & CHK0.lstARCHIVIO.List(CHK0.lstARCHIVIO.ListIndex) & "] TO [" & CHK0.Text2.Text & "]"
  
End Sub

Private Sub lstARCHIVIO_KeyDown(KeyCode As Integer, Shift As Integer)

Dim TIPO_LAVORAZIONE As String
Dim CODICE_PEZZO     As String
Dim i                As Long
Dim rsp              As Long
Dim stmp             As String
Dim k1               As Double
Dim ftmp             As Double
Dim FileCONTROLLO    As String
Dim RIGHE()          As String
Dim ASSEGNAZIONE     As String

On Error Resume Next
  '
  If (KeyCode = 13) Then
    ASSEGNAZIONE = CHK0.lstARCHIVIO.List(CHK0.lstARCHIVIO.ListIndex)
    FileCONTROLLO = UCase$(Trim$(CHK0.Text2.Text))
    'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
    TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
    TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
    Select Case TIPO_LAVORAZIONE
      '
      Case "VITI_ESTERNE"
        Open PathMIS & "\" & FileCONTROLLO For Input As 1
          For i = 1 To 6
            Line Input #1, CODICE_PEZZO
          Next i
        Close #1
        CODICE_PEZZO = UCase$(Trim$(CODICE_PEZZO))
        '
      Case Else
        Open PathMIS & "\" & FileCONTROLLO For Input As 1
          Line Input #1, stmp
          Line Input #1, stmp
          Line Input #1, stmp
          Input #1, ftmp, rsp  '0=SERIE10, 1=840D 2=840D CON TOLLERANZE
          If (rsp = 2) Then
            'CODICE DEL PEZZO SALVATO NEL FILE DI CONTROLLO
            Input #1, k1, ftmp, k1, k1, k1, k1, CODICE_PEZZO
          Else
            'CODICE DEL PEZZO COME IL NOME DEL FILE DI CONTROLLO
            CODICE_PEZZO = GetInfo("CHK0", "FileMisura", Path_LAVORAZIONE_INI): CODICE_PEZZO = UCase$(Trim$(CODICE_PEZZO))
            'LEGGO COMUNQUE FINO AL PRIMO SPAZIO
            rsp = InStr(CODICE_PEZZO, " "): If rsp > 0 Then CODICE_PEZZO = Left$(CODICE_PEZZO, rsp - 1)
            'LEGGO COMUNQUE FINO AL PRIMO PUNTO
            rsp = InStr(CODICE_PEZZO, "."): If rsp > 0 Then CODICE_PEZZO = Left$(CODICE_PEZZO, rsp - 1)
          End If
        Close #1
        CODICE_PEZZO = UCase$(Trim$(CODICE_PEZZO))
        '
    End Select
    '
    StopRegieEvents
    rsp = MsgBox(CODICE_PEZZO & " --> " & ASSEGNAZIONE, vbYesNo + vbCritical + vbDefaultButton2, "WARNING: CONFIRM ASSIGNMENT")
    ResumeRegieEvents
    '
    If (rsp = 6) Then
      '
      Select Case TIPO_LAVORAZIONE
        '
        Case "VITI_ESTERNE"
          'LETTURA FILE
          Open PathMIS & "\" & FileCONTROLLO For Input As 1
            i = 1
            While Not EOF(1)
              ReDim Preserve RIGHE(i)
              
              Line Input #1, RIGHE(i)
              i = i + 1
            Wend
          Close #1
          'SCRITTURA FILE
          Open PathMIS & "\" & FileCONTROLLO For Output As 1
            i = 1
            For i = 1 To 5
              Print #1, RIGHE(i)
            Next i
            Print #1, ASSEGNAZIONE
            For i = 7 To UBound(RIGHE)
              Print #1, RIGHE(i)
            Next i
          Close #1
          '
        Case Else
          'LETTURA FILE
          Open PathMIS & "\" & FileCONTROLLO For Input As 1
            '
            'righe(4) '0=SERIE10, 1=840D 2=840D CON TOLLERANZE
            '
            'righe(5) 'CODICE DEL PEZZO SALVATO NEL FILE DI CONTROLLO
            i = 1
            While Not EOF(1)
              ReDim Preserve RIGHE(i)
                Line Input #1, RIGHE(i)
              i = i + 1
            Wend
          Close #1
          '
          'SCRITTURA FILE
          Open PathMIS & "\" & FileCONTROLLO For Output As 1
            i = 1
            For i = 1 To 3
              Print #1, RIGHE(i)
            Next i
            'DIVENTA DI TIPO 2
            RIGHE(4) = Trim$(RIGHE(4))
            Print #1, Left$(RIGHE(4), InStr(RIGHE(4), " ") - 1) & " 2"
            RIGHE(5) = Trim$(RIGHE(5))
            If InStr(RIGHE(5), CODICE_PEZZO) > 0 Then
              Print #1, Left$(RIGHE(5), InStr(RIGHE(5), CODICE_PEZZO) - 1) & " " & ASSEGNAZIONE
            Else
              Print #1, RIGHE(5) & " " & ASSEGNAZIONE
            End If
            For i = 6 To UBound(RIGHE)
              Print #1, RIGHE(i)
            Next i
          Close #1
        '
      End Select
      If (Dir$(PathMIS & "\DEFAULT") <> "") Then Kill PathMIS & "\DEFAULT"
      Call FileCopy(PathMIS & "\" & FileCONTROLLO, PathMIS & "\DEFAULT")
      WRITE_DIALOG CODICE_PEZZO & " --> " & ASSEGNAZIONE & " OK"
    Else
      WRITE_DIALOG "Operation aborted by user !!"
    End If
    '
  End If
  '
End Sub

Private Sub lstARCHIVIO_LostFocus()

On Error Resume Next

  CHK0.lblARCHIVIO.Caption = ""
  
End Sub

Private Sub lstCONTROLLOONOFF_DblClick()
  
  Call lstCONTROLLOONOFF_KeyUp(vbKeyReturn, 0)
  
End Sub

Private Sub lstCONTROLLOONOFF_KeyUp(KeyCode As Integer, Shift As Integer)

Dim i     As Integer
Dim sel   As Boolean
Dim tstr  As String
'
Dim PERCORSO           As String
Dim COORDINATE         As String
Dim RADICE             As String
Dim PERCORSO_CONTROLLI As String
Dim PERCORSO_PROFILI   As String
Dim sequenza           As String
Dim TY_ROT             As String
Dim PASSO              As Double
Dim CONTROLLORE        As String
Dim DM_ROT             As String
Dim INVERSIONE         As Integer
Dim IdxCHKtmp()        As Integer
Dim tX()               As Double
Dim tY()               As Double
Dim tN()               As Integer
Dim dtmp               As Double
Dim Raggio             As Double
Dim ANGOLO             As Double
Dim PRESSIONE          As Double
Dim npt                As Integer
Dim stmp1              As String
Dim stmp2              As String
Dim TIPO_LAVORAZIONE   As String
  '
  'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  Select Case TIPO_LAVORAZIONE
     Case "ROTORI_ESTERNI"
          Call LEGGI_INFO_ROTORI(PERCORSO, COORDINATE, RADICE, PERCORSO_CONTROLLI, PERCORSO_PROFILI, sequenza, npt, TY_ROT, PASSO, CONTROLLORE, DM_ROT, INVERSIONE)
     Case "PROFILO_RAB_ESTERNI"
          Call LEGGI_INFO_RAB(RADICE, npt, NP1, TY_ROT, PASSO, CONTROLLORE, DM_ROT)
  End Select
  
  
  Select Case KeyCode
  
    Case vbKeyReturn
      If (lstCONTROLLOONOFF.List(lstCONTROLLOONOFF.ListIndex) = "") Then
        Exit Sub
      End If
      i = CInt(Left(lstCONTROLLOONOFF.List(lstCONTROLLOONOFF.ListIndex), 3))
      sel = (UCase(Right(lstCONTROLLOONOFF.List(lstCONTROLLOONOFF.ListIndex), 1)) = "X")
      If sel Then
        Call grfCONTROLLOONOFF.SerieNRem(i, "1")
      Else
        Call grfCONTROLLOONOFF.SerieNAgg(i, "1")
      End If
      tstr = Right$("   " & i, 3) & IIf(sel, " -", " X")
      lstCONTROLLOONOFF.List(lstCONTROLLOONOFF.ListIndex) = tstr
      'Call LEGGI_INFO_ROTORI(PERCORSO, COORDINATE, RADICE, PERCORSO_CONTROLLI, PERCORSO_PROFILI, sequenza, npt, TY_ROT, PASSO, CONTROLLORE, DM_ROT, INVERSIONE)
      'SALVATAGGIO DEI PUNTI CONTROLLATI
      If (Dir$(RADICE & "\CHKRAB.OLD") <> "") Then Call Kill(RADICE & "\CHKRAB.OLD")
      If (Dir$(RADICE & "\CHKRAB.ROT") <> "") Then Call FileCopy(RADICE & "\CHKRAB.ROT", RADICE & "\CHKRAB.OLD")
      'SALVATAGGIO DEI PUNTI DI CONTROLLO ABILITATI
      Open RADICE & "\CHKRAB.ROT" For Output As #1
        Print #1, "[PuntiControllo]"
        For i = 1 To npt
          If (Right$(CHK0.lstCONTROLLOONOFF.List(i - 1), 1) = "X") Then
            Print #1, "CHK[0," & CStr(i) & "]=0"
          Else
            Print #1, "CHK[0," & CStr(i) & "]=1"
          End If
        Next i
      Close #1
      'CALCOLO PROFILO SOLO SE DIVERSO
      Open RADICE & "\CHKRAB.OLD" For Input As #1
        stmp1 = Input$(LOF(1), 1)
      Close #1
      Open RADICE & "\CHKRAB.ROT" For Input As #1
        stmp2 = Input$(LOF(1), 1)
      Close #1
      'CONFRONTO FILE
    If (stmp2 <> stmp1) Then
      Select Case TIPO_LAVORAZIONE
        Case "ROTORI_ESTERNI"
          Call CARICA_DATI_CONTROLLO
        Case "PROFILO_RAB_ESTERNI"
          Call CARICA_DATI_CONTROLLO_RAB
      End Select
    End If
    
    Case 46
      'Call LEGGI_INFO_ROTORI(PERCORSO, COORDINATE, RADICE, PERCORSO_CONTROLLI, PERCORSO_PROFILI, sequenza, npt, TY_ROT, PASSO, CONTROLLORE, DM_ROT, INVERSIONE)
      For i = 1 To npt
        If (CHK0.lstCONTROLLOONOFF.Selected(i - 1) = True) Then
          If (Right$(CHK0.lstCONTROLLOONOFF.List(i - 1), 1) = "X") Then
            CHK0.lstCONTROLLOONOFF.List(i - 1) = Right$(String$(3, " ") & Format$(i, "##0"), 3) & " -"
            Call grfCONTROLLOONOFF.SerieNRem(i, "1")
          Else
            CHK0.lstCONTROLLOONOFF.List(i - 1) = Right$(String$(3, " ") & Format$(i, "##0"), 3) & " X"
            Call grfCONTROLLOONOFF.SerieNAgg(i, "1")
          End If
        End If
      Next i
      'SALVATAGGIO DEI PUNTI CONTROLLATI
      If (Dir$(RADICE & "\CHKRAB.OLD") <> "") Then Call Kill(RADICE & "\CHKRAB.OLD")
      If (Dir$(RADICE & "\CHKRAB.ROT") <> "") Then Call FileCopy(RADICE & "\CHKRAB.ROT", RADICE & "\CHKRAB.OLD")
      'SALVATAGGIO DEI PUNTI DI CONTROLLO ABILITATI
      Open RADICE & "\CHKRAB.ROT" For Output As #1
        Print #1, "[PuntiControllo]"
        For i = 1 To npt
          If (Right$(CHK0.lstCONTROLLOONOFF.List(i - 1), 1) = "X") Then
            Print #1, "CHK[0," & CStr(i) & "]=0"
          Else
            Print #1, "CHK[0," & CStr(i) & "]=1"
          End If
        Next i
      Close #1
      'CALCOLO PROFILO SOLO SE DIVERSO
      Open RADICE & "\CHKRAB.OLD" For Input As #1
        stmp1 = Input$(LOF(1), 1)
      Close #1
      Open RADICE & "\CHKRAB.ROT" For Input As #1
        stmp2 = Input$(LOF(1), 1)
      Close #1
      'CONFRONTO FILE
      If (stmp2 <> stmp1) Then
        Select Case TIPO_LAVORAZIONE
          Case "ROTORI_ESTERNI"
            Call CARICA_DATI_CONTROLLO
          Case "PROFILO_RAB_ESTERNI"
            Call CARICA_DATI_CONTROLLO_RAB
        End Select
      End If
      
  End Select

End Sub

Private Sub lstCORRETTORIONOFF_DblClick()
  
  Call lstCORRETTORIONOFF_KeyUp(vbKeyReturn, 0)
  
End Sub

Private Sub lstCORRETTORIONOFF_KeyUp(KeyCode As Integer, Shift As Integer)
'
Dim i     As Integer
Dim sel   As Boolean
Dim tstr  As String
'
Dim PERCORSO           As String
Dim COORDINATE         As String
Dim RADICE             As String
Dim PERCORSO_CONTROLLI As String
Dim PERCORSO_PROFILI   As String
Dim sequenza           As String
Dim TY_ROT             As String
Dim PASSO              As Double
Dim CONTROLLORE        As String
Dim DM_ROT             As String
Dim INVERSIONE         As Integer
Dim IdxCHKtmp()        As Integer
Dim tX()               As Double
Dim tY()               As Double
Dim tN()               As Integer
Dim dtmp               As Double
Dim Raggio             As Double
Dim ANGOLO             As Double
Dim PRESSIONE          As Double
Dim npt                As Integer
Dim TIPO_LAVORAZIONE   As String
  '
  Select Case KeyCode
  
    Case vbKeyReturn
      If (lstCORRETTORIONOFF.List(lstCORRETTORIONOFF.ListIndex) = "") Then
        Exit Sub
      End If
      i = CInt(Left(lstCORRETTORIONOFF.List(lstCORRETTORIONOFF.ListIndex), 3))
      sel = (UCase(Right(lstCORRETTORIONOFF.List(lstCORRETTORIONOFF.ListIndex), 1)) = "X")
      If sel Then
        Call grfCORRETTORIONOFF.SerieNAgg(i + 1, "1")
      Else
        Call grfCORRETTORIONOFF.SerieNRem(i + 1, "1")
      End If
      tstr = Right$("   " & i, 3) & IIf(sel, " -", " X")
      lstCORRETTORIONOFF.List(lstCORRETTORIONOFF.ListIndex) = tstr
      Call LEGGI_INFO_ROTORI(PERCORSO, COORDINATE, RADICE, PERCORSO_CONTROLLI, PERCORSO_PROFILI, sequenza, npt, TY_ROT, PASSO, CONTROLLORE, DM_ROT, INVERSIONE)
      'SALVATAGGIO DEI CORRETTORI ABILITATI
      Open RADICE & "\ABILITA.COR" For Output As #1
        For i = 0 To npt + 1
          If (Right$(CHK0.lstCORRETTORIONOFF.List(i), 1) = "X") Then
            Print #1, "1"
          Else
            Print #1, "0"
          End If
        Next i
      Close #1
      'SEGNALAZIONE UTENTE
      WRITE_DIALOG "SELECTED CORRECTORS HAVE BEEN SAVED!!"
    
    Case 46
      Call LEGGI_INFO_ROTORI(PERCORSO, COORDINATE, RADICE, PERCORSO_CONTROLLI, PERCORSO_PROFILI, sequenza, npt, TY_ROT, PASSO, CONTROLLORE, DM_ROT, INVERSIONE)
      For i = 1 To npt + 2
        If (CHK0.lstCORRETTORIONOFF.Selected(i - 1) = True) Then
          If (Right$(CHK0.lstCORRETTORIONOFF.List(i - 1), 1) = "X") Then
            CHK0.lstCORRETTORIONOFF.List(i - 1) = Right$(String$(3, " ") & Format$(i, "##0"), 3) & " -"
            Call grfCORRETTORIONOFF.SerieNAgg(i, "1")
          Else
            CHK0.lstCORRETTORIONOFF.List(i - 1) = Right$(String$(3, " ") & Format$(i, "##0"), 3) & " X"
            Call grfCORRETTORIONOFF.SerieNRem(i, "1")
          End If
        End If
      Next i
      'SALVATAGGIO DEI CORRETTORI ABILITATI
      Open RADICE & "\ABILITA.COR" For Output As #1
        For i = 0 To npt + 1
          If (Right$(CHK0.lstCORRETTORIONOFF.List(i), 1) = "X") Then
            Print #1, "1"
          Else
            Print #1, "0"
          End If
        Next i
      Close #1
      'SEGNALAZIONE UTENTE
      WRITE_DIALOG "SELECTED CORRECTORS HAVE BEEN SAVED!!"
    
  End Select

End Sub

Private Sub lstCORRETTORIONOFF2_DblClick()
  
  Call lstCORRETTORIONOFF2_KeyUP(vbKeyReturn, 0)
  
End Sub

Private Sub lstCORRETTORIONOFF2_KeyUP(KeyCode As Integer, Shift As Integer)
'
Dim i     As Integer
Dim sel   As Boolean
Dim tstr  As String
'
Dim PERCORSO           As String
Dim COORDINATE         As String
Dim RADICE             As String
Dim PERCORSO_CONTROLLI As String
Dim PERCORSO_PROFILI   As String
Dim sequenza           As String
Dim TY_ROT             As String
Dim PASSO              As Double
Dim CONTROLLORE        As String
Dim DM_ROT             As String
Dim INVERSIONE         As Integer
Dim npt                As Integer
Dim NptABILITATI       As Integer
Dim TIPO_LAVORAZIONE   As String
'
On Error Resume Next
'
  
  'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  Select Case TIPO_LAVORAZIONE
     Case "ROTORI_ESTERNI"
          Call LEGGI_INFO_ROTORI(PERCORSO, COORDINATE, RADICE, PERCORSO_CONTROLLI, PERCORSO_PROFILI, sequenza, npt, TY_ROT, PASSO, CONTROLLORE, DM_ROT, INVERSIONE)
     Case "PROFILO_RAB_ESTERNI"
          Call LEGGI_INFO_RAB(RADICE, npt, NP1, TY_ROT, PASSO, CONTROLLORE, DM_ROT)
  End Select
  
  Select Case KeyCode
  
    Case vbKeyReturn
      If (lstCORRETTORIONOFF2.List(lstCORRETTORIONOFF2.ListIndex) = "") Then
        Exit Sub
      End If
      i = CInt(Left(lstCORRETTORIONOFF2.List(lstCORRETTORIONOFF2.ListIndex), 4))
      sel = (UCase(Right(lstCORRETTORIONOFF2.List(lstCORRETTORIONOFF2.ListIndex), 1)) = "X")
      If sel Then
        Call Grafico1.SerieNAgg(i + 1, "1")
      Else
        Call Grafico1.SerieNRem(i + 1, "1")
      End If
      tstr = Right$("    " & i, 4) & IIf(sel, " -", " X")
      lstCORRETTORIONOFF2.List(lstCORRETTORIONOFF2.ListIndex) = tstr
      'Call LEGGI_INFO_ROTORI(PERCORSO, COORDINATE, RADICE, PERCORSO_CONTROLLI, PERCORSO_PROFILI, sequenza, npt, TY_ROT, PASSO, CONTROLLORE, DM_ROT, INVERSIONE)
      'SALVATAGGIO DEI CORRETTORI ABILITATI
      NptABILITATI = 0
      Open RADICE & "\ABILITA.COR" For Output As #1
        For i = 0 To npt + 1
          If (Right$(CHK0.lstCORRETTORIONOFF2.List(i), 1) = "X") Then
            Print #1, "1"
            NptABILITATI = NptABILITATI + 1
          Else
            Print #1, "0"
          End If
        Next i
      Close #1
      If (NptABILITATI < npt + 2) Then
        CHK0.lblCORRETTORIONOFF2(0).ForeColor = QBColor(15)
        CHK0.lblCORRETTORIONOFF2(0).BackColor = QBColor(12)
        CHK0.lblCORRETTORIONOFF2(0).Caption = " ENABLED " & Format$(NptABILITATI, "######0") & "/" & Format$(npt + 2, "######0") & " "
      Else
        CHK0.lblCORRETTORIONOFF2(0).ForeColor = CHK0.lblScY.ForeColor
        CHK0.lblCORRETTORIONOFF2(0).BackColor = CHK0.lblScY.BackColor
        CHK0.lblCORRETTORIONOFF2(0).Caption = "All correctors ENABLED"
      End If
      'SEGNALAZIONE UTENTE
      WRITE_DIALOG "SELECTED CORRECTORS HAVE BEEN SAVED!!"
    
    Case 46
      'Call LEGGI_INFO_ROTORI(PERCORSO, COORDINATE, RADICE, PERCORSO_CONTROLLI, PERCORSO_PROFILI, sequenza, npt, TY_ROT, PASSO, CONTROLLORE, DM_ROT, INVERSIONE)
      For i = 0 To npt + 1
        If (CHK0.lstCORRETTORIONOFF2.Selected(i) = True) Then
          'If (Right$(CHK0.lstCORRETTORIONOFF2.List(i), 1) = "X") Then
            CHK0.lstCORRETTORIONOFF2.List(i) = Right$(String$(4, " ") & Format$(i, "##0"), 4) & " -"
          'Else
          '  CHK0.lstCORRETTORIONOFF2.List(i) = Right$(String$(4, " ") & Format$(i, "##0"), 4) & " X"
          'End If
        End If
      Next i
      'SALVATAGGIO DEI CORRETTORI ABILITATI
      Open RADICE & "\ABILITA.COR" For Output As #1
        NptABILITATI = 0
        For i = 0 To npt + 1
          If (Right$(CHK0.lstCORRETTORIONOFF2.List(i), 1) = "X") Then
            Print #1, "1"
            Call Grafico1.SerieNRem(i + 1, "1")
            NptABILITATI = NptABILITATI + 1
          Else
            Print #1, "0"
            Call Grafico1.SerieNAgg(i + 1, "1")
          End If
        Next i
      Close #1
      If (NptABILITATI < npt + 2) Then
        CHK0.lblCORRETTORIONOFF2(0).ForeColor = QBColor(15)
        CHK0.lblCORRETTORIONOFF2(0).BackColor = QBColor(12)
        CHK0.lblCORRETTORIONOFF2(0).Caption = " ENABLED " & Format$(NptABILITATI, "######0") & "/" & Format$(npt + 2, "######0") & " "
      Else
        CHK0.lblCORRETTORIONOFF2(0).ForeColor = CHK0.lblScY.ForeColor
        CHK0.lblCORRETTORIONOFF2(0).BackColor = CHK0.lblScY.BackColor
        CHK0.lblCORRETTORIONOFF2(0).Caption = "All correctors ENABLED"
      End If
      'SEGNALAZIONE UTENTE
      WRITE_DIALOG "SELECTED CORRECTORS HAVE BEEN SAVED!!"
    
  End Select

End Sub

Private Sub optVANI_GotFocus(Index As Integer)
'
On Error Resume Next
  '
  CHK0.optVANI(Index).ForeColor = &H80000014
  CHK0.optVANI(Index).BackColor = &HFF&
  '
End Sub

Private Sub optVANI_LostFocus(Index As Integer)
'
Dim i As Integer
'
On Error Resume Next
  '
  For i = 0 To 3
    CHK0.optVANI(i).ForeColor = &H80000012
    CHK0.optVANI(i).BackColor = &HEBE1D7
  Next
  '
End Sub

Private Sub Text1_GotFocus(Index As Integer)
'
On Error Resume Next
  '
  CHK0.Label2(Index).BackColor = QBColor(12)
  CHK0.Label2(Index).ForeColor = QBColor(15)
  Call SELEZIONA_TESTO

End Sub

Private Sub Text1_KeyUp(Index As Integer, KeyCode As Integer, Shift As Integer)
'
Dim TIPO_LAVORAZIONE As String
'
On Error Resume Next
  '
  'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  Select Case KeyCode
    Case 33:  SendKeys "+{TAB}"     'INIZIO LISTA
    Case 34:  SendKeys "{TAB}"      'FINE LISTA
    Case 40:  SendKeys "{TAB}"      'AVANTI
    Case 38:  SendKeys "+{TAB}"     'INDIETRO
    Case 13
      Select Case TIPO_LAVORAZIONE
        Case "CREATORI_STANDARD":   Call CONTROLLO_CREATORI_VISUALIZZAZIONE(val(CHK0.Text1(0).Text), val(CHK0.Text1(1).Text))
        Case "CREATORI_FORMA":      Call CONTROLLO_CRFORMA_VISUALIZZAZIONE(val(CHK0.Text1(0).Text), val(CHK0.Text1(1).Text))
        Case "ROTORI_ESTERNI":      Call CONTROLLO_ROTORI_VISUALIZZAZIONE '(val(CHK0.Text1(0).Text), val(CHK0.Text1(1).Text))
        Case "INGR380_ESTERNI":     Call VISUALIZZA_CONTROLLO
        Case "INGRANAGGI_ESTERNIV": Call VISUALIZZA_CONTROLLO
        Case "INGRANAGGI_ESTERNIO": Call VISUALIZZA_CONTROLLO
        Case "MOLAVITE":            Call VISUALIZZA_CONTROLLO
        'Case "MOLAVITE_SG":         Call VISUALIZZA_CONTROLLO
        Case "DENTATURA":           Call VISUALIZZA_CONTROLLO
        Case "SCANALATIEVO":        Call VISUALIZZA_CONTROLLO
        Case "VITI_ESTERNE":        Call VISUALIZZA_CONTROLLO
        Case "INGRANAGGI_INTERNI":  Call VISUALIZZA_CONTROLLO
      End Select
  End Select
  
End Sub

Private Sub Text1_LostFocus(Index As Integer)
  
On Error Resume Next
    
  CHK0.Label2(Index).BackColor = &HFFFFFF
  CHK0.Label2(Index).ForeColor = QBColor(0)

End Sub

Private Sub Text2_GotFocus()

On Error Resume Next

  Call SELEZIONA_TESTO

End Sub

Private Sub txtCORR_GotFocus(Index As Integer)

On Error Resume Next

  Call SELEZIONA_TESTO
  
End Sub

Private Sub txtCORR_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
'
Dim i    As Integer
Dim nMAX As Integer
'
On Error Resume Next
  '
  Select Case KeyCode
    '
    Case 33
      'PRIMO ELEMENTO
      List3(Index).ListIndex = 0
      '
    Case 34
      'FINE LISTA
      nMAX = List3(Index).ListCount
      If (nMAX > 10) Then
        i = List3(Index).ListIndex + 10
        If i >= nMAX Then i = nMAX - 1
      Else
        'VADO SULL'ULTIMO
        i = nMAX - 1
      End If
      List3(Index).ListIndex = i
      '
    Case 38
      'ELEMENTO PRECEDENTE
      i = List3(Index).ListIndex
      If i > 0 Then List3(Index).ListIndex = i - 1
      '
    Case 40
      'ELEMENTO SUCCESSIVO
      i = List3(Index).ListIndex
      If i < List3(0).ListCount Then List3(Index).ListIndex = i + 1
      '
    Case 13
      i = val(txtCORR(0).Text)
      Select Case Index
        '
        Case 0
          If (i > 0) And (i <= List3(0).ListCount) Then
            List3(0).ListIndex = i
          End If
          '
        Case 1
          If (val(txtCORR(1).Text) <> CorXX(i + 1)) Then
            CorXX(i + 1) = val(txtCORR(1).Text)
            If (CorXX(i + 1) >= 0) Then
              List3(1).List(i) = "+" & frmt0(Abs(CorXX(i + 1)), 4)
            Else
              List3(1).List(i) = "-" & frmt0(Abs(CorXX(i + 1)), 4)
            End If
          End If
          '
        Case 2
          If (val(txtCORR(2).Text) <> CorYY(i + 1)) Then
            CorYY(i + 1) = val(txtCORR(2).Text)
            If (CorYY(i + 1) >= 0) Then
              List3(2).List(i) = "+" & frmt0(Abs(CorYY(i + 1)), 4)
            Else
              List3(2).List(i) = "-" & frmt0(Abs(CorYY(i + 1)), 4)
            End If
          End If
          '
      End Select
      '
  End Select
  '
End Sub

Private Sub txtDIAMETRO_GotFocus(Index As Integer)
'
On Error Resume Next
  
  CHK0.lblDIAMETRO(Index).BackColor = QBColor(12)
  CHK0.lblDIAMETRO(Index).ForeColor = QBColor(15)
  Call SELEZIONA_TESTO

End Sub

Private Sub txtDIAMETRO_LostFocus(Index As Integer)
'
Dim i As Integer
'
On Error Resume Next
  '
  For i = 0 To 2
    CHK0.lblDIAMETRO(i).BackColor = &HFFFFFF
    CHK0.lblDIAMETRO(i).ForeColor = QBColor(0)
  Next i

End Sub

Private Sub txtEDITOR_GotFocus()
'
On Error Resume Next
  '
  lblEDITOR_INDICE.Caption = CStr(CurrentLine(txtEDITOR)) & "/" & CStr(NoOfLines(txtEDITOR))
  '
End Sub

Private Sub txtEDITOR_KeyUp(KeyCode As Integer, Shift As Integer)
'
On Error Resume Next
  '
  lblEDITOR_INDICE.Caption = CStr(CurrentLine(txtEDITOR)) & "/" & CStr(NoOfLines(txtEDITOR))
  '
End Sub

Private Sub txtEDITOR_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
'
On Error Resume Next
  '
  lblEDITOR_INDICE.Caption = CStr(CurrentLine(txtEDITOR)) & "/" & CStr(NoOfLines(txtEDITOR))
  '
End Sub

Private Sub txtPBBF1_GotFocus(Index As Integer)

On Error Resume Next
  
  IF1(Index).BackColor = QBColor(12)
  IF1(Index).ForeColor = QBColor(15)
  lblBB(0).BackColor = QBColor(12)
  lblBB(0).ForeColor = QBColor(15)
  Call SELEZIONA_TESTO
  
End Sub

Private Sub txtPBBF1_KeyUp(Index As Integer, KeyCode As Integer, Shift As Integer)

Dim rsp As Integer

On Error Resume Next
  '
  WRITE_DIALOG ""
  Select Case KeyCode
    '
    Case 33
      'INIZIO LISTA
      txtPBBF1(6).SetFocus
      '
    Case 34
      'FINE LISTA
      txtPBBF1(0).SetFocus
      '
    Case 38
      'AVANTI
      If (Index < 6) Then
        txtPBBF1(Index + 1).SetFocus
      Else
        txtPBBF1(0).SetFocus
      End If
      '
    Case 40
      'INDIETRO
      If (Index > 0) Then
        txtPBBF1(Index - 1).SetFocus
      Else
        txtPBBF1(6).SetFocus
      End If
      '
    Case 13
      If (Shift = 4) Then
        rsp = MsgBox(fraFIANCO1.Caption & " --> " & fraFIANCO2.Caption, vbQuestion + vbDefaultButton2 + vbYesNo, "WARNING: " & fraTOLLERANZA.Caption)
        If (rsp = 6) Then
          For i = 0 To 6
            txtPBBF2(i).Text = frmt(-val(txtPBBF1(i).Text), 4)
            txtPYYF2(i).Text = txtPYYF1(i).Text
            txtPXXF2(i).Text = frmt(-val(txtPXXF1(i).Text), 4)
          Next i
        Else
          WRITE_DIALOG "Operation aborted by user!"
        End If
      End If
      '
  End Select
  '
End Sub

Private Sub txtPBBF1_LostFocus(Index As Integer)

On Error Resume Next
  
  IF1(Index).BackColor = &HEBE1D7
  IF1(Index).ForeColor = &H80000012
  lblBB(0).BackColor = &HEBE1D7
  lblBB(0).ForeColor = &H80000012

End Sub

Private Sub txtPBBF2_GotFocus(Index As Integer)

On Error Resume Next
  
  IF2(Index).BackColor = QBColor(12)
  IF2(Index).ForeColor = QBColor(15)
  lblBB(1).BackColor = QBColor(12)
  lblBB(1).ForeColor = QBColor(15)
  Call SELEZIONA_TESTO

End Sub

Private Sub txtPBBF2_KeyUp(Index As Integer, KeyCode As Integer, Shift As Integer)

Dim rsp As Integer

On Error Resume Next
  '
  WRITE_DIALOG ""
  Select Case KeyCode
    '
    Case 33
      'INIZIO LISTA
      txtPBBF2(6).SetFocus
      '
    Case 34
      'FINE LISTA
      txtPBBF2(0).SetFocus
      '
    Case 38
      'AVANTI
      If (Index < 6) Then
        txtPBBF2(Index + 1).SetFocus
      Else
        txtPBBF2(0).SetFocus
      End If
      '
    Case 40
      'INDIETRO
      If (Index > 0) Then
        txtPBBF2(Index - 1).SetFocus
      Else
        txtPBBF2(6).SetFocus
      End If
      '
    Case 13
      If (Shift = 4) Then
        rsp = MsgBox(fraFIANCO2.Caption & " --> " & fraFIANCO1.Caption, vbQuestion + vbDefaultButton2 + vbYesNo, "WARNING: " & fraTOLLERANZA.Caption)
        If (rsp = 6) Then
          For i = 0 To 6
            txtPBBF1(i).Text = frmt(-val(txtPBBF2(i).Text), 4)
            txtPYYF1(i).Text = txtPYYF2(i).Text
            txtPXXF1(i).Text = frmt(-val(txtPXXF2(i).Text), 4)
          Next i
        Else
          WRITE_DIALOG "Operation aborted by user!"
        End If
      End If
      '
  End Select
  '
End Sub

Private Sub txtPBBF2_LostFocus(Index As Integer)

On Error Resume Next
  
  IF2(Index).BackColor = &HEBE1D7
  IF2(Index).ForeColor = &H80000012
  lblBB(1).BackColor = &HEBE1D7
  lblBB(1).ForeColor = &H80000012

End Sub

Private Sub txtPXXF1_GotFocus(Index As Integer)

On Error Resume Next
  
  IF1(Index).BackColor = QBColor(12)
  IF1(Index).ForeColor = QBColor(15)
  lblXX(0).BackColor = QBColor(12)
  lblXX(0).ForeColor = QBColor(15)
  Call SELEZIONA_TESTO

End Sub

Private Sub txtPXXF1_KeyUp(Index As Integer, KeyCode As Integer, Shift As Integer)

Dim rsp As Integer

On Error Resume Next
  '
  WRITE_DIALOG ""
  Select Case KeyCode
    '
    Case 33
      'INIZIO LISTA
      txtPXXF1(7).SetFocus
      '
    Case 34
      'FINE LISTA
      txtPXXF1(0).SetFocus
      '
    Case 38
      'AVANTI
      If (Index < 7) Then
        txtPXXF1(Index + 1).SetFocus
      Else
        txtPXXF1(0).SetFocus
      End If
      '
    Case 40
      'INDIETRO
      If (Index > 0) Then
        txtPXXF1(Index - 1).SetFocus
      Else
        txtPXXF1(7).SetFocus
      End If
      '
    Case 13
      If (Shift = 4) Then
        rsp = MsgBox(fraFIANCO1.Caption & " --> " & fraFIANCO2.Caption, vbQuestion + vbDefaultButton2 + vbYesNo, "WARNING: " & fraTOLLERANZA.Caption)
        If (rsp = 6) Then
          For i = 0 To 6
            txtPBBF2(i).Text = frmt(-val(txtPBBF1(i).Text), 4)
            txtPYYF2(i).Text = txtPYYF1(i).Text
            txtPXXF2(i).Text = frmt(-val(txtPXXF1(i).Text), 4)
          Next i
        Else
          WRITE_DIALOG "Operation aborted by user!"
        End If
      End If
      '
  End Select
  '
End Sub

Private Sub txtPXXF1_LostFocus(Index As Integer)

On Error Resume Next
  
  IF1(Index).BackColor = &HEBE1D7
  IF1(Index).ForeColor = &H80000012
  lblXX(0).BackColor = &HEBE1D7
  lblXX(0).ForeColor = &H80000012
  
End Sub

Private Sub txtPXXF2_GotFocus(Index As Integer)

On Error Resume Next
  
  IF2(Index).BackColor = QBColor(12)
  IF2(Index).ForeColor = QBColor(15)
  lblXX(1).BackColor = QBColor(12)
  lblXX(1).ForeColor = QBColor(15)
  Call SELEZIONA_TESTO

End Sub

Private Sub txtPXXF2_KeyUp(Index As Integer, KeyCode As Integer, Shift As Integer)

Dim rsp As Integer

On Error Resume Next
  '
  WRITE_DIALOG ""
  Select Case KeyCode
    '
    Case 33
      'INIZIO LISTA
      txtPXXF2(7).SetFocus
      '
    Case 34
      'FINE LISTA
      txtPXXF2(0).SetFocus
      '
    Case 38
      'AVANTI
      If (Index < 7) Then
        txtPXXF2(Index + 1).SetFocus
      Else
        txtPXXF2(0).SetFocus
      End If
      '
    Case 40
      'INDIETRO
      If (Index > 0) Then
        txtPXXF2(Index - 1).SetFocus
      Else
        txtPXXF2(7).SetFocus
      End If
      '
    Case 13
      If (Shift = 4) Then
        rsp = MsgBox(fraFIANCO2.Caption & " --> " & fraFIANCO1.Caption, vbQuestion + vbDefaultButton2 + vbYesNo, "WARNING: " & fraTOLLERANZA.Caption)
        If (rsp = 6) Then
          For i = 0 To 6
            txtPBBF1(i).Text = frmt(-val(txtPBBF2(i).Text), 4)
            txtPYYF1(i).Text = txtPYYF2(i).Text
            txtPXXF1(i).Text = frmt(-val(txtPXXF2(i).Text), 4)
          Next i
        Else
          WRITE_DIALOG "Operation aborted by user!"
        End If
      End If
      '
  End Select
  '
End Sub

Private Sub txtPXXF2_LostFocus(Index As Integer)

On Error Resume Next
  
  IF2(Index).BackColor = &HEBE1D7
  IF2(Index).ForeColor = &H80000012
  lblXX(1).BackColor = &HEBE1D7
  lblXX(1).ForeColor = &H80000012

End Sub

Private Sub txtPYYF1_GotFocus(Index As Integer)

On Error Resume Next
  
  IF1(Index).BackColor = QBColor(12)
  IF1(Index).ForeColor = QBColor(15)
  lblYY(0).BackColor = QBColor(12)
  lblYY(0).ForeColor = QBColor(15)
  Call SELEZIONA_TESTO

End Sub

Private Sub txtPYYF1_KeyUp(Index As Integer, KeyCode As Integer, Shift As Integer)

Dim rsp As Integer

On Error Resume Next
  '
  WRITE_DIALOG ""
  Select Case KeyCode
    '
    Case 33
      'INIZIO LISTA
      txtPYYF1(7).SetFocus
      '
    Case 34
      'FINE LISTA
      txtPYYF1(0).SetFocus
      '
    Case 38
      'AVANTI
      If (Index < 7) Then
        txtPYYF1(Index + 1).SetFocus
      Else
        txtPYYF1(0).SetFocus
      End If
      '
    Case 40
      'INDIETRO
      If (Index > 0) Then
        txtPYYF1(Index - 1).SetFocus
      Else
        txtPYYF1(7).SetFocus
      End If
      '
    Case 13
      If (Shift = 4) Then
        rsp = MsgBox(fraFIANCO1.Caption & " --> " & fraFIANCO2.Caption, vbQuestion + vbDefaultButton2 + vbYesNo, "WARNING: " & fraTOLLERANZA.Caption)
        If (rsp = 6) Then
          For i = 0 To 6
            txtPBBF2(i).Text = frmt(-val(txtPBBF1(i).Text), 4)
            txtPYYF2(i).Text = txtPYYF1(i).Text
            txtPXXF2(i).Text = frmt(-val(txtPXXF1(i).Text), 4)
          Next i
        Else
          WRITE_DIALOG "Operation aborted by user!"
        End If
      End If
      '
  End Select
  '
End Sub

Private Sub txtPYYF1_LostFocus(Index As Integer)

On Error Resume Next
  
  IF1(Index).BackColor = &HEBE1D7
  IF1(Index).ForeColor = &H80000012
  lblYY(0).BackColor = &HEBE1D7
  lblYY(0).ForeColor = &H80000012

End Sub

Private Sub txtPYYF2_GotFocus(Index As Integer)

On Error Resume Next
  
  IF2(Index).BackColor = QBColor(12)
  IF2(Index).ForeColor = QBColor(15)
  lblYY(1).BackColor = QBColor(12)
  lblYY(1).ForeColor = QBColor(15)
  Call SELEZIONA_TESTO

End Sub

Private Sub txtPYYF2_KeyUp(Index As Integer, KeyCode As Integer, Shift As Integer)
'
Dim rsp As Integer
'
On Error Resume Next
  '
  WRITE_DIALOG ""
  Select Case KeyCode
    '
    Case 33
      'INIZIO LISTA
      txtPYYF2(7).SetFocus
      '
    Case 34
      'FINE LISTA
      txtPYYF2(0).SetFocus
      '
    Case 38
      'AVANTI
      If (Index < 7) Then
        txtPYYF2(Index + 1).SetFocus
      Else
        txtPYYF2(0).SetFocus
      End If
      '
    Case 40
      'INDIETRO
      If (Index > 0) Then
        txtPYYF2(Index - 1).SetFocus
      Else
        txtPYYF2(7).SetFocus
      End If
      '
    Case 13
      If (Shift = 4) Then
        rsp = MsgBox(fraFIANCO2.Caption & " --> " & fraFIANCO1.Caption, vbQuestion + vbDefaultButton2 + vbYesNo, "WARNING: " & fraTOLLERANZA.Caption)
        If (rsp = 6) Then
          For i = 0 To 6
            txtPBBF1(i).Text = frmt(-val(txtPBBF2(i).Text), 4)
            txtPYYF1(i).Text = txtPYYF2(i).Text
            txtPXXF1(i).Text = frmt(-val(txtPXXF2(i).Text), 4)
          Next i
        Else
          WRITE_DIALOG "Operation aborted by user!"
        End If
      End If
      '
  End Select
  '
End Sub

Private Sub txtPYYF2_LostFocus(Index As Integer)

On Error Resume Next
  
  IF2(Index).BackColor = &HEBE1D7
  IF2(Index).ForeColor = &H80000012
  lblYY(1).BackColor = &HEBE1D7
  lblYY(1).ForeColor = &H80000012

End Sub

Private Sub txtSCALA_KeyUp(KeyCode As Integer, Shift As Integer)

  If KeyCode = 13 Then
  
    Call CHK0.grfCORRETTORIONOFF.Pulisci
    Call CONTROLLO_ROTORI_VISUALIZZAZIONE
    
  End If
  
End Sub

Private Sub txtScX_GotFocus()
  
On Error Resume Next
  
  CHK0.lblScX.BackColor = QBColor(12)
  CHK0.lblScX.ForeColor = QBColor(15)
  Call SELEZIONA_TESTO

End Sub

Private Sub txtScX_KeyUp(KeyCode As Integer, Shift As Integer)
'
Dim Campo             As String
Dim stmp              As String
Dim ret               As Integer
Dim TIPO_LAVORAZIONE  As String
'
On Error GoTo errtxtScX_KeyUp
  '
  Select Case KeyCode
    Case 33: SendKeys "+{TAB}" 'INIZIO LISTA
    Case 34: SendKeys "{TAB}"  'FINE LISTA
    Case 40: SendKeys "{TAB}"  'AVANTI
    Case 38: SendKeys "+{TAB}" 'INDIETRO
    Case 13: 'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
      TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
      TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
      Select Case TIPO_LAVORAZIONE
        Case "CREATORI_STANDARD"
          If CHK0.Combo1.ListIndex = 0 Then Campo = "ScalaProfiloCreatori"
          'If CHK0.Combo1.ListIndex = 1 Then Campo = "ScalaELICAx"
          'If CHK0.Combo1.ListIndex = 2 Then Campo = "ScalaDIVISIONEx"
          'If CHK0.Combo1.ListIndex = 3 Then Campo = "ScalaDIVISIONEx"
          'If CHK0.Combo1.ListIndex = 4 Then Campo = "ScalaDIVISIONEx"
        Case "CREATORI_FORMA"  '--- FlagFORMA 011
          If CHK0.Combo1.ListIndex = 0 Then Campo = "ScalaProfiloCreatori"
          'If CHK0.Combo1.ListIndex = 1 Then Campo = "ScalaELICAx"
          'If CHK0.Combo1.ListIndex = 2 Then Campo = "ScalaDIVISIONEx"
          'If CHK0.Combo1.ListIndex = 3 Then Campo = "ScalaDIVISIONEx"
          'If CHK0.Combo1.ListIndex = 4 Then Campo = "ScalaDIVISIONEx"
        Case "ROTORI_ESTERNI"
          ret = CHK0.Combo1.ListIndex
          stmp = GetInfo("CONTROLLO", "itemCOD(" & Format$(ret, "#0") & ")", Path_LAVORAZIONE_INI)
          stmp = UCase$(Trim$(stmp))
          Select Case stmp
            Case "PROFILE": Campo = "ScalaProfiloRotori"
            Case "CORROT"
            Case "CORRPROP"
            Case "PROPOSED"
            Case "PRFTOL"
            Case "CHKRAB"
            Case "LEAD": Campo = "ScalaELICARotoriX"
          End Select
        Case Else
          If CHK0.Combo1.ListIndex = 0 Then Campo = "ScalaPROFILOx"
          If CHK0.Combo1.ListIndex = 1 Then Campo = "ScalaELICAx"
          If CHK0.Combo1.ListIndex = 2 Then Campo = "ScalaDIVISIONEx"
          If CHK0.Combo1.ListIndex = 3 Then Campo = "ScalaDIVISIONEx"
          If CHK0.Combo1.ListIndex = 4 Then Campo = "ScalaDIVISIONEx"
      End Select
      'LETTURA SCALA
      stmp = GetInfo("CHK0", Campo, Path_LAVORAZIONE_INI)
      If (stmp <> CHK0.txtScX.Text) Then
        If val(CHK0.txtScX.Text) > 0 And val(CHK0.txtScX.Text) <= 2000 Then
          CHK0.txtScX.Text = frmt(val(CHK0.txtScX.Text), 4)
          ret = WritePrivateProfileString("CHK0", Campo, CHK0.txtScX.Text, Path_LAVORAZIONE_INI)
          Call VISUALIZZA_CONTROLLO
        Else
          CHK0.txtScX.Text = stmp
        End If
      End If
      'EVIDENZIO LA CASELLA
      Call SELEZIONA_TESTO
      '
  End Select

Exit Sub

errtxtScX_KeyUp:
  WRITE_DIALOG "SUB txtScX_KeyUp ERROR: " & str(Err)
  Exit Sub

End Sub

Private Sub txtScX_LostFocus()

On Error Resume Next
  
  CHK0.lblScX.BackColor = &HFFFFFF
  CHK0.lblScX.ForeColor = QBColor(0)

End Sub

Private Sub txtScY_GotFocus()

On Error Resume Next
  
  CHK0.lblScY.BackColor = QBColor(12)
  CHK0.lblScY.ForeColor = QBColor(15)
  Call SELEZIONA_TESTO

End Sub

Private Sub txtScY_KeyUp(KeyCode As Integer, Shift As Integer)

Dim Campo             As String
Dim stmp              As String
Dim ret               As Integer
Dim TIPO_LAVORAZIONE  As String
  
On Error GoTo errtxtScY_KeyUp

  Select Case KeyCode
    Case 33: SendKeys "+{TAB}" 'INIZIO LISTA
    Case 34: SendKeys "{TAB}"  'FINE LISTA
    Case 40: SendKeys "{TAB}"  'AVANTI
    Case 38: SendKeys "+{TAB}" 'INDIETRO
    Case 13
      'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
      TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
      TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
      Select Case TIPO_LAVORAZIONE
        Case "CREATORI_STANDARD"
          If CHK0.Combo1.ListIndex = 0 Then Campo = "ScalaErroreCreatori"
          'If CHK0.Combo1.ListIndex = 1 Then Campo = "ScalaELICAy"
          'If CHK0.Combo1.ListIndex = 2 Then Campo = "ScalaDIVISIONEy"
          'If CHK0.Combo1.ListIndex = 3 Then Campo = "ScalaDIVISIONEy"
          'If CHK0.Combo1.ListIndex = 4 Then Campo = "ScalaDIVISIONEy"
        Case "CREATORI_FORMA"  '--- FlagFORMA 012
          If CHK0.Combo1.ListIndex = 0 Then Campo = "ScalaErroreCreatori"
          'If CHK0.Combo1.ListIndex = 1 Then Campo = "ScalaELICAy"
          'If CHK0.Combo1.ListIndex = 2 Then Campo = "ScalaDIVISIONEy"
          'If CHK0.Combo1.ListIndex = 3 Then Campo = "ScalaDIVISIONEy"
          'If CHK0.Combo1.ListIndex = 4 Then Campo = "ScalaDIVISIONEy"
        Case "ROTORI_ESTERNI", "PROFILO_RAB_ESTERNI"  '--- FlagROT 014
          ret = CHK0.Combo1.ListIndex
          stmp = GetInfo("CONTROLLO", "itemCOD(" & Format$(ret, "#0") & ")", Path_LAVORAZIONE_INI)
          stmp = UCase$(Trim$(stmp))
          Select Case stmp
            Case "PROFILE": Campo = "ScalaErroreROTORI"
            Case "CORROT"
            Case "CORRPROP"
            Case "PROPOSED"
            Case "PRFTOL"
            Case "CHKRAB"
            Case "LEAD": Campo = "ScalaELICARotoriY"
            Case Else: Campo = "ScalaErroreROTORI"
          End Select
        Case Else
          If CHK0.Combo1.ListIndex = 0 Then Campo = "ScalaPROFILOy"
          If CHK0.Combo1.ListIndex = 1 Then Campo = "ScalaELICAy"
          If CHK0.Combo1.ListIndex = 2 Then Campo = "ScalaDIVISIONEy"
          If CHK0.Combo1.ListIndex = 3 Then Campo = "ScalaDIVISIONEy"
          If CHK0.Combo1.ListIndex = 4 Then Campo = "ScalaDIVISIONEy"
      End Select
      stmp = GetInfo("CHK0", Campo, Path_LAVORAZIONE_INI)
      If (stmp <> CHK0.txtScY.Text) Then
        If (val(CHK0.txtScY.Text) > 0) And (val(CHK0.txtScY.Text) <= 2000) Then
          CHK0.txtScY.Text = frmt(val(CHK0.txtScY.Text), 4)
          ret = WritePrivateProfileString("CHK0", Campo, CHK0.txtScY.Text, Path_LAVORAZIONE_INI)
          Call VISUALIZZA_CONTROLLO
        Else
          CHK0.txtScY.Text = stmp
        End If
      End If
      Call SELEZIONA_TESTO
      '
  End Select
  
Exit Sub

errtxtScY_KeyUp:
  WRITE_DIALOG "SUB txtScY_KeyUp ERROR: " & str(Err)
  Exit Sub

End Sub

Private Sub txtScY_LostFocus()
  
On Error Resume Next
  
  CHK0.lblScY.BackColor = &HFFFFFF
  CHK0.lblScY.ForeColor = QBColor(0)

End Sub

Private Sub Form_Deactivate()
  '
  Call ChildDeActivate(Me)
  '
End Sub

Private Sub Form_Load()
'
Dim i    As Integer
'
On Error GoTo errForm_Load
  '
  'NASCONDO I TASTI DELLA SECONDA RIGA
  MDIForm1.SSPanel1.Height = MDIForm1.Picture1(0).Height
  For i = 0 To 7
    MDIForm1.Picture3(i).Visible = False
  Next i
  Call AlMakeBorder(hwnd)
  Call Move(g_nBeArtLeft, g_nBeArtTop, g_nBeArtWidth, g_nBeArtHeight)
  Call InitEnvironment(Me)
  CHK0.Image1.Picture = LoadPicture(g_chOemPATH & "\Gapp4\images\Probe.bmp")
  CHK0.Image2.Picture = LoadPicture(g_chOemPATH & "\Gapp4\images\Archive1.bmp")
  CHK0.Image3.Picture = LoadPicture(g_chOemPATH & "\Gapp4\images\Archive1.bmp")
  Call IMPOSTA_FONT(Me)
  CHK0.txtHELP.Font.Name = "Courier New"
  Call INIZIALIZZA_CHK0_PARTE1
  '
Exit Sub

errForm_Load:
  WRITE_DIALOG Error(Err)
  Exit Sub
      
End Sub
