Attribute VB_Name = "INTERNI"
Option Explicit

'DefDbl A-Z

'dimensione vettori
Const dv = 200
Dim MMEAP(2) As Double

Sub CREAZIONE_DATI_PROFILO_RAB_ESTERNI()
'
'CALCOLO PRFROT COME INGRANAGGIO
Dim jj            As Integer
Dim stmp          As String
Dim stmp1         As String
Dim stmp2         As String
Dim sfmt0         As String
Dim i             As Integer
Dim RA()          As Double
Dim al()          As Double
Dim BE()          As Double
Dim ret           As Long
Dim RAmin         As Double
Dim RAmax         As Double
Dim DeltaAlfa     As Double
Dim NPntF1        As Integer
Dim DIAMETRO_MOLA As Double
Dim SpessoreNormale As Double
Dim QuotaWILDABER As Double
Dim DentiWILDABER As Integer
Dim QuotaRulli    As Double
Dim DiametroRulli As Double
Dim RaggioTesta   As Double
Dim RaggioFondo   As Double
Dim DistanzaPunti As Double
Dim Arco          As Double
'
Dim Alfa1 As Double
Dim Alfa2 As Double
Dim beta1 As Double
Dim Beta2 As Double
Dim Mn    As Double
Dim Z     As Integer
Dim DEm   As Double
Dim DSAP1 As Double
Dim DSAP2 As Double
Dim DEAP  As Double
Dim DInt  As Double
Dim PASSO(2) As Double
Dim DIAMETRO_PRIMITIVO As Double
Dim ANGOLO_ELICA       As Double
Dim PathProfilo        As String
Dim NumPuntiPrf        As Integer
'
Const NumeroPuntiProfilo = 200
'
On Error Resume Next
  '
  WRITE_DIALOG "RAB EVALUATION"
  StopRegieEvents
  ret = MsgBox("NEW RAB EVALUATION???", vbQuestion + vbYesNo + vbDefaultButton2, "WARNING")
  ResumeRegieEvents
If (ret = vbYes) Then
    
  DIAMETRO_MOLA = val(LEP_STR("DM_ROT"))
  
  
  i = WritePrivateProfileString("DIS0", "RaggioMola", DIAMETRO_MOLA / 2, Path_LAVORAZIONE_INI)
  
  DIAMETRO_MOLA = 2 * val(GetInfo("DIS0", "RaggioMola", Path_LAVORAZIONE_INI))
  If (DIAMETRO_MOLA <= 0) Then DIAMETRO_MOLA = 250
  '
  SpessoreNormale = val(LEP("S0N"))
  QuotaWILDABER = val(LEP("QW"))
  DentiWILDABER = val(LEP("ZW"))
  QuotaRulli = val(LEP("QR"))
  DiametroRulli = val(LEP("DR"))
  RaggioTesta = val(LEP("RT"))
  RaggioFondo = val(LEP("RF"))
  DistanzaPunti = val(LEP("DEVO"))
  Arco = val(LEP("ARCO"))
  '
  Mn = val(LEP("MN"))                'Modulo Normale
  Z = val(LEP("Z"))                  'Numero Denti
  DEm = DIAMETRO_MOLA                'Diametro Esterno Mola
  DEAP = val(LEP("DEAP"))            'Diametro EAP
  DInt = val(LEP("DINT"))            'Diametro Interno
  'FIANCO 1
  Alfa1 = val(LEP("A1")) * PG / 180  'Angolo di pressione F1
  DSAP1 = val(LEP("DSAPF1"))         'Diametro SAP F1
  beta1 = val(LEP("B1")) * PG / 180  'Angolo Elica (rad)
  'FIANCO 2
  Alfa2 = val(LEP("A2")) * PG / 180  'Angolo di pressione F2
  DSAP2 = val(LEP("DSAPF2"))         'Diametro SAP F2
  Beta2 = val(LEP("B2")) * PG / 180  'Angolo Elica (rad)
  '
  'FIANCO 1
  INP.ALFA = Alfa1                      'Angolo di pressione F1
  INP.Beta = beta1                      'Angolo Elica (rad)
  INP.Mn = Mn                           'Modulo Normale
  INP.Z = Z                             'Numero Denti
  INP.DEm = DIAMETRO_MOLA               'Diametro Esterno Mola
  INP.DSAP = DSAP1                      'Diametro SAP F1
  INP.DEAP = DEAP                       'Diametro EAP
  INP.S0Nn = SpessoreNormale            'SON Normale
  INP.Wk = QuotaWILDABER                'Quota WILDABER
  INP.k = DentiWILDABER                 'Denti WILDABER
  INP.QR = QuotaRulli                   'Quota Rulli
  INP.DR = DiametroRulli                'Diametro Rulli
  INP.X = 0                             'coefficiente di spostamento
  INP.RT = RaggioTesta                  'Raggio di Testa
  INP.RF = RaggioFondo                  'Raggio di Fondo
  INP.ART = 0                           'Angolo Raccordo Testa (rad)
  INP.ARF = 0                           'angolo raccordo di fondo (rad)
  INP.DInt = DInt                       'Diametro Interno
  INP.BetaMola = INP.Beta               'Inclinazione Asse Mola (rad)
  INP.ValutazioneDIA = True             'Valutazione (true=DIA; false=TIF)
  INP.NPR1 = 5                          'N� Punti per Raggio
  INP.DIPU = DistanzaPunti              'Distanza Punti Evolvente
  INP.TifDia(1, 1) = 0                  'TIF/DIA Fine Zona
  INP.Rastr(1, 1) = 0                   'Rastremazione
  INP.BOMB(1, 1) = 0                    'Bombatura
  INP.TifDia(1, 2) = 0                  'TIF/DIA Fine Zona
  INP.Rastr(1, 2) = 0                   'Rastremazione
  INP.BOMB(1, 2) = 0                    'Bombatura
  INP.TifDia(1, 3) = 0                  'TIF/DIA Fine Zona
  INP.Rastr(1, 3) = 0                   'Rastremazione
  INP.BOMB(1, 3) = 0                    'Bombatura
  INP.TifDia(2, 1) = 0                  'TIF/DIA Fine Zona
  INP.Rastr(2, 1) = 0                   'Rastremazione
  INP.BOMB(2, 1) = 0                    'Bombatura
  INP.TifDia(2, 2) = 0                  'TIF/DIA Fine Zona
  INP.Rastr(2, 2) = 0                   'Rastremazione
  INP.BOMB(2, 2) = 0                    'Bombatura
  INP.TifDia(2, 3) = 0                  'TIF/DIA Fine Zona
  INP.Rastr(2, 3) = 0                   'Rastremazione
  INP.BOMB(2, 3) = 0                    'Bombatura
  INP.TipoCORR = "KSTD"                 'tipo correzione profilo (KSTD=correzioneK, KPARAB=correzioneK tipo parab.)
  INP.Trocoid = False                   'true=trocoide sul fondo
  INP.RCT(1, 1) = 0                     'raggio di raccordo tra le zone (RCT = raggio cerchio tangente)
  INP.RCT(1, 2) = 0                     'raggio di raccordo tra le zone (RCT = raggio cerchio tangente)
  INP.RCT(2, 1) = 0                     'raggio di raccordo tra le zone (RCT = raggio cerchio tangente)
  INP.RCT(2, 2) = 0                     'raggio di raccordo tra le zone (RCT = raggio cerchio tangente)
  'INPUT DA FILEINI
  INP.NPEmin = 11
  INP.NPEmax = 89
  Call CalcolaProfilo(INP)
  If (Arco > 0) Then
    NPnt(1) = NPnt(1) + 2
    ReDim Preserve RA(NPnt(1)): ReDim Preserve al(NPnt(1)): ReDim Preserve BE(NPnt(1))
    DeltaAlfa = (Arco / RaggioRAB(1)) * 180 / PG
    DeltaAlfa = Int(DeltaAlfa * 10000) / 10000
    i = 1
      RA(i) = RaggioRAB(1)
      al(i) = AlfaRAB(1) * 180 / PG - DeltaAlfa
      BE(i) = BetaRAB(1)
    i = 2
      RA(i) = RaggioRAB(1)
      al(i) = AlfaRAB(1) * 180 / PG - DeltaAlfa / 2
      BE(i) = BetaRAB(1)
    For i = 1 To NPnt(1) - 2
      RA(i + 2) = RaggioRAB(i)
      al(i + 2) = AlfaRAB(i) * 180 / PG
      BE(i + 2) = BetaRAB(i)
    Next i
  Else
    ReDim Preserve RA(NPnt(1)): ReDim Preserve al(NPnt(1)): ReDim Preserve BE(NPnt(1))
    For i = 1 To NPnt(1)
      RA(i) = RaggioRAB(i)
      al(i) = AlfaRAB(i) * 180 / PG
      BE(i) = BetaRAB(i)
    Next i
  End If
  'SALVO IL VALORE PERCHE' VIENE RICALCOLATO SUL SECONDO FIANCO
  NPntF1 = NPnt(1)
  
  'Diam primitivo F1
  Dim DPF1 As Double
  DPF1 = Z * (Mn / Cos(Abs(beta1)))
  PASSO(1) = PAS
  
  'FIANCO 2
  INP.ALFA = Alfa2                      'Angolo di pressione F2
  INP.Beta = Beta2                      'Angolo Elica (rad)
  INP.Mn = Mn                           'Modulo Normale
  INP.Z = Z                             'Numero Denti
  INP.DEm = DIAMETRO_MOLA               'Diametro Esterno Mola
  INP.DSAP = DSAP2                      'Diametro SAP F2
  INP.DEAP = DEAP                       'Diametro EAP
  INP.S0Nn = SpessoreNormale            'SON Normale
  INP.Wk = QuotaWILDABER                'Quota WILDABER
  INP.k = DentiWILDABER                 'Denti WILDABER
  INP.QR = QuotaRulli                   'Quota Rulli
  INP.DR = DiametroRulli                'Diametro Rulli
  INP.X = 0                             'coefficiente di spostamento
  INP.RT = RaggioTesta                  'Raggio di Testa
  INP.RF = RaggioFondo                  'Raggio di Fondo
  INP.ART = 0                           'Angolo Raccordo Testa (rad)
  INP.ARF = 0                           'angolo raccordo di fondo (rad)
  INP.DInt = DInt                       'Diametro Interno
  INP.BetaMola = INP.Beta               'Inclinazione Asse Mola (rad)
  INP.ValutazioneDIA = True             'Valutazione (true=DIA; false=TIF)
  INP.NPR1 = 5                          'N� Punti per Raggio
  INP.DIPU = DistanzaPunti              'Distanza Punti Evolvente
  INP.TifDia(1, 1) = 0                  'TIF/DIA Fine Zona
  INP.Rastr(1, 1) = 0                   'Rastremazione
  INP.BOMB(1, 1) = 0                    'Bombatura
  INP.TifDia(1, 2) = 0                  'TIF/DIA Fine Zona
  INP.Rastr(1, 2) = 0                   'Rastremazione
  INP.BOMB(1, 2) = 0                    'Bombatura
  INP.TifDia(1, 3) = 0                  'TIF/DIA Fine Zona
  INP.Rastr(1, 3) = 0                   'Rastremazione
  INP.BOMB(1, 3) = 0                    'Bombatura
  INP.TifDia(2, 1) = 0                  'TIF/DIA Fine Zona
  INP.Rastr(2, 1) = 0                   'Rastremazione
  INP.BOMB(2, 1) = 0                    'Bombatura
  INP.TifDia(2, 2) = 0                  'TIF/DIA Fine Zona
  INP.Rastr(2, 2) = 0                   'Rastremazione
  INP.BOMB(2, 2) = 0                    'Bombatura
  INP.TifDia(2, 3) = 0                  'TIF/DIA Fine Zona
  INP.Rastr(2, 3) = 0                   'Rastremazione
  INP.BOMB(2, 3) = 0                    'Bombatura
  INP.TipoCORR = "KSTD"                 'tipo correzione profilo (KSTD=correzioneK, KPARAB=correzioneK tipo parab.)
  INP.Trocoid = False                   'true=trocoide sul fondo
  INP.RCT(1, 1) = 0                     'raggio di raccordo tra le zone (RCT = raggio cerchio tangente)
  INP.RCT(1, 2) = 0                     'raggio di raccordo tra le zone (RCT = raggio cerchio tangente)
  INP.RCT(2, 1) = 0                     'raggio di raccordo tra le zone (RCT = raggio cerchio tangente)
  INP.RCT(2, 2) = 0                     'raggio di raccordo tra le zone (RCT = raggio cerchio tangente)
  'INPUT DA FILEINI
  INP.NPEmin = 11
  INP.NPEmax = 89
  Call CalcolaProfilo(INP)
  If (Arco > 0) Then
    NPnt(2) = NPnt(2) + 2
    ReDim Preserve RA(NPntF1 + NPnt(2)): ReDim Preserve al(NPntF1 + NPnt(2)): ReDim Preserve BE(NPntF1 + NPnt(2))
    DeltaAlfa = (Arco / RaggioRAB(NPnt(1) + NPnt(2) - 2)) * 180 / PG
    DeltaAlfa = Int(DeltaAlfa * 10000) / 10000
    i = NPnt(2)
      RA(NPntF1 + i) = RaggioRAB(NPnt(1) + NPnt(2) - 2)
      al(NPntF1 + i) = AlfaRAB(NPnt(1) + NPnt(2) - 2) * 180 / PG + DeltaAlfa
      BE(NPntF1 + i) = BetaRAB(NPnt(1) + NPnt(2) - 2)
    i = NPnt(2) - 1
      RA(NPntF1 + i) = RaggioRAB(NPnt(1) + NPnt(2) - 2)
      al(NPntF1 + i) = AlfaRAB(NPnt(1) + NPnt(2) - 2) * 180 / PG + DeltaAlfa / 2
      BE(NPntF1 + i) = BetaRAB(NPnt(1) + NPnt(2) - 2)
    For i = 1 To NPnt(2) - 2
      RA(NPntF1 + i) = RaggioRAB(NPnt(1) + i)
      al(NPntF1 + i) = AlfaRAB(NPnt(1) + i) * 180 / PG
      BE(NPntF1 + i) = BetaRAB(NPnt(1) + i)
    Next i
  Else
    ReDim Preserve RA(NPntF1 + NPnt(2)): ReDim Preserve al(NPntF1 + NPnt(2)): ReDim Preserve BE(NPntF1 + NPnt(2))
    For i = 1 To NPnt(2)
      RA(NPntF1 + i) = RaggioRAB(NPnt(1) + i)
      al(NPntF1 + i) = AlfaRAB(NPnt(1) + i) * 180 / PG
      BE(NPntF1 + i) = BetaRAB(NPnt(1) + i)
    Next i
  End If
  
  'Diam primitivo F2
  Dim DPF2 As Double
  DPF2 = Z * (Mn / Cos(Abs(Beta2)))
   PASSO(2) = PAS
  '
  'NUMERO DENTI
  Call SP("R[5]", frmt(Z, 4), False)
  'DIAMETRO PRIMITIVO
  DIAMETRO_PRIMITIVO = (Mn * Z / 2) * (1 / Cos(Abs(beta1)) + 1 / Cos(Abs(Beta2)))
  Call SP("R[103]", frmt(DIAMETRO_PRIMITIVO, 4), False)
  'ANGOLO DI PRESSIONE MEDIO
  Call SP("R[40]", frmt((Alfa1 + Alfa2) / 2 * 180 / PG, 8), False)
  'NUMERO PUNTI TOTALE
  Call SP("R[147]", Format(NPntF1 + NPnt(2), "####0"), False)
  'NUMERO PUNTI DEL FIANCO 1
  If (Int(NPntF1 / 2) = NPntF1 / 2) Then
    stmp1 = Format(NPntF1 - 1, "####0")
  Else
    stmp1 = Format(NPntF1, "####0")
  End If
  Call SP("R[43]", stmp1, False)
  
  Dim SensoElica As Integer
  Dim BetaMedio  As Double
  BetaMedio = (beta1 + Beta2) / 2
  
  If BetaMedio >= 0 Then
     SensoElica = -1
  Else
     SensoElica = 1
  End If
  'Call SP("iSensoElica", SensoElica, False)
  Call SP("SensoElica", SensoElica, False)
  
  'ANGOLO ELICA
  ANGOLO_ELICA = Acos(Mn * Z / DIAMETRO_PRIMITIVO) '(beta1 + Beta2) / 2
  Call SP("R[41]", frmt(ANGOLO_ELICA * 180 / PG, 4), False)
  'PASSO ELICA
  PASSO(0) = PG * DIAMETRO_PRIMITIVO / Tan(ANGOLO_ELICA)
  
  Call SP("R[390]", frmt(PASSO(0), 4), False)
  Call SP("PASSOF1", frmt(Abs(PASSO(1)), 4), False)
  Call SP("PASSOF2", frmt(Abs(PASSO(2)), 4), False)
  
  'SAP
  Call SP("R[269]", frmt(DSAP1, 4), False)
  Call SP("R[284]", frmt(DSAP2, 4), False)
  'EAP
  Call SP("R[270]", frmt(DEAP, 4), False)
  Call SP("R[285]", frmt(DEAP, 4), False)
  '
  'Leggere Nome Pezzo da file INI
  'stmp = GetInfo("PROFILO_RAB_ESTERNI", "NomePezzoInserimento", Path_LAVORAZIONE_INI)
  stmp = LEP_STR("PERCORSO")
  PathProfilo = g_chOemPATH & "\RAB_ESTERNI\" & stmp
  
  'Verificare se esiste un direttorio "RAB_ESTERNI\NomePezzo", se non esiste crearlo
  If (Dir(PathProfilo, vbDirectory) = "") Then
    Call MkDir(PathProfilo)
  End If

    
  'CREAZIONE DEL PROFILO RAB CON LA ROTAZIONE
  If (Dir$(g_chOemPATH & "\PRFROT") <> "") Then Call Kill(g_chOemPATH & "\PRFROT")
  RAmin = RaggioRAB(1): RAmax = RaggioRAB(1)
  Open g_chOemPATH & "\PRFROT" For Output As #1
  
  For i = 1 To NPntF1 + NPnt(2)
    If (RA(i) <= RAmin) Then RAmin = RA(i)
    If (RA(i) >= RAmax) Then RAmax = RA(i)
    stmp = ""
    If (RA(i) >= 0) Then
      stmp = stmp & "+" & Left$(frmt0(Abs(RA(i)), 4) & String(15, " "), 15) & " "
      Call SP("RA[0," & Format(i, "###0") & "]", "+" & frmt(Abs(RA(i)), 4), False)
    Else
      stmp = stmp & "-" & Left$(frmt0(Abs(RA(i)), 4) & String(15, " "), 15) & " "
      Call SP("RA[0," & Format(i, "###0") & "]", "-" & frmt(Abs(RA(i)), 4), False)
    End If
    If (al(i) >= 0) Then
      stmp = stmp & "+" & Left$(frmt0(Abs(al(i)), 4) & String(15, " "), 15) & " "
      Call SP("AL[0," & Format(i, "###0") & "]", "+" & frmt(Abs(al(i)), 4), False)
    Else
      stmp = stmp & "-" & Left$(frmt0(Abs(al(i)), 4) & String(15, " "), 15) & " "
      Call SP("AL[0," & Format(i, "###0") & "]", "-" & frmt(Abs(al(i)), 4), False)
    End If
    If (BE(i) >= 0) Then
      stmp = stmp & "+" & Left$(frmt0(Abs(BE(i)), 4) & String(15, " "), 15) & " "
      Call SP("BE[0," & Format(i, "###0") & "]", "+" & frmt(Abs(BE(i)), 4), False)
    Else
      stmp = stmp & "-" & Left$(frmt0(Abs(BE(i)), 4) & String(15, " "), 15) & " "
      Call SP("BE[0," & Format(i, "###0") & "]", "-" & frmt(Abs(BE(i)), 4), False)
    End If
    Print #1, stmp
  Next i
  If (NPntF1 + NPnt(2) < NumeroPuntiProfilo) Then
    For i = NPntF1 + NPnt(2) + 1 To NumeroPuntiProfilo
      Call SP("RA[0," & Format(i, "###0") & "]", "+0.000", False)
      Call SP("AL[0," & Format(i, "###0") & "]", "+0.000", False)
      Call SP("BE[0," & Format(i, "###0") & "]", "+0.000", False)
    Next i
  End If
  Close #1
  
  'SALVATAGGIO NEL DIRETTORIO DEL PEZZO
  If (Dir$(PathProfilo & "\PRFROT") <> "") Then Call Kill(PathProfilo & "\PRFROT")
  Call FileCopy(g_chOemPATH & "\PRFROT", PathProfilo & "\PRFROT")
  
  'AGGIORNAMENTO DEL DATABASE
  Call AggiornaDB
  WRITE_DIALOG ""
     
  Dim NumPntF1 As Integer
  Dim NumPntF2 As Integer
  
  'Numero punti profilo RAB
  NumPuntiPrf = NPntF1 + NPnt(2)
  
  'NUMERO PUNTI DEL FIANCO 1
  If (Int(NPntF1 / 2) = NPntF1 / 2) Then
    NumPntF1 = NPntF1 - 1
  Else
    NumPntF1 = NPntF1
  End If
  
  'NUMERO PUNTI DEL FIANCO 2
  NumPntF2 = NumPuntiPrf - NumPntF1
  
  'Creazione INFO.INI
  i = WritePrivateProfileString("CREAZIONE", "RADICE", PathProfilo, PathProfilo & "\INFO.INI")
  i = WritePrivateProfileString("CREAZIONE", "PRINCIPI", Format$(Z, "#####0"), PathProfilo & "\INFO.INI")
  i = WritePrivateProfileString("CREAZIONE", "PASSO", frmt(PASSO(0), 4), PathProfilo & "\INFO.INI")
  i = WritePrivateProfileString("CREAZIONE", "ELICA", frmt(Abs(ANGOLO_ELICA) * 180 / PG, 4), PathProfilo & "\INFO.INI")

  i = WritePrivateProfileString("CREAZIONE", "NpM_F1", Format$(NumPntF1 + 1, "#####0"), PathProfilo & "\INFO.INI")
  i = WritePrivateProfileString("CREAZIONE", "NpM_F2", Format$(NumPntF2 + 1, "#####0"), PathProfilo & "\INFO.INI")
  i = WritePrivateProfileString("CREAZIONE", "NpC_F1", Format$(NumPntF1, "#####0"), PathProfilo & "\INFO.INI")
  i = WritePrivateProfileString("CREAZIONE", "NpC_F2", Format$(NumPntF2, "#####0"), PathProfilo & "\INFO.INI")
  i = WritePrivateProfileString("CREAZIONE", "Npt", Format$(NumPuntiPrf, "#####0"), PathProfilo & "\INFO.INI")
  i = WritePrivateProfileString("CREAZIONE", "DiametroMola", Format$(DIAMETRO_MOLA, "#####0"), PathProfilo & "\INFO.INI")
  
'  Dim RADICE   As String
'  Dim NP_Tot   As Integer
'  Dim Npt_F1   As Integer
'  Dim TCHK     As String
'  Dim PasElica As Double
'  Dim MCHK     As String
'  Dim DiametroMed As String
'
'  Call LEGGI_INFO_RAB(RADICE, NP_Tot, Npt_F1, TCHK, PasElica, MCHK, DiametroMed)
  
  'Creazione files per gestione RAB Esterni
  Call CREAZIONE_FILES_RAB_ESTERNI(PathProfilo, NumPuntiPrf, NumPntF1, NumPntF2)

  'CREAZIONE CHKRAB.ROT e Selezione pinti centratura
  Call CREAZIONE_DATI_CONTROLLO_RAB(PathProfilo)

Else
  WRITE_DIALOG "Operation Aborted by user!!!"
End If
  '
End Sub
Public Sub CREAZIONE_FILES_RAB_ESTERNI(RADICE As String, npt As Integer, NPF1 As Integer, NpF2 As Integer)

Dim DiametroMola  As Integer
Dim X             As Double
Dim Y             As Double
Dim T             As Double
Dim XH            As Double
Dim YH            As Double
Dim TH            As Double
Dim riga          As String
Dim i             As Integer
Dim Rmin          As Double
Dim Rmax          As Double
Dim RminIDX       As Integer
Dim R             As Double
Dim A             As Double
Dim B             As Double
Dim Rag()         As Double
Dim ALFA()        As Double
Dim Beta()        As Double
Dim stmp          As String

'
On Error GoTo errCREAZIONE_FILES_RAB_ESTERNI

ReDim Rag(npt + 1)
ReDim ALFA(npt + 1)
ReDim Beta(npt + 1)

DiametroMola = 250

'Calcolo profilo mola e crea file XYT.250
Call ROTORI1.CALCOLO_MOLA_PROFILO_RAB_ESTERNI(DiametroMola, 0)
    
'Creazione di TGMROT.ALL
Open RADICE & "\XYT." & Format$(DiametroMola, "##0") For Input As #1
  Open RADICE & "\TGMROT.ALL" For Output As #3
      Input #1, X, Y, T
      Print #3, frmt(Abs(T), 4)
      Print #3, frmt(Abs(T), 4)
    While Not EOF(1)
      Input #1, X, Y, T
      Print #3, frmt(Abs(T), 4)
    Wend
      Print #3, frmt(Abs(T), 4)
  Close #3
Close #1
'
'Legge PRFROT per ricerca Rmin e indice punto di Rmin
'Open RADICE & "\PRFROT" For Input As #1
'    Input #1, R, A, B
'    Rmin = R: Rmax = 0: RminIDX = 1
'    For i = 2 To npt
'      Input #1, R, A, B
'      If (R < Rmin) Then
'        Rmin = R: RminIDX = i
'      End If
'      If (R > Rmax) Then Rmax = R
'    Next i
'Close #1
Open RADICE & "\PRFROT" For Input As #1
    i = 1
    Input #1, Rag(i), ALFA(i), Beta(i)
    Rmin = Rag(i): Rmax = 0: RminIDX = 1
    For i = 2 To npt
      Input #1, Rag(i), ALFA(i), Beta(i)
      If (Rag(i) < Rmin) Then
        Rmin = Rag(i): RminIDX = i
      End If
      If (Rag(i) > Rmax) Then Rmax = Rag(i)
    Next i
Close #1
'

'Crea file RABROT.INT da PRFROT
Open RADICE & "\RABROT.INT" For Output As #1
    For i = 1 To NPF1
      stmp = ""
      If (Rag(i) > 0) Then
        stmp = stmp & "+" & frmt0(Abs(Rag(i)), 4)
      Else
        stmp = stmp & "-" & frmt0(Abs(Rag(i)), 4)
      End If
      If (ALFA(i) > 0) Then
        stmp = stmp & "+" & frmt0(Abs(ALFA(i)), 4)
      Else
        stmp = stmp & "-" & frmt0(Abs(ALFA(i)), 4)
      End If
      If (Beta(i) > 0) Then
        stmp = stmp & "+" & frmt0(Abs(Beta(i)), 4)
      Else
        stmp = stmp & "-" & frmt0(Abs(Beta(i)), 4)
      End If
      Print #1, stmp
    Next i
    'Rigira FIANCO 2
    For i = NPF1 + NpF2 To NPF1 + 1 Step -1
      stmp = ""
      If (Rag(i) > 0) Then
        stmp = stmp & "+" & frmt0(Abs(Rag(i)), 4)
      Else
        stmp = stmp & "-" & frmt0(Abs(Rag(i)), 4)
      End If
      If (ALFA(i) > 0) Then
        stmp = stmp & "+" & frmt0(Abs(ALFA(i)), 4)
      Else
        stmp = stmp & "-" & frmt0(Abs(ALFA(i)), 4)
      End If
      If (Beta(i) > 0) Then
        stmp = stmp & "+" & frmt0(Abs(Beta(i)), 4)
      Else
        stmp = stmp & "-" & frmt0(Abs(Beta(i)), 4)
      End If
      Print #1, stmp
    Next i
Close #1


'Legge XYT.250 per ricerca punto mola (X,Y,T) che corrisponde a indice di Rmin
Open RADICE & "\" & "XYT." & Format$(DiametroMola, "##0") For Input As #1
    i = 1
    Input #1, X, Y, T
    Do
      If (i = RminIDX) Then XH = X: YH = Y: TH = T
      Input #1, X, Y, T
      i = i + 1
    Loop While Not EOF(1)
Close #1
'
'Creazione di XYT.ALL => XYT.250 + Punto mola corrispondente a Rmin
Open RADICE & "\XYT." & Format$(DiametroMola, "##0") For Input As #1
    Open RADICE & "\XYT.ALL" For Output As #2
      For i = 1 To npt
        Input #1, X, Y, T
          riga = ""
          If (X > 0) Then riga = riga & "+" Else riga = riga & "-"
          riga = riga & frmt0(Abs(X), 4) & " "
          If (Y > 0) Then riga = riga & "+" Else riga = riga & "-"
          riga = riga & frmt0(Abs(Y), 4) & " "
          If (T > 0) Then riga = riga & "+" Else riga = riga & "-"
          riga = riga & frmt0(Abs(T), 4)
          Print #2, riga
      Next i
      riga = ""
      If (XH > 0) Then riga = riga & "+" Else riga = riga & "-"
      riga = riga & frmt0(Abs(XH), 4) & " "
      If (YH > 0) Then riga = riga & "+" Else riga = riga & "-"
      riga = riga & frmt0(Abs(YH), 4) & " "
      If (TH > 0) Then riga = riga & "+" Else riga = riga & "-"
      riga = riga & frmt0(Abs(TH), 3)
      Print #2, riga
    Close #2
Close #1
'
'Creazione di CORROT
Open RADICE & "\CORROT.BAK" For Output As #1
  Open RADICE & "\CORROT" For Output As #2
    'PRIMA RIGA AGGIUNTIVA
    riga = ""
    riga = riga & "+" & frmt0(0, 4)
    riga = riga & "+" & frmt0(0, 4)
    Print #1, riga
    Print #2, riga
    For i = 1 + 1 To npt + 2 - 1
      Print #1, "+0.0000+0.0000"
      Print #2, "+0.0000+0.0000"
    Next i
    'ULTIMA RIGA AGGIUNTIVA
    riga = ""
    riga = riga & "+" & frmt0(0, 4)
    riga = riga & "+" & frmt0(0, 4)
    Print #1, riga
    Print #2, riga
  Close #1
Close #2
'
'Creazione di PUNCHK
Open RADICE & "\PUNCHK" For Output As #1
    For i = 2 To npt + 1
      Print #1, i
    Next i
Close #1
'
'Creazione di PRFTOL.DAT
Open RADICE & "\PRFTOL.DAT" For Output As #1
    For i = 1 To npt
        Print #1, "+0.0100 +0.0100"
    Next i
Close #1
'
'Creazione di ABILITA.COR
Open RADICE & "\ABILITA.COR" For Output As #1
    For i = 0 To npt + 1
      Print #1, "1"
    Next i
Close #1
'
'Creazione di EPMROT
Open RADICE & "\EPMROT" For Output As #1
    For i = 0 To npt + 2
        Print #1, "+0.0000,+0.0000"
    Next i
Close #1
'
'Creazione di ERRORI.MOD
Open RADICE & "\ERRORI.MOD" For Output As #1
    For i = 0 To npt + 2
        Print #1, "+0.0000"
    Next i
Close #1
'


Exit Sub

errCREAZIONE_FILES_RAB_ESTERNI:
  If FreeFile > 0 Then Close
  MsgBox "ERROR: " & Error(Err), 48, "SUB: CREAZIONE_FILES_RAB_ESTERNI"
  Exit Sub

End Sub

Public Sub CALCOLO_SCANALATO(ByVal SiStampa As Integer)
'
Dim TIPO_SCANALATO As String
  '
  TIPO_SCANALATO = LEP_STR("TIPOSCA")
  If (TIPO_SCANALATO = "SPE") Then
    Call CALCOLO_SCANALATO_SPE(SiStampa)
  Else
    Call CALCOLO_SCANALATO_STD(SiStampa)
  End If
  '
End Sub

Public Sub CALCOLO_SCANALATO_STD(ByVal SiStampa As Integer)
'
Dim ZPC   As Single
Dim XPC   As Single
Dim ZPC2  As Single
Dim XPC2  As Single
Dim ZP0   As Single
Dim XP0   As Single
Dim ZP1   As Single
Dim XP1   As Single
Dim ZP2   As Single
Dim XP2   As Single
Dim ZP3   As Single
Dim Xp3   As Single
Dim ZP4   As Single
Dim XP4   As Single
Dim ZP5   As Single
Dim Xp5   As Single
Dim ZP10  As Single
Dim XP10  As Single
Dim ZP11  As Single
Dim XP11  As Single
Dim ZP12  As Single
Dim XP12  As Single
Dim ZP13  As Single
Dim Xp13  As Single
Dim ZP14  As Single
Dim XP14  As Single
Dim R96   As Single
Dim R97   As Single
Dim ANGF1 As Single
Dim ANGF2 As Single
Dim ANGT1 As Single
Dim ANGT2 As Single
Dim RINT  As Single
'
Dim R5    As Single
Dim R44   As Single
Dim R63   As Single
Dim R67   As Double
Dim R103  As Single
Dim R750  As Single
Dim R751  As Single
Dim R752  As Single
Dim R753  As Single
Dim R754  As Single
Dim R755  As Single
Dim R756  As Single
Dim R757  As Single
Dim R155  As Single
Dim R37   As Single
Dim R36   As Single
Dim R197  As Single
Dim R51   As Single
Dim R758  As Single
Dim R759  As Single
Dim R760  As Single
Dim R761  As Single
Dim R762  As Single
Dim R764  As Single
Dim R765  As Single
Dim R766  As Single
Dim R767  As Single
Dim R768  As Single
Dim R769  As Single
Dim R771  As Single
Dim R772  As Single
Dim R773  As Single
Dim R774  As Single
Dim R776  As Single
Dim R777  As Single
Dim R778  As Single
Dim R779  As Single
Dim R781  As Single
Dim R782  As Single
Dim R783  As Single
Dim R784  As Single
Dim R786  As Single
Dim R787  As Single
Dim R788  As Single
Dim R789  As Single
Dim R790  As Single
Dim R791  As Single
Dim R795  As Single
Dim R796  As Single
Dim R797  As Single
Dim R798  As Single
Dim R799  As Single
'
Dim PunX(2, 30) As Single
Dim PunZ(2, 30) As Single
'
Dim NPun  As Integer
Dim Nump  As Integer
Dim i     As Integer
'
On Error GoTo errCALCOLO_SCANALATO_STD
  '
  If (SiStampa = 0) Then
    OEMX.OEM1PicCALCOLO.BackColor = &HFFFFFF
    For i = 0 To 4
      OEMX.txtINP(i).Visible = False
      OEMX.lblINP(i).Visible = False
    Next i
    OEMX.OEM1chkINPUT(0).Visible = False
    OEMX.OEM1chkINPUT(1).Visible = False
    OEMX.OEM1chkINPUT(2).Visible = False
    OEMX.OEM1lblOX.Visible = False
    OEMX.OEM1lblOY.Visible = False
    OEMX.OEM1txtOX.Visible = False
    OEMX.OEM1txtOY.Visible = False
    OEMX.OEM1Command2.Visible = False
    OEMX.OEM1Label2.Visible = False
    OEMX.OEM1fraOpzioni.Visible = False
    OEMX.OEM1frameCalcolo.Caption = ""
  End If
  '
  R5 = LEP("R[5]")
  R103 = LEP("R[103]")  'DINT
  'R63 = LEP("R[63]")   'Dp
  R44 = LEP("R[44]")    'Dest
  R750 = LEP("R[750]")  'spessore
  'R96 = LEP("R[96]")
  'R97 = LEP("R[97]")
  R51 = LEP("R[51]")
  R754 = LEP("R[754]")
  R755 = LEP("R[755]")
  R752 = LEP("R[752]")
  R756 = LEP("R[756]")
  R753 = LEP("R[753]")
  R757 = LEP("R[757]")
  'ANGT1 = LEP("R[68]")
  'ANGT2 = LEP("R[69]")
  R751 = LEP("R[751]")
  R67 = PG
  RINT = R103 / 2
  '
  R37 = LEP("SPMOLA_G[0,1]")
  'If (R37 = 0) Then R37 = LEP("SPMOLA_G[0,2]")
  If (LEP("TMOLA_G[0,1]") = 2) Then R51 = 0.1
  '
  R197 = 0.1
  '
  ';CALCOLO PROFILO SCANALATO Std (DIN)
  ';Versione: 21.05.09
  ';Conversione da mm a gradi
  R155 = (R754 / ((R44 - R103) / 2)): R754 = Atn(R155)
  R155 = (R755 / ((R44 - R103) / 2)): R755 = Atn(R155)
  ';ANG. GAMMA
  R758 = (360 / R5 / 2) * R67 / 180
  R96 = R758 'ANGOLO FIANCO 1
  ';RAGGIO INT.
  R790 = R103 / 2
  ';RAGGIO EST.
  R791 = R44 / 2
  ';FIANCO 1
  ';ANG. BETA EST.
  R761 = FnASN((R750 + R751) / 2 / (R791 + R753))
  ';ANG. ALFA EST.
  R759 = R758 - R761
  ';ANG.BETA INT.
  R762 = FnASN((R750 + R751) / 2 / (R790 + R752))
  ';ANG.ALFA INT.
  R760 = R758 - R762
  R799 = R760 + 5 * R67 / 180
  ';CALCOLO PUNTI F1
  ';Z P3
  R773 = (R791 + R753) * Sin(R759)
  ';Y P3
  R783 = -(R791 + R753) * Cos(R759) + R790
  ';Z P4
  R774 = (R790 + R752) * Sin(R760)
  ';Y P4
  R784 = R790 - (R790 + R752) * Cos(R760)
  ';RICALCOLO DI P3
  R795 = R784 - R783
  R796 = R795 * Tan(R758)
  R798 = R758 + R754
  R782 = R795 / Cos(R798)
  R797 = R782 * Sin(R798)
  R773 = R773 + R797 - R796
  R783 = R784 - R782 * Cos(R798)
  ';Z P1
  R771 = R37 / 2 + R197 + 1
  ';Y P1
  R781 = R783 - R51 * (1 - Sin(R798))
  ';Z P2
  R772 = R773 + R51 * Cos(R798)
  ';Z P10
  R764 = R790 * Sin(R799)
  ';Y P10
  R769 = R790 * (1 - Cos(R799))
  '
  ZP1 = R771: XP1 = R781
  ZP2 = R772: XP2 = R781
  ZP3 = R773: Xp3 = R783
  ZP4 = R774: XP4 = R784
  '
  R96 = R798
  ZPC = R51 * Cos(R96) + ZP3
  XPC = Xp3 + R51 * Sin(R96)
  '
  ';FIANCO 2
  R97 = R758 'ANGOLO FIANCO 2
  ';BETA 2 EST. e BETA 2 INT.
  R766 = FnASN((R750 + R751) / 2 / (R791 + R757))
  R767 = FnASN((R750 + R751) / 2 / (R790 + R756))
  ';ALFA 2 EST. e ALFA 2 INT.
  R765 = R758 - R766
  R768 = R758 - R767
  ';PUNTI FIANCO 2
  ';Z P8
  R778 = -(R791 + R757) * Sin(R765)
  ';Y P8
  R788 = -(R791 + R757) * Cos(R765) + R790
  ';Z P9
  R779 = -(R790 + R756) * Sin(R768)
  ';Y P9
  R789 = R790 - (R790 + R756) * Cos(R768)
  ';RICALCOLO DI P8
  R795 = R789 - R788
  R796 = R795 * Tan(R758)
  R798 = R758 + R755
  R782 = R795 / Cos(R798)
  R797 = R782 * Sin(R798)
  R778 = R778 - R797 + R796
  R788 = R789 - R782 * Cos(R798)
  ';Z P6
  R776 = -R771
  ';Y P6
  R786 = R788 - R51 * (1 - Sin(R798))
  ';Z P7
  R777 = R778 - R51 * Cos(R798)
  '
  ZP11 = R776: XP11 = R786
  ZP12 = R777: XP12 = R786
  ZP13 = R778: Xp13 = R788
  ZP14 = R779: XP14 = R789
  '
  R97 = R798
  ZPC2 = -R51 * Cos(R97) + ZP13
  XPC2 = Xp13 + R51 * Sin(R97)
  '
  'Punto 5
  ZP5 = 0
  Xp5 = 0
  ';************************
  NPun = 9
  '*************************
  '
  Dim Angint    As Single
  Dim DAng      As Single
  Dim varang    As Single
  Dim j         As Integer
  Dim Np        As Integer
  Dim nPr       As Integer
  Dim NpRt      As Integer
  Dim AngRt     As Single
  Dim DAngRt    As Single
  Dim VarAngRt  As Single
  Dim Numvar    As Integer
  '
  'nPr = 5
  nPr = 15
  NpRt = 5
  'Np = nPr + 2
  Np = 5 + 2
  ANGF1 = R96
  ANGF2 = R97
  ANGT1 = 0
  ANGT2 = 0
  Numvar = nPr - 3
  '
  'FIANCO 1
  '*********************************
  PunZ(1, 1) = Int(ZP1 * 1000) / 1000: PunX(1, 1) = Int(XP1 * 1000) / 1000
  PunZ(1, 2) = Int(ZP2 * 1000) / 1000: PunX(1, 2) = Int(XP2 * 1000) / 1000
  'Punti sul raggio di testa fianco1
  AngRt = R67 / 2 - ANGF1 - ANGT1
  DAngRt = AngRt / (NpRt - 1)
  For i = 2 To NpRt - 1
    j = i + 1
    VarAngRt = ANGT1 + DAngRt * (i - 1)
    PunZ(1, j) = ZPC - R51 * Sin(VarAngRt)
    PunX(1, j) = XPC - R51 * Cos(VarAngRt)
  Next i
  'Punti sul fianco 1
  PunZ(1, Np - 1) = Int(ZP3 * 1000) / 1000: PunX(1, Np - 1) = Int(Xp3 * 1000) / 1000
  PunZ(1, Np) = Int(ZP4 * 1000) / 1000: PunX(1, Np) = Int(XP4 * 1000) / 1000
  'Punti sul diametro interno fianco1
  Angint = Atn(ZP4 / (RINT - XP4))
  DAng = Angint / (nPr - 1)
  For i = 2 To nPr - 1
    j = i + Np - 1
    varang = Angint - DAng * (i - 1)
    PunZ(1, j) = (RINT + R752 - R752 * (i - 2) / Numvar) * Sin(varang)
    PunX(1, j) = RINT - ((RINT + R752 - R752 * (i - 2) / Numvar) * Cos(varang))
    'PunX(1, j) = RINT - PunZ(1, j) / Tan(varang)
  Next i
  'Punto su centro mola
  PunZ(1, Np + nPr - 1) = Int(ZP5 * 1000) / 1000: PunX(1, Np + nPr - 1) = Int(Xp5 * 1000) / 1000
  '
  'Fianco 2
  '***************************************
  PunZ(2, 1) = Int(ZP11 * 1000) / 1000: PunX(2, 1) = Int(XP11 * 1000) / 1000
  PunZ(2, 2) = Int(ZP12 * 1000) / 1000: PunX(2, 2) = Int(XP12 * 1000) / 1000
  'Punti sul raggio di testa fianco2
  AngRt = R67 / 2 - ANGF2 - ANGT2
  DAngRt = AngRt / (NpRt - 1)
  For i = 2 To NpRt - 1
    j = i + 1
    VarAngRt = ANGT2 + DAngRt * (i - 1)
    PunZ(2, j) = ZPC2 + R51 * Sin(VarAngRt)
    PunX(2, j) = XPC2 - R51 * Cos(VarAngRt)
  Next i
  'Punti sul fianco 2
  PunZ(2, Np - 1) = Int(ZP13 * 1000) / 1000: PunX(2, Np - 1) = Int(Xp13 * 1000) / 1000
  PunZ(2, Np) = Int(ZP14 * 1000) / 1000: PunX(2, Np) = Int(XP14 * 1000) / 1000
  'Punti sul diametro interno fianco2
  Angint = Atn(-ZP14 / (RINT - XP14))
  DAng = Angint / (nPr - 1)
  For i = 2 To nPr - 1
    j = i + Np - 1
    varang = Angint - DAng * (i - 1)
    PunZ(2, j) = -(RINT + R756 - R756 * (i - 2) / Numvar) * Sin(varang)
    PunX(2, j) = RINT - ((RINT + R756 - R756 * (i - 2) / Numvar) * Cos(varang))
  Next i
  PunZ(2, Np + nPr - 1) = Int(ZP5 * 1000) / 1000: PunX(2, Np + nPr - 1) = Int(Xp5 * 1000) / 1000
  '
  '*******************************
  'Visualizzazione profilo scanalati
  '
  Dim pX1       As Single
  Dim pY1       As Single
  Dim pX2       As Single
  Dim pY2       As Single
  Dim Altezza   As Single
  Dim Larghezza As Single
  Dim ScalaX    As Single
  Dim ScalaY    As Single
  Dim Xmax(2)   As Single
  Dim Ymax(2)   As Single
  Dim Xmin(2)   As Single
  Dim Ymin(2)   As Single
  Dim PicH      As Single
  Dim PicW      As Single
  Dim da        As Double
  Dim stmp      As String
  Dim TextFont  As String
  Dim Atmp      As String * 255
  Dim Fianco    As Integer
  'LARGHEZZA
  stmp = GetInfo("CALCOLO", "Larghezza", Path_LAVORAZIONE_INI)
  If val(stmp) > 0 Then PicW = val(stmp) Else PicW = 190
  OEMX.OEM1PicCALCOLO.ScaleWidth = PicW
  'ALTEZZA
  stmp = GetInfo("CALCOLO", "Altezza", Path_LAVORAZIONE_INI)
  If val(stmp) > 0 Then PicH = val(stmp) Else PicH = 95
  OEMX.OEM1PicCALCOLO.ScaleHeight = PicH
  'CARATTERE DI STAMPA TESTO
      If (LINGUA = "CH") Then
    TextFont = "MS Song"
  ElseIf (LINGUA = "RU") Then
    TextFont = "Arial Cyr"
  Else
    TextFont = "Courier New"
  End If
  If (SiStampa = 1) Then
    For i = 1 To 2
      Printer.FontName = TextFont
      Printer.FontSize = 12
      Printer.FontBold = False
    Next i
  Else
    For i = 1 To 2
      OEMX.OEM1PicCALCOLO.FontName = TextFont
      OEMX.OEM1PicCALCOLO.FontSize = 8
      OEMX.OEM1PicCALCOLO.FontBold = False
    Next i
  End If
  OEMX.lblINP(0).Visible = True
  OEMX.txtINP(0).Visible = True
  OEMX.lblINP(1).Visible = False
  OEMX.txtINP(1).Visible = False
  'NASCONDO LA CASELLA DI TESTO
  OEMX.OEM1txtHELP.Visible = False
  'VISUALIZZO LE SCALE
  i = LoadString(g_hLanguageLibHandle, 1000, Atmp, 255):  If i > 0 Then OEMX.lblINP(0).Caption = Left$(Atmp, i)
  i = LoadString(g_hLanguageLibHandle, 1001, Atmp, 255):  If i > 0 Then OEMX.lblINP(1).Caption = Left$(Atmp, i)
  '
  If (SiStampa = 1) Then
    Printer.ScaleMode = 6
    WRITE_DIALOG OEMX.OEM1Combo1.Caption & " --> " & Printer.DeviceName
  Else
    OEMX.OEM1PicCALCOLO.Cls
  End If
  '
  'Estremi dei fianchi
  For Fianco = 1 To 2
    Xmax(Fianco) = PunZ(Fianco, 1)
    Ymax(Fianco) = PunX(Fianco, 1)
    Xmin(Fianco) = PunZ(Fianco, 1)
    Ymin(Fianco) = PunX(Fianco, 1)
    For i = 1 To Np
      If PunZ(Fianco, i) >= Xmax(Fianco) Then Xmax(Fianco) = PunZ(Fianco, i)
      If PunX(Fianco, i) >= Ymax(Fianco) Then Ymax(Fianco) = PunX(Fianco, i)
      If PunZ(Fianco, i) <= Xmin(Fianco) Then Xmin(Fianco) = PunZ(Fianco, i)
      If PunX(Fianco, i) <= Ymin(Fianco) Then Ymin(Fianco) = PunX(Fianco, i)
    Next i
  Next Fianco
  '
  Xmax(0) = Xmax(1): Ymax(0) = Ymax(1): Xmin(0) = Xmin(1): Ymin(0) = Ymin(1)
  For Fianco = 1 To 2
    If Xmax(Fianco) >= Xmax(0) Then Xmax(0) = Xmax(Fianco)
    If Ymax(Fianco) >= Ymax(0) Then Ymax(0) = Ymax(Fianco)
    If Xmin(Fianco) <= Xmin(0) Then Xmin(0) = Xmin(Fianco)
    If Ymin(Fianco) <= Ymin(0) Then Ymin(0) = Ymin(Fianco)
  Next Fianco
  '
  ScalaX = val(GetInfo("CALCOLO", "ScalaMOLA", Path_LAVORAZIONE_INI))
  ScalaY = ScalaX
  OEMX.txtINP(0).Text = frmt(ScalaX, 4)
  OEMX.txtINP(1).Text = frmt(ScalaY, 4)
  '
  'Fianco 1
  For i = 1 To Np + nPr - 1
    pX1 = -PunZ(1, i) * ScalaX + PicW / 2
    pY1 = -(PunX(1, i) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
    pX2 = -PunZ(1, i + 1) * ScalaX + PicW / 2
    pY2 = -(PunX(1, i + 1) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
    If (SiStampa = 1) Then
      'Profilo
      Printer.Line (pX1, pY1)-(pX2, pY2)
      'Croci
      Printer.Line (pX2 - 0.5, pY2)-(pX2 + 0.5, pY2)
      Printer.Line (pX2, pY2 - 0.5)-(pX2, pY2 + 0.5)
    Else
      'Profilo
      OEMX.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2)
      'Croci
      OEMX.OEM1PicCALCOLO.Line (pX2 - 0.5, pY2)-(pX2 + 0.5, pY2)
      OEMX.OEM1PicCALCOLO.Line (pX2, pY2 - 0.5)-(pX2, pY2 + 0.5)
    End If
  Next i
  'Fianco 2
  For i = 1 To Np + nPr - 1
    pX1 = -PunZ(2, i) * ScalaX + PicW / 2
    pY1 = -(PunX(2, i) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
    pX2 = -PunZ(2, i + 1) * ScalaX + PicW / 2
    pY2 = -(PunX(2, i + 1) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
    If (SiStampa = 1) Then
      'Profilo
      Printer.Line (pX1, pY1)-(pX2, pY2)
      'Croci
      Printer.Line (pX2 - 0.5, pY2)-(pX2 + 0.5, pY2)
      Printer.Line (pX2, pY2 - 0.5)-(pX2, pY2 + 0.5)
    Else
      'Profilo
      OEMX.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2)
      'Croci
      OEMX.OEM1PicCALCOLO.Line (pX2 - 0.5, pY2)-(pX2 + 0.5, pY2)
      OEMX.OEM1PicCALCOLO.Line (pX2, pY2 - 0.5)-(pX2, pY2 + 0.5)
    End If
  Next i
  pX1 = -Xmax(1) * ScalaX + PicW / 2
  pY1 = -((Ymax(0) - Ymin(0)) / 2) * ScalaY + PicH / 2
  pX2 = -Xmin(2) * ScalaX + PicW / 2  'Xmax(2) * ScalaX + PicW / 2
  pY2 = ((Ymax(0) - Ymin(0)) / 2) * ScalaY + PicH / 2
  If (SiStampa = 1) Then
    'Nome
    stmp = "F1"
    Printer.CurrentX = pX1 + Printer.TextWidth(stmp)
    Printer.CurrentY = pY2 - Printer.TextHeight(stmp)
    Printer.Print stmp
    stmp = "F2"
    Printer.CurrentX = pX2 - Printer.TextWidth(stmp)
    Printer.CurrentY = pY2 - Printer.TextHeight(stmp)
    Printer.Print stmp
    'Larghezza mola: valore
    stmp = "W=" & frmt(Xmax(1) - Xmin(2), 4) & "mm  "
    Printer.CurrentX = PicW / 2 - Printer.TextWidth(stmp)
    Printer.CurrentY = pY2 - Printer.TextHeight(stmp)
    Printer.Print stmp
    'Altezza mola: valore
    stmp = " H=" & frmt(Ymax(0) - Ymin(0), 4) & "mm"
    Printer.CurrentX = PicW / 2
    Printer.CurrentY = pY2 - Printer.TextHeight(stmp)
    Printer.Print stmp
    Printer.DrawStyle = 4
    'Larghezza mola: linea
    Printer.Line (pX1, pY2)-(pX2, pY2)
    'Altezza mola: linea
    Printer.Line (PicW / 2, pY1)-(PicW / 2, pY2)
    Printer.DrawStyle = 0
  Else
    'Nome
    stmp = "F1"
    OEMX.OEM1PicCALCOLO.CurrentX = pX1 + OEMX.OEM1PicCALCOLO.TextWidth(stmp)
    OEMX.OEM1PicCALCOLO.CurrentY = pY2 - OEMX.OEM1PicCALCOLO.TextHeight(stmp)
    OEMX.OEM1PicCALCOLO.Print stmp
    stmp = "F2"
    OEMX.OEM1PicCALCOLO.CurrentX = pX2 - OEMX.OEM1PicCALCOLO.TextWidth(stmp) 'pX2
    OEMX.OEM1PicCALCOLO.CurrentY = pY2 - OEMX.OEM1PicCALCOLO.TextHeight(stmp)
    OEMX.OEM1PicCALCOLO.Print stmp
    'Larghezza mola: valore
    stmp = "W=" & frmt(Xmax(1) - Xmin(2), 4) '& "mm  "
    OEMX.OEM1PicCALCOLO.CurrentX = PicW / 2 - OEMX.OEM1PicCALCOLO.TextWidth(stmp)
    OEMX.OEM1PicCALCOLO.CurrentY = pY2 - OEMX.OEM1PicCALCOLO.TextHeight(stmp)
    OEMX.OEM1PicCALCOLO.Print stmp
    'Altezza mola: valore
    stmp = " H=" & frmt(Ymax(0) - Ymin(0), 4) '& "mm"
    OEMX.OEM1PicCALCOLO.CurrentX = PicW / 2
    OEMX.OEM1PicCALCOLO.CurrentY = pY2 - OEMX.OEM1PicCALCOLO.TextHeight(stmp)
    OEMX.OEM1PicCALCOLO.Print stmp
    OEMX.OEM1PicCALCOLO.DrawStyle = 4
    'Larghezza mola: linea
    OEMX.OEM1PicCALCOLO.Line (pX1, pY2)-(pX2, pY2)
    'Altezza mola: linea
    OEMX.OEM1PicCALCOLO.Line (PicW / 2, pY1)-(PicW / 2, pY2)
    OEMX.OEM1PicCALCOLO.DrawStyle = 0
  End If
  '
  'Dimensione dei caratteri
  If (SiStampa = 1) Then
    For i = 1 To 2
      Printer.FontName = TextFont
      Printer.FontSize = 14
      Printer.FontBold = True
    Next i
    'Nome del documento
    stmp = Date & " " & Time & "                  " & OEMX.OEM1Combo1.Caption
    Printer.CurrentX = 0 + 25
    Printer.CurrentY = PicH
    Printer.Print stmp
    'Scala X
    If (OEMX.lblINP(0).Visible = True) Then
      stmp = OEMX.lblINP(0).Caption & ": x" & OEMX.txtINP(0).Text
      Printer.CurrentX = 0 + 25
      Printer.CurrentY = PicH + 10
      Printer.Print stmp
    End If
    'Scala Y
    If (OEMX.lblINP(1).Visible = True) Then
      stmp = OEMX.lblINP(1).Caption & ": x" & OEMX.txtINP(1).Text
      Printer.CurrentX = 0 + PicW / 2
      Printer.CurrentY = PicH + 10
      Printer.Print stmp
    End If
    WRITE_DIALOG OEMX.OEM1Combo1.Caption & " --> " & Printer.DeviceName & " OK!!"
  End If
  '
Exit Sub

errCALCOLO_SCANALATO_STD:

Resume Next


End Sub


Public Sub CALCOLO_SCANALATO_SPE(ByVal SiStampa As Integer)
'
';Calcolo profilo scanalati speciali
';***********************
';Definizioni
Dim ANGF1     As Single
Dim ANGF2     As Single
Dim Tm        As Single
Dim TMP0      As Single
Dim TMINT     As Single
Dim VARYC     As Single
Dim VARX0     As Single
Dim VARY0     As Single
Dim VARXM     As Single
Dim VARYM     As Single
Dim VARXI     As Single
Dim VARYI     As Single
Dim DeltaY    As Single
Dim DIST      As Single
Dim RINT      As Single
Dim REST      As Single
Dim CONTATORE As Single
Dim ORD_INT   As Single
Dim RAGPR     As Single
Dim ANGSOND   As Single
Dim ANGSONM   As Single
Dim ZPC       As Single
Dim XPC       As Single
Dim ZPC2      As Single
Dim XPC2      As Single
Dim ZP0       As Single
Dim XP0       As Single
Dim ZP1       As Single
Dim XP1       As Single
Dim ZP2       As Single
Dim XP2       As Single
Dim ZP3       As Single
Dim Xp3       As Single
Dim ZP4       As Single
Dim XP4       As Single
Dim ZP5       As Single
Dim Xp5       As Single
Dim ZP10      As Single
Dim XP10      As Single
Dim ZP11      As Single
Dim XP11      As Single
Dim ZP12      As Single
Dim XP12      As Single
Dim ZP13      As Single
Dim Xp13      As Single
Dim ZP14      As Single
Dim XP14      As Single
Dim TOLL      As Single
Dim CORSPE    As Single
Dim CORAF1    As Single
Dim CORAF2    As Single
Dim CALCOR1   As Single
Dim CALCOR2   As Single
Dim dz        As Single
Dim ANGT1     As Single
Dim ANGT2     As Single
'def per test VB
Dim R5    As Single
Dim R44   As Single
Dim R45   As Single
Dim R46   As Single
Dim R67   As Single
Dim R96   As Single
Dim R97   As Single
Dim R103  As Single
Dim R751  As Single
Dim R752  As Single
Dim R753  As Single
Dim R754  As Single
Dim R755  As Single
Dim R756  As Single
Dim R757  As Single
Dim R155  As Single
Dim R37   As Single
Dim R36   As Single
Dim R197  As Single
Dim R51   As Single
'
Dim PunX(2, 30) As Single
Dim PunZ(2, 30) As Single
Dim NPun As Integer
Dim Nump As Integer
Dim i As Integer
'
On Error GoTo errCALCOLO_SCANALATO_SPE
  '
  WRITE_DIALOG ""
  OEMX.OEM1PicCALCOLO.BackColor = &HFFFFFF
  For i = 0 To 4
    OEMX.txtINP(i).Visible = False
    OEMX.lblINP(i).Visible = False
  Next i
  OEMX.OEM1chkINPUT(0).Visible = False
  OEMX.OEM1chkINPUT(1).Visible = False
  OEMX.OEM1chkINPUT(2).Visible = False
  OEMX.OEM1lblOX.Visible = False
  OEMX.OEM1lblOY.Visible = False
  OEMX.OEM1txtOX.Visible = False
  OEMX.OEM1txtOY.Visible = False
  OEMX.OEM1Command2.Visible = False
  OEMX.OEM1Label2.Visible = False
  OEMX.OEM1fraOpzioni.Visible = False
  OEMX.OEM1frameCalcolo.Caption = ""
  '
  R5 = LEP("R[5]")
  R45 = LEP("R[45]")
  R103 = LEP("R[103]")
  R44 = LEP("R[44]")
  R46 = LEP("R[750]")
  R96 = LEP("R[96]")
  R97 = LEP("R[97]")
  R51 = LEP("R[51]")
  R754 = LEP("R[754]")
  R755 = LEP("R[755]")
  R752 = LEP("R[752]")
  R756 = LEP("R[756]")
  R753 = LEP("R[753]")
  R757 = LEP("R[757]")
  ANGT1 = LEP("R[68]")
  ANGT2 = LEP("R[69]")
  R751 = LEP("R[751]")
  '
  R37 = LEP("SPMOLA_G[0,1]")
  'If (R37 = 0) Then R37 = LEP("SPMOLA_G[0,2]")
  '
  R197 = 0.1
  R155 = 0
  TOLL = 0.001
  R67 = 3.141592654
  RAGPR = R103 / 2
  RINT = R45 / 2
  REST = R44 / 2
  ANGT1 = ANGT1 * R67 / 180
  ANGT2 = ANGT2 * R67 / 180
  'Correttore di spessore
  CORSPE = R751
  '
  '*************************
  ' FIANCO 1               *
  '*************************
  '; ANGOLO F1
  ANGF1 = R96 * R67 / 180
  ';Angolo relativo a spessore su dp
  ANGSOND = (R46 + CORSPE) / RAGPR '* 180 / R67
  ANGSONM = (360 / R5) * R67 / 180 - ANGSOND
  ';P0 SUL DIAM. PRIMITIVO
  ZP0 = RAGPR * Sin(ANGSONM / 2)
  XP0 = -(RAGPR * Cos(ANGSONM / 2) - RINT)
  CALCOR1 = 0
 
CALPF1:
  ';Calcolo punti sul diametro interno F1
  ';Punto P0 F1
  VARX0 = ZP0: VARY0 = XP0
  Tm = ANGF1
  ';CALCOLO PUNTO SUL DIAMETRO INTERNO
  VARYC = RINT
  VARXM = VARX0:  VARYM = VARY0
  DeltaY = 0.5
  CONTATORE = 0
CALCINT:
  VARYI = VARYM + DeltaY
  VARXI = VARXM - DeltaY * Tan(Tm)
  DIST = Sqr((VARXI) ^ 2 + (VARYC - VARYI) ^ 2)
  If DIST < (RINT + R752 - TOLL) Then
    CONTATORE = CONTATORE + 1
    DeltaY = DeltaY / 2
    GoTo CALCINT
  Else
    If DIST > (RINT + R752 + TOLL) Then
      VARYM = VARYI
      VARXM = VARXI
      CONTATORE = CONTATORE + 1
      GoTo CALCINT
    Else
      CONTATORE = CONTATORE + 1
      XP4 = VARYI: ZP4 = VARXI ';PUNTO DIAM INT. F1
    End If
  End If
  ';************************
  ';Calcolo punti sul diametro esterno F1
  ';Punto P0 F1
  VARX0 = ZP0: VARY0 = XP0
  Tm = ANGF1
  ';CALCOLO PUNTO SUL DIAMETRO ESTERNO
  VARYC = RINT
  VARXM = VARX0:  VARYM = VARY0
  DeltaY = 0.5
  CONTATORE = 0
CALCEST:
  VARYI = VARYM - DeltaY
  VARXI = VARXM + DeltaY * Tan(Tm)
  DIST = Sqr((VARXI) ^ 2 + (VARYC - VARYI) ^ 2)
  If DIST > (REST + R753 + TOLL) Then
    CONTATORE = CONTATORE + 1
    DeltaY = DeltaY / 2
    GoTo CALCEST
  Else
    If DIST < (REST + R753 - TOLL) Then
      VARYM = VARYI
      VARXM = VARXI
      CONTATORE = CONTATORE + 1
      GoTo CALCEST
    Else
      CONTATORE = CONTATORE + 1
      Xp3 = VARYI: ZP3 = VARXI ';PUNTO DIAM EST. F1
    End If
  End If
  ';************************
  ';CORR. ANGOLO F1
  If (R754 <> 0) And (CALCOR1 = 0) Then
    dz = (ZP3 - ZP4) / Sin(ANGF1)
    CORAF1 = R754 / dz   ';R754 corr. in mm fulcro su Dp
    ANGF1 = R96 * R67 / 180 + CORAF1 'CORAF1 corr. in radianti
    CALCOR1 = 1
    GoTo CALPF1
  End If
  'Punti 1,2
  If ANGT1 = 0 Then
    ZPC = R51 * Cos(ANGF1) + ZP3
    XPC = Xp3 + R51 * Sin(ANGF1)
    ZP2 = R51 * Cos(ANGF1) + ZP3
    XP2 = Xp3 - R51 * (1 - Sin(ANGF1))
    ZP1 = R37 / 2 + R197 / 2 + 0.5
    XP1 = XP2
  Else
    ZPC = R51 * Cos(ANGF1) + ZP3
    XPC = Xp3 + R51 * Sin(ANGF1)
    ZP2 = ZPC - R51 * Sin(ANGT1)
    XP2 = XPC - R51 * Cos(ANGT1)
    ZP1 = R37 / 2 + R197 / 2 + 0.5
    XP1 = XP2 - (ZP1 - ZP2) * Tan(ANGT1)
  End If
  '*************************
  ' FIANCO 2               *
  '*************************
  ANGF2 = R97 * R67 / 180
  ';P10 SUL DIAM. PRIMITIVO
  ZP10 = -(RAGPR * Sin(ANGSONM / 2))
  XP10 = -(RAGPR * Cos(ANGSONM / 2) - RINT)
  CALCOR2 = 0
CALPF2:
  ';Calcolo punti sul diametro interno F2
  ';Punto P10 F2
  VARX0 = ZP10: VARY0 = XP10
  Tm = ANGF2
  ';CALCOLO PUNTO SUL DIAMETRO INTERNO
  VARYC = RINT
  VARXM = VARX0:  VARYM = VARY0
  DeltaY = 0.5
  CONTATORE = 0
CALCINT2:
  VARYI = VARYM + DeltaY
  VARXI = VARXM + DeltaY * Tan(Tm)
  DIST = Sqr((VARXI) ^ 2 + (VARYC - VARYI) ^ 2)
  If DIST < (RINT + R756 - TOLL) Then
    CONTATORE = CONTATORE + 1
    DeltaY = DeltaY / 2
    GoTo CALCINT2
  Else
    If DIST > (RINT + R756 + TOLL) Then
      VARYM = VARYI
      VARXM = VARXI
      CONTATORE = CONTATORE + 1
      GoTo CALCINT2
    Else
      CONTATORE = CONTATORE + 1
      XP14 = VARYI: ZP14 = VARXI ';PUNTO DIAM INT. F2
    End If
  End If
  ';************************
  ';Calcolo punti sul diametro esterno F2
  ';Punto P10 F1
  VARX0 = ZP10: VARY0 = XP10
  Tm = ANGF2
  ';CALCOLO PUNTO SUL DIAMETRO ESTERNO
  VARYC = RINT
  VARXM = VARX0:  VARYM = VARY0
  DeltaY = 0.5
  CONTATORE = 0
CALCEST2:
  VARYI = VARYM - DeltaY
  VARXI = VARXM - DeltaY * Tan(Tm)
  DIST = Sqr((VARXI) ^ 2 + (VARYC - VARYI) ^ 2)
  If DIST > (REST + R757 + TOLL) Then
    CONTATORE = CONTATORE + 1
    DeltaY = DeltaY / 2
    GoTo CALCEST2
  Else
    If DIST < (REST + R757 - TOLL) Then
      VARYM = VARYI
      VARXM = VARXI
      CONTATORE = CONTATORE + 1
      GoTo CALCEST2
    Else
      CONTATORE = CONTATORE + 1
      Xp13 = VARYI: ZP13 = VARXI ';PUNTO DIAM EST. F2
    End If
  End If
  ';************************
  ';CORR. ANGOLO F2
  If (R755 <> 0) And (CALCOR2 = 0) Then
    dz = (ZP14 - ZP13) / Sin(ANGF2)
    CORAF2 = R755 / dz   ';R755 corr. in mm fulcro su Dp
    ANGF2 = R97 * R67 / 180 + CORAF2 'CORAF2 corr. in radianti
    CALCOR2 = 1
    GoTo CALPF2
  End If
  'Punti 11,12
  If ANGT2 = 0 Then
    ZPC2 = -R51 * Cos(ANGF2) + ZP13
    XPC2 = Xp13 + R51 * Sin(ANGF2)
    ZP12 = -R51 * Cos(ANGF2) + ZP13
    XP12 = Xp13 - R51 * (1 - Sin(ANGF2))
    ZP11 = -(R37 / 2 + R197 / 2 + 0.5)
    XP11 = XP12
  Else
    ZPC2 = -R51 * Cos(ANGF2) + ZP13
    XPC2 = Xp13 + R51 * Sin(ANGF2)
    ZP12 = ZPC2 + R51 * Sin(ANGT2)
    XP12 = XPC2 - R51 * Cos(ANGT2)
    ZP11 = -(R37 / 2 + R197 / 2 + 0.5)
    XP11 = XP12 - (ZP12 - ZP11) * Tan(ANGT2)
  End If
  'Punto 5
  ZP5 = 0
  Xp5 = 0
  ';************************
  NPun = 9
  '*************************
Dim Angint    As Single
Dim DAng      As Single
Dim varang    As Single
Dim j         As Integer
Dim Np        As Integer
Dim nPr       As Integer
Dim NpRt      As Integer
Dim AngRt     As Single
Dim DAngRt    As Single
Dim VarAngRt  As Single
  nPr = 5
  NpRt = 5
  Np = nPr + 2
  'FIANCO 1
  '*********************************
  PunZ(1, 1) = Int(ZP1 * 1000) / 1000: PunX(1, 1) = Int(XP1 * 1000) / 1000
  PunZ(1, 2) = Int(ZP2 * 1000) / 1000: PunX(1, 2) = Int(XP2 * 1000) / 1000
  'Punti sul raggio di testa fianco1
  AngRt = R67 / 2 - ANGF1 - ANGT1
  DAngRt = AngRt / (NpRt - 1)
  For i = 2 To NpRt - 1
    j = i + 1
    VarAngRt = ANGT1 + DAngRt * (i - 1)
    PunZ(1, j) = ZPC - R51 * Sin(VarAngRt)
    PunX(1, j) = XPC - R51 * Cos(VarAngRt)
  Next i
  'Punti sul fianco 1
  PunZ(1, Np - 1) = Int(ZP3 * 1000) / 1000: PunX(1, Np - 1) = Int(Xp3 * 1000) / 1000
  PunZ(1, Np) = Int(ZP4 * 1000) / 1000: PunX(1, Np) = Int(XP4 * 1000) / 1000
  'Punti sul diametro interno fianco1
  Angint = Atn(ZP4 / (RINT - XP4))
  DAng = Angint / (nPr - 1)
  For i = 2 To nPr - 1
    j = i + Np - 1
    varang = Angint - DAng * (i - 1)
    PunZ(1, j) = RINT * Sin(varang)
    PunX(1, j) = RINT - PunZ(1, j) / Tan(varang)
  Next i
  'Punto su centro mola
  PunZ(1, Np + nPr - 1) = Int(ZP5 * 1000) / 1000: PunX(1, Np + nPr - 1) = Int(Xp5 * 1000) / 1000
  'Fianco 2
  '***************************************
  PunZ(2, 1) = Int(ZP11 * 1000) / 1000: PunX(2, 1) = Int(XP11 * 1000) / 1000
  PunZ(2, 2) = Int(ZP12 * 1000) / 1000: PunX(2, 2) = Int(XP12 * 1000) / 1000
  'Punti sul raggio di testa fianco2
  AngRt = R67 / 2 - ANGF2 - ANGT2
  DAngRt = AngRt / (NpRt - 1)
  For i = 2 To NpRt - 1
    j = i + 1
    VarAngRt = ANGT2 + DAngRt * (i - 1)
   PunZ(2, j) = ZPC2 + R51 * Sin(VarAngRt)
   PunX(2, j) = XPC2 - R51 * Cos(VarAngRt)
  Next i
  'Punti sul fianco 2
  PunZ(2, Np - 1) = Int(ZP13 * 1000) / 1000: PunX(2, Np - 1) = Int(Xp13 * 1000) / 1000
  PunZ(2, Np) = Int(ZP14 * 1000) / 1000: PunX(2, Np) = Int(XP14 * 1000) / 1000
  'Punti sul diametro interno fianco2
  Angint = Atn(-ZP14 / (RINT - XP14))
  DAng = Angint / (nPr - 1)
  For i = 2 To nPr - 1
    j = i + Np - 1
    varang = Angint - DAng * (i - 1)
    PunZ(2, j) = -RINT * Sin(varang)
    PunX(2, j) = RINT - (-PunZ(2, j) / Tan(varang))
  Next i
  PunZ(2, Np + nPr - 1) = Int(ZP5 * 1000) / 1000: PunX(2, Np + nPr - 1) = Int(Xp5 * 1000) / 1000
  '*******************************
  'Visualizzazione profilo scanalati
  Dim pX1       As Single
  Dim pY1       As Single
  Dim pX2       As Single
  Dim pY2       As Single
  Dim Altezza   As Single
  Dim Larghezza As Single
  Dim ScalaX    As Single
  Dim ScalaY    As Single
  Dim Xmax(2)   As Single
  Dim Ymax(2)   As Single
  Dim Xmin(2)   As Single
  Dim Ymin(2)   As Single
  Dim PicH      As Single
  Dim PicW      As Single
  Dim da        As Double
  Dim stmp      As String
  Dim TextFont  As String
  Dim Atmp      As String * 255
  Dim Fianco    As Integer
  '
  'LARGHEZZA
  stmp = GetInfo("CALCOLO", "Larghezza", Path_LAVORAZIONE_INI)
  If val(stmp) > 0 Then PicW = val(stmp) Else PicW = 190
  OEMX.OEM1PicCALCOLO.ScaleWidth = PicW
  'ALTEZZA
  stmp = GetInfo("CALCOLO", "Altezza", Path_LAVORAZIONE_INI)
  If val(stmp) > 0 Then PicH = val(stmp) Else PicH = 95
  OEMX.OEM1PicCALCOLO.ScaleHeight = PicH
  'CARATTERE DI STAMPA TESTO
      If (LINGUA = "CH") Then
    TextFont = "MS Song"
  ElseIf (LINGUA = "RU") Then
    TextFont = "Arial Cyr"
  Else
    TextFont = "Courier New"
  End If
  If (SiStampa = 1) Then
    For i = 1 To 2
      Printer.FontName = TextFont
      Printer.FontSize = 12
      Printer.FontBold = False
    Next i
  Else
    For i = 1 To 2
      OEMX.OEM1PicCALCOLO.FontName = TextFont
      OEMX.OEM1PicCALCOLO.FontSize = 8
      OEMX.OEM1PicCALCOLO.FontBold = False
    Next i
  End If
  OEMX.lblINP(0).Visible = True
  OEMX.txtINP(0).Visible = True
  OEMX.lblINP(1).Visible = False
  OEMX.txtINP(1).Visible = False
  'NASCONDO LA CASELLA DI TESTO
  OEMX.OEM1txtHELP.Visible = False
  'VISUALIZZO LE SCALE
  i = LoadString(g_hLanguageLibHandle, 1000, Atmp, 255)
  If i > 0 Then OEMX.lblINP(0).Caption = Left$(Atmp, i)
  i = LoadString(g_hLanguageLibHandle, 1001, Atmp, 255)
  If i > 0 Then OEMX.lblINP(1).Caption = Left$(Atmp, i)
  If (SiStampa = 1) Then
    Printer.ScaleMode = 6
    WRITE_DIALOG OEMX.OEM1Combo1.Caption & " --> " & Printer.DeviceName
  Else
    OEMX.OEM1PicCALCOLO.Cls
  End If
 'Estremi dei fianchi
  For Fianco = 1 To 2
    Xmax(Fianco) = PunZ(Fianco, 1)
    Ymax(Fianco) = PunX(Fianco, 1)
    Xmin(Fianco) = PunZ(Fianco, 1)
    Ymin(Fianco) = PunX(Fianco, 1)
    For i = 1 To Np
      If PunZ(Fianco, i) >= Xmax(Fianco) Then Xmax(Fianco) = PunZ(Fianco, i)
      If PunX(Fianco, i) >= Ymax(Fianco) Then Ymax(Fianco) = PunX(Fianco, i)
      If PunZ(Fianco, i) <= Xmin(Fianco) Then Xmin(Fianco) = PunZ(Fianco, i)
      If PunX(Fianco, i) <= Ymin(Fianco) Then Ymin(Fianco) = PunX(Fianco, i)
    Next i
  Next Fianco
  Xmax(0) = Xmax(1): Ymax(0) = Ymax(1): Xmin(0) = Xmin(1): Ymin(0) = Ymin(1)
  For Fianco = 1 To 2
    If Xmax(Fianco) >= Xmax(0) Then Xmax(0) = Xmax(Fianco)
    If Ymax(Fianco) >= Ymax(0) Then Ymax(0) = Ymax(Fianco)
    If Xmin(Fianco) <= Xmin(0) Then Xmin(0) = Xmin(Fianco)
    If Ymin(Fianco) <= Ymin(0) Then Ymin(0) = Ymin(Fianco)
  Next Fianco
  ScalaX = val(GetInfo("CALCOLO", "ScalaMOLA", Path_LAVORAZIONE_INI))
  ScalaY = ScalaX
  OEMX.txtINP(0).Text = frmt(ScalaX, 4)
  OEMX.txtINP(1).Text = frmt(ScalaY, 4)
  'Fianco 1
  For i = 1 To Np + nPr - 1
    pX1 = -PunZ(1, i) * ScalaX + PicW / 2
    pY1 = -(PunX(1, i) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
    pX2 = -PunZ(1, i + 1) * ScalaX + PicW / 2
    pY2 = -(PunX(1, i + 1) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
    If (SiStampa = 1) Then
      'Profilo
      Printer.Line (pX1, pY1)-(pX2, pY2)
      'Croci
      Printer.Line (pX2 - 0.5, pY2)-(pX2 + 0.5, pY2)
      Printer.Line (pX2, pY2 - 0.5)-(pX2, pY2 + 0.5)
    Else
      'Profilo
      OEMX.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2)
      'Croci
      OEMX.OEM1PicCALCOLO.Line (pX2 - 0.5, pY2)-(pX2 + 0.5, pY2)
      OEMX.OEM1PicCALCOLO.Line (pX2, pY2 - 0.5)-(pX2, pY2 + 0.5)
    End If
  Next i
  'Fianco 2
  For i = 1 To Np + nPr - 1
    pX1 = -PunZ(2, i) * ScalaX + PicW / 2
    pY1 = -(PunX(2, i) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
    pX2 = -PunZ(2, i + 1) * ScalaX + PicW / 2
    pY2 = -(PunX(2, i + 1) - (Ymax(0) + Ymin(0)) / 2) * ScalaY + PicH / 2
    If (SiStampa = 1) Then
      'Profilo
      Printer.Line (pX1, pY1)-(pX2, pY2)
      'Croci
      Printer.Line (pX2 - 0.5, pY2)-(pX2 + 0.5, pY2)
      Printer.Line (pX2, pY2 - 0.5)-(pX2, pY2 + 0.5)
    Else
      'Profilo
      OEMX.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2)
      'Croci
      OEMX.OEM1PicCALCOLO.Line (pX2 - 0.5, pY2)-(pX2 + 0.5, pY2)
      OEMX.OEM1PicCALCOLO.Line (pX2, pY2 - 0.5)-(pX2, pY2 + 0.5)
    End If
  Next i
  pX1 = -Xmax(1) * ScalaX + PicW / 2
  pY1 = -((Ymax(0) - Ymin(0)) / 2) * ScalaY + PicH / 2
  pX2 = -Xmin(2) * ScalaX + PicW / 2  'Xmax(2) * ScalaX + PicW / 2
  pY2 = ((Ymax(0) - Ymin(0)) / 2) * ScalaY + PicH / 2
  If (SiStampa = 1) Then
    'Nome
    stmp = "F1"
    Printer.CurrentX = pX1 + Printer.TextWidth(stmp)
    Printer.CurrentY = pY2 - Printer.TextHeight(stmp)
    Printer.Print stmp
    stmp = "F2"
    Printer.CurrentX = pX2 - Printer.TextWidth(stmp)
    Printer.CurrentY = pY2 - Printer.TextHeight(stmp)
    Printer.Print stmp
    'Larghezza mola: valore
    stmp = "W=" & frmt(Xmax(1) - Xmin(2), 4) & "mm  "
    Printer.CurrentX = PicW / 2 - Printer.TextWidth(stmp)
    Printer.CurrentY = pY2 - Printer.TextHeight(stmp)
    Printer.Print stmp
    'Altezza mola: valore
    stmp = " H=" & frmt(Ymax(0) - Ymin(0), 4) & "mm"
    Printer.CurrentX = PicW / 2
    Printer.CurrentY = pY2 - Printer.TextHeight(stmp)
    Printer.Print stmp
    Printer.DrawStyle = 4
    'Larghezza mola: linea
    Printer.Line (pX1, pY2)-(pX2, pY2)
    'Altezza mola: linea
    Printer.Line (PicW / 2, pY1)-(PicW / 2, pY2)
    Printer.DrawStyle = 0
  Else
    'Nome
    stmp = "F1"
    OEMX.OEM1PicCALCOLO.CurrentX = pX1 + OEMX.OEM1PicCALCOLO.TextWidth(stmp)
    OEMX.OEM1PicCALCOLO.CurrentY = pY2 - OEMX.OEM1PicCALCOLO.TextHeight(stmp)
    OEMX.OEM1PicCALCOLO.Print stmp
    stmp = "F2"
    OEMX.OEM1PicCALCOLO.CurrentX = pX2 - OEMX.OEM1PicCALCOLO.TextWidth(stmp) 'pX2
    OEMX.OEM1PicCALCOLO.CurrentY = pY2 - OEMX.OEM1PicCALCOLO.TextHeight(stmp)
    OEMX.OEM1PicCALCOLO.Print stmp
    'Larghezza mola: valore
    stmp = "W=" & frmt(Xmax(1) - Xmin(2), 4) '& "mm  "
    OEMX.OEM1PicCALCOLO.CurrentX = PicW / 2 - OEMX.OEM1PicCALCOLO.TextWidth(stmp)
    OEMX.OEM1PicCALCOLO.CurrentY = pY2 - OEMX.OEM1PicCALCOLO.TextHeight(stmp)
    OEMX.OEM1PicCALCOLO.Print stmp
    'Altezza mola: valore
    stmp = " H=" & frmt(Ymax(0) - Ymin(0), 4) '& "mm"
    OEMX.OEM1PicCALCOLO.CurrentX = PicW / 2
    OEMX.OEM1PicCALCOLO.CurrentY = pY2 - OEMX.OEM1PicCALCOLO.TextHeight(stmp)
    OEMX.OEM1PicCALCOLO.Print stmp
    OEMX.OEM1PicCALCOLO.DrawStyle = 4
    'Larghezza mola: linea
    OEMX.OEM1PicCALCOLO.Line (pX1, pY2)-(pX2, pY2)
    'Altezza mola: linea
    OEMX.OEM1PicCALCOLO.Line (PicW / 2, pY1)-(PicW / 2, pY2)
    OEMX.OEM1PicCALCOLO.DrawStyle = 0
  End If
 'Dimensione dei caratteri
  If (SiStampa = 1) Then
    For i = 1 To 2
      Printer.FontName = TextFont
      Printer.FontSize = 14
      Printer.FontBold = True
    Next i
    'Nome del documento
    stmp = Date & " " & Time & "                  " & OEMX.OEM1Combo1.Caption
    Printer.CurrentX = 0 + 25
    Printer.CurrentY = PicH
    Printer.Print stmp
    'Scala X
    If OEMX.lblINP(0).Visible = True Then
      stmp = OEMX.lblINP(0).Caption & ": x" & OEMX.txtINP(0).Text
      Printer.CurrentX = 0 + 25
      Printer.CurrentY = PicH + 10
      Printer.Print stmp
    End If
    'Scala Y
    If OEMX.lblINP(1).Visible = True Then
      stmp = OEMX.lblINP(1).Caption & ": x" & OEMX.txtINP(1).Text
      Printer.CurrentX = 0 + PicW / 2
      Printer.CurrentY = PicH + 10
      Printer.Print stmp
    End If
    WRITE_DIALOG OEMX.OEM1Combo1.Caption & " --> " & Printer.DeviceName & " OK!!"
  End If

Exit Sub
errCALCOLO_SCANALATO_SPE:

Resume Next

End Sub

Sub LINEA_FIANCO(NomeForm As Form, ByVal Fianco As Integer, ByVal SiStampa As Integer, ByVal COMMENTO As String, ByVal pX1 As Single, ByVal pY1 As Single, ByVal pX2 As Single, ByVal pY2 As Single)
'
Dim PicW As Single
Dim PicH As Single
'
On Error Resume Next
  '
  PicW = NomeForm.OEM1PicCALCOLO.ScaleWidth
  PicH = NomeForm.OEM1PicCALCOLO.ScaleHeight
  If (SiStampa = 1) Then
    If Fianco = 1 Then
      Printer.DrawStyle = 2
      Printer.Line (pX1, pY1)-(pX2, pY2)
      Printer.CurrentX = pX1 - Printer.TextWidth(COMMENTO) / 2
      Printer.CurrentY = pY1
      Printer.Print COMMENTO
      Printer.DrawStyle = 0
    Else
      Printer.DrawStyle = 2
      Printer.Line (pX1, pY1)-(pX2, pY2)
      Printer.CurrentX = pX1 - Printer.TextWidth(COMMENTO) / 2
      Printer.CurrentY = pY1 - Printer.TextHeight(COMMENTO)
      Printer.Print COMMENTO
      Printer.DrawStyle = 0
    End If
  Else
    If Fianco = 1 Then
      NomeForm.OEM1PicCALCOLO.DrawStyle = 2
      NomeForm.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2)
      NomeForm.OEM1PicCALCOLO.CurrentX = pX1 - NomeForm.OEM1PicCALCOLO.TextWidth(COMMENTO) / 2
      NomeForm.OEM1PicCALCOLO.CurrentY = pY1
      NomeForm.OEM1PicCALCOLO.Print COMMENTO
      NomeForm.OEM1PicCALCOLO.DrawStyle = 0
    Else
      NomeForm.OEM1PicCALCOLO.DrawStyle = 2
      NomeForm.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2)
      NomeForm.OEM1PicCALCOLO.CurrentX = pX1 - NomeForm.OEM1PicCALCOLO.TextWidth(COMMENTO) / 2
      NomeForm.OEM1PicCALCOLO.CurrentY = pY1 - NomeForm.OEM1PicCALCOLO.TextHeight(COMMENTO)
      NomeForm.OEM1PicCALCOLO.Print COMMENTO
      NomeForm.OEM1PicCALCOLO.DrawStyle = 0
    End If
  End If
  '
End Sub

Sub TIF_di_riferimento(NomeForm As Form, ByVal Val1 As Double, ByVal Val2 As Double, ScalaY As Single, Ymax As Single, Ymin As Single, SiStampa As Integer)
'
Dim stmp As String
'
Dim pX1 As Single
Dim pY1 As Single
Dim pX2 As Single
Dim pY2 As Single
'
Dim PicH As Single
Dim PicW As Single
'
On Error Resume Next
  '
  PicH = NomeForm.OEM1PicCALCOLO.ScaleHeight
  PicW = NomeForm.OEM1PicCALCOLO.ScaleWidth
  '
  stmp = frmt(Val1, 4) ' & "mm"
  pX1 = 0
  pY1 = -(Val2 - (Ymax + Ymin) / 2) * ScalaY + PicH / 2
  If (SiStampa = 1) Then
    Printer.CurrentX = pX1
    Printer.CurrentY = pY1
    Printer.Print stmp
  Else
    NomeForm.OEM1PicCALCOLO.CurrentX = pX1
    NomeForm.OEM1PicCALCOLO.CurrentY = pY1
    NomeForm.OEM1PicCALCOLO.Print stmp
  End If
  pX1 = NomeForm.OEM1PicCALCOLO.TextWidth(stmp)
  stmp = frmt(Val2, 4) '& "mm"
  pX2 = PicW - NomeForm.OEM1PicCALCOLO.TextWidth(stmp)
  pY2 = -(Val2 - (Ymax + Ymin) / 2) * ScalaY + PicH / 2
  If (SiStampa = 1) Then
    Printer.DrawStyle = 4
    Printer.Line (pX1, pY1)-(pX2, pY2)
    Printer.DrawStyle = 0
    Printer.CurrentX = pX2
    Printer.CurrentY = pY2
    Printer.Print stmp
  Else
    NomeForm.OEM1PicCALCOLO.DrawStyle = 4
    NomeForm.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2)
    NomeForm.OEM1PicCALCOLO.DrawStyle = 0
    NomeForm.OEM1PicCALCOLO.CurrentX = pX2
    NomeForm.OEM1PicCALCOLO.CurrentY = pY2
    NomeForm.OEM1PicCALCOLO.Print stmp
  End If
  '
End Sub

Sub TIF_di_riferimento_Fianco(NomeForm As Form, ByVal Val1 As Double, ByVal Val2 As Double, ScalaY As Single, Ymax As Single, Ymin As Single, SiStampa As Integer, Fianco As Integer, Valutazione As Integer)
'
Dim stmp As String
'
Dim pX1  As Single
Dim pY1  As Single
Dim pX2  As Single
Dim pY2  As Single
'
Dim PicH As Single
Dim PicW As Single
'
On Error Resume Next
  '
  PicH = NomeForm.OEM1PicCALCOLO.ScaleHeight
  PicW = NomeForm.OEM1PicCALCOLO.ScaleWidth
  pY1 = -(Val2 - (Ymax + Ymin) / 2) * ScalaY + PicH / 2
  pY2 = pY1
  If (Valutazione = 1) Then
    stmp = " " & frmt(Val1, 3) & " "
  Else
    stmp = " " & frmt(Val2, 3) & " "
  End If
  If (SiStampa = 1) Then
    If (Fianco = 1) Then
      pX1 = 1 * PicW / 4 - 0.5 * PicW / 4
      pX2 = PicW / 2 - Printer.TextWidth(stmp)
    Else
      pX1 = 3 * PicW / 4 + 0.5 * PicW / 4
      pX2 = PicW / 2 + Printer.TextWidth(stmp)
    End If
    Printer.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
    If Fianco = 1 Then
      Printer.CurrentX = pX2
    Else
      Printer.CurrentX = pX2 - Printer.TextWidth(stmp)
    End If
    Printer.CurrentY = pY2 - Printer.TextHeight(stmp) / 2
    Printer.Print stmp
  Else
    If (Fianco = 1) Then
      pX1 = 1 * PicW / 4 - 0.5 * PicW / 4
      pX2 = PicW / 2 - NomeForm.OEM1PicCALCOLO.TextWidth(stmp)
    Else
      pX1 = 3 * PicW / 4 + 0.5 * PicW / 4
      pX2 = PicW / 2 + NomeForm.OEM1PicCALCOLO.TextWidth(stmp)
    End If
    NomeForm.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
    If (Fianco = 1) Then
      NomeForm.OEM1PicCALCOLO.CurrentX = pX2
    Else
      NomeForm.OEM1PicCALCOLO.CurrentX = pX2 - NomeForm.OEM1PicCALCOLO.TextWidth(stmp)
    End If
    NomeForm.OEM1PicCALCOLO.CurrentY = pY2 - NomeForm.OEM1PicCALCOLO.TextHeight(stmp) / 2
    NomeForm.OEM1PicCALCOLO.Print stmp
  End If
  '
End Sub

Function QUOTA_RULLI_SON(ByVal Z As Integer, ByVal Rb As Double, ByVal ALFAt As Double, ByVal Betab As Double, ByVal R As Double, ByVal Q As Double, ByVal DR As Double) As Double
'
Dim VAR1    As Double
Dim PARIZ   As Integer
Dim A2      As Double
Dim invA2   As Double
Dim ibMOLA  As Double
Dim invAP   As Double
'
On Error Resume Next
  '
  QUOTA_RULLI_SON = 0
  PARIZ = Int(Z / 2) * 2
  If PARIZ = Z Then
    VAR1 = Rb * 2 / (Q + DR)      'PARI
  Else
    VAR1 = Rb * 2 / (Q + DR) * Cos(PG / 2 / Z) 'DISPARI
  End If
  A2 = FnARC(VAR1)
  invA2 = Tan(A2) - A2
  ibMOLA = Rb * 2 * (invA2 + DR / Cos(Betab) / (Rb * 2))
  invAP = Tan(ALFAt) - ALFAt
  QUOTA_RULLI_SON = R * (ibMOLA / Rb - 2 * invAP)
  '
Exit Function

errQUOTA_RULLI_SON:
  StopRegieEvents
  MsgBox "Sub QUOTA_RULLI_SON: ERROR -> " & Err
  ResumeRegieEvents
  Resume Next

End Function

Sub CALCOLO_INGRANAGGIO_INTERNO(ByVal SiStampa As Integer, Optional SoloCalcolo As Boolean = False)
'
Dim pX1    As Single
Dim pY1    As Single
Dim pX2    As Single
Dim pY2    As Single
'
Dim PicH   As Single
Dim PicW   As Single
Dim Xmax   As Single
Dim Ymax   As Single
Dim Xmin   As Single
Dim Ymin   As Single
'
Dim ScalaX As Single
Dim ScalaY As Single
'
Dim bomba  As Double
Dim T0     As Double
'
Dim Raggio As Double
Dim ANGOLO As Double
'
Dim Np     As Integer
'
Dim AngR         As Double
Dim CorrSpessore As Double
Dim FHaF(2)      As Double
'
Dim Tif(2, 3)           As Double
Dim Rastremazione(2, 3) As Double
Dim Bombatura(2, 3)     As Double
''
Dim Atmp      As String * 255
Dim TextFont  As String
Dim Scelta    As Integer
Dim ret       As Integer
Dim R1        As Double
Dim R2        As Double
'
Dim Rayon As Double
Dim Alfa1 As Double
Dim beta1 As Double
Dim Ent   As Double
Dim INC   As Double
Dim PAS   As Double
Dim EXF   As Double
Dim RXF   As Double
Dim APFD  As Double
Dim Ltang As Double
'
Dim DIAM As Double
''
Dim SONn      As Double
Dim Ap        As Double
Dim Bp        As Double
Dim Cp        As Double
Dim Lp        As Double
Dim ArgSeno   As Double
Dim SINa2     As Double
Dim Tpromola  As Double
Dim Tpromolag As Double
Dim Uprof     As Double
Dim Vprof     As Double
Dim Wprof     As Double
Dim Rprof     As Double
'
Dim TSPF    As Double
Dim CTT     As Double
Dim STT     As Double
Dim XS      As Double
Dim Ys      As Double
Dim Zs      As Double
Dim TT      As Double
Dim TSIa    As Double
Dim FP      As Double
Dim F       As Double
Dim TSI     As Double
Dim TOL     As Double
Dim CS      As Double
Dim SS      As Double
Dim Rmin    As Double
Dim Tp      As Double
Dim DRM     As Double
Dim RMPD    As Double
Dim THIN    As Double
Dim T0S     As Double
Dim SF0     As Double
Dim CF0     As Double
Dim ST0     As Double
Dim CT0     As Double
Dim ST0S    As Double
Dim CT0S    As Double
Dim T0PF    As Double
Dim ST0F0   As Double
Dim CT0F0   As Double
Dim TMF     As Double
Dim CTF0    As Double
Dim RQC     As Double
Dim PSUDP2  As Double
Dim AA      As Double
Dim BB      As Double
Dim cc      As Double
Dim brid    As Double
Dim Crid    As Double
Dim RAD     As Double
Dim T1      As Double
Dim T2      As Double
Dim Ti      As Double
''
Dim SigmaG  As Double
Dim SIGMA   As Double
Dim DP      As Double
Dim CosA    As Double
Dim SinA    As Double
Dim TanA    As Double
Dim Ax      As Double
Dim InvAx   As Double
Dim Sx      As Double
Dim VX      As Double
''
Dim CS1     As Double
Dim CS2     As Double
Dim ALFAt   As Double
Dim BETAt   As Double
Dim BETAseg As Integer
''
Dim i       As Integer
Dim j       As Integer
Dim jj      As Integer
Dim dALFAm  As Double
Dim PM      As Double
Dim Sh      As Double
Dim AST$
Dim Ri      As Double
Dim vi      As Double
Dim DInt    As Double
Dim hu1     As Double
Dim HU2     As Double
Dim AngMed  As Double
''
Dim Xi  As Double
Dim Yi  As Double
Dim Ytr As Double
Dim Xtr As Double
''
Dim TH      As Double
Dim P       As Double
Dim FI0     As Double
Dim corrHa  As Double
Dim Corr    As Double
Dim RX      As Double
Dim X0      As Double
Dim Y0      As Double
Dim R0      As Double
Dim ALFA0   As Double
Dim BETA0   As Double
Dim Tif1    As Double
Dim Tif2    As Double
Dim Tif3    As Double
Dim TETA    As Double
Dim C1      As Double
Dim C2      As Double
Dim C3      As Double
Dim C4      As Double
Dim LL      As Double
Dim h       As Double
Dim Betab   As Double
Dim D3      As Double
Dim F3      As Double
Dim Sb      As Double
Dim PB      As Double
Dim iB      As Double
Dim MMp     As Double
Dim MM0     As Double
Dim MM1     As Double
Dim MM2     As Double
''
Dim s(dv)     As Double
Dim D(dv)     As Double
Dim A(dv)     As Double
Dim X(dv)     As Double
Dim Y(dv)     As Double
Dim API(dv)   As Double
Dim AlfaM(dv) As Double
Dim MM(dv)    As Double
Dim B(dv)     As Double
''
Dim XD(dv)    As Double
Dim YD(dv)    As Double
Dim TET(dv)   As Double
Dim L(2, dv)  As Double
Dim CB(dv)    As Double
Dim T(dv)     As Double
Dim Xm(dv)    As Double
Dim Ym(dv)    As Double
'
Dim CT(2, dv) As Double
''
Dim stmp As String
''
Dim AlfaD       As Double
Dim BetaD       As Double
Dim AM          As Double
Dim Z           As Double
Dim DEm         As Double
Dim D1          As Double
Dim D2          As Double
Dim SONdonnee   As Double
Dim Q           As Double
Dim DR          As Double
Dim RF          As Double
Dim DIint       As Double
Dim Valutazione As Integer
Dim NPR1        As Integer
Dim DIPU        As Double
Dim ALFAn       As Double
Dim BetaR       As Double
Dim IL          As Double
Dim R           As Double
Dim dprim       As Double
Dim D5          As Double
Dim SON         As Double
Dim Rb          As Double
Dim Fianco      As Integer
''
On Error GoTo errCALCOLO_INGRANAGGIO_INTERNO
  '
  WRITE_DIALOG ""
  '
  If Not SoloCalcolo Then
    Scelta = OEMX.OEM1ListaCombo1.ListIndex
    For i = 0 To 4
      OEMX.txtINP(i).Visible = False
      OEMX.lblINP(i).Visible = False
    Next i
    OEMX.OEM1chkINPUT(0).Visible = False
    OEMX.OEM1chkINPUT(1).Visible = False
    OEMX.OEM1chkINPUT(2).Visible = False
    OEMX.OEM1lblOX.Visible = False
    OEMX.OEM1lblOY.Visible = False
    OEMX.OEM1txtOX.Visible = False
    OEMX.OEM1txtOY.Visible = False
    OEMX.OEM1PicCALCOLO.BackColor = &HFFFFFF
    OEMX.OEM1PicCALCOLO.Cls
    OEMX.OEM1Command2.Visible = False
    OEMX.OEM1Label2.Visible = False
    OEMX.OEM1frameCalcolo.Caption = ""
  End If
  '
  'Numero dei punti del raggio di testa della mola
  NPR1 = val(GetInfo("CALCOLO", "NumeroPuntiRaggio", Path_LAVORAZIONE_INI))
  If (NPR1 = 0) Then NPR1 = 5
  '
  DEm = val(GetInfo("CALCOLO", "DiametroEsternoMolaINT", Path_LAVORAZIONE_INI))
  
  For Fianco = 1 To 2
    'PROFILO INGRANAGGIO
    Z = LEP("R[5]")
    AM = LEP("R[42]")
    AlfaD = LEP("R[40]")
    BetaD = LEP("R[41]")
    'SALVO IL SEGNO DI BETA
    If BetaD >= 0 Then BETAseg = 1 Else BETAseg = -1
    '
    D1 = LEP("R[43]"):    If (LEP("EAPF2_G") < D1) Then D1 = LEP("EAPF2_G")
    D2 = LEP("R[44]"):    If (LEP("SAPF2_G") > D2) Then D2 = LEP("SAPF2_G")
    '
    DIint = LEP("R[45]")
    SONdonnee = LEP("R[46]")
    If (DEm <= 0) Then DEm = DIint
    Q = LEP("R[49]")
    DR = LEP("R[50]")
    RF = LEP("R[52]")
    DIPU = LEP("R[194]")
    Valutazione = LEP("R[901]")
    'CORREZIONI RACCORDO FONDO DENTE
    AngR = LEP("R[56]")
    CorrSpessore = val(GetInfo("CALCOLO", "CorrSpessore", Path_LAVORAZIONE_INI))
    FHaF(1) = LEP("R[175]")
    FHaF(2) = LEP("R[178]")
    'CORREZIONI FIANCO 1
    Tif(1, 1) = LEP("R[270]")
    Rastremazione(1, 1) = LEP("R[271]")
    Bombatura(1, 1) = LEP("R[272]")
    Tif(1, 2) = LEP("R[273]")
    Rastremazione(1, 2) = LEP("R[274]")
    Bombatura(1, 2) = LEP("R[275]")
    Tif(1, 3) = LEP("R[276]")
    Rastremazione(1, 3) = LEP("R[277]")
    Bombatura(1, 3) = LEP("R[278]")
    'CORREZIONI FIANCO 2
    Tif(2, 1) = LEP("R[285]")
    Rastremazione(2, 1) = LEP("R[286]")
    Bombatura(2, 1) = LEP("R[287]")
    Tif(2, 2) = LEP("R[288]")
    Rastremazione(2, 2) = LEP("R[289]")
    Bombatura(2, 2) = LEP("R[290]")
    Tif(2, 3) = LEP("R[291]")
    Rastremazione(2, 3) = LEP("R[292]")
    Bombatura(2, 3) = LEP("R[293]")
    '
    If BetaD = 0 Then BetaD = 0.0001
    ALFAn = AlfaD * PG / 180
    BetaR = Abs(BetaD) * PG / 180
    '
    'If (Scelta <> 6) Then
      If Dir(g_chOemPATH & "\INTERNI" & Format$(Fianco, "0") & ".TEO") <> "" Then
        Kill g_chOemPATH & "\INTERNI" & Format$(Fianco, "0") & ".TEO"
      End If
      'CALCOLO MOLA/INGRANAGGIO/CONTATTO/CORREZIONI
      IL = (DIint - DEm) / 2
      'CALCOLO COSTANTI
      ALFAt = Atn(Tan(ALFAn) / Cos(BetaR))
      T1 = Tan(ALFAt)
      CS1 = Cos(ALFAt)
      CS2 = Cos(BETAt)
      F3 = T1 - Atn(T1)             'INVOLUTA(ALFAON)
      R = AM * Z / 2 / Cos(BetaR)   'R primitivo
      Betab = Atn(Tan(BetaR) * CS1) 'elica base
      Rb = R * CS1                  'R base
      PB = (Rb * 2 * PG) / Z        'Passo di base
      D3 = 2 * Rb
      D5 = 2 * R
      dprim = R * 2
      'SALVO LE COSTANTI PER LA STAMPA
      'stmp = "Diametro di base"
      j = WritePrivateProfileString("CALCOLO", "Dato(0)", frmt(2 * Rb, 4), Path_LAVORAZIONE_INI)
      'stmp = "Elica di base"
      j = WritePrivateProfileString("CALCOLO", "Dato(1)", frmt(Betab * 180 / PG, 4), Path_LAVORAZIONE_INI)
      'stmp = "Diametro primitivo"
      j = WritePrivateProfileString("CALCOLO", "Dato(2)", frmt(dprim, 4), Path_LAVORAZIONE_INI)
      If (SONdonnee <= 0) Then
        SON = QUOTA_RULLI_SON(Z, Rb, ALFAt, Betab, R, Q, DR)
        SONdonnee = (D5 * PG / Z - SON) * Cos(BetaR)
      End If
      'UTILIZZO LA CORREZIONE DI SPESSORE
      SONdonnee = SONdonnee + CorrSpessore
      SON = D5 * PG / Z - SONdonnee / Cos(BetaR)   'Spessore mola t.
      Sb = Rb * (SON / R + 2 * F3): iB = PB - Sb
      MM1 = Sqr(D1 ^ 2 - D3 ^ 2) / 2
      MM2 = Sqr(D2 ^ 2 - D3 ^ 2) / 2
      h = iB / 2 / Tan(Betab)  'H
      LL = R / Tan(BetaR)        'L
      C1 = Rb * Cos(Betab) * Tan(BetaR) + IL * Sin(Betab)
      C2 = Rb * Sin(Betab) + IL * Cos(Betab) * Tan(BetaR)
      C4 = Cos(Betab)
      C3 = h * C4
      If DIPU = 0 Then DIPU = 0.3
      DIPU = Int(DIPU * 100) / 100
      If NPR1 < 3 Then NPR1 = 10
      Open g_chOemPATH & "\PUNINTERNI.INI" For Output As #1
        Print #1, "Nr.   Diametro   mm     Ang_Pres_Ing     X      Y     Ang_Mola    L ass."
        TETA = -0.1
        MM0 = MM1 - DIPU
        Np = Abs(Int((MM2 - MM1) / DIPU + 0.5) + 1)
        DIPU = (MM2 - MM1) / (Np - 1)
        If Valutazione = 2 Then
          If Tif(Fianco, 1) > 0 Then Tif1 = Sqr(Tif(Fianco, 1) ^ 2 / 4 - Rb ^ 2)
          If Tif(Fianco, 2) > 0 Then Tif2 = Sqr(Tif(Fianco, 2) ^ 2 / 4 - Rb ^ 2)
          If Tif(Fianco, 3) > 0 Then Tif3 = Sqr(Tif(Fianco, 3) ^ 2 / 4 - Rb ^ 2)
        Else
          Tif1 = Tif(Fianco, 1)
          Tif2 = Tif(Fianco, 2)
          Tif3 = Tif(Fianco, 3)
        End If
        GoSub CALCKN
        'EVENTUALE GESTIONE DEL RAGGIO DI TESTA
        'INSERIRE I PUNTI E CREARE I FILE INTERNIX.TEO
        For i = 1 To Np                 'loop calcolo
          'corrHa = (i - 1) * FhaF(Fianco) / (NP - 1)
          corrHa = (Np - i) * FHaF(Fianco) / (Np - 1)
          Corr = -CB(Np - i + 1) - corrHa
          CT(Fianco, Np - i + 1) = Corr
          MM(i) = MM1 + DIPU * (Np - i)
          RX = Sqr(Rb ^ 2 + MM(i) ^ 2)
          GoSub 7000 'CALCOLO T0, FI0
          If i = Np Then
            X0 = RX * Cos(T0)
            Y0 = RX * Sin(T0)
            R0 = RX
            ALFA0 = T0
            BETA0 = FI0
          End If
          GoSub 10000 'PUREA
          AlfaM(i) = B(i) * PG / 180
          TET(i) = TH / PG * 180
          L(Fianco, i) = TET(i) / 360 * P
          D(i) = 2 * Sqr(MM(i) ^ 2 + Rb ^ 2)
          API(i) = Atn(MM(i) / Rb) * 180 / PG
          Print #1, Left$(Format$(i, "##0") & String(5, " "), 5);
          Print #1, Left$(frmt(D(i), 3) & String(10, " "), 10);
          Print #1, Left$(frmt(MM(i), 3) & String(10, " "), 10);
          Print #1, Left$(frmt(API(i), 3) & String(10, " "), 10);
          Print #1, Left$(frmt(X(i), 4) & String(10, " "), 10);
          Print #1, Left$(frmt(Y(i), 4) & String(10, " "), 10);
          Print #1, Left$(frmt(B(i), 4) & String(10, " "), 10);
          Print #1, Left$(frmt(L(Fianco, i), 4) & String(10, " "), 10)
        Next i
        'PRIMITIVO
        i = 0
        MM(i) = Sqr(R ^ 2 - Rb ^ 2)
        RX = R
        API(i) = FnARC(Rb / R) * 180 / PG
        GoSub 7000
        GoSub 10000
        TET(i) = TH / PG * 180
        L(Fianco, i) = TET(i) / 360 * P
        D(i) = R * 2
        AlfaM(i) = B(i) * PG / 180
        'MODIFICA RACCORDO FONDO
        AlfaM(1) = AlfaM(1) + AngR * (PG / 180)
        Yi = -IL + DIint / 2
        Ytr = Yi - RF * (1 - Sin(AlfaM(1)))
        If (Y(1) < Ytr) Then
          Xtr = X(1) - (Ytr - Y(1)) * Tan(AlfaM(1))
          Xi = Xtr - RF * Cos(AlfaM(1))
        End If
        If (Y(1) >= Ytr) Then
          Xtr = X(1)
          Ytr = Y(1)
          Yi = Y(1) + RF * (1 - Sin(AlfaM(1)))
          Xi = X(1) - RF * Cos(AlfaM(1))
        End If
        Ri = IL + Yi
        vi = Xi * 2
        DInt = Ri * 2
        AST$ = " ": If vi < 0 Then AST$ = "*"
        hu1 = Y(1) - Y(Np)
        HU2 = hu1 + RF * (1 - Sin(AlfaM(1)))
        AngMed = Atn((X(Np) - X(1)) / hu1)
        Sh = hu1 / Cos(AngMed) / 2
        PM = Int(Np / 2)
        bomba = X(Np) - (Y(PM) - Y(Np)) * Tan(AngMed) - X(PM)
        bomba = bomba * Cos(AngMed)
        R = (Sh ^ 2 + bomba ^ 2) / 2 / bomba
        'Raggio di fondo
        dALFAm = (PG / 2 - AlfaM(1)) / (NPR1 - 1) 'delta alfam in rd
        For j = 0 To NPR1 - 1
          jj = Np + j + 1
          AlfaM(jj) = AlfaM(1) + dALFAm * j
          X(jj) = Xi + RF * Cos(AlfaM(jj))
          Y(jj) = Yi - RF * (1 - Sin(AlfaM(jj)))
          GoSub 19000  ' PROFILO INGRANAGGIO SEZ. TRASVERSALE
        Next j
        jj = Np + NPR1 + 1
        AlfaM(jj) = PG / 2
        X(jj) = 0
        Y(jj) = Yi
        GoSub 19000  ' PROFILO INGRANAGGIO SEZ. TRASVERSALE
        HU2 = Yi - Y(Np)
        Print #1, ""
        Print #1, ";SEZIONE DA LEGGERE COME INI"
        Print #1, "[INFORMAZIONI]"
        Print #1, Left$("Alfan" & String(15, " "), 15) & "= " & frmt(AlfaD, 4)
        Print #1, Left$("BETA" & String(15, " "), 15) & "= " & frmt(BetaD, 4)
        Print #1, Left$("Modulo" & String(15, " "), 15) & "= " & frmt(AM, 4)
        Print #1, Left$("N denti" & String(15, " "), 15) & "= " & frmt(Z, 4)
        Print #1, Left$("SON n" & String(15, " "), 15) & "= " & frmt(SONdonnee, 4)
        Print #1, Left$("D base" & String(15, " "), 15) & "= " & frmt(D3, 4)
        Print #1, Left$("D prim" & String(15, " "), 15) & "= " & frmt(D5, 4)
        Print #1, Left$("EAP" & String(15, " "), 15) & "= " & frmt(D1, 4)
        Print #1, Left$("SAP" & String(15, " "), 15) & "= " & frmt(D2, 4)
        Print #1, Left$("Rf" & String(15, " "), 15) & "= " & frmt(RF, 4)
        Print #1, Left$("D int" & String(15, " "), 15) & "= " & frmt(DInt, 4)
        Print #1, Left$("L f" & String(15, " "), 15) & "= " & frmt(vi, 4) & " ";: Print #1, AST$
        Print #1, Left$("SWEAP" & String(15, " "), 15) & "= " & frmt(X(Np) * 2, 4)
        Print #1, Left$("SWSAP" & String(15, " "), 15) & "= " & frmt(X(1) * 2, 4)
        Print #1, Left$("ANGM" & String(15, " "), 15) & "= " & frmt(AngMed / PG * 180, 4)
        Print #1, Left$("R bom" & String(15, " "), 15) & "= " & frmt(R, 4)
        Print #1, Left$("Cav" & String(15, " "), 15) & "= " & frmt(bomba, 4)
        Print #1, Left$("HU1" & String(15, " "), 15) & "= " & frmt(hu1, 4)
        Print #1, Left$("HU2" & String(15, " "), 15) & "= " & frmt(HU2, 4)
        Print #1, Left$("L(1)pr" & String(15, " "), 15) & "= " & frmt(L(1, Np), 4)
        Print #1, Left$("L(2)pr" & String(15, " "), 15) & "= " & frmt(L(2, Np), 4)
        Print #1, Left$("Ang min mola" & String(15, " "), 15) & "= " & frmt(AlfaM(Np) / PG * 180, 4)
        Print #1, Left$("Interasse" & String(15, " "), 15) & "= " & frmt(IL, 4)
      Close #1
      'Registra profilo teorico per verifica profilo mola
      ReDim Preserve CalcoloEsterni.MolaX(2, Np + NPR1 + 1)
      ReDim Preserve CalcoloEsterni.MolaY(2, Np + NPR1 + 1)
      ReDim Preserve CalcoloEsterni.IngrX(2, Np + NPR1 + 1)
      ReDim Preserve CalcoloEsterni.IngrY(2, Np + NPR1 + 1)
      '
      Dim iK As Integer
      iK = 1
      '
      Open g_chOemPATH & "\INTERNI" & Format$(Fianco, "0") & ".TEO" For Output As #1
        Print #1, "Nr     R       ALFA    BETA      X        Y        G"
        For i = Np To 1 Step -1
          A(i) = Atn(XD(i) / YD(i))
          CalcoloEsterni.MolaX(Fianco, iK) = X(i)
          CalcoloEsterni.MolaY(Fianco, iK) = Y(i)
          CalcoloEsterni.IngrX(Fianco, iK) = -(D(i) / 2) * Sin(A(i))
          CalcoloEsterni.IngrY(Fianco, iK) = (D(i) / 2) * Cos(A(i))
          iK = iK + 1
          Print #1, Left$(Format$(i, "##0") & String(5, " "), 5);
          Print #1, Left$(frmt(D(i) / 2, 3) & String(10, " "), 10);
          Print #1, Left$(frmt(A(i) * 180 / PG, 3) & String(10, " "), 10);
          Print #1, Left$(frmt(API(i), 3) & String(10, " "), 10);
          Print #1, Left$(frmt(X(i), 4) & String(10, " "), 10);
          Print #1, Left$(frmt(Y(i), 4) & String(10, " "), 10);
          Print #1, Left$(frmt(AlfaM(i) / PG * 180, 4) & String(10, " "), 10)
        Next i
        For i = Np + 1 To Np + NPR1 + 1
          CalcoloEsterni.MolaX(Fianco, iK) = X(i)
          CalcoloEsterni.MolaY(Fianco, iK) = Y(i)
          CalcoloEsterni.IngrX(Fianco, iK) = -(D(i) / 2) * Sin(A(i))
          CalcoloEsterni.IngrY(Fianco, iK) = (D(i) / 2) * Cos(A(i))
          iK = iK + 1
          Print #1, Left$(Format$(i, "##0") & String(5, " "), 5);
          Print #1, Left$(frmt(D(i) / 2, 3) & String(10, " "), 10);
          Print #1, Left$(frmt(A(i) * 180 / PG, 3) & String(10, " "), 10);
          Print #1, Left$(frmt(API(i), 3) & String(10, " "), 10);
          Print #1, Left$(frmt(X(i), 4) & String(10, " "), 10);
          Print #1, Left$(frmt(Y(i), 4) & String(10, " "), 10);
          Print #1, Left$(frmt(AlfaM(i) / PG * 180, 4) & String(10, " "), 10)
        Next i
      Close #1
      'SALVATAGGIO DEL CALCOLO SU FILE ASCII (RIPRESO DAGLI ESTERNI DA VERIFICARE)
      If (Fianco = 1) Then
        Open g_chOemPATH & "\DATI.TEO" For Output As #1
      Else
        Open g_chOemPATH & "\DATI.TEO" For Append As #1
      End If
          Print #1, String(41, "-") & " F" & Format$(Fianco, "0") & " " & String(41, "-")
          Print #1, Left$("Nr" & String(6, " "), 6);
          Print #1, Left$("D " & String(10, " "), 10);
          Print #1, Left$("MM" & String(10, " "), 10);
          Print #1, Left$("Ap" & String(10, " "), 10);
          Print #1, Left$("Ra" & String(10, " "), 10);
          Print #1, Left$("Xm" & String(10, " "), 10);
          Print #1, Left$("Ym" & String(10, " "), 10);
          Print #1, Left$("Tm" & String(10, " "), 10);
          Print #1, Left$("Corr." & String(10, " "), 10)
          Print #1, String(6 + 8 * 10, "-")
          'PRIMITIVO
          i = 0
          Print #1, Left$("DP(" & Format$(Fianco, "0") & ")" & String(6, " "), 6);
          Print #1, Left$(frmt(D(i), 4) & String(10, " "), 10);
          Print #1, Left$(frmt(MM(i), 4) & String(10, " "), 10);
          Print #1, Left$(frmt(API(i), 4) & String(10, " "), 10);
          Print #1, Left$(frmt(Tan(API(i) * PG / 180) * 180 / PG, 4) & String(10, " "), 10);
          Print #1, Left$(frmt(X(i), 4) & String(10, " "), 10);
          Print #1, Left$(frmt(Y(i), 4) & String(10, " "), 10);
          Print #1, Left$(frmt((AlfaM(i) * (180 / PG)), 4) & String(10, " "), 10);
          Print #1, Left$(String(10, " "), 10)
          'FIANCO
          For i = Np To 1 Step -1
            Print #1, Left$(Format$(i, "#####0") & String(6, " "), 6);
            Print #1, Left$(frmt(D(i), 4) & String(10, " "), 10);
            Print #1, Left$(frmt(MM(i), 4) & String(10, " "), 10);
            Print #1, Left$(frmt(API(i), 4) & String(10, " "), 10);
            Print #1, Left$(frmt(Tan(API(i) * PG / 180) * 180 / PG, 4) & String(10, " "), 10);
            Print #1, Left$(frmt(X(i), 4) & String(10, " "), 10);
            Print #1, Left$(frmt(Y(i), 4) & String(10, " "), 10);
            Print #1, Left$(frmt((AlfaM(i) * (180 / PG)), 4) & String(10, " "), 10);
            Print #1, Left$(frmt(CT(Fianco, Np - i), 4) & String(10, " "), 10)
          Next i
          'RAGGIO DI FONDO DENTE
          For i = Np + 1 To Np + NPR1 + 1
            Print #1, Left$(String(6, " "), 6);
            Print #1, Left$(String(10, " "), 10);
            Print #1, Left$(String(10, " "), 10);
            Print #1, Left$(String(10, " "), 10);
            Print #1, Left$(String(10, " "), 10);
            Print #1, Left$(frmt(X(i), 4) & String(10, " "), 10);
            Print #1, Left$(frmt(Y(i), 4) & String(10, " "), 10);
            Print #1, Left$(frmt((AlfaM(i) * (180 / PG)), 4) & String(10, " "), 10);
            Print #1, Left$(String(10, " "), 10)
          Next i
        Close #1
    'End If
  Next Fianco
  '
  'If (Scelta <> 6) Then
    ReDim Preserve CalcoloEsterni.IngrX(2, iK - 1 - (NPR1 + 1))
    ReDim Preserve CalcoloEsterni.IngrY(2, iK - 1 - (NPR1 + 1))
  'End If
  '
  If Not SoloCalcolo Then
    'LARGHEZZA
    stmp = GetInfo("CALCOLO", "Larghezza", Path_LAVORAZIONE_INI):  If val(stmp) > 0 Then PicW = val(stmp) Else PicW = 190
    OEMX.OEM1PicCALCOLO.ScaleWidth = PicW
    'ALTEZZA
    stmp = GetInfo("CALCOLO", "Altezza", Path_LAVORAZIONE_INI):    If val(stmp) > 0 Then PicH = val(stmp) Else PicH = 95
    OEMX.OEM1PicCALCOLO.ScaleHeight = PicH
    'CARATTERE DI STAMPA TESTO
        If (LINGUA = "CH") Then
      TextFont = "MS Song"
    ElseIf (LINGUA = "RU") Then
      TextFont = "Arial Cyr"
    Else
      TextFont = "Courier New"
    End If
    If (SiStampa = 1) Then
      For i = 1 To 2
        Printer.FontName = TextFont
        Printer.FontSize = 12
        Printer.FontBold = False
      Next i
    Else
      For i = 1 To 2
        OEMX.OEM1PicCALCOLO.FontName = TextFont
        OEMX.OEM1PicCALCOLO.FontSize = 10
        OEMX.OEM1PicCALCOLO.FontBold = False
      Next i
    End If
    '
    'SELEZIONE DEL TIPO DI GRAFICO
    Select Case OEMX.OEM1ListaCombo1.ListIndex
        '
      Case 0: Call VISUALIZZA_MOLA_INGRANAGGIO(OEMX, 0, SiStampa) 'PROFILO MOLA
        '
      Case 1: Call VISUALIZZA_MOLA_INGRANAGGIO(OEMX, 1, SiStampa) 'PROFILO INGRANAGGIO
        '
      Case 2 'CONTATTO
        OEMX.OEM1fraOpzioni.Visible = False
        OEMX.lblINP(0).Visible = True: OEMX.txtINP(0).Visible = True
        OEMX.lblINP(1).Visible = True: OEMX.txtINP(1).Visible = True
        GoSub ETICHETTE_GRAFICI
        Fianco = 1
          Xmax = D(1): Ymax = L(Fianco, 1)
          Xmin = D(1): Ymin = L(Fianco, 1)
          For i = 1 To Np '+ NpR1 + 1
            If L(Fianco, i) >= Ymax Then Ymax = L(Fianco, i)
            If D(i) >= Xmax Then Xmax = D(i)
            If L(Fianco, i) <= Ymin Then Ymin = L(Fianco, i)
            If D(i) <= Xmin Then Xmin = D(i)
          Next i
        Fianco = 2
          For i = 1 To Np ' + NpR1 + 1
            If L(Fianco, i) >= Ymax Then Ymax = L(Fianco, i)
            If L(Fianco, i) <= Ymin Then Ymin = L(Fianco, i)
          Next i
        ScalaX = val(GetInfo("CALCOLO", "ScalaCONTATTOx", Path_LAVORAZIONE_INI))
        ScalaY = val(GetInfo("CALCOLO", "ScalaCONTATTOy", Path_LAVORAZIONE_INI))
        OEMX.txtINP(0).Text = frmt(ScalaX, 4)
        OEMX.txtINP(1).Text = frmt(ScalaY, 4)
        'FIANCO 1
        For i = 1 To Np - 1 '+ NpR1 + 1
          pX1 = (D(i) - Xmax) * ScalaX + PicW / 2
          pY1 = -L(1, i) * ScalaY * BETAseg + PicH / 2
          pX2 = (D(i + 1) - Xmax) * ScalaX + PicW / 2
          pY2 = -L(1, i + 1) * ScalaY * BETAseg + PicH / 2
          If (SiStampa = 1) Then
            Printer.Line (pX1, pY1)-(pX2, pY2)
          Else
            OEMX.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2)
          End If
        Next i
        i = Np: stmp = "EAP": GoSub LineaF1
        i = 0:  stmp = "Pri": GoSub LineaF1
        i = 1:  stmp = "SAP": GoSub LineaF1
        'FIANCO 2
        For i = 1 To Np - 1 '+ NpR1 + 1
          pX1 = -(D(i) - Xmax) * ScalaX + PicW / 2
          pY1 = L(2, i) * ScalaY * BETAseg + PicH / 2
          pX2 = -(D(i + 1) - Xmax) * ScalaX + PicW / 2
          pY2 = L(2, i + 1) * ScalaY * BETAseg + PicH / 2
          If (SiStampa = 1) Then
            Printer.Line (pX1, pY1)-(pX2, pY2)
          Else
            OEMX.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2)
          End If
        Next i
        i = Np: stmp = "EAP": GoSub LineaF2
        i = 0:  stmp = "Pri": GoSub LineaF2
        i = 1:  stmp = "SAP": GoSub LineaF2
        'Larghezza ed estensione
        pX1 = -(D(1) - Xmin) * ScalaX + PicW / 2
        pY1 = PicH / 2
        pX2 = (D(1) - Xmin) * ScalaX + PicW / 2
        pY2 = PicH / 2
          If (SiStampa = 1) Then
            Printer.DrawStyle = 2
            Printer.Line (pX1, pY1)-(pX2, pY2)
            Printer.DrawStyle = 0
            stmp = "F1"
            Printer.CurrentX = pX1
            Printer.CurrentY = pY1
            Printer.Print stmp
            stmp = "F2"
            Printer.CurrentX = pX2
            Printer.CurrentY = pY2
            Printer.Print stmp
          Else
            OEMX.OEM1PicCALCOLO.DrawStyle = 2
            OEMX.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2)
            OEMX.OEM1PicCALCOLO.DrawStyle = 0
            stmp = "F1"
            OEMX.OEM1PicCALCOLO.CurrentX = pX1
            OEMX.OEM1PicCALCOLO.CurrentY = pY1
            OEMX.OEM1PicCALCOLO.Print stmp
            stmp = "F2"
            OEMX.OEM1PicCALCOLO.CurrentX = pX2
            OEMX.OEM1PicCALCOLO.CurrentY = pY2
            OEMX.OEM1PicCALCOLO.Print stmp
          End If
        i = 0
        stmp = "Lmax=" & frmt(L(1, 1) + L(2, 1), 4) & "mm "
        GoSub ScriviCONTATTO
        stmp = "Leap-Lsap=" & frmt(L(1, 1) / 2 - L(1, Np) / 2 + L(2, 1) / 2 - L(2, Np) / 2, 4) & "mm "
        GoSub ScriviCONTATTO
        stmp = "Lpri=" & frmt((L(1, 0) + L(2, 0)) / 2, 4) & "mm "
        GoSub ScriviCONTATTO
        stmp = "F1 :Leap=" & frmt(L(1, 1), 4) & "mm "
        GoSub ScriviCONTATTO
        stmp = "F2 :Leap=" & frmt(L(2, 1), 4) & "mm "
        GoSub ScriviCONTATTO
        stmp = "F1 :Lsap=" & frmt(L(1, Np), 4) & "mm "
        GoSub ScriviCONTATTO
        stmp = "F2 :Lsap=" & frmt(L(2, Np), 4) & "mm "
        GoSub ScriviCONTATTO
        GoSub FineDocumento
        '
      Case 3    'CORREZIONI
        OEMX.OEM1fraOpzioni.Visible = False
        OEMX.lblINP(0).Visible = True
        OEMX.txtINP(0).Visible = True
        OEMX.lblINP(1).Visible = True
        OEMX.txtINP(1).Visible = True
        GoSub ETICHETTE_GRAFICI
        Fianco = 1
          Xmax = CT(Fianco, 1)
          Ymax = MM(Np)
          Xmin = CT(Fianco, 1)
          Ymin = MM(Np)
          For i = 1 To Np
            If CT(Fianco, i) >= Xmax Then Xmax = CT(Fianco, i)
            If MM(i) >= Ymax Then Ymax = MM(i)
            If CT(Fianco, i) <= Xmin Then Xmin = CT(Fianco, i)
            If MM(i) <= Ymin Then Ymin = MM(i)
          Next i
        Fianco = 2
          For i = 1 To Np
            If CT(Fianco, i) >= Xmax Then Xmax = CT(Fianco, i)
            If MM(i) >= Ymax Then Ymax = MM(i)
            If CT(Fianco, i) <= Xmin Then Xmin = CT(Fianco, i)
            If MM(i) <= Ymin Then Ymin = MM(i)
          Next i
        ScalaX = val(GetInfo("CALCOLO", "ScalaCORREZIONIx", Path_LAVORAZIONE_INI))
        ScalaY = val(GetInfo("CALCOLO", "ScalaCORREZIONIy", Path_LAVORAZIONE_INI))
        OEMX.txtINP(0).Text = frmt(ScalaX, 4)
        OEMX.txtINP(1).Text = frmt(ScalaY, 4)
        For Fianco = 1 To 2
          stmp = "F" & Format$(Fianco, "0")
          For i = 1 To Np - 1
            pX1 = CT(Fianco, i) * ScalaX * (3 - 2 * Fianco) + PicW / 2 - PicW / 4 * (3 - 2 * Fianco)
            pY1 = (MM(i) - (Ymax + Ymin) / 2) * ScalaY + PicH / 2
            pX2 = CT(Fianco, i + 1) * ScalaX * (3 - 2 * Fianco) + PicW / 2 - PicW / 4 * (3 - 2 * Fianco)
            pY2 = (MM(i + 1) - (Ymax + Ymin) / 2) * ScalaY + PicH / 2
            If (SiStampa = 1) Then
              If i = 1 Then
                Printer.CurrentX = PicW / 2 - PicW / 4 * (3 - 2 * Fianco) - Printer.TextWidth(stmp) / 2
                Printer.CurrentY = pY1
                Printer.Print stmp
              End If
              Printer.Line (pX1, pY1)-(pX2, pY2)
              Printer.DrawStyle = 2
              Printer.Line (PicW / 2 - PicW / 4 * (3 - 2 * Fianco), pY2)-(pX2, pY2)
              Printer.DrawStyle = 0
              Printer.Line (PicW / 2 - PicW / 4 * (3 - 2 * Fianco), pY1)-(PicW / 2 - PicW / 4 * (3 - 2 * Fianco), pY2)
            Else
              If i = 1 Then
                OEMX.OEM1PicCALCOLO.CurrentX = PicW / 2 - PicW / 4 * (3 - 2 * Fianco) - OEMX.OEM1PicCALCOLO.TextWidth(stmp) / 2
                OEMX.OEM1PicCALCOLO.CurrentY = pY1
                OEMX.OEM1PicCALCOLO.Print stmp
              End If
              OEMX.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2)
              OEMX.OEM1PicCALCOLO.DrawStyle = 2
              OEMX.OEM1PicCALCOLO.Line (PicW / 2 - PicW / 4 * (3 - 2 * Fianco), pY2)-(pX2, pY2)
              OEMX.OEM1PicCALCOLO.DrawStyle = 0
              OEMX.OEM1PicCALCOLO.Line (PicW / 2 - PicW / 4 * (3 - 2 * Fianco), pY1)-(PicW / 2 - PicW / 4 * (3 - 2 * Fianco), pY2)
            End If
          Next i
        Next Fianco
        If (SiStampa = 1) Then
          stmp = " + "
          Printer.CurrentX = 0
          Printer.CurrentY = 0
          Printer.Print stmp
          stmp = " - "
          Printer.CurrentX = PicW / 2 - Printer.TextWidth(stmp) / 2
          Printer.CurrentY = 0
          Printer.Print stmp
          stmp = " + "
          Printer.CurrentX = PicW - Printer.TextWidth(stmp)
          Printer.CurrentY = 0
          Printer.Print stmp
        End If
        stmp = " + "
        OEMX.OEM1PicCALCOLO.CurrentX = 0
        OEMX.OEM1PicCALCOLO.CurrentY = 0
        OEMX.OEM1PicCALCOLO.Print stmp
        stmp = " - "
        OEMX.OEM1PicCALCOLO.CurrentX = PicW / 2 - OEMX.OEM1PicCALCOLO.TextWidth(stmp) / 2
        OEMX.OEM1PicCALCOLO.CurrentY = 0
        OEMX.OEM1PicCALCOLO.Print stmp
        stmp = " + "
        OEMX.OEM1PicCALCOLO.CurrentX = PicW - OEMX.OEM1PicCALCOLO.TextWidth(stmp)
        OEMX.OEM1PicCALCOLO.CurrentY = 0
        OEMX.OEM1PicCALCOLO.Print stmp
        Call TIF_di_riferimento(OEMX, D2, MM2, ScalaY, Ymax, Ymin, SiStampa) 'MM fine profilo
        Call TIF_di_riferimento(OEMX, dprim, MMp, ScalaY, Ymax, Ymin, SiStampa) 'MM primitivo
        Call TIF_di_riferimento(OEMX, D1, MM1, ScalaY, Ymax, Ymin, SiStampa) 'MM inizio profilo
        Dim Val1 As Double
        Dim Val2 As Double
        For Fianco = 1 To 2
          If Valutazione = 2 Then
            Val1 = IIf(Fianco = 1, LEP("R[270]"), LEP("R[285]"))
            If Val1 > 0 Then
              Val2 = Sqr(Val1 ^ 2 / 4 - Rb ^ 2)
              Call TIF_di_riferimento_Fianco(OEMX, Val1, Val2, ScalaY, Ymax, Ymin, SiStampa, Fianco, Valutazione)
            End If
            Val1 = IIf(Fianco = 1, LEP("R[273]"), LEP("R[288]"))
            If Val1 > 0 Then
              Val2 = Sqr(Val1 ^ 2 / 4 - Rb ^ 2)
              Call TIF_di_riferimento_Fianco(OEMX, Val1, Val2, ScalaY, Ymax, Ymin, SiStampa, Fianco, Valutazione)
            End If
            Val1 = IIf(Fianco = 1, LEP("R[276]"), LEP("R[291]"))
            If Val1 > 0 Then
              Val2 = Sqr(Val1 ^ 2 / 4 - Rb ^ 2)
              Call TIF_di_riferimento_Fianco(OEMX, Val1, Val2, ScalaY, Ymax, Ymin, SiStampa, Fianco, Valutazione)
            End If
          Else
            Val2 = IIf(Fianco = 1, LEP("R[270]"), LEP("R[285]"))
            If Val2 > 0 Then
              Val1 = 2 * Sqr(Val2 ^ 2 + Rb ^ 2)
              Call TIF_di_riferimento_Fianco(OEMX, Val1, Val2, ScalaY, Ymax, Ymin, SiStampa, Fianco, Valutazione)
            End If
            Val2 = IIf(Fianco = 1, LEP("R[273]"), LEP("R[288]"))
            If Val2 > 0 Then
              Val1 = 2 * Sqr(Val2 ^ 2 + Rb ^ 2)
              Call TIF_di_riferimento_Fianco(OEMX, Val1, Val2, ScalaY, Ymax, Ymin, SiStampa, Fianco, Valutazione)
            End If
            Val2 = IIf(Fianco = 1, LEP("R[276]"), LEP("R[291]"))
            If Val2 > 0 Then
              Val1 = 2 * Sqr(Val2 ^ 2 + Rb ^ 2)
              Call TIF_di_riferimento_Fianco(OEMX, Val1, Val2, ScalaY, Ymax, Ymin, SiStampa, Fianco, Valutazione)
            End If
          End If
        Next Fianco
        GoSub FineDocumento
        '
      Case 4    'INFORMAZIONI1
        OEMX.OEM1fraOpzioni.Visible = False
        GoSub ETICHETTE_GRAFICI
        OEMX.lblINP(0).Visible = True
        OEMX.txtINP(0).Visible = True
        OEMX.lblINP(0).Caption = "Dem"
        OEMX.txtINP(0).Text = frmt(DEm, 4)
        'OEMX.lblINP(1).Visible = True
        'OEMX.txtINP(1).Visible = True
        'OEMX.lblINP(1).Caption = "Im"
        i = 0
          pY2 = OEMX.OEM1PicCALCOLO.TextHeight("X")
          pX1 = 1
          pY1 = 1
          'stmp = "Diametro di base"
          ret = LoadString(g_hLanguageLibHandle, 1201, Atmp, 255)
          If ret > 0 Then stmp = Left$(Atmp, ret)
          stmp = stmp & " = "
          stmp = stmp & frmt(2 * Rb, 4)
          GoSub Scrivi
          'stmp = "Elica di base"
          ret = LoadString(g_hLanguageLibHandle, 1202, Atmp, 255)
          If ret > 0 Then stmp = Left$(Atmp, ret)
          stmp = stmp & " = "
          stmp = stmp & frmt(Betab * 180 / PG, 4)
          GoSub Scrivi
          'stmp = "Diametro primitivo"
          ret = LoadString(g_hLanguageLibHandle, 1203, Atmp, 255)
          If ret > 0 Then stmp = Left$(Atmp, ret)
          stmp = stmp & " = "
          stmp = stmp & frmt(dprim, 4)
          GoSub Scrivi
          'stmp = "Diametro esterno mola"
          ret = LoadString(g_hLanguageLibHandle, 1200, Atmp, 255)
          If ret > 0 Then stmp = Left$(Atmp, ret)
          stmp = stmp & " (Dem)"
          stmp = stmp & " = "
          stmp = stmp & frmt(DEm, 4)
          GoSub Scrivi
          'SON normale
          stmp = LNP("R[46]") ' OEMX.List1(0).List(8)
          stmp = stmp & " = "
          stmp = stmp & frmt(SONdonnee, 4)
          GoSub Scrivi
          'SAP
          stmp = LNP("R[44]") ' OEMX.List1(0).List(4)
          stmp = stmp & " = "
          stmp = stmp & frmt(D2, 4)
          GoSub Scrivi
          'EAP
          stmp = LNP("R[43]") ' OEMX.List1(0).List(5)
          stmp = stmp & " = "
          stmp = stmp & frmt(D1, 4)
          GoSub Scrivi
          'Raggio di fondo
          stmp = LNP("R[52]") ' OEMX.List1(0).List(12)
          stmp = stmp & " = "
          stmp = stmp & frmt(RF, 4)
          GoSub Scrivi
          'Diametro interno
          stmp = LNP("R[45]") ' OEMX.List1(0).List(7)
          stmp = stmp & " = "
          stmp = stmp & frmt(DIint, 4)
          GoSub Scrivi
          'stmp = GetInfo("INFORMAZIONI", "Alfan", PathFILEINI)
          'stmp = GetInfo("INFORMAZIONI", "BETA", PathFILEINI)
          'stmp = GetInfo("INFORMAZIONI", "Modulo", PathFILEINI)
          'stmp = GetInfo("INFORMAZIONI", "N denti", PathFILEINI)
          'stmp = GetInfo("INFORMAZIONI", "SON n", PathFILEINI)
          'stmp = GetInfo("INFORMAZIONI", "D base", PathFILEINI)
          'stmp = GetInfo("INFORMAZIONI", "D prim", PathFILEINI)
          'stmp = GetInfo("INFORMAZIONI", "EAP", PathFILEINI)
          'stmp = GetInfo("INFORMAZIONI", "SAP", PathFILEINI)
          'stmp = GetInfo("INFORMAZIONI", "Rf", PathFILEINI)
          'stmp = GetInfo("INFORMAZIONI", "D int", PathFILEINI)
          'stmp = GetInfo("INFORMAZIONI", "L f", PathFILEINI)
          'stmp = GetInfo("INFORMAZIONI", "SWEAP", PathFILEINI)
          'stmp = GetInfo("INFORMAZIONI", "SWSAP", PathFILEINI)
          'stmp = GetInfo("INFORMAZIONI", "ANGM", PathFILEINI)
          'stmp = GetInfo("INFORMAZIONI", "R bom", PathFILEINI)
          'stmp = GetInfo("INFORMAZIONI", "Cav", PathFILEINI)
          'stmp = GetInfo("INFORMAZIONI", "HU1", PathFILEINI)
          'stmp = GetInfo("INFORMAZIONI", "HU2", PathFILEINI)
          'stmp = GetInfo("INFORMAZIONI", "L pr", PathFILEINI)
          'stmp = GetInfo("INFORMAZIONI", "Ang min mola", PathFILEINI)
          'stmp = GetInfo("INFORMAZIONI", "Interasse", PathFILEINI)
        '
      Case 5    'INFORMAZIONI2
        OEMX.OEM1fraOpzioni.Visible = False
        'CARATTERE DI STAMPA TESTO
        TextFont = "Courier New"
        If (SiStampa = 1) Then
          For i = 1 To 2
            Printer.FontName = TextFont
            Printer.FontSize = 8
            Printer.FontBold = False
          Next i
        Else
          For i = 1 To 2
            OEMX.OEM1txtHELP.FontName = TextFont
            OEMX.OEM1txtHELP.FontSize = 10
            OEMX.OEM1txtHELP.FontBold = False
            OEMX.OEM1txtHELP.BackColor = &H0&
            OEMX.OEM1txtHELP.ForeColor = &HFFFFFF
          Next i
        End If
        OEMX.lblINP(0).Visible = False
        OEMX.txtINP(0).Visible = False
        OEMX.lblINP(1).Visible = False
        OEMX.txtINP(1).Visible = False
        GoSub ETICHETTE_GRAFICI
        OEMX.OEM1txtHELP.Visible = True
        OEMX.OEM1txtHELP.Move OEMX.OEM1PicCALCOLO.Left, OEMX.OEM1PicCALCOLO.Top, OEMX.OEM1PicCALCOLO.Width, OEMX.OEM1PicCALCOLO.Height + OEMX.OEM1Combo1.Height
        Open g_chOemPATH & "\DATI.TEO" For Input As 1
          OEMX.OEM1txtHELP.Text = ""
          OEMX.OEM1txtHELP.Text = Input$(LOF(1), 1)
        Close #1
        If (SiStampa = 1) Then
          Open g_chOemPATH & "\DATI.TEO" For Input As 1
            While Not EOF(1)
              Line Input #1, stmp
              Printer.Print stmp
              WRITE_DIALOG stmp
            Wend
          Close #1
          Printer.EndDoc
          WRITE_DIALOG OEMX.OEM1Combo1.Caption & " --> " & Printer.DeviceName & " OK!!"
        End If
        '
      Case 6    'VERIFICA PROFILO
        OEMX.lblINP(0).Visible = False
        OEMX.txtINP(0).Visible = False
        OEMX.lblINP(1).Visible = False
        OEMX.txtINP(1).Visible = False
        GoSub ETICHETTE_GRAFICI
        Call VERIFICA_PROFILO_INGRANAGGIO_INTERNO(OEMX, SiStampa)
        GoSub FineDocumento
        '
    End Select
    '
  End If
  '
  Dim SensoElica As Integer
  '
  'CalcoloEsterni.MolaX = x
  'CalcoloEsterni.MolaY = Y
  'CalcoloEsterni.IngrX = XD
  'CalcoloEsterni.IngrY = YD
  CalcoloEsterni.DCont = D
  CalcoloEsterni.LCont = L
  SensoElica = LEP("iSensoElica")
  For i = 1 To UBound(CalcoloEsterni.LCont, 2)
    CalcoloEsterni.LCont(1, i) = -SensoElica * CalcoloEsterni.LCont(1, i)
    CalcoloEsterni.LCont(2, i) = -SensoElica * CalcoloEsterni.LCont(2, i)
  Next i
  CalcoloEsterni.DB = 2 * Rb
  CalcoloEsterni.DP = dprim
  CalcoloEsterni.DE = LEP("R[38]")
  CalcoloEsterni.DI = LEP("R[45]")
  CalcoloEsterni.DIOtt = Ri * 2
  CalcoloEsterni.Np = Np
  '
  If FreeFile > 1 Then Close
  '
Exit Sub

FineDocumento:
  'Dimensione dei caratteri
  If (SiStampa = 1) Then
    For i = 1 To 2
      Printer.FontName = TextFont
      Printer.FontSize = 14
      Printer.FontBold = True
    Next i
    'Logo
    'Printer.PaintPicture OEMX.PicLogo, 0, PicH, 20, 20
    'Nome del documento
    stmp = Date & " " & Time & "                  " & OEMX.OEM1Combo1.Caption
    Printer.CurrentX = 0 + 25
    Printer.CurrentY = PicH
    Printer.Print stmp
    'Scala X
    If OEMX.lblINP(0).Visible = True Then
      stmp = OEMX.lblINP(0).Caption & ": x" & OEMX.txtINP(0).Text
      Printer.CurrentX = 0 + 25
      Printer.CurrentY = PicH + 10
      Printer.Print stmp
    End If
    'Scala Y
    If OEMX.lblINP(1).Visible = True Then
      stmp = OEMX.lblINP(1).Caption & ": x" & OEMX.txtINP(1).Text
      Printer.CurrentX = 0 + PicW / 2
      Printer.CurrentY = PicH + 10
      Printer.Print stmp
    End If
    '
    OEMX.SuOEM1.Visible = True
    Call OEMX.SuOEM1.STAMPA(False, , , , 130)
    OEMX.SuOEM1.Visible = False
    '
    Printer.EndDoc
    WRITE_DIALOG OEMX.OEM1Combo1.Caption & " --> " & Printer.DeviceName & " OK!!"
  End If
Return
'
ETICHETTE_GRAFICI:
  'NASCONDO LA CASELLA DI TESTO
  OEMX.OEM1txtHELP.Visible = False
  'VISUALIZZO LE SCALE
  i = LoadString(g_hLanguageLibHandle, 1000, Atmp, 255)
  If i > 0 Then OEMX.lblINP(0).Caption = Left$(Atmp, i)
  i = LoadString(g_hLanguageLibHandle, 1001, Atmp, 255)
  If i > 0 Then OEMX.lblINP(1).Caption = Left$(Atmp, i)
  If (SiStampa = 1) Then
    Printer.ScaleMode = 6
    WRITE_DIALOG OEMX.OEM1Combo1.Caption & " --> " & Printer.DeviceName
  Else
    OEMX.OEM1PicCALCOLO.BackColor = &HFFFFFF
    OEMX.OEM1PicCALCOLO.Cls
  End If
Return

LineaF1:
  pX1 = (D(i) - Xmax) * ScalaX + PicW / 2
  pY1 = -L(1, i) * ScalaY * BETAseg + PicH / 2
  pX2 = (D(i) - Xmax) * ScalaX + PicW / 2
  pY2 = 0 + PicH / 2
  Call LINEA_FIANCO(OEMX, 1, SiStampa, stmp, pX1, pY1, pX2, pY2)
Return
'
LineaF2:
  pX1 = -(D(i) - Xmax) * ScalaX + PicW / 2
  pY1 = L(2, i) * ScalaY * BETAseg + PicH / 2
  pX2 = -(D(i) - Xmax) * ScalaX + PicW / 2
  pY2 = 0 + PicH / 2
  Call LINEA_FIANCO(OEMX, 2, SiStampa, stmp, pX1, pY1, pX2, pY2)
Return

Scrivi:
  'Visualizza nella finestra di calcolo
  OEMX.OEM1PicCALCOLO.CurrentX = pX1
  OEMX.OEM1PicCALCOLO.CurrentY = pY1 + i * pY2
  OEMX.OEM1PicCALCOLO.Print stmp
  If (SiStampa = 1) Then
    'Invia alla stampante
    Printer.CurrentX = pX1
    Printer.CurrentY = pY1 + i * pY2
   Printer.Print stmp
  End If
  i = i + 1
Return

ScriviCONTATTO:
  If (SiStampa = 1) Then
    If BETAseg > 0 Then
      Printer.CurrentX = PicW - Printer.TextWidth(stmp)
    Else
      Printer.CurrentX = 0
    End If
    Printer.CurrentY = Printer.TextHeight(stmp) * i
    Printer.Print stmp
  Else
    If BETAseg > 0 Then
      OEMX.OEM1PicCALCOLO.CurrentX = PicW - OEMX.OEM1PicCALCOLO.TextWidth(stmp)
    Else
      OEMX.OEM1PicCALCOLO.CurrentX = 0
    End If
    OEMX.OEM1PicCALCOLO.CurrentY = OEMX.OEM1PicCALCOLO.TextHeight(stmp) * i
    OEMX.OEM1PicCALCOLO.Print stmp
  End If
  i = i + 1
Return

2100 'OUTPUT COSTANTI
  stmp = ""
  stmp = stmp & "Diam base =" & frmt(D3, 4) & Chr(13)
  stmp = stmp & "Diam prim =" & frmt(D5, 4) & Chr(13)
  stmp = stmp & "Elica base=" & frmt(Betab * 180 / PG, 4) & Chr(13)
  StopRegieEvents
  MsgBox stmp, 48, "WARNING"
  ResumeRegieEvents
Return

7000 'CALCOLO T0, FI0
  RX = Sqr(MM(i) ^ 2 + Rb ^ 2)
  CosA = Rb / RX
  SinA = Sqr(1 - CosA ^ 2)
  TanA = SinA / CosA
  Ax = Atn(TanA)
  InvAx = TanA - Ax
  Sx = (SON / 2 / R + F3 - InvAx) * 2 * RX
  VX = 2 * PG * RX / Z - Sx
  '7080 T0 = PG / 2 + SX / 2 / RX  'MODIFICA PER INTERNI ( + SX )
7080 T0 = PG / 2 + Sx / 2 / RX + Atn(Corr / RX)
  FI0 = T0 - Ax: If T0 < 0 Then FI0 = FI0 - 2 * PG
7092 If FI0 > PG Then FI0 = FI0 - PG * 2: GoTo 7092
  If FI0 < -PG Then FI0 = FI0 + PG * 2: GoTo 7092
Return
'
10000 'PUREA
  DP = 2 * PG
  PAS = Tan(PG / 2 - BetaR) * 2 * PG * R
  P = PAS
  SigmaG = 90 - BetaR * 180 / PG
  SIGMA = PG * SigmaG / 180
  CS = Cos(SIGMA)
  SS = Sin(SIGMA)
  Rmin = P / (DP * Tan(SIGMA))
  Tp = P / DP
  DRM = IL * Rmin
  RMPD = Rmin + IL
  YD(i) = RX * Sin(T0)
  XD(i) = RX * Cos(T0)
  THIN = XD(i) * Sin(BetaR) / PAS * 2 * PG
  SF0 = Sin(FI0)
  CF0 = Cos(FI0)
  ST0 = Sin(T0)
  CT0 = Cos(T0)
  ST0S = Sin(T0S)
  CT0S = Cos(T0S)
  T0PF = T0S + FI0
  ST0F0 = Sin(T0PF)
  CT0F0 = Cos(T0PF)
  TMF = T0 - FI0
  CTF0 = Cos(TMF)
  RQC = RX * RX * CTF0
  PSUDP2 = Tp * Tp
  AA = RQC * ST0 + (2 * PSUDP2 + DRM) * SF0
  BB = (DRM + PSUDP2) * CF0 + RQC * CT0
  cc = 2 * (-DRM * SF0 - RQC * ST0 + RX * RMPD * CTF0)
  brid = BB / AA: Crid = cc / AA
  RAD = brid * brid - Crid
  RAD = Sqr(RAD)
  T1 = brid + RAD
  T2 = brid - RAD
  Ti = T2
  If Abs(T2) > Abs(T1) Then Ti = T1
  If T0 < 0 Then Ti = T1
  While Ti >= PG: Ti = PG * 0.9: Wend
  'TETA --> TS
  TOL = 0.01 * PG / 180
  TSI = Ti
  j = 1
  While j < 3
    F = RQC * Sin(T0 + TSI) + DRM * Sin(FI0 + TSI) + PSUDP2 * TSI * Cos(FI0 + TSI) - RX * RMPD * CTF0
    FP = RQC * Cos(T0 + TSI) + DRM * Cos(FI0 + TSI) + PSUDP2 * Cos(FI0 + TSI) - PSUDP2 * TSI * Sin(FI0 + TSI)
    TSIa = TSI - F / FP
    j = j + 1
    TSI = TSIa
  Wend
  Ti = TSIa
  TH = Ti
  TT = T0 + Ti
  CTT = Cos(TT)
  STT = Sin(TT)
  XS = RX * CTT
  Ys = RX * STT
  Zs = Tp * Ti
  'CALCOLO TANGENTE AL PROFILO MOLA IN TETAS
  TSPF = Ti + FI0
  Ap = Tan(TSPF) / XS
  Bp = -1 / XS
  Cp = (1 + Ys * Tan(TSPF) / XS) / Tp
  Lp = Tan(SIGMA)
  ArgSeno = (Ap * Lp + Cp) / (Sqr(Ap * Ap + Bp * Bp + Cp * Cp) * Sqr(Lp * Lp + 1))
  SINa2 = ArgSeno * ArgSeno
  TanA = Sqr(SINa2 / (1 - SINa2))
  Tpromola = Atn(TanA)
  Tpromolag = 90 - Tpromola / PG * 180
  B(i) = Tpromolag
  Uprof = XS * CS - Zs * SS: Vprof = Ys - IL
  Wprof = Zs * CS + XS * SS
  Rprof = Sqr(Uprof ^ 2 + Vprof ^ 2)
  X(i) = -Wprof       'MODIFICA PER INTERNI X(I)=-WPROF
  Y(i) = Rprof
Return
'
19000 'MOLA --> INGRANAGGIO (sez.trasversale)
Dim ENTRX As Double
Dim INF As Double
Dim APF As Double
Dim ALFAs  As Double

  'INPUT  X(JJ),Y(JJ),ALFAM(JJ)
  'OUTPUT  XD(JJ),YD(JJ)
  'pg = 3.141593
  ENTRX = IL
  INF = BetaR
  EXF = -X(jj)
  RXF = Y(jj)
  APFD = AlfaM(jj) * 180 / PG
  If EXF < 0 Then APFD = -APFD
  If APFD < 0 Then APF = (180 + APFD) * PG / 180 Else APF = (APFD) * PG / 180
  GoSub 19200
  XD(jj) = Rayon * Sin(ALFAs)
  YD(jj) = Rayon * Cos(ALFAs)
  Print #1, Left$(Format$(jj, "##0") & String(5, " "), 5);
  Print #1, String(10, " ");
  Print #1, String(10, " ");
  Print #1, String(10, " ");
  Print #1, Left$(frmt(X(jj), 4) & String(10, " "), 10);
  Print #1, Left$(frmt(Y(jj), 4) & String(10, " "), 10);
  Print #1, Left$(frmt(AlfaM(jj) * 180 / PG, 4) & String(10, " "), 10)
Return
'
19200 'CALCUL PIECE APRES FRAISE
Dim SENS As Integer
Dim v7 As Double
Dim RXV As Double
Dim Ang As Double
Dim ang1 As Double
Dim YAPP  As Double
Dim XAPP  As Double
Dim BETAi As Double

  SENS = 1: If EXF < 0 Then SENS = -1
  EXF = EXF * SENS
  APF = APF * SENS
  v7 = PAS / 2 / PG
  GoSub 20500:  Rayon = RXV: ALFAs = Ang * SENS:  ang1 = Ang
  RXF = RXF + 0.01: EXF = EXF - 0.01 * Tan(APF): GoSub 20500: Y1 = YAPP: X1 = XAPP
  RXF = RXF - 0.02: EXF = EXF + 0.02 * Tan(APF): GoSub 20500
  RXF = RXF + 0.01: EXF = EXF - 0.01 * Tan(APF)
  BETAi = (Atn((X1 - XAPP) / (YAPP - Y1)) - ang1) * SENS
  APF = APF * SENS: EXF = EXF * SENS
Return
'
20500 '***************
Dim V12 As Double
Dim V13 As Double
Dim V14 As Double
Dim V15 As Double
Dim V16 As Double
Dim VPB As Double
Dim EXV As Double

  V12 = -Tan(APF) * (ENTRX * Sin(INF) + v7 * Cos(INF))
  V13 = v7 * Sin(INF) - ENTRX * Cos(INF)
  V14 = Cos(INF) * ((-Tan(APF) * EXF) + RXF)
  V15 = (V14 ^ 2) + (V12 ^ 2) - (V13 ^ 2)
  If V15 > 0 Then
    V15 = Sqr(V15)
    V16 = -(V15 + V12) / (V13 - V14)
    V15 = (V15 - V12) / (V13 - V14)
    VPB = V16: If Abs(V15) < Abs(V16) Then VPB = V15
    V12 = 2 * Atn(VPB)
    V13 = (RXF * Sin(INF) * Sin(V12)) + EXF * Cos(INF)
    V13 = Atn(V13 / ((RXF * Cos(V12)) - ENTRX))
   RXV = (ENTRX - RXF * Cos(V12)) / Cos(V13)
    EXV = EXF * Sin(INF) - v7 * V13 - RXF * Sin(V12) * Cos(INF)
    Ang = 2 * PG * EXV / PAS
    YAPP = RXV * Cos(Ang): XAPP = -RXV * Sin(Ang)
  Else
    MsgBox "CALCOLO IMPOSSIBILE", 48, "ERRORE"
  End If
Return

CALCKN:
Dim E260 As Double
Dim E261 As Double
Dim E262 As Double
Dim E263 As Double
Dim E264 As Double
Dim E265 As Double
Dim E266 As Double
Dim E267 As Double
Dim E268 As Double
Dim E269 As Double
Dim E19 As Integer
Dim E17 As Integer
' 13.4.98
' Calcola correzzioni prof. K
' Fianco 1
E265 = 0
E19 = 1
For i = 1 To Np
CB(i) = 0
Next i
MM0 = MM1
' Controllo TIF1
If Tif1 = 0 Then GoTo CALCK2
If Tif1 < MM0 Then GoTo CALCK2
If Tif1 > MM2 Then Tif1 = MM2
E260 = Int((Tif1 - MM0 + DIPU / 2) / DIPU) * DIPU
E261 = E260 / DIPU + 1
If Bombatura(Fianco, 1) <> 0 Then E262 = (E260 * E260 / 4 + Bombatura(Fianco, 1) * Bombatura(Fianco, 1)) / 2 / Bombatura(Fianco, 1) ' RB ( - , + )
E263 = 0
E17 = 0
ALCK1:
E263 = E263 + 1
E17 = E17 + 1
CB(E17) = Rastremazione(Fianco, 1) * (E263 - 1) / (E261 - 1)
If Bombatura(Fianco, 1) = 0 Then GoTo NOB1F1
E264 = Abs(E260 / 2 - (E263 - 1) * DIPU)
CB(E17) = CB(E17) + Bombatura(Fianco, 1) + Sqr(E262 * E262 - E264 * E264) * E262 / Abs(E262) - E262
NOB1F1:
If E263 < E261 Then GoTo ALCK1
E265 = CB(E17)
If Tif1 = MM2 Then GoTo CALCK2
' Tratto 2
E19 = E261
' Tratto 2:TIF2,RASTR2,BOMB2
23000:
If Tif2 = 0 Then GoTo AZCK
If Tif2 <= Tif1 Then GoTo AZCK
If Tif2 > MM2 Then Tif2 = MM2
E266 = Int((Tif2 - MM0 - E260 + DIPU / 2) / DIPU) * DIPU
E267 = E266 / DIPU + 1
If Bombatura(Fianco, 2) <> 0 Then E262 = (E266 * E266 / 4 + Bombatura(Fianco, 2) * Bombatura(Fianco, 2)) / 2 / Bombatura(Fianco, 2)
E263 = 1
ALCK2:
E263 = E263 + 1
E17 = E17 + 1
CB(E17) = Rastremazione(Fianco, 2) * (E263 - 1) / (E267 - 1) + E265
If Bombatura(Fianco, 2) = 0 Then GoTo NOB2F1
E264 = Abs(E266 / 2 - (E263 - 1) * DIPU)
CB(E17) = CB(E17) + Bombatura(Fianco, 2) + Sqr(E262 * E262 - E264 * E264) * E262 / Abs(E262) - E262
NOB2F1:
If E263 < E267 Then GoTo ALCK2
E265 = CB(E17)
E19 = E17 - 1
If Tif3 = 0 Then GoTo AZCK
If Tif3 <= Tif2 Then Stop   ' ERRORE se TIF3 <= TIF2
If Tif3 > MM2 Then Tif3 = MM2
E268 = Int((Tif3 - MM0 - E260 - E266 + DIPU / 2) / DIPU) * DIPU
E269 = E268 / DIPU + 1
If Bombatura(Fianco, 3) <> 0 Then E262 = (E268 * E268 / 4 + Bombatura(Fianco, 3) * Bombatura(Fianco, 3)) / 2 / Bombatura(Fianco, 3)
E263 = 1
ALCK3:
E263 = E263 + 1
E17 = E17 + 1
CB(E17) = Rastremazione(Fianco, 3) * (E263 - 1) / (E269 - 1) + E265
If Bombatura(Fianco, 3) = 0 Then GoTo NOB3F1
E264 = Abs(E268 / 2 - (E263 - 1) * DIPU)
CB(E17) = CB(E17) + Bombatura(Fianco, 3) + Sqr(E262 * E262 - E264 * E264) * E262 / Abs(E262) - E262
NOB3F1:
If E263 < E269 Then GoTo ALCK3
E19 = E17 - 1
E265 = CB(E17)
AZCK:
E17 = 1 + E19
E19 = Np - E19
If E19 < 1 Then GoTo CALCK2
For i = 1 To E19
CB(E17) = E265
E17 = E17 + 1
Next i
CALCK2:
MMp = Sqr(dprim ^ 2 / 4 - Rb * Rb)
E261 = Int((MMp - MM0 + 0.5) / DIPU)
If E261 < 0 Then E261 = 0
If E261 >= Np Then E261 = Np - 1
E262 = CB(E261)
E17 = 1
For i = 1 To Np
CB(E17) = CB(E17) - E262
E17 = E17 + 1
Next i
Return
'
errCALCOLO_INGRANAGGIO_INTERNO:
  WRITE_DIALOG "Sub CALCOLO_INGRANAGGIO_INTERNO: ERROR -> " & Err
  If FreeFile > 1 Then Close
  Err = 0
  'Resume Next
  Exit Sub

End Sub

Function Ainv(ByVal X As Double) As Double
'
Dim Ymini   As Double
Dim Ymaxi   As Double
Dim Ymoyen  As Double
Dim i       As Integer
'
On Error Resume Next
  '
  Ymini = 0: Ymaxi = PG / 2
  For i = 1 To 25
    Ymoyen = (Ymini + Ymaxi) / 2: X1 = inv(Ymoyen)
    If (X1 < X) Then Ymini = Ymoyen Else Ymaxi = Ymoyen
  Next i
  Ainv = Ymoyen
  '
End Function

Sub QUOTA_RULLI(ByVal Dpige As Double, ByVal Z As Integer, ByVal DB As Double, ByVal HELB As Double, Eb As Double, Kpig As Double, ByVal Code$)
'
Dim W As Double
Dim X As Double
Dim A As Double
'
On Error Resume Next
  '
  ' Code$ = "E": Calcul de Eb
  ' Code$ = "K": Calcul de Kpg
  If Int(Z / 2) = Z / 2 Then W = DB Else W = Cos(PG / (2 * Z)) * DB
  '
  If (Dpige = 0) Then
    Kpig = 0
    Exit Sub
  End If
  '
  If (Kpig = 0) And (Code$ = "E") Then
    Exit Sub
  End If
  '
  If (Code$ = "K") Then
    X = (Eb - Dpige / Cos(HELB)) / DB
    Kpig = W / Cos(Ainv(X)) - Dpige
  Else
    A = FnARC(W / (Kpig + Dpige))
    Eb = DB * inv(A) + Dpige / Cos(HELB)
  End If
  '
End Sub

Function inv(ByVal A As Double) As Double
'
On Error Resume Next
  '
  inv = Tan(A) - A
  '
End Function

Sub Piece1(Rayon As Double, ALFA As Double, Beta As Double, Ent As Double, INC As Double, PAS As Double, X As Double, Y As Double, gama2 As Double, Ltang As Double)
'
Dim SENS      As Integer
Dim v7        As Double
Dim V12       As Double
Dim V13       As Double
Dim V14       As Double
Dim V15       As Double
Dim V16       As Double
Dim V17       As Double
Dim VPB       As Double
Dim AngAxeRf  As Double
Dim YobtL     As Double
Dim AngL      As Double
Dim B         As Double
Dim C         As Double
'
On Error GoTo errPiece1
  '
  'Entrees Constantes : ENT   , INC   , PAS
  'Entrees variables  : X     ,  Y    , GAMA2
  'Sorties            : RAYON , ALFA  , BETA  , LTANG
  '
  SENS = 1: If gama2 < 0 Then SENS = -1
  X = X * SENS: gama2 = gama2 * SENS
  v7 = PAS / 2 / PG
  V12 = -Tan(gama2) * (Ent * Sin(INC) + v7 * Cos(INC))
  V13 = v7 * Sin(INC) - Ent * Cos(INC)
  V14 = Cos(INC) * (Tan(gama2) * X - Y)
  V15 = (V14 ^ 2) + (V12 ^ 2) - (V13 ^ 2)
  If (V15 > 0) Then
    V15 = Sqr(V15)
    V16 = -(V15 + V12) / (V13 - V14)
    V15 = (V15 - V12) / (V13 - V14)
    VPB = V16: If Abs(V15) < Abs(V16) Then VPB = V15
    AngAxeRf = 2 * Atn(VPB)
    ' AngAxeRf : Angle Axe fraise / Rayon Fraise a la distance consideree
    Ltang = -X * Sin(INC) + Y * Sin(AngAxeRf) * Cos(INC)
    ' Ltang : Distance axe meule / plan tangent ( type Programme 'JPG' )
    V13 = (-Y * Sin(INC) * Sin(AngAxeRf)) - X * Cos(INC)
    ' ou V13 = - X / COS(Inc) - Ltang * TAN(Inc)
    YobtL = Ent + Y * Cos(AngAxeRf)
    AngL = Atn(V13 / -YobtL)
    Rayon = YobtL / Cos(AngL)
    ALFA = (AngL - 2 * PG * Ltang / PAS) * SENS
    B = 1 / Tan(gama2) + Sin(AngAxeRf) * Tan(INC)
    C = Cos(AngAxeRf) / B / Cos(INC)
    Beta = (Atn(C) + AngL) * SENS
    X = X * SENS
    gama2 = gama2 * SENS
  Else
    MsgBox "CALCOLO IMPOSSIBILE", 48, "ERROR"
  End If
  '
Exit Sub

errPiece1:
  WRITE_DIALOG "Sub Piece1: ERROR -> " & Err
  Resume Next

End Sub

Sub VERIFICA_PROFILO_INGRANAGGIO_INTERNO(NomeForm As Form, ByVal SiStampa As Integer)
'
Dim Eb        As Double
Dim D3        As Double
Dim CS2       As Double
Dim DeltaAng  As Double
'
Const DF = 5
Const AF = 15
Const NumeroRette = 15
'
Dim i As Integer
Dim Ang As Single
Dim Qmola As Double
'
Dim CosA As Double
Dim SinA As Double
Dim TanA As Double
Dim Ax As Double
Dim InvAx As Double
Dim Sx As Double
Dim VX As Double
'
Dim Xpr     As Double
Dim Ypr     As Double
Dim X0      As Double
Dim Y0      As Double
Dim T0      As Double
Dim SONt    As Double
Dim SONs    As Double
Dim SONd    As Double
Dim SONmola As Double
'
Dim RX    As Double
Dim NpD   As Integer
Dim NpS   As Integer
Dim Rayon As Double
Dim Alfa1 As Double
Dim beta1 As Double
Dim Ent   As Double
Dim INC   As Double
Dim PAS   As Double
Dim EXF   As Double
Dim RXF   As Double
Dim APFD  As Double
Dim Ltang As Double
Dim DIAM  As Double
'
Dim jSX As Integer
Dim jDX As Integer
'
Dim ERM As Double
Dim INDICE1 As Integer
Dim INDICE2 As Integer
'
Dim ALFAt As Double
Dim BETAt As Double
Dim T1    As Double
Dim CS1   As Double
Dim F3    As Double
Dim Betab As Double
Dim PB    As Double
Dim DB    As Double
Dim iB    As Double
Dim Sb    As Double
Dim MMp   As Double
Dim MM1   As Double
Dim MM2   As Double
Dim Np    As Integer
Dim j     As Integer
'
'Dim Raggio As Double
'Dim ANGOLO As Double
'
Dim AlfaM(999) As Double
Dim Xm(999)    As Double
Dim Ym(999)    As Double
'
Dim XSs(999) As Double
Dim YSs(999) As Double
Dim XDd(999) As Double
Dim YDd(999) As Double
Dim LS(999)  As Double
Dim MS(999)  As Double
Dim LD(999)  As Double
Dim MD(999)  As Double

Dim k1 As Double
Dim k2 As Double
Dim k3 As Double
Dim k4 As Double
Dim K5 As Double
'
Dim AlfaD        As Double
Dim BetaD        As Double
Dim AM           As Double
Dim Z            As Double
Dim DEm          As Double
Dim D1           As Double
Dim D2           As Double
Dim SONdonnee    As Double
Dim Q            As Double
Dim DR           As Double
Dim RF           As Double
Dim DIint        As Double
Dim Valutazione  As Integer
Dim NPR1         As Integer
Dim DIPU         As Double
Dim ALFAn        As Double
Dim BetaR        As Double
Dim IL           As Double
Dim R            As Double
Dim dprim        As Double
Dim D5           As Double
Dim SONn         As Double
Dim Rb           As Double
Dim Fianco       As Integer
Dim stmp         As String
Dim TextFont     As String
Dim INm          As Double
Dim CorINT       As Double
Dim CorINCL      As Double
Dim DeltaX       As Double
Dim ConCorr      As Double
Dim SAP          As Double
Dim EAP          As Double
Dim DPG          As Double
Dim pX1          As Single
Dim pY1          As Single
Dim pX2          As Single
Dim pY2          As Single
Dim Altezza      As Single
Dim Larghezza    As Single
Dim ScalaX       As Single
Dim ScalaY       As Single
Dim Xmax         As Single
Dim Ymax         As Single
Dim Xmin         As Single
Dim Ymin         As Single
Dim PicH         As Single
Dim PicW         As Single
Dim da           As Double
Dim Risoluzione  As Double
Dim CorrSpessore As Double
'
On Error GoTo errVERIFICA_PROFILO_INGRANAGGIO_INTERNO
  '
  For i = 0 To 4
    NomeForm.txtINP(i).Visible = False
    NomeForm.lblINP(i).Visible = False
  Next i
  '
  If (SiStampa = 1) Then
    Printer.ScaleMode = 6
    WRITE_DIALOG NomeForm.OEM1Combo1.Text & " --> " & Printer.DeviceName
  Else
    NomeForm.OEM1PicCALCOLO.Cls
  End If
  '
  If NomeForm.txtHELP.Visible = True Then NomeForm.txtHELP.Visible = False
  '
  NomeForm.OEM1chkINPUT(0).Visible = False
  NomeForm.OEM1chkINPUT(1).Visible = False
  '
  NomeForm.OEM1lblOX.Visible = False
  NomeForm.OEM1lblOY.Visible = False
  NomeForm.OEM1txtOX.Visible = False
  NomeForm.OEM1txtOY.Visible = False
  '
  NomeForm.OEM1Command2.Visible = True
  NomeForm.OEM1Label2.Visible = True
  '
  NomeForm.lblINP(0).Visible = True
  NomeForm.lblINP(1).Visible = True
  NomeForm.lblINP(3).Visible = True
  NomeForm.lblINP(4).Visible = True
  '
  NomeForm.lblINP(3).Caption = "RES"
  NomeForm.lblINP(4).Caption = "DSP"
  '
  NomeForm.txtINP(0).Visible = True
  NomeForm.txtINP(1).Visible = True
  NomeForm.txtINP(3).Visible = True
  NomeForm.txtINP(4).Visible = True
  '
  ScalaX = val(GetInfo("CALCOLO", "ScalaCORREZIONIx", Path_LAVORAZIONE_INI))
  ScalaY = val(GetInfo("CALCOLO", "ScalaCORREZIONIy", Path_LAVORAZIONE_INI))
  Risoluzione = val(GetInfo("CALCOLO", "Risoluzione", Path_LAVORAZIONE_INI))
  CorrSpessore = val(GetInfo("CALCOLO", "CorrSpessore", Path_LAVORAZIONE_INI))
  '
  NomeForm.txtINP(0).Text = frmt(ScalaX, 4)
  NomeForm.txtINP(1).Text = frmt(ScalaY, 4)
  NomeForm.txtINP(3).Text = frmt(Risoluzione, 4)
  NomeForm.txtINP(4).Text = frmt(CorrSpessore, 4)
  '
  'LARGHEZZA
  stmp = GetInfo("CALCOLO", "Larghezza", Path_LAVORAZIONE_INI)
  If val(stmp) > 0 Then PicW = val(stmp) Else PicW = 190
  NomeForm.OEM1PicCALCOLO.ScaleWidth = PicW
  'ALTEZZA
  stmp = GetInfo("CALCOLO", "Altezza", Path_LAVORAZIONE_INI)
  If val(stmp) > 0 Then PicH = val(stmp) Else PicH = 95
  NomeForm.OEM1PicCALCOLO.ScaleHeight = PicH
  'CARATTERE DI STAMPA TESTO
      If (LINGUA = "CH") Then
    TextFont = "MS Song"
  ElseIf (LINGUA = "RU") Then
    TextFont = "Arial Cyr"
  Else
    TextFont = "Courier New"
  End If
  If (SiStampa = 1) Then
    For i = 1 To 2
      Printer.FontName = TextFont
      Printer.FontSize = 12
      Printer.FontBold = False
    Next i
  Else
    For i = 1 To 2
      NomeForm.OEM1PicCALCOLO.FontName = TextFont
      NomeForm.OEM1PicCALCOLO.FontSize = 8
      NomeForm.OEM1PicCALCOLO.FontBold = False
    Next i
  End If
  '
  'PROFILO INGRANAGGIO
  Z = LEP("R[5]")
  AM = LEP("R[42]")
  AlfaD = LEP("R[40]")
  BetaD = LEP("R[41]")
  '
  D2 = val(GetInfo("CALCOLO", "SAP", Path_LAVORAZIONE_INI))
  D1 = val(GetInfo("CALCOLO", "EAP", Path_LAVORAZIONE_INI))
  '
  DIint = LEP("R[45]")
  SONdonnee = LEP("R[46]")
  Q = LEP("R[49]")
  DR = LEP("R[50]")
  RF = LEP("R[52]")
  'DIPU = LEP("R[194]")
  Valutazione = LEP("R[901]")
  '
  If BetaD = 0 Then BetaD = 0.000001
  ALFAn = AlfaD * PG / 180
  BetaR = Abs(BetaD) * PG / 180
  '
  'CALCOLO COSTANTI
  ALFAt = Atn(Tan(ALFAn) / Cos(BetaR))
  T1 = Tan(ALFAt)
  CS1 = Cos(ALFAt)
  CS2 = Cos(BETAt)
  F3 = T1 - Atn(T1)             'INVOLUTA(ALFAON)
  R = AM * Z / 2 / Cos(BetaR)   'R primitivo
  Betab = Atn(Tan(BetaR) * CS1) 'elica base
  Rb = R * CS1                  'R base
  PB = (Rb * 2 * PG) / Z        'Passo di base
  D3 = 2 * Rb
  D5 = 2 * R
  dprim = R * 2
  DPG = val(GetInfo("CALCOLO", "DPG", Path_LAVORAZIONE_INI))
  '
  If (SONdonnee > 0) Then
    'PRENDO LO SPESSORE IMPOSTATO
    SONn = SONdonnee
  Else
    'RICAVO LO SPESSORE DALLA QUOTA RULLI
    'SONn = QUOTA_RULLI_SON(Z, Rb, ALFAt, BETAb, R, Q, Dr)
    Call QUOTA_RULLI(DR, Z, D3, Betab, Sb, Q, "E")
    SONn = D5 * (Sb / D3 - F3)
    'SONdonnee = (AM / Cos(BetaR) * PG - SONn)
  End If
  '
  CorINT = val(GetInfo("CALCOLO", "CorINT", Path_LAVORAZIONE_INI))
  CorINCL = val(GetInfo("CALCOLO", "CorINCL", Path_LAVORAZIONE_INI))
  DeltaX = val(GetInfo("CALCOLO", "DeltaX", Path_LAVORAZIONE_INI))
  ConCorr = val(GetInfo("CALCOLO", "ConCorr", Path_LAVORAZIONE_INI))
  EAP = val(GetInfo("CALCOLO", "SAP", Path_LAVORAZIONE_INI))
  SAP = val(GetInfo("CALCOLO", "EAP", Path_LAVORAZIONE_INI))
  DEm = val(GetInfo("CALCOLO", "DiametroEsternoMolaINT", Path_LAVORAZIONE_INI))
  INm = val(GetInfo("CALCOLO", "InclinazioneMolaINT", Path_LAVORAZIONE_INI))
  '
  ' input coordinate mola
  Open g_chOemPATH & "\INTERNI1.TEO" For Input As #1
    Line Input #1, stmp
    i = 0
    While Not EOF(1)
      i = i + 1
      Input #1, k1, k2, k3, k4, Xm(i), Ym(i), AlfaM(i)
    Wend
  Close #1
  Np = i
  Open g_chOemPATH & "\INTERNI2.TEO" For Input As #1
    Line Input #1, stmp
    i = 0
    While Not EOF(1)
      i = i + 1
      Input #1, k1, k2, k3, k4, Xm(Np + Np - i), Ym(Np + Np - i), AlfaM(Np + Np - i)
      Xm(Np + Np - i) = -Xm(Np + Np - i)
      AlfaM(Np + Np - i) = -AlfaM(Np + Np - i)
    Wend
  Close #1
  Np = Np + i - 1 ' NP diventa il numero punti totale
  ' inversione XMola
  If Xm(1) > 0 Then
    For i = 1 To Np
      Xm(i) = -Xm(i): AlfaM(i) = -AlfaM(i)
    Next i
  End If
  ' adesso abbiamo questa situazione:
  ' punto  1 = primo punto fianco SX (coordinate X e AlfaM negative)
  ' punto NP = ultimo punto fianco DX (coordinate X e AlfaM positive)
  stmp = ""
  stmp = stmp & " Angolo di pressione ---- : " & frmt(AlfaD, 4) & Chr(13)
  stmp = stmp & " Angolo elica ----------- : " & frmt(BetaD, 4) & Chr(13)
  stmp = stmp & " Modulo normale --------- : " & frmt(AM, 4) & Chr(13)
  stmp = stmp & " Numero di denti -------- : " & frmt(Z, 4) & Chr(13)
  stmp = stmp & " Spessore circolare ----- : " & frmt(SONn, 4) & Chr(13)
  'MsgBox stmp, 64, "WARNING"
  '
  Ent = (-DEm + DIint) / 2 + CorINT '- CorINT   'FlagMOD 028
  INC = BetaR + CorINCL * PG / 180
  If (DeltaX <> 0) Then
    For i = 1 To Np
      Xm(i) = Xm(i) + DeltaX
    Next i
  End If
  '
  'CALCOLO COSTANTI
  ALFAt = Atn(Tan(ALFAn) / Cos(BetaR))
  T1 = Tan(ALFAt)
  CS1 = Cos(ALFAt)
  F3 = T1 - Atn(T1)             'INVOLUTA(ALFAON)
  R = AM * Z / 2 / Cos(BetaR)   'R primitivo
  Betab = Atn(Tan(BetaR) * CS1) 'elica base
  Rb = R * CS1                  'R base
  PB = (Rb * 2 * PG) / Z        'Passo di base
  DB = 2 * Rb                   'diam. base
  D5 = 2 * R                    'diam. primitivo
  PAS = Tan(PG / 2 - BetaR - ConCorr * PG / 180) * 2 * PG * R
  SONt = 2 * PG * R / Z - SONn / Cos(BetaR)
  iB = Rb * (SONt / R + 2 * F3)
  Sb = PB - iB
  MM1 = Sqr(SAP ^ 2 - DB ^ 2) / 2
  MM2 = Sqr(EAP ^ 2 - DB ^ 2) / 2
  'SEPARAZIONE FIANCHI SX DX
  'JSX = indice ultimo punto fianco SX (EAP)
  '      (primo punto fianco SX: 1)
  'JDX = indice primo punto fianco DX  (EAP)
  '      (ultimo punto fianco DX: NP)
  jSX = 1
  While (Abs(AlfaM(jSX)) < 85) 'AND (XM(JSX) < 0 AND RT > 0)
    jSX = jSX + 1
  Wend
  jSX = jSX - 1
  jDX = jSX
  While (Abs(AlfaM(jDX)) > 80) Or (Xm(jDX) < 0)
    jDX = jDX + 1
  Wend
  'modifico due punti per introdurre errori (piu materiale sulla mola:+ERM)
  'ERM = 0.005
  'INDICE1 = 10
  'INDICE2 = 10
  'XM(INDICE1) = XM(INDICE1) - ERM
  'XM(Np - INDICE2) = XM(Np - INDICE2) + ERM
  'calcolo ingranaggio da mola (SX)
  i = 0
  For j = 1 To jSX
    EXF = Xm(j)
    RXF = Ym(j)
    APFD = AlfaM(j) * PG / 180
    Call Piece1(Rayon, Alfa1, beta1, Ent, INC, PAS, EXF, RXF, APFD, Ltang)
    DIAM = Rayon * 2
    If DIAM >= (SAP - 5) And DIAM <= (EAP + 1) And DIAM > DB Then
      i = i + 1
      XSs(i) = Rayon * Sin(Alfa1)
      YSs(i) = Rayon * Cos(Alfa1)   'coordinate ingranaggio ottenuto
    End If
  Next j
  NpS = i   ' numero punti fianco sx (da graficare)
  RX = -999
  i = 0
  While (RX < R) And (i < NpS)
    i = i + 1
    RX = Sqr(XSs(i) ^ 2 + YSs(i) ^ 2)
  Wend
  'primitivo tra I e I-1
  R1 = Sqr(XSs(i - 1) ^ 2 + YSs(i - 1) ^ 2)
  R2 = Sqr(XSs(i) ^ 2 + YSs(i) ^ 2)
  Xpr = -XSs(i) + (-XSs(i - 1) + XSs(i)) * (R - R2) / (R1 - R2)
  Ypr = Sqr(R ^ 2 - Xpr ^ 2)
  T0 = Atn(Xpr / Ypr)
  SONt = 2 * R * T0   'vano
  SONs = SONt * Cos(BetaR)    'vano normale su prim
  For i = 1 To NpS
    RX = Sqr(XSs(i) ^ 2 + YSs(i) ^ 2)
    CosA = Rb / RX
    SinA = Sqr(1 - CosA ^ 2)
    TanA = SinA / CosA
    Ax = Atn(TanA)
    InvAx = TanA - Ax
    VX = (SONt / 2 / R + F3 - InvAx) * 2 * RX
    T0 = PG / 2 - VX / 2 / RX
    X0 = -RX * Cos(T0)
    Y0 = RX * Sin(T0)
    LS(i) = Sqr((XSs(i) - X0) ^ 2 + (Y0 - YSs(i)) ^ 2)    'ERRORE=ascissa
    MS(i) = Sqr(RX ^ 2 - Rb ^ 2)            'Ordinata
    If X0 > XSs(i) Then LS(i) = -LS(i)
  Next i
  '***********************   calcolo ingranaggio da mola (DX)
  i = 0
  For j = jDX To Np
    EXF = Xm(j)
    RXF = Ym(j)
    APFD = AlfaM(j) * PG / 180
    Call Piece1(Rayon, Alfa1, beta1, Ent, INC, PAS, EXF, RXF, APFD, Ltang)
    DIAM = Rayon * 2
    If DIAM >= (SAP - 5) And DIAM <= (EAP + 1) And DIAM > DB Then
      i = i + 1
      XDd(i) = Rayon * Sin(Alfa1)
      YDd(i) = Rayon * Cos(Alfa1)   'coordinate ingranaggio ottenuto
    End If
  Next j
  NpD = i   ' numero punti fianco DX (da graficare)
  'calcolo Spessore sul primitivo
  'calcolo errore rispetto teorico
  RX = -999
  i = NpD
  While (RX < R) And (i > 1)
    i = i - 1
    RX = Sqr(XDd(i) ^ 2 + YDd(i) ^ 2)
  Wend
  'prim. tra I e I+1
  R1 = Sqr(XDd(i + 1) ^ 2 + YDd(i + 1) ^ 2)
  R2 = Sqr(XDd(i) ^ 2 + YDd(i) ^ 2)
  Xpr = XDd(i) + (XDd(i + 1) - XDd(i)) * (R - R2) / (R1 - R2)
  Ypr = Sqr(R ^ 2 - Xpr ^ 2)
  T0 = Atn(Xpr / Ypr)
  SONt = 2 * R * T0    'vano
  SONd = SONt * Cos(BetaR)
  For i = 1 To NpD
    RX = Sqr(XDd(i) ^ 2 + YDd(i) ^ 2)
    CosA = Rb / RX
    SinA = Sqr(1 - CosA ^ 2)
    TanA = SinA / CosA
    Ax = Atn(TanA)
    InvAx = TanA - Ax
    VX = (SONt / 2 / R + F3 - InvAx) * 2 * RX
    T0 = PG / 2 - VX / 2 / RX
    X0 = RX * Cos(T0)
    Y0 = RX * Sin(T0)
    LD(i) = Sqr((X0 - XDd(i)) ^ 2 + (Y0 - YDd(i)) ^ 2)     'ERRORE=ascissa
    MD(i) = Sqr(RX ^ 2 - Rb ^ 2)            'Ordinata
    If X0 < XDd(i) Then LD(i) = -LD(i)
  Next i
  '
  SONmola = (PG * R * 2 / Z - (SONs + SONd) / 2 / Cos(BetaR)) * Cos(BetaR)
  '
  'CALCOLO DIAMETRO PRIMITIVO
  MMp = Sqr(R ^ 2 - Rb ^ 2)
  'RICERCA DEL MAX
  Ymax = MM2
  If MMp > Ymax Then Ymax = MMp
  If MM1 > Ymax Then Ymax = MM1
  'RICERCA DEL MIN
  Ymin = MM2
  If MMp < Ymin Then Ymin = MMp
  If MM1 < Ymin Then Ymin = MM1
    '
  Dim OGGETTO As Control
  Dim COLORE  As Long
  If (SiStampa = 1) Then Set OGGETTO = Printer Else Set OGGETTO = OEMX.OEM1PicCALCOLO
  '
  'IMPOSTAZIONE FONT
  For i = 1 To 2
    OGGETTO.FontName = TextFont
    OGGETTO.FontSize = 10
    OGGETTO.FontBold = False
  Next i
  '
  'INTESTAZIONE F1
  stmp = " F1 "
  OGGETTO.CurrentX = PicW / 2 - PicW / 4 - OGGETTO.TextWidth(stmp) / 2
  OGGETTO.CurrentY = 0
  OGGETTO.Print stmp
  'INTESTAZIONE F2
  stmp = " F2 "
  OGGETTO.CurrentX = PicW / 2 + PicW / 4 - OGGETTO.TextWidth(stmp) / 2
  OGGETTO.CurrentY = 0
  OGGETTO.Print stmp
  'SEGNI
  stmp = " + "
  OGGETTO.CurrentX = 0
  OGGETTO.CurrentY = 0
  OGGETTO.Print stmp
  stmp = " - "
  OGGETTO.CurrentX = PicW / 2 - OGGETTO.TextWidth(stmp) / 2
  OGGETTO.CurrentY = 0
  OGGETTO.Print stmp
  stmp = " + "
  OGGETTO.CurrentX = PicW - OGGETTO.TextWidth(stmp)
  OGGETTO.CurrentY = 0
  OGGETTO.Print stmp
  'IMPOSTAZIONE LINEA PER DIAMETRI: INIZIO
  OGGETTO.DrawStyle = 4
  'EAP
  pY1 = -(MM2 - (Ymax + Ymin) / 2) * ScalaY + PicH / 2
  stmp = " " & frmt(EAP, 4) & " "
  OGGETTO.CurrentX = 0
  OGGETTO.CurrentY = pY1 - OGGETTO.TextHeight(stmp) / 2
  OGGETTO.Print stmp
  pX1 = OGGETTO.TextWidth(stmp)
  stmp = " " & frmt(MM2, 4) & " "
  pX2 = PicW - OGGETTO.TextWidth(stmp)
  OGGETTO.CurrentX = pX2
  OGGETTO.CurrentY = pY1 - OGGETTO.TextHeight(stmp) / 2
  OGGETTO.Print stmp
  pY2 = pY1
  OGGETTO.Line (pX1, pY1)-(pX2, pY2)
  'SAP
  pY1 = -(MM1 - (Ymax + Ymin) / 2) * ScalaY + PicH / 2
  stmp = " " & frmt(SAP, 4) & " "
  OGGETTO.CurrentX = 0
  OGGETTO.CurrentY = pY1 - OGGETTO.TextHeight(stmp) / 2
  OGGETTO.Print stmp
  pX1 = OGGETTO.TextWidth(stmp)
  stmp = " " & frmt(MM1, 4) & " "
  pX2 = PicW - OGGETTO.TextWidth(stmp)
  OGGETTO.CurrentX = pX2
  OGGETTO.CurrentY = pY1 - OGGETTO.TextHeight(stmp) / 2
  OGGETTO.Print stmp
  pY2 = pY1
  OGGETTO.Line (pX1, pY1)-(pX2, pY2)
  'MMp
  pY1 = -(MMp - (Ymax + Ymin) / 2) * ScalaY + PicH / 2
  stmp = " " & frmt(2 * R, 4) & " "
  OGGETTO.CurrentX = 0
  OGGETTO.CurrentY = pY1 - OGGETTO.TextHeight(stmp) / 2
  OGGETTO.Print stmp
  pX1 = OGGETTO.TextWidth(stmp)
  stmp = " " & frmt(MMp, 4) & " "
  pX2 = PicW - OGGETTO.TextWidth(stmp)
  OGGETTO.CurrentX = pX2
  OGGETTO.CurrentY = pY1 - OGGETTO.TextHeight(stmp) / 2
  OGGETTO.Print stmp
  pY2 = pY1
  OGGETTO.Line (pX1, pY1)-(pX2, pY2)
  OGGETTO.DrawStyle = 0
  '
  'SONmola
  stmp = "S0N= " & frmt(SONmola, 4)
  OGGETTO.CurrentX = PicW / 2 - OGGETTO.TextWidth(stmp) / 2
  OGGETTO.CurrentY = 5
  OGGETTO.Print stmp
  '
  If (DPG > 0) Then
    Eb = Rb * (SONmola / Cos(BetaR) / R - 2 * F3)
    iB = PB - Eb
    Call QUOTA_RULLI(DPG, Z, DB, Betab, iB, Qmola, "K")
    stmp = "Q(" & frmt(DPG, 4) & ")=" & frmt(Qmola, 4)
    OGGETTO.CurrentX = PicW - OGGETTO.TextWidth(stmp)
    OGGETTO.CurrentY = 5
    OGGETTO.Print stmp
  End If
  '
  'RISOLUZIONE
  stmp = frmt(Risoluzione, 4)
  OGGETTO.CurrentX = -OGGETTO.TextWidth(stmp) / 2 + PicW / 2
  OGGETTO.CurrentY = -OGGETTO.TextHeight(stmp) + PicH
  OGGETTO.Print stmp
  'Freccia a sinistra del testo
  pX1 = 0.5 * Risoluzione * ScalaX + PicW / 2
  pY1 = PicH - 10
  Ang = AF / 180 * PG
  DeltaAng = Ang / NumeroRette
  For i = -NumeroRette To NumeroRette
    pX2 = DF * Cos(Ang) + PicW / 2
    pY2 = -(DF * Sin(i * DeltaAng)) + PicH - 10
    OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
  Next i
  OGGETTO.Line (pX1, pY1 - DF / 2)-(pX1, pY1 + DF / 2), QBColor(12)
  'Freccia a destra del testo
  pX1 = -0.5 * Risoluzione * ScalaX + PicW / 2
  pY1 = PicH - 10
  Ang = AF / 180 * PG
  DeltaAng = Ang / NumeroRette
  For i = -NumeroRette To NumeroRette
    pX2 = -DF * Cos(Ang) + PicW / 2
    pY2 = -(DF * Sin(i * DeltaAng)) + PicH - 10
    OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
  Next i
  OGGETTO.Line (pX1, pY1 - DF / 2)-(pX1, pY1 + DF / 2), QBColor(12)
  'GRIGLIA
  OGGETTO.DrawStyle = 2
  For i = -5 To 5
    'SX/F1
    pX1 = i * Risoluzione * ScalaX - PicW / 4 + PicW / 2
    pY1 = -(Ymax - (Ymax + Ymin) / 2) * ScalaY + PicH / 2 - 5
    pX2 = i * Risoluzione * ScalaX - PicW / 4 + PicW / 2
    pY2 = -(Ymin - (Ymax + Ymin) / 2) * ScalaY + PicH / 2 + 5
    OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
    'DX/F2
    pX1 = i * Risoluzione * ScalaX + PicW / 4 + PicW / 2
    pY1 = -(Ymax - (Ymax + Ymin) / 2) * ScalaY + PicH / 2 - 5
    pX2 = i * Risoluzione * ScalaX + PicW / 4 + PicW / 2
    pY2 = -(Ymin - (Ymax + Ymin) / 2) * ScalaY + PicH / 2 + 5
    OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(12)
  Next i
  OGGETTO.DrawStyle = 0
  '
  '***************  PROFILO  ***************
  'SX/F1
  For j = 1 To NpS - 1
    pX1 = -LS(j) * ScalaX - PicW / 4 + PicW / 2
    pY1 = -(MS(j) - (Ymax + Ymin) / 2) * ScalaY + PicH / 2
    pX2 = -LS(j + 1) * ScalaX - PicW / 4 + PicW / 2
    pY2 = -(MS(j + 1) - (Ymax + Ymin) / 2) * ScalaY + PicH / 2
    OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(9)
  Next
  'DX/F2
  For j = 1 To NpD - 1
    pX1 = LD(j) * ScalaX + PicW / 4 + PicW / 2
    pY1 = -(MD(j) - (Ymax + Ymin) / 2) * ScalaY + PicH / 2
    pX2 = LD(j + 1) * ScalaX + PicW / 4 + PicW / 2
    pY2 = -(MD(j + 1) - (Ymax + Ymin) / 2) * ScalaY + PicH / 2
    OGGETTO.Line (pX1, pY1)-(pX2, pY2), QBColor(9)
  Next
  '***************  *******  ***************
  '
  Dim Val1 As Double
  Dim Val2 As Double
  For Fianco = 1 To 2
    If Valutazione = 2 Then
      Val1 = IIf(Fianco = 1, LEP("R[270]"), LEP("R[285]"))
      If Val1 > 0 Then
        Val2 = Sqr(Val1 ^ 2 / 4 - Rb ^ 2)
        Call TIF_di_riferimento_Fianco(NomeForm, Val1, Val2, ScalaY, Ymax, Ymin, SiStampa, Fianco, Valutazione)
      End If
      Val1 = IIf(Fianco = 1, LEP("R[273]"), LEP("R[288]"))
      If Val1 > 0 Then
        Val2 = Sqr(Val1 ^ 2 / 4 - Rb ^ 2)
        Call TIF_di_riferimento_Fianco(NomeForm, Val1, Val2, ScalaY, Ymax, Ymin, SiStampa, Fianco, Valutazione)
      End If
      Val1 = IIf(Fianco = 1, LEP("R[276]"), LEP("R[291]"))
      If Val1 > 0 Then
        Val2 = Sqr(Val1 ^ 2 / 4 - Rb ^ 2)
        Call TIF_di_riferimento_Fianco(NomeForm, Val1, Val2, ScalaY, Ymax, Ymin, SiStampa, Fianco, Valutazione)
      End If
    Else
      Val2 = IIf(Fianco = 1, LEP("R[270]"), LEP("R[285]"))
      If Val2 > 0 Then
        Val1 = 2 * Sqr(Val2 ^ 2 + Rb ^ 2)
        Call TIF_di_riferimento_Fianco(NomeForm, Val1, Val2, ScalaY, Ymax, Ymin, SiStampa, Fianco, Valutazione)
      End If
      Val2 = IIf(Fianco = 1, LEP("R[273]"), LEP("R[288]"))
      If Val2 > 0 Then
        Val1 = 2 * Sqr(Val2 ^ 2 + Rb ^ 2)
        Call TIF_di_riferimento_Fianco(NomeForm, Val1, Val2, ScalaY, Ymax, Ymin, SiStampa, Fianco, Valutazione)
      End If
      Val2 = IIf(Fianco = 1, LEP("R[276]"), LEP("R[291]"))
      If Val2 > 0 Then
        Val1 = 2 * Sqr(Val2 ^ 2 + Rb ^ 2)
        Call TIF_di_riferimento_Fianco(NomeForm, Val1, Val2, ScalaY, Ymax, Ymin, SiStampa, Fianco, Valutazione)
      End If
    End If
  Next Fianco

  If FreeFile > 1 Then Close

Exit Sub

errVERIFICA_PROFILO_INGRANAGGIO_INTERNO:
  WRITE_DIALOG "Sub VERIFICA_PROFILO_INGRANAGGIO_INTERNO: ERROR -> " & Err
  If FreeFile > 1 Then Close
  Resume Next
  'Exit Sub

End Sub

Sub VISUALIZZA_MOLA_INGRANAGGIO(NomeForm As Form, ByVal TIPO As Integer, ByVal SiStampa As Integer)
'
Dim Atmp      As String * 255
Dim i         As Integer
Dim pX1       As Single
Dim pY1       As Single
Dim pX2       As Single
Dim pY2       As Single
Dim Altezza   As Single
Dim Larghezza As Single
Dim ScalaX    As Single
Dim ScalaY    As Single
Dim Xmax      As Single
Dim Ymax      As Single
Dim Xmin      As Single
Dim Ymin      As Single
Dim PicH      As Single
Dim PicW      As Single
Dim da        As Double
Dim Raggio    As Double
Dim ANGOLO    As Double
Dim XX(2, dv) As Double
Dim YY(2, dv) As Double
Dim NumeroPunti As Integer
Dim NPR1 As Integer
Dim stmp As String
Dim TextFont As String
Dim k1 As Double
Dim k2 As Double
Dim k3 As Double
Dim k4 As Double
Dim K5 As Double
Dim Np As Integer
'
On Error GoTo errVISUALIZZA_MOLA_INGRANAGGIO
  '
  For i = 0 To 4
    NomeForm.txtINP(i).Visible = False
    NomeForm.lblINP(i).Visible = False
  Next i
  'LARGHEZZA
  stmp = GetInfo("CALCOLO", "Larghezza", Path_LAVORAZIONE_INI)
  If val(stmp) > 0 Then PicW = val(stmp) Else PicW = 190
  NomeForm.OEM1PicCALCOLO.ScaleWidth = PicW
  'ALTEZZA
  stmp = GetInfo("CALCOLO", "Altezza", Path_LAVORAZIONE_INI)
  If val(stmp) > 0 Then PicH = val(stmp) Else PicH = 95
  NomeForm.OEM1PicCALCOLO.ScaleHeight = PicH
  'CARATTERE DI STAMPA TESTO
      If (LINGUA = "CH") Then
    TextFont = "MS Song"
  ElseIf (LINGUA = "RU") Then
    TextFont = "Arial Cyr"
  Else
    TextFont = "Courier New"
  End If
  If (SiStampa = 1) Then
    For i = 1 To 2
      Printer.FontName = TextFont
      Printer.FontSize = 12
      Printer.FontBold = False
    Next i
  Else
    For i = 1 To 2
      NomeForm.OEM1PicCALCOLO.FontName = TextFont
      NomeForm.OEM1PicCALCOLO.FontSize = 8
      NomeForm.OEM1PicCALCOLO.FontBold = False
    Next i
  End If
  NomeForm.OEM1PicCALCOLO.Line (PicW / 2, 0)-(PicW / 2, PicH)
  'Numero dei punti del raggio di testa della mola
  NPR1 = val(GetInfo("CALCOLO", "NumeroPuntiRaggio", Path_LAVORAZIONE_INI))
  If (NPR1 = 0) Then NPR1 = 5
  '
  Select Case TIPO
      '
    Case 0    'PROFILO MOLA
      NomeForm.OEM1fraOpzioni.Visible = False
      NomeForm.lblINP(0).Visible = True
      NomeForm.txtINP(0).Visible = True
      NomeForm.lblINP(1).Visible = False
      NomeForm.txtINP(1).Visible = False
      GoSub ETICHETTE_GRAFICI
      For Fianco = 1 To 2
        i = 1
        Open g_chOemPATH & "\INTERNI" & Format$(Fianco, "0") & ".TEO" For Input As #1
          Line Input #1, stmp
          While Not EOF(1)
            Input #1, k1, k2, k3, k4, XX(Fianco, i), YY(Fianco, i), K5
            i = i + 1
          Wend
        Close #1
        NumeroPunti = i - 1  'NumeroPunti=Np + NpR1 + 1
      Next Fianco
      'ESTREMI
      Xmax = XX(1, 1)
      If XX(2, 1) > Xmax Then Xmax = XX(2, 1)
      Ymax = YY(1, NumeroPunti)
      If YY(2, NumeroPunti) > Ymax Then Ymax = YY(2, NumeroPunti)
      Xmin = 0
      Ymin = YY(1, 1)
      If YY(2, 1) > Ymin Then Ymin = YY(2, 1)
      ScalaX = val(GetInfo("CALCOLO", "ScalaMOLA", Path_LAVORAZIONE_INI))
      ScalaY = ScalaX
      NomeForm.txtINP(0).Text = frmt(ScalaX, 4)
      NomeForm.txtINP(1).Text = frmt(ScalaY, 4)
      'Fianco 1
      stmp = "F1"
      For i = 1 To NumeroPunti - 1
        pX1 = -XX(1, i) * ScalaX + PicW / 2
        pY1 = -(YY(1, i) - (Ymax + Ymin) / 2) * ScalaY + PicH / 2
        pX2 = -XX(1, i + 1) * ScalaX + PicW / 2
        pY2 = -(YY(1, i + 1) - (Ymax + Ymin) / 2) * ScalaY + PicH / 2
        If (SiStampa = 1) Then
          'Profilo
          Printer.Line (pX1, pY1)-(pX2, pY2)
          'Croci
          Printer.Line (pX2 - 0.5, pY2)-(pX2 + 0.5, pY2)
          Printer.Line (pX2, pY2 - 0.5)-(pX2, pY2 + 0.5)
        Else
          'Profilo
          NomeForm.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2)
          'Croci
          NomeForm.OEM1PicCALCOLO.Line (pX2 - 0.5, pY2)-(pX2 + 0.5, pY2)
          NomeForm.OEM1PicCALCOLO.Line (pX2, pY2 - 0.5)-(pX2, pY2 + 0.5)
        End If
      Next i
      'Fianco 2
      stmp = "F2"
      For i = 1 To NumeroPunti - 1
        pX1 = XX(2, i) * ScalaX + PicW / 2
        pY1 = -(YY(2, i) - (Ymax + Ymin) / 2) * ScalaY + PicH / 2
        pX2 = XX(2, i + 1) * ScalaX + PicW / 2
        pY2 = -(YY(2, i + 1) - (Ymax + Ymin) / 2) * ScalaY + PicH / 2
        If (SiStampa = 1) Then
          'Profilo
          Printer.Line (pX1, pY1)-(pX2, pY2)
          'Croci
          Printer.Line (pX2 - 0.5, pY2)-(pX2 + 0.5, pY2)
          Printer.Line (pX2, pY2 - 0.5)-(pX2, pY2 + 0.5)
        Else
          'Profilo
          NomeForm.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2)
          'Croci
          NomeForm.OEM1PicCALCOLO.Line (pX2 - 0.5, pY2)-(pX2 + 0.5, pY2)
          NomeForm.OEM1PicCALCOLO.Line (pX2, pY2 - 0.5)-(pX2, pY2 + 0.5)
        End If
      Next i
      pX1 = -Xmax * ScalaX + PicW / 2
      pY1 = -(Ymax - (Ymax + Ymin) / 2) * ScalaY + PicH / 2
      pX2 = Xmax * ScalaX + PicW / 2
      pY2 = -(Ymin - (Ymax + Ymin) / 2) * ScalaY + PicH / 2
      If (SiStampa = 1) Then
        'Nome
        stmp = "F1 "
        Printer.CurrentX = pX1 - Printer.TextWidth(stmp)
        Printer.CurrentY = pY2 - Printer.TextHeight(stmp)
        Printer.Print stmp
        stmp = " F2"
        Printer.CurrentX = pX2
        Printer.CurrentY = pY2 - Printer.TextHeight(stmp)
        Printer.Print stmp
        'Larghezza mola: valore
        stmp = "W=" & frmt(2 * Xmax, 4) & "mm  "
        Printer.CurrentX = PicW / 2 - Printer.TextWidth(stmp)
        Printer.CurrentY = pY2 - Printer.TextHeight(stmp)
        Printer.Print stmp
        'Altezza mola: valore
        stmp = " H=" & frmt(Ymax - Ymin, 4) & "mm"
        Printer.CurrentX = PicW / 2
        Printer.CurrentY = pY2 - Printer.TextHeight(stmp)
        Printer.Print stmp
        Printer.DrawStyle = 4
        'Larghezza mola: linea
        Printer.Line (pX1, pY2)-(pX2, pY2)
        'Altezza mola: linea
        Printer.Line (PicW / 2, pY1)-(PicW / 2, pY2)
        Printer.DrawStyle = 0
      Else
        'Nome
        stmp = "F1 "
        NomeForm.OEM1PicCALCOLO.CurrentX = pX1 - NomeForm.OEM1PicCALCOLO.TextWidth(stmp)
        NomeForm.OEM1PicCALCOLO.CurrentY = pY2 - NomeForm.OEM1PicCALCOLO.TextHeight(stmp)
        NomeForm.OEM1PicCALCOLO.Print stmp
        stmp = " F2"
        NomeForm.OEM1PicCALCOLO.CurrentX = pX2
        NomeForm.OEM1PicCALCOLO.CurrentY = pY2 - NomeForm.OEM1PicCALCOLO.TextHeight(stmp)
        NomeForm.OEM1PicCALCOLO.Print stmp
        'Larghezza mola: valore
        stmp = "W=" & frmt(Xmax + Xmax, 4) & "mm  "
        NomeForm.OEM1PicCALCOLO.CurrentX = PicW / 2 - NomeForm.OEM1PicCALCOLO.TextWidth(stmp)
        NomeForm.OEM1PicCALCOLO.CurrentY = pY2 - NomeForm.OEM1PicCALCOLO.TextHeight(stmp)
        NomeForm.OEM1PicCALCOLO.Print stmp
        'Altezza mola: valore
        stmp = " H=" & frmt(Ymax - Ymin, 4) & "mm"
        NomeForm.OEM1PicCALCOLO.CurrentX = PicW / 2
        NomeForm.OEM1PicCALCOLO.CurrentY = pY2 - NomeForm.OEM1PicCALCOLO.TextHeight(stmp)
        NomeForm.OEM1PicCALCOLO.Print stmp
        NomeForm.OEM1PicCALCOLO.DrawStyle = 4
        'Larghezza mola: linea
        NomeForm.OEM1PicCALCOLO.Line (pX1, pY2)-(pX2, pY2)
        'Altezza mola: linea
        NomeForm.OEM1PicCALCOLO.Line (PicW / 2, pY1)-(PicW / 2, pY2)
        NomeForm.OEM1PicCALCOLO.DrawStyle = 0
      End If
      GoSub FineDocumento
    
    Case 1    'INGRANAGGIO
      NomeForm.OEM1fraOpzioni.Visible = False
      NomeForm.lblINP(0).Visible = True
      NomeForm.txtINP(0).Visible = True
      NomeForm.lblINP(1).Visible = False
      NomeForm.txtINP(1).Visible = False
      GoSub ETICHETTE_GRAFICI
      For Fianco = 1 To 2
        i = 1
        Open g_chOemPATH & "\INTERNI" & Format$(Fianco, "0") & ".TEO" For Input As #1
          Line Input #1, stmp
          While Not EOF(1)
            Input #1, k1, Raggio, ANGOLO, k2, k3, k4, K5
            XX(Fianco, i) = -Raggio * Sin(ANGOLO / 180 * PG)
            YY(Fianco, i) = Raggio * Cos(ANGOLO / 180 * PG)
            i = i + 1
          Wend
        Close #1
        NumeroPunti = i - 1 - (NPR1 + 1) 'NumeroPunti=Np
      Next Fianco
      'ESTREMI
      Xmax = XX(1, 1)
      If XX(2, 1) > Xmax Then Xmax = XX(2, 1)
      Ymax = YY(1, Np)
      If YY(2, NumeroPunti) > Ymax Then Ymax = YY(2, NumeroPunti)
      Xmin = 0
      Ymin = YY(1, 1)
      If YY(2, 1) > Ymin Then Ymin = YY(2, 1)
      ScalaX = val(GetInfo("CALCOLO", "ScalaINGRANAGGIO", Path_LAVORAZIONE_INI))
      ScalaY = ScalaX
      NomeForm.txtINP(0).Text = frmt(ScalaX, 4)
      NomeForm.txtINP(1).Text = frmt(ScalaY, 4)
      'Fianco 1
      stmp = "F1"
      For i = 1 To NumeroPunti - 1
        pX1 = -XX(1, i) * ScalaX + PicW / 2
        pY1 = -(YY(1, i) - (Ymax + Ymin) / 2) * ScalaY + PicH / 2
        pX2 = -XX(1, i + 1) * ScalaX + PicW / 2
        pY2 = -(YY(1, i + 1) - (Ymax + Ymin) / 2) * ScalaY + PicH / 2
        If (SiStampa = 1) Then
          'Profilo
          Printer.Line (pX1, pY1)-(pX2, pY2)
          'Croci
          Printer.Line (pX2 - 0.5, pY2)-(pX2 + 0.5, pY2)
          Printer.Line (pX2, pY2 - 0.5)-(pX2, pY2 + 0.5)
        Else
          'Profilo
          NomeForm.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2)
          'Croci
          NomeForm.OEM1PicCALCOLO.Line (pX2 - 0.5, pY2)-(pX2 + 0.5, pY2)
          NomeForm.OEM1PicCALCOLO.Line (pX2, pY2 - 0.5)-(pX2, pY2 + 0.5)
        End If
      Next i
      'Fianco 2
      stmp = "F2"
      For i = 1 To NumeroPunti - 1
        pX1 = XX(2, i) * ScalaX + PicW / 2
        pY1 = -(YY(2, i) - (Ymax + Ymin) / 2) * ScalaY + PicH / 2
        pX2 = XX(2, i + 1) * ScalaX + PicW / 2
        pY2 = -(YY(2, i + 1) - (Ymax + Ymin) / 2) * ScalaY + PicH / 2
        If (SiStampa = 1) Then
          'Profilo
          Printer.Line (pX1, pY1)-(pX2, pY2)
          'Croci
          Printer.Line (pX2 - 0.5, pY2)-(pX2 + 0.5, pY2)
          Printer.Line (pX2, pY2 - 0.5)-(pX2, pY2 + 0.5)
        Else
          'Profilo
          NomeForm.OEM1PicCALCOLO.Line (pX1, pY1)-(pX2, pY2)
          'Croci
          NomeForm.OEM1PicCALCOLO.Line (pX2 - 0.5, pY2)-(pX2 + 0.5, pY2)
          NomeForm.OEM1PicCALCOLO.Line (pX2, pY2 - 0.5)-(pX2, pY2 + 0.5)
        End If
      Next i
      pX1 = -Xmax * ScalaX + PicW / 2
      pY1 = -(Ymax - (Ymax + Ymin) / 2) * ScalaY + PicH / 2
      pX2 = Xmax * ScalaX + PicW / 2
      pY2 = -(Ymin - (Ymax + Ymin) / 2) * ScalaY + PicH / 2
      If (SiStampa = 1) Then
        'Nome
        stmp = "F1 "
        Printer.CurrentX = pX1 - Printer.TextWidth(stmp)
        Printer.CurrentY = pY2 - Printer.TextHeight(stmp)
        Printer.Print stmp
        stmp = " F2"
        Printer.CurrentX = pX2
        Printer.CurrentY = pY2 - Printer.TextHeight(stmp)
        Printer.Print stmp
        'Larghezza mola: valore
        stmp = "W=" & frmt(2 * Xmax, 4) & "mm  "
        Printer.CurrentX = PicW / 2 - Printer.TextWidth(stmp)
        Printer.CurrentY = pY2 - Printer.TextHeight(stmp)
        Printer.Print stmp
        'Altezza mola: valore
        stmp = " H=" & frmt(Ymax - Ymin, 4) & "mm"
        Printer.CurrentX = PicW / 2
        Printer.CurrentY = pY2 - Printer.TextHeight(stmp)
        Printer.Print stmp
        Printer.DrawStyle = 4
        'Larghezza mola: linea
        Printer.Line (pX1, pY2)-(pX2, pY2)
        'Altezza mola: linea
        Printer.Line (PicW / 2, pY1)-(PicW / 2, pY2)
        Printer.DrawStyle = 0
      Else
        'Nome
        stmp = "F1 "
        NomeForm.OEM1PicCALCOLO.CurrentX = pX1 - NomeForm.OEM1PicCALCOLO.TextWidth(stmp)
        NomeForm.OEM1PicCALCOLO.CurrentY = pY2 - NomeForm.OEM1PicCALCOLO.TextHeight(stmp)
        NomeForm.OEM1PicCALCOLO.Print stmp
        stmp = " F2"
        NomeForm.OEM1PicCALCOLO.CurrentX = pX2
        NomeForm.OEM1PicCALCOLO.CurrentY = pY2 - NomeForm.OEM1PicCALCOLO.TextHeight(stmp)
        NomeForm.OEM1PicCALCOLO.Print stmp
        'Larghezza mola: valore
        stmp = "W=" & frmt(Xmax + Xmax, 4) & "mm  "
        NomeForm.OEM1PicCALCOLO.CurrentX = PicW / 2 - NomeForm.OEM1PicCALCOLO.TextWidth(stmp)
        NomeForm.OEM1PicCALCOLO.CurrentY = pY2 - NomeForm.OEM1PicCALCOLO.TextHeight(stmp)
        NomeForm.OEM1PicCALCOLO.Print stmp
        'Altezza mola: valore
        stmp = " H=" & frmt(Ymax - Ymin, 4) & "mm"
        NomeForm.OEM1PicCALCOLO.CurrentX = PicW / 2
        NomeForm.OEM1PicCALCOLO.CurrentY = pY2 - NomeForm.OEM1PicCALCOLO.TextHeight(stmp)
        NomeForm.OEM1PicCALCOLO.Print stmp
        NomeForm.OEM1PicCALCOLO.DrawStyle = 4
        'Larghezza mola: linea
        NomeForm.OEM1PicCALCOLO.Line (pX1, pY2)-(pX2, pY2)
        'Altezza mola: linea
        NomeForm.OEM1PicCALCOLO.Line (PicW / 2, pY1)-(PicW / 2, pY2)
        NomeForm.OEM1PicCALCOLO.DrawStyle = 0
      End If
      GoSub FineDocumento
  End Select
  If FreeFile > 1 Then Close

Exit Sub

FineDocumento:
  'Dimensione dei caratteri
  If (SiStampa = 1) Then
    For i = 1 To 2
      Printer.FontName = TextFont
      Printer.FontSize = 14
      Printer.FontBold = True
    Next i
    'Logo
    'Printer.PaintPicture NomeForm.PicLogo, 0, PicH, 20, 20
    'Nome del documento
    stmp = Date & " " & Time & "                  " & NomeForm.OEM1Combo1.Caption
    Printer.CurrentX = 0 + 25
    Printer.CurrentY = PicH
    Printer.Print stmp
    'Scala X
    If NomeForm.lblINP(0).Visible = True Then
      stmp = NomeForm.lblINP(0).Caption & ": x" & NomeForm.txtINP(0).Text
      Printer.CurrentX = 0 + 25
      Printer.CurrentY = PicH + 10
      Printer.Print stmp
    End If
    'Scala Y
    If NomeForm.lblINP(1).Visible = True Then
      stmp = NomeForm.lblINP(1).Caption & ": x" & NomeForm.txtINP(1).Text
      Printer.CurrentX = 0 + PicW / 2
      Printer.CurrentY = PicH + 10
      Printer.Print stmp
    End If
    'Parametri
    OEMX.SuOEM1.Visible = True
    Call OEMX.SuOEM1.STAMPA(False, , , , 150)
    OEMX.SuOEM1.Visible = False
    Printer.EndDoc
    WRITE_DIALOG NomeForm.OEM1Combo1.Caption & " --> " & Printer.DeviceName & " OK!!"
  End If
Return

ETICHETTE_GRAFICI:
  'NASCONDO LA CASELLA DI TESTO
  NomeForm.txtHELP.Visible = False
  'VISUALIZZO LE SCALE
  i = LoadString(g_hLanguageLibHandle, 1000, Atmp, 255)
  If i > 0 Then NomeForm.lblINP(0).Caption = Left$(Atmp, i)
  i = LoadString(g_hLanguageLibHandle, 1001, Atmp, 255)
  If i > 0 Then NomeForm.lblINP(1).Caption = Left$(Atmp, i)
  If (SiStampa = 1) Then
    Printer.ScaleMode = 6
    WRITE_DIALOG NomeForm.OEM1Combo1.Caption & " --> " & Printer.DeviceName
  Else
    NomeForm.OEM1PicCALCOLO.Cls
  End If
Return

errVISUALIZZA_MOLA_INGRANAGGIO:
  WRITE_DIALOG "Sub VISUALIZZA_MOLA_INGRANAGGIO: ERROR -> " & Err
  If FreeFile > 1 Then Close
  Exit Sub

End Sub

Sub RAG3P(ByVal X1 As Double, ByVal Y1 As Double, _
          ByVal X2 As Double, ByVal Y2 As Double, _
          ByVal X3 As Double, ByVal Y3 As Double, _
          ByRef Xc As Double, ByRef YC As Double, ByRef R0 As Double)
'
'  Calcolo raggio per tre punti
'X1 = X(i - 2): X2 = X(i - 1): X3 = X(i)
'Y1 = Y(i - 2): Y2 = Y(i - 1): Y3 = Y(i)
'
Dim X4 As Double, Y4 As Double, X5 As Double, Y5 As Double
Dim M1 As Double, M2 As Double, Q1 As Double, Q2 As Double
'
On Error Resume Next
  '
  If Y2 = Y1 Then Y2 = Y2 + 0.00001
  If Y3 = Y2 Then Y3 = Y3 + 0.00001
  M1 = -((X2 - X1) / (Y2 - Y1))
  M2 = -((X3 - X2) / (Y3 - Y2))
  X4 = (X1 + X2) / 2: Y4 = (Y1 + Y2) / 2
  X5 = (X3 + X2) / 2: Y5 = (Y3 + Y2) / 2
  Q1 = Y4 - M1 * X4: Q2 = Y5 - M2 * X5
  Xc = (Q2 - Q1) / (M1 - M2): YC = M1 * Xc + Q1
  R0 = Sqr((Xc - X2) ^ 2 + (YC - Y2) ^ 2)
  '
End Sub

Sub RMaxRullo(ByVal Fianco As Integer, ByVal HelAng As Double, ByVal MM0 As Double, ByVal idxSAP, _
              ByRef X() As Double, ByRef Y() As Double, _
              ByRef RMaxRulProf, Optional XCRulProf = 0, Optional YCRulProf = 0)
'INPUT:  HelAng=beta, MM0, IDXSAP = indice di MM0 (NP1 + NPE),
'        X() e Y() = COORDINATE DEI PUNTI DELLA MOLA
'OUTPUT: RMaxRulProf = Raggio Max del rullo profilatore,
'        XCRulProf e YCRulProf = Coordinate del centro

Dim X1 As Double, Y1 As Double
Dim X2 As Double, Y2 As Double
Dim X3 As Double, Y3 As Double
Dim Xc As Double, YC As Double, RR As Double
'
On Error GoTo errRMaxRullo
  '
  If Abs(HelAng) < (0.01 * PG / 180) Then
    RR = MM0
  Else
    X1 = X(Fianco, idxSAP - 2): X2 = X(Fianco, idxSAP - 1): X3 = X(Fianco, idxSAP)
    Y1 = Y(Fianco, idxSAP - 2): Y2 = Y(Fianco, idxSAP - 1): Y3 = Y(Fianco, idxSAP)
    Call RAG3P(X1, Y1, X2, Y2, X3, Y3, Xc, YC, RR)
  End If
  RMaxRulProf = RR
  XCRulProf = Xc
  YCRulProf = YC
  '
Exit Sub

errRMaxRullo:
  WRITE_DIALOG "Sub RMaxRullo: ERROR -> " & Err

End Sub

Sub CICLI_VISUALIZZAZIONE_SCANALATI_CBN(SiStampa As String)
'
Dim TITOLO        As String
Dim stmp          As String
Dim Atmp          As String * 255
Dim i             As Integer
Dim j             As Integer
Dim k             As Integer
Dim ret           As Integer
Dim RPC           As Integer
Dim ASP_X         As Single
Dim ASP_C         As Single
Dim DE            As Single
Dim DI            As Single
Dim Altezza       As Single
Dim DB            As Database
Dim TB            As Recordset
Dim FEED1         As Single
Dim FEED2         As Single
Dim sAsportazione As String
Dim TabXl         As Single
Dim TabXt         As Single
Dim FINESTRA      As Object
Dim QwA           As Single
Dim QwR           As Single
Dim TextFont      As String
'
On Error GoTo errCICLI_VISUALIZZAZIONE_SCANALATI_CBN
  '
    If (LINGUA = "CH") Then
    TextFont = "MS Song"
  ElseIf (LINGUA = "RU") Then
    TextFont = "Arial Cyr"
  Else
    TextFont = "Arial"
  End If
  'SELEZIONE DEL TIPO DI PERIFERICA
  If (SiStampa = "N") Then
    OEMX.PicCALCOLO.Cls
    OEMX.PicCALCOLO.FontName = TextFont
    OEMX.PicCALCOLO.FontSize = OEMX.PicCALCOLO.Height / 500
    Set FINESTRA = OEMX.PicCALCOLO
  Else
    Printer.FontName = TextFont
    Printer.FontSize = 12
    Set FINESTRA = Printer
  End If
  'SEGNALAZIONE UTENTE
  i = InStr(Tabella1.INTESTAZIONE(0), ";")
  TITOLO = Left$(Tabella1.INTESTAZIONE(0), i - 1)
  WRITE_DIALOG TITOLO
  'COLONNA DA VISUALIZZARE
  Dim INDICI()   As String
  Dim VV(6, 20)  As String
  Dim LISTA      As String
  Dim ii         As Integer
  Dim Nparametri As Integer
  LISTA = GetInfo("SCANALATI_CBN", "LISTA", Path_LAVORAZIONE_INI)
  LISTA = Trim$(LISTA)
  If (Len(LISTA) > 0) Then
    INDICI = Split(LISTA, ";")
    Nparametri = UBound(INDICI)
    Set DB = OpenDatabase(PathDB, True, False)
    Set TB = DB.OpenRecordset("SCANALATI_CBN", dbOpenTable)
    For ii = 0 To Nparametri
      TB.Index = "INDICE"
      TB.Seek "=", CInt(INDICI(ii))
      VV(0, ii) = TB.Fields("NOME_" & LINGUA)
      i = InStr(VV(0, ii), "|")
      If (i > 0) Then VV(0, ii) = Left$(VV(0, ii), i - 1)
      VV(1, ii) = Split(TB.Fields("VARIABILE"), ";")(0): VV(1, ii) = LEP_STR(VV(1, ii))
      VV(2, ii) = Split(TB.Fields("VARIABILE"), ";")(1): VV(2, ii) = LEP_STR(VV(2, ii))
      VV(3, ii) = Split(TB.Fields("VARIABILE"), ";")(2): VV(3, ii) = LEP_STR(VV(3, ii))
      VV(4, ii) = Split(TB.Fields("VARIABILE"), ";")(3): VV(4, ii) = LEP_STR(VV(4, ii))
      VV(5, ii) = Split(TB.Fields("VARIABILE"), ";")(4): VV(5, ii) = LEP_STR(VV(5, ii))
      VV(6, ii) = Split(TB.Fields("VARIABILE"), ";")(5): VV(6, ii) = LEP_STR(VV(6, ii))
    Next ii
    TB.Close
    DB.Close
  Else
    WRITE_DIALOG "ERROR: CYCLE LIST HAVE NOT BEEN DEFINED!!!"
    Exit Sub
  End If
  FINESTRA.ScaleWidth = 190
  FINESTRA.ScaleHeight = 95
  DE = Abs(Lp("R[44]"))   'DIAMETRO ESTERNO
  DI = Abs(Lp("R[103]"))  'DIAMETRO INTERNO
  Altezza = (DE - DI) / 2      'ALTEZZA PROFILO
  '
  TabXt = 0
  '
  i = 0
    '
    TabXl = 0
      'INTESTAZIONE DEI CICLI
      stmp = " " & TITOLO & " "
      FINESTRA.CurrentX = TabXt
      FINESTRA.CurrentY = 0.5 * FINESTRA.TextHeight(stmp)
      FINESTRA.Print stmp
      If (TabXl < FINESTRA.TextWidth(stmp)) Then TabXl = FINESTRA.TextWidth(stmp)
      'CAMPI DEI CICLI
      For j = 0 To Nparametri
        stmp = " " & VV(0, j) & " "
        FINESTRA.CurrentX = TabXt
        FINESTRA.CurrentY = 0 + (j + 2) * FINESTRA.TextHeight(stmp)
        FINESTRA.Print stmp
        If (TabXl < FINESTRA.TextWidth(stmp)) Then TabXl = FINESTRA.TextWidth(stmp)
      Next j
      'VISUALIZZO ASPORTAZIONI SPECIFICHE
      j = Nparametri + 1
      stmp = " Qwa "
      FINESTRA.CurrentX = TabXt
      FINESTRA.CurrentY = 0 + (j + 2) * FINESTRA.TextHeight(stmp)
      FINESTRA.Print stmp
      If (TabXl < FINESTRA.TextWidth(stmp)) Then TabXl = FINESTRA.TextWidth(stmp)
      j = Nparametri + 2
      stmp = " Qwr "
      FINESTRA.CurrentX = TabXt
      FINESTRA.CurrentY = 0 + (j + 2) * FINESTRA.TextHeight(stmp)
      FINESTRA.Print stmp
      'VISUALIZZO L'ASPORTAZIONE SULLA FINESTRA
      ret = LoadString(g_hLanguageLibHandle, 1011, Atmp, 255)
      If (ret > 0) Then sAsportazione = Left$(Atmp, ret)
      stmp = " " & sAsportazione
      j = Nparametri + 3
      FINESTRA.CurrentX = TabXt
      FINESTRA.CurrentY = 0 + (j + 2) * FINESTRA.TextHeight(stmp)
      FINESTRA.Print stmp
    '
    TabXt = TabXt + TabXl
    'CASELLE
    FINESTRA.Line (0, 0)-(TabXt, (j + 3) * FINESTRA.TextHeight(stmp)), , B
    '
  '
  For i = 1 To 6
    TabXl = 0
    'INTESTAZIONE DEI CICLI
    stmp = " " & "C" & Format$(i, "#0") & " "
    FINESTRA.CurrentX = TabXt
    FINESTRA.CurrentY = 0.5 * FINESTRA.TextHeight(stmp)
    FINESTRA.Print stmp
    If (TabXl < FINESTRA.TextWidth(stmp)) Then TabXl = FINESTRA.TextWidth(stmp)
    'CAMPI DEI CICLI
    For j = 0 To Nparametri
      If (j = 7) Then
        stmp = " M" & VV(i, j) & " "
      Else
        stmp = " " & VV(i, j) & " "
      End If
      FINESTRA.CurrentX = TabXt
      FINESTRA.CurrentY = 0 + (j + 2) * FINESTRA.TextHeight(stmp)
      FINESTRA.Print stmp
      If (TabXl < FINESTRA.TextWidth(stmp)) Then TabXl = FINESTRA.TextWidth(stmp)
    Next j
    'ASPORTAZIONI SPECIFICHE
    j = Nparametri + 1
    stmp = " " & frmt(VV(i, 2) * VV(i, 3) / 60, 1) & " "
    FINESTRA.CurrentX = TabXt
    FINESTRA.CurrentY = 0 + (j + 2) * FINESTRA.TextHeight(stmp)
    FINESTRA.Print stmp
    If (TabXl < FINESTRA.TextWidth(stmp)) Then TabXl = FINESTRA.TextWidth(stmp)
    j = Nparametri + 2
    stmp = " " & frmt(VV(i, 4) * VV(i, 5) / 60, 1) & " "
    FINESTRA.CurrentX = TabXt
    FINESTRA.CurrentY = 0 + (j + 2) * FINESTRA.TextHeight(stmp)
    FINESTRA.Print stmp
    'INCREMENTI
    If (VV(i, 0) <> "0") Then
      RPC = val(VV(i, 1))
      FEED1 = val(VV(i, 3))
      FEED2 = val(VV(i, 5))
      'CALCOLO DELL'ASPORTAZIONE TOTALE DEI CICLI
      Select Case VV(i, 0)
        Case 30
         ASP_C = ASP_C + RPC * (val(VV(i, 2)) + val(VV(i, 4)))
         stmp = " " & frmt(RPC * (val(VV(i, 2)) + val(VV(i, 4))), 4) & " "
        Case 13
         ASP_X = ASP_X + RPC * val(VV(i, 2))
         stmp = " " & frmt(RPC * val(VV(i, 2)), 4) & " "
        Case Else
         ASP_X = ASP_X + RPC * (val(VV(i, 2)) + val(VV(i, 4)))
         stmp = " " & frmt(RPC * (val(VV(i, 2)) + val(VV(i, 4))), 4) & " "
      End Select
    Else
      stmp = " 0 "
    End If
    'VISUALIZZO L'ASPORTAZIONE SULLA FINESTRA
    j = Nparametri + 3
    FINESTRA.CurrentX = TabXt
    FINESTRA.CurrentY = 0 + (j + 2) * FINESTRA.TextHeight(stmp)
    FINESTRA.Print stmp
    'INCREMENTO TABULAZIONE LATERALE
    TabXt = TabXt + TabXl
    'CASELLE
    FINESTRA.Line (0, 0)-(TabXt, (j + 3) * FINESTRA.TextHeight(stmp)), , B
  Next i
  j = 2
  FINESTRA.Line (0, j * FINESTRA.TextHeight(stmp))-(TabXt, j * FINESTRA.TextHeight(stmp))
  j = 11
  FINESTRA.Line (0, j * FINESTRA.TextHeight(stmp))-(TabXt, j * FINESTRA.TextHeight(stmp))
  '
  'TOTALE DEI CICLI X
  stmp = " " & sAsportazione & " X --> " & frmt(ASP_X, 4) & "mm "
  FINESTRA.CurrentX = 0
  FINESTRA.CurrentY = (0.5 + 14) * FINESTRA.TextHeight(stmp)
  FINESTRA.Print stmp
  'TOTALE RICHIESTO DEI CICLI X
  stmp = sAsportazione & "(THEOR.) = " & frmt(Altezza, 4) & "mm "
  FINESTRA.CurrentX = FINESTRA.ScaleWidth / 2
  FINESTRA.CurrentY = (0.5 + 14) * FINESTRA.TextHeight(stmp)
  FINESTRA.Print stmp
  'TOTALE DEI CICLI C
  stmp = " " & sAsportazione & " C --> " & frmt(ASP_C, 4) & "mm "
  FINESTRA.CurrentX = 0
  FINESTRA.CurrentY = (0.5 + 16) * FINESTRA.TextHeight(stmp)
  FINESTRA.Print stmp
  '
  'SEGNALAZIONE UTENTE
  If (Altezza > ASP_X) Then
    WRITE_DIALOG "WARNING: INCREASE CYCLE OF " & frmt(Altezza - ASP_X, 4) & "mm"
  Else
    WRITE_DIALOG ""
  End If
  '
Exit Sub

errCICLI_VISUALIZZAZIONE_SCANALATI_CBN:
  WRITE_DIALOG "Sub CICLI_VISUALIZZAZIONE_SCANALATI_CBN: ERROR -> " & Err
Resume Next

End Sub

Sub CICLI_VISUALIZZAZIONE_SCANALATI_INTERNI_CBN(SiStampa As String)
'
Dim TITOLO        As String
Dim stmp          As String
Dim Atmp          As String * 255
Dim i             As Integer
Dim j             As Integer
Dim k             As Integer
Dim ret           As Integer
Dim RPC           As Integer
Dim ASP_X         As Single
Dim ASP_C         As Single
Dim DE            As Single
Dim DI            As Single
Dim Altezza       As Single
Dim DB            As Database
Dim TB            As Recordset
Dim FEED1         As Single
Dim FEED2         As Single
Dim sAsportazione As String
Dim TabXl         As Single
Dim TabXt         As Single
Dim FINESTRA      As Object
Dim QwA           As Single
Dim QwR           As Single
Dim TextFont      As String
'
On Error GoTo errCICLI_VISUALIZZAZIONE_SCANALATI_INTERNI_CBN
  '
    If (LINGUA = "CH") Then
    TextFont = "MS Song"
  ElseIf (LINGUA = "RU") Then
    TextFont = "Arial Cyr"
  Else
    TextFont = "Arial"
  End If
  'SELEZIONE DEL TIPO DI PERIFERICA
  If (SiStampa = "N") Then
    OEMX.PicCALCOLO.Cls
    OEMX.PicCALCOLO.FontName = TextFont
    OEMX.PicCALCOLO.FontSize = OEMX.PicCALCOLO.Height / 500
    Set FINESTRA = OEMX.PicCALCOLO
  Else
    Printer.FontName = TextFont
    Printer.FontSize = 12
    Set FINESTRA = Printer
  End If
  'SEGNALAZIONE UTENTE
  i = InStr(Tabella1.INTESTAZIONE(0), ";")
  TITOLO = Left$(Tabella1.INTESTAZIONE(0), i - 1)
  WRITE_DIALOG TITOLO
  'COLONNA DA VISUALIZZARE
  Dim INDICI()   As String
  Dim VV(6, 20)  As String
  Dim LISTA      As String
  Dim ii         As Integer
  Dim Nparametri As Integer
  LISTA = GetInfo("SCANALATI_INTERNI_CBN", "LISTA", Path_LAVORAZIONE_INI)
  LISTA = Trim$(LISTA)
  If (Len(LISTA) > 0) Then
    INDICI = Split(LISTA, ";")
    Nparametri = UBound(INDICI)
    Set DB = OpenDatabase(PathDB, True, False)
    Set TB = DB.OpenRecordset("SCANALATI_INTERNI_CBN", dbOpenTable)
    For ii = 0 To Nparametri
      TB.Index = "INDICE"
      TB.Seek "=", CInt(INDICI(ii))
      VV(0, ii) = TB.Fields("NOME_" & LINGUA)
      i = InStr(VV(0, ii), "|")
      If (i > 0) Then VV(0, ii) = Left$(VV(0, ii), i - 1)
      VV(1, ii) = Split(TB.Fields("VARIABILE"), ";")(0): VV(1, ii) = LEP_STR(VV(1, ii))
      VV(2, ii) = Split(TB.Fields("VARIABILE"), ";")(1): VV(2, ii) = LEP_STR(VV(2, ii))
      VV(3, ii) = Split(TB.Fields("VARIABILE"), ";")(2): VV(3, ii) = LEP_STR(VV(3, ii))
      VV(4, ii) = Split(TB.Fields("VARIABILE"), ";")(3): VV(4, ii) = LEP_STR(VV(4, ii))
      VV(5, ii) = Split(TB.Fields("VARIABILE"), ";")(4): VV(5, ii) = LEP_STR(VV(5, ii))
      VV(6, ii) = Split(TB.Fields("VARIABILE"), ";")(5): VV(6, ii) = LEP_STR(VV(6, ii))
    Next ii
    TB.Close
    DB.Close
  Else
    WRITE_DIALOG "ERROR: CYCLE LIST HAVE NOT BEEN DEFINED!!!"
    Exit Sub
  End If
  FINESTRA.ScaleWidth = 190
  FINESTRA.ScaleHeight = 95
  DE = Abs(Lp("R[45]"))   'DIAMETRO INTERNO
  DI = Abs(Lp("R[38]"))  'DIAMETRO ESTERNO
  Altezza = (DE - DI) / 2      'ALTEZZA PROFILO
  '
  TabXt = 0
  '
  i = 0
    '
    TabXl = 0
      'INTESTAZIONE DEI CICLI
      stmp = " " & TITOLO & " "
      FINESTRA.CurrentX = TabXt
      FINESTRA.CurrentY = 0.5 * FINESTRA.TextHeight(stmp)
      FINESTRA.Print stmp
      If (TabXl < FINESTRA.TextWidth(stmp)) Then TabXl = FINESTRA.TextWidth(stmp)
      'CAMPI DEI CICLI
      For j = 0 To Nparametri
        stmp = " " & VV(0, j) & " "
        FINESTRA.CurrentX = TabXt
        FINESTRA.CurrentY = 0 + (j + 2) * FINESTRA.TextHeight(stmp)
        FINESTRA.Print stmp
        If (TabXl < FINESTRA.TextWidth(stmp)) Then TabXl = FINESTRA.TextWidth(stmp)
      Next j
      'VISUALIZZO ASPORTAZIONI SPECIFICHE
      j = Nparametri + 1
      stmp = " Qwa "
      FINESTRA.CurrentX = TabXt
      FINESTRA.CurrentY = 0 + (j + 2) * FINESTRA.TextHeight(stmp)
      FINESTRA.Print stmp
      If (TabXl < FINESTRA.TextWidth(stmp)) Then TabXl = FINESTRA.TextWidth(stmp)
      j = Nparametri + 2
      stmp = " Qwr "
      FINESTRA.CurrentX = TabXt
      FINESTRA.CurrentY = 0 + (j + 2) * FINESTRA.TextHeight(stmp)
      FINESTRA.Print stmp
      'VISUALIZZO L'ASPORTAZIONE SULLA FINESTRA
      ret = LoadString(g_hLanguageLibHandle, 1011, Atmp, 255)
      If (ret > 0) Then sAsportazione = Left$(Atmp, ret)
      stmp = " " & sAsportazione
      j = Nparametri + 3
      FINESTRA.CurrentX = TabXt
      FINESTRA.CurrentY = 0 + (j + 2) * FINESTRA.TextHeight(stmp)
      FINESTRA.Print stmp
    '
    TabXt = TabXt + TabXl
    'CASELLE
    FINESTRA.Line (0, 0)-(TabXt, (j + 3) * FINESTRA.TextHeight(stmp)), , B
    '
  '
  For i = 1 To 6
    TabXl = 0
    'INTESTAZIONE DEI CICLI
    stmp = " " & "C" & Format$(i, "#0") & " "
    FINESTRA.CurrentX = TabXt
    FINESTRA.CurrentY = 0.5 * FINESTRA.TextHeight(stmp)
    FINESTRA.Print stmp
    If (TabXl < FINESTRA.TextWidth(stmp)) Then TabXl = FINESTRA.TextWidth(stmp)
    'CAMPI DEI CICLI
    For j = 0 To Nparametri
      If (j = 7) Then
        stmp = " M" & VV(i, j) & " "
      Else
        stmp = " " & VV(i, j) & " "
      End If
      FINESTRA.CurrentX = TabXt
      FINESTRA.CurrentY = 0 + (j + 2) * FINESTRA.TextHeight(stmp)
      FINESTRA.Print stmp
      If (TabXl < FINESTRA.TextWidth(stmp)) Then TabXl = FINESTRA.TextWidth(stmp)
    Next j
    'ASPORTAZIONI SPECIFICHE
    j = Nparametri + 1
    stmp = " " & frmt(VV(i, 2) * VV(i, 3) / 60, 1) & " "
    FINESTRA.CurrentX = TabXt
    FINESTRA.CurrentY = 0 + (j + 2) * FINESTRA.TextHeight(stmp)
    FINESTRA.Print stmp
    If (TabXl < FINESTRA.TextWidth(stmp)) Then TabXl = FINESTRA.TextWidth(stmp)
    j = Nparametri + 2
    stmp = " " & frmt(VV(i, 4) * VV(i, 5) / 60, 1) & " "
    FINESTRA.CurrentX = TabXt
    FINESTRA.CurrentY = 0 + (j + 2) * FINESTRA.TextHeight(stmp)
    FINESTRA.Print stmp
    'INCREMENTI
    If (VV(i, 0) <> "0") Then
      RPC = val(VV(i, 1))
      FEED1 = val(VV(i, 3))
      FEED2 = val(VV(i, 5))
      'CALCOLO DELL'ASPORTAZIONE TOTALE DEI CICLI
      Select Case VV(i, 0)
        Case 30
         ASP_C = ASP_C + RPC * (val(VV(i, 2)) + val(VV(i, 4)))
         stmp = " " & frmt(RPC * (val(VV(i, 2)) + val(VV(i, 4))), 4) & " "
        Case 13
         ASP_X = ASP_X + RPC * val(VV(i, 2))
         stmp = " " & frmt(RPC * val(VV(i, 2)), 4) & " "
        Case Else
         ASP_X = ASP_X + RPC * (val(VV(i, 2)) + val(VV(i, 4)))
         stmp = " " & frmt(RPC * (val(VV(i, 2)) + val(VV(i, 4))), 4) & " "
      End Select
    Else
      stmp = " 0 "
    End If
    'VISUALIZZO L'ASPORTAZIONE SULLA FINESTRA
    j = Nparametri + 3
    FINESTRA.CurrentX = TabXt
    FINESTRA.CurrentY = 0 + (j + 2) * FINESTRA.TextHeight(stmp)
    FINESTRA.Print stmp
    'INCREMENTO TABULAZIONE LATERALE
    TabXt = TabXt + TabXl
    'CASELLE
    FINESTRA.Line (0, 0)-(TabXt, (j + 3) * FINESTRA.TextHeight(stmp)), , B
  Next i
  j = 2
  FINESTRA.Line (0, j * FINESTRA.TextHeight(stmp))-(TabXt, j * FINESTRA.TextHeight(stmp))
  j = 11
  FINESTRA.Line (0, j * FINESTRA.TextHeight(stmp))-(TabXt, j * FINESTRA.TextHeight(stmp))
  '
  'TOTALE DEI CICLI X
  stmp = " " & sAsportazione & " X --> " & frmt(ASP_X, 4) & "mm "
  FINESTRA.CurrentX = 0
  FINESTRA.CurrentY = (0.5 + 14) * FINESTRA.TextHeight(stmp)
  FINESTRA.Print stmp
  'TOTALE RICHIESTO DEI CICLI X
  stmp = sAsportazione & "(THEOR.) = " & frmt(Altezza, 4) & "mm "
  FINESTRA.CurrentX = FINESTRA.ScaleWidth / 2
  FINESTRA.CurrentY = (0.5 + 14) * FINESTRA.TextHeight(stmp)
  FINESTRA.Print stmp
  'TOTALE DEI CICLI C
  stmp = " " & sAsportazione & " C --> " & frmt(ASP_C, 4) & "mm "
  FINESTRA.CurrentX = 0
  FINESTRA.CurrentY = (0.5 + 16) * FINESTRA.TextHeight(stmp)
  FINESTRA.Print stmp
  '
  'SEGNALAZIONE UTENTE
  If (Altezza > ASP_X) Then
    WRITE_DIALOG "WARNING: INCREASE CYCLE OF " & frmt(Altezza - ASP_X, 4) & "mm"
  Else
    WRITE_DIALOG ""
  End If
  '
Exit Sub

errCICLI_VISUALIZZAZIONE_SCANALATI_INTERNI_CBN:
  WRITE_DIALOG "Sub CICLI_VISUALIZZAZIONE_SCANALATI_INTERNI_CBN: ERROR -> " & Err
Resume Next
End Sub
