Attribute VB_Name = "ModOperate"
'Public SuOP As New SUOperate45.SUOperate
Public SuOP As New SUOperate
'
Public sDeb As String

' Variabili per gestione ridimensionamento
Private Type ControlPositionType
    Left      As Single
    Top       As Single
    Width     As Single
    Height    As Single
    FontSize  As Single
End Type

Private m_ControlPositions() As ControlPositionType
Private m_FormWid As Single
Private m_FormHgt As Single

Public Frms As Collection

Public SUOP_Language As String

'Funzioni HEADER
Function VISUALIZZA_NOME_PEZZO() As String
'
Dim sDDE                  As String
Dim sMACRO                As String
Dim MACRO                 As Long
Dim sLAVORAZIONE          As String
Dim LAVORAZIONE           As Long
Dim TIPO_LAVORAZIONE      As String
Dim NomePezzo             As String
Dim Path_LAVORAZIONE_INI  As String
Dim stmp                  As String
Dim NomeMACRO             As String
Dim COMBINAZIONI_DB       As String
Dim sNOME_PROGRAMMA       As String
'Dim DB                    As Database
'Dim RS                    As Recordset
'
On Error Resume Next
  '
  sDDE = GetInfo(SEZIONE, "sDDE_MACRO", g_chOemPATH & "\" & NomeFile)
  If (Len(sDDE) > 0) Then
    '
    'LETTURA DEL VALORE MACRO
    sMACRO = OPC_LEGGI_DATO(sDDE): MACRO = val(sMACRO)
    If (MACRO < 0) Then
      '
      'LAVORAZIONE SINGOLA: IL VALORE DI MACRO CORRISPONDE ALLA SEQUENZA
      'DI TIPO_LAVORAZIONE DICHIARATA IN GAPP4.INI CAMBIATA DI SEGNO
      '
      'LETTURA DEL VALORE LAVORAZIONE
      sDDE = GetInfo(SEZIONE, "sDDE_LAVORAZIONE_SINGOLA", g_chOemPATH & "\" & NomeFile)
      If (Len(sDDE) > 0) Then
        sLAVORAZIONE = OPC_LEGGI_DATO(sDDE): LAVORAZIONE = val(sLAVORAZIONE)
      Else
        LAVORAZIONE = 0
      End If
      If (LAVORAZIONE > 0) Then
        'INDICE PROGRAMMA  SINGOLO
        MACRO = Abs(MACRO)
        'LETTURA DEL TIPO LAVORAZIONE
        TIPO_LAVORAZIONE = GetInfo("TIPO_LAVORAZIONE", "TIPO(" & Format$(MACRO, "####0") & ")", g_chOemPATH & "\" & NomeFile)
        TIPO_LAVORAZIONE = Trim$(UCase$(TIPO_LAVORAZIONE))
        'LETTURA DEL PERCORSO FILE INI
        Path_LAVORAZIONE_INI = GetInfo("TIPO_LAVORAZIONE", "FILEINI(" & Format$(MACRO, "####0") & ")", g_chOemPATH & "\" & NomeFile)
        Path_LAVORAZIONE_INI = g_chOemPATH & "\" & Path_LAVORAZIONE_INI & ".INI"
        'LETTURA DEL NOME DEL PEZZO SINGOLO
        NomePezzo = GetInfo(TIPO_LAVORAZIONE, "NomePezzo(" & sLAVORAZIONE & ")", Path_LAVORAZIONE_INI)
        NomePezzo = UCase$(Trim$(NomePezzo))
        'LEGGO IL NOME DEL TIPO DAL FILE INI
        stmp = GetInfo("TIPO_LAVORAZIONE", "NOMETIPO(" & Format$(MACRO, "####0") & ")", g_chOemPATH & "\" & NomeFile)
        'COSTRUISCO LA STRINGA DI VISUALIZZAZIONE
        NomePezzo = stmp & "--> [" & Chr(64 + LAVORAZIONE) & "] " & NomePezzo
      Else
        'COMPATIBILITA' CON IL VECCHIO HEADER
        NomePezzo = GetInfo(SEZIONE, Campo, g_chOemPATH & "\" & NomeFile)
      End If
      '
    ElseIf (MACRO = 0) Then
      '
      'COMPATIBILITA' CON IL VECCHIO HEADER
      NomePezzo = GetInfo(SEZIONE, Campo, g_chOemPATH & "\" & NomeFile)
      '
    ElseIf (MACRO >= 1) Then
      '
      'LAVORAZIONE COMBINATA: IL VALORE DI MACRO CORRISPONDE ALLA SEQUENZA
      ' DELLE COMBINAZIONI DICHIARATA DEL FILE GAPP4.INI
      '
      'LETTURA DEL NOME PEZZO DELLA COMBINAZIONE
      NomePezzo = GetInfo("COMBINAZIONE(" & sMACRO & ")", "NomePezzo", g_chOemPATH & "\" & NomeFile)
      NomePezzo = UCase$(Trim$(NomePezzo))
      'LETTURA NOME TIPO
      NomeMACRO = GetInfo("COMBINAZIONE(" & sMACRO & ")", "NomeMACRO", g_chOemPATH & "\" & NomeFile)
      NomeMACRO = UCase$(Trim$(NomeMACRO))
      'LETTURA DEL VALORE LAVORAZIONE
      sDDE = GetInfo(SEZIONE, "sDDE_LAVORAZIONE_SINGOLA", g_chOemPATH & "\" & NomeFile)
      If (Len(sDDE) > 0) Then
        sLAVORAZIONE = OPC_LEGGI_DATO(sDDE): LAVORAZIONE = val(sLAVORAZIONE)
      Else
        LAVORAZIONE = 0
      End If
      If (LAVORAZIONE > 0) Then
        'LETTURA DEL TIPO LAVORAZIONE
        TIPO_LAVORAZIONE = GetInfo("COMBINAZIONE(" & sMACRO & ")", "TIPO(" & sLAVORAZIONE & ")", g_chOemPATH & "\" & NomeFile)
        'LETTURA DEL PERCORSO FILE INI
        Path_LAVORAZIONE_INI = GetInfo("COMBINAZIONE(" & sMACRO & ")", "FILEINI(" & sLAVORAZIONE & ")", g_chOemPATH & "\" & NomeFile)
        Path_LAVORAZIONE_INI = g_chOemPATH & "\" & Path_LAVORAZIONE_INI & ".INI"
        'LETTURA DEL NOME DEL PEZZO SINGOLO
        stmp = GetInfo(TIPO_LAVORAZIONE, "NomePezzo(" & sLAVORAZIONE & ")", Path_LAVORAZIONE_INI)
        stmp = UCase$(Trim$(stmp))
        'COSTRUISCO LA STRINGA DI VISUALIZZAZIONE
        NomePezzo = "[" & sMACRO & "] " & NomeMACRO & " ( " & NomePezzo & " -> [" & Chr(64 + LAVORAZIONE) & "] " & stmp & " )"
      End If
      '
    End If
    '
  Else
    '
    'COMPATIBILITA' CON IL VECCHIO HEADER
    NomePezzo = GetInfo(SEZIONE, Campo, g_chOemPATH & "\" & NomeFile)
    '
  End If
  VISUALIZZA_NOME_PEZZO = NomePezzo
  '
End Function

Function LEGGI_NOME_PEZZO() As String
'
Dim sDDE                  As String
Dim sMACRO                As String
Dim MACRO                 As Long
Dim sLAVORAZIONE          As String
Dim LAVORAZIONE           As Long
Dim TIPO_LAVORAZIONE      As String
'
Dim Path_LAVORAZIONE_INI  As String
Dim stmp                  As String
Dim NomeMACRO             As String
Dim COMBINAZIONI_DB       As String
Dim sNOME_PROGRAMMA       As String
'Dim DB                    As Database
'Dim RS                    As Recordset
'
On Error Resume Next
  '
  sDDE = GetInfo(SEZIONE, "sDDE_MACRO", g_chOemPATH & "\" & NomeFile)
  If (Len(sDDE) > 0) Then
    '
    'LETTURA DEL VALORE MACRO
    sMACRO = OPC_LEGGI_DATO(sDDE): MACRO = val(sMACRO)
    If (MACRO < 0) Then
      '
      'LAVORAZIONE SINGOLA: IL VALORE DI MACRO CORRISPONDE ALLA SEQUENZA
      'DI TIPO_LAVORAZIONE DICHIARATA IN GAPP4.INI CAMBIATA DI SEGNO
      '
      'LETTURA DEL VALORE LAVORAZIONE
      sDDE = GetInfo(SEZIONE, "sDDE_LAVORAZIONE_SINGOLA", g_chOemPATH & "\" & NomeFile)
      If (Len(sDDE) > 0) Then
        sLAVORAZIONE = OPC_LEGGI_DATO(sDDE): LAVORAZIONE = val(sLAVORAZIONE)
      Else
        LAVORAZIONE = 0
      End If
      If (LAVORAZIONE > 0) Then
        'INDICE PROGRAMMA  SINGOLO
        MACRO = Abs(MACRO)
        'LETTURA DEL TIPO LAVORAZIONE
        TIPO_LAVORAZIONE = GetInfo("TIPO_LAVORAZIONE", "TIPO(" & Format$(MACRO, "####0") & ")", g_chOemPATH & "\" & NomeFile)
        TIPO_LAVORAZIONE = Trim$(UCase$(TIPO_LAVORAZIONE))
        'LETTURA DEL PERCORSO FILE INI
        Path_LAVORAZIONE_INI = GetInfo("TIPO_LAVORAZIONE", "FILEINI(" & Format$(MACRO, "####0") & ")", g_chOemPATH & "\" & NomeFile)
        Path_LAVORAZIONE_INI = g_chOemPATH & "\" & Path_LAVORAZIONE_INI & ".INI"
        'LETTURA DEL NOME DEL PEZZO SINGOLO
        LEGGI_NOME_PEZZO = GetInfo(TIPO_LAVORAZIONE, "NomePezzo(" & sLAVORAZIONE & ")", Path_LAVORAZIONE_INI)
        LEGGI_NOME_PEZZO = UCase$(Trim$(LEGGI_NOME_PEZZO))
      Else
        'COMPATIBILITA' CON IL VECCHIO HEADER
        LEGGI_NOME_PEZZO = GetInfo(SEZIONE, Campo, g_chOemPATH & "\" & NomeFile)
      End If
      '
    ElseIf (MACRO = 0) Then
      '
      'COMPATIBILITA' CON IL VECCHIO HEADER
      LEGGI_NOME_PEZZO = GetInfo(SEZIONE, Campo, g_chOemPATH & "\" & NomeFile)
      '
    ElseIf (MACRO >= 1) Then
      '
      'LAVORAZIONE COMBINATA: IL VALORE DI MACRO CORRISPONDE ALLA SEQUENZA
      ' DELLE COMBINAZIONI DICHIARATA DEL FILE GAPP4.INI
      '
      'LETTURA DEL NOME PEZZO DELLA COMBINAZIONE
      LEGGI_NOME_PEZZO = GetInfo("COMBINAZIONE(" & sMACRO & ")", "NomePezzo", g_chOemPATH & "\" & NomeFile)
      LEGGI_NOME_PEZZO = UCase$(Trim$(LEGGI_NOME_PEZZO))
      '
    End If
    '
  Else
    '
    'COMPATIBILITA' CON IL VECCHIO HEADER
    LEGGI_NOME_PEZZO = GetInfo(SEZIONE, Campo, g_chOemPATH & "\" & NomeFile)
    '
  End If
  '
End Function

Public Sub SetFocusFRM()

On Error Resume Next

   If Not frmLast Is Nothing Then frmLast.SetFocus

End Sub
