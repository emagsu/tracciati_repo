VERSION 5.00
Begin VB.Form MNG3 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BackColor       =   &H00EBE1D7&
   BorderStyle     =   0  'None
   Caption         =   "MNG3"
   ClientHeight    =   6912
   ClientLeft      =   2136
   ClientTop       =   1428
   ClientWidth     =   10260
   ForeColor       =   &H80000008&
   Icon            =   "Mng3.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6912
   ScaleWidth      =   10260
   ShowInTaskbar   =   0   'False
   Tag             =   "MNG3"
   Begin VB.Frame fra_SELEZIONE 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBE1D7&
      Caption         =   "fra_SELEZIONE"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   4900
      Left            =   3240
      TabIndex        =   4
      Top             =   3720
      Visible         =   0   'False
      Width           =   4500
      Begin VB.DirListBox DIRECTORY_SELEZIONE 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4104
         Left            =   50
         TabIndex        =   6
         Top             =   720
         Width           =   4400
      End
      Begin VB.DriveListBox DRIVE_SELEZIONE 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   324
         Left            =   50
         TabIndex        =   5
         Top             =   315
         Width           =   4400
      End
   End
   Begin VB.ListBox List1 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   10.2
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3576
      Index           =   1
      Left            =   4900
      MultiSelect     =   2  'Extended
      Sorted          =   -1  'True
      TabIndex        =   1
      Top             =   1000
      Width           =   4700
   End
   Begin VB.ListBox List1 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   10.2
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3576
      Index           =   0
      Left            =   100
      MultiSelect     =   2  'Extended
      Sorted          =   -1  'True
      TabIndex        =   0
      Top             =   1000
      Width           =   4700
   End
   Begin VB.Image Image4 
      BorderStyle     =   1  'Fixed Single
      Height          =   432
      Left            =   120
      Picture         =   "Mng3.frx":030A
      Top             =   5160
      Width           =   432
   End
   Begin VB.Label lblPATH 
      BorderStyle     =   1  'Fixed Single
      Caption         =   "lblPATH"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   10.2
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   480
      Index           =   0
      Left            =   1920
      TabIndex        =   8
      Top             =   0
      Width           =   1068
      WordWrap        =   -1  'True
   End
   Begin VB.Image Image2 
      BorderStyle     =   1  'Fixed Single
      Height          =   432
      Left            =   9120
      Picture         =   "Mng3.frx":074C
      Top             =   5040
      Width           =   432
   End
   Begin VB.Label lblPATH 
      Alignment       =   1  'Right Justify
      BorderStyle     =   1  'Fixed Single
      Caption         =   "lblPATH"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   10.2
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   480
      Index           =   1
      Left            =   960
      TabIndex        =   7
      Top             =   5160
      Width           =   3972
      WordWrap        =   -1  'True
   End
   Begin VB.Image Image3 
      BorderStyle     =   1  'Fixed Single
      Height          =   432
      Left            =   108
      Picture         =   "Mng3.frx":0B8E
      Top             =   36
      Width           =   432
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "MS Song"
         Size            =   12
         Charset         =   134
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   415
      Index           =   1
      Left            =   4900
      TabIndex        =   3
      Top             =   600
      Width           =   4700
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "MS Song"
         Size            =   12
         Charset         =   134
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   415
      Index           =   0
      Left            =   100
      TabIndex        =   2
      Top             =   600
      Width           =   4700
   End
   Begin VB.Image Image1 
      BorderStyle     =   1  'Fixed Single
      Height          =   500
      Left            =   8880
      Picture         =   "Mng3.frx":0FD0
      Stretch         =   -1  'True
      Top             =   0
      Width           =   500
   End
End
Attribute VB_Name = "MNG3"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Private Sub Form_Activate()
  '
  MNG3.List1(0).BackColor = &HFFFFFF
  MNG3.List1(1).BackColor = &HFFFFFF
  Call ChildActivate(Me)
  
  MNG3.Label1(0).Left = 100
  MNG3.Label1(0).Width = MNG3.Width / 2 - 200
  MNG3.List1(0).Left = MNG3.Label1(0).Left
  MNG3.List1(0).Width = MNG3.Label1(0).Width
  
  MNG3.List1(0).Height = MNG3.Height - MNG3.Label1(0).Height - MNG3.lblPATH(0).Height - MNG3.lblPATH(1).Height - 200

  MNG3.Label1(1).Left = MNG3.Label1(0).Left + MNG3.Label1(0).Width + 100
  MNG3.Label1(1).Width = MNG3.Width / 2 - 200
  MNG3.List1(1).Left = MNG3.Label1(1).Left
  MNG3.List1(1).Width = MNG3.Label1(1).Width
  
  
  'ALLINEO L'ALTEZZA DELLE LISTE
  If (MNG3.List1(0).Height > MNG3.List1(1).Height) Then
    MNG3.List1(1).Height = MNG3.List1(0).Height
  Else
    MNG3.List1(0).Height = MNG3.List1(1).Height
  End If
  'POSIZIONO L'IMMAGINE DEL PERCORSO
  MNG3.Image3.Top = MNG3.Label1(0).Top - MNG3.Image3.Height - 50
  MNG3.Image3.Left = MNG3.Label1(0).Left
  'POSIZIONO L'IMMAGINE SAMP
  MNG3.Image1.Height = MNG3.Image3.Height
  MNG3.Image1.Width = MNG3.Image3.Height
  MNG3.Image1.Top = MNG3.Image3.Top
  MNG3.Image1.Left = (MNG3.List1(1).Left + MNG3.List1(1).Width) - MNG3.Image1.Width
  'POSIZIONO LA LABEL DEL PERCORSO
  MNG3.lblPATH(0).Top = MNG3.Image3.Top
  MNG3.lblPATH(0).Left = MNG3.Image3.Left + MNG3.Image3.Width
  MNG3.lblPATH(0).Width = MNG3.Image1.Left - (MNG3.Image3.Left + MNG3.Image3.Width)
  MNG3.lblPATH(0).Height = MNG3.Image3.Height
  'POSIZIONO L'IMMAGINE FINESTRA
  MNG3.Image4.Top = MNG3.List1(0).Top + MNG3.List1(0).Height + 100
  MNG3.Image4.Left = MNG3.List1(0).Left
  'POSIZIONO L'IMMAGINE DEL PERCORSO 2
  MNG3.Image2.Top = MNG3.List1(1).Top + MNG3.List1(1).Height + 100
  MNG3.Image2.Left = MNG3.List1(1).Left + MNG3.List1(1).Width - MNG3.Image2.Width
  'POSIZIONO LA LABEL DEL PERCORSO 2
  MNG3.lblPATH(1).Top = MNG3.List1(1).Top + MNG3.List1(1).Height + 100
  MNG3.lblPATH(1).Left = MNG3.Image4.Left + MNG3.Image4.Width
  MNG3.lblPATH(1).Width = (MNG3.List1(1).Left + MNG3.List1(1).Width - MNG3.Image2.Width) - (MNG3.Image4.Left + MNG3.Image4.Width)
  MNG3.lblPATH(1).Height = MNG3.Image2.Height
  '
End Sub

Private Sub Form_Deactivate()
  '
  Call ChildDeActivate(Me)
  '
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  '
  Call GENERA_EVENTO_CLICK(KeyCode, Shift)
  '
End Sub

Private Sub Form_Load()
'
Dim stmp              As String
Dim ret               As Long
Dim PEZZO             As String
Dim riga              As String
Dim TIPO_LAVORAZIONE  As String
Dim DB                As Database
Dim TB                As Recordset
Dim DISCHETTO         As String
Dim i                 As Integer
'
On Error GoTo errForm_Load
  '
  'NASCONDO I TASTI DELLA SECONDA RIGA
  MDIForm1.SSPanel1.Height = MDIForm1.Picture1(0).Height
  For i = 0 To 7
    MDIForm1.Picture3(i).Visible = False
  Next i
  Call AlMakeBorder(hwnd)
  Call Move(g_nBeArtLeft, g_nBeArtTop, g_nBeArtWidth, g_nBeArtHeight)
  Call InitEnvironment(Me)
  Image1.Picture = LoadPicture(g_chOemPATH & "\logo1.bmp")
  Image2.Picture = LoadPicture(g_chOemPATH & "\Gapp4\images\archive2.bmp")
  Image3.Picture = LoadPicture(g_chOemPATH & "\Gapp4\images\archive1.bmp")
  Image4.Picture = LoadPicture(g_chOemPATH & "\Gapp4\images\PROC00.bmp")
  Call IMPOSTA_FONT(Me)
  MNG3.Label1(0).Caption = "HD: WORKPIECE"
  MNG3.Label1(1).Caption = "FD: PROCEDURE"
  'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  'ELENCO DISEGNI IN ARCHIVIO: VERIFICA ESISTENZA DEL DIRETTORIO DELLE PROCEDURE
  MNG3.List1(0).Clear
  Set DB = OpenDatabase(PathDB, True, False)
  Set TB = DB.OpenRecordset("ARCHIVIO_" & TIPO_LAVORAZIONE)
  Do Until TB.EOF
    If Not IsNull(TB.Fields("NOME_PEZZO").Value) Then
      stmp = g_chOemPATH & "\" & TIPO_LAVORAZIONE & "\PROCEDURE"
      If (Dir$(stmp, 16) <> "") Then
      stmp = g_chOemPATH & "\" & TIPO_LAVORAZIONE & "\PROCEDURE\" & TB.Fields("NOME_PEZZO").Value
      If (Dir$(stmp, 16) <> "") Then
        Call MNG3.List1(0).AddItem(" " & TB.Fields("NOME_PEZZO").Value)
      Else
        Call MNG3.List1(0).AddItem(" " & TB.Fields("NOME_PEZZO").Value & " (??)")
      End If
      Else
        Call MNG3.List1(0).AddItem(" " & TB.Fields("NOME_PEZZO").Value)
      End If
    End If
    TB.MoveNext
  Loop
  TB.Close
  DB.Close
  'ELENCO PROCEDURE DISPONIBILI SULLA CHIAVETTA
  DISCHETTO = GetInfo("Configurazione", "DISCHETTO", Path_LAVORAZIONE_INI)
  DISCHETTO = UCase$(Trim$(DISCHETTO))
  If (Len(DISCHETTO) <= 0) Then DISCHETTO = "A:"
  'SEGNALAZIONE UTENTE
  MNG3.lblPATH(0).Caption = " " & g_chOemPATH & " "
  MNG3.lblPATH(1).Caption = " " & DISCHETTO & "\" & " "
  PEZZO = DISCHETTO & "\" & TIPO_LAVORAZIONE & "\PROCEDURE\"
  'SE NON ESISTE LO CREO
  If (Dir$(PEZZO, 16) = "") Then
    i = InStr(PEZZO, "\")
    While (i <> 0)
      MkDir Left$(PEZZO, i - 1)
      'MsgBox Left$(PEZZO, i - 1)
      i = InStr(i + 1, PEZZO, "\")
    Wend
    Call MkDir(PEZZO)
  End If
  'CREAZIONE LISTA FILE CONTENUTI NEL DIRETTORIO
  MNG3.List1(1).Clear
  i = 1
  riga = Dir(PEZZO, vbDirectory)
  Do While (riga <> "")
    If (riga <> ".") And (riga <> "..") Then
      ret = GetAttr(PEZZO & riga)
      If (ret = vbDirectory) Then
        Call MNG3.List1(1).AddItem(" " & riga)
        i = i + 1
      End If
    End If
    riga = Dir
  Loop
  'INIZIALIZZO LE LISTE
  MNG3.List1(0).ListIndex = -1
  MNG3.List1(1).ListIndex = -1
  '
Exit Sub

errForm_Load:
  WRITE_DIALOG Error(Err)
  Resume Next

End Sub

Private Sub DIRECTORY_SELEZIONE_Change()
'
On Error Resume Next
  '
  MNG3.lblPATH(1).Caption = Trim$(MNG3.DIRECTORY_SELEZIONE.Path)
  MNG3.lblPATH(1).Caption = " " & MNG3.lblPATH(1).Caption & " "
  'SEGNALAZIONE UTENTE
  WRITE_DIALOG ""
  '
End Sub

Private Sub DRIVE_SELEZIONE_Change()
'
On Error Resume Next
  '
  MNG3.DIRECTORY_SELEZIONE.Path = MNG3.DRIVE_SELEZIONE.Drive
  MNG3.lblPATH(1).Caption = Trim$(MNG3.DIRECTORY_SELEZIONE.Path)
  MNG3.lblPATH(1).Caption = " " & MNG3.lblPATH(1).Caption & " "
  'SEGNALAZIONE UTENTE
  WRITE_DIALOG ""
  '
End Sub

Private Sub Image2_Click()
'
Dim DISCHETTO As String
Dim stmp      As String
Dim ret       As Long
Dim PEZZO     As String
Dim riga      As String
Dim i         As Integer
Dim TIPO_LAVORAZIONE  As String
'
On Error Resume Next
  '
  If (MNG3.fra_SELEZIONE.Visible = False) Then
    MNG3.fra_SELEZIONE.Left = MNG3.Label1(1).Left
    MNG3.fra_SELEZIONE.Top = MNG3.Label1(1).Top
    MNG3.fra_SELEZIONE.Width = MNG3.List1(1).Width
    MNG3.fra_SELEZIONE.Height = MNG3.Label1(1).Height + MNG3.List1(1).Height
    '
    MNG3.DRIVE_SELEZIONE.Top = 300
    MNG3.DRIVE_SELEZIONE.Width = MNG3.fra_SELEZIONE.Width * 0.95
    MNG3.DRIVE_SELEZIONE.Left = (MNG3.fra_SELEZIONE.Width - MNG3.DRIVE_SELEZIONE.Width) / 2
    '
    MNG3.DIRECTORY_SELEZIONE.Left = MNG3.DRIVE_SELEZIONE.Left
    MNG3.DIRECTORY_SELEZIONE.Top = MNG3.DRIVE_SELEZIONE.Top + MNG3.DRIVE_SELEZIONE.Height + 50
    MNG3.DIRECTORY_SELEZIONE.Width = MNG3.DRIVE_SELEZIONE.Width
    MNG3.DIRECTORY_SELEZIONE.Height = MNG3.fra_SELEZIONE.Height - 300 - MNG3.DRIVE_SELEZIONE.Height - 50 - 50
    '
    MNG3.fra_SELEZIONE.Visible = True
    MNG3.fra_SELEZIONE.Caption = "SELECT PATH WITH MOUSE"
    DISCHETTO = GetInfo("Configurazione", "DISCHETTO", Path_LAVORAZIONE_INI)
    DISCHETTO = UCase$(Trim$(DISCHETTO))
    If (Len(DISCHETTO) <= 0) Then DISCHETTO = "A:"
    MNG3.DRIVE_SELEZIONE.Drive = DISCHETTO
    MNG3.DIRECTORY_SELEZIONE.Path = DISCHETTO
  Else
    MNG3.fra_SELEZIONE.Visible = False
    'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
    TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
    TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
    'ELENCO PROCEDURE DISPONIBILI
    stmp = UCase$(Trim$(MNG3.DIRECTORY_SELEZIONE.Path))
    If (Len(stmp) > 0) Then
      If (Right$(stmp, 1) = "\") Then stmp = Left$(stmp, Len(stmp) - 1)
      DISCHETTO = GetInfo("Configurazione", "DISCHETTO", Path_LAVORAZIONE_INI)
      DISCHETTO = UCase$(Trim$(DISCHETTO))
      If (Len(DISCHETTO) <= 0) Then DISCHETTO = "A:"
      If (DISCHETTO <> stmp) Then
        ret = WritePrivateProfileString("Configurazione", "DISCHETTO", stmp, Path_LAVORAZIONE_INI)
        DISCHETTO = stmp
      End If
    End If
    PEZZO = DISCHETTO & "\" & TIPO_LAVORAZIONE & "\PROCEDURE\"
    'CREAZIONE LISTA FILE CONTENUTI NEL DIRETTORIO
    MNG3.List1(1).Clear
    i = 1
    riga = Dir(PEZZO, vbDirectory)
    Do While (riga <> "")
      If (riga <> ".") And (riga <> "..") Then
        ret = GetAttr(PEZZO & riga)
        If (ret = vbDirectory) Then
          Call MNG3.List1(1).AddItem(" " & riga)
          i = i + 1
        End If
      End If
      riga = Dir
    Loop
    'INIZIALIZZO LE LISTE
    MNG3.List1(0).ListIndex = -1
    MNG3.List1(1).ListIndex = -1
  End If
  '
End Sub

Private Sub List1_GotFocus(Index As Integer)
'
Dim i As Integer
'
On Error GoTo errList1_GotFocus
  '
  Select Case Index
    '
    Case 0
      'RIMETTO A POSTO L'ALTRA LISTA
      MNG3.lblPATH(1).BackColor = QBColor(10)
      MNG3.lblPATH(1).ForeColor = QBColor(0)
      MNG3.Label1(1).BackColor = QBColor(10)
      MNG3.Label1(1).ForeColor = QBColor(0)
      'EVIDENZIO LA LISTA
      MNG3.lblPATH(0).BackColor = QBColor(12)
      MNG3.lblPATH(0).ForeColor = QBColor(15)
      MNG3.Label1(0).BackColor = QBColor(12)
      MNG3.Label1(0).ForeColor = QBColor(15)
      '
    Case 1
      'RIMETTO A POSTO L'ALTRA LISTA
      MNG3.lblPATH(0).BackColor = QBColor(10)
      MNG3.lblPATH(0).ForeColor = QBColor(0)
      MNG3.Label1(0).BackColor = QBColor(10)
      MNG3.Label1(0).ForeColor = QBColor(0)
      'EVIDENZIO LA LISTA
      MNG3.lblPATH(1).BackColor = QBColor(12)
      MNG3.lblPATH(1).ForeColor = QBColor(15)
      MNG3.Label1(1).BackColor = QBColor(12)
      MNG3.Label1(1).ForeColor = QBColor(15)
      '
  End Select
  '
Exit Sub

errList1_GotFocus:
  WRITE_DIALOG "Sub List1_GotFocus: Error -> " & Err
  Resume Next
  
End Sub
