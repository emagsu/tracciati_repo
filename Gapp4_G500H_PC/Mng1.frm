VERSION 5.00
Begin VB.Form MNG1 
   Appearance      =   0  'Flat
   AutoRedraw      =   -1  'True
   BackColor       =   &H00EBE1D7&
   BorderStyle     =   0  'None
   Caption         =   "MNG1"
   ClientHeight    =   7176
   ClientLeft      =   1272
   ClientTop       =   1692
   ClientWidth     =   11988
   ForeColor       =   &H80000008&
   Icon            =   "Mng1.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7176
   ScaleWidth      =   11988
   ShowInTaskbar   =   0   'False
   Tag             =   "MNG1"
   Begin VB.Frame fra_SELEZIONE 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBE1D7&
      Caption         =   "fra_SELEZIONE"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.6
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   4730
      Left            =   6360
      TabIndex        =   5
      Top             =   1800
      Visible         =   0   'False
      Width           =   4500
      Begin VB.DriveListBox DRIVE_SELEZIONE 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   324
         Left            =   50
         TabIndex        =   7
         Top             =   315
         Width           =   4400
      End
      Begin VB.DirListBox DIRECTORY_SELEZIONE 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.6
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3852
         Left            =   50
         TabIndex        =   6
         Top             =   720
         Width           =   4400
      End
   End
   Begin VB.ListBox List1 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   10.2
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4308
      Index           =   1
      Left            =   4800
      MultiSelect     =   2  'Extended
      Sorted          =   -1  'True
      TabIndex        =   1
      Top             =   1000
      Width           =   4500
   End
   Begin VB.ListBox List1 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   10.2
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4308
      Index           =   0
      Left            =   100
      MultiSelect     =   2  'Extended
      Sorted          =   -1  'True
      TabIndex        =   0
      Top             =   1000
      Width           =   4500
   End
   Begin VB.Image Image4 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   408
      Left            =   96
      Picture         =   "Mng1.frx":030A
      Top             =   5400
      Width           =   408
   End
   Begin VB.Label lblPATH 
      Appearance      =   0  'Flat
      BackColor       =   &H00EBE1D7&
      Caption         =   "lblPATH"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   10.2
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   1100
      TabIndex        =   8
      Top             =   5400
      Width           =   7050
   End
   Begin VB.Label lblERROR 
      BackColor       =   &H00EBE1D7&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "lblERROR"
      BeginProperty Font 
         Name            =   "MS Song"
         Size            =   9.6
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   600
      Left            =   1260
      TabIndex        =   4
      Top             =   0
      Width           =   3350
   End
   Begin VB.Image Image3 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   408
      Left            =   108
      Picture         =   "Mng1.frx":074C
      Top             =   24
      Width           =   408
   End
   Begin VB.Image Image2 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   408
      Left            =   4800
      Picture         =   "Mng1.frx":0B8E
      Top             =   24
      Width           =   408
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H000000FF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "MS Song"
         Size            =   8.4
         Charset         =   134
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   430
      Index           =   1
      Left            =   4800
      TabIndex        =   3
      Top             =   600
      Width           =   4500
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H000000FF&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "MS Song"
         Size            =   8.4
         Charset         =   134
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   430
      Index           =   0
      Left            =   100
      TabIndex        =   2
      Top             =   600
      Width           =   4500
   End
   Begin VB.Image Image1 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Height          =   615
      Left            =   8650
      Picture         =   "Mng1.frx":0FD0
      Stretch         =   -1  'True
      Top             =   0
      Width           =   645
   End
End
Attribute VB_Name = "MNG1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub DIRECTORY_SELEZIONE_Change()
'
On Error Resume Next
  '
  MNG1.lblPATH.Caption = MNG1.DIRECTORY_SELEZIONE.Path
  'SEGNALAZIONE UTENTE
  WRITE_DIALOG ""
  '
End Sub

Private Sub DRIVE_SELEZIONE_Change()
'
On Error Resume Next
  '
  MNG1.DIRECTORY_SELEZIONE.Path = MNG1.DRIVE_SELEZIONE.Drive
  MNG1.lblPATH.Caption = MNG1.DIRECTORY_SELEZIONE.Path
  'SEGNALAZIONE UTENTE
  WRITE_DIALOG ""
  '
End Sub

Private Sub Form_Activate()
  '
  MNG1.List1(0).BackColor = &HFFFFFF
  MNG1.List1(1).BackColor = &HFFFFFF
  MNG1.DRIVE_SELEZIONE.BackColor = &HFFFFFF
  MNG1.DIRECTORY_SELEZIONE.BackColor = &HFFFFFF
  Call ChildActivate(Me)
  '
End Sub

Private Sub Form_Deactivate()
  '
  Call ChildDeActivate(Me)
  '
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
  '
  Call GENERA_EVENTO_CLICK(KeyCode, Shift)
  '
End Sub

Private Sub Form_Load()
'
Dim stmp  As String
Dim ret   As Integer
Dim i     As Integer
'
On Error GoTo errForm_Load
  '
  'NASCONDO I TASTI DELLA SECONDA RIGA
  MDIForm1.SSPanel1.Height = MDIForm1.Picture1(0).Height
  For i = 0 To 7
    MDIForm1.Picture3(i).Visible = False
  Next i
  Call AlMakeBorder(hwnd)
  Call Move(g_nBeArtLeft, g_nBeArtTop, g_nBeArtWidth, g_nBeArtHeight)
  Call InitEnvironment(Me)
  Image1.Picture = LoadPicture(g_chOemPATH & "\logo1.bmp")
  Image4.Picture = LoadPicture(g_chOemPATH & "\Gapp4\images\SYNCHRO0.bmp")
  Image2.Picture = LoadPicture(g_chOemPATH & "\Gapp4\images\archive2.bmp")
  Image3.Picture = LoadPicture(g_chOemPATH & "\Gapp4\images\archive1.bmp")
  ACTUAL_PARAMETER_GROUP = ""
  Call IMPOSTA_FONT(Me)
  Call INIZIALIZZA_MNG1
  '
Exit Sub

errForm_Load:
  WRITE_DIALOG Error(Err)
  Resume Next

End Sub

Private Sub Image2_Click()
'
Dim DISCHETTO As String
Dim stmp      As String
Dim ret       As Long
'
On Error Resume Next
  '
  If (MNG1.fra_SELEZIONE.Visible = False) Then
    MNG1.fra_SELEZIONE.Move MNG1.Label1(1).Left, MNG1.Label1(1).Top
    MNG1.fra_SELEZIONE.Visible = True
    MNG1.fra_SELEZIONE.Caption = "SELECT PATH WITH MOUSE"
    DISCHETTO = GetInfo("Configurazione", "DISCHETTO", PathFILEINI)
    DISCHETTO = UCase$(Trim$(DISCHETTO))
    If (Len(DISCHETTO) <= 0) Then DISCHETTO = "A:"
    MNG1.DRIVE_SELEZIONE.Drive = DISCHETTO
    MNG1.DIRECTORY_SELEZIONE.Path = DISCHETTO
  Else
    'PULISCO LA BARRA DEI MESSAGGI
    WRITE_DIALOG ""
    'PULISCO LA FINESTRA DEGLI ERRORI
    MNG1.lblERROR.Caption = ""
    stmp = UCase$(Trim$(MNG1.DIRECTORY_SELEZIONE.Path))
    If (Len(stmp) > 0) Then
      If (Right$(stmp, 1) = "\") Then stmp = Left$(stmp, Len(stmp) - 1)
      DISCHETTO = GetInfo("Configurazione", "DISCHETTO", PathFILEINI)
      DISCHETTO = UCase$(Trim$(DISCHETTO))
      If (Len(DISCHETTO) <= 0) Then DISCHETTO = "A:"
      If (DISCHETTO <> stmp) Then
        ret = WritePrivateProfileString("Configurazione", "DISCHETTO", stmp, PathFILEINI)
        Call INIZIALIZZA_MNG1
      End If
    End If
    MNG1.fra_SELEZIONE.Visible = False
  End If
  '
End Sub

Private Sub Image4_Click()
'
Dim DISCHETTO As String
Dim stmp      As String
Dim ret       As Long
'
On Error Resume Next
  '
  If (MNG1.fra_SELEZIONE.Visible = False) Then
    MNG1.fra_SELEZIONE.Move MNG1.Label1(1).Left, MNG1.Label1(1).Top
    MNG1.fra_SELEZIONE.Visible = True
    MNG1.fra_SELEZIONE.Caption = "SELECT PATH WITH MOUSE"
    DISCHETTO = GetInfo("Configurazione", "DISCHETTO", PathFILEINI)
    DISCHETTO = UCase$(Trim$(DISCHETTO))
    If (Len(DISCHETTO) <= 0) Then DISCHETTO = "A:"
    MNG1.DRIVE_SELEZIONE.Drive = DISCHETTO
    MNG1.DIRECTORY_SELEZIONE.Path = DISCHETTO
  Else
    'PULISCO LA BARRA DEI MESSAGGI
    WRITE_DIALOG ""
    'PULISCO LA FINESTRA DEGLI ERRORI
    MNG1.lblERROR.Caption = ""
    stmp = UCase$(Trim$(MNG1.DIRECTORY_SELEZIONE.Path))
    If (Len(stmp) > 0) Then
      If (Right$(stmp, 1) = "\") Then stmp = Left$(stmp, Len(stmp) - 1)
      DISCHETTO = GetInfo("Configurazione", "DISCHETTO", PathFILEINI)
      DISCHETTO = UCase$(Trim$(DISCHETTO))
      If (Len(DISCHETTO) <= 0) Then DISCHETTO = "A:"
      If (DISCHETTO <> stmp) Then
        ret = WritePrivateProfileString("Configurazione", "DISCHETTO", stmp, PathFILEINI)
        Call INIZIALIZZA_MNG1
      End If
    End If
    MNG1.fra_SELEZIONE.Visible = False
  End If
  '
End Sub

Private Sub List1_GotFocus(Index As Integer)
'
Dim i As Integer
'
On Error GoTo errList1_GotFocus
  '
  Select Case Index
  Case 0
      'RIMETTO A POSTO L'ALTRA LISTA
      MNG1.Label1(1).BackColor = QBColor(10)
      MNG1.Label1(1).ForeColor = QBColor(0)
      MNG1.List1(1).ListIndex = -1
      For i = 0 To MNG1.List1(1).ListCount - 1
        MNG1.List1(1).Selected(i) = False
      Next i
      'EVIDENZIO LA LISTA
      MNG1.Label1(0).BackColor = QBColor(12)
      MNG1.Label1(0).ForeColor = QBColor(15)
  Case 1
      'RIMETTO A POSTO L'ALTRA LISTA
      MNG1.Label1(0).BackColor = QBColor(10)
      MNG1.Label1(0).ForeColor = QBColor(0)
      MNG1.List1(0).ListIndex = -1
      For i = 0 To MNG1.List1(0).ListCount - 1
        MNG1.List1(0).Selected(i) = False
      Next i
      'EVIDENZIO LA LISTA
      MNG1.Label1(1).BackColor = QBColor(12)
      MNG1.Label1(1).ForeColor = QBColor(15)
  End Select
  '
Exit Sub

errList1_GotFocus:
  WRITE_DIALOG "Sub List1_GotFocus: Error -> " & Err
  Resume Next
  
End Sub

Private Sub List1_KeyUp(Index As Integer, KeyCode As Integer, Shift As Integer)
'
Dim NomePezzo         As String
Dim DATA_SALVATAGGIO  As String
Dim DB                As Database
Dim TB                As Recordset
Dim riga              As String
Dim stmp              As String
Dim TIPO_LAVORAZIONE  As String
Dim nDATABASE         As String
Dim sELENCO           As String
Dim ELENCO()          As String
'
On Error GoTo errList1_KeyUp
  '
  If (MNG1.List1(Index).ListIndex > -1) Then
    Select Case KeyCode
      Case 33
        'INIZIO LISTA
        GoSub SCRIVI_DATA
      Case 34
        'FINE LISTA
        GoSub SCRIVI_DATA
      Case 40
        'AVANTI
        GoSub SCRIVI_DATA
      Case 38
        'INDIETRO
        GoSub SCRIVI_DATA
      Case 13
        'INVIO
        GoSub SCRIVI_DATA
    End Select
  End If
  '
Exit Sub

errList1_KeyUp:
  WRITE_DIALOG Error(Err)
  Resume Next

SCRIVI_DATA:
  'LEGGO IL PERCORSO DEL DATABASE
  If (Index = 1) Then
    'FLOPPY
    Dim DISCHETTO As String
    DISCHETTO = GetInfo("Configurazione", "DISCHETTO", PathFILEINI)
    DISCHETTO = UCase$(Trim$(DISCHETTO))
    If Len(DISCHETTO) <= 0 Then DISCHETTO = "A:"
    nDATABASE = DISCHETTO & "\" & "ARCHIVIO.MDB"
  Else
    'LOCALE
    nDATABASE = PathDB
  End If
  WRITE_DIALOG nDATABASE & " (reading....)"
  'ASSEGNO IL NOME DEL PEZZO SELEZIONATO
  NomePezzo = MNG1.List1(Index).List(MNG1.List1(Index).ListIndex)
  NomePezzo = UCase$(Trim$(NomePezzo))
  'LEGGO IL TIPO DI LAVORAZIONE
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  'COSTRUISCO IL NOME DELLA TABELLA
  stmp = "ARCHIVIO_" & TIPO_LAVORAZIONE
  'APRO IL DATABASE
  Set DB = OpenDatabase(nDATABASE, True, False)
  Set TB = DB.OpenRecordset(stmp)
  'RICERCO NELLA TABELLA ARCHIVIO SE ESISTE
  TB.Index = "NOME_PEZZO"
  TB.Seek "=", NomePezzo
  If Not TB.NoMatch Then
    If IsNull(TB.Fields("ELENCO").Value) Then sELENCO = "" Else sELENCO = TB.Fields("ELENCO").Value
    DATA_SALVATAGGIO = TB.Fields("DATA_ARCHIVIAZIONE").Value
    WRITE_DIALOG MNG1.List1(Index).List(MNG1.List1(Index).ListIndex) & " : " & DATA_SALVATAGGIO
  End If
  TB.Close
  DB.Close
  '***************************************************************************************
  If (Len(sELENCO) > 0) And (KeyCode = 13) Then
    ELENCO = Split(sELENCO, ";")
    stmp = ""
    For i = 0 To UBound(ELENCO) - 1
      stmp = stmp & Format$(i + 1, "##0") & ") " & ELENCO(i) & Chr(13)
    Next i
    Select Case LINGUA
    Case "IT": riga = "I pezzi seguenti avranno gli stessi parametri di rettifica:"
    Case Else: riga = "Following workpieces will have same grinding data:"
    End Select
    stmp = riga & Chr(13) & Chr(13) & stmp
    Call MsgBox(stmp, vbInformation, "WARNING: " & NomePezzo)
  End If
  '***************************************************************************************
  '
Return

End Sub
