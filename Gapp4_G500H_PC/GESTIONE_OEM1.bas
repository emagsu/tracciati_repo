Attribute VB_Name = "GESTIONE_OEM1"
Option Explicit

Sub INIZIALIZZA_OEM1_CNC()
'
Dim Gruppo      As String
Dim i           As Integer
Dim i1          As Integer
Dim i2          As Integer
Dim i3          As Integer
Dim j           As Integer
Dim k           As Integer
Dim stmp        As String
Dim DB          As Database
Dim TB          As Recordset
Dim RS          As Recordset
Dim NomeFile    As String
Dim scmd        As String
Dim NomeSPF     As String
Dim NomeDIR     As String
Dim NomeSK      As String
Dim IndiceTASTO As Integer
Dim iGRUPPO     As Integer
Dim ret         As Integer
Dim Atmp        As String * 255
Dim MESSAGGI    As String
'
On Error GoTo errINIZIALIZZA_OEM1_CNC
  '
  OEM1.Text1(1).Enabled = True
  OEM1.Text2(2).Enabled = True
  OEM1.Text3(1).Enabled = True
  OEM1.Text3(2).Enabled = True
  OEM1.lblINP(0).Visible = False
  OEM1.txtINP(0).Visible = False
  OEM1.PicELABORA.Visible = False
  '
  Call OEM1_INTESTAZIONE("CNC " & LETTERA_LAVORAZIONE, "CNC.bmp")
  '
  IndiceTASTO = val(GetInfo("OEM1", "IndiceTASTO", Path_LAVORAZIONE_INI))
  Gruppo = GetInfo("OEM1", "GRUPPO(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
  Gruppo = UCase$(Trim$(Gruppo))
  If (Len(Gruppo) <= 0) Then Call OEM1_NASCONDO_CONTROLLI: Exit Sub
  '
  NomeDIR = GetInfo("OEM1", "NomeDIR(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
  NomeDIR = UCase$(Trim$(NomeDIR))
  If (InStr(NomeDIR, "*") > 0) Then NomeDIR = Replace(NomeDIR, "*", Format$(INDICE_LAVORAZIONE_SINGOLA, "0"))
  '
  Call AZZERO_CONTROLLI_OEM1
  '
  NomeSPF = GetInfo("Configurazione", "NomeFileTemporaneoSPF", PathFILEINI)
  stmp = g_chOemPATH & "\" & NomeSPF
  If (Dir(stmp & ".SPF") <> "") Then Call Kill(stmp & ".SPF")
  If (Dir(stmp & ".INI") <> "") Then Call Kill(stmp & ".INI")
  If (Dir(stmp & ".DDE") <> "") Then Call Kill(stmp & ".DDE")
  If (Dir(stmp & ".TXT") <> "") Then Call Kill(stmp & ".TXT")
  '
  OEM1.FraPARAMETRI.Caption = GetInfo("OEM1", "NomeGRP(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
  stmp = GetInfo("OEM1", "NomeINP(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI): stmp = UCase$(Trim$(stmp))
  If (stmp = "N") Then
    OEM1.imgCODICE.Visible = False
    OEM1.txtCODICE.Visible = False
  Else
    OEM1.txtCODICE.Text = OEM1.FraPARAMETRI.Caption
    OEM1.txtCODICE.Visible = True
    NomeSK = GetInfo("OEM1", "NomeSK(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
    NomeSK = UCase$(Trim$(NomeSK))
    If (IsNumeric(NomeSK)) Then
      ret = val(NomeSK)
      ret = LoadString(g_hLanguageLibHandle, ret, Atmp, 255)
      If (ret > 0) Then NomeSK = Left$(Atmp, ret)
    End If
    'CONTROLLO SE INDIRIZZO UNA IMMAGINE \\
    ret = InStr(NomeSK, "\")
    If (ret = 1) Then
      NomeSK = Mid$(NomeSK, ret)
      OEM1.imgCODICE.Picture = LoadPicture(g_chOemPATH & "\Gapp4\images\" & NomeSK)
      OEM1.imgCODICE.Left = 0
      OEM1.imgCODICE.Top = 0
      OEM1.imgCODICE.Width = OEM1.Image3.Width
      OEM1.imgCODICE.Visible = True
      OEM1.txtCODICE.Left = OEM1.imgCODICE.Left + OEM1.imgCODICE.Width
      OEM1.txtCODICE.Top = 0
      OEM1.txtCODICE.Width = OEM1.FraPARAMETRI.Width - OEM1.imgCODICE.Width - 15
      OEM1.txtCODICE.Height = 300
      OEM1.imgCODICE.Height = OEM1.txtCODICE.Height
    Else
      If (Len(NomeSK) <= 0) Then NomeSK = "SK" & Format$(IndiceTASTO, "0")
      OEM1.imgCODICE.Picture = LoadPicture("")
      OEM1.imgCODICE.Visible = False
      OEM1.txtCODICE.Left = 0
      OEM1.txtCODICE.Top = 0
      OEM1.txtCODICE.Width = OEM1.FraPARAMETRI.Width - 15
      OEM1.txtCODICE.Height = 300
    End If
  End If
  '
  WRITE_DIALOG "Reading data from CNC."
  If (Len(NomeDIR) > 0) Then
    Set Session = GetObject("@SinHMIMCDomain.MCDomain")
    Session.CopyNC "/NC/WKS.DIR/" & NomeDIR & ".WPD/" & Gruppo & ".SPF", g_chOemPATH & "\" & NomeSPF & ".SPF", MCDOMAIN_COPY_NC
    Set Session = Nothing
    Call SCRIVI_SEZIONE(NomeSPF, Gruppo)
  End If
  '
  Set DB = OpenDatabase(PathDB_ADDIN, True, False)
  'CONTROLLO ESISTENZA TABELLA DI CONFIGURAZIONE
  stmp = ""
  For j = 0 To DB.TableDefs.count - 1
    If (DB.TableDefs(j).Name = Gruppo) Then
      stmp = "TROVATO"
      Exit For
    End If
  Next j
  If (stmp <> "TROVATO") Then
    WRITE_DIALOG Gruppo & " it is not available!!!"
    DB.Close
    Exit Sub
  End If
  'CONTROLLO SE ESISTE LA TABELLA MESSAGGI NEL DATABASE
  stmp = "N"
  For i = 0 To DB.TableDefs.count - 1
    If (DB.TableDefs(i).Name = "MESSAGGI") Then stmp = "Y"
  Next i
  If (stmp = "Y") Then
    Set RS = DB.OpenRecordset("MESSAGGI", dbOpenDynaset)
    'TROVO IL RECORD DEL MESSAGGIO
    RS.FindFirst "TABELLA = '" & Gruppo & "'"
    If Not RS.NoMatch Then
      If (IsNull(RS.Fields("NOME_" & LINGUA).Value) = True) Then
        MESSAGGI = ""
      Else
        MESSAGGI = RS.Fields("NOME_" & LINGUA).Value
      End If
    End If
    RS.Close
  Else
    MESSAGGI = ""
  End If
  DB.Close
  '
  Call OEM1_CREAZIONE_ARCHIVIO(Gruppo)
  Open g_chOemPATH & "\" & NomeSPF & ".DDE" For Output As #1
  Open g_chOemPATH & "\" & NomeSPF & ".TXT" For Output As #2
  Set DB = OpenDatabase(PathDB_ADDIN, True, False)
  Set TB = DB.OpenRecordset(Gruppo, dbOpenTable)
  TB.MoveFirst
  i1 = 0: i2 = 0: i3 = 0
  Do Until TB.EOF
    If TB.Fields("ABILITATO").Value = "Y" Then
      iGRUPPO = TB.Fields("GRUPPO").Value
      If (iGRUPPO <> 1) And (iGRUPPO <> 2) And (iGRUPPO <> 3) Then iGRUPPO = 1
      Select Case iGRUPPO
        Case 1
          i1 = i1 + 1
          ReDim Preserve INDICI_1(i1)
          INDICI_1(i1) = TB.Fields("INDICE").Value
          Call OEM1.List1(0).AddItem(" " & TB.Fields("NOME_" & LINGUA).Value)
          j = 1
            GoSub LETTURA
            Call OEM1.List1(j).AddItem(" " & TB.Fields("ACTUAL_" & Format$(j, "0")).Value)
            Print #2, TB.Fields("ACTUAL_" & Format$(j, "0")).Value & ";0;";
        Case 2
          i2 = i2 + 1
          ReDim Preserve INDICI_2(i2)
          INDICI_2(i2) = TB.Fields("INDICE").Value
          Call OEM1.List2(0).AddItem(" " & TB.Fields("NOME_" & LINGUA).Value)
          j = 2
            GoSub LETTURA
            Call OEM1.List2(j).AddItem(" " & TB.Fields("ACTUAL_" & Format$(j, "0")).Value)
            Print #2, "0;" & TB.Fields("ACTUAL_" & Format$(j, "0")).Value & ";";
        Case 3
          i3 = i3 + 1
          ReDim Preserve INDICI_3(i3)
          INDICI_3(i3) = TB.Fields("INDICE").Value
          Call OEM1.List3(0).AddItem(" " & TB.Fields("NOME_" & LINGUA).Value)
          For j = 1 To 2
            GoSub LETTURA
            Call OEM1.List3(j).AddItem(" " & TB.Fields("ACTUAL_" & Format$(j, "0")).Value)
            Print #2, TB.Fields("ACTUAL_" & Format$(j, "0")).Value & ";";
          Next j
      End Select
    End If
    TB.MoveNext
  Loop
  TB.Close
  DB.Close
  Close #2
  Close #1
  Call INIZIALIZZO_CONTROLLI_OEM1(i1, i2, i3, Gruppo, MESSAGGI)
  WRITE_DIALOG Gruppo & " read from CNC "
  '
Exit Sub

errINIZIALIZZA_OEM1_CNC:
  WRITE_DIALOG "Sub INIZIALIZZA_OEM1_CNC: Error -> " & Err
  Resume Next

LETTURA:
  If IsNull(TB.Fields("VARIABILE_" & Format$(j, "0")).Value) Then
    stmp = ""
  Else
    stmp = Trim$(TB.Fields("VARIABILE_" & Format$(j, "0")).Value)
  End If
  If (Len(NomeDIR) > 0) And (Len(stmp) > 0) Then
    'LEGGO IL VALORE DEL PARAMETRO DAL FILE INI
    stmp = GetInfo(Gruppo, TB.Fields("VARIABILE_" & Format$(j, "0")).Value, PathFileSPF)
    '***********************************************************
    k = InStr(stmp, ";")
    If (k > 0) Then stmp = Left$(stmp, k - 1): stmp = Trim$(stmp)
    k = InStr(stmp, Chr$(34))
    If (k > 0) Then stmp = Mid$(stmp, 2, Len(stmp) - 2): stmp = Trim$(stmp)
    '**********************************************************
    ret = WritePrivateProfileString(Gruppo, TB.Fields("VARIABILE_" & Format$(j, "0")).Value, stmp, g_chOemPATH & "\ADDIN.INI")
  Else
    'LEGGO IL VALORE DEL PARAMETRO SUL CNC
    stmp = TB.Fields("ITEMDDE_" & Format$(j, "0")).Value
    Print #1, stmp & "=";
    stmp = OPC_LEGGI_DATO(stmp)
    'TOLGO GLI SPAZI
    stmp = Trim$(stmp)
    If IsNumeric(stmp) Then
      'SE E' NUMERICO FORMATTO IL VALORE
      stmp = frmt(val(stmp), 8)
    End If
    Print #1, stmp & ";";
    ret = WritePrivateProfileString(Gruppo, TB.Fields("ITEMDDE_" & Format$(j, "0")).Value, stmp, g_chOemPATH & "\ADDIN.INI")
  End If
  'AGGIORNO IL DATABASE DEL VALORE ATTUALE
  TB.Edit
  TB.Fields("ACTUAL_" & Format$(j, "0")).Value = stmp
  TB.Update
Return

End Sub

Sub INIZIALIZZA_OEM1_OFFLINE()
'
Dim Gruppo            As String
Dim i                 As Integer
Dim i1                As Integer
Dim i2                As Integer
Dim i3                As Integer
Dim j                 As Integer
Dim k                 As Integer
Dim L1                As Integer
Dim L2                As Integer
Dim stmp              As String
Dim stmp1             As String
Dim DB                As Database
Dim TB                As Recordset
Dim RS                As Recordset
Dim NomeFile          As String
Dim scmd              As String
Dim NomeSPF           As String
Dim NomeDIR           As String
Dim Nomegruppo        As String
Dim IndiceTASTO       As Integer
Dim iGRUPPO           As Integer
Dim ret               As Integer
Dim MESSAGGI          As String
Dim DATI_SPF          As String
Dim DATI_DDE          As String
Dim DATI_TXT          As String
Dim riga              As String
'
On Error GoTo errINIZIALIZZA_OEM1_OFFLINE
  '
  OEM1.Text1(1).Enabled = True
  OEM1.Text2(2).Enabled = True
  OEM1.Text3(1).Enabled = True
  OEM1.Text3(2).Enabled = True
  OEM1.lblINP(0).Visible = False
  OEM1.txtINP(0).Visible = False
  OEM1.PicELABORA.Visible = False
  '
  IndiceTASTO = val(GetInfo("OEM1", "IndiceTASTO", Path_LAVORAZIONE_INI))
  Gruppo = GetInfo("OEM1", "GRUPPO(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
  Gruppo = UCase$(Trim$(Gruppo))
  If (Len(Gruppo) <= 0) Then Call OEM1_NASCONDO_CONTROLLI: Exit Sub
  '
  NomeDIR = GetInfo("OEM1", "NomeDIR(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
  NomeDIR = UCase$(Trim$(NomeDIR))
  If (InStr(NomeDIR, "*") > 0) Then NomeDIR = Replace(NomeDIR, "*", Format$(INDICE_LAVORAZIONE_SINGOLA, "0"))
  '
  OEM1.FraPARAMETRI.Caption = UCase$(Trim$(OEM1.txtNomePezzo.Text))
  Nomegruppo = OEM1.FraPARAMETRI.Caption
  If (Len(Nomegruppo) > 0) Then
    WRITE_DIALOG "Reading data from HD."
    Set DB = OpenDatabase(PathDB_ADDIN, True, False)
    Set RS = DB.OpenRecordset(Gruppo & "_ARCHIVIO", dbOpenTable)
    RS.Index = "NOME_PEZZO"
    RS.Seek "=", Nomegruppo
    If RS.NoMatch Then
      RS.Close
      DB.Close
      WRITE_DIALOG Nomegruppo & " not found."
      'RITORNO AL LIVELLO 1
      ret = WritePrivateProfileString("OEM1", "LIVELLO", "1", Path_LAVORAZIONE_INI)
      Call OEM1_VERTICALI(IndiceTASTO, 1)
      OEM1.FraPARAMETRI.Visible = False
      OEM1.fraARCHIVIO.Visible = True
      OEM1.txtMEMO.Text = ""
      Exit Sub
    Else
      DATI_SPF = RS.Fields("DATI_SPF").Value
      DATI_DDE = RS.Fields("DATI_DDE").Value
      DATI_TXT = RS.Fields("DATI_TXT").Value
    End If
    RS.Close
    DB.Close
    OEM1.FraPARAMETRI.Visible = True
    OEM1.txtCODICE.Visible = False
    OEM1.imgCODICE.Visible = False
    OEM1.fraARCHIVIO.Visible = False
    OEM1.PicGRUPPO_HD.Visible = False
    '
    Call OEM1_INTESTAZIONE("OFF-LINE", "INPUT.bmp")
    Call AZZERO_CONTROLLI_OEM1
    '
    NomeSPF = GetInfo("Configurazione", "NomeFileTemporaneoSPF", PathFILEINI)
    NomeSPF = g_chOemPATH & "\" & NomeSPF
    If (Dir(NomeSPF & ".SPF") <> "") Then Call Kill(NomeSPF & ".SPF")
    If (Dir(NomeSPF & ".INI") <> "") Then Call Kill(NomeSPF & ".INI")
    If (Dir(NomeSPF & ".DDE") <> "") Then Call Kill(NomeSPF & ".DDE")
    If (Dir(NomeSPF & ".TXT") <> "") Then Call Kill(NomeSPF & ".TXT")
    'Open NomeSPF & ".SPF" For Output As #1
    'Print #1, DATI_SPF
    'Close #1
    'Open NomeSPF & ".DDE" For Output As #1
    'Print #1, DATI_DDE
    'Close #1
    'Open NomeSPF & ".SPF" For Input As #1
    'Open NomeSPF & ".INI" For Output As #2
    'Print #2, "[" & Gruppo & "]"
    'While Not EOF(1)
    '  Line Input #1, riga
    '  If (Left$(riga, 1) <> ";") And (Left$(riga, 3) <> "M17") Then
    '    Print #2, riga
    '  End If
    'Wend
    'Close #2
    'Close #1
    '
    Set DB = OpenDatabase(PathDB_ADDIN, True, False)
    'CONTROLLO SE ESISTE LA TABELLA MESSAGGI NEL DATABASE
    stmp = "N"
    For i = 0 To DB.TableDefs.count - 1
      If (DB.TableDefs(i).Name = "MESSAGGI") Then stmp = "Y"
    Next i
    If (stmp = "Y") Then
      Set RS = DB.OpenRecordset("MESSAGGI", dbOpenDynaset)
      RS.FindFirst "TABELLA = '" & Gruppo & "'"
      If Not RS.NoMatch Then
        If (IsNull(RS.Fields("NOME_" & LINGUA).Value) = True) Then
          MESSAGGI = ""
        Else
          MESSAGGI = RS.Fields("NOME_" & LINGUA).Value
        End If
      End If
      RS.Close
    Else
      MESSAGGI = ""
    End If
    Set TB = DB.OpenRecordset(Gruppo, dbOpenTable)
    TB.MoveFirst
    i1 = 0: i2 = 0: i3 = 0
    L1 = 1: L2 = 1
    Do Until TB.EOF
    If TB.Fields("ABILITATO").Value = "Y" Then
      iGRUPPO = TB.Fields("GRUPPO").Value
      If (iGRUPPO <> 1) And (iGRUPPO <> 2) And (iGRUPPO <> 3) Then iGRUPPO = 1
      Select Case iGRUPPO
        Case 1
          i1 = i1 + 1
          ReDim Preserve INDICI_1(i1)
          INDICI_1(i1) = TB.Fields("INDICE").Value
          Call OEM1.List1(0).AddItem(" " & TB.Fields("NOME_" & LINGUA).Value)
          j = 1
            L2 = InStr(L1, DATI_TXT, ";")
            stmp = Mid$(DATI_TXT, L1, L2 - L1)
            L1 = L2 + 1
            L2 = InStr(L1, DATI_TXT, ";")
            L1 = L2 + 1
            'AGGIORNO IL DATABASE DEL VALORE ATTUALE
            TB.Edit
            TB.Fields("ACTUAL_" & Format$(j, "0")).Value = stmp
            TB.Update
            If IsNull(TB.Fields("VARIABILE_" & Format$(j, "0")).Value) Then
              stmp1 = ""
            Else
              stmp1 = Trim$(TB.Fields("VARIABILE_" & Format$(j, "0")).Value)
            End If
            If (Len(stmp1) > 0) Then
              ret = WritePrivateProfileString(Gruppo, stmp1, stmp, NomeSPF & ".INI")
            Else
              stmp1 = TB.Fields("ITEMDDE_" & Format$(j, "0")).Value
              ret = WritePrivateProfileString(Gruppo, stmp1, stmp, NomeSPF & ".INI")
            End If
            Call OEM1.List1(j).AddItem(" " & TB.Fields("ACTUAL_" & Format$(j, "0")).Value)
        Case 2
          i2 = i2 + 1
          ReDim Preserve INDICI_2(i2)
          INDICI_2(i2) = TB.Fields("INDICE").Value
          Call OEM1.List2(0).AddItem(" " & TB.Fields("NOME_" & LINGUA).Value)
          j = 2
            L2 = InStr(L1, DATI_TXT, ";")
            L1 = L2 + 1
            L2 = InStr(L1, DATI_TXT, ";")
            stmp = Mid$(DATI_TXT, L1, L2 - L1)
            L1 = L2 + 1
            'AGGIORNO IL DATABASE DEL VALORE ATTUALE
            TB.Edit
            TB.Fields("ACTUAL_" & Format$(j, "0")).Value = stmp
            TB.Update
            If IsNull(TB.Fields("VARIABILE_" & Format$(j, "0")).Value) Then
              stmp1 = ""
            Else
              stmp1 = Trim$(TB.Fields("VARIABILE_" & Format$(j, "0")).Value)
            End If
            If (Len(stmp1) > 0) Then
              ret = WritePrivateProfileString(Gruppo, stmp1, stmp, NomeSPF & ".INI")
            Else
              stmp1 = TB.Fields("ITEMDDE_" & Format$(j, "0")).Value
              ret = WritePrivateProfileString(Gruppo, stmp1, stmp, NomeSPF & ".INI")
            End If
            Call OEM1.List2(j).AddItem(" " & TB.Fields("ACTUAL_" & Format$(j, "0")).Value)
        Case 3
          i3 = i3 + 1
          ReDim Preserve INDICI_3(i3)
          INDICI_3(i3) = TB.Fields("INDICE").Value
          Call OEM1.List3(0).AddItem(" " & TB.Fields("NOME_" & LINGUA).Value)
          For j = 1 To 2
            L2 = InStr(L1, DATI_TXT, ";")
            stmp = Mid$(DATI_TXT, L1, L2 - L1)
            L1 = L2 + 1
            'AGGIORNO IL DATABASE DEL VALORE ATTUALE
            TB.Edit
            TB.Fields("ACTUAL_" & Format$(j, "0")).Value = stmp
            TB.Update
            If IsNull(TB.Fields("VARIABILE_" & Format$(j, "0")).Value) Then
              stmp1 = ""
            Else
              stmp1 = Trim$(TB.Fields("VARIABILE_" & Format$(j, "0")).Value)
            End If
            If (Len(stmp1) > 0) Then
              ret = WritePrivateProfileString(Gruppo, stmp1, stmp, NomeSPF & ".INI")
            Else
              stmp1 = TB.Fields("ITEMDDE_" & Format$(j, "0")).Value
              ret = WritePrivateProfileString(Gruppo, stmp1, stmp, NomeSPF & ".INI")
            End If
            Call OEM1.List3(j).AddItem(" " & TB.Fields("ACTUAL_" & Format$(j, "0")).Value)
          Next j
      End Select
    End If
    TB.MoveNext
    Loop
    TB.Close
    DB.Close
    Call INIZIALIZZO_CONTROLLI_OEM1(i1, i2, i3, Gruppo, MESSAGGI)
    WRITE_DIALOG Gruppo & " read from HD "
  Else
    'RITORNO AL LIVELLO 0
    ret = WritePrivateProfileString("OEM1", "LIVELLO", "1", Path_LAVORAZIONE_INI)
    Call OEM1_VERTICALI(IndiceTASTO, 1)
    OEM1.FraPARAMETRI.Visible = False
    OEM1.txtCODICE.Visible = False
    OEM1.imgCODICE.Visible = False
    OEM1.fraARCHIVIO.Visible = True
    OEM1.txtMEMO.Text = ""
    WRITE_DIALOG Nomegruppo & " NOT POSSIBLE: Operation aborted !!!"
  End If
  '
Exit Sub

errINIZIALIZZA_OEM1_OFFLINE:
  WRITE_DIALOG "Sub INIZIALIZZA_OEM1_OFFLINE: Error -> " & Err
  Resume Next

End Sub

Public Function INIZIALIZZA_OFFLINE(ByVal Gruppo As String, NomePezzo As String) As String
'
Dim i        As Integer
Dim j        As Integer
Dim L1       As Integer
Dim L2       As Integer
Dim stmp     As String
Dim Valore   As String
Dim DB       As Database
Dim TB       As Recordset
Dim RS       As Recordset
Dim NomeFile As String
Dim NomeSPF  As String
Dim iGRUPPO  As Integer
Dim ret      As Integer
Dim DATI_SPF As String
Dim DATI_DDE As String
Dim DATI_TXT As String
'
On Error GoTo errINIZIALIZZA_OFFLINE
  '
  INIZIALIZZA_OFFLINE = ""
  If (Len(NomePezzo) > 0) Then
    WRITE_DIALOG "Reading data from HD."
    Set DB = OpenDatabase(PathDB_ADDIN, True, False)
    '
    Set RS = DB.OpenRecordset(Gruppo & "_ARCHIVIO", dbOpenTable)
    RS.Index = "NOME_PEZZO"
    RS.Seek "=", NomePezzo
    If RS.NoMatch Then
      RS.Close
      DB.Close
      WRITE_DIALOG NomePezzo & " not found."
      Exit Function
    Else
      DATI_SPF = RS.Fields("DATI_SPF").Value
      DATI_DDE = RS.Fields("DATI_DDE").Value
      DATI_TXT = RS.Fields("DATI_TXT").Value
    End If
    RS.Close
    '
    NomeSPF = GetInfo("Configurazione", "NomeFileTemporaneoSPF", PathFILEINI)
    NomeSPF = g_chOemPATH & "\" & NomeSPF
    If (Dir(NomeSPF & ".SPF") <> "") Then Call Kill(NomeSPF & ".SPF")
    If (Dir(NomeSPF & ".INI") <> "") Then Call Kill(NomeSPF & ".INI")
    If (Dir(NomeSPF & ".DDE") <> "") Then Call Kill(NomeSPF & ".DDE")
    If (Dir(NomeSPF & ".TXT") <> "") Then Call Kill(NomeSPF & ".TXT")
    '
    Set TB = DB.OpenRecordset(Gruppo, dbOpenTable)
    TB.MoveFirst
    L1 = 1: L2 = 1
    Do Until TB.EOF
    If TB.Fields("ABILITATO").Value = "Y" Then
      iGRUPPO = TB.Fields("GRUPPO").Value
      If (iGRUPPO <> 1) And (iGRUPPO <> 2) And (iGRUPPO <> 3) Then iGRUPPO = 1
      Select Case iGRUPPO
        Case 1
          j = 1
            L2 = InStr(L1, DATI_TXT, ";")
            Valore = Mid$(DATI_TXT, L1, L2 - L1)
            L1 = L2 + 1
            L2 = InStr(L1, DATI_TXT, ";")
            L1 = L2 + 1
            GoSub SCRITTURA
        Case 2
          j = 2
            L2 = InStr(L1, DATI_TXT, ";")
            L1 = L2 + 1
            L2 = InStr(L1, DATI_TXT, ";")
            Valore = Mid$(DATI_TXT, L1, L2 - L1)
            L1 = L2 + 1
            GoSub SCRITTURA
        Case 3
          For j = 1 To 2
            L2 = InStr(L1, DATI_TXT, ";")
            Valore = Mid$(DATI_TXT, L1, L2 - L1)
            L1 = L2 + 1
            GoSub SCRITTURA
          Next j
      End Select
    End If
    TB.MoveNext
    Loop
    TB.Close
    '
    DB.Close
    INIZIALIZZA_OFFLINE = "TROVATO"
    WRITE_DIALOG Gruppo & " read from HD "
  Else
    WRITE_DIALOG NomePezzo & " NOT POSSIBLE: Operation aborted !!!"
  End If
  '
Exit Function

errINIZIALIZZA_OFFLINE:
  WRITE_DIALOG "Sub INIZIALIZZA_OFFLINE: Error -> " & Err
  Resume Next

SCRITTURA:
  If IsNull(TB.Fields("VARIABILE_" & Format$(j, "0")).Value) Then
    stmp = ""
  Else
    stmp = Trim$(TB.Fields("VARIABILE_" & Format$(j, "0")).Value)
  End If
  If (Len(stmp) > 0) Then
    ret = WritePrivateProfileString(Gruppo, stmp, Valore, NomeSPF & ".INI")
  Else
    ret = WritePrivateProfileString(Gruppo, TB.Fields("ITEMDDE_" & Format$(j, "0")).Value, Valore, NomeSPF & ".INI")
  End If
  TB.Edit
  TB.Fields("ACTUAL_" & Format$(j, "0")).Value = Valore
  TB.Update
Return

End Function

Sub AZZERO_CONTROLLI_OEM1()
'
Dim i As Integer
Dim j As Integer
'
On Error GoTo errAZZERO_CONTROLLI_OEM1
  '
  'INIZIALIZZO LE LISTE
  For i = 0 To 1
    OEM1.Label3(i).Visible = True
    OEM1.Label3(i).Caption = ""
  Next i
  For i = 0 To 1
    OEM1.Label4(i).Visible = True
    OEM1.Label4(i).Caption = ""
  Next i
  For i = 0 To 2
    OEM1.Label5(i).Visible = True
    OEM1.Label5(i).Caption = ""
  Next i
  For i = 0 To 1
    OEM1.List1(i).Clear
  Next i
  For i = 0 To 2
    If i <> 1 Then
      OEM1.List2(i).Clear
    End If
  Next i
  For i = 0 To 2
    OEM1.List3(i).Clear
  Next i
  'INIZIALIZZO LE CASELLE
  For i = 1 To 1
    OEM1.Text1(i).Visible = True
    OEM1.Text1(i).Text = ""
  Next i
  For i = 2 To 2
    OEM1.Text2(i).Visible = True
    OEM1.Text2(i).Text = ""
  Next i
  For i = 1 To 2
    OEM1.Text3(i).Visible = True
    OEM1.Text3(i).Text = ""
  Next i
  For i = 0 To 2
    OEM1.Label1(i).Visible = True
    OEM1.Label1(i).Caption = ""
  Next i
  '
Exit Sub

errAZZERO_CONTROLLI_OEM1:
  WRITE_DIALOG "Sub AZZERO_CONTROLLI_OEM1: Error -> " & Err
  Resume Next

End Sub

Sub INIZIALIZZO_CONTROLLI_OEM1(i1 As Integer, i2 As Integer, i3 As Integer, ByVal Gruppo As String, Optional INTESTAZIONI As String = "T1;T2;T3;T4;T5;")
'
Dim i As Integer
Dim j As Integer
'
Const Frazione1 = 0.5
Const Frazione2 = 0.4
'
On Error GoTo errINIZIALIZZO_CONTROLLI_OEM1
  '
  'RICAVO LE STRINGHE
  For j = 0 To 1
    i = InStr(INTESTAZIONI, ";")
    If (i > 0) Then
      OEM1.Label3(j).Caption = Left$(INTESTAZIONI, i - 1)
      INTESTAZIONI = Right$(INTESTAZIONI, Len(INTESTAZIONI) - i)
    End If
  Next j
  For j = 0 To 1
    i = InStr(INTESTAZIONI, ";")
    If (i > 0) Then
      OEM1.Label4(j).Caption = Left$(INTESTAZIONI, i - 1)
      INTESTAZIONI = Right$(INTESTAZIONI, Len(INTESTAZIONI) - i)
    End If
  Next j
  For j = 0 To 1
    i = InStr(INTESTAZIONI, ";")
    If (i > 0) Then
      OEM1.Label5(j).Caption = Left$(INTESTAZIONI, i - 1)
      INTESTAZIONI = Right$(INTESTAZIONI, Len(INTESTAZIONI) - i)
    End If
  Next j
  If (Len(INTESTAZIONI) > 0) Then OEM1.Label5(j).Caption = INTESTAZIONI
  'GRUPPO 1
  OEM1.List1(0).Height = OEM1.FraPARAMETRI.Height - OEM1.List1(0).Top - 50
  OEM1.List1(1).Height = OEM1.List1(0).Height
  OEM1.List1(0).Width = (OEM1.FraPARAMETRI.Width / 2) * (3 / 4)
  OEM1.List1(1).Width = (OEM1.FraPARAMETRI.Width / 2) * (1 / 4)
  OEM1.Label3(0).Width = OEM1.List1(0).Width
  OEM1.Label3(1).Width = OEM1.List1(1).Width
  OEM1.Label1(0).Width = OEM1.List1(0).Width
  OEM1.Label1(0).Height = OEM1.Text1(1).Height
  OEM1.Text1(1).Width = OEM1.List1(1).Width
  OEM1.List1(0).Left = 0
  OEM1.List1(1).Left = OEM1.List1(0).Left + OEM1.List1(0).Width
  OEM1.Label3(0).Left = OEM1.List1(0).Left
  OEM1.Label3(1).Left = OEM1.List1(1).Left
  OEM1.Label1(0).Left = OEM1.List1(0).Left
  OEM1.Text1(1).Left = OEM1.List1(1).Left
  'GRUPPO 2
  OEM1.List2(0).Width = (OEM1.FraPARAMETRI.Width / 2) * (3 / 4)
  OEM1.List2(1).Width = (OEM1.FraPARAMETRI.Width / 2) * (1 / 4)
  OEM1.List2(2).Width = (OEM1.FraPARAMETRI.Width / 2) * (1 / 4)
  OEM1.Label4(0).Width = OEM1.List2(0).Width
  OEM1.Label4(1).Width = OEM1.List2(1).Width
  OEM1.Label1(1).Width = OEM1.List2(0).Width
  OEM1.Label1(1).Height = OEM1.Text2(2).Height
  OEM1.Text2(2).Width = OEM1.List2(1).Width
  OEM1.List2(0).Left = (OEM1.FraPARAMETRI.Width / 2)
  OEM1.List2(1).Left = OEM1.List2(0).Left + OEM1.List2(0).Width
  OEM1.List2(2).Left = OEM1.List2(0).Left + OEM1.List2(0).Width
  OEM1.Label4(0).Left = OEM1.List2(0).Left
  OEM1.Label4(1).Left = OEM1.List2(1).Left
  OEM1.Label1(1).Left = OEM1.List2(0).Left
  OEM1.Text2(2).Left = OEM1.List2(1).Left
  'GRUPPO 3
  OEM1.List3(0).Width = (OEM1.FraPARAMETRI.Width / 2) * (2 / 4)
  OEM1.List3(1).Width = (OEM1.FraPARAMETRI.Width / 2) * (1 / 4)
  OEM1.List3(2).Width = (OEM1.FraPARAMETRI.Width / 2) * (1 / 4)
  OEM1.Label5(0).Width = OEM1.List3(0).Width
  OEM1.Label5(1).Width = OEM1.List3(1).Width
  OEM1.Label5(2).Width = OEM1.List3(2).Width
  OEM1.Label1(2).Width = OEM1.List3(0).Width
  OEM1.Label1(2).Height = OEM1.Text3(2).Height
  OEM1.Text3(1).Width = OEM1.List3(1).Width
  OEM1.Text3(2).Width = OEM1.List3(2).Width
  OEM1.List3(0).Left = (OEM1.FraPARAMETRI.Width / 2)
  OEM1.List3(1).Left = OEM1.List3(0).Left + OEM1.List3(0).Width
  OEM1.List3(2).Left = OEM1.List3(1).Left + OEM1.List3(1).Width
  OEM1.Label5(0).Left = OEM1.List3(0).Left
  OEM1.Label5(1).Left = OEM1.List3(1).Left
  OEM1.Label5(2).Left = OEM1.List3(2).Left
  OEM1.Label1(2).Left = OEM1.List3(0).Left
  OEM1.Text3(1).Left = OEM1.List3(1).Left
  OEM1.Text3(2).Left = OEM1.List3(2).Left
  '
  'NON VISUALIZZO IL PRIMO GRUPPO
  If (i1 <= 0) Then
    OEM1.List1(0).Visible = False
    OEM1.List1(1).Visible = False
    OEM1.Label1(0).Visible = False
    OEM1.Text1(1).Visible = False
    OEM1.Label3(0).Visible = False
    OEM1.Label3(1).Visible = False
  End If
  If (i2 <= 0) And (i3 <= 0) Then
    'NON VISUALIZZO IL SECONDO GRUPPO
    OEM1.List2(0).Visible = False
    OEM1.List2(1).Visible = False
    OEM1.List2(2).Visible = False
    OEM1.Label1(1).Visible = False
    OEM1.Text2(2).Visible = False
    OEM1.Label4(0).Visible = False
    OEM1.Label4(1).Visible = False
    'NON VISUALIZZO IL TERZO GRUPPO
    OEM1.List3(0).Visible = False
    OEM1.List3(1).Visible = False
    OEM1.List3(2).Visible = False
    OEM1.Label1(2).Visible = False
    OEM1.Text3(1).Visible = False
    OEM1.Text3(2).Visible = False
    OEM1.Label5(0).Visible = False
    OEM1.Label5(1).Visible = False
    OEM1.Label5(2).Visible = False
  ElseIf (i2 > 0) And (i3 <= 0) Then
    'VISUALIZZO IL SECONDO GRUPPO
    OEM1.List2(0).Visible = True
    'OEM1.List2(1).Visible = True
    OEM1.List2(2).Visible = True
    OEM1.Label1(1).Visible = True
    OEM1.Text2(2).Visible = True
    OEM1.Label4(0).Visible = True
    OEM1.Label4(1).Visible = True
    'ALLUNGO IL SECONDO GRUPPO AL MASSIMO
    OEM1.List2(0).Height = OEM1.List1(0).Height
    OEM1.List2(1).Height = OEM1.List1(0).Height
    OEM1.List2(2).Height = OEM1.List1(0).Height
    'NON VISUALIZZO IL TERZO GRUPPO
    OEM1.List3(0).Visible = False
    OEM1.List3(1).Visible = False
    OEM1.List3(2).Visible = False
    OEM1.Label1(2).Visible = False
    OEM1.Text3(1).Visible = False
    OEM1.Text3(2).Visible = False
    OEM1.Label5(0).Visible = False
    OEM1.Label5(1).Visible = False
    OEM1.Label5(2).Visible = False
    'ACCORCIO IL TERZO GRUPPO
    i = -70
    i = i + OEM1.List2(0).Height
    i = i + OEM1.Label1(1).Height
    i = i + OEM1.Label4(0).Height
    OEM1.List3(0).Height = OEM1.List1(0).Height * Frazione2
    OEM1.List3(1).Height = OEM1.List1(0).Height * Frazione2
    OEM1.List3(2).Height = OEM1.List1(0).Height * Frazione2
    OEM1.List3(0).Top = OEM1.List1(0).Top + i
    OEM1.List3(1).Top = OEM1.List1(0).Top + i
    OEM1.List3(2).Top = OEM1.List1(0).Top + i
    OEM1.Label1(2).Top = OEM1.Label1(0).Top + i
    OEM1.Text3(1).Top = OEM1.Text1(1).Top + i
    OEM1.Text3(2).Top = OEM1.Text1(1).Top + i
    OEM1.Label5(0).Top = OEM1.Label3(0).Top + i
    OEM1.Label5(1).Top = OEM1.Label3(0).Top + i
    OEM1.Label5(2).Top = OEM1.Label3(0).Top + i
  ElseIf (i2 > 0) And (i3 > 0) Then
    'VISUALIZZO IL SECONDO GRUPPO
    OEM1.List2(0).Visible = True
    'OEM1.List2(1).Visible = True
    OEM1.List2(2).Visible = True
    OEM1.Label1(1).Visible = True
    OEM1.Text2(2).Visible = True
    OEM1.Label4(0).Visible = True
    OEM1.Label4(1).Visible = True
    'ACCORCIO IL SECONDO GRUPPO
    OEM1.List2(0).Height = OEM1.List1(0).Height * Frazione1
    OEM1.List2(1).Height = OEM1.List1(0).Height * Frazione1
    OEM1.List2(2).Height = OEM1.List1(0).Height * Frazione1
    'VISUALIZZO IL TERZO GRUPPO
    OEM1.List3(0).Visible = True
    OEM1.List3(1).Visible = True
    OEM1.List3(2).Visible = True
    OEM1.Label1(2).Visible = True
    OEM1.Text3(1).Visible = True
    OEM1.Text3(2).Visible = True
    OEM1.Label5(0).Visible = True
    OEM1.Label5(1).Visible = True
    OEM1.Label5(2).Visible = True
    'ACCORCIO IL TERZO GRUPPO
    i = -70
    i = i + OEM1.List2(0).Height
    i = i + OEM1.Label1(1).Height
    i = i + OEM1.Label4(0).Height
    OEM1.List3(0).Height = OEM1.List1(0).Height * Frazione2
    OEM1.List3(1).Height = OEM1.List1(0).Height * Frazione2
    OEM1.List3(2).Height = OEM1.List1(0).Height * Frazione2
    OEM1.List3(0).Top = OEM1.List1(0).Top + i
    OEM1.List3(1).Top = OEM1.List1(0).Top + i
    OEM1.List3(2).Top = OEM1.List1(0).Top + i
    OEM1.Label1(2).Top = OEM1.Label1(0).Top + i
    OEM1.Text3(1).Top = OEM1.Text1(1).Top + i
    OEM1.Text3(2).Top = OEM1.Text1(1).Top + i
    OEM1.Label5(0).Top = OEM1.Label3(0).Top + i
    OEM1.Label5(1).Top = OEM1.Label3(0).Top + i
    OEM1.Label5(2).Top = OEM1.Label3(0).Top + i
  ElseIf (i2 <= 0) And (i3 > 0) Then
    'NON VISUALIZZO IL SECONDO GRUPPO
    OEM1.List2(0).Visible = False
    OEM1.List2(1).Visible = False
    OEM1.List2(2).Visible = False
    OEM1.Label1(1).Visible = False
    OEM1.Text2(2).Visible = False
    OEM1.Label4(0).Visible = False
    OEM1.Label4(1).Visible = False
    'VISUALIZZO IL TERZO GRUPPO
    OEM1.List3(0).Visible = True
    OEM1.List3(1).Visible = True
    OEM1.List3(2).Visible = True
    OEM1.Label1(2).Visible = True
    OEM1.Text3(1).Visible = True
    OEM1.Text3(2).Visible = True
    OEM1.Label5(0).Visible = True
    OEM1.Label5(1).Visible = True
    OEM1.Label5(2).Visible = True
    'ALLUNGO IL TERZO GRUPPO
    OEM1.List3(0).Height = OEM1.List1(0).Height
    OEM1.List3(1).Height = OEM1.List1(0).Height
    OEM1.List3(2).Height = OEM1.List1(0).Height
    OEM1.List3(0).Top = OEM1.List1(0).Top
    OEM1.List3(1).Top = OEM1.List1(0).Top
    OEM1.List3(2).Top = OEM1.List1(0).Top
    OEM1.Label1(2).Top = OEM1.Label1(0).Top
    OEM1.Text3(1).Top = OEM1.Text1(1).Top
    OEM1.Text3(2).Top = OEM1.Text1(1).Top
    OEM1.Label5(0).Top = OEM1.Label3(0).Top
    OEM1.Label5(1).Top = OEM1.Label3(0).Top
    OEM1.Label5(2).Top = OEM1.Label3(0).Top
  End If
  '
  'INIZIALIZZO LE CASELLE DI MODIFICA
  If OEM1.Text1(1).Visible = True Then
    OEM1.Text1(1).Enabled = True
    OEM1.Label1(0).Caption = OEM1.List1(0).List(0)
    OEM1.Text1(1).Text = OEM1.List1(1).List(0)
  End If
  If OEM1.Text2(2).Visible = True Then
    OEM1.Text2(2).Enabled = True
    OEM1.Label1(1).Caption = OEM1.List2(0).List(0)
    OEM1.Text2(2).Text = OEM1.List2(2).List(0)
  End If
  If OEM1.Text3(1).Visible = True And OEM1.Text3(2).Visible = True Then
    OEM1.Text3(1).Enabled = True
    OEM1.Text3(2).Enabled = True
    OEM1.Label1(2).Caption = OEM1.List3(0).List(0)
    OEM1.Text3(1).Text = OEM1.List3(1).List(0)
    OEM1.Text3(2).Text = OEM1.List3(2).List(0)
  End If
  For i = 0 To 1
    If OEM1.List1(i).Visible = True Then
      If OEM1.List1(i).ListCount > 0 Then OEM1.List1(i).ListIndex = 0
    End If
  Next i
  For i = 0 To 2
    If i <> 1 And OEM1.List2(i).Visible = True Then
      If OEM1.List2(i).ListCount > 0 Then OEM1.List2(i).ListIndex = 0
    End If
  Next i
  For i = 0 To 2
    If OEM1.List3(i).Visible = True Then
      If OEM1.List3(i).ListCount > 0 Then OEM1.List3(i).ListIndex = 0
    End If
  Next i
  If OEM1.Text1(1).Visible = True Then OEM1.Text1(1).SetFocus

Exit Sub

errINIZIALIZZO_CONTROLLI_OEM1:
  WRITE_DIALOG "Sub INIZIALIZZO_CONTROLLI_OEM1: ERROR -> " & Err
  Resume Next

End Sub

Sub INIZIALIZZA_OEM1_ARCHIVIO()
'
Dim ret         As Integer
Dim stmp        As String
Dim Atmp        As String * 255
Dim IndiceTASTO As Integer
Dim Nomegruppo  As String
Dim Gruppo      As String
Dim DB          As Database
Dim RS          As Recordset
Dim NomeSK      As String
Dim NomeSTO     As String
'
On Error GoTo errINIZIALIZZA_OEM1_ARCHIVIO
  '
  IndiceTASTO = val(GetInfo("OEM1", "IndiceTASTO", Path_LAVORAZIONE_INI))
  Gruppo = GetInfo("OEM1", "GRUPPO(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
  Gruppo = UCase$(Trim$(Gruppo))
  If (Len(Gruppo) > 0) Then
    stmp = GetInfo("OEM1", "NomeARC(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
    If (IsNumeric(stmp)) Then
      ret = val(stmp)
      ret = LoadString(g_hLanguageLibHandle, ret, Atmp, 255)
      If (ret > 0) Then stmp = Left$(Atmp, ret)
    End If
    OEM1.fraARCHIVIO.Caption = stmp
    '
    NomeSTO = GetInfo("OEM1", "NomeSTO(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
    NomeSTO = UCase$(Trim$(NomeSTO))
    If (IsNumeric(NomeSTO)) Then
      ret = val(NomeSTO)
      ret = LoadString(g_hLanguageLibHandle, ret, Atmp, 255)
      If (ret > 0) Then NomeSTO = Left$(Atmp, ret)
    End If
    'CONTROLLO SE INDIRIZZO UNA IMMAGINE \\
    ret = InStr(NomeSTO, "\")
    If (ret = 1) Then
      NomeSTO = Mid$(NomeSTO, ret)
      OEM1.btnSTORICO.Picture = LoadPicture(g_chOemPATH & "\Gapp4\images\" & NomeSTO)
      OEM1.btnSTORICO.Visible = True
    Else
      OEM1.btnSTORICO.Picture = LoadPicture("")
      OEM1.btnSTORICO.Visible = False
    End If
    OEM1.lstSTORICO.Clear
    OEM1.chkSTORICO(0).Visible = False
    OEM1.chkSTORICO(1).Visible = False
    OEM1.chkSTORICO(2).Visible = False
    OEM1.lstSTORICO.Visible = False
    '
    Nomegruppo = GetInfo("OEM1", "NomeGRP(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
    Nomegruppo = UCase$(Trim$(Nomegruppo))
    OEM1.txtNomePezzo.Text = Nomegruppo
    '
    OEM1.FraPARAMETRI.Visible = False
    OEM1.txtCODICE.Visible = False
    OEM1.imgCODICE.Visible = False
    OEM1.fraARCHIVIO.Visible = True
    OEM1.txtMEMO.Text = ""
    OEM1.lblINP_HD(0).Visible = False
    OEM1.txtINP_HD(0).Visible = False
    OEM1.PicELABORA_HD.Visible = False
    OEM1.PicGRUPPO_HD.Visible = False
    '
    OEM1.txtNomePezzo.Enabled = True
    OEM1.txtMEMO.Enabled = True
    OEM1.btnMEMO.Enabled = True
    If (OEM1.btnSTORICO.Visible = True) Then OEM1.btnSTORICO.Enabled = True
    '
    Call OEM1_INTESTAZIONE("OFF-LINE", "ARCHIVE1.bmp")
    '
    NomeSK = GetInfo("OEM1", "NomeSK(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
    NomeSK = UCase$(Trim$(NomeSK))
    If (IsNumeric(NomeSK)) Then
      ret = val(NomeSK)
      ret = LoadString(g_hLanguageLibHandle, ret, Atmp, 255)
      If (ret > 0) Then NomeSK = Left$(Atmp, ret)
    End If
    'CONTROLLO SE INDIRIZZO UNA IMMAGINE \\
    ret = InStr(NomeSK, "\")
    If (ret = 1) Then
      NomeSK = Mid$(NomeSK, ret)
      OEM1.Image4.Picture = LoadPicture(g_chOemPATH & "\Gapp4\images\" & NomeSK)
      OEM1.Image4.Visible = True
    Else
      If (Len(NomeSK) <= 0) Then NomeSK = "SK" & Format$(IndiceTASTO, "0")
      OEM1.Image4.Picture = LoadPicture("")
      OEM1.Image4.Visible = False
    End If
    '
    Call OEM1_CREAZIONE_ARCHIVIO(Gruppo)
    '
    Set DB = OpenDatabase(PathDB_ADDIN)
    Set RS = DB.OpenRecordset(Gruppo & "_ARCHIVIO", dbOpenDynaset)
    OEM1.lstARCHIVIO.Clear
    Do Until RS.EOF
      If IsNull(RS.Fields("NOME_PEZZO").Value) Then
        RS.MoveNext
      Else
        Call OEM1.lstARCHIVIO.AddItem(" " & RS.Fields("NOME_PEZZO").Value)
        If (RS.Fields("NOME_PEZZO").Value = OEM1.txtNomePezzo.Text) Then
          OEM1.txtMEMO.Text = RS.Fields("MEMO").Value
        End If
        RS.MoveNext
      End If
    Loop
    RS.Close
    DB.Close
    WRITE_DIALOG Gruppo & " ARCHIVE !!!"
  Else
    'RITORNO AL LIVELLO 0
    ret = WritePrivateProfileString("OEM1", "LIVELLO", "0", Path_LAVORAZIONE_INI)
    ret = WritePrivateProfileString("OEM1", "NomeGRP(" & Format$(IndiceTASTO, "0") & ")", "", Path_LAVORAZIONE_INI)
    Call OEM1_VERTICALI(IndiceTASTO, 0)
    OEM1.FraPARAMETRI.Visible = True
    OEM1.fraARCHIVIO.Visible = False
    OEM1.PicGRUPPO_HD.Visible = False
    WRITE_DIALOG "Operation not allowed!!!"
  End If
  '
Exit Sub

errINIZIALIZZA_OEM1_ARCHIVIO:
  WRITE_DIALOG "Sub INIZIALIZZA_OEM1_ARCHIVIO: Error -> " & Err
  Resume Next
  
End Sub

Sub OEM1_CANCELLARE_ARCHIVIO()
'
Dim IndiceTASTO As Integer
Dim Nomegruppo  As String
Dim Gruppo      As String
Dim DB          As Database
Dim RS          As Recordset
Dim iPEZZO      As Integer
Dim ret1        As Integer
Dim ret2        As Integer
Dim riga        As String
Dim Atmp        As String * 255
Dim stmp        As String
Dim ELENCO()    As String
Dim i           As Integer
Dim k           As Integer
'
On Error Resume Next
  '
  iPEZZO = OEM1.lstARCHIVIO.ListIndex
  Nomegruppo = Trim$(UCase$(OEM1.lstARCHIVIO.List(iPEZZO)))
  If (Len(Nomegruppo) > 0) Then
    ret1 = LoadString(g_hLanguageLibHandle, 1004, Atmp, 255)
    If (ret1 > 0) Then riga = Left$(Atmp, ret1)
    riga = riga & " " & Nomegruppo & " ? "
    StopRegieEvents
    ret1 = MsgBox(riga, 256 + 4 + 32, "WARNING")
    ResumeRegieEvents
    If (ret1 = 6) Then
      IndiceTASTO = val(GetInfo("OEM1", "IndiceTASTO", Path_LAVORAZIONE_INI))
      Gruppo = GetInfo("OEM1", "GRUPPO(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
      Gruppo = UCase$(Trim$(Gruppo))
      Set DB = OpenDatabase(PathDB_ADDIN)
      Set RS = DB.OpenRecordset(Gruppo & "_ARCHIVIO", dbOpenTable)
      RS.Index = "NOME_PEZZO"
      RS.Seek "=", Nomegruppo
      If RS.NoMatch Then
        WRITE_DIALOG Nomegruppo & " not found."
      Else
        stmp = RS.Fields("STORICO").Value
        ELENCO = Split(stmp, ";")
        If (UBound(ELENCO) > 0) Then
          ret1 = LoadString(g_hLanguageLibHandle, 2050, Atmp, 255)
          If (ret1 > 0) Then stmp = Left$(Atmp, ret1): stmp = stmp & Chr(10) & Chr(13)
          k = UBound(ELENCO): If (k >= 11) Then k = 11
          For i = 0 To k - 1
            stmp = stmp & Format$(i + 1, "###0") & "] " & ELENCO(i) & Chr(10) & Chr(13)
          Next i
          ret2 = MsgBox(stmp, 256 + 4 + 32, riga)
        Else
          ret2 = 6
        End If
        If (ret2 = 6) Then
          RS.Delete
          Call OEM1.lstARCHIVIO.RemoveItem(iPEZZO)
          Call OEM1.lstARCHIVIO.Refresh
          DoEvents
          WRITE_DIALOG Nomegruppo & ": DELETE OK!!!"
        Else
          WRITE_DIALOG "Operation aborted by user!!!"
        End If
      End If
      RS.Close
      DB.Close
    Else
      WRITE_DIALOG "Operation aborted by user!!!"
    End If
  Else
    WRITE_DIALOG "Operation not allowed!!!"
  End If
  '
End Sub
Sub OEM1_CARICARE_MOLA()

Dim TIPO_LAVORAZIONE  As String
Dim DB                As Database
Dim TB                As Recordset
Dim stmp              As String
Dim NroCar            As Integer
Dim i                 As Integer
Dim Parz1Stmp         As String
Dim Parz2Stmp         As String
Dim Pos               As Integer
Dim NroRec()          As String
Dim NomeVariabile(20) As String
Dim ValVariabile(20)  As String
Dim Gruppo            As String
Dim NomeSPF           As String
Dim IndiceTASTO       As Integer
Dim SEZIONE           As String
Dim ret               As Integer
Dim NomeMOLA          As String
Dim itmDDE            As String
Dim riga              As String

On Error GoTo errOEM1_CARICARE_MOLA

  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))

  IndiceTASTO = val(GetInfo("OEM1", "IndiceTASTO", Path_LAVORAZIONE_INI))
  NomeSPF = GetInfo("Configurazione", "NomeFileTemporaneoSPF", PathFILEINI)
  NomeSPF = UCase$(Trim$(NomeSPF))
  Gruppo = GetInfo("OEM1", "GRUPPO(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
  Gruppo = UCase$(Trim$(Gruppo))
  'INVECE DI CANCELLARE ADDIN.INI CANCELLO SOLAMENTE LA SEZIONE: SVG270417
  'If (Dir$("ADDIN.INI") <> "") Then Call Kill("ADDIN.INI")
  Call WritePrivateProfileString(Gruppo, vbNullString, vbNullString, g_chOemPATH & "\" & "ADDIN.INI")

  NomeMOLA = GetInfo("OEM1", "NomeGRP(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)

  If (Dir$(NomeSPF & ".SPF") <> "") Then
    Call SCRIVI_SEZIONE_INI(NomeSPF & ".SPF", "ADDIN.INI", Gruppo)
    ret = WritePrivateProfileString(Gruppo, "CODICE_MOLA", NomeMOLA, g_chOemPATH & "\" & "ADDIN.INI")
  Else
    'Codice selezionato = Trim$(UCase$(OEM1.FraPARAMETRI.Caption))
    riga = "Operazione non possibile. Prima salvare  il codice mola : " & NomeMOLA & " in archivio "
    MsgBox riga
  End If

  Set DB = OpenDatabase(PathDB, True, False)
  Set TB = DB.OpenRecordset(TIPO_LAVORAZIONE & "_CONFIG")

  TB.MoveFirst
  Do Until (TB.Fields("NOMEGRUPPO2").Value = "MOLA")
    TB.MoveNext
  Loop
  stmp = TB.Fields("SEQUENZA").Value
  TB.Close

  NroCar = Len(stmp)
  For i = 1 To NroCar
    Pos = InStr(i, stmp, "D")
    If Pos = 0 Then Exit For
    Parz1Stmp = Mid(stmp, 1, Pos - 1)
    Parz2Stmp = Mid(stmp, Pos + 1, NroCar)
    stmp = Parz1Stmp & ";" & Parz2Stmp
  Next i
  For i = 1 To NroCar
    Pos = InStr(i, stmp, "A")
    If Pos = 0 Then Exit For
    Parz1Stmp = Mid(stmp, 1, Pos - 1)
    Parz2Stmp = Mid(stmp, Pos + 1, NroCar)
    stmp = Parz1Stmp & ";" & Parz2Stmp
  Next i
  NroCar = Len(stmp)
  Pos = InStr(1, stmp, ";")
  Parz1Stmp = Mid(stmp, Pos, NroCar)
  NroCar = Len(Parz1Stmp)
  Pos = InStr(1, Parz1Stmp, "T")
  stmp = Mid(Parz1Stmp, 1, Pos - 1)

  NroRec = Split(stmp, ";")

  SEZIONE = "CNC_" & LETTERA_LAVORAZIONE

  Set TB = DB.OpenRecordset(TIPO_LAVORAZIONE)
  
  For i = 1 To UBound(NroRec)
    TB.Index = "INDICE"
    TB.Seek "=", NroRec(i)
    If TB.NoMatch Then
      WRITE_DIALOG "Record number : " & NroRec(i) & " not found."
    Else
      TB.Edit
      NomeVariabile(i) = TB.Fields("VARIABILE").Value
      ValVariabile(i) = GetInfo(Gruppo, NomeVariabile(i), g_chOemPATH & "\" & "ADDIN.INI")
      If TB.Fields("NOMESPF").Value = "ONLINE" Then
        itmDDE = TB.Fields("ITEMDDE").Value
        Call OPC_SCRIVI_DATO(itmDDE, ValVariabile(i))
        'Aggiorno campo Actual della Tabella
        TB.Fields("ACTUAL").Value = ValVariabile(i)
      Else
        'Aggiorno campo Actual della Tabella
        TB.Fields("ACTUAL").Value = ValVariabile(i)
        'Aggiorno file TIPO_LAVORAZIONE.INI
        ret = WritePrivateProfileString(SEZIONE, NomeVariabile(i), ValVariabile(i), g_chOemPATH & "\" & TIPO_LAVORAZIONE & ".INI")
      End If
    End If
  Next i
  TB.Update
  TB.Close

  DB.Close

Exit Sub

errOEM1_CARICARE_MOLA:
  WRITE_DIALOG "ERROR: " & Error(Err)
  Resume Next

End Sub

Sub OEM1_CARICARE_ARCHIVIO()
'
Dim IndiceTASTO   As Integer
Dim Gruppo        As String
Dim NomePezzotmp  As String
Dim NomePezzo     As String
Dim DB            As Database
Dim RS            As Recordset
Dim iPEZZO        As Integer
Dim ret           As Integer
Dim DATI_SPF      As String
Dim DATI_DDE      As String
Dim SORGENTE      As String
Dim DESTINAZIONE  As String
Dim NomeSPF       As String
Dim NomeDIR       As String
Dim v()           As String
Dim i             As Integer
Dim k             As Integer
Dim itmDDE        As String
Dim valDDE        As String
Dim rsp           As Integer
Dim stmp          As String
Dim riga          As String
Dim Atmp          As String * 255
'
On Error GoTo errOEM1_CARICARE_ARCHIVIO
  '
  iPEZZO = OEM1.lstARCHIVIO.ListIndex
  NomePezzotmp = Trim$(UCase$(OEM1.lstARCHIVIO.List(iPEZZO)))
  If (Len(NomePezzotmp) > 0) Then
    IndiceTASTO = val(GetInfo("OEM1", "IndiceTASTO", Path_LAVORAZIONE_INI))
    If (IndiceTASTO < 0) Or (IndiceTASTO > 7) Then
      WRITE_DIALOG "IndiceTASTO=" & Format$(IndiceTASTO, "0") & " Operation Aborted!!!"
      Exit Sub
    End If
    Gruppo = GetInfo("OEM1", "GRUPPO(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
    Gruppo = UCase$(Trim$(Gruppo))
    'NomePezzo = GetInfo("OEM1", "NomeGRP(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
    'NomePezzo = UCase$(Trim$(NomePezzo))
    Set DB = OpenDatabase(PathDB_ADDIN)
    Set RS = DB.OpenRecordset(Gruppo & "_ARCHIVIO", dbOpenTable)
    RS.Index = "NOME_PEZZO"
    RS.Seek "=", NomePezzotmp
    If RS.NoMatch Then
      WRITE_DIALOG NomePezzotmp & " not found."
    Else
      ret = LoadString(g_hLanguageLibHandle, 1003, Atmp, 255)
      If (ret > 0) Then riga = Left$(Atmp, ret)
      stmp = "" & Chr(13)
      stmp = stmp & " " & RS.Fields("NOME_PEZZO").Value
      stmp = stmp & " " & RS.Fields("DATA_ARCHIVIAZIONE").Value
      riga = riga & stmp & Chr(13)
      riga = riga & " --> " & Chr(13) & NomePezzotmp & " ? "
      'riga = riga & " --> " & Chr(13) & NomePezzo & " ? "
      StopRegieEvents
      rsp = MsgBox(riga, 256 + 4 + 32, "WARNING")
      ResumeRegieEvents
      If (rsp = 6) Then
        DATI_SPF = RS.Fields("DATI_SPF").Value
        DATI_DDE = RS.Fields("DATI_DDE").Value
        'TRASMISSIONE DEL SOTTOPROGRAMMA AL CNC
        If (DATI_SPF <> "NO_SPF") Then
          NomeSPF = GetInfo("Configurazione", "NomeFileTemporaneoSPF", PathFILEINI)
          NomeSPF = UCase$(Trim$(NomeSPF))
          '
          NomeDIR = GetInfo("OEM1", "NomeDIR(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
          NomeDIR = UCase$(Trim$(NomeDIR))
          If (InStr(NomeDIR, "*") > 0) Then NomeDIR = Replace(NomeDIR, "*", Format$(INDICE_LAVORAZIONE_SINGOLA, "0"))
          '
          Open g_chOemPATH & "\" & NomeSPF & ".SPF" For Output As #1
          Print #1, DATI_SPF
          Close #1
          SORGENTE = g_chOemPATH & "\" & NomeSPF & ".SPF"
          DESTINAZIONE = "/NC/WKS.DIR/" & NomeDIR & ".WPD/" & Gruppo & "." & "SPF"
          Set Session = GetObject("@SinHMIMCDomain.MCDomain")
          Call Session.CopyNC(SORGENTE, DESTINAZIONE, MCDOMAIN_COPY_NC)
          Set Session = Nothing
        Else
           riga = "Operazione non possibile. Prima salvare  il codice mola : " & NomePezzotmp & " in archivio "
           StopRegieEvents
           MsgBox (riga)
           ResumeRegieEvents
           RS.Close
           DB.Close
           Exit Sub
        End If
        'TRASMISSIONE VALORI SINGOLI
        If (DATI_DDE <> "NO_DDE") Then
          v = Split(DATI_DDE, ";")
          For i = 1 To UBound(v)
            k = InStr(v(i - 1), "=")
            itmDDE = Left$(v(i - 1), k - 1)
            itmDDE = Trim$(itmDDE)
            valDDE = Right$(v(i - 1), Len(v(i - 1)) - k)
            valDDE = Trim$(valDDE)
            Call OPC_SCRIVI_DATO(itmDDE, valDDE)
          Next i
        End If
        WRITE_DIALOG NomePezzotmp & " --> " & NomePezzotmp & ": LOAD OK!!!"
        ret = WritePrivateProfileString("OEM1", "NomeGRP(" & Format$(IndiceTASTO, "0") & ")", NomePezzotmp, Path_LAVORAZIONE_INI)
      Else
        WRITE_DIALOG "Operation aborted by user!!!"
      End If
    End If
    RS.Close
    DB.Close
  Else
    WRITE_DIALOG NomePezzo & " NOT POSSIBLE: Operation aborted !!!"
  End If
  
  Call OEM1_CARICARE_MOLA

Exit Sub

errOEM1_CARICARE_ARCHIVIO:
  WRITE_DIALOG "ERROR: " & Error(Err)
  Resume Next

End Sub

Sub OEM1_VERTICALI(ByVal IndiceTASTO As Integer, ByVal LIVELLO As Integer)
'
Dim IMG()   As String
Dim stmp    As String
Dim ret     As Long
Dim Atmp    As String * 255
Dim NomeSK  As String
'
On Error Resume Next
  '
  stmp = GetInfo("OEM1", "LIVELLO(" & Format$(IndiceTASTO, "0") & "," & Format$(LIVELLO, "0") & ")", Path_LAVORAZIONE_INI)
  If (Len(stmp) <= 0) Then Exit Sub
  IMG = Split(stmp, ";")
  For i = 0 To 7
    If IsNumeric(IMG(i)) Then
      ret = val(IMG(i))
      ret = LoadString(g_hLanguageLibHandle, ret, Atmp, 255)
      If (ret > 0) Then NomeSK = Left$(Atmp, ret)
      'CONTROLLO SE INDIRIZZO UNA IMMAGINE \\
      ret = InStr(NomeSK, "\")
      If (ret = 1) Then
        'CARICO L'IMMAGINE
        NomeSK = Mid$(NomeSK, ret)
      End If
      Call Change_SkTextOnScr(8 + i, NomeSK)
    Else
      Call Change_SkTextOnScr(8 + i, "\" & IMG(i) & ".BMP")
    End If
  Next i
    
End Sub

Sub OEM1_AZIONI(ByVal AZIONI As String)
'
Dim Nomegruppo  As String
Dim stmp        As String
Dim stmp1       As String
Dim COMANDO     As String
Dim X           As Variant
Dim LIVELLO     As Integer
Dim ret         As Integer
Dim IndiceTASTO As Integer
'
On Error GoTo errOEM1_AZIONI
  '
  If (Len(PathDB_ADDIN) <= 0) Then
    WRITE_DIALOG "ADDIN database it is not available!!!!"""
    Exit Sub
  End If
  '
  stmp = GetInfo("OEM1", "CARICATA", Path_LAVORAZIONE_INI)
  stmp = UCase$(Trim$(stmp))
  If (stmp <> "Y") Then Exit Sub
  '
  IndiceTASTO = val(GetInfo("OEM1", "IndiceTASTO", Path_LAVORAZIONE_INI))
  LIVELLO = GetInfo("OEM1", "LIVELLO", Path_LAVORAZIONE_INI)
  If (AZIONI = "AUMENTA") Then
      LIVELLO = LIVELLO + 1
      If (LIVELLO > 2) Then
        stmp1 = GetInfo("OEM1", "LIVELLO(" & Format$(IndiceTASTO, "0") & ",0)", Path_LAVORAZIONE_INI)
        If (Len(stmp1) > 0) Then LIVELLO = 0 Else LIVELLO = 1
      End If
      ret = WritePrivateProfileString("OEM1", "LIVELLO", Format$(LIVELLO, "0"), Path_LAVORAZIONE_INI)
  End If
  If (AZIONI = "RIDUCI") Then
      LIVELLO = LIVELLO - 1
      stmp1 = GetInfo("OEM1", "LIVELLO(" & Format$(IndiceTASTO, "0") & ",0)", Path_LAVORAZIONE_INI)
      If (Len(stmp1) > 0) Then
      If (LIVELLO < 0) Then LIVELLO = 2
      Else
      If (LIVELLO < 1) Then LIVELLO = 2
      End If
      ret = WritePrivateProfileString("OEM1", "LIVELLO", Format$(LIVELLO, "0"), Path_LAVORAZIONE_INI)
  End If
  '
  Select Case LIVELLO
    
    Case 2 'OFFLINE
      Select Case AZIONI
        Case "AUMENTA", "RIDUCI": Call OEM1_VERTICALI(IndiceTASTO, LIVELLO): Call INIZIALIZZA_OEM1_OFFLINE
        Case "STAMPARE":          Call OEM1_STAMPARE_OFFLINE
        Case "COPIARE":           WRITE_DIALOG "Softkey disenabled!!!"
        Case "ELABORA":           Call OEM1_ELABORA_FraPARAMETRI("N", stmp)
        Case "SALVARE":           Call OEM1_SALVARE_OFFLINE
        Case "CARICARE":          WRITE_DIALOG "Softkey disenabled!!!"
        Case "CANCELLARE":        WRITE_DIALOG "Softkey disenabled!!!"
        Case "COMANDO"
          COMANDO = g_chOemPATH & "\Comando.Bat"
          If (Dir$(COMANDO) <> "") Then
          X = Shell(COMANDO, 1)
          End If
      End Select
        
    Case 1 'ARCHIVIO
      Select Case AZIONI
        Case "AUMENTA", "RIDUCI": Call OEM1_VERTICALI(IndiceTASTO, LIVELLO): Call INIZIALIZZA_OEM1_ARCHIVIO
        Case "STAMPARE":          Call OEM1_STAMPARE_ARCHIVIO
        Case "COPIARE":           Call OEM1_COPIARE_ARCHIVIO
        Case "ELABORA":           Call OEM1_ELABORA_FraARCHIVIO("N", stmp)
        Case "SALVARE":           Call OEM1_SALVARE_ARCHIVIO
        Case "CARICARE":          Call OEM1_CARICARE_ARCHIVIO
        Case "CANCELLARE":        Call OEM1_CANCELLARE_ARCHIVIO
        Case "COMANDO"
          COMANDO = g_chOemPATH & "\Comando.Bat"
          If (Dir$(COMANDO) <> "") Then
          X = Shell(COMANDO, 1)
          End If
      End Select
    
    Case 0 'CNC
      OEM1.FraPARAMETRI.Visible = True
      OEM1.fraARCHIVIO.Visible = False
      OEM1.PicGRUPPO_HD.Visible = False
      Select Case AZIONI
        Case "AUMENTA", "RIDUCI": Call OEM1_VERTICALI(IndiceTASTO, LIVELLO): Call INIZIALIZZA_OEM1_CNC
        Case "STAMPARE":          Call OEM1_STAMPARE_CNC
        Case "COPIARE":           WRITE_DIALOG "Softkey disenabled!!!"
        Case "ELABORA":           Call OEM1_ELABORA_FraPARAMETRI("N", stmp)
        Case "SALVARE":           Call OEM1_SALVARE_CNC
        Case "CARICARE":          Call OEM1_CARICARE_CNC
        Case "CANCELLARE":        Call OEM1_CANCELLARE_CNC
        Case "COMANDO"
          COMANDO = g_chOemPATH & "\Comando.Bat"
          If (Dir$(COMANDO) <> "") Then
          X = Shell(COMANDO, 1)
          End If
      End Select

  End Select
  '
Exit Sub

errOEM1_AZIONI:
  WRITE_DIALOG "Sub OEM1_AZIONI: ERROR -> " & str(Err)
  Resume Next

End Sub

Sub OEM1_SALVARE_CNC()

Dim Gruppo      As String
Dim NomeFile    As String
Dim DB          As Database
Dim TB          As Recordset
Dim i           As Integer
Dim j           As Integer
Dim k           As Integer
Dim stmp        As String
Dim stmp1       As String
Dim stmp2       As String
Dim Atmp        As String * 255
Dim riga        As String
Dim ret         As Integer
Dim rsp         As Integer
Dim sPARAMETRO  As String
Dim NomePezzo   As String
Dim NomeDIR     As String
Dim NomeSPF     As String
Dim IndiceTASTO As Integer
Dim iGRUPPO     As Integer
'
On Error GoTo errOEM1_SALVARE_CNC
  '
  If (OEM1.PicERRORE.Visible = True) Then OEM1.PicERRORE.Visible = False
  '
  NomeSPF = GetInfo("Configurazione", "NomeFileTemporaneoSPF", PathFILEINI)
  IndiceTASTO = val(GetInfo("OEM1", "IndiceTASTO", Path_LAVORAZIONE_INI))
  NomePezzo = GetInfo("OEM1", "NomeGRP(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
  NomePezzo = UCase$(Trim$(NomePezzo))
  '
  NomeDIR = GetInfo("OEM1", "NomeDIR(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
  NomeDIR = UCase$(Trim$(NomeDIR))
  If (InStr(NomeDIR, "*") > 0) Then NomeDIR = Replace(NomeDIR, "*", Format$(INDICE_LAVORAZIONE_SINGOLA, "0"))
  '
  Gruppo = GetInfo("OEM1", "GRUPPO(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
  Gruppo = UCase$(Trim$(Gruppo))
  If (Len(Gruppo) > 0) Then
    NomeFile = GetInfo("Configurazione", "NomeFileTMP", PathFILEINI)
    NomeFile = UCase$(Trim$(NomeFile))
    NomeFile = g_chOemPATH & "\" & NomeFile
    ret = LoadString(g_hLanguageLibHandle, 1002, Atmp, 255)
    If (ret > 0) Then riga = Left$(Atmp, ret)
    stmp = "" & Chr(13)
    stmp = stmp & " " & NomePezzo
    riga = riga & stmp & Chr(13)
    riga = riga & " --> " & Chr(13) & NomeFile & " ? "
    StopRegieEvents
    rsp = MsgBox(riga, 256 + 4 + 32, "WARNING")
    ResumeRegieEvents
    If (rsp = 6) Then
      Set DB = OpenDatabase(PathDB_ADDIN, True, False)
      Set TB = DB.OpenRecordset(Gruppo, dbOpenTable)
      k = 1
      Do Until TB.EOF
        If TB.Fields("ABILITATO").Value = "Y" Then
          iGRUPPO = TB.Fields("GRUPPO").Value
          If (iGRUPPO <> 1) And (iGRUPPO <> 2) And (iGRUPPO <> 3) Then iGRUPPO = 1
          Select Case iGRUPPO
            Case 1
              i = 1
              j = 1
                GoSub COSTRUISCI_sPARAMETRO
                rsp = WritePrivateProfileString(Gruppo, "PAR(" & Format$(k, "0") & "," & Format$(i, "0") & "," & Format$(j, "0") & ")", sPARAMETRO, NomeFile)
                k = k + 1
            Case 2
              i = 2
              j = 2
                GoSub COSTRUISCI_sPARAMETRO
                rsp = WritePrivateProfileString(Gruppo, "PAR(" & Format$(k, "0") & "," & Format$(i, "0") & "," & Format$(j, "0") & ")", sPARAMETRO, NomeFile)
                k = k + 1
            Case 3
              i = 3
              For j = 1 To 2
                GoSub COSTRUISCI_sPARAMETRO
                rsp = WritePrivateProfileString(Gruppo, "PAR(" & Format$(k, "0") & "," & Format$(i, "0") & "," & Format$(j, "0") & ")", sPARAMETRO, NomeFile)
                k = k + 1
              Next j
          End Select
        End If
        TB.MoveNext
      Loop
      TB.Close
      DB.Close
      'AGGIUNGO LA DATA ED IL NOME DEL PEZZO SALVATO
      stmp = Date & "  " & Time
      rsp = WritePrivateProfileString(Gruppo, "DATE", stmp, NomeFile)
      rsp = WritePrivateProfileString(Gruppo, "NomePezzo", NomePezzo, NomeFile)
      WRITE_DIALOG Gruppo & ": OK!!!"
    Else
      WRITE_DIALOG Gruppo & " Operation aborted !!!"
    End If
  Else
    WRITE_DIALOG "Operation not allowed!!!"
  End If
  '
Exit Sub

errOEM1_SALVARE_CNC:
  WRITE_DIALOG "Sub OEM1_SALVARE_CNC: Error -> " & Err
  Resume Next
      
COSTRUISCI_sPARAMETRO:
  If (Len(TB.Fields("ACTUAL_" & Format(j, "0")).Value) > 0) Then
    sPARAMETRO = TB.Fields("ACTUAL_" & Format(j, "0")).Value & " ; "
    stmp1 = TB.Fields("ACTUAL_" & Format(j, "0")).Value
  Else
    sPARAMETRO = " ; "
    stmp1 = ""
  End If
  If (Len(TB.Fields("NOME_" & LINGUA).Value) > 0) Then
    sPARAMETRO = sPARAMETRO & TB.Fields("NOME_" & LINGUA).Value
    stmp2 = TB.Fields("NOME_" & LINGUA).Value
  Else
    sPARAMETRO = sPARAMETRO & " ; Nome parametro non definito nella tabella."
    stmp2 = ""
  End If
  WRITE_DIALOG stmp2 & "= " & stmp1
Return

End Sub

Sub OEM1_CARICARE_CNC()
'
Dim Gruppo      As String
Dim NomeFile    As String
Dim DB          As Database
Dim TB          As Recordset
Dim i           As Integer
Dim i1          As Integer
Dim i2          As Integer
Dim i3          As Integer
Dim j           As Integer
Dim k           As Integer
Dim kj          As Integer
Dim stmp        As String
Dim stmp1       As String
Dim stmp2       As String
Dim TipoWDW     As String
Dim rsp         As Integer
Dim k1          As Integer
Dim Atmp        As String * 255
Dim riga        As String
Dim ret         As Integer
Dim sRESET      As String
Dim iRESET      As Integer
Dim sPARAMETRO  As String
Dim SVG         As String
Dim IndiceTASTO As Integer
Dim NomeDIR     As String
Dim iGRUPPO     As Integer
Dim NomePezzo   As String
'
On Error GoTo errOEM1_CARICARE_CNC
  '
  iRESET = CONTROLLO_STATO_RESET
  If (iRESET = 1) Then
    If (OEM1.PicERRORE.Visible = True) Then OEM1.PicERRORE.Visible = False
    '
    IndiceTASTO = val(GetInfo("OEM1", "IndiceTASTO", Path_LAVORAZIONE_INI))
    '
    NomeDIR = GetInfo("OEM1", "NomeDIR(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
    NomeDIR = UCase$(Trim$(NomeDIR))
    If (InStr(NomeDIR, "*") > 0) Then NomeDIR = Replace(NomeDIR, "*", Format$(INDICE_LAVORAZIONE_SINGOLA, "0"))
    '
    Gruppo = GetInfo("OEM1", "GRUPPO(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
    Gruppo = UCase$(Trim$(Gruppo))
    If (Len(Gruppo) > 0) Then
      NomeFile = GetInfo("Configurazione", "NomeFileTMP", PathFILEINI)
      NomeFile = UCase$(Trim$(NomeFile))
      NomeFile = g_chOemPATH & "\" & NomeFile
      '
      NomePezzo = GetInfo("OEM1", "NomeGRP(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
      NomePezzo = UCase$(Trim$(NomePezzo))
      '
      ret = LoadString(g_hLanguageLibHandle, 1003, Atmp, 255)
      If (ret > 0) Then riga = Left$(Atmp, ret)
      stmp = "" & Chr(13)
      stmp = stmp & " " & GetInfo(Gruppo, "NomePezzo", NomeFile)
      stmp = stmp & " " & GetInfo(Gruppo, "DATE", NomeFile)
      riga = riga & stmp & Chr(13)
      riga = riga & " --> " & Chr(13) & NomePezzo & " ? "
      StopRegieEvents
      rsp = MsgBox(riga, 256 + 4 + 32, "WARNING")
      ResumeRegieEvents
      If (rsp = 6) Then
        If (Len(NomeDIR) > 0) Then ret = WritePrivateProfileString(Gruppo, vbNullString, vbNullString, PathFileSPF)
        'INIZIALIZZO LE LISTE
        For i = 1 To 1
          If (OEM1.List1(i).Visible = True) Then
            OEM1.Text1(i).Enabled = False
            OEM1.List1(i).Clear
          End If
        Next i
        For i = 1 To 2
          If (i <> 1) And (OEM1.List2(i).Visible = True) Then
            OEM1.Text2(i).Enabled = False
            OEM1.List2(i).Clear
          End If
        Next i
        For i = 1 To 2
          If (OEM1.List3(i).Visible = True) Then
            OEM1.Text3(i).Enabled = False
            OEM1.List3(i).Clear
          End If
        Next i
        Set DB = OpenDatabase(PathDB_ADDIN, True, False)
        Set TB = DB.OpenRecordset(Gruppo, dbOpenTable)
        k = 1: i1 = 0: i2 = 0: i3 = 0
        Do Until TB.EOF
          If TB.Fields("ABILITATO").Value = "Y" Then
            iGRUPPO = TB.Fields("GRUPPO").Value
            If (iGRUPPO <> 1) And (iGRUPPO <> 2) And (iGRUPPO <> 3) Then iGRUPPO = 1
            Select Case iGRUPPO
              Case 1
                i = 1
                j = 1
                  If (TB.Fields("MODIFICABILE").Value = "Y") Then
                    sPARAMETRO = GetInfo(Gruppo, "PAR(" & Format$(k, "0") & "," & Format$(i, "0") & "," & Format$(j, "0") & ")", NomeFile)
                  Else
                    sPARAMETRO = TB.Fields("ACTUAL_" & Format$(j, "0")).Value
                  End If
                  If (Len(sPARAMETRO) <= 0) Then sPARAMETRO = ";"
                  GoSub AGGIORNA
                  k = k + 1
                  Call OEM1.List1(j).AddItem(" " & sPARAMETRO)
                i1 = i1 + 1
              Case 2
                i = 2
                j = 2
                  If (TB.Fields("MODIFICABILE").Value = "Y") Then
                    sPARAMETRO = GetInfo(Gruppo, "PAR(" & Format$(k, "0") & "," & Format$(i, "0") & "," & Format$(j, "0") & ")", NomeFile)
                  Else
                    sPARAMETRO = TB.Fields("ACTUAL_" & Format$(j, "0")).Value
                  End If
                  If (Len(sPARAMETRO) <= 0) Then sPARAMETRO = ";"
                  GoSub AGGIORNA
                  k = k + 1
                  Call OEM1.List2(j).AddItem(" " & sPARAMETRO)
                i2 = i2 + 1
              Case 3
                i = 3
                For j = 1 To 2
                  If (TB.Fields("MODIFICABILE").Value = "Y") Then
                    sPARAMETRO = GetInfo(Gruppo, "PAR(" & Format$(k, "0") & "," & Format$(i, "0") & "," & Format$(j, "0") & ")", NomeFile)
                  Else
                    sPARAMETRO = TB.Fields("ACTUAL_" & Format$(j, "0")).Value
                  End If
                  If (Len(sPARAMETRO) <= 0) Then sPARAMETRO = ";"
                  GoSub AGGIORNA
                  k = k + 1
                  Call OEM1.List3(j).AddItem(" " & sPARAMETRO)
                Next j
                i3 = i3 + 1
            End Select
          End If
          TB.MoveNext
        Loop
        TB.Close
        DB.Close
        'ATTIVAZIONE DELLE CASELLE DI INSERIMENTO
        For i = 1 To 1
          If (OEM1.List1(i).Visible = True) Then
            OEM1.Text1(i).Enabled = True
          End If
        Next i
        For i = 1 To 2
          If (i <> 1) And (OEM1.List2(i).Visible = True) Then
            OEM1.Text2(i).Enabled = True
          End If
        Next i
        For i = 1 To 2
          If (OEM1.List3(i).Visible = True) Then
            OEM1.Text3(i).Enabled = True
          End If
        Next i
            If (OEM1.Text1(1).Visible = True) Then
          OEM1.Text1(1).SetFocus
        ElseIf (OEM1.Text2(2).Visible = True) Then
          OEM1.Text2(2).SetFocus
        ElseIf (OEM1.Text3(1).Visible = True) Then
          OEM1.Text3(1).SetFocus
        End If
        Call INVIO_SOTTOPROGRAMMA_OEM1(Gruppo, NomeDIR)
      Else
        WRITE_DIALOG "Operation aborted by user!!!"
      End If
    Else
      WRITE_DIALOG "Operation not allowed!!!"
    End If
  Else
    WRITE_DIALOG "Data cannot be modified: CNC is not on RESET state"
  End If
  '
Exit Sub

errOEM1_CARICARE_CNC:
  WRITE_DIALOG "Sub OEM1_CARICARE_CNC: Error -> " & Err
  Resume Next
      
AGGIORNA:
Dim cTIPO As String
Dim cLIMITI As String
  '
  If (Len(TB.Fields("LIMITI").Value) > 0) Then
    cLIMITI = TB.Fields("LIMITI").Value
  Else
    cLIMITI = ""
  End If
  If (Len(TB.Fields("TIPO").Value) > 0) Then
    cTIPO = TB.Fields("TIPO").Value
  Else
    cTIPO = ""
  End If
  cLIMITI = Trim$(cLIMITI)
  cTIPO = UCase$(Trim$(cTIPO))
  k1 = InStr(sPARAMETRO, ";")
  If (k1 > 0) Then
    sPARAMETRO = Trim$(Left$(sPARAMETRO, k1 - 1))
    Select Case cTIPO
      Case "S", "N"
        SVG = sPARAMETRO
      Case "A"
        SVG = Chr$(34) & sPARAMETRO & Chr$(34)
    End Select
    If IsNull(TB.Fields("VARIABILE_" & Format(j, "0")).Value) Then
      stmp1 = ""
    Else
      stmp1 = Trim$(TB.Fields("VARIABILE_" & Format(j, "0")).Value)
    End If
    If (Len(NomeDIR) > 0) And (Len(stmp1) > 0) Then
      ret = WritePrivateProfileString(Gruppo, stmp1, SVG, PathFileSPF)
    Else
      stmp1 = TB.Fields("ITEMDDE_" & Format$(j, "0")).Value
      stmp1 = Replace(stmp1, "*", INDICE_LAVORAZIONE_SINGOLA + 1)
      Call OPC_SCRIVI_DATO(stmp1, sPARAMETRO)
    End If
    'AGGIORNO IL VALORE NEL DATABASE
    TB.Edit
    TB.Fields("ACTUAL_" & Format$(j, "0")).Value = sPARAMETRO
    TB.Update
    'RISCRIVO IL COMMENTO
    WRITE_DIALOG TB.Fields("NOME_" & LINGUA).Value & " = " & sPARAMETRO
  End If
Return
      
End Sub

Sub OEM1_CANCELLARE_CNC()
'
Dim Gruppo      As String
Dim DB          As Database
Dim TB          As Recordset
Dim i           As Integer
Dim j           As Integer
Dim k           As Integer
Dim Atmp        As String * 255
Dim stmp        As String
Dim stmp1       As String
Dim stmp2       As String
Dim riga        As String
Dim i1          As Integer
Dim i2          As Integer
Dim i3          As Integer
Dim rsp         As Integer
Dim ret         As Integer
Dim sRESET      As String
Dim iRESET      As Integer
Dim sPARAMETRO  As String
Dim cTIPO       As String
Dim cLIMITI     As String
Dim SVG         As String
Dim NomeDIR     As String
Dim IndiceTASTO As Integer
Dim iGRUPPO     As Integer
'
On Error GoTo errOEM1_CANCELLARE_CNC
  '
  If (OEM1.PicERRORE.Visible = True) Then OEM1.PicERRORE.Visible = False
  '
  IndiceTASTO = val(GetInfo("OEM1", "IndiceTASTO", Path_LAVORAZIONE_INI))
  Gruppo = GetInfo("OEM1", "GRUPPO(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
  Gruppo = UCase$(Trim$(Gruppo))
  If (Len(Gruppo) <= 0) Then
    WRITE_DIALOG "Operation not allowed!!!"
    Exit Sub
  End If
  '
  NomeDIR = GetInfo("OEM1", "NomeDIR(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
  NomeDIR = UCase$(Trim$(NomeDIR))
  If (InStr(NomeDIR, "*") > 0) Then NomeDIR = Replace(NomeDIR, "*", Format$(INDICE_LAVORAZIONE_SINGOLA, "0"))
  '
  ret = LoadString(g_hLanguageLibHandle, 1004, Atmp, 255)
  If (ret > 0) Then riga = Left$(Atmp, ret)
  riga = stmp & riga
  riga = riga & " " & OEM1.Label3(0).Caption & " ? "
  StopRegieEvents
  rsp = MsgBox(riga, 256 + 4 + 32, "WARNING: " & OEM1.FraPARAMETRI.Caption)
  ResumeRegieEvents
  '
  If (rsp = 6) Then
    iRESET = CONTROLLO_STATO_RESET
    If (iRESET = 1) Then
      If (Len(NomeDIR) > 0) Then ret = WritePrivateProfileString(Gruppo, vbNullString, vbNullString, PathFileSPF)
      'INIZIALIZZO LE LISTE
      For i = 1 To 1
        If (OEM1.List1(i).Visible = True) Then
          OEM1.Text1(i).Enabled = False
          OEM1.List1(i).Clear
        End If
      Next i
      For i = 1 To 2
        If (i <> 1) And (OEM1.List2(i).Visible = True) Then
          OEM1.Text2(i).Enabled = False
          OEM1.List2(i).Clear
        End If
      Next i
      For i = 1 To 2
        If (OEM1.List3(i).Visible = True) Then
          OEM1.Text3(i).Enabled = False
          OEM1.List3(i).Clear
        End If
      Next i
      Set DB = OpenDatabase(PathDB_ADDIN, True, False)
      Set TB = DB.OpenRecordset(Gruppo, dbOpenTable)
      'SCORRO TUTTI I VALORI DELLA TABELLA DEL PARTICOLARE GRUPPO DI DATI
      i1 = 0: i2 = 0: i3 = 0
      Do Until TB.EOF
        If TB.Fields("ABILITATO").Value = "Y" Then
          iGRUPPO = TB.Fields("GRUPPO").Value
          If (iGRUPPO <> 1) And (iGRUPPO <> 2) And (iGRUPPO <> 3) Then iGRUPPO = 1
          Select Case iGRUPPO
            Case 1
              j = 1
                GoSub AGGIORNA
                Call OEM1.List1(j).AddItem(" " & sPARAMETRO)
              i1 = i1 + 1
            Case 2
              j = 2
                GoSub AGGIORNA
                Call OEM1.List2(j).AddItem(" " & sPARAMETRO)
              i2 = i2 + 1
            Case 3
              For j = 1 To 2
                GoSub AGGIORNA
                Call OEM1.List3(j).AddItem(" " & sPARAMETRO)
              Next j
              i3 = i3 + 1
          End Select
        End If
        TB.MoveNext
      Loop
      TB.Close
      DB.Close
      'ATTIVAZIONE DELLE CASELLE DI INSERIMENTO
      For i = 1 To 1
        If (OEM1.List1(i).Visible = True) Then
          OEM1.Text1(i).Enabled = True
        End If
      Next i
      For i = 1 To 2
        If (i <> 1) And (OEM1.List2(i).Visible = True) Then
          OEM1.Text2(i).Enabled = True
        End If
      Next i
      For i = 1 To 2
        If (OEM1.List3(i).Visible = True) Then
          OEM1.Text3(i).Enabled = True
        End If
      Next i
      Call INVIO_SOTTOPROGRAMMA_OEM1(Gruppo, NomeDIR)
    Else
      WRITE_DIALOG "Data cannot be modified: CNC is not on RESET state"
    End If
  Else
    WRITE_DIALOG "Operation aborted by user!!!"
  End If
  '
Exit Sub

errOEM1_CANCELLARE_CNC:
  WRITE_DIALOG "Sub OEM1_CANCELLARE_CNC: Error -> " & Err
  Resume Next
  
AGGIORNA:
  If (TB.Fields("MODIFICABILE").Value = "Y") Then
    If (Len(TB.Fields("LIMITI").Value) > 0) Then
      cLIMITI = TB.Fields("LIMITI").Value
    Else
      cLIMITI = ""
    End If
    If (Len(TB.Fields("TIPO").Value) > 0) Then
      cTIPO = TB.Fields("TIPO").Value
    Else
      cTIPO = ""
    End If
    cLIMITI = Trim$(cLIMITI)
    cTIPO = UCase$(Trim$(cTIPO))
    Select Case cTIPO
      Case "S"
        ret = InStr(cLIMITI, ",")
        If (ret > 0) Then
          sPARAMETRO = Trim$(Left$(cLIMITI, ret - 1))
        Else
          sPARAMETRO = "0"
        End If
        'SENZA APICI
        SVG = sPARAMETRO
      Case "A"
        ret = InStr(cLIMITI, ",")
        If (ret > 0) Then
          sPARAMETRO = Trim$(Left$(cLIMITI, ret - 1))
        Else
          sPARAMETRO = ""
        End If
        'CON APICI
        SVG = Chr$(34) & sPARAMETRO & Chr$(34)
      Case "N"
        sPARAMETRO = "0"
        'SENZA APICI
        SVG = sPARAMETRO
    End Select
    If IsNull(TB.Fields("VARIABILE_" & Format(j, "0")).Value) Then
      stmp1 = ""
    Else
      stmp1 = Trim$(TB.Fields("VARIABILE_" & Format(j, "0")).Value)
    End If
    If (Len(NomeDIR) > 0) And (Len(stmp1) > 0) Then
      ret = WritePrivateProfileString(Gruppo, stmp1, SVG, PathFileSPF)
    Else
      stmp1 = TB.Fields("ITEMDDE_" & Format$(j, "0")).Value
      stmp1 = Replace(stmp1, "*", INDICE_LAVORAZIONE_SINGOLA + 1)
      Call OPC_SCRIVI_DATO(stmp1, sPARAMETRO)
    End If
  Else
    sPARAMETRO = TB.Fields("ACTUAL_" & Format$(j, "0")).Value
  End If
  'AGGIORNO IL VALORE NEL DATABASE
  TB.Edit
  TB.Fields("ACTUAL_" & Format$(j, "0")).Value = sPARAMETRO
  TB.Update
  'RISCRIVO IL COMMENTO
  WRITE_DIALOG TB.Fields("NOME_" & LINGUA).Value & " = " & sPARAMETRO
Return

End Sub

Sub OEM1_SALVARE_ARCHIVIO()
'
Dim IndiceTASTO As Integer
Dim Nomegruppo  As String
Dim Gruppo      As String
Dim NomeSPF     As String
Dim DB          As Database
Dim RS          As Recordset
Dim DATI_SPF    As String
Dim DATI_DDE    As String
Dim DATI_TXT    As String
Dim rsp         As Integer
Dim ret         As Integer
Dim riga        As String
Dim Atmp        As String * 255
Dim stmp        As String
'
On Error GoTo errOEM1_SALVARE_ARCHIVIO
  '
  Nomegruppo = Trim$(UCase$(OEM1.txtNomePezzo.Text))
  If (Len(Nomegruppo) > 0) Then
    IndiceTASTO = val(GetInfo("OEM1", "IndiceTASTO", Path_LAVORAZIONE_INI))
    ret = WritePrivateProfileString("OEM1", "NomeGRP(" & Format$(IndiceTASTO, "0") & ")", Nomegruppo, Path_LAVORAZIONE_INI)
    Call INIZIALIZZA_OEM1_CNC
    Gruppo = GetInfo("OEM1", "GRUPPO(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
    Gruppo = UCase$(Trim$(Gruppo))
    NomeSPF = GetInfo("Configurazione", "NomeFileTemporaneoSPF", PathFILEINI)
    NomeSPF = g_chOemPATH & "\" & NomeSPF
    If (Dir$(NomeSPF & ".SPF") <> "") Then
      Open NomeSPF & ".SPF" For Input As #1
      DATI_SPF = Input$(LOF(1), 1)
      Close #1
    Else
      DATI_SPF = "NO_SPF"
    End If
    If (Dir$(NomeSPF & ".DDE") <> "") Then
      Open NomeSPF & ".DDE" For Input As #1
      DATI_DDE = Input$(LOF(1), 1)
      Close #1
      DATI_DDE = Trim$(DATI_DDE)
      If (Len(DATI_DDE) <= 0) Then DATI_DDE = "NO_DDE"
    Else
      DATI_DDE = "NO_DDE"
    End If
    If (Dir$(NomeSPF & ".TXT") <> "") Then
      Open NomeSPF & ".TXT" For Input As #1
      DATI_TXT = Input$(LOF(1), 1)
      Close #1
      DATI_TXT = Trim$(DATI_TXT)
      If (Len(DATI_TXT) <= 0) Then DATI_TXT = "NO_TXT"
    Else
      DATI_TXT = "NO_TXT"
    End If
    Set DB = OpenDatabase(PathDB_ADDIN)
    Set RS = DB.OpenRecordset(Gruppo & "_ARCHIVIO", dbOpenDynaset)
    RS.MoveFirst
    Call RS.FindFirst("NOME_PEZZO='" & UCase(Trim(Nomegruppo)) & "'")
    If RS.NoMatch Then
      RS.AddNew
      RS.Fields("NOME_PEZZO").Value = Nomegruppo
      RS.Fields("DATA_ARCHIVIAZIONE").Value = Now
      RS.Fields("DATI_SPF").Value = DATI_SPF
      RS.Fields("DATI_DDE").Value = DATI_DDE
      RS.Fields("DATI_TXT").Value = DATI_TXT
      RS.Fields("MEMO").Value = Trim$(OEM1.txtMEMO.Text)
      RS.Update
      RS.Close
      WRITE_DIALOG Nomegruppo & ": SAVE OK!!!"
      If (OEM1.fraARCHIVIO.Visible = True) Then
        Set RS = DB.OpenRecordset(Gruppo & "_ARCHIVIO", dbOpenDynaset)
        OEM1.lstARCHIVIO.Clear
        Do Until RS.EOF
        If IsNull(RS.Fields("NOME_PEZZO").Value) Then
          RS.MoveNext
        Else
          Call OEM1.lstARCHIVIO.AddItem(" " & RS.Fields("NOME_PEZZO").Value)
          RS.MoveNext
        End If
        Loop
        RS.Close
      End If
    Else
      ret = LoadString(g_hLanguageLibHandle, 1002, Atmp, 255)
      If (ret > 0) Then riga = Left$(Atmp, ret)
      stmp = "" & Chr(13)
      stmp = stmp & " " & Nomegruppo
      stmp = stmp & " " & Now
      riga = riga & stmp & Chr(13)
      riga = riga & " --> " & Chr(13) & " " & Nomegruppo & " " & RS.Fields("DATA_ARCHIVIAZIONE").Value & " ? "
      StopRegieEvents
      rsp = MsgBox(riga, 256 + 4 + 32, "WARNING")
      ResumeRegieEvents
      If (rsp = 6) Then
        RS.Edit
        RS.Fields("NOME_PEZZO").Value = Nomegruppo
        RS.Fields("DATA_ARCHIVIAZIONE").Value = Now
        RS.Fields("DATI_SPF").Value = DATI_SPF
        RS.Fields("DATI_DDE").Value = DATI_DDE
        RS.Fields("DATI_TXT").Value = DATI_TXT
        RS.Fields("MEMO").Value = Trim$(OEM1.txtMEMO.Text)
        RS.Update
        RS.Close
        WRITE_DIALOG Nomegruppo & ": SAVE OK!!!"
      Else
        WRITE_DIALOG "Operation aborted by user!!!"
      End If
    End If
    DB.Close
  Else
    WRITE_DIALOG Nomegruppo & " NOT POSSIBLE: Operation aborted !!!"
  End If
  '
Exit Sub

errOEM1_SALVARE_ARCHIVIO:
  WRITE_DIALOG "Sub OEM1_SALVARE_ARCHIVIO: Error -> " & Err
  Resume Next
  
End Sub

Sub OEM1_SALVARE_MEMO()
'
Dim IndiceTASTO As Integer
Dim Nomegruppo  As String
Dim Gruppo      As String
Dim NomeSPF     As String
Dim DB          As Database
Dim RS          As Recordset
Dim DATI_SPF    As String
Dim DATI_DDE    As String
Dim DATI_TXT    As String
Dim rsp         As Integer
Dim ret         As Integer
Dim riga        As String
Dim Atmp        As String * 255
Dim stmp        As String
'
On Error GoTo errOEM1_SALVARE_ARCHIVIO
  '
  IndiceTASTO = val(GetInfo("OEM1", "IndiceTASTO", Path_LAVORAZIONE_INI))
  Gruppo = GetInfo("OEM1", "GRUPPO(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
  Gruppo = UCase$(Trim$(Gruppo))
  Nomegruppo = Trim$(UCase$(OEM1.txtNomePezzo.Text))
  If (Len(Nomegruppo) > 0) And (Len(Gruppo) > 0) Then
    Set DB = OpenDatabase(PathDB_ADDIN)
    Set RS = DB.OpenRecordset(Gruppo & "_ARCHIVIO", dbOpenDynaset)
    RS.MoveFirst
    Call RS.FindFirst("NOME_PEZZO='" & UCase(Trim(Nomegruppo)) & "'")
    If RS.NoMatch Then
      WRITE_DIALOG "Operation cannot be performed!!!"
    Else
      ret = LoadString(g_hLanguageLibHandle, 1002, Atmp, 255)
      If (ret > 0) Then riga = Left$(Atmp, ret) & " MEMO"
      stmp = "" & Chr(13)
      stmp = stmp & " " & Nomegruppo
      stmp = stmp & " " & Now
      riga = riga & stmp & Chr(13)
      riga = riga & " --> " & Chr(13) & " " & Nomegruppo & " " & RS.Fields("DATA_ARCHIVIAZIONE").Value & " ? "
      StopRegieEvents
      rsp = MsgBox(riga, 256 + 4 + 32, "WARNING: MEMO")
      ResumeRegieEvents
      If (rsp = 6) Then
        RS.Edit
        RS.Fields("NOME_PEZZO").Value = Nomegruppo
        RS.Fields("DATA_ARCHIVIAZIONE").Value = Now
        RS.Fields("MEMO").Value = Trim$(OEM1.txtMEMO.Text)
        RS.Update
        WRITE_DIALOG Nomegruppo & ": SAVE OK!!!"
      Else
        WRITE_DIALOG "Operation aborted by user!!!"
      End If
    End If
    RS.Close
    DB.Close
  Else
    WRITE_DIALOG "Operation not allowed!!!"
  End If
  '
Exit Sub

errOEM1_SALVARE_ARCHIVIO:
  WRITE_DIALOG "Sub OEM1_SALVARE_ARCHIVIO: Error -> " & Err
  Resume Next
  
End Sub

Sub OEM1_SALVARE_OFFLINE(Optional Msg As Boolean = True)
'
Dim IndiceTASTO As Integer
Dim Nomegruppo  As String
Dim Gruppo      As String
Dim NomeSPF     As String
Dim NomeDIR     As String
Dim DB          As Database
Dim RS          As Recordset
Dim TB          As Recordset
Dim DATI_SPF    As String
Dim DATI_DDE    As String
Dim DATI_TXT    As String
Dim rsp         As Integer
Dim ret         As Integer
Dim riga        As String
Dim Atmp        As String * 255
Dim stmp        As String
Dim iGRUPPO     As Integer
'
On Error GoTo errOEM1_SALVARE_OFFLINE
  '
  IndiceTASTO = val(GetInfo("OEM1", "IndiceTASTO", Path_LAVORAZIONE_INI))
  Gruppo = GetInfo("OEM1", "GRUPPO(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
  Gruppo = UCase$(Trim$(Gruppo))
  Nomegruppo = Trim$(UCase$(OEM1.FraPARAMETRI.Caption))
  If (Len(Nomegruppo) > 0) And (Len(Gruppo) > 0) Then
    '
    NomeDIR = GetInfo("OEM1", "NomeDIR(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
    NomeDIR = UCase$(Trim$(NomeDIR))
    If (InStr(NomeDIR, "*") > 0) Then NomeDIR = Replace(NomeDIR, "*", Format$(INDICE_LAVORAZIONE_SINGOLA, "0"))
    '
    NomeSPF = GetInfo("Configurazione", "NomeFileTemporaneoSPF", PathFILEINI)
    NomeSPF = g_chOemPATH & "\" & NomeSPF
    '
    Set DB = OpenDatabase(PathDB_ADDIN)
    '
    Open NomeSPF & ".DDE" For Output As #1
    Open NomeSPF & ".TXT" For Output As #2
    '
    If (Len(NomeDIR)) > 0 Then
      Open NomeSPF & ".SPF" For Output As #3
      Print #3, ";PROGRAMMA : " & Gruppo
      Print #3, ";VERSIONE  : " & Date & " " & Time
    End If
    '
    Set TB = DB.OpenRecordset(Gruppo, dbOpenTable)
    TB.MoveFirst
    Do Until TB.EOF
      If TB.Fields("ABILITATO").Value = "Y" Then
        iGRUPPO = TB.Fields("GRUPPO").Value
        If (iGRUPPO <> 1) And (iGRUPPO <> 2) And (iGRUPPO <> 3) Then iGRUPPO = 1
        Select Case iGRUPPO
          Case 1
            j = 1
              GoSub SCRITTURA
              Print #2, TB.Fields("ACTUAL_" & Format$(j, "0")).Value & ";0;";
          Case 2
            j = 2
              GoSub SCRITTURA
              Print #2, "0;" & TB.Fields("ACTUAL_" & Format$(j, "0")).Value & ";";
          Case 3
            For j = 1 To 2
              GoSub SCRITTURA
              Print #2, TB.Fields("ACTUAL_" & Format$(j, "0")).Value & ";";
            Next j
        End Select
      End If
      TB.MoveNext
    Loop
    TB.Close
    '
    If (Len(NomeDIR)) > 0 Then
      Print #3, "M17"
      Close #3
    End If
    '
    Close #2
    Close #1
    '
    If (Dir$(NomeSPF & ".SPF") <> "") Then
      Open NomeSPF & ".SPF" For Input As #1
      DATI_SPF = Input$(LOF(1), 1)
      Close #1
    Else
      DATI_SPF = "NO_SPF"
    End If
    If (Dir$(NomeSPF & ".DDE") <> "") Then
      Open NomeSPF & ".DDE" For Input As #1
      DATI_DDE = Input$(LOF(1), 1)
      Close #1
      DATI_DDE = Trim$(DATI_DDE)
      If (Len(DATI_DDE) <= 0) Then DATI_DDE = "NO_DDE"
    Else
      DATI_DDE = "NO_DDE"
    End If
    If (Dir$(NomeSPF & ".TXT") <> "") Then
      Open NomeSPF & ".TXT" For Input As #1
      DATI_TXT = Input$(LOF(1), 1)
      Close #1
      DATI_TXT = Trim$(DATI_TXT)
      If (Len(DATI_TXT) <= 0) Then DATI_TXT = "NO_TXT"
    Else
      DATI_TXT = "NO_TXT"
    End If
    '
    Set RS = DB.OpenRecordset(Gruppo & "_ARCHIVIO", dbOpenDynaset)
    RS.MoveFirst
    Call RS.FindFirst("NOME_PEZZO='" & UCase(Trim(Nomegruppo)) & "'")
    If RS.NoMatch Then
      RS.AddNew
      RS.Fields("NOME_PEZZO").Value = Nomegruppo
      RS.Fields("DATA_ARCHIVIAZIONE").Value = Now
      RS.Fields("DATI_SPF").Value = DATI_SPF
      RS.Fields("DATI_DDE").Value = DATI_DDE
      RS.Fields("DATI_TXT").Value = DATI_TXT
      RS.Fields("MEMO").Value = Trim$(OEM1.txtMEMO.Text)
      RS.Update
      RS.Close
      WRITE_DIALOG Nomegruppo & ": SAVE OK!!!"
      If (OEM1.fraARCHIVIO.Visible = True) Then
        Set RS = DB.OpenRecordset(Gruppo & "_ARCHIVIO", dbOpenDynaset)
        OEM1.lstARCHIVIO.Clear
        Do Until RS.EOF
        If IsNull(RS.Fields("NOME_PEZZO").Value) Then
          RS.MoveNext
        Else
          Call OEM1.lstARCHIVIO.AddItem(" " & RS.Fields("NOME_PEZZO").Value)
          RS.MoveNext
        End If
        Loop
        RS.Close
      End If
    Else
      If Msg Then
      ret = LoadString(g_hLanguageLibHandle, 1002, Atmp, 255)
      If (ret > 0) Then riga = Left$(Atmp, ret)
      stmp = "" & Chr(13)
      stmp = stmp & " " & Nomegruppo
      stmp = stmp & " " & Now
      riga = riga & stmp & Chr(13)
      riga = riga & " --> " & Chr(13) & " " & Nomegruppo & " " & RS.Fields("DATA_ARCHIVIAZIONE").Value & " ? "
      StopRegieEvents
      rsp = MsgBox(riga, 256 + 4 + 32, "WARNING")
      ResumeRegieEvents
      If (rsp = 6) Then
        RS.Edit
        RS.Fields("NOME_PEZZO").Value = Nomegruppo
        RS.Fields("DATA_ARCHIVIAZIONE").Value = Now
        RS.Fields("DATI_SPF").Value = DATI_SPF
        RS.Fields("DATI_DDE").Value = DATI_DDE
        RS.Fields("DATI_TXT").Value = DATI_TXT
        RS.Fields("MEMO").Value = Trim$(OEM1.txtMEMO.Text)
        RS.Update
        WRITE_DIALOG Nomegruppo & ": SAVE OK!!!"
      Else
        WRITE_DIALOG "Operation aborted by user!!!"
      End If
      Else
        RS.Edit
        RS.Fields("NOME_PEZZO").Value = Nomegruppo
        RS.Fields("DATA_ARCHIVIAZIONE").Value = Now
        RS.Fields("DATI_SPF").Value = DATI_SPF
        RS.Fields("DATI_DDE").Value = DATI_DDE
        RS.Fields("DATI_TXT").Value = DATI_TXT
        RS.Fields("MEMO").Value = Trim$(OEM1.txtMEMO.Text)
        RS.Update
        WRITE_DIALOG Nomegruppo & ": SAVE OK!!!"
      End If
      RS.Close
    End If
    DB.Close
  Else
    WRITE_DIALOG "Operation not allowed!!!"
  End If
  '
Exit Sub

errOEM1_SALVARE_OFFLINE:
  WRITE_DIALOG "Sub OEM1_SALVARE_OFFLINE: Error -> " & Err
  Resume Next
  
SCRITTURA:
  If IsNull(TB.Fields("VARIABILE_" & Format$(j, "0")).Value) Then
    stmp = ""
  Else
    stmp = Trim$(TB.Fields("VARIABILE_" & Format$(j, "0")).Value)
  End If
  If (Len(NomeDIR) > 0) And (Len(stmp) > 0) Then
    Print #3, stmp & "=" & TB.Fields("ACTUAL_" & Format$(j, "0")).Value
  Else
    stmp = TB.Fields("ITEMDDE_" & Format$(j, "0")).Value
    Print #1, stmp & "=" & TB.Fields("ACTUAL_" & Format$(j, "0")).Value & ";";
  End If
  Return

End Sub

Sub OEM1_STAMPA_ETICHETTE(riga As String, i2 As Integer, _
                          MLeft As Single, TopLista As Single, _
                          Wnome1 As Single, Wvalore1 As Single, _
                          Wnome2 As Single, Wvalore2 As Single, _
                          ByRef USCITA As Object)

Dim i         As Integer
Dim j         As Integer
Dim stmp      As String
Dim X1        As Single
Dim Y1        As Single
Dim iRG       As Single
Dim iCl       As Single

On Error Resume Next
  '
  If (Len(riga) <= 0) Then Exit Sub
  '
  iRG = USCITA.TextHeight("A")
  iCl = USCITA.TextWidth("A")
  '
  USCITA.FontBold = True
  'SCRIVO NELLE ETICHETTE
  stmp = riga
  For j = 0 To 1
    i = InStr(stmp, ";")
    If i > 0 Then
      'Posiziono il cursore sulla stampante
      X1 = MLeft + iCl + j * Wnome1
      Y1 = TopLista + 0 * iRG
      USCITA.CurrentX = X1
      USCITA.CurrentY = Y1
      USCITA.Print Left$(stmp, i - 1)
      stmp = Right$(stmp, Len(stmp) - i)
    End If
  Next j
  For j = 0 To 1
    i = InStr(stmp, ";")
    If i > 0 Then
      'Posiziono il cursore sulla stampante
      X1 = MLeft + iCl + (Wnome1 + Wvalore1) + j * Wnome1
      Y1 = TopLista + 0 * iRG
      USCITA.CurrentX = X1
      USCITA.CurrentY = Y1
      USCITA.Print Left$(stmp, i - 1)
      stmp = Right$(stmp, Len(stmp) - i)
    End If
  Next j
  For j = 0 To 1
    i = InStr(stmp, ";")
    If i > 0 Then
      'Posiziono il cursore sulla stampante
      X1 = MLeft + iCl + (Wnome1 + Wvalore1) + j * Wnome2
      Y1 = TopLista + 0 * iRG + (i2 + 1) * iRG
      USCITA.CurrentX = X1
      USCITA.CurrentY = Y1
      USCITA.Print Left$(stmp, i - 1)
      stmp = Right$(stmp, Len(stmp) - i)
    End If
  Next j
  If Len(stmp) > 0 Then
    'Posiziono il cursore sulla stampante
    X1 = MLeft + iCl + (Wnome1 + Wvalore1) + Wnome2 + Wvalore2
    Y1 = TopLista + 0 * iRG + (i2 + 1) * iRG
    USCITA.CurrentX = X1
    USCITA.CurrentY = Y1
    USCITA.Print stmp
  End If
  USCITA.FontBold = False

End Sub

Sub OEM1_STAMPARE_ARCHIVIO()
'
Dim IndiceTASTO As Integer
Dim Gruppo      As String
Dim stmp        As String
'
Dim tstr() As String
Dim i      As Integer
Dim rsp    As Integer
Dim TESTO  As String
Dim DB     As Database
Dim TB     As Recordset
Dim MTop   As Single
Dim MBot   As Single
Dim DATA   As String
'
On Error Resume Next
  '
  If (OEM1.PicERRORE.Visible = True) Then OEM1.PicERRORE.Visible = False
  If (OEM1.PicELABORA_HD.Visible = True) Then
    OEM1.lblINP_HD(0).Visible = False
    OEM1.txtINP_HD(0).Visible = False
    OEM1.PicELABORA_HD.Visible = False
  End If
  '
  IndiceTASTO = val(GetInfo("OEM1", "IndiceTASTO", Path_LAVORAZIONE_INI))
  Gruppo = GetInfo("OEM1", "GRUPPO(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
  Gruppo = UCase$(Trim$(Gruppo))
  If (Len(Gruppo) > 0) Then
    If (OEM1.PicGRUPPO_HD.Visible = False) Then
      'VISUALIZZAZIONE DEI PARAMETRI
      OEM1.lstSTORICO.Clear
      OEM1.chkSTORICO(0).Visible = False
      OEM1.chkSTORICO(1).Visible = False
      OEM1.chkSTORICO(2).Visible = False
      OEM1.lstSTORICO.Visible = False
      OEM1.PicGRUPPO_HD.Cls
      OEM1.PicGRUPPO_HD.Visible = True
      Call OEM1_STAMPA_GRUPPO_ARCHIVIO(Gruppo, OEM1.PicGRUPPO_HD)
    Else
      rsp = MsgBox(OEM1.txtNomePezzo.Text & " --> " & Printer.DeviceName & " ? ", 4 + 32, "WARNING")
      If (rsp = 6) Then
        'STAMPA DEI PARAMETRI VISUALIZZATI
        Set DB = OpenDatabase(PathDB_ADDIN, True, False)
        Set TB = DB.OpenRecordset(Gruppo & "_ARCHIVIO", dbOpenTable)
        TB.Index = "NOME_PEZZO"
        TB.Seek "=", OEM1.txtNomePezzo.Text
        If Not TB.NoMatch Then
          If IsNull(TB.Fields("MEMO").Value) Then
            TESTO = ""
          Else
            TESTO = TB.Fields("MEMO").Value
          End If
          DATA = TB.Fields("DATA_ARCHIVIAZIONE").Value
        Else
          TESTO = ""
          DATA = ""
          DATA = DATA & " " & Format$(Now, "dd/mm/yyyy")
          DATA = DATA & " " & Format$(Now, "hh:mm:ss")
        End If
        TB.Close
        DB.Close
        Call OEM1_ELABORA_FraARCHIVIO("Y", stmp)
        If (stmp = "Y") Then MTop = 10 'SPOSTAMENTO IN BASSO ALLA PAGINA A4
        'SE HO MODIFICATO IL MEMO USO LA DATA CORRENTE
        If (TESTO <> OEM1.txtMEMO.Text) Then
          TESTO = OEM1.txtMEMO.Text
          DATA = ""
          DATA = DATA & " " & Format$(Now, "dd/mm/yyyy")
          DATA = DATA & " " & Format$(Now, "hh:mm:ss")
        End If
        Call OEM1_STAMPA_ACTUAL(OEM1.txtNomePezzo.Text, Gruppo, Printer, MTop, MBot, "OFF-LINE", DATA)
        If (Len(TESTO) > 0) Then
          TESTO = vbCrLf & "MEMO " & vbCrLf & vbCrLf & TESTO
          tstr = Split(TESTO, vbCrLf)
          Printer.CurrentY = MBot
          For i = 0 To UBound(tstr)
            Printer.CurrentX = 1
            Printer.Print tstr(i)
          Next i
        End If
        Printer.EndDoc
      Else
        OEM1.PicGRUPPO_HD.Visible = False
        OEM1.txtNomePezzo.Enabled = True
        OEM1.txtMEMO.Enabled = True
        OEM1.btnMEMO.Enabled = True
        If (OEM1.btnSTORICO.Visible = True) Then OEM1.btnSTORICO.Enabled = True
      End If
    End If
  Else
    OEM1.PicGRUPPO_HD.Visible = False
    OEM1.txtNomePezzo.Enabled = True
    OEM1.txtMEMO.Enabled = True
    OEM1.btnMEMO.Enabled = True
    If (OEM1.btnSTORICO.Visible = True) Then OEM1.btnSTORICO.Enabled = True
  End If
  '
End Sub

Sub OEM1_STAMPA_ACTUAL(ByVal NomePezzo As String, ByVal Gruppo As String, ByRef USCITA As Object, ByRef MTop As Single, ByRef BottomLista As Single, ByVal TITOLO As String, ByVal DATA As String)
'
Dim TIPO_LAVORAZIONE As String
'
Dim DB As Database
Dim TB As Recordset
Dim RS As Recordset
'
Dim i      As Integer
Dim j      As Integer
Dim k1     As Integer
Dim k2     As Integer
Dim stmp   As String
Dim TITOLI As String
Dim ret    As Integer
Dim Atmp   As String * 255
'
Dim MLeft As Single
Dim TopLista  As Single
Dim X1    As Single
Dim Y1    As Single
Dim X2    As Single
Dim Y2    As Single
Dim iRG   As Single
Dim iCl   As Single
'
Dim TextFont As String
Dim TextSize As Single
'
Dim Wnome1   As Single
Dim Wnome2   As Single
Dim Wvalore1 As Single
Dim Wvalore2 As Single
'
Dim i1 As Integer
Dim i2 As Integer
Dim i3 As Integer
'
Dim Fattore     As Single
Dim WLine       As Integer
Dim Larghezza   As Single
Dim xLeft       As Single
Dim MBottom     As Single
Dim IndiceTASTO As Integer
Dim iGRUPPO     As Integer
Dim NomeSK      As String
'
On Error GoTo errOEM1_STAMPA_ACTUAL
  '
  MLeft = val(GetInfo("StampaParametri", "MLeft", PathFILEINI))
  If (MTop = 0) Then MTop = val(GetInfo("StampaParametri", "MTop", PathFILEINI))
  Fattore = val(GetInfo("StampaParametri", "fattore", PathFILEINI))
  Wnome1 = val(GetInfo("StampaParametri", "Wnome1", PathFILEINI))
  Wnome2 = val(GetInfo("StampaParametri", "Wnome2", PathFILEINI))
  Wvalore1 = val(GetInfo("StampaParametri", "Wvalore1", PathFILEINI))
  Wvalore2 = val(GetInfo("StampaParametri", "Wvalore2", PathFILEINI))
      If (LINGUA = "CH") Then
    TextFont = "MS Song"
  ElseIf (LINGUA = "RU") Then
    TextFont = "Arial Cyr"
  Else
    TextFont = "Arial"
  End If
  TextSize = val(GetInfo("StampaParametri", "TextSize", PathFILEINI))
  Larghezza = Abs(val(GetInfo("StampaParametri", "Larghezza", PathFILEINI)))
  WLine = Abs(val(GetInfo("StampaParametri", "Spessore", PathFILEINI)))
  '
  USCITA.ScaleMode = 7
  '
  X1 = MLeft
  Y1 = MTop
  USCITA.PaintPicture OEM1.Image3.Picture, X1, Y1, 2, 1
  '
  For i = 1 To 2
    USCITA.FontName = OEM1.FontName
    USCITA.FontSize = TextSize
    USCITA.FontBold = False
  Next i
  '
  iRG = USCITA.TextHeight("A")
  iCl = USCITA.TextWidth("A")
  '
  IndiceTASTO = val(GetInfo("OEM1", "IndiceTASTO", Path_LAVORAZIONE_INI))
  '
  NomeSK = GetInfo("OEM1", "NomeSK(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
  NomeSK = UCase$(Trim$(NomeSK))
  If (IsNumeric(NomeSK)) Then
    ret = val(NomeSK)
    ret = LoadString(g_hLanguageLibHandle, ret, Atmp, 255)
    If (ret > 0) Then NomeSK = Left$(Atmp, ret)
  End If
  'CONTROLLO SE INDIRIZZO UNA IMMAGINE \\
  ret = InStr(NomeSK, "\")
  If (ret = 1) Then
    NomeSK = Mid$(NomeSK, ret)
    OEM1.Image2.Picture = LoadPicture(g_chOemPATH & "\Gapp4\images" & NomeSK)
    USCITA.PaintPicture OEM1.Image2.Picture, Larghezza - 2, Y1 + 1, 2, 1
  Else
    If (Len(NomeSK) <= 0) Then NomeSK = "SK" & Format$(IndiceTASTO, "0")
    OEM1.Image2.Picture = LoadPicture("")
    USCITA.CurrentX = Larghezza - USCITA.TextWidth(NomeSK)
    USCITA.CurrentY = Y1 + 1
    USCITA.Print NomeSK
  End If
  '
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  stmp = RICERCA_NOME_TIPO(TIPO_LAVORAZIONE)
  USCITA.CurrentX = X1 + iCl + 2
  USCITA.CurrentY = Y1 + 0
  USCITA.Print stmp
  '
  stmp = TITOLO
  USCITA.CurrentX = Larghezza - USCITA.TextWidth(stmp)
  USCITA.CurrentY = Y1 + 0
  USCITA.Print stmp
  '
  USCITA.CurrentX = X1 + iCl + 2
  USCITA.CurrentY = Y1 + 1
  USCITA.Print DATA
  '
  Y1 = USCITA.CurrentY + 2 * iRG
  '
  USCITA.CurrentX = X1
  USCITA.CurrentY = Y1 - iRG
  USCITA.Print NomePezzo
  '
  USCITA.DrawWidth = WLine
  USCITA.Line (MLeft, Y1)-(Larghezza, Y1)
  USCITA.DrawWidth = 1
  '
  MTop = USCITA.CurrentY + 3 * iRG
  TopLista = MTop
  '
  MBottom = 3 * iRG
  BottomLista = 0
  '
  Set DB = OpenDatabase(PathDB_ADDIN)
  '
  Set RS = DB.OpenRecordset("MESSAGGI", dbOpenDynaset)
  RS.FindFirst "TABELLA = '" & Gruppo & "'"
  If Not RS.NoMatch Then
    TITOLI = RS.Fields("NOME_" & LINGUA).Value
  Else
    TITOLI = ""
  End If
  RS.Close
  '
  Set TB = DB.OpenRecordset(Gruppo)
  i1 = 1: i2 = 1: i3 = 1
  k1 = 1: k2 = 1
  Do Until TB.EOF
    If (TB.Fields("ABILITATO").Value = "Y") Then
      iGRUPPO = TB.Fields("GRUPPO").Value
      If (iGRUPPO <> 1) And (iGRUPPO <> 2) And (iGRUPPO <> 3) Then iGRUPPO = 1
      Select Case iGRUPPO
        Case 1
          i = 1
          j = 1
          xLeft = MLeft + iCl
          Y1 = TopLista + i1 * iRG
          Y2 = Y1 + Fattore * iRG
          'Posiziono il cursore sulla stampante
          X1 = xLeft
          USCITA.CurrentX = X1
          USCITA.CurrentY = Y1
          USCITA.Print TB.Fields("NOME_" & LINGUA).Value
          'Traccio la linea per evidenziare il valore
          X1 = xLeft + USCITA.TextWidth(TB.Fields("NOME_" & LINGUA).Value)
          X2 = xLeft + Wnome1
          USCITA.Line (X1, Y2)-(X2, Y2)
          'Posiziono il cursore sulla stampante
          X1 = xLeft + Wnome1
          USCITA.CurrentX = X1
          USCITA.CurrentY = Y1
          'RICAVO IL VALORE DEL PARAMETRO
          stmp = TB.Fields("ACTUAL_" & Format(j, "0")).Value
          USCITA.Print stmp
          If Y1 > BottomLista Then BottomLista = Y1
          'incremento il contatore di riga
          i1 = i1 + 1
        Case 2
          i = 2
          j = 2
          xLeft = MLeft + iCl + (Wnome1 + Wvalore1)
          Y1 = TopLista + i2 * iRG
          Y2 = Y1 + Fattore * iRG
          'Posiziono il cursore sulla stampante
          X1 = xLeft
          USCITA.CurrentX = X1
          USCITA.CurrentY = Y1
          USCITA.Print TB.Fields("NOME_" & LINGUA).Value
          'Traccio la linea per evidenziare il valore
          X1 = xLeft + USCITA.TextWidth(TB.Fields("NOME_" & LINGUA).Value)
          X2 = xLeft + Wnome1
          USCITA.Line (X1, Y2)-(X2, Y2)
          'Posiziono il cursore sulla stampante
          X1 = MLeft + iCl + (Wnome1 + Wvalore1) + Wnome1
          USCITA.CurrentX = X1
          USCITA.CurrentY = Y1
          'RICAVO IL VALORE DEL PARAMETRO
          stmp = TB.Fields("ACTUAL_" & Format(j, "0")).Value
          USCITA.Print stmp
          If Y1 > BottomLista Then BottomLista = Y1
          'incremento il contatore di riga
          i2 = i2 + 1
        Case 3
          i = 3
          Y1 = i3 * iRG + i2 * iRG
          Y1 = Y1 + iRG
          Y1 = TopLista + Y1
          Y2 = Y1 + Fattore * iRG
          'Posiziono il cursore sulla stampante
          xLeft = MLeft + iCl + (Wnome1 + Wvalore1)
          X1 = xLeft
          USCITA.CurrentX = X1
          USCITA.CurrentY = Y1
          'LEGGO IL CAMPO RELATIVO AL NOME DEL PARAMETRO
          stmp = TB.Fields("NOME_" & LINGUA).Value
          USCITA.Print stmp
          'Traccio la linea per evidenziare il valore
          X1 = xLeft + USCITA.TextWidth(TB.Fields("NOME_" & LINGUA).Value)
          X2 = xLeft + Wnome2
          USCITA.Line (X1, Y2)-(X2, Y2)
          'Posiziono il cursore sulla stampante
          j = 1
          X1 = xLeft + Wnome2
          USCITA.CurrentX = X1
          USCITA.CurrentY = Y1
          'RICAVO IL VALORE DEL PARAMETRO
          stmp = TB.Fields("ACTUAL_" & Format(j, "0")).Value
          USCITA.Print stmp
          'Traccio la linea per evidenziare il valore
          X1 = xLeft + Wnome2 + USCITA.TextWidth(stmp)
          X2 = xLeft + Wnome2 + Wvalore2
          USCITA.Line (X1, Y2)-(X2, Y2)
          'Posiziono il cursore sulla stampante
          j = 2
          X1 = xLeft + Wnome2 + Wvalore2
          USCITA.CurrentX = X1
          USCITA.CurrentY = Y1
          'RICAVO IL VALORE DEL PARAMETRO
          stmp = TB.Fields("ACTUAL_" & Format(j, "0")).Value
          USCITA.Print stmp
          If Y1 > BottomLista Then BottomLista = Y1
          'incremento il contatore di riga
          i3 = i3 + 1
      End Select
    End If
    TB.MoveNext
  Loop
  TB.Close
  DB.Close
  '
  BottomLista = BottomLista + 3 * iRG
  USCITA.DrawWidth = WLine
  USCITA.Line (MLeft, BottomLista)-(Larghezza, BottomLista)
  USCITA.DrawWidth = 1
  '
  Call OEM1_STAMPA_ETICHETTE(TITOLI, i2, MLeft, TopLista, Wnome1, Wvalore1, Wnome2, Wvalore2, USCITA)
  '
  WRITE_DIALOG Gruppo & " --> " & USCITA.DeviceName
  '
Exit Sub

errOEM1_STAMPA_ACTUAL:
  WRITE_DIALOG "Sub OEM1_STAMPA_ACTUAL: Error -> " & Err
  Resume Next

End Sub

Sub OEM1_STAMPA_GRUPPO_ARCHIVIO(ByVal Gruppo As String, ByRef USCITA As Object)
'
Dim TopLista    As Single
Dim BottomLista As Single
'
Dim DB As Database
Dim TB As Recordset
Dim RS As Recordset
'
Dim i      As Integer
Dim j      As Integer
Dim k1     As Integer
Dim k2     As Integer
Dim stmp   As String
Dim TITOLI As String
Dim ret    As Integer
Dim Atmp   As String * 255
'
Dim MLeft As Single
Dim MTop  As Single
Dim X1    As Single
Dim Y1    As Single
Dim X2    As Single
Dim Y2    As Single
Dim iRG   As Single
Dim iCl   As Single
'
Dim TextFont As String
Dim TextSize As Single
'
Dim Wnome1   As Single
Dim Wnome2   As Single
Dim Wvalore1 As Single
Dim Wvalore2 As Single
'
Dim i1 As Integer
Dim i2 As Integer
Dim i3 As Integer
'
Dim Fattore     As Single
Dim WLine       As Integer
Dim Larghezza   As Single
Dim xLeft       As Single
Dim MBottom     As Single
Dim IndiceTASTO As Integer
Dim iGRUPPO     As Integer
Dim Nomegruppo  As String
Dim NomeSK      As String
Dim DATI_TXT    As String
Dim riga        As String
'
Dim L1  As Integer
Dim L2  As Integer
'
On Error GoTo errOEM1_STAMPA_GRUPPO_ARCHIVIO
  '
      If (LINGUA = "CH") Then
    TextFont = "MS Song"
  ElseIf (LINGUA = "RU") Then
    TextFont = "Arial Cyr"
  Else
    TextFont = "Arial"
  End If
  '
  MLeft = 0
  MTop = 0
  Fattore = 0.9
  Wnome1 = 5.3
  Wnome2 = 4.3
  Wvalore1 = 2.2
  Wvalore2 = 2.2
  TextSize = 10
  Larghezza = 19
  WLine = 1
  '
  X1 = MLeft
  Y1 = MTop
  '
  Dim Scala As Single
  Scala = Int((USCITA.Width / 10000) * 1000) / 1000
  If (Scala <= 0) Then Scala = 1
  '
  Wnome1 = Wnome1 * Scala
  Wnome2 = Wnome2 * Scala
  Wvalore1 = Wvalore1 * Scala
  Wvalore2 = Wvalore2 * Scala
  TextSize = TextSize * Scala
  Larghezza = Larghezza * Scala
  WLine = WLine * Scala
  '
  USCITA.ScaleMode = 7
  For i = 1 To 2
    USCITA.FontName = OEM1.FontName
    USCITA.FontSize = TextSize
    USCITA.FontBold = False
  Next i
  '
  iRG = USCITA.TextHeight("A")
  iCl = USCITA.TextWidth("A")
  '
  Nomegruppo = OEM1.txtNomePezzo.Text
  Nomegruppo = UCase$(Trim$(Nomegruppo))
  '
  IndiceTASTO = val(GetInfo("OEM1", "IndiceTASTO", Path_LAVORAZIONE_INI))
  NomeSK = GetInfo("OEM1", "NomeSK(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
  NomeSK = UCase$(Trim$(NomeSK))
  If (IsNumeric(NomeSK)) Then
    ret = val(NomeSK)
    ret = LoadString(g_hLanguageLibHandle, ret, Atmp, 255)
    If (ret > 0) Then NomeSK = Left$(Atmp, ret)
  End If
  'CONTROLLO SE INDIRIZZO UNA IMMAGINE \\
  ret = InStr(NomeSK, "\")
  If (ret = 1) Then
    NomeSK = Mid$(NomeSK, ret)
    OEM1.Image2.Picture = LoadPicture(g_chOemPATH & "\Gapp4\images" & NomeSK)
  Else
    If (Len(NomeSK) <= 0) Then NomeSK = "SK" & Format$(IndiceTASTO, "0")
    OEM1.Image2.Picture = LoadPicture("")
    USCITA.CurrentX = Larghezza - USCITA.TextWidth(NomeSK)
    USCITA.CurrentY = Y1 + 1
  End If
  '
  MBottom = 3 * iRG
  BottomLista = 0
  '
  Set DB = OpenDatabase(PathDB_ADDIN)
  '
  Set RS = DB.OpenRecordset(Gruppo & "_ARCHIVIO", dbOpenTable)
  RS.Index = "NOME_PEZZO"
  RS.Seek "=", Nomegruppo
  If RS.NoMatch Then
    USCITA.Visible = False
    WRITE_DIALOG Nomegruppo & " not found."
    RS.Close
    Exit Sub
  Else
    DATI_TXT = RS.Fields("DATI_TXT").Value
  End If
  RS.Close
  '
  Set RS = DB.OpenRecordset("MESSAGGI", dbOpenDynaset)
  RS.FindFirst "TABELLA = '" & Gruppo & "'"
  If Not RS.NoMatch Then
    TITOLI = RS.Fields("NOME_" & LINGUA).Value
  Else
    TITOLI = ""
  End If
  RS.Close
  '
  Set TB = DB.OpenRecordset(Gruppo)
  i1 = 1: i2 = 1: i3 = 1
  k1 = 1: k2 = 1
  L1 = 1: L2 = 1
  Do Until TB.EOF
    If (TB.Fields("ABILITATO").Value = "Y") Then
      iGRUPPO = TB.Fields("GRUPPO").Value
      If (iGRUPPO <> 1) And (iGRUPPO <> 2) And (iGRUPPO <> 3) Then iGRUPPO = 1
      Select Case iGRUPPO
        Case 1
          i = 1
          j = 1
          xLeft = MLeft + iCl
          Y1 = TopLista + i1 * iRG
          Y2 = Y1 + Fattore * iRG
          'Posiziono il cursore sulla stampante
          X1 = xLeft
          USCITA.CurrentX = X1
          USCITA.CurrentY = Y1
          USCITA.Print TB.Fields("NOME_" & LINGUA).Value
          'Traccio la linea per evidenziare il valore
          X1 = xLeft + USCITA.TextWidth(TB.Fields("NOME_" & LINGUA).Value)
          X2 = xLeft + Wnome1
          USCITA.Line (X1, Y2)-(X2, Y2)
          'Posiziono il cursore sulla stampante
          X1 = xLeft + Wnome1
          USCITA.CurrentX = X1
          USCITA.CurrentY = Y1
          'RICAVO IL VALORE DEL PARAMETRO
          L2 = InStr(L1, DATI_TXT, ";")
          stmp = Mid$(DATI_TXT, L1, L2 - L1)
          L1 = L2 + 1
          L2 = InStr(L1, DATI_TXT, ";")
          L1 = L2 + 1
          USCITA.Print stmp
          'AGGIORNO IL DATABASE DEL VALORE ATTUALE
          TB.Edit
          TB.Fields("ACTUAL_" & Format$(j, "0")).Value = stmp
          TB.Update
          If Y1 > BottomLista Then BottomLista = Y1
          'incremento il contatore di riga
          i1 = i1 + 1
        Case 2
          i = 2
          j = 2
          xLeft = MLeft + iCl + (Wnome1 + Wvalore1)
          Y1 = TopLista + i2 * iRG
          Y2 = Y1 + Fattore * iRG
          'Posiziono il cursore sulla stampante
          X1 = xLeft
          USCITA.CurrentX = X1
          USCITA.CurrentY = Y1
          USCITA.Print TB.Fields("NOME_" & LINGUA).Value
          'Traccio la linea per evidenziare il valore
          X1 = xLeft + USCITA.TextWidth(TB.Fields("NOME_" & LINGUA).Value)
          X2 = xLeft + Wnome1
          USCITA.Line (X1, Y2)-(X2, Y2)
          'Posiziono il cursore sulla stampante
          X1 = MLeft + iCl + (Wnome1 + Wvalore1) + Wnome1
          USCITA.CurrentX = X1
          USCITA.CurrentY = Y1
          'RICAVO IL VALORE DEL PARAMETRO
          L2 = InStr(L1, DATI_TXT, ";")
          L1 = L2 + 1
          L2 = InStr(L1, DATI_TXT, ";")
          stmp = Mid$(DATI_TXT, L1, L2 - L1)
          L1 = L2 + 1
          USCITA.Print stmp
          'AGGIORNO IL DATABASE DEL VALORE ATTUALE
          TB.Edit
          TB.Fields("ACTUAL_" & Format$(j, "0")).Value = stmp
          TB.Update
          If Y1 > BottomLista Then BottomLista = Y1
          'incremento il contatore di riga
          i2 = i2 + 1
        Case 3
          i = 3
          Y1 = i3 * iRG + (i2 + 1) * iRG
          Y1 = TopLista + Y1
          Y2 = Y1 + Fattore * iRG
          'Posiziono il cursore sulla stampante
          xLeft = MLeft + iCl + (Wnome1 + Wvalore1)
          X1 = xLeft
          USCITA.CurrentX = X1
          USCITA.CurrentY = Y1
          'LEGGO IL CAMPO RELATIVO AL NOME DEL PARAMETRO
          stmp = TB.Fields("NOME_" & LINGUA).Value
          USCITA.Print stmp
          'Traccio la linea per evidenziare il valore
          X1 = xLeft + USCITA.TextWidth(TB.Fields("NOME_" & LINGUA).Value)
          X2 = xLeft + Wnome2
          USCITA.Line (X1, Y2)-(X2, Y2)
          'Posiziono il cursore sulla stampante
          j = 1
          X1 = xLeft + Wnome2
          USCITA.CurrentX = X1
          USCITA.CurrentY = Y1
          'RICAVO IL VALORE DEL PARAMETRO
          L2 = InStr(L1, DATI_TXT, ";")
          stmp = Mid$(DATI_TXT, L1, L2 - L1)
          L1 = L2 + 1
          USCITA.Print stmp
          'AGGIORNO IL DATABASE DEL VALORE ATTUALE
          TB.Edit
          TB.Fields("ACTUAL_" & Format$(j, "0")).Value = stmp
          TB.Update
          'Traccio la linea per evidenziare il valore
          X1 = xLeft + Wnome2 + USCITA.TextWidth(stmp)
          X2 = xLeft + Wnome2 + Wvalore2
          USCITA.Line (X1, Y2)-(X2, Y2)
          'Posiziono il cursore sulla stampante
          j = 2
          X1 = xLeft + Wnome2 + Wvalore2
          USCITA.CurrentX = X1
          USCITA.CurrentY = Y1
          'RICAVO IL VALORE DEL PARAMETRO
          L2 = InStr(L1, DATI_TXT, ";")
          stmp = Mid$(DATI_TXT, L1, L2 - L1)
          L1 = L2 + 1
          USCITA.Print stmp
          'AGGIORNO IL DATABASE DEL VALORE ATTUALE
          TB.Edit
          TB.Fields("ACTUAL_" & Format$(j, "0")).Value = stmp
          TB.Update
          If Y1 > BottomLista Then BottomLista = Y1
          'incremento il contatore di riga
          i3 = i3 + 1
      End Select
    End If
    TB.MoveNext
  Loop
  TB.Close
  DB.Close
  '
  Call OEM1_STAMPA_ETICHETTE(TITOLI, i2, MLeft, TopLista, Wnome1, Wvalore1, Wnome2, Wvalore2, USCITA)
  '
Exit Sub

errOEM1_STAMPA_GRUPPO_ARCHIVIO:
  WRITE_DIALOG "Sub OEM1_STAMPA_GRUPPO_ARCHIVIO: Error -> " & Err
  Resume Next

End Sub

Sub OEM1_STAMPARE_CNC()
'
Dim IndiceTASTO As Integer
Dim Gruppo      As String
Dim stmp        As String
Dim MTop        As Single
Dim DATA        As String
'
On Error GoTo errOEM1_STAMPARE_CNC
  '
  If (OEM1.PicERRORE.Visible = True) Then OEM1.PicERRORE.Visible = False
  '
  IndiceTASTO = val(GetInfo("OEM1", "IndiceTASTO", Path_LAVORAZIONE_INI))
  Gruppo = GetInfo("OEM1", "GRUPPO(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
  Gruppo = UCase$(Trim$(Gruppo))
  If (Len(Gruppo) > 0) Then
    MTop = 0
    If (OEM1.PicELABORA.Visible = True) Then
      Call OEM1_ELABORA_FraPARAMETRI("Y", stmp)
      If (stmp = "Y") Then MTop = 10 'SPOSTAMENTO IN BASSO ALLA PAGINA A4
    End If
    DATA = ""
    DATA = DATA & " " & Format$(Now, "dd/mm/yyyy")
    DATA = DATA & " " & Format$(Now, "hh:mm:ss")
    Call OEM1_STAMPA_ACTUAL(OEM1.FraPARAMETRI.Caption, Gruppo, Printer, MTop, 0, "CNC " & LETTERA_LAVORAZIONE, DATA)
    Printer.EndDoc
  Else
    WRITE_DIALOG "Operation not allowed!!!"
  End If
  '
Exit Sub

errOEM1_STAMPARE_CNC:
  WRITE_DIALOG "Sub OEM1_STAMPARE_CNC: Error -> " & Err
  Resume Next
      
End Sub

Sub OEM1_STAMPARE_OFFLINE()
'
Dim IndiceTASTO As Integer
Dim Gruppo      As String
'
Dim tstr() As String
Dim i      As Integer
Dim TESTO  As String
Dim DB     As Database
Dim TB     As Recordset
Dim MTop   As Single
Dim MBot   As Single
Dim DATA   As String
Dim stmp   As String
'
On Error GoTo errOEM1_STAMPARE_OFFLINE
  '
  If (OEM1.PicERRORE.Visible = True) Then OEM1.PicERRORE.Visible = False
  '
  IndiceTASTO = val(GetInfo("OEM1", "IndiceTASTO", Path_LAVORAZIONE_INI))
  Gruppo = GetInfo("OEM1", "GRUPPO(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
  Gruppo = UCase$(Trim$(Gruppo))
  If (Len(Gruppo) > 0) Then
    'LETTURA DEL CAMPO MEMO
    Set DB = OpenDatabase(PathDB_ADDIN, True, False)
    Set TB = DB.OpenRecordset(Gruppo & "_ARCHIVIO", dbOpenTable)
    TB.Index = "NOME_PEZZO"
    TB.Seek "=", OEM1.FraPARAMETRI.Caption
    If Not TB.NoMatch Then
      If IsNull(TB.Fields("MEMO").Value) Then
        TESTO = ""
      Else
        TESTO = TB.Fields("MEMO").Value
      End If
      DATA = TB.Fields("DATA_ARCHIVIAZIONE").Value
    Else
      TESTO = ""
      DATA = ""
      DATA = DATA & " " & Format$(Now, "dd/mm/yyyy")
      DATA = DATA & " " & Format$(Now, "hh:mm:ss")
    End If
    TB.Close
    DB.Close
    '
    MTop = 0
    If (OEM1.PicELABORA.Visible = True) Then
      Call OEM1_ELABORA_FraPARAMETRI("Y", stmp)
      If (stmp = "Y") Then MTop = 10 'SPOSTAMENTO IN BASSO ALLA PAGINA A4
    End If
    Call OEM1_STAMPA_ACTUAL(OEM1.FraPARAMETRI.Caption, Gruppo, Printer, MTop, MBot, "OFF-LINE", DATA)
    If (Len(TESTO) > 0) Then
      TESTO = vbCrLf & "MEMO " & vbCrLf & vbCrLf & TESTO
      tstr = Split(TESTO, vbCrLf)
      Printer.CurrentY = MBot
      For i = 0 To UBound(tstr)
        Printer.CurrentX = 1
        Printer.Print tstr(i)
      Next i
    End If
    Printer.EndDoc
  Else
    WRITE_DIALOG "Operation not allowed!!!"
  End If
  '
Exit Sub

errOEM1_STAMPARE_OFFLINE:
  WRITE_DIALOG "Sub OEM1_STAMPARE_OFFLINE: Error -> " & Err
  Resume Next
      
End Sub

Sub INVIO_SOTTOPROGRAMMA_OEM1(ByVal Gruppo As String, ByVal NomeDIR As String)
'
Dim NomeSPF      As String
Dim SORGENTE     As String
Dim DESTINAZIONE As String
'
On Error GoTo errINVIO_SOTTOPROGRAMMA_OEM1
  '
  If (Len(NomeDIR) > 0) Then
    Call CREAZIONE_SOTTOPROGRAMMA_OEM1(Gruppo)
    WRITE_DIALOG "INVIO_SOTTOPROGRAMMA " & Gruppo & ".SPF"
    'TRASMISSIONE DEL SOTTOPROGRAMMA AL CNC
    NomeSPF = GetInfo("Configurazione", "NomeFileTemporaneoSPF", PathFILEINI)
    SORGENTE = g_chOemPATH & "\" & NomeSPF & ".SPF"
    DESTINAZIONE = "/NC/WKS.DIR/" & NomeDIR & ".WPD/" & Gruppo & "." & "SPF"
    Set Session = GetObject("@SinHMIMCDomain.MCDomain")
    Session.CopyNC SORGENTE, DESTINAZIONE, MCDOMAIN_COPY_NC
    Set Session = Nothing
    'CREAZIONE DEL SOTTOPROGRAMMA NEL DIRETTORIO
    If (Dir$(g_chOemPATH & "\" & Gruppo & ".SPF") <> "") Then Kill g_chOemPATH & "\" & Gruppo & ".SPF"
    FileCopy g_chOemPATH & "\" & NomeSPF & ".SPF", g_chOemPATH & "\" & Gruppo & ".SPF"
    WRITE_DIALOG ""
  End If
  '
Exit Sub

errINVIO_SOTTOPROGRAMMA_OEM1:
  WRITE_DIALOG "Sub INVIO_SOTTOPROGRAMMA_OEM1: ERROR -> " & Err
  Resume Next
  
End Sub

Sub CREAZIONE_SOTTOPROGRAMMA_OEM1(ByVal Gruppo As String)
'
Dim NomeSPF As String
Dim stmp    As String
Dim i       As Integer
Dim CHbgn   As String
Dim CHend   As String
'
On Error GoTo errCREAZIONE_SOTTOPROGRAMMA_OEM1
  '
  'SEGNALAZIONE UTENTE
  WRITE_DIALOG "CREAZIONE SOTTOPROGRAMMA " & Gruppo & ".SPF"
  NomeSPF = GetInfo("Configurazione", "NomeFileTemporaneoSPF", PathFILEINI)
  Open PathFileSPF For Input As #1
    Open g_chOemPATH & "\" & NomeSPF & ".SPF" For Output As #2
      'INTESTAZIONE SOTTOPROGRAMMA
      Print #2, ";PROGRAMMA: " & Gruppo
      Print #2, ";VERSIONE : " & Date & " " & Time
      'RICERCA SEZIONE INIZIALE
      Do While Not EOF(1)
        Line Input #1, stmp
        stmp = UCase$(Trim$(stmp))
        If (Len(stmp) > 0) Then
          i = InStr(stmp, "[" & Gruppo & "]")
          If (i > 0) Then
            'SEZIONE INIZIALE TROVATA
            'ESCO DAL CICLO
            Exit Do
          End If
        End If
      Loop
      'SCRITTURA FINO ALLA PROSSIMA SEZIONE O EOF
      Do While Not EOF(1)
        Line Input #1, stmp
        stmp = UCase$(Trim$(stmp))
        If (Len(stmp) > 0) Then
          CHbgn = Left$(stmp, 1)
          CHend = Right$(stmp, 1)
          If (CHbgn <> "[") And (CHend <> "]") Then
            'SCRIVO LA RIGA NON NULLA
            Print #2, stmp
          Else
            'SEZIONE FINALE TROVATA
            'ESCO DAL CICLO
            Exit Do
          End If
        End If
      Loop
      'CHIUSURA SOTTOPROGRAMMA
      Print #2, "M17"
    Close #2
  Close #1
  '
Exit Sub

errCREAZIONE_SOTTOPROGRAMMA_OEM1:
  WRITE_DIALOG "Sub CREAZIONE_SOTTOPROGRAMMA_OEM1: ERROR -> " & Err
  Resume Next
  
End Sub

Sub SALVARE_ARCHIVIO_ADDIN(ByVal TIPO_LAVORAZIONE As String, ByVal NomePezzo As String)
'
Dim GruppoTMP   As String
Dim Gruppo      As String
Dim i           As Integer
Dim j           As Integer
Dim k           As Integer
Dim stmp        As String
Dim DB          As Database
Dim TB          As Recordset
Dim NomeSPF     As String
Dim NomeDIR     As String
Dim NomeSTO     As String
Dim Nomegruppo  As String
Dim iGRUPPO     As Integer
Dim ret         As Integer
Dim DATI_SPF    As String
Dim DATI_DDE    As String
Dim DATI_TXT    As String
Dim STORICO     As String
Dim ELENCO()    As String
Dim SEZIONE     As String
Dim IndiceTASTO As Integer
Dim NomeVar     As String
Dim sIncrPrf    As String
'
On Error GoTo errSALVARE_ARCHIVIO_ADDIN
  '
  If (Len(PathDB_ADDIN) > 0) Then
    NomeSPF = GetInfo("Configurazione", "NomeFileTemporaneoSPF", PathFILEINI)
    NomeSPF = UCase$(Trim$(NomeSPF))
    NomeSPF = g_chOemPATH & "\" & NomeSPF
    For IndiceTASTO = 0 To 7
      If (Dir$(NomeSPF & ".SPF") <> "") Then Call Kill(NomeSPF & ".SPF")
      If (Dir$(NomeSPF & ".DDE") <> "") Then Call Kill(NomeSPF & ".DDE")
      If (Dir$(NomeSPF & ".TXT") <> "") Then Call Kill(NomeSPF & ".TXT")
      Gruppo = GetInfo("OEM1", "GRUPPO(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
      Gruppo = UCase$(Trim$(Gruppo))
      If (Len(Gruppo) > 0) Then
        '***************************
        Select Case TIPO_LAVORAZIONE
        '***************************
        Case "VITI_CONICHE"
        '***************************
          GruppoTMP = GetInfo("OEM1", "TB_CONFRONTA_MOLE", Path_LAVORAZIONE_INI)
          GruppoTMP = UCase$(Trim$(GruppoTMP)) 'NOME DEL CAMPO
          GruppoTMP = GetInfo("OEM1", GruppoTMP, Path_LAVORAZIONE_INI)
          GruppoTMP = UCase$(Trim$(GruppoTMP)) 'NOME DELLA TABELLA
        Case "VITIV_ESTERNE"
        '***************************
          GruppoTMP = GetInfo("OEM1", "TB_CONFRONTA_MOLE", Path_LAVORAZIONE_INI)
          GruppoTMP = UCase$(Trim$(GruppoTMP)) 'NOME DEL CAMPO
          GruppoTMP = GetInfo("OEM1", GruppoTMP, Path_LAVORAZIONE_INI)
          GruppoTMP = UCase$(Trim$(GruppoTMP)) 'NOME DELLA TABELLA
          sIncrPrf = OPC_LEGGI_DATO("/ACC/NCK/UGUD/ORIG[5]")
          Call OPC_SCRIVI_DATO("/ACC/NCK/GUD7/INCRPRF", sIncrPrf)
          
        '***************************
        Case Else
        '***************************
          GruppoTMP = ""
        End Select
        '***************************
        If (Gruppo = GruppoTMP) Then
          'LETTURA VALORI DAL FILE INI TEMPORANEO DELLA LAVORAZIONE
          SEZIONE = "CNC_" & LETTERA_LAVORAZIONE
          Nomegruppo = GetInfo(SEZIONE, "CODICE_MOLA", g_chOemPATH & "\" & TIPO_LAVORAZIONE & ".INI")
          Nomegruppo = UCase(Trim(Nomegruppo))
          If (Nomegruppo <> "NONE") And (Len(Nomegruppo) > 0) Then
            Open NomeSPF & ".DDE" For Output As #1
            Open NomeSPF & ".TXT" For Output As #2
            Open "ADDIN.INI" For Output As #3
            Print #3, "[" & GruppoTMP & "]"
            Print #3, "CODICE_MOLA" & "=" & Nomegruppo
            Set DB = OpenDatabase(PathDB_ADDIN, True, False)
            Set TB = DB.OpenRecordset(Gruppo, dbOpenTable)
            TB.MoveFirst
            Do Until TB.EOF
            If TB.Fields("ABILITATO").Value = "Y" Then
              iGRUPPO = TB.Fields("GRUPPO").Value
              If (iGRUPPO <> 1) And (iGRUPPO <> 2) And (iGRUPPO <> 3) Then iGRUPPO = 1
              Select Case iGRUPPO
                Case 1
                  j = 1
                    GoSub LETTURA1
                    Print #2, TB.Fields("ACTUAL_" & Format$(j, "0")).Value & ";0;";
                Case 2
                  j = 2
                    GoSub LETTURA1
                    Print #2, "0;" & TB.Fields("ACTUAL_" & Format$(j, "0")).Value & ";";
                Case 3
                  For j = 1 To 2
                    GoSub LETTURA1
                    Print #2, TB.Fields("ACTUAL_" & Format$(j, "0")).Value & ";";
                  Next j
              End Select
            End If
            TB.MoveNext
            Loop
            TB.Close
            DB.Close
            Close #3
            Close #2
            Close #1
          Else
            Nomegruppo = ""
          End If
        Else
          NomeSTO = GetInfo("OEM1", "NomeSTO(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
          NomeSTO = UCase$(Trim$(NomeSTO))
          If (Len(NomeSTO) > 0) Then
            Nomegruppo = GetInfo("OEM1", "NomeGRP(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
            Nomegruppo = UCase(Trim(Nomegruppo))
            If (Len(Nomegruppo) > 0) Then
              NomeDIR = GetInfo("OEM1", "NomeDIR(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
              NomeDIR = UCase$(Trim$(NomeDIR))
              If (InStr(NomeDIR, "*") > 0) Then NomeDIR = Replace(NomeDIR, "*", Format$(INDICE_LAVORAZIONE_SINGOLA, "0"))
              If (Len(NomeDIR) > 0) Then
                Set Session = GetObject("@SinHMIMCDomain.MCDomain")
                Session.CopyNC "/NC/WKS.DIR/" & NomeDIR & ".WPD/" & Gruppo & ".SPF", NomeSPF & ".SPF", MCDOMAIN_COPY_NC
                Set Session = Nothing
                Call SCRIVI_SEZIONE_INI(NomeSPF & ".SPF", NomeSPF & ".INI", Gruppo)
              End If
              Open NomeSPF & ".DDE" For Output As #1
              Open NomeSPF & ".TXT" For Output As #2
              Set DB = OpenDatabase(PathDB_ADDIN, True, False)
              Set TB = DB.OpenRecordset(Gruppo, dbOpenTable)
              TB.MoveFirst
              Do Until TB.EOF
              If TB.Fields("ABILITATO").Value = "Y" Then
                iGRUPPO = TB.Fields("GRUPPO").Value
                If (iGRUPPO <> 1) And (iGRUPPO <> 2) And (iGRUPPO <> 3) Then iGRUPPO = 1
                Select Case iGRUPPO
                  Case 1
                    j = 1
                      GoSub lettura2
                      Print #2, TB.Fields("ACTUAL_" & Format$(j, "0")).Value & ";0;";
                  Case 2
                    j = 2
                      GoSub lettura2
                      Print #2, "0;" & TB.Fields("ACTUAL_" & Format$(j, "0")).Value & ";";
                  Case 3
                    For j = 1 To 2
                      GoSub lettura2
                      Print #2, TB.Fields("ACTUAL_" & Format$(j, "0")).Value & ";";
                    Next j
                End Select
              End If
              TB.MoveNext
              Loop
              TB.Close
              DB.Close
              Close #2
              Close #1
            End If
          Else
            Nomegruppo = ""
          End If
        End If
        'SALVATAGGIO NEL DATABASE ADDIN
        If (Len(Nomegruppo) > 0) Then
          If (Dir$(NomeSPF & ".SPF") <> "") Then
            Open NomeSPF & ".SPF" For Input As #1
            DATI_SPF = Input$(LOF(1), 1)
            Close #1
            If (Len(DATI_SPF) <= 0) Then DATI_SPF = "NO_SPF"
          Else
            DATI_SPF = "NO_SPF"
          End If
          If (Dir$(NomeSPF & ".DDE") <> "") Then
            Open NomeSPF & ".DDE" For Input As #1
            DATI_DDE = Input$(LOF(1), 1)
            Close #1
            DATI_DDE = Trim$(DATI_DDE)
            If (Len(DATI_DDE) <= 0) Then DATI_DDE = "NO_DDE"
          Else
            DATI_DDE = "NO_DDE"
          End If
          If (Dir$(NomeSPF & ".TXT") <> "") Then
            Open NomeSPF & ".TXT" For Input As #1
            DATI_TXT = Input$(LOF(1), 1)
            Close #1
            DATI_TXT = Trim$(DATI_TXT)
            If (Len(DATI_TXT) <= 0) Then DATI_TXT = "NO_TXT"
          Else
            DATI_TXT = "NO_TXT"
          End If
          Set DB = OpenDatabase(PathDB_ADDIN, True, False)
          Set TB = DB.OpenRecordset(Gruppo & "_ARCHIVIO", dbOpenDynaset)
          TB.MoveFirst
          Call TB.FindFirst("NOME_PEZZO='" & Nomegruppo & "'")
          If TB.NoMatch Then
            TB.AddNew
          Else
            TB.Edit
          End If
          TB.Fields("NOME_PEZZO").Value = Nomegruppo
          TB.Fields("DATA_ARCHIVIAZIONE").Value = Now
          TB.Fields("DATI_SPF").Value = DATI_SPF
          TB.Fields("DATI_DDE").Value = DATI_DDE
          TB.Fields("DATI_TXT").Value = DATI_TXT
          '*************************************
          STORICO = TB.Fields("STORICO").Value
          STORICO = UCase$(Trim$(STORICO))
          ELENCO = Split(STORICO, ";")
          If (UBound(ELENCO) > 0) Then
            stmp = ""
            For i = 0 To UBound(ELENCO) - 1
            If (InStr(ELENCO(i), NomePezzo) > 0) Then
              k = i
              stmp = "TROVATO"
              Exit For
            End If
            Next i
            If (stmp = "TROVATO") Then
              TB.Fields("STORICO").Value = ""
              For i = 0 To k - 1
                TB.Fields("STORICO").Value = TB.Fields("STORICO").Value & ELENCO(i) & ";"
              Next i
              For i = k + 1 To UBound(ELENCO) - 1
                TB.Fields("STORICO").Value = TB.Fields("STORICO").Value & ELENCO(i) & ";"
              Next i
            End If
            'L'ULTIMO SALVATAGGIO E' ALLA FINE
            TB.Fields("STORICO").Value = TB.Fields("STORICO").Value & TIPO_LAVORAZIONE & "|" & NomePezzo & "|" & Now & ";"
          Else
            'INSERISCO IL PRIMO ELEMENTO
            TB.Fields("STORICO").Value = TIPO_LAVORAZIONE & "|" & NomePezzo & "|" & Now & ";"
          End If
          TB.Fields("MEMO").Value = ""
          TB.Update
          TB.Close
          DB.Close
          ret = WritePrivateProfileString("OEM1", "NomeGRP(" & Format$(IndiceTASTO, "0") & ")", Nomegruppo, Path_LAVORAZIONE_INI)
          WRITE_DIALOG Nomegruppo & ": SAVE OK!!!"
        End If
      End If
    Next IndiceTASTO
  End If
  '
Exit Sub

errSALVARE_ARCHIVIO_ADDIN:
  WRITE_DIALOG "Sub SALVARE_ARCHIVIO_ADDIN: Error -> " & Err
  Resume Next

LETTURA1:
  If IsNull(TB.Fields("VARIABILE_" & Format$(j, "0")).Value) Then
    stmp = ""
  Else
    stmp = Trim$(TB.Fields("VARIABILE_" & Format$(j, "0")).Value)
    NomeVar = stmp
  End If
  If (Len(stmp) > 0) Then
    'LEGGO IL VALORE DEL PARAMETRO DAL FILE INI
    stmp = GetInfo(SEZIONE, TB.Fields("VARIABILE_" & Format$(j, "0")).Value, g_chOemPATH & "\" & TIPO_LAVORAZIONE & ".INI")
    '***********************************************************
    k = InStr(stmp, ";")
    If (k > 0) Then stmp = Left$(stmp, k - 1): stmp = Trim$(stmp)
    k = InStr(stmp, Chr$(34))
    If (k > 0) Then stmp = Mid$(stmp, 2, Len(stmp) - 2): stmp = Trim$(stmp)
    '**********************************************************
    '
    Print #3, NomeVar & "=" & stmp
  Else
    'LEGGO IL VALORE DEL PARAMETRO SUL CNC
    stmp = TB.Fields("ITEMDDE_" & Format$(j, "0")).Value
    Print #1, stmp & "=";
    stmp = OPC_LEGGI_DATO(stmp)
    'TOLGO GLI SPAZI
    stmp = Trim$(stmp)
    If IsNumeric(stmp) Then
      'SE E' NUMERICO FORMATTO IL VALORE
      stmp = frmt(val(stmp), 8)
    End If
    Print #1, stmp & ";";
  End If
  'AGGIORNO IL DATABASE DEL VALORE ATTUALE
  TB.Edit
  TB.Fields("ACTUAL_" & Format$(j, "0")).Value = stmp
  TB.Update
Return

lettura2:
  If IsNull(TB.Fields("VARIABILE_" & Format$(j, "0")).Value) Then
    stmp = ""
  Else
    stmp = Trim$(TB.Fields("VARIABILE_" & Format$(j, "0")).Value)
  End If
  If (Len(stmp) > 0) And (Len(NomeDIR) > 0) Then
    'LEGGO IL VALORE DEL PARAMETRO DAL FILE INI
    stmp = GetInfo(Gruppo, TB.Fields("VARIABILE_" & Format$(j, "0")).Value, NomeSPF & ".INI")
    '***********************************************************
    k = InStr(stmp, ";")
    If (k > 0) Then stmp = Left$(stmp, k - 1): stmp = Trim$(stmp)
    k = InStr(stmp, Chr$(34))
    If (k > 0) Then stmp = Mid$(stmp, 2, Len(stmp) - 2): stmp = Trim$(stmp)
    '**********************************************************
  Else
    'LEGGO IL VALORE DEL PARAMETRO SUL CNC
    stmp = TB.Fields("ITEMDDE_" & Format$(j, "0")).Value
    Print #1, stmp & "=";
    stmp = OPC_LEGGI_DATO(stmp)
    'TOLGO GLI SPAZI
    stmp = Trim$(stmp)
    If IsNumeric(stmp) Then
      'SE E' NUMERICO FORMATTO IL VALORE
      stmp = frmt(val(stmp), 8)
    End If
    Print #1, stmp & ";";
  End If
  'AGGIORNO IL DATABASE DEL VALORE ATTUALE
  TB.Edit
  TB.Fields("ACTUAL_" & Format$(j, "0")).Value = stmp
  TB.Update
Return

End Sub

Sub OEM1_CREAZIONE_ARCHIVIO(ByVal Gruppo As String)
'
Dim i    As Integer
Dim j    As Integer
Dim stmp As String
Dim DB   As Database
Dim TB   As Recordset
'
On Error GoTo errOEM1_CREAZIONE_ARCHIVIO
  '
  Set DB = OpenDatabase(PathDB_ADDIN, True, False)
  'CONTROLLO ESISTENZA TABELLA DI ARCHIVIAZIONE
  stmp = ""
  For j = 0 To DB.TableDefs.count - 1
    If (DB.TableDefs(j).Name = Gruppo & "_ARCHIVIO") Then
      stmp = "TROVATO"
      Exit For
    End If
  Next j
  If (stmp <> "TROVATO") Then
    'CREAZIONE DELLA TABELLA DI ARCHIVIAZIONE
    Dim TBB   As TableDef
    Dim Fd()  As Field
    Dim FdId  As Field
    Dim IdTb  As Index
    Set TBB = DB.CreateTableDef(Gruppo & "_ARCHIVIO")
    j = 1
      ReDim Preserve Fd(j)
      Set Fd(j) = TBB.CreateField("NOME_PEZZO", dbText, 255)
      Call TBB.Fields.Append(Fd(j))
    j = j + 1
      ReDim Preserve Fd(j)
      Set Fd(j) = TBB.CreateField("DATA_ARCHIVIAZIONE", dbText, 50)
      Call TBB.Fields.Append(Fd(j))
    j = j + 1
      ReDim Preserve Fd(j)
      Set Fd(j) = TBB.CreateField("DATI_SPF", dbMemo)
      Call TBB.Fields.Append(Fd(j))
    j = j + 1
      ReDim Preserve Fd(j)
      Set Fd(j) = TBB.CreateField("DATI_DDE", dbMemo)
      Call TBB.Fields.Append(Fd(j))
    j = j + 1
      ReDim Preserve Fd(j)
      Set Fd(j) = TBB.CreateField("DATI_TXT", dbMemo)
      Call TBB.Fields.Append(Fd(j))
    j = j + 1
      ReDim Preserve Fd(j)
      Set Fd(j) = TBB.CreateField("STORICO", dbMemo)
      Call TBB.Fields.Append(Fd(j))
    j = j + 1
      ReDim Preserve Fd(j)
      Set Fd(j) = TBB.CreateField("MEMO", dbMemo)
      Call TBB.Fields.Append(Fd(j))
    'CREAZIONE DELL'INDICE
    'IL NOME DEL CAMPO DEL NOME DEL PEZZO E' LO STESSO DELL'INDICE
    Set IdTb = TBB.CreateIndex("NOME_PEZZO")
    IdTb.Unique = True
    Set FdId = TBB.CreateField("NOME_PEZZO")
    Call IdTb.Fields.Append(FdId)
    Call TBB.Indexes.Append(IdTb)
    Call DB.TableDefs.Append(TBB)
    j = 0
      TBB.Fields(j).AllowZeroLength = False
      TBB.Fields(j).OrdinalPosition = j + 1
    j = 1
      TBB.Fields(j).AllowZeroLength = False
      TBB.Fields(j).OrdinalPosition = j + 1
    For j = 2 To TBB.Fields.count - 1
      TBB.Fields(j).AllowZeroLength = True
      TBB.Fields(j).OrdinalPosition = j + 1
    Next j
  End If
  DB.Close
  '
Exit Sub

errOEM1_CREAZIONE_ARCHIVIO:
  WRITE_DIALOG "Sub OEM1_CREAZIONE_ARCHIVIO: Error -> " & Err
  Resume Next

End Sub

Sub OEM1_COPIARE_ARCHIVIO()
'
Dim DB                As Database
Dim TB1               As Recordset
Dim TB2               As Recordset
Dim Atmp              As String * 255
Dim stmp              As String
Dim stmp1             As String
Dim ret               As Integer
Dim riga              As String
Dim NomePezzoCORRENTE As String
Dim NomePezzoFINALE   As String
Dim rsp               As Integer
Dim IndiceTASTO       As Integer
Dim Gruppo            As String
'
On Error Resume Next
  '
  NomePezzoCORRENTE = Trim$(OEM1.lstARCHIVIO.List(OEM1.lstARCHIVIO.ListIndex))
  NomePezzoCORRENTE = UCase$(Trim$(NomePezzoCORRENTE))
  NomePezzoFINALE = UCase$(Trim$(OEM1.txtNomePezzo.Text))
  If (Len(NomePezzoCORRENTE) <= 0) Then WRITE_DIALOG "SELECT THE PART CODE FROM LIST": Exit Sub
  If (Len(NomePezzoFINALE) <= 0) Then WRITE_DIALOG "INPUT THE PART CODE INTO TEXTBOX": Exit Sub
  '
  ret = LoadString(g_hLanguageLibHandle, 1002, Atmp, 255)
  If (ret > 0) Then riga = Left$(Atmp, ret)
  If (NomePezzoCORRENTE <> NomePezzoFINALE) Then
    'SONO DIVERSI
    riga = riga & Chr(13)
    riga = riga & NomePezzoCORRENTE & " (HD) --> " & NomePezzoFINALE & " (HD) ? "
    StopRegieEvents
    ret = MsgBox(riga, 16 + 4 + 256, "WARNING")
    ResumeRegieEvents
  Else
    WRITE_DIALOG "SAME PART CODE: Operation not possible!!!"
    Exit Sub
  End If
  '
  If (ret = 6) Then
    IndiceTASTO = val(GetInfo("OEM1", "IndiceTASTO", Path_LAVORAZIONE_INI))
    Gruppo = GetInfo("OEM1", "GRUPPO(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
    Gruppo = UCase$(Trim$(Gruppo))
    Set DB = OpenDatabase(PathDB_ADDIN, True, False)
    Set TB1 = DB.OpenRecordset(Gruppo & "_ARCHIVIO")
    TB1.Index = "NOME_PEZZO"
    TB1.Seek "=", NomePezzoCORRENTE
    If (Not TB1.NoMatch) Then
      Set TB2 = DB.OpenRecordset(Gruppo & "_ARCHIVIO")
      TB2.Index = "NOME_PEZZO"
      TB2.Seek "=", NomePezzoFINALE
      If Not TB2.NoMatch Then
        TB2.Edit
      Else
        TB2.AddNew
        Call OEM1.lstARCHIVIO.AddItem(" " & NomePezzoFINALE)
        DoEvents
      End If
      For i = 0 To TB1.Fields.count - 1
        If (TB1.Fields(i).Name = "NOME_PEZZO") Then
          TB2.Fields(i).Value = NomePezzoFINALE
        Else
          TB2.Fields(i).Value = TB1.Fields(i).Value
        End If
      Next i
      TB2.Update
      TB2.Close
    Else
      WRITE_DIALOG NomePezzoCORRENTE & " not found????"
    End If
    TB1.Close
    DB.Close
    WRITE_DIALOG NomePezzoCORRENTE & " --> " & NomePezzoFINALE & ": OK!!!"
  Else
    WRITE_DIALOG "Operation aborted by user!!!"
  End If
  '
End Sub

Sub OEM1_ELABORA_FraARCHIVIO(ByVal SiStampa As String, ByRef STAMPA_ELABORAZIONE_COMPLETATA As String)
'
Dim TipoELABORA As String
Dim IndiceTASTO As Integer
Dim Gruppo      As String
Dim NomePezzo   As String
'MOLA VITI CONICHE
Dim RR37 As Double
Dim RR38 As Double
Dim RR45 As Double
Dim RR55 As Double
Dim RR41 As Double
Dim RR43 As Double
Dim RR44 As Double
Dim RR46 As Double
Dim RR47 As Double
Dim RR48 As Double
Dim RR49 As Double
Dim RR51 As Double
Dim RR53 As Double
Dim RR54 As Double
Dim RR56 As Double
Dim RR57 As Double
Dim RR58 As Double
Dim RR59 As Double
'MOLA RDE
Dim Altezza   As Double
Dim Larghezza As Double
Dim Raggio    As Double
Dim CONICITA  As Double
'*********************
Dim NomeINI As String
'
On Error Resume Next
  '
  STAMPA_ELABORAZIONE_COMPLETATA = "N"
  OEM1.PicGRUPPO_HD.Visible = False
  OEM1.txtNomePezzo.Enabled = True
  OEM1.txtMEMO.Enabled = True
  OEM1.btnMEMO.Enabled = True
  If (OEM1.btnSTORICO.Visible = True) Then OEM1.btnSTORICO.Enabled = True
  '
  If (OEM1.PicELABORA_HD.Visible = False) Or (SiStampa = "Y") Then
    NomePezzo = OEM1.txtNomePezzo.Text
    NomePezzo = UCase$(Trim$(NomePezzo))
    IndiceTASTO = val(GetInfo("OEM1", "IndiceTASTO", Path_LAVORAZIONE_INI))
    Gruppo = GetInfo("OEM1", "GRUPPO(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
    Gruppo = UCase$(Trim$(Gruppo))
    If (Len(NomePezzo) > 0) And (Len(Gruppo) > 0) Then
      TipoELABORA = GetInfo("OEM1", "TipoELABORA(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
      TipoELABORA = UCase$(Trim$(TipoELABORA))
      Select Case TipoELABORA
      '***********************
      Case "MOLA_VITI_CONICHE"
      '***********************
        If (INIZIALIZZA_OFFLINE(Gruppo, NomePezzo) = "TROVATO") Then
          GoSub VISUALIZZA_FraARCHIVIO
          RR37 = val(GetInfo(Gruppo, "R[37]", NomeINI))
          RR38 = val(GetInfo(Gruppo, "R[38]", NomeINI))
          RR45 = val(GetInfo(Gruppo, "R[45]", NomeINI))
          RR55 = val(GetInfo(Gruppo, "R[55]", NomeINI))
          RR41 = val(GetInfo(Gruppo, "R[41]", NomeINI))
          RR43 = val(GetInfo(Gruppo, "R[43]", NomeINI))
          RR44 = val(GetInfo(Gruppo, "R[44]", NomeINI))
          RR46 = val(GetInfo(Gruppo, "R[46]", NomeINI))
          RR47 = val(GetInfo(Gruppo, "R[47]", NomeINI))
          RR48 = val(GetInfo(Gruppo, "R[48]", NomeINI))
          RR49 = val(GetInfo(Gruppo, "R[49]", NomeINI))
          RR51 = val(GetInfo(Gruppo, "R[51]", NomeINI))
          RR53 = val(GetInfo(Gruppo, "R[53]", NomeINI))
          RR54 = val(GetInfo(Gruppo, "R[54]", NomeINI))
          RR56 = val(GetInfo(Gruppo, "R[56]", NomeINI))
          RR57 = val(GetInfo(Gruppo, "R[57]", NomeINI))
          RR58 = val(GetInfo(Gruppo, "R[58]", NomeINI))
          RR59 = val(GetInfo(Gruppo, "R[59]", NomeINI))
          Call VISUALIZZA_MOLA_PARAMETRI(RR37, RR38, RR45, RR55, "ADDIN: " & NomePezzo, 1, 1, "OEM1_HD", 0, 0, SiStampa, _
                                         RR41, RR43, RR44, RR46, RR47, RR48, RR49, _
                                         RR51, RR53, RR54, RR56, RR57, RR58, RR59)
          STAMPA_ELABORAZIONE_COMPLETATA = "Y"
        Else
          WRITE_DIALOG NomePezzo & " " & Error(53)
        End If
      
      '***************************
      Case "MOLA_DIAMETRO_ESTERNO"
      '***************************
        If (INIZIALIZZA_OFFLINE(Gruppo, NomePezzo) = "TROVATO") Then
          GoSub VISUALIZZA_FraARCHIVIO
          Altezza = val(GetInfo(Gruppo, "ALTEZZAFIANCOF1_G", NomeINI))
          Larghezza = val(GetInfo(Gruppo, "LARGMOLAPR_G", NomeINI))
          Raggio = val(GetInfo(Gruppo, "RFONDO_G", NomeINI))
          CONICITA = val(GetInfo(Gruppo, "CORRFHAF1_G", NomeINI))
          Call VISUALIZZA_MOLA_RDE(Altezza, Larghezza, Raggio, CONICITA, NomePezzo, 1, "OEM1_HD", 0, 0, SiStampa)
          STAMPA_ELABORAZIONE_COMPLETATA = "Y"
        Else
          WRITE_DIALOG NomePezzo & " " & Error(53)
        End If
        
      End Select
    Else
      WRITE_DIALOG "CODE HAVE NOT BEEN DEFINED!!!"
    End If
  Else
    OEM1.txtNomePezzo.Enabled = True
    OEM1.txtMEMO.Enabled = True
    OEM1.btnMEMO.Enabled = True
    If (OEM1.btnSTORICO.Visible = True) Then OEM1.btnSTORICO.Enabled = True
    OEM1.lblINP_HD(0).Visible = False
    OEM1.txtINP_HD(0).Visible = False
    OEM1.PicELABORA_HD.Visible = False
  End If
  '
Exit Sub
'
VISUALIZZA_FraARCHIVIO:
  NomeINI = GetInfo("Configurazione", "NomeFileTemporaneoSPF", PathFILEINI)
  NomeINI = g_chOemPATH & "\" & NomeINI & ".INI"
  If (SiStampa <> "Y") Then
    OEM1.lstSTORICO.Clear
    OEM1.chkSTORICO(0).Visible = False
    OEM1.chkSTORICO(1).Visible = False
    OEM1.chkSTORICO(2).Visible = False
    OEM1.lstSTORICO.Visible = False
    OEM1.txtNomePezzo.Enabled = False
    OEM1.txtMEMO.Enabled = False
    OEM1.btnMEMO.Enabled = False
    OEM1.btnSTORICO.Enabled = False
    OEM1.PicELABORA_HD.Left = OEM1.Image4.Left
    OEM1.PicELABORA_HD.Top = OEM1.Image4.Top + OEM1.Image4.Height
    OEM1.PicELABORA_HD.Height = OEM1.lstARCHIVIO.Height + 30
    OEM1.PicELABORA_HD.Width = OEM1.Image4.Width + OEM1.txtNomePezzo.Width
    OEM1.PicELABORA_HD.Cls
    OEM1.PicELABORA_HD.Visible = True
    '
    OEM1.txtINP_HD(0).Left = OEM1.Image4.Left + OEM1.Image4.Width + OEM1.txtNomePezzo.Width - OEM1.txtINP_HD(0).Width
    OEM1.txtINP_HD(0).Top = 0
    OEM1.txtINP_HD(0).Visible = True
    '
    OEM1.lblINP_HD(0).Left = OEM1.txtINP_HD(0).Left - OEM1.lblINP_HD(0).Width
    OEM1.lblINP_HD(0).Top = 0
    OEM1.lblINP_HD(0).Visible = True
  End If
Return

End Sub

Sub OEM1_ELABORA_FraPARAMETRI(ByVal SiStampa As String, ByRef STAMPA_ELABORAZIONE_COMPLETATA As String)
'
Dim TipoELABORA As String
Dim IndiceTASTO As Integer
Dim Gruppo      As String
Dim NomePezzo   As String
'MOLA VITI CONICHE
Dim RR37    As Double
Dim RR38    As Double
Dim RR45    As Double
Dim RR55    As Double
Dim RR41    As Double
Dim RR43    As Double
Dim RR44    As Double
Dim RR46    As Double
Dim RR47    As Double
Dim RR48    As Double
Dim RR49    As Double
Dim RR51    As Double
Dim RR53    As Double
Dim RR54    As Double
Dim RR56    As Double
Dim RR57    As Double
Dim RR58    As Double
Dim RR59    As Double
Dim RR208   As Double
'MOLA RDE
Dim Altezza   As Double
Dim Larghezza As Double
Dim Raggio    As Double
Dim CONICITA  As Double
'*********************
Dim NomeINI As String
Dim stmp    As String
Dim Atmp    As String * 255
Dim NomeSK  As String
Dim ret     As Long
Dim TIPO_LAVORAZIONE As String
'
On Error Resume Next
  '
  STAMPA_ELABORAZIONE_COMPLETATA = "N"
  If (OEM1.PicELABORA.Visible = False) Or (SiStampa = "Y") Then
    NomePezzo = OEM1.FraPARAMETRI.Caption
    NomePezzo = UCase$(Trim$(NomePezzo))
    IndiceTASTO = val(GetInfo("OEM1", "IndiceTASTO", Path_LAVORAZIONE_INI))
    Gruppo = GetInfo("OEM1", "GRUPPO(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
    Gruppo = UCase$(Trim$(Gruppo))
    If (Len(Gruppo) > 0) Then
      TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
      TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
      TipoELABORA = GetInfo("OEM1", "TipoELABORA(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
      TipoELABORA = UCase$(Trim$(TipoELABORA))
      Select Case TipoELABORA
      '***********************
      Case "MOLA_VITI_CONICHE"
      '***********************
        GoSub VISUALIZZA_FraParametri
        RR37 = val(GetInfo(Gruppo, "R[37]", NomeINI))
        RR38 = val(GetInfo(Gruppo, "R[38]", NomeINI))
        '******************************************************************************************************
        stmp = GetInfo("CNC_" & LETTERA_LAVORAZIONE, "RST_R208", g_chOemPATH & "\" & TIPO_LAVORAZIONE & ".INI")
        RR208 = val(stmp)
        RR37 = RR37 + RR208
        RR38 = RR38 + RR208
        '******************************************************************************************************
        RR45 = val(GetInfo(Gruppo, "R[45]", NomeINI))
        RR55 = val(GetInfo(Gruppo, "R[55]", NomeINI))
        RR41 = val(GetInfo(Gruppo, "R[41]", NomeINI))
        RR43 = val(GetInfo(Gruppo, "R[43]", NomeINI))
        RR44 = val(GetInfo(Gruppo, "R[44]", NomeINI))
        RR46 = val(GetInfo(Gruppo, "R[46]", NomeINI))
        RR47 = val(GetInfo(Gruppo, "R[47]", NomeINI))
        RR48 = val(GetInfo(Gruppo, "R[48]", NomeINI))
        RR49 = val(GetInfo(Gruppo, "R[49]", NomeINI))
        RR51 = val(GetInfo(Gruppo, "R[51]", NomeINI))
        RR53 = val(GetInfo(Gruppo, "R[53]", NomeINI))
        RR54 = val(GetInfo(Gruppo, "R[54]", NomeINI))
        RR56 = val(GetInfo(Gruppo, "R[56]", NomeINI))
        RR57 = val(GetInfo(Gruppo, "R[57]", NomeINI))
        RR58 = val(GetInfo(Gruppo, "R[58]", NomeINI))
        RR59 = val(GetInfo(Gruppo, "R[59]", NomeINI))
        Call VISUALIZZA_MOLA_PARAMETRI(RR37, RR38, RR45, RR55, "ADDIN: " & NomePezzo, 1, 1, "OEM1", 0, 0, SiStampa, _
                                     RR41, RR43, RR44, RR46, RR47, RR48, RR49, _
                                     RR51, RR53, RR54, RR56, RR57, RR58, RR59)
        STAMPA_ELABORAZIONE_COMPLETATA = "Y"
      
      '***************************
      Case "MOLA_DIAMETRO_ESTERNO"
      '***************************
        GoSub VISUALIZZA_FraParametri
        Altezza = val(GetInfo(Gruppo, "ALTEZZAFIANCOF1_G", NomeINI))
        Larghezza = val(GetInfo(Gruppo, "LARGMOLAPR_G", NomeINI))
        Raggio = val(GetInfo(Gruppo, "RFONDO_G", NomeINI))
        CONICITA = val(GetInfo(Gruppo, "CORRFHAF1_G", NomeINI))
        Call VISUALIZZA_MOLA_RDE(Altezza, Larghezza, Raggio, CONICITA, NomePezzo, 1, "OEM1", 0, 0, SiStampa)
        STAMPA_ELABORAZIONE_COMPLETATA = "Y"
            
      '***********************
      Case "MOLA_VITIV"
      '***********************
        GoSub VISUALIZZA_FraParametri
        OEM1.lblINP(0).Caption = "SCALE"
        OEM1.txtINP(0).Text = GetInfo("DIS0", "ScalaMOLA", Path_LAVORAZIONE_INI)
        Call VITI_ESTERNE_VISUALIZZA_MOLA_ADDIN(OEM1.PicELABORA, val(OEM1.txtINP(0).Text), val("0"), val("0"), "N", val("0"), "S400G")
        STAMPA_ELABORAZIONE_COMPLETATA = "Y"

      End Select
      
    End If
  Else
    OEM1.Text1(1).Enabled = True
    OEM1.Text2(2).Enabled = True
    OEM1.Text3(1).Enabled = True
    OEM1.Text3(2).Enabled = True
    OEM1.lblINP(0).Visible = False
    OEM1.txtINP(0).Visible = False
    OEM1.PicELABORA.Visible = False
    IndiceTASTO = val(GetInfo("OEM1", "IndiceTASTO", Path_LAVORAZIONE_INI))
    stmp = GetInfo("OEM1", "NomeINP(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI): stmp = UCase$(Trim$(stmp))
    If (stmp = "N") Then
      OEM1.imgCODICE.Visible = False
      OEM1.txtCODICE.Visible = False
    Else
      OEM1.txtCODICE.Visible = True
      OEM1.txtCODICE.Text = OEM1.FraPARAMETRI.Caption
      NomeSK = GetInfo("OEM1", "NomeSK(" & Format$(IndiceTASTO, "0") & ")", Path_LAVORAZIONE_INI)
      NomeSK = UCase$(Trim$(NomeSK))
      If (IsNumeric(NomeSK)) Then
        ret = val(NomeSK)
        ret = LoadString(g_hLanguageLibHandle, ret, Atmp, 255)
        If (ret > 0) Then NomeSK = Left$(Atmp, ret)
      End If
      ret = InStr(NomeSK, "\")
      If (ret = 1) Then
        NomeSK = Mid$(NomeSK, ret)
        OEM1.imgCODICE.Picture = LoadPicture(g_chOemPATH & "\Gapp4\images\" & NomeSK)
        OEM1.imgCODICE.Left = 0
        OEM1.imgCODICE.Top = 0
        OEM1.imgCODICE.Width = OEM1.Image3.Width
        OEM1.imgCODICE.Visible = True
        OEM1.txtCODICE.Left = OEM1.imgCODICE.Left + OEM1.imgCODICE.Width
        OEM1.txtCODICE.Top = 0
        OEM1.txtCODICE.Width = OEM1.FraPARAMETRI.Width - OEM1.imgCODICE.Width - 15
        OEM1.txtCODICE.Height = 300
        OEM1.imgCODICE.Height = OEM1.txtCODICE.Height
      Else
        If (Len(NomeSK) <= 0) Then NomeSK = "SK" & Format$(IndiceTASTO, "0")
        OEM1.imgCODICE.Picture = LoadPicture("")
        OEM1.imgCODICE.Visible = False
        OEM1.txtCODICE.Left = 0
        OEM1.txtCODICE.Top = 0
        OEM1.txtCODICE.Width = OEM1.FraPARAMETRI.Width - 15
        OEM1.txtCODICE.Height = 300
      End If
    End If
  End If
  '
Exit Sub
'
VISUALIZZA_FraParametri:
  NomeINI = GetInfo("Configurazione", "NomeFileTemporaneoSPF", PathFILEINI)
  NomeINI = g_chOemPATH & "\" & NomeINI & ".INI"
  If (SiStampa <> "Y") Then
    OEM1.imgCODICE.Visible = False
    OEM1.txtCODICE.Visible = False
    OEM1.Text1(1).Enabled = False
    OEM1.Text2(2).Enabled = False
    OEM1.Text3(1).Enabled = False
    OEM1.Text3(2).Enabled = False
    OEM1.PicELABORA.Left = OEM1.Label3(0).Left
    OEM1.PicELABORA.Top = OEM1.Label3(0).Top
    OEM1.PicELABORA.Height = OEM1.FraPARAMETRI.Height - OEM1.PicELABORA.Top
    OEM1.PicELABORA.Width = OEM1.FraPARAMETRI.Width
    OEM1.PicELABORA.Cls
    OEM1.PicELABORA.Visible = True
    '
    OEM1.txtINP(0).Left = OEM1.FraPARAMETRI.Width - OEM1.txtINP(0).Width - 30
    OEM1.txtINP(0).Top = 0
    OEM1.txtINP(0).Visible = True
    '
    OEM1.lblINP(0).Left = OEM1.txtINP(0).Left - OEM1.lblINP(0).Width
    OEM1.lblINP(0).Top = 0
    OEM1.lblINP(0).Visible = True
  End If
Return

End Sub

Sub OEM1_INTESTAZIONE(ByVal MODO As String, ByVal IMMAGINE As String)
'
Dim stmp             As String
Dim TIPO_LAVORAZIONE As String
'
On Error Resume Next
  '
  'INIZIALIZZAZIONE DELL'INDICE DELLA LAVORAZIONE SINGOLA
  OEM1.Image1.Picture = LoadPicture(g_chOemPATH & "\Gapp4\images\" & IMMAGINE)
  OEM1.Image3.Picture = LoadPicture(g_chOemPATH & "\Gapp4\images\ADDIN.bmp")
  If (MODO = "OFF-LINE") Then
  OEM1.lblINDICE(0).BackColor = QBColor(10)
  OEM1.lblINDICE(0).ForeColor = QBColor(0)
  Else
  OEM1.lblINDICE(0).BackColor = QBColor(12)
  OEM1.lblINDICE(0).ForeColor = QBColor(15)
  End If
  OEM1.lblINDICE(0).Left = OEM1.Image3.Width
  OEM1.lblINDICE(0).Top = OEM1.Image3.Top
  OEM1.lblINDICE(0).Height = OEM1.Image3.Height / 2
  OEM1.lblINDICE(0).Caption = MODO
  'LIVELLO DI ACCESSO
  OEM1.lblINDICE(1).Caption = "lvl: " & Format$(g_nAccessLevel, "#0")
  'LEGGO IL TIPO DI LAVORAZIONE IMPOSTATA
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  stmp = RICERCA_NOME_TIPO(TIPO_LAVORAZIONE)
  OEM1.INTESTAZIONE.Caption = stmp
  If (OEM1.PicERRORE.Visible = True) Then OEM1.PicERRORE.Visible = False
  '
End Sub

Sub OEM1_NASCONDO_CONTROLLI()
'
On Error Resume Next
  '
  'NON VISUALIZZO IL PRIMO GRUPPO
  OEM1.List1(0).Visible = False
  OEM1.List1(1).Visible = False
  OEM1.Label1(0).Visible = False
  OEM1.Text1(1).Visible = False
  OEM1.Label3(0).Visible = False
  OEM1.Label3(1).Visible = False
  'NON VISUALIZZO IL SECONDO GRUPPO
  OEM1.List2(0).Visible = False
  OEM1.List2(1).Visible = False
  OEM1.List2(2).Visible = False
  OEM1.Label1(1).Visible = False
  OEM1.Text2(2).Visible = False
  OEM1.Label4(0).Visible = False
  OEM1.Label4(1).Visible = False
  'NON VISUALIZZO IL TERZO GRUPPO
  OEM1.List3(0).Visible = False
  OEM1.List3(1).Visible = False
  OEM1.List3(2).Visible = False
  OEM1.Label1(2).Visible = False
  OEM1.Text3(1).Visible = False
  OEM1.Text3(2).Visible = False
  OEM1.Label5(0).Visible = False
  OEM1.Label5(1).Visible = False
  OEM1.Label5(2).Visible = False
  '
End Sub
