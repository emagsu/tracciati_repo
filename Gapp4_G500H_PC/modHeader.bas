Attribute VB_Name = "ModHeader"
Option Explicit

Public VecchioTempo  As Variant
Public lblTimer      As String
Public bTimerON      As Boolean
Public lblPercorso   As String
Public lblDisegno    As String
Public INDICE_LINGUA As String * 1

Sub AZIONI_TIMER()
'
Dim StatoApp                    As Long
Dim StatoRet                    As String
Dim i                           As Long
Dim ii                          As Long
Dim k                           As Long
Dim stmp                        As String
Dim riga                        As String
Dim PEZZO                       As String
Dim Programma                   As String
Dim ABILITAZIONE                As String
Dim INDICE_LINGUA_LOC           As String * 1
Dim BASE                        As Variant
Static VECCHIO_VALORE_CONTATORE As Long
'
On Error Resume Next
  '
  Call AlMakeBorder(0)
  'TOTALIZZATORE
  If (bTimerON = True) Then
    If (VecchioTempo <> 0) Then
      If TimeValue(Time) >= TimeValue(VecchioTempo) Then
        lblTimer = Format$(TimeValue(Time) - TimeValue(VecchioTempo), "hh:mm:ss")
      Else
        BASE = TimeValue("23:59:59") - TimeValue(VecchioTempo)
        BASE = BASE + TimeValue("00:00:01")
        lblTimer = Format$(TimeValue(Time) + TimeValue(BASE), "hh:mm:ss")
      End If
    End If
  End If
  Call VISUALIZZA_NOME_PEZZO
  i = val(OPC_LEGGI_DATO("/CHANNEL/PARAMETER/R[896]"))
  If (i <> VECCHIO_VALORE_CONTATORE) Then
    VECCHIO_VALORE_CONTATORE = i
    Select Case i
      Case 0
        'Arresto del timer
        VecchioTempo = 0
      Case 1
        'Avvio del timer
        bTimerON = True
        VecchioTempo = Time
        lblTimer = "00:00:00"
      Case 2
        bTimerON = False
        VecchioTempo = 0
      Case 99: 'VALORE NEUTRO
      Case 100
        StatoApp = Abs(val(OPC_LEGGI_DATO("/ACC/NCK/MGUD/VAL[1]")))
        StatoRet = UCase$(Trim$(OPC_LEGGI_DATO("/ACC/NCK/MGUD/STR[1]")))
        k = CONTROLLA_ESISTENZA_STATO(StatoApp)
        If (k > 0) Then
          If (StatoRet <> "") Then
            'SELEZIONE ARCHIVIO PEZZI
            Call Set_State(StatoApp)
            Call State_Reached(StatoRet, StatoApp, StatoApp)
          Else
            Call Set_State(StatoApp)
          End If
        End If
      Case 101
        'SELEZIONE DIRETTA DELLA PAGINA DI RUN
        StatoApp = 1
        'SELEZIONE STATO APPLICAZIONE GAPP4
        k = CONTROLLA_ESISTENZA_STATO(StatoApp)
        If (k > 0) Then Call Set_State(StatoApp)
      Case 102
        'SELEZIONE ARCHIVIO PEZZI
        Call Set_State(41)
        Call State_Reached("ARCHIVIO", 41, 41)
        'Seleziona il OFFLINE pezzo corrente
        'Call State_Reached("INSERIMENTO", 5, 41)
      
      Case 110
        'SELEZIONE ARCHIVIO PEZZI
        Call Set_State(0)
        Call State_Reached("PROGRAMMI", 0, 0)
      
      Case 141, 142, 143
        'Fasi 1,2,3 Cambio attrezzatura
        Call Set_State(100)
        Call STEPXX_ATTREZZATURA1(i)
      
      Case 144, 145, 146
        'Fasi 1,2,3 Cambio attrezzatura
        Call Set_State(100)
        Call STEPXX_ATTREZZATURA2(i)
      
      Case 150
        'Fase 0 Cambio mola
        Call Set_State(100)
        Call STEPXX_CAMBIOMOLA(i)
      
      Case 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172
        'Fasi 1..22 Cambio Mola
        Call Set_State(100)
        Call STEPXX_CAMBIOMOLA(i)
            
      Case 180
        Call Set_State(100)
        Call STEPXX_INIZIALIZZA("STEP05")
      
      Case 181, 182, 183
        'Fasi 1,2,3 Cambio Rullo
        Call Set_State(100)
        Call STEPXX_CAMBIORULLO(i)
            
      Case 190
        Call Set_State(100)
        'Seleziona form STEPXX con indice 1
        Call STEPXX_INIZIALIZZA("STEP01")
      
      Case 192
        Call Set_State(100)
        Call STEPXX_INIZIALIZZA("STEP02")
      
      Case 193
        Call Set_State(100)
        Call STEPXX_INIZIALIZZA("STEP03")
      
      Case 194
        Call Set_State(100)
        Call STEPXX_INIZIALIZZA("STEP04")
      
        '
'      Case 103
'        'SELEZIONE CONTATORE PEZZI
'        Call Set_State(67)
      
'      Case 104
'        'SELEZIONE ONLINE DELLA PAGINA E RICHIAMO ELABORAZIONE DATI
'        'NOTA : NON VISUALIZZA L'ICONA CALCOLATRICE
'        Call Set_State(0)
'        Call State_Reached("OEMX_ONLINE", 0, 41)
'        Call OEMX.SuOEM1.GoToPagina(1)
'        Call State_Reached("OEMX_ELABORAZIONI_DATI", 0, 0)
'        '
'        '
'      Case 105
'        'SELEZIONE OFFLINE DELLA PAGINA E RICHIAMO ELABORAZIONE DATI
'        Call Set_State(41)
'        Call State_Reached("ARCHIVIO", 41, 41)
'        Call State_Reached("INSERIMENTO", 5, 41)
'        Call OEMX.SuOEM1.GoToPagina(1)
'        Call State_Reached("OEMX_ELABORAZIONI_DATI", 5, 5)
'
        
        '
      Case 200
        '
    End Select
    '
  End If
  '
  Call AGGIORNA_LISTA_PRG
  '
End Sub

Sub AGGIORNA_LISTA_PRG()
'
Dim TIPO_LAVORAZIONE    As String
Dim stmp                As String
Dim Item_Change         As String
Dim Cambio_Stato        As String
'
On Error GoTo errAGGIORNA_LISTA_PRG
  '
  'LETTURA DEL TIPO DI LAVORAZIONE
  TIPO_LAVORAZIONE = GetInfo("Configurazione", "TIPO_LAVORAZIONE", PathFILEINI)
  TIPO_LAVORAZIONE = UCase$(Trim$(TIPO_LAVORAZIONE))
  If (TIPO_LAVORAZIONE = "MOLAVITE_SG") Or (TIPO_LAVORAZIONE = "MOLAVITE") Or (TIPO_LAVORAZIONE = "MOLAVITEG160") Then
      'Per Gestire altre Lavorazioni � necessario modificare la relativa tabella Programmi
      stmp = GetInfo(TIPO_LAVORAZIONE, "STATI_DISABILITATI", Path_LAVORAZIONE_INI)
      stmp = UCase$(Trim$(stmp))
      If (stmp = "N") Then
        Item_Change = GetInfo(TIPO_LAVORAZIONE, "CambioStato", Path_LAVORAZIONE_INI)
        Item_Change = UCase$(Trim$(Item_Change))
        Cambio_Stato = OPC_LEGGI_DATO(Item_Change)
        Cambio_Stato = UCase$(Trim$(Cambio_Stato))
        If (Cambio_Stato = "Y") Then
           stmp = TIPO_LAVORAZIONE
           Call VISUALIZZA_PROGRAMMI_DISPONIBILI(stmp)
           Call VISUALIZZA_AZIONI_DISPONIBILI(TIPO_LAVORAZIONE)
           Cambio_Stato = "N"
           Call OPC_SCRIVI_DATO(Item_Change, Cambio_Stato)
        End If
      End If
  End If
  '
Exit Sub

errAGGIORNA_LISTA_PRG:
  WRITE_DIALOG "Sub AGGIORNA_LISTA_PRG: ERROR -> " & Err
  Resume Next
  
  Exit Sub


End Sub

Sub VISUALIZZA_NOME_PEZZO()
'
Dim sDDE                  As String
Dim sMACRO                As String
Dim MACRO                 As Long
Dim sLAVORAZIONE          As String
Dim LAVORAZIONE           As Long
Dim TIPO_LAVORAZIONE      As String
Dim NomePezzo             As String
Dim Path_LAVORAZIONE_INI  As String
Dim stmp                  As String
Dim NomeMACRO             As String
Dim COMBINAZIONI_DB       As String
Dim sNOME_PROGRAMMA       As String
Dim ret                   As Long
Dim Atmp                  As String * 255
'
On Error Resume Next
  '
  sDDE = GetInfo("Configurazione", "sDDE_MACRO", PathFILEINI)
  If (Len(sDDE) > 0) Then
    '
    'LETTURA DEL VALORE MACRO
    sMACRO = OPC_LEGGI_DATO(sDDE): MACRO = val(sMACRO)
    If (MACRO < 0) Then
      '
      'LAVORAZIONE SINGOLA: IL VALORE DI MACRO CORRISPONDE ALLA SEQUENZA
      'DI TIPO_LAVORAZIONE DICHIARATA IN GAPP4.INI CAMBIATA DI SEGNO
      '
      'LETTURA DEL VALORE LAVORAZIONE
      sDDE = GetInfo("Configurazione", "sDDE_LAVORAZIONE_SINGOLA", PathFILEINI)
      If (Len(sDDE) > 0) Then
        sLAVORAZIONE = OPC_LEGGI_DATO(sDDE): LAVORAZIONE = val(sLAVORAZIONE)
      Else
        LAVORAZIONE = 0
      End If
      If (LAVORAZIONE > 0) Then
        'INDICE PROGRAMMA  SINGOLO
        MACRO = Abs(MACRO)
        'LETTURA DEL TIPO LAVORAZIONE
        TIPO_LAVORAZIONE = GetInfo("TIPO_LAVORAZIONE", "TIPO(" & Format$(MACRO, "####0") & ")", PathFILEINI)
        TIPO_LAVORAZIONE = Trim$(UCase$(TIPO_LAVORAZIONE))
        'LETTURA DEL PERCORSO FILE INI
        Path_LAVORAZIONE_INI = GetInfo("TIPO_LAVORAZIONE", "FILEINI(" & Format$(MACRO, "####0") & ")", PathFILEINI)
        Path_LAVORAZIONE_INI = g_chOemPATH & "\" & Path_LAVORAZIONE_INI & ".INI"
        'LETTURA DEL NOME DEL PEZZO SINGOLO
        NomePezzo = GetInfo(TIPO_LAVORAZIONE, "NomePezzo(" & sLAVORAZIONE & ")", Path_LAVORAZIONE_INI)
        NomePezzo = UCase$(Trim$(NomePezzo))
        'LEGGO IL NOME DEL TIPO DAL FILE INI
        stmp = GetInfo("TIPO_LAVORAZIONE", "NOMETIPO(" & Format$(MACRO, "####0") & ")", PathFILEINI)
        If IsNumeric(stmp) Then
          ret = val(stmp)
          ret = LoadString(g_hLanguageLibHandle, ret, Atmp, 255)
          If ret > 0 Then stmp = Left$(Atmp, ret) Else stmp = "No name at " & stmp
        End If
        'COSTRUISCO LA STRINGA DI VISUALIZZAZIONE
        NomePezzo = stmp & "--> [" & Chr(64 + LAVORAZIONE) & "] " & NomePezzo
      Else
        'COMPATIBILITA' CON IL VECCHIO HEADER
        NomePezzo = GetInfo("Configurazione", "NomePezzo", PathFILEINI)
      End If
      '
    ElseIf (MACRO = 0) Then
      '
      'COMPATIBILITA' CON IL VECCHIO HEADER
      NomePezzo = GetInfo("Configurazione", "NomePezzo", PathFILEINI)
      '
    ElseIf (MACRO >= 1) Then
      '
      'LAVORAZIONE COMBINATA: IL VALORE DI MACRO CORRISPONDE ALLA SEQUENZA
      ' DELLE COMBINAZIONI DICHIARATA DEL FILE GAPP4.INI
      '
      'LETTURA DEL NOME PEZZO DELLA COMBINAZIONE
      NomePezzo = GetInfo("COMBINAZIONE(" & sMACRO & ")", "NomePezzo", PathFILEINI)
      NomePezzo = UCase$(Trim$(NomePezzo))
      'LETTURA NOME TIPO
      NomeMACRO = GetInfo("COMBINAZIONE(" & sMACRO & ")", "NomeMACRO", PathFILEINI)
      NomeMACRO = UCase$(Trim$(NomeMACRO))
      'LETTURA DEL VALORE LAVORAZIONE
      sDDE = GetInfo("Configurazione", "sDDE_LAVORAZIONE_SINGOLA", PathFILEINI)
      If (Len(sDDE) > 0) Then
        sLAVORAZIONE = OPC_LEGGI_DATO(sDDE): LAVORAZIONE = val(sLAVORAZIONE)
      Else
        LAVORAZIONE = 0
      End If
      If (LAVORAZIONE > 0) Then
        'LETTURA DEL TIPO LAVORAZIONE
        TIPO_LAVORAZIONE = GetInfo("COMBINAZIONE(" & sMACRO & ")", "TIPO(" & sLAVORAZIONE & ")", PathFILEINI)
        'LETTURA DEL PERCORSO FILE INI
        Path_LAVORAZIONE_INI = GetInfo("COMBINAZIONE(" & sMACRO & ")", "FILEINI(" & sLAVORAZIONE & ")", PathFILEINI)
        Path_LAVORAZIONE_INI = g_chOemPATH & "\" & Path_LAVORAZIONE_INI & ".INI"
        'LETTURA DEL NOME DEL PEZZO SINGOLO
        stmp = GetInfo(TIPO_LAVORAZIONE, "NomePezzo(" & sLAVORAZIONE & ")", Path_LAVORAZIONE_INI)
        stmp = UCase$(Trim$(stmp))
        'COSTRUISCO LA STRINGA DI VISUALIZZAZIONE
        NomePezzo = "[" & sMACRO & "] " & NomeMACRO & " ( " & NomePezzo & " -> [" & Chr(64 + LAVORAZIONE) & "] " & stmp & " )"
      End If
      '
    End If
    '
  Else
    '
    'COMPATIBILITA' CON IL VECCHIO HEADER
    NomePezzo = GetInfo("Configurazione", "NomePezzo", PathFILEINI)
    '
  End If
  lblDisegno = " " & NomePezzo
  '
End Sub
