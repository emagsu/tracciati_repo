﻿Imports System.Text
Imports System.Math
Imports Siemens.Sinumerik.Operate.Services
Imports System.Runtime.InteropServices
Imports System.Reflection
Imports System.IO.Compression


Module Module1

    'Public MyHandle As IntPtr
    Public MyHandle As Long
    Public vVecchioTempo As Long
    Public ItemDDE_CONTATORE As String
    Public g_chOemPATH As String

    Public sLog As String
    Public sItem As String
    Public sValore As String
    Public bLog As Boolean = False
    Public WM_BERTOLDI As Long
    Public CheckFile As Boolean = True
    Public Tracciati4_0 As Boolean
    Public Path_PRODUZIONE_INI As String

    Public Const HWND_TOPMOST = -1
    Public Const HWND_NOTOPMOST = -2
    Public Const SWP_NOMOVE = &H2
    Public Const SWP_NOSIZE = &H1
    Public Const SWP_NOACTIVATE = &H10
    Public Const SWP_SHOWWINDOW = &H40
    Public Const FLAGS = SWP_NOMOVE Or SWP_NOSIZE

    'EDGE
    Public VarEdgeState As String
    Public MyEdgeState As Long
    Public PercorsoSambaServer As String
    Public JobName(5) As String
    Public JobNameNoZip As String
    Public JobStart As Boolean
    Public JobPath As String
    Public FromSambaToOutside As String
    Public AttesaScambio As Integer


    Public Declare Function SendMessage Lib "user32.dll" Alias "SendMessageA" (ByVal winHandle As Int32, _
        ByVal wMsg As Int32, ByVal wParam As Int32, ByVal lParam As Int32) As Int32
    Public Declare Function PostMessage Lib "user32.dll" Alias "PostMessageA" (ByVal winHandle As Long, _
    ByVal wMsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
    Private Declare Auto Function GetPrivateProfileString Lib "kernel32" (ByVal lpAppName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As StringBuilder, ByVal nSize As Integer, ByVal lpFileName As String) As Integer
    Private Declare Auto Function WritePrivateProfileString Lib "kernel32" (ByVal lpAppName As String, ByVal lpKeyName As String, ByVal lpString As String, ByVal lpFileName As String) As Boolean
    Public Declare Function GetParent Lib "user32" (ByVal hWnd As IntPtr) As IntPtr
    Public Declare Function GetForegroundWindow Lib "user32" () As IntPtr
    Public Declare Function SetForegroundWindow Lib "user32" (ByVal hwnd As Long) As IntPtr
    Public Declare Function SetWindowPos Lib "user32" (ByVal hwnd As Long, ByVal hWndInsertAfter As Long, ByVal x As Long, ByVal y As Long, ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long) As Long
    Public Declare Auto Function GetWindowThreadProcessId Lib "user32" (ByVal hwnd As IntPtr, ByRef lpdwProcessId As Integer) As Integer
    Public Declare Auto Function FindWindow Lib "user32.dll" (ByVal lpClassName As String, ByVal lpWindowName As String) As IntPtr
    Public Declare Function RegisterWindowMessage Lib "user32" Alias "RegisterWindowMessageA" (ByVal lpString As String) As Long

    Public Sub WriteINI(ByVal Section As String, ByVal Key As String, ByVal Value As String)

        WritePrivateProfileString(Section, Key, Value, Application.StartupPath & "\WinMgr.ini")

    End Sub

    Function GetINI(ByVal sSection As String, ByVal sKey As String) As String

        Dim res As Integer
        Dim sb As StringBuilder

        sb = New StringBuilder(500)
        res = GetPrivateProfileString(sSection, sKey, "", sb, sb.Capacity, Application.StartupPath & "\WinMgr.ini")

        GetINI = sb.ToString



    End Function

    Function GetINFO(ByVal sSection As String, ByVal sKey As String, ByVal sPATH As String) As String

        Dim res As Integer
        Dim sb As StringBuilder

        sb = New StringBuilder(500)
        res = GetPrivateProfileString(sSection, sKey, "", sb, sb.Capacity, sPATH)

        GetINFO = sb.ToString

    End Function

    Sub ToLog(ByVal sMessage As String, Optional ByVal bWrite As Boolean = False, Optional ByVal bReset As Boolean = False)

        If bReset Then sLog = ""
        sLog = sLog & Now & " : " & sMessage & vbCrLf

        If bWrite And bLog Then System.IO.File.AppendAllText(Application.StartupPath & "\WinMgr.log", sLog)
        Form1.Label1.Text = Now & " : " & sMessage

    End Sub

    Function LEGGI_NOME_PEZZO() As String

        Dim sDDE As String
        Dim sMACRO As String
        Dim MACRO As Long
        Dim sLAVORAZIONE As String
        Dim LAVORAZIONE As Long
        Dim TIPO_LAVORAZIONE As String

        Dim Path_LAVORAZIONE_INI As String
        Dim stmp As String
        Dim NomeMACRO As String
        Dim COMBINAZIONI_DB As String
        Dim sNOME_PROGRAMMA As String

        On Error Resume Next

        sDDE = GetINFO("Configurazione", "sDDE_MACRO", g_chOemPATH & "\GAPP4.INI")
        If (Len(sDDE) > 0) Then
            'LETTURA DEL VALORE MACRO
            sMACRO = OPC_LEGGI_DATO(sDDE) : MACRO = Val(sMACRO)
            If (MACRO < 0) Then
                'LAVORAZIONE SINGOLA: IL VALORE DI MACRO CORRISPONDE ALLA SEQUENZA
                'DI TIPO_LAVORAZIONE DICHIARATA IN GAPP4.INI CAMBIATA DI SEGNO
                'LETTURA DEL VALORE LAVORAZIONE
                sDDE = GetINFO("Configurazione", "sDDE_LAVORAZIONE_SINGOLA", g_chOemPATH & "\GAPP4.INI")
                If (Len(sDDE) > 0) Then
                    sLAVORAZIONE = OPC_LEGGI_DATO(sDDE)
                    LAVORAZIONE = Val(sLAVORAZIONE)
                Else
                    sLAVORAZIONE = "0"
                    LAVORAZIONE = 0
                End If
                If (LAVORAZIONE > 0) Then
                    'INDICE PROGRAMMA  SINGOLO
                    MACRO = Abs(MACRO)
                    'LETTURA DEL TIPO LAVORAZIONE
                    TIPO_LAVORAZIONE = GetINFO("TIPO_LAVORAZIONE", "TIPO(" & Format$(MACRO, "####0") & ")", g_chOemPATH & "\GAPP4.INI")
                    TIPO_LAVORAZIONE = Trim$(UCase$(TIPO_LAVORAZIONE))
                    'LETTURA DEL PERCORSO FILE INI
                    Path_LAVORAZIONE_INI = GetINFO("TIPO_LAVORAZIONE", "FILEINI(" & Format$(MACRO, "####0") & ")", g_chOemPATH & "\GAPP4.INI")
                    Path_LAVORAZIONE_INI = g_chOemPATH & "\" & Path_LAVORAZIONE_INI & ".INI"
                    'LETTURA DEL NOME DEL PEZZO SINGOLO
                    LEGGI_NOME_PEZZO = GetINFO(TIPO_LAVORAZIONE, "NomePezzo(" & sLAVORAZIONE & ")", Path_LAVORAZIONE_INI)
                    LEGGI_NOME_PEZZO = UCase$(Trim$(LEGGI_NOME_PEZZO))
                Else
                    'COMPATIBILITA' CON IL VECCHIO HEADER
                    LEGGI_NOME_PEZZO = GetINFO("Configurazione", "NomePezzo", g_chOemPATH & "\GAPP4.INI")
                End If
            ElseIf (MACRO = 0) Then
                'COMPATIBILITA' CON IL VECCHIO HEADER
                LEGGI_NOME_PEZZO = GetINFO("Configurazione", "NomePezzo", g_chOemPATH & "\GAPP4.INI")
            ElseIf (MACRO >= 1) Then
                'LAVORAZIONE COMBINATA: IL VALORE DI MACRO CORRISPONDE ALLA SEQUENZA
                ' DELLE COMBINAZIONI DICHIARATA DEL FILE GAPP4.INI
                'LETTURA DEL NOME PEZZO DELLA COMBINAZIONE
                LEGGI_NOME_PEZZO = GetINFO("COMBINAZIONE(" & sMACRO & ")", "NomePezzo", g_chOemPATH & "\GAPP4.INI")
                LEGGI_NOME_PEZZO = UCase$(Trim$(LEGGI_NOME_PEZZO))
            End If
        Else
            'COMPATIBILITA' CON IL VECCHIO HEADER
            LEGGI_NOME_PEZZO = GetINFO("Configurazione", "NomePezzo", g_chOemPATH & "\GAPP4.INI")
        End If

    End Function

    Function OPC_LEGGI_DATO_PC(ByVal sOPC As String) As String

        On Error Resume Next

        OPC_LEGGI_DATO_PC = GetINFO("VARIABILI_STATO", sOPC, g_chOemPATH & "\CNC.INI")
        OPC_LEGGI_DATO_PC = Replace(OPC_LEGGI_DATO_PC, ",", ".")
        If UCase(Trim(OPC_LEGGI_DATO_PC)) = "TRUE" Then OPC_LEGGI_DATO_PC = 1
        If UCase(Trim(OPC_LEGGI_DATO_PC)) = "FALSE" Then OPC_LEGGI_DATO_PC = 0

    End Function

    Private Function GudConv(ByVal sNome As String) As String

        GudConv = sNome

        Dim i1 As Integer
        Dim i2 As Integer
        Dim newidx As Integer
        Dim sVar As String

        sVar = sNome.Replace("/ACC/NCK/MGUD/", "/NC/_N_NC_GD2_ACX/")
        sVar = sVar.Replace("/ACC/NCK/UGUD/", "/NC/_N_NC_GD3_ACX/")
        sVar = sVar.Replace("/ACC/NCK/GUD7/", "/NC/_N_NC_GD7_ACX/")

        If (sNome <> sVar) Then
            i1 = sVar.LastIndexOf("[")
            i2 = sVar.LastIndexOf("]")
            If (i1 > 0) Then
                newidx = CInt(sVar.Substring(i1 + 1, i2 - i1 - 1)) - 1
                sVar = sVar.Substring(0, i1 + 1) & newidx & "]"
            End If
        End If
        GudConv = sVar

    End Function

    Function OPC_LEGGI_DATO(ByVal sOPC As String) As String

        Dim MyData As DataSvc = Nothing

        Try
            MyData = New DataSvc()

            Dim itemsRead(0) As Item

            sOPC = GudConv(sOPC)

            itemsRead(0) = New Item(sOPC)
            MyData.Read(itemsRead)
            OPC_LEGGI_DATO = itemsRead(0).Value.ToString()

        Catch ex As DataSvcException
            OPC_LEGGI_DATO = "DataSvcException: ErrorNr: " & ex.ErrorNumber.ToString() & " ; Message: " & ex.Message

        Catch ex As Exception
            OPC_LEGGI_DATO = ex.Message

        End Try

    End Function

    Public Sub OPC_SCRIVI_DATO(ByVal sOPC As String, ByVal Valore As String)

        Dim MyData As DataSvc = Nothing

        Try
            MyData = New DataSvc()

            Dim itemsRead(0) As Item

            sOPC = GudConv(sOPC)
            itemsRead(0) = New Item(sOPC)
            itemsRead(0).Value = Valore

            MyData.Write(itemsRead)

        Catch ex As DataSvcException

        Catch ex As Exception

        End Try

    End Sub

    Sub OPC_SCRIVI_DATO_PC(ByVal sOPC As String, ByVal Valore As String)

        On Error Resume Next

        Valore = Replace(Valore, ",", ".")
        WritePrivateProfileString("VARIABILI_STATO", sOPC, Valore, g_chOemPATH & "\CNC.INI")

    End Sub

    Function frmt(ByVal Valore As Double, ByVal Ndec As Integer) As String

        Dim SEGNO As String
        Dim AA As String
        Dim sfmt As String
        Dim BB As Long
        Dim decim As Integer
        Dim INTERA As String
        Dim DECIMALE As String

        On Error GoTo errfrmt
        decim = Ndec
        'RICAVO IL SEGNO DI Valore
        If (Valore >= 0) Then SEGNO = "+" Else SEGNO = "-"
        'If (Valore >= 0) Then SEGNO = "" Else SEGNO = "-"
        'CONSIDERO SOLO IL VALORE ASSOLUTO
        Valore = Abs(Valore)
        'COSTRUISCO LA STRINGA DI FORMATO
        If (Ndec = 0) Then Ndec = 1
        sfmt = "#######0." & StrDup(Ndec - 1, "#") & "0"
        'FORMATTO IL VALORE
        AA = Format$(Valore, sfmt)
        'CONTROLLO SE IL SEPARATORE E' LA VIRGOLA
        BB = InStr(AA, ",")
        If (BB = 0) Then
            'CONTROLLO SE IL SEPARATORE E' IL PUNTO
            BB = InStr(AA, ".")
        End If
        INTERA = Left$(AA, BB - 1)
        DECIMALE = Mid$(AA, BB + 1)
        If (Right$(DECIMALE, 1) = "0") Then DECIMALE = Left$(DECIMALE, Len(DECIMALE) - 1)
        If (Right$(DECIMALE, 1) = "0") Then DECIMALE = Left$(DECIMALE, Len(DECIMALE) - 1)
        If (Val(DECIMALE) >= 0) And (decim > 0) Then
            frmt = SEGNO & INTERA & "." & DECIMALE
        Else
            frmt = SEGNO & INTERA
        End If

        Exit Function

errfrmt:
        MsgBox("ERRORE", , "WARNING")
        frmt = Format(Valore, sfmt)
        Exit Function

    End Function

    Public Sub AGGIORNA_OUTPUT(ByVal TIPO_LAVORAZIONE As String, ByVal Path_LAVORAZIONE_INI As String, ByRef rigaOUTPUT As String)
        '
        Dim i As Integer
        Dim j As Integer
        Dim k1 As Integer
        Dim k2 As Integer
        Dim k2I As Integer
        Dim k2D As Integer
        Dim k3 As Integer
        Dim stmp1 As String
        Dim stmp2 As String
        Dim LUNGHEZZA() As String
        Dim iSEQ() As String
        Dim pINT As String
        Dim pDEC As String
        Dim Formato As String
        On Error Resume Next

        Formato = GetINFO("Configurazione", "Formato_file", Path_PRODUZIONE_INI)

        Select Case TIPO_LAVORAZIONE

            Case "ROTORI_ESTERNI"
                'FORMATO RICHIESTO X GT500H-FINI
                stmp1 = GetINFO("Configurazione", "LUNGHEZZE", Path_PRODUZIONE_INI)
                LUNGHEZZA = Split(stmp1, ";")
                stmp1 = GetINFO("Configurazione", "lblOUTPUT", Path_PRODUZIONE_INI)
                iSEQ = Split(stmp1, ";")
                k1 = 1 : rigaOUTPUT = ""
                For i = 0 To UBound(iSEQ)
                    ' Modifica 2018: Corrispondenza fra indici e lunghezza.
                    'Call RICAVO_INDICI(LUNGHEZZA(i), k2I, k2D)
                    'k2 = k2I + k2D
                    'j = Val(iSEQ(i))
                    j = Val(iSEQ(i))
                    Call RICAVO_INDICI(LUNGHEZZA(j), k2I, k2D)
                    k2 = k2I + k2D

                    stmp1 = GetINFO("Configurazione", "ItemDDE(" & Format$(j, "#0") & ")", Path_PRODUZIONE_INI)
                    stmp1 = Trim$(stmp1)
                    If (Len(stmp1) > 0) Then
                        stmp2 = OPC_LEGGI_DATO(stmp1)
                        ' Modifica 2018: Un record di output corrisponde ad un pezzo lavorato.
                        ' Il contatore PEZZIRET viene aggiornato in RR1_RET.MPF per visualizzare i pezzi lavorati
                        ' Nella cassetta degli attrezzi.
                        If stmp1 = "/ACC/NCK/UGUD/PEZZIRET" Then
                            stmp2 = "1"
                        End If
                        If (k2D > 0) Then
                            k3 = InStr(stmp2, ".")
                            If (k3 > 0) Then
                                'SEPARAZIONE INTERA E DECIMALE
                                pINT = Left$(stmp2, k3 - 1) : pDEC = Mid$(stmp2, k3 + 1, k2D)
                                'PRENDO LA PARTE INTERA, AGGIUNGO LE CIFRE FINALI E "0"
                                stmp2 = pINT & pDEC
                                If (Len(pDEC) < k2D) Then
                                    stmp2 = stmp2 & STRINGA(k2D - Len(pDEC), "0")
                                End If
                            Else
                                'AGGIUNGO LE CIFRE DOPO IL PUNTO
                                stmp2 = stmp2 & STRINGA(k2D, "0")
                            End If
                            'AGGIUNGO GLI ZERI PRIMA DEL PUNTO PER COMPLETARE IL CAMPO
                            stmp2 = STRINGA(k2 - Len(stmp2), "0") & stmp2
                        Else
                            'AGGIUNGO GLI SPAZI A SINISTRA PER COMPLETARE IL CAMPO
                            'stmp2 = StringA(k2 - Len(stmp2), " ") & stmp2
                            'Modifica 2018: AGGIUNGO GLI SPAZI A DESTRA PER COMPLETARE IL CAMPO
                            stmp2 = stmp2 & STRINGA(k2 - Len(stmp2), " ")

                        End If
                    Else
                        stmp2 = STRINGA(k2, "X")
                    End If
                    k1 = k1 + k2
                    rigaOUTPUT = rigaOUTPUT & stmp2
                Next i
            Case "MOLAVITE"
                If Formato = "CSV" Then
                    rigaOUTPUT = ""
                    stmp1 = GetINFO("Configurazione", "lblOUTPUT", Path_PRODUZIONE_INI)
                    iSEQ = Split(stmp1, ";")
                    rigaOUTPUT = ""
                    For i = 0 To UBound(iSEQ)
                        Dim TestStr As String
                        Dim TestStr1 As String
                        TestStr = "ITEMDDE(" & Format$(iSEQ(i)) & ")"
                        TestStr1 = "ITEMDDE(" & Trim(Str(iSEQ(i))) & ")"

                        stmp1 = GetINFO("Configurazione", "ITEMDDE(" & Format$(iSEQ(i)) & ")", Path_PRODUZIONE_INI)
                        stmp1 = Trim$(stmp1)
                        If (Len(stmp1) > 0) Then
                            stmp2 = OPC_LEGGI_DATO(stmp1)


                        Else
                            stmp2 = STRINGA(k2, "X")
                        End If
                        If i = UBound(iSEQ) Then
                            rigaOUTPUT = rigaOUTPUT & stmp2
                        Else
                            rigaOUTPUT = rigaOUTPUT & stmp2 & ";"
                        End If

                    Next i
                End If

        End Select

    End Sub

    Function STRINGA(ByVal K As Integer, ByVal CARATTERE As String) As String
        '
        Dim i As Integer
        '
        STRINGA = ""
        If (K >= 1) Then
            For i = 1 To K
                STRINGA = STRINGA & CARATTERE
            Next i
        End If

    End Function

    Sub RICAVO_INDICI(ByVal sLUNGHEZZA As String, ByRef k2I As Integer, ByRef k2D As Integer)
        '
        Dim kk2 As Integer
        '
        On Error Resume Next
        '
        If (IsNumeric(sLUNGHEZZA)) Then
            k2I = Val(sLUNGHEZZA) : k2D = 0
        Else
            kk2 = InStr(sLUNGHEZZA, "+")
            If (kk2 > 0) Then
                k2I = Val(Mid$(sLUNGHEZZA, 1, kk2 - 1)) : k2D = Val(Mid$(sLUNGHEZZA, kk2 + 1, Len(sLUNGHEZZA) - kk2))
            Else
                k2I = Val(sLUNGHEZZA) : k2D = 0
            End If
        End If

    End Sub
    Sub GESTIONE_FILEINPUT(ByVal TIPO_LAVORAZIONE As String, ByVal Path_LAVORAZIONE_INI As String)
        Try
            Dim NomeFileScambio As String
            Dim NomeFileSpot As String
            Dim NomeFilePiano As String


            Dim PercorsoScambio As String
            Dim PercorsoPiano As String

MyStart:

            NomeFilePiano = GetINFO("Configurazione", "NomeFilePIANO", Path_PRODUZIONE_INI)
            NomeFileScambio = GetINFO("Configurazione", "NomeFileSCAMBIO", Path_PRODUZIONE_INI)
            NomeFileSpot = GetINFO("Configurazione", "NomeFileSPOT", Path_PRODUZIONE_INI)

            ToLog("Gestione File di Input. File di scambio:" & NomeFileScambio & "File piano di produzione: " & NomeFilePiano, True, True)

            PercorsoScambio = GetINFO("Configurazione", "PERCORSO_SCAMBIO", Path_PRODUZIONE_INI)
            PercorsoPiano = g_chOemPATH & "\PRODUZIONE\" & TIPO_LAVORAZIONE
            With My.Computer.FileSystem
                'Verifico la presenza del file per lo scambio dati
                If (.FileExists(PercorsoScambio & "\" & NomeFileScambio)) Then
                    'Rinomino il file
                    Threading.Thread.Sleep(1000)
                    .RenameFile(PercorsoScambio & "\" & NomeFileScambio, NomeFilePiano)
                    ToLog("Ho rinominato il file", True, True)
                    'Trasferisco il file nella cartella di lettura - Refresh 
                    Threading.Thread.Sleep(1000)
                    .CopyFile(PercorsoScambio & "\" & NomeFilePiano, PercorsoPiano & "\" & NomeFilePiano, True)
                    ToLog("Ho copiato il file", True, True)
                    'Cancello il file
                    Threading.Thread.Sleep(1000)
                    .DeleteFile(PercorsoScambio & "\" & NomeFilePiano)
                    ToLog("Ho cancellato il file", True, True)
                End If

                If (.FileExists(PercorsoScambio & "\" & NomeFileSpot)) Then
                    ' Rinomino il file di scambio - procedura spot 
                    Threading.Thread.Sleep(1000)
                    .RenameFile(PercorsoScambio & "\" & NomeFileSpot, NomeFilePiano)
                    'Leggo il contenuto del file
                    Threading.Thread.Sleep(1000)
                    Dim MyRighe As String
                    MyRighe = System.IO.File.ReadAllText(PercorsoScambio & "\" & NomeFilePiano)
                    'Aggiorno il file del piano di produzione 
                    System.IO.File.AppendAllText(PercorsoPiano & "\" & NomeFilePiano, MyRighe)

                    'Cancello il file 
                    Threading.Thread.Sleep(1000)
                    .DeleteFile(PercorsoScambio & "\" & NomeFilePiano)
                End If

            End With
        Catch ex As System.IO.IOException
            Call ToLog("MyStart +" & ex.ToString, True, True)
            Threading.Thread.Sleep(1000)
            GoTo MyStart
        Catch ex As Exception
            Call ToLog(ex.ToString, True, True)

        End Try

    End Sub


End Module
