﻿Imports System.Text
Imports System.Math
Imports Siemens.Sinumerik.Operate.Services
Imports System.Runtime.InteropServices
Imports System.Reflection
'
Imports System.IO
Imports Microsoft.VisualBasic
Imports System.Security.Permissions





Public Class Form1


    <PermissionSet(SecurityAction.Demand, Name:="FullTrust")> _
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim tstr As String
        If (UCase(GetINI("Config", "Log")) = "Y") Then bLog = True Else bLog = False
        Me.WindowState = FormWindowState.Minimized
        Call ToLog("Application started!!", True, True)
        '
        g_chOemPATH = GetINI("Config", "PERCORSO_GAPP4")
        '
        tstr = GetINI("Config", "TimerInterval")
        If (Val(tstr) <> 0) Then
            Call ToLog("Timer enabled: " & Val(tstr) & "ms", True, True)
            Timer1.Interval = Val(tstr)
            Timer1.Enabled = True
        Else
            Call ToLog("Timer NOT enabled: " & Val(tstr) & "ms", True, True)
            Timer1.Enabled = False
        End If
        tstr = GetINI("Config", "Item")
        If (tstr <> "") Then tstr = GetINI("Config", "Valore")

        ' *****

        Dim PercorsoScambio As String
        Dim TIPO_LAVORAZIONE As String
        Dim Path_LAVORAZIONE_INI As String


        'LETTURA DEL TIPO LAVORAZIONE
        TIPO_LAVORAZIONE = GetINFO("Configurazione", "TIPO_LAVORAZIONE", g_chOemPATH & "\GAPP4.INI")
        TIPO_LAVORAZIONE = Trim$(UCase$(TIPO_LAVORAZIONE))
        'INIZIALIZZAZIONE DEL PERCORSO DEL FILE INI DELLA LAVORAZIONE SINGOLA
        Path_LAVORAZIONE_INI = GetINFO("Configurazione", "FILEINI", g_chOemPATH & "\GAPP4.INI")
        Path_LAVORAZIONE_INI = UCase$(Trim$(Path_LAVORAZIONE_INI)) & ".INI"
        Path_LAVORAZIONE_INI = g_chOemPATH & "\" & Path_LAVORAZIONE_INI
        '
        Path_PRODUZIONE_INI = g_chOemPATH & "\PRODUZIONE.INI"


        'Dim MyTmp As String
        'MyTmp = GetINFO("Configurazione", "ABILITATRACCIATIINDUSTRIA4.0", Path_LAVORAZIONE_INI)
        'If (MyTmp = "Y") Then
        '    Tracciati4_0 = True
        'Else
        '    Tracciati4_0 = False
        'End If
        'If Tracciati4_0 Then



        Try
            Dim MyWatcherExchange As New FileSystemWatcher()
            Dim MyWatcherMessage As New FileSystemWatcher()

            'EDGE
            VarEdgeState = GetINI("Config", "VariabileEDGE")
            FromSambaToOutside = GetINI("Config", "PercorsoOutside")
            PercorsoSambaServer = GetINI("Config", "PercorsoSambaServer")
            AttesaScambio = GetINI("Config", "TempoAttesa")
            ToLog("EdgeConfig: " & VarEdgeState & " " & FromSambaToOutside & " " & PercorsoSambaServer, True, True)

            Try
                Threading.Thread.Sleep(5000)
                Dim MyEdgeWatcher As New FileSystemWatcher()
                MyEdgeWatcher.Path = PercorsoSambaServer
                MyEdgeWatcher.NotifyFilter = (NotifyFilters.LastAccess Or NotifyFilters.LastWrite Or NotifyFilters.FileName Or NotifyFilters.DirectoryName)
                MyEdgeWatcher.Filter = "*.zip"
                AddHandler MyEdgeWatcher.Created, AddressOf OnCreated_Edge
                AddHandler MyEdgeWatcher.Changed, AddressOf OnChanged_Edge

                ' Begin watching.
                MyEdgeWatcher.EnableRaisingEvents = True
                ToLog("Start Watching " & PercorsoSambaServer, True, True)
            Catch ex As Exception
                ToLog(ex.ToString, True, True)
            End Try




            PercorsoScambio = GetINFO("Configurazione", "PERCORSO_SCAMBIO", Path_PRODUZIONE_INI)
            MyWatcherExchange.Path = PercorsoScambio
            MyWatcherMessage.Path = g_chOemPATH & "\PRODUZIONE\" & TIPO_LAVORAZIONE
            ' Watch for changes in LastAccess and LastWrite times, and
            ' the renaming of files or directories. 
            MyWatcherExchange.NotifyFilter = (NotifyFilters.LastAccess Or NotifyFilters.LastWrite Or NotifyFilters.FileName Or NotifyFilters.DirectoryName)
            MyWatcherMessage.NotifyFilter = (NotifyFilters.LastAccess Or NotifyFilters.LastWrite Or NotifyFilters.FileName Or NotifyFilters.DirectoryName)

            ' Only watch text files.

            'MyWatcherExchange.Filter = "*.txt"
            MyWatcherMessage.Filter = "PIANO_PRODUZIONE.txt"

            ' Add event handlers.
            AddHandler MyWatcherMessage.Changed, AddressOf OnChanged_Message
            AddHandler MyWatcherMessage.Created, AddressOf OnChanged_Message
            AddHandler MyWatcherExchange.Created, AddressOf OnChanged_Exchange
            AddHandler MyWatcherExchange.Deleted, AddressOf OnChanged_Exchange


            ' Begin watching.
            MyWatcherExchange.EnableRaisingEvents = True
            MyWatcherMessage.EnableRaisingEvents = True

            ' *****

            ToLog("Start Watching " & PercorsoScambio & "Tipo lavorazione: " & Path_LAVORAZIONE_INI, True, True)



            WM_BERTOLDI = RegisterWindowMessage("WM_BERTOLDI")
        Catch ex As Exception
            Call ToLog(ex.ToString, True, True)
        End Try
        ' End If

    End Sub
    '    <DllImport("user32.dll", CharSet:=CharSet.Auto)> _
    'Public Shared Function SendMessage(ByVal hwnd As IntPtr, ByVal Msg As Integer, ByVal wParam As Integer, ByVal lParam As Integer) As IntPtr


    '    End Function
    Sub ProblemaOEMFRAME()

        Try
            Dim sCurrentArea As String = ""
            Dim iHandle As Long
            Dim sWinName As String = ""
            '
            Call ToLog("OEM Frame problem has been detected!")
            sCurrentArea = Infrastructure.CurrentActiveArea
            sWinName = GetINI("Applicazioni", sCurrentArea)
            Call ToLog("Current Area: " & sCurrentArea)
            '
            If (Trim(sWinName) = "") Then Exit Sub
            If (Trim(UCase(sWinName)).StartsWith("CLASSNAME:")) Then
                sWinName = Mid(sWinName, InStr(sWinName, ":") + 1)
                iHandle = FindWindow(sWinName, Nothing)
                Call ToLog("Classname: " & sWinName & " - Handle: " & iHandle)
            Else
                sWinName = Mid(sWinName, InStr(sWinName, ":") + 1)
                iHandle = FindWindow(Nothing, sWinName)
                Call ToLog("Windowname: " & sWinName & " - Handle: " & iHandle)
            End If
            If (iHandle <> 0) Then Call RipristinaAPP(iHandle)
            '
        Catch ex As Exception
            Call ToLog("NC connection failure", True, True)
            '
        End Try

    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        Static UNA_VOLTA As Integer

        Call ToLog("", False, True)
        If (UCase(GetINI("Config", "End")) = "Y") Then
            WriteINI("Config", "End", "N")
            End
        End If
        Call ProblemaOEMFRAME()
        If (UNA_VOLTA = 0) Then Timer2.Enabled = True : UNA_VOLTA = 1

    End Sub

    Sub RipristinaAPP(ByVal hwnd As Long)

        Try
            Dim tHwnd As Long
            '
            tHwnd = GetForegroundWindow
            If (hwnd <> tHwnd) Then
                If (hwnd <> GetParent(tHwnd)) Then
                    Call SetForegroundWindow(hwnd)
                    'Call SetWindowPos(hwnd, HWND_TOPMOST, 0, 0, 0, 0, FLAGS)
                    Call ToLog("SetForeground executed!", True)
                    'Call ToLog("SetWindowPos executed!", True)
                Else
                    Call ToLog("MessageBox has been detected!", True)
                End If
            Else
                Call ToLog("", False, True)
            End If
            '
        Catch ex As Exception
            Call ToLog("Error detected! Searching for active window....", True)
            '
        End Try

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        Call ProblemaOEMFRAME()

    End Sub

    Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick

        Dim i As Long
        Static ii As Long
        Dim k As Long
        Dim stmp As String
        Dim stmp1 As String
        Dim stmp2 As String
        Dim riga As String
        Dim PEZZO As String
        Dim Programma As String
        Static VECCHIO_VALORE_CONTATORE As Long
        Dim ItemDDE_PARZIALE As String
        Dim HH As Integer
        Dim MM As Integer
        Dim SS As Integer
        Dim INTERVALLO As Long
        Dim PERCORSO_CONTROLLI As String
        '
        Dim Codice As String
        Dim j As Long
        Dim COMMENTO As String
        Dim RAGGIO_FONDO As String
        Dim PASSO As String
        Dim INTERASSE As String
        Dim Valori() As String
        Dim EE() As String
        Dim ItemDDE_ERRORI As String
        Dim ItemDDE_PASSO As String
        Dim ItemDDE_INTERASSE As String
        Dim TIPO_LAVORAZIONE As String
        Dim Path_LAVORAZIONE_INI As String
        Dim NomeWINMGR As String
        Dim NomeOUTPUT As String
        Dim NomeCartellaOUT As String
        '
        On Error Resume Next
        '
        'CALCOLO INTERVALLO DI RETTIFICA E VISUALIZZAZIONE IN lblTIMER
        If (vVecchioTempo > 0) Then
            'CALCOLO IN SECONDI DEL VALORE DI INTERVALLO INTERCORSO
            INTERVALLO = Microsoft.VisualBasic.TimeValue(Now.ToLongTimeString).Hour * 60 * 60 + Microsoft.VisualBasic.TimeValue(Now.ToLongTimeString).Minute * 60 + Microsoft.VisualBasic.TimeValue(Now.ToLongTimeString).Second - vVecchioTempo
            'PASSAGGIO DA 24:00:00 A 00:00:00 DEL GIORNO DOPO
            If (INTERVALLO < 0) Then
                INTERVALLO = INTERVALLO + 24 * 60 * 60
            End If
            HH = (INTERVALLO / 3600)
            MM = (INTERVALLO - HH * 3600) / 60
            'RIMOZIONE DELL'ORA IN ECCESSO
            If (MM < 0) Then
                HH = HH - 1
                MM = (INTERVALLO - HH * 3600) / 60
            End If
            SS = (INTERVALLO - HH * 3600 - MM * 60)
            'RIMOZIONE DEL MINUTO IN ECCESSO
            If (SS < 0) Then
                MM = MM - 1
                SS = (INTERVALLO - HH * 3600 - MM * 60)
            End If
            lblTIMER.Text = HH.ToString & ":" & MM.ToString & ":" & SS.ToString
        End If
        '
        'LETTURA DEL VALORE DEL PARAMETRO CN R[896]
        i = Val(OPC_LEGGI_DATO("/CHANNEL/PARAMETER/R[896]"))
        If (i <> VECCHIO_VALORE_CONTATORE) Then
            '************** DEBUG *************************************************************************
            'Call ToLog("R896: " & Format$(i, "####0"), True, True)
            'If bLog Then Programma = Trim$(UCase$(OPC_LEGGI_DATO("/Channel/ProgramPointer/progName[u1]")))
            'Call ToLog("Programma: " & Programma, True, True)
            'If bLog Then ItemDDE_PARZIALE = Trim$(UCase$(GetINI(Programma, "PARZIALE")))
            'Call ToLog("ItemDDE_PARZIALE: " & ItemDDE_PARZIALE, True, True)
            '**********************************************************************************************
            VECCHIO_VALORE_CONTATORE = i
            Select Case i
                Case 0
                    'LETTURA DEL NOME DEL PROGRAMMA PEZZO SELEZIONATO SUL CN
                    Programma = Trim$(UCase$(OPC_LEGGI_DATO("/Channel/ProgramPointer/progName[u1]")))
                    'LETTURA DELL'ITEM PER LA LETTURA DEL CONTATORE PEZZO
                    ItemDDE_PARZIALE = Trim$(UCase$(GetINI(Programma, "PARZIALE")))
                    If (ItemDDE_PARZIALE <> "") Then
                        'LETTURA DEL NOME DEL PEZZO
                        PEZZO = Trim$(UCase$(GetINFO("Configurazione", "NomePezzo", g_chOemPATH & "\GAPP4.INI")))
                        'LETTURA VALORE DEL PARAMETRO CN DEL CONTATORE PEZZI
                        ii = Val(OPC_LEGGI_DATO(ItemDDE_PARZIALE))
                        'LETTURA DEL TIPO LAVORAZIONE
                        TIPO_LAVORAZIONE = GetINFO("Configurazione", "TIPO_LAVORAZIONE", g_chOemPATH & "\GAPP4.INI")
                        TIPO_LAVORAZIONE = Trim$(UCase$(TIPO_LAVORAZIONE))
                        'INIZIALIZZAZIONE DEL PERCORSO DEL FILE INI DELLA LAVORAZIONE SINGOLA
                        Path_LAVORAZIONE_INI = GetINFO("Configurazione", "FILEINI", g_chOemPATH & "\GAPP4.INI")
                        Path_LAVORAZIONE_INI = UCase$(Trim$(Path_LAVORAZIONE_INI)) & ".INI"
                        Path_LAVORAZIONE_INI = g_chOemPATH & "\" & Path_LAVORAZIONE_INI
                        'GESTIONE FORMATO OUTPUT RICHIESTA DA FINI
                        NomeWINMGR = GetINFO("Configurazione", "NomeWINMGR", Path_PRODUZIONE_INI)
                        NomeWINMGR = UCase$(Trim$(NomeWINMGR))
                        NomeCartellaOUT = GetINFO("Configurazione", "NomeCartellaOUT", Path_PRODUZIONE_INI)
                        If (NomeWINMGR <> "") Then

                            'SCRITTURA --> FORMATO GT500H-FINI
                            Call AGGIORNA_OUTPUT(TIPO_LAVORAZIONE, Path_LAVORAZIONE_INI, riga)
                            'riga = Replace(riga, " ", "0") 'SOSTITUZIONE DEGLI SPAZI CON LO ZERO
                            System.IO.File.AppendAllText(g_chOemPATH & "\PRODUZIONE\" & TIPO_LAVORAZIONE & "\" & NomeCartellaOUT & "\" & NomeWINMGR, riga & vbCrLf)

                        Else
                            'SCRITTURA --> pezzo, indice, data, tempo finale, tempo
                            stmp1 = ""
                            stmp1 = stmp1 & Microsoft.VisualBasic.DateValue(Now.ToLongDateString).Day & ":"
                            stmp1 = stmp1 & Microsoft.VisualBasic.DateValue(Now.ToLongDateString).Month & ":"
                            stmp1 = stmp1 & Microsoft.VisualBasic.DateValue(Now.ToLongDateString).Year
                            stmp2 = Now.ToLongTimeString
                            riga = "[" & PEZZO & "] " & Format$(ii, "000000") & " | " & stmp1 & " | " & stmp2 & " | " & lblTIMER.Text & vbCrLf
                            System.IO.File.AppendAllText(g_chOemPATH & "\PRODUZIONE.TXT", riga)
                        End If
                    End If
        'ARRESTO
        vVecchioTempo = 0
                Case 1
                    'AVVIO
                    vVecchioTempo = Microsoft.VisualBasic.TimeValue(Now.ToLongTimeString).Hour * 60 * 60 + Microsoft.VisualBasic.TimeValue(Now.ToLongTimeString).Minute * 60 + Microsoft.VisualBasic.TimeValue(Now.ToLongTimeString).Second
                Case 40
                    'LETTURA DEL TIPO LAVORAZIONE
                    TIPO_LAVORAZIONE = GetINFO("Configurazione", "TIPO_LAVORAZIONE", g_chOemPATH & "\GAPP4.INI")
                    TIPO_LAVORAZIONE = Trim$(UCase$(TIPO_LAVORAZIONE))
                    'INIZIALIZZAZIONE DEL PERCORSO DEL FILE INI DELLA LAVORAZIONE SINGOLA
                    Path_LAVORAZIONE_INI = GetINFO("Configurazione", "FILEINI", g_chOemPATH & "\GAPP4.INI")
                    Path_LAVORAZIONE_INI = UCase$(Trim$(Path_LAVORAZIONE_INI)) & ".INI"
                    Path_LAVORAZIONE_INI = g_chOemPATH & "\" & Path_LAVORAZIONE_INI



                    NomeWINMGR = GetINFO("Configurazione", "NomeWINMGR", Path_PRODUZIONE_INI)
                    NomeWINMGR = UCase$(Trim$(NomeWINMGR))
                    NomeCartellaOUT = GetINFO("Configurazione", "NomeCartellaOUT", Path_PRODUZIONE_INI)
                    NomeOUTPUT = GetINFO("Configurazione", "NomeFileOUT", Path_PRODUZIONE_INI)
                    riga = ""
                    Call AGGIORNA_OUTPUT(TIPO_LAVORAZIONE, Path_LAVORAZIONE_INI, riga)

                    System.IO.File.AppendAllText(NomeCartellaOUT & NomeOUTPUT, riga & vbCrLf)



                Case 1000
                    'SCRITTURA RIGA DI TESTO
                Case 1001
                    'CANCELLAZIONE FILE DI TESTO
            End Select
        End If
        '

        'AUTOCORREZIONE X SEIM CON CONTROLLORE DEA
        PERCORSO_CONTROLLI = UCase$(Trim$(GetINI("CONTROLLO", "PERCORSO_CONTROLLI")))
        If (Len(PERCORSO_CONTROLLI) > 0) Then
            'AGGIUNGO IL PEZZO MANCANTE
            PERCORSO_CONTROLLI = g_chOemPATH & PERCORSO_CONTROLLI
            COMMENTO = ""
            RAGGIO_FONDO = ""
            PASSO = ""
            INTERASSE = ""
            Codice = LEGGI_NOME_PEZZO()
            lblPERCORSO.Text = PERCORSO_CONTROLLI & "\" & Codice & ".CHK"
            If (Dir$(PERCORSO_CONTROLLI & "\" & Codice & ".CHK") <> "") Then
                'SPOSTAMENTO DEL FILE DEL RISULTATO DAL DEPOSITO AL DIRETTORIO DI LAVORO
                If (Dir$(g_chOemPATH & "\" & Codice & ".CHK") <> "") Then Call Kill(g_chOemPATH & "\" & Codice & ".CHK")
                Call FileCopy(PERCORSO_CONTROLLI & "\" & Codice & ".CHK", g_chOemPATH & "\" & Codice & ".CHK")
                Call Kill(PERCORSO_CONTROLLI & "\" & Codice & ".CHK")
                'LETTURA DEL RISULTATO
                FileOpen(1, g_chOemPATH & "\" & Codice & ".CHK", OpenMode.Input, OpenAccess.Read)
                Do
                    'LEGGO LA PRIMA RIGA E LA PULISCO
                    stmp = LineInput(1) : stmp = UCase$(Trim$(stmp))
                    'ESTRAZIONE DEL COMMENTO
                    j = InStr(stmp, "COMMENT =")
                    If (j > 0) Then
                        j = InStr(stmp, "=")
                        COMMENTO = Microsoft.VisualBasic.Right(stmp, Len(stmp) - j)
                    End If
                    'ESTRAZIONE RAGGIO INTERNO
                    j = InStr(stmp, "ROOT POINT =")
                    If (j > 0) Then
                        j = InStr(stmp, "=")
                        RAGGIO_FONDO = Microsoft.VisualBasic.Right(stmp, Len(stmp) - j)
                    End If
                    'ESTRAZIONE PASSO
                    j = InStr(stmp, "PASSO =")
                    If (j > 0) Then
                        j = InStr(stmp, "=")
                        PASSO = Microsoft.VisualBasic.Right(stmp, Len(stmp) - j)
                    End If
                    'ESTRAZIONE INTERASSE
                    j = InStr(stmp, "INTERASSE =")
                    If (j > 0) Then
                        j = InStr(stmp, "=")
                        INTERASSE = Microsoft.VisualBasic.Right(stmp, Len(stmp) - j)
                    End If
                    'CONTROLLO SE SONO ARRIVATO ALLA SEZIONE DATI
                    j = InStr(stmp, "$POINTS")
                Loop While (j <= 0) Or EOF(1)
                ItemDDE_ERRORI = GetINI("CONTROLLO", "ItemDDE_ERRORI")
                ItemDDE_ERRORI = UCase$(Trim$(ItemDDE_ERRORI))
                'ESTRAZIONI ERRORI SUI PUNTI
                ReDim EE(1) : j = 2 'SCRITTURA DA ERRORI[0,1]
                Do
                    ReDim Preserve EE(j)
                    'LEGGO LA PRIMA RIGA E LA PULISCO
                    stmp = LineInput(1) : stmp = UCase$(Trim$(stmp))
                    Valori = Split(stmp, ",")
                    EE(j) = Valori(6)
                    If (Val(EE(j)) > Val(EE(0))) Then EE(0) = EE(j) 'errore massimo
                    j = j + 1
                Loop While Not EOF(1)
                FileClose(1)
                'INVIO DELLA CORREZIONE DI PASSO NELLA MEMORIA CNC
                If (Val(PASSO) <> 0) Then
                    ItemDDE_PASSO = GetINI("CONTROLLO", "ItemDDE_PASSO")
                    ItemDDE_PASSO = UCase$(Trim$(ItemDDE_PASSO))
                    Call OPC_SCRIVI_DATO(ItemDDE_PASSO, PASSO)
                End If
                'INVIO DELLA CORREZIONE DI INTERASSE NELLA MEMORIA CNC
                If (Val(INTERASSE) <> 0) Then
                    ItemDDE_INTERASSE = GetINI("CONTROLLO", "ItemDDE_INTERASSE")
                    ItemDDE_INTERASSE = UCase$(Trim$(ItemDDE_INTERASSE))
                    Call OPC_SCRIVI_DATO(ItemDDE_INTERASSE, INTERASSE)
                End If
                'INVIO DELLA CORREZIONE DEI PUNTI PROFILO NELLA MEMORIA CNC
                If (Val(EE(0)) <> 0) Then
                    'SCRITTURA DI ERRORI[0,1] SU ERRORI[0,0]
                    Call OPC_SCRIVI_DATO(ItemDDE_ERRORI & "[1]", EE(2))
                    j = 2 'SCRITTURA DA ERRORI[0,1]
                    For j = 2 To UBound(EE)
                        Call OPC_SCRIVI_DATO(ItemDDE_ERRORI & "[" & Format$(j, "####0") & "]", EE(j))
                    Next j
                    'SCRITTURA DI ERRORI[0,j] SU ERRORI[0,j+1]
                    j = UBound(EE)
                    Call OPC_SCRIVI_DATO(ItemDDE_ERRORI & "[" & Format$(j + 1, "####0") & "]", EE(j))
                End If
            End If
        Else
            lblPERCORSO.Text = ""
        End If


        'Monitoraggio stato EDGE
        'MyEdgeState = Val(OPC_LEGGI_DATO(VarEdgeState))
        'If (MyEdgeState = 0) And JobStart Then
        '    JobStart = False
        '    With My.Computer.FileSystem

        '        Threading.Thread.Sleep(1000)
        '        .CopyFile(JobPath, FromSambaToOutside & "\" & JobName, True)
        '        ToLog("Ho copiato il file " & JobName, True, True)


        '    End With
        'End If



    End Sub

    ' Define the event handlers.
    Public Shared Sub OnChanged_Exchange(ByVal source As Object, ByVal e As FileSystemEventArgs)
        ' Specify what is done when a file is changed, created, or deleted
        ' Se il file viene creato, richiamo la gestione dello scambio dei dati.
        If e.ChangeType = WatcherChangeTypes.Created Then
            Dim TIPO_LAVORAZIONE As String
            Dim Path_LAVORAZIONE_INI As String
            'LETTURA DEL TIPO LAVORAZIONE
            TIPO_LAVORAZIONE = GetINFO("Configurazione", "TIPO_LAVORAZIONE", g_chOemPATH & "\GAPP4.INI")
            TIPO_LAVORAZIONE = Trim$(UCase$(TIPO_LAVORAZIONE))
            'INIZIALIZZAZIONE DEL PERCORSO DEL FILE INI DELLA LAVORAZIONE SINGOLA
            Path_LAVORAZIONE_INI = GetINFO("Configurazione", "FILEINI", g_chOemPATH & "\GAPP4.INI")
            Path_LAVORAZIONE_INI = UCase$(Trim$(Path_LAVORAZIONE_INI)) & ".INI"
            Path_LAVORAZIONE_INI = g_chOemPATH & "\" & Path_LAVORAZIONE_INI

            Call GESTIONE_FILEINPUT(TIPO_LAVORAZIONE, Path_LAVORAZIONE_INI)


        End If
        If e.ChangeType = WatcherChangeTypes.Deleted Then

        End If

    End Sub

    Public Shared Sub OnRenamed_Exchange(ByVal source As Object, ByVal e As RenamedEventArgs)
        ' Specify what is done when a file is renamed.

    End Sub
    Public Shared Sub OnChanged_Message(ByVal source As Object, ByVal e As FileSystemEventArgs)
        ' Specify what is done when a file is changed, created, or deleted

        If e.ChangeType = WatcherChangeTypes.Changed Or e.ChangeType = WatcherChangeTypes.Created Then
            Try


                MyHandle = FindWindow(Nothing, "Gapp4")
                If MyHandle <> 0 Then

                    Call PostMessage(MyHandle, WM_BERTOLDI, 0, 1)
                End If
            Catch ex As Exception
                Dim MyErr As String

                MyErr = ex.ToString



            End Try


        End If

    End Sub
    Public Shared Sub OnCreated_Edge(ByVal source As Object, ByVal e As FileSystemEventArgs)

        ToLog("Scatenato Evento OnCreated_Edge", True, True)
        If e.ChangeType = WatcherChangeTypes.Created Then
            ToLog(e.FullPath, True, True)
            JobName = Split(e.FullPath, "\")
            JobNameNoZip = Replace(JobName(UBound(JobName)), ".zip", "")

            ToLog(JobName(UBound(JobName)) & Str(UBound(JobName)), True, True)
            JobStart = True
            JobPath = e.FullPath
            ToLog("Condizione per lo scambio file. " & JobName(UBound(JobName)) & " " & JobPath, True, True)
            ToLog("Destinazione :" & FromSambaToOutside & "\" & JobName(UBound(JobName)), True, True)
            With My.Computer.FileSystem

                .CopyFile(g_chOemPATH & "\PARAMETRI1\OEM0_CICLI85.SPF", FromSambaToOutside & "\" & JobNameNoZip & "_CYCLES.txt", True)
                .CopyFile(g_chOemPATH & "\PARAMETRI1\OEM1_BIAS85.SPF", FromSambaToOutside & "\" & JobNameNoZip & "_BIAS.txt", True)
                .CopyFile(g_chOemPATH & "\PARAMETRI1\OEM1_MACCHINA85.SPF", FromSambaToOutside & "\" & JobNameNoZip & "_MACHINE.txt", True)
                .CopyFile(g_chOemPATH & "\PARAMETRI1\OEM1_PROFILOK85.SPF", FromSambaToOutside & "\" & JobNameNoZip & "_KPROFILE.txt", True)
                .CopyFile(g_chOemPATH & "\PARAMETRI1\OEM1_CORR85.SPF", FromSambaToOutside & "\" & JobNameNoZip & "_CORR.txt", True)
                .CopyFile(g_chOemPATH & "\PARAMETRI1\OEM1_LAVORO85.SPF", FromSambaToOutside & "\" & JobNameNoZip & "_WORK.txt", True)
                .CopyFile(g_chOemPATH & "\PARAMETRI1\OEM1_PROFILO85.SPF", FromSambaToOutside & "\" & JobNameNoZip & "_PROFILE.txt", True)
                .CopyFile(g_chOemPATH & "\PARAMETRI1\OEM1_MOLA85.SPF", FromSambaToOutside & "\" & JobNameNoZip & "_WHEEL.txt", True)
                .CopyFile(g_chOemPATH & "\PARAMETRI1\OEM1_RULLO85.SPF", FromSambaToOutside & "\" & JobNameNoZip & "_ROLLER.txt", True)


                Threading.Thread.Sleep(AttesaScambio)
                .CopyFile(e.FullPath, FromSambaToOutside & "\" & JobName(UBound(JobName)), True)
                ToLog("Ho copiato il file ", True, True)


            End With


        End If


    End Sub
    Public Shared Sub OnChanged_Edge(ByVal source As Object, ByVal e As FileSystemEventArgs)

        ToLog("Scatenato Evento OnChanged_Edge", True, True)
        If e.ChangeType = WatcherChangeTypes.Changed Then
            'ToLog(e.FullPath, True, True)
            JobName = Split(e.FullPath, "\")
            JobNameNoZip = Replace(JobName(UBound(JobName)), ".zip", "")

            ToLog(JobName(UBound(JobName)) & Str(UBound(JobName)), True, True)
            JobStart = True
            JobPath = e.FullPath
            'ToLog("Condizione per lo scambio file. " & JobName(UBound(JobName)) & " " & JobPath, True, True)
            'ToLog("Destinazione :" & FromSambaToOutside & "\" & JobName(UBound(JobName)), True, True)
            With My.Computer.FileSystem

                .CopyFile(g_chOemPATH & "\PARAMETRI1\OEM0_CICLI85.SPF", FromSambaToOutside & "\" & JobNameNoZip & "_CYCLES.txt", True)
                .CopyFile(g_chOemPATH & "\PARAMETRI1\OEM1_BIAS85.SPF", FromSambaToOutside & "\" & JobNameNoZip & "_BIAS.txt", True)
                .CopyFile(g_chOemPATH & "\PARAMETRI1\OEM1_MACCHINA85.SPF", FromSambaToOutside & "\" & JobNameNoZip & "_MACHINE.txt", True)
                .CopyFile(g_chOemPATH & "\PARAMETRI1\OEM1_PROFILOK85.SPF", FromSambaToOutside & "\" & JobNameNoZip & "_KPROFILE.txt", True)
                .CopyFile(g_chOemPATH & "\PARAMETRI1\OEM1_CORR85.SPF", FromSambaToOutside & "\" & JobNameNoZip & "_CORR.txt", True)
                .CopyFile(g_chOemPATH & "\PARAMETRI1\OEM1_LAVORO85.SPF", FromSambaToOutside & "\" & JobNameNoZip & "_WORK.txt", True)
                .CopyFile(g_chOemPATH & "\PARAMETRI1\OEM1_PROFILO85.SPF", FromSambaToOutside & "\" & JobNameNoZip & "_PROFILE.txt", True)
                .CopyFile(g_chOemPATH & "\PARAMETRI1\OEM1_MOLA85.SPF", FromSambaToOutside & "\" & JobNameNoZip & "_WHEEL.txt", True)
                .CopyFile(g_chOemPATH & "\PARAMETRI1\OEM1_RULLO85.SPF", FromSambaToOutside & "\" & JobNameNoZip & "_ROLLER.txt", True)

                Threading.Thread.Sleep(AttesaScambio + 1000)
                .CopyFile(e.FullPath, FromSambaToOutside & "\" & JobName(UBound(JobName)), True)
                ToLog("Ho copiato il file " & JobName(UBound(JobName)), True, True)

            End With


        End If


    End Sub

End Class
